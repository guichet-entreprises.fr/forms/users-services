var cerfaFields = {};

var civNomPrenom = $ds092PE3.etatCivil.identificationDeclarant.civilite + ' ' + $ds092PE3.etatCivil.identificationDeclarant.nomUsage + ' ' + $ds092PE3.etatCivil.identificationDeclarant.prenomsDeclarant;

//Identité Déclarant
cerfaFields['madame']                        = Value('id').of($ds092PE3.etatCivil.identificationDeclarant.civilite).eq("madame") ? true : false;
cerfaFields['monsieur']                      = Value('id').of($ds092PE3.etatCivil.identificationDeclarant.civilite).eq("monsieur") ? true : false;
cerfaFields['nomUsage']                      = $ds092PE3.etatCivil.identificationDeclarant.nomUsage;
cerfaFields['nomNaissance']                  = $ds092PE3.etatCivil.identificationDeclarant.nomNaissance;
cerfaFields['lieuNaissance']                 = $ds092PE3.etatCivil.identificationDeclarant.lieuNaissance;
cerfaFields['dateNaissance']                 = $ds092PE3.etatCivil.identificationDeclarant.dateNaissance;
cerfaFields['prenomsDeclarant']              = $ds092PE3.etatCivil.identificationDeclarant.prenomsDeclarant;

//Identification Entreprise
cerfaFields['denominationSociale']          = $ds092PE3.identificationEntreprise.identificationEntreprise.denominationSociale;
cerfaFields['siret']                        = $ds092PE3.identificationEntreprise.identificationEntreprise.siret;
cerfaFields['formeJuridique']               = $ds092PE3.identificationEntreprise.identificationEntreprise.formeJuridique;
cerfaFields['etablissementPrincipal']       = Value('id').of($ds092PE3.identificationEntreprise.identificationEntreprise.typeEtablissement).eq("etablissementPrincipal") ? true : false;
cerfaFields['etablissementSecondaire']      = Value('id').of($ds092PE3.identificationEntreprise.identificationEntreprise.typeEtablissement).eq("etablissementSecondaire") ? true : false;
cerfaFields['adresseEntreprise']            = $ds092PE3.identificationEntreprise.identificationEntreprise.adresseEntreprise;
cerfaFields['codePostalDeclarant']          = $ds092PE3.identificationEntreprise.identificationEntreprise.codePostalDeclarant != null ? $ds092PE3.identificationEntreprise.identificationEntreprise.codePostalDeclarant : '' ;
cerfaFields['villeEntreprise']              = $ds092PE3.identificationEntreprise.identificationEntreprise.villeEntreprise;
cerfaFields['telephoneEntreprise']              = $ds092PE3.identificationEntreprise.identificationEntreprise.villeEntreprise;

var mailEntreprise = $ds092PE3.telephoneEntreprise.telephoneEntreprise.mailEntreprise;
	cerfaFields['debutMail']              = '';
	cerfaFields['finMail']                 = '';
if(mailEntreprise != null) {
	var adresseSplite = mailEntreprise.split("@");
	cerfaFields['debutMail']              = adresseSplite[0];
	cerfaFields['finMail']                 = adresseSplite[1];

}

//Activités
cerfaFields['surveillance']         =  Value('id').of($ds092PE3.activite.activite0.activite1).eq("surveillance") ? true : false;
cerfaFields['videoProtection']      =  Value('id').of($ds092PE3.activite.activite0.activite1).eq("videoProtection") ? true : false;
cerfaFields['sureteAero']           =  Value('id').of($ds092PE3.activite.activite0.activite1).eq("sureteAero") ? true : false;
cerfaFields['transportFonds']       =  Value('id').of($ds092PE3.activite.activite0.activite1).eq("transportFonds") ? true : false;
cerfaFields['maintenanceGestion']   =  Value('id').of($ds092PE3.activite.activite0.activite1).eq("maintenanceGestion") ? true : false;
cerfaFields['protectionPhysique']   =  Value('id').of($ds092PE3.activite.activite0.activite1).eq("protectionPhysique") ? true : false;
cerfaFields['recherches']           =  Value('id').of($ds092PE3.activite.activite0.activite1).eq("recherches") ? true : false;

//Sollicitation associés
cerfaFields['associationAutorisation']   =  Value('id').of($ds092PE3.associes.associes1.associes2).eq("associationAutorisation") ? true : false;
cerfaFields['nonAssociation']            =  Value('id').of($ds092PE3.associes.associes1.associes2).eq("nonAssociation") ? true : false;
cerfaFields['nombreAssocies']            = $ds092PE3.associes.associes1.nombreAssocies != null ? $ds092PE3.associes.associes1.nombreAssocies : '' ;

//Dirigeant Gérant
cerfaFields['titulaireAgrement']       =  Value('id').of($ds092PE3.identificationDirigeant.identificationDirigeant1.dirigeantGerant).eq("titulaireAgrement") ? true : false;
cerfaFields['nonTitulaire']            =  Value('id').of($ds092PE3.identificationDirigeant.identificationDirigeant1.dirigeantGerant).eq("nonTitulaire") ? true : false;
cerfaFields['numeroCNAPS']            = $ds092PE3.identificationDirigeant.identificationDirigeant1.numeroCNAPS != null ? $ds092PE3.identificationDirigeant.identificationDirigeant1.numeroCNAPS : '' ;


//Signature
cerfaFields['signatureDeclarant']            = civNomPrenom;
cerfaFields['lieuSignature']                 = $ds092PE3.signatureGroup.signature.lieuSignature;
cerfaFields['dateSignature']                 = $ds092PE3.signatureGroup.signature.dateSignature;
cerfaFields['declarationHonneur']            = $ds092PE3.signatureGroup.signature.declarationHonneur;
cerfaFields['certificationHonneur']          = $ds092PE3.signatureGroup.signature.certificationHonneur;
cerfaFields['certificationCoupableFraude']   = $ds092PE3.signatureGroup.signature.certificationCoupableFraude;

//Annexe 2 Déclaration A FAIRE
cerfaFields['denominationSocialeDeclaration']      = $ds092PE3.identificationEntreprise.identificationEntreprise.denominationSociale;
cerfaFields['aucuneParticipation']                 =  Value('id').of($ds092PE3.declarationRelative.declarationRelative.participationFinanciere).eq("aucuneParticipation") ? true : false;
cerfaFields['participation']                       =  Value('id').of($ds092PE3.declarationRelative.declarationRelative.participationFinanciere).eq("participation") ? true : false;
for (var i= 0; i < $ds092PE3.declarationRelative.declarationRelative.size(); i++ ){
	cerfaFields['denominationSociale'+i]           = $ds044PE1.declarationRelative.declarationRelative[i].denominationSociale != null ? $ds044PE1.declarationRelative.declarationRelative[i].denominationSociale : '' ;
	cerfaFields['num'+i]                           = $ds044PE1.declarationRelative.declarationRelative[i].num != null ? $ds044PE1.declarationRelative.declarationRelative[i].num : '';
	cerfaFields['activite'+i]           = $ds044PE1.declarationRelative.declarationRelative[i].activite != null ? $ds044PE1.declarationRelative.declarationRelative[i].activite : '' ;
	cerfaFields['parts'+i]                           = $ds044PE1.declarationRelative.declarationRelative[i].parts != null ? $ds044PE1.declarationRelative.declarationRelative[i].parts : '';
	
}
cerfaFields['lieuSignatureDeclaration']                 = $ds092PE3.declarationRelative.declarationRelative.lieuSignatureDeclaration;
cerfaFields['dateSignatureDeclaration']                 = $ds092PE3.declarationRelative.declarationRelative.dateSignatureDeclaration;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $ds092PE3.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */


//finalDoc.append(accompDoc.save('courrier.pdf'));


var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		date: $ds092PE3.signatureGroup.signature.dateSignature,
		autoriteHabilitee : "CNAPS",
		demandeContexte : "Demande d’agrément pour le dirigeant ou le gérant d’une entreprise de sécurité privée",
		civiliteNomPrenom : civNomPrenom
	});
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Formulaire DEMANDE D’AGRÉMENT DIRIGEANT.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
 
/*
 * ID, titre séjour ou Bulletin
 */
var pj=$ds092PE3.etatCivil.identificationDeclarant.nationalite;
if(Value('id').of(pj).contains('france') || Value('id').of(pj).contains('membreUE')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
}

var pj=$ds092PE3.etatCivil.identificationDeclarant.nationalite;
if(Value('id').of(pj).contains('accordBilateral')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjTitreSejour);
}

var pj=$ds092PE3.etatCivil.identificationDeclarant.nationalite;
if(Value('id').of(pj).contains('horsUE')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjCopieBulletin);
}


appendPj($attachmentPreprocess.attachmentPreprocess.pjAptitudeProfessionnelle);
appendPj($attachmentPreprocess.attachmentPreprocess.pjFormation);

var pj=$ds092PE3.domaineActivite.domaineActivite0.activitePrivee;
if(Value('id').of(pj).contains('ouiRegle')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjExercice);
}




/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Telesurveillance_securite_electronique.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Télésurveillance et sécurité électronique - Demande d’agrément pour le dirigeant ou le gérant d’une entreprise de sécurité privée.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande d’agrément pour le dirigeant ou le gérant d’une entreprise de sécurité privée pour l\'activité de télésurveillance et sécurité électronique.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
