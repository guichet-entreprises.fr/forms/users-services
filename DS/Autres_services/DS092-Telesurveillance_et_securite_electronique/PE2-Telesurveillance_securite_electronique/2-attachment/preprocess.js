var pj=$ds092PE1.etatCivil.identificationDeclarant.nationalite;
if(Value('id').of(pj).contains('france') || Value('id').of(pj).contains('membreUE')) {
    attachment('pjID', 'pjID', { mandatory:"true"});
}

var pj=$ds092PE1.etatCivil.identificationDeclarant.nationalite;
if(Value('id').of(pj).contains('accordBilateral')) {
    attachment('pjTitreSejour', 'pjTitreSejour', { mandatory:"true"});
}

var pj=$ds092PE1.etatCivil.identificationDeclarant.nationalite;
if(Value('id').of(pj).contains('horsUE')) {
    attachment('pjCopieBulletin', 'pjCopieBulletin', { mandatory:"true"});
}

