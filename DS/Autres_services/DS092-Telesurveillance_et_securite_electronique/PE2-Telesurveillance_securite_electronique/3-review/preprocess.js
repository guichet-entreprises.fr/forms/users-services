var cerfaFields = {};

var civNomPrenom = $ds092PE1.etatCivil.identificationDeclarant.civilite + ' ' + $ds092PE1.etatCivil.identificationDeclarant.nomUsage + ' ' + $ds092PE1.etatCivil.identificationDeclarant.prenomsDeclarant;
var civ2 = $ds092PE1.etatCivil.identificationDeclarant.nomUsage + ' ' + $ds092PE1.etatCivil.identificationDeclarant.prenomsDeclarant;
//Identité Déclarant
cerfaFields['madame']                        = Value('id').of($ds092PE1.etatCivil.identificationDeclarant.civilite).eq("madame") ? true : false;
cerfaFields['monsieur']                      = Value('id').of($ds092PE1.etatCivil.identificationDeclarant.civilite).eq("monsieur") ? true : false;
cerfaFields['nomUsage']                      = $ds092PE1.etatCivil.identificationDeclarant.nomUsage;
cerfaFields['nomNaissance']                  = $ds092PE1.etatCivil.identificationDeclarant.nomNaissance;
cerfaFields['lieuNaissance']                 = $ds092PE1.etatCivil.identificationDeclarant.lieuNaissance;
cerfaFields['dateNaissance']                 = $ds092PE1.etatCivil.identificationDeclarant.dateNaissance;
cerfaFields['prenomsDeclarant']              = $ds092PE1.etatCivil.identificationDeclarant.prenomsDeclarant;
cerfaFields['nubNumero']                     = $ds092PE1.etatCivil.identificationDeclarant.nubNumero;

//Coordonnées 
cerfaFields['paysDeclarant']                 = $ds092PE1.adressePersoGroup.adressePerso.paysDeclarant;
cerfaFields['communeDeclarant']              = $ds092PE1.adressePersoGroup.adressePerso.communeDeclarant;
cerfaFields['telephoneDeclarant']            = $ds092PE1.adressePersoGroup.adressePerso.telephoneDeclarant;
cerfaFields['codePostalDeclarant']           = $ds092PE1.adressePersoGroup.adressePerso.codePostalDeclarant != null ? $ds092PE1.adressePersoGroup.adressePerso.codePostalDeclarant : '' ;

var siret = $ds092PE1.adressePersoGroup.adressePerso.mailDeclarant;
	cerfaFields['debutMail']              = '';
	cerfaFields['finMail']                 = '';
if(siret != null) {
	var adresseSplite = siret.split("@");
	cerfaFields['debutMail']              = adresseSplite[0];
	cerfaFields['finMail']                 = adresseSplite[1];

}


cerfaFields['adresseDeclarant']              = $ds092PE1.adressePersoGroup.adressePerso.adresseDeclarant;
cerfaFields['nomTiers']                      = $ds092PE1.adressePersoGroup.adressePerso.nomTiers != null ? $ds092PE1.adressePersoGroup.adressePerso.nomTiers : '' ;



//Signature
cerfaFields['signatureDeclarant']            = civ2;
cerfaFields['lieuSignature']                 = $ds092PE1.signatureGroup.signature.lieuSignature;
cerfaFields['dateSignature']                 = $ds092PE1.signatureGroup.signature.dateSignature;
cerfaFields['declarationHonneur']            = $ds092PE1.signatureGroup.signature.declarationHonneur;
cerfaFields['certificationHonneur']          = $ds092PE1.signatureGroup.signature.certificationHonneur;
cerfaFields['certificationCoupableFraude']   = $ds092PE1.signatureGroup.signature.certificationCoupableFraude;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $ds092PE1.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */


//finalDoc.append(accompDoc.save('courrier.pdf'));


var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		date: $ds092PE1.signatureGroup.signature.dateSignature,
		autoriteHabilitee : "CNAPS",
		demandeContexte : "Renouvellement de la demande d’agrément pour le dirigeant ou le gérant d’une entreprise de sécurité privée",
		civiliteNomPrenom : civNomPrenom
	});
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Formulaire DEMANDE DE RENOUVELLEMENT D’AGRÉMENT DIRIGEANT - GÉRANT.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
 
/*
 * ID, titre séjour ou Bulletin
 */
var pj=$ds092PE1.etatCivil.identificationDeclarant.nationalite;
if(Value('id').of(pj).contains('france') || Value('id').of(pj).contains('membreUE')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
}

var pj=$ds092PE1.etatCivil.identificationDeclarant.nationalite;
if(Value('id').of(pj).contains('accordBilateral')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjTitreSejour);
}

var pj=$ds092PE1.etatCivil.identificationDeclarant.nationalite;
if(Value('id').of(pj).contains('horsUE')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjCopieBulletin);
}





/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Telesurveillance_securite_electronique.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Télésurveillance et sécurité électronique - Demande d’agrément pour le dirigeant ou le gérant d’une entreprise de sécurité privée',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande d’agrément pour le dirigeant ou le gérant d’une entreprise de sécurité privée pour l\'activité de télésurveillance et sécurité électronique.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
