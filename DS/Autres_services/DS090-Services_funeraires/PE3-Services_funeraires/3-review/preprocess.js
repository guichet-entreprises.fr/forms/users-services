

var cerfaFields = {};

var civNomPrenom = $ds090PE1.representant.representantLegal.nomPatronymique + ' ' + $ds090PE1.representant.representantLegal.prenom;

//entreprise
cerfaFields['denomination']                 = $ds090PE1.entreprise.indicationsEntreprise.denomination;
cerfaFields['formeSociale']                 = $ds090PE1.entreprise.indicationsEntreprise.formeSociale;
cerfaFields['nombreSalaries']               = $ds090PE1.entreprise.indicationsEntreprise.nombreSalaries;
cerfaFields['adresseSiegeSocial']           = ($ds090PE1.entreprise.indicationsEntreprise.adresseSiege) 
											+ ', ' + ($ds090PE1.entreprise.indicationsEntreprise.codePostalSiege !=null ? $ds090PE1.entreprise.indicationsEntreprise.codePostalSiege : '')
											+ ' ' + ($ds090PE1.entreprise.indicationsEntreprise.villeSiege);
cerfaFields['adresseEtablissement']         = $ds090PE1.entreprise.indicationsEntreprise.adresseEta 
											+ ', ' + ($ds090PE1.entreprise.indicationsEntreprise.codePostalEta !=null ? $ds090PE1.entreprise.indicationsEntreprise.codePostalEta :'')
											+ ' ' + $ds090PE1.entreprise.indicationsEntreprise.villeEta;
cerfaFields['numeroEtablissement']          = $ds090PE1.entreprise.indicationsEntreprise.numeroEtablissement;
cerfaFields['numeroTelecopieEtablissement'] = $ds090PE1.entreprise.indicationsEntreprise.numeroTelecopieEtablissement;
cerfaFields['enseigne']                     = $ds090PE1.entreprise.indicationsEntreprise.enseigne;

//Représentant
cerfaFields['nomPatronymique']              = $ds090PE1.representant.representantLegal.nomPatronymique ;
cerfaFields['nomEpouse']                    = $ds090PE1.representant.representantLegal.nomEpouse !=null ? $ds090PE1.representant.representantLegal.nomEpouse :'';
cerfaFields['prenom']                       = $ds090PE1.representant.representantLegal.prenom;
cerfaFields['dateNaissance']                = $ds090PE1.representant.representantLegal.dateNaissance;
cerfaFields['lieuNaissance']                = $ds090PE1.representant.representantLegal.villeNaissance 
											+ ', ' + $ds090PE1.representant.representantLegal.departementNaissance
											+ ', ' + $ds090PE1.representant.representantLegal.paysNaissance;
cerfaFields['nationalite']                  = $ds090PE1.representant.representantLegal.nationalite;
cerfaFields['adresseDomicile']              = $ds090PE1.representant.representantLegal.adresseDom 
											+ ', ' + ($ds090PE1.representant.representantLegal.codePostalDom !=null ? $ds090PE1.representant.representantLegal.codePostalDom :'')
											+' '+ $ds090PE1.representant.representantLegal.villeDom;
cerfaFields['qualiteRepresentant']          = '';

//Activités
cerfaFields['organisationObseques']         = Value('id').of($ds090PE1.listeActivite.listeActivite.activites).contains("organisationObseques") ? true : false;
cerfaFields['fournitureCercueil']           = Value('id').of($ds090PE1.listeActivite.listeActivite.activites).contains("fournitureCercueil") ? true : false;
cerfaFields['fourniturePersonnel']          = Value('id').of($ds090PE1.listeActivite.listeActivite.activites).contains("fourniturePersonnel") ? true : false;
cerfaFields['soinConservation']             = Value('id').of($ds090PE1.listeActivite.listeActivite.activites).contains("soinConservation") ? true : false;
cerfaFields['gestionChambre']               = Value('id').of($ds090PE1.listeActivite.listeActivite.activites).contains("gestionChambre") ? true : false;
cerfaFields['transportCorps']               = Value('id').of($ds090PE1.listeActivite.listeActivite.activites).contains("transportCorps") ? true : false;
cerfaFields['transportCorpsApres']          = Value('id').of($ds090PE1.listeActivite.listeActivite.activites).contains("transportCorpsApres") ? true : false;

//Signature
cerfaFields['signatureRepresentant']        = civNomPrenom;
cerfaFields['dateSignature']                = $ds090PE1.signature.signature.dateSignature;



/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $ds090PE1.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		dateSignature: $ds090PE1.signature.signature.dateSignature,
		autoriteHabilitee :"Préfet",
		demandeContexte : "Modification d’habilitation pour l’exercice d’activités funéraires pour une entreprise privée",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Demande modification habilitation exercice activités funéraires.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDeclaration);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExtrait);
appendPj($attachmentPreprocess.attachmentPreprocess.pjImposition);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjRegistrePersonnel);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertificat);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationRepresentant);

/*
 *PJ Transport de corps
 */
var pj=$ds090PE1.listeActivite.listeActivite.activites;
if(Value('id').of(pj).contains('transportCorps')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjConformite);
}

var pj=$ds090PE1.listeActivite.listeActivite.activites;
if(Value('id').of(pj).contains('transportCorps')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjPermisConduire);
}

/*
 *PJ Gestion utilisation chambre funéraire
 */
var pj=$ds090PE1.listeActivite.listeActivite.activites;
if(Value('id').of(pj).contains('gestionChambre')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjConformiteChambre);
}

/*
 *PJ Gestion crematorium
 */
var pj=$ds090PE1.listeActivite.listeActivite.activites;
if(Value('id').of(pj).contains('fourniturePersonnel')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjConformiteCrematorium);
}

/*
 *PJ Activité de thanatopracteur
 */
var pj=$ds090PE1.listeActivite.listeActivite.activites;
if(Value('id').of(pj).contains('soinConservation')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationPersonnel);
}

var pj=$ds090PE1.listeActivite.listeActivite.activites;
if(Value('id').of(pj).contains('soinConservation')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjCertif);
}


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Services_funeraire_demande_habilitation.pdf');
 
/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Services funéraires - Modification d’habilitation pour l’exercice d\'activités funéraires pour une entreprise privée',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Modification d’habilitation pour l\'exercice d’activités funéraires pour une entreprise privée',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
