msgid "Identification du déclarant"
msgstr "La identificación del declarante"

msgid "Demande de reconnaissance de qualifications professionnelles en vue d’un libre établissement."
msgstr "De reconocimiento de las cualificaciones profesionales Solicitud de libre establecimiento."

msgid "Formulaire de demande d'autorisation d'exercice"
msgstr "Huelga formulario de solicitud de Autorización"
 
msgid "Civilité :"
msgstr "civilidad"

msgid "Nom de naissance :"
msgstr "Nombre de nacimiento :"

msgid "Nom d'usage :"
msgstr "Nombre de usuario :"


msgid "Prénom(s) :"
msgstr "Nombre (s):"


msgid "Date de naissance :"
msgstr "Fecha de nacimiento :"

msgid "Commune ou ville de naissance :"
msgstr "Ciudad o pueblo de nacimiento:"


msgid "Pays de naissance :"
msgstr "País de nacimiento :"


msgid "Nationalité :"
msgstr "Nacionalidad"

msgid "Adresse personnelle"
msgstr "Dirección personal"

msgid "Adresse professionnelle"
msgstr "Dirección comercial"

msgid "Signature de la formalité"
msgstr "Firma de formalidad"

msgid "Je déclare sur l'honneur l'exactitude des informations de la formalité"
msgstr "Por la presente declaro la exactitud de la información de la formalidad"

msgid "Lieu :"
msgstr "Lugar"

msgid "Date :"
msgstr "Fecha"

msgid "Numéro et libellé de voie :"
msgstr "Y número de pista del siguiente modo"

msgid "Complément d'adresse :"
msgstr "Complemento de dirección"

msgid "Code postal :"
msgstr "Código postal :"

msgid "Commune ou ville :"
msgstr "Pueblo o ciudad :"

msgid "Pays :"
msgstr "País"

msgid "Téléphone fixe :"
msgstr "Teléfono fijo"
 
msgid "Téléphone mobile :"
msgstr "Teléfono móvil"

msgid "Courriel :"
msgstr "Correo electrónico"

msgid "Copie de la pièce d’identité (en cours de validité, recto verso : extrait d'acte de naissance ou fiche d'état civil ou carte d'identité ou passeport)."
msgstr "Copia de la identificación (válido, dúplex: extracto del acta de nacimiento o registro de estado civil o tarjeta de identidad o pasaporte)."

msgid "Copie d’un diplôme de second cycle de l’enseignement supérieur juridique, scientifique ou technique (ou équivalent)."
msgstr "Copia de un título de grado de la educación superior jurídico, científico o técnico (o equivalente)."

msgid "Copie du diplôme du Centre d'études internationales de la propriété industrielle de Strasbourg (Ceipi) ou du titre reconnu équivalent."
msgstr "Copia del diploma del Centro de Estudios Internacionales de la Propiedad Industrial (CEIPI de Estrasburgo) o equivalente reconocido."

msgid "Un diplôme national de troisième cycle ou un diplôme national de master sanctionnant une formation dans le domaine de la propriété industrielle."
msgstr "A título de postgrado nacional o un diploma de maestro nacional de formación en el ámbito de la propiedad industrial."

msgid "Copie(s) de l'attestation professionnelle."
msgstr "Copia (s) certificado profesional."

msgid "Le(s) certificat(s) doit(vent) préciser le lieu d’exercice de la pratique professionnelle, celle-ci doit avoir été acquise au sein d’un Etat de l’Union européenne ou de l’Espace économique européen ou en Suisse."
msgstr "El certificado (s) (s) es (viento) especificar el lugar de ejercicio de la práctica profesional, que debe haber sido adquirido en un Estado de la Unión Europea o del Espacio Económico Europeo o Suiza."

msgid "Un diplôme national de troisième cycle ou un diplôme national de master sanctionnant une formation dans le domaine de la propriété industrielle"
msgstr "A título de postgrado nacional o un diploma nacional de formación principal en el campo de la propiedad industrial"
  
msgid "les documents suivants doivent être joints à la demande (en format .PDF, .JPG ou .PNG) :"
msgstr "los siguientes documentos deben adjuntarse a la solicitud (.PDF, JPG o PNG) :"

msgid "Demande de RQP en vue d'un libre établissement.pdf"
msgstr "QPR solicitud para un establecimiento libre"

msgid "Courrier obtenu à partir des données saisies"
msgstr "Correo obtenido a partir de los datos capturados"

msgid "Déclaration préalable dans le cadre d’un libre établissement"
msgstr "Declaración previa como parte de un establecimiento libre"

msgid "Merci d'avoir traité cette formalité sur le site www.guichet-qualifications.fr. Votre dossier sera transmis à l'autorité compétente pour traitement."
msgstr "Gracias por haber tratado a esta formalidad en el sitio www.guichet-qualifications.fr usted. El archivo se remite a la autoridad competente para su procesamiento."