attachment('pjID', 'pjID');
attachment('pjDeclaration', 'pjDeclaration');
attachment('pjExtrait', 'pjExtrait');
attachment('pjImposition', 'pjImposition');
attachment('pjAttestation', 'pjAttestation');
attachment('pjRegistrePersonnel', 'pjRegistrePersonnel');
attachment('pjCertificat', 'pjCertificat');
attachment('pjAttestationRepresentant', 'pjAttestationRepresentant');
//Transport de corps


var pj=$ds090PE1.listeActivite.listeActivite.activites;
if(Value('id').of(pj).contains('transportCorps') || Value('id').of(pj).contains('transportCorpsApres')) {
	attachment('pjConformite', 'pjConformite', { mandatory:"true"});
}

var pj=$ds090PE1.listeActivite.listeActivite.activites;
if(Value('id').of(pj).contains('transportCorps') || Value('id').of(pj).contains('transportCorpsApres')) {
	attachment('pjPermisConduire', 'pjPermisConduire', { mandatory:"true"});
}


//Gestion utilisation chambre funéraire
var pj=$ds090PE1.listeActivite.listeActivite.activites;
if(Value('id').of(pj).contains('gestionChambre')) {
	attachment('pjConformiteChambre', 'pjConformiteChambre', { mandatory:"true"});
}

//Gestion crematorium
var pj=$ds090PE1.listeActivite.listeActivite.activites;
if(Value('id').of(pj).contains('fourniturePersonnel')) {
	attachment('pjConformiteCrematorium', 'pjConformiteCrematorium', { mandatory:"true"});
}

//Activité de thanatopracteur
var pj=$ds090PE1.listeActivite.listeActivite.activites;
if(Value('id').of(pj).contains('soinConservation')) {
	attachment('pjAttestationPersonnel', 'pjAttestationPersonnel', { mandatory:"true"});
}

var pj=$ds090PE1.listeActivite.listeActivite.activites;
if(Value('id').of(pj).contains('soinConservation')) {
	attachment('pjCertif', 'pjCertif', { mandatory:"true"});
}