var formFields = {};

var civNomPrenom = $ds032PE1.cadre1Group.cadre1.civilite + ' ' + $ds032PE1.cadre1Group.cadre1.nomNaissance + ' ' + $ds032PE1.cadre1Group.cadre1.prenomDeclarant;


//Etat Civil
formFields['nomNaissance']                             = $ds032PE1.cadre1Group.cadre1.nomNaissance;
formFields['prenomDeclarant']                          = $ds032PE1.cadre1Group.cadre1.prenomDeclarant;
formFields['nationalite']                              = $ds032PE1.cadre1Group.cadre1.nationaliteDeclarant;

//Coordonnées

formFields['adresseDeclarantVoieComp']                 = ($ds032PE1.cadre1Group.adresse.adresseDeclarantNumeroNomRue != null ? $ds032PE1.cadre1Group.adresse.adresseDeclarantNumeroNomRue : '') + " " 
														+ ($ds032PE1.cadre1Group.adresse.adressecomplement != null ? $ds032PE1.cadre1Group.adresse.adressecomplement : '');
formFields['adresseDeclarantCPVille']                  = ($ds032PE1.cadre1Group.adresse.adresseDeclarantCodePostal != null ? $ds032PE1.cadre1Group.adresse.adresseDeclarantCodePostal : '') + " " 
														+ ($ds032PE1.cadre1Group.adresse.adresseDeclarantVille != null ? $ds032PE1.cadre1Group.adresse.adresseDeclarantVille : '')
														+ ($ds032PE1.cadre1Group.adresse.adresseDeclarantPays != null ? $ds032PE1.cadre1Group.adresse.adresseDeclarantPays : '');

/* formFields['telephoneFixeDeclarant']                   = $ds032PE1.cadre1Group.adresse.telephoneFixeDeclarant;
formFields['telephoneMobileDeclarant']                 = $ds032PE1.adresse.adressePersonnelle.telephoneMobileDeclarant;
formFields['courrielDeclarant']                        = $ds032PE1.adresse.adressePersonnelle.courrielDeclarant; */


// Activité

formFields['regulier']                             		= Value('id').of($ds032PE1.declaratioActiviteGroup.declarationActivite.prestationsAccueil).contains('regulier');
formFields['tempsPlein']                             	= Value('id').of($ds032PE1.declaratioActiviteGroup.declarationActivite.prestationsAccueil).contains('tempsPlein');
formFields['tempsPartiel']                              = Value('id').of($ds032PE1.declaratioActiviteGroup.declarationActivite.prestationsAccueil).contains('tempsPartiel');
formFields['occasionnel']                               = Value('id').of($ds032PE1.declaratioActiviteGroup.declarationActivite.prestationsAccueil).contains('occasionnel');
formFields['saisonnier']                                = Value('id').of($ds032PE1.declaratioActiviteGroup.declarationActivite.prestationsAccueil).contains('saisonnier');
formFields['restauration']                              = Value('id').of($ds032PE1.declaratioActiviteGroup.declarationActivite.prestationsAutres).contains('restauration');
formFields['accueilMaladeHandicap']                     = Value('id').of($ds032PE1.declaratioActiviteGroup.declarationActivite.prestationsAutres).contains('accueilMaladeHandicap');
formFields['autre']                                     = Value('id').of($ds032PE1.declaratioActiviteGroup.declarationActivite.prestationsAutres).contains('autre');
formFields['prestationAutresPrecisions']                = $ds032PE1.declaratioActiviteGroup.declarationActivite.prestationAutresPrecisions;

formFields['capaciteAccueil']                           = $ds032PE1.declaratioActiviteGroup.declarationActivite.capaciteAccueil;
formFields['ageEnfants']                                = $ds032PE1.declaratioActiviteGroup.declarationActivite.ageEnfants;
formFields['joursOuverture']                            = $ds032PE1.declaratioActiviteGroup.declarationActivite.joursOuverture;
formFields['horairesOuverture']                         = $ds032PE1.declaratioActiviteGroup.declarationActivite.horairesOuverture;
formFields['effectifs']                                 = $ds032PE1.declaratioActiviteGroup.declarationActivite.effectifs;
formFields['qualifPersonnel']                           = $ds032PE1.declaratioActiviteGroup.declarationActivite.qualifPersonnel;


// Renseignements établissement

formFields['lieuEnvisage']                              = $ds032PE1.declarationEtablissement.adresseEtablissement.departementExervice;
formFields['adresseEtabVoieComp']                       = ($ds032PE1.declarationEtablissement.adresseEtablissement.adresseEtab.adresseEtabNumeroNomRue != null ? $ds032PE1.declarationEtablissement.adresseEtablissement.adresseEtab.adresseEtabNumeroNomRue : '') + " " 
															+ ($ds032PE1.declarationEtablissement.adresseEtablissement.adresseEtab.adresseEtabComp != null ? $ds032PE1.declarationEtablissement.adresseEtablissement.adresseEtab.adresseEtabComp : '');
formFields['adresseEtabCPVille']                        = ($ds032PE1.declarationEtablissement.adresseEtablissement.adresseEtab.adresseEtabCodePostal != null ? $ds032PE1.declarationEtablissement.adresseEtablissement.adresseEtab.adresseEtabCodePostal : '') + " " 
															+ ($ds032PE1.declarationEtablissement.adresseEtablissement.adresseEtab.adresseEtabCommune != null ? $ds032PE1.declarationEtablissement.adresseEtablissement.adresseEtab.adresseEtabCommune : '');
formFields['directeur']                                 = $ds032PE1.declarationEtablissement.directeurEtab.civiliteDirecteur + " " + $ds032PE1.declarationEtablissement.directeurEtab.nomDirecteur + " " +$ds032PE1.declarationEtablissement.directeurEtab.prenomDirecteur;
formFields['referentTechnique']                         = $ds032PE1.declarationEtablissement.refTechniqueEtab.civiliteRefTech + " " + $ds032PE1.declarationEtablissement.refTechniqueEtab.nomRefTech + " " +$ds032PE1.declarationEtablissement.refTechniqueEtab.prenomRefTech;


//Signature

formFields['nomPrenomSignature']                       = $ds032PE1.cadre1Group.cadre1.nomNaissance + ' ' + $ds032PE1.cadre1Group.cadre1.prenomDeclarant;
formFields['signatureCoche']                           = $ds032PE1.signatureGroup.signature.signatureCoche;
formFields['dateSignature']                            = $ds032PE1.signatureGroup.signature.faitLe;
formFields['lieuSignature']                            = $ds032PE1.signatureGroup.signature.faitA;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
 
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp085PE3.signature.signature.lieuSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $ds032PE1.signatureGroup.signature.faitLe,
		autoriteHabilitee :"Président du conseil départemental de " + $ds032PE1.declarationEtablissement.adresseEtablissement.departementExervice,
		demandeContexte : "Demande d’autorisation d’ouverture d'une crèche pour l'accueil d’enfants de moins de 6 ans  ",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
    .load('models/FORLMULAIRES DE DÉCLARATION  PE1 DS032.pdf') //
    .apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
 
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjEtudeBesoins);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAdresseEtab);
appendPj($attachmentPreprocess.attachmentPreprocess.pjStatutsEtab);
appendPj($attachmentPreprocess.attachmentPreprocess.pjModalitesEtab);
appendPj($attachmentPreprocess.attachmentPreprocess.pjProjetEtab);
appendPj($attachmentPreprocess.attachmentPreprocess.pjReglementEtab);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPlanLocaux);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAutorisationOuverture);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDeclarationPrefet);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Creche_demande_ouverture.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
   label : 'Crèche – Demande d’autorisation d’ouverture',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Crèche – Demande d’autorisation d’ouverture',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});