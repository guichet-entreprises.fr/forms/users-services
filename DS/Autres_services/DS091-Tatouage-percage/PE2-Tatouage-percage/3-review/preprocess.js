var cerfaFields = {};

var civNomPrenom = $ds091PE2.info1.identificationDeclarant.civilite + ' ' + $ds091PE2.info1.identificationDeclarant.nomDeclarant1 + ' ' + $ds091PE2.info1.identificationDeclarant.prenomsDeclarant1;

cerfaFields['civiliteNomPrenom']       				= $ds091PE2.info1.identificationDeclarant.civilite + ' ' + $ds091PE2.info1.identificationDeclarant.nomDeclarant1 + ' ' + $ds091PE2.info1.identificationDeclarant.prenomsDeclarant1;

//Idetification du déclarant
cerfaFields['madame']								= ($ds091PE2.info1.identificationDeclarant.civilite=='Madame');
cerfaFields['monsieur']                        		= ($ds091PE2.info1.identificationDeclarant.civilite=='Monsieur');
cerfaFields['nomDeclarant1']           				= $ds091PE2.info1.identificationDeclarant.nomDeclarant1;
cerfaFields['prenomsDeclarant1']        			= $ds091PE2.info1.identificationDeclarant.prenomsDeclarant1;
cerfaFields['nomEpouse']            				= $ds091PE2.info1.identificationDeclarant.nomEpouse;
cerfaFields['dateNaissanceDeclarant']   			= $ds091PE2.info1.identificationDeclarant.dateNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']     			= $ds091PE2.info1.identificationDeclarant.nationaliteDeclarant;
cerfaFields['paysNaissanceDeclarant']   			= $ds091PE2.info1.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['telephoneFixeDeclarant']   			= $ds091PE2.info1.identificationDeclarant.telephoneFixeDeclarant;
cerfaFields['telephoneMobileDeclarant'] 			= $ds091PE2.info1.identificationDeclarant.telephoneMobileDeclarant;
cerfaFields['mailDeclarant']   						= $ds091PE2.info1.identificationDeclarant.mailDeclarant;
cerfaFields['codePostalDeclarant']            		= $ds091PE2.info1.identificationDeclarant.codePostalDeclarant;

//Déclaration de l'activité
cerfaFields['nomDeclarant2']            			= $ds091PE2.info1.identificationDeclarant.nomDeclarant1;
cerfaFields['prenomsDeclarant2']        			= $ds091PE2.info1.identificationDeclarant.prenomsDeclarant1;
cerfaFields['adresseEtablissement']     			= $ds091PE2.info2.declarationActvite.adresseEtablissement + ' ' + ($ds091PE2.info2.declarationActvite.complementAdresseEtablissement != null ? ', ' + $ds091PE2.info2.declarationActvite.complementAdresseEtablissement : ' ') + ' ' + $ds091PE2.info2.declarationActvite.codePostalDeclarant + ' ' + $ds091PE2.info2.declarationActvite.communeVille + ' ' + $ds091PE2.info2.declarationActvite.paysDeclarant;
cerfaFields['codePostalDeclarant']            		= $ds091PE2.info2.declarationActvite.codePostalDeclarant;
cerfaFields['communeVille']            				= $ds091PE2.info2.declarationActvite.communeVille;
cerfaFields['paysDeclarant']            			= $ds091PE2.info2.declarationActvite.paysDeclarant;
cerfaFields['autreAdresse']             			= $ds091PE2.info2.declarationActvite.autreAdresse;
cerfaFields['natureTechnique']          			= $ds091PE2.info2.declarationActvite.natureTechnique;
cerfaFields['complementAdresseEtablissement']       = $ds091PE2.info2.declarationActvite.complementAdresseEtablissement;

//Déclaration des autres personnes physiques
cerfaFields['nom1']       							= $ds091PE2.info3.personnesPhysiqueTechnique.nom1;
cerfaFields['prenoms1']      						= $ds091PE2.info3.personnesPhysiqueTechnique.prenoms1;
cerfaFields['natureTechnique1']       				= $ds091PE2.info3.personnesPhysiqueTechnique.natureTechnique1;
cerfaFields['dateDebutTechnique1']       			= $ds091PE2.info3.personnesPhysiqueTechnique.dateDebutTechnique1;

cerfaFields['nom2']       							= $ds091PE2.info3.personnesPhysiqueTechnique2.nom2;
cerfaFields['prenoms2']      						= $ds091PE2.info3.personnesPhysiqueTechnique2.prenoms2;
cerfaFields['natureTechnique2']       				= $ds091PE2.info3.personnesPhysiqueTechnique2.natureTechnique2;
cerfaFields['dateDebutTechnique2']       			= $ds091PE2.info3.personnesPhysiqueTechnique2.dateDebutTechnique2;

cerfaFields['nom3']       							= $ds091PE2.info3.personnesPhysiqueTechnique3.nom3;
cerfaFields['prenoms3']      						= $ds091PE2.info3.personnesPhysiqueTechnique3.prenoms3;
cerfaFields['natureTechnique3']       				= $ds091PE2.info3.personnesPhysiqueTechnique3.natureTechnique3;
cerfaFields['dateDebutTechnique3']       			= $ds091PE2.info3.personnesPhysiqueTechnique3.dateDebutTechnique3;

cerfaFields['nom4']       							= $ds091PE2.info3.personnesPhysiqueTechnique4.nom4;
cerfaFields['prenoms4']      						= $ds091PE2.info3.personnesPhysiqueTechnique4.prenoms4;
cerfaFields['natureTechnique4']       				= $ds091PE2.info3.personnesPhysiqueTechnique4.natureTechnique4;
cerfaFields['dateDebutTechnique4']       			= $ds091PE2.info3.personnesPhysiqueTechnique4.dateDebutTechnique4;

cerfaFields['nom5']       							= $ds091PE2.info3.personnesPhysiqueTechnique5.nom5;
cerfaFields['prenoms5']      						= $ds091PE2.info3.personnesPhysiqueTechnique5.prenoms5;
cerfaFields['natureTechnique5']       				= $ds091PE2.info3.personnesPhysiqueTechnique5.natureTechnique5;
cerfaFields['dateDebutTechnique5']       			= $ds091PE2.info3.personnesPhysiqueTechnique5.dateDebutTechnique5;


cerfaFields['nom6']       							= $ds091PE2.info3.personnesPhysiqueTechnique6.nom6;
cerfaFields['prenoms6']      						= $ds091PE2.info3.personnesPhysiqueTechnique6.prenoms6;
cerfaFields['natureTechnique6']       				= $ds091PE2.info3.personnesPhysiqueTechnique6.natureTechnique6;
cerfaFields['dateDebutTechnique6']       			= $ds091PE2.info3.personnesPhysiqueTechnique6.dateDebutTechnique6;

cerfaFields['nom7']       							= $ds091PE2.info3.personnesPhysiqueTechnique7.nom7;
cerfaFields['prenoms7']      						= $ds091PE2.info3.personnesPhysiqueTechnique7.prenoms7;
cerfaFields['natureTechnique7']       				= $ds091PE2.info3.personnesPhysiqueTechnique7.natureTechnique7;
cerfaFields['dateDebutTechnique7']       			= $ds091PE2.info3.personnesPhysiqueTechnique7.dateDebutTechnique7;

cerfaFields['nom8']       							= $ds091PE2.info3.personnesPhysiqueTechnique8.nom8;
cerfaFields['prenoms8']      						= $ds091PE2.info3.personnesPhysiqueTechnique8.prenoms8;
cerfaFields['natureTechnique8']       				= $ds091PE2.info3.personnesPhysiqueTechnique8.natureTechnique8;
cerfaFields['dateDebutTechnique8']       			= $ds091PE2.info3.personnesPhysiqueTechnique8.dateDebutTechnique8;


cerfaFields['nom9']       							= $ds091PE2.info3.personnesPhysiqueTechnique9.nom9;
cerfaFields['prenoms9']      						= $ds091PE2.info3.personnesPhysiqueTechnique9.prenoms9;
cerfaFields['natureTechnique9']       				= $ds091PE2.info3.personnesPhysiqueTechnique9.natureTechnique9;
cerfaFields['dateDebutTechnique9']       			= $ds091PE2.info3.personnesPhysiqueTechnique9.dateDebutTechnique9;


cerfaFields['nom10']       							= $ds091PE2.info3.personnesPhysiqueTechnique10.nom10;
cerfaFields['prenoms10']      						= $ds091PE2.info3.personnesPhysiqueTechnique10.prenoms10;
cerfaFields['natureTechnique10']       				= $ds091PE2.info3.personnesPhysiqueTechnique10.natureTechnique10;
cerfaFields['dateDebutTechnique10']       			= $ds091PE2.info3.personnesPhysiqueTechnique10.dateDebutTechnique10;

//signature
cerfaFields['date']                					= $ds091PE2.info4.signature.dateSignature;
cerfaFields['regionExercice']           			= $ds091PE2.info4.signature.regionExercice;
cerfaFields['attestationHygiene']           		= $ds091PE2.info4.signature.attestationHygiene;
cerfaFields['signature']           					= $ds091PE2.info4.signature.signature;
cerfaFields['lieuSignature']                  		= $ds091PE2.info4.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']			= $ds091PE2.info1.identificationDeclarant.civilite + ' ' + $ds091PE2.info1.identificationDeclarant.nomDeclarant1 + ' ' + $ds091PE2.info1.identificationDeclarant.prenomsDeclarant1;

cerfaFields['libelleProfession']					= "Tatouage-perçage"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $ds091PE2.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $ds091PE2.info4.signature.dateSignature,
		autoriteHabilitee :"Agence régionale de santé",
		demandeContexte : "Déclaration d’activité en cas de prestations occasionnelles.",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier_libre_LPS_V4.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Tatouage_perçage_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Tatouage-perçage - déclaration d\’activité en cas de prestations occasionnelles',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration d\'activité de tatouage-perçage en cas de prestations occasionnelles .',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
