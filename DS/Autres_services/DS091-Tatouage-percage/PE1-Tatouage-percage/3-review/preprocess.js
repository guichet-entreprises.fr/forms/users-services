var cerfaFields = {};

var civNomPrenom = $ds091PE1.info1.identificationDeclarant.civilite + ' ' + $ds091PE1.info1.identificationDeclarant.nomDeclarant1 + ' ' + $ds091PE1.info1.identificationDeclarant.prenomsDeclarant1;

cerfaFields['civiliteNomPrenom']       				= $ds091PE1.info1.identificationDeclarant.civilite + ' ' + $ds091PE1.info1.identificationDeclarant.nomDeclarant1 + ' ' + $ds091PE1.info1.identificationDeclarant.prenomsDeclarant1;

//Idetification du déclarant
cerfaFields['madame']								= ($ds091PE1.info1.identificationDeclarant.civilite=='Madame');
cerfaFields['monsieur']                        		= ($ds091PE1.info1.identificationDeclarant.civilite=='Monsieur');
cerfaFields['nomDeclarant1']           				= $ds091PE1.info1.identificationDeclarant.nomDeclarant1;
cerfaFields['prenomsDeclarant1']        			= $ds091PE1.info1.identificationDeclarant.prenomsDeclarant1;
cerfaFields['nomEpouse']            				= $ds091PE1.info1.identificationDeclarant.nomEpouse;
cerfaFields['dateNaissanceDeclarant']   			= $ds091PE1.info1.identificationDeclarant.dateNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']     			= $ds091PE1.info1.identificationDeclarant.nationaliteDeclarant;
cerfaFields['paysNaissanceDeclarant']   			= $ds091PE1.info1.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['telephoneFixeDeclarant']   			= $ds091PE1.info1.identificationDeclarant.telephoneFixeDeclarant;
cerfaFields['telephoneMobileDeclarant'] 			= $ds091PE1.info1.identificationDeclarant.telephoneMobileDeclarant;
cerfaFields['mailDeclarant']   						= $ds091PE1.info1.identificationDeclarant.mailDeclarant;
cerfaFields['codePostalDeclarant']            		= $ds091PE1.info1.identificationDeclarant.codePostalDeclarant;

//Déclaration de l'activité
cerfaFields['nomDeclarant2']            			= $ds091PE1.info1.identificationDeclarant.nomDeclarant1;
cerfaFields['prenomsDeclarant2']        			= $ds091PE1.info1.identificationDeclarant.prenomsDeclarant1;
cerfaFields['adresseEtablissement']     			= $ds091PE1.info2.declarationActvite.adresseEtablissement + ' ' + ($ds091PE1.info2.declarationActvite.complementAdresseEtablissement != null ? ', ' + $ds091PE1.info2.declarationActvite.complementAdresseEtablissement : ' ') + ' ' + $ds091PE1.info2.declarationActvite.codePostalDeclarant + ' ' + $ds091PE1.info2.declarationActvite.communeVille + ' ' + $ds091PE1.info2.declarationActvite.paysDeclarant;
cerfaFields['codePostalDeclarant']            		= $ds091PE1.info2.declarationActvite.codePostalDeclarant;
cerfaFields['communeVille']            				= $ds091PE1.info2.declarationActvite.communeVille;
cerfaFields['paysDeclarant']            			= $ds091PE1.info2.declarationActvite.paysDeclarant;
cerfaFields['autreAdresse']             			= $ds091PE1.info2.declarationActvite.autreAdresse;
cerfaFields['natureTechnique']          			= $ds091PE1.info2.declarationActvite.natureTechnique;
cerfaFields['complementAdresseEtablissement']       = $ds091PE1.info2.declarationActvite.complementAdresseEtablissement;

//signature
cerfaFields['date']                					= $ds091PE1.info3.signature.dateSignature;
cerfaFields['regionExercice']           			= $ds091PE1.info3.signature.regionExercice;
cerfaFields['signature']           					= $ds091PE1.info3.signature.signature;
cerfaFields['lieuSignature']                  		= $ds091PE1.info3.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']			= $ds091PE1.info1.identificationDeclarant.civilite + ' ' + $ds091PE1.info1.identificationDeclarant.nomDeclarant1 + ' ' + $ds091PE1.info1.identificationDeclarant.prenomsDeclarant1;

cerfaFields['libelleProfession']					= "Tatouage-perçage"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $ds091PE1.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $ds091PE1.info3.signature.dateSignature,
		autoriteHabilitee :"Agence régionale de santé",
		demandeContexte : "Déclaration d'activité.",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier_libre_LE_V4.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Tatouage_perçage_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Tatouage-perçage - Déclaration d\'activité',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration d\'activité de tatouage-perçage.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
