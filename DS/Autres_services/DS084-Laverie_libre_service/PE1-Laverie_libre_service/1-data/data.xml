<?xml version="1.0" encoding="UTF-8" ?>

<?xml-stylesheet href="../css/data.css" type="text/css"?>
<form id="ds084PE1" label="Laverie libre service - déclaration d’installation classée pour la protection de l’environnement" xmlns="http://www.ge.fr/schema/1.2/form-manager" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.ge.fr/schema/1.2/form-manager http://www.ge.fr/schema/form-manager-1.2.xsd">
    <default>
		<!-- The Input type here use the default data type String -->
        <data mandatory="true" type="String" />
    </default>
	
	<!-- Identification Déclarant -->
	<group id="idDeclarant" label="Identification du déclarant">
		<group id="idDeclarant1" label="Identification du déclarant">
			<data id="personne" label="Statut :" mandatory="true" type="Radios(inline:true)">		
				<conftype>
					<referential>
						<text id="personneMorale">Personne morale</text>
						<text id="personnePhysique">Personne physique</text>
					</referential>
				</conftype>
			</data>
			<data id="personne2" label="Statut :" mandatory="true" type="Radios(inline:true)"><if>Value('id').of($idDeclarant1.personne).eq("personnePhysique")</if>
				<help>Il s'agit d'indiquer votre titre : Monsieur si vous êtes un homme, Madame si vous êtes une femme. Remarque : le terme Mademoiselle n'est plus utilisé en France il convient donc dans ce cas de selectionner Madame.</help>
				<conftype>
					<referential>
						<text id="madame" >Madame</text>
						<text id="monsieur" >Monsieur</text>
					</referential>
				</conftype>
			</data>
			<data id="nomMorale" label="Raison sociale :" mandatory="true"><if>Value('id').of($idDeclarant1.personne).eq("personneMorale")</if></data>
			<data id="nomDeclarant" label="Nom et prénoms pour une personne physique:" mandatory="true"><if>Value('id').of($idDeclarant1.personne).eq("personnePhysique")</if></data>
			<data id="formeJuridique" label="Forme juridique :" mandatory="true"/>
			<data id="siret" label="N° SIRET :" mandatory="true" type="SIRET"/>
			<data id="numVoieAdresseDeclarant" label="Numéro et voie ou lieu-dit :" mandatory="true" type="String">
			<help>Il convient d'indiquer dans l'ordre : le numéro de voie, le cas échéant l'indice de répétition, le type de voie et le nom de la voie. Exemple : 12 bis rue de Paris.</help></data>
			<data  id="complementAdresseDeclarant" label="Complément d'adresse :" mandatory="false" type="String">
			<help>Il convient d'indiquer ici tout complément d'adresse permettant de l'identifier tels que : lieu-dit, BP, CS, bâtiment, escalier, étage, appartement, etc.</help></data>
			<data id="codePostalDeclarant" label="Code postal :" mandatory="false">
			<help>Il convient d'indiquer le code postal de la localité.</help></data>
			<data id="communeDeclarant" label="Commune :" mandatory="true">
			<help>Il convient d'indiquer la localité.</help></data>
			<data id="paysDeclarant" label="Pays, si le déclarant réside à l'étranger :" mandatory="false">
			<help>Il convient d'indiquer ici en toutes lettres le pays du déclarant.</help></data>
			<data id="regionDeclarant" label="Province ou région étrangère :" mandatory="true"/>
			<data id="numTelDeclarant" label="Téléphone fixe :" mandatory="false" type="Phone">
			<help>Il convient de sélectionner le pays d'attribution du numéro et de renseigner le numéro de téléphone fixe.</help></data>
			<data id="numMobileDeclarant" label="Téléphone mobile :" mandatory="true" type="Phone">
			<help>Il convient de sélectionner le pays d'attribution du numéro et de renseigner le numéro de téléphone mobile.</help></data>
			<data id="faxDeclarant" label="Fax :" mandatory="false" type="Phone">
			<help>Il convient de sélectionner le pays d'attribution du numéro et de renseigner le numéro de fax.</help></data>
			<data id="emailDeclarant" label="Courriel :" mandatory="true" type="Email">
			<help>Il convient de renseigner une adresse e-mail valide.</help></data>	
			<data id="nomSignataire" label="Nom :" mandatory="true"><if>Value('id').of($idDeclarant1.personne).eq("personneMorale")</if>
			<help>Il s'agit du nom qui a été donné à la naissance tel qu'il apparaît sur le justificatif d'identité.</help></data>
			<data id="prenomsSignataire" label="Prénoms :" mandatory="true"><if>Value('id').of($idDeclarant1.personne).eq("personneMorale")</if>
			<help>Il s'agit de l'ensemble des prénoms figurant sur le justificatif d'identité. L'ordre indiqué sur le justificatif d'identité doit être respecté et les prénoms doivent êtres séparés par une virgule.</help></data>
			<data id="qualiteSignataire" label="Qualité :" mandatory="true"><if>Value('id').of($idDeclarant1.personne).eq("personneMorale")</if></data>
		</group>
	</group>
	
	<!-- Infos générales -->
	<group id="informations" label="Informations générales concernant l'installation">
		<group id="informations" label="Informations générales concernant l'installation">
			<data id="siretInstallation" label="N° SIRET :" mandatory="true" type="SIRET"/>
			<data id="enseigneSite" label="Enseigne ou nom usuel du site :" mandatory="true"/>
			 <data id="adresseIdentique" label="Adresse de l'installation, identique à celle du déclarant (mentionnée ci-dessus) :" mandatory="true" type="Radios">
				<conftype>
			<referential>
				<text id="ouiAdresse">Oui</text>
				<text id="nonAdresse">Non</text>
			</referential>
		</conftype>
		</data>			 
			<data id="numVoieInstallation" label="Numéro et voie ou lieu-dit :" mandatory="true" type="String"><if>Value('id').of($informations.adresseIdentique).eq("nonAdresse")</if>
			<help>Il convient d'indiquer dans l'ordre : le numéro de voie, le cas échéant l'indice de répétition, le type de voie et le nom de la voie. Exemple : 12 bis rue de Paris.</help></data>
			<data  id="complementAdresseInstallation" label="Complément d'adresse :" mandatory="false" type="String"><if>Value('id').of($informations.adresseIdentique).eq("nonAdresse")</if>
			<help>Il convient d'indiquer ici tout complément d'adresse permettant de l'identifier tels que : lieu-dit, BP, CS, bâtiment, escalier, étage, appartement, etc.</help></data>
			<data id="codePostalInstallation" label="Code postal :" mandatory="false"><if>Value('id').of($informations.adresseIdentique).eq("nonAdresse")</if>
			<help>Il convient d'indiquer le code postal de la localité.</help></data>
			<data id="communeInstallation" label="Commune :" mandatory="true"><if>Value('id').of($informations.adresseIdentique).eq("nonAdresse")</if>
			<help>Il convient d'indiquer la localité.</help></data>
			<data id="numTelInstallation" label="Téléphone fixe :" mandatory="false" type="Phone">
			<help>Il convient de sélectionner le pays d'attribution du numéro et de renseigner le numéro de téléphone fixe.</help></data>
			<data id="numMobileInstallation" label="Téléphone mobile :" mandatory="true" type="Phone">
			<help>Il convient de sélectionner le pays d'attribution du numéro et de renseigner le numéro de téléphone mobile.</help></data>
			<data id="faxInstallation" label="Fax :" mandatory="false" type="Phone">
			<help>Il convient de sélectionner le pays d'attribution du numéro et de renseigner le numéro de fax.</help></data>
			<data id="emailInstallation" label="Courriel :" mandatory="true" type="Email">
			<help>Il convient de renseigner une adresse e-mail valide.</help></data>
		</group>
	</group> 
	
	<!-- Description Installation-->
	<group id="description" label="Description générale de l'installation">
		<group id="description" label="Description générale de l'installation">
			<data id="descriptionInstallation" label="Description générale de l'installation (présentation de l'activité exercée sur le site...) : " mandatory="true" type="Text"/>
			
					
			<data id="autorisation" label="Le déclarant exploite déjà au moins : une installation classée relevant du régime d'autorisation:" mandatory="true" type="Radios(inline:true)">		
		<conftype>
			<referential>
				<text id="ouiAutorisation" >Oui</text>
				<text id="nonAutorisation" >Non</text>
			</referential>
		</conftype>
		</data>		
		<data id="enregistrement" label="Le déclarant exploite déjà au moins : une installation classée relevant du régime d'enregistrement:" mandatory="true" type="Radios(inline:true)">		
		<conftype>
			<referential>
				<text id="ouiEnregistrement" >Oui</text>
				<text id="nonEnregistrement" >Non</text>
			</referential>
		</conftype>
		</data>	
		<data id="declaration" label="Le déclarant exploite déjà au moins : une installation classée relevant du régime de declaration:" mandatory="true" type="Radios(inline:true)">		
		<conftype>
			<referential>
				<text id="ouiDeclaration" >Oui</text>
				<text id="nonDeclaration" >Non</text>
			</referential>
		</conftype>
		</data>
		</group>
	</group>
	
	<!-- Implantation installation -->
	<group id="implantation" label="Implantation de l'installation">
		<group id="cadastrePlan" label="Cadastre et plans">
		<data id="implantationTerritoire" label="L'installation est implantée sur le territoire de plusieurs départements :" mandatory="true" type="Radios(inline:true)">		
		<conftype>
			<referential>
				<text id="ouiInstallationTerritoire" >Oui</text>
				<text id="nonInstallationTerritoire" >Non</text>
			</referential>
		</conftype>
		</data>	
		<data id="numDepartements" label="Préciser les numéros des départements concernés, séparés par une virgule :" mandatory="true"><if>Value('id').of($cadastrePlan.implantationTerritoire).eq("ouiInstallationTerritoire")</if></data>	
		<data id="implantationCommune" label="L'installation est implantée sur le territoire de plusieurs communes :" mandatory="true" type="Radios(inline:true)">		
		<conftype>
			<referential>
				<text id="ouiInstallationCommune" >Oui</text>
				<text id="nonInstallationCommune" >Non</text>
			</referential>
		</conftype>
		</data>
		<data id="nomCommunes" label="Préciser les noms des communes concernées, séparés par une virgule :" mandatory="true"><if>Value('id').of($cadastrePlan.implantationCommune).eq("ouiInstallationCommune")</if></data>		
		</group>
		
		<group id="permis" label="Permis de construire">
			<data id="permisConstruire" label="La mise en oeuvre de l'installation nécessite un permis de construire :" mandatory="true" type="Radios(inline:true)">		
		<conftype>
			<referential>
				<text id="ouiPermis">Oui</text>
				<text id="nonPermis">Non</text>
			</referential>
		</conftype>
		</data>
		</group>
	</group>

	<!-- Nature, volume Activités -->
	<group id="natureVolumeActivite" label="Nature et volume des activités">
		<group id="natureVolumeActivite[]" label="Espèces animales hébergées et capacité d'hébergement" min-occurs="1" max-occurs="12">
		<help>Vous pouvez saisir jusqu'à 12 rubriques en cliquant sur le + à droite</help>
			<data id="numRubrique" label="Numéro de la rubrique :" mandatory="true" type="Integer" />
			<data id="alinea" label="Alinéa :" mandatory="true" type="string" />
			<data id="designation" label="Désignation de la rubrique : " mandatory="true" type="string" />
			<data id="capacite" label="Capacité de l'activité :" mandatory="true" type="string" />
			<data id="unite" label="Unité :" mandatory="true" type="Integer" />
			<data id="regime" label="Régime D ou DC ( D : Régime de déclaration, DC : Régime de déclaration avec contrôle périodique):" mandatory="true" type="string"/>	 
		</group>
		<group id="commentaires" label="Commentaires">
		<data id="commentaireActivite" label="Commentaires (notamment, pour les rubriques de la nomenclature des installations classées dont la capacité est exprimée en 'équivalent', préciser le détail des calculs :" mandatory="false" type="Text"/>
		</group>
	</group> 
	
	<!-- Présentation des modes d'exploitation -->
	<group id="presentation1" label="Présentation des modes d'exploitation">
		<group id="modeCondition1" label="Modes et conditions d'utilisation, d'épuration et d'évacuation des eaux résiduaires, effluents et des emanations de toute nature">
		<data id="prelevementEau" label="Prélèvement d'eau pour l'exploitation de l'installation classée :" mandatory="true" type="Radios(inline:true)" >
				<conftype>
					<referential>
				<text id="ouiPrelevement">Oui</text>
				<text id="nonPrelevement">Non</text>
					</referential>
				</conftype>
			</data>
			<data id="modePrelevementEau" label="Préciser:" mandatory="true" type="CheckBoxes"><if>Value('id').of($modeCondition1.prelevementEau).eq("ouiPrelevement")</if>
				<conftype>
					<referential>
				<text id="ouiReseauPublic">Réseau public de distribution d'eau :</text>
				<text id="ouiMilieuNaturel">Milieu naturel (hors forage souterrain) :</text>
				<text id="ouiForage">Forage souterrain :</text>
				<text id="ouiAutres">Autres modes de prélèvements d'eau :</text>
					</referential>
				</conftype>
			</data>
			<data id="ouiForageDix" label="S'agit-il d'un forage souterrain de plus de 10 mètres de profondeur ?" type="Radios(inline:true)"><if>Value('id').of($modeCondition1.modePrelevementEau).contains("ouiForage")</if>
				<conftype>
					<referential>
				<text id="ouiForageDixMetres">Oui</text>
				<text id="nonForageDixMetres">Non</text>
					</referential>
				</conftype>
			</data>
			<data id="volumeReseauPublic" label="Quel est le volume maximum annuel prélevé en m3 par le réseau public de distribution d'eau :" mandatory="true" type="Integer" ><if>Value('id').of($modeCondition1.modePrelevementEau).contains("ouiReseauPublic")</if></data>
			<data id="volumeMilieuNaturel" label="Quel est le volume maximum annuel prélevé en m3 par milieu naturel (hors forage souterrain) :" mandatory="true" type="Integer" ><if>Value('id').of($modeCondition1.modePrelevementEau).contains("ouiMilieuNaturel")</if></data>
			<data id="volumeForage" label="Quel est le volume maximum annuel prélevé en m3 par forage souterrain :" mandatory="true" type="Integer" ><if>Value('id').of($modeCondition1.modePrelevementEau).contains("ouiForage")</if></data>
			<data id="autresModes" label="Veuillez préciser les autres modes de prélèvement de l'eau :" mandatory="true" type="Text"><if>Value('id').of($modeCondition1.modePrelevementEau).contains("ouiAutres")</if></data>
		</group>
	</group>
	
	<!-- Rejet -->
	<group id="presentation2" label="Présentation des modes d'exploitation">
		<group id="modeCondition2" label="Modes et conditions d'utilisation, d'épuration et d'évacuation des eaux résiduaires, effluents et des emanations de toute nature">		
			<data id="rejetEau" label="Rejet d'eaux résiduaires issues de l'exploitation de l'installation classée :" mandatory="true" type="Radios(inline:true)" >
				<conftype>
					<referential>
				<text id="ouiRejet">Oui</text>
				<text id="nonRejet">Non</text>
					</referential>
				</conftype>
			</data>
			<data id="commentaireOrigine" label="Veuillez préciser les l'origine et la nature des eaux résiduaires :" mandatory="true" type="Text"><if>Value('id').of($modeCondition2.rejetEau).eq("ouiRejet")</if></data>
			<data id="exutoireEauResiduaires" label="Exutoire des eaux résiduaires :" mandatory="true" type="Radios"><if>Value('id').of($modeCondition2.rejetEau).eq("ouiRejet")</if>
				<conftype>
					<referential>
				<text id="reseauAssainissement">Réseau d'assainissement collectif avec station d'épuration</text>
				<text id="milieuNaturel">Milieu naturel ou réseau d'assainissement collectif dépourvu de station d'épuration</text>
					</referential>
				</conftype>
			</data>
			<data id="precisionsTraitement" label="S'il y a traitement (ou pré-traitement) sur site des eaux résiduaires avant rejet, préciser le traitement :" mandatory="false" type="Text"><if>Value('id').of($modeCondition2.rejetEau).eq("ouiRejet")</if></data>
			<data id="volumeAnnuel" label="Veuillez préciser le volume maximum rejeté dans le milieu naturel en m3 :" mandatory="true" type="Integer"><if>Value('id').of($modeCondition2.rejetEau).eq("ouiRejet")</if></data>
			<data id="commentairesRejetEau" label="Autres commentaires sur les rejets d'eaux résiduaires :" mandatory="false" type="Text"><if>Value('id').of($modeCondition2.rejetEau).eq("ouiRejet")</if></data>
		</group>
	</group>
	
	<!-- Epandage -->
	<group id="presentation3" label="Présentation des modes d'exploitation">
		<group id="modeCondition3" label="Modes et conditions d'utilisation, d'épuration et d'évacuation des eaux résiduaires, effluents et des emanations de toute nature">
			<data id="epandageDechets" label="Epandage de déchets, effluents ou sous-produits sur ou dans des sols agricoles :" mandatory="true" type="Radios(inline:true)">
				<conftype>
					<referential>
				<text id="ouiEpendage">Oui</text>
				<text id="nonEpendage">Non</text>
					</referential>
				</conftype>
			</data>	
			<data id="origineMatieresEpandues" label="Veuillez préciser l'origine et la nature des matières épandues :" mandatory="true" type="Text"><if>Value('id').of($modeCondition3.epandageDechets).eq("ouiEpendage")</if></data>
			<data id="ilotPAC" label="Ilots PAC (Politique agricole commune) faisant partie du plan d'épandage (pour chaque exploitant et/ou prêteur, préciser son nom, son numéro PACAGE (il s'agit du numéro d'identification attribué
			à tout exploitant agricole pour sa déclaration PAC) et les numéros d'îlots correspondants) :" mandatory="true" type="Text"><if>Value('id').of($modeCondition3.epandageDechets).eq("ouiEpendage")</if></data>
			<data id="surfacePlanEpandage" label="Surface totale du plan d'épandage en ha (calculée sur la base de la SAU, surface agricole utile) :" mandatory="true" type="Integer"><if>Value('id').of($modeCondition3.epandageDechets).eq("ouiEpendage")</if></data>
			<data id="quantiteAzoteInscrite" label="Q : Quantité d'azote épandue inscrite au plan d'épandage (en kg N), A1 + A2 = Q :" mandatory="true" type="Integer"><if>Value('id').of($modeCondition3.epandageDechets).eq("ouiEpendage")</if></data>
			<data id="azoteTerre" label="A1 : dont épandue sur les terres de l'exploitation (kg N) :" mandatory="true" type="Integer"><if>Value('id').of($modeCondition3.epandageDechets).eq("ouiEpendage")</if></data>	
			<data id="azoteTerreDispo" label="A2 : dont épandue sur les terres mises à disposition par un tiers (kg N) :" mandatory="true" type="Integer"><if>Value('id').of($modeCondition3.epandageDechets).eq("ouiEpendage")</if></data>
			<data id="azoteInstallation" label="B1 : dont produite sur l'installation (kg N) :" mandatory="true" type="Integer"><if>Value('id').of($modeCondition3.epandageDechets).eq("ouiEpendage")</if></data>
			<data id="azoteTiers" label="B2 : dont provenant de tiers (kg N) :" mandatory="true" type="Integer"><if>Value('id').of($modeCondition3.epandageDechets).eq("ouiEpendage")</if></data>
			<data id="capaciteStockage" label="Capacité de stockage des amtières épandues (en mois) :" mandatory="true" type="Integer"><if>Value('id').of($modeCondition3.epandageDechets).eq("ouiEpendage")</if></data>
	</group>
	</group>
	
	<!-- Rejet atmosphere -->
	<group id="presentation4" label="Présentation des modes d'exploitation">
		<group id="modeCondition4" label="Modes et conditions d'utilisation, d'épuration et d'évacuation des eaux résiduaires, effluents et des emanations de toute nature">
		<data id="rejetAtmosphere" label="Rejets à l'atmosphère (fumées, gaz, poussières, odeurs...) :" mandatory="true" type="Radios(inline:true)">
				<conftype>
					<referential>
				<text id="ouiRejetAtmosphere">Oui</text>
				<text id="nonRejetAtmosphere">Non</text>
					</referential>
				</conftype>
			</data>
			<data id="origineNatureDechet" label="Origine et nature des rejets :" mandatory="true" type="Text"><if>Value('id').of($modeCondition4.rejetAtmosphere).eq("ouiRejetAtmosphere")</if></data>
			<data id="dispositifCapatation" label="Veuillez préciser s'il y a des dispositifs de captation ou de traitement sur site avant rejet :" mandatory="true" type="Text"><if>Value('id').of($modeCondition4.rejetAtmosphere).eq("ouiRejetAtmosphere")</if></data>
			<data id="commentaireRejet" label="Autres commentaires sur les rejets à l'atmosphère :" mandatory="false" type="Text"><if>Value('id').of($modeCondition4.rejetAtmosphere).eq("ouiRejetAtmosphere")</if></data>
		</group>
	</group>
	
	<!-- Elimination nature dechets -->
	<group id="presentation5" label="Présentation des modes d'exploitation">
		<group id="presentation5" label="Elimination des déchets et résidus de l'exploitation">
		<data id="typeDechetResidus" label="Types de déchets et résidus :" mandatory="true" type="Text"/>
		<data id="collecteDechets" label="Collecte des déchets par le service public de gestion des déchets :" mandatory="true" type="Radios(inline:true)">
				<conftype>
					<referential>
				<text id="ouiCollecte">Oui</text>
				<text id="nonCollecte">Non</text>
					</referential>
				</conftype>
			</data>
		</group>
	</group>
	<!-- Dospositions -->
	<group id="presentation6" label="Présentation des modes d'exploitation">
		<group id="presentation6" label="Dispositions prévues en cas de sinistre">
		<data id="capaciteEauLutte" label="Capacité en eau pour la lutte contre l'incendie :" mandatory="true" type="Radios(inline:true)">
				<conftype>
					<referential>
				<text id="priseEau">Prise d'eau sur le réseau incendie public</text>
				<text id="autreCapaciteEau">Autres</text>
					</referential>
				</conftype>
			</data>
			<data id="dispositionPrevues" label="Si autres, veuillez préciser :" mandatory="true" type="Text"><if>Value('id').of($presentation6.capaciteEauLutte).eq("autreCapaciteEau")</if></data>
			<data id="moyenSecours" label="Autres moyens de secours et de protection dont dispose le déclarant (préciser) :" mandatory="false" type="Text"/>
		</group>
	</group>
	
	<!-- Demande d'agrément-->
	<group id="demandeAgrement1" label="Demande d'agrément de l'exploitant d'une installation de traitement de déchets">
		<group id="demandeAgrement" label="Demande d'agrément de l'exploitant d'une installation de traitement de déchets en application de l'article L541-22 du code de l'environnement">
		<data id="installationTraitement" label="Il s'agit d'une installation classée de traitement de déchets (hors collecte des déchets*) soumise à déclaration et
		nécessitant un agrément en application de l'article L541-22 du code de l'environnement (valorisation de déchets d'emballage...)
		(*Rappel : Les agréments autres que ceux relatifs au traitement de déchets et nécessaire en application de l'article L541-22 (collecteurs de déchets de pneumatiques, collecteurs d'huiles usagées...) ne sont pas gérés
		par la présente déclaration.) :" mandatory="true" type="Radios(inline:true)">
				<conftype>
					<referential>
				<text id="ouiTraitementDechets">Oui</text>
				<text id="nonTraitementDechets">Non</text>
					</referential>
				</conftype>
			</data>
		</group>
	
		<group id="demandeAgrement2[]" label="Saisie des déchets à traiter, de la filière de traitement et quantités maximales" min-occurs="1" max-occurs="25">
		<help>Vous pouvez saisir jusqu'à 25 déchets à traiter en cliquant sur le + à droite</help>
		<if>Value('id').of($demandeAgrement.installationTraitement).eq("ouiTraitementDechets")</if>
			<data id="nature" label="Nature des déchets à  :" mandatory="true" type="string" />
			<data id="codification" label="Codification déchets :" mandatory="true" type="string" />
			<data id="type" label="Type de traitement : " mandatory="true" type="string" />
			<data id="codificationTraitement" label="Codification du traitement :" mandatory="true" type="string" />
			<data id="quantite" label="Quantités maximales :" mandatory="true" type="Integer" />
		</group>
	
	
	<group id="demandeAgrement3" label="Commentaires">	
			<if>Value('id').of($demandeAgrement.installationTraitement).eq("ouiTraitementDechets")</if>	
		
			<data id="commentaireTraitementDechets" label="Commentaires (préciser notamment le ou les types d'agréments de traitements de déchets demandés) :" mandatory="true" type="Text" />
	</group>
	</group>
	
	<!-- Natura 2000 -->
	<group id="natura" label="Natura 2000">
		<group id="natura" label="Nature 2000">
			<data id="reference" label="En référence notamment : aux rubriques de la nomenclature précisées au point 4 ci-dessus, et aux listes mentionnées au III de l'article L414-4 du 
			code de l'environnement (liste nationale ou listes locales définies par arrêtés préfectoraux), le projet est soumis à évaluation des incidences Natura 2000:" mandatory="true" type="Radios(inline:true)" >
				<conftype>
					<referential>
				<text id="ouiNatura">Oui</text>
				<text id="nonNatura">Non</text>
					</referential>
				</conftype>
			</data>
		</group>
	</group>
	
	<!-- Prescription -->
	<group id="signature" label="Engagements et signature">
		<group id="signature" label="Engagements et signature">
			<data id="declarationPrescription" label="Le déclarant confirme qu'il a pris connaissance des prescriptions générales applicables aux activités objet de la présente déclaration et notamment 
			des éventuelles distances d'éloignement qui s'imposent pour l'implantation de l'installation." mandatory="true" type="BooleanCheckBox"/>
			<data id="demandeModif" label="Demande de modification de certaines prescriptions applicables à l'installation :" mandatory="true" type="Radios(inline:true)" >
				<conftype>
					<referential>
				<text id="ouiPrescription">Oui</text>
				<text id="nonPrescription">Non</text>
					</referential>
				</conftype>
			</data>
			<data id="departementInstallation" label="Dans quel département se trouve le lieu de l'installation:" type="ComboBox(ref:'departements')" mandatory="true"/>
			<data id="villeSignature" label="Fait à :" mandatory="true" type="string"/>
			<data id="dateSignature" label="Fait le:" mandatory="true" type="Date"/>
			</group>
	</group>
	</form>