var cerfaFields = {};

var civNomPrenom = ($ds059PE2.personnePhysique.personnePhysique0.civilite != null ? $ds059PE2.personnePhysique.personnePhysique0.civilite + ' ' : '') + ($ds059PE2.personnePhysique.personnePhysique0.nom != null ? $ds059PE2.personnePhysique.personnePhysique0.nom + ' ' : '') + ($ds059PE2.personnePhysique.personnePhysique0.prenom != null ? $ds059PE2.personnePhysique.personnePhysique0.prenom + ' ' : '');
/* var region = $ds059PE2.signatureGroup.signature.regionExercice; */
var denomination = $ds059PE2.personneMorale.personneMorale0.denomination;

var civiliteNomPrenom = denomination != null ? denomination : civNomPrenom;
//Personne physique
cerfaFields['nom']  							= $ds059PE2.personnePhysique.personnePhysique0.nom;
cerfaFields['nomUsage']                			= $ds059PE2.personnePhysique.personnePhysique0.nomUsage;
cerfaFields['prenom']             				= $ds059PE2.personnePhysique.personnePhysique0.prenom;

//Personne morale
cerfaFields['denomination']  					= $ds059PE2.personneMorale.personneMorale0.denomination;
cerfaFields['formeJuridique']                	= $ds059PE2.personneMorale.personneMorale0.formeJuridique;
cerfaFields['numRueComplement']             	= ($ds059PE2.personneMorale.personneMorale0.numRue != null ? $ds059PE2.personneMorale.personneMorale0.numRue : ' ') + ($ds059PE2.personneMorale.personneMorale0.complement != null ? ', ' + $ds059PE2.personneMorale.personneMorale0.complement : ' ');
cerfaFields['cpVillePays']       				= ($ds059PE2.personneMorale.personneMorale0.codePostal != null ? $ds059PE2.personneMorale.personneMorale0.codePostal + ' ' : '') + ($ds059PE2.personneMorale.personneMorale0.ville != null ? $ds059PE2.personneMorale.personneMorale0.ville + ', ' : '') + ' ' + ($ds059PE2.personneMorale.personneMorale0.pays != null ? $ds059PE2.personneMorale.personneMorale0.pays + ' ' : '');

//Signature
cerfaFields['date']                				= $ds059PE2.signature.signature.dateSignature;
cerfaFields['signature']           				= $ds059PE2.signature.signature.signature;
cerfaFields['lieuSignature']                    = $ds059PE2.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']       = ($ds059PE2.personnePhysique.personnePhysique0.civilite != null ? $ds059PE2.personnePhysique.personnePhysique0.civilite + ' ' : '') + ' ' + ($ds059PE2.personnePhysique.personnePhysique0.nom != null ? $ds059PE2.personnePhysique.personnePhysique0.nom + ' ' : '') + ' ' + ($ds059PE2.personnePhysique.personnePhysique0.prenom != null ? $ds059PE2.personnePhysique.personnePhysique0.prenom + ' ' : '');
cerfaFields['denominationSignature']  			= $ds059PE2.personneMorale.personneMorale0.denomination;

// cerfaFields['libelleProfession']								  	  = "Agence de mannequins"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //0
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $ds059PE2.signatureGroup.signature.dateSignature,
		civiliteNomPrenom: civiliteNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $ds059PE2.signature.signature.dateSignature,
		autoriteHabilitee :"DIRECCTE",
		demandeContexte : "Renouvellement de la demande de licence d'agence de mannequins",
		civiliteNomPrenom : civiliteNomPrenom
	});
	
var cerfaDoc = nash.doc //
    .load('models/Formulaire_Agence de mannequins_Renouv.pdf') //
    .apply(cerfaFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificatif);
appendPj($attachmentPreprocess.attachmentPreprocess.pjChangement);

var finalDocItem = finalDoc.save('Agence_mannequins_Renouv.pdf');


return spec.create({
    id : 'review',
   label : 'Agence de mannequins - renouvellement de la demande de licence d\'agence de mannequins.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Agence de mannequins - renouvellement de la demande de licence d\'agence de mannequins.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});