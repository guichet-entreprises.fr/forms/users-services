var cerfaFields = {};


/* var region = $ds056PE1.signatureGroup.signature.regionExercice; */
var civiliteNomPrenom = $ds056PE1.identiteOrganisme.identiteOrganisme.denominationSociale;

//Identité de l'organisme

cerfaFields['denominationSociale']             = $ds056PE1.identiteOrganisme.identiteOrganisme.denominationSociale;
cerfaFields['numeroSiret']                     = $ds056PE1.identiteOrganisme.identiteOrganisme.numeroSiret;
cerfaFields['nomCommercial']                   = $ds056PE1.identiteOrganisme.identiteOrganisme.nomCommercial;
cerfaFields['numeroNIC']                       = $ds056PE1.identiteOrganisme.identiteOrganisme.numeroNIC;
cerfaFields['numeroSiren']                     = $ds056PE1.identiteOrganisme.identiteOrganisme.numeroSiren;
cerfaFields['numeroTVA']                       = $ds056PE1.identiteOrganisme.identiteOrganisme.numeroTVA;
cerfaFields['nombreImplantation']              = $ds056PE1.identiteOrganisme.identiteOrganisme.nombreImplantation;
cerfaFields['categorieJuridique']              = $ds056PE1.identiteOrganisme.identiteOrganisme.categorieJuridique;
cerfaFields['immatriculationOui']              = Value('id').of($ds056PE1.identiteOrganisme.identiteOrganisme.immatriculationOuiNon).eq('immatriculationOui')? true : false;
cerfaFields['immatriculationNon']              = Value('id').of($ds056PE1.identiteOrganisme.identiteOrganisme.immatriculationOuiNon).eq('immatriculationNon')? true : false;

cerfaFields['numeroNomVoieAdresse']            = $ds056PE1.coordonnees.coordonnees.numeroNomVoieAdresse;
cerfaFields['complementAdresse']               =($ds056PE1.coordonnees.coordonnees.complementAdresse != null ? $ds056PE1.coordonnees.coordonnees.complementAdresse :'');
cerfaFields['codePostalAdresse']               = $ds056PE1.coordonnees.coordonnees.codePostalAdresse;
cerfaFields['villeAdresse']                    = $ds056PE1.coordonnees.coordonnees.villeAdresse;
cerfaFields['paysAdresse']                     = $ds056PE1.coordonnees.coordonnees.paysAdresse;
cerfaFields['telephoneAdresse']                = $ds056PE1.coordonnees.coordonnees.telephoneAdresse;
cerfaFields['faxAdresse']                      = ($ds056PE1.coordonnees.coordonnees.faxAdresse  != null ? $ds056PE1.coordonnees.coordonnees.faxAdresse :'') ;

//Siége locaux 

cerfaFields['localDedieOuiSiege']              = Value('id').of($ds056PE1.siegeLocaux.siegeLocaux.localDedie).eq('localDedieOuiSiege')? true : false;
cerfaFields['localDedieNonSiege']              = Value('id').of($ds056PE1.siegeLocaux.siegeLocaux.localDedie).eq('localDedieNonSiege')? true : false;
cerfaFields['proprietaireSiege']               = Value('id').of($ds056PE1.siegeLocaux.siegeLocaux.occupant).contains('proprietaireSiege')? true : false;
cerfaFields['locataireSiege']                  = Value('id').of($ds056PE1.siegeLocaux.siegeLocaux.occupant).contains('locataireSiege')? true : false;
cerfaFields['miseDispositionSiege']            = Value('id').of($ds056PE1.siegeLocaux.siegeLocaux.occupant).contains('miseDispositionSiege')? true : false;
cerfaFields['disponibiliteTemporaireSiege']    = Value('id').of($ds056PE1.siegeLocaux.siegeLocaux.disponibilte).eq('disponibiliteTemporaireSiege')? true : false;
cerfaFields['disponibilitePermanenteSiege']    = Value('id').of($ds056PE1.siegeLocaux.siegeLocaux.disponibilte).eq('disponibilitePermanenteSiege')? true : false;
cerfaFields['surfaceTotalSiege']               = $ds056PE1.siegeLocaux.siegeLocaux.surfaceTotalSiege;
cerfaFields['accueilPublicSiege']              = $ds056PE1.siegeLocaux.siegeLocaux.accueilPublicSiege;
cerfaFields['handicapAccueilPublicOuiCoche']   = Value('id').of($ds056PE1.siegeLocaux.siegeLocaux.handicapOuiNon).eq('handicapAccueilPublicOuiCoche')? true : false;
cerfaFields['handicapAccueilPublicNonCoche']   = Value('id').of($ds056PE1.siegeLocaux.siegeLocaux.handicapOuiNon).eq('handicapAccueilPublicNonCoche')? true : false;
cerfaFields['reunionPersonnelSiege']           = $ds056PE1.siegeLocaux.siegeLocaux.reunionPersonnelSiege;
cerfaFields['handicapReunionPersonnelOui']     = Value('id').of($ds056PE1.siegeLocaux.siegeLocaux.handicapOuiNon2).eq('handicapReunionPersonnelOui')? true : false;
cerfaFields['handicapReunionPersonnelNon']     = Value('id').of($ds056PE1.siegeLocaux.siegeLocaux.handicapOuiNon2).eq('handicapReunionPersonnelNon')? true : false;


cerfaFields['monsieurDirigeant']               = Value('id').of($ds056PE1.dirigeant.dirigeant.civilite).eq('monsieurDirigeant')? true : false;
cerfaFields['madameDirigeant']                 = Value('id').of($ds056PE1.dirigeant.dirigeant.civilite).eq('madameDirigeant')? true : false;
cerfaFields['nomDirigeant']                    = $ds056PE1.dirigeant.dirigeant.nomDirigeant;
cerfaFields['prenomDirigeant']                 = $ds056PE1.dirigeant.dirigeant.prenomDirigeant;
cerfaFields['titreFonctionDirigeant']          = $ds056PE1.dirigeant.dirigeant.titreFonctionDirigeant;
cerfaFields['nationaliteDirigeant']            = $ds056PE1.dirigeant.dirigeant.nationaliteDirigeant;
cerfaFields['dataNaissanceDirigeant']          = $ds056PE1.dirigeant.dirigeant.dataNaissanceDirigeant;
cerfaFields['lieuNaissanceDirigeant']          = $ds056PE1.dirigeant.dirigeant.lieuNaissanceDirigeant;
cerfaFields['departementDirigeant']            = $ds056PE1.dirigeant.dirigeant.departementDirigeant;
cerfaFields['paysNaissanceDirigeant']          = $ds056PE1.dirigeant.dirigeant.paysNaissanceDirigeant;

//Personne chargé de l'administration

cerfaFields['madameContactAdministration']     = Value('id').of($ds056PE1.contactAdministration.contactAdministration.civiliteContact).eq('monsieurContactAdministration')? true : false;
cerfaFields['monsieurContactAdministration']   = Value('id').of($ds056PE1.contactAdministration.contactAdministration.civiliteContact).eq('madameContactAdministration')? true : false;
cerfaFields['nomContactAdministration']        = $ds056PE1.contactAdministration.contactAdministration.nomContactAdministration;
cerfaFields['prenomContactAdministration']     = $ds056PE1.contactAdministration.contactAdministration.prenomContactAdministration;
cerfaFields['fonctionContactAdministration']   = $ds056PE1.contactAdministration.contactAdministration.fonctionContactAdministration;
cerfaFields['telephoneContactAdministration']  = $ds056PE1.contactAdministration.contactAdministration.telephoneContactAdministration;
cerfaFields['courrielContactAdministration']   = $ds056PE1.contactAdministration.contactAdministration.courrielContactAdministration;

// Mode d'intervention

cerfaFields['prestataireModeIntervention']     = Value('id').of($ds056PE1.modeIntervention.modeIntervention.prestataireMandataire).eq('prestataireModeIntervention')? true : false;
cerfaFields['mandataireModeIntervention']      = Value('id').of($ds056PE1.modeIntervention.modeIntervention.prestataireMandataire).eq('mandataireModeIntervention')? true : false;
cerfaFields['mandatairePrestataireModeIntervention'] = Value('id').of($ds056PE1.modeIntervention.modeIntervention.prestataireMandataire).eq('mandatairePrestataireModeIntervention')? true : false;




cerfaFields['gardeEnfantCoche1']               = Value('id').of($ds056PE1.modeIntervention.modeIntervention.tousModeIntervention).contains('gardeEnfantCoche1')? true : false;
cerfaFields['accompagnementEnfantCoche1']      = Value('id').of($ds056PE1.modeIntervention.modeIntervention.tousModeIntervention).contains('accompagnementEnfantCoche1')? true : false;

cerfaFields['assistanceHandicapeesCoche2']     = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modeMandataire).contains('assistanceHandicapeesCoche2')? true : false;
cerfaFields['assistanceAgeesCoche2']           = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modeMandataire).contains('assistanceAgeesCoche2')? true : false;
cerfaFields['coduiteVehiculeCoche2']           = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modeMandataire).contains('coduiteVehiculeCoche2')? true : false;
cerfaFields['accompagnement2']                 = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modeMandataire).contains('accompagnement2')? true : false;


cerfaFields['assistanceAgeesCoche3']           = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modePrestataire).contains('assistanceAgeesCoche3')? true : false;
cerfaFields['assistanceHandicapeesCoche3']     = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modePrestataire).contains('assistanceHandicapeesCoche3')? true : false;
cerfaFields['conduiteVehiculeCoche3']          = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modePrestataire).contains('conduiteVehiculeCoche3')? true : false;
cerfaFields['accompagnementPACoche3']          = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modePrestataire).contains('accompagnementPACoche3')? true : false;
cerfaFields['aideFamFragiliseesCoche3']        = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modePrestataire).contains('aideFamFragiliseesCoche3')? true : false;

cerfaFields['entretienMaisonCoche4']           = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modeHorsAutorisation).contains('entretienMaisonCoche4')? true : false;
cerfaFields['travauxpetitBricolageCoche4']     = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modeHorsAutorisation).contains('travauxpetitBricolageCoche4')? true : false;
cerfaFields['petitTravauxJardinageCoche4']     = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modeHorsAutorisation).contains('petitTravauxJardinageCoche4')? true : false;
cerfaFields['gardeEnfantPlus3ansCoche4']       = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modeHorsAutorisation).contains('gardeEnfantPlus3ansCoche4')? true : false;
cerfaFields['soutienScolaireCoche4']           = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modeHorsAutorisation).contains('soutienScolaireCoche4')? true : false;
cerfaFields['soinsEsthetiquesCoche4']          = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modeHorsAutorisation).contains('soinsEsthetiquesCoche4')? true : false;
cerfaFields['preparationRepasCoche4']          = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modeHorsAutorisation).contains('preparationRepasCoche4')? true : false;
cerfaFields['livraisonRepasDomicileCoche4']    = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modeHorsAutorisation).contains('livraisonRepasDomicileCoche4')? true : false;
cerfaFields['livraisonLingeCoche4']            = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modeHorsAutorisation).contains('livraisonLingeCoche4')? true : false;
cerfaFields['livraisonCoche4']                 = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modeHorsAutorisation).contains('livraisonCoche4')? true : false;
cerfaFields['assistantInformatiqueCoche4']     = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modeHorsAutorisation).contains('assistantInformatiqueCoche4')? true : false;
cerfaFields['soinsPromenadeCoche4']            = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modeHorsAutorisation).contains('soinsPromenadeCoche4')? true : false;
cerfaFields['maintenanceCoche4']               = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modeHorsAutorisation).contains('maintenanceCoche4')? true : false;
cerfaFields['assistanceAdministrativeCoche4']  = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modeHorsAutorisation).contains('assistanceAdministrativeCoche4')? true : false;
cerfaFields['accompagnementEnfantPlus3Coche4'] = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modeHorsAutorisation).contains('accompagnementEnfantPlus3Coche4')? true : false;
cerfaFields['teleassistanceCoche4']            = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modeHorsAutorisation).contains('teleassistanceCoche4')? true : false;
cerfaFields['interpreteCoche4']                = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modeHorsAutorisation).contains('interpreteCoche4')? true : false;
cerfaFields['conduiteVehiculeCoche4']          = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modeHorsAutorisation).contains('conduiteVehiculeCoche4')? true : false;
cerfaFields['accompagnementCoche4']            = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modeHorsAutorisation).contains('accompagnementCoche4')? true : false;
cerfaFields['assistanceCoche4']                = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modeHorsAutorisation).contains('assistanceCoche4')? true : false;
cerfaFields['coordinationDelivranceCoche4']    = Value('id').of($ds056PE1.modeIntervention.modeIntervention.modeHorsAutorisation).contains('coordinationDelivranceCoche4')? true : false;

cerfaFields['dateSignature']                   = $ds056PE1.signature.signature.dateSignature;
cerfaFields['lieuSignature']                   = $ds056PE1.signature.signature.lieuSignature;
cerfaFields['signatureConditionOui']           = Value('id').of($ds056PE1.signature.signature.signature2).contains('signatureConditionOui')? true : false;
cerfaFields['signatureConditionNon']           = Value('id').of($ds056PE1.signature.signature.signature2).contains('signatureConditionNon')? true : false;
cerfaFields['signatureCahierChargesOui']       = Value('id').of($ds056PE1.signature.signature.signature1).contains('signatureCahierChargesOui')? true : false;
cerfaFields['signatureCahierChargesNon']       = Value('id').of($ds056PE1.signature.signature.signature1).contains('signatureCahierChargesNon')? true : false;


// cerfaFields['libelleProfession']								  	  = "Agence de mannequins"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //0
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $ds056PE1.signatureGroup.signature.dateSignature,
		civiliteNomPrenom: civiliteNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $ds056PE1.signature.signature.dateSignature,
		autoriteHabilitee :"Préfecture du departement",
		demandeContexte : "Demande d'agrement",
		civiliteNomPrenom : civiliteNomPrenom
	});
	
var cerfaDoc = nash.doc //
    .load('models/demande_agrément.pdf') //
    .apply(cerfaFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */

appendPj($attachmentPreprocess.attachmentPreprocess.pjExtraitRegistreComemrce);
appendPj($attachmentPreprocess.attachmentPreprocess.pjQualiteService);
appendPj($attachmentPreprocess.attachmentPreprocess.pjInformationClients);
appendPj($attachmentPreprocess.attachmentPreprocess.pjListeSousTraitants);
appendPj($attachmentPreprocess.attachmentPreprocess.pjinformationEtatEtablissement);


var finalDocItem = finalDoc.save('Service_personne.pdf');


return spec.create({
    id : 'review',
   label : 'Service à la personne - demande d\’agrément.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Service à la personne - demande d\’agrément.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});