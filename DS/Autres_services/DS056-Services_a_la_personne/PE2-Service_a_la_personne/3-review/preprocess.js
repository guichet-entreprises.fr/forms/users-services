var cerfaFields = {};


/* var region = $ds056PE2.signatureGroup.signature.regionExercice; */

var civiliteNomPrenom = $ds056PE2.identiteOrganisme.identiteOrganisme.denominationSociale;

//Identité de l'organisme

cerfaFields['denominationSociale']             = $ds056PE2.identiteOrganisme.identiteOrganisme.denominationSociale;
cerfaFields['numeroSiret']                     = $ds056PE2.identiteOrganisme.identiteOrganisme.numeroSiret;
cerfaFields['nomCommercial']                   = $ds056PE2.identiteOrganisme.identiteOrganisme.nomCommercial;
cerfaFields['numeroNIC']                       = $ds056PE2.identiteOrganisme.identiteOrganisme.numeroNIC;
cerfaFields['numeroSiren']                     = $ds056PE2.identiteOrganisme.identiteOrganisme.numeroSiren;
cerfaFields['numeroTVA']                       = $ds056PE2.identiteOrganisme.identiteOrganisme.numeroTVA;
cerfaFields['nombreImplantation']              = $ds056PE2.identiteOrganisme.identiteOrganisme.nombreImplantation;
cerfaFields['categorieJuridique']              = $ds056PE2.identiteOrganisme.identiteOrganisme.categorieJuridique;
cerfaFields['immatriculationOui']              = Value('id').of($ds056PE2.identiteOrganisme.identiteOrganisme.immatriculationOuiNon).eq('immatriculationOui')? true : false;
cerfaFields['immatriculationNon']              = Value('id').of($ds056PE2.identiteOrganisme.identiteOrganisme.immatriculationOuiNon).eq('immatriculationNon')? true : false;

//Coordonnées 
cerfaFields['numeroNomVoieAdresse']            = $ds056PE2.coordonnees.coordonnees.numeroNomVoieAdresse;
cerfaFields['complementAdresse']               =($ds056PE2.coordonnees.coordonnees.complementAdresse != null ? $ds056PE2.coordonnees.coordonnees.complementAdresse :'');
cerfaFields['codePostalAdresse']               = $ds056PE2.coordonnees.coordonnees.codePostalAdresse;
cerfaFields['villeAdresse']                    = $ds056PE2.coordonnees.coordonnees.villeAdresse;
cerfaFields['paysAdresse']                     = $ds056PE2.coordonnees.coordonnees.paysAdresse;
cerfaFields['telephoneAdresse']                = $ds056PE2.coordonnees.coordonnees.telephoneAdresse;
cerfaFields['faxAdresse']                      = ($ds056PE2.coordonnees.coordonnees.faxAdresse  != null ? $ds056PE2.coordonnees.coordonnees.faxAdresse :'') ;

//Siége locaux 

cerfaFields['localDedieOuiSiege']              = Value('id').of($ds056PE2.siegeLocaux.siegeLocaux.localDedie).eq('localDedieOuiSiege')? true : false;
cerfaFields['localDedieNonSiege']              = Value('id').of($ds056PE2.siegeLocaux.siegeLocaux.localDedie).eq('localDedieNonSiege')? true : false;
cerfaFields['proprietaireSiege']               = Value('id').of($ds056PE2.siegeLocaux.siegeLocaux.occupant).contains('proprietaireSiege')? true : false;
cerfaFields['locataireSiege']                  = Value('id').of($ds056PE2.siegeLocaux.siegeLocaux.occupant).contains('locataireSiege')? true : false;
cerfaFields['miseDispositionSiege']            = Value('id').of($ds056PE2.siegeLocaux.siegeLocaux.occupant).contains('miseDispositionSiege')? true : false;
cerfaFields['disponibiliteTemporaireSiege']    = Value('id').of($ds056PE2.siegeLocaux.siegeLocaux.disponibilte).eq('disponibiliteTemporaireSiege')? true : false;
cerfaFields['disponibilitePermanenteSiege']    = Value('id').of($ds056PE2.siegeLocaux.siegeLocaux.disponibilte).eq('disponibilitePermanenteSiege')? true : false;
cerfaFields['surfaceTotalSiege']               = $ds056PE2.siegeLocaux.siegeLocaux.surfaceTotalSiege;
cerfaFields['accueilPublicSiege']              = $ds056PE2.siegeLocaux.siegeLocaux.accueilPublicSiege;
cerfaFields['handicapAccueilPublicOuiCoche']   = Value('id').of($ds056PE2.siegeLocaux.siegeLocaux.handicapOuiNon).eq('handicapAccueilPublicOuiCoche')? true : false;
cerfaFields['handicapAccueilPublicNonCoche']   = Value('id').of($ds056PE2.siegeLocaux.siegeLocaux.handicapOuiNon).eq('handicapAccueilPublicNonCoche')? true : false;
cerfaFields['reunionPersonnelSiege']           = $ds056PE2.siegeLocaux.siegeLocaux.reunionPersonnelSiege;
cerfaFields['handicapReunionPersonnelOui']     = Value('id').of($ds056PE2.siegeLocaux.siegeLocaux.handicapOuiNon2).eq('handicapReunionPersonnelOui')? true : false;
cerfaFields['handicapReunionPersonnelNon']     = Value('id').of($ds056PE2.siegeLocaux.siegeLocaux.handicapOuiNon2).eq('handicapReunionPersonnelNon')? true : false;


cerfaFields['monsieurDirigeant']               = Value('id').of($ds056PE2.dirigeant.dirigeant.civilite).eq('monsieurDirigeant')? true : false;
cerfaFields['madameDirigeant']                 = Value('id').of($ds056PE2.dirigeant.dirigeant.civilite).eq('madameDirigeant')? true : false;
cerfaFields['nomDirigeant']                    = $ds056PE2.dirigeant.dirigeant.nomDirigeant;
cerfaFields['prenomDirigeant']                 = $ds056PE2.dirigeant.dirigeant.prenomDirigeant;
cerfaFields['titreFonctionDirigeant']          = $ds056PE2.dirigeant.dirigeant.titreFonctionDirigeant;
cerfaFields['nationaliteDirigeant']            = $ds056PE2.dirigeant.dirigeant.nationaliteDirigeant;
cerfaFields['dataNaissanceDirigeant']          = $ds056PE2.dirigeant.dirigeant.dataNaissanceDirigeant;
cerfaFields['lieuNaissanceDirigeant']          = $ds056PE2.dirigeant.dirigeant.lieuNaissanceDirigeant;
cerfaFields['departementDirigeant']            = $ds056PE2.dirigeant.dirigeant.departementDirigeant;
cerfaFields['paysNaissanceDirigeant']          = $ds056PE2.dirigeant.dirigeant.paysNaissanceDirigeant;

//Personne chargé de l'administration

cerfaFields['madameContactAdministration']     = Value('id').of($ds056PE2.contactAdministration.contactAdministration.civiliteContact).eq('monsieurContactAdministration')? true : false;
cerfaFields['monsieurContactAdministration']   = Value('id').of($ds056PE2.contactAdministration.contactAdministration.civiliteContact).eq('madameContactAdministration')? true : false;
cerfaFields['nomContactAdministration']        = $ds056PE2.contactAdministration.contactAdministration.nomContactAdministration;
cerfaFields['prenomContactAdministration']     = $ds056PE2.contactAdministration.contactAdministration.prenomContactAdministration;
cerfaFields['fonctionContactAdministration']   = $ds056PE2.contactAdministration.contactAdministration.fonctionContactAdministration;
cerfaFields['telephoneContactAdministration']  = $ds056PE2.contactAdministration.contactAdministration.telephoneContactAdministration;
cerfaFields['courrielContactAdministration']   = $ds056PE2.contactAdministration.contactAdministration.courrielContactAdministration;

// Mode d'intervention


cerfaFields['assistanceAgeesCoche3']           = Value('id').of($ds056PE2.modeIntervention.modeIntervention.modePrestataire).contains('assistanceAgeesCoche3')? true : false;
cerfaFields['assistanceHandicapeesCoche3']     = Value('id').of($ds056PE2.modeIntervention.modeIntervention.modePrestataire).contains('assistanceHandicapeesCoche3')? true : false;
cerfaFields['conduiteVehiculeCoche3']          = Value('id').of($ds056PE2.modeIntervention.modeIntervention.modePrestataire).contains('conduiteVehiculeCoche3')? true : false;
cerfaFields['accompagnementPACoche3']          = Value('id').of($ds056PE2.modeIntervention.modeIntervention.modePrestataire).contains('accompagnementPACoche3')? true : false;
cerfaFields['aideFamFragiliseesCoche3']        = Value('id').of($ds056PE2.modeIntervention.modeIntervention.modePrestataire).contains('aideFamFragiliseesCoche3')? true : false;

// Signature

cerfaFields['dateSignature']                   = $ds056PE2.signature.signature.dateSignature;
cerfaFields['lieuSignature']                   = $ds056PE2.signature.signature.lieuSignature;
cerfaFields['signatureConditionOui']           = Value('id').of($ds056PE2.signature.signature.signature2).contains('signatureConditionOui')? true : false;
cerfaFields['signatureConditionNon']           = Value('id').of($ds056PE2.signature.signature.signature2).contains('signatureConditionNon')? true : false;
cerfaFields['signatureCahierChargesOui']       = Value('id').of($ds056PE2.signature.signature.signature1).contains('signatureCahierChargesOui')? true : false;
cerfaFields['signatureCahierChargesNon']       = Value('id').of($ds056PE2.signature.signature.signature1).contains('signatureCahierChargesNon')? true : false;


// cerfaFields['libelleProfession']								  	  = "Agence de mannequins"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //0
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $ds056PE2.signatureGroup.signature.dateSignature,
		civiliteNomPrenom: civiliteNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $ds056PE2.signature.signature.dateSignature,
		autoriteHabilitee :"Préfecture du departement",
		demandeContexte : "Demande d'autorisation d'exercice de l'activité avec les personnes âgées et les personnes handicapées",
		civiliteNomPrenom : civiliteNomPrenom
	});
	
var cerfaDoc = nash.doc //
    .load('models/demande_autorisation.pdf') //
    .apply(cerfaFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */

appendPj($attachmentPreprocess.attachmentPreprocess.pjExtraitRegistreComemrce);
appendPj($attachmentPreprocess.attachmentPreprocess.pjQualiteService);
appendPj($attachmentPreprocess.attachmentPreprocess.pjInformationClients);
appendPj($attachmentPreprocess.attachmentPreprocess.pjListeSousTraitants);
appendPj($attachmentPreprocess.attachmentPreprocess.pjinformationEtatEtablissement);


var finalDocItem = finalDoc.save('Service_personne.pdf');


return spec.create({
    id : 'review',
   label : 'Service à la personne - demande d\'autorisation d\'exercice de l\'activité avec les personnes âgées et les personnes handicapées.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Service à la personne - demande d\'autorisation d\'exercice de l\'activité avec les personnes âgées et les personnes handicapées.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});