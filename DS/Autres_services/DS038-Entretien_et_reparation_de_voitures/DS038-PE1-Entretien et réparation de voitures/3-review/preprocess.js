var cerfaFields = {};



var civiliteNomPrenom = ($ds038PE1.declarant.declarant.nom !=null ? $ds038PE1.declarant.declarant.nom :'')  + ' ' + ($ds038PE1.declarant.declarant.prenom !=null ? $ds038PE1.declarant.declarant.prenom :'') + ' ' + ($ds038PE1.declarant.declarant.nomRaisonSociale !=null ?$ds038PE1.declarant.declarant.nomRaisonSociale:'');


// Declarant                     

cerfaFields['personneMoraleCoche']                 = Value('id').of($ds038PE1.declarant.declarant.personneMoralePhysique).eq('personneMoraleCoche') ? true : false;
cerfaFields['personnePhysiqueCoche']               = Value('id').of($ds038PE1.declarant.declarant.personneMoralePhysique).eq('personnePhysiqueCoche') ? true : false;
cerfaFields['madame']                              = Value('id').of($ds038PE1.declarant.declarant.civilite).eq('madame') ? true : false;
cerfaFields['monsieur']                            = Value('id').of($ds038PE1.declarant.declarant.civilite).eq('monsieur') ? true : false;
cerfaFields['nomRaisonSocialeDeclarant']           = ($ds038PE1.declarant.declarant.nom !=null ? $ds038PE1.declarant.declarant.nom :'')  + ' ' + ($ds038PE1.declarant.declarant.prenom !=null ? $ds038PE1.declarant.declarant.prenom :'') + ' ' + ($ds038PE1.declarant.declarant.nomRaisonSociale !=null ?$ds038PE1.declarant.declarant.nomRaisonSociale:'');
cerfaFields['formeJuridique']                      = $ds038PE1.declarant.declarant.formeJuridique;
cerfaFields['numeroSiret']                         = $ds038PE1.declarant.declarant.numeroSiret;
cerfaFields['numeroNomVoieAdresse']                = $ds038PE1.declarant.declarant.numeroNomVoieAdresse;
cerfaFields['complementAdresse']                   = ($ds038PE1.declarant.declarant.complementAdresse !=null ? $ds038PE1.declarant.declarant.complementAdresse :'');
cerfaFields['codePostalAdresse']                   = ($ds038PE1.declarant.declarant.codePostalAdresse !=null  ? $ds038PE1.declarant.declarant.codePostalAdresse :'');
cerfaFields['communeAdresse']                      = ($ds038PE1.declarant.declarant.communeAdresse !=null  ?$ds038PE1.declarant.declarant.communeAdresse :'');
cerfaFields['paysEtrangerAdresse']                 = $ds038PE1.declarant.declarant.paysEtrangerAdresse;
cerfaFields['regionEtrangerAdresse']               = $ds038PE1.declarant.declarant.regionEtrangerAdresse;
cerfaFields['telephonePortableDeclarant']          = $ds038PE1.declarant.declarant.telephonePortableDeclarant;
cerfaFields['faxDeclarant']                        = $ds038PE1.declarant.declarant.faxDeclarant;
cerfaFields['telephoneFixeDeclarant']              = $ds038PE1.declarant.declarant.telephoneFixeDeclarant;
cerfaFields['courrielDeclarant']                   = $ds038PE1.declarant.declarant.courrielDeclarant;
cerfaFields['nomSignataire']                       = $ds038PE1.declarant.signataireDeclarant.nomSignataire;
cerfaFields['prenomsSignataire']                   = $ds038PE1.declarant.signataireDeclarant.prenomsSignataire;
cerfaFields['qualiteSignataure']                   = ($ds038PE1.declarant.signataireDeclarant.qualiteSignataure !=null ? $ds038PE1.declarant.signataireDeclarant.qualiteSignataure :'');

// Information génerales concernant l'installationAutorisation

cerfaFields['numeroSiretInstallation']             = $ds038PE1.informationsGenerales.informationsGenerales.numeroSiretInstallation;
cerfaFields['siteNomInstallation']                 = $ds038PE1.informationsGenerales.informationsGenerales.siteNomInstallation;
cerfaFields['adresseIdentiqueInstallation']        = Value('id').of($ds038PE1.informationsGenerales.informationsGenerales.adresseSiteInstallation).eq('adresseIdentiqueInstallation') ? true : false;
cerfaFields['numeroNomVoieAdresseInstallation']    = $ds038PE1.informationsGenerales.informationsGenerales.numeroNomVoieAdresseInstallation;
cerfaFields['complementAdresseInstallation']       = $ds038PE1.informationsGenerales.informationsGenerales.complementAdresseInstallation;
cerfaFields['codePostalAdresseInstallation']       = $ds038PE1.informationsGenerales.informationsGenerales.complementAdresseInstallation;
cerfaFields['communeAdresseInstallation']          = $ds038PE1.informationsGenerales.informationsGenerales.communeAdresseInstallation;
cerfaFields['telephoneFixeInstallation']           = $ds038PE1.informationsGenerales.informationsGenerales.telephoneFixeInstallation;
cerfaFields['faxInstallation']                     = $ds038PE1.informationsGenerales.informationsGenerales.faxInstallation;
cerfaFields['telephonePortableInstallation']       = $ds038PE1.informationsGenerales.informationsGenerales.telephonePortableInstallation;
cerfaFields['courrielInstallation']                = $ds038PE1.informationsGenerales.informationsGenerales.courrielInstallation;
cerfaFields['descriptionGeneraleInstallation']     = $ds038PE1.informationsGenerales.informationsGenerales.descriptionGeneraleInstallation;
cerfaFields['installationRegimeAutorisatioNon']    = Value('id').of($ds038PE1.informationsGenerales.siteInstallation.installationAutorisation).eq('installationRegimeAutorisatioNon') ? true : false;
cerfaFields['installationRegimeAutorisationOui']   = Value('id').of($ds038PE1.informationsGenerales.siteInstallation.installationAutorisation).eq('installationRegimeAutorisationOui') ? true : false;
cerfaFields['installationRegimeEnregistrementOui'] = Value('id').of($ds038PE1.informationsGenerales.siteInstallation.installationEnregistrement).eq('installationRegimeEnregistrementOui') ? true : false;
cerfaFields['installationRegimeEnregistrementNon'] = Value('id').of($ds038PE1.informationsGenerales.siteInstallation.installationEnregistrement).eq('installationRegimeEnregistrementNon') ? true : false;
cerfaFields['installationRegimeDeclarationOui']    = Value('id').of($ds038PE1.informationsGenerales.siteInstallation.installationdeclaration).eq('installationRegimeDeclarationOui') ? true : false;
cerfaFields['installationRegimeDeclarationNon']    = Value('id').of($ds038PE1.informationsGenerales.siteInstallation.installationdeclaration).eq('installationRegimeDeclarationNon') ? true : false;

//Implantation de l'installation

cerfaFields['implantatinPlusieursDepartementOui']  = Value('id').of($ds038PE1.implantationInstallation.implantationInstallation.implantationDepartement).eq('implantatinPlusieursDepartementOui') ? true : false;
cerfaFields['implantatinPlusieursDepartementNon']  = Value('id').of($ds038PE1.implantationInstallation.implantationInstallation.implantationDepartement).eq('implantatinPlusieursDepartementNon') ? true : false;
cerfaFields['implantatinPlusieursDepartement']     = $ds038PE1.implantationInstallation.implantationInstallation.implantatinPlusieursDepartement;
cerfaFields['implantatinPlusieursCommunesOui']     = Value('id').of($ds038PE1.implantationInstallation.implantationInstallation.implantationCommune).eq('implantatinPlusieursCommunesOui') ? true : false;
cerfaFields['implantatinPlusieursCommunesNon']     = Value('id').of($ds038PE1.implantationInstallation.implantationInstallation.implantationCommune).eq('implantatinPlusieursCommunesNon') ? true : false;
cerfaFields['implantationPlusieursCommunes']       = $ds038PE1.implantationInstallation.implantationInstallation.implantationPlusieursCommunes;
cerfaFields['permisConstruireOui']                 = Value('id').of($ds038PE1.implantationInstallation.permisConstruire.permisConstruire1).eq('permisConstruireOui') ? true : false;
cerfaFields['permisConstruireNon']                 = Value('id').of($ds038PE1.implantationInstallation.permisConstruire.permisConstruire1).eq('permisConstruireNon') ? true : false;

// Nature et Volume des activités 
//remplissage à vide pour le visuel du formulaire final
for (var i= $ds038PE1.natureActivites.natureActivites.size(); i < 13; i++ ){
	cerfaFields['numeroRubrique'+i]                =   '';
	cerfaFields['alinea'+i]                        =   '';
	cerfaFields['designationRubrique'+i]           =   '';
	cerfaFields['capaciteActivite'+i]              =   '';
	cerfaFields['unite'+i]                         =   '';
	cerfaFields['regime'+i]                        =   '';
}

//Remplissage cardinalité
for (var i= 0; i < $ds038PE1.natureActivites.natureActivites.size(); i++ ){
	var r = i+1;
	cerfaFields['numeroRubrique'+r]                       = $ds038PE1.natureActivites.natureActivites[i].numeroRubrique ;
	cerfaFields['alinea'+r]                         	 = $ds038PE1.natureActivites.natureActivites[i].alinea;
	cerfaFields['designationRubrique'+r]                  = $ds038PE1.natureActivites.natureActivites[i].designationRubrique;
	cerfaFields['capaciteActivite'+r]                     = $ds038PE1.natureActivites.natureActivites[i].capaciteActivite;
	cerfaFields['unite'+r]                        		 = $ds038PE1.natureActivites.natureActivites[i].unite;
	cerfaFields['regime'+r]                       		 = $ds038PE1.natureActivites.natureActivites[i].regime;
}

cerfaFields['commentaireNatureActivite']           = $ds038PE1.natureActivites.commentaireActivite.commentaireNatureActivite;

//Présentation des modes d'exploitation

cerfaFields['prelevementEauExploitationNon']       = Value('id').of($ds038PE1.modeExploitation.modeExploitation.implantationCommune).eq('prelevementEauExploitationNon') ? true : false;
cerfaFields['prelevementEauExploitationOui']       = Value('id').of($ds038PE1.modeExploitation.modeExploitation.implantationCommune).eq('prelevementEauExploitationOui') ? true : false;
cerfaFields['reseauPublicExploitation']            = Value('id').of($ds038PE1.modeExploitation.modeExploitation.modePrelevementEau).contains('reseauPublicExploitation') ? true : false;
cerfaFields['milieuNaturelExploitation']           = Value('id').of($ds038PE1.modeExploitation.modeExploitation.modePrelevementEau).contains('milieuNaturelExploitation') ? true : false;
cerfaFields['forageSouterrainExploitation']        = Value('id').of($ds038PE1.modeExploitation.modeExploitation.modePrelevementEau).contains('forageSouterrainExploitation') ? true : false;
cerfaFields['autresExploitationCoche']             = Value('id').of($ds038PE1.modeExploitation.modeExploitation.modePrelevementEau).contains('autresExploitationCoche') ? true : false;
cerfaFields['volumeReseauPublicExploitation']      = $ds038PE1.modeExploitation.modeExploitation.volumeReseauPublicExploitation;
cerfaFields['volumeMilieuNaturelExploitation']     = $ds038PE1.modeExploitation.modeExploitation.volumeMilieuNaturelExploitation;
cerfaFields['volumeForageSouterrain']              = $ds038PE1.modeExploitation.modeExploitation.volumeForageSouterrain;
cerfaFields['forageSouterrainPlus10Exploitation']  = $ds038PE1.modeExploitation.modeExploitation.forageSouterrainPlus10Exploitation;
cerfaFields['autresExploitation']                  = $ds038PE1.modeExploitation.modeExploitation.autresExploitation;
cerfaFields['rejetEauxResiduairesNon']             = Value('id').of($ds038PE1.modeExploitation.modeExploitation.rejetEauxResiduaires).eq('rejetEauxResiduairesNon') ? true : false;
cerfaFields['rejetEauxResiduairesOui']             = Value('id').of($ds038PE1.modeExploitation.modeExploitation.rejetEauxResiduaires).eq('rejetEauxResiduairesOui') ? true : false;
cerfaFields['origineNatureEauxResiduaires']        = $ds038PE1.modeExploitation.modeExploitation.origineNatureEauxResiduaires;
cerfaFields['milieuNaturel']                       = Value('id').of($ds038PE1.modeExploitation.modeExploitation.exutoireEauxResiduaires).eq('milieuNaturel') ? true : false;
cerfaFields['reseauAssainissementCollectif']       = Value('id').of($ds038PE1.modeExploitation.modeExploitation.exutoireEauxResiduaires).eq('reseauAssainissementCollectif') ? true : false;
cerfaFields['traitementEauxResiduaires']           = $ds038PE1.modeExploitation.modeExploitation.traitementEauxResiduaires;
cerfaFields['volumeAnnuelRejete']                  = $ds038PE1.modeExploitation.modeExploitation.volumeAnnuelRejete;
cerfaFields['autresCommentairesEauxResiduaires']   = $ds038PE1.modeExploitation.modeExploitation.autresCommentairesEauxResiduaires;
cerfaFields['epandageDechetsOui']                  = Value('id').of($ds038PE1.modeExploitation.modeExploitation.epandageDechets1).eq('epandageDechetsOui') ? true : false;
cerfaFields['epandageDechetsNon']                  = Value('id').of($ds038PE1.modeExploitation.modeExploitation.epandageDechets1).eq('epandageDechetsNon') ? true : false;
cerfaFields['epandageDechets']                     = $ds038PE1.modeExploitation.modeExploitation.epandageDechets;
cerfaFields['ilotPac']                             = $ds038PE1.modeExploitation.modeExploitation.ilotPac;
cerfaFields['surafeTotaleEpandage']                = $ds038PE1.modeExploitation.modeExploitation.surafeTotaleEpandage;
cerfaFields['quantiteAzoteEpandue']                = $ds038PE1.modeExploitation.modeExploitation.quantiteAzoteEpandue;
cerfaFields['epandueA1']                           = $ds038PE1.modeExploitation.modeExploitation.epandueA1;
cerfaFields['epandueA2']                           = $ds038PE1.modeExploitation.modeExploitation.epandueA2;
cerfaFields['produitB2']                           = $ds038PE1.modeExploitation.modeExploitation.produitB2;
cerfaFields['produitB1']                           = $ds038PE1.modeExploitation.modeExploitation.produitB1;
cerfaFields['capaciteStockage']                    = $ds038PE1.modeExploitation.modeExploitation.capaciteStockage;
cerfaFields['rejetsAtmosphereOui']                 = Value('id').of($ds038PE1.modeExploitation.modeExploitation.rejetsAtmosphere).eq('rejetsAtmosphereOui') ? true : false;
cerfaFields['rejetsAtmosphereNon']                 = Value('id').of($ds038PE1.modeExploitation.modeExploitation.rejetsAtmosphere).eq('rejetsAtmosphereNon') ? true : false;
cerfaFields['origineNatureRejets']                 = $ds038PE1.modeExploitation.modeExploitation.origineNatureRejets;
cerfaFields['dispositifsCaptationTraitement']      = $ds038PE1.modeExploitation.modeExploitation.dispositifsCaptationTraitement;
cerfaFields['autresCommentairesRejets']            = $ds038PE1.modeExploitation.modeExploitation.autresCommentairesRejets;
cerfaFields['autresCommentairesDechets']           = $ds038PE1.modeExploitation.eliminationDechetsResidus.autresCommentairesDechets;
cerfaFields['collecteDechetsPublicOui']            = Value('id').of($ds038PE1.modeExploitation.eliminationDechetsResidus.collecteDechetsPublic).eq('collecteDechetsPublicOui') ? true : false;
cerfaFields['collecteDechetsPublicNon']            = Value('id').of($ds038PE1.modeExploitation.eliminationDechetsResidus.collecteDechetsPublic).eq('collecteDechetsPublicNon') ? true : false;


cerfaFields['priseEauSinistre']                    = Value('id').of($ds038PE1.modeExploitation.casSinistre.capaciteEau).contains('priseEauSinistre') ? true : false;
cerfaFields['autreSinistre']                       = Value('id').of($ds038PE1.modeExploitation.casSinistre.capaciteEau).contains('autreSinistre') ? true : false;
cerfaFields['autreCommentaireSinistre']            = $ds038PE1.modeExploitation.casSinistre.autreCommentaireSinistre;
cerfaFields['autresMoyensSinistre']                = $ds038PE1.modeExploitation.casSinistre.autresMoyensSinistre;

// Demande d'agrément 

cerfaFields['demandeAgrementOui']                  = Value('id').of($ds038PE1.demandeAgrement.demandeAgrement.installationTraitementDechets).eq('demandeAgrementOui') ? true : false;
cerfaFields['demandeAgrementNon']                  = Value('id').of($ds038PE1.demandeAgrement.demandeAgrement.installationTraitementDechets).eq('demandeAgrementNon') ? true : false;

for (var i= $ds038PE1.demandeAgrement.tableauDechets.size(); i < 26; i++ ){
	cerfaFields['natureDechets'+i]                	=   '';
	cerfaFields['codificationDechets'+i]            =   '';
	cerfaFields['traitement'+i]           			=   '';
	cerfaFields['codification'+i]              		=   '';
	cerfaFields['quantitesMaximales'+i]             =   '';
	
}

//Remplissage cardinalité
for (var i= 0; i < $ds038PE1.demandeAgrement.tableauDechets.size(); i++ ){
	var r = i+1;
	cerfaFields['natureDechets'+r]               = $ds038PE1.demandeAgrement.tableauDechets[i].natureDechets ;
	cerfaFields['codificationDechets'+r]         = $ds038PE1.demandeAgrement.tableauDechets[i].codificationDechets;
	cerfaFields['traitement'+r]                  = $ds038PE1.demandeAgrement.tableauDechets[i].traitement;
	cerfaFields['codification'+r]                = $ds038PE1.demandeAgrement.tableauDechets[i].codification;
	cerfaFields['quantitesMaximales'+r]          = $ds038PE1.demandeAgrement.tableauDechets[i].quantitesMaximales;
	
}

cerfaFields['commentairesTypesAgrements']          = $ds038PE1.demandeAgrement.commentairesAgrement.commentairesTypesAgrements;


//Natura 2000

cerfaFields['naturaNon']                           = Value('id').of($ds038PE1.natura.natura.natura1).eq('naturaNon') ? true : false;
cerfaFields['naturaOui']                           = Value('id').of($ds038PE1.natura.natura.natura1).eq('naturaOui') ? true : false;
cerfaFields['prescriptionsOui']                    = Value('id').of($ds038PE1.prescriptionApplicable.prescriptionApplicable.prescriptionApplicable1).eq('prescriptionsOui') ? true : false;
cerfaFields['prescriptionsNon']                    = Value('id').of($ds038PE1.prescriptionApplicable.prescriptionApplicable.prescriptionApplicable1).eq('prescriptionsNon') ? true : false;


cerfaFields['lieuSignature']                       = $ds038PE1.signature.signature.lieuSignature; 
cerfaFields['dateSignature']                       = $ds038PE1.signature.signature.dateSignature; 



 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $ds038PE1.signature.signature.dateSignature,
		autoriteHabilitee :"Préfecture du département",
		demandeContexte : "Déclaration ICPE",
		civiliteNomPrenom : civiliteNomPrenom
	});
	
var cerfaDoc = nash.doc //
    .load('models/Cerfa n° 15271_02.pdf') //
    .apply(cerfaFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjLettreDemande);
appendPj($attachmentPreprocess.attachmentPreprocess.pjrubriquesnomenclature);
appendPj($attachmentPreprocess.attachmentPreprocess.pjpresentationModeExploitataion);
appendPj($attachmentPreprocess.attachmentPreprocess.pjNatura2000);

var finalDocItem = finalDoc.save('Entretien_reparation_voiture.pdf');


return spec.create({
    id : 'review',
   label : 'Entretien et reparation de voitures - déclaration ICPE',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Entretien et reparation de voitures - déclaration ICPE',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});