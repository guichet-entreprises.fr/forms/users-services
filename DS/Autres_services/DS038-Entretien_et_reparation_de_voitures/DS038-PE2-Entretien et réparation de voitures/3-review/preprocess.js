var cerfaFields = {};



var civiliteNomPrenom = ($ds038PE1.declarant.declarant.nom !=null ? $ds038PE1.declarant.declarant.nom :'')  + ' ' + ($ds038PE1.declarant.declarant.prenom !=null ? $ds038PE1.declarant.declarant.prenom :'') + ' ' + ($ds038PE1.declarant.declarant.nomRaisonSociale !=null ?$ds038PE1.declarant.declarant.nomRaisonSociale:'');


// Declarant                     

cerfaFields['personneMoraleCoche']                 = Value('id').of($ds038PE1.declarant.declarant.personneMoralePhysique).eq('personneMoraleCoche') ? true : false;
cerfaFields['personnePhysiqueCoche']               = Value('id').of($ds038PE1.declarant.declarant.personneMoralePhysique).eq('personnePhysiqueCoche') ? true : false;
cerfaFields['madame']                              = Value('id').of($ds038PE1.declarant.declarant.civilite).eq('madame') ? true : false;
cerfaFields['monsieur']                            = Value('id').of($ds038PE1.declarant.declarant.civilite).eq('monsieur') ? true : false;
cerfaFields['nomRaisonSocialeDeclarant']           = ($ds038PE1.declarant.declarant.nom !=null ? $ds038PE1.declarant.declarant.nom :'')  + ' ' + ($ds038PE1.declarant.declarant.prenom !=null ? $ds038PE1.declarant.declarant.prenom :'') + ' ' + ($ds038PE1.declarant.declarant.nomRaisonSociale !=null ?$ds038PE1.declarant.declarant.nomRaisonSociale:'');
cerfaFields['formeJuridique']                      = $ds038PE1.declarant.declarant.formeJuridique;
cerfaFields['numeroSiret']                         = $ds038PE1.declarant.declarant.numeroSiret;
cerfaFields['numeroNomVoieAdresse']                = $ds038PE1.declarant.declarant.numeroNomVoieAdresse;
cerfaFields['complementAdresse']                   = ($ds038PE1.declarant.declarant.complementAdresse !=null ? $ds038PE1.declarant.declarant.complementAdresse :'');
cerfaFields['codePostalAdresse']                   = ($ds038PE1.declarant.declarant.codePostalAdresse !=null  ? $ds038PE1.declarant.declarant.codePostalAdresse :'');
cerfaFields['communeAdresse']                      = ($ds038PE1.declarant.declarant.communeAdresse !=null  ?$ds038PE1.declarant.declarant.communeAdresse :'');
cerfaFields['paysEtrangerAdresse']                 = $ds038PE1.declarant.declarant.paysEtrangerAdresse;
cerfaFields['regionEtrangerAdresse']               = $ds038PE1.declarant.declarant.regionEtrangerAdresse;
cerfaFields['telephonePortableDeclarant']          = $ds038PE1.declarant.declarant.telephonePortableDeclarant;
cerfaFields['faxDeclarant']                        = $ds038PE1.declarant.declarant.faxDeclarant;
cerfaFields['telephoneFixeDeclarant']              = $ds038PE1.declarant.declarant.telephoneFixeDeclarant;
cerfaFields['courrielDeclarant']                   = $ds038PE1.declarant.declarant.courrielDeclarant;
cerfaFields['nomSignataire']                       = $ds038PE1.declarant.signataireDeclarant.nomSignataire;
cerfaFields['prenomsSignataire']                   = $ds038PE1.declarant.signataireDeclarant.prenomsSignataire;
cerfaFields['qualiteSignataure']                   = ($ds038PE1.declarant.signataireDeclarant.qualiteSignataure !=null ? $ds038PE1.declarant.signataireDeclarant.qualiteSignataure :'');



cerfaFields['lieuSignature']                       = $ds038PE1.signature.signature.lieuSignature; 
cerfaFields['dateSignature']                       = $ds038PE1.signature.signature.dateSignature; 



 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $ds038PE1.signature.signature.dateSignature,
		autoriteHabilitee :"Préfecture du département",
		demandeContexte : "Déclaration ICPE",
		civiliteNomPrenom : civiliteNomPrenom
	});
	
var cerfaDoc = nash.doc //
    .load('models/Cerfa n° 15271_02.pdf') //
    .apply(cerfaFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjLettreDemande);
appendPj($attachmentPreprocess.attachmentPreprocess.pjrubriquesnomenclature);
appendPj($attachmentPreprocess.attachmentPreprocess.pjpresentationModeExploitataion);
appendPj($attachmentPreprocess.attachmentPreprocess.pjNatura2000);

var finalDocItem = finalDoc.save('Entretien_reparation_voiture.pdf');


return spec.create({
    id : 'review',
   label : 'Entretien et réparation de voitures - demande d\'attestation de capacité',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Entretien et réparation de voitures - Demande d'attestation de capacité',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});