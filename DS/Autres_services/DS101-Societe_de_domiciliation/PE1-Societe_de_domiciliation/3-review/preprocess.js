var formFields = {};


var civNomPrenom = $ds101PE1.declarationGroup.identificationExploitant.civilite + " " + $ds101PE1.declarationGroup.identificationExploitant.nomNaissanceExploitant + " " + $ds101PE1.declarationGroup.identificationExploitant.prenomExploitant;

// Déclaration de la société

formFields['denominationSociale']              = $ds101PE1.declarationSocieteGroup.declarationSociete.denominationSociale;
formFields['activitesEntreprise']              = $ds101PE1.declarationSocieteGroup.declarationSociete.activitesEntreprise;
formFields['adresseEntreprise']                = ($ds101PE1.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntreprise1 != null ? $ds101PE1.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntreprise1 : '') + ' ' + ($ds101PE1.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntreprise2 != null ? $ds101PE1.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntreprise2 : '') + ' '+($ds101PE1.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntreprise3 != null ? $ds101PE1.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntreprise3 : '')+' '+ ($ds101PE1.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntreprise4 != null ? $ds101PE1.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntreprise4 : '');

for (var i = 0;i < $ds101PE1.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEtablissementSecondaireGroup.size(); i++) {

formFields['adresseEtablissementSecondaire' + i]         = ($ds101PE1.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEtablissementSecondaireGroup[i].adresseEtabSecVoie != null ? $ds101PE1.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEtablissementSecondaireGroup[i].adresseEtabSecVoie : '') + " " + ($ds101PE1.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEtablissementSecondaireGroup[i].adresseEtabSecComp != null ? $ds101PE1.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEtablissementSecondaireGroup[i].adresseEtabSecComp : '') + " " +($ds101PE1.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEtablissementSecondaireGroup[i].adresseEtabSecVille != null ? $ds101PE1.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEtablissementSecondaireGroup[i].adresseEtabSecVille : '') + " " +($ds101PE1.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEtablissementSecondaireGroup[i].adresseEtabSecCP != null ? $ds101PE1.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEtablissementSecondaireGroup[i].adresseEtabSecCP : '');

}

// Identification de l'exploitant

formFields['nomPrenomExploitant']              = $ds101PE1.declarationGroup.identificationExploitant.nomNaissanceExploitant + " " + $ds101PE1.declarationGroup.identificationExploitant.prenomExploitant;
formFields['dateNaissanceExploitant']      	   = $ds101PE1.declarationGroup.identificationExploitant.dateNaissanceExploitant;
formFields['lieuNaissanceExploitant']          = "à" + " " + $ds101PE1.declarationGroup.identificationExploitant.lieuNaissanceExploitant + ", " + $ds101PE1.declarationGroup.identificationExploitant.paysNaissanceExploitant;
formFields['adresseDomicileExploitant']        = ($ds101PE1.declarationGroup.identificationExploitant.adresseDomicileExploitant.adresseDomicileExploitant1 != null ? $ds101PE1.declarationGroup.identificationExploitant.adresseDomicileExploitant.adresseDomicileExploitant1 : '') + ' ' + ($ds101PE1.declarationGroup.identificationExploitant.adresseDomicileExploitant.adresseDomicileExploitant2 != null ? $ds101PE1.declarationGroup.identificationExploitant.adresseDomicileExploitant.adresseDomicileExploitant2 : '') + ' ' + ($ds101PE1.declarationGroup.identificationExploitant.adresseDomicileExploitant.adresseDomicileExploitant3 != null ? $ds101PE1.declarationGroup.identificationExploitant.adresseDomicileExploitant.adresseDomicileExploitant3 : '') + ' ' + ($ds101PE1.declarationGroup.identificationExploitant.adresseDomicileExploitant.adresseDomicileExploitant4 != null ? $ds101PE1.declarationGroup.identificationExploitant.adresseDomicileExploitant.adresseDomicileExploitant4 : '') + ' ' + ($ds101PE1.declarationGroup.identificationExploitant.adresseDomicileExploitant.adresseDomicileExploitant5 != null ? $ds101PE1.declarationGroup.identificationExploitant.adresseDomicileExploitant.adresseDomicileExploitant5 : '');
formFields['professionExploitant']             = $ds101PE1.declarationGroup.identificationExploitant.professionExploitant;

/*
 * Chargement de l'attestation d'honorabilité du déclarant
 */

 
//formFields[nomPrenomExploitant] = $ds101PE1.declarationGroup.identificationExploitant.nomNaissanceExploitant + " " + $ds101PE1.declarationGroup.identificationExploitant.prenomExploitant;
//formFields[adresseDomicileExploitant] = ($ds101PE1.declarationGroup.identificationExploitant.adresseDomicileExploitant.adresseDomicileExploitant1 != null ? $ds101PE1.declarationGroup.identificationExploitant.adresseDomicileExploitant.adresseDomicileExploitant1 : '') + ' ' + ($ds101PE1.declarationGroup.identificationExploitant.adresseDomicileExploitant.adresseDomicileExploitant2 != null ? $ds101PE1.declarationGroup.identificationExploitant.adresseDomicileExploitant.adresseDomicileExploitant2 : '') + ' ' + ($ds101PE1.declarationGroup.identificationExploitant.adresseDomicileExploitant.adresseDomicileExploitant3 != null ? $ds101PE1.declarationGroup.identificationExploitant.adresseDomicileExploitant.adresseDomicileExploitant3 : '') + ' ' + ($ds101PE1.declarationGroup.identificationExploitant.adresseDomicileExploitant.adresseDomicileExploitant4 != null ? $ds101PE1.declarationGroup.identificationExploitant.adresseDomicileExploitant.adresseDomicileExploitant4 : '') + ' ' + ($ds101PE1.declarationGroup.identificationExploitant.adresseDomicileExploitant.adresseDomicileExploitant5 != null ? $ds101PE1.declarationGroup.identificationExploitant.adresseDomicileExploitant.adresseDomicileExploitant5 : '');
formFields['professionExploitant2'] = $ds101PE1.declarationGroup.identificationExploitant.professionExploitant;
formFields['qualiteExploitantIndividuel'] = true;
formFields['qualiteDirigeant'] = false;
formFields['qualiteActionnaireAssocie'] = false;
//formFields[denominationSociale] = $ds101PE1.declarationSocieteGroup.declarationSociete.denominationSociale;
formFields['formeJuridique'] = ($ds101PE1.declarationSocieteGroup.declarationSociete.formeJuridique != null ? (Value('id').of($ds101PE1.declarationSocieteGroup.declarationSociete.formeJuridique).eq('microEntrepreneur') ? "Micro-entrepreneur" : (Value('id').of($ds101PE1.declarationSocieteGroup.declarationSociete.formeJuridique).eq('entrepreneurIndividuel') ? "Entrepreneur individuel à responsabilité limitée (EIRL)" : "Micro-entrepreneur à responsabilité limitée")) : "Entreprise individuelle");
//formFields[adresseEntreprise] = ($ds101PE1.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntreprise1 != null ? $ds101PE1.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntreprise1 : '') + ' ' + ($ds101PE1.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntreprise2 != null ? $ds101PE1.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntreprise2 : '') + ' '+($ds101PE1.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntreprise3 != null ? $ds101PE1.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntreprise3 : '')+' '+ ($ds101PE1.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntreprise4 != null ? $ds101PE1.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntreprise4 : '') +' '+ ($ds101PE1.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntreprise5 != null ? $ds101PE1.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntreprise5 : '');
formFields['signatureLieu'] = $ds101PE1.signatureGroup.signature.signatureLieu;
formFields['signatureDate'] = $ds101PE1.signatureGroup.signature.signatureDate;
formFields['signatureCoche'] = $ds101PE1.signatureGroup.signature.signatureCoche;



	
/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qpxxxPEx.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
		
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var paris = Value('id').of($ds101PE1.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntreprise0).eq(true)? true : false;
var autre = "Préfecture de " + $ds101PE1.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntreprise3;

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $ds101PE1.signatureGroup.signature.signatureDate,
		autoriteHabilitee : (paris == true ? 'Préfecture de Police de Paris' : autre),
		demandeContexte : "Demande d’agrément d’une société de domiciliation pour un entrepreneur individuel",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(attestationDoc.save('courrier.pdf'));

/*
 * Ajout du formulaire de déclaration puis de l'attestation d'honorabilité
 */
var cerfaDoc = nash.doc //
	.load('models/model.pdf') //
	.apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));

var attestationDoc = nash.doc //
	.load('models/Attestation.pdf') //
	.apply(formFields);
	
finalDoc.append(attestationDoc.save('cerfa.pdf'));

function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}



/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjLocaux);
appendPj($attachmentPreprocess.attachmentPreprocess.pjProprietaireLocaux);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Societe_Domiciliation.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Société de domiciliation - Demande d’agrément d’une société de domiciliation pour un entrepreneur individuel',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Société de domiciliation - Demande d’agrément d’une société de domiciliation pour un entrepreneur individuel',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});