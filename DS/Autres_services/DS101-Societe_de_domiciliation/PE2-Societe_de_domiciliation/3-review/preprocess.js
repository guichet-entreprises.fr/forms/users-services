var formFields = {};

var civNomPrenom = $ds101PE2.identificationDeclarantGroup.identificationDeclarant.civiliteDeclarant + " " + $ds101PE2.identificationDeclarantGroup.identificationDeclarant.nomNaissanceDeclarant + " " + $ds101PE2.identificationDeclarantGroup.identificationDeclarant.prenomDeclarant;

// Identification du déclarant

formFields['nomPrenomDeclarant']                         = $ds101PE2.identificationDeclarantGroup.identificationDeclarant.nomNaissanceDeclarant + " " + $ds101PE2.identificationDeclarantGroup.identificationDeclarant.prenomDeclarant;
formFields['adresseDomicileDeclarant']                   = ($ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileVoie != null ? $ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileVoie : '') + " " 
															+($ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileComp != null ? $ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileComp : '') + " " 
															+($ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileVille != null ? $ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileVille : '') + " " 
															+($ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileCP != null ? $ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileCP : '') + " " 
															+($ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicilePays != null ? $ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicilePays : '');
formFields['professionDeclarant']                        = $ds101PE2.identificationDeclarantGroup.identificationDeclarant.professionDeclarant;
formFields['qualiteExploitantIndividuel']                = false;
formFields['qualiteDirigeant']                           = Value('id').of($ds101PE2.identificationDeclarantGroup.identificationDeclarant.professionQualiteDeclarant).eq('qualiteDirigeant') ? true : false;
formFields['qualiteActionnaireAssocie']                  = Value('id').of($ds101PE2.identificationDeclarantGroup.identificationDeclarant.professionQualiteDeclarant).eq('qualiteActionnaireAssocie') ? true : false;

// Déclaration de la société

formFields['denominationSociale']                        = $ds101PE2.declarationSocieteGroup.declarationSociete.denominationSociale;
formFields['formeJuridique']                             = $ds101PE2.declarationSocieteGroup.declarationSociete.formeJuridique;
formFields['activitesEntreprise']                        = $ds101PE2.declarationSocieteGroup.declarationSociete.activitesEntreprise;
formFields['adresseEntreprise']                          = ($ds101PE2.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntrepriseVoie != null ? $ds101PE2.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntrepriseVoie : '') + ' ' 
															+ ($ds101PE2.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntrepriseComp != null ? $ds101PE2.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntrepriseComp : '') + ' '
															+ ($ds101PE2.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntrepriseVille != null ? $ds101PE2.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntrepriseVille : '')+' '
															+ ($ds101PE2.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntrepriseCP != null ? $ds101PE2.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntrepriseCP : '');


															
	for (var i = 0;i < $ds101PE2.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEtablissementSecondaireGroup.size(); i++) {

formFields['adresseEtablissementSecondaire' + i]         = ($ds101PE2.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEtablissementSecondaireGroup[i].adresseEtabSecVoie != null ? $ds101PE2.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEtablissementSecondaireGroup[i].adresseEtabSecVoie : '') + " " 
															+ ($ds101PE2.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEtablissementSecondaireGroup[i].adresseEtabSecComp != null ? $ds101PE2.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEtablissementSecondaireGroup[i].adresseEtabSecComp : '') + " " 
															+($ds101PE2.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEtablissementSecondaireGroup[i].adresseEtabSecVille != null ? $ds101PE2.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEtablissementSecondaireGroup[i].adresseEtabSecVille : '') + " " 
															+($ds101PE2.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEtablissementSecondaireGroup[i].adresseEtabSecCP != null ? $ds101PE2.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEtablissementSecondaireGroup[i].adresseEtabSecCP : '');

}

	for (var i = $ds101PE2.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEtablissementSecondaireGroup.size(); i < 4; i++) {
		
formFields['adresseEtablissementSecondaire' + i]         = '';
	
	}

// Identification des représentants légaux
	

if ($ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.declarantRL == true) {
	
	formFields['representantsLegauxNomPrenom0']              = $ds101PE2.identificationDeclarantGroup.identificationDeclarant.nomNaissanceDeclarant + " " 
															 + $ds101PE2.identificationDeclarantGroup.identificationDeclarant.prenomDeclarant;
	formFields['representantsLegauxDateNaissance0']          = $ds101PE2.identificationDeclarantGroup.identificationDeclarant.dateNaissanceDeclarant;
	formFields['representantsLegauxLieuNaissance0']          = $ds101PE2.identificationDeclarantGroup.identificationDeclarant.lieuNaissanceDeclarant + ", " 
															 + $ds101PE2.identificationDeclarantGroup.identificationDeclarant.paysNaissanceDeclarant;
	formFields['adresseDomicileRepresentantLegaux0']         =($ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileVoie != null ? $ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileVoie : '') + " " 
															 +($ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileComp != null ? $ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileComp : '') + " " 
															 +($ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileVille != null ? $ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileVille : '') + " " 
															 +($ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileCP != null ? $ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileCP : '') + " " 
															 +($ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicilePays != null ? $ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicilePays : '');
	formFields['professionRepresentantLegaux0']              = $ds101PE2.identificationDeclarantGroup.identificationDeclarant.professionDeclarant;

	for (var j = 1;j < $ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux.size()+1; j++) {

	formFields['representantsLegauxNomPrenom' + j]              = ($ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[j-1].nomRepresentLegal != null ? $ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[j-1].nomRepresentLegal : '') + " " 
																+ ($ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[j-1].prenomRepresentLegal != null ? $ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[j-1].prenomRepresentLegal : '');
	formFields['representantsLegauxDateNaissance'+ j]           = $ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[j-1].dateNaissanceRepresentLegal;
	formFields['representantsLegauxLieuNaissance' + j]          = ($ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[j-1].lieuNaissanceRepresentLegal != null ? $ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[j-1].lieuNaissanceRepresentLegal : '') + ", " 
																+ ($ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[j-1].paysNaissanceRepresentLegal != null ? $ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[j-1].paysNaissanceRepresentLegal : '');
	formFields['adresseDomicileRepresentantLegaux' + j]         = ($ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[j-1].adresseDomicileRepresentantLegal.adresseRepresentantLegalVoie != null ? $ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[j-1].adresseDomicileRepresentantLegal.adresseRepresentantLegalVoie :'') + ' ' 
																+ ($ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[j-1].adresseDomicileRepresentantLegal.adresseRepresentantLegalComp != null ? $ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[j-1].adresseDomicileRepresentantLegal.adresseRepresentantLegalComp :'') + ' ' 
																+($ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[j-1].adresseDomicileRepresentantLegal.adresseRepresentantLegalVille != null ? $ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[j-1].adresseDomicileRepresentantLegal.adresseRepresentantLegalVille :'') + ' ' 
																+ ($ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[j-1].adresseDomicileRepresentantLegal.adresseRepresentantLegalCP != null ? $ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[j-1].adresseDomicileRepresentantLegal.adresseRepresentantLegalCP :'') + ' ' 
																+ ($ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[j-1].adresseDomicileRepresentantLegal.adresseRepresentantLegalPays != null ? $ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[j-1].adresseDomicileRepresentantLegal.adresseRepresentantLegalPays :'');
	formFields['professionRepresentantLegaux' + j]              = $ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[j-1].professionRepresentantLegal;
	}
	
	for (var j = $ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux.size()+1 ; j < 10; j++) {
	
	formFields['representantsLegauxNomPrenom' + j]              = '';
	formFields['representantsLegauxDateNaissance'+ j]           = '';
	formFields['representantsLegauxLieuNaissance' + j]          = '';
	formFields['adresseDomicileRepresentantLegaux' + j]         = '';
	formFields['professionRepresentantLegaux' + j]              = '';
	
}
}

else {

	for (var i = 0;i < $ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux.size(); i++) {

	formFields['representantsLegauxNomPrenom' + i]              = ($ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[i].nomRepresentLegal != null ? $ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[i].nomRepresentLegal : '') + " " 
																+ ($ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[i].prenomRepresentLegal != null ? $ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[i].prenomRepresentLegal : '');
	formFields['representantsLegauxDateNaissance'+ i]           = $ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[i].dateNaissanceRepresentLegal;
	formFields['representantsLegauxLieuNaissance' + i]          = ($ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[i].lieuNaissanceRepresentLegal != null ? $ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[i].lieuNaissanceRepresentLegal : '') + ", " 
																+ ($ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[i].paysNaissanceRepresentLegal != null ? $ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[i].paysNaissanceRepresentLegal : '');
	formFields['adresseDomicileRepresentantLegaux' + i]         = ($ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[i].adresseDomicileRepresentantLegal.adresseRepresentantLegalVoie != null ? $ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[i].adresseDomicileRepresentantLegal.adresseRepresentantLegalVoie :'') + ' ' 
																+ ($ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[i].adresseDomicileRepresentantLegal.adresseRepresentantLegalComp != null ? $ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[i].adresseDomicileRepresentantLegal.adresseRepresentantLegalComp :'') + ' ' 
																+($ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[i].adresseDomicileRepresentantLegal.adresseRepresentantLegalVille != null ? $ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[i].adresseDomicileRepresentantLegal.adresseRepresentantLegalVille :'') + ' ' 
																+ ($ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[i].adresseDomicileRepresentantLegal.adresseRepresentantLegalCP != null ? $ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[i].adresseDomicileRepresentantLegal.adresseRepresentantLegalCP :'') + ' ' 
																+ ($ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[i].adresseDomicileRepresentantLegal.adresseRepresentantLegalPays != null ? $ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[i].adresseDomicileRepresentantLegal.adresseRepresentantLegalPays :'');
	formFields['professionRepresentantLegaux' + i]              = $ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux[i].professionRepresentantLegal;

}

	for (var j = $ds101PE2.identificationRepresentauxLegauxGroup.identificationDirigeantsLegaux.identificationRepresentauxLegaux.size() ; j < 10; j++) {
	
	formFields['representantsLegauxNomPrenom' + j]              = '';
	formFields['representantsLegauxDateNaissance'+ j]           = '';
	formFields['representantsLegauxLieuNaissance' + j]          = '';
	formFields['adresseDomicileRepresentantLegaux' + j]         = '';
	formFields['professionRepresentantLegaux' + j]              = '';
	
}

}

// Identification des dirigeants

if ($ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.declarantD == true) {
	
	formFields['dirigeantNomPrenom0']              = $ds101PE2.identificationDeclarantGroup.identificationDeclarant.nomNaissanceDeclarant + " " 
													+ $ds101PE2.identificationDeclarantGroup.identificationDeclarant.prenomDeclarant;
	formFields['dirigeantDateNaissance0']          = $ds101PE2.identificationDeclarantGroup.identificationDeclarant.dateNaissanceDeclarant;
	formFields['dirigeantLieuNaissance0']          = $ds101PE2.identificationDeclarantGroup.identificationDeclarant.lieuNaissanceDeclarant + ", " 
													+ $ds101PE2.identificationDeclarantGroup.identificationDeclarant.paysNaissanceDeclarant;
	formFields['adresseDomicileDirigeant0']        =($ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileVoie != null ? $ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileVoie : '') + " " 
													+($ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileComp != null ? $ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileComp : '') + " " 
													+($ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileVille != null ? $ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileVille : '') + " " 
													+($ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileCP != null ? $ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileCP : '') + " " 
													+($ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicilePays != null ? $ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicilePays : '');
	formFields['professionDirigeant0']             = $ds101PE2.identificationDeclarantGroup.identificationDeclarant.professionDeclarant;

	for (var j = 1;j < $ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants.size()+1; j++) {

	formFields['dirigeantNomPrenom' + j]              = ($ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[j-1].nomDirigeant != null ? $ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[j-1].nomDirigeant : '') + " " 
														+ ($ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[j-1].prenomDirigeant != null ? $ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[j-1].prenomDirigeant : '');
	formFields['dirigeantDateNaissance'+ j]           = $ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[j-1].dateNaissanceDirigeant;
	formFields['dirigeantLieuNaissance' + j]          = ($ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[j-1].lieuNaissanceDirigeant != null ? $ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[j-1].lieuNaissanceDirigeant : '') + ", " 
														+ ($ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[j-1].paysNaissanceDirigeant != null ? $ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[j-1].paysNaissanceDirigeant : '');
	formFields['adresseDomicileDirigeant' + j]         = ($ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[j-1].adresseDomicileDirigeant.adresseDirigeantVoie != null ? $ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[j-1].adresseDomicileDirigeant.adresseDirigeantVoie :'') + ' ' 
														+ ($ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[j-1].adresseDomicileDirigeant.adresseDirigeantComp != null ? $ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[j-1].adresseDomicileDirigeant.adresseDirigeantComp :'') + ' ' 
														+($ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[j-1].adresseDomicileDirigeant.adresseDirigeantVille != null ? $ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[j-1].adresseDomicileDirigeant.adresseDirigeantVille :'') + ' ' 
														+ ($ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[j-1].adresseDomicileDirigeant.adresseDirigeantCP != null ? $ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[j-1].adresseDomicileDirigeant.adresseDirigeantCP :'') + ' ' 
														+ ($ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[j-1].adresseDomicileDirigeant.adresseDirigeantPays != null ? $ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[j-1].adresseDomicileDirigeant.adresseDirigeantPays :'');
	formFields['professionDirigeant' + j]              = $ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[j-1].professionDirigeant;
	
	}
	
	for (var j = $ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants.size()+1 ; j < 10 ; j++) {
		
	formFields['dirigeantNomPrenom' + j]              = '';
	formFields['dirigeantDateNaissance'+ j]           = '';
	formFields['dirigeantLieuNaissance' + j]          = '';
	formFields['adresseDomicileDirigeant' + j]        = '';
	formFields['professionDirigeant' + j]             = '';
	
	}
}
else {
	for (var i = 0;i < $ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants.size(); i++) {

formFields['dirigeantNomPrenom' + i]                         = ($ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[i].nomDirigeant != null ? $ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[i].nomDirigeant : '') + " " 
																+ ($ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[i].prenomDirigeant != null ? $ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[i].prenomDirigeant : '');
formFields['dirigeantDateNaissance' + i]                     = $ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[i].dateNaissanceDirigeant;
formFields['dirigeantLieuNaissance' + i]                     = ($ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[i].lieuNaissanceDirigeant != null ? $ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[i].lieuNaissanceDirigeant : '') + ", " 
																+ ($ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[i].paysNaissanceDirigeant != null ? $ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[i].paysNaissanceDirigeant : '');
formFields['adresseDomicileDirigeant' + i]                   = ($ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[i].adresseDomicileDirigeant.adresseDirigeantVoie != null ? $ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[i].adresseDomicileDirigeant.adresseDirigeantVoie :'') + ' ' 
																+ ($ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[i].adresseDomicileDirigeant.adresseDirigeantComp != null ? $ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[i].adresseDomicileDirigeant.adresseDirigeantComp :'') + ' ' 
																+($ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[i].adresseDomicileDirigeant.adresseDirigeantVille != null ? $ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[i].adresseDomicileDirigeant.adresseDirigeantVille :'') + ' ' 
																+ ($ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[i].adresseDomicileDirigeant.adresseDirigeantCP != null ? $ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[i].adresseDomicileDirigeant.adresseDirigeantCP :'') + ' ' 
																+ ($ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[i].adresseDomicileDirigeant.adresseDirigeantPays != null ? $ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[i].adresseDomicileDirigeant.adresseDirigeantPays :'');
formFields['professionDirigeant' + i]                        = $ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants[i].professionDirigeant;

}

for (var j = $ds101PE2.identificationDirigeantsGroup.identificationAutresDirigeants.identificationDirigeants.size() ; j < 10 ; j++) {
	
	formFields['dirigeantNomPrenom' + j]              = '';
	formFields['dirigeantDateNaissance'+ j]           = '';
	formFields['dirigeantLieuNaissance' + j]          = '';
	formFields['adresseDomicileDirigeant' + j]        = '';
	formFields['professionDirigeant' + j]             = '';
	
	}
}

// Identification des actionnaires/associés

if ($ds101PE2.identificationAssociesGroup.identificationAssocie.declarantA == true) {
	
	formFields['associeNomPrenom0']              = $ds101PE2.identificationDeclarantGroup.identificationDeclarant.nomNaissanceDeclarant + " " 
													+ $ds101PE2.identificationDeclarantGroup.identificationDeclarant.prenomDeclarant;
	formFields['associeDateNaissance0']          = $ds101PE2.identificationDeclarantGroup.identificationDeclarant.dateNaissanceDeclarant;
	formFields['associeLieuNaissance0']          = $ds101PE2.identificationDeclarantGroup.identificationDeclarant.lieuNaissanceDeclarant + ", " 
													+ $ds101PE2.identificationDeclarantGroup.identificationDeclarant.paysNaissanceDeclarant;
	formFields['adresseDomicileAssocie0']         =($ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileVoie != null ? $ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileVoie : '') + " " 
													+($ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileComp != null ? $ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileComp : '') + " " 
													+($ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileVille != null ? $ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileVille : '') + " " 
													+($ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileCP != null ? $ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicileCP : '') + " " 
													+($ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicilePays != null ? $ds101PE2.identificationDeclarantGroup.identificationDeclarant.adresseDomicileDeclarant.adresseDomicilePays : '');
	formFields['professionAssocie0']              = $ds101PE2.identificationDeclarantGroup.identificationDeclarant.professionDeclarant;

	for (var j = 1;j < $ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies.size()+1; j++) {

	formFields['associeNomPrenom' + j]              = ($ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[j-1].nomAssocie != null ? $ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[j-1].nomAssocie : '') + " " 
														+ ($ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[j-1].prenomAssocie != null ? $ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[j-1].prenomAssocie : '');
	formFields['associeDateNaissance'+ j]           = $ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[j-1].dateNaissanceAssocie;
	formFields['associeLieuNaissance' + j]          = ($ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[j-1].lieuNaissanceAssocie != null ? $ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[j-1].lieuNaissanceAssocie : '') + ", " 
														+ ($ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[j-1].paysNaissanceAssocie != null ? $ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[j-1].paysNaissanceAssocie : '');
	formFields['adresseDomicileAssocie' + j]         = ($ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[j-1].adresseDomicileAssocie.adresseAssocieVoie != null ? $ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[j-1].adresseDomicileAssocie.adresseAssocieVoie :'') + ' ' 
														+ ($ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[j-1].adresseDomicileAssocie.adresseAssocieComp != null ? $ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[j-1].adresseDomicileAssocie.adresseAssocieComp :'') + ' ' 
														+($ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[j-1].adresseDomicileAssocie.adresseAssocieVille != null ? $ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[j-1].adresseDomicileAssocie.adresseAssocieVille :'') + ' ' 
														+ ($ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[j-1].adresseDomicileAssocie.adresseAssocieCP != null ? $ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[j-1].adresseDomicileAssocie.adresseAssocieCP :'') + ' ' 
														+ ($ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[j-1].adresseDomicileAssocie.adresseAssociePays != null ? $ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[j-1].adresseDomicileAssocie.adresseAssociePays :'');
	formFields['professionAssocie' + j]              = $ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[j-1].professionAssocie;
	
	}
	
	for (var j = $ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies.size()+1; j < 9 ; j++) {
	
	formFields['associeNomPrenom' + j]              = '';
	formFields['associeDateNaissance'+ j]           = '';
	formFields['associeLieuNaissance' + j]          = '';
	formFields['adresseDomicileAssocie' + j]        = '';
	formFields['professionAssocie' + j]             = '';
	
	}
}
else {
for (var i = 0;i < $ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies.size(); i++) {

formFields['associeNomPrenom' + i]                           = ($ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[i].nomAssocie != null ? $ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[i].nomAssocie : '') + " " 
																+ ($ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[i].prenomAssocie != null ? $ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[i].prenomAssocie : '');
formFields['associeDateNaissance' + i]                       = $ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[i].dateNaissanceAssocie;
formFields['associeLieuNaissance' + i]                       = ($ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[i].lieuNaissanceAssocie != null ? $ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[i].lieuNaissanceAssocie : '') + ", " 
																+ ($ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[i].paysNaissanceAssocie != null ? $ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[i].paysNaissanceAssocie : '');
formFields['adresseDomicileAssocie' + i]                     = ($ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[i].adresseDomicileAssocie.adresseAssocieVoie != null ? $ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[i].adresseDomicileAssocie.adresseAssocieVoie :'') + ' ' 
																+ ($ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[i].adresseDomicileAssocie.adresseAssocieComp != null ? $ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[i].adresseDomicileAssocie.adresseAssocieComp :'') + ' ' 
																+($ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[i].adresseDomicileAssocie.adresseAssocieVille != null ? $ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[i].adresseDomicileAssocie.adresseAssocieVille :'') + ' ' 
																+ ($ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[i].adresseDomicileAssocie.adresseAssocieCP != null ? $ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[i].adresseDomicileAssocie.adresseAssocieCP :'') + ' ' 
																+ ($ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[i].adresseDomicileAssocie.adresseAssociePays != null ? $ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[i].adresseDomicileAssocie.adresseAssociePays :'');
formFields['professionAssocie' + i]                          = $ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies[i].professionAssocie;

}

for (var j = $ds101PE2.identificationAssociesGroup.identificationAssocie.identificationAssocies.size(); j < 9 ; j++) {
	
	formFields['associeNomPrenom' + j]              = '';
	formFields['associeDateNaissance'+ j]           = '';
	formFields['associeLieuNaissance' + j]          = '';
	formFields['adresseDomicileAssocie' + j]        = '';
	formFields['professionAssocie' + j]             = '';
	
	}
}

// Signature 

formFields['signatureLieu'] = $ds101PE2.signatureGroup.signature.signatureLieu;
formFields['signatureDate'] = $ds101PE2.signatureGroup.signature.signatureDate;
formFields['signatureCoche'] = $ds101PE2.signatureGroup.signature.signatureCoche;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qpxxxPEx.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */


 
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var paris = Value('id').of($ds101PE2.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntreprise0).eq(true)? true : false;
var autre = "Préfecture de " + $ds101PE2.declarationSocieteGroup.declarationSociete.adresseEntreprise.adresseEntrepriseVille;
var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $ds101PE2.signatureGroup.signature.signatureDate,
		autoriteHabilitee : (paris == true ? 'Préfecture de Police de Paris' : autre),
		demandeContexte : "Demande d’agrément d’une société de domiciliation pour une personne morale",
		civiliteNomPrenom : civNomPrenom
	});


/*
 * Ajout du formulaire de déclaration puis de l'attestation d'honorabilité
 */
var cerfaDoc = nash.doc //
	.load('models/model_PM.pdf') //
	.apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));

var attestationDoc = nash.doc //
	.load('models/Attestation.pdf') //
	.apply(formFields);
	
finalDoc.append(attestationDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjLocaux);
appendPj($attachmentPreprocess.attachmentPreprocess.pjProprietaireLocaux);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Societe_Domiciliation.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Société de domiciliation - Demande d’agrément d’une société de domiciliation pour une personne morale',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Société de domiciliation - Demande d’agrément d’une société de domiciliation pour une personne morale',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});