//PJ obligatoires 
attachment('pjID', 'pjID');
attachment('pjCV', 'pjCV');
attachment('pjCasierJudiciaire', 'pjCasierJudiciaire');
attachment('pjDiplomes', 'pjDiplomes');
attachment('pjAttestations', 'pjAttestations');

// PJ soumises à conditions
var cheminFormeActivite = $ds099PE1.pratiqueExpertalePage.pratiqueExpertaleGroup.incriptionListeExpertJustice;

if (Value('id').of(cheminFormeActivite.formeExercice).eq(
		'formeExerciceActivite')) {
	attachment('pjSiretExpertale', 'pjSiretExpertale');
}

if (Value('id').of(cheminFormeActivite.formeExercice).eq('societeExercice')) {
	attachment('pjKbisExpertale', 'pjKbisExpertale');
}

if (Value('id').of(cheminFormeActivite.formeExercice).eq('salarie')) {
	attachment('pjSalarieExpertale', 'pjSalarieExpertale');
}

if (Value('id').of(cheminFormeActivite.formeExercice).eq('sousRespTiers')) {
	attachment('pjResponsabiliteTiers', 'pjResponsabiliteTiers');
}

var cheminPratiqueExpertale = $ds099PE1.pratiqueExpertalePage.pratiqueExpertaleGroup.incriptionListeExpertJustice;

if (cheminPratiqueExpertale.assuranceRespCivilProOUINON) {
	attachment('pjSouscriptionAssurnce', 'pjSouscriptionAssurnce');
}

var cheminActiviteAutreExpertale = $ds099PE1.activiteAutreExpertalePage.activiteAutreExpertaleGroup;

if (Value('id').of(cheminActiviteAutreExpertale.formeExerciceAutreGroup).eq(
		'formeExerciceAutre')) {
	attachment('pjSiretAUtreActivite', 'pjSiretAUtreActivite');
}

if (Value('id').of(cheminActiviteAutreExpertale.formeExerciceAutreGroup).eq(
		'autreSociete')) {
	attachment('pjKbisAutre', 'pjKbisAutre');
}

if (Value('id').of(cheminActiviteAutreExpertale.formeExerciceAutreGroup).eq(
		'autreSalarie')) {
	attachment('pjSalarieAutre', 'pjSalarieAutre');
}

if (cheminActiviteAutreExpertale.gerantActionnaireOUINON) {
	attachment('pjKbisActionnaire', 'pjKbisActionnaire');
}

var cheminactiviteTransactionImmobiliere = $ds099PE1.activiteTransImmoPage.activiteTransImmoGroup;

if (cheminactiviteTransactionImmobiliere.carteGestionImmibiliere) {
	attachment('pjCarteGestionImmo', 'pjCarteGestionImmo');
}

if (cheminactiviteTransactionImmobiliere.carteTransactionImmobiliere) {
	attachment('pjCarteTransImmo', 'pjCarteTransImmo');
}

// Stage
//
// var cheminStage = $ds099PE1.formationPage.formationGroup.expertiseGroup;
//
// if (cheminStage.stage) {
// attachment('pjStage', 'pjStage');
// }
