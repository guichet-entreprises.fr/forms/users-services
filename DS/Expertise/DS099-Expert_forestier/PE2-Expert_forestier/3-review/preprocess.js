var formFields = {};

/*******************************************************************************
 * SCP et SEL
 ******************************************************************************/
var cheminSCPetSEL = $ds099PE2.etatCivilGroup.scpSELGroup;

formFields['scpSEL']											 = cheminSCPetSEL.scpSEL ? true : false;

/*******************************************************************************
 * Etat civil
 ******************************************************************************/
var cheminEtatCivil   = $ds099PE2.etatCivilGroup.etatCivil;

formFields['civilite']                                           = cheminEtatCivil.civilite;
formFields['nomNaissanceDeclarant']                              = cheminEtatCivil.nomNaissanceDeclarant;
formFields['nomUsageDeclarant']                                  = cheminEtatCivil.nomUsageDeclarant;
formFields['prenomUsageDeclarant']                               = cheminEtatCivil.prenomUsageDeclarant;
formFields['dateNaissance']                                      = cheminEtatCivil.dateNaissance;
formFields['lieuDeNaissanceDeclarant']                           = cheminEtatCivil.lieuDeNaissanceDeclarant;
formFields['nationaliteDeclarant']                               = cheminEtatCivil.nationaliteDeclarant;


/*******************************************************************************
 * Coordonnées
 ******************************************************************************/

var cheminCoordonnees	= $ds099PE2.coordonneesPage.coordonneesGroup;

formFields['adressePro']                                         = cheminCoordonnees.adressePro.numNomVoiePro + " " + (cheminCoordonnees.adressePro.complementPro ? cheminCoordonnees.adressePro.complementPro : ' ,') ;
formFields['communePro']                                         = cheminCoordonnees.adressePro.communePro;
formFields['codePostalPro']                                      = cheminCoordonnees.adressePro.codePostalPro;
formFields['numeroTelephonePro']                                 = cheminCoordonnees.adressePro.numeroTelephonePro;
formFields['numeroFixPro']                                       = cheminCoordonnees.adressePro.numeroFixPro;
formFields['numeroFaxPro']                                       = cheminCoordonnees.adressePerso.numeroFax;
formFields['emailPro']                                  		 = cheminCoordonnees.adressePro.emailProDeclarant;

formFields['adressePerso']                                       = cheminCoordonnees.adressePerso.numNomVoiePerso + " " + (cheminCoordonnees.adressePerso.complementPerso ? cheminCoordonnees.adressePerso.complementPerso : ' ,') ;
formFields['communePerso']                                       = cheminCoordonnees.adressePerso.communePerso;
formFields['codePostalPerso']                                    = cheminCoordonnees.adressePerso.codePostalPerso;
formFields['numeroFixPerso']                                     = cheminCoordonnees.adressePerso.numeroFixPerso;
formFields['numeroTelephonePerso']                               = cheminCoordonnees.adressePerso.numeroTelephonePerso;
formFields['emailPerso']                                         = cheminCoordonnees.adressePerso.emailDeclarantPeso;


/*******************************************************************************
 * La taille du tableau à remplir pour les diplômes
 ******************************************************************************/

var cheminFormation		= $ds099PE2.formationPage.formationGroup;

var tailleDuTableauDiplomes = 3;

for(var i = 0 ; i<tailleDuTableauDiplomes; i++){
	formFields['dimplome'+i]                                     = '';
	formFields['etablissement'+i]                                = '';
	formFields['dateObtention'+i] 								 = '';
}

for (var i = 0 ; i<cheminFormation.diplomesGroup.size(); i++ ){
formFields['dimplome'+i]                                         = cheminFormation.diplomesGroup[i].dimplome;
formFields['etablissement'+i]                                    = cheminFormation.diplomesGroup[i].etablissement;
formFields['dateObtention'+i]                                    = cheminFormation.diplomesGroup[i].dateObtention;
}


/*******************************************************************************
 * La taille du tableau à remplir pour les formation
 ******************************************************************************/

var tailleTableauFormation = 4;

for(var i = 0; i< tailleTableauFormation ; i++){
	formFields['dateFormation'+i]                                 = '';
	formFields['lieuDeFormation'+i]                               = '';
	formFields['organismeDeFormation'+i]                          = '';
	formFields['sujetFormation'+i]                  			  = '';
}


for(var i = 0; i< cheminFormation.fomationGroup.size() ; i++){	
	formFields['dateFormation'+i]                                 = cheminFormation.fomationGroup[i].dateFormation;
	formFields['lieuDeFormation'+i]                               = cheminFormation.fomationGroup[i].lieuDeFormation;
	formFields['organismeDeFormation'+i]                          = cheminFormation.fomationGroup[i].organismeDeFormation;
	formFields['sujetFormation'+i]                                = cheminFormation.fomationGroup[i].sujetFormation;
}

formFields['stage']												 = cheminFormation.expertiseGroup.stage ? true :false;


/*******************************************************************************
 *  expertise tiers
 ******************************************************************************/
var cheminPratiqueExpertale		= $ds099PE2.pratiqueExpertalePage.pratiqueExpertaleGroup;

formFields['datePremiereExpertiseTiers']                         = cheminPratiqueExpertale.expertiseTiers.datePremiereExpertiseTiers;
formFields['nombreExpertiseTiers']                             	 = cheminPratiqueExpertale.expertiseTiers.nombreExpertiseTiers;

formFields['nombreExpertiseAmiableTiers']                        = cheminPratiqueExpertale.expertiseTiers.missionAmiableTiers.nombreExpertiseAmiableTiers;
formFields['dateExpertiseAmiableTiers']                          = cheminPratiqueExpertale.expertiseTiers.missionAmiableTiers.dateExpertiseAmiableTiers == null ? cheminPratiqueExpertale.expertiseTiers.missionAmiableTiers.dateExpertiseAmiableTiers : ' ';
formFields['nombreExpertiseAssuranceTiers']                      = cheminPratiqueExpertale.expertiseTiers.missionAssuranceTiers.nombreExpertiseAssuranceTiers;
formFields['dateExpertiseAssuranceTiers']                        = cheminPratiqueExpertale.expertiseTiers.missionAssuranceTiers.dateExpertiseAssuranceTiers == null ? cheminPratiqueExpertale.expertiseTiers.missionAssuranceTiers.dateExpertiseAssuranceTiers : ' ' ;
formFields['nombreExpertiseJudiciaireTiers']                     = cheminPratiqueExpertale.expertiseTiers.missionJudiciaireTiers.nombreExpertiseJudiciaireTiers;
formFields['dateExpertiseJudiciairetiers']                       = cheminPratiqueExpertale.expertiseTiers.missionJudiciaireTiers.dateExpertiseJudiciairetiers == null ? cheminPratiqueExpertale.expertiseTiers.missionJudiciaireTiers.dateExpertiseJudiciairetiers : ' ';


/*******************************************************************************
 * Expertise personnelle
 ******************************************************************************/

//var datesAmiables = '';
//for (var index = 0; index < cheminPratiqueExpertale.expertisesPersonelle.missionAmiablePerso.dateExpertiseAmiablePerso.size(); index++) {
//	var currentDateAmiable = cheminPratiqueExpertale.expertisesPersonelle.missionAmiablePerso.dateExpertiseAmiablePerso[index];
//}

formFields['datePremiereExpertisePerso']                         = cheminPratiqueExpertale.expertisesPersonelle.datePremiereExpertisePerso;
formFields['nombreExpertisePerso']                               = cheminPratiqueExpertale.expertisesPersonelle.nombreExpertisePerso;
formFields['nombreExpertiseAmiablePerso']                        = cheminPratiqueExpertale.expertisesPersonelle.missionAmiablePerso.nombreExpertiseAmiablePerso;
formFields['dateExpertiseAmiablePerso']                          = cheminPratiqueExpertale.expertisesPersonelle.missionAmiablePerso.dateExpertiseAmiablePerso == null ? cheminPratiqueExpertale.expertisesPersonelle.missionAmiablePerso.dateExpertiseAmiablePerso : ' ';
formFields['nombreExpertiseAssurancePerso']                      = cheminPratiqueExpertale.expertisesPersonelle.missionAssurancePerso.nombreExpertiseAssurancePerso;
formFields['dateExpertiseAssurancePerso']                        = cheminPratiqueExpertale.expertisesPersonelle.missionAssurancePerso.dateExpertiseAssurancePerso == null ? cheminPratiqueExpertale.expertisesPersonelle.missionAssurancePerso.dateExpertiseAssurancePerso :' ';
formFields['nombreExpertiseJudiciairePerso']                     = cheminPratiqueExpertale.expertisesPersonelle.missionJudiciairePerso.nombreExpertiseJudiciairePerso;
formFields['dateExpertiseJudiciairePerso']                       = cheminPratiqueExpertale.expertisesPersonelle.missionJudiciairePerso.dateExpertiseJudiciairePerso == null ? cheminPratiqueExpertale.expertisesPersonelle.missionJudiciairePerso.dateExpertiseJudiciairePerso : ' ';


/*******************************************************************************
 * Inscription sur une liste d'experts de justice
 ******************************************************************************/

formFields['inscrListeExpertJusticeOUI']                         = cheminPratiqueExpertale.incriptionListeExpertJustice.incritListeJusticeOUINON ? true : false;
formFields['inscriExpertJusticeNON']                             = cheminPratiqueExpertale.incriptionListeExpertJustice.incritListeJusticeOUINON ? false : true;

var tailleTableauJuridiction = 3;

for(var i = 0; i < tailleTableauJuridiction; i++){	
	formFields['juridiction'+i]                                  = '';
	formFields['dateInscri'+i]                                   = '';
	formFields['nomoclatureListeAppel'+i]                        = '';
}

for(var i = 0; i < cheminPratiqueExpertale.incriptionListeExpertJustice.incriptionListeExpertJusticeGroup.size(); i++){	
	
	formFields['juridiction'+i]                                  = cheminPratiqueExpertale.incriptionListeExpertJustice.incriptionListeExpertJusticeGroup[i].juridiction;
	formFields['dateInscri'+i]                                   = cheminPratiqueExpertale.incriptionListeExpertJustice.incriptionListeExpertJusticeGroup[i].dateInscri;
	formFields['nomoclatureListeAppel'+i]                        = cheminPratiqueExpertale.incriptionListeExpertJustice.incriptionListeExpertJusticeGroup[i].nomoclatureListeAppel;
}

formFields['formeExerciceActivite']                              = Value('id').of(cheminPratiqueExpertale.incriptionListeExpertJustice.formeExercice).eq('formeExerciceActivite') ? true : false;
formFields['societeExercice']                                    = Value('id').of(cheminPratiqueExpertale.incriptionListeExpertJustice.formeExercice).eq('societeExercice') ? true : false;
formFields['salarie']                                            = Value('id').of(cheminPratiqueExpertale.incriptionListeExpertJustice.formeExercice).eq('salarie') ? true : false;
formFields['sousRespTiers']                                      = Value('id').of(cheminPratiqueExpertale.incriptionListeExpertJustice.formeExercice).eq('sousRespTiers') ? true : false;

formFields['formeExerciceAvenir']                                = Value('id').of(cheminPratiqueExpertale.incriptionListeExpertJustice.formExcercie).eq('formeExerciceAvenir') ? true : false;
formFields['societeAvenir']                                      = Value('id').of(cheminPratiqueExpertale.incriptionListeExpertJustice.formExcercie).eq('societeAvenir') ? true : false;

formFields['assuranceRespCivileOK']                              = cheminPratiqueExpertale.incriptionListeExpertJustice.assuranceRespCivilProOUINON ? true : false;
formFields['assuranceRespCivileKO']                              = cheminPratiqueExpertale.incriptionListeExpertJustice.assuranceRespCivilProOUINON ? false : true;


/*******************************************************************************
 * Activité autre que l'activité expertale
 ******************************************************************************/

var cheminActiviteAutreExpertale = $ds099PE2.activiteAutreExpertalePage.activiteAutreExpertaleGroup;

formFields['autreActiviteProOK']                                 = cheminActiviteAutreExpertale.autreActiviteProOUINON ? true : false;
formFields['autreActiviteProKO']                                 = cheminActiviteAutreExpertale.autreActiviteProOUINON ? false : true;

formFields['autreActiviteHorsExpertise']                         = cheminActiviteAutreExpertale.autreActiviteProGroup.autreActiviteHorsExpertise;
formFields['tempsConsacreExpertiseMensuelle']                    = cheminActiviteAutreExpertale.autreActiviteProGroup.tempsConsacreExpertiseMensuelle;
formFields['tempsConsacreExpertiseAnnuelles']                    = cheminActiviteAutreExpertale.autreActiviteProGroup.tempsConsacreExpertiseAnnuelles;
formFields['formeExerciceAutre']                                 = Value('id').of(cheminActiviteAutreExpertale.formeExerciceAutre).eq('formeExerciceAutre') ? true : false;
formFields['autreSociete']                                       = Value('id').of(cheminActiviteAutreExpertale.formeExerciceAutre).eq('autreSociete') ? true : false;
formFields['autreSalarie']                                       = Value('id').of(cheminActiviteAutreExpertale.formeExerciceAutre).eq('autreSalarie') ? true : false;
formFields['continuerExercerAutreActiviteParallelleExpertiseOK'] = cheminActiviteAutreExpertale.continuerExercerActParallelleOUINON ? true : false;
formFields['continuerExercerAutreActiviteParallelleExpertiseKO'] = cheminActiviteAutreExpertale.continuerExercerActParallelleOUINON ? false : true; 
formFields['gerantActionnaireAutreEntrepriseOK']                 = cheminActiviteAutreExpertale.gerantActionnaireOUINON ? true : false;
formFields['nomEntrepriseSiOUI1']                                = cheminActiviteAutreExpertale.nomEntrepriseSiOUI ;
formFields['fonctionElectiveCadrePro']                           = cheminActiviteAutreExpertale.fonctionElectiveCadrePro;


/*******************************************************************************
 * Activités de gestion et de transaction immobilière
 ******************************************************************************/

var cheminactiviteTransactionImmobiliere = $ds099PE2.activiteTransImmoPage.activiteTransImmoGroup;

formFields['carteGestionImmibiliereOK']                          = cheminactiviteTransactionImmobiliere.carteGestionImmibiliere ? true : false;
formFields['carteGestionImmibiliereKO']                          = cheminactiviteTransactionImmobiliere.carteGestionImmibiliere ? false : true;
formFields['carteTransactionImmobiliereOK']                      = cheminactiviteTransactionImmobiliere.carteTransactionImmobiliere ? true : false;
formFields['carteTransactionImmobiliereKO']                      = cheminactiviteTransactionImmobiliere.carteTransactionImmobiliere ? false : true;
formFields['pratiquerGestionImmoOK']                             = cheminactiviteTransactionImmobiliere.pratiquerGestionImmo ? true : false;
formFields['pratiquerGestionImmoKO']                             = cheminactiviteTransactionImmobiliere.pratiquerGestionImmo ? false : true;
formFields['pratiquerTrnsactionImmoOK']                          = cheminactiviteTransactionImmobiliere.pratiquerTrnsactionImmo ? true : false;
formFields['pratiquerTransactionImmoKO']                         = cheminactiviteTransactionImmobiliere.pratiquerTrnsactionImmo ? false : true;


/*******************************************************************************
 * Siagnatre du contrat
 ******************************************************************************/

var cheminSignature = $ds099PE2.signatureGroup.signature;

formFields['certifiSurHonneur']                                  = cheminSignature.certifiSurHonneur;
formFields['lieuSignature']                                      = cheminSignature.lieuSignature;
formFields['dateSignature']                                      = cheminSignature.dateSignature;


var cerfaDoc = nash.doc //
.load('models/model.pdf') //
.apply (formFields);



/*******************************************************************************
 * Pieces jointes
 ******************************************************************************/

function appendPj(fld) {
    fld.forEach(function (elm) {
    	cerfaDoc.append(elm);
    });
}

appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCV);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCasierJudiciaire);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestations);

if (Value('id').of(cheminFormeActivite.formeExercice).eq('formeExerciceActivite')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjSiretExpertale);
}

if (Value('id').of(cheminFormeActivite.formeExercice).eq('societeExercice')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjKbisExpertale);
}

if (Value('id').of(cheminFormeActivite.formeExercice).eq('salarie')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjSalarieExpertale);
}

if (Value('id').of(cheminFormeActivite.formeExercice).eq('sousRespTiers')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjResponsabiliteTiers);
}

var cheminPratiqueExpertale = $ds099PE2.pratiqueExpertalePage.pratiqueExpertaleGroup.incriptionListeExpertJustice;

if (cheminPratiqueExpertale.assuranceRespCivilProOUINON) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjSouscriptionAssurnce);
}

var cheminActiviteAutreExpertale = $ds099PE2.activiteAutreExpertalePage.activiteAutreExpertaleGroup;

if (Value('id').of(cheminActiviteAutreExpertale.formeExerciceAutreGroup).eq('formeExerciceAutre')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjSiretAUtreActivite);
}

if (Value('id').of(cheminActiviteAutreExpertale.formeExerciceAutreGroup).eq('autreSociete')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjKbisAutre);
}

if (Value('id').of(cheminActiviteAutreExpertale.formeExerciceAutreGroup).eq('autreSalarie')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjSalarieAutre);
}

if (cheminActiviteAutreExpertale.gerantActionnaireOUINON) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjKbisActionnaire);
}

var cheminactiviteTransactionImmobiliere = $ds099PE2.activiteTransImmoPage.activiteTransImmoGroup;

if (cheminactiviteTransactionImmobiliere.carteGestionImmibiliere) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjCarteGestionImmo);
}

if (cheminactiviteTransactionImmobiliere.carteTransactionImmobiliere) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjCarteTransImmo);
}

//PJ pour les SCP et les SEL

if (Value('id').of(cheminSCPetSEL.scpSEL).eq('scp')
		| Value('id').of(cheminSCPetSEL.scpSEL).eq('sel')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.statuts);
	appendPj($attachmentPreprocess.attachmentPreprocess.attestationGreffier);
	appendPj($attachmentPreprocess.attachmentPreprocess.repartitionCapital);
}

//PJ que pour les SEL

if (Value('id').of(cheminSCPetSEL.scpSEL).eq('sel')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.diplomeAssocie);
	appendPj($attachmentPreprocess.attachmentPreprocess.pratiqueProAssocie);
	appendPj($attachmentPreprocess.attachmentPreprocess.pjCasierJudiciaire);
	appendPj($attachmentPreprocess.attachmentPreprocess.declarationHonneur);
	appendPj($attachmentPreprocess.attachmentPreprocess.declarationActivite);
}

/*******************************************************************************
 * Chargement du courrier d'accompagnement à destination de l'AC
 ******************************************************************************/

var civiliteNomPrenom = $ds099PE2.etatCivilGroup.etatCivil.civilite +" "+ $ds099PE2.etatCivilGroup.etatCivil.nomNaissanceDeclarant +" "+ $ds099PE2.etatCivilGroup.etatCivil.prenomUsageDeclarant +" ";

var finalDoc = nash.doc //
.load('models/Courrier au premier dossier v2.1 GE.pdf') //
.apply({
	dateSignature : $ds099PE2.signatureGroup.signature.dateSignature ,
	autoriteHabilitee :"Conseil national de l’expertise foncière, agricole et forestière (CNEFAF)" ,
	demandeContexte : "inscription sur la liste nationale des experts forestiers - Personne physique.",
	civiliteNomPrenom : civiliteNomPrenom
});

finalDoc.append(cerfaDoc.save('cerfa.pdf'));	


/*******************************************************************************
 *  Enregistrement du fichier (en mémoire)
 ******************************************************************************/

var finalDocItem = finalDoc.save('Expert_Forestier.pdf');	

/*******************************************************************************
 *  Persistance des données obtenues
 ******************************************************************************/

return spec.create({
id : 'review',
label : 'Expert forestier - inscription sur la liste nationale des experts forestiers - Personne physique',
groups : [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : [ spec.createData({
        id : 'formulaire',
        label : "Demande d'inscription sur la liste nationale des experts forestiers en tant que personne physique.",
        description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
        type : 'FileReadOnly',
        value : [ finalDocItem ]
    }) ]
}) ]
});
