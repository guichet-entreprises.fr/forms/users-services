var formFields = {};


/*******************************************************************************
 * Etat civil
 ******************************************************************************/
var cheminEtatCivil = $ds099PE3.declarationGroup.declaration;


formFields['nomPro']             = cheminEtatCivil.nomPro;
formFields['activite']           = cheminEtatCivil.activite;
formFields['telephone']          = cheminEtatCivil.telephone;
formFields['courriel']           = cheminEtatCivil.courriel;

/*******************************************************************************
 * Adresse
 ******************************************************************************/
formFields['adresseSiege']       = cheminEtatCivil.adresseVoie +' '+ 
										(cheminEtatCivil.adresseVoieComplement ? cheminEtatCivil.adresseVoieComplement : ' ') +', '+
										 cheminEtatCivil.adresseCP +' '+ cheminEtatCivil.adresseVille +' '+ 
										 cheminEtatCivil.adressePays;

if (cheminEtatCivil.adressedifferenteQuestion){
	formFields['adresseExercice']    = cheminEtatCivil.adressedifferenteGroup.adressedifferenteVoie +' '+ 
											(cheminEtatCivil.adressedifferenteGroup.adressedifferenteComplement ? cheminEtatCivil.adressedifferenteGroup.adressedifferenteComplement : ' ') +' '+ 
											 cheminEtatCivil.adressedifferenteGroup.adressedifferenteCP +' '+ 
											 cheminEtatCivil.adressedifferenteGroup.adressedifferenteVille +' '+ 
											 cheminEtatCivil.adressedifferenteGroup.adressedifferentePays;
}else{
	formFields['adresseExercice']	 = ' ';
}


formFields['objet']              = "déclaration préalable en vue d'une libre prestation de services";


/*******************************************************************************
 * Signature
 ******************************************************************************/
var cheminSignature = $ds099PE3.signatureGroup.signature;
var signatureNomPrenom = $ds099PE3.signatureGroup.signature.signatureNom +'  '+ $ds099PE3.signatureGroup.signature.signaturePrenom;

formFields['declarationHonneur'] = cheminSignature.declarationHonneur;
formFields['signature']          = cheminSignature.signature;
formFields['dateSignature']      = cheminSignature.dateSignature;
formFields['lieuSignature']      = cheminSignature.lieuSignature;



formFields['signature']			 = $ds099PE3.signatureGroup.signature.signatureNom +'  '+ $ds099PE3.signatureGroup.signature.signaturePrenom;

var cerfaDoc1 = nash.doc //
.load('models/model.pdf') //
.apply (formFields);



/**********************
Pieces jointes
***********************/
function appendPj(fld) {
fld.forEach(function (elm) {
    cerfaDoc1.append(elm);
});
}	

appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationEtablissement);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveQualifications);
appendPj($attachmentPreprocess.attachmentPreprocess.policeAssurance); 



/*******************************************************************************
 * Chargement du courrier d'accompagnement à destination de l'AC
 ******************************************************************************/

var finalDoc = nash.doc //
.load('models/Courrier au premier dossier v2.1 GE.pdf') //
.apply({
	dateSignature: $ds099PE3.signatureGroup.signature.dateSignature ,
	autoriteHabilitee :"Conseil national de l’expertise foncière agricole et forestière (CNEFAF)" ,
	demandeContexte : "Libre prestation de services.",
	civiliteNomPrenom : signatureNomPrenom
});
finalDoc.append(cerfaDoc1.save('cerfa.pdf'));	



/*******************************************************************************
 * Enregistrement du fichier (en mémoire)
 ******************************************************************************/
var finalDocItem = finalDoc.save('Expert_forestier.pdf');	


/*******************************************************************************
 * Persistance des données obtenues
 ******************************************************************************/

return spec.create({
id : 'review',
label : 'Expert forestier - déclaration préalable en vue d\'une libre prestation de services',
groups : [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : [ spec.createData({
        id : 'formulaire',
        label : "Déclaration préalable en vue d'une libre prestation de service.",
        description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
        type : 'FileReadOnly',
        value : [ finalDocItem ]
    }) ]
}) ]
});