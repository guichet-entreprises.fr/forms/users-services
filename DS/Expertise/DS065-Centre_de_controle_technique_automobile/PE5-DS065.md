**Procédure élémentaire 4 :** Centre de contrôle technique automobile (DS065)

**Contexte :** agrément préfectoral centre de contrôle technique de véhicules
lourds

-   Exploitant d’un centre de contrôle technique de véhicules lourds qui
    souhaite obtenir un agrément des installations de ce centre de contrôle

**Formulaire :** déclaration libre

**Reference\_id :** Formalités SCN/DS/Expertise/centre de contrôle technique automobile/Agrément préfectoral centre de contrôle technique/véhicules lourds
**Mode de transmission :** transmission 2 exemplaires et 3 dans le cas d’un
centre non rattaché à un réseau

**Destinataire :** préfectures de département du lieu d’implantation du centre
de contrôle auquel il est rattaché

**Délais :**

En l’absence de réponse au terme d’un délai de 4 mois, le silence de la
préfecture vaut décision d’acceptation

**Pièces justificatives :**

Pour le centre de contrôle rattaché à un réseau, les pièces sont les suivantes :

-   la demande d'agrément sur papier ;

-   une attestation du réseau de contrôle certifiant que le centre a fait
    l'objet d'une étude favorable ;

-   le cahier des charges ;

-   une description de l'organisation et des moyens matériels mis en place ;

-   l'engagement du demandeur d'établir tous les documents permettant de
    justifier de la surveillance du bon fonctionnement des centres de contrôle ;

-   la copie de la notification d’agrément du réseau pour le contrôle technique
    des véhicules lourds ;

-   le justificatif indiquant que l’installation fait partie du périmètre
    d’accréditation du réseau ou un récépissé délivré par l’organisme
    accréditeur indiquant que la demande d’intégration de l’installation dans le
    périmètre d’accréditation du réseau a été déposée ;

-   l'engagement du demandeur à respecter le cahier des charges.

Pour un centre de contrôle non rattaché à un réseau, les pièces sont les
suivantes :

-   la demande d'agrément sur papier ;

-   une pièce d'identité en cours de validité s'il s'agit d'une personne
    physique et un extrait KBis de moins de trois mois si c'est une personne
    morale ;

-   une copie du certificat d'accréditation de la personne physique ou morale
    exploitante ou un récépissé délivré par l'organisme accréditeur tel que
    prévu à l'article 22 du présent arrêté attestant que le centre a déposé, en
    vue de son accréditation, son système qualité complet et conforme à la norme
    NF EN ISO/CEI 17020 : 2012 ;

-   un cahier des charges ;

-   une description de l'organisation et des moyens matériels mis en place ;

-   l'engagement du demandeur d'établir tous les documents permettant de
    justifier de la surveillance du bon fonctionnement des centres de contrôle ;

-   les références techniques permettant d’apprécier l’expérience du demandeur
    dans le domaine du contrôle technique ;

-   l'avis de l’organisme technique central ;

-   l'attestation de conformité de l'outil informatique délivrée par l'OTC ;

-   l'engagement du demandeur à respecter le cahier des charges.

**Commentaires :**

La décision préfectorale d'agrément est notifiée simultanément au demandeur et
pour les centres non rattachés, à l'organisme technique central.
