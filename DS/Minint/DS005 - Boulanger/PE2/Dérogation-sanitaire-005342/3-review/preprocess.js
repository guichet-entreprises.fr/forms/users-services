//--------------- VARIABLES -------------------//

/* init datas pour review (variables génériques)
 *
 */
 
var data = [] ;
var pjUser = [];
var metas = [];
var repCerfa = 'models/';
var iCardinality = 0 ;

/* init datas pour cerfa (variables spécifiques)
 * contient a minnima les références des groupes 
 * génériques de data.xml
 */

/*
 * init pointeurs saisies, Pj...
 */
 
var datasPtr = $ds005cerfa13982 ;
var datasSignature = datasPtr.cadreSignatureGroupe ;
var datasIdentite = datasPtr.cadreIdentiteGroupe ;
var refFormTitle = 'Boulangerie-pâtisserie : Dérogation sanitaire' ;

if (typeof datasPtr.cadreDemandeGroupe !== 'undefined') {
    var datasDemande = datasPtr.cadreDemandeGroupe ;
} else {
	var datasDemande = null ;
}

/*
 * init pointeurs PJ déclarants ou mandataires
 */
 
var pjSignataire = datasSignature.cadreSignature.soussigne ;
var idDeclarant = 'signataireQualiteDeclarant' ;
var idMandataire = 'signataireQualiteMandataire' ;

/* import description du CERFA (si il existe)
 * il génère les tables de paramétrage formFields et equivCerfa
 * formFields = dictionnaire des valeurs des champs du Cerfa
 * equivCerfa = dictionnaire de description des champs du Cerfa
 * il définit les variables repCerfa et nameCerfa
 * nameCerfa = nom du fichier pdf associé au Cerfa
 */

/*
 * Remplissage des champs du Cerfa 13982 (dérogation agrément sanitaire)
 */

if (datasDemande !== null) {
	if ((Value('id').of(datasDemande.cadreDemande.nature).eq('depotDossier')) &&  (Value('id').of(datasDemande.cadreDemande.objet).eq(''))) {
		var flgCerfa = true;
	} else {
		var flgCerfa = false;
	}
} else {
	var flgCerfa = true;
}

if (flgCerfa === true) {

	//constantes et variables
	var formFields = {} ;
	var nameCerfa = 'Cerfa_13982.pdf' ;
	//pointeurs pages de saisie
	var datasInfosDeclaration = datasPtr.infosDeclaration ; //page 1
	var datasInfosEtablissement = datasPtr.infosEtablissement ; //page 2
	var datasInfosActivites = datasPtr.infosActivite ; //page 3

	//paramétrages des cardinalités
	var setCommerces = "[ "
    	+ "{ src: root.nomEtablissement, dst: 'etablissement[i]', valeur: null, type: 'string' },"
		+ "{ src: null, dst: 'adresse[i]', valeur: mergeAdresse(root.cadreAdresse, 'T', false), type: 'string' },"
		+ "{ src: root.distanceEtablissement, dst: 'distance[i]', valeur: null, type: 'string' },"
		+ "{ src: root.categoriesProduits, dst: 'categorie[i]', valeur: null, type: 'string' }"
		+ " ] ;" ;
				
	var setProduits = "[ "
		+ "{ src: root.cadreViandeFraiche.quantite, dst: 'quantitéViandeFraiche', valeur: null, type: 'string' },"
		+ "{ src: root.cadreProduitsBaseViande.quantite, dst: 'quantiteProduitsBaseViande', valeur: null, type: 'string' },"
		+ "{ src: root.cadreLaitsTraites.quantite, dst: 'quantitéLaitsTraites', valeur: null, type: 'string' },"
		+ "{ src: root.cadreProduitsLaitiers.quantite, dst: 'quantiteproduitslaitiers', valeur: null, type: 'string' },"
		+ "{ src: root.cadrePreparationBaseOeuf.quantite, dst: 'quantitePreparationbaseOeuf', valeur: null, type: 'string' },"
		+ "{ src: root.cadreProduitsNonTransformes.quantite, dst: 'quantiteProduitsNonTransformes', valeur: null, type: 'string' },"
		+ "{ src: root.cadreProduitsTransformes.quantite, dst: 'quantiteProduitsTransformes', valeur: null, type: 'string' },"
		+ "{ src: root.cadreEscargots.quantite, dst: 'quantiteEscargots', valeur: null, type: 'string' },"
		+ "{ src: root.cadreRepasPreparation.quantite, dst: 'quantiteRepasPreparation', valeur: null, type: 'string' }"
		+ " ] ;" ;

	var setProduitsHebdo = "[ "					 
		+ "{ src: root.cadreViandeFraiche.quantiteHebdo, dst: 'quantiteHebdoViandeFraiche', valeur: null, type: 'string' },"
		+ "{ src: root.cadreProduitsBaseViande.quantiteHebdo, dst: 'quantiteHebdoProduitsBaseViande', valeur: null, type: 'string' },"
		+ "{ src: root.cadreLaitsTraites.quantiteHebdo, dst: 'quantiteHebdoLaitsTraites', valeur: null, type: 'string' },"
		+ "{ src: root.cadreProduitsLaitiers.quantiteHebdo, dst: 'quantiteHebdoProduitsLaitiers', valeur: null, type: 'string' },"
		+ "{ src: root.cadrePreparationBaseOeuf.quantiteHebdo, dst: 'quantiteHebdoPreparationbaseOeuf', valeur: null, type: 'string' },"
		+ "{ src: root.cadreProduitsNonTransformes.quantiteHebdo, dst: 'quantiteHebdoProduitsNonTransformes', valeur: null, type: 'string' },"
		+ "{ src: root.cadreProduitsTransformes.quantiteHebdo, dst: 'quantiteHebdoProduitsTransformes', valeur: null, type: 'string' },"
		+ "{ src: root.cadreEscargots.quantiteHebdo, dst: 'quantiteHebdoEscargots', valeur: null, type: 'string' },"
		+ "{ src: root.cadreRepasPreparation.quantiteHebdo, dst: 'quantiteHebdoRepasPreparation', valeur: null, type: 'string' }"
		+ " ] ;" ;
	
	var setProduitsRapports = "[ "					 
		+ "{ src: null, dst: 'rapport1', valeur: calculPercent(root.cadreViandeFraiche.quantiteHebdo,root.cadreViandeFraiche.quantite,0), type: 'string' },"
		+ "{ src: null, dst: 'rapport2', valeur: calculPercent(root.cadreProduitsBaseViande.quantiteHebdo,root.cadreProduitsBaseViande.quantite,0), type: 'string' },"
		+ "{ src: null, dst: 'rapport3', valeur: calculPercent(root.cadreLaitsTraites.quantiteHebdo,root.cadreLaitsTraites.quantite,0), type: 'string' },"
		+ "{ src: null, dst: 'rapport4', valeur: calculPercent(root.cadreProduitsLaitiers.quantiteHebdo,root.cadreProduitsLaitiers.quantite,0), type: 'string' },"
		+ "{ src: null, dst: 'rapport5', valeur: calculPercent(root.cadrePreparationBaseOeuf.quantiteHebdo,root.cadrePreparationBaseOeuf.quantite,0), type: 'string' },"
		+ "{ src: null, dst: 'rapport6', valeur: calculPercent(root.cadreProduitsNonTransformes.quantiteHebdo,root.cadreProduitsNonTransformes.quantite,0), type: 'string' },"
		+ "{ src: null, dst: 'rapport7', valeur: calculPercent(root.cadreProduitsTransformes.quantiteHebdo,root.cadreProduitsTransformes.quantite,0), type: 'string' },"
		+ "{ src: null, dst: 'rapport8', valeur: calculPercent(root.cadreEscargots.quantiteHebdo,root.cadreEscargots.quantite,0), type: 'string' },"
		+ "{ src: null, dst: 'rapport9', valeur: calculPercent(root.cadreRepasPreparation.quantiteHebdo,root.cadreRepasPreparation.quantite,0), type: 'string' }"
		+ " ] ;" ;
						 
	//description des listes liées aux produits dans le Cerfa (type complex)
	var listProduits = [ setProduits, setProduitsHebdo, setProduitsRapports ] ;

	//description des zones génériques du Cerfa (type bool, type string...)
	var equivCerfa = [ 
		//page 1
		{ src: datasInfosDeclaration.typeDeclaration, dst: 'premiereDeclaration', valeur: 'typePremiere', type: 'radio' },
		{ src: datasInfosDeclaration.typeDeclaration, dst: 'actualisation', valeur: 'typeActualisation', type: 'radio' },
		//page 2
		{ src: null, dst: 'numeroSiret', valeur: supprimeEspaces(datasInfosEtablissement.siret), type: 'string' },
		{ src: datasInfosEtablissement.nomEtablissement, dst: 'raisonSociale', valeur: null, type: 'string' },
		{ src: null, dst: 'numeroNomVoie', valeur: mergeAdresse(datasInfosEtablissement.cadreAdresse, 'V', false), type: 'string' },
		{ src: datasInfosEtablissement.cadreAdresse.codePostal, dst: 'codePostal', valeur: null, type: 'string' },
		{ src: null, dst: 'commune', valeur: extractInfoAdresse(datasInfosEtablissement.cadreAdresse, 'C', false), type: 'string' },
		{ src: datasIdentite.cadreIdentite.nomNaissance, dst: 'nomExploitant', valeur: null, type: 'string' },
		{ src: null, dst: 'prenomExploitant', valeur: mergePrenoms(datasIdentite.cadreIdentite.prenoms), type: 'string' },
		{ src: null, dst: 'telephoneFixeExploitant', valeur: formatNumTelephone(datasIdentite.cadreContact.telephoneFixe.telephoneNum), type: 'string' },	
		{ src: null, dst: 'telephoneMobileExploitant', valeur: formatNumTelephone(datasIdentite.cadreContact.telephoneMobile.telephoneNum), type: 'string' },	
		{ src: datasIdentite.cadreContact.courriel, dst: 'courrielExploitant', valeur: null, type: 'string' },
		{ src: datasInfosEtablissement.exploitantDifferent, dst: 'nomExploitant', valeur: datasInfosEtablissement.infosExploitant.cadreIdentite.nomNaissance, type: 'bString' },
		{ src: datasInfosEtablissement.exploitantDifferent, dst: 'prenomExploitant', valeur: mergePrenoms(datasInfosEtablissement.infosExploitant.cadreIdentite.prenoms), type: 'bString' },
		{ src: datasInfosEtablissement.exploitantDifferent, dst: 'telephoneFixe', valeur: formatNumTelephone(datasInfosEtablissement.infosExploitant.cadreContact.telephoneFixe.telephoneNum), type: 'bString' },	
		{ src: datasInfosEtablissement.exploitantDifferent, dst: 'telephoneMobile', valeur: formatNumTelephone(datasInfosEtablissement.infosExploitant.cadreContact.telephoneMobile.telephoneNum), type: 'bString' },	
		{ src: datasInfosEtablissement.exploitantDifferent, dst: 'courriel', valeur: datasInfosEtablissement.infosExploitant.cadreContact.courriel, type: 'bString' },
		//page 3
		{ src: datasInfosActivites.cadreProduits, dst: 'datasInfosActivites.cadreProduits', valeur: listProduits, type: 'complex' },
		{ src: datasInfosActivites.cadreCommerces, dst: 'datasInfosActivites.cadreCommerces', valeur: setCommerces, type: 'cardinality' },
		//bloc signature
		{ src: null, dst: 'signatureNom', valeur: genereInfoSignataire(false, true), type: 'string' },	
		{ src: null, dst: 'signatureJour', valeur: formatDate(datasSignature.cadreSignature.signatureDate, 'J', true), type: 'string' },
		{ src: null, dst: 'signatureMois', valeur: formatDate(datasSignature.cadreSignature.signatureDate, 'M', true), type: 'string' },
		{ src: null, dst: 'signatureAnnee', valeur: formatDate(datasSignature.cadreSignature.signatureDate, 'A', true), type: 'string' },
		{ src: null, dst: 'signature', valeur: genereInfoSignataire(true, true), type: 'string' }		
	] ; 	
}
			
			

//--------------- VARIABLES -------------------//

//--------------- FONCTIONS -------------------//

/* fonctions génériques de formatage des données
 *
 */

/*
 * calculs mathématiques : calcul de pourcentage
 */
 
function calculPercent(valNumerateur, valDenominateur, nbDecimales) {
	var valResult = '' ;
	var tmpValeur = 0 ;
	while (true) {
		if (typeof valNumerateur === 'undefined' || typeof valNumerateur !== 'number') {
			break ;
		}
		if (typeof valDenominateur === 'undefined' || typeof valDenominateur !== 'number') {
			break;
		}
		if (typeof nbDecimales === 'undefined' || typeof nbDecimales !== 'number') {
			tmpValeur = 0 ;
		} else {
			tmpValeur = nbDecimales >= 0 ? nbDecimales : 0 ; 
		}	
		tmpValeur = Math.pow(10, tmpValeur);
		if (valDenominateur !== 0) {
			tmpValeur = Math.round(((valNumerateur / valDenominateur) * 100) / tmpValeur) ;
			valResult = tmpValeur.toString();
			
		}
		break ;
	}
	return valResult ;
 }

/*
 * calculs mathématiques : calcul de soustraction
 */
 
function calculSubstraction(valTotal, tabValeurs, cumulValeur, keyCumul) {
	var valResult = '' ;
	var tmpValeur = 0 ;
	while (true) {
		if (typeof valTotal === 'undefined' || typeof valTotal !== 'number') {
			break ;
		}
		tmpValeur = valTotal ;
		if (typeof tabValeurs === 'object' && Array.isArray(tabValeurs)) {
			for (tmpIndex = 0; tmpIndex < tabValeurs.length; tmpIndex++) {
				if (typeof tabValeurs[tmpIndex] === 'number') {
					tmpValeur -= tabValeurs[tmpIndex];
					tmpFlag = true ;
				}
			}	
		}
		if (tmpFlag === true) {
			valResult = tmpValeur.toString();
			if (typeof cumulValeur === 'object' && cumulValeur !== null && typeof keyCumul === 'string') {
				if (typeof cumulValeur[keyCumul] !== 'undefined' && isNaN(cumulValeur[keyCumul]) === false) {
					cumulValeur[keyCumul] = (Number(cumulValeur[keyCumul]) - tmpValeur).toString() ;
				} else {
					cumulValeur[keyCumul] = valResult ;
				}
			}
		}
		break;
	}
	return valResult ;
 }

function calculAddition(tabValeurs, cumulValeur, keyCumul) {
	var valResult = '' ;
	var tmpFlag = false ;
	var tmpValeur = 0 ;
	var tmpIndex = 0 ;
	while (true) {
		if (typeof tabValeurs === 'object' && Array.isArray(tabValeurs)) {
			for (tmpIndex = 0; tmpIndex < tabValeurs.length; tmpIndex++) {
				if (typeof tabValeurs[tmpIndex] === 'number') {
					tmpValeur += tabValeurs[tmpIndex];
					tmpFlag = true ;
				}
			}	
		}
		if (tmpFlag === true) {
			valResult = tmpValeur.toString();
			if (typeof cumulValeur === 'object' && cumulValeur !== null && typeof keyCumul === 'string') {
				if (typeof cumulValeur[keyCumul] !== 'undefined' && isNaN(cumulValeur[keyCumul]) === false) {
					cumulValeur[keyCumul] = (tmpValeur + Number(cumulValeur[keyCumul])).toString() ;
				} else {
					cumulValeur[keyCumul] = valResult ;
				}
			}
		}
		break ;
	}
	return valResult ;
 }

/* suppression des espaces dans une chaine
 *
 */

function supprimeEspaces(szLibel) {
	var tmpRegex = /\s/g ;
	var tmpLibel = '';
	if (typeof szLibel !== 'undefined' && typeof szLibel === 'string') {
		tmpLibel = szLibel !== null ? szLibel.replace(tmpRegex, '') : '' ;	
	}
	return tmpLibel ;
}
 
/* merge des prénoms (séparateur ',')
 *
 */
	
function mergePrenoms(listPrenoms) {
	var prenoms = [];
	if (typeof listPrenoms !== 'undefined' && typeof listPrenoms === 'object') {
		for (i = 0; i < listPrenoms.length; i++) {
			prenoms.push(listPrenoms[i]);
    	}
	} else {
		prenoms.push('');
	}
	return prenoms.toString();
}

/* merge du nom et des prénoms (séparateur ',')
 *
 */

function mergeNomPrenoms(szNom, listPrenoms) {
	var result = '';
	if (typeof szNom === 'string') {
		result = szNom + ' ';
		result += mergePrenoms(listPrenoms);
	} 
	return result;
}

/* construction de la date et du lieu de naissance
 *
 */

function mergeNaissance(blocNaissance) {
	var naissance = '' ;
	if (typeof blocNaissance !== 'undefined' && typeof blocNaissance === 'object') {
		naissance = blocNaissance.lieuNaissance ? blocNaissance.lieuNaissance + ' / ' : '' ;
		naissance += blocNaissance.paysNaissance ? blocNaissance.paysNaissance : '' ;
	}
	return naissance ;
}
	
/* construction de l'adresse en tenant compte du pays ou non
 *
 */

function mergeAdresse(blocAdresse, typeMerge, multiLine) {
	var tmpVar = '' ;
	var adresse = '' ;
	var chgLigne = ' \n' ;
	if (typeof blocAdresse !== 'undefined' && typeof blocAdresse === 'object') {
		var fmtLigne = (multiLine === true ? chgLigne : ' ');
		var typeAct = (typeof typeMerge !== 'undefined' && typeof typeMerge === 'string') ? typeMerge.toUpperCase() : 'T' ;
		switch (typeAct) {
			case 'N':
				adresse += blocAdresse.numeroVoie ? blocAdresse.numeroVoie + ' ' : '' ;
				adresse += blocAdresse.indiceVoie ? blocAdresse.indiceVoie : '' ;
				break;
			case 'V':	
				tmpVar = mergeAdresse(blocAdresse, 'N', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + ' ' : '' ;
				adresse += mergeAdresse(blocAdresse, 'W', multiLine) ;
				break;
			case 'W':
				adresse += blocAdresse.typeVoie ? blocAdresse.typeVoie + ' ' : '' ;
				adresse += blocAdresse.nomVoie ?  blocAdresse.nomVoie : '' ;
				break;
			case 'S':
				adresse += blocAdresse.complementAdresse ? blocAdresse.complementAdresse + fmtLigne : '' ;
				adresse += blocAdresse.distributionSpeciale ? blocAdresse.distributionSpeciale : '' ;
				break;
			case 'C':
				if (typeof blocAdresse.pays !== 'undefined') {
					if (typeof blocAdresse.codePostal !== 'undefined' && blocAdresse.codePostal) {
						adresse += blocAdresse.codePostal ? blocAdresse.codePostal + ' ' : '' ;
						adresse += blocAdresse.commune ? blocAdresse.commune : '' ;
					} else {
						adresse += blocAdresse.ville ? blocAdresse.ville + fmtLigne : '' ;
						adresse += blocAdresse.pays ? blocAdresse.pays : '' ;
					}
				} else {
					adresse += blocAdresse.codePostal ? blocAdresse.codePostal + ' ' : '' ;
					adresse += blocAdresse.commune ? blocAdresse.commune : '' ;
				}
				break;
			case 'X':
				tmpVar = mergeAdresse(blocAdresse, 'S', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + fmtLigne : '' ;
				adresse += mergeAdresse(blocAdresse, 'C', multiLine) ;
				break;
			case 'Z':
				tmpVar = mergeAdresse(blocAdresse, 'V', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + fmtLigne : '' ;
				adresse += mergeAdresse(blocAdresse, 'S', multiLine) ;
				break;
			case 'T':
			default:
				tmpVar = mergeAdresse(blocAdresse, 'V', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + fmtLigne : '' ;
				tmpVar = mergeAdresse(blocAdresse, 'S', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + fmtLigne : '' ;
				adresse += mergeAdresse(blocAdresse, 'C', multiLine) ;
		}
	}
	return adresse;
}

/*
 * mise en forme numéro de téléphone au format 10 chiffres
 */

function formatNumTelephone(objNumTel) {
	var tmpRegexTelIntro = /^(\+\d{1,3}[\s\.\-]?)(.*)$/ ;
	var tmpRegexTel = /(\d{1,})[\s\.\-]?/g ;
	var tmpTel = '' ;
	var tmpIntro = '';
	var tmpTelFmt = '';
	if (typeof objNumTel !== 'undefined' && typeof objNumTel === 'object' && objNumTel) {
		tmpTel = objNumTel.international ;
		if (tmpRegexTelIntro.test(tmpTel) === true) {
			tmpIntro = tmpTel.replace(tmpRegexTelIntro, '$1') ;
			tmpTelFmt = '$2' ;
			if (tmpIntro.substring(0,3) == '+33') {
				tmpTelFmt = '0' + tmpTelFmt ;
			}	
			tmpTel = tmpTel.replace(tmpRegexTelIntro, tmpTelFmt) ;
			tmpTel = tmpTel.replace(tmpRegexTel, '$1') ;
		}
	}
	return tmpTel ;
}

/*
 * infos contacts téléphoniques
 */
 
function mergeNumTelephone(blocNumTel, flgMerge, flgInternational) {
	var numTel = '';
	var tmpTel = '';
	if (typeof blocNumTel !== 'undefined' && typeof blocNumTel === 'object') {
		if (typeof blocNumTel.telephoneFixe.telephoneNum !== 'undefined' && blocNumTel.telephoneFixe.telephoneNum ) {
			if (flgInternational === false) {
				tmpTel = formatNumTelephone(blocNumTel.telephoneFixe.telephoneNum) ;
			} else {
				tmpTel = blocNumTel.telephoneFixe.telephoneNum.international ;
			}
			numTel += tmpTel ;
		}
		if (typeof blocNumTel.telephoneMobile.telephoneNum !== 'undefined' && blocNumTel.telephoneMobile.telephoneNum) {
			if (numTel.length > 0 && flgMerge === true) {
				numTel += ' / ';
			}
			if (flgMerge === true || numTel.length === 0) {
				if (flgInternational === false) {
					tmpTel = formatNumTelephone(blocNumTel.telephoneMobile.telephoneNum) ;
				} else {
					tmpTel = blocNumTel.telephoneMobile.telephoneNum.international;
				}
				numTel += tmpTel ;
			}
		}
	}
	return numTel;
}

/* formatage d'une date avec extaction ou non d'une partie (J, M, A)
 *
 */

function formatDate(valDate, typeExtract, fullYear) {
	var dateResult = '';
	var tmpYear = '' ;
	if (typeof valDate === 'object' && valDate) {
    	var dateTmp = new Date(parseInt(valDate.getTimeInMillis())) ; ;
		var dateIso = ((dateTmp.toLocaleDateString()).substr(0, 10)).split('-') ;
		var typeAct = (typeof typeExtract !== 'undefined' && typeof typeExtract === 'string') ? typeExtract.toUpperCase() : 'T' ;
		if (dateIso.length > 1) {
			tmpYear = (fullYear === true ? dateIso[0] : dateIso[0].substr(2)) ;
			switch (typeAct) {
				case 'D':
				case 'J':
					dateResult = dateIso[2] ;
					break;
				case 'M':
					dateResult = dateIso[1] ;
					break;
				case 'A':
				case 'Y':
					dateResult = tmpYear ;
					break;
				case 'T':
				default:
					dateResult = dateIso[2] + '/' + dateIso[1] + '/' + tmpYear ;
			}
		} else {
			dateResult = dateIso[0] ;
		}
	}
	return dateResult ;
}

/*
 * infos signataire
 */

function genereInfoSignataire(flgCompleteInfos, flgMergePrenoms) {
	var contactInfos = [] ;
	var infoSignataire = '' ;
	var chgLigne = ' \n' ;
	if (typeof datasSignature !== 'undefined' && typeof datasIdentite !== 'undefined' && typeof idMandataire !== 'undefined') {
		if (flgCompleteInfos === true) {
			infoSignataire = 'Cette déclaration respecte les attendus de l\’article 1367 du code civil.' + chgLigne ;
			infoSignataire += datasSignature.cadreSignature.soussigne + ' ';
		}
		if (Value('id').of(datasSignature.cadreSignature.soussigne).eq(idMandataire)) {
			infoSignataire += datasSignature.cadreSignature.informationsMandataire.nomPrenomDenomination ;
			if (flgCompleteInfos === true) {
				infoSignataire += chgLigne ;
				infoSignataire += mergeAdresse(datasSignature.cadreSignature.informationsMandataire.cadreAdresse, 'T', true) ;
			}
		} else {
			infoSignataire += (datasIdentite.cadreIdentite.nomUsage ? datasIdentite.cadreIdentite.nomUsage + ' ' : datasIdentite.cadreIdentite.nomNaissance) + ' ' ;
			if (flgMergePrenoms === true) {
				infoSignataire += mergePrenoms(datasIdentite.cadreIdentite.prenoms) ;
			}
			if (flgCompleteInfos === true) {
				infoSignataire += chgLigne ;
				if (typeof datasIdentite.cadreContact.telephoneFixe.telephoneNum !== 'undefined' && datasIdentite.cadreContact.telephoneFixe.telephoneNum) {
					contactInfos.push(datasIdentite.cadreContact.telephoneFixe.telephoneNum) ;
				}
				if (typeof datasIdentite.cadreContact.telephoneMobile.telephoneNum !== 'undefined' && datasIdentite.cadreContact.telephoneMobile.telephoneNum) {
					contactInfos.push(datasIdentite.cadreContact.telephoneMobile.telephoneNum);
				}
				if (typeof datasIdentite.cadreContact.courriel !== 'undefined' && datasIdentite.cadreContact.courriel) {
					contactInfos.push(datasIdentite.cadreContact.courriel);
				}
				infoSignataire += contactInfos.toString() ;
			}
		}
	}
	return infoSignataire ;
}

/*
 * extraction d'informations liées à l'identité (nom, prénoms...)
 */

function extractInfoIdentite(blocIdentite, typeExtract, flgExtract) {
	var infoIdentite = '';
	if (typeof blocIdentite !== 'undefined' && typeof blocIdentite === 'object') {
		var typeExt = (typeExtract !== 'undefined' && typeof typeExtract === 'string') ? typeExtract.toUpperCase() : 'T' ;
		switch (typeExt) {
			case 'N':
			case 'L':
				infoIdentite = blocIdentite.nomUsage ? blocIdentite.nomUsage : (blocIdentite.nomNaissance ? blocIdentite.nomNaissance : '') ;
				break;
			case 'P':
			case 'F':
				infoIdentite = flgExtract ? mergePrenoms(blocIdentite.prenoms) : (blocIdentite.prenoms.length > 0 ? blocIdentite.prenoms[0] : '') ;
				break ;
			case 'C':
				infoIdentite = blocIdentite.civilite ? blocIdentite.civilite : '' ;
				break
			case 'X':
				infoIdentite += extractInfoIdentite(blocIdentite, 'N', flgExtract) ;
				infoIdentite += ' ' ;
				infoIdentite += extractInfoIdentite(blocIdentite, 'P', flgExtract) ;
				break;
			case 'T':
				infoIdentite  = extractInfoIdentite(blocIdentite, 'C', flgExtract) ;
				infoIdentite += ' ' ;
				infoIdentite += extractInfoIdentite(blocIdentite, 'N', flgExtract) ;
				infoIdentite += ' ' ;
				infoIdentite += extractInfoIdentite(blocIdentite, 'P', flgExtract) ;
				break;
			default:
				infoIdentite = '';
		}
	}
	return infoIdentite ;
}

/*
 * extraction d'informations liées aux adresses
 */
 
function extractInfoAdresse(blocAdresse, typeExtract, flgExtract) {
	var tmpDept = /(0[1-9]|[1-8][0-9]|2[A-B]|9[0-5]|9[7-8][0-9])(\d{2,3})/ ;
	var adresse = '' ;
	if (typeof blocAdresse !== 'undefined' && typeof blocAdresse === 'object') {
		var typeAct = (typeof typeExtract !== 'undefined' && typeof typeExtract === 'string') ? typeExtract.toUpperCase() : 'T' ;
		switch (typeAct) {
			case 'D':
				adresse = blocAdresse.commune ? blocAdresse.commune.id : '' ;
				adresse = adresse.replace(tmpDept, '$1') ;
				break;
			case 'P':
				if (typeof blocAdresse.ville !== 'undefined' && blocAdresse.ville) {
					adresse = blocAdresse.pays ;
				} else {
					if (flgExtract === true) {
						adresse = blocAdresse.pays ;
					}
				}
				break;
			case 'C':
				if (typeof blocAdresse.ville !== 'undefined' && blocAdresse.ville) {
					adresse = blocAdresse.ville ;
				} else {
					adresse = blocAdresse.commune ;
				}
				break;
			default:
		}
	}
	return adresse;
}

/*
 * extraction indicatif téléphone si indicatif non français (+33)
 */

function extractIndicatifEtrangerTelephone(objNumTel) {
	var tmpRegexTelIntro = /^(\+\d{1,3}[\s\.\-]?)(.*)$/ ;
	var tmpTel = '' ;
	var tmpIntro = '';
	if (typeof objNumTel !== 'undefined' && typeof objNumTel === 'object' && objNumTel) {
		if (tmpRegexTelIntro.test(objNumTel.international) === true) {
			tmpIntro = objNumTel.international.replace(tmpRegexTelIntro, '$1').trim() ;
			if (tmpIntro !== '+33') {
				tmpTel = tmpIntro ;
			}
		}
	}
	return tmpTel ;
}

/* merge de PJs
 * 
 */

function merge(pjs) {
	var mergedFile = nash.doc.create();
	pjs.forEach(function (elm) {		
		mergedFile.append(elm).save('item.pdf');
    });
	return mergedFile;
}

/* ajout de PJ
 * 
 */

function appendPj(fld, listPj) {
	fld.forEach(function (elm) {
        listPj.push(elm);
		_log.info('elm.getAbsolutePath() = >'+elm.getAbsolutePath());
		metas.push({'name':'userAttachments', 'value': '/'+elm.getAbsolutePath()});
		metas.push({'name':'document', 'value': '/'+elm.getAbsolutePath()});
    });
}

/* remplissage d'un cerfa à partir d'une table de paramétrage
 *
*/

function genereDynamicParams(tabParam, baseAddress) {
	var tmpTab = tabParam.replace(/root/gi, baseAddress);
	var tmpObj = [] ;
	try {
		tmpObj = eval(tmpTab) ;
	} catch(error) {
		tmpObj = [];
	}
  	return tmpObj;
}

function genereXDynamicParams(tabParam, baseAddress) {
	var tmpTab = '';
	var tmpVal ;
	var tmpFlg = false ;
	var tmpInd = 0;
	var tmpKey = {} ;
	var tmpObj = [] ;
	var tmpRegG = /root/gi ; // pour remplacer root. par le préfixe réel
	var tmpReg = [ /src\:[\s]*([^;\{\}]*)[;]*.*$/i, // pour isoler la valeur associée à src
				   /dst\:[\s]*([^;\{\}]*)[;]*.*$/i, // pour isoler la valeur associée à dst
				   /valeur\:[\s]*([^;\{\}]*)[;]*.*$/i, // pour isoler la valeur associée à value
				   /type\:[\s]*([^;\{\}]*)[;]*.*$/i ] ; // pour isoler la valeur associée à type
	var tmpLib = [ 'src', 'dst', 'valeur', 'type' ] ; // table des propriétés src, dst, value, type
	tabParam.forEach(function (elm) {
		try {
			tmpTab = elm.replace(tmpRegG, baseAddress);
			tmpKey = {} ;
			for (tmpInd = 0; tmpInd < tmpLib.length; tmpInd++) {
				tmpVal = tmpReg[tmpInd].exec(tmpTab) ;
				if (tmpVal !== null && tmpVal.length > 0) {
					tmpKey[tmpLib[tmpInd]] = eval(tmpVal[1].trim());
					tmpFlg = true ;
				} else {
					tmpFlg = false ;
					break;
				}
			}
			if (tmpFlg == true) {
				tmpObj.push(tmpKey) ;
			}
		} catch(error) {
			tmpFlg = false ;
		}
		return ;
    });
  	return tmpObj;
}

function completeCerfa(paramFields, valFields, indFields) {
	var tmpDst = '';
	var tmpResult = true ;
	var tmpRegexIndice = /\[i\]/gi ; // forme 'champ-cerfa[i] par exemple'
	var tmpRegexIndex = /\[x\]/gi ; // forme 'champ-cerfa[x] par exemple'
	var tmpBase = '' ;
	var tmpType = '' ;
	var tmpValue = '' ;
	var tmpFlag = false ;
	var objParam = [] ;
	
	if (typeof paramFields !== 'object' || !(Array.isArray(paramFields)) || typeof formFields !== 'object' || typeof indFields !== 'number') {
		return false;
	}
	paramFields.forEach(function (item) {
		if (typeof item.src === 'undefined') {
			return ;
		}
		tmpType = (typeof item.type !== undefined && typeof item.type === 'string') ? (item.type).toUpperCase() : '' ;
		if (tmpType === 'CARDINALITY' || tmpType === 'XCARDINALITY') {
			if (typeof item.src !== 'object' || item.src === null) {
				return ;
			}
			for (iCardinality = 0; iCardinality < (item.src).length; iCardinality++) {
				tmpBase = item.dst + '[' + iCardinality.toString() + ']' ;
				switch (tmpType) {
					case 'CARDINALITY':
						objParam = genereDynamicParams(item.valeur, tmpBase);
						break;
					case 'XCARDINALITY':
						objParam = genereXDynamicParams(item.valeur, tmpBase);
						break;
					default:
						objParam = [];
						break;
				}
				if (completeCerfa(objParam, valFields, iCardinality) == false) {
					tmpResult = false ;
				}
			}
			return ;
		}
		if (tmpType === 'COMPLEX' || tmpType === 'XCOMPLEX') {
			if (typeof item.src !== 'object') {
				return ;
			}
			(item.valeur).forEach(function (xtem) {
				switch (tmpType) {
					case 'COMPLEX':
						objParam = genereDynamicParams(xtem, item.dst);
						break;
					case 'XCOMPLEX':
						objParam = genereXDynamicParams(xtem, item.dst);
						break;
					default:
						objParam = [];
						break;
				}
				if (completeCerfa(objParam, valFields, 0) == false) {
					tmpResult = false ;
				}
				return ;
			});
			return ;
		}
		tmpDst = (item.dst).replace(tmpRegexIndice, (indFields + 1).toString()) ;
		tmpDst = tmpDst.replace(tmpRegexIndex, indFields.toString()) ;
		switch (tmpType) {
			case 'BOOL':
				var tmpFlg = typeof item.valeur === 'boolean' ? item.valeur : false ;
				if (item.src === null) {
					valFields[tmpDst] = tmpFlg ;
				} else {
					valFields[tmpDst] = item.src === tmpFlg ? true : false;
				}
				break;
			case 'XBOOL':
				if ((typeof item.src === 'boolean') && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					if (item.valeur[0] === item.valeur[1]) {
						if (item.src === false) {
							valFields[tmpDst] = null;
						}
					} else {
						valFields[tmpDst] = item.src;
					}
				}
				break;
			case 'RADIO':
				if (item.src) {
					valFields[tmpDst] = Value('id').of(item.src).eq(item.valeur) ? true : false;
				}
				break;
			case 'BRADIO':
				if ((typeof item.src === 'boolean' && item.src) && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					valFields[tmpDst] = Value('id').of(item.valeur[0]).eq(item.valeur[1]) ? true : false;
				}
				break;
			case 'XRADIO':
				if ((typeof item.src === 'boolean') && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					if (Value('id').of(item.valeur[0]).eq(item.valeur[1])) {
						if (item.src === false) {
							valFields[tmpDst] = null;
						}
					} else {
						valFields[tmpDst] = item.src;
					}
				}
				break;
			case 'CHECK':
				if (item.src) {
					valFields[tmpDst] = Value('id').of(item.src).contains(item.valeur) ? true : false;
				}
				break;
			case 'BCHECK':
				if ((typeof item.src === 'boolean' && item.src) && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					valFields[tmpDst] = Value('id').of(item.valeur[0]).contains(item.valeur[1]) ? true : false;
				}
				break;
			case 'XCHECK':
				if ((typeof item.src === 'boolean') && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					if (Value('id').of(item.valeur[0]).contains(item.valeur[1])) {
						if (item.src === false) {
							valFields[tmpDst] = null;
						}
					} else {
						valFields[tmpDst] = item.src;
					}
				}
				break;
			case 'STRING':
				if (item.valeur) {
					tmpValue = item.valeur + ' ' ;
					tmpFlag = true ;
				} else {
					tmpValue = '' ;
					tmpFlag = false ;
				}
				if (item.src) {
					tmpValue += item.src ;
					tmpFlag = true ;
				}
				if (tmpFlag) {
					valFields[tmpDst] = tmpValue ;
				}
				break;
			case 'BSTRING':
				if ((typeof item.src === 'boolean' && item.src) && (typeof item.valeur !== 'undefined' && item.valeur)) {
					valFields[tmpDst] = item.valeur ;
				}
				break;
			case 'XSTRING':
				if (((typeof item.src === 'string' or typeof item.src === 'object') && item.src) && (typeof item.valeur !== 'undefined' && item.valeur)) {
					valFields[tmpDst] = item.valeur ;
				}
				break;
			default:
				break;
		}
		return ;
	});
	return tmpResult ;
}

//--------------- FONCTIONS -------------------//

//--------------- MAIN GENERATION CERFA -------//

/* génération du Cerfa à partir de la table d'équivalences
 * (génération optionnelle)
 */

if (typeof formFields !== 'undefined' && typeof nameCerfa !== 'undefined') {
	var flgCerfa = true ;
	if (typeof equivCerfa !== 'undefined') { 
		flgCerfa = completeCerfa(equivCerfa, formFields, 0);
	}
	if (flgCerfa === true) {
		var cerfaDoc = nash.doc
			.load(repCerfa + nameCerfa)
			.apply(formFields);
		var pjCerfa = cerfaDoc.save(nameCerfa);
	}
}

/* ajout des PJ utilisateurs le cas échéant
 *
 */
 
if (typeof pjSignataire !== 'undefined') {
	if(Value('id').of(pjSignataire).contains(idDeclarant)) {
		appendPj($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantSignataire, pjUser);
	}
	if(Value('id').of(pjSignataire).contains(idMandataire)) {
		appendPj($attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire, pjUser);
	}
}

/* ajout des autres PJ le cas échéant
 *
 */

if($attachmentPreprocess.attachmentPreprocess.pjAutres !== 'undefined') {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjAutres, pjUser);
}

/* ajout du Cerfa en tant que PJ (s'il a été généré)
 *
 */
 
if (typeof pjCerfa !== 'undefined') {
	data.push(spec.createData({
			id: 'record',
			label: refFormTitle,
			description: 'Voici le formulaire obtenu à partir des données saisies.',
			type: 'FileReadOnly',
			value: [pjCerfa] 
	})) ;
}

/*
 * Enregistrement du fichier (en mémoire)
 */ 

var metasToDelete = [];
var recordMetas = nash.record.meta() != null ? nash.record.meta().metas : null;
var recordMetasSize = recordMetas != null ? recordMetas.size() : 0;

for (var i = 0; i < recordMetasSize; i++) {
    if (recordMetas.get(i).name == 'document') {
        metasToDelete.push({'name':'document', 'value': recordMetas.get(i).value});
    }
}
nash.record.removeMeta(metasToDelete);
nash.record.meta(metas);

data.push(spec.createData({
    id : 'uploaded',
    label : 'Pièces jointes',
    description : 'Pièces jointes ajoutées au formulaire.',
    type : 'FileReadOnly',
    value : [ merge(pjUser).save('saisine.pdf') ]
	})) ;

var groups = [ spec.createGroup({
    id : 'attachments',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Saisine du Ministère de l\'intérieur',
    groups : groups
});

//--------------- MAIN GENERATION CERFA -------//
