/* init datas pour pj (variables spécifiques)
 * contient a minnima les références des groupes 
 * génériques de data.xml
 */

/*
 * init pointeurs saisies, Pj...
 */
 
var datasPtr = $ds032cerfa13824 ;
var datasSignature = datasPtr.cadreSignatureGroupe ;
var datasIdentite = datasPtr.cadreIdentiteGroupe ;
var refFormTitle = 'Crèche-accueil enfants moins de 6 ans : Erp' ;

if (typeof datasPtr.cadreDemandeGroupe !== 'undefined') {
    var datasDemande = datasPtr.cadreDemandeGroupe ;
} else {
	var datasDemande = null ;
}

/*
 * init pointeurs PJ déclarants ou mandataires
 */
 
var pjSignataire = datasSignature.cadreSignature.soussigne ;
var idDeclarant = 'signataireQualiteDeclarant' ;
var idMandataire = 'signataireQualiteMandataire' ;

/*
 * fonction pushinbox
 */

pushingbox();
	
// algo to execute
var algo = 'calcDestinataireMairie' ;
// info to send
var codeCommune = typeof datasIdentite.cadreAdresse.commune !== 'undefined' ? datasIdentite.cadreAdresse.commune.getId() : null;
var attachementCerfa = '/' + $review.attachments.record[0].getAbsolutePath();
var attachementPJ = [
	{
		"id" : "pj",
		"label" : '/' + $review.attachments.uploaded[0].getAbsolutePath()
	}
];

// generate parameters for transition 
var data = nash.instance.load('output.xml');
data.bind("parameters",{
	"algo" : {
		"algo" : algo,
		"params" : [ 
			{ "key" : "codeCommune", "value" : codeCommune }
		]
	},
	"attachment" : {
		"cerfa" : attachementCerfa,
		"others" : attachementPJ
	}
});