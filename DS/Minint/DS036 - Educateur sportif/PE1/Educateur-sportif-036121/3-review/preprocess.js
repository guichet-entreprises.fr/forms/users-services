//--------------- VARIABLES -------------------//

/* init datas pour review (variables génériques)
 *
 */
 
var data = [] ;
var pjUser = [];
var metas = [];
var repCerfa = 'models/';
var iCardinality = 0 ;

/* init datas pour cerfa (variables spécifiques)
 * contient a minnima les références des groupes 
 * génériques de data.xml
 */

/*
 * init pointeurs saisies, Pj...
 */
 
var datasPtr = $ds036cerfa12699 ;
var datasSignature = datasPtr.cadreSignatureGroupe ;
var datasIdentite = datasPtr.cadreIdentiteGroupe ;
var refFormTitle = 'Educateur sportif' ;

if (typeof datasPtr.cadreDemandeGroupe !== 'undefined') {
    var datasDemande = datasPtr.cadreDemandeGroupe ;
} else {
	var datasDemande = null ;
}

/*
 * init pointeurs PJ déclarants ou mandataires
 */
 
var pjSignataire = datasSignature.cadreSignature.soussigne ;
var idDeclarant = 'signataireQualiteDeclarant' ;
var idMandataire = 'signataireQualiteMandataire' ;

/* import description du CERFA (si il existe)
 * il génère les tables de paramétrage formFields et equivCerfa
 * formFields = dictionnaire des valeurs des champs du Cerfa
 * equivCerfa = dictionnaire de description des champs du Cerfa
 * il définit les variables repCerfa et nameCerfa
 * nameCerfa = nom du fichier pdf associé au Cerfa
 */

/*
 * Remplissage des champs du formulaire (autorisation de desossage)
 */

if (datasDemande !== null) {
	if ((Value('id').of(datasDemande.cadreDemande.nature).eq('depotDossier')) &&  (Value('id').of(datasDemande.cadreDemande.objet).eq(''))) {
		var flgCerfa = true;
	} else {
		var flgCerfa = false;
	}
} else {
	var flgCerfa = true;
}

if (flgCerfa === true) {
	
	//constantes et variables
	var formFields = {} ;
	var nameCerfa = 'cerfa_12699.pdf' ;
	//pointeurs pages de saisie
	var datasInfosDeclaration = datasPtr.infosDeclaration ; //page 1
	var datasInfosCapacite = datasPtr.infosCapacite ; //page 2
	var datasInfosActivite = datasPtr.infosActivite ; //page 3
	
	//paramétrages des cardinalités
	//description des zones liées aux diplômes	
	var setQualifications = "[ "
    	+ "{ src: root.typeQualification, dst: 'typeQualification[i]', valeur: null, type: 'string' },"
    	+ "{ src: root.typeQualificationAutre, dst: 'typeQualification[i]', valeur: null, type: 'string' },"
		+ "{ src: root.activiteQualification, dst: 'activiteQualification[i]', valeur: null, type: 'string' },"
		+ "{ src: root.referenceQualification, dst: 'referenceQualification[i]', valeur: null, type: 'string' },"
		+ "{ src: null, dst: 'jourQualification[i]', valeur: formatDate(root.dateQualification, 'J', true), type: 'string' },"
		+ "{ src: null, dst: 'moisQualification[i]', valeur: formatDate(root.dateQualification, 'M', true), type: 'string' },"
		+ "{ src: null, dst: 'anneeQualification[i]', valeur: formatDate(root.dateQualification, 'A', true), type: 'string' },"
		+ "{ src: root.lieuQualification, dst: 'lieuQualification[i]', valeur: null, type: 'string' },"
		+ "{ src: null, dst: 'jourRecyclageQualification[i]', valeur: formatDate(root.dateRecyclageQualification, 'J', true), type: 'string' },"
		+ "{ src: null, dst: 'moisRecyclageQualification[i]', valeur: formatDate(root.dateRecyclageQualification, 'M', true), type: 'string' },"
		+ "{ src: null, dst: 'anneeRecyclageQualification[i]', valeur: formatDate(root.dateRecyclageQualification, 'A', true), type: 'string' }"
		+ " ] ;" ;

	//description des zones liées aux équivalences	
	var setEquivalences = "[ "
    	+ "{ src: root.typeEquivalence, dst: 'typeEquivalence[i]', valeur: null, type: 'string' },"
    	+ "{ src: root.typeEquivalenceAutre, dst: 'typeEquivalence[i]', valeur: null, type: 'string' },"
		+ "{ src: root.activiteEquivalence, dst: 'activiteEquivalence[i]', valeur: null, type: 'string' },"
		+ "{ src: root.referenceEquivalence, dst: 'referenceEquivalence[i]', valeur: null, type: 'string' },"
		+ "{ src: null, dst: 'jourEquivalence[i]', valeur: formatDate(root.dateEquivalence, 'J', true), type: 'string' },"
		+ "{ src: null, dst: 'moisEquivalence[i]', valeur: formatDate(root.dateEquivalence, 'M', true), type: 'string' },"
		+ "{ src: null, dst: 'anneeEquivalence[i]', valeur: formatDate(root.dateEquivalence, 'A', true), type: 'string' }"
		+ " ] ;" ;
		
	//description des zones liées aux stages	
	var setStages = "[ "
    	+ "{ src: root.stageEtablissement, dst: 'stageEtablissement[i]', valeur: null, type: 'string' },"
    	+ "{ src: null, dst: 'stageAdresse[i]', valeur: mergeAdresse(root.cadreAdresse,'V', false), type: 'string' },"
    	+ "{ src: null, dst: 'stageAdresseSuite[i]', valeur: mergeAdresse(root.cadreAdresse, 'S', false), type: 'string' },"
    	+ "{ src: root.cadreAdresse.codePostal, dst: 'stageCodePostal[i]', valeur: null, type: 'string' },"
    	+ "{ src: null, dst: 'stageCommune[i]', valeur: extractInfoAdresse(root.cadreAdresse, 'C', false), type: 'string' },"
		+ "{ src: null, dst: 'jourDebutStage[i]', valeur: formatDate(root.datesStage.from, 'J', true), type: 'string' },"
		+ "{ src: null, dst: 'moisDebutStage[i]', valeur: formatDate(root.datesStage.from, 'M', true), type: 'string' },"
		+ "{ src: null, dst: 'anneeDebutStage[i]', valeur: formatDate(root.datesStage.from, 'A', true), type: 'string' },"
		+ "{ src: null, dst: 'jourFinStage[i]', valeur: formatDate(root.datesStage.to, 'J', true), type: 'string' },"
		+ "{ src: null, dst: 'moisFinStage[i]', valeur: formatDate(root.datesStage.to, 'M', true), type: 'string' },"
		+ "{ src: null, dst: 'anneeFinStage[i]', valeur: formatDate(root.datesStage.to, 'A', true), type: 'string' },"
    	+ "{ src: root.nomTuteurStage, dst: 'nomTuteurStage[i]', valeur: null, type: 'string' }"
		+ " ] ;" ;

	//description des zones liées aux activités en tant que salarié	
	var setActivitesSalarie = "[ "
    	+ "{ src: root.salarieEtablissement, dst: 'salarieEtablissement[i]', valeur: null, type: 'string' },"
    	+ "{ src: root.salarieSiret, dst: 'salarieSiret[i]', valeur: null, type: 'string' },"
    	+ "{ src: null, dst: 'salarieAdresse[i]', valeur: mergeAdresse(root.cadreAdresse, 'V', false), type: 'string' },"
    	+ "{ src: null, dst: 'salarieAdresseSuite[i]', valeur: mergeAdresse(root.cadreAdresse, 'S', false), type: 'string' },"
    	+ "{ src: root.cadreAdresse.codePostal, dst: 'salarieCodePostal[i]', valeur: null, type: 'string' },"
    	+ "{ src: null, dst: 'salarieCommune[i]', valeur: extractInfoAdresse(root.cadreAdresse, 'C', false), type: 'string' },"
    	+ "{ src: root.salarieActivite, dst: 'salarieActivite[i]', valeur: null, type: 'string' },"
    	+ "{ src: root.salarieDicipline, dst: 'salarieDicipline[i]', valeur: null, type: 'string' },"
		+ "{ src: null, dst: 'salarieMoisDebut[i]', valeur: formatDate(root.salarieDebutExercice, 'M', true), type: 'string' },"
		+ "{ src: null, dst: 'salarieAnneeDebut[i]', valeur: formatDate(root.salarieDebutExercice, 'A', true), type: 'string' },"
		+ "{ src: null, dst: 'salarieMoisFin[i]', valeur: formatDate(root.salarieFinExercice, 'M', true), type: 'string' },"
		+ "{ src: null, dst: 'salarieAnneeFin[i]', valeur: formatDate(root.salarieFinExercice, 'A', true), type: 'string' }"
		+ " ] ;" ;

	//description des zones liées aux activités en tant qu'indépendant	
	var setActivitesIndependant = "[ "
    	+ "{ src: root.independantActivite, dst: 'independantActivite[i]', valeur: null, type: 'string' },"
    	+ "{ src: root.independantDicipline, dst: 'independantDicipline[i]', valeur: null, type: 'string' },"
    	+ "{ src: root.independantLieuExercice, dst: 'independantLieuExercice[i]', valeur: null, type: 'string' },"
    	+ "{ src: null, dst: 'independantAdresseExercice[i]', valeur: mergeAdresse(root.cadreAdresse, 'V', false), type: 'string' },"
    	+ "{ src: null, dst: 'independantAdresseSuiteExercice[i]', valeur: mergeAdresse(root.cadreAdresse, 'S', false), type: 'string' },"
    	+ "{ src: null, dst: 'independantDepartementExercice[i]', valeur: extractInfoAdresse(root.cadreAdresse, 'D', false), type: 'string' },"
    	+ "{ src: root.cadreAdresse.codePostal, dst: 'independantCodePostalExercice[i]', valeur: null, type: 'string' },"
    	+ "{ src: null, dst: 'independantCommuneExercice[i]', valeur: extractInfoAdresse(root.cadreAdresse, 'C', false), type: 'string' },"
		+ "{ src: null, dst: 'independantJourDebutExercice[i]', valeur: formatDate(root.independantDebutExercice, 'J', true), type: 'string' },"
		+ "{ src: null, dst: 'independantMoisDebutExercice[i]', valeur: formatDate(root.independantDebutExercice, 'M', true), type: 'string' },"
		+ "{ src: null, dst: 'independantAnneeDebutExercice[i]', valeur: formatDate(root.independantDebutExercice, 'A', true), type: 'string' }"
		+ " ] ;" ;

	//description des zones de prenoms
	var setPrenoms = "[ "
    	+ "{ src: root, dst: 'prenom[i]', valeur: null, type: 'string' }"
		+ " ] ;" ;
	
		//description des zones génériques du Cerfa (type bool, type string...)
	var equivCerfa = [
		// page 1 saisie
		{ src: datasInfosDeclaration.typeDemande, dst: 'premiereDeclaration', valeur: 'premiereDeclaration', type: 'radio' },
		{ src: datasInfosDeclaration.typeDemande, dst: 'renouvellementDeclaration', valeur: 'renouvellementDeclaration', type: 'radio' },
		{ src: null, dst: 'departementExercice', valeur: extractInfoAdresse(datasIdentite.cadreAdresse,'D', false), type: 'string' }, 
    	{ src: datasIdentite.cadreIdentite.civilite, dst: 'civiliteMonsieur', valeur: 'civiliteMasculin', type: 'radio' },
    	{ src: datasIdentite.cadreIdentite.civilite, dst: 'civiliteMadame', valeur: 'civiliteFeminin', type: 'radio' },
		{ src: datasIdentite.cadreIdentite.nomNaissance, dst: 'nomNaissance', valeur: null, type: 'string' },
		{ src: datasIdentite.cadreIdentite.nomUsage, dst: 'nomUsage', valeur: null, type: 'string' },
		{ src: datasIdentite.cadreIdentite.prenoms, dst: 'datasIdentite.cadreIdentite.prenoms', valeur: setPrenoms, type: 'cardinality' },
		{ src: null, dst: 'jourNaissance', valeur: formatDate(datasInfosDeclaration.cadreNaissance.dateNaissance, 'J', true), type: 'string' },
		{ src: null, dst: 'moisNaissance', valeur: formatDate(datasInfosDeclaration.cadreNaissance.dateNaissance, 'M', true), type: 'string' },
		{ src: null, dst: 'anneeNaissance', valeur: formatDate(datasInfosDeclaration.cadreNaissance.dateNaissance, 'A', true), type: 'string' },
		{ src: datasInfosDeclaration.cadreNaissance.lieuNaissance, dst: 'communeNaissance', valeur: null, type: 'string' },
		{ src: datasInfosDeclaration.cadreNaissance.paysNaissance, dst: 'paysNaissance', valeur: null, type: 'string' },
		{ src: datasInfosDeclaration.cadreNaissance.cadreComplementNaissance.nomPere, dst: 'nomPere', valeur: null, type: 'string' },
		{ src: null, dst: 'prenomsPere', valeur: mergePrenoms(datasInfosDeclaration.cadreNaissance.cadreComplementNaissance.prenomsPere), type: 'string' },
		{ src: datasInfosDeclaration.cadreNaissance.cadreComplementNaissance.nomMere, dst: 'nomMere', valeur: null, type: 'string' },
		{ src: null, dst: 'prenomsMere', valeur: mergePrenoms(datasInfosDeclaration.cadreNaissance.cadreComplementNaissance.prenomsMere), type: 'string' },
		{ src: datasInfosDeclaration.cadreNaissance.nationalite, dst: 'nationalite', valeur: null, type: 'string' },
		{ src: datasInfosDeclaration.cadreNaissance.autreNationalite, dst: 'autreNationalite', valeur: null, type: 'string' },
		{ src: null, dst: 'adresse', valeur: mergeAdresse(datasIdentite.cadreAdresse, 'V', false), type: 'string' },
		{ src: null, dst: 'adresseSuite', valeur: mergeAdresse(datasIdentite.cadreAdresse, 'S', false), type: 'string' },
		{ src: datasIdentite.cadreAdresse.codePostal, dst: 'codePostal', valeur: null, type: 'string' },
		{ src: null, dst: 'commune', valeur: extractInfoAdresse(datasIdentite.cadreAdresse, 'C', false), type: 'string' },
		{ src: null, dst: 'pays', valeur: extractInfoAdresse(datasIdentite.cadreAdresse, 'P', false), type: 'string' },
		{ src: datasIdentite.cadreContact.telephoneFixe.telephoneNum, dst: 'telephoneFixe', valeur: null, type: 'string' },
		{ src: datasIdentite.cadreContact.telephoneMobile.telephoneNum, dst: 'telephoneMobile', valeur: null, type: 'string' },
		{ src: datasIdentite.cadreContact.courriel, dst: 'courriel', valeur: null, type: 'string' },
		// page 2 saisie
		{ src: datasInfosCapacite.cadreDiplomes.cadreQualifications, dst: 'datasInfosCapacite.cadreDiplomes.cadreQualifications', valeur: setQualifications, type: 'cardinality' },
		{ src: datasInfosCapacite.cadreDiplomes.cadreEquivalences, dst: 'datasInfosCapacite.cadreDiplomes.cadreEquivalences', valeur: setEquivalences, type: 'cardinality' },
		{ src: datasInfosCapacite.cadreFormations.typeFormation, dst: 'typeFormation', valeur: null, type: 'string' },
		{ src: datasInfosCapacite.cadreFormations.typeFormationAutre, dst: 'typeFormation', valeur: null, type: 'string' },
		{ src: datasInfosCapacite.cadreFormations.activiteFormation, dst: 'activiteFormation', valeur: null, type: 'string' },
		{ src: datasInfosCapacite.cadreFormations.cadreStages, dst: 'datasInfosCapacite.cadreFormations.cadreStages', valeur: setStages, type: 'cardinality' },
		// page 3 saisie
		{ src: datasInfosActivite.cadreSalarie.salarieTitre, dst: 'salarieTitrePrincipal', valeur: 'titrePrincipal', type: 'radio' },
		{ src: datasInfosActivite.cadreSalarie.salarieTitre, dst: 'salarieTitreSecondaire', valeur: 'titreSecondaire', type: 'radio' },
		{ src: datasInfosActivite.cadreSalarie.cadreActivites, dst: 'datasInfosActivite.cadreSalarie.cadreActivites', valeur: setActivitesSalarie, type: 'cardinality' },
		{ src: datasInfosActivite.cadreIndependant.independantTitre, dst: 'independantTitrePrincipal', valeur: 'titrePrincipal', type: 'radio' },
		{ src: datasInfosActivite.cadreIndependant.independantTitre, dst: 'independantTitreSecondaire', valeur: 'titreSecondaire', type: 'radio' },
		{ src: datasInfosActivite.cadreIndependant.independantDenominationSociale, dst: 'independantDenominationSociale', valeur: null, type: 'string' },
		{ src: datasInfosActivite.cadreIndependant.independantEntreprise, dst: 'independantEurl', valeur: 'eurl', type: 'radio' },
		{ src: datasInfosActivite.cadreIndependant.independantEntreprise, dst: 'independantEi', valeur: 'ei', type: 'radio' },
		{ src: datasInfosActivite.cadreIndependant.independantSiret, dst: 'independantSiret', valeur: null, type: 'string' },
		{ src: datasInfosActivite.cadreIndependant, dst: 'independantAdresse', valeur: mergeAdresse(datasIdentite.cadreAdresse, 'V', false), type: 'xString' },
		{ src: datasInfosActivite.cadreIndependant, dst: 'independantAdresseSuite', valeur: mergeAdresse(datasIdentite.cadreAdresse, 'S', false), type: 'xString' },
		{ src: datasInfosActivite.cadreIndependant, dst: 'independantCodePostal', valeur: datasIdentite.cadreAdresse.codePostal, type: 'xString' },
		{ src: datasInfosActivite.cadreIndependant, dst: 'independantCommune', valeur: extractInfoAdresse(datasIdentite.cadreAdresse, 'C', false), type: 'xString' },
		{ src: datasInfosActivite.cadreIndependant, dst: 'independantTelephoneFixe', valeur: datasIdentite.cadreContact.telephoneFixe.telephoneNum, type: 'xString' },
		{ src: datasInfosActivite.cadreIndependant, dst: 'independantTelephoneMobile', valeur: datasIdentite.cadreContact.telephoneMobile.telephoneNum, type: 'xString' },
		{ src: datasInfosActivite.cadreIndependant, dst: 'independantCourriel', valeur: datasIdentite.cadreContact.courriel, type: 'xString' },
		{ src: datasInfosActivite.cadreIndependant, dst: 'independantSiteWeb', valeur: datasIdentite.cadreContact.siteWeb, type: 'xString' },
		{ src: datasInfosActivite.cadreIndependant.adresseDifferente, dst: 'independantAdresse', valeur: mergeAdresse(datasInfosActivite.cadreIndependant.adresseIndependant.cadreAdresse, 'V', false), type: 'bString' },
		{ src: datasInfosActivite.cadreIndependant.adresseDifferente, dst: 'independantAdresseSuite', valeur: mergeAdresse(datasInfosActivite.cadreIndependant.adresseIndependant.cadreAdresse, 'S', false), type: 'bString' },
		{ src: datasInfosActivite.cadreIndependant.adresseDifferente, dst: 'independantCodePostal', valeur: datasInfosActivite.cadreIndependant.adresseIndependant.cadreAdresse.codePostal, type: 'bString' },
		{ src: datasInfosActivite.cadreIndependant.adresseDifferente, dst: 'independantCommune', valeur: extractInfoAdresse(datasInfosActivite.cadreIndependant.adresseIndependant.cadreAdresse, 'C', false), type: 'bString' },		
		{ src: datasInfosActivite.cadreIndependant.cadreActivites, dst: 'datasInfosActivite.cadreIndependant.cadreActivites', valeur: setActivitesIndependant, type: 'cardinality' },
		// bloc signature
		{ src: null, dst: 'signatureNom', valeur: genereInfoSignataire(false, true), type: 'string' },
		{ src: null, dst: 'signatureJour', valeur: formatDate(datasSignature.cadreSignature.signatureDate, 'J', true), type: 'string' },	
		{ src: null, dst: 'signatureMois', valeur: formatDate(datasSignature.cadreSignature.signatureDate, 'M', true), type: 'string' },	
		{ src: null, dst: 'signatureAnnee', valeur: formatDate(datasSignature.cadreSignature.signatureDate, 'A', true), type: 'string' },
		{ src: null, dst: 'signature', valeur: genereInfoSignataire(true, true), type: 'string' }		
	] ; 
}


//--------------- VARIABLES -------------------//

//--------------- FONCTIONS -------------------//

/* fonctions génériques de formatage des données
 *
 */

/*
 * calculs mathématiques : calcul de pourcentage
 */
 
function calculPercent(valNumerateur, valDenominateur, nbDecimales) {
	var valResult = '' ;
	var tmpValeur = 0 ;
	while (true) {
		if (typeof valNumerateur === 'undefined' || typeof valNumerateur !== 'number') {
			break ;
		}
		if (typeof valDenominateur === 'undefined' || typeof valDenominateur !== 'number') {
			break;
		}
		if (typeof nbDecimales === 'undefined' || typeof nbDecimales !== 'number') {
			tmpValeur = 0 ;
		} else {
			tmpValeur = nbDecimales >= 0 ? nbDecimales : 0 ; 
		}	
		tmpValeur = Math.pow(10, tmpValeur);
		if (valDenominateur !== 0) {
			tmpValeur = Math.round(((valNumerateur / valDenominateur) * 100) / tmpValeur) ;
			valResult = tmpValeur.toString();
			
		}
		break ;
	}
	return valResult ;
 }

/*
 * calculs mathématiques : calcul de soustraction
 */
 
function calculSubstraction(valTotal, tabValeurs, cumulValeur, keyCumul) {
	var valResult = '' ;
	var tmpValeur = 0 ;
	while (true) {
		if (typeof valTotal === 'undefined' || typeof valTotal !== 'number') {
			break ;
		}
		tmpValeur = valTotal ;
		if (typeof tabValeurs === 'object' && Array.isArray(tabValeurs)) {
			for (tmpIndex = 0; tmpIndex < tabValeurs.length; tmpIndex++) {
				if (typeof tabValeurs[tmpIndex] === 'number') {
					tmpValeur -= tabValeurs[tmpIndex];
					tmpFlag = true ;
				}
			}	
		}
		if (tmpFlag === true) {
			valResult = tmpValeur.toString();
			if (typeof cumulValeur === 'object' && cumulValeur !== null && typeof keyCumul === 'string') {
				if (typeof cumulValeur[keyCumul] !== 'undefined' && isNaN(cumulValeur[keyCumul]) === false) {
					cumulValeur[keyCumul] = (Number(cumulValeur[keyCumul]) - tmpValeur).toString() ;
				} else {
					cumulValeur[keyCumul] = valResult ;
				}
			}
		}
		break;
	}
	return valResult ;
 }

function calculAddition(tabValeurs, cumulValeur, keyCumul) {
	var valResult = '' ;
	var tmpFlag = false ;
	var tmpValeur = 0 ;
	var tmpIndex = 0 ;
	while (true) {
		if (typeof tabValeurs === 'object' && Array.isArray(tabValeurs)) {
			for (tmpIndex = 0; tmpIndex < tabValeurs.length; tmpIndex++) {
				if (typeof tabValeurs[tmpIndex] === 'number') {
					tmpValeur += tabValeurs[tmpIndex];
					tmpFlag = true ;
				}
			}	
		}
		if (tmpFlag === true) {
			valResult = tmpValeur.toString();
			if (typeof cumulValeur === 'object' && cumulValeur !== null && typeof keyCumul === 'string') {
				if (typeof cumulValeur[keyCumul] !== 'undefined' && isNaN(cumulValeur[keyCumul]) === false) {
					cumulValeur[keyCumul] = (tmpValeur + Number(cumulValeur[keyCumul])).toString() ;
				} else {
					cumulValeur[keyCumul] = valResult ;
				}
			}
		}
		break ;
	}
	return valResult ;
 }

/* suppression des espaces dans une chaine
 *
 */

function supprimeEspaces(szLibel) {
	var tmpRegex = /\s/g ;
	var tmpLibel = '';
	if (typeof szLibel !== 'undefined' && typeof szLibel === 'string') {
		tmpLibel = szLibel !== null ? szLibel.replace(tmpRegex, '') : '' ;	
	}
	return tmpLibel ;
}
 
/* merge des prénoms (séparateur ',')
 *
 */
	
function mergePrenoms(listPrenoms) {
	var prenoms = [];
	if (typeof listPrenoms !== 'undefined' && typeof listPrenoms === 'object') {
		for (i = 0; i < listPrenoms.length; i++) {
			prenoms.push(listPrenoms[i]);
    	}
	} else {
		prenoms.push('');
	}
	return prenoms.toString();
}

/* merge du nom et des prénoms (séparateur ',')
 *
 */

function mergeNomPrenoms(szNom, listPrenoms) {
	var result = '';
	if (typeof szNom === 'string') {
		result = szNom + ' ';
		result += mergePrenoms(listPrenoms);
	} 
	return result;
}

/* construction de la date et du lieu de naissance
 *
 */

function mergeNaissance(blocNaissance) {
	var naissance = '' ;
	if (typeof blocNaissance !== 'undefined' && typeof blocNaissance === 'object') {
		naissance = blocNaissance.lieuNaissance ? blocNaissance.lieuNaissance + ' / ' : '' ;
		naissance += blocNaissance.paysNaissance ? blocNaissance.paysNaissance : '' ;
	}
	return naissance ;
}
	
/* construction de l'adresse en tenant compte du pays ou non
 *
 */

function mergeAdresse(blocAdresse, typeMerge, multiLine) {
	var tmpVar = '' ;
	var adresse = '' ;
	var chgLigne = ' \n' ;
	if (typeof blocAdresse !== 'undefined' && typeof blocAdresse === 'object') {
		var fmtLigne = (multiLine === true ? chgLigne : ' ');
		var typeAct = (typeof typeMerge !== 'undefined' && typeof typeMerge === 'string') ? typeMerge.toUpperCase() : 'T' ;
		switch (typeAct) {
			case 'N':
				adresse += blocAdresse.numeroVoie ? blocAdresse.numeroVoie + ' ' : '' ;
				adresse += blocAdresse.indiceVoie ? blocAdresse.indiceVoie : '' ;
				break;
			case 'V':	
				tmpVar = mergeAdresse(blocAdresse, 'N', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + ' ' : '' ;
				adresse += mergeAdresse(blocAdresse, 'W', multiLine) ;
				break;
			case 'W':
				adresse += blocAdresse.typeVoie ? blocAdresse.typeVoie + ' ' : '' ;
				adresse += blocAdresse.nomVoie ?  blocAdresse.nomVoie : '' ;
				break;
			case 'S':
				adresse += blocAdresse.complementAdresse ? blocAdresse.complementAdresse + fmtLigne : '' ;
				adresse += blocAdresse.distributionSpeciale ? blocAdresse.distributionSpeciale : '' ;
				break;
			case 'C':
				if (typeof blocAdresse.pays !== 'undefined') {
					if (typeof blocAdresse.codePostal !== 'undefined' && blocAdresse.codePostal) {
						adresse += blocAdresse.codePostal ? blocAdresse.codePostal + ' ' : '' ;
						adresse += blocAdresse.commune ? blocAdresse.commune : '' ;
					} else {
						adresse += blocAdresse.ville ? blocAdresse.ville + fmtLigne : '' ;
						adresse += blocAdresse.pays ? blocAdresse.pays : '' ;
					}
				} else {
					adresse += blocAdresse.codePostal ? blocAdresse.codePostal + ' ' : '' ;
					adresse += blocAdresse.commune ? blocAdresse.commune : '' ;
				}
				break;
			case 'X':
				tmpVar = mergeAdresse(blocAdresse, 'S', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + fmtLigne : '' ;
				adresse += mergeAdresse(blocAdresse, 'C', multiLine) ;
				break;
			case 'Z':
				tmpVar = mergeAdresse(blocAdresse, 'V', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + fmtLigne : '' ;
				adresse += mergeAdresse(blocAdresse, 'S', multiLine) ;
				break;
			case 'T':
			default:
				tmpVar = mergeAdresse(blocAdresse, 'V', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + fmtLigne : '' ;
				tmpVar = mergeAdresse(blocAdresse, 'S', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + fmtLigne : '' ;
				adresse += mergeAdresse(blocAdresse, 'C', multiLine) ;
		}
	}
	return adresse;
}

/*
 * mise en forme numéro de téléphone au format 10 chiffres
 */

function formatNumTelephone(objNumTel) {
	var tmpRegexTelIntro = /^(\+\d{1,3}[\s\.\-]?)(.*)$/ ;
	var tmpRegexTel = /(\d{1,})[\s\.\-]?/g ;
	var tmpTel = '' ;
	var tmpIntro = '';
	var tmpTelFmt = '';
	if (typeof objNumTel !== 'undefined' && typeof objNumTel === 'object' && objNumTel) {
		tmpTel = objNumTel.international ;
		if (tmpRegexTelIntro.test(tmpTel) === true) {
			tmpIntro = tmpTel.replace(tmpRegexTelIntro, '$1') ;
			tmpTelFmt = '$2' ;
			if (tmpIntro.substring(0,3) == '+33') {
				tmpTelFmt = '0' + tmpTelFmt ;
			}	
			tmpTel = tmpTel.replace(tmpRegexTelIntro, tmpTelFmt) ;
			tmpTel = tmpTel.replace(tmpRegexTel, '$1') ;
		}
	}
	return tmpTel ;
}

/*
 * infos contacts téléphoniques
 */
 
function mergeNumTelephone(blocNumTel, flgMerge, flgInternational) {
	var numTel = '';
	var tmpTel = '';
	if (typeof blocNumTel !== 'undefined' && typeof blocNumTel === 'object') {
		if (typeof blocNumTel.telephoneFixe.telephoneNum !== 'undefined' && blocNumTel.telephoneFixe.telephoneNum ) {
			if (flgInternational === false) {
				tmpTel = formatNumTelephone(blocNumTel.telephoneFixe.telephoneNum) ;
			} else {
				tmpTel = blocNumTel.telephoneFixe.telephoneNum.international ;
			}
			numTel += tmpTel ;
		}
		if (typeof blocNumTel.telephoneMobile.telephoneNum !== 'undefined' && blocNumTel.telephoneMobile.telephoneNum) {
			if (numTel.length > 0 && flgMerge === true) {
				numTel += ' / ';
			}
			if (flgMerge === true || numTel.length === 0) {
				if (flgInternational === false) {
					tmpTel = formatNumTelephone(blocNumTel.telephoneMobile.telephoneNum) ;
				} else {
					tmpTel = blocNumTel.telephoneMobile.telephoneNum.international;
				}
				numTel += tmpTel ;
			}
		}
	}
	return numTel;
}

/* formatage d'une date avec extaction ou non d'une partie (J, M, A)
 *
 */

function formatDate(valDate, typeExtract, fullYear) {
	var dateResult = '';
	var tmpYear = '' ;
	if (typeof valDate === 'object' && valDate) {
    	var dateTmp = new Date(parseInt(valDate.getTimeInMillis())) ; ;
		var dateIso = ((dateTmp.toLocaleDateString()).substr(0, 10)).split('-') ;
		var typeAct = (typeof typeExtract !== 'undefined' && typeof typeExtract === 'string') ? typeExtract.toUpperCase() : 'T' ;
		if (dateIso.length > 1) {
			tmpYear = (fullYear === true ? dateIso[0] : dateIso[0].substr(2)) ;
			switch (typeAct) {
				case 'D':
				case 'J':
					dateResult = dateIso[2] ;
					break;
				case 'M':
					dateResult = dateIso[1] ;
					break;
				case 'A':
				case 'Y':
					dateResult = tmpYear ;
					break;
				case 'T':
				default:
					dateResult = dateIso[2] + '/' + dateIso[1] + '/' + tmpYear ;
			}
		} else {
			dateResult = dateIso[0] ;
		}
	}
	return dateResult ;
}

/*
 * infos signataire
 */

function genereInfoSignataire(flgCompleteInfos, flgMergePrenoms) {
	var contactInfos = [] ;
	var infoSignataire = '' ;
	var chgLigne = ' \n' ;
	if (typeof datasSignature !== 'undefined' && typeof datasIdentite !== 'undefined' && typeof idMandataire !== 'undefined') {
		if (flgCompleteInfos === true) {
			infoSignataire = 'Cette déclaration respecte les attendus de l\’article 1367 du code civil.' + chgLigne ;
			infoSignataire += datasSignature.cadreSignature.soussigne + ' ';
		}
		if (Value('id').of(datasSignature.cadreSignature.soussigne).eq(idMandataire)) {
			infoSignataire += datasSignature.cadreSignature.informationsMandataire.nomPrenomDenomination ;
			if (flgCompleteInfos === true) {
				infoSignataire += chgLigne ;
				infoSignataire += mergeAdresse(datasSignature.cadreSignature.informationsMandataire.cadreAdresse, 'T', true) ;
			}
		} else {
			infoSignataire += (datasIdentite.cadreIdentite.nomUsage ? datasIdentite.cadreIdentite.nomUsage + ' ' : datasIdentite.cadreIdentite.nomNaissance) + ' ' ;
			if (flgMergePrenoms === true) {
				infoSignataire += mergePrenoms(datasIdentite.cadreIdentite.prenoms) ;
			}
			if (flgCompleteInfos === true) {
				infoSignataire += chgLigne ;
				if (typeof datasIdentite.cadreContact.telephoneFixe.telephoneNum !== 'undefined' && datasIdentite.cadreContact.telephoneFixe.telephoneNum) {
					contactInfos.push(datasIdentite.cadreContact.telephoneFixe.telephoneNum) ;
				}
				if (typeof datasIdentite.cadreContact.telephoneMobile.telephoneNum !== 'undefined' && datasIdentite.cadreContact.telephoneMobile.telephoneNum) {
					contactInfos.push(datasIdentite.cadreContact.telephoneMobile.telephoneNum);
				}
				if (typeof datasIdentite.cadreContact.courriel !== 'undefined' && datasIdentite.cadreContact.courriel) {
					contactInfos.push(datasIdentite.cadreContact.courriel);
				}
				infoSignataire += contactInfos.toString() ;
			}
		}
	}
	return infoSignataire ;
}

/*
 * extraction d'informations liées à l'identité (nom, prénoms...)
 */

function extractInfoIdentite(blocIdentite, typeExtract, flgExtract) {
	var infoIdentite = '';
	if (typeof blocIdentite !== 'undefined' && typeof blocIdentite === 'object') {
		var typeExt = (typeExtract !== 'undefined' && typeof typeExtract === 'string') ? typeExtract.toUpperCase() : 'T' ;
		switch (typeExt) {
			case 'N':
			case 'L':
				infoIdentite = blocIdentite.nomUsage ? blocIdentite.nomUsage : (blocIdentite.nomNaissance ? blocIdentite.nomNaissance : '') ;
				break;
			case 'P':
			case 'F':
				infoIdentite = flgExtract ? mergePrenoms(blocIdentite.prenoms) : (blocIdentite.prenoms.length > 0 ? blocIdentite.prenoms[0] : '') ;
				break ;
			case 'C':
				infoIdentite = blocIdentite.civilite ? blocIdentite.civilite : '' ;
				break
			case 'X':
				infoIdentite += extractInfoIdentite(blocIdentite, 'N', flgExtract) ;
				infoIdentite += ' ' ;
				infoIdentite += extractInfoIdentite(blocIdentite, 'P', flgExtract) ;
				break;
			case 'T':
				infoIdentite  = extractInfoIdentite(blocIdentite, 'C', flgExtract) ;
				infoIdentite += ' ' ;
				infoIdentite += extractInfoIdentite(blocIdentite, 'N', flgExtract) ;
				infoIdentite += ' ' ;
				infoIdentite += extractInfoIdentite(blocIdentite, 'P', flgExtract) ;
				break;
			default:
				infoIdentite = '';
		}
	}
	return infoIdentite ;
}

/*
 * extraction d'informations liées aux adresses
 */
 
function extractInfoAdresse(blocAdresse, typeExtract, flgExtract) {
	var tmpDept = /(0[1-9]|[1-8][0-9]|2[A-B]|9[0-5]|9[7-8][0-9])(\d{2,3})/ ;
	var adresse = '' ;
	if (typeof blocAdresse !== 'undefined' && typeof blocAdresse === 'object') {
		var typeAct = (typeof typeExtract !== 'undefined' && typeof typeExtract === 'string') ? typeExtract.toUpperCase() : 'T' ;
		switch (typeAct) {
			case 'D':
				adresse = blocAdresse.commune ? blocAdresse.commune.id : '' ;
				adresse = adresse.replace(tmpDept, '$1') ;
				break;
			case 'P':
				if (typeof blocAdresse.ville !== 'undefined' && blocAdresse.ville) {
					adresse = blocAdresse.pays ;
				} else {
					if (flgExtract === true) {
						adresse = blocAdresse.pays ;
					}
				}
				break;
			case 'C':
				if (typeof blocAdresse.ville !== 'undefined' && blocAdresse.ville) {
					adresse = blocAdresse.ville ;
				} else {
					adresse = blocAdresse.commune ;
				}
				break;
			default:
		}
	}
	return adresse;
}

/*
 * extraction indicatif téléphone si indicatif non français (+33)
 */

function extractIndicatifEtrangerTelephone(objNumTel) {
	var tmpRegexTelIntro = /^(\+\d{1,3}[\s\.\-]?)(.*)$/ ;
	var tmpTel = '' ;
	var tmpIntro = '';
	if (typeof objNumTel !== 'undefined' && typeof objNumTel === 'object' && objNumTel) {
		if (tmpRegexTelIntro.test(objNumTel.international) === true) {
			tmpIntro = objNumTel.international.replace(tmpRegexTelIntro, '$1').trim() ;
			if (tmpIntro !== '+33') {
				tmpTel = tmpIntro ;
			}
		}
	}
	return tmpTel ;
}

/* merge de PJs
 * 
 */

function merge(pjs) {
	var mergedFile = nash.doc.create();
	pjs.forEach(function (elm) {		
		mergedFile.append(elm).save('item.pdf');
    });
	return mergedFile;
}

/* ajout de PJ
 * 
 */

function appendPj(fld, listPj) {
	fld.forEach(function (elm) {
        listPj.push(elm);
		_log.info('elm.getAbsolutePath() = >'+elm.getAbsolutePath());
		metas.push({'name':'userAttachments', 'value': '/'+elm.getAbsolutePath()});
		metas.push({'name':'document', 'value': '/'+elm.getAbsolutePath()});
    });
}

/* remplissage d'un cerfa à partir d'une table de paramétrage
 *
*/

function genereDynamicParams(tabParam, baseAddress) {
	var tmpTab = tabParam.replace(/root/gi, baseAddress);
	var tmpObj = [] ;
	try {
		tmpObj = eval(tmpTab) ;
	} catch(error) {
		tmpObj = [];
	}
  	return tmpObj;
}

function genereXDynamicParams(tabParam, baseAddress) {
	var tmpTab = '';
	var tmpVal ;
	var tmpFlg = false ;
	var tmpInd = 0;
	var tmpKey = {} ;
	var tmpObj = [] ;
	var tmpRegG = /root/gi ; // pour remplacer root. par le préfixe réel
	var tmpReg = [ /src\:[\s]*([^;\{\}]*)[;]*.*$/i, // pour isoler la valeur associée à src
				   /dst\:[\s]*([^;\{\}]*)[;]*.*$/i, // pour isoler la valeur associée à dst
				   /valeur\:[\s]*([^;\{\}]*)[;]*.*$/i, // pour isoler la valeur associée à value
				   /type\:[\s]*([^;\{\}]*)[;]*.*$/i ] ; // pour isoler la valeur associée à type
	var tmpLib = [ 'src', 'dst', 'valeur', 'type' ] ; // table des propriétés src, dst, value, type
	tabParam.forEach(function (elm) {
		try {
			tmpTab = elm.replace(tmpRegG, baseAddress);
			tmpKey = {} ;
			for (tmpInd = 0; tmpInd < tmpLib.length; tmpInd++) {
				tmpVal = tmpReg[tmpInd].exec(tmpTab) ;
				if (tmpVal !== null && tmpVal.length > 0) {
					tmpKey[tmpLib[tmpInd]] = eval(tmpVal[1].trim());
					tmpFlg = true ;
				} else {
					tmpFlg = false ;
					break;
				}
			}
			if (tmpFlg == true) {
				tmpObj.push(tmpKey) ;
			}
		} catch(error) {
			tmpFlg = false ;
		}
		return ;
    });
  	return tmpObj;
}

function completeCerfa(paramFields, valFields, indFields) {
	var tmpDst = '';
	var tmpResult = true ;
	var tmpRegexIndice = /\[i\]/gi ; // forme 'champ-cerfa[i] par exemple'
	var tmpRegexIndex = /\[x\]/gi ; // forme 'champ-cerfa[x] par exemple'
	var tmpBase = '' ;
	var tmpType = '' ;
	var tmpValue = '' ;
	var tmpFlag = false ;
	var objParam = [] ;
	
	if (typeof paramFields !== 'object' || !(Array.isArray(paramFields)) || typeof formFields !== 'object' || typeof indFields !== 'number') {
		return false;
	}
	paramFields.forEach(function (item) {
		if (typeof item.src === 'undefined') {
			return ;
		}
		tmpType = (typeof item.type !== undefined && typeof item.type === 'string') ? (item.type).toUpperCase() : '' ;
		if (tmpType === 'CARDINALITY' || tmpType === 'XCARDINALITY') {
			if (typeof item.src !== 'object' || item.src === null) {
				return ;
			}
			for (iCardinality = 0; iCardinality < (item.src).length; iCardinality++) {
				tmpBase = item.dst + '[' + iCardinality.toString() + ']' ;
				switch (tmpType) {
					case 'CARDINALITY':
						objParam = genereDynamicParams(item.valeur, tmpBase);
						break;
					case 'XCARDINALITY':
						objParam = genereXDynamicParams(item.valeur, tmpBase);
						break;
					default:
						objParam = [];
						break;
				}
				if (completeCerfa(objParam, valFields, iCardinality) == false) {
					tmpResult = false ;
				}
			}
			return ;
		}
		if (tmpType === 'COMPLEX' || tmpType === 'XCOMPLEX') {
			if (typeof item.src !== 'object') {
				return ;
			}
			(item.valeur).forEach(function (xtem) {
				switch (tmpType) {
					case 'COMPLEX':
						objParam = genereDynamicParams(xtem, item.dst);
						break;
					case 'XCOMPLEX':
						objParam = genereXDynamicParams(xtem, item.dst);
						break;
					default:
						objParam = [];
						break;
				}
				if (completeCerfa(objParam, valFields, 0) == false) {
					tmpResult = false ;
				}
				return ;
			});
			return ;
		}
		tmpDst = (item.dst).replace(tmpRegexIndice, (indFields + 1).toString()) ;
		tmpDst = tmpDst.replace(tmpRegexIndex, indFields.toString()) ;
		switch (tmpType) {
			case 'BOOL':
				var tmpFlg = typeof item.valeur === 'boolean' ? item.valeur : false ;
				if (item.src === null) {
					valFields[tmpDst] = tmpFlg ;
				} else {
					valFields[tmpDst] = item.src === tmpFlg ? true : false;
				}
				break;
			case 'XBOOL':
				if ((typeof item.src === 'boolean') && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					if (item.valeur[0] === item.valeur[1]) {
						if (item.src === false) {
							valFields[tmpDst] = null;
						}
					} else {
						valFields[tmpDst] = item.src;
					}
				}
				break;
			case 'RADIO':
				if (item.src) {
					valFields[tmpDst] = Value('id').of(item.src).eq(item.valeur) ? true : false;
				}
				break;
			case 'BRADIO':
				if ((typeof item.src === 'boolean' && item.src) && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					valFields[tmpDst] = Value('id').of(item.valeur[0]).eq(item.valeur[1]) ? true : false;
				}
				break;
			case 'XRADIO':
				if ((typeof item.src === 'boolean') && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					if (Value('id').of(item.valeur[0]).eq(item.valeur[1])) {
						if (item.src === false) {
							valFields[tmpDst] = null;
						}
					} else {
						valFields[tmpDst] = item.src;
					}
				}
				break;
			case 'CHECK':
				if (item.src) {
					valFields[tmpDst] = Value('id').of(item.src).contains(item.valeur) ? true : false;
				}
				break;
			case 'BCHECK':
				if ((typeof item.src === 'boolean' && item.src) && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					valFields[tmpDst] = Value('id').of(item.valeur[0]).contains(item.valeur[1]) ? true : false;
				}
				break;
			case 'XCHECK':
				if ((typeof item.src === 'boolean') && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					if (Value('id').of(item.valeur[0]).contains(item.valeur[1])) {
						if (item.src === false) {
							valFields[tmpDst] = null;
						}
					} else {
						valFields[tmpDst] = item.src;
					}
				}
				break;
			case 'STRING':
				if (item.valeur) {
					tmpValue = item.valeur + ' ' ;
					tmpFlag = true ;
				} else {
					tmpValue = '' ;
					tmpFlag = false ;
				}
				if (item.src) {
					tmpValue += item.src ;
					tmpFlag = true ;
				}
				if (tmpFlag) {
					valFields[tmpDst] = tmpValue ;
				}
				break;
			case 'BSTRING':
				if ((typeof item.src === 'boolean' && item.src) && (typeof item.valeur !== 'undefined' && item.valeur)) {
					valFields[tmpDst] = item.valeur ;
				}
				break;
			case 'XSTRING':
				if (((typeof item.src === 'string' or typeof item.src === 'object') && item.src) && (typeof item.valeur !== 'undefined' && item.valeur)) {
					valFields[tmpDst] = item.valeur ;
				}
				break;
			default:
				break;
		}
		return ;
	});
	return tmpResult ;
}

//--------------- FONCTIONS -------------------//

//--------------- MAIN GENERATION CERFA -------//

/* génération du Cerfa à partir de la table d'équivalences
 * (génération optionnelle)
 */

if (typeof formFields !== 'undefined' && typeof nameCerfa !== 'undefined') {
	var flgCerfa = true ;
	if (typeof equivCerfa !== 'undefined') { 
		flgCerfa = completeCerfa(equivCerfa, formFields, 0);
	}
	if (flgCerfa === true) {
		var cerfaDoc = nash.doc
			.load(repCerfa + nameCerfa)
			.apply(formFields);
		var pjCerfa = cerfaDoc.save(nameCerfa);
	}
}

/* ajout des PJ utilisateurs le cas échéant
 *
 */
 
if (typeof pjSignataire !== 'undefined') {
	if(Value('id').of(pjSignataire).contains(idDeclarant)) {
		appendPj($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantSignataire, pjUser);
	}
	if(Value('id').of(pjSignataire).contains(idMandataire)) {
		appendPj($attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire, pjUser);
	}
}

/* ajout des autres PJ le cas échéant
 *
 */

if($attachmentPreprocess.attachmentPreprocess.pjAutres !== 'undefined') {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjAutres, pjUser);
}

/* ajout du Cerfa en tant que PJ (s'il a été généré)
 *
 */
 
if (typeof pjCerfa !== 'undefined') {
	data.push(spec.createData({
			id: 'record',
			label: refFormTitle,
			description: 'Voici le formulaire obtenu à partir des données saisies.',
			type: 'FileReadOnly',
			value: [pjCerfa] 
	})) ;
}

/*
 * Enregistrement du fichier (en mémoire)
 */ 

var metasToDelete = [];
var recordMetas = nash.record.meta() != null ? nash.record.meta().metas : null;
var recordMetasSize = recordMetas != null ? recordMetas.size() : 0;

for (var i = 0; i < recordMetasSize; i++) {
    if (recordMetas.get(i).name == 'document') {
        metasToDelete.push({'name':'document', 'value': recordMetas.get(i).value});
    }
}
nash.record.removeMeta(metasToDelete);
nash.record.meta(metas);

data.push(spec.createData({
    id : 'uploaded',
    label : 'Pièces jointes',
    description : 'Pièces jointes ajoutées au formulaire.',
    type : 'FileReadOnly',
    value : [ merge(pjUser).save('saisine.pdf') ]
	})) ;

var groups = [ spec.createGroup({
    id : 'attachments',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Saisine du Ministère de l\'intérieur',
    groups : groups
});

//--------------- MAIN GENERATION CERFA -------//
