//--------------- VARIABLES -------------------//

/* init datas pour review (variables génériques)
 *
 */
 
var data = [] ;
var pjUser = [];
var metas = [];
var repCerfa = 'models/';
var iCardinality = 0 ;

/* init datas pour cerfa (variables spécifiques)
 * contient a minnima les références des groupes 
 * génériques de data.xml
 */

/*
 * init pointeurs saisies, Pj...
 */
 
var datasPtr = $ds070cerfa13984 ;
var datasSignature = datasPtr.cadreSignatureGroupe ;
var datasIdentite = datasPtr.cadreIdentiteGroupe ;
var refFormTitle = 'Confiseur-chocolatier : Déclaration sanitaire' ;

if (typeof datasPtr.cadreDemandeGroupe !== 'undefined') {
    var datasDemande = datasPtr.cadreDemandeGroupe ;
} else {
	var datasDemande = null ;
}

/*
 * init pointeurs PJ déclarants ou mandataires
 */
 
var pjSignataire = datasSignature.cadreSignature.soussigne ;
var idDeclarant = 'signataireQualiteDeclarant' ;
var idMandataire = 'signataireQualiteMandataire' ;

/* import description du CERFA (si il existe)
 * il génère les tables de paramétrage formFields et equivCerfa
 * formFields = dictionnaire des valeurs des champs du Cerfa
 * equivCerfa = dictionnaire de description des champs du Cerfa
 * il définit les variables repCerfa et nameCerfa
 * nameCerfa = nom du fichier pdf associé au Cerfa
 */

/*
 * Remplissage des champs du Cerfa 13982 (dérogation agrément sanitaire)
 */

if (datasDemande !== null) {
	if ((Value('id').of(datasDemande.cadreDemande.nature).eq('depotDossier')) &&  (Value('id').of(datasDemande.cadreDemande.objet).eq(''))) {
		var flgCerfa = true;
	} else {
		var flgCerfa = false;
	}
} else {
	var flgCerfa = true;
}

if (flgCerfa === true) {

	//constantes et variables
	var formFields = {} ;
	var nameCerfa = 'cerfa_13984.pdf' ;
	//pointeurs pages de saisie
	var datasInfosDeclaration = datasPtr.infosDeclaration ; //page 1 - déclaration
	var datasInfosEtablissement = datasPtr.infosEtablissement ; //page 2 - établissement
	var datasInfosRestauration = datasPtr.infosRestauration ; //page 3A - restauration
	var datasInfosCommerce = datasPtr.infosCommerce ; //page 3B - commerce
	var datasInfosAbattage = datasPtr.infosAbattage ; //page 3C - abattage
	var datasInfosPeche = datasPtr.infosPeche ; //page 3D - peche
	var datasInfosTransport = datasPtr.infosTransport ; //page 3E - transport, entreposage
	var datasInfosCollecte = datasPtr.infosCollecte ; //page 3F - collecte
	var datasInfosGestion = datasPtr.infosGestion ; //page 3G - gestion administrative

	//paramétrages des cardinalités
	setSiretCommerces = "[ "
		+ "{ src: null, dst: 'abattageSiretCommerce[i]', valeur: supprimeEspaces(root), type: 'string' }"
		+ " ] ;" ;
						 
	//description des listes liées aux produits dans le Cerfa (type complex)

	//description des zones génériques du Cerfa (type bool, type string...)
	var equivCerfa = [ 
		//page 1
		{ src: datasInfosDeclaration.typeDeclaration, dst: 'premiereDeclaration', valeur: 'premiereDeclaration', type: 'radio' },
		{ src: datasInfosDeclaration.typeDeclaration, dst: 'actualisation', valeur: 'actualisation', type: 'radio' },
		//page 2
		{ src: null, dst: 'siretEtablissement', valeur: supprimeEspaces(datasInfosEtablissement.etablissement.siretEtablissement), type: 'string' },
		{ src: datasInfosEtablissement.etablissement.nomEtablissement, dst: 'nomEtablissement', valeur: null, type: 'string' },
		{ src: null, dst: 'adresseEtablissement', valeur: mergeAdresse(datasIdentite.cadreAdresse, 'V', false), type: 'string' },
		{ src: null, dst: 'adresseSuiteEtablissement', valeur: mergeAdresse(datasIdentite.cadreAdresse, 'S', false), type: 'string' },
		{ src: null, dst: 'codePostalEtablissement', valeur: datasIdentite.cadreAdresse.codePostal, type: 'string' },
		{ src: null, dst: 'communeEtablissement', valeur: extractInfoAdresse(datasIdentite.cadreAdresse, 'C', false), type: 'string' },
		{ src: datasInfosEtablissement.etablissement.adresseEtablissement, dst: 'adresseEtablissement', valeur: mergeAdresse(datasInfosEtablissement.etablissement.adresseEtablissement.cadreAdresse, 'V', false), type: 'xString' },
		{ src: datasInfosEtablissement.etablissement.adresseEtablissement, dst: 'adresseSuiteEtablissement', valeur: mergeAdresse(datasInfosEtablissement.etablissement.adresseEtablissement.cadreAdresse, 'S', false), type: 'xString' },
		{ src: datasInfosEtablissement.etablissement.adresseEtablissement, dst: 'codePostalEtablissement', valeur: datasInfosEtablissement.etablissement.adresseEtablissement.cadreAdresse.codePostal, type: 'xString' },
		{ src: datasInfosEtablissement.etablissement.adresseEtablissement, dst: 'communeEtablissement', valeur: extractInfoAdresse(datasInfosEtablissement.etablissement.adresseEtablissement.cadreAdresse, 'C', false), type: 'xString' },
		{ src: null, dst: 'adresseSite', valeur: mergeAdresse(datasIdentite.cadreAdresse, 'V', false), type: 'string' },
		{ src: null, dst: 'adresseSuiteSite', valeur: mergeAdresse(datasIdentite.cadreAdresse, 'S', false), type: 'string' },
		{ src: null, dst: 'codePostalSite', valeur: datasIdentite.cadreAdresse.codePostal, type: 'string' },
		{ src: null, dst: 'communeSite', valeur: extractInfoAdresse(datasIdentite.cadreAdresse, 'C', false), type: 'string' },
		{ src: datasInfosEtablissement.etablissement.adresseEtablissement, dst: 'adresseSite', valeur: mergeAdresse(datasInfosEtablissement.etablissement.adresseEtablissement.cadreAdresse, 'V', false), type: 'xString' },
		{ src: datasInfosEtablissement.etablissement.adresseEtablissement, dst: 'adresseSuiteSite', valeur: mergeAdresse(datasInfosEtablissement.etablissement.adresseEtablissement.cadreAdresse, 'S', false), type: 'xString' },
		{ src: datasInfosEtablissement.etablissement.adresseEtablissement, dst: 'codePostalSite', valeur: datasInfosEtablissement.etablissement.adresseEtablissement.cadreAdresse.codePostal, type: 'xString' },
		{ src: datasInfosEtablissement.etablissement.adresseEtablissement, dst: 'communeSite', valeur: extractInfoAdresse(datasInfosEtablissement.etablissement.adresseEtablissement.cadreAdresse, 'C', false), type: 'xString' },
		{ src: datasInfosEtablissement.etablissement.adresseSite, dst: 'adresseSite', valeur: mergeAdresse(datasInfosEtablissement.etablissement.adresseSite.cadreAdresse, 'V', false), type: 'xString' },
		{ src: datasInfosEtablissement.etablissement.adresseSite, dst: 'adresseSuiteSite', valeur: mergeAdresse(datasInfosEtablissement.etablissement.adresseSite.cadreAdresse, 'S', false), type: 'xString' },
		{ src: datasInfosEtablissement.etablissement.adresseSite, dst: 'codePostalSite', valeur: datasInfosEtablissement.etablissement.adresseSite.cadreAdresse.codePostal, type: 'xString' },
		{ src: datasInfosEtablissement.etablissement.adresseSite, dst: 'communeSite', valeur: extractInfoAdresse(datasInfosEtablissement.etablissement.adresseSite.cadreAdresse, 'C', false), type: 'xString' },
		{ src: datasIdentite.cadreIdentite.civilite, dst: 'civiliteMrResponsable', valeur: 'civiliteMasculin', type: 'radio' },
		{ src: datasIdentite.cadreIdentite.civilite, dst: 'civiliteMmeResponsable', valeur: 'civiliteFeminin', type: 'radio' },
		{ src: null, dst: 'nomResponsable', valeur: extractInfoIdentite(datasIdentite.cadreIdentite, 'X', true), type: 'string' },
    	{ src: datasInfosEtablissement.etablissement.responsableEtablissementDifferent, dst: 'civiliteMrResponsable', valeur: [datasInfosEtablissement.etablissement.responsableEtablissement.cadreIdentite.civilite, 'civiliteMasculin'], type: 'bRadio' },
    	{ src: datasInfosEtablissement.etablissement.responsableEtablissementDifferent, dst: 'civiliteMmeResponsable', valeur: [datasInfosEtablissement.etablissement.responsableEtablissement.cadreIdentite.civilite, 'civiliteFeminin'], type: 'bRadio' },
    	{ src: datasInfosEtablissement.etablissement.responsableEtablissementDifferent, dst: 'nomResponsable', valeur: extractInfoIdentite(datasInfosEtablissement.etablissement.responsableEtablissement.cadreIdentite, 'X', true), type: 'bString' },
		{ src: datasIdentite.cadreIdentite.civilite, dst: 'civiliteMrContact', valeur: 'civiliteMasculin', type: 'radio' },
		{ src: datasIdentite.cadreIdentite.civilite, dst: 'civiliteMmeContact', valeur: 'civiliteFeminin', type: 'radio' },
		{ src: null, dst: 'nomContact', valeur: extractInfoIdentite(datasIdentite.cadreIdentite, 'X', true), type: 'string' },
		{ src: null, dst: 'telephoneContact', valeur: mergeNumTelephone(datasIdentite.cadreContact ,true, true), type: 'string' },
		{ src: datasIdentite.cadreContact.courriel, dst: 'emailContact', valeur: null, type: 'string' },
		{ src: datasInfosDeclaration.contactFormaliteDifferent, dst: 'civiliteMrContact', valeur: [datasInfosDeclaration.contactFormalite.cadreIdentite.civilite, 'civiliteMasculin'], type: 'bradio' },
		{ src: datasInfosDeclaration.contactFormaliteDifferent, dst: 'civiliteMmeContact', valeur: [datasInfosDeclaration.contactFormalite.cadreIdentite.civilite, 'civiliteFeminin'], type: 'bradio' },
		{ src: datasInfosDeclaration.contactFormaliteDifferent, dst: 'nomContact', valeur: extractInfoIdentite(datasInfosDeclaration.contactFormalite.cadreIdentite, 'X', true), type: 'bstring' },
		{ src: datasInfosDeclaration.contactFormaliteDifferent, dst: 'telephoneContact', valeur: mergeNumTelephone(datasInfosDeclaration.contactFormalite.cadreContact ,true, true), type: 'bstring' },
		{ src: datasInfosDeclaration.contactFormaliteDifferent, dst: 'emailContact', valeur: datasInfosDeclaration.contactFormalite.cadreContact.courriel, type: 'bstring' },
		{ src: datasInfosDeclaration.fonctionContact, dst: 'fonctionContactResponsableQualite', valeur: 'responsableQualite', type: 'radio' },
		{ src: datasInfosDeclaration.fonctionContact, dst: 'fonctionContactResponsableSite', valeur: 'responsableSite', type: 'radio' },
		{ src: datasInfosDeclaration.fonctionContact, dst: 'fonctionContactResponsableCommercial', valeur: 'responsableCommercial', type: 'radio' },
		{ src: datasInfosDeclaration.fonctionContact, dst: 'fonctionContactResponsableJuridique', valeur: 'responsableJuridique', type: 'radio' },
		{ src: datasInfosDeclaration.fonctionContact, dst: 'fonctionContactGestionnairePersonnel', valeur: 'gestionnairePersonnel', type: 'radio' },
		{ src: datasInfosDeclaration.fonctionContact, dst: 'fonctionContactGestionnaireComptable', valeur: 'gestionnaireCompatble', type: 'radio' },
		{ src: datasInfosDeclaration.fonctionContact, dst: 'fonctionContactSalarie', valeur: 'salarie', type: 'radio' },
		{ src: datasInfosEtablissement.locaux.usageLocaux, dst: 'usageProfessionnelLocaux', valeur: 'usageProfessionnel', type: 'radio' },
		{ src: datasInfosEtablissement.locaux.usageLocaux, dst: 'usageMixteLocaux', valeur: 'usageMixte', type: 'radio' },
		{ src: datasInfosEtablissement.locaux.usageTiers, dst: 'usageTiersLocaux', valeur: true, type: 'bool' },
		{ src: datasInfosEtablissement.locaux.usageTiers, dst: 'siretTiersLocaux', valeur: supprimeEspaces(datasInfosEtablissement.locaux.siretTiers), type: 'bString' },
		{ src: datasInfosEtablissement.production.effectifMaxProduction, dst: 'effectifMaxProduction', valeur: null, type: 'string' },
		{ src: datasInfosEtablissement.production.effectifMoyenProduction, dst: 'effectifMoyenProduction', valeur: null, type: 'string' },
		//page 3A - restauration
		{ src: datasInfosRestauration.perimetre, dst: 'restaurationCommerciale', valeur: 'restaurationCommerciale', type: 'check' },
		{ src: datasInfosRestauration.perimetre, dst: 'restaurationCollective', valeur: 'restaurationCollective', type: 'check' },
		{ src: datasInfosRestauration.restaurationCommerciale.type, dst: 'restaurationCommercialeTraditionnelle', valeur: 'restaurationCommercialeTraditionnelle', type: 'check' },
		{ src: datasInfosRestauration.restaurationCommerciale.type, dst: 'restaurationCommercialeRapide', valeur: 'restaurationCommercialeRapide', type: 'check' },
		{ src: datasInfosRestauration.restaurationCommerciale.restaurationCommercialeNbPlacesAssises, dst: 'restaurationCommercialeNbPlacesAssises', valeur: null, type: 'string' },
		{ src: datasInfosRestauration.restaurationCommerciale.particularites, dst: 'restaurationCommercialeLivraison', valeur: 'restaurationCommercialeLivraison', type: 'check' },
		{ src: datasInfosRestauration.restaurationCommerciale.particularites, dst: 'restaurationCommercialeADistance', valeur: 'restaurationCommercialeADistance', type: 'check' },
		{ src: datasInfosRestauration.restaurationCommerciale.particularites, dst: 'restaurationCommercialeCamion', valeur: 'restaurationCommercialeCamion', type: 'check' },
		{ src: datasInfosRestauration.restaurationCommerciale.particularites, dst: 'restaurationCommercialeCaritatif', valeur: 'restaurationCommercialeCaritatif', type: 'check' },
		{ src: datasInfosRestauration.restaurationCommerciale.particularites, dst: 'restaurationCommercialeADomicile', valeur: 'restaurationCommercialeADomicile', type: 'check' },
		{ src: datasInfosRestauration.restaurationCommerciale.especes, dst: 'restaurationCommercialeGrenouilles', valeur: 'restaurationCommercialeGrenouilles', type: 'check' },
		{ src: datasInfosRestauration.restaurationCommerciale.especes, dst: 'restaurationCommercialeEscargots', valeur: 'restaurationCommercialeEscargots', type: 'check' },
		{ src: datasInfosRestauration.restaurationCollective.restaurationCollectiveNbRepas, dst: 'restaurationCollectiveNbRepas', valeur: null, type: 'string' },
		{ src: datasInfosRestauration.restaurationCollective.statut, dst: 'restaurationEtablissementProprietaire', valeur: 'restaurationEtablissementProprietaire', type: 'check' },
		{ src: datasInfosRestauration.restaurationCollective.statut, dst: 'restaurationEtablissementPrestataire', valeur: 'restaurationEtablissementPrestataire', type: 'check' },
		{ src: null, dst: 'restaurationSiretBeneficiaire', valeur: supprimeEspaces(datasInfosRestauration.restaurationCollective.restaurationSiretBeneficiaire), type: 'string' },
		{ src: null, dst: 'restaurationSiretProprietaire', valeur: supprimeEspaces(datasInfosRestauration.restaurationCollective.restaurationEtablissementProprietaire.restaurationSiretProprietaire), type: 'string' },
		{ src: datasInfosRestauration.restaurationCollective.restaurationEtablissementProprietaire.publics, dst: 'restaurationJeunesEnfants', valeur: 'restaurationJeunesEnfants', type: 'check' },
		{ src: datasInfosRestauration.restaurationCollective.restaurationEtablissementProprietaire.publics, dst: 'restaurationEnfants', valeur: 'restaurationEnfants', type: 'check' },
		{ src: datasInfosRestauration.restaurationCollective.restaurationEtablissementProprietaire.publics, dst: 'restaurationPersonnesHospitalisees', valeur: 'restaurationPersonnesHospitalisees', type: 'check' },
		{ src: datasInfosRestauration.restaurationCollective.restaurationEtablissementProprietaire.publics, dst: 'restaurationPersonnesAgees', valeur: 'restaurationPersonnesAgees', type: 'check' },
		{ src: datasInfosRestauration.restaurationCollective.restaurationEtablissementProprietaire.lieux, dst: 'restaurationCentreAere', valeur: 'restaurationCentreAere', type: 'check' },
		{ src: datasInfosRestauration.restaurationCollective.restaurationEtablissementProprietaire.lieux, dst: 'restaurationCreche', valeur: 'restaurationCreche', type: 'check' },
		{ src: datasInfosRestauration.restaurationCollective.restaurationEtablissementProprietaire.lieux, dst: 'restaurationCrecheParentale', valeur: 'restaurationCrecheParentale', type: 'check' },
		{ src: datasInfosRestauration.restaurationCollective.restaurationEtablissementProprietaire.lieux, dst: 'restaurationCentreVacancesPermanent', valeur: 'restaurationCentreVacancesPermanent', type: 'check' },
		{ src: datasInfosRestauration.restaurationCollective.restaurationEtablissementProprietaire.lieux, dst: 'restaurationCentreVacancesOccasionnel', valeur: 'restaurationCentreVacancesOccasionnel', type: 'check' },
		{ src: datasInfosRestauration.restaurationCollective.restaurationEtablissementProprietaire.lieux, dst: 'restaurationPortage', valeur: 'restaurationPortage', type: 'check' },
		{ src: datasInfosRestauration.restaurationCollective.restaurationEtablissementProprietaire.lieux, dst: 'restaurationPrison', valeur: 'restaurationPrison', type: 'check' },
		{ src: datasInfosRestauration.restaurationCollective.restaurationEtablissementProprietaire.lieux, dst: 'restaurationEhpad', valeur: 'restaurationEhpad', type: 'check' },
		{ src: datasInfosRestauration.restaurationCollective.restaurationEtablissementProprietaire.lieux, dst: 'restaurationEcole', valeur: 'restaurationEcole', type: 'check' },
		{ src: datasInfosRestauration.restaurationCollective.restaurationEtablissementProprietaire.lieux, dst: 'restaurationRestaurantEntreprise', valeur: 'restaurationRestaurantEntreprise', type: 'check' },
		{ src: datasInfosRestauration.restaurationCollective.restaurationEtablissementProprietaire.lieux, dst: 'restaurationCuisinePedagogique', valeur: 'restaurationCuisinePedagogique', type: 'check' },
		{ src: datasInfosRestauration.restaurationCollective.restaurationEtablissementProprietaire.fonctionnement, dst: 'restaurationCuisineCentrale', valeur: 'restaurationCuisineCentrale', type: 'check' },
		{ src: datasInfosRestauration.restaurationCollective.restaurationEtablissementProprietaire.fonctionnement, dst: 'restaurationCuisineSurPlace', valeur: 'restaurationCuisineSurPlace', type: 'check' },
		{ src: datasInfosRestauration.restaurationCollective.restaurationEtablissementProprietaire.fonctionnement, dst: 'restaurationCuisineSatellite', valeur: 'restaurationCuisineSatellite', type: 'check' },
		{ src: datasInfosRestauration.restaurationCollective.restaurationEtablissementProprietaire.restaurationNbCuisinesSatellites, dst: 'restaurationNbCuisinesSatellites', valeur: null, type: 'string' },
		{ src: null, dst: 'restaurationSiretCuisineSatellite', valeur: supprimeEspaces(datasInfosRestauration.restaurationCollective.restaurationEtablissementProprietaire.restaurationSiretCuisineSatellite), type: 'string' },
		{ src: datasInfosRestauration.restaurationCollective.restaurationEtablissementProprietaire.liaisons, dst: 'restaurationLiaisonChaude', valeur: 'restaurationLiaisonChaude', type: 'check' },
		{ src: datasInfosRestauration.restaurationCollective.restaurationEtablissementProprietaire.liaisons, dst: 'restaurationLiaisonFroide', valeur: 'restaurationLiaisonFroide', type: 'check' },		
		//page 3B - commerce
		{ src: datasInfosCommerce.perimetre, dst: 'commerceProducteurFermier', valeur: 'commerceProducteurFermier', type: 'check' },
		{ src: datasInfosCommerce.perimetre, dst: 'commerceMetierDeBouche', valeur: 'commerceMetierDeBouche', type: 'check' },
		{ src: datasInfosCommerce.perimetre, dst: 'commerceAlimentaire', valeur: 'commerceAlimentaire', type: 'check' },
		{ src: datasInfosCommerce.lieux.typeVente, dst: 'commerceVente', valeur: 'commerceVente', type: 'check' },
		{ src: datasInfosCommerce.lieux.typeVente, dst: 'commerceVenteADistance', valeur: 'commerceVenteADistance', type: 'check' },
		{ src: datasInfosCommerce.lieux.commerceProducteurFermierPointDeVente, dst: 'commerceProducteurFermierPointDeVente', valeur: true, type: 'bool' },
		{ src: null, dst: 'commerceProducteurFermierSiret', valeur: supprimeEspaces(datasInfosCommerce.lieux.commerceProducteurFermierSiret), type: 'string' },
		{ src: datasInfosCommerce.lieux.commerceAtelierCollectif, dst: 'commerceAtelierCollectif', valeur: true, type: 'bool' },
		{ src: datasInfosCommerce.lieux.lieuxCommerce, dst: 'commerceAlimentaireEpicerie', valeur: 'commerceAlimentaireEpicerie', type: 'check' },
		{ src: datasInfosCommerce.lieux.lieuxCommerce, dst: 'commerceAlimentaireGrandeSurface', valeur: 'commerceAlimentaireGrandeSurface', type: 'check' },
		{ src: datasInfosCommerce.lieux.lieuxCommerce, dst: 'commerceAlimentaireMagasinSurgele', valeur: 'commerceAlimentaireMagasinSurgele', type: 'check' },
		{ src: datasInfosCommerce.lieux.lieuxSpecifiques, dst: 'commerceDistributionAutomatique', valeur: 'commerceDistributionAutomatique', type: 'check' },
		{ src: datasInfosCommerce.lieux.lieuxSpecifiques, dst: 'commerceCamion', valeur: 'commerceCamion', type: 'check' },
		{ src: datasInfosCommerce.lieux.lieuxSpecifiques, dst: 'commerceEtalsMarches', valeur: 'commerceEtalsMarches', type: 'check' },
		{ src: datasInfosCommerce.lieux.commerceMarchesListe, dst: 'commerceMarchesListe', valeur: null, type: 'string' },
		{ src: datasInfosCommerce.denrees.commercePreparation, dst: 'commercePreparation', valeur: true, type: 'bool' },
		{ src: datasInfosCommerce.denrees.type, dst: 'commerceTransformationViande', valeur: 'commerceTransformationViande', type: 'check' },
		{ src: datasInfosCommerce.denrees.type, dst: 'commerceDecoupeViande', valeur: 'commerceDecoupeViande', type: 'check' },
		{ src: datasInfosCommerce.denrees.type, dst: 'commerceProduitsLaitiers', valeur: 'commerceProduitsLaitiers', type: 'check' },
		{ src: datasInfosCommerce.denrees.type, dst: 'commerceLaitCru', valeur: 'commerceLaitCru', type: 'check' },
		{ src: datasInfosCommerce.denrees.type, dst: 'commerceAutresProduitsLaitiers', valeur: 'commerceAutresProduitsLaitiers', type: 'check' },
		{ src: datasInfosCommerce.denrees.type, dst: 'commerceGlaces', valeur: 'commerceGlaces', type: 'check' },
		{ src: datasInfosCommerce.denrees.type, dst: 'commerceChocolats', valeur: 'commerceChocolats', type: 'check' },
		{ src: datasInfosCommerce.denrees.type, dst: 'commerceMiel', valeur: 'commerceMiel', type: 'check' },
		{ src: datasInfosCommerce.denrees.type, dst: 'commercePainPatisserie', valeur: 'commercePainPatisserie', type: 'check' },
		{ src: datasInfosCommerce.denrees.type, dst: 'commercePlatsCuisines', valeur: 'commercePlatsCuisines', type: 'check' },
		{ src: datasInfosCommerce.denrees.type, dst: 'commercePoissons', valeur: 'commercePoissons', type: 'check' },
		{ src: datasInfosCommerce.denrees.type, dst: 'commerceOeufsFrais', valeur: 'commerceOeufsFrais', type: 'check' },
		{ src: datasInfosCommerce.denrees.type, dst: 'commerceEscargotsCuisines', valeur: 'commerceEscargotsCuisines', type: 'check' },
		{ src: datasInfosCommerce.denrees.type, dst: 'commerceGrenouillesCuisines', valeur: 'commerceGrenouillesCuisines', type: 'check' },
		{ src: datasInfosCommerce.denrees.preparationDenreesMixtes, dst: 'preparationDenreesMixtes', valeur: true, type: 'bool' },
		{ src: datasInfosCommerce.denrees.commerceProduitsLaitiersAutres, dst: 'commerceProduitsLaitiersAutres', valeur: null, type: 'string' },
		{ src: datasInfosCommerce.denrees.commerceVolumeProduitsCarnes, dst: 'commerceVolumeProduitsCarnes', valeur: null, type: 'string' },
		{ src: datasInfosCommerce.denrees.commerceVolumeViande, dst: 'commerceVolumeViande', valeur: null, type: 'string' },
		{ src: datasInfosCommerce.denrees.commerceVolumeVolailles, dst: 'commerceVolumeVolailles', valeur: null, type: 'string' },
		{ src: datasInfosCommerce.denrees.commerceVolumeLait, dst: 'commerceVolumeLait', valeur: null, type: 'string' },
		{ src: datasInfosCommerce.denrees.commerceVolumeOvoproduits, dst: 'commerceVolumeOvoproduits', valeur: null, type: 'string' },
		{ src: datasInfosCommerce.denrees.commerceVolumePeche, dst: 'commerceVolumePeche', valeur: null, type: 'string' },
		{ src: datasInfosCommerce.denrees.commerceVolumeEscargots, dst: 'commerceVolumeEscargots', valeur: null, type: 'string' },
		{ src: datasInfosCommerce.denrees.commerceVolumeGrenouilles, dst: 'commerceVolumeGrenouilles', valeur: null, type: 'string' },
		{ src: datasInfosCommerce.denrees.commerceEscargots, dst: 'commerceEscargots', valeur: true, type: 'bool' },
		{ src: datasInfosCommerce.denrees.commerceGrenouilles, dst: 'commerceGrenouilles', valeur: true, type: 'bool' },
		//page 3C - abattage
		{ src: datasInfosAbattage.abattageNonAgree, dst: 'abattageNonAgree', valeur: true, type: 'bool' },
		{ src: datasInfosAbattage.especesConcernees, dst: 'abattagePoulet', valeur: 'poulets', type: 'check' },
		{ src: datasInfosAbattage.especesConcernees, dst: 'abattagePintade', valeur: 'pintades', type: 'check' },
		{ src: datasInfosAbattage.especesConcernees, dst: 'abattageFaisan', valeur: 'faisans', type: 'check' },
		{ src: datasInfosAbattage.especesConcernees, dst: 'abattageCanard', valeur: 'canards', type: 'check' },
		{ src: datasInfosAbattage.especesConcernees, dst: 'abattageOie', valeur: 'oies', type: 'check' },
		{ src: datasInfosAbattage.especesConcernees, dst: 'abattageDinde', valeur: 'dindes', type: 'check' },
		{ src: datasInfosAbattage.especesConcernees, dst: 'abattagePerdrix', valeur: 'perdrix', type: 'check' },
		{ src: datasInfosAbattage.especesConcernees, dst: 'abattagePigeon', valeur: 'pigeons', type: 'check' },
		{ src: datasInfosAbattage.especesConcernees, dst: 'abattageCaille', valeur: 'cailles', type: 'check' },
		{ src: datasInfosAbattage.especesConcernees, dst: 'abattageLievre', valeur: 'lievres', type: 'check' },
		{ src: datasInfosAbattage.especesConcernees, dst: 'abattageLapin', valeur: 'lapins', type: 'check' },
		{ src: datasInfosAbattage.especesConcernees, dst: 'abattageAutres', valeur: 'autres', type: 'check' },
		{ src: datasInfosAbattage.autresEspeces, dst: 'abattageAutresListe', valeur: null, type: 'string' },
		{ src: datasInfosAbattage.equivalentPoulets, dst: 'abattageEquivalentsPoulets', valeur: null, type: 'string' },
		{ src: datasInfosAbattage.autresTravaux.complementaires, dst: 'abattageDecoupe', valeur: 'decoupe', type: 'check' },
		{ src: datasInfosAbattage.autresTravaux.complementaires, dst: 'abattageTransformation', valeur: 'transformation', type: 'check' },
		{ src: datasInfosAbattage.autresTravaux.cadreCommerces.siretCommerce, dst: 'datasInfosAbattage.autresTravaux.cadreCommerces.siretCommerce', valeur: setSiretCommerces, type: 'cardinality' },
		//page 3D - pêche
		{ src: datasInfosDeclaration.activitesConcernees, dst: 'peche', valeur: 'peche', type: 'check' },
		{ src: datasInfosPeche.lieuPeche, dst: 'pecheMer', valeur: 'mer', type: 'check' },
		{ src: datasInfosPeche.lieuPeche, dst: 'pecheEauDouce', valeur: 'eauDouce', type: 'check' },
		{ src: datasInfosPeche.typePeche, dst: 'pecheCoquillage', valeur: 'coquillage', type: 'check' },
		{ src: datasInfosPeche.typePeche, dst: 'pechePoisson', valeur: 'poisson', type: 'check' },
		{ src: datasInfosPeche.nbNavires, dst: 'pecheNbNavires', valeur: null, type: 'string' },
		//page 3E - transport, entreposage
		{ src: datasInfosTransport.typeActivites, dst: 'transport', valeur: 'transport', type: 'check' },
		{ src: datasInfosTransport.transport.typeTransport, dst: 'transportFroidPositif', valeur: 'froidPositif', type: 'check' },
		{ src: datasInfosTransport.transport.typeTransport, dst: 'transportFroidNegatif', valeur: 'froidNegatif', type: 'check' },
		{ src: datasInfosTransport.transport.typeTransport, dst: 'transportLiaisonChaude', valeur: 'liaisonChaude', type: 'check' },
		{ src: datasInfosTransport.nbEngins, dst: 'transportNbEngins', valeur: null, type: 'string' },
		{ src: datasInfosTransport.typeActivites, dst: 'entreposage', valeur: 'entreposage', type: 'check' },
		{ src: datasInfosTransport.entreposage.typeEntreposage, dst: 'entreposageTemperatureAmbiante', valeur: 'temperatureAmbiante', type: 'check' },
		{ src: datasInfosTransport.entreposage.typeEntreposage, dst: 'entreposageTemperatureSpecifique', valeur: 'temperatureSpecifique', type: 'check' },
		{ src: datasInfosTransport.typeActivites, dst: 'entreposagePeche', valeur: 'debarquePeche', type: 'check' },
		{ src: datasInfosTransport.typeActivites, dst: 'entreposageEclatementPeche', valeur: 'eclatementPeche', type: 'check' },
		//page 3F - collecte
		{ src: datasInfosCollecte.produitsConcernes, dst: 'collecteOeufs', valeur: 'oeufs', type: 'check' },
		{ src: datasInfosCollecte.produitsConcernes, dst: 'collecteGibierSauvage', valeur: 'gibierSauvage', type: 'check' },
		{ src: datasInfosCollecte.produitsConcernes, dst: 'collecteMiel', valeur: 'miel', type: 'check' },
		//page 3G - gestion administrative
		{ src: datasInfosDeclaration.activitesConcernees, dst: 'negoce', valeur: 'negoce', type: 'check' },
		{ src: datasInfosDeclaration.activitesConcernees, dst: 'siegesSociaux', valeur: 'gestion', type: 'check' },
		{ src: datasInfosGestion.perimetre, dst: 'siegesSociauxRemiseDirecte', valeur: 'remiseDirecte', type: 'check' },	
		{ src: datasInfosGestion.perimetre, dst: 'siegesSociauxRestaurationCollective', valeur: 'restaurationCollective', type: 'check' },
		{ src: datasInfosGestion.perimetre, dst: 'siegesSociauxTransformation', valeur: 'transformation', type: 'check' },
		//bloc signature
		{ src: null, dst: 'engagementPouvoir', valeur: true, type: 'bool' },	
		{ src: null, dst: 'engagementExactitude', valeur: true, type: 'bool' },	
		{ src: null, dst: 'engagementPlan', valeur: true, type: 'bool' },	
		{ src: null, dst: 'engagementActualisation', valeur: true, type: 'bool' },	
		{ src: null, dst: 'signatureNom', valeur: genereInfoSignataire(false, true), type: 'string' },	
		{ src: null, dst: 'signatureDate', valeur: formatDate(datasSignature.cadreSignature.signatureDate, 'T', true), type: 'string' },
		{ src: null, dst: 'signature', valeur: genereInfoSignataire(true, true), type: 'string' }		
	] ; 	
}
			
			

//--------------- VARIABLES -------------------//

//--------------- FONCTIONS -------------------//

/* fonctions génériques de formatage des données
 *
 */

/*
 * calculs mathématiques : calcul de pourcentage
 */
 
function calculPercent(valNumerateur, valDenominateur, nbDecimales) {
	var valResult = '' ;
	var tmpValeur = 0 ;
	while (true) {
		if (typeof valNumerateur === 'undefined' || typeof valNumerateur !== 'number') {
			break ;
		}
		if (typeof valDenominateur === 'undefined' || typeof valDenominateur !== 'number') {
			break;
		}
		if (typeof nbDecimales === 'undefined' || typeof nbDecimales !== 'number') {
			tmpValeur = 0 ;
		} else {
			tmpValeur = nbDecimales >= 0 ? nbDecimales : 0 ; 
		}	
		tmpValeur = Math.pow(10, tmpValeur);
		if (valDenominateur !== 0) {
			tmpValeur = Math.round(((valNumerateur / valDenominateur) * 100) / tmpValeur) ;
			valResult = tmpValeur.toString();
			
		}
		break ;
	}
	return valResult ;
 }

/*
 * calculs mathématiques : calcul de soustraction
 */
 
function calculSubstraction(valTotal, tabValeurs, cumulValeur, keyCumul) {
	var valResult = '' ;
	var tmpValeur = 0 ;
	while (true) {
		if (typeof valTotal === 'undefined' || typeof valTotal !== 'number') {
			break ;
		}
		tmpValeur = valTotal ;
		if (typeof tabValeurs === 'object' && Array.isArray(tabValeurs)) {
			for (tmpIndex = 0; tmpIndex < tabValeurs.length; tmpIndex++) {
				if (typeof tabValeurs[tmpIndex] === 'number') {
					tmpValeur -= tabValeurs[tmpIndex];
					tmpFlag = true ;
				}
			}	
		}
		if (tmpFlag === true) {
			valResult = tmpValeur.toString();
			if (typeof cumulValeur === 'object' && cumulValeur !== null && typeof keyCumul === 'string') {
				if (typeof cumulValeur[keyCumul] !== 'undefined' && isNaN(cumulValeur[keyCumul]) === false) {
					cumulValeur[keyCumul] = (Number(cumulValeur[keyCumul]) - tmpValeur).toString() ;
				} else {
					cumulValeur[keyCumul] = valResult ;
				}
			}
		}
		break;
	}
	return valResult ;
 }

function calculAddition(tabValeurs, cumulValeur, keyCumul) {
	var valResult = '' ;
	var tmpFlag = false ;
	var tmpValeur = 0 ;
	var tmpIndex = 0 ;
	while (true) {
		if (typeof tabValeurs === 'object' && Array.isArray(tabValeurs)) {
			for (tmpIndex = 0; tmpIndex < tabValeurs.length; tmpIndex++) {
				if (typeof tabValeurs[tmpIndex] === 'number') {
					tmpValeur += tabValeurs[tmpIndex];
					tmpFlag = true ;
				}
			}	
		}
		if (tmpFlag === true) {
			valResult = tmpValeur.toString();
			if (typeof cumulValeur === 'object' && cumulValeur !== null && typeof keyCumul === 'string') {
				if (typeof cumulValeur[keyCumul] !== 'undefined' && isNaN(cumulValeur[keyCumul]) === false) {
					cumulValeur[keyCumul] = (tmpValeur + Number(cumulValeur[keyCumul])).toString() ;
				} else {
					cumulValeur[keyCumul] = valResult ;
				}
			}
		}
		break ;
	}
	return valResult ;
 }

/* suppression des espaces dans une chaine
 *
 */

function supprimeEspaces(szLibel) {
	var tmpRegex = /\s/g ;
	var tmpLibel = '';
	if (typeof szLibel !== 'undefined' && typeof szLibel === 'string') {
		tmpLibel = szLibel !== null ? szLibel.replace(tmpRegex, '') : '' ;	
	}
	return tmpLibel ;
}
 
/* merge des prénoms (séparateur ',')
 *
 */
	
function mergePrenoms(listPrenoms) {
	var prenoms = [];
	if (typeof listPrenoms !== 'undefined' && typeof listPrenoms === 'object') {
		for (i = 0; i < listPrenoms.length; i++) {
			prenoms.push(listPrenoms[i]);
    	}
	} else {
		prenoms.push('');
	}
	return prenoms.toString();
}

/* merge du nom et des prénoms (séparateur ',')
 *
 */

function mergeNomPrenoms(szNom, listPrenoms) {
	var result = '';
	if (typeof szNom === 'string') {
		result = szNom + ' ';
		result += mergePrenoms(listPrenoms);
	} 
	return result;
}

/* construction de la date et du lieu de naissance
 *
 */

function mergeNaissance(blocNaissance) {
	var naissance = '' ;
	if (typeof blocNaissance !== 'undefined' && typeof blocNaissance === 'object') {
		naissance = blocNaissance.lieuNaissance ? blocNaissance.lieuNaissance + ' / ' : '' ;
		naissance += blocNaissance.paysNaissance ? blocNaissance.paysNaissance : '' ;
	}
	return naissance ;
}
	
/* construction de l'adresse en tenant compte du pays ou non
 *
 */

function mergeAdresse(blocAdresse, typeMerge, multiLine) {
	var tmpVar = '' ;
	var adresse = '' ;
	var chgLigne = ' \n' ;
	if (typeof blocAdresse !== 'undefined' && typeof blocAdresse === 'object') {
		var fmtLigne = (multiLine === true ? chgLigne : ' ');
		var typeAct = (typeof typeMerge !== 'undefined' && typeof typeMerge === 'string') ? typeMerge.toUpperCase() : 'T' ;
		switch (typeAct) {
			case 'N':
				adresse += blocAdresse.numeroVoie ? blocAdresse.numeroVoie + ' ' : '' ;
				adresse += blocAdresse.indiceVoie ? blocAdresse.indiceVoie : '' ;
				break;
			case 'V':	
				tmpVar = mergeAdresse(blocAdresse, 'N', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + ' ' : '' ;
				adresse += mergeAdresse(blocAdresse, 'W', multiLine) ;
				break;
			case 'W':
				adresse += blocAdresse.typeVoie ? blocAdresse.typeVoie + ' ' : '' ;
				adresse += blocAdresse.nomVoie ?  blocAdresse.nomVoie : '' ;
				break;
			case 'S':
				adresse += blocAdresse.complementAdresse ? blocAdresse.complementAdresse + fmtLigne : '' ;
				adresse += blocAdresse.distributionSpeciale ? blocAdresse.distributionSpeciale : '' ;
				break;
			case 'C':
				if (typeof blocAdresse.pays !== 'undefined') {
					if (typeof blocAdresse.codePostal !== 'undefined' && blocAdresse.codePostal) {
						adresse += blocAdresse.codePostal ? blocAdresse.codePostal + ' ' : '' ;
						adresse += blocAdresse.commune ? blocAdresse.commune : '' ;
					} else {
						adresse += blocAdresse.ville ? blocAdresse.ville + fmtLigne : '' ;
						adresse += blocAdresse.pays ? blocAdresse.pays : '' ;
					}
				} else {
					adresse += blocAdresse.codePostal ? blocAdresse.codePostal + ' ' : '' ;
					adresse += blocAdresse.commune ? blocAdresse.commune : '' ;
				}
				break;
			case 'X':
				tmpVar = mergeAdresse(blocAdresse, 'S', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + fmtLigne : '' ;
				adresse += mergeAdresse(blocAdresse, 'C', multiLine) ;
				break;
			case 'Z':
				tmpVar = mergeAdresse(blocAdresse, 'V', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + fmtLigne : '' ;
				adresse += mergeAdresse(blocAdresse, 'S', multiLine) ;
				break;
			case 'T':
			default:
				tmpVar = mergeAdresse(blocAdresse, 'V', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + fmtLigne : '' ;
				tmpVar = mergeAdresse(blocAdresse, 'S', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + fmtLigne : '' ;
				adresse += mergeAdresse(blocAdresse, 'C', multiLine) ;
		}
	}
	return adresse;
}

/*
 * mise en forme numéro de téléphone au format 10 chiffres
 */

function formatNumTelephone(objNumTel) {
	var tmpRegexTelIntro = /^(\+\d{1,3}[\s\.\-]?)(.*)$/ ;
	var tmpRegexTel = /(\d{1,})[\s\.\-]?/g ;
	var tmpTel = '' ;
	var tmpIntro = '';
	var tmpTelFmt = '';
	if (typeof objNumTel !== 'undefined' && typeof objNumTel === 'object' && objNumTel) {
		tmpTel = objNumTel.international ;
		if (tmpRegexTelIntro.test(tmpTel) === true) {
			tmpIntro = tmpTel.replace(tmpRegexTelIntro, '$1') ;
			tmpTelFmt = '$2' ;
			if (tmpIntro.substring(0,3) == '+33') {
				tmpTelFmt = '0' + tmpTelFmt ;
			}	
			tmpTel = tmpTel.replace(tmpRegexTelIntro, tmpTelFmt) ;
			tmpTel = tmpTel.replace(tmpRegexTel, '$1') ;
		}
	}
	return tmpTel ;
}

/*
 * infos contacts téléphoniques
 */
 
function mergeNumTelephone(blocNumTel, flgMerge, flgInternational) {
	var numTel = '';
	var tmpTel = '';
	if (typeof blocNumTel !== 'undefined' && typeof blocNumTel === 'object') {
		if (typeof blocNumTel.telephoneFixe.telephoneNum !== 'undefined' && blocNumTel.telephoneFixe.telephoneNum ) {
			if (flgInternational === false) {
				tmpTel = formatNumTelephone(blocNumTel.telephoneFixe.telephoneNum) ;
			} else {
				tmpTel = blocNumTel.telephoneFixe.telephoneNum.international ;
			}
			numTel += tmpTel ;
		}
		if (typeof blocNumTel.telephoneMobile.telephoneNum !== 'undefined' && blocNumTel.telephoneMobile.telephoneNum) {
			if (numTel.length > 0 && flgMerge === true) {
				numTel += ' / ';
			}
			if (flgMerge === true || numTel.length === 0) {
				if (flgInternational === false) {
					tmpTel = formatNumTelephone(blocNumTel.telephoneMobile.telephoneNum) ;
				} else {
					tmpTel = blocNumTel.telephoneMobile.telephoneNum.international;
				}
				numTel += tmpTel ;
			}
		}
	}
	return numTel;
}

/* formatage d'une date avec extaction ou non d'une partie (J, M, A)
 *
 */

function formatDate(valDate, typeExtract, fullYear) {
	var dateResult = '';
	var tmpYear = '' ;
	if (typeof valDate === 'object' && valDate) {
    	var dateTmp = new Date(parseInt(valDate.getTimeInMillis())) ; ;
		var dateIso = ((dateTmp.toLocaleDateString()).substr(0, 10)).split('-') ;
		var typeAct = (typeof typeExtract !== 'undefined' && typeof typeExtract === 'string') ? typeExtract.toUpperCase() : 'T' ;
		if (dateIso.length > 1) {
			tmpYear = (fullYear === true ? dateIso[0] : dateIso[0].substr(2)) ;
			switch (typeAct) {
				case 'D':
				case 'J':
					dateResult = dateIso[2] ;
					break;
				case 'M':
					dateResult = dateIso[1] ;
					break;
				case 'A':
				case 'Y':
					dateResult = tmpYear ;
					break;
				case 'T':
				default:
					dateResult = dateIso[2] + '/' + dateIso[1] + '/' + tmpYear ;
			}
		} else {
			dateResult = dateIso[0] ;
		}
	}
	return dateResult ;
}

/*
 * infos signataire
 */

function genereInfoSignataire(flgCompleteInfos, flgMergePrenoms) {
	var contactInfos = [] ;
	var infoSignataire = '' ;
	var chgLigne = ' \n' ;
	if (typeof datasSignature !== 'undefined' && typeof datasIdentite !== 'undefined' && typeof idMandataire !== 'undefined') {
		if (flgCompleteInfos === true) {
			infoSignataire = 'Cette déclaration respecte les attendus de l\’article 1367 du code civil.' + chgLigne ;
			infoSignataire += datasSignature.cadreSignature.soussigne + ' ';
		}
		if (Value('id').of(datasSignature.cadreSignature.soussigne).eq(idMandataire)) {
			infoSignataire += datasSignature.cadreSignature.informationsMandataire.nomPrenomDenomination ;
			if (flgCompleteInfos === true) {
				infoSignataire += chgLigne ;
				infoSignataire += mergeAdresse(datasSignature.cadreSignature.informationsMandataire.cadreAdresse, 'T', true) ;
			}
		} else {
			infoSignataire += (datasIdentite.cadreIdentite.nomUsage ? datasIdentite.cadreIdentite.nomUsage + ' ' : datasIdentite.cadreIdentite.nomNaissance) + ' ' ;
			if (flgMergePrenoms === true) {
				infoSignataire += mergePrenoms(datasIdentite.cadreIdentite.prenoms) ;
			}
			if (flgCompleteInfos === true) {
				infoSignataire += chgLigne ;
				if (typeof datasIdentite.cadreContact.telephoneFixe.telephoneNum !== 'undefined' && datasIdentite.cadreContact.telephoneFixe.telephoneNum) {
					contactInfos.push(datasIdentite.cadreContact.telephoneFixe.telephoneNum) ;
				}
				if (typeof datasIdentite.cadreContact.telephoneMobile.telephoneNum !== 'undefined' && datasIdentite.cadreContact.telephoneMobile.telephoneNum) {
					contactInfos.push(datasIdentite.cadreContact.telephoneMobile.telephoneNum);
				}
				if (typeof datasIdentite.cadreContact.courriel !== 'undefined' && datasIdentite.cadreContact.courriel) {
					contactInfos.push(datasIdentite.cadreContact.courriel);
				}
				infoSignataire += contactInfos.toString() ;
			}
		}
	}
	return infoSignataire ;
}

/*
 * extraction d'informations liées à l'identité (nom, prénoms...)
 */

function extractInfoIdentite(blocIdentite, typeExtract, flgExtract) {
	var infoIdentite = '';
	if (typeof blocIdentite !== 'undefined' && typeof blocIdentite === 'object') {
		var typeExt = (typeExtract !== 'undefined' && typeof typeExtract === 'string') ? typeExtract.toUpperCase() : 'T' ;
		switch (typeExt) {
			case 'N':
			case 'L':
				infoIdentite = blocIdentite.nomUsage ? blocIdentite.nomUsage : (blocIdentite.nomNaissance ? blocIdentite.nomNaissance : '') ;
				break;
			case 'P':
			case 'F':
				infoIdentite = flgExtract ? mergePrenoms(blocIdentite.prenoms) : (blocIdentite.prenoms.length > 0 ? blocIdentite.prenoms[0] : '') ;
				break ;
			case 'C':
				infoIdentite = blocIdentite.civilite ? blocIdentite.civilite : '' ;
				break
			case 'X':
				infoIdentite += extractInfoIdentite(blocIdentite, 'N', flgExtract) ;
				infoIdentite += ' ' ;
				infoIdentite += extractInfoIdentite(blocIdentite, 'P', flgExtract) ;
				break;
			case 'T':
				infoIdentite  = extractInfoIdentite(blocIdentite, 'C', flgExtract) ;
				infoIdentite += ' ' ;
				infoIdentite += extractInfoIdentite(blocIdentite, 'N', flgExtract) ;
				infoIdentite += ' ' ;
				infoIdentite += extractInfoIdentite(blocIdentite, 'P', flgExtract) ;
				break;
			default:
				infoIdentite = '';
		}
	}
	return infoIdentite ;
}

/*
 * extraction d'informations liées aux adresses
 */
 
function extractInfoAdresse(blocAdresse, typeExtract, flgExtract) {
	var tmpDept = /(0[1-9]|[1-8][0-9]|2[A-B]|9[0-5]|9[7-8][0-9])(\d{2,3})/ ;
	var adresse = '' ;
	if (typeof blocAdresse !== 'undefined' && typeof blocAdresse === 'object') {
		var typeAct = (typeof typeExtract !== 'undefined' && typeof typeExtract === 'string') ? typeExtract.toUpperCase() : 'T' ;
		switch (typeAct) {
			case 'D':
				adresse = blocAdresse.commune ? blocAdresse.commune.id : '' ;
				adresse = adresse.replace(tmpDept, '$1') ;
				break;
			case 'P':
				if (typeof blocAdresse.ville !== 'undefined' && blocAdresse.ville) {
					adresse = blocAdresse.pays ;
				} else {
					if (flgExtract === true) {
						adresse = blocAdresse.pays ;
					}
				}
				break;
			case 'C':
				if (typeof blocAdresse.ville !== 'undefined' && blocAdresse.ville) {
					adresse = blocAdresse.ville ;
				} else {
					adresse = blocAdresse.commune ;
				}
				break;
			default:
		}
	}
	return adresse;
}

/*
 * extraction indicatif téléphone si indicatif non français (+33)
 */

function extractIndicatifEtrangerTelephone(objNumTel) {
	var tmpRegexTelIntro = /^(\+\d{1,3}[\s\.\-]?)(.*)$/ ;
	var tmpTel = '' ;
	var tmpIntro = '';
	if (typeof objNumTel !== 'undefined' && typeof objNumTel === 'object' && objNumTel) {
		if (tmpRegexTelIntro.test(objNumTel.international) === true) {
			tmpIntro = objNumTel.international.replace(tmpRegexTelIntro, '$1').trim() ;
			if (tmpIntro !== '+33') {
				tmpTel = tmpIntro ;
			}
		}
	}
	return tmpTel ;
}

/* merge de PJs
 * 
 */

function merge(pjs) {
	var mergedFile = nash.doc.create();
	pjs.forEach(function (elm) {		
		mergedFile.append(elm).save('item.pdf');
    });
	return mergedFile;
}

/* ajout de PJ
 * 
 */

function appendPj(fld, listPj) {
	fld.forEach(function (elm) {
        listPj.push(elm);
		_log.info('elm.getAbsolutePath() = >'+elm.getAbsolutePath());
		metas.push({'name':'userAttachments', 'value': '/'+elm.getAbsolutePath()});
		metas.push({'name':'document', 'value': '/'+elm.getAbsolutePath()});
    });
}

/* remplissage d'un cerfa à partir d'une table de paramétrage
 *
*/

function genereDynamicParams(tabParam, baseAddress) {
	var tmpTab = tabParam.replace(/root/gi, baseAddress);
	var tmpObj = [] ;
	try {
		tmpObj = eval(tmpTab) ;
	} catch(error) {
		tmpObj = [];
	}
  	return tmpObj;
}

function genereXDynamicParams(tabParam, baseAddress) {
	var tmpTab = '';
	var tmpVal ;
	var tmpFlg = false ;
	var tmpInd = 0;
	var tmpKey = {} ;
	var tmpObj = [] ;
	var tmpRegG = /root/gi ; // pour remplacer root. par le préfixe réel
	var tmpReg = [ /src\:[\s]*([^;\{\}]*)[;]*.*$/i, // pour isoler la valeur associée à src
				   /dst\:[\s]*([^;\{\}]*)[;]*.*$/i, // pour isoler la valeur associée à dst
				   /valeur\:[\s]*([^;\{\}]*)[;]*.*$/i, // pour isoler la valeur associée à value
				   /type\:[\s]*([^;\{\}]*)[;]*.*$/i ] ; // pour isoler la valeur associée à type
	var tmpLib = [ 'src', 'dst', 'valeur', 'type' ] ; // table des propriétés src, dst, value, type
	tabParam.forEach(function (elm) {
		try {
			tmpTab = elm.replace(tmpRegG, baseAddress);
			tmpKey = {} ;
			for (tmpInd = 0; tmpInd < tmpLib.length; tmpInd++) {
				tmpVal = tmpReg[tmpInd].exec(tmpTab) ;
				if (tmpVal !== null && tmpVal.length > 0) {
					tmpKey[tmpLib[tmpInd]] = eval(tmpVal[1].trim());
					tmpFlg = true ;
				} else {
					tmpFlg = false ;
					break;
				}
			}
			if (tmpFlg == true) {
				tmpObj.push(tmpKey) ;
			}
		} catch(error) {
			tmpFlg = false ;
		}
		return ;
    });
  	return tmpObj;
}

function completeCerfa(paramFields, valFields, indFields) {
	var tmpDst = '';
	var tmpResult = true ;
	var tmpRegexIndice = /\[i\]/gi ; // forme 'champ-cerfa[i] par exemple'
	var tmpRegexIndex = /\[x\]/gi ; // forme 'champ-cerfa[x] par exemple'
	var tmpBase = '' ;
	var tmpType = '' ;
	var tmpValue = '' ;
	var tmpFlag = false ;
	var objParam = [] ;
	
	if (typeof paramFields !== 'object' || !(Array.isArray(paramFields)) || typeof formFields !== 'object' || typeof indFields !== 'number') {
		return false;
	}
	paramFields.forEach(function (item) {
		if (typeof item.src === 'undefined') {
			return ;
		}
		tmpType = (typeof item.type !== undefined && typeof item.type === 'string') ? (item.type).toUpperCase() : '' ;
		if (tmpType === 'CARDINALITY' || tmpType === 'XCARDINALITY') {
			if (typeof item.src !== 'object' || item.src === null) {
				return ;
			}
			for (iCardinality = 0; iCardinality < (item.src).length; iCardinality++) {
				tmpBase = item.dst + '[' + iCardinality.toString() + ']' ;
				switch (tmpType) {
					case 'CARDINALITY':
						objParam = genereDynamicParams(item.valeur, tmpBase);
						break;
					case 'XCARDINALITY':
						objParam = genereXDynamicParams(item.valeur, tmpBase);
						break;
					default:
						objParam = [];
						break;
				}
				if (completeCerfa(objParam, valFields, iCardinality) == false) {
					tmpResult = false ;
				}
			}
			return ;
		}
		if (tmpType === 'COMPLEX' || tmpType === 'XCOMPLEX') {
			if (typeof item.src !== 'object') {
				return ;
			}
			(item.valeur).forEach(function (xtem) {
				switch (tmpType) {
					case 'COMPLEX':
						objParam = genereDynamicParams(xtem, item.dst);
						break;
					case 'XCOMPLEX':
						objParam = genereXDynamicParams(xtem, item.dst);
						break;
					default:
						objParam = [];
						break;
				}
				if (completeCerfa(objParam, valFields, 0) == false) {
					tmpResult = false ;
				}
				return ;
			});
			return ;
		}
		tmpDst = (item.dst).replace(tmpRegexIndice, (indFields + 1).toString()) ;
		tmpDst = tmpDst.replace(tmpRegexIndex, indFields.toString()) ;
		switch (tmpType) {
			case 'BOOL':
				var tmpFlg = typeof item.valeur === 'boolean' ? item.valeur : false ;
				if (item.src === null) {
					valFields[tmpDst] = tmpFlg ;
				} else {
					valFields[tmpDst] = item.src === tmpFlg ? true : false;
				}
				break;
			case 'XBOOL':
				if ((typeof item.src === 'boolean') && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					if (item.valeur[0] === item.valeur[1]) {
						if (item.src === false) {
							valFields[tmpDst] = null;
						}
					} else {
						valFields[tmpDst] = item.src;
					}
				}
				break;
			case 'RADIO':
				if (item.src) {
					valFields[tmpDst] = Value('id').of(item.src).eq(item.valeur) ? true : false;
				}
				break;
			case 'BRADIO':
				if ((typeof item.src === 'boolean' && item.src) && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					valFields[tmpDst] = Value('id').of(item.valeur[0]).eq(item.valeur[1]) ? true : false;
				}
				break;
			case 'XRADIO':
				if ((typeof item.src === 'boolean') && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					if (Value('id').of(item.valeur[0]).eq(item.valeur[1])) {
						if (item.src === false) {
							valFields[tmpDst] = null;
						}
					} else {
						valFields[tmpDst] = item.src;
					}
				}
				break;
			case 'CHECK':
				if (item.src) {
					valFields[tmpDst] = Value('id').of(item.src).contains(item.valeur) ? true : false;
				}
				break;
			case 'BCHECK':
				if ((typeof item.src === 'boolean' && item.src) && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					valFields[tmpDst] = Value('id').of(item.valeur[0]).contains(item.valeur[1]) ? true : false;
				}
				break;
			case 'XCHECK':
				if ((typeof item.src === 'boolean') && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					if (Value('id').of(item.valeur[0]).contains(item.valeur[1])) {
						if (item.src === false) {
							valFields[tmpDst] = null;
						}
					} else {
						valFields[tmpDst] = item.src;
					}
				}
				break;
			case 'STRING':
				if (item.valeur) {
					tmpValue = item.valeur + ' ' ;
					tmpFlag = true ;
				} else {
					tmpValue = '' ;
					tmpFlag = false ;
				}
				if (item.src) {
					tmpValue += item.src ;
					tmpFlag = true ;
				}
				if (tmpFlag) {
					valFields[tmpDst] = tmpValue ;
				}
				break;
			case 'BSTRING':
				if ((typeof item.src === 'boolean' && item.src) && (typeof item.valeur !== 'undefined' && item.valeur)) {
					valFields[tmpDst] = item.valeur ;
				}
				break;
			case 'XSTRING':
				if (((typeof item.src === 'string' or typeof item.src === 'object') && item.src) && (typeof item.valeur !== 'undefined' && item.valeur)) {
					valFields[tmpDst] = item.valeur ;
				}
				break;
			default:
				break;
		}
		return ;
	});
	return tmpResult ;
}

//--------------- FONCTIONS -------------------//

//--------------- MAIN GENERATION CERFA -------//

/* génération du Cerfa à partir de la table d'équivalences
 * (génération optionnelle)
 */

if (typeof formFields !== 'undefined' && typeof nameCerfa !== 'undefined') {
	var flgCerfa = true ;
	if (typeof equivCerfa !== 'undefined') { 
		flgCerfa = completeCerfa(equivCerfa, formFields, 0);
	}
	if (flgCerfa === true) {
		var cerfaDoc = nash.doc
			.load(repCerfa + nameCerfa)
			.apply(formFields);
		var pjCerfa = cerfaDoc.save(nameCerfa);
	}
}

/* ajout des PJ utilisateurs le cas échéant
 *
 */
 
if (typeof pjSignataire !== 'undefined') {
	if(Value('id').of(pjSignataire).contains(idDeclarant)) {
		appendPj($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantSignataire, pjUser);
	}
	if(Value('id').of(pjSignataire).contains(idMandataire)) {
		appendPj($attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire, pjUser);
	}
}

/* ajout des autres PJ le cas échéant
 *
 */

if($attachmentPreprocess.attachmentPreprocess.pjAutres !== 'undefined') {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjAutres, pjUser);
}

/* ajout du Cerfa en tant que PJ (s'il a été généré)
 *
 */
 
if (typeof pjCerfa !== 'undefined') {
	data.push(spec.createData({
			id: 'record',
			label: refFormTitle,
			description: 'Voici le formulaire obtenu à partir des données saisies.',
			type: 'FileReadOnly',
			value: [pjCerfa] 
	})) ;
}

/*
 * Enregistrement du fichier (en mémoire)
 */ 

var metasToDelete = [];
var recordMetas = nash.record.meta() != null ? nash.record.meta().metas : null;
var recordMetasSize = recordMetas != null ? recordMetas.size() : 0;

for (var i = 0; i < recordMetasSize; i++) {
    if (recordMetas.get(i).name == 'document') {
        metasToDelete.push({'name':'document', 'value': recordMetas.get(i).value});
    }
}
nash.record.removeMeta(metasToDelete);
nash.record.meta(metas);

data.push(spec.createData({
    id : 'uploaded',
    label : 'Pièces jointes',
    description : 'Pièces jointes ajoutées au formulaire.',
    type : 'FileReadOnly',
    value : [ merge(pjUser).save('saisine.pdf') ]
	})) ;

var groups = [ spec.createGroup({
    id : 'attachments',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Saisine du Ministère de l\'intérieur',
    groups : groups
});

//--------------- MAIN GENERATION CERFA -------//
