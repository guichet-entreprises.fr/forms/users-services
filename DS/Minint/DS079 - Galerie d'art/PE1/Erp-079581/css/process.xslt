<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns="http://www.w3.org/1999/xhtml" xmlns:n="http://www.ge.fr/schema/1.2/form-manager" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:include href="common.xslt" />

    <xsl:template match="/n:processes">
        <div class="container">
            <xsl:apply-templates />
        </div>
    </xsl:template>

    <xsl:template match="n:process">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-1">
                        <xsl:text>#</xsl:text>
                        <xsl:value-of select="@id" />
                    </div>
                    <div class="col-md-offset-9 col-md-2 text-right">
                        <xsl:value-of select="translate(@type, $lowerCase, $upperCase)" />
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <xsl:apply-templates select="@input" />
                <xsl:apply-templates select="@output" />
                <xsl:apply-templates select="@script | text()" />
                <xsl:apply-templates select="@ref" />
            </div>
        </div>
    </xsl:template>

    <xsl:template match="n:process/@ref">
        <xsl:call-template name="buildAttribute">
            <xsl:with-param name="label" select="concat(translate(substring(name(), 1, 1), $lowerCase, $upperCase), substring(name(), 2))" />
            <xsl:with-param name="value">
                <xsl:value-of select="." />
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="n:process/@script">
        <xsl:call-template name="buildAttribute">
            <xsl:with-param name="label" select="concat(translate(substring(name(), 1, 1), $lowerCase, $upperCase), substring(name(), 2))" />
            <xsl:with-param name="value">
                <pre class="language-javascript">
                    <xsl:attribute name="data-source">
                        <xsl:value-of select="." />
                    </xsl:attribute>
                    <xsl:value-of select="." />
                </pre>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="n:process/text()">
        <xsl:call-template name="buildAttribute">
            <xsl:with-param name="label" select="'Script'" />
            <xsl:with-param name="value">
                <pre class="language-javascript">
                    <code>
                        <xsl:value-of select="." />
                    </code>
                </pre>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="n:process/@input | n:process/@output">
        <xsl:call-template name="buildAttribute">
            <xsl:with-param name="label" select="concat(translate(substring(name(), 1, 1), $lowerCase, $upperCase), substring(name(), 2))" />
            <xsl:with-param name="value">
                <a>
                    <xsl:attribute name="href">
                        <xsl:value-of select="." />
                    </xsl:attribute>
                    <xsl:value-of select="." />
                </a>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>

</xsl:stylesheet>
