//--------------- VARIABLES -------------------//

/* init datas pour review (variables génériques)
 *
 */
 
var data = [] ;
var pjUser = [];
var metas = [];
var repCerfa = 'models/';
var iCardinality = 0 ;

/* init datas pour cerfa (variables spécifiques)
 * contient a minnima les références des groupes 
 * génériques de data.xml
 */

/*
 * init pointeurs saisies, Pj...
 */
 
var datasPtr = $ds008cerfa13824 ;
var datasSignature = datasPtr.cadreSignatureGroupe ;
var datasIdentite = datasPtr.cadreIdentiteGroupe ;
var refFormTitle = 'Coiffeur : Erp' ;

if (typeof datasPtr.cadreDemandeGroupe !== 'undefined') {
    var datasDemande = datasPtr.cadreDemandeGroupe ;
} else {
	var datasDemande = null ;
}

/*
 * init pointeurs PJ déclarants ou mandataires
 */
 
var pjSignataire = datasSignature.cadreSignature.soussigne ;
var idDeclarant = 'signataireQualiteDeclarant' ;
var idMandataire = 'signataireQualiteMandataire' ;

/* import description du CERFA (si il existe)
 * il génère les tables de paramétrage formFields et equivCerfa
 * formFields = dictionnaire des valeurs des champs du Cerfa
 * equivCerfa = dictionnaire de description des champs du Cerfa
 * il définit les variables repCerfa et nameCerfa
 * nameCerfa = nom du fichier pdf associé au Cerfa
 */

/*
 * Remplissage des champs du formulaire (autorisation de desossage)
 */

if (datasDemande !== null) {
	if ((Value('id').of(datasDemande.cadreDemande.nature).eq('depotDossier')) &&  (Value('id').of(datasDemande.cadreDemande.objet).eq(''))) {
		var flgCerfa = true;
	} else {
		var flgCerfa = false;
	}
} else {
	var flgCerfa = true;
}

if (flgCerfa === true) {
	
	//constantes et variables
	var formFields = {} ;
	var nameCerfa = 'Cerfa_13824.pdf' ;
	//pointeurs sur les pages de saisie
	var datasInfosDemandeur = datasPtr.infosDemandeur ; //page 1 : demandeur
	var datasInfosProjet = datasPtr.infosProjet ; //page 2 : projet
	var datasInfosTravaux = datasPtr.infosTravaux ; //page 3 : travaux
	var datasInfosAnnexes = datasPtr.infosAnnexes ; //page 4 : annexes

	//paramétrages des cardinalités
	var setEtages = "[ "
		+ "{ src: root.activitePrincipaleAvantEtage, dst: 'activitePrincipaleAvantEtage[x]', valeur: null, type: 'string' },"
		+ "{ src: root.activitePrincipaleApresEtage, dst: 'activitePrincipaleApresEtage[x]', valeur: null, type: 'string' },"
		+ "{ src: root.activiteAnnexeAvantEtage, dst: 'activiteAnnexeAvantEtage[x]', valeur: null, type: 'string' },"
		+ "{ src: root.activiteAnnexeApresEtage, dst: 'activiteAnnexeApresEtage[x]', valeur: null, type: 'string' },"
		+ "{ src: root.classementErpAvantEtage, dst: 'classementErpAvantEtage[x]', valeur: null, type: 'string' },"
		+ "{ src: root.classementErpApresEtage, dst: 'classementErpApresEtage[x]', valeur: null, type: 'string' },"
		+ "{ src: root.exploitantErpAvantEtage, dst: 'exploitantErpAvantEtage[x]', valeur: null, type: 'string' },"
		+ "{ src: root.exploitantErpApresEtage, dst: 'exploitantErpApresEtage[x]', valeur: null, type: 'string' },"
		+ "{ src: root.activitePrincipaleApresEtage, dst: 'activiteEffectifEtage[x]', valeur: null, type: 'string' },"
		+ "{ src: null, dst: 'publicEffectifEtage[x]', valeur: calculAddition([root.publicEffectifEtage],formFields,'publicEffectifCumule'), type: 'string' },"
		+ "{ src: null, dst: 'personnelEffectifEtage[x]', valeur: calculAddition([root.personnelEffectifEtage],formFields,'personnelEffectifCumule'), type: 'string' },"
		+ "{ src: null, dst: 'totalEffectifEtage[x]', valeur: calculAddition([root.publicEffectifEtage, root.personnelEffectifEtage],formFields,'totalEffectifCumule') , type: 'string' }"
		+ " ] ;" ;	
							 
	//description des zones génériques du Cerfa (type bool, type string...)
	var equivCerfa = [ 
		//page 1 : demandeur
    	{ src: !datasInfosDemandeur.personneMorale, dst: 'civiliteMrDeclarantPP', valeur: [datasIdentite.cadreIdentite.civilite, 'civiliteMasculin'], type: 'bRadio' },
    	{ src: !datasInfosDemandeur.personneMorale, dst: 'civiliteMmeDeclarantPP', valeur: [datasIdentite.cadreIdentite.civilite, 'civiliteFeminin'], type: 'bRadio' },
    	{ src: !datasInfosDemandeur.personneMorale, dst: 'nomDeclarantPP', valeur: extractInfoIdentite(datasIdentite.cadreIdentite, 'N', true), type: 'bString' },
    	{ src: !datasInfosDemandeur.personneMorale, dst: 'prenomDeclarantPP', valeur: extractInfoIdentite(datasIdentite.cadreIdentite, 'P', true), type: 'bString' },
    	{ src: !datasInfosDemandeur.personneMorale, dst: 'jourNaissanceDeclarantPP', valeur: formatDate(datasInfosDemandeur.infosPersonnePhysique.cadreNaissance.dateNaissance, 'J', true), type: 'bString' },
    	{ src: !datasInfosDemandeur.personneMorale, dst: 'moisNaissanceDeclarantPP', valeur: formatDate(datasInfosDemandeur.infosPersonnePhysique.cadreNaissance.dateNaissance, 'M', true), type: 'bString' },
    	{ src: !datasInfosDemandeur.personneMorale, dst: 'anneeNaissanceDeclarantPP', valeur: formatDate(datasInfosDemandeur.infosPersonnePhysique.cadreNaissance.dateNaissance, 'A', true), type: 'bString' },
    	{ src: datasInfosDemandeur.infosPersonneMorale, dst: 'raisonSocialeDeclarantPM', valeur: datasInfosDemandeur.infosPersonneMorale.raisonSocialeDeclarantPM, type: 'bString' },
    	{ src: datasInfosDemandeur.infosPersonneMorale, dst: 'siretDeclarantPM', valeur: supprimeEspaces(datasInfosDemandeur.infosPersonneMorale.siretDeclarantPM), type: 'bString' },
    	{ src: datasInfosDemandeur.infosPersonneMorale.representantPM, dst: 'civiliteMrDeclarantPM', valeur: [datasIdentite.cadreIdentite.civilite, 'civiliteMasculin'], type: 'bRadio' },
    	{ src: datasInfosDemandeur.infosPersonneMorale.representantPM, dst: 'civiliteMmeDeclarantPM', valeur: [datasIdentite.cadreIdentite.civilite, 'civiliteFeminin'], type: 'bRadio' },
    	{ src: datasInfosDemandeur.infosPersonneMorale.representantPM, dst: 'nomDeclarantPM', valeur: extractInfoIdentite(datasIdentite.cadreIdentite, 'N', true), type: 'bString' },
    	{ src: datasInfosDemandeur.infosPersonneMorale.representantPM, dst: 'prenomDeclarantPM', valeur: extractInfoIdentite(datasIdentite.cadreIdentite, 'P', true), type: 'bString' },
    	{ src: datasInfosDemandeur.infosPersonneMorale.representantPM, dst: 'jourNaissanceDeclarantPM', valeur: formatDate(datasInfosDemandeur.infosPersonnePhysique.cadreNaissance.dateNaissance, 'J', true), type: 'bString' },
    	{ src: datasInfosDemandeur.infosPersonneMorale.representantPM, dst: 'moisNaissanceDeclarantPM', valeur: formatDate(datasInfosDemandeur.infosPersonnePhysique.cadreNaissance.dateNaissance, 'M', true), type: 'bString' },
    	{ src: datasInfosDemandeur.infosPersonneMorale.representantPM, dst: 'anneeNaissanceDeclarantPM', valeur: formatDate(datasInfosDemandeur.infosPersonnePhysique.cadreNaissance.dateNaissance, 'A', true), type: 'bString' },
    	{ src: !datasInfosDemandeur.infosPersonneMorale.representantPM, dst: 'civiliteMrDeclarantPM', valeur: [datasInfosDemandeur.infosPersonneMorale.infosRepresentantPM.cadreIdentite.civilite, 'civiliteMasculin'], type: 'bRadio' },
    	{ src: !datasInfosDemandeur.infosPersonneMorale.representantPM, dst: 'civiliteMmeDeclarantPM', valeur: [datasInfosDemandeur.infosPersonneMorale.infosRepresentantPM.cadreIdentite.civilite, 'civiliteFeminin'], type: 'bRadio' },
    	{ src: !datasInfosDemandeur.infosPersonneMorale.representantPM, dst: 'nomDeclarantPM', valeur: extractInfoIdentite(datasInfosDemandeur.infosPersonneMorale.infosRepresentantPM.cadreIdentite, 'N', true), type: 'bString' },
    	{ src: !datasInfosDemandeur.infosPersonneMorale.representantPM, dst: 'prenomDeclarantPM', valeur: extractInfoIdentite(datasInfosDemandeur.infosPersonneMorale.infosRepresentantPM.cadreIdentite, 'P', true), type: 'bString' },
    	{ src: !datasInfosDemandeur.infosPersonneMorale.representantPM, dst: 'jourNaissanceDeclarantPM', valeur: formatDate(datasInfosDemandeur.infosPersonneMorale.infosRepresentantPM.cadreNaissance.dateNaissance, 'J', true), type: 'bString' },
    	{ src: !datasInfosDemandeur.infosPersonneMorale.representantPM, dst: 'moisNaissanceDeclarantPM', valeur: formatDate(datasInfosDemandeur.infosPersonneMorale.infosRepresentantPM.cadreNaissance.dateNaissance, 'M', true), type: 'bString' },
    	{ src: !datasInfosDemandeur.infosPersonneMorale.representantPM, dst: 'anneeNaissanceDeclarantPM', valeur: formatDate(datasInfosDemandeur.infosPersonneMorale.infosRepresentantPM.cadreNaissance.dateNaissance, 'A', true), type: 'bString' },
    	{ src: datasInfosDemandeur.demandeur, dst: 'numeroVoieDemandeur', valeur: mergeAdresse(datasIdentite.cadreAdresse, 'N', false), type: 'bString' },
    	{ src: datasInfosDemandeur.demandeur, dst: 'nomVoieDemandeur', valeur: mergeAdresse(datasIdentite.cadreAdresse, 'W', false), type: 'bString' },
    	{ src: datasInfosDemandeur.demandeur, dst: 'complementAdresseDemandeur', valeur: mergeAdresse(datasIdentite.cadreAdresse, 'S', false), type: 'bString' },
    	{ src: datasInfosDemandeur.demandeur, dst: 'codePostalDemandeur', valeur: datasIdentite.cadreAdresse.codePostal, type: 'bString' },
    	{ src: datasInfosDemandeur.demandeur, dst: 'communeDemandeur', valeur: extractInfoAdresse(datasIdentite.cadreAdresse, 'C', false), type: 'bString' },
    	{ src: datasInfosDemandeur.demandeur, dst: 'paysDemandeur', valeur: extractInfoAdresse(datasIdentite.cadreAdresse, 'P', false), type: 'bString' },
		{ src: datasInfosDemandeur.demandeur, dst: 'telephoneFixeDemandeur', valeur: formatNumTelephone(datasIdentite.cadreContact.telephoneFixe.telephoneNum), type: 'bString' },
		{ src: datasInfosDemandeur.demandeur, dst: 'telephoneMobileDemandeur', valeur: formatNumTelephone(datasIdentite.cadreContact.telephoneMobile.telephoneNum), type: 'bString' },
		{ src: datasInfosDemandeur.demandeur, dst: 'IndicatifTelephoneDemandeur', valeur: extractIndicatifEtrangerTelephone(datasIdentite.cadreContact.telephoneFixe.telephoneNum), type: 'bString' },
		{ src: datasInfosDemandeur.demandeur, dst: 'emailDemandeur', valeur: datasIdentite.cadreContact.courriel, type: 'bString' },
    	{ src: !datasInfosDemandeur.demandeur, dst: 'numeroVoieDemandeur', valeur: mergeAdresse(datasInfosDemandeur.cadreDemandeur.cadreAdresse, 'N', false), type: 'bString' },
    	{ src: !datasInfosDemandeur.demandeur, dst: 'nomVoieDemandeur', valeur: mergeAdresse(datasInfosDemandeur.cadreDemandeur.cadreAdresse, 'W', false), type: 'bString' },
    	{ src: !datasInfosDemandeur.demandeur, dst: 'complementAdresseDemandeur', valeur: mergeAdresse(datasInfosDemandeur.cadreDemandeur.cadreAdresse, 'S', false), type: 'bString' },
    	{ src: !datasInfosDemandeur.demandeur, dst: 'codePostalDemandeur', valeur: datasInfosDemandeur.cadreDemandeur.cadreAdresse.codePostal, type: 'bString' },
    	{ src: !datasInfosDemandeur.demandeur, dst: 'communeDemandeur', valeur: extractInfoAdresse(datasInfosDemandeur.cadreDemandeur.cadreAdresse, 'C', false), type: 'bString' },
    	{ src: !datasInfosDemandeur.demandeur, dst: 'paysDemandeur', valeur: extractInfoAdresse(datasInfosDemandeur.cadreDemandeur.cadreAdresse, 'P', false), type: 'bString' },
		{ src: !datasInfosDemandeur.demandeur, dst: 'telephoneFixeDemandeur', valeur: formatNumTelephone(datasInfosDemandeur.cadreDemandeur.cadreContact.telephoneFixe.telephoneNum), type: 'bString' },
		{ src: !datasInfosDemandeur.demandeur, dst: 'telephoneMobileDemandeur', valeur: formatNumTelephone(datasInfosDemandeur.cadreDemandeur.cadreContact.telephoneMobile.telephoneNum), type: 'bString' },
		{ src: !datasInfosDemandeur.demandeur, dst: 'IndicatifTelephoneDemandeur', valeur: extractIndicatifEtrangerTelephone(datasInfosDemandeur.cadreDemandeur.cadreContact.telephoneFixe.telephoneNum), type: 'bString' },
		{ src: !datasInfosDemandeur.demandeur, dst: 'emailDemandeur', valeur: datasInfosDemandeur.cadreDemandeur.cadreContact.courriel, type: 'bString' },
		{ src: datasInfosDemandeur.oppositionDiffusionCommerciale, dst: 'oppositionDiffusionCommerciale', valeur: true, type: 'bool' },
		//page 2 : projet
    	{ src: datasInfosProjet.projetAdresse, dst: 'numeroVoieEtablissement', valeur: mergeAdresse(datasIdentite.cadreAdresse, 'N', false), type: 'bString' },
    	{ src: datasInfosProjet.projetAdresse, dst: 'nomVoieEtablissement', valeur: mergeAdresse(datasIdentite.cadreAdresse, 'W', false), type: 'bString' },
    	{ src: datasInfosProjet.projetAdresse, dst: 'complementAdresseEtablissement', valeur: mergeAdresse(datasIdentite.cadreAdresse, 'S', false), type: 'bString' },
    	{ src: datasInfosProjet.projetAdresse, dst: 'codePostalEtablissement', valeur: datasIdentite.cadreAdresse.codePostal, type: 'bString' },
    	{ src: datasInfosProjet.projetAdresse, dst: 'communeEtablissement', valeur: extractInfoAdresse(datasIdentite.cadreAdresse, 'C', false), type: 'bString' },
    	{ src: !datasInfosProjet.projetAdresse, dst: 'numeroVoieEtablissement', valeur: mergeAdresse(datasInfosProjet.cadreEtablissement.cadreAdresse, 'N', false), type: 'bString' },
    	{ src: !datasInfosProjet.projetAdresse, dst: 'nomVoieEtablissement', valeur: mergeAdresse(datasInfosProjet.cadreEtablissement.cadreAdresse, 'W', false), type: 'bString' },
    	{ src: !datasInfosProjet.projetAdresse, dst: 'complementAdresseEtablissement', valeur: mergeAdresse(datasInfosProjet.cadreEtablissement.cadreAdresse, 'S', false), type: 'bString' },
    	{ src: !datasInfosProjet.projetAdresse, dst: 'codePostalEtablissement', valeur: datasInfosProjet.cadreEtablissement.cadreAdresse.codePostal, type: 'bString' },
    	{ src: !datasInfosProjet.projetAdresse, dst: 'communeEtablissement', valeur: extractInfoAdresse(datasInfosProjet.cadreEtablissement.cadreAdresse, 'C', false), type: 'bString' },
		{ src: datasInfosProjet.infosMoe.personneMorale, dst: 'personneMoraleMoe', valeur: true, type: 'bool' },		
    	{ src: datasInfosProjet.personneMoe, dst: 'civiliteMrMoePP', valeur: [datasIdentite.cadreIdentite.civilite, 'civiliteMasculin'], type: 'bRadio' },
    	{ src: datasInfosProjet.personneMoe, dst: 'civiliteMmeMoePP', valeur: [datasIdentite.cadreIdentite.civilite, 'civiliteFeminin'], type: 'bRadio' },
    	{ src: datasInfosProjet.personneMoe, dst: 'nomMoePP', valeur: extractInfoIdentite(datasIdentite.cadreIdentite, 'N', true), type: 'bString' },
    	{ src: datasInfosProjet.personneMoe, dst: 'prenomMoePP', valeur: extractInfoIdentite(datasIdentite.cadreIdentite, 'P', true), type: 'bString' },
    	{ src: datasInfosProjet.personneMoe, dst: 'numeroVoieMoe', valeur: mergeAdresse(datasIdentite.cadreAdresse, 'N', false), type: 'bString' },
    	{ src: datasInfosProjet.personneMoe, dst: 'nomVoieMoe', valeur: mergeAdresse(datasIdentite.cadreAdresse, 'W', false), type: 'bString' },
    	{ src: datasInfosProjet.personneMoe, dst: 'complementAdresseMoe', valeur: mergeAdresse(datasIdentite.cadreAdresse, 'S', false), type: 'bString' },
    	{ src: datasInfosProjet.personneMoe, dst: 'codePostalMoe', valeur: datasIdentite.cadreAdresse.codePostal, type: 'bString' },
    	{ src: datasInfosProjet.personneMoe, dst: 'communeMoe', valeur: extractInfoAdresse(datasIdentite.cadreAdresse, 'C', false), type: 'bString' },
    	{ src: datasInfosProjet.personneMoe, dst: 'paysMoe', valeur: extractInfoAdresse(datasIdentite.cadreAdresse, 'P', false), type: 'bString' },
		{ src: datasInfosProjet.personneMoe, dst: 'telephoneFixeMoe', valeur: formatNumTelephone(datasIdentite.cadreContact.telephoneFixe.telephoneNum), type: 'bString' },
		{ src: datasInfosProjet.personneMoe, dst: 'telephoneMobileMoe', valeur: formatNumTelephone(datasIdentite.cadreContact.telephoneMobile.telephoneNum), type: 'bString' },
		{ src: datasInfosProjet.personneMoe, dst: 'IndicatifTelephoneMoe', valeur: extractIndicatifEtrangerTelephone(datasIdentite.cadreContact.telephoneFixe.telephoneNum), type: 'bString' },
		{ src: datasInfosProjet.personneMoe, dst: 'emailMoe', valeur: datasIdentite.cadreContact.courriel, type: 'bString' },
    	{ src: datasInfosProjet.personneMoe, dst: 'raisonSocialeMoePM', valeur: datasInfosDemandeur.infosPersonneMorale.raisonSocialeDeclarantPM, type: 'bString' },
    	{ src: datasInfosProjet.personneMoe, dst: 'siretMoePM', valeur: supprimeEspaces(datasInfosDemandeur.infosPersonneMorale.siretDeclarantPM), type: 'bString' },
    	{ src: !datasInfosProjet.personneMoe, dst: 'civiliteMrMoePP', valeur: [datasInfosProjet.infosMoe.cadreIdentite.civilite, 'civiliteMasculin'], type: 'bRadio' },
    	{ src: !datasInfosProjet.personneMoe, dst: 'civiliteMmeMoePP', valeur: [datasInfosProjet.infosMoe.cadreIdentite.civilite, 'civiliteFeminin'], type: 'bRadio' },
    	{ src: !datasInfosProjet.personneMoe, dst: 'nomMoePP', valeur: extractInfoIdentite(datasInfosProjet.infosMoe.cadreIdentite, 'N', true), type: 'bString' },
    	{ src: !datasInfosProjet.personneMoe, dst: 'prenomMoePP', valeur: extractInfoIdentite(datasInfosProjet.infosMoe.cadreIdentite, 'P', true), type: 'bString' },
    	{ src: !datasInfosProjet.personneMoe, dst: 'numeroVoieMoe', valeur: mergeAdresse(datasInfosProjet.infosMoe.cadreAdresse, 'N', false), type: 'bString' },
    	{ src: !datasInfosProjet.personneMoe, dst: 'nomVoieMoe', valeur: mergeAdresse(datasInfosProjet.infosMoe.cadreAdresse, 'W', false), type: 'bString' },
    	{ src: !datasInfosProjet.personneMoe, dst: 'complementAdresseMoe', valeur: mergeAdresse(datasInfosProjet.infosMoe.cadreAdresse, 'S', false), type: 'bString' },
    	{ src: !datasInfosProjet.personneMoe, dst: 'codePostalMoe', valeur: datasInfosProjet.infosMoe.cadreAdresse.codePostal, type: 'bString' },
    	{ src: !datasInfosProjet.personneMoe, dst: 'communeMoe', valeur: extractInfoAdresse(datasInfosProjet.infosMoe.cadreAdresse, 'C', false), type: 'bString' },
    	{ src: !datasInfosProjet.personneMoe, dst: 'paysMoe', valeur: extractInfoAdresse(datasInfosProjet.infosMoe.cadreAdresse, 'P', false), type: 'bString' },
		{ src: !datasInfosProjet.personneMoe, dst: 'telephoneFixeMoe', valeur: formatNumTelephone(datasInfosProjet.infosMoe.cadreContact.telephoneFixe.telephoneNum), type: 'bString' },
		{ src: !datasInfosProjet.personneMoe, dst: 'telephoneMobileMoe', valeur: formatNumTelephone(datasInfosProjet.infosMoe.cadreContact.telephoneMobile.telephoneNum), type: 'bString' },
		{ src: !datasInfosProjet.personneMoe, dst: 'IndicatifTelephoneMoe', valeur: extractIndicatifEtrangerTelephone(datasInfosProjet.infosMoe.cadreContact.telephoneFixe.telephoneNum), type: 'bString' },
		{ src: !datasInfosProjet.personneMoe, dst: 'emailMoe', valeur: datasInfosProjet.infosMoe.cadreContact.courriel, type: 'bString' },
    	{ src: !datasInfosProjet.personneMoe, dst: 'raisonSocialeMoePM', valeur: datasInfosProjet.infosMoe.infosPersonneMorale.raisonSocialeMoePM, type: 'bString' },
    	{ src: !datasInfosProjet.personneMoe, dst: 'siretMoePM', valeur: supprimeEspaces(datasInfosProjet.infosMoe.infosPersonneMorale.siretMoePM), type: 'bString' },
		{ src: datasInfosProjet.infosMoe.courriersMoe, dst: 'courriersMoe', valeur: true, type: 'bool' },		
		{ src: datasInfosProjet.sectionCadastraleEtablissement, dst: 'sectionCadastraleEtablissement', valeur: null, type: 'string' },
		{ src: datasInfosProjet.parcelleEtablissement, dst: 'parcelleEtablissement', valeur: null, type: 'string' },
		{ src: datasInfosProjet.projetParticularites.icpe, dst: 'icpeOui', valeur: true, type: 'bool' },
		{ src: datasInfosProjet.projetParticularites.icpe, dst: 'icpeNon', valeur: false, type: 'bool' },
		{ src: datasInfosProjet.projetParticularites.derogationIncendie, dst: 'derogationIncendie', valeur: true, type: 'bool' },
		{ src: datasInfosProjet.projetParticularites.nbDerogationsIncendie, dst: 'nbDerogationsIncendie', valeur: null, type: 'string' },
		{ src: datasInfosProjet.projetParticularites.derogationAccessibilite, dst: 'derogationAccessibilite', valeur: true, type: 'bool' },
		{ src: datasInfosProjet.projetParticularites.nbDerogationsAccessibilite, dst: 'nbDerogationsAccessibilite', valeur: null, type: 'string' },
		{ src: datasInfosProjet.projetParticularites.modalitesParticulieres, dst: 'modalitesParticulieres', valeur: true, type: 'bool' },
		{ src: datasInfosProjet.projetParticularites.noteAnnexe, dst: 'noteAnnexe', valeur: null, type: 'string' },		
		//page 3 : travaux
		{ src: datasInfosTravaux.travaux.travauxConstructionNeuve, dst: 'travauxConstructionNeuve', valeur: true, type: 'bool' },
		{ src: datasInfosTravaux.travaux.natureTravaux, dst: 'travauxMiseEnConformite', valeur: 'travauxMiseEnConformite', type: 'check' },
		{ src: datasInfosTravaux.travaux.natureTravaux, dst: 'travauxExtension', valeur: 'travauxExtension', type: 'check' },
		{ src: datasInfosTravaux.travaux.natureTravaux, dst: 'travauxRehabilitation', valeur: 'travauxRehabilitation', type: 'check' },
		{ src: datasInfosTravaux.travaux.natureTravaux, dst: 'travauxAmenagement', valeur: 'travauxAmenagement', type: 'check' },
		{ src: datasInfosTravaux.travaux.natureTravaux, dst: 'travauxCreationVolumes', valeur: 'travauxCreationVolumes', type: 'check' },
		{ src: datasInfosTravaux.travaux.natureTravaux, dst: 'travauxAcces', valeur: 'travauxAcces', type: 'check' },
		{ src: datasInfosTravaux.travaux.travauxSurfacePlancherAvant, dst: 'travauxSurfacePlancherAvant', valeur: null, type: 'string' },
		{ src: datasInfosTravaux.travaux.travauxSurfacePlancherApres, dst: 'travauxSurfacePlancherApres', valeur: null, type: 'string' },
		{ src: datasInfosTravaux.travaux.adAPAnterieur, dst: 'adAPAnterieurOui', valeur: true, type: 'bool' },
		{ src: datasInfosTravaux.travaux.adAPAnterieur, dst: 'adAPAnterieurNon', valeur: false, type: 'bool' },
		{ src: datasInfosTravaux.travaux.adAPAnterieur, dst: 'adAPOui', valeur: true, type: 'bool' },
		{ src: datasInfosTravaux.travaux.adAPAnterieur, dst: 'adAPNon', valeur: false, type: 'bool' },
		{ src: datasInfosTravaux.travaux.adAPAnterieurNumero, dst: 'adAPAnterieurNumero', valeur: null, type: 'string' },
		{ src: null, dst: 'adAPAnterieurJour', valeur: formatDate(datasInfosTravaux.travaux.adAPAnterieurDate, 'J', true), type: 'string' },
		{ src: null, dst: 'adAPAnterieurMois', valeur: formatDate(datasInfosTravaux.travaux.adAPAnterieurDate, 'M', true), type: 'string' },
		{ src: null, dst: 'adAPAnterieurAnnee', valeur: formatDate(datasInfosTravaux.travaux.adAPAnterieurDate, 'A', true), type: 'string' },
		{ src: datasInfosTravaux.cadreStationnement.typeStationnement, dst: 'stationnementCouvert', valeur: 'stationnementCouvert', type: 'check' },
		{ src: datasInfosTravaux.cadreStationnement.typeStationnement, dst: 'stationnementParkingIntegre', valeur: 'stationnementParkingIntegre', type: 'check' },
		{ src: datasInfosTravaux.cadreStationnement.typeStationnement, dst: 'stationnementParkingIsole', valeur: 'stationnementParkingIsole', type: 'check' },
		{ src: null, dst: 'stationnementParkingAnneePermis', valeur: formatDate(datasInfosTravaux.cadreStationnement.stationnementParkingAnneePermis, 'T', true), type: 'string' },
		{ src: datasInfosTravaux.cadreStationnement.stationnementPlacesAvantTravaux, dst: 'stationnementPlacesAvantTravaux', valeur: null, type: 'string' },
		{ src: datasInfosTravaux.cadreStationnement.stationnementHandicapesAvantTravaux, dst: 'stationnementHandicapesAvantTravaux', valeur: null, type: 'string' },
		{ src: datasInfosTravaux.cadreStationnement.stationnementPlacesApresTravaux, dst: 'stationnementPlacesApresTravaux', valeur: null, type: 'string' },
		{ src: datasInfosTravaux.cadreStationnement.stationnementHandicapesApresTravaux, dst: 'stationnementHandicapesApresTravaux', valeur: null, type: 'string' },
		{ src: datasInfosTravaux.sousSol, dst: 'activiteEffectifSousSol', valeur: datasInfosTravaux.cadreTravauxSousSol.activitePrincipaleApresEtage, type: 'bString' },
		{ src: datasInfosTravaux.sousSol, dst: 'publicEffectifSousSol', valeur: calculAddition([datasInfosTravaux.cadreTravauxSousSol.publicEffectifEtage],formFields,'publicEffectifCumule'), type: 'bString' },
		{ src: datasInfosTravaux.sousSol, dst: 'personnelEffectifSousSol', valeur: calculAddition([datasInfosTravaux.cadreTravauxSousSol.personnelEffectifEtage],formFields,'personnelEffectifCumule'), type: 'bString' },
		{ src: null, dst: 'totalEffectifSousSol', valeur: calculAddition([datasInfosTravaux.cadreTravauxSousSol.publicEffectifEtage, datasInfosTravaux.cadreTravauxSousSol.personnelEffectifEtage],formFields,'totalEffectifCumule') , type: 'string' },
		{ src: datasInfosTravaux.cadreTravauxEtages, dst: 'datasInfosTravaux.cadreTravauxEtages', valeur: setEtages, type: 'cardinality' },
		//page 3 : annexes
		{ src: null, dst: 'cerfaERP', valeur: true, type: 'bool' },
		{ src: datasInfosAnnexes.annexesObligatoires, dst: 'planDeSituation', valeur: 'planDeSituation', type: 'check' },
		{ src: datasInfosAnnexes.annexesObligatoires, dst: 'reglementSecurite', valeur: 'reglementSecurite', type: 'check' },
		{ src: datasInfosAnnexes.annexesObligatoires, dst: 'planDeMasse', valeur: 'planDeMasse', type: 'check' },
		{ src: datasInfosAnnexes.annexesObligatoires, dst: 'planDeCoupe', valeur: 'planDeCoupe', type: 'check' },
		{ src: datasInfosAnnexes.annexesObligatoires, dst: 'planDetailleArchi', valeur: 'planDetailleArchi', type: 'check' },
		{ src: datasInfosAnnexes.annexesObligatoires, dst: 'planDetailleCirculation', valeur: 'planDetailleCirculation', type: 'check' },
		{ src: datasInfosAnnexes.annexesObligatoires, dst: 'noticeAccessibilite', valeur: 'noticeAccessibilite', type: 'check' },
		{ src: datasInfosAnnexes.annexesFacultatives, dst: 'demandesDeDerogations', valeur: 'demandesDeDerogations', type: 'check' },
		{ src: datasInfosAnnexes.annexesFacultatives, dst: 'planAvantTravaux', valeur: 'planAvantTravaux', type: 'check' },
		{ src: datasInfosAnnexes.annexesFacultatives, dst: 'planParking', valeur: 'planParking', type: 'check' },
		//bloc signature
		{ src: null, dst: 'signature', valeur: genereInfoSignataire(true, true), type: 'string' },
		{ src: datasSignature.cadreSignature.signatureLieu, dst: 'signatureLieu', valeur: null, type: 'string' },
		{ src: null, dst: 'signatureDate', valeur: formatDate(datasSignature.cadreSignature.signatureDate, 'T', true), type: 'string' }
	] ; 
}

//--------------- VARIABLES -------------------//

//--------------- FONCTIONS -------------------//

/* fonctions génériques de formatage des données
 *
 */

/*
 * calculs mathématiques : calcul de pourcentage
 */
 
function calculPercent(valNumerateur, valDenominateur, nbDecimales) {
	var valResult = '' ;
	var tmpValeur = 0 ;
	while (true) {
		if (typeof valNumerateur === 'undefined' || typeof valNumerateur !== 'number') {
			break ;
		}
		if (typeof valDenominateur === 'undefined' || typeof valDenominateur !== 'number') {
			break;
		}
		if (typeof nbDecimales === 'undefined' || typeof nbDecimales !== 'number') {
			tmpValeur = 0 ;
		} else {
			tmpValeur = nbDecimales >= 0 ? nbDecimales : 0 ; 
		}	
		tmpValeur = Math.pow(10, tmpValeur);
		if (valDenominateur !== 0) {
			tmpValeur = Math.round(((valNumerateur / valDenominateur) * 100) / tmpValeur) ;
			valResult = tmpValeur.toString();
			
		}
		break ;
	}
	return valResult ;
 }

/*
 * calculs mathématiques : calcul de soustraction
 */
 
function calculSubstraction(valTotal, tabValeurs, cumulValeur, keyCumul) {
	var valResult = '' ;
	var tmpValeur = 0 ;
	while (true) {
		if (typeof valTotal === 'undefined' || typeof valTotal !== 'number') {
			break ;
		}
		tmpValeur = valTotal ;
		if (typeof tabValeurs === 'object' && Array.isArray(tabValeurs)) {
			for (tmpIndex = 0; tmpIndex < tabValeurs.length; tmpIndex++) {
				if (typeof tabValeurs[tmpIndex] === 'number') {
					tmpValeur -= tabValeurs[tmpIndex];
					tmpFlag = true ;
				}
			}	
		}
		if (tmpFlag === true) {
			valResult = tmpValeur.toString();
			if (typeof cumulValeur === 'object' && cumulValeur !== null && typeof keyCumul === 'string') {
				if (typeof cumulValeur[keyCumul] !== 'undefined' && isNaN(cumulValeur[keyCumul]) === false) {
					cumulValeur[keyCumul] = (Number(cumulValeur[keyCumul]) - tmpValeur).toString() ;
				} else {
					cumulValeur[keyCumul] = valResult ;
				}
			}
		}
		break;
	}
	return valResult ;
 }

function calculAddition(tabValeurs, cumulValeur, keyCumul) {
	var valResult = '' ;
	var tmpFlag = false ;
	var tmpValeur = 0 ;
	var tmpIndex = 0 ;
	while (true) {
		if (typeof tabValeurs === 'object' && Array.isArray(tabValeurs)) {
			for (tmpIndex = 0; tmpIndex < tabValeurs.length; tmpIndex++) {
				if (typeof tabValeurs[tmpIndex] === 'number') {
					tmpValeur += tabValeurs[tmpIndex];
					tmpFlag = true ;
				}
			}	
		}
		if (tmpFlag === true) {
			valResult = tmpValeur.toString();
			if (typeof cumulValeur === 'object' && cumulValeur !== null && typeof keyCumul === 'string') {
				if (typeof cumulValeur[keyCumul] !== 'undefined' && isNaN(cumulValeur[keyCumul]) === false) {
					cumulValeur[keyCumul] = (tmpValeur + Number(cumulValeur[keyCumul])).toString() ;
				} else {
					cumulValeur[keyCumul] = valResult ;
				}
			}
		}
		break ;
	}
	return valResult ;
 }

/* suppression des espaces dans une chaine
 *
 */

function supprimeEspaces(szLibel) {
	var tmpRegex = /\s/g ;
	var tmpLibel = '';
	if (typeof szLibel !== 'undefined' && typeof szLibel === 'string') {
		tmpLibel = szLibel !== null ? szLibel.replace(tmpRegex, '') : '' ;	
	}
	return tmpLibel ;
}
 
/* merge des prénoms (séparateur ',')
 *
 */
	
function mergePrenoms(listPrenoms) {
	var prenoms = [];
	if (typeof listPrenoms !== 'undefined' && typeof listPrenoms === 'object') {
		for (i = 0; i < listPrenoms.length; i++) {
			prenoms.push(listPrenoms[i]);
    	}
	} else {
		prenoms.push('');
	}
	return prenoms.toString();
}

/* merge du nom et des prénoms (séparateur ',')
 *
 */

function mergeNomPrenoms(szNom, listPrenoms) {
	var result = '';
	if (typeof szNom === 'string') {
		result = szNom + ' ';
		result += mergePrenoms(listPrenoms);
	} 
	return result;
}

/* construction de la date et du lieu de naissance
 *
 */

function mergeNaissance(blocNaissance) {
	var naissance = '' ;
	if (typeof blocNaissance !== 'undefined' && typeof blocNaissance === 'object') {
		naissance = blocNaissance.lieuNaissance ? blocNaissance.lieuNaissance + ' / ' : '' ;
		naissance += blocNaissance.paysNaissance ? blocNaissance.paysNaissance : '' ;
	}
	return naissance ;
}
	
/* construction de l'adresse en tenant compte du pays ou non
 *
 */

function mergeAdresse(blocAdresse, typeMerge, multiLine) {
	var tmpVar = '' ;
	var adresse = '' ;
	var chgLigne = ' \n' ;
	if (typeof blocAdresse !== 'undefined' && typeof blocAdresse === 'object') {
		var fmtLigne = (multiLine === true ? chgLigne : ' ');
		var typeAct = (typeof typeMerge !== 'undefined' && typeof typeMerge === 'string') ? typeMerge.toUpperCase() : 'T' ;
		switch (typeAct) {
			case 'N':
				adresse += blocAdresse.numeroVoie ? blocAdresse.numeroVoie + ' ' : '' ;
				adresse += blocAdresse.indiceVoie ? blocAdresse.indiceVoie : '' ;
				break;
			case 'V':	
				tmpVar = mergeAdresse(blocAdresse, 'N', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + ' ' : '' ;
				adresse += mergeAdresse(blocAdresse, 'W', multiLine) ;
				break;
			case 'W':
				adresse += blocAdresse.typeVoie ? blocAdresse.typeVoie + ' ' : '' ;
				adresse += blocAdresse.nomVoie ?  blocAdresse.nomVoie : '' ;
				break;
			case 'S':
				adresse += blocAdresse.complementAdresse ? blocAdresse.complementAdresse + fmtLigne : '' ;
				adresse += blocAdresse.distributionSpeciale ? blocAdresse.distributionSpeciale : '' ;
				break;
			case 'C':
				if (typeof blocAdresse.pays !== 'undefined') {
					if (typeof blocAdresse.codePostal !== 'undefined' && blocAdresse.codePostal) {
						adresse += blocAdresse.codePostal ? blocAdresse.codePostal + ' ' : '' ;
						adresse += blocAdresse.commune ? blocAdresse.commune : '' ;
					} else {
						adresse += blocAdresse.ville ? blocAdresse.ville + fmtLigne : '' ;
						adresse += blocAdresse.pays ? blocAdresse.pays : '' ;
					}
				} else {
					adresse += blocAdresse.codePostal ? blocAdresse.codePostal + ' ' : '' ;
					adresse += blocAdresse.commune ? blocAdresse.commune : '' ;
				}
				break;
			case 'X':
				tmpVar = mergeAdresse(blocAdresse, 'S', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + fmtLigne : '' ;
				adresse += mergeAdresse(blocAdresse, 'C', multiLine) ;
				break;
			case 'Z':
				tmpVar = mergeAdresse(blocAdresse, 'V', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + fmtLigne : '' ;
				adresse += mergeAdresse(blocAdresse, 'S', multiLine) ;
				break;
			case 'T':
			default:
				tmpVar = mergeAdresse(blocAdresse, 'V', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + fmtLigne : '' ;
				tmpVar = mergeAdresse(blocAdresse, 'S', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + fmtLigne : '' ;
				adresse += mergeAdresse(blocAdresse, 'C', multiLine) ;
		}
	}
	return adresse;
}

/*
 * mise en forme numéro de téléphone au format 10 chiffres
 */

function formatNumTelephone(objNumTel) {
	var tmpRegexTelIntro = /^(\+\d{1,3}[\s\.\-]?)(.*)$/ ;
	var tmpRegexTel = /(\d{1,})[\s\.\-]?/g ;
	var tmpTel = '' ;
	var tmpIntro = '';
	var tmpTelFmt = '';
	if (typeof objNumTel !== 'undefined' && typeof objNumTel === 'object' && objNumTel) {
		tmpTel = objNumTel.international ;
		if (tmpRegexTelIntro.test(tmpTel) === true) {
			tmpIntro = tmpTel.replace(tmpRegexTelIntro, '$1') ;
			tmpTelFmt = '$2' ;
			if (tmpIntro.substring(0,3) == '+33') {
				tmpTelFmt = '0' + tmpTelFmt ;
			}	
			tmpTel = tmpTel.replace(tmpRegexTelIntro, tmpTelFmt) ;
			tmpTel = tmpTel.replace(tmpRegexTel, '$1') ;
		}
	}
	return tmpTel ;
}

/*
 * infos contacts téléphoniques
 */
 
function mergeNumTelephone(blocNumTel, flgMerge, flgInternational) {
	var numTel = '';
	var tmpTel = '';
	if (typeof blocNumTel !== 'undefined' && typeof blocNumTel === 'object') {
		if (typeof blocNumTel.telephoneFixe.telephoneNum !== 'undefined' && blocNumTel.telephoneFixe.telephoneNum ) {
			if (flgInternational === false) {
				tmpTel = formatNumTelephone(blocNumTel.telephoneFixe.telephoneNum) ;
			} else {
				tmpTel = blocNumTel.telephoneFixe.telephoneNum.international ;
			}
			numTel += tmpTel ;
		}
		if (typeof blocNumTel.telephoneMobile.telephoneNum !== 'undefined' && blocNumTel.telephoneMobile.telephoneNum) {
			if (numTel.length > 0 && flgMerge === true) {
				numTel += ' / ';
			}
			if (flgMerge === true || numTel.length === 0) {
				if (flgInternational === false) {
					tmpTel = formatNumTelephone(blocNumTel.telephoneMobile.telephoneNum) ;
				} else {
					tmpTel = blocNumTel.telephoneMobile.telephoneNum.international;
				}
				numTel += tmpTel ;
			}
		}
	}
	return numTel;
}

/* formatage d'une date avec extaction ou non d'une partie (J, M, A)
 *
 */

function formatDate(valDate, typeExtract, fullYear) {
	var dateResult = '';
	var tmpYear = '' ;
	if (typeof valDate === 'object' && valDate) {
    	var dateTmp = new Date(parseInt(valDate.getTimeInMillis())) ; ;
		var dateIso = ((dateTmp.toLocaleDateString()).substr(0, 10)).split('-') ;
		var typeAct = (typeof typeExtract !== 'undefined' && typeof typeExtract === 'string') ? typeExtract.toUpperCase() : 'T' ;
		if (dateIso.length > 1) {
			tmpYear = (fullYear === true ? dateIso[0] : dateIso[0].substr(2)) ;
			switch (typeAct) {
				case 'D':
				case 'J':
					dateResult = dateIso[2] ;
					break;
				case 'M':
					dateResult = dateIso[1] ;
					break;
				case 'A':
				case 'Y':
					dateResult = tmpYear ;
					break;
				case 'T':
				default:
					dateResult = dateIso[2] + '/' + dateIso[1] + '/' + tmpYear ;
			}
		} else {
			dateResult = dateIso[0] ;
		}
	}
	return dateResult ;
}

/*
 * infos signataire
 */

function genereInfoSignataire(flgCompleteInfos, flgMergePrenoms) {
	var contactInfos = [] ;
	var infoSignataire = '' ;
	var chgLigne = ' \n' ;
	if (typeof datasSignature !== 'undefined' && typeof datasIdentite !== 'undefined' && typeof idMandataire !== 'undefined') {
		if (flgCompleteInfos === true) {
			infoSignataire = 'Cette déclaration respecte les attendus de l\’article 1367 du code civil.' + chgLigne ;
			infoSignataire += datasSignature.cadreSignature.soussigne + ' ';
		}
		if (Value('id').of(datasSignature.cadreSignature.soussigne).eq(idMandataire)) {
			infoSignataire += datasSignature.cadreSignature.informationsMandataire.nomPrenomDenomination ;
			if (flgCompleteInfos === true) {
				infoSignataire += chgLigne ;
				infoSignataire += mergeAdresse(datasSignature.cadreSignature.informationsMandataire.cadreAdresse, 'T', true) ;
			}
		} else {
			infoSignataire += (datasIdentite.cadreIdentite.nomUsage ? datasIdentite.cadreIdentite.nomUsage + ' ' : datasIdentite.cadreIdentite.nomNaissance) + ' ' ;
			if (flgMergePrenoms === true) {
				infoSignataire += mergePrenoms(datasIdentite.cadreIdentite.prenoms) ;
			}
			if (flgCompleteInfos === true) {
				infoSignataire += chgLigne ;
				if (typeof datasIdentite.cadreContact.telephoneFixe.telephoneNum !== 'undefined' && datasIdentite.cadreContact.telephoneFixe.telephoneNum) {
					contactInfos.push(datasIdentite.cadreContact.telephoneFixe.telephoneNum) ;
				}
				if (typeof datasIdentite.cadreContact.telephoneMobile.telephoneNum !== 'undefined' && datasIdentite.cadreContact.telephoneMobile.telephoneNum) {
					contactInfos.push(datasIdentite.cadreContact.telephoneMobile.telephoneNum);
				}
				if (typeof datasIdentite.cadreContact.courriel !== 'undefined' && datasIdentite.cadreContact.courriel) {
					contactInfos.push(datasIdentite.cadreContact.courriel);
				}
				infoSignataire += contactInfos.toString() ;
			}
		}
	}
	return infoSignataire ;
}

/*
 * extraction d'informations liées à l'identité (nom, prénoms...)
 */

function extractInfoIdentite(blocIdentite, typeExtract, flgExtract) {
	var infoIdentite = '';
	if (typeof blocIdentite !== 'undefined' && typeof blocIdentite === 'object') {
		var typeExt = (typeExtract !== 'undefined' && typeof typeExtract === 'string') ? typeExtract.toUpperCase() : 'T' ;
		switch (typeExt) {
			case 'N':
			case 'L':
				infoIdentite = blocIdentite.nomUsage ? blocIdentite.nomUsage : (blocIdentite.nomNaissance ? blocIdentite.nomNaissance : '') ;
				break;
			case 'P':
			case 'F':
				infoIdentite = flgExtract ? mergePrenoms(blocIdentite.prenoms) : (blocIdentite.prenoms.length > 0 ? blocIdentite.prenoms[0] : '') ;
				break ;
			case 'C':
				infoIdentite = blocIdentite.civilite ? blocIdentite.civilite : '' ;
				break
			case 'X':
				infoIdentite += extractInfoIdentite(blocIdentite, 'N', flgExtract) ;
				infoIdentite += ' ' ;
				infoIdentite += extractInfoIdentite(blocIdentite, 'P', flgExtract) ;
				break;
			case 'T':
				infoIdentite  = extractInfoIdentite(blocIdentite, 'C', flgExtract) ;
				infoIdentite += ' ' ;
				infoIdentite += extractInfoIdentite(blocIdentite, 'N', flgExtract) ;
				infoIdentite += ' ' ;
				infoIdentite += extractInfoIdentite(blocIdentite, 'P', flgExtract) ;
				break;
			default:
				infoIdentite = '';
		}
	}
	return infoIdentite ;
}

/*
 * extraction d'informations liées aux adresses
 */
 
function extractInfoAdresse(blocAdresse, typeExtract, flgExtract) {
	var tmpDept = /(0[1-9]|[1-8][0-9]|2[A-B]|9[0-5]|9[7-8][0-9])(\d{2,3})/ ;
	var adresse = '' ;
	if (typeof blocAdresse !== 'undefined' && typeof blocAdresse === 'object') {
		var typeAct = (typeof typeExtract !== 'undefined' && typeof typeExtract === 'string') ? typeExtract.toUpperCase() : 'T' ;
		switch (typeAct) {
			case 'D':
				adresse = blocAdresse.commune ? blocAdresse.commune.id : '' ;
				adresse = adresse.replace(tmpDept, '$1') ;
				break;
			case 'P':
				if (typeof blocAdresse.ville !== 'undefined' && blocAdresse.ville) {
					adresse = blocAdresse.pays ;
				} else {
					if (flgExtract === true) {
						adresse = blocAdresse.pays ;
					}
				}
				break;
			case 'C':
				if (typeof blocAdresse.ville !== 'undefined' && blocAdresse.ville) {
					adresse = blocAdresse.ville ;
				} else {
					adresse = blocAdresse.commune ;
				}
				break;
			default:
		}
	}
	return adresse;
}

/*
 * extraction indicatif téléphone si indicatif non français (+33)
 */

function extractIndicatifEtrangerTelephone(objNumTel) {
	var tmpRegexTelIntro = /^(\+\d{1,3}[\s\.\-]?)(.*)$/ ;
	var tmpTel = '' ;
	var tmpIntro = '';
	if (typeof objNumTel !== 'undefined' && typeof objNumTel === 'object' && objNumTel) {
		if (tmpRegexTelIntro.test(objNumTel.international) === true) {
			tmpIntro = objNumTel.international.replace(tmpRegexTelIntro, '$1').trim() ;
			if (tmpIntro !== '+33') {
				tmpTel = tmpIntro ;
			}
		}
	}
	return tmpTel ;
}

/* merge de PJs
 * 
 */

function merge(pjs) {
	var mergedFile = nash.doc.create();
	pjs.forEach(function (elm) {		
		mergedFile.append(elm).save('item.pdf');
    });
	return mergedFile;
}

/* ajout de PJ
 * 
 */

function appendPj(fld, listPj) {
	fld.forEach(function (elm) {
        listPj.push(elm);
		_log.info('elm.getAbsolutePath() = >'+elm.getAbsolutePath());
		metas.push({'name':'userAttachments', 'value': '/'+elm.getAbsolutePath()});
		metas.push({'name':'document', 'value': '/'+elm.getAbsolutePath()});
    });
}

/* remplissage d'un cerfa à partir d'une table de paramétrage
 *
*/

function genereDynamicParams(tabParam, baseAddress) {
	var tmpTab = tabParam.replace(/root/gi, baseAddress);
	var tmpObj = [] ;
	try {
		tmpObj = eval(tmpTab) ;
	} catch(error) {
		tmpObj = [];
	}
  	return tmpObj;
}

function genereXDynamicParams(tabParam, baseAddress) {
	var tmpTab = '';
	var tmpVal ;
	var tmpFlg = false ;
	var tmpInd = 0;
	var tmpKey = {} ;
	var tmpObj = [] ;
	var tmpRegG = /root/gi ; // pour remplacer root. par le préfixe réel
	var tmpReg = [ /src\:[\s]*([^;\{\}]*)[;]*.*$/i, // pour isoler la valeur associée à src
				   /dst\:[\s]*([^;\{\}]*)[;]*.*$/i, // pour isoler la valeur associée à dst
				   /valeur\:[\s]*([^;\{\}]*)[;]*.*$/i, // pour isoler la valeur associée à value
				   /type\:[\s]*([^;\{\}]*)[;]*.*$/i ] ; // pour isoler la valeur associée à type
	var tmpLib = [ 'src', 'dst', 'valeur', 'type' ] ; // table des propriétés src, dst, value, type
	tabParam.forEach(function (elm) {
		try {
			tmpTab = elm.replace(tmpRegG, baseAddress);
			tmpKey = {} ;
			for (tmpInd = 0; tmpInd < tmpLib.length; tmpInd++) {
				tmpVal = tmpReg[tmpInd].exec(tmpTab) ;
				if (tmpVal !== null && tmpVal.length > 0) {
					tmpKey[tmpLib[tmpInd]] = eval(tmpVal[1].trim());
					tmpFlg = true ;
				} else {
					tmpFlg = false ;
					break;
				}
			}
			if (tmpFlg == true) {
				tmpObj.push(tmpKey) ;
			}
		} catch(error) {
			tmpFlg = false ;
		}
		return ;
    });
  	return tmpObj;
}

function completeCerfa(paramFields, valFields, indFields) {
	var tmpDst = '';
	var tmpResult = true ;
	var tmpRegexIndice = /\[i\]/gi ; // forme 'champ-cerfa[i] par exemple'
	var tmpRegexIndex = /\[x\]/gi ; // forme 'champ-cerfa[x] par exemple'
	var tmpBase = '' ;
	var tmpType = '' ;
	var tmpValue = '' ;
	var tmpFlag = false ;
	var objParam = [] ;
	
	if (typeof paramFields !== 'object' || !(Array.isArray(paramFields)) || typeof formFields !== 'object' || typeof indFields !== 'number') {
		return false;
	}
	paramFields.forEach(function (item) {
		if (typeof item.src === 'undefined') {
			return ;
		}
		tmpType = (typeof item.type !== undefined && typeof item.type === 'string') ? (item.type).toUpperCase() : '' ;
		if (tmpType === 'CARDINALITY' || tmpType === 'XCARDINALITY') {
			if (typeof item.src !== 'object' || item.src === null) {
				return ;
			}
			for (iCardinality = 0; iCardinality < (item.src).length; iCardinality++) {
				tmpBase = item.dst + '[' + iCardinality.toString() + ']' ;
				switch (tmpType) {
					case 'CARDINALITY':
						objParam = genereDynamicParams(item.valeur, tmpBase);
						break;
					case 'XCARDINALITY':
						objParam = genereXDynamicParams(item.valeur, tmpBase);
						break;
					default:
						objParam = [];
						break;
				}
				if (completeCerfa(objParam, valFields, iCardinality) == false) {
					tmpResult = false ;
				}
			}
			return ;
		}
		if (tmpType === 'COMPLEX' || tmpType === 'XCOMPLEX') {
			if (typeof item.src !== 'object') {
				return ;
			}
			(item.valeur).forEach(function (xtem) {
				switch (tmpType) {
					case 'COMPLEX':
						objParam = genereDynamicParams(xtem, item.dst);
						break;
					case 'XCOMPLEX':
						objParam = genereXDynamicParams(xtem, item.dst);
						break;
					default:
						objParam = [];
						break;
				}
				if (completeCerfa(objParam, valFields, 0) == false) {
					tmpResult = false ;
				}
				return ;
			});
			return ;
		}
		tmpDst = (item.dst).replace(tmpRegexIndice, (indFields + 1).toString()) ;
		tmpDst = tmpDst.replace(tmpRegexIndex, indFields.toString()) ;
		switch (tmpType) {
			case 'BOOL':
				var tmpFlg = typeof item.valeur === 'boolean' ? item.valeur : false ;
				if (item.src === null) {
					valFields[tmpDst] = tmpFlg ;
				} else {
					valFields[tmpDst] = item.src === tmpFlg ? true : false;
				}
				break;
			case 'XBOOL':
				if ((typeof item.src === 'boolean') && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					if (item.valeur[0] === item.valeur[1]) {
						if (item.src === false) {
							valFields[tmpDst] = null;
						}
					} else {
						valFields[tmpDst] = item.src;
					}
				}
				break;
			case 'RADIO':
				if (item.src) {
					valFields[tmpDst] = Value('id').of(item.src).eq(item.valeur) ? true : false;
				}
				break;
			case 'BRADIO':
				if ((typeof item.src === 'boolean' && item.src) && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					valFields[tmpDst] = Value('id').of(item.valeur[0]).eq(item.valeur[1]) ? true : false;
				}
				break;
			case 'XRADIO':
				if ((typeof item.src === 'boolean') && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					if (Value('id').of(item.valeur[0]).eq(item.valeur[1])) {
						if (item.src === false) {
							valFields[tmpDst] = null;
						}
					} else {
						valFields[tmpDst] = item.src;
					}
				}
				break;
			case 'CHECK':
				if (item.src) {
					valFields[tmpDst] = Value('id').of(item.src).contains(item.valeur) ? true : false;
				}
				break;
			case 'BCHECK':
				if ((typeof item.src === 'boolean' && item.src) && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					valFields[tmpDst] = Value('id').of(item.valeur[0]).contains(item.valeur[1]) ? true : false;
				}
				break;
			case 'XCHECK':
				if ((typeof item.src === 'boolean') && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					if (Value('id').of(item.valeur[0]).contains(item.valeur[1])) {
						if (item.src === false) {
							valFields[tmpDst] = null;
						}
					} else {
						valFields[tmpDst] = item.src;
					}
				}
				break;
			case 'STRING':
				if (item.valeur) {
					tmpValue = item.valeur + ' ' ;
					tmpFlag = true ;
				} else {
					tmpValue = '' ;
					tmpFlag = false ;
				}
				if (item.src) {
					tmpValue += item.src ;
					tmpFlag = true ;
				}
				if (tmpFlag) {
					valFields[tmpDst] = tmpValue ;
				}
				break;
			case 'BSTRING':
				if ((typeof item.src === 'boolean' && item.src) && (typeof item.valeur !== 'undefined' && item.valeur)) {
					valFields[tmpDst] = item.valeur ;
				}
				break;
			case 'XSTRING':
				if (((typeof item.src === 'string' or typeof item.src === 'object') && item.src) && (typeof item.valeur !== 'undefined' && item.valeur)) {
					valFields[tmpDst] = item.valeur ;
				}
				break;
			default:
				break;
		}
		return ;
	});
	return tmpResult ;
}

//--------------- FONCTIONS -------------------//

//--------------- MAIN GENERATION CERFA -------//

/* génération du Cerfa à partir de la table d'équivalences
 * (génération optionnelle)
 */

if (typeof formFields !== 'undefined' && typeof nameCerfa !== 'undefined') {
	var flgCerfa = true ;
	if (typeof equivCerfa !== 'undefined') { 
		flgCerfa = completeCerfa(equivCerfa, formFields, 0);
	}
	if (flgCerfa === true) {
		var cerfaDoc = nash.doc
			.load(repCerfa + nameCerfa)
			.apply(formFields);
		var pjCerfa = cerfaDoc.save(nameCerfa);
	}
}

/* ajout des PJ utilisateurs le cas échéant
 *
 */
 
if (typeof pjSignataire !== 'undefined') {
	if(Value('id').of(pjSignataire).contains(idDeclarant)) {
		appendPj($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantSignataire, pjUser);
	}
	if(Value('id').of(pjSignataire).contains(idMandataire)) {
		appendPj($attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire, pjUser);
	}
}

/* ajout des autres PJ le cas échéant
 *
 */

if($attachmentPreprocess.attachmentPreprocess.pjAutres !== 'undefined') {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjAutres, pjUser);
}

/* ajout du Cerfa en tant que PJ (s'il a été généré)
 *
 */
 
if (typeof pjCerfa !== 'undefined') {
	data.push(spec.createData({
			id: 'record',
			label: refFormTitle,
			description: 'Voici le formulaire obtenu à partir des données saisies.',
			type: 'FileReadOnly',
			value: [pjCerfa] 
	})) ;
}

/*
 * Enregistrement du fichier (en mémoire)
 */ 

var metasToDelete = [];
var recordMetas = nash.record.meta() != null ? nash.record.meta().metas : null;
var recordMetasSize = recordMetas != null ? recordMetas.size() : 0;

for (var i = 0; i < recordMetasSize; i++) {
    if (recordMetas.get(i).name == 'document') {
        metasToDelete.push({'name':'document', 'value': recordMetas.get(i).value});
    }
}
nash.record.removeMeta(metasToDelete);
nash.record.meta(metas);

data.push(spec.createData({
    id : 'uploaded',
    label : 'Pièces jointes',
    description : 'Pièces jointes ajoutées au formulaire.',
    type : 'FileReadOnly',
    value : [ merge(pjUser).save('saisine.pdf') ]
	})) ;

var groups = [ spec.createGroup({
    id : 'attachments',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Saisine du Ministère de l\'intérieur',
    groups : groups
});

//--------------- MAIN GENERATION CERFA -------//
