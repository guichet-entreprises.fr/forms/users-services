//--------------- VARIABLES -------------------//

/* init datas pour review (variables génériques)
 *
 */
 
var data = [] ;
var pjUser = [];
var metas = [];
var repCerfa = 'models/';
var iCardinality = 0 ;

/* init datas pour cerfa (variables spécifiques)
 * contient a minnima les références des groupes 
 * génériques de data.xml
 */

/*
 * init pointeurs saisies, Pj...
 */
 
var datasPtr = $ds058cerfa15312 ;
var datasSignature = datasPtr.cadreSignatureGroupe ;
var datasIdentite = datasPtr.cadreIdentiteGroupe ;
var refFormTitle = 'Administrateur de biens' ;

if (typeof datasPtr.cadreDemandeGroupe !== 'undefined') {
    var datasDemande = datasPtr.cadreDemandeGroupe ;
} else {
	var datasDemande = null ;
}

/*
 * init pointeurs PJ déclarants ou mandataires
 */
 
var pjSignataire = datasSignature.cadreSignature.soussigne ;
var idDeclarant = 'signataireQualiteDeclarant' ;
var idMandataire = 'signataireQualiteMandataire' ;

/* import description du CERFA (si il existe)
 * il génère les tables de paramétrage formFields et equivCerfa
 * formFields = dictionnaire des valeurs des champs du Cerfa
 * equivCerfa = dictionnaire de description des champs du Cerfa
 * il définit les variables repCerfa et nameCerfa
 * nameCerfa = nom du fichier pdf associé au Cerfa
 */

/*
 * Remplissage des champs du formulaire (autorisation de desossage)
 */

if (datasDemande !== null) {
	if ((Value('id').of(datasDemande.cadreDemande.nature).eq('depotDossier')) &&  (Value('id').of(datasDemande.cadreDemande.objet).eq(''))) {
		var flgCerfa = true;
	} else {
		var flgCerfa = false;
	}
} else {
	var flgCerfa = true;
}

if (flgCerfa === true) {
	
	//constantes et variables
	var formFields = {} ;
	var nameCerfa = 'cerfa_15312.pdf' ;
	var nonReceptionFonds = "Je déclare sur l'honneur que je ne reçois aucun fonds, effet ou valeur à l'occasion des opérations spécifiées "
		+ "par l'article 1 de la loi 70-9 du 2 janvier 1970 (à l'exception des activités mentionnées aux 6 et 9 de cet article)" ;
	//pointeurs sur les pages de saisie
	var datasInfosFormalite = datasPtr.infosFormalite ; //page 1 : formalité
	var datasInfosCarte = datasPtr.infosCarte ; //page 2A : informations demande carte
	var datasInfosDemandeInitiale = datasPtr.infosDemandeInitiale ; //page 2B : demande carte initiale
	var datasInfosDemandeRenouvellement = datasPtr.infosDemandeRenouvellement ; //page 2C : renouvellement carte
	var datasInfosModificationsPP = datasPtr.infosModificationsPP ; //page 2D : modifications PP
	var datasInfosModificationsPM = datasPtr.infosModificationsPM ; //page 2E : modifications PM
	var datasInfosActivite = datasPtr.infosActivite ; //page 3A : déclaration début activité
	
	//paramétrages des cardinalités et complex
	var setXPPRepresentant = [
		"{ src: root.personne.cadreIdentite.nomNaissance; dst: 'nomNaissancePP'; valeur: null; type: 'string' }",
		"{ src: root.personne.cadreIdentite.nomUsage; dst: 'nomUsagePP'; valeur: null; type: 'string' }",
		"{ src: null; dst: 'prenomsPP'; valeur: mergePrenoms(root.personne.cadreIdentite.prenoms); type: 'string' }",
		"{ src: null; dst: 'jourNaissancePP'; valeur: formatDate(root.personne.cadreNaissance.dateNaissance, 'J', true); type: 'string' }",
		"{ src: null; dst: 'moisNaissancePP'; valeur: formatDate(root.personne.cadreNaissance.dateNaissance, 'M', true); type: 'string' }",
		"{ src: null; dst: 'anneeNaissancePP'; valeur: formatDate(root.personne.cadreNaissance.dateNaissance, 'A', true); type: 'string' }",
		"{ src: root.personne.cadreNaissance.lieuNaissance; dst: 'villeNaissancePP'; valeur: null; type: 'string' }",
		"{ src: root.personne.cadreNaissance.paysNaissance; dst: 'paysNaissancePP'; valeur: null; type: 'string' }",
		"{ src: root.personne.cadreNaissance.nationalite; dst: 'nationalitePP'; valeur: null; type: 'string' }",
		"{ src: null; dst: 'adressePP'; valeur: mergeAdresse(root.personne.cadreAdresse, 'V', false); type: 'string' }",
		"{ src: null; dst: 'adresseSuitePP'; valeur: mergeAdresse(root.personne.cadreAdresse, 'X', false); type: 'string' }",
		"{ src: root.personne.profession; dst: 'professionPP'; valeur: null; type: 'string' }",
		"{ src: null; dst: 'adresseEtablissementPP'; valeur: mergeAdresse(root.etablissement.cadreAdresse, 'T', false); type: 'string' }",
		"{ src: root.etablissement.nomCommercial; dst: 'nomCommercialPP'; valeur: null; type: 'string' }",
		"{ src: root.etablissement.enseigne; dst: 'enseignePP'; valeur: null; type: 'string' }",
		"{ src: root.personne.cadreComplementNaissanceFr; dst: 'departementNaissancePP'; valeur: root.personne.cadreComplementNaissanceFr.departement; type: 'xString' }",
		"{ src: root.personne.cadreNaissance.cadreComplementNaissance; dst: 'perePP'; "
			+ "valeur: mergeNomPrenoms(root.personne.cadreNaissance.cadreComplementNaissance.nomPere, root.personne.cadreNaissance.cadreComplementNaissance.prenomsPere); type: 'xString' }",
		"{ src: root.personne.cadreComplementNaissanceFr; dst: 'perePP'; "
			+ "valeur: mergeNomPrenoms(root.personne.cadreComplementNaissanceFr.nomPere, root.personne.cadreComplementNaissanceFr.prenomsPere); type: 'xString' }",
		"{ src: root.personne.cadreNaissance.cadreComplementNaissance; dst: 'merePP'; "
			+ "valeur: mergeNomPrenoms(root.personne.cadreNaissance.cadreComplementNaissance.nomMere, root.personne.cadreNaissance.cadreComplementNaissance.prenomsMere); type: 'xString' }",
		"{ src: root.personne.cadreComplementNaissanceFr; dst: 'merePP'; "
			+ "valeur: mergeNomPrenoms(root.personne.cadreComplementNaissanceFr.nomMere, root.personne.cadreComplementNaissanceFr.prenomsMere); type: 'xString' }"
	] ;

	var setPMInformations = "[ "
		+ "{ src: root.informations.denomination, dst: 'denominationPM', valeur: null, type: 'string' },"
		+ "{ src: root.informations.formeJuridique, dst: 'formeJuridiquePM', valeur: null, type: 'string' },"
		+ "{ src: root.informations.objetSocial, dst: 'objetSocialPM', valeur: null, type: 'string' },"
		+ "{ src: root.informations.nomCommercial, dst: 'nomCommercialPM', valeur: null, type: 'string' },"
		+ "{ src: root.informations.enseigne, dst: 'enseignePM', valeur: null, type: 'string' },"
		+ "{ src: null, dst: 'adresseSiegePM', valeur: mergeAdresse(root.informations.cadreAdresse, 'T', false), type: 'string' }"
		+ " ] ;" ;

	var setPMxInformations = "[ "
		+ "{ src: root.informations.denomination, dst: 'denominationPMx', valeur: null, type: 'string' },"
		+ "{ src: root.informations.formeJuridique, dst: 'formeJuridiquePMx', valeur: null, type: 'string' },"
		+ "{ src: root.informations.objetSocial, dst: 'objetSocialPMx', valeur: null, type: 'string' },"
		+ "{ src: root.informations.nomCommercial, dst: 'nomCommercialPMx', valeur: null, type: 'string' },"
		+ "{ src: root.informations.enseigne, dst: 'enseignePMx', valeur: null, type: 'string' },"
		+ "{ src: null, dst: 'adresseSiegePMx', valeur: mergeAdresse(root.informations.cadreAdresse, 'T', false), type: 'string' }"
		+ " ] ;" ;	

	var setXPMNouveauRepresentant = [
		"{ src: root.nouveau.qualite; dst: 'nouveauRepresentantPM[i]'; valeur: true; type: 'xString' }",
		"{ src: root.nouveau.cadreIdentite.nomNaissance; dst: 'nomNaissanceRepresentantPM[i]'; valeur: null; type: 'string' }",
		"{ src: root.nouveau.cadreIdentite.nomUsage; dst: 'nomUsageRepresentantPM[i]'; valeur: null; type: 'string' }",
		"{ src: null; dst: 'prenomsRepresentantPM[i]'; valeur: mergePrenoms(root.nouveau.cadreIdentite.prenoms); type: 'string' }",
		"{ src: null; dst: 'jourNaissanceRepresentantPM[i]'; valeur: formatDate(root.nouveau.cadreNaissance.dateNaissance, 'J', true); type: 'string' }",
		"{ src: null; dst: 'moisNaissanceRepresentantPM[i]'; valeur: formatDate(root.nouveau.cadreNaissance.dateNaissance, 'M', true); type: 'string' }",
		"{ src: null; dst: 'anneeNaissanceRepresentantPM[i]'; valeur: formatDate(root.nouveau.cadreNaissance.dateNaissance, 'A', true); type: 'string' }",
		"{ src: root.nouveau.cadreNaissance.lieuNaissance; dst: 'villeNaissanceRepresentantPM[i]'; valeur: null; type: 'string' }",
		"{ src: root.nouveau.cadreNaissance.paysNaissance; dst: 'paysNaissanceRepresentantPM[i]'; valeur: null; type: 'string' }",
		"{ src: root.nouveau.cadreNaissance.nationalite; dst: 'nationaliteRepresentantPM[i]'; valeur: null; type: 'string' }",
		"{ src: null; dst: 'adresseRepresentantPM[i]'; valeur: mergeAdresse(root.nouveau.cadreAdresse, 'V', false); type: 'string' }",
		"{ src: null; dst: 'adresseSuiteRepresentantPM[i]'; valeur: mergeAdresse(root.nouveau.cadreAdresse, 'X', false); type: 'string' }",
		"{ src: root.nouveau.qualite; dst: 'qualiteRepresentantPM[i]'; valeur: null; type: 'string' }",
		"{ src: root.nouveau.profession; dst: 'professionRepresentantPM[i]'; valeur: null; type: 'string' }",
		"{ src: root.nouveau.cadreComplementNaissanceFr; dst: 'departementNaissanceRepresentantPM[i]'; valeur: root.nouveau.cadreComplementNaissanceFr.departement; type: 'xString' }",
		"{ src: root.nouveau.cadreNaissance.cadreComplementNaissance; dst: 'pereRepresentantPM[i]'; "
			+ "valeur: mergeNomPrenoms(root.nouveau.cadreNaissance.cadreComplementNaissance.nomPere, root.nouveau.cadreNaissance.cadreComplementNaissance.prenomsPere); type: 'xString' }",
		"{ src: root.nouveau.cadreComplementNaissanceFr; dst: 'pereRepresentantPM[i]'; "
			+ "valeur: mergeNomPrenoms(root.nouveau.cadreComplementNaissanceFr.nomPere, root.nouveau.cadreComplementNaissanceFr.prenomsPere); type: 'xString' }",
		"{ src: root.nouveau.cadreNaissance.cadreComplementNaissance; dst: 'mereRepresentantPM[i]'; "
			+ "valeur: mergeNomPrenoms(root.nouveau.cadreNaissance.cadreComplementNaissance.nomMere, root.nouveau.cadreNaissance.cadreComplementNaissance.prenomsMere); type: 'xString' }",
		"{ src: root.nouveau.cadreComplementNaissanceFr; dst: 'mereRepresentantPM[i]'; "
			+ "valeur: mergeNomPrenoms(root.nouveau.cadreComplementNaissanceFr.nomMere, root.nouveau.cadreComplementNaissanceFr.prenomsMere); type: 'xString' }"
	];

	var setPMPartantRestantRepresentant = "[ "
		+ "{ src: root.typeMaj, dst: 'partantRepresentantPM[i]', valeur: 'typePartant', type: 'check' },"
		+ "{ src: root.typeMaj, dst: 'restantRepresentantPM[i]', valeur: 'typeRestant', type: 'check' },"
		+ "{ src: root.partantRestant.cadreIdentite.nomNaissance, dst: 'nomNaissanceRepresentantPM[i]', valeur: null, type: 'string' },"
		+ "{ src: root.partantRestant.cadreIdentite.nomUsage, dst: 'nomUsageRepresentantPM[i]', valeur: null, type: 'string' },"
		+ "{ src: null, dst: 'prenomsRepresentantPM[i]', valeur: mergePrenoms(root.partantRestant.cadreIdentite.prenoms), type: 'string' }"
		+ " ] ;" ;
	
	var setXPMxNouveauRepresentant = [
		"{ src: root.nouveau.qualite; dst: 'nouveauRepresentantPMx'; valeur: true; type: 'xString' }",
		"{ src: root.nouveau.cadreIdentite.nomNaissance; dst: 'nomNaissanceRepresentantPMx'; valeur: null; type: 'string' }",
		"{ src: root.nouveau.cadreIdentite.nomUsage; dst: 'nomUsageRepresentantPMx'; valeur: null; type: 'string' }",
		"{ src: null; dst: 'prenomsRepresentantPMx'; valeur: mergePrenoms(root.nouveau.cadreIdentite.prenoms); type: 'string' }",
		"{ src: null; dst: 'jourNaissanceRepresentantPMx'; valeur: formatDate(root.nouveau.cadreNaissance.dateNaissance, 'J', true); type: 'string' }",
		"{ src: null; dst: 'moisNaissanceRepresentantPMx'; valeur: formatDate(root.nouveau.cadreNaissance.dateNaissance, 'M', true); type: 'string' }",
		"{ src: null; dst: 'anneeNaissanceRepresentantPMx'; valeur: formatDate(root.nouveau.cadreNaissance.dateNaissance, 'A', true); type: 'string' }",
		"{ src: root.nouveau.cadreNaissance.lieuNaissance; dst: 'villeNaissanceRepresentantPMx'; valeur: null; type: 'string' }",
		"{ src: root.nouveau.cadreNaissance.paysNaissance; dst: 'paysNaissanceRepresentantPMx'; valeur: null; type: 'string' }",
		"{ src: root.nouveau.cadreNaissance.nationalite; dst: 'nationaliteRepresentantPMx'; valeur: null; type: 'string' }",
		"{ src: null; dst: 'adresseRepresentantPMx'; valeur: mergeAdresse(root.nouveau.cadreAdresse, 'V', false); type: 'string' }",
		"{ src: null; dst: 'adresseSuiteRepresentantPMx'; valeur: mergeAdresse(root.nouveau.cadreAdresse, 'X', false); type: 'string' }",
		"{ src: root.nouveau.qualite; dst: 'qualiteRepresentantPMx'; valeur: null; type: 'string' }",
		"{ src: root.nouveau.profession; dst: 'professionRepresentantPMx'; valeur: null; type: 'string' }",
		"{ src: root.nouveau.cadreComplementNaissanceFr; dst: 'departementNaissanceRepresentantPMx'; valeur: root.nouveau.cadreComplementNaissanceFr.departement; type: 'xString' }",
		"{ src: root.nouveau.cadreNaissance.cadreComplementNaissance; dst: 'pereRepresentantPMx'; "
			+ "valeur: mergeNomPrenoms(root.nouveau.cadreNaissance.cadreComplementNaissance.nomPere, root.nouveau.cadreNaissance.cadreComplementNaissance.prenomsPere); type: 'xString' }",
		"{ src: root.nouveau.cadreComplementNaissanceFr; dst: 'pereRepresentantPMx'; "
			+ "valeur: mergeNomPrenoms(root.nouveau.cadreComplementNaissanceFr.nomPere, root.nouveau.cadreComplementNaissanceFr.prenomsPere); type: 'xString' }",
		"{ src: root.nouveau.cadreNaissance.cadreComplementNaissance; dst: 'mereRepresentantPMx'; "
			+ "valeur: mergeNomPrenoms(root.nouveau.cadreNaissance.cadreComplementNaissance.nomMere, root.nouveau.cadreNaissance.cadreComplementNaissance.prenomsMere); type: 'xString' }",
		"{ src: root.nouveau.cadreComplementNaissanceFr; dst: 'mereRepresentantPMx'; "
			+ "valeur: mergeNomPrenoms(root.nouveau.cadreComplementNaissanceFr.nomMere, root.nouveau.cadreComplementNaissanceFr.prenomsMere); type: 'xString' }"
	] ;
	
	var setPMxPartantRestantRepresentant = "[ "
		+ "{ src: root.typeMaj, dst: 'partantRepresentantPMx', valeur: 'typePartant', type: 'check' },"
		+ "{ src: root.typeMaj, dst: 'restantRepresentantPMx', valeur: 'typeRestant', type: 'check' },"
		+ "{ src: root.partantRestant.cadreIdentite.nomNaissance, dst: 'nomNaissanceRepresentantPMx', valeur: null, type: 'string' },"
		+ "{ src: root.partantRestant.cadreIdentite.nomUsage, dst: 'nomUsageRepresentantPMx', valeur: null, type: 'string' },"
		+ "{ src: null, dst: 'prenomsRepresentantPMx', valeur: mergePrenoms(root.partantRestant.cadreIdentite.prenoms), type: 'string' }"
		+ " ] ;" ;
		
	var setXDirecteurEP = [
		"{ src: root.typeMaj; dst: 'nouveauDirecteurEP'; valeur: 'typeNouveau'; type: 'check' }",
		"{ src: root.typeMaj; dst: 'partantDirecteurEP'; valeur: 'typePartant'; type: 'check' }",
		"{ src: root.nouveau.cadreIdentite.nomNaissance; dst: 'nomNaissanceNouveauDirecteurEP'; valeur: null; type: 'string' }",
		"{ src: root.nouveau.cadreIdentite.nomUsage; dst: 'nomUsageNouveauDirecteurEP'; valeur: null; type: 'string' }",
		"{ src: null; dst: 'prenomsNouveauDirecteurEP'; valeur: mergePrenoms(root.nouveau.cadreIdentite.prenoms); type: 'string' }",
		"{ src: null; dst: 'jourNaissanceNouveauDirecteurEP'; valeur: formatDate(root.nouveau.cadreNaissance.dateNaissance, 'J', true); type: 'string' }",
		"{ src: null; dst: 'moisNaissanceNouveauDirecteurEP'; valeur: formatDate(root.nouveau.cadreNaissance.dateNaissance, 'M', true); type: 'string' }",
		"{ src: null; dst: 'anneeNaissanceNouveauDirecteurEP'; valeur: formatDate(root.nouveau.cadreNaissance.dateNaissance, 'A', true); type: 'string' }",
		"{ src: root.nouveau.cadreNaissance.lieuNaissance; dst: 'villeNaissanceNouveauDirecteurEP'; valeur: null; type: 'string' }",
		"{ src: root.nouveau.cadreNaissance.paysNaissance; dst: 'paysNaissanceNouveauDirecteurEP'; valeur: null; type: 'string' }",
		"{ src: root.nouveau.cadreNaissance.nationalite; dst: 'nationaliteNouveauDirecteurEP'; valeur: null; type: 'string' }",
		"{ src: null; dst: 'adresseNouveauDirecteurEP'; valeur: mergeAdresse(root.nouveau.cadreAdresse, 'V', false); type: 'string' }",
		"{ src: null; dst: 'adresseSuiteNouveauDirecteurEP'; valeur: mergeAdresse(root.nouveau.cadreAdresse, 'X', false); type: 'string' }",
		"{ src: root.nouveau.qualite; dst: 'qualiteNouveauDirecteurEP'; valeur: null; type: 'string' }",
		"{ src: root.nouveau.profession; dst: 'professionNouveauDirecteurEP'; valeur: null; type: 'string' }",
		"{ src: root.partant.cadreIdentite.nomNaissance; dst: 'nomNaissancePartantDirecteurEP'; valeur: null; type: 'string' }",
		"{ src: root.partant.cadreIdentite.nomUsage; dst: 'nomUsagePartantDirecteurEP'; valeur: null; type: 'string' }",
		"{ src: null; dst: 'prenomsPartantDirecteurEP'; valeur: mergePrenoms(root.partant.cadreIdentite.prenoms); type: 'string' }",
		"{ src: root.nouveau.cadreComplementNaissanceFr; dst: 'departementNaissanceNouveauDirecteurEP'; valeur: root.nouveau.cadreComplementNaissanceFr.departement; type: 'xString' }",
		"{ src: root.nouveau.cadreNaissance.cadreComplementNaissance; dst: 'pereNouveauDirecteurEP'; "
			+ "valeur: mergeNomPrenoms(root.nouveau.cadreNaissance.cadreComplementNaissance.nomPere, root.nouveau.cadreNaissance.cadreComplementNaissance.prenomsPere); type: 'xString' }",
		"{ src: root.nouveau.cadreComplementNaissanceFr; dst: 'pereNouveauDirecteurEP'; "
			+ "valeur: mergeNomPrenoms(root.nouveau.cadreComplementNaissanceFr.nomPere, root.nouveau.cadreComplementNaissanceFr.prenomsPere); type: 'xString' }",
		"{ src: root.nouveau.cadreNaissance.cadreComplementNaissance; dst: 'mereNouveauDirecteurEP'; "
			+ "valeur: mergeNomPrenoms(root.nouveau.cadreNaissance.cadreComplementNaissance.nomMere, root.nouveau.cadreNaissance.cadreComplementNaissance.prenomsMere); type: 'xString' }",
		"{ src: root.nouveau.cadreComplementNaissanceFr; dst: 'mereNouveauDirecteurEP'; "
			+ "valeur: mergeNomPrenoms(root.nouveau.cadreComplementNaissanceFr.nomMere, root.nouveau.cadreComplementNaissanceFr.prenomsMere); type: 'xString' }"
	] ;

	var setXDirecteurES = [
		"{ src: root.typeMaj; dst: 'nouveauDirecteurES'; valeur: 'typeNouveau'; type: 'check' }",
		"{ src: root.typeMaj; dst: 'partantDirecteurES'; valeur: 'typePartant'; type: 'check' }",
		"{ src: root.nouveau.cadreIdentite.nomNaissance; dst: 'nomNaissanceNouveauDirecteurES'; valeur: null; type: 'string' }",
		"{ src: root.nouveau.cadreIdentite.nomUsage; dst: 'nomUsageNouveauDirecteurES'; valeur: null; type: 'string' }",
		"{ src: null; dst: 'prenomsNouveauDirecteurES'; valeur: mergePrenoms(root.nouveau.cadreIdentite.prenoms); type: 'string' }",
		"{ src: null; dst: 'jourNaissanceNouveauDirecteurES'; valeur: formatDate(root.nouveau.cadreNaissance.dateNaissance, 'J', true); type: 'string' }",
		"{ src: null; dst: 'moisNaissanceNouveauDirecteurES'; valeur: formatDate(root.nouveau.cadreNaissance.dateNaissance, 'M', true); type: 'string' }",
		"{ src: null; dst: 'anneeNaissanceNouveauDirecteurES'; valeur: formatDate(root.nouveau.cadreNaissance.dateNaissance, 'A', true); type: 'string' }",
		"{ src: root.nouveau.cadreNaissance.lieuNaissance; dst: 'villeNaissanceNouveauDirecteurES'; valeur: null; type: 'string' }",
		"{ src: root.nouveau.cadreNaissance.paysNaissance; dst: 'paysNaissanceNouveauDirecteurES'; valeur: null; type: 'string' }",
		"{ src: root.nouveau.cadreNaissance.nationalite; dst: 'nationaliteNouveauDirecteurES'; valeur: null; type: 'string' }",
		"{ src: null; dst: 'adresseNouveauDirecteurES'; valeur: mergeAdresse(root.nouveau.cadreAdresse, 'V', false); type: 'string' }",
		"{ src: null; dst: 'adresseSuiteNouveauDirecteurES'; valeur: mergeAdresse(root.nouveau.cadreAdresse, 'X', false); type: 'string' }",
		"{ src: root.nouveau.qualite; dst: 'qualiteNouveauDirecteurES'; valeur: null; type: 'string' }",
		"{ src: root.nouveau.profession; dst: 'professionNouveauDirecteurES'; valeur: null; type: 'string' }",
		"{ src: root.partant.cadreIdentite.nomNaissance; dst: 'nomNaissancePartantDirecteurES'; valeur: null; type: 'string' }",
		"{ src: root.partant.cadreIdentite.nomUsage; dst: 'nomUsagePartantDirecteurES'; valeur: null; type: 'string' }",
		"{ src: null; dst: 'prenomsPartantDirecteurES'; valeur: mergePrenoms(root.partant.cadreIdentite.prenoms); type: 'string' }",
		"{ src: root.nouveau.cadreComplementNaissanceFr; dst: 'departementNaissanceNouveauDirecteurES'; valeur: root.nouveau.cadreComplementNaissanceFr.departement; type: 'xString' }",
		"{ src: root.nouveau.cadreNaissance.cadreComplementNaissance; dst: 'pereNouveauDirecteurES'; "
			+ "valeur: mergeNomPrenoms(root.nouveau.cadreNaissance.cadreComplementNaissance.nomPere, root.nouveau.cadreNaissance.cadreComplementNaissance.prenomsPere); type: 'xString' }",
		"{ src: root.nouveau.cadreComplementNaissanceFr; dst: 'pereNouveauDirecteurES'; "
			+ "valeur: mergeNomPrenoms(root.nouveau.cadreComplementNaissanceFr.nomPere, root.nouveau.cadreComplementNaissanceFr.prenomsPere); type: 'xString' }",
		"{ src: root.nouveau.cadreNaissance.cadreComplementNaissance; dst: 'mereNouveauDirecteurES'; "
			+ "valeur: mergeNomPrenoms(root.nouveau.cadreNaissance.cadreComplementNaissance.nomMere, root.nouveau.cadreNaissance.cadreComplementNaissance.prenomsMere); type: 'xString' }",
		"{ src: root.nouveau.cadreComplementNaissanceFr; dst: 'mereNouveauDirecteurES'; "
			+ "valeur: mergeNomPrenoms(root.nouveau.cadreComplementNaissanceFr.nomMere, root.nouveau.cadreComplementNaissanceFr.prenomsMere); type: 'xString' }"
	] ;
		
	var setGarantie = "[ "
		+ "{ src: root.activites, dst: 'transactionGarantieFinanciere[i]', valeur: 'transactionsMentions', type: 'check' },"
		+ "{ src: root.activites, dst: 'syndicGarantieFinanciere[i]', valeur: 'syndicMentions', type: 'check' },"
		+ "{ src: root.activites, dst: 'gestionImmobiliereGarantieFinanciere[i]', valeur: 'gestionImmobiliereMentions', type: 'check' },"
		+ "{ src: root.activites, dst: 'marchandListesGarantieFinanciere[i]', valeur: 'marchandListesMentions', type: 'check' },"
		+ "{ src: root.activites, dst: 'prestationsTouristiquesGarantieFinanciere[i]', valeur: 'prestationsTouristiquesMentions', type: 'check' },"
		+ "{ src: root.activites, dst: 'prestationsServicesGarantieFinanciere[i]', valeur: 'prestationsServicesMentions', type: 'check' },"
		+ "{ src: root.denomination, dst: 'denominationGarantieFinanciere[i]', valeur: null, type: 'string' },"
		+ "{ src: root.montant, dst: 'montantGarantieFinanciere[i]', valeur: null, type: 'string' },"
		+ "{ src: null, dst: 'adresseGarantieFinanciere[i]', valeur: mergeAdresse(root.cadreAdresse, 'V', false), type: 'string' },"
		+ "{ src: null, dst: 'adresseSuiteGarantieFinanciere[i]', valeur: mergeAdresse(root.cadreAdresse, 'X', false), type: 'string' }"
		+ " ] ;" ;

	var setAssurance = "[ "
		+ "{ src: root.activites, dst: 'transactionResponsabiliteCivile[i]', valeur: 'transactionsMentions', type: 'check' },"
		+ "{ src: root.activites, dst: 'syndicResponsabiliteCivile[i]', valeur: 'syndicMentions', type: 'check' },"
		+ "{ src: root.activites, dst: 'gestionImmobiliereResponsabiliteCivile[i]', valeur: 'gestionImmobiliereMentions', type: 'check' },"
		+ "{ src: root.activites, dst: 'marchandListesResponsabiliteCivile[i]', valeur: 'marchandListesMentions', type: 'check' },"
		+ "{ src: root.activites, dst: 'prestationsTouristiquesResponsabiliteCivile[i]', valeur: 'prestationsTouristiquesMentions', type: 'check' },"
		+ "{ src: root.activites, dst: 'prestationsServicesResponsabiliteCivile[i]', valeur: 'prestationsServicesMentions', type: 'check' },"
		+ "{ src: root.denomination, dst: 'denominationResponsabiliteCivile[i]', valeur: null, type: 'string' },"
		+ "{ src: null, dst: 'adresseResponsabiliteCivile[i]', valeur: mergeAdresse(root.cadreAdresse, 'T', false), type: 'string' }"
		+ " ] ;" ;
		
	var setBanque = "[ "
		+ "{ src: root.denomination, dst: 'denominationCompteBancaire[i]', valeur: null, type: 'string' },"
		+ "{ src: root.compte, dst: 'numeroCompteBancaire[i]', valeur: null, type: 'string' },"
		+ "{ src: null, dst: 'adresseCompteBancaire[i]', valeur: mergeAdresse(root.cadreAdresse, 'V', false), type: 'string' },"
		+ "{ src: null, dst: 'adresseSuiteCompteBancaire[i]', valeur: mergeAdresse(root.cadreAdresse, 'X', false), type: 'string' }"
		+ " ] ;" ;

	//paramétrages des types complex
	var listPPRepresentant = [ setXPPRepresentant ] ;
	var listPMxNouveauRepresentant = [ setXPMxNouveauRepresentant ] ;
	var listPMxPartantRestantRepresentant = [ setPMxPartantRestantRepresentant ] ;
	var listPMInformations = [ setPMInformations ] ;
	var listPMxInformations = [ setPMxInformations ] ;
	var listDirecteurEP = [ setXDirecteurEP ] ;
	var listDirecteurES = [ setXDirecteurES ] ;
	
	//description des zones génériques du Cerfa (type bool, type string...)
	var equivCerfa = [ 
		//page 1 : formalité
		{ src: datasInfosFormalite.typeFormalite, dst: 'carteProfessionnelle', valeur: 'carteProfessionnelle', type: 'radio' },
		{ src: datasInfosFormalite.typeFormalite, dst: 'declarationActivite', valeur: 'declarationActivite', type: 'radio' },
		{ src: !datasInfosFormalite.nonReceptionFonds, dst: 'nonReceptionFonds', valeur: nonReceptionFonds, type: 'bString' },
		{ src: null, dst: 'adresseContact', valeur: mergeAdresse(datasIdentite.cadreAdresse, 'T', false), type: 'string' },
		{ src: null, dst: 'emailContact', valeur: datasIdentite.cadreContact.courriel, type: 'string' },
		{ src: null, dst: 'telephoneContact', valeur: mergeNumTelephone(datasIdentite.cadreContact, true, true), type: 'string' },
		{ src: datasInfosFormalite.contactFormaliteDifferent, dst: 'adresseContact', valeur: mergeAdresse(datasInfosFormalite.contactFormalite.cadreAdresse, 'T', false), type: 'bString' },
		{ src: datasInfosFormalite.contactFormaliteDifferent, dst: 'emailContact', valeur: datasInfosFormalite.contactFormalite.cadreContact.courriel, type: 'bString' },
		{ src: datasInfosFormalite.contactFormaliteDifferent, dst: 'telephoneContact', valeur: mergeNumTelephone(datasInfosFormalite.contactFormalite.cadreContact, true, true), type: 'bString' },
		{ src: datasInfosFormalite.observations, dst: 'observations', valeur: null, type: 'string' },
		{ src: null, dst: 'siren', valeur: supprimeEspaces(datasInfosFormalite.siren), type: 'string' },
		//page 2A : informations demande carte
		{ src: datasInfosCarte.perimetre, dst: 'demandeInitialeCarte', valeur: 'demandeInitialeCarte', type: 'radio' },
		{ src: datasInfosCarte.perimetre, dst: 'renouvellementCarte', valeur: 'renouvellementCarte', type: 'radio' },
		{ src: datasInfosCarte.perimetre, dst: 'modificationPPCarte', valeur: 'modificationPPCarte', type: 'radio' },
		{ src: datasInfosCarte.perimetre, dst: 'modificationPMCarte', valeur: 'modificationPMCarte', type: 'radio' },
		{ src: datasInfosCarte.infosCarteDelivree.numeroCarte, dst: 'numeroCarte', valeur: null, type: 'string' },
		{ src: datasInfosCarte.infosCarteDelivree.lieuCarte, dst: 'lieuCarte', valeur: null, type: 'string' },
		{ src: null, dst: 'jourFinValiditeCarte', valeur: formatDate(datasInfosCarte.infosCarteDelivree.dateValiditeCarte, 'J', true), type: 'string' },
		{ src: null, dst: 'moisFinValiditeCarte', valeur: formatDate(datasInfosCarte.infosCarteDelivree.dateValiditeCarte, 'M', true), type: 'string' },
		{ src: null, dst: 'anneeFinValiditeCarte', valeur: formatDate(datasInfosCarte.infosCarteDelivree.dateValiditeCarte, 'A', true), type: 'string' },	
		{ src: datasInfosCarte.mentionsCarte.activites, dst: 'transactionsMentions', valeur: 'transactionsMentions', type: 'check' },
		{ src: datasInfosCarte.mentionsCarte.activites, dst: 'syndicMentions', valeur: 'syndicMentions', type: 'check' },
		{ src: datasInfosCarte.mentionsCarte.activites, dst: 'gestionImmobiliereMentions', valeur: 'gestionImmobiliereMentions', type: 'check' },
		{ src: datasInfosCarte.mentionsCarte.activites, dst: 'marchandListesMentions', valeur: 'marchandListesMentions', type: 'check' },
		{ src: datasInfosCarte.mentionsCarte.activites, dst: 'prestationsTouristiquesMentions', valeur: 'prestationsTouristiquesMentions', type: 'check' },
		{ src: datasInfosCarte.mentionsCarte.activites, dst: 'prestationsServicesMentions', valeur: 'prestationsServicesMentions', type: 'check' },
		//page 2B : demande carte initiale
		{ src: datasInfosDemandeInitiale.pp, dst: 'datasInfosDemandeInitiale.pp', valeur: listPPRepresentant, type: 'xComplex' },
		{ src: datasInfosDemandeInitiale.pm, dst: 'datasInfosDemandeInitiale.pm', valeur: listPMInformations, type: 'complex' },
		{ src: datasInfosDemandeInitiale.pm.representantsLegaux, dst: 'datasInfosDemandeInitiale.pm.representantsLegaux', valeur: setXPMNouveauRepresentant, type: 'xCardinality' },
		{ src: datasInfosDemandeInitiale.pm.pmX, dst: 'datasInfosDemandeInitiale.pm.pmX', valeur: listPMxInformations, type: 'complex' },
		{ src: datasInfosDemandeInitiale.pm.pmX, dst: 'datasInfosDemandeInitiale.pm.pmX', valeur: listPMxNouveauRepresentant, type: 'xComplex' },
		//page 2C : renouvellement carte
		{ src: datasInfosDemandeRenouvellement.pp, dst: 'datasInfosDemandeRenouvellement.pp', valeur: listPPRepresentant, type: 'xComplex' },
		{ src: datasInfosDemandeRenouvellement.pm, dst: 'datasInfosDemandeRenouvellement.pm', valeur: listPMInformations, type: 'complex' },
		{ src: datasInfosDemandeRenouvellement.pm.representantsLegaux, dst: 'datasInfosDemandeRenouvellement.pm.representantsLegaux', valeur: setXPMNouveauRepresentant, type: 'xCardinality' },
		{ src: datasInfosDemandeRenouvellement.pm.representantsLegaux, dst: 'datasInfosDemandeRenouvellement.pm.representantsLegaux', valeur: setPMPartantRestantRepresentant, type: 'cardinality' },
		{ src: datasInfosDemandeRenouvellement.pm.pmX, dst: 'datasInfosDemandeRenouvellement.pm.pmX', valeur: listPMxInformations, type: 'complex' },
		{ src: datasInfosDemandeRenouvellement.pm.pmX, dst: 'datasInfosDemandeRenouvellement.pm.pmX', valeur: listPMxNouveauRepresentant, type: 'xComplex' },
		{ src: datasInfosDemandeRenouvellement.pm.pmX, dst: 'datasInfosDemandeRenouvellement.pm.pmX', valeur: listPMxPartantRestantRepresentant, type: 'complex' },
		//page 2D : modifications PP
		{ src: datasInfosModificationsPP.type, dst: 'adresseEtablissementPPCarte', valeur: 'adresseEtablissementPPCarte', type: 'check' },
		{ src: datasInfosModificationsPP.type, dst: 'directeurEtablissementPPCarte', valeur: 'directeurEtablissementPPCarte', type: 'check' },
		{ src: datasInfosModificationsPP.type, dst: 'assurancePPCarte', valeur: 'assurancePPCarte', type: 'check' },
		{ src: datasInfosModificationsPP.type, dst: 'garantiePPCarte', valeur: 'garantiePPCarte', type: 'check' },
		{ src: datasInfosModificationsPP.type, dst: 'sequestrePPCarte', valeur: 'sequestrePPCarte', type: 'check' },
		{ src: datasInfosModificationsPP.adresseEtablissement, dst: 'adresseEtablissementPP', valeur: mergeAdresse(datasInfosModificationsPP.adresseEtablissement.cadreAdresse, 'T', false), type: 'xString' },
		{ src: datasInfosModificationsPP.directeurEtablissement, dst: 'datasInfosModificationsPP.directeurEtablissement', valeur: listDirecteurEP, type: 'xComplex' },
		{ src: datasInfosModificationsPP.garanties.garantie, dst: 'datasInfosModificationsPP.garanties.garantie', valeur: setGarantie, type: 'cardinality' },
		{ src: datasInfosModificationsPP.assurances.assurance, dst: 'datasInfosModificationsPP.assurances.assurance', valeur: setAssurance, type: 'cardinality' },
		{ src: datasInfosModificationsPP.banques.banque, dst: 'datasInfosModificationsPP.banques.banque', valeur: setBanque, type: 'cardinality' },
		//page 2E : modifications PM
		{ src: datasInfosModificationsPM.type, dst: 'denominationPMCarte', valeur: 'denominationPMCarte', type: 'check' },
		{ src: datasInfosModificationsPM.type, dst: 'formeJuridiquePMCarte', valeur: 'formeJuridiquePMCarte', type: 'check' },
		{ src: datasInfosModificationsPM.type, dst: 'adresseSiegePMCarte', valeur: 'adresseSiegePMCarte', type: 'check' },
		{ src: datasInfosModificationsPM.type, dst: 'representantLegalPMCarte', valeur: 'representantLegalPMCarte', type: 'check' },
		{ src: datasInfosModificationsPM.type, dst: 'directeurEtablissementPMCarte', valeur: 'directeurEtablissementPMCarte', type: 'check' },
		{ src: datasInfosModificationsPM.type, dst: 'assurancePMCarte', valeur: 'assurancePMCarte', type: 'check' },
		{ src: datasInfosModificationsPM.type, dst: 'garantiePMCarte', valeur: 'garantiePMCarte', type: 'check' },
		{ src: datasInfosModificationsPM.type, dst: 'sequestrePMCarte', valeur: 'sequestrePMCarte', type: 'check' },
		{ src: datasInfosModificationsPM.denominationPM, dst: 'denominationPM', valeur: null, type: 'string' },
		{ src: datasInfosModificationsPM.formeJuridiquePM, dst: 'formeJuridiquePM', valeur: null, type: 'string' },
		{ src: datasInfosModificationsPM.adresseSiegePM, dst: 'adresseSiegePM', valeur: mergeAdresse(datasInfosModificationsPM.adresseSiegePM.cadreAdresse, 'T', false), type: 'xString' },
		{ src: datasInfosModificationsPP.directeurEtablissement, dst: 'datasInfosModificationsPM.directeurEtablissement', valeur: listDirecteurEP, type: 'xComplex' },
		{ src: datasInfosModificationsPM.representants.representantsLegaux, dst: 'datasInfosModificationsPM.representants.representantsLegaux', valeur: setXPMNouveauRepresentant, type: 'xCardinality' },
		{ src: datasInfosModificationsPM.representants.representantsLegaux, dst: 'datasInfosModificationsPM.representants.representantsLegaux', valeur: setPMPartantRestantRepresentant, type: 'cardinality' },
		{ src: datasInfosModificationsPM.garanties.garantie, dst: 'datasInfosModificationsPM.garanties.garantie', valeur: setGarantie, type: 'cardinality' },
		{ src: datasInfosModificationsPM.assurances.assurance, dst: 'datasInfosModificationsPM.assurances.assurance', valeur: setAssurance, type: 'cardinality' },
		{ src: datasInfosModificationsPM.banques.banque, dst: 'datasInfosModificationsPM.banques.banque', valeur: setBanque, type: 'cardinality' },	
		//page 3A : déclaration début activité 
		{ src: datasInfosActivite.perimetre, dst: 'ouvertureEtablissementActivite', valeur: 'ouvertureEtablissementActivite', type: 'radio' },
		{ src: datasInfosActivite.perimetre, dst: 'modificationEtablissementActivite', valeur: 'modificationEtablissementActivite', type: 'radio' },
		{ src: datasInfosActivite.perimetre, dst: 'lpsActivite', valeur: 'lpsActivite', type: 'radio' },
		{ src: datasInfosActivite.infosCarteDelivree.numeroCarte, dst: 'numeroCarte', valeur: null, type: 'string' },
		{ src: datasInfosActivite.infosCarteDelivree.lieuCarte, dst: 'lieuCarte', valeur: null, type: 'string' },
		{ src: null, dst: 'jourFinValiditeCarte', valeur: formatDate(datasInfosActivite.infosCarteDelivree.dateValiditeCarte, 'J', true), type: 'string' },
		{ src: null, dst: 'moisFinValiditeCarte', valeur: formatDate(datasInfosActivite.infosCarteDelivree.dateValiditeCarte, 'M', true), type: 'string' },
		{ src: null, dst: 'anneeFinValiditeCarte', valeur: formatDate(datasInfosActivite.infosCarteDelivree.dateValiditeCarte, 'A', true), type: 'string' },	
		{ src: datasInfosActivite.ouvertureEtablissement.directeurEtablissement, dst: 'datasInfosActivite.ouvertureEtablissement.directeurEtablissement', valeur: listDirecteurES, type: 'xComplex' },
		{ src: datasInfosActivite.ouvertureEtablissement.adresseEtablissement.enseigne, dst: 'enseigneNouveauES', valeur: null, type: 'string' },
		{ src: null, dst: 'adresseNouveauES', valeur: mergeAdresse(datasInfosActivite.ouvertureEtablissement.adresseEtablissement.cadreAdresse, 'V', false), type: 'string' },
		{ src: null, dst: 'adresseSuiteNouveauES', valeur: mergeAdresse(datasInfosActivite.ouvertureEtablissement.adresseEtablissement.cadreAdresse, 'X', false), type: 'string' },
		{ src: datasInfosActivite.modificationEtablissement.type, dst: 'adresseEtablissementActivite', valeur: 'adresseEtablissementActivite', type: 'check' },
		{ src: datasInfosActivite.modificationEtablissement.type, dst: 'directeurEtablissementActivite', valeur: 'directeurEtablissementActivite', type: 'check' },
		{ src: null, dst: 'adresseNouveauES', valeur: mergeAdresse(datasInfosActivite.modificationEtablissement.adresseEtablissement.cadreAdresse, 'V', false), type: 'string' },
		{ src: null, dst: 'adresseSuiteNouveauES', valeur: mergeAdresse(datasInfosActivite.modificationEtablissement.adresseEtablissement.cadreAdresse, 'X', false), type: 'string' },
		{ src: datasInfosActivite.modificationEtablissement.directeurEtablissement, dst: 'datasInfosActivite.modificationEtablissement.directeurEtablissement', valeur: listDirecteurES, type: 'xComplex' },
		//bloc signature
		{ src: null, dst: 'nomPrenomsDeclarant', valeur: extractInfoIdentite(datasIdentite.cadreIdentite, 'T', true), type: 'string' },
		{ src: null, dst: 'signature', valeur: genereInfoSignataire(true, true), type: 'string' },
		{ src: datasSignature.cadreSignature.signatureLieu, dst: 'signatureLieu', valeur: null, type: 'string' },
		{ src: null, dst: 'signatureDate', valeur: formatDate(datasSignature.cadreSignature.signatureDate, 'T', true), type: 'string' }
	] ; 
}

//--------------- VARIABLES -------------------//

//--------------- FONCTIONS -------------------//

/* fonctions génériques de formatage des données
 *
 */

/*
 * calculs mathématiques : calcul de pourcentage
 */
 
function calculPercent(valNumerateur, valDenominateur, nbDecimales) {
	var valResult = '' ;
	var tmpValeur = 0 ;
	while (true) {
		if (typeof valNumerateur === 'undefined' || typeof valNumerateur !== 'number') {
			break ;
		}
		if (typeof valDenominateur === 'undefined' || typeof valDenominateur !== 'number') {
			break;
		}
		if (typeof nbDecimales === 'undefined' || typeof nbDecimales !== 'number') {
			tmpValeur = 0 ;
		} else {
			tmpValeur = nbDecimales >= 0 ? nbDecimales : 0 ; 
		}	
		tmpValeur = Math.pow(10, tmpValeur);
		if (valDenominateur !== 0) {
			tmpValeur = Math.round(((valNumerateur / valDenominateur) * 100) / tmpValeur) ;
			valResult = tmpValeur.toString();
			
		}
		break ;
	}
	return valResult ;
 }

/*
 * calculs mathématiques : calcul de soustraction
 */
 
function calculSubstraction(valTotal, tabValeurs, cumulValeur, keyCumul) {
	var valResult = '' ;
	var tmpValeur = 0 ;
	while (true) {
		if (typeof valTotal === 'undefined' || typeof valTotal !== 'number') {
			break ;
		}
		tmpValeur = valTotal ;
		if (typeof tabValeurs === 'object' && Array.isArray(tabValeurs)) {
			for (tmpIndex = 0; tmpIndex < tabValeurs.length; tmpIndex++) {
				if (typeof tabValeurs[tmpIndex] === 'number') {
					tmpValeur -= tabValeurs[tmpIndex];
					tmpFlag = true ;
				}
			}	
		}
		if (tmpFlag === true) {
			valResult = tmpValeur.toString();
			if (typeof cumulValeur === 'object' && cumulValeur !== null && typeof keyCumul === 'string') {
				if (typeof cumulValeur[keyCumul] !== 'undefined' && isNaN(cumulValeur[keyCumul]) === false) {
					cumulValeur[keyCumul] = (Number(cumulValeur[keyCumul]) - tmpValeur).toString() ;
				} else {
					cumulValeur[keyCumul] = valResult ;
				}
			}
		}
		break;
	}
	return valResult ;
 }

function calculAddition(tabValeurs, cumulValeur, keyCumul) {
	var valResult = '' ;
	var tmpFlag = false ;
	var tmpValeur = 0 ;
	var tmpIndex = 0 ;
	while (true) {
		if (typeof tabValeurs === 'object' && Array.isArray(tabValeurs)) {
			for (tmpIndex = 0; tmpIndex < tabValeurs.length; tmpIndex++) {
				if (typeof tabValeurs[tmpIndex] === 'number') {
					tmpValeur += tabValeurs[tmpIndex];
					tmpFlag = true ;
				}
			}	
		}
		if (tmpFlag === true) {
			valResult = tmpValeur.toString();
			if (typeof cumulValeur === 'object' && cumulValeur !== null && typeof keyCumul === 'string') {
				if (typeof cumulValeur[keyCumul] !== 'undefined' && isNaN(cumulValeur[keyCumul]) === false) {
					cumulValeur[keyCumul] = (tmpValeur + Number(cumulValeur[keyCumul])).toString() ;
				} else {
					cumulValeur[keyCumul] = valResult ;
				}
			}
		}
		break ;
	}
	return valResult ;
 }

/* suppression des espaces dans une chaine
 *
 */

function supprimeEspaces(szLibel) {
	var tmpRegex = /\s/g ;
	var tmpLibel = '';
	if (typeof szLibel !== 'undefined' && typeof szLibel === 'string') {
		tmpLibel = szLibel !== null ? szLibel.replace(tmpRegex, '') : '' ;	
	}
	return tmpLibel ;
}
 
/* merge des prénoms (séparateur ',')
 *
 */
	
function mergePrenoms(listPrenoms) {
	var prenoms = [];
	if (typeof listPrenoms !== 'undefined' && typeof listPrenoms === 'object') {
		for (i = 0; i < listPrenoms.length; i++) {
			prenoms.push(listPrenoms[i]);
    	}
	} else {
		prenoms.push('');
	}
	return prenoms.toString();
}

/* merge du nom et des prénoms (séparateur ',')
 *
 */

function mergeNomPrenoms(szNom, listPrenoms) {
	var result = '';
	if (typeof szNom === 'string') {
		result = szNom + ' ';
		result += mergePrenoms(listPrenoms);
	} 
	return result;
}

/* construction de la date et du lieu de naissance
 *
 */

function mergeNaissance(blocNaissance) {
	var naissance = '' ;
	if (typeof blocNaissance !== 'undefined' && typeof blocNaissance === 'object') {
		naissance = blocNaissance.lieuNaissance ? blocNaissance.lieuNaissance + ' / ' : '' ;
		naissance += blocNaissance.paysNaissance ? blocNaissance.paysNaissance : '' ;
	}
	return naissance ;
}
	
/* construction de l'adresse en tenant compte du pays ou non
 *
 */

function mergeAdresse(blocAdresse, typeMerge, multiLine) {
	var tmpVar = '' ;
	var adresse = '' ;
	var chgLigne = ' \n' ;
	if (typeof blocAdresse !== 'undefined' && typeof blocAdresse === 'object') {
		var fmtLigne = (multiLine === true ? chgLigne : ' ');
		var typeAct = (typeof typeMerge !== 'undefined' && typeof typeMerge === 'string') ? typeMerge.toUpperCase() : 'T' ;
		switch (typeAct) {
			case 'N':
				adresse += blocAdresse.numeroVoie ? blocAdresse.numeroVoie + ' ' : '' ;
				adresse += blocAdresse.indiceVoie ? blocAdresse.indiceVoie : '' ;
				break;
			case 'V':	
				tmpVar = mergeAdresse(blocAdresse, 'N', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + ' ' : '' ;
				adresse += mergeAdresse(blocAdresse, 'W', multiLine) ;
				break;
			case 'W':
				adresse += blocAdresse.typeVoie ? blocAdresse.typeVoie + ' ' : '' ;
				adresse += blocAdresse.nomVoie ?  blocAdresse.nomVoie : '' ;
				break;
			case 'S':
				adresse += blocAdresse.complementAdresse ? blocAdresse.complementAdresse + fmtLigne : '' ;
				adresse += blocAdresse.distributionSpeciale ? blocAdresse.distributionSpeciale : '' ;
				break;
			case 'C':
				if (typeof blocAdresse.pays !== 'undefined') {
					if (typeof blocAdresse.codePostal !== 'undefined' && blocAdresse.codePostal) {
						adresse += blocAdresse.codePostal ? blocAdresse.codePostal + ' ' : '' ;
						adresse += blocAdresse.commune ? blocAdresse.commune : '' ;
					} else {
						adresse += blocAdresse.ville ? blocAdresse.ville + fmtLigne : '' ;
						adresse += blocAdresse.pays ? blocAdresse.pays : '' ;
					}
				} else {
					adresse += blocAdresse.codePostal ? blocAdresse.codePostal + ' ' : '' ;
					adresse += blocAdresse.commune ? blocAdresse.commune : '' ;
				}
				break;
			case 'X':
				tmpVar = mergeAdresse(blocAdresse, 'S', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + fmtLigne : '' ;
				adresse += mergeAdresse(blocAdresse, 'C', multiLine) ;
				break;
			case 'Z':
				tmpVar = mergeAdresse(blocAdresse, 'V', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + fmtLigne : '' ;
				adresse += mergeAdresse(blocAdresse, 'S', multiLine) ;
				break;
			case 'T':
			default:
				tmpVar = mergeAdresse(blocAdresse, 'V', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + fmtLigne : '' ;
				tmpVar = mergeAdresse(blocAdresse, 'S', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + fmtLigne : '' ;
				adresse += mergeAdresse(blocAdresse, 'C', multiLine) ;
		}
	}
	return adresse;
}

/*
 * mise en forme numéro de téléphone au format 10 chiffres
 */

function formatNumTelephone(objNumTel) {
	var tmpRegexTelIntro = /^(\+\d{1,3}[\s\.\-]?)(.*)$/ ;
	var tmpRegexTel = /(\d{1,})[\s\.\-]?/g ;
	var tmpTel = '' ;
	var tmpIntro = '';
	var tmpTelFmt = '';
	if (typeof objNumTel !== 'undefined' && typeof objNumTel === 'object' && objNumTel) {
		tmpTel = objNumTel.international ;
		if (tmpRegexTelIntro.test(tmpTel) === true) {
			tmpIntro = tmpTel.replace(tmpRegexTelIntro, '$1') ;
			tmpTelFmt = '$2' ;
			if (tmpIntro.substring(0,3) == '+33') {
				tmpTelFmt = '0' + tmpTelFmt ;
			}	
			tmpTel = tmpTel.replace(tmpRegexTelIntro, tmpTelFmt) ;
			tmpTel = tmpTel.replace(tmpRegexTel, '$1') ;
		}
	}
	return tmpTel ;
}

/*
 * infos contacts téléphoniques
 */
 
function mergeNumTelephone(blocNumTel, flgMerge, flgInternational) {
	var numTel = '';
	var tmpTel = '';
	if (typeof blocNumTel !== 'undefined' && typeof blocNumTel === 'object') {
		if (typeof blocNumTel.telephoneFixe.telephoneNum !== 'undefined' && blocNumTel.telephoneFixe.telephoneNum ) {
			if (flgInternational === false) {
				tmpTel = formatNumTelephone(blocNumTel.telephoneFixe.telephoneNum) ;
			} else {
				tmpTel = blocNumTel.telephoneFixe.telephoneNum.international ;
			}
			numTel += tmpTel ;
		}
		if (typeof blocNumTel.telephoneMobile.telephoneNum !== 'undefined' && blocNumTel.telephoneMobile.telephoneNum) {
			if (numTel.length > 0 && flgMerge === true) {
				numTel += ' / ';
			}
			if (flgMerge === true || numTel.length === 0) {
				if (flgInternational === false) {
					tmpTel = formatNumTelephone(blocNumTel.telephoneMobile.telephoneNum) ;
				} else {
					tmpTel = blocNumTel.telephoneMobile.telephoneNum.international;
				}
				numTel += tmpTel ;
			}
		}
	}
	return numTel;
}

/* formatage d'une date avec extaction ou non d'une partie (J, M, A)
 *
 */

function formatDate(valDate, typeExtract, fullYear) {
	var dateResult = '';
	var tmpYear = '' ;
	if (typeof valDate === 'object' && valDate) {
    	var dateTmp = new Date(parseInt(valDate.getTimeInMillis())) ; ;
		var dateIso = ((dateTmp.toLocaleDateString()).substr(0, 10)).split('-') ;
		var typeAct = (typeof typeExtract !== 'undefined' && typeof typeExtract === 'string') ? typeExtract.toUpperCase() : 'T' ;
		if (dateIso.length > 1) {
			tmpYear = (fullYear === true ? dateIso[0] : dateIso[0].substr(2)) ;
			switch (typeAct) {
				case 'D':
				case 'J':
					dateResult = dateIso[2] ;
					break;
				case 'M':
					dateResult = dateIso[1] ;
					break;
				case 'A':
				case 'Y':
					dateResult = tmpYear ;
					break;
				case 'T':
				default:
					dateResult = dateIso[2] + '/' + dateIso[1] + '/' + tmpYear ;
			}
		} else {
			dateResult = dateIso[0] ;
		}
	}
	return dateResult ;
}

/*
 * infos signataire
 */

function genereInfoSignataire(flgCompleteInfos, flgMergePrenoms) {
	var contactInfos = [] ;
	var infoSignataire = '' ;
	var chgLigne = ' \n' ;
	if (typeof datasSignature !== 'undefined' && typeof datasIdentite !== 'undefined' && typeof idMandataire !== 'undefined') {
		if (flgCompleteInfos === true) {
			infoSignataire = 'Cette déclaration respecte les attendus de l\’article 1367 du code civil.' + chgLigne ;
			infoSignataire += datasSignature.cadreSignature.soussigne + ' ';
		}
		if (Value('id').of(datasSignature.cadreSignature.soussigne).eq(idMandataire)) {
			infoSignataire += datasSignature.cadreSignature.informationsMandataire.nomPrenomDenomination ;
			if (flgCompleteInfos === true) {
				infoSignataire += chgLigne ;
				infoSignataire += mergeAdresse(datasSignature.cadreSignature.informationsMandataire.cadreAdresse, 'T', true) ;
			}
		} else {
			infoSignataire += (datasIdentite.cadreIdentite.nomUsage ? datasIdentite.cadreIdentite.nomUsage + ' ' : datasIdentite.cadreIdentite.nomNaissance) + ' ' ;
			if (flgMergePrenoms === true) {
				infoSignataire += mergePrenoms(datasIdentite.cadreIdentite.prenoms) ;
			}
			if (flgCompleteInfos === true) {
				infoSignataire += chgLigne ;
				if (typeof datasIdentite.cadreContact.telephoneFixe.telephoneNum !== 'undefined' && datasIdentite.cadreContact.telephoneFixe.telephoneNum) {
					contactInfos.push(datasIdentite.cadreContact.telephoneFixe.telephoneNum) ;
				}
				if (typeof datasIdentite.cadreContact.telephoneMobile.telephoneNum !== 'undefined' && datasIdentite.cadreContact.telephoneMobile.telephoneNum) {
					contactInfos.push(datasIdentite.cadreContact.telephoneMobile.telephoneNum);
				}
				if (typeof datasIdentite.cadreContact.courriel !== 'undefined' && datasIdentite.cadreContact.courriel) {
					contactInfos.push(datasIdentite.cadreContact.courriel);
				}
				infoSignataire += contactInfos.toString() ;
			}
		}
	}
	return infoSignataire ;
}

/*
 * extraction d'informations liées à l'identité (nom, prénoms...)
 */

function extractInfoIdentite(blocIdentite, typeExtract, flgExtract) {
	var infoIdentite = '';
	if (typeof blocIdentite !== 'undefined' && typeof blocIdentite === 'object') {
		var typeExt = (typeExtract !== 'undefined' && typeof typeExtract === 'string') ? typeExtract.toUpperCase() : 'T' ;
		switch (typeExt) {
			case 'N':
			case 'L':
				infoIdentite = blocIdentite.nomUsage ? blocIdentite.nomUsage : (blocIdentite.nomNaissance ? blocIdentite.nomNaissance : '') ;
				break;
			case 'P':
			case 'F':
				infoIdentite = flgExtract ? mergePrenoms(blocIdentite.prenoms) : (blocIdentite.prenoms.length > 0 ? blocIdentite.prenoms[0] : '') ;
				break ;
			case 'C':
				infoIdentite = blocIdentite.civilite ? blocIdentite.civilite : '' ;
				break
			case 'X':
				infoIdentite += extractInfoIdentite(blocIdentite, 'N', flgExtract) ;
				infoIdentite += ' ' ;
				infoIdentite += extractInfoIdentite(blocIdentite, 'P', flgExtract) ;
				break;
			case 'T':
				infoIdentite  = extractInfoIdentite(blocIdentite, 'C', flgExtract) ;
				infoIdentite += ' ' ;
				infoIdentite += extractInfoIdentite(blocIdentite, 'N', flgExtract) ;
				infoIdentite += ' ' ;
				infoIdentite += extractInfoIdentite(blocIdentite, 'P', flgExtract) ;
				break;
			default:
				infoIdentite = '';
		}
	}
	return infoIdentite ;
}

/*
 * extraction d'informations liées aux adresses
 */
 
function extractInfoAdresse(blocAdresse, typeExtract, flgExtract) {
	var tmpDept = /(0[1-9]|[1-8][0-9]|2[A-B]|9[0-5]|9[7-8][0-9])(\d{2,3})/ ;
	var adresse = '' ;
	if (typeof blocAdresse !== 'undefined' && typeof blocAdresse === 'object') {
		var typeAct = (typeof typeExtract !== 'undefined' && typeof typeExtract === 'string') ? typeExtract.toUpperCase() : 'T' ;
		switch (typeAct) {
			case 'D':
				adresse = blocAdresse.commune ? blocAdresse.commune.id : '' ;
				adresse = adresse.replace(tmpDept, '$1') ;
				break;
			case 'P':
				if (typeof blocAdresse.ville !== 'undefined' && blocAdresse.ville) {
					adresse = blocAdresse.pays ;
				} else {
					if (flgExtract === true) {
						adresse = blocAdresse.pays ;
					}
				}
				break;
			case 'C':
				if (typeof blocAdresse.ville !== 'undefined' && blocAdresse.ville) {
					adresse = blocAdresse.ville ;
				} else {
					adresse = blocAdresse.commune ;
				}
				break;
			default:
		}
	}
	return adresse;
}

/*
 * extraction indicatif téléphone si indicatif non français (+33)
 */

function extractIndicatifEtrangerTelephone(objNumTel) {
	var tmpRegexTelIntro = /^(\+\d{1,3}[\s\.\-]?)(.*)$/ ;
	var tmpTel = '' ;
	var tmpIntro = '';
	if (typeof objNumTel !== 'undefined' && typeof objNumTel === 'object' && objNumTel) {
		if (tmpRegexTelIntro.test(objNumTel.international) === true) {
			tmpIntro = objNumTel.international.replace(tmpRegexTelIntro, '$1').trim() ;
			if (tmpIntro !== '+33') {
				tmpTel = tmpIntro ;
			}
		}
	}
	return tmpTel ;
}

/* merge de PJs
 * 
 */

function merge(pjs) {
	var mergedFile = nash.doc.create();
	pjs.forEach(function (elm) {		
		mergedFile.append(elm).save('item.pdf');
    });
	return mergedFile;
}

/* ajout de PJ
 * 
 */

function appendPj(fld, listPj) {
	fld.forEach(function (elm) {
        listPj.push(elm);
		_log.info('elm.getAbsolutePath() = >'+elm.getAbsolutePath());
		metas.push({'name':'userAttachments', 'value': '/'+elm.getAbsolutePath()});
		metas.push({'name':'document', 'value': '/'+elm.getAbsolutePath()});
    });
}

/* remplissage d'un cerfa à partir d'une table de paramétrage
 *
*/

function genereDynamicParams(tabParam, baseAddress) {
	var tmpTab = tabParam.replace(/root/gi, baseAddress);
	var tmpObj = [] ;
	try {
		tmpObj = eval(tmpTab) ;
	} catch(error) {
		tmpObj = [];
	}
  	return tmpObj;
}

function genereXDynamicParams(tabParam, baseAddress) {
	var tmpTab = '';
	var tmpVal ;
	var tmpFlg = false ;
	var tmpInd = 0;
	var tmpKey = {} ;
	var tmpObj = [] ;
	var tmpRegG = /root/gi ; // pour remplacer root. par le préfixe réel
	var tmpReg = [ /src\:[\s]*([^;\{\}]*)[;]*.*$/i, // pour isoler la valeur associée à src
				   /dst\:[\s]*([^;\{\}]*)[;]*.*$/i, // pour isoler la valeur associée à dst
				   /valeur\:[\s]*([^;\{\}]*)[;]*.*$/i, // pour isoler la valeur associée à value
				   /type\:[\s]*([^;\{\}]*)[;]*.*$/i ] ; // pour isoler la valeur associée à type
	var tmpLib = [ 'src', 'dst', 'valeur', 'type' ] ; // table des propriétés src, dst, value, type
	tabParam.forEach(function (elm) {
		try {
			tmpTab = elm.replace(tmpRegG, baseAddress);
			tmpKey = {} ;
			for (tmpInd = 0; tmpInd < tmpLib.length; tmpInd++) {
				tmpVal = tmpReg[tmpInd].exec(tmpTab) ;
				if (tmpVal !== null && tmpVal.length > 0) {
					tmpKey[tmpLib[tmpInd]] = eval(tmpVal[1].trim());
					tmpFlg = true ;
				} else {
					tmpFlg = false ;
					break;
				}
			}
			if (tmpFlg == true) {
				tmpObj.push(tmpKey) ;
			}
		} catch(error) {
			tmpFlg = false ;
		}
		return ;
    });
  	return tmpObj;
}

function completeCerfa(paramFields, valFields, indFields) {
	var tmpDst = '';
	var tmpResult = true ;
	var tmpRegexIndice = /\[i\]/gi ; // forme 'champ-cerfa[i] par exemple'
	var tmpRegexIndex = /\[x\]/gi ; // forme 'champ-cerfa[x] par exemple'
	var tmpBase = '' ;
	var tmpType = '' ;
	var tmpValue = '' ;
	var tmpFlag = false ;
	var objParam = [] ;
	
	if (typeof paramFields !== 'object' || !(Array.isArray(paramFields)) || typeof formFields !== 'object' || typeof indFields !== 'number') {
		return false;
	}
	paramFields.forEach(function (item) {
		if (typeof item.src === 'undefined') {
			return ;
		}
		tmpType = (typeof item.type !== undefined && typeof item.type === 'string') ? (item.type).toUpperCase() : '' ;
		if (tmpType === 'CARDINALITY' || tmpType === 'XCARDINALITY') {
			if (typeof item.src !== 'object' || item.src === null) {
				return ;
			}
			for (iCardinality = 0; iCardinality < (item.src).length; iCardinality++) {
				tmpBase = item.dst + '[' + iCardinality.toString() + ']' ;
				switch (tmpType) {
					case 'CARDINALITY':
						objParam = genereDynamicParams(item.valeur, tmpBase);
						break;
					case 'XCARDINALITY':
						objParam = genereXDynamicParams(item.valeur, tmpBase);
						break;
					default:
						objParam = [];
						break;
				}
				if (completeCerfa(objParam, valFields, iCardinality) == false) {
					tmpResult = false ;
				}
			}
			return ;
		}
		if (tmpType === 'COMPLEX' || tmpType === 'XCOMPLEX') {
			if (typeof item.src !== 'object') {
				return ;
			}
			(item.valeur).forEach(function (xtem) {
				switch (tmpType) {
					case 'COMPLEX':
						objParam = genereDynamicParams(xtem, item.dst);
						break;
					case 'XCOMPLEX':
						objParam = genereXDynamicParams(xtem, item.dst);
						break;
					default:
						objParam = [];
						break;
				}
				if (completeCerfa(objParam, valFields, 0) == false) {
					tmpResult = false ;
				}
				return ;
			});
			return ;
		}
		tmpDst = (item.dst).replace(tmpRegexIndice, (indFields + 1).toString()) ;
		tmpDst = tmpDst.replace(tmpRegexIndex, indFields.toString()) ;
		switch (tmpType) {
			case 'BOOL':
				var tmpFlg = typeof item.valeur === 'boolean' ? item.valeur : false ;
				if (item.src === null) {
					valFields[tmpDst] = tmpFlg ;
				} else {
					valFields[tmpDst] = item.src === tmpFlg ? true : false;
				}
				break;
			case 'XBOOL':
				if ((typeof item.src === 'boolean') && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					if (item.valeur[0] === item.valeur[1]) {
						if (item.src === false) {
							valFields[tmpDst] = null;
						}
					} else {
						valFields[tmpDst] = item.src;
					}
				}
				break;
			case 'RADIO':
				if (item.src) {
					valFields[tmpDst] = Value('id').of(item.src).eq(item.valeur) ? true : false;
				}
				break;
			case 'BRADIO':
				if ((typeof item.src === 'boolean' && item.src) && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					valFields[tmpDst] = Value('id').of(item.valeur[0]).eq(item.valeur[1]) ? true : false;
				}
				break;
			case 'XRADIO':
				if ((typeof item.src === 'boolean') && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					if (Value('id').of(item.valeur[0]).eq(item.valeur[1])) {
						if (item.src === false) {
							valFields[tmpDst] = null;
						}
					} else {
						valFields[tmpDst] = item.src;
					}
				}
				break;
			case 'CHECK':
				if (item.src) {
					valFields[tmpDst] = Value('id').of(item.src).contains(item.valeur) ? true : false;
				}
				break;
			case 'BCHECK':
				if ((typeof item.src === 'boolean' && item.src) && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					valFields[tmpDst] = Value('id').of(item.valeur[0]).contains(item.valeur[1]) ? true : false;
				}
				break;
			case 'XCHECK':
				if ((typeof item.src === 'boolean') && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					if (Value('id').of(item.valeur[0]).contains(item.valeur[1])) {
						if (item.src === false) {
							valFields[tmpDst] = null;
						}
					} else {
						valFields[tmpDst] = item.src;
					}
				}
				break;
			case 'STRING':
				if (item.valeur) {
					tmpValue = item.valeur + ' ' ;
					tmpFlag = true ;
				} else {
					tmpValue = '' ;
					tmpFlag = false ;
				}
				if (item.src) {
					tmpValue += item.src ;
					tmpFlag = true ;
				}
				if (tmpFlag) {
					valFields[tmpDst] = tmpValue ;
				}
				break;
			case 'BSTRING':
				if ((typeof item.src === 'boolean' && item.src) && (typeof item.valeur !== 'undefined' && item.valeur)) {
					valFields[tmpDst] = item.valeur ;
				}
				break;
			case 'XSTRING':
				if (((typeof item.src === 'string' or typeof item.src === 'object') && item.src) && (typeof item.valeur !== 'undefined' && item.valeur)) {
					valFields[tmpDst] = item.valeur ;
				}
				break;
			default:
				break;
		}
		return ;
	});
	return tmpResult ;
}

//--------------- FONCTIONS -------------------//

//--------------- MAIN GENERATION CERFA -------//

/* génération du Cerfa à partir de la table d'équivalences
 * (génération optionnelle)
 */

if (typeof formFields !== 'undefined' && typeof nameCerfa !== 'undefined') {
	var flgCerfa = true ;
	if (typeof equivCerfa !== 'undefined') { 
		flgCerfa = completeCerfa(equivCerfa, formFields, 0);
	}
	if (flgCerfa === true) {
		var cerfaDoc = nash.doc
			.load(repCerfa + nameCerfa)
			.apply(formFields);
		var pjCerfa = cerfaDoc.save(nameCerfa);
	}
}

/* ajout des PJ utilisateurs le cas échéant
 *
 */
 
if (typeof pjSignataire !== 'undefined') {
	if(Value('id').of(pjSignataire).contains(idDeclarant)) {
		appendPj($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantSignataire, pjUser);
	}
	if(Value('id').of(pjSignataire).contains(idMandataire)) {
		appendPj($attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire, pjUser);
	}
}

/* ajout des autres PJ le cas échéant
 *
 */

if($attachmentPreprocess.attachmentPreprocess.pjAutres !== 'undefined') {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjAutres, pjUser);
}

/* ajout du Cerfa en tant que PJ (s'il a été généré)
 *
 */
 
if (typeof pjCerfa !== 'undefined') {
	data.push(spec.createData({
			id: 'record',
			label: refFormTitle,
			description: 'Voici le formulaire obtenu à partir des données saisies.',
			type: 'FileReadOnly',
			value: [pjCerfa] 
	})) ;
}

/*
 * Enregistrement du fichier (en mémoire)
 */ 

var metasToDelete = [];
var recordMetas = nash.record.meta() != null ? nash.record.meta().metas : null;
var recordMetasSize = recordMetas != null ? recordMetas.size() : 0;

for (var i = 0; i < recordMetasSize; i++) {
    if (recordMetas.get(i).name == 'document') {
        metasToDelete.push({'name':'document', 'value': recordMetas.get(i).value});
    }
}
nash.record.removeMeta(metasToDelete);
nash.record.meta(metas);

data.push(spec.createData({
    id : 'uploaded',
    label : 'Pièces jointes',
    description : 'Pièces jointes ajoutées au formulaire.',
    type : 'FileReadOnly',
    value : [ merge(pjUser).save('saisine.pdf') ]
	})) ;

var groups = [ spec.createGroup({
    id : 'attachments',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Saisine du Ministère de l\'intérieur',
    groups : groups
});

//--------------- MAIN GENERATION CERFA -------//
