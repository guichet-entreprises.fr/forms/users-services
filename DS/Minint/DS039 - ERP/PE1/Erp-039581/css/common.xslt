<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns="http://www.w3.org/1999/xhtml" xmlns:n="http://www.ge.fr/schema/1.2/form-manager" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="html" encoding="utf-8" doctype-public="html" />

    <xsl:variable name="lowerCase" select="'abcdefghijklmnopqrstuvwxyz'" />
    <xsl:variable name="upperCase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />

    <xsl:template match="/">
        <html>
            <head>
                <base target="_blank" />
                <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
                <script src="data:text/javascript;base64,LyogaHR0cDovL3ByaXNtanMuY29tL2Rvd25sb2FkLmh0bWw/dGhlbWVzPXByaXNtJmxhbmd1YWdlcz1jbGlrZStqYXZhc2NyaXB0JnBsdWdpbnM9bGluZS1udW1iZXJzICovCnZhciBfc2VsZj0idW5kZWZpbmVkIiE9dHlwZW9mIHdpbmRvdz93aW5kb3c6InVuZGVmaW5lZCIhPXR5cGVvZiBXb3JrZXJHbG9iYWxTY29wZSYmc2VsZiBpbnN0YW5jZW9mIFdvcmtlckdsb2JhbFNjb3BlP3NlbGY6e30sUHJpc209ZnVuY3Rpb24oKXt2YXIgZT0vXGJsYW5nKD86dWFnZSk/LShcdyspXGIvaSx0PTAsbj1fc2VsZi5QcmlzbT17bWFudWFsOl9zZWxmLlByaXNtJiZfc2VsZi5QcmlzbS5tYW51YWwsdXRpbDp7ZW5jb2RlOmZ1bmN0aW9uKGUpe3JldHVybiBlIGluc3RhbmNlb2YgYT9uZXcgYShlLnR5cGUsbi51dGlsLmVuY29kZShlLmNvbnRlbnQpLGUuYWxpYXMpOiJBcnJheSI9PT1uLnV0aWwudHlwZShlKT9lLm1hcChuLnV0aWwuZW5jb2RlKTplLnJlcGxhY2UoLyYvZywiJmFtcDsiKS5yZXBsYWNlKC88L2csIiZsdDsiKS5yZXBsYWNlKC9cdTAwYTAvZywiICIpfSx0eXBlOmZ1bmN0aW9uKGUpe3JldHVybiBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwoZSkubWF0Y2goL1xbb2JqZWN0IChcdyspXF0vKVsxXX0sb2JqSWQ6ZnVuY3Rpb24oZSl7cmV0dXJuIGUuX19pZHx8T2JqZWN0LmRlZmluZVByb3BlcnR5KGUsIl9faWQiLHt2YWx1ZTorK3R9KSxlLl9faWR9LGNsb25lOmZ1bmN0aW9uKGUpe3ZhciB0PW4udXRpbC50eXBlKGUpO3N3aXRjaCh0KXtjYXNlIk9iamVjdCI6dmFyIGE9e307Zm9yKHZhciByIGluIGUpZS5oYXNPd25Qcm9wZXJ0eShyKSYmKGFbcl09bi51dGlsLmNsb25lKGVbcl0pKTtyZXR1cm4gYTtjYXNlIkFycmF5IjpyZXR1cm4gZS5tYXAmJmUubWFwKGZ1bmN0aW9uKGUpe3JldHVybiBuLnV0aWwuY2xvbmUoZSl9KX1yZXR1cm4gZX19LGxhbmd1YWdlczp7ZXh0ZW5kOmZ1bmN0aW9uKGUsdCl7dmFyIGE9bi51dGlsLmNsb25lKG4ubGFuZ3VhZ2VzW2VdKTtmb3IodmFyIHIgaW4gdClhW3JdPXRbcl07cmV0dXJuIGF9LGluc2VydEJlZm9yZTpmdW5jdGlvbihlLHQsYSxyKXtyPXJ8fG4ubGFuZ3VhZ2VzO3ZhciBsPXJbZV07aWYoMj09YXJndW1lbnRzLmxlbmd0aCl7YT1hcmd1bWVudHNbMV07Zm9yKHZhciBpIGluIGEpYS5oYXNPd25Qcm9wZXJ0eShpKSYmKGxbaV09YVtpXSk7cmV0dXJuIGx9dmFyIG89e307Zm9yKHZhciBzIGluIGwpaWYobC5oYXNPd25Qcm9wZXJ0eShzKSl7aWYocz09dClmb3IodmFyIGkgaW4gYSlhLmhhc093blByb3BlcnR5KGkpJiYob1tpXT1hW2ldKTtvW3NdPWxbc119cmV0dXJuIG4ubGFuZ3VhZ2VzLkRGUyhuLmxhbmd1YWdlcyxmdW5jdGlvbih0LG4pe249PT1yW2VdJiZ0IT1lJiYodGhpc1t0XT1vKX0pLHJbZV09b30sREZTOmZ1bmN0aW9uKGUsdCxhLHIpe3I9cnx8e307Zm9yKHZhciBsIGluIGUpZS5oYXNPd25Qcm9wZXJ0eShsKSYmKHQuY2FsbChlLGwsZVtsXSxhfHxsKSwiT2JqZWN0IiE9PW4udXRpbC50eXBlKGVbbF0pfHxyW24udXRpbC5vYmpJZChlW2xdKV0/IkFycmF5IiE9PW4udXRpbC50eXBlKGVbbF0pfHxyW24udXRpbC5vYmpJZChlW2xdKV18fChyW24udXRpbC5vYmpJZChlW2xdKV09ITAsbi5sYW5ndWFnZXMuREZTKGVbbF0sdCxsLHIpKToocltuLnV0aWwub2JqSWQoZVtsXSldPSEwLG4ubGFuZ3VhZ2VzLkRGUyhlW2xdLHQsbnVsbCxyKSkpfX0scGx1Z2luczp7fSxoaWdobGlnaHRBbGw6ZnVuY3Rpb24oZSx0KXt2YXIgYT17Y2FsbGJhY2s6dCxzZWxlY3RvcjonY29kZVtjbGFzcyo9Imxhbmd1YWdlLSJdLCBbY2xhc3MqPSJsYW5ndWFnZS0iXSBjb2RlLCBjb2RlW2NsYXNzKj0ibGFuZy0iXSwgW2NsYXNzKj0ibGFuZy0iXSBjb2RlJ307bi5ob29rcy5ydW4oImJlZm9yZS1oaWdobGlnaHRhbGwiLGEpO2Zvcih2YXIgcixsPWEuZWxlbWVudHN8fGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoYS5zZWxlY3RvciksaT0wO3I9bFtpKytdOyluLmhpZ2hsaWdodEVsZW1lbnQocixlPT09ITAsYS5jYWxsYmFjayl9LGhpZ2hsaWdodEVsZW1lbnQ6ZnVuY3Rpb24odCxhLHIpe2Zvcih2YXIgbCxpLG89dDtvJiYhZS50ZXN0KG8uY2xhc3NOYW1lKTspbz1vLnBhcmVudE5vZGU7byYmKGw9KG8uY2xhc3NOYW1lLm1hdGNoKGUpfHxbLCIiXSlbMV0udG9Mb3dlckNhc2UoKSxpPW4ubGFuZ3VhZ2VzW2xdKSx0LmNsYXNzTmFtZT10LmNsYXNzTmFtZS5yZXBsYWNlKGUsIiIpLnJlcGxhY2UoL1xzKy9nLCIgIikrIiBsYW5ndWFnZS0iK2wsbz10LnBhcmVudE5vZGUsL3ByZS9pLnRlc3Qoby5ub2RlTmFtZSkmJihvLmNsYXNzTmFtZT1vLmNsYXNzTmFtZS5yZXBsYWNlKGUsIiIpLnJlcGxhY2UoL1xzKy9nLCIgIikrIiBsYW5ndWFnZS0iK2wpO3ZhciBzPXQudGV4dENvbnRlbnQsdT17ZWxlbWVudDp0LGxhbmd1YWdlOmwsZ3JhbW1hcjppLGNvZGU6c307aWYobi5ob29rcy5ydW4oImJlZm9yZS1zYW5pdHktY2hlY2siLHUpLCF1LmNvZGV8fCF1LmdyYW1tYXIpcmV0dXJuIHUuY29kZSYmKHUuZWxlbWVudC50ZXh0Q29udGVudD11LmNvZGUpLG4uaG9va3MucnVuKCJjb21wbGV0ZSIsdSksdm9pZCAwO2lmKG4uaG9va3MucnVuKCJiZWZvcmUtaGlnaGxpZ2h0Iix1KSxhJiZfc2VsZi5Xb3JrZXIpe3ZhciBnPW5ldyBXb3JrZXIobi5maWxlbmFtZSk7Zy5vbm1lc3NhZ2U9ZnVuY3Rpb24oZSl7dS5oaWdobGlnaHRlZENvZGU9ZS5kYXRhLG4uaG9va3MucnVuKCJiZWZvcmUtaW5zZXJ0Iix1KSx1LmVsZW1lbnQuaW5uZXJIVE1MPXUuaGlnaGxpZ2h0ZWRDb2RlLHImJnIuY2FsbCh1LmVsZW1lbnQpLG4uaG9va3MucnVuKCJhZnRlci1oaWdobGlnaHQiLHUpLG4uaG9va3MucnVuKCJjb21wbGV0ZSIsdSl9LGcucG9zdE1lc3NhZ2UoSlNPTi5zdHJpbmdpZnkoe2xhbmd1YWdlOnUubGFuZ3VhZ2UsY29kZTp1LmNvZGUsaW1tZWRpYXRlQ2xvc2U6ITB9KSl9ZWxzZSB1LmhpZ2hsaWdodGVkQ29kZT1uLmhpZ2hsaWdodCh1LmNvZGUsdS5ncmFtbWFyLHUubGFuZ3VhZ2UpLG4uaG9va3MucnVuKCJiZWZvcmUtaW5zZXJ0Iix1KSx1LmVsZW1lbnQuaW5uZXJIVE1MPXUuaGlnaGxpZ2h0ZWRDb2RlLHImJnIuY2FsbCh0KSxuLmhvb2tzLnJ1bigiYWZ0ZXItaGlnaGxpZ2h0Iix1KSxuLmhvb2tzLnJ1bigiY29tcGxldGUiLHUpfSxoaWdobGlnaHQ6ZnVuY3Rpb24oZSx0LHIpe3ZhciBsPW4udG9rZW5pemUoZSx0KTtyZXR1cm4gYS5zdHJpbmdpZnkobi51dGlsLmVuY29kZShsKSxyKX0sdG9rZW5pemU6ZnVuY3Rpb24oZSx0KXt2YXIgYT1uLlRva2VuLHI9W2VdLGw9dC5yZXN0O2lmKGwpe2Zvcih2YXIgaSBpbiBsKXRbaV09bFtpXTtkZWxldGUgdC5yZXN0fWU6Zm9yKHZhciBpIGluIHQpaWYodC5oYXNPd25Qcm9wZXJ0eShpKSYmdFtpXSl7dmFyIG89dFtpXTtvPSJBcnJheSI9PT1uLnV0aWwudHlwZShvKT9vOltvXTtmb3IodmFyIHM9MDtzPG8ubGVuZ3RoOysrcyl7dmFyIHU9b1tzXSxnPXUuaW5zaWRlLGM9ISF1Lmxvb2tiZWhpbmQsaD0hIXUuZ3JlZWR5LGY9MCxkPXUuYWxpYXM7aWYoaCYmIXUucGF0dGVybi5nbG9iYWwpe3ZhciBwPXUucGF0dGVybi50b1N0cmluZygpLm1hdGNoKC9baW11eV0qJC8pWzBdO3UucGF0dGVybj1SZWdFeHAodS5wYXR0ZXJuLnNvdXJjZSxwKyJnIil9dT11LnBhdHRlcm58fHU7Zm9yKHZhciBtPTAseT0wO208ci5sZW5ndGg7eSs9clttXS5sZW5ndGgsKyttKXt2YXIgdj1yW21dO2lmKHIubGVuZ3RoPmUubGVuZ3RoKWJyZWFrIGU7aWYoISh2IGluc3RhbmNlb2YgYSkpe3UubGFzdEluZGV4PTA7dmFyIGI9dS5leGVjKHYpLGs9MTtpZighYiYmaCYmbSE9ci5sZW5ndGgtMSl7aWYodS5sYXN0SW5kZXg9eSxiPXUuZXhlYyhlKSwhYilicmVhaztmb3IodmFyIHc9Yi5pbmRleCsoYz9iWzFdLmxlbmd0aDowKSxfPWIuaW5kZXgrYlswXS5sZW5ndGgsUD1tLEE9eSxqPXIubGVuZ3RoO2o+UCYmXz5BOysrUClBKz1yW1BdLmxlbmd0aCx3Pj1BJiYoKyttLHk9QSk7aWYoclttXWluc3RhbmNlb2YgYXx8cltQLTFdLmdyZWVkeSljb250aW51ZTtrPVAtbSx2PWUuc2xpY2UoeSxBKSxiLmluZGV4LT15fWlmKGIpe2MmJihmPWJbMV0ubGVuZ3RoKTt2YXIgdz1iLmluZGV4K2YsYj1iWzBdLnNsaWNlKGYpLF89dytiLmxlbmd0aCx4PXYuc2xpY2UoMCx3KSxPPXYuc2xpY2UoXyksUz1bbSxrXTt4JiZTLnB1c2goeCk7dmFyIE49bmV3IGEoaSxnP24udG9rZW5pemUoYixnKTpiLGQsYixoKTtTLnB1c2goTiksTyYmUy5wdXNoKE8pLEFycmF5LnByb3RvdHlwZS5zcGxpY2UuYXBwbHkocixTKX19fX19cmV0dXJuIHJ9LGhvb2tzOnthbGw6e30sYWRkOmZ1bmN0aW9uKGUsdCl7dmFyIGE9bi5ob29rcy5hbGw7YVtlXT1hW2VdfHxbXSxhW2VdLnB1c2godCl9LHJ1bjpmdW5jdGlvbihlLHQpe3ZhciBhPW4uaG9va3MuYWxsW2VdO2lmKGEmJmEubGVuZ3RoKWZvcih2YXIgcixsPTA7cj1hW2wrK107KXIodCl9fX0sYT1uLlRva2VuPWZ1bmN0aW9uKGUsdCxuLGEscil7dGhpcy50eXBlPWUsdGhpcy5jb250ZW50PXQsdGhpcy5hbGlhcz1uLHRoaXMubGVuZ3RoPTB8KGF8fCIiKS5sZW5ndGgsdGhpcy5ncmVlZHk9ISFyfTtpZihhLnN0cmluZ2lmeT1mdW5jdGlvbihlLHQscil7aWYoInN0cmluZyI9PXR5cGVvZiBlKXJldHVybiBlO2lmKCJBcnJheSI9PT1uLnV0aWwudHlwZShlKSlyZXR1cm4gZS5tYXAoZnVuY3Rpb24obil7cmV0dXJuIGEuc3RyaW5naWZ5KG4sdCxlKX0pLmpvaW4oIiIpO3ZhciBsPXt0eXBlOmUudHlwZSxjb250ZW50OmEuc3RyaW5naWZ5KGUuY29udGVudCx0LHIpLHRhZzoic3BhbiIsY2xhc3NlczpbInRva2VuIixlLnR5cGVdLGF0dHJpYnV0ZXM6e30sbGFuZ3VhZ2U6dCxwYXJlbnQ6cn07aWYoImNvbW1lbnQiPT1sLnR5cGUmJihsLmF0dHJpYnV0ZXMuc3BlbGxjaGVjaz0idHJ1ZSIpLGUuYWxpYXMpe3ZhciBpPSJBcnJheSI9PT1uLnV0aWwudHlwZShlLmFsaWFzKT9lLmFsaWFzOltlLmFsaWFzXTtBcnJheS5wcm90b3R5cGUucHVzaC5hcHBseShsLmNsYXNzZXMsaSl9bi5ob29rcy5ydW4oIndyYXAiLGwpO3ZhciBvPU9iamVjdC5rZXlzKGwuYXR0cmlidXRlcykubWFwKGZ1bmN0aW9uKGUpe3JldHVybiBlKyc9IicrKGwuYXR0cmlidXRlc1tlXXx8IiIpLnJlcGxhY2UoLyIvZywiJnF1b3Q7IikrJyInfSkuam9pbigiICIpO3JldHVybiI8IitsLnRhZysnIGNsYXNzPSInK2wuY2xhc3Nlcy5qb2luKCIgIikrJyInKyhvPyIgIitvOiIiKSsiPiIrbC5jb250ZW50KyI8LyIrbC50YWcrIj4ifSwhX3NlbGYuZG9jdW1lbnQpcmV0dXJuIF9zZWxmLmFkZEV2ZW50TGlzdGVuZXI/KF9zZWxmLmFkZEV2ZW50TGlzdGVuZXIoIm1lc3NhZ2UiLGZ1bmN0aW9uKGUpe3ZhciB0PUpTT04ucGFyc2UoZS5kYXRhKSxhPXQubGFuZ3VhZ2Uscj10LmNvZGUsbD10LmltbWVkaWF0ZUNsb3NlO19zZWxmLnBvc3RNZXNzYWdlKG4uaGlnaGxpZ2h0KHIsbi5sYW5ndWFnZXNbYV0sYSkpLGwmJl9zZWxmLmNsb3NlKCl9LCExKSxfc2VsZi5QcmlzbSk6X3NlbGYuUHJpc207dmFyIHI9ZG9jdW1lbnQuY3VycmVudFNjcmlwdHx8W10uc2xpY2UuY2FsbChkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZSgic2NyaXB0IikpLnBvcCgpO3JldHVybiByJiYobi5maWxlbmFtZT1yLnNyYywhZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcnx8bi5tYW51YWx8fHIuaGFzQXR0cmlidXRlKCJkYXRhLW1hbnVhbCIpfHwoImxvYWRpbmciIT09ZG9jdW1lbnQucmVhZHlTdGF0ZT93aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lP3dpbmRvdy5yZXF1ZXN0QW5pbWF0aW9uRnJhbWUobi5oaWdobGlnaHRBbGwpOndpbmRvdy5zZXRUaW1lb3V0KG4uaGlnaGxpZ2h0QWxsLDE2KTpkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCJET01Db250ZW50TG9hZGVkIixuLmhpZ2hsaWdodEFsbCkpKSxfc2VsZi5QcmlzbX0oKTsidW5kZWZpbmVkIiE9dHlwZW9mIG1vZHVsZSYmbW9kdWxlLmV4cG9ydHMmJihtb2R1bGUuZXhwb3J0cz1QcmlzbSksInVuZGVmaW5lZCIhPXR5cGVvZiBnbG9iYWwmJihnbG9iYWwuUHJpc209UHJpc20pOwpQcmlzbS5sYW5ndWFnZXMuY2xpa2U9e2NvbW1lbnQ6W3twYXR0ZXJuOi8oXnxbXlxcXSlcL1wqW1x3XFddKj9cKlwvLyxsb29rYmVoaW5kOiEwfSx7cGF0dGVybjovKF58W15cXDpdKVwvXC8uKi8sbG9va2JlaGluZDohMH1dLHN0cmluZzp7cGF0dGVybjovKFsiJ10pKFxcKD86XHJcbnxbXHNcU10pfCg/IVwxKVteXFxcclxuXSkqXDEvLGdyZWVkeTohMH0sImNsYXNzLW5hbWUiOntwYXR0ZXJuOi8oKD86XGIoPzpjbGFzc3xpbnRlcmZhY2V8ZXh0ZW5kc3xpbXBsZW1lbnRzfHRyYWl0fGluc3RhbmNlb2Z8bmV3KVxzKyl8KD86Y2F0Y2hccytcKCkpW2EtejAtOV9cLlxcXSsvaSxsb29rYmVoaW5kOiEwLGluc2lkZTp7cHVuY3R1YXRpb246LyhcLnxcXCkvfX0sa2V5d29yZDovXGIoaWZ8ZWxzZXx3aGlsZXxkb3xmb3J8cmV0dXJufGlufGluc3RhbmNlb2Z8ZnVuY3Rpb258bmV3fHRyeXx0aHJvd3xjYXRjaHxmaW5hbGx5fG51bGx8YnJlYWt8Y29udGludWUpXGIvLCJib29sZWFuIjovXGIodHJ1ZXxmYWxzZSlcYi8sImZ1bmN0aW9uIjovW2EtejAtOV9dKyg/PVwoKS9pLG51bWJlcjovXGItPyg/OjB4W1xkYS1mXSt8XGQqXC4/XGQrKD86ZVsrLV0/XGQrKT8pXGIvaSxvcGVyYXRvcjovLS0/fFwrXCs/fCE9Pz0/fDw9P3w+PT98PT0/PT98JiY/fFx8XHw/fFw/fFwqfFwvfH58XF58JS8scHVuY3R1YXRpb246L1t7fVtcXTsoKSwuOl0vfTsKUHJpc20ubGFuZ3VhZ2VzLmphdmFzY3JpcHQ9UHJpc20ubGFuZ3VhZ2VzLmV4dGVuZCgiY2xpa2UiLHtrZXl3b3JkOi9cYihhc3xhc3luY3xhd2FpdHxicmVha3xjYXNlfGNhdGNofGNsYXNzfGNvbnN0fGNvbnRpbnVlfGRlYnVnZ2VyfGRlZmF1bHR8ZGVsZXRlfGRvfGVsc2V8ZW51bXxleHBvcnR8ZXh0ZW5kc3xmaW5hbGx5fGZvcnxmcm9tfGZ1bmN0aW9ufGdldHxpZnxpbXBsZW1lbnRzfGltcG9ydHxpbnxpbnN0YW5jZW9mfGludGVyZmFjZXxsZXR8bmV3fG51bGx8b2Z8cGFja2FnZXxwcml2YXRlfHByb3RlY3RlZHxwdWJsaWN8cmV0dXJufHNldHxzdGF0aWN8c3VwZXJ8c3dpdGNofHRoaXN8dGhyb3d8dHJ5fHR5cGVvZnx2YXJ8dm9pZHx3aGlsZXx3aXRofHlpZWxkKVxiLyxudW1iZXI6L1xiLT8oMHhbXGRBLUZhLWZdK3wwYlswMV0rfDBvWzAtN10rfFxkKlwuP1xkKyhbRWVdWystXT9cZCspP3xOYU58SW5maW5pdHkpXGIvLCJmdW5jdGlvbiI6L1tfJGEtekEtWlx4QTAtXHVGRkZGXVtfJGEtekEtWjAtOVx4QTAtXHVGRkZGXSooPz1cKCkvaSxvcGVyYXRvcjovLS0/fFwrXCs/fCE9Pz0/fDw9P3w+PT98PT0/PT98JiY/fFx8XHw/fFw/fFwqXCo/fFwvfH58XF58JXxcLnszfS99KSxQcmlzbS5sYW5ndWFnZXMuaW5zZXJ0QmVmb3JlKCJqYXZhc2NyaXB0Iiwia2V5d29yZCIse3JlZ2V4OntwYXR0ZXJuOi8oXnxbXlwvXSlcLyg/IVwvKShcWy4rP118XFwufFteXC9cXFxyXG5dKStcL1tnaW15dV17MCw1fSg/PVxzKigkfFtcclxuLC47fSldKSkvLGxvb2tiZWhpbmQ6ITAsZ3JlZWR5OiEwfX0pLFByaXNtLmxhbmd1YWdlcy5pbnNlcnRCZWZvcmUoImphdmFzY3JpcHQiLCJzdHJpbmciLHsidGVtcGxhdGUtc3RyaW5nIjp7cGF0dGVybjovYCg/OlxcXFx8XFw/W15cXF0pKj9gLyxncmVlZHk6ITAsaW5zaWRlOntpbnRlcnBvbGF0aW9uOntwYXR0ZXJuOi9cJFx7W159XStcfS8saW5zaWRlOnsiaW50ZXJwb2xhdGlvbi1wdW5jdHVhdGlvbiI6e3BhdHRlcm46L15cJFx7fFx9JC8sYWxpYXM6InB1bmN0dWF0aW9uIn0scmVzdDpQcmlzbS5sYW5ndWFnZXMuamF2YXNjcmlwdH19LHN0cmluZzovW1xzXFNdKy99fX0pLFByaXNtLmxhbmd1YWdlcy5tYXJrdXAmJlByaXNtLmxhbmd1YWdlcy5pbnNlcnRCZWZvcmUoIm1hcmt1cCIsInRhZyIse3NjcmlwdDp7cGF0dGVybjovKDxzY3JpcHRbXHdcV10qPz4pW1x3XFddKj8oPz08XC9zY3JpcHQ+KS9pLGxvb2tiZWhpbmQ6ITAsaW5zaWRlOlByaXNtLmxhbmd1YWdlcy5qYXZhc2NyaXB0LGFsaWFzOiJsYW5ndWFnZS1qYXZhc2NyaXB0In19KSxQcmlzbS5sYW5ndWFnZXMuanM9UHJpc20ubGFuZ3VhZ2VzLmphdmFzY3JpcHQ7CiFmdW5jdGlvbigpeyJ1bmRlZmluZWQiIT10eXBlb2Ygc2VsZiYmc2VsZi5QcmlzbSYmc2VsZi5kb2N1bWVudCYmUHJpc20uaG9va3MuYWRkKCJjb21wbGV0ZSIsZnVuY3Rpb24oZSl7aWYoZS5jb2RlKXt2YXIgdD1lLmVsZW1lbnQucGFyZW50Tm9kZSxzPS9ccypcYmxpbmUtbnVtYmVyc1xiXHMqLztpZih0JiYvcHJlL2kudGVzdCh0Lm5vZGVOYW1lKSYmKHMudGVzdCh0LmNsYXNzTmFtZSl8fHMudGVzdChlLmVsZW1lbnQuY2xhc3NOYW1lKSkmJiFlLmVsZW1lbnQucXVlcnlTZWxlY3RvcigiLmxpbmUtbnVtYmVycy1yb3dzIikpe3MudGVzdChlLmVsZW1lbnQuY2xhc3NOYW1lKSYmKGUuZWxlbWVudC5jbGFzc05hbWU9ZS5lbGVtZW50LmNsYXNzTmFtZS5yZXBsYWNlKHMsIiIpKSxzLnRlc3QodC5jbGFzc05hbWUpfHwodC5jbGFzc05hbWUrPSIgbGluZS1udW1iZXJzIik7dmFyIG4sYT1lLmNvZGUubWF0Y2goL1xuKD8hJCkvZyksbD1hP2EubGVuZ3RoKzE6MSxyPW5ldyBBcnJheShsKzEpO3I9ci5qb2luKCI8c3Bhbj48L3NwYW4+Iiksbj1kb2N1bWVudC5jcmVhdGVFbGVtZW50KCJzcGFuIiksbi5zZXRBdHRyaWJ1dGUoImFyaWEtaGlkZGVuIiwidHJ1ZSIpLG4uY2xhc3NOYW1lPSJsaW5lLW51bWJlcnMtcm93cyIsbi5pbm5lckhUTUw9cix0Lmhhc0F0dHJpYnV0ZSgiZGF0YS1zdGFydCIpJiYodC5zdHlsZS5jb3VudGVyUmVzZXQ9ImxpbmVudW1iZXIgIisocGFyc2VJbnQodC5nZXRBdHRyaWJ1dGUoImRhdGEtc3RhcnQiKSwxMCktMSkpLGUuZWxlbWVudC5hcHBlbmRDaGlsZChuKX19fSl9KCk7Cg=="></script>
                <link rel="stylesheet" href="data:text/css;base64,LyogaHR0cDovL3ByaXNtanMuY29tL2Rvd25sb2FkLmh0bWw/dGhlbWVzPXByaXNtJmxhbmd1YWdlcz1jbGlrZStqYXZhc2NyaXB0JnBsdWdpbnM9bGluZS1udW1iZXJzICovCi8qKgogKiBwcmlzbS5qcyBkZWZhdWx0IHRoZW1lIGZvciBKYXZhU2NyaXB0LCBDU1MgYW5kIEhUTUwKICogQmFzZWQgb24gZGFiYmxldCAoaHR0cDovL2RhYmJsZXQuY29tKQogKiBAYXV0aG9yIExlYSBWZXJvdQogKi8KCmNvZGVbY2xhc3MqPSJsYW5ndWFnZS0iXSwKcHJlW2NsYXNzKj0ibGFuZ3VhZ2UtIl0gewoJY29sb3I6IGJsYWNrOwoJYmFja2dyb3VuZDogbm9uZTsKCXRleHQtc2hhZG93OiAwIDFweCB3aGl0ZTsKCWZvbnQtZmFtaWx5OiBDb25zb2xhcywgTW9uYWNvLCAnQW5kYWxlIE1vbm8nLCAnVWJ1bnR1IE1vbm8nLCBtb25vc3BhY2U7Cgl0ZXh0LWFsaWduOiBsZWZ0OwoJd2hpdGUtc3BhY2U6IHByZTsKCXdvcmQtc3BhY2luZzogbm9ybWFsOwoJd29yZC1icmVhazogbm9ybWFsOwoJd29yZC13cmFwOiBub3JtYWw7CglsaW5lLWhlaWdodDogMS41OwoKCS1tb3otdGFiLXNpemU6IDQ7Cgktby10YWItc2l6ZTogNDsKCXRhYi1zaXplOiA0OwoKCS13ZWJraXQtaHlwaGVuczogbm9uZTsKCS1tb3otaHlwaGVuczogbm9uZTsKCS1tcy1oeXBoZW5zOiBub25lOwoJaHlwaGVuczogbm9uZTsKfQoKcHJlW2NsYXNzKj0ibGFuZ3VhZ2UtIl06Oi1tb3otc2VsZWN0aW9uLCBwcmVbY2xhc3MqPSJsYW5ndWFnZS0iXSA6Oi1tb3otc2VsZWN0aW9uLApjb2RlW2NsYXNzKj0ibGFuZ3VhZ2UtIl06Oi1tb3otc2VsZWN0aW9uLCBjb2RlW2NsYXNzKj0ibGFuZ3VhZ2UtIl0gOjotbW96LXNlbGVjdGlvbiB7Cgl0ZXh0LXNoYWRvdzogbm9uZTsKCWJhY2tncm91bmQ6ICNiM2Q0ZmM7Cn0KCnByZVtjbGFzcyo9Imxhbmd1YWdlLSJdOjpzZWxlY3Rpb24sIHByZVtjbGFzcyo9Imxhbmd1YWdlLSJdIDo6c2VsZWN0aW9uLApjb2RlW2NsYXNzKj0ibGFuZ3VhZ2UtIl06OnNlbGVjdGlvbiwgY29kZVtjbGFzcyo9Imxhbmd1YWdlLSJdIDo6c2VsZWN0aW9uIHsKCXRleHQtc2hhZG93OiBub25lOwoJYmFja2dyb3VuZDogI2IzZDRmYzsKfQoKQG1lZGlhIHByaW50IHsKCWNvZGVbY2xhc3MqPSJsYW5ndWFnZS0iXSwKCXByZVtjbGFzcyo9Imxhbmd1YWdlLSJdIHsKCQl0ZXh0LXNoYWRvdzogbm9uZTsKCX0KfQoKLyogQ29kZSBibG9ja3MgKi8KcHJlW2NsYXNzKj0ibGFuZ3VhZ2UtIl0gewoJcGFkZGluZzogMWVtOwoJbWFyZ2luOiAuNWVtIDA7CglvdmVyZmxvdzogYXV0bzsKfQoKOm5vdChwcmUpID4gY29kZVtjbGFzcyo9Imxhbmd1YWdlLSJdLApwcmVbY2xhc3MqPSJsYW5ndWFnZS0iXSB7CgliYWNrZ3JvdW5kOiAjZjVmMmYwOwp9CgovKiBJbmxpbmUgY29kZSAqLwo6bm90KHByZSkgPiBjb2RlW2NsYXNzKj0ibGFuZ3VhZ2UtIl0gewoJcGFkZGluZzogLjFlbTsKCWJvcmRlci1yYWRpdXM6IC4zZW07Cgl3aGl0ZS1zcGFjZTogbm9ybWFsOwp9CgoudG9rZW4uY29tbWVudCwKLnRva2VuLnByb2xvZywKLnRva2VuLmRvY3R5cGUsCi50b2tlbi5jZGF0YSB7Cgljb2xvcjogc2xhdGVncmF5Owp9CgoudG9rZW4ucHVuY3R1YXRpb24gewoJY29sb3I6ICM5OTk7Cn0KCi5uYW1lc3BhY2UgewoJb3BhY2l0eTogLjc7Cn0KCi50b2tlbi5wcm9wZXJ0eSwKLnRva2VuLnRhZywKLnRva2VuLmJvb2xlYW4sCi50b2tlbi5udW1iZXIsCi50b2tlbi5jb25zdGFudCwKLnRva2VuLnN5bWJvbCwKLnRva2VuLmRlbGV0ZWQgewoJY29sb3I6ICM5MDU7Cn0KCi50b2tlbi5zZWxlY3RvciwKLnRva2VuLmF0dHItbmFtZSwKLnRva2VuLnN0cmluZywKLnRva2VuLmNoYXIsCi50b2tlbi5idWlsdGluLAoudG9rZW4uaW5zZXJ0ZWQgewoJY29sb3I6ICM2OTA7Cn0KCi50b2tlbi5vcGVyYXRvciwKLnRva2VuLmVudGl0eSwKLnRva2VuLnVybCwKLmxhbmd1YWdlLWNzcyAudG9rZW4uc3RyaW5nLAouc3R5bGUgLnRva2VuLnN0cmluZyB7Cgljb2xvcjogI2E2N2Y1OTsKCWJhY2tncm91bmQ6IGhzbGEoMCwgMCUsIDEwMCUsIC41KTsKfQoKLnRva2VuLmF0cnVsZSwKLnRva2VuLmF0dHItdmFsdWUsCi50b2tlbi5rZXl3b3JkIHsKCWNvbG9yOiAjMDdhOwp9CgoudG9rZW4uZnVuY3Rpb24gewoJY29sb3I6ICNERDRBNjg7Cn0KCi50b2tlbi5yZWdleCwKLnRva2VuLmltcG9ydGFudCwKLnRva2VuLnZhcmlhYmxlIHsKCWNvbG9yOiAjZTkwOwp9CgoudG9rZW4uaW1wb3J0YW50LAoudG9rZW4uYm9sZCB7Cglmb250LXdlaWdodDogYm9sZDsKfQoudG9rZW4uaXRhbGljIHsKCWZvbnQtc3R5bGU6IGl0YWxpYzsKfQoKLnRva2VuLmVudGl0eSB7CgljdXJzb3I6IGhlbHA7Cn0KCnByZS5saW5lLW51bWJlcnMgewoJcG9zaXRpb246IHJlbGF0aXZlOwoJcGFkZGluZy1sZWZ0OiAzLjhlbTsKCWNvdW50ZXItcmVzZXQ6IGxpbmVudW1iZXI7Cn0KCnByZS5saW5lLW51bWJlcnMgPiBjb2RlIHsKCXBvc2l0aW9uOiByZWxhdGl2ZTsKfQoKLmxpbmUtbnVtYmVycyAubGluZS1udW1iZXJzLXJvd3MgewoJcG9zaXRpb246IGFic29sdXRlOwoJcG9pbnRlci1ldmVudHM6IG5vbmU7Cgl0b3A6IDA7Cglmb250LXNpemU6IDEwMCU7CglsZWZ0OiAtMy44ZW07Cgl3aWR0aDogM2VtOyAvKiB3b3JrcyBmb3IgbGluZS1udW1iZXJzIGJlbG93IDEwMDAgbGluZXMgKi8KCWxldHRlci1zcGFjaW5nOiAtMXB4OwoJYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgIzk5OTsKCgktd2Via2l0LXVzZXItc2VsZWN0OiBub25lOwoJLW1vei11c2VyLXNlbGVjdDogbm9uZTsKCS1tcy11c2VyLXNlbGVjdDogbm9uZTsKCXVzZXItc2VsZWN0OiBub25lOwoKfQoKCS5saW5lLW51bWJlcnMtcm93cyA+IHNwYW4gewoJCXBvaW50ZXItZXZlbnRzOiBub25lOwoJCWRpc3BsYXk6IGJsb2NrOwoJCWNvdW50ZXItaW5jcmVtZW50OiBsaW5lbnVtYmVyOwoJfQoKCQkubGluZS1udW1iZXJzLXJvd3MgPiBzcGFuOmJlZm9yZSB7CgkJCWNvbnRlbnQ6IGNvdW50ZXIobGluZW51bWJlcik7CgkJCWNvbG9yOiAjOTk5OwoJCQlkaXNwbGF5OiBibG9jazsKCQkJcGFkZGluZy1yaWdodDogMC44ZW07CgkJCXRleHQtYWxpZ246IHJpZ2h0OwoJCX0K" />
                <script>
                <![CDATA[
                $(function () {
                    $('[data-source]').each(function () {
                        var self = $(this), src = self.data('source');
                        $('<code></code>')
                            .appendTo(self.empty())
                            .text('Loading ...')
                            .load(self.data('source'), function (evt) {
                                Prism.highlightElement(self.get(0));
                            });
                    });
                })
                ]]>
                </script>
                <style>
                <![CDATA[
                * {
                    box-sizing: border-box;
                }
                body {
                    font-family: "Helvetica Neue LT Std 45 Light", Helvetica, Arial, sans-serif;
                    font-size: 14px;
                }

                @media(min-width: 768px) {
                    .container { width: 750px; }
                }

                @media(min-width: 992px) {
                    .container { width: 970px; }
                }

                @media(min-width: 1200px) {
                    .container { width: 1170px; }
                }

                .container {
                    padding-right: 15px;
                    padding-left: 15px;
                    margin-right: auto;
                    margin-left: auto;
                }

                .panel {
                    border: 1px solid transparent;
                    border-radius: 4px;
                    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
                    margin-bottom: 20px;
                }

                .panel-heading {
                    border-bottom: 1px solid transparent;
                    border-top-left-radius: 3px;
                    border-top-right-radius: 3px;
                    padding: 10px 15px;
                }

                .panel-default {
                    border-color: #ddd;
                }

                .panel-default .panel-heading {
                    color: #333;
                    background-color: #f5f5f5;
                    border-color: #ddd;
                }

                .panel-info {
                    border-color: #bce8f1;
                }

                .panel-info .panel-heading {
                    color: #31708f;
                    background-color: #d9edf7;
                    border-color: #bce8f1;
                }

                .panel-success {
                    border-color: #d6e9c6;
                }

                .panel-success .panel-heading {
                    color: #3c763d;
                    background-color: #dff0d8;
                    border-color: #d6e9c6;
                }

                .panel-warning {
                    border-color: #faebcc;
                }

                .panel-warning .panel-heading {
                    color: #8a6d3b;
                    background-color: #fcf8e3;
                    border-color: #faebcc;
                }

                .panel-danger {
                    border-color: #ebccd1;
                }

                .panel-danger .panel-heading {
                    color: #a94442;
                    background-color: #f2dede;
                    border-color: #ebccd1;
                }

                .panel-body {
                    padding: 15px;
                }

                a {
                    color: #337ab7;
                    text-decoration: none;
                }

                a:focus, a:hover {
                    color: #23527c;
                    text-decoration: underline;
                }

                .text-center {
                    text-align: center;
                }

                .text-right {
                    text-align: right;
                }

                .text-left {
                    text-align: left;
                }

                .text-muted {
                    color: #aaa;
                }

                h1 {
                    font-size: 2.6em;
                }

                h3 {
                    font-size: 1.7em;
                }

                h1, h3 {
                    font-weight: normal;
                    margin-top: 20px;
                    margin-bottom: 10px;
                    line-height: 1.1;
                }
                dl, .row {
                    display: block;
                    margin: 0 -15px;
                }

                dl:before, .row:before {
                    content: " ";
                    display: table;
                }

                dl:after, .row:after {
                    content: " ";
                    display: table;
                    clear: both;
                }

                dt, dd, .col-md-1, .col-md-2, .col-md-8, .col-md-10, .col-md-11, .col-md-12 {
                    float: left;
                    min-height: 1px;
                    padding-left: 15px;
                    padding-right: 15px;
                    position: relative;
                }

                .col-md-1  { width:   8.333333%; }
                .col-md-2  { width:  16.666667%; }
                .col-md-8  { width:  66.666667%; }
                .col-md-9  { width:  75.000000%; }
                .col-md-10 { width:  83.333333%; }
                .col-md-11 { width:  93.333333%; }
                .col-md-12 { width: 100.000000%; }

                .col-md-offset-1  { margin-left:   8.333333%; }
                .col-md-offset-2  { margin-left:  16.666667%; }
                .col-md-offset-8  { margin-left:  66.666667%; }
                .col-md-offset-9  { margin-left:  75.000000%; }
                .col-md-offset-10 { margin-left:  83.333333%; }
                .col-md-offset-11 { margin-left:  93.333333%; }
                .col-md-offset-12 { margin-left: 100.000000%; }

                dl {
                    margin-bottom: 15px;
                }

                dt {
                    display: inline-block;
                    font-weight: bold;
                    margin: 0px;
                    padding-top: 7px;
                    text-align: right;
                    width: 16.666667%;
                }

                dd {
                    display: inline-block;
                    margin: 0px;
                    width: 83.333333%;
                }

                dd div {
                    display: block;
                    border: 1px solid #ccc;
                    border-radius: 4px;
                    box-shadow: inset 0 1px 1px rgba(0, 0, 0, .1);
                    padding: 6px 12px;
                }

                dd div pre[class*="language-"] {
                    border-radius: 4px;
                    margin: -6px -12px;
                }
                ]]>
                </style>
            </head>
            <body>
                <xsl:apply-templates />
            </body>
        </html>
    </xsl:template>

    <xsl:template name="buildSection">
        <xsl:param name="label" />
        <div class="row">
            <div class="col-md-12">
                <h3><xsl:value-of select="$label" /></h3>
            </div>
        </div>
        <form class="form-horizontal">
            <xsl:apply-templates />
        </form>
    </xsl:template>

    <xsl:template name="buildAttribute">
        <xsl:param name="label" />
        <xsl:param name="value" select="." />
        <dl>
            <dt>
                <xsl:value-of select="$label" />
            </dt>
            <dd>
                <div>
                    <xsl:copy-of select="$value" />
                </div>
            </dd>
        </dl>
    </xsl:template>

</xsl:stylesheet>
