//--------------- VARIABLES -------------------//

/* init datas pour review (variables génériques)
 *
 */
 
var data = [] ;
var pjUser = [];
var metas = [];
var repCerfa = 'models/';
var iCardinality = 0 ;

/* init datas pour cerfa (variables spécifiques)
 * contient a minnima les références des groupes 
 * génériques de data.xml
 */

/*
 * init pointeurs saisies, Pj...
 */
 
var datasPtr = $ds006cerfa15271 ;
var datasSignature = datasPtr.cadreSignatureGroupe ;
var datasIdentite = datasPtr.cadreIdentiteGroupe ;
var refFormTitle = 'Centre équestre : Icpe' ;

if (typeof datasPtr.cadreDemandeGroupe !== 'undefined') {
    var datasDemande = datasPtr.cadreDemandeGroupe ;
} else {
	var datasDemande = null ;
}

/*
 * init pointeurs PJ déclarants ou mandataires
 */
 
var pjSignataire = datasSignature.cadreSignature.soussigne ;
var idDeclarant = 'signataireQualiteDeclarant' ;
var idMandataire = 'signataireQualiteMandataire' ;

/* import description du CERFA (si il existe)
 * il génère les tables de paramétrage formFields et equivCerfa
 * formFields = dictionnaire des valeurs des champs du Cerfa
 * equivCerfa = dictionnaire de description des champs du Cerfa
 * il définit les variables repCerfa et nameCerfa
 * nameCerfa = nom du fichier pdf associé au Cerfa
 */

/*
 * Remplissage des champs du formulaire (autorisation de desossage)
 */

if (datasDemande !== null) {
	if ((Value('id').of(datasDemande.cadreDemande.nature).eq('depotDossier')) &&  (Value('id').of(datasDemande.cadreDemande.objet).eq(''))) {
		var flgCerfa = true;
	} else {
		var flgCerfa = false;
	}
} else {
	var flgCerfa = true;
}

if (flgCerfa === true) {
	
	//constantes et variables
	var formFields = {} ;
	var nameCerfa = 'cerfa_15271.pdf' ;
	//pointeurs pages de saisie
	var datasInfosDeclarant = datasPtr.infosDeclarant ; //page 1
	var datasInfosInstallation = datasPtr.infosInstallation ; // page 2
	var datasInfosActivites = datasPtr.infosActivites ; // page 3
	var datasInfosEaux = datasPtr.infosEaux ; // page 4
	var datasInfosRejets = datasPtr.infosRejets ; // page 5
	var datasInfosAgrements = datasPtr.infosAgrements // page 6
	
	//paramétrages des cardinalités
	var setActivites = "[ "
		+ "{ src: root.rubrique.id, dst: 'numeroRubrique[i]', valeur: null, type: 'string' },"
		+ "{ src: root.rubriqueAutre, dst: 'numeroRubrique[i]', valeur: root.rubriqueAutre, type: 'xString' },"
		+ "{ src: root.rubrique, dst: 'designationRubrique[i]', valeur: null, type: 'string' },"
		+ "{ src: root.designationAutre, dst: 'designationRubrique[i]', valeur: root.designationAutre, type: 'xString' },"
		+ "{ src: root.alinea, dst: 'alinea[i]', valeur: null, type: 'string' },"
		+ "{ src: root.capacite, dst: 'capaciteActivite[i]', valeur: null, type: 'string' },"
		+ "{ src: root.unite, dst: 'unite[i]', valeur: null, type: 'string' },"
		+ "{ src: root.regime, dst: 'regime[i]', valeur: null, type: 'string' }"
		+ " ] ;" ;

	var setAgrements = "[ "
		+ "{ src: root.nature, dst: 'natureDechets[i]', valeur: null, type: 'string' },"
		+ "{ src: root.codifDechet, dst: 'codificationDechets[i]', valeur: null, type: 'string' },"
		+ "{ src: root.traitement, dst: 'traitement[i]', valeur: null, type: 'string' },"
		+ "{ src: root.codifTraitement, dst: 'codification[i]', valeur: null, type: 'string' },"
		+ "{ src: root.quantite, dst: 'quantitesMaximales[i]', valeur: null, type: 'string' }"
		+ " ] ;" ;
	
	//description des zones génériques du Cerfa (type bool, type string...)
	var equivCerfa = [
		// page 1 saisie
		{ src: datasInfosDeclarant.personnePhysique, dst: 'personnePhysiqueCoche', valeur: true, type: 'bool' },
		{ src: datasInfosDeclarant.personnePhysique, dst: 'personneMoraleCoche', valeur: false, type: 'bool' },
    	{ src: datasInfosDeclarant.personnePhysique, dst: 'monsieur', valeur: [datasIdentite.cadreIdentite.civilite, 'civiliteMasculin'], type: 'bRadio' },
    	{ src: datasInfosDeclarant.personnePhysique, dst: 'madame', valeur: [datasIdentite.cadreIdentite.civilite, 'civiliteFeminin'], type: 'bRadio' },
    	{ src: null, dst: 'nomRaisonSocialeDeclarant', valeur: extractInfoIdentite(datasIdentite.cadreIdentite, 'X', true), type: 'string' },
    	{ src: datasInfosDeclarant.infosPM.raisonSociale, dst: 'nomRaisonSocialeDeclarant', valeur: datasInfosDeclarant.infosPM.raisonSociale, type: 'xString' },
    	{ src: datasInfosDeclarant.infosPM.formeJuridique, dst: 'formeJuridique', valeur: null, type: 'string' },
    	{ src: datasInfosDeclarant.infosPM.siret, dst: 'numeroSiret', valeur: null, type: 'string' },
    	{ src: null, dst: 'numeroNomVoieAdresse', valeur: mergeAdresse(datasIdentite.cadreAdresse, 'V', false), type: 'string' },
    	{ src: null, dst: 'complementAdresse', valeur: mergeAdresse(datasIdentite.cadreAdresse, 'S', false), type: 'string' },
    	{ src: datasIdentite.cadreAdresse.codePostal, dst: 'codePostalAdresse', valeur: null, type: 'string' },
    	{ src: null, dst: 'communeAdresse', valeur: extractInfoAdresse(datasIdentite.cadreAdresse, 'C', false), type: 'string' },
    	{ src: null, dst: 'paysEtrangerAdresse', valeur: extractInfoAdresse(datasIdentite.cadreAdresse, 'P', false), type: 'string' },
    	{ src: datasIdentite.cadreContact.telephoneFixe.telephoneNum, dst: 'telephoneFixeDeclarant', valeur: null, type: 'string' },
    	{ src: datasIdentite.cadreContact.telephoneMobile.telephoneNum, dst: 'telephonePortableDeclarant', valeur: null, type: 'string' },
    	{ src: datasIdentite.cadreContact.courriel, dst: 'courrielDeclarant', valeur: null, type: 'string' },
    	{ src: !datasInfosDeclarant.personnePhysique, dst: 'nomSignataire', valeur: extractInfoIdentite(datasIdentite.cadreIdentite, 'N', true), type: 'bString' },
    	{ src: !datasInfosDeclarant.personnePhysique, dst: 'prenomsSignataire', valeur: extractInfoIdentite(datasIdentite.cadreIdentite, 'P', true), type: 'bString' },
    	{ src: datasInfosDeclarant.infosPM.qualiteSignataire, dst: 'qualiteSignataire', valeur: null, type: 'string' },
		// page 2 saisie
    	{ src: datasInfosInstallation.siret, dst: 'numeroSiretInstallation', valeur: null, type: 'string' },
    	{ src: datasInfosInstallation.nomSite, dst: 'siteNomInstallation', valeur: null, type: 'string' },
		{ src: datasInfosInstallation.adresseDifferente, dst: 'adresseIdentiqueInstallation', valeur: false, type: 'bool' },
    	{ src: null, dst: 'numeroNomVoieAdresseInstallation', valeur: mergeAdresse(datasInfosInstallation.infosAdresse.cadreAdresse, 'V', false), type: 'string' },
    	{ src: null, dst: 'complementAdresseInstallation', valeur: mergeAdresse(datasInfosInstallation.infosAdresse.cadreAdresse, 'C', false), type: 'string' },
    	{ src: datasInfosInstallation.infosAdresse.cadreAdresse.codePostal, dst: 'codePostalAdresseInstallation', valeur: null, type: 'string' },
    	{ src: null, dst: 'communeAdresseInstallation', valeur: extractInfoAdresse(datasInfosInstallation.infosAdresse.cadreAdresse, 'C', false), type: 'string' },
    	{ src: datasInfosInstallation.infosContact.cadreContact.telephoneFixe.telephoneNum, dst: 'telephoneFixeInstallation', valeur: null, type: 'string' },
    	{ src: datasInfosInstallation.infosContact.cadreContact.telephoneMobile.telephoneNum, dst: 'telephonePortableInstallation', valeur: null, type: 'string' },
    	{ src: datasInfosInstallation.infosContact.cadreContact.courriel, dst: 'courrielInstallation', valeur: null, type: 'string' },
    	{ src: datasInfosInstallation.infosContact.cadreContact.courriel, dst: 'courrielInstallation', valeur: null, type: 'string' },	
    	{ src: datasInfosInstallation.descriptionGenerale.description, dst: 'descriptionGeneraleInstallation', valeur: null, type: 'string' },
    	{ src: datasInfosInstallation.descriptionGenerale.existant, dst: 'installationRegimeAutorisationOui', valeur: 'autorisation', type: 'check' },
    	{ src: true, dst: 'installationRegimeAutorisationNon', valeur: [ datasInfosInstallation.descriptionGenerale.existant, 'autorisation'], type: 'xCheck' },
    	{ src: datasInfosInstallation.descriptionGenerale.existant, dst: 'installationRegimeEnregistrementOui', valeur: 'enregistrement', type: 'check' },
    	{ src: true, dst: 'installationRegimeEnregistrementNon', valeur: [ datasInfosInstallation.descriptionGenerale.existant, 'enregistrement'], type: 'xCheck' },
    	{ src: datasInfosInstallation.descriptionGenerale.existant, dst: 'installationRegimeDeclarationOui', valeur: 'declaration', type: 'check' },
    	{ src: true, dst: 'installationRegimeDeclarationNon', valeur: [ datasInfosInstallation.descriptionGenerale.existant, 'declaration'], type: 'xCheck' },
		{ src: datasInfosInstallation.plusieursDepartements, dst: 'implantationPlusieursDepartementsOui', valeur: true, type: 'bool' },
    	{ src: true, dst: 'implantationPlusieursDepartementsNon', valeur: [datasInfosInstallation.plusieursDepartements, true], type: 'xBool' },
    	{ src: datasInfosInstallation.departements.liste, dst: 'implantationPlusieursDepartements', valeur: null, type: 'string' },
		{ src: datasInfosInstallation.plusieursCommunes, dst: 'implantationPlusieursCommunesOui', valeur: true, type: 'bool' },
    	{ src: true, dst: 'implantationPlusieursCommunesNon', valeur: [datasInfosInstallation.plusieursCommunes, true], type: 'xBool' },
    	{ src: datasInfosInstallation.communes.liste, dst: 'implantationPlusieursCommunes', valeur: null, type: 'string' },
		{ src: datasInfosInstallation.permisConstruire, dst: 'permisConstruireOui', valeur: true, type: 'bool' },
    	{ src: true, dst: 'permisConstruireNon', valeur: [datasInfosInstallation.permisConstruire, true], type: 'xBool' },
		{ src: datasInfosInstallation.natura2000, dst: 'naturaOui', valeur: true, type: 'bool' },
    	{ src: true, dst: 'naturaNon', valeur: [datasInfosInstallation.natura2000, true], type: 'xBool' },
		{ src: null, dst: 'prescriptionsNon', valeur: true, type: 'bool' },
		// page 3 saisie
		{ src: datasInfosActivites.cadreActivites, dst: 'datasInfosActivites.cadreActivites', valeur: setActivites, type: 'cardinality' },		
		{ src: datasInfosActivites.commentaires, dst: 'commentaireNatureActivite', valeur: null, type: 'string' },
		// page 4 saisie
		{ src: datasInfosEaux.prelevementEau, dst: 'prelevementEauExploitationOui', valeur: true, type: 'bool' },
    	{ src: true, dst: 'prelevementEauExploitationNon', valeur: [datasInfosEaux.prelevementEau, true], type: 'xBool' },
    	{ src: datasInfosEaux.infosPrelevementEau.typePrelevement, dst: 'reseauPublicExploitation', valeur: 'reseauPublic', type: 'check' },
    	{ src: datasInfosEaux.infosPrelevementEau.reseauPublic, dst: 'volumeReseauPublicExploitation', valeur: null, type: 'string' },
    	{ src: datasInfosEaux.infosPrelevementEau.typePrelevement, dst: 'milieuNaturelExploitation', valeur: 'milieuNaturel', type: 'check' },
    	{ src: datasInfosEaux.infosPrelevementEau.milieuNaturel, dst: 'volumeMilieuNaturelExploitation', valeur: null, type: 'string' },
    	{ src: datasInfosEaux.infosPrelevementEau.typePrelevement, dst: 'forageSouterrainExploitation', valeur: 'forageSouterrain', type: 'check' },
    	{ src: datasInfosEaux.infosPrelevementEau.forageSouterrainHauteur, dst: 'forageSouterrainPlus10Exploitation', valeur: true, type: 'bool' },
    	{ src: datasInfosEaux.infosPrelevementEau.forageSouterrain, dst: 'volumeForageSouterrain', valeur: null, type: 'string' },
    	{ src: datasInfosEaux.infosPrelevementEau.typePrelevement, dst: 'autresExploitationCoche', valeur: 'autre', type: 'check' },
    	{ src: datasInfosEaux.infosPrelevementEau.autre, dst: 'autresExploitation', valeur: null, type: 'string' },
		{ src: datasInfosEaux.rejetEau, dst: 'rejetEauxResiduairesOui', valeur: true, type: 'bool' },
    	{ src: true, dst: 'rejetEauxResiduairesNon', valeur: [datasInfosEaux.rejetEau, true], type: 'xBool' },
    	{ src: datasInfosEaux.infosRejetEau.origine, dst: 'origineNatureEauxResiduaires', valeur: null, type: 'string' },
    	{ src: datasInfosEaux.infosRejetEau.exutoire, dst: 'reseauAssainissementCollectif', valeur: 'reseauAssainissement', type: 'check' },
    	{ src: datasInfosEaux.infosRejetEau.exutoire, dst: 'milieuNaturel', valeur: 'autreReseau', type: 'check' },
    	{ src: datasInfosEaux.infosRejetEau.traitements, dst: 'traitementEauxResiduaires', valeur: null, type: 'string' },
    	{ src: datasInfosEaux.infosRejetEau.rejetMilieuNaturel, dst: 'volumeAnnuelRejete', valeur: null, type: 'string' },
    	{ src: datasInfosEaux.infosRejetEau.commentaires, dst: 'autresCommentairesEauxResiduaires', valeur: null, type: 'string' },
		// page 5 saisie
		{ src: datasInfosRejets.epandage, dst: 'epandageDechetsOui', valeur: true, type: 'bool' },
    	{ src: true, dst: 'epandageDechetsNon', valeur: [datasInfosRejets.epandage, true], type: 'xBool' },
    	{ src: datasInfosRejets.infosEpandage.origine, dst: 'epandageDechets', valeur: null, type: 'string' },
    	{ src: datasInfosRejets.infosEpandage.ilotsPac, dst: 'ilotPac', valeur: null, type: 'string' },
    	{ src: datasInfosRejets.infosEpandage.surface, dst: 'surfaceTotaleEpandage', valeur: null, type: 'string' },
    	{ src: datasInfosRejets.infosEpandage.volume, dst: 'capaciteStockage', valeur: null, type: 'string' },
    	{ src: datasInfosRejets.infosEpandage.infosAzote.azoteVolume, dst: 'quantiteAzoteEpandue', valeur: null, type: 'string' },
    	{ src: datasInfosRejets.infosEpandage.infosAzote.azoteExploitation, dst: 'epandueA1', valeur: null, type: 'string' },
    	{ src: datasInfosRejets.infosEpandage.infosAzote.azoteAutres, dst: 'epandueA2', valeur: null, type: 'string' },
    	{ src: datasInfosRejets.infosEpandage.infosAzote.azoteProduites, dst: 'produitB1', valeur: null, type: 'string' },
    	{ src: datasInfosRejets.infosEpandage.infosAzote.azoteProduite, dst: 'produitB1', valeur: null, type: 'string' },
    	{ src: datasInfosRejets.infosEpandage.infosAzote.azoteTiers, dst: 'produitB2', valeur: null, type: 'string' },
		{ src: datasInfosRejets.atmosphere, dst: 'rejetsAtmosphereOui', valeur: true, type: 'bool' },
    	{ src: true, dst: 'rejetsAtmosphereNon', valeur: [datasInfosRejets.atmosphere, true], type: 'xBool' },
    	{ src: datasInfosRejets.infosAtmosphere.origine, dst: 'origineNatureRejets', valeur: null, type: 'string' },
    	{ src: datasInfosRejets.infosAtmosphere.traitements, dst: 'dispositifsCaptationTraitement', valeur: null, type: 'string' },
    	{ src: datasInfosRejets.infosAtmosphere.commentaires, dst: 'autresCommentairesRejets', valeur: null, type: 'string' },
    	{ src: datasInfosRejets.infosDechets.origine, dst: 'autresCommentairesDechets', valeur: null, type: 'string' },
		{ src: datasInfosRejets.infosDechets.collecte, dst: 'collecteDechetsPublicOui', valeur: true, type: 'bool' },
    	{ src: true, dst: 'collecteDechetsPublicNon', valeur: [datasInfosRejets.infosDechets.collecte, true], type: 'xBool' },
    	{ src: datasInfosRejets.infosSinistres.incendie, dst: 'priseEauSinistre', valeur: 'reseauPublic', type: 'check' },
    	{ src: datasInfosRejets.infosSinistres.incendie, dst: 'autreSinistre', valeur: 'autre', type: 'check' },
    	{ src: datasInfosRejets.infosSinistres.autreIncendie, dst: 'autreCommentaireSinistre', valeur: null, type: 'string' },
    	{ src: datasInfosRejets.infosSinistres.autres, dst: 'autresMoyensSinistre', valeur: null, type: 'string' },
		// page 6 saisie
		{ src: datasInfosAgrements.agrements, dst: 'demandeAgrementOui', valeur: true, type: 'bool' },
    	{ src: true, dst: 'demandeAgrementNon', valeur: [datasInfosAgrements.agrements, true], type: 'xBool' },
		{ src: datasInfosAgrements.demandeAgrements.cadreAgrement, dst: 'datasInfosAgrements.demandeAgrements.cadreAgrement', valeur: setAgrements, type: 'cardinality' },		
		// bloc signature
		{ src: datasSignature.cadreSignature.signatureLieu, dst: 'signatureLieu', valeur: null, type: 'string' },
		{ src: null, dst: 'signatureDate', valeur: formatDate(datasSignature.cadreSignature.signatureDate, 'T', true), type: 'string' },
		{ src: null, dst: 'signature', valeur: genereInfoSignataire(true, true), type: 'string' }
	] ; 
}


//--------------- VARIABLES -------------------//

//--------------- FONCTIONS -------------------//

/* fonctions génériques de formatage des données
 *
 */

/*
 * calculs mathématiques : calcul de pourcentage
 */
 
function calculPercent(valNumerateur, valDenominateur, nbDecimales) {
	var valResult = '' ;
	var tmpValeur = 0 ;
	while (true) {
		if (typeof valNumerateur === 'undefined' || typeof valNumerateur !== 'number') {
			break ;
		}
		if (typeof valDenominateur === 'undefined' || typeof valDenominateur !== 'number') {
			break;
		}
		if (typeof nbDecimales === 'undefined' || typeof nbDecimales !== 'number') {
			tmpValeur = 0 ;
		} else {
			tmpValeur = nbDecimales >= 0 ? nbDecimales : 0 ; 
		}	
		tmpValeur = Math.pow(10, tmpValeur);
		if (valDenominateur !== 0) {
			tmpValeur = Math.round(((valNumerateur / valDenominateur) * 100) / tmpValeur) ;
			valResult = tmpValeur.toString();
			
		}
		break ;
	}
	return valResult ;
 }

/*
 * calculs mathématiques : calcul de soustraction
 */
 
function calculSubstraction(valTotal, tabValeurs, cumulValeur, keyCumul) {
	var valResult = '' ;
	var tmpValeur = 0 ;
	while (true) {
		if (typeof valTotal === 'undefined' || typeof valTotal !== 'number') {
			break ;
		}
		tmpValeur = valTotal ;
		if (typeof tabValeurs === 'object' && Array.isArray(tabValeurs)) {
			for (tmpIndex = 0; tmpIndex < tabValeurs.length; tmpIndex++) {
				if (typeof tabValeurs[tmpIndex] === 'number') {
					tmpValeur -= tabValeurs[tmpIndex];
					tmpFlag = true ;
				}
			}	
		}
		if (tmpFlag === true) {
			valResult = tmpValeur.toString();
			if (typeof cumulValeur === 'object' && cumulValeur !== null && typeof keyCumul === 'string') {
				if (typeof cumulValeur[keyCumul] !== 'undefined' && isNaN(cumulValeur[keyCumul]) === false) {
					cumulValeur[keyCumul] = (Number(cumulValeur[keyCumul]) - tmpValeur).toString() ;
				} else {
					cumulValeur[keyCumul] = valResult ;
				}
			}
		}
		break;
	}
	return valResult ;
 }

function calculAddition(tabValeurs, cumulValeur, keyCumul) {
	var valResult = '' ;
	var tmpFlag = false ;
	var tmpValeur = 0 ;
	var tmpIndex = 0 ;
	while (true) {
		if (typeof tabValeurs === 'object' && Array.isArray(tabValeurs)) {
			for (tmpIndex = 0; tmpIndex < tabValeurs.length; tmpIndex++) {
				if (typeof tabValeurs[tmpIndex] === 'number') {
					tmpValeur += tabValeurs[tmpIndex];
					tmpFlag = true ;
				}
			}	
		}
		if (tmpFlag === true) {
			valResult = tmpValeur.toString();
			if (typeof cumulValeur === 'object' && cumulValeur !== null && typeof keyCumul === 'string') {
				if (typeof cumulValeur[keyCumul] !== 'undefined' && isNaN(cumulValeur[keyCumul]) === false) {
					cumulValeur[keyCumul] = (tmpValeur + Number(cumulValeur[keyCumul])).toString() ;
				} else {
					cumulValeur[keyCumul] = valResult ;
				}
			}
		}
		break ;
	}
	return valResult ;
 }

/* suppression des espaces dans une chaine
 *
 */

function supprimeEspaces(szLibel) {
	var tmpRegex = /\s/g ;
	var tmpLibel = '';
	if (typeof szLibel !== 'undefined' && typeof szLibel === 'string') {
		tmpLibel = szLibel !== null ? szLibel.replace(tmpRegex, '') : '' ;	
	}
	return tmpLibel ;
}
 
/* merge des prénoms (séparateur ',')
 *
 */
	
function mergePrenoms(listPrenoms) {
	var prenoms = [];
	if (typeof listPrenoms !== 'undefined' && typeof listPrenoms === 'object') {
		for (i = 0; i < listPrenoms.length; i++) {
			prenoms.push(listPrenoms[i]);
    	}
	} else {
		prenoms.push('');
	}
	return prenoms.toString();
}

/* merge du nom et des prénoms (séparateur ',')
 *
 */

function mergeNomPrenoms(szNom, listPrenoms) {
	var result = '';
	if (typeof szNom === 'string') {
		result = szNom + ' ';
		result += mergePrenoms(listPrenoms);
	} 
	return result;
}

/* construction de la date et du lieu de naissance
 *
 */

function mergeNaissance(blocNaissance) {
	var naissance = '' ;
	if (typeof blocNaissance !== 'undefined' && typeof blocNaissance === 'object') {
		naissance = blocNaissance.lieuNaissance ? blocNaissance.lieuNaissance + ' / ' : '' ;
		naissance += blocNaissance.paysNaissance ? blocNaissance.paysNaissance : '' ;
	}
	return naissance ;
}
	
/* construction de l'adresse en tenant compte du pays ou non
 *
 */

function mergeAdresse(blocAdresse, typeMerge, multiLine) {
	var tmpVar = '' ;
	var adresse = '' ;
	var chgLigne = ' \n' ;
	if (typeof blocAdresse !== 'undefined' && typeof blocAdresse === 'object') {
		var fmtLigne = (multiLine === true ? chgLigne : ' ');
		var typeAct = (typeof typeMerge !== 'undefined' && typeof typeMerge === 'string') ? typeMerge.toUpperCase() : 'T' ;
		switch (typeAct) {
			case 'N':
				adresse += blocAdresse.numeroVoie ? blocAdresse.numeroVoie + ' ' : '' ;
				adresse += blocAdresse.indiceVoie ? blocAdresse.indiceVoie : '' ;
				break;
			case 'V':	
				tmpVar = mergeAdresse(blocAdresse, 'N', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + ' ' : '' ;
				adresse += mergeAdresse(blocAdresse, 'W', multiLine) ;
				break;
			case 'W':
				adresse += blocAdresse.typeVoie ? blocAdresse.typeVoie + ' ' : '' ;
				adresse += blocAdresse.nomVoie ?  blocAdresse.nomVoie : '' ;
				break;
			case 'S':
				adresse += blocAdresse.complementAdresse ? blocAdresse.complementAdresse + fmtLigne : '' ;
				adresse += blocAdresse.distributionSpeciale ? blocAdresse.distributionSpeciale : '' ;
				break;
			case 'C':
				if (typeof blocAdresse.pays !== 'undefined') {
					if (typeof blocAdresse.codePostal !== 'undefined' && blocAdresse.codePostal) {
						adresse += blocAdresse.codePostal ? blocAdresse.codePostal + ' ' : '' ;
						adresse += blocAdresse.commune ? blocAdresse.commune : '' ;
					} else {
						adresse += blocAdresse.ville ? blocAdresse.ville + fmtLigne : '' ;
						adresse += blocAdresse.pays ? blocAdresse.pays : '' ;
					}
				} else {
					adresse += blocAdresse.codePostal ? blocAdresse.codePostal + ' ' : '' ;
					adresse += blocAdresse.commune ? blocAdresse.commune : '' ;
				}
				break;
			case 'X':
				tmpVar = mergeAdresse(blocAdresse, 'S', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + fmtLigne : '' ;
				adresse += mergeAdresse(blocAdresse, 'C', multiLine) ;
				break;
			case 'Z':
				tmpVar = mergeAdresse(blocAdresse, 'V', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + fmtLigne : '' ;
				adresse += mergeAdresse(blocAdresse, 'S', multiLine) ;
				break;
			case 'T':
			default:
				tmpVar = mergeAdresse(blocAdresse, 'V', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + fmtLigne : '' ;
				tmpVar = mergeAdresse(blocAdresse, 'S', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + fmtLigne : '' ;
				adresse += mergeAdresse(blocAdresse, 'C', multiLine) ;
		}
	}
	return adresse;
}

/*
 * mise en forme numéro de téléphone au format 10 chiffres
 */

function formatNumTelephone(objNumTel) {
	var tmpRegexTelIntro = /^(\+\d{1,3}[\s\.\-]?)(.*)$/ ;
	var tmpRegexTel = /(\d{1,})[\s\.\-]?/g ;
	var tmpTel = '' ;
	var tmpIntro = '';
	var tmpTelFmt = '';
	if (typeof objNumTel !== 'undefined' && typeof objNumTel === 'object' && objNumTel) {
		tmpTel = objNumTel.international ;
		if (tmpRegexTelIntro.test(tmpTel) === true) {
			tmpIntro = tmpTel.replace(tmpRegexTelIntro, '$1') ;
			tmpTelFmt = '$2' ;
			if (tmpIntro.substring(0,3) == '+33') {
				tmpTelFmt = '0' + tmpTelFmt ;
			}	
			tmpTel = tmpTel.replace(tmpRegexTelIntro, tmpTelFmt) ;
			tmpTel = tmpTel.replace(tmpRegexTel, '$1') ;
		}
	}
	return tmpTel ;
}

/*
 * infos contacts téléphoniques
 */
 
function mergeNumTelephone(blocNumTel, flgMerge, flgInternational) {
	var numTel = '';
	var tmpTel = '';
	if (typeof blocNumTel !== 'undefined' && typeof blocNumTel === 'object') {
		if (typeof blocNumTel.telephoneFixe.telephoneNum !== 'undefined' && blocNumTel.telephoneFixe.telephoneNum ) {
			if (flgInternational === false) {
				tmpTel = formatNumTelephone(blocNumTel.telephoneFixe.telephoneNum) ;
			} else {
				tmpTel = blocNumTel.telephoneFixe.telephoneNum.international ;
			}
			numTel += tmpTel ;
		}
		if (typeof blocNumTel.telephoneMobile.telephoneNum !== 'undefined' && blocNumTel.telephoneMobile.telephoneNum) {
			if (numTel.length > 0 && flgMerge === true) {
				numTel += ' / ';
			}
			if (flgMerge === true || numTel.length === 0) {
				if (flgInternational === false) {
					tmpTel = formatNumTelephone(blocNumTel.telephoneMobile.telephoneNum) ;
				} else {
					tmpTel = blocNumTel.telephoneMobile.telephoneNum.international;
				}
				numTel += tmpTel ;
			}
		}
	}
	return numTel;
}

/* formatage d'une date avec extaction ou non d'une partie (J, M, A)
 *
 */

function formatDate(valDate, typeExtract, fullYear) {
	var dateResult = '';
	var tmpYear = '' ;
	if (typeof valDate === 'object' && valDate) {
    	var dateTmp = new Date(parseInt(valDate.getTimeInMillis())) ; ;
		var dateIso = ((dateTmp.toLocaleDateString()).substr(0, 10)).split('-') ;
		var typeAct = (typeof typeExtract !== 'undefined' && typeof typeExtract === 'string') ? typeExtract.toUpperCase() : 'T' ;
		if (dateIso.length > 1) {
			tmpYear = (fullYear === true ? dateIso[0] : dateIso[0].substr(2)) ;
			switch (typeAct) {
				case 'D':
				case 'J':
					dateResult = dateIso[2] ;
					break;
				case 'M':
					dateResult = dateIso[1] ;
					break;
				case 'A':
				case 'Y':
					dateResult = tmpYear ;
					break;
				case 'T':
				default:
					dateResult = dateIso[2] + '/' + dateIso[1] + '/' + tmpYear ;
			}
		} else {
			dateResult = dateIso[0] ;
		}
	}
	return dateResult ;
}

/*
 * infos signataire
 */

function genereInfoSignataire(flgCompleteInfos, flgMergePrenoms) {
	var contactInfos = [] ;
	var infoSignataire = '' ;
	var chgLigne = ' \n' ;
	if (typeof datasSignature !== 'undefined' && typeof datasIdentite !== 'undefined' && typeof idMandataire !== 'undefined') {
		if (flgCompleteInfos === true) {
			infoSignataire = 'Cette déclaration respecte les attendus de l\’article 1367 du code civil.' + chgLigne ;
			infoSignataire += datasSignature.cadreSignature.soussigne + ' ';
		}
		if (Value('id').of(datasSignature.cadreSignature.soussigne).eq(idMandataire)) {
			infoSignataire += datasSignature.cadreSignature.informationsMandataire.nomPrenomDenomination ;
			if (flgCompleteInfos === true) {
				infoSignataire += chgLigne ;
				infoSignataire += mergeAdresse(datasSignature.cadreSignature.informationsMandataire.cadreAdresse, 'T', true) ;
			}
		} else {
			infoSignataire += (datasIdentite.cadreIdentite.nomUsage ? datasIdentite.cadreIdentite.nomUsage + ' ' : datasIdentite.cadreIdentite.nomNaissance) + ' ' ;
			if (flgMergePrenoms === true) {
				infoSignataire += mergePrenoms(datasIdentite.cadreIdentite.prenoms) ;
			}
			if (flgCompleteInfos === true) {
				infoSignataire += chgLigne ;
				if (typeof datasIdentite.cadreContact.telephoneFixe.telephoneNum !== 'undefined' && datasIdentite.cadreContact.telephoneFixe.telephoneNum) {
					contactInfos.push(datasIdentite.cadreContact.telephoneFixe.telephoneNum) ;
				}
				if (typeof datasIdentite.cadreContact.telephoneMobile.telephoneNum !== 'undefined' && datasIdentite.cadreContact.telephoneMobile.telephoneNum) {
					contactInfos.push(datasIdentite.cadreContact.telephoneMobile.telephoneNum);
				}
				if (typeof datasIdentite.cadreContact.courriel !== 'undefined' && datasIdentite.cadreContact.courriel) {
					contactInfos.push(datasIdentite.cadreContact.courriel);
				}
				infoSignataire += contactInfos.toString() ;
			}
		}
	}
	return infoSignataire ;
}

/*
 * extraction d'informations liées à l'identité (nom, prénoms...)
 */

function extractInfoIdentite(blocIdentite, typeExtract, flgExtract) {
	var infoIdentite = '';
	if (typeof blocIdentite !== 'undefined' && typeof blocIdentite === 'object') {
		var typeExt = (typeExtract !== 'undefined' && typeof typeExtract === 'string') ? typeExtract.toUpperCase() : 'T' ;
		switch (typeExt) {
			case 'N':
			case 'L':
				infoIdentite = blocIdentite.nomUsage ? blocIdentite.nomUsage : (blocIdentite.nomNaissance ? blocIdentite.nomNaissance : '') ;
				break;
			case 'P':
			case 'F':
				infoIdentite = flgExtract ? mergePrenoms(blocIdentite.prenoms) : (blocIdentite.prenoms.length > 0 ? blocIdentite.prenoms[0] : '') ;
				break ;
			case 'C':
				infoIdentite = blocIdentite.civilite ? blocIdentite.civilite : '' ;
				break
			case 'X':
				infoIdentite += extractInfoIdentite(blocIdentite, 'N', flgExtract) ;
				infoIdentite += ' ' ;
				infoIdentite += extractInfoIdentite(blocIdentite, 'P', flgExtract) ;
				break;
			case 'T':
				infoIdentite  = extractInfoIdentite(blocIdentite, 'C', flgExtract) ;
				infoIdentite += ' ' ;
				infoIdentite += extractInfoIdentite(blocIdentite, 'N', flgExtract) ;
				infoIdentite += ' ' ;
				infoIdentite += extractInfoIdentite(blocIdentite, 'P', flgExtract) ;
				break;
			default:
				infoIdentite = '';
		}
	}
	return infoIdentite ;
}

/*
 * extraction d'informations liées aux adresses
 */
 
function extractInfoAdresse(blocAdresse, typeExtract, flgExtract) {
	var tmpDept = /(0[1-9]|[1-8][0-9]|2[A-B]|9[0-5]|9[7-8][0-9])(\d{2,3})/ ;
	var adresse = '' ;
	if (typeof blocAdresse !== 'undefined' && typeof blocAdresse === 'object') {
		var typeAct = (typeof typeExtract !== 'undefined' && typeof typeExtract === 'string') ? typeExtract.toUpperCase() : 'T' ;
		switch (typeAct) {
			case 'D':
				adresse = blocAdresse.commune ? blocAdresse.commune.id : '' ;
				adresse = adresse.replace(tmpDept, '$1') ;
				break;
			case 'P':
				if (typeof blocAdresse.ville !== 'undefined' && blocAdresse.ville) {
					adresse = blocAdresse.pays ;
				} else {
					if (flgExtract === true) {
						adresse = blocAdresse.pays ;
					}
				}
				break;
			case 'C':
				if (typeof blocAdresse.ville !== 'undefined' && blocAdresse.ville) {
					adresse = blocAdresse.ville ;
				} else {
					adresse = blocAdresse.commune ;
				}
				break;
			default:
		}
	}
	return adresse;
}

/*
 * extraction indicatif téléphone si indicatif non français (+33)
 */

function extractIndicatifEtrangerTelephone(objNumTel) {
	var tmpRegexTelIntro = /^(\+\d{1,3}[\s\.\-]?)(.*)$/ ;
	var tmpTel = '' ;
	var tmpIntro = '';
	if (typeof objNumTel !== 'undefined' && typeof objNumTel === 'object' && objNumTel) {
		if (tmpRegexTelIntro.test(objNumTel.international) === true) {
			tmpIntro = objNumTel.international.replace(tmpRegexTelIntro, '$1').trim() ;
			if (tmpIntro !== '+33') {
				tmpTel = tmpIntro ;
			}
		}
	}
	return tmpTel ;
}

/* merge de PJs
 * 
 */

function merge(pjs) {
	var mergedFile = nash.doc.create();
	pjs.forEach(function (elm) {		
		mergedFile.append(elm).save('item.pdf');
    });
	return mergedFile;
}

/* ajout de PJ
 * 
 */

function appendPj(fld, listPj) {
	fld.forEach(function (elm) {
        listPj.push(elm);
		_log.info('elm.getAbsolutePath() = >'+elm.getAbsolutePath());
		metas.push({'name':'userAttachments', 'value': '/'+elm.getAbsolutePath()});
		metas.push({'name':'document', 'value': '/'+elm.getAbsolutePath()});
    });
}

/* remplissage d'un cerfa à partir d'une table de paramétrage
 *
*/

function genereDynamicParams(tabParam, baseAddress) {
	var tmpTab = tabParam.replace(/root/gi, baseAddress);
	var tmpObj = [] ;
	try {
		tmpObj = eval(tmpTab) ;
	} catch(error) {
		tmpObj = [];
	}
  	return tmpObj;
}

function genereXDynamicParams(tabParam, baseAddress) {
	var tmpTab = '';
	var tmpVal ;
	var tmpFlg = false ;
	var tmpInd = 0;
	var tmpKey = {} ;
	var tmpObj = [] ;
	var tmpRegG = /root/gi ; // pour remplacer root. par le préfixe réel
	var tmpReg = [ /src\:[\s]*([^;\{\}]*)[;]*.*$/i, // pour isoler la valeur associée à src
				   /dst\:[\s]*([^;\{\}]*)[;]*.*$/i, // pour isoler la valeur associée à dst
				   /valeur\:[\s]*([^;\{\}]*)[;]*.*$/i, // pour isoler la valeur associée à value
				   /type\:[\s]*([^;\{\}]*)[;]*.*$/i ] ; // pour isoler la valeur associée à type
	var tmpLib = [ 'src', 'dst', 'valeur', 'type' ] ; // table des propriétés src, dst, value, type
	tabParam.forEach(function (elm) {
		try {
			tmpTab = elm.replace(tmpRegG, baseAddress);
			tmpKey = {} ;
			for (tmpInd = 0; tmpInd < tmpLib.length; tmpInd++) {
				tmpVal = tmpReg[tmpInd].exec(tmpTab) ;
				if (tmpVal !== null && tmpVal.length > 0) {
					tmpKey[tmpLib[tmpInd]] = eval(tmpVal[1].trim());
					tmpFlg = true ;
				} else {
					tmpFlg = false ;
					break;
				}
			}
			if (tmpFlg == true) {
				tmpObj.push(tmpKey) ;
			}
		} catch(error) {
			tmpFlg = false ;
		}
		return ;
    });
  	return tmpObj;
}

function completeCerfa(paramFields, valFields, indFields) {
	var tmpDst = '';
	var tmpResult = true ;
	var tmpRegexIndice = /\[i\]/gi ; // forme 'champ-cerfa[i] par exemple'
	var tmpRegexIndex = /\[x\]/gi ; // forme 'champ-cerfa[x] par exemple'
	var tmpBase = '' ;
	var tmpType = '' ;
	var tmpValue = '' ;
	var tmpFlag = false ;
	var objParam = [] ;
	
	if (typeof paramFields !== 'object' || !(Array.isArray(paramFields)) || typeof formFields !== 'object' || typeof indFields !== 'number') {
		return false;
	}
	paramFields.forEach(function (item) {
		if (typeof item.src === 'undefined') {
			return ;
		}
		tmpType = (typeof item.type !== undefined && typeof item.type === 'string') ? (item.type).toUpperCase() : '' ;
		if (tmpType === 'CARDINALITY' || tmpType === 'XCARDINALITY') {
			if (typeof item.src !== 'object' || item.src === null) {
				return ;
			}
			for (iCardinality = 0; iCardinality < (item.src).length; iCardinality++) {
				tmpBase = item.dst + '[' + iCardinality.toString() + ']' ;
				switch (tmpType) {
					case 'CARDINALITY':
						objParam = genereDynamicParams(item.valeur, tmpBase);
						break;
					case 'XCARDINALITY':
						objParam = genereXDynamicParams(item.valeur, tmpBase);
						break;
					default:
						objParam = [];
						break;
				}
				if (completeCerfa(objParam, valFields, iCardinality) == false) {
					tmpResult = false ;
				}
			}
			return ;
		}
		if (tmpType === 'COMPLEX' || tmpType === 'XCOMPLEX') {
			if (typeof item.src !== 'object') {
				return ;
			}
			(item.valeur).forEach(function (xtem) {
				switch (tmpType) {
					case 'COMPLEX':
						objParam = genereDynamicParams(xtem, item.dst);
						break;
					case 'XCOMPLEX':
						objParam = genereXDynamicParams(xtem, item.dst);
						break;
					default:
						objParam = [];
						break;
				}
				if (completeCerfa(objParam, valFields, 0) == false) {
					tmpResult = false ;
				}
				return ;
			});
			return ;
		}
		tmpDst = (item.dst).replace(tmpRegexIndice, (indFields + 1).toString()) ;
		tmpDst = tmpDst.replace(tmpRegexIndex, indFields.toString()) ;
		switch (tmpType) {
			case 'BOOL':
				var tmpFlg = typeof item.valeur === 'boolean' ? item.valeur : false ;
				if (item.src === null) {
					valFields[tmpDst] = tmpFlg ;
				} else {
					valFields[tmpDst] = item.src === tmpFlg ? true : false;
				}
				break;
			case 'XBOOL':
				if ((typeof item.src === 'boolean') && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					if (item.valeur[0] === item.valeur[1]) {
						if (item.src === false) {
							valFields[tmpDst] = null;
						}
					} else {
						valFields[tmpDst] = item.src;
					}
				}
				break;
			case 'RADIO':
				if (item.src) {
					valFields[tmpDst] = Value('id').of(item.src).eq(item.valeur) ? true : false;
				}
				break;
			case 'BRADIO':
				if ((typeof item.src === 'boolean' && item.src) && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					valFields[tmpDst] = Value('id').of(item.valeur[0]).eq(item.valeur[1]) ? true : false;
				}
				break;
			case 'XRADIO':
				if ((typeof item.src === 'boolean') && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					if (Value('id').of(item.valeur[0]).eq(item.valeur[1])) {
						if (item.src === false) {
							valFields[tmpDst] = null;
						}
					} else {
						valFields[tmpDst] = item.src;
					}
				}
				break;
			case 'CHECK':
				if (item.src) {
					valFields[tmpDst] = Value('id').of(item.src).contains(item.valeur) ? true : false;
				}
				break;
			case 'BCHECK':
				if ((typeof item.src === 'boolean' && item.src) && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					valFields[tmpDst] = Value('id').of(item.valeur[0]).contains(item.valeur[1]) ? true : false;
				}
				break;
			case 'XCHECK':
				if ((typeof item.src === 'boolean') && (typeof item.valeur !== 'undefined' && Array.isArray(item.valeur) && item.valeur.length > 1)) {
					if (Value('id').of(item.valeur[0]).contains(item.valeur[1])) {
						if (item.src === false) {
							valFields[tmpDst] = null;
						}
					} else {
						valFields[tmpDst] = item.src;
					}
				}
				break;
			case 'STRING':
				if (item.valeur) {
					tmpValue = item.valeur + ' ' ;
					tmpFlag = true ;
				} else {
					tmpValue = '' ;
					tmpFlag = false ;
				}
				if (item.src) {
					tmpValue += item.src ;
					tmpFlag = true ;
				}
				if (tmpFlag) {
					valFields[tmpDst] = tmpValue ;
				}
				break;
			case 'BSTRING':
				if ((typeof item.src === 'boolean' && item.src) && (typeof item.valeur !== 'undefined' && item.valeur)) {
					valFields[tmpDst] = item.valeur ;
				}
				break;
			case 'XSTRING':
				if (((typeof item.src === 'string' or typeof item.src === 'object') && item.src) && (typeof item.valeur !== 'undefined' && item.valeur)) {
					valFields[tmpDst] = item.valeur ;
				}
				break;
			default:
				break;
		}
		return ;
	});
	return tmpResult ;
}

//--------------- FONCTIONS -------------------//

//--------------- MAIN GENERATION CERFA -------//

/* génération du Cerfa à partir de la table d'équivalences
 * (génération optionnelle)
 */

if (typeof formFields !== 'undefined' && typeof nameCerfa !== 'undefined') {
	var flgCerfa = true ;
	if (typeof equivCerfa !== 'undefined') { 
		flgCerfa = completeCerfa(equivCerfa, formFields, 0);
	}
	if (flgCerfa === true) {
		var cerfaDoc = nash.doc
			.load(repCerfa + nameCerfa)
			.apply(formFields);
		var pjCerfa = cerfaDoc.save(nameCerfa);
	}
}

/* ajout des PJ utilisateurs le cas échéant
 *
 */
 
if (typeof pjSignataire !== 'undefined') {
	if(Value('id').of(pjSignataire).contains(idDeclarant)) {
		appendPj($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantSignataire, pjUser);
	}
	if(Value('id').of(pjSignataire).contains(idMandataire)) {
		appendPj($attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire, pjUser);
	}
}

/* ajout des autres PJ le cas échéant
 *
 */

if($attachmentPreprocess.attachmentPreprocess.pjAutres !== 'undefined') {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjAutres, pjUser);
}

/* ajout du Cerfa en tant que PJ (s'il a été généré)
 *
 */
 
if (typeof pjCerfa !== 'undefined') {
	data.push(spec.createData({
			id: 'record',
			label: refFormTitle,
			description: 'Voici le formulaire obtenu à partir des données saisies.',
			type: 'FileReadOnly',
			value: [pjCerfa] 
	})) ;
}

/*
 * Enregistrement du fichier (en mémoire)
 */ 

var metasToDelete = [];
var recordMetas = nash.record.meta() != null ? nash.record.meta().metas : null;
var recordMetasSize = recordMetas != null ? recordMetas.size() : 0;

for (var i = 0; i < recordMetasSize; i++) {
    if (recordMetas.get(i).name == 'document') {
        metasToDelete.push({'name':'document', 'value': recordMetas.get(i).value});
    }
}
nash.record.removeMeta(metasToDelete);
nash.record.meta(metas);

data.push(spec.createData({
    id : 'uploaded',
    label : 'Pièces jointes',
    description : 'Pièces jointes ajoutées au formulaire.',
    type : 'FileReadOnly',
    value : [ merge(pjUser).save('saisine.pdf') ]
	})) ;

var groups = [ spec.createGroup({
    id : 'attachments',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Saisine du Ministère de l\'intérieur',
    groups : groups
});

//--------------- MAIN GENERATION CERFA -------//
