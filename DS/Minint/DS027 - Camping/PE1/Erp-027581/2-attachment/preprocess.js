/* init datas pour pj (variables spécifiques)
 * contient a minnima les références des groupes 
 * génériques de data.xml
 */

/*
 * init pointeurs saisies, Pj...
 */
 
var datasPtr = $ds027cerfa13824 ;
var datasSignature = datasPtr.cadreSignatureGroupe ;
var datasIdentite = datasPtr.cadreIdentiteGroupe ;
var refFormTitle = 'Camping-caravaning : Erp' ;

if (typeof datasPtr.cadreDemandeGroupe !== 'undefined') {
    var datasDemande = datasPtr.cadreDemandeGroupe ;
} else {
	var datasDemande = null ;
}

/*
 * init pointeurs PJ déclarants ou mandataires
 */
 
var pjSignataire = datasSignature.cadreSignature.soussigne ;
var idDeclarant = 'signataireQualiteDeclarant' ;
var idMandataire = 'signataireQualiteMandataire' ;

/* fonctions génériques de formatage des données
 *
 */

/*
 * calculs mathématiques : calcul de pourcentage
 */
 
function calculPercent(valNumerateur, valDenominateur, nbDecimales) {
	var valResult = '' ;
	var tmpValeur = 0 ;
	while (true) {
		if (typeof valNumerateur === 'undefined' || typeof valNumerateur !== 'number') {
			break ;
		}
		if (typeof valDenominateur === 'undefined' || typeof valDenominateur !== 'number') {
			break;
		}
		if (typeof nbDecimales === 'undefined' || typeof nbDecimales !== 'number') {
			tmpValeur = 0 ;
		} else {
			tmpValeur = nbDecimales >= 0 ? nbDecimales : 0 ; 
		}	
		tmpValeur = Math.pow(10, tmpValeur);
		if (valDenominateur !== 0) {
			tmpValeur = Math.round(((valNumerateur / valDenominateur) * 100) / tmpValeur) ;
			valResult = tmpValeur.toString();
			
		}
		break ;
	}
	return valResult ;
 }

/*
 * calculs mathématiques : calcul de soustraction
 */
 
function calculSubstraction(valTotal, tabValeurs, cumulValeur, keyCumul) {
	var valResult = '' ;
	var tmpValeur = 0 ;
	while (true) {
		if (typeof valTotal === 'undefined' || typeof valTotal !== 'number') {
			break ;
		}
		tmpValeur = valTotal ;
		if (typeof tabValeurs === 'object' && Array.isArray(tabValeurs)) {
			for (tmpIndex = 0; tmpIndex < tabValeurs.length; tmpIndex++) {
				if (typeof tabValeurs[tmpIndex] === 'number') {
					tmpValeur -= tabValeurs[tmpIndex];
					tmpFlag = true ;
				}
			}	
		}
		if (tmpFlag === true) {
			valResult = tmpValeur.toString();
			if (typeof cumulValeur === 'object' && cumulValeur !== null && typeof keyCumul === 'string') {
				if (typeof cumulValeur[keyCumul] !== 'undefined' && isNaN(cumulValeur[keyCumul]) === false) {
					cumulValeur[keyCumul] = (Number(cumulValeur[keyCumul]) - tmpValeur).toString() ;
				} else {
					cumulValeur[keyCumul] = valResult ;
				}
			}
		}
		break;
	}
	return valResult ;
 }

function calculAddition(tabValeurs, cumulValeur, keyCumul) {
	var valResult = '' ;
	var tmpFlag = false ;
	var tmpValeur = 0 ;
	var tmpIndex = 0 ;
	while (true) {
		if (typeof tabValeurs === 'object' && Array.isArray(tabValeurs)) {
			for (tmpIndex = 0; tmpIndex < tabValeurs.length; tmpIndex++) {
				if (typeof tabValeurs[tmpIndex] === 'number') {
					tmpValeur += tabValeurs[tmpIndex];
					tmpFlag = true ;
				}
			}	
		}
		if (tmpFlag === true) {
			valResult = tmpValeur.toString();
			if (typeof cumulValeur === 'object' && cumulValeur !== null && typeof keyCumul === 'string') {
				if (typeof cumulValeur[keyCumul] !== 'undefined' && isNaN(cumulValeur[keyCumul]) === false) {
					cumulValeur[keyCumul] = (tmpValeur + Number(cumulValeur[keyCumul])).toString() ;
				} else {
					cumulValeur[keyCumul] = valResult ;
				}
			}
		}
		break ;
	}
	return valResult ;
 }

/* suppression des espaces dans une chaine
 *
 */

function supprimeEspaces(szLibel) {
	var tmpRegex = /\s/g ;
	var tmpLibel = '';
	if (typeof szLibel !== 'undefined' && typeof szLibel === 'string') {
		tmpLibel = szLibel !== null ? szLibel.replace(tmpRegex, '') : '' ;	
	}
	return tmpLibel ;
}
 
/* merge des prénoms (séparateur ',')
 *
 */
	
function mergePrenoms(listPrenoms) {
	var prenoms = [];
	if (typeof listPrenoms !== 'undefined' && typeof listPrenoms === 'object') {
		for (i = 0; i < listPrenoms.length; i++) {
			prenoms.push(listPrenoms[i]);
    	}
	} else {
		prenoms.push('');
	}
	return prenoms.toString();
}

/* merge du nom et des prénoms (séparateur ',')
 *
 */

function mergeNomPrenoms(szNom, listPrenoms) {
	var result = '';
	if (typeof szNom === 'string') {
		result = szNom + ' ';
		result += mergePrenoms(listPrenoms);
	} 
	return result;
}

/* construction de la date et du lieu de naissance
 *
 */

function mergeNaissance(blocNaissance) {
	var naissance = '' ;
	if (typeof blocNaissance !== 'undefined' && typeof blocNaissance === 'object') {
		naissance = blocNaissance.lieuNaissance ? blocNaissance.lieuNaissance + ' / ' : '' ;
		naissance += blocNaissance.paysNaissance ? blocNaissance.paysNaissance : '' ;
	}
	return naissance ;
}
	
/* construction de l'adresse en tenant compte du pays ou non
 *
 */

function mergeAdresse(blocAdresse, typeMerge, multiLine) {
	var tmpVar = '' ;
	var adresse = '' ;
	var chgLigne = ' \n' ;
	if (typeof blocAdresse !== 'undefined' && typeof blocAdresse === 'object') {
		var fmtLigne = (multiLine === true ? chgLigne : ' ');
		var typeAct = (typeof typeMerge !== 'undefined' && typeof typeMerge === 'string') ? typeMerge.toUpperCase() : 'T' ;
		switch (typeAct) {
			case 'N':
				adresse += blocAdresse.numeroVoie ? blocAdresse.numeroVoie + ' ' : '' ;
				adresse += blocAdresse.indiceVoie ? blocAdresse.indiceVoie : '' ;
				break;
			case 'V':	
				tmpVar = mergeAdresse(blocAdresse, 'N', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + ' ' : '' ;
				adresse += mergeAdresse(blocAdresse, 'W', multiLine) ;
				break;
			case 'W':
				adresse += blocAdresse.typeVoie ? blocAdresse.typeVoie + ' ' : '' ;
				adresse += blocAdresse.nomVoie ?  blocAdresse.nomVoie : '' ;
				break;
			case 'S':
				adresse += blocAdresse.complementAdresse ? blocAdresse.complementAdresse + fmtLigne : '' ;
				adresse += blocAdresse.distributionSpeciale ? blocAdresse.distributionSpeciale : '' ;
				break;
			case 'C':
				if (typeof blocAdresse.pays !== 'undefined') {
					if (typeof blocAdresse.codePostal !== 'undefined' && blocAdresse.codePostal) {
						adresse += blocAdresse.codePostal ? blocAdresse.codePostal + ' ' : '' ;
						adresse += blocAdresse.commune ? blocAdresse.commune : '' ;
					} else {
						adresse += blocAdresse.ville ? blocAdresse.ville + fmtLigne : '' ;
						adresse += blocAdresse.pays ? blocAdresse.pays : '' ;
					}
				} else {
					adresse += blocAdresse.codePostal ? blocAdresse.codePostal + ' ' : '' ;
					adresse += blocAdresse.commune ? blocAdresse.commune : '' ;
				}
				break;
			case 'X':
				tmpVar = mergeAdresse(blocAdresse, 'S', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + fmtLigne : '' ;
				adresse += mergeAdresse(blocAdresse, 'C', multiLine) ;
				break;
			case 'Z':
				tmpVar = mergeAdresse(blocAdresse, 'V', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + fmtLigne : '' ;
				adresse += mergeAdresse(blocAdresse, 'S', multiLine) ;
				break;
			case 'T':
			default:
				tmpVar = mergeAdresse(blocAdresse, 'V', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + fmtLigne : '' ;
				tmpVar = mergeAdresse(blocAdresse, 'S', multiLine) ;
				adresse += tmpVar !== '' ? tmpVar + fmtLigne : '' ;
				adresse += mergeAdresse(blocAdresse, 'C', multiLine) ;
		}
	}
	return adresse;
}

/*
 * mise en forme numéro de téléphone au format 10 chiffres
 */

function formatNumTelephone(objNumTel) {
	var tmpRegexTelIntro = /^(\+\d{1,3}[\s\.\-]?)(.*)$/ ;
	var tmpRegexTel = /(\d{1,})[\s\.\-]?/g ;
	var tmpTel = '' ;
	var tmpIntro = '';
	var tmpTelFmt = '';
	if (typeof objNumTel !== 'undefined' && typeof objNumTel === 'object' && objNumTel) {
		tmpTel = objNumTel.international ;
		if (tmpRegexTelIntro.test(tmpTel) === true) {
			tmpIntro = tmpTel.replace(tmpRegexTelIntro, '$1') ;
			tmpTelFmt = '$2' ;
			if (tmpIntro.substring(0,3) == '+33') {
				tmpTelFmt = '0' + tmpTelFmt ;
			}	
			tmpTel = tmpTel.replace(tmpRegexTelIntro, tmpTelFmt) ;
			tmpTel = tmpTel.replace(tmpRegexTel, '$1') ;
		}
	}
	return tmpTel ;
}

/*
 * infos contacts téléphoniques
 */
 
function mergeNumTelephone(blocNumTel, flgMerge, flgInternational) {
	var numTel = '';
	var tmpTel = '';
	if (typeof blocNumTel !== 'undefined' && typeof blocNumTel === 'object') {
		if (typeof blocNumTel.telephoneFixe.telephoneNum !== 'undefined' && blocNumTel.telephoneFixe.telephoneNum ) {
			if (flgInternational === false) {
				tmpTel = formatNumTelephone(blocNumTel.telephoneFixe.telephoneNum) ;
			} else {
				tmpTel = blocNumTel.telephoneFixe.telephoneNum.international ;
			}
			numTel += tmpTel ;
		}
		if (typeof blocNumTel.telephoneMobile.telephoneNum !== 'undefined' && blocNumTel.telephoneMobile.telephoneNum) {
			if (numTel.length > 0 && flgMerge === true) {
				numTel += ' / ';
			}
			if (flgMerge === true || numTel.length === 0) {
				if (flgInternational === false) {
					tmpTel = formatNumTelephone(blocNumTel.telephoneMobile.telephoneNum) ;
				} else {
					tmpTel = blocNumTel.telephoneMobile.telephoneNum.international;
				}
				numTel += tmpTel ;
			}
		}
	}
	return numTel;
}

/* formatage d'une date avec extaction ou non d'une partie (J, M, A)
 *
 */

function formatDate(valDate, typeExtract, fullYear) {
	var dateResult = '';
	var tmpYear = '' ;
	if (typeof valDate === 'object' && valDate) {
    	var dateTmp = new Date(parseInt(valDate.getTimeInMillis())) ; ;
		var dateIso = ((dateTmp.toLocaleDateString()).substr(0, 10)).split('-') ;
		var typeAct = (typeof typeExtract !== 'undefined' && typeof typeExtract === 'string') ? typeExtract.toUpperCase() : 'T' ;
		if (dateIso.length > 1) {
			tmpYear = (fullYear === true ? dateIso[0] : dateIso[0].substr(2)) ;
			switch (typeAct) {
				case 'D':
				case 'J':
					dateResult = dateIso[2] ;
					break;
				case 'M':
					dateResult = dateIso[1] ;
					break;
				case 'A':
				case 'Y':
					dateResult = tmpYear ;
					break;
				case 'T':
				default:
					dateResult = dateIso[2] + '/' + dateIso[1] + '/' + tmpYear ;
			}
		} else {
			dateResult = dateIso[0] ;
		}
	}
	return dateResult ;
}

/*
 * infos signataire
 */

function genereInfoSignataire(flgCompleteInfos, flgMergePrenoms) {
	var contactInfos = [] ;
	var infoSignataire = '' ;
	var chgLigne = ' \n' ;
	if (typeof datasSignature !== 'undefined' && typeof datasIdentite !== 'undefined' && typeof idMandataire !== 'undefined') {
		if (flgCompleteInfos === true) {
			infoSignataire = 'Cette déclaration respecte les attendus de l\’article 1367 du code civil.' + chgLigne ;
			infoSignataire += datasSignature.cadreSignature.soussigne + ' ';
		}
		if (Value('id').of(datasSignature.cadreSignature.soussigne).eq(idMandataire)) {
			infoSignataire += datasSignature.cadreSignature.informationsMandataire.nomPrenomDenomination ;
			if (flgCompleteInfos === true) {
				infoSignataire += chgLigne ;
				infoSignataire += mergeAdresse(datasSignature.cadreSignature.informationsMandataire.cadreAdresse, 'T', true) ;
			}
		} else {
			infoSignataire += (datasIdentite.cadreIdentite.nomUsage ? datasIdentite.cadreIdentite.nomUsage + ' ' : datasIdentite.cadreIdentite.nomNaissance) + ' ' ;
			if (flgMergePrenoms === true) {
				infoSignataire += mergePrenoms(datasIdentite.cadreIdentite.prenoms) ;
			}
			if (flgCompleteInfos === true) {
				infoSignataire += chgLigne ;
				if (typeof datasIdentite.cadreContact.telephoneFixe.telephoneNum !== 'undefined' && datasIdentite.cadreContact.telephoneFixe.telephoneNum) {
					contactInfos.push(datasIdentite.cadreContact.telephoneFixe.telephoneNum) ;
				}
				if (typeof datasIdentite.cadreContact.telephoneMobile.telephoneNum !== 'undefined' && datasIdentite.cadreContact.telephoneMobile.telephoneNum) {
					contactInfos.push(datasIdentite.cadreContact.telephoneMobile.telephoneNum);
				}
				if (typeof datasIdentite.cadreContact.courriel !== 'undefined' && datasIdentite.cadreContact.courriel) {
					contactInfos.push(datasIdentite.cadreContact.courriel);
				}
				infoSignataire += contactInfos.toString() ;
			}
		}
	}
	return infoSignataire ;
}

/*
 * extraction d'informations liées à l'identité (nom, prénoms...)
 */

function extractInfoIdentite(blocIdentite, typeExtract, flgExtract) {
	var infoIdentite = '';
	if (typeof blocIdentite !== 'undefined' && typeof blocIdentite === 'object') {
		var typeExt = (typeExtract !== 'undefined' && typeof typeExtract === 'string') ? typeExtract.toUpperCase() : 'T' ;
		switch (typeExt) {
			case 'N':
			case 'L':
				infoIdentite = blocIdentite.nomUsage ? blocIdentite.nomUsage : (blocIdentite.nomNaissance ? blocIdentite.nomNaissance : '') ;
				break;
			case 'P':
			case 'F':
				infoIdentite = flgExtract ? mergePrenoms(blocIdentite.prenoms) : (blocIdentite.prenoms.length > 0 ? blocIdentite.prenoms[0] : '') ;
				break ;
			case 'C':
				infoIdentite = blocIdentite.civilite ? blocIdentite.civilite : '' ;
				break
			case 'X':
				infoIdentite += extractInfoIdentite(blocIdentite, 'N', flgExtract) ;
				infoIdentite += ' ' ;
				infoIdentite += extractInfoIdentite(blocIdentite, 'P', flgExtract) ;
				break;
			case 'T':
				infoIdentite  = extractInfoIdentite(blocIdentite, 'C', flgExtract) ;
				infoIdentite += ' ' ;
				infoIdentite += extractInfoIdentite(blocIdentite, 'N', flgExtract) ;
				infoIdentite += ' ' ;
				infoIdentite += extractInfoIdentite(blocIdentite, 'P', flgExtract) ;
				break;
			default:
				infoIdentite = '';
		}
	}
	return infoIdentite ;
}

/*
 * extraction d'informations liées aux adresses
 */
 
function extractInfoAdresse(blocAdresse, typeExtract, flgExtract) {
	var tmpDept = /(0[1-9]|[1-8][0-9]|2[A-B]|9[0-5]|9[7-8][0-9])(\d{2,3})/ ;
	var adresse = '' ;
	if (typeof blocAdresse !== 'undefined' && typeof blocAdresse === 'object') {
		var typeAct = (typeof typeExtract !== 'undefined' && typeof typeExtract === 'string') ? typeExtract.toUpperCase() : 'T' ;
		switch (typeAct) {
			case 'D':
				adresse = blocAdresse.commune ? blocAdresse.commune.id : '' ;
				adresse = adresse.replace(tmpDept, '$1') ;
				break;
			case 'P':
				if (typeof blocAdresse.ville !== 'undefined' && blocAdresse.ville) {
					adresse = blocAdresse.pays ;
				} else {
					if (flgExtract === true) {
						adresse = blocAdresse.pays ;
					}
				}
				break;
			case 'C':
				if (typeof blocAdresse.ville !== 'undefined' && blocAdresse.ville) {
					adresse = blocAdresse.ville ;
				} else {
					adresse = blocAdresse.commune ;
				}
				break;
			default:
		}
	}
	return adresse;
}

/*
 * extraction indicatif téléphone si indicatif non français (+33)
 */

function extractIndicatifEtrangerTelephone(objNumTel) {
	var tmpRegexTelIntro = /^(\+\d{1,3}[\s\.\-]?)(.*)$/ ;
	var tmpTel = '' ;
	var tmpIntro = '';
	if (typeof objNumTel !== 'undefined' && typeof objNumTel === 'object' && objNumTel) {
		if (tmpRegexTelIntro.test(objNumTel.international) === true) {
			tmpIntro = objNumTel.international.replace(tmpRegexTelIntro, '$1').trim() ;
			if (tmpIntro !== '+33') {
				tmpTel = tmpIntro ;
			}
		}
	}
	return tmpTel ;
}

// PJ Déclarant
if (datasIdentite.cadreIdentite.nomUsage != null) {
    var userDeclarant = datasIdentite.cadreIdentite.nomUsage + '  ' + mergePrenoms(datasIdentite.cadreIdentite.prenoms) ;
} else {
    var userDeclarant = datasIdentite.cadreIdentite.nomNaissance + '  '+ mergePrenoms(datasIdentite.cadreIdentite.prenoms) ;
}
if(Value('id').of(pjSignataire).contains(idDeclarant)) {
    attachment('pjIDDeclarantSignataire', 'pjIDDeclarantSignataire', { label: userDeclarant, mandatory:"true"});
}

// // PJ Mandataire
if(Value('id').of(pjSignataire).contains(idMandataire)) {
    attachment('pjPouvoir', 'pjPouvoir', { mandatory:"true"});
}
if(Value('id').of(pjSignataire).contains(idMandataire)) {
    var userMandataire=datasSignature.cadreSignature.informationsMandataire
    attachment('pjIDMandataireSignataire', 'pjIDMandataireSignataire', {label: userMandataire.nomPrenomDenomination, mandatory:"true"});
}

// PJ Autres
attachment('pjAutres', 'pjAutres', { mandatory:"false"});




