function pad(s) { return (s < 10) ? '0' + s : s; }

var formFields = {};

var civNomPrenom = $ds083PE5.cadre1.cadre1.civilite + ' ' + $ds083PE5.cadre1.cadre1.nomNaissance + ' ' + $ds083PE5.cadre1.cadre1.prenomDeclarant;


/// Dossier de déclaration

formFields['declarationInitiale']                     = false
formFields['renouvellement']                          = true
formFields['departementExercice']                     = $ds083PE5.declaration.declaration0.departementexercice;


//Etat Civil
formFields['civiliteMadame']                           = ($ds083PE5.cadre1.cadre1.civilite=='Madame');
formFields['civiliteMonsieur']                         = ($ds083PE5.cadre1.cadre1.civilite=='Monsieur');
formFields['nomNaissance']                             = $ds083PE5.cadre1.cadre1.nomNaissance;
formFields['nomUsage']                                 = $ds083PE5.cadre1.cadre1.nomUsage;
formFields['prenom']                                   = $ds083PE5.cadre1.cadre1.prenomDeclarant;
formFields['nomPere']                                  = $ds083PE5.cadre1.cadre1.nomPereDeclarant;
formFields['nomMere']                                  = $ds083PE5.cadre1.cadre1.nomMereDeclarant;
formFields['prenomPere']                               = $ds083PE5.cadre1.cadre1.prenomPereDeclarant;
formFields['prenomMere']                               = $ds083PE5.cadre1.cadre1.prenomMereDeclarant;


if($ds083PE5.cadre1.cadre1.dateNaissanceDeclarant != null) {
var dateTemp = new Date(parseInt ($ds083PE5.cadre1.cadre1.dateNaissanceDeclarant.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['dateNaissanceJour'] = day;
	formFields['dateNaissanceMois'] = month;
	formFields['dateNaissanceAnnee'] = year;
}


formFields['villeNaissance']                           = $ds083PE5.cadre1.cadre1.lieuNaissanceDeclarant;
formFields['paysNaissance']                            = $ds083PE5.cadre1.cadre1.paysNaissanceDeclarant;
formFields['arrondissementNaissance']                  = $ds083PE5.cadre1.cadre1.arrondissement;
formFields['departementNaissance']                     = $ds083PE5.cadre1.cadre1.departementNaissance;
formFields['nationalite']                              = $ds083PE5.cadre1.cadre1.nationaliteDeclarant;
formFields['nationaliteAutre']                         = $ds083PE5.cadre1.cadre1.autrenationaliteDeclarant;



//Coordonnées

formFields['adresseDeclarantPays']                     = $ds083PE5.adresse.adressePersonnelle.adresseDeclarantPays;
formFields['adresseDeclarantVille']                    = $ds083PE5.adresse.adressePersonnelle.adresseDeclarantVille;
formFields['adresseDeclarantComplement2']              = $ds083PE5.adresse.adressePersonnelle.adressecomplement2;
formFields['adresseDeclarantComplement1']              = $ds083PE5.adresse.adressePersonnelle.adressecomplement1;
formFields['telephoneFixeDeclarant']                   = $ds083PE5.adresse.adressePersonnelle.telephoneFixeDeclarant;
formFields['telephoneMobileDeclarant']                 = $ds083PE5.adresse.adressePersonnelle.telephoneMobileDeclarant;
formFields['adresseDeclarantNumeroNomRue']             = $ds083PE5.adresse.adressePersonnelle.adresseDeclarantNumeroNomRue;
formFields['courrielDeclarant']                        = $ds083PE5.adresse.adressePersonnelle.courrielDeclarant;
formFields['adresseDeclarantCodePostal']               = $ds083PE5.adresse.adressePersonnelle.adresseDeclarantCodePostal;

//Equivalences

///qualification1

formFields['equivalenceType1']                       = $ds083PE5.declaration.declaration0.equivalence.typequalification1;
formFields['equivalenceActivite1']                   = $ds083PE5.declaration.declaration0.equivalence.activitequalification1;
formFields['equivalenceNumero1']                     = $ds083PE5.declaration.declaration0.equivalence.numeroequivalence1;

	formFields['equivalenceJourDelivrance1'] = '';
	formFields['equivalenceMoisDelivrance1'] = '';
	formFields['equivalenceAnneeDelivrance1'] = '';

if($ds083PE5.declaration.declaration0.equivalence.datedelivrance1 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.equivalence.datedelivrance1.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['equivalenceJourDelivrance1'] = day;
	formFields['equivalenceMoisDelivrance1'] = month;
	formFields['equivalenceAnneeDelivrance1'] = year;
}


formFields['equivalenceType2']                       = $ds083PE5.declaration.declaration0.equivalence.typequalification2;
formFields['equivalenceActivite2']                   = $ds083PE5.declaration.declaration0.equivalence.activitequalification2;
formFields['equivalenceNumero2']                     = $ds083PE5.declaration.declaration0.equivalence.numeroequivalence2;

	formFields['equivalenceJourDelivrance2'] = '';
	formFields['equivalenceMoisDelivrance2'] = '';
	formFields['equivalenceAnneeDelivrance2'] = '';

if($ds083PE5.declaration.declaration0.equivalence.datedelivrance2 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.equivalence.datedelivrance2.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['equivalenceJourDelivrance2'] = day;
	formFields['equivalenceMoisDelivrance2'] = month;
	formFields['equivalenceAnneeDelivrance2'] = year;
}


formFields['equivalenceType3']                       = $ds083PE5.declaration.declaration0.equivalence.typequalification3;
formFields['equivalenceActivite3']                   = $ds083PE5.declaration.declaration0.equivalence.activitequalification3;
formFields['equivalenceNumero3']                     = $ds083PE5.declaration.declaration0.equivalence.numeroequivalence3;

	formFields['equivalenceJourDelivrance3'] = '';
	formFields['equivalenceMoisDelivrance3'] = '';
	formFields['equivalenceAnneeDelivrance3'] = '';

if($ds083PE5.declaration.declaration0.equivalence.datedelivrance3 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.equivalence.datedelivrance3.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['equivalenceJourDelivrance3'] = day;
	formFields['equivalenceMoisDelivrance3'] = month;
	formFields['equivalenceAnneeDelivrance3'] = year;
}


formFields['equivalenceType4']                       = $ds083PE5.declaration.declaration0.equivalence.typequalification4;
formFields['equivalenceActivite4']                   = $ds083PE5.declaration.declaration0.equivalence.activitequalification4;
formFields['equivalenceNumero4']                     = $ds083PE5.declaration.declaration0.equivalence.numeroequivalence4;

	formFields['equivalenceJourDelivrance4'] = '';
	formFields['equivalenceMoisDelivrance4'] = '';
	formFields['equivalenceAnneeDelivrance4'] = '';

if($ds083PE5.declaration.declaration0.equivalence.datedelivrance4 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.equivalence.datedelivrance4.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['equivalenceJourDelivrance4'] = day;
	formFields['equivalenceMoisDelivrance4'] = month;
	formFields['equivalenceAnneeDelivrance4'] = year;
}

formFields['equivalenceType5']                       = $ds083PE5.declaration.declaration0.equivalence.typequalification5;
formFields['equivalenceActivite5']                   = $ds083PE5.declaration.declaration0.equivalence.activitequalification5;
formFields['equivalenceNumero5']                     = $ds083PE5.declaration.declaration0.equivalence.numeroequivalence5;

	formFields['equivalenceJourDelivrance5'] = '';
	formFields['equivalenceMoisDelivrance5'] = '';
	formFields['equivalenceAnneeDelivrance5'] = '';

if($ds083PE5.declaration.declaration0.equivalence.datedelivrance5 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.equivalence.datedelivrance5.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['equivalenceJourDelivrance5'] = day;
	formFields['equivalenceMoisDelivrance5'] = month;
	formFields['equivalenceAnneeDelivrance5'] = year;
}

formFields['equivalenceType6']                       = $ds083PE5.declaration.declaration0.equivalence.typequalification6;
formFields['equivalenceActivite6']                   = $ds083PE5.declaration.declaration0.equivalence.activitequalification6;
formFields['equivalenceNumero6']                     = $ds083PE5.declaration.declaration0.equivalence.numeroequivalence6;

	formFields['equivalenceJourDelivrance6'] = '';
	formFields['equivalenceMoisDelivrance6'] = '';
	formFields['equivalenceAnneeDelivrance6'] = '';

if($ds083PE5.declaration.declaration0.equivalence.datedelivrance6 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.equivalence.datedelivrance6.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['equivalenceJourDelivrance6'] = day;
	formFields['equivalenceMoisDelivrance6'] = month;
	formFields['equivalenceAnneeDelivrance6'] = year;
}

formFields['equivalenceType7']                       = $ds083PE5.declaration.declaration0.equivalence.typequalification7;
formFields['equivalenceActivite7']                   = $ds083PE5.declaration.declaration0.equivalence.activitequalification7;
formFields['equivalenceNumero7']                     = $ds083PE5.declaration.declaration0.equivalence.numeroequivalence7;

	formFields['equivalenceJourDelivrance7'] = '';
	formFields['equivalenceMoisDelivrance7'] = '';
	formFields['equivalenceAnneeDelivrance7'] = '';

if($ds083PE5.declaration.declaration0.equivalence.datedelivrance7 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.equivalence.datedelivrance7.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['equivalenceJourDelivrance7'] = day;
	formFields['equivalenceMoisDelivrance7'] = month;
	formFields['equivalenceAnneeDelivrance7'] = year;
}

formFields['equivalenceType8']                       = ($ds083PE5.declaration.declaration0.equivalence.typequalification8 !=null ? $ds083PE5.declaration.declaration0.equivalence.typequalification8 :'') ;
formFields['equivalenceActivite8']                   = ($ds083PE5.declaration.declaration0.equivalence.activitequalification8 !=null ? $ds083PE5.declaration.declaration0.equivalence.activitequalification8:'');
formFields['equivalenceNumero8']                     = ($ds083PE5.declaration.declaration0.equivalence.numeroequivalence8 !=null ? $ds083PE5.declaration.declaration0.equivalence.numeroequivalence8:'');

	formFields['equivalenceJourDelivrance8'] = '';
	formFields['equivalenceMoisDelivrance8'] = '';
	formFields['equivalenceAnneeDelivrance8'] = '';

if($ds083PE5.declaration.declaration0.equivalence.datedelivrance8 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.equivalence.datedelivrance7.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['equivalenceJourDelivrance8'] = day;
	formFields['equivalenceMoisDelivrance8'] = month;
	formFields['equivalenceAnneeDelivrance8'] = year;
}

formFields['equivalenceType9']                       = $ds083PE5.declaration.declaration0.equivalence.typequalification8;
formFields['equivalenceActivite9']                   = $ds083PE5.declaration.declaration0.equivalence.activitequalification8;
formFields['equivalenceNumero9']                     = $ds083PE5.declaration.declaration0.equivalence.numeroequivalence8;

	formFields['equivalenceJourDelivrance9'] = '';
	formFields['equivalenceMoisDelivrance9'] = '';
	formFields['equivalenceAnneeDelivrance9'] = '';

if($ds083PE5.declaration.declaration0.equivalence.datedelivrance8 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.equivalence.datedelivrance7.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['equivalenceJourDelivrance9'] = day;
	formFields['equivalenceMoisDelivrance9'] = month;
	formFields['equivalenceAnneeDelivrance9'] = year;
}

//qualification

formFields['qualificationType1']                       = $ds083PE5.declaration.declaration0.qualification.typequalification1;
formFields['qualificationActivite1']                   = $ds083PE5.declaration.declaration0.qualification.activitequalification1;
formFields['qualificationNumeroDiplome1']              = $ds083PE5.declaration.declaration0.qualification.numerodiplome1;

	formFields['qualificationJourObtention1'] = '';
	formFields['qualificationMoisObtention1'] = '';
	formFields['qualificationAnneeObtention1'] = '';
	
if($ds083PE5.declaration.declaration0.qualification.dateobtentiondiplome1 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.qualification.dateobtentiondiplome1.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourObtention1'] = day;
	formFields['qualificationMoisObtention1'] = month;
	formFields['qualificationAnneeObtention1'] = year;
}

formFields['qualificationLieuObtention1']              = $ds083PE5.declaration.declaration0.qualification.lieuobtention1;


	formFields['qualificationJourRecyclage1'] = '';
	formFields['qualificationMoisRecyclage1'] = '';
	formFields['qualificationAnneeRecyclage1'] = '';
if($ds083PE5.declaration.declaration0.qualification.daterecyclage1 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.qualification.daterecyclage1.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourRecyclage1'] = day;
	formFields['qualificationMoisRecyclage1'] = month;
	formFields['qualificationAnneeRecyclage1'] = year;
	}
	
///qualification2

formFields['qualificationType2']                       = $ds083PE5.declaration.declaration0.qualification.typequalification2;
formFields['qualificationActivite2']                   = $ds083PE5.declaration.declaration0.qualification.activitequalification2;
formFields['qualificationNumeroDiplome2']              = $ds083PE5.declaration.declaration0.qualification.numerodiplome2;

	formFields['qualificationJourObtention2'] = '';
	formFields['qualificationMoisObtention2'] = '';
	formFields['qualificationAnneeObtention2'] = '';
	
if($ds083PE5.declaration.declaration0.qualification.dateobtentiondiplome2 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.qualification.dateobtentiondiplome2.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourObtention2'] = day;
	formFields['qualificationMoisObtention2'] = month;
	formFields['qualificationAnneeObtention2'] = year;
}

formFields['qualificationLieuObtention2']              = $ds083PE5.declaration.declaration0.qualification.lieuobtention2;

	formFields['qualificationJourRecyclage2'] = '';
	formFields['qualificationMoisRecyclage2'] = '';
	formFields['qualificationAnneeRecyclage2'] = '';
	
if($ds083PE5.declaration.declaration0.qualification.daterecyclage2 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.qualification.daterecyclage2.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourRecyclage2'] = day;
	formFields['qualificationMoisRecyclage2'] = month;
	formFields['qualificationAnneeRecyclage2'] = year;
	}
	
///qualification3

formFields['qualificationType3']                       = $ds083PE5.declaration.declaration0.qualification.typequalification3;
formFields['qualificationActivite3']                   = $ds083PE5.declaration.declaration0.qualification.activitequalification3;
formFields['qualificationNumeroDiplome3']              = $ds083PE5.declaration.declaration0.qualification.numerodiplome3;

	formFields['qualificationJourObtention3'] = '';
	formFields['qualificationMoisObtention3'] = '';
	formFields['qualificationAnneeObtention3'] = '';

if($ds083PE5.declaration.declaration0.qualification.dateobtentiondiplome3 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.qualification.dateobtentiondiplome3.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourObtention3'] = day;
	formFields['qualificationMoisObtention3'] = month;
	formFields['qualificationAnneeObtention3'] = year;
}

formFields['qualificationLieuObtention3']              = $ds083PE5.declaration.declaration0.qualification.lieuobtention3;

	formFields['qualificationJourRecyclage3'] = '';
	formFields['qualificationMoisRecyclage3'] = '';
	formFields['qualificationAnneeRecyclage3'] = '';

if($ds083PE5.declaration.declaration0.qualification.daterecyclage3 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.qualification.daterecyclage3.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourRecyclage3'] = day;
	formFields['qualificationMoisRecyclage3'] = month;
	formFields['qualificationAnneeRecyclage3'] = year;
	}
	
	///qualification 4
	
formFields['qualificationType4']                       = $ds083PE5.declaration.declaration0.qualification.typequalification4;
formFields['qualificationActivite4']                   = $ds083PE5.declaration.declaration0.qualification.activitequalification4;
formFields['qualificationNumeroDiplome4']              = $ds083PE5.declaration.declaration0.qualification.numerodiplome4;

	formFields['qualificationJourObtention4'] ='';
	formFields['qualificationMoisObtention4'] = '';
	formFields['qualificationAnneeObtention4'] = '';

if($ds083PE5.declaration.declaration0.qualification.dateobtentiondiplome4 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.qualification.dateobtentiondiplome4.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourObtention4'] = day;
	formFields['qualificationMoisObtention4'] = month;
	formFields['qualificationAnneeObtention4'] = year;
}

formFields['qualificationLieuObtention4']              = $ds083PE5.declaration.declaration0.qualification.lieuobtention4;

	formFields['qualificationJourRecyclage4'] = '';
	formFields['qualificationMoisRecyclage4'] = '';
	formFields['qualificationAnneeRecyclage4'] = '';

if($ds083PE5.declaration.declaration0.qualification.daterecyclage4 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.qualification.daterecyclage4.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourRecyclage4'] = day;
	formFields['qualificationMoisRecyclage4'] = month;
	formFields['qualificationAnneeRecyclage4'] = year;
	}
	
	///qualification 5
	
	
formFields['qualificationType5']                       = $ds083PE5.declaration.declaration0.qualification.typequalification5;
formFields['qualificationActivite5']                   = $ds083PE5.declaration.declaration0.qualification.activitequalification5;
formFields['qualificationNumeroDiplome5']              = $ds083PE5.declaration.declaration0.qualification.numerodiplome5;

	formFields['qualificationJourObtention5'] = '';
	formFields['qualificationMoisObtention5'] = '';
	formFields['qualificationAnneeObtention5'] = '';

if($ds083PE5.declaration.declaration0.qualification.dateobtentiondiplome5 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.qualification.dateobtentiondiplome5.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourObtention5'] = day;
	formFields['qualificationMoisObtention5'] = month;
	formFields['qualificationAnneeObtention5'] = year;
}

formFields['qualificationLieuObtention5']              = $ds083PE5.declaration.declaration0.qualification.lieuobtention5;

	formFields['qualificationJourRecyclage5'] = '';
	formFields['qualificationMoisRecyclage5'] = '';
	formFields['qualificationAnneeRecyclage5'] = '';

if($ds083PE5.declaration.declaration0.qualification.daterecyclage5 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.qualification.daterecyclage5.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourRecyclage5'] = day;
	formFields['qualificationMoisRecyclage5'] = month;
	formFields['qualificationAnneeRecyclage5'] = year;
	}
	
	///qualification 6
	
formFields['qualificationType6']                       = $ds083PE5.declaration.declaration0.qualification.typequalification6;
formFields['qualificationActivite6']                   = $ds083PE5.declaration.declaration0.qualification.activitequalification6;
formFields['qualificationNumeroDiplome6']              = $ds083PE5.declaration.declaration0.qualification.numerodiplome6;

	formFields['qualificationJourObtention6'] = '';
	formFields['qualificationMoisObtention6'] = '';
	formFields['qualificationAnneeObtention6'] = '';

if($ds083PE5.declaration.declaration0.qualification.dateobtentiondiplome6 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.qualification.dateobtentiondiplome6.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourObtention6'] = day;
	formFields['qualificationMoisObtention6'] = month;
	formFields['qualificationAnneeObtention6'] = year;
}

formFields['qualificationLieuObtention6']              = $ds083PE5.declaration.declaration0.qualification.lieuobtention6;

	formFields['qualificationJourRecyclage6'] = '';
	formFields['qualificationMoisRecyclage6'] = '';
	formFields['qualificationAnneeRecyclage6'] = '';

if($ds083PE5.declaration.declaration0.qualification.daterecyclage6 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.qualification.daterecyclage6.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourRecyclage6'] = day;
	formFields['qualificationMoisRecyclage6'] = month;
	formFields['qualificationAnneeRecyclage6'] = year;
	
	}
	
	///qualification 7

formFields['qualificationType7']                       = $ds083PE5.declaration.declaration0.qualification.typequalification7;
formFields['qualificationActivite7']                   = $ds083PE5.declaration.declaration0.qualification.activitequalification7;
formFields['qualificationNumeroDiplome7']              = $ds083PE5.declaration.declaration0.qualification.numerodiplome7;

	formFields['qualificationJourObtention7'] = '';
	formFields['qualificationMoisObtention7'] = '';
	formFields['qualificationAnneeObtention7'] = '';

if($ds083PE5.declaration.declaration0.qualification.dateobtentiondiplome7 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.qualification.dateobtentiondiplome7.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourObtention7'] = day;
	formFields['qualificationMoisObtention7'] = month;
	formFields['qualificationAnneeObtention7'] = year;
}

formFields['qualificationLieuObtention7']              = $ds083PE5.declaration.declaration0.qualification.lieuobtention7;

	formFields['qualificationJourRecyclage7'] = '';
	formFields['qualificationMoisRecyclage7'] = '';
	formFields['qualificationAnneeRecyclage7'] = '';

if($ds083PE5.declaration.declaration0.qualification.daterecyclage7 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.qualification.daterecyclage7.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourRecyclage7'] = day;
	formFields['qualificationMoisRecyclage7'] = month;
	formFields['qualificationAnneeRecyclage7'] = year;
	
	}
	
	///qualification 8
	
formFields['qualificationType8']                       = $ds083PE5.declaration.declaration0.qualification.typequalification8;
formFields['qualificationActivite8']                   = $ds083PE5.declaration.declaration0.qualification.activitequalification8;
formFields['qualificationNumeroDiplome8']              = $ds083PE5.declaration.declaration0.qualification.numerodiplome8;

	formFields['qualificationJourObtention8'] = '';
	formFields['qualificationMoisObtention8'] = '';
	formFields['qualificationAnneeObtention8'] = '';

if($ds083PE5.declaration.declaration0.qualification.dateobtentiondiplome8 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.qualification.dateobtentiondiplome8.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourObtention8'] = day;
	formFields['qualificationMoisObtention8'] = month;
	formFields['qualificationAnneeObtention8'] = year;
}

formFields['qualificationLieuObtention8']              = $ds083PE5.declaration.declaration0.qualification.lieuobtention8;

	formFields['qualificationJourRecyclage8'] = '';
	formFields['qualificationMoisRecyclage8'] = '';
	formFields['qualificationAnneeRecyclage8'] = '';
	

if($ds083PE5.declaration.declaration0.qualification.daterecyclage8 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.qualification.daterecyclage8.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourRecyclage8'] = day;
	formFields['qualificationMoisRecyclage8'] = month;
	formFields['qualificationAnneeRecyclage8'] = year;
	
	}
	
	
	///qualification 9

	
formFields['qualificationType9']                       = $ds083PE5.declaration.declaration0.qualification.typequalification9;
formFields['qualificationActivite9']                   = $ds083PE5.declaration.declaration0.qualification.activitequalification9;
formFields['qualificationNumeroDiplome9']              = $ds083PE5.declaration.declaration0.qualification.numerodiplome9;


	formFields['qualificationJourObtention9'] = '';
	formFields['qualificationMoisObtention9'] = '';
	formFields['qualificationAnneeObtention9'] = '';
	
if($ds083PE5.declaration.declaration0.qualification.dateobtentiondiplome8 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.qualification.dateobtentiondiplome9.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourObtention9'] = day;
	formFields['qualificationMoisObtention9'] = month;
	formFields['qualificationAnneeObtention9'] = year;
}

formFields['qualificationLieuObtention9']              = $ds083PE5.declaration.declaration0.qualification.lieuobtention9;

	formFields['qualificationJourRecyclage9'] = '';
	formFields['qualificationMoisRecyclage9'] = '';
	formFields['qualificationAnneeRecyclage9'] = '';

if($ds083PE5.declaration.declaration0.qualification.daterecyclage9 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.qualification.daterecyclage9.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourRecyclage9'] = day;
	formFields['qualificationMoisRecyclage9'] = month;
	formFields['qualificationAnneeRecyclage9'] = year;
	
	}
	
	
	//Stages
	
	
formFields['intituleDiplomePreparation']                                   = $ds083PE5.declaration.declaration0.stagiaire.intituleDiplomePreparation;
formFields['formationType']                                                = $ds083PE5.declaration.declaration0.stagiaire.formationType;
formFields['formationActivite']                                            = $ds083PE5.declaration.declaration0.stagiaire.formationactivite;




///stage pratique 1

formFields['stageNomEtablissement1']                                   = $ds083PE5.declaration.declaration0.stagiaire.stagenomEtablissement1;
formFields['stageNumeroNomRueAdresseEtablissement1']                   = $ds083PE5.declaration.declaration0.stagiaire.numeroNomRueadresseEtablissement1;
formFields['stageVillePaysAdresseEtablissement1']                      = ($ds083PE5.declaration.declaration0.stagiaire.villeEtablissement1 !=null ? $ds083PE5.declaration.declaration0.stagiaire.villeEtablissement1 :'') + ' ' +  ($ds083PE5.declaration.declaration0.stagiaire.paysEtablissement1  !=null ? $ds083PE5.declaration.declaration0.stagiaire.paysEtablissement1:'');
formFields['stageNomTuteur1']                  						   = $ds083PE5.declaration.declaration0.stagiaire.stageNomTuteur1;              

	formFields['stageJourDebut1'] = '';
	formFields['stageMoisDebut1'] = '';
	formFields['stageAnneeDebut1'] = '';

if($ds083PE5.declaration.declaration0.stagiaire.datedebutstage1 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.stagiaire.datedebutstage1.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourDebut1'] = day;
	formFields['stageMoisDebut1'] = month;
	formFields['stageAnneeDebut1'] = year;
}

	formFields['stageJourFin1'] = '';
	formFields['stageMoisFin1'] = '';
	formFields['stageAnneeFin1'] = '';


if($ds083PE5.declaration.declaration0.stagiaire.datefinstage1 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.stagiaire.datefinstage1.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourFin1'] = day;
	formFields['stageMoisFin1'] = month;
	formFields['stageAnneeFin1'] = year;

}


///stage pratique 2


formFields['stageNomEtablissement2']                                   = $ds083PE5.declaration.declaration0.stagiaire.stagenomEtablissement2;
formFields['stageNumeroNomRueAdresseEtablissement2']                   = $ds083PE5.declaration.declaration0.stagiaire.numeroNomRueadresseEtablissement2;
formFields['stageVillePaysAdresseEtablissement2']                      = ($ds083PE5.declaration.declaration0.stagiaire.villeEtablissement1 !=null ? $ds083PE5.declaration.declaration0.stagiaire.villeEtablissement1 :'') + ' ' +  ($ds083PE5.declaration.declaration0.stagiaire.paysEtablissement2 !=null ? $ds083PE5.declaration.declaration0.stagiaire.paysEtablissement2 :'') ;
formFields['stageNomTuteur2']                  						   = $ds083PE5.declaration.declaration0.stagiaire.stageNomTuteur2; 


	formFields['stageJourDebut2'] = '';
	formFields['stageMoisDebut2'] = '';
	formFields['stageAnneeDebut2'] = '';

if($ds083PE5.declaration.declaration0.stagiaire.datedebutstage2 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.stagiaire.datedebutstage2.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourDebut2'] = day;
	formFields['stageMoisDebut2'] = month;
	formFields['stageAnneeDebut2'] = year;
}

	formFields['stageJourFin2'] = '';
	formFields['stageMoisFin2'] = '';
	formFields['stageAnneeFin2'] = '';

if($ds083PE5.declaration.declaration0.stagiaire.datefinstage2 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.stagiaire.datefinstage2.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourFin2'] = day;
	formFields['stageMoisFin2'] = month;
	formFields['stageAnneeFin2'] = year;

}


///stage pratique 3

formFields['stageNomEtablissement3']                                   = $ds083PE5.declaration.declaration0.stagiaire.stagenomEtablissement3;
formFields['stageNumeroNomRueAdresseEtablissement3']                   = $ds083PE5.declaration.declaration0.stagiaire.numeroNomRueadresseEtablissement3;
formFields['stageVillePaysAdresseEtablissement3']                      = ($ds083PE5.declaration.declaration0.stagiaire.villeEtablissement3  !=null ? $ds083PE5.declaration.declaration0.stagiaire.villeEtablissement3:'') + ' ' +  ($ds083PE5.declaration.declaration0.stagiaire.paysEtablissement3 !=null ? $ds083PE5.declaration.declaration0.stagiaire.paysEtablissement3 :'');
formFields['stageNomTuteur3']                  						   = $ds083PE5.declaration.declaration0.stagiaire.stageNomTuteur3; 

	formFields['stageJourDebut3'] = '';
	formFields['stageMoisDebut3'] = '';
	formFields['stageAnneeDebut3'] = '';

if($ds083PE5.declaration.declaration0.stagiaire.datedebutstage3 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.stagiaire.datedebutstage3.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourDebut3'] = day;
	formFields['stageMoisDebut3'] = month;
	formFields['stageAnneeDebut3'] = year;
}

	formFields['stageJourFin3'] = '';
	formFields['stageMoisFin3'] = '';
	formFields['stageAnneeFin3'] = '';

if($ds083PE5.declaration.declaration0.stagiaire.datefinstage3 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.stagiaire.datefinstage3.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourFin3'] = day;
	formFields['stageMoisFin3'] = month;
	formFields['stageAnneeFin3'] = year;

}

///stage pratique 4

formFields['stageNomEtablissement4']                                   = $ds083PE5.declaration.declaration0.stagiaire.stagenomEtablissement4;
formFields['stageNumeroNomRueAdresseEtablissement4']                   = $ds083PE5.declaration.declaration0.stagiaire.numeroNomRueadresseEtablissement4;
formFields['stageVillePaysAdresseEtablissement4']                      = ($ds083PE5.declaration.declaration0.stagiaire.villeEtablissement4 !=null ? $ds083PE5.declaration.declaration0.stagiaire.villeEtablissement4:'') + ' ' +  ($ds083PE5.declaration.declaration0.stagiaire.paysEtablissement4 !=null ? $ds083PE5.declaration.declaration0.stagiaire.paysEtablissement4:'');
formFields['stageNomTuteur4']                  						   = $ds083PE5.declaration.declaration0.stagiaire.stageNomTuteur4; 

	formFields['stageJourDebut4'] = '';
	formFields['stageMoisDebut4'] = '';
	formFields['stageAnneeDebut4'] = '';

if($ds083PE5.declaration.declaration0.stagiaire.datedebutstage4 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.stagiaire.datedebutstage4.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourDebut4'] = day;
	formFields['stageMoisDebut4'] = month;
	formFields['stageAnneeDebut4'] = year;
}
	formFields['stageJourFin4'] = '';
	formFields['stageMoisFin4'] = '';
	formFields['stageAnneeFin4'] = '';

if($ds083PE5.declaration.declaration0.stagiaire.datefinstage4 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.stagiaire.datefinstage4.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourFin4'] = day;
	formFields['stageMoisFin4'] = month;
	formFields['stageAnneeFin4'] = year;

}

///stage pratique 5


formFields['stageNomEtablissement5']                                   = $ds083PE5.declaration.declaration0.stagiaire.stagenomEtablissement5;
formFields['stageNumeroNomRueAdresseEtablissement5']                   = $ds083PE5.declaration.declaration0.stagiaire.numeroNomRueadresseEtablissement5;
formFields['stageVillePaysAdresseEtablissement5']                      = ($ds083PE5.declaration.declaration0.stagiaire.villeEtablissement5 !=null ? $ds083PE5.declaration.declaration0.stagiaire.villeEtablissement5 :'') + '' +  ($ds083PE5.declaration.declaration0.stagiaire.paysEtablissement5 !=null ? $ds083PE5.declaration.declaration0.stagiaire.paysEtablissement5 :'') ;
formFields['stageNomTuteur5']                  						   = $ds083PE5.declaration.declaration0.stagiaire.stageNomTuteur5; 

	formFields['stageJourDebut5'] = '';
	formFields['stageMoisDebut5'] = '';
	formFields['stageAnneeDebut5'] = '';

if($ds083PE5.declaration.declaration0.stagiaire.datedebutstage5 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.stagiaire.datedebutstage5.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourDebut5'] = day;
	formFields['stageMoisDebut5'] = month;
	formFields['stageAnneeDebut5'] = year;
}

	formFields['stageJourFin5'] = '';
	formFields['stageMoisFin5'] = '';
	formFields['stageAnneeFin5'] = '';

if($ds083PE5.declaration.declaration0.stagiaire.datefinstage5 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.stagiaire.datefinstage5.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourFin5'] = day;
	formFields['stageMoisFin5'] = month;
	formFields['stageAnneeFin5'] = year;

}

///stage pratique 6

formFields['stageNomEtablissement6']                                   = $ds083PE5.declaration.declaration0.stagiaire.stagenomEtablissement6;
formFields['stageNumeroNomRueAdresseEtablissement6']                   = $ds083PE5.declaration.declaration0.stagiaire.numeroNomRueadresseEtablissement6;
formFields['stageVillePaysAdresseEtablissement6']                      = ($ds083PE5.declaration.declaration0.stagiaire.villeEtablissement6 !=null ? $ds083PE5.declaration.declaration0.stagiaire.villeEtablissement6 :'') + ' ' +  ($ds083PE5.declaration.declaration0.stagiaire.paysEtablissement6  !=null ? $ds083PE5.declaration.declaration0.stagiaire.paysEtablissement6 :'') ;
formFields['stageNomTuteur6']                  						   = $ds083PE5.declaration.declaration0.stagiaire.stageNomTuteur6; 

	formFields['stageJourDebut6'] = '';
	formFields['stageMoisDebut6'] = '';
	formFields['stageAnneeDebut6'] = '';

if($ds083PE5.declaration.declaration0.stagiaire.datedebutstage6 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.stagiaire.datedebutstage6.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourDebut6'] = day;
	formFields['stageMoisDebut6'] = month;
	formFields['stageAnneeDebut6'] = year;
}

	formFields['stageJourFin6'] = '';
	formFields['stageMoisFin6'] = '';
	formFields['stageAnneeFin6'] = '';

if($ds083PE5.declaration.declaration0.stagiaire.datefinstage6 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.stagiaire.datefinstage6.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourFin6'] = day;
	formFields['stageMoisFin6'] = month;
	formFields['stageAnneeFin6'] = year;

}

///stage pratique 7

formFields['stageNomEtablissement7']                                   = $ds083PE5.declaration.declaration0.stagiaire.stagenomEtablissement7;
formFields['stageNumeroNomRueAdresseEtablissement7']                   = $ds083PE5.declaration.declaration0.stagiaire.numeroNomRueadresseEtablissement7;
formFields['stageVillePaysAdresseEtablissement7']                      = ($ds083PE5.declaration.declaration0.stagiaire.villeEtablissement7 !=null ? $ds083PE5.declaration.declaration0.stagiaire.villeEtablissement7 :'')  + ' ' + ($ds083PE5.declaration.declaration0.stagiaire.paysEtablissement7 !=null ? $ds083PE5.declaration.declaration0.stagiaire.paysEtablissement7 :'');
formFields['stageNomTuteur7']                  						   = $ds083PE5.declaration.declaration0.stagiaire.stageNomTuteur7; 


	formFields['stageJourDebut7'] = '';
	formFields['stageMoisDebut7'] = '';
	formFields['stageAnneeDebut7'] = '';

if($ds083PE5.declaration.declaration0.stagiaire.datedebutstage7 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.stagiaire.datedebutstage7.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourDebut7'] = day;
	formFields['stageMoisDebut7'] = month;
	formFields['stageAnneeDebut7'] = year;
}

	formFields['stageJourFin7'] = '';
	formFields['stageMoisFin7'] = '';
	formFields['stageAnneeFin7'] = '';

if($ds083PE5.declaration.declaration0.stagiaire.datefinstage7 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.stagiaire.datefinstage7.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourFin7'] = day;
	formFields['stageMoisFin7'] = month;
	formFields['stageAnneeFin7'] = year;

}

///stage pratique 8

formFields['stageNomEtablissement8']                                   = $ds083PE5.declaration.declaration0.stagiaire.stagenomEtablissement8;
formFields['stageNumeroNomRueAdresseEtablissement8']                   = $ds083PE5.declaration.declaration0.stagiaire.numeroNomRueadresseEtablissement8;
formFields['stageVillePaysAdresseEtablissement8']                      =($ds083PE5.declaration.declaration0.stagiaire.villeEtablissement8 !=null ? $ds083PE5.declaration.declaration0.stagiaire.villeEtablissement8:'') + ' ' + ($ds083PE5.declaration.declaration0.stagiaire.paysEtablissement8 !=null ? $ds083PE5.declaration.declaration0.stagiaire.paysEtablissement8 :'');
formFields['stageNomTuteur8']                  						   = $ds083PE5.declaration.declaration0.stagiaire.stageNomTuteur8; 


	formFields['stageJourDebut8'] = '';
	formFields['stageMoisDebut8'] = '';
	formFields['stageAnneeDebut8'] = '';

if($ds083PE5.declaration.declaration0.stagiaire.datedebutstage8 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.stagiaire.datedebutstage8.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 8;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourDebut8'] = day;
	formFields['stageMoisDebut8'] = month;
	formFields['stageAnneeDebut8'] = year;
}

	formFields['stageJourFin8'] = '';
	formFields['stageMoisFin8'] = '';
	formFields['stageAnneeFin8'] = '';

if($ds083PE5.declaration.declaration0.stagiaire.datefinstage8 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.stagiaire.datefinstage8.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 8;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourFin8'] = day;
	formFields['stageMoisFin8'] = month;
	formFields['stageAnneeFin8'] = year;

}


// autorisation

	formFields['autorisationExerciceJourDelivrance'] = '';
	formFields['autorisationExerciceMoisDelivrance'] = '';
	formFields['autorisationExerciceAnneeDelivrance'] = '';

if($ds083PE5.declaration.declaration0.autorisation.autorisation1 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.declaration.declaration0.autorisation.autorisation1.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['autorisationExerciceJourDelivrance'] = day;
	formFields['autorisationExerciceMoisDelivrance'] = month;
	formFields['autorisationExerciceAnneeDelivrance'] = year;
}
	
	
// activités physiques ou sportives encadrées

formFields['metierIndependantPrincipal']               = ($ds083PE5.activites.activites.metierEducateur=='Principal');
formFields['metierIndependantSecondaire']              = ($ds083PE5.activites.activites.metierEducateur=='Secondaire');

formFields['raisonSocialeIndependant']                 = $ds083PE5.activites.activites.raisonsociale;
formFields['formeEURL']                                = ($ds083PE5.activites.activites.naturejuridique=='EURL');
formFields['formeIndividuelle']                        = ($ds083PE5.activites.activites.naturejuridique=='Entreprise individuelle');
formFields['numeroSiretIndependants']                  =$ds083PE5.activites.activites.numerosiret;

formFields['adresseDeclarantNumeroNomRueIndependants'] = $ds083PE5.activites.activites.adresseDeclarantNumeroNomRueIndependant;
formFields['adresseDeclarantCodePostalIndependants']   = $ds083PE5.activites.activites.adresseDeclarantCodePostalIndependant;
formFields['adresseDeclarantVilleIndependants']        = $ds083PE5.activites.activites.adresseDeclarantVilleIndependant;
formFields['telephoneFixeDeclarantIndependant']        = $ds083PE5.activites.activites.telephoneFixeDeclarantIndependant;
formFields['telephoneMobileDeclarantIndependant']      = $ds083PE5.activites.activites.telephoneMobileDeclarantIndependant;
formFields['courrielDeclarantIndependant']             = $ds083PE5.activites.activites.courrielDeclarantIndependant;
formFields['siteInternetDeclarantIndependant']         = $ds083PE5.activites.activites.siteinternet;

// Activite encadrée independant
/// 1

formFields['activiteEncadreeIndependant']              = $ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.activiteencadreeindependant1;
formFields['disciplineIndependant']                    = $ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.disciplineIndependant1;
formFields['lieuExerciceIndependant']                  = $ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.lieuexerciceIndependant1;
formFields['numeroNomVoieLieuExerciceIndependant']     = $ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.adresseExerciceIndependant1;
formFields['departementLieuExerciceIndependant']       = $ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.departementIndependant1;
formFields['codePostalLieuExerciceIndependant']        = $ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.codePostalIndependant1;
formFields['communeLieuExerciceIndependant']           = $ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.communeIndependant1;

	formFields['jourDebutExerciceIndependant'] = '';
	formFields['moisDebutExerciceIndependant'] = '';
	formFields['anneeDebutExerciceIndependant'] = '';

if($ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant1 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant1.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourDebutExerciceIndependant'] = day;
	formFields['moisDebutExerciceIndependant'] = month;
	formFields['anneeDebutExerciceIndependant'] = year;
}

///2 

formFields['activiteEncadreeIndependant2']              = $ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.activiteencadreeindependant2;
formFields['disciplineIndependant2']                    = $ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.disciplineIndependant2;
formFields['lieuExerciceIndependant2']                  = $ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.lieuexerciceIndependant2;
formFields['numeroNomVoieLieuExerciceIndependant2']     = $ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.adresseExerciceIndependant2;
formFields['departementLieuExerciceIndependant2']       = $ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.departementIndependant2;
formFields['codePostalLieuExerciceIndependant2']        = $ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.codePostalIndependant2;
formFields['communeLieuExerciceIndependant2']           = $ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.communeIndependant2;

	formFields['jourDebutExerciceIndependant2'] = '';
	formFields['moisDebutExerciceIndependant2'] = '';
	formFields['anneeDebutExerciceIndependant2'] = '';

if($ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant2 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant2.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourDebutExerciceIndependant2'] = day;
	formFields['moisDebutExerciceIndependant2'] = month;
	formFields['anneeDebutExerciceIndependant2'] = year;
}


///3

formFields['activiteEncadreeIndependant3']              = $ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.activiteencadreeindependant3;
formFields['disciplineIndependant3']                    = $ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.disciplineIndependant3;
formFields['lieuExerciceIndependant3']                  = $ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.lieuexerciceIndependant3;
formFields['numeroNomVoieLieuExerciceIndependant3']     = $ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.adresseExerciceIndependant3;
formFields['departementLieuExerciceIndependant3']       = $ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.departementIndependant3;
formFields['codePostalLieuExerciceIndependant3']        = $ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.codePostalIndependant3;
formFields['communeLieuExerciceIndependant3']           = $ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.communeIndependant3;

	formFields['jourDebutExerciceIndependant3'] = '';
	formFields['moisDebutExerciceIndependant3'] = '';
	formFields['anneeDebutExerciceIndependant3'] = '';

if($ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant3 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant3.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourDebutExerciceIndependant3'] = day;
	formFields['moisDebutExerciceIndependant3'] = month;
	formFields['anneeDebutExerciceIndependant3'] = year;
}


///4

formFields['activiteEncadreeIndependant4']              = $ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.activiteencadreeindependant4;
formFields['disciplineIndependant4']                    = $ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.disciplineIndependant4;
formFields['lieuExerciceIndependant4']                  = $ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.lieuexerciceIndependant4;
formFields['numeroNomVoieLieuExerciceIndependant4']     = $ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.adresseExerciceIndependant4;
formFields['departementLieuExerciceIndependant4']       = $ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.departementIndependant4;
formFields['codePostalLieuExerciceIndependant4']        = $ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.codePostalIndependant4;
formFields['communeLieuExerciceIndependant4']           = $ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.communeIndependant4;

	formFields['jourDebutExerciceIndependant4'] = '';
	formFields['moisDebutExerciceIndependant4'] = '';
	formFields['anneeDebutExerciceIndependant4'] = '';

if($ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant4 != null) {
var dateTemp = new Date(parseInt ($ds083PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant4.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourDebutExerciceIndependant4'] = day;
	formFields['moisDebutExerciceIndependant4'] = month;
	formFields['anneeDebutExerciceIndependant4'] = year;
}




//Signature



formFields['nomPrenomSignature']                       = $ds083PE5.cadre1.cadre1.nomNaissance + ' ' + $ds083PE5.cadre1.cadre1.prenomDeclarant;
formFields['Signature']                                = $ds083PE5.signature.signature.signatureCoche;
formFields['dateSignature']                            = $ds083PE5.signature.signature.faitLe;

formFields['formationType1']                           = '';
formFields['formationActivite1']                       = '';
formFields['autorisationExerciceJourDelivrance2']       = '';
formFields['autorisationExerciceMoisDelivrance2']       = '';
formFields['autorisationExerciceAnneeDelivrance2']      = '';



/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
 
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp085PE3.signature.signature.lieuSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		dateSignature: $ds086PE5.signature.signature.faitLe,
		autoriteHabilitee :"DDCSPP",
		demandeContexte : "Renouvellement",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
    .load('models/cerfa1269903.pdf') //
    .apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
 
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertificatMedical);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPhoto);
appendPj($attachmentPreprocess.attachmentPreprocess.pjRecyclage);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Moniteur_de_ski_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
   label : 'Moniteur de ski  - Renouvellement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Renouvellement',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});