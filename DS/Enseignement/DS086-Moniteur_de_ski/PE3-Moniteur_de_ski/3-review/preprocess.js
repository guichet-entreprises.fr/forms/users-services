function pad(s) { return (s < 10) ? '0' + s : s; }

var formFields = {};

var civNomPrenom = $ds083PE3.cadre1.cadre1.civilite + ' ' + $ds083PE3.cadre1.cadre1.nomNaissance + ' ' + $ds083PE3.cadre1.cadre1.prenomDeclarant;


/// Dossier de déclaration

formFields['declarationInitiale']                     = true
formFields['renouvellement']                          = false
formFields['departementExercice']                     = $ds083PE3.declaration.declaration.departementexercice;


//Etat Civil
formFields['civiliteMadame']                           = ($ds083PE3.cadre1.cadre1.civilite=='Madame');
formFields['civiliteMonsieur']                         = ($ds083PE3.cadre1.cadre1.civilite=='Monsieur');
formFields['nomNaissance']                             = $ds083PE3.cadre1.cadre1.nomNaissance;
formFields['nomUsage']                                 = $ds083PE3.cadre1.cadre1.nomUsage;
formFields['prenom']                                   = $ds083PE3.cadre1.cadre1.prenomDeclarant;
formFields['nomPere']                                  = $ds083PE3.cadre1.cadre1.nomPereDeclarant;
formFields['nomMere']                                  = $ds083PE3.cadre1.cadre1.nomMereDeclarant;
formFields['prenomPere']                               = $ds083PE3.cadre1.cadre1.prenomPereDeclarant;
formFields['prenomMere']                               = $ds083PE3.cadre1.cadre1.prenomMereDeclarant;


if($ds083PE3.cadre1.cadre1.dateNaissanceDeclarant != null) {
var dateTemp = new Date(parseInt ($ds083PE3.cadre1.cadre1.dateNaissanceDeclarant.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['dateNaissanceJour'] = day;
	formFields['dateNaissanceMois'] = month;
	formFields['dateNaissanceAnnee'] = year;
}


formFields['villeNaissance']                           = $ds083PE3.cadre1.cadre1.lieuNaissanceDeclarant;
formFields['paysNaissance']                            = $ds083PE3.cadre1.cadre1.paysNaissanceDeclarant;
formFields['arrondissementNaissance']                  = $ds083PE3.cadre1.cadre1.arrondissement;
formFields['departementNaissance']                     = $ds083PE3.cadre1.cadre1.departementNaissance;
formFields['nationalite']                              = $ds083PE3.cadre1.cadre1.nationaliteDeclarant;
formFields['nationaliteAutre']                         = $ds083PE3.cadre1.cadre1.autrenationaliteDeclarant;



//Coordonnées

formFields['adresseDeclarantPays']                     = $ds083PE3.adresse.adressePersonnelle.adresseDeclarantPays;
formFields['adresseDeclarantVille']                    = $ds083PE3.adresse.adressePersonnelle.adresseDeclarantVille;
formFields['adresseDeclarantComplement2']              = $ds083PE3.adresse.adressePersonnelle.adressecomplement2;
formFields['adresseDeclarantComplement1']              = $ds083PE3.adresse.adressePersonnelle.adressecomplement1;
formFields['telephoneFixeDeclarant']                   = $ds083PE3.adresse.adressePersonnelle.telephoneFixeDeclarant;
formFields['telephoneMobileDeclarant']                 = $ds083PE3.adresse.adressePersonnelle.telephoneMobileDeclarant;
formFields['adresseDeclarantNumeroNomRue']             = $ds083PE3.adresse.adressePersonnelle.adresseDeclarantNumeroNomRue;
formFields['courrielDeclarant']                        = $ds083PE3.adresse.adressePersonnelle.courrielDeclarant;
formFields['adresseDeclarantCodePostal']               = $ds083PE3.adresse.adressePersonnelle.adresseDeclarantCodePostal;

//stages pratiques

formFields['intituleDiplomePreparation']                                   = $ds083PE3.diplome.diplome.intituleDiplomePreparation;
formFields['formationType']                                                = $ds083PE3.diplome.diplome.formationType;
formFields['formationActivite']                                            = $ds083PE3.diplome.diplome.formationactivite;




///stage pratique 1

formFields['stageNomEtablissement1']                                   = $ds083PE3.stages.stages.stagenomEtablissement1;
formFields['stageNumeroNomRueAdresseEtablissement1']                   = $ds083PE3.stages.stages.numeroNomRueadresseEtablissement1;
formFields['stageVillePaysAdresseEtablissement1']                      = ($ds083PE3.stages.stages.villeEtablissement1 !=null ? $ds083PE3.stages.stages.villeEtablissement1 :' ') + ' ' + ($ds083PE3.stages.stages.paysEtablissement1 !=null ? $ds083PE3.stages.stages.paysEtablissement1:' ');
formFields['stageNomTuteur1'] 										   = $ds083PE3.stages.stages.stageNomTuteur1;

	formFields['stageJourDebut1'] =  '';
	formFields['stageMoisDebut1'] =  '';
	formFields['stageAnneeDebut1']=  '';
	
if($ds083PE3.stages.stages.datedebutstage1 != null) {
var dateTemp = new Date(parseInt ($ds083PE3.stages.stages.datedebutstage1.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourDebut1'] = day;
	formFields['stageMoisDebut1'] = month;
	formFields['stageAnneeDebut1'] = year;
}

	formFields['stageJourFin1'] = '';
	formFields['stageMoisFin1'] = '';
	formFields['stageAnneeFin1'] = '';
	
	
if($ds083PE3.stages.stages.datefinstage1 != null) {
var dateTemp = new Date(parseInt ($ds083PE3.stages.stages.datefinstage1.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourFin1'] = day;
	formFields['stageMoisFin1'] = month;
	formFields['stageAnneeFin1'] = year;

}


///stage pratique 2


formFields['stageNomEtablissement2']                                   = $ds083PE3.stages.stages.stagenomEtablissement2;
formFields['stageNumeroNomRueAdresseEtablissement2']                   = $ds083PE3.stages.stages.numeroNomRueadresseEtablissement2;
formFields['stageVillePaysAdresseEtablissement2']                      = ($ds083PE3.stages.stages.villeEtablissement1 !=null ? $ds083PE3.stages.stages.villeEtablissement1 : ' ')+ ' ' + ($ds083PE3.stages.stages.paysEtablissement2  !=null ? $ds083PE3.stages.stages.paysEtablissement2 :' ');
formFields['stageNomTuteur2'] 										   = $ds083PE3.stages.stages.stageNomTuteur2;

	formFields['stageJourDebut2'] = '';
	formFields['stageMoisDebut2'] = '';
	formFields['stageAnneeDebut2'] = '';
	
if($ds083PE3.stages.stages.datedebutstage2 != null) {
var dateTemp = new Date(parseInt ($ds083PE3.stages.stages.datedebutstage2.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourDebut2'] = day;
	formFields['stageMoisDebut2'] = month;
	formFields['stageAnneeDebut2'] = year;
}

	formFields['stageJourFin2'] = '';
	formFields['stageMoisFin2'] = '';
	formFields['stageAnneeFin2'] = '';
	
if($ds083PE3.stages.stages.datefinstage2 != null) {
var dateTemp = new Date(parseInt ($ds083PE3.stages.stages.datefinstage2.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourFin2'] = day;
	formFields['stageMoisFin2'] = month;
	formFields['stageAnneeFin2'] = year;

}


///stage pratique 3

formFields['stageNomEtablissement3']                                   = $ds083PE3.stages.stages.stagenomEtablissement3;
formFields['stageNumeroNomRueAdresseEtablissement3']                   = $ds083PE3.stages.stages.numeroNomRueadresseEtablissement3;
formFields['stageVillePaysAdresseEtablissement3']                      = ($ds083PE3.stages.stages.villeEtablissement3 !=null ? $ds083PE3.stages.stages.villeEtablissement3 : '')  + ' ' + ($ds083PE3.stages.stages.paysEtablissement3 !=null ? $ds083PE3.stages.stages.paysEtablissement3 : '');
formFields['stageNomTuteur3']                          			   = $ds083PE3.stages.stages.stageNomTuteur3;

	formFields['stageJourDebut3'] =  '';
	formFields['stageMoisDebut3'] =  '';
	formFields['stageAnneeDebut3'] = '';

if($ds083PE3.stages.stages.datedebutstage3 != null) {
var dateTemp = new Date(parseInt ($ds083PE3.stages.stages.datedebutstage3.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourDebut3'] = day;
	formFields['stageMoisDebut3'] = month;
	formFields['stageAnneeDebut3'] = year;
}

	formFields['stageJourFin3'] = '';
	formFields['stageMoisFin3'] = '';
	formFields['stageAnneeFin3'] = '';
	
if($ds083PE3.stages.stages.datefinstage3 != null) {
var dateTemp = new Date(parseInt ($ds083PE3.stages.stages.datefinstage3.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourFin3'] = day;
	formFields['stageMoisFin3'] = month;
	formFields['stageAnneeFin3'] = year;

}

///stage pratique 4

formFields['stageNomEtablissement4']                                   = $ds083PE3.stages.stages.stagenomEtablissement4;
formFields['stageNumeroNomRueAdresseEtablissement4']                   = $ds083PE3.stages.stages.numeroNomRueadresseEtablissement4;
formFields['stageVillePaysAdresseEtablissement4']                      = ($ds083PE3.stages.stages.villeEtablissement4 !=null ? $ds083PE3.stages.stages.villeEtablissement4 : '') + ' ' + ($ds083PE3.stages.stages.paysEtablissement4 !=null ? $ds083PE3.stages.stages.paysEtablissement4 : '');
formFields['stageNomTuteur4']                                         = $ds083PE3.stages.stages.stageNomTuteur4;


	formFields['stageJourDebut4'] = '';
	formFields['stageMoisDebut4'] = '';
	formFields['stageAnneeDebut4'] = '';
if($ds083PE3.stages.stages.datedebutstage4 != null) {
var dateTemp = new Date(parseInt ($ds083PE3.stages.stages.datedebutstage4.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourDebut4'] = day;
	formFields['stageMoisDebut4'] = month;
	formFields['stageAnneeDebut4'] = year;
}

	formFields['stageJourFin4'] = '';
	formFields['stageMoisFin4'] = '';
	formFields['stageAnneeFin4'] = '';
if($ds083PE3.stages.stages.datefinstage4 != null) {
var dateTemp = new Date(parseInt ($ds083PE3.stages.stages.datefinstage4.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourFin4'] = day;
	formFields['stageMoisFin4'] = month;
	formFields['stageAnneeFin4'] = year;

}

///stage pratique 5


formFields['stageNomEtablissement5']                                   = $ds083PE3.stages.stages.stagenomEtablissement5;
formFields['stageNumeroNomRueAdresseEtablissement5']                   = $ds083PE3.stages.stages.numeroNomRueadresseEtablissement5;
formFields['stageVillePaysAdresseEtablissement5']                      = ($ds083PE3.stages.stages.villeEtablissement5 !=null ? $ds083PE3.stages.stages.villeEtablissement5 :'') + ' ' + ($ds083PE3.stages.stages.paysEtablissement5 !=null ? $ds083PE3.stages.stages.paysEtablissement5 :'');
formFields['stageNomTuteur5']                                         = $ds083PE3.stages.stages.stageNomTuteur5;

	
	
	formFields['stageJourDebut5'] = '';
	formFields['stageMoisDebut5'] = '';
	formFields['stageAnneeDebut5'] = '';
if($ds083PE3.stages.stages.datedebutstage5 != null) {
var dateTemp = new Date(parseInt ($ds083PE3.stages.stages.datedebutstage5.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourDebut5'] = day;
	formFields['stageMoisDebut5'] = month;
	formFields['stageAnneeDebut5'] = year;
}

	formFields['stageJourFin5'] = '';
	formFields['stageMoisFin5'] = '';
	formFields['stageAnneeFin5'] = '';
if($ds083PE3.stages.stages.datefinstage5 != null) {
var dateTemp = new Date(parseInt ($ds083PE3.stages.stages.datefinstage5.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourFin5'] = day;
	formFields['stageMoisFin5'] = month;
	formFields['stageAnneeFin5'] = year;

}

///stage pratique 6

formFields['stageNomEtablissement6']                                   = $ds083PE3.stages.stages.stagenomEtablissement6;
formFields['stageNumeroNomRueAdresseEtablissement6']                   = $ds083PE3.stages.stages.numeroNomRueadresseEtablissement6;
formFields['stageVillePaysAdresseEtablissement6']                      = ($ds083PE3.stages.stages.villeEtablissement6 !=null ? $ds083PE3.stages.stages.villeEtablissement6 :'') + ' ' + ($ds083PE3.stages.stages.paysEtablissement6 !=null ? $ds083PE3.stages.stages.paysEtablissement6 : '') ;
formFields['stageNomTuteur6']  									   = $ds083PE3.stages.stages.stageNomTuteur6;

	formFields['stageJourDebut6'] = '';
	formFields['stageMoisDebut6'] = '';
	formFields['stageAnneeDebut6'] = '';
	
if($ds083PE3.stages.stages.datedebutstage6 != null) {
var dateTemp = new Date(parseInt ($ds083PE3.stages.stages.datedebutstage6.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourDebut6'] = day;
	formFields['stageMoisDebut6'] = month;
	formFields['stageAnneeDebut6'] = year;
}
	formFields['stageJourFin6'] = '';
	formFields['stageMoisFin6'] = '';
	formFields['stageAnneeFin6'] = '';
	
if($ds083PE3.stages.stages.datefinstage6 != null) {
var dateTemp = new Date(parseInt ($ds083PE3.stages.stages.datefinstage6.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourFin6'] = day;
	formFields['stageMoisFin6'] = month;
	formFields['stageAnneeFin6'] = year;

}

///stage pratique 7

formFields['stageNomEtablissement7']                                   = $ds083PE3.stages.stages.stagenomEtablissement7;
formFields['stageNumeroNomRueAdresseEtablissement7']                   = $ds083PE3.stages.stages.numeroNomRueadresseEtablissement7;
formFields['stageVillePaysAdresseEtablissement7']                      = ($ds083PE3.stages.stages.villeEtablissement7 !=null ? $ds083PE3.stages.stages.villeEtablissement7 : '') + ' ' +  ($ds083PE3.stages.stages.paysEtablissement7 !=null ? $ds083PE3.stages.stages.paysEtablissement7: '')  ;
formFields['stageNomTuteur7']										   = $ds083PE3.stages.stages.stageNomTuteur7 ;


	formFields['stageJourDebut7'] = '';
	formFields['stageMoisDebut7'] = '';
	formFields['stageAnneeDebut7'] = '';
if($ds083PE3.stages.stages.datedebutstage7 != null) {
var dateTemp = new Date(parseInt ($ds083PE3.stages.stages.datedebutstage7.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourDebut7'] = day;
	formFields['stageMoisDebut7'] = month;
	formFields['stageAnneeDebut7'] = year;
}

	formFields['stageJourFin7'] = '';
	formFields['stageMoisFin7'] = '';
	formFields['stageAnneeFin7'] = '';

if($ds083PE3.stages.stages.datefinstage7 != null) {
var dateTemp = new Date(parseInt ($ds083PE3.stages.stages.datefinstage7.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourFin7'] = day;
	formFields['stageMoisFin7'] = month;
	formFields['stageAnneeFin7'] = year;

}

///stage pratique 8

formFields['stageNomEtablissement8']                                   = $ds083PE3.stages.stages.stagenomEtablissement8;
formFields['stageNumeroNomRueAdresseEtablissement8']                   = $ds083PE3.stages.stages.numeroNomRueadresseEtablissement8;
formFields['stageVillePaysAdresseEtablissement8']                      = ($ds083PE3.stages.stages.villeEtablissement8 !=null ? $ds083PE3.stages.stages.villeEtablissement8 : '')  + ' ' +  ($ds083PE3.stages.stages.paysEtablissement8 !=null ? $ds083PE3.stages.stages.paysEtablissement8 : '') ;
formFields['stageNomTuteur8']										   = $ds083PE3.stages.stages.stageNomTuteur8;

	formFields['stageJourDebut8'] = '';
	formFields['stageMoisDebut8'] = '';
	formFields['stageAnneeDebut8'] = '';
	
if($ds083PE3.stages.stages.datedebutstage8 != null) {
var dateTemp = new Date(parseInt ($ds083PE3.stages.stages.datedebutstage8.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 8;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourDebut8'] = day;
	formFields['stageMoisDebut8'] = month;
	formFields['stageAnneeDebut8'] = year;
}

	formFields['stageJourFin8'] = '';
	formFields['stageMoisFin8'] = '';
	formFields['stageAnneeFin8'] = '';
if($ds083PE3.stages.stages.datefinstage8 != null) {
var dateTemp = new Date(parseInt ($ds083PE3.stages.stages.datefinstage8.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 8;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['stageJourFin8'] = day;
	formFields['stageMoisFin8'] = month;
	formFields['stageAnneeFin8'] = year;

}


	
	
// activités physiques ou sportives encadrées

formFields['metierIndependantPrincipal']               = ($ds083PE3.activites.activites.metierEducateur=='Principal');
formFields['metierIndependantSecondaire']              = ($ds083PE3.activites.activites.metierEducateur=='Secondaire');

formFields['raisonSocialeIndependant']                 = $ds083PE3.activites.activites.raisonsociale;
formFields['formeEURL']                                = ($ds083PE3.activites.activites.naturejuridique=='EURL');
formFields['formeIndividuelle']                        = ($ds083PE3.activites.activites.naturejuridique=='Entreprise individuelle');
formFields['numeroSiretIndependants']                  =$ds083PE3.activites.activites.numerosiret;

formFields['adresseDeclarantNumeroNomRueIndependants'] = $ds083PE3.activites.activites.adresseDeclarantNumeroNomRueIndependant;
formFields['adresseDeclarantCodePostalIndependants']   = $ds083PE3.activites.activites.adresseDeclarantCodePostalIndependant;
formFields['adresseDeclarantVilleIndependants']        = $ds083PE3.activites.activites.adresseDeclarantVilleIndependant;
formFields['telephoneFixeDeclarantIndependant']        = $ds083PE3.activites.activites.telephoneFixeDeclarantIndependant;
formFields['telephoneMobileDeclarantIndependant']      = $ds083PE3.activites.activites.telephoneMobileDeclarantIndependant;
formFields['courrielDeclarantIndependant']             = $ds083PE3.activites.activites.courrielDeclarantIndependant;
formFields['siteInternetDeclarantIndependant']         = $ds083PE3.activites.activites.siteinternet;

// Activite encadrée independant
/// 1

formFields['activiteEncadreeIndependant']              = $ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.activiteencadreeindependant1;
formFields['disciplineIndependant']                    = $ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.disciplineIndependant1;
formFields['lieuExerciceIndependant']                  = $ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.lieuexerciceIndependant1;
formFields['numeroNomVoieLieuExerciceIndependant']     = $ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.adresseExerciceIndependant1;
formFields['departementLieuExerciceIndependant']       = $ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.departementIndependant1;
formFields['codePostalLieuExerciceIndependant']        = $ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.codePostalIndependant1;
formFields['communeLieuExerciceIndependant']           = $ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.communeIndependant1;


if($ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant1 != null) {
var dateTemp = new Date(parseInt ($ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant1.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourDebutExerciceIndependant'] = day;
	formFields['moisDebutExerciceIndependant'] = month;
	formFields['anneeDebutExerciceIndependant'] = year;
}

///2 

formFields['activiteEncadreeIndependant2']              = $ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.activiteencadreeindependant2;
formFields['disciplineIndependant2']                    = $ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.disciplineIndependant2;
formFields['lieuExerciceIndependant2']                  = $ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.lieuexerciceIndependant2;
formFields['numeroNomVoieLieuExerciceIndependant2']     = $ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.adresseExerciceIndependant2;
formFields['departementLieuExerciceIndependant2']       = $ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.departementIndependant2;
formFields['codePostalLieuExerciceIndependant2']        = $ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.codePostalIndependant2;
formFields['communeLieuExerciceIndependant2']           = $ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.communeIndependant2;

	formFields['jourDebutExerciceIndependant2'] = '';
	formFields['moisDebutExerciceIndependant2'] = '';
	formFields['anneeDebutExerciceIndependant2'] = '';
	
if($ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant2 != null) {
var dateTemp = new Date(parseInt ($ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant2.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourDebutExerciceIndependant2'] = day;
	formFields['moisDebutExerciceIndependant2'] = month;
	formFields['anneeDebutExerciceIndependant2'] = year;
}


///3

formFields['activiteEncadreeIndependant3']              = $ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.activiteencadreeindependant3;
formFields['disciplineIndependant3']                    = $ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.disciplineIndependant3;
formFields['lieuExerciceIndependant3']                  = $ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.lieuexerciceIndependant3;
formFields['numeroNomVoieLieuExerciceIndependant3']     = $ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.adresseExerciceIndependant3;
formFields['departementLieuExerciceIndependant3']       = $ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.departementIndependant3;
formFields['codePostalLieuExerciceIndependant3']        = $ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.codePostalIndependant3;
formFields['communeLieuExerciceIndependant3']           = $ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.communeIndependant3;

	formFields['jourDebutExerciceIndependant3'] = '';
	formFields['moisDebutExerciceIndependant3'] = '';
	formFields['anneeDebutExerciceIndependant3'] = '';
	
if($ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant3 != null) {
var dateTemp = new Date(parseInt ($ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant3.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourDebutExerciceIndependant3'] = day;
	formFields['moisDebutExerciceIndependant3'] = month;
	formFields['anneeDebutExerciceIndependant3'] = year;
}


///4

formFields['activiteEncadreeIndependant4']              = $ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.activiteencadreeindependant4;
formFields['disciplineIndependant4']                    = $ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.disciplineIndependant4;
formFields['lieuExerciceIndependant4']                  = $ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.lieuexerciceIndependant4;
formFields['numeroNomVoieLieuExerciceIndependant4']     = $ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.adresseExerciceIndependant4;
formFields['departementLieuExerciceIndependant4']       = $ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.departementIndependant4;
formFields['codePostalLieuExerciceIndependant4']        = $ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.codePostalIndependant4;
formFields['communeLieuExerciceIndependant4']           = $ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.communeIndependant4;


	formFields['jourDebutExerciceIndependant4'] = '';
	formFields['moisDebutExerciceIndependant4'] = '';
	formFields['anneeDebutExerciceIndependant4'] = '';
if($ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant4 != null) {
var dateTemp = new Date(parseInt ($ds083PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant4.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourDebutExerciceIndependant4'] = day;
	formFields['moisDebutExerciceIndependant4'] = month;
	formFields['anneeDebutExerciceIndependant4'] = year;
}




//Signature



formFields['nomPrenomSignature']                       = $ds083PE3.cadre1.cadre1.nomNaissance + ' ' + $ds083PE3.cadre1.cadre1.prenomDeclarant;
formFields['Signature']                                = $ds083PE3.signature.signature.signatureCoche;
formFields['dateSignature']                            = $ds083PE3.signature.signature.faitLe;

//Vide en bleu 

formFields['qualificationType4']                       = '';
formFields['qualificationActivite4']                   = '';
formFields['qualificationNumeroDiplome4']              = '';
formFields['qualificationJourObtention4']              = '';
formFields['qualificationMoisObtention4']              = '';
formFields['qualificationAnneeObtention4']             = '';
formFields['qualificationLieuObtention4']              = '';
formFields['qualificationJourRecyclage4']              = '';
formFields['qualificationMoisRecyclage4']              = '';
formFields['qualificationAnneeRecyclage4']             = '';
formFields['qualificationJourRecyclage5']              = '';
formFields['qualificationMoisRecyclage5']              = '';
formFields['qualificationAnneeRecyclage5']             = '';
formFields['qualificationJourRecyclage1']              = '';
formFields['qualificationMoisRecyclage1']              = '';
formFields['qualificationAnneeRecyclage1']             = '';
formFields['qualificationType2']                       = '';
formFields['qualificationActivite2']                   = '';
formFields['qualificationNumeroDiplome2']              = '';
formFields['qualificationJourObtention2']              = '';
formFields['qualificationMoisObtention2']              = '';
formFields['qualificationAnneeObtention2']             = '';
formFields['qualificationLieuObtention2']              = '';
formFields['qualificationJourRecyclage2']              = '';
formFields['qualificationMoisRecyclage2']              = '';
formFields['qualificationAnneeRecyclage2']             = '';
formFields['qualificationType3']                       = '';
formFields['qualificationActivite3']                   = '';
formFields['qualificationNumeroDiplome3']              = '';
formFields['qualificationJourObtention3']              = '';
formFields['qualificationMoisObtention3']              = '';
formFields['qualificationAnneeObtention3']             = '';
formFields['qualificationLieuObtention3']              = '';
formFields['qualificationType1']                       = '';
formFields['qualificationActivite1']                   = '';
formFields['qualificationNumeroDiplome1']              = '';
formFields['qualificationJourObtention1']              = '';
formFields['qualificationMoisObtention1']              = '';
formFields['qualificationAnneeObtention1']             = '';
formFields['qualificationLieuObtention1']              = '';
formFields['qualificationType5']                       = '';
formFields['qualificationActivite5']                   = '';
formFields['qualificationNumeroDiplome5']              = '';
formFields['qualificationLieuObtention5']              = '';
formFields['qualificationJourObtention5']              = '';
formFields['qualificationMoisObtention5']              = '';
formFields['qualificationAnneeObtention5']             = '';
formFields['qualificationJourRecyclage3']              = '';
formFields['qualificationMoisRecyclage3']              = '';
formFields['qualificationAnneeRecyclage3']             = '';
formFields['equivalenceActivite1']                     = '';
formFields['equivalenceType1']                         = '';
formFields['equivalenceNumero1']                       = '';
formFields['equivalenceJourDelivrance1']               = '';
formFields['equivalenceMoisDelivrance1']               = '';
formFields['equivalenceAnneeDelivrance1']              = '';
formFields['equivalenceType6']                         = '';
formFields['equivalenceActivite6']                     = '';
formFields['equivalenceNumero6']                       = '';
formFields['equivalenceJourDelivrance6']               = '';
formFields['equivalenceMoisDelivrance6']               = '';
formFields['equivalenceAnneeDelivrance6']              = '';
formFields['equivalenceType7']                         = '';
formFields['equivalenceActivite7']                     = '';
formFields['equivalenceNumero7']                       = '';
formFields['equivalenceJourDelivrance7']               = '';
formFields['equivalenceMoisDelivrance7']               = '';
formFields['equivalenceAnneeDelivrance7']              = '';
formFields['equivalenceType8']                         = '';
formFields['equivalenceActivite8']                     = '';
formFields['equivalenceNumero8']                       = '';
formFields['equivalenceJourDelivrance8']               = '';
formFields['equivalenceMoisDelivrance8']               = '';
formFields['equivalenceAnneeDelivrance8']              = '';
formFields['equivalenceType9']                         = '';
formFields['equivalenceActivite9']                     = '';
formFields['equivalenceNumero9']                       = '';
formFields['equivalenceJourDelivrance9']               = '';
formFields['equivalenceMoisDelivrance9']               = '';
formFields['equivalenceAnneeDelivrance9']              = '';
formFields['qualificationType6']                       = '';
formFields['qualificationActivite6']                   = '';
formFields['qualificationNumeroDiplome6']              = '';
formFields['qualificationLieuObtention6']              = '';
formFields['qualificationAnneeObtention6']             = '';
formFields['qualificationMoisObtention6']              = '';
formFields['qualificationJourObtention6']              = '';
formFields['qualificationJourRecyclage6']              = '';
formFields['qualificationMoisRecyclage6']              = '';
formFields['qualificationAnneeRecyclage6']             = '';
formFields['qualificationType7']                       = '';
formFields['qualificationActivite7']                   = '';
formFields['qualificationNumeroDiplome7']              = '';
formFields['qualificationLieuObtention7']              = '';
formFields['qualificationAnneeObtention7']             = '';
formFields['qualificationMoisObtention7']              = '';
formFields['qualificationJourObtention7']              = '';
formFields['qualificationJourRecyclage7']              = '';
formFields['qualificationMoisRecyclage7']              = '';
formFields['qualificationAnneeRecyclage7']             = '';
formFields['qualificationType8']                       = '';
formFields['qualificationActivite8']                   = '';
formFields['qualificationNumeroDiplome8']              = '';
formFields['qualificationJourObtention8']              = '';
formFields['qualificationMoisObtention8']              = '';
formFields['qualificationAnneeObtention8']             = '';
formFields['qualificationLieuObtention8']              = '';
formFields['qualificationJourRecyclage8']              = '';
formFields['qualificationMoisRecyclage8']              = '';
formFields['qualificationAnneeRecyclage8']             = '';
formFields['qualificationType9']                       = '';
formFields['qualificationActivite9']                   = '';
formFields['qualificationNumeroDiplome9']              = '';
formFields['qualificationJourObtention9']              = '';
formFields['qualificationMoisObtention9']              = '';
formFields['qualificationAnneeObtention9']             = '';
formFields['qualificationLieuObtention9']              = '';
formFields['qualificationJourRecyclage9']              = '';
formFields['qualificationMoisRecyclage9']              = '';
formFields['qualificationAnneeRecyclage9']             = '';
formFields['equivalenceType2']                         = '';
formFields['equivalenceActivite2']                     = '';
formFields['equivalenceNumero2']                       = '';
formFields['equivalenceJourDelivrance2']               = '';
formFields['equivalenceMoisDelivrance2']               = '';
formFields['equivalenceAnneeDelivrance2']              = '';
formFields['equivalenceType3']                         = '';
formFields['equivalenceActivite3']                     = '';
formFields['equivalenceNumero3']                       = '';
formFields['equivalenceJourDelivrance3']               = '';
formFields['equivalenceMoisDelivrance3']               = '';
formFields['equivalenceAnneeDelivrance3']              = '';
formFields['equivalenceActivite1']                     = '';
formFields['equivalenceType1']                         = '';
formFields['equivalenceNumero1']                       = '';
formFields['equivalenceJourDelivrance1']               = '';
formFields['equivalenceMoisDelivrance1']               = '';
formFields['equivalenceAnneeDelivrance1']              = '';
formFields['equivalenceType6']                         = '';
formFields['equivalenceActivite6']                     = '';
formFields['equivalenceNumero6']                       = '';
formFields['equivalenceJourDelivrance6']               = '';
formFields['equivalenceMoisDelivrance6']               = '';
formFields['equivalenceAnneeDelivrance6']              = '';
formFields['equivalenceType7']                         = '';
formFields['equivalenceActivite7']                     = '';
formFields['equivalenceNumero7']                       = '';
formFields['equivalenceJourDelivrance7']               = '';
formFields['equivalenceMoisDelivrance7']               = '';
formFields['equivalenceAnneeDelivrance7']              = '';
formFields['equivalenceType8']                         = '';
formFields['equivalenceActivite8']                     = '';
formFields['equivalenceNumero8']                       = '';
formFields['equivalenceJourDelivrance8']               = '';
formFields['equivalenceMoisDelivrance8']               = '';
formFields['equivalenceAnneeDelivrance8']              = '';
formFields['equivalenceType9']                         = '';
formFields['equivalenceActivite9']                     = '';
formFields['equivalenceNumero9']                       = '';
formFields['equivalenceJourDelivrance9']               = '';
formFields['equivalenceMoisDelivrance9']               = '';
formFields['equivalenceAnneeDelivrance9']              = '';
formFields ['autorisationExerciceJourDelivrance2']	   = '';
formFields ['autorisationExerciceMoisDelivrance2']	   = '';
formFields ['autorisationExerciceAnneeDelivrance2']	   = '';
formFields['autorisationExerciceJourDelivrance']       = '';
formFields['autorisationExerciceMoisDelivrance']       = '';
formFields['autorisationExerciceAnneeDelivrance']      = '';
formFields['formationType1']                           = '';
formFields['formationActivite1']                       = '';



/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
 
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp085PE3.signature.signature.lieuSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		dateSignature: $ds086PE3.signature.signature.faitLe,
		autoriteHabilitee :"DDCSPP",
		demandeContexte : "Stagiaire en cours de formation",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
    .load('models/cerfa1269903.pdf') //
    .apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
 
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertificatMedical);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPhoto);
appendPj($attachmentPreprocess.attachmentPreprocess.pjexigencesminimales);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Moniteur_de_ski_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
   label : 'Moniteur de ski  - Stagiaire en cours de formation',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Stagiaire en cours de formation',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});