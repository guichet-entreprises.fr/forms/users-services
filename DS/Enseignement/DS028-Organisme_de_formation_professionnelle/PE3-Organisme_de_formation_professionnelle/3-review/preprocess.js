var cerfaFields = {};

function pad(s) { return (s < 10) ? '0' + s : s; } 
var civNomPrenom = $ds028PE3.a.a0.nomPrenomDenomination;
// var region = $ds028PE3.signatureGroup.signature.regionExercice;


// Identification du déclarant 
cerfaFields['numDeclaration']                             		= $ds028PE3.a.a0.numDeclaration;
cerfaFields['formeJuridique']                                	= $ds028PE3.a.a0.formeJuridique;
cerfaFields['siret']                                   			= $ds028PE3.a.a0.siret;
cerfaFields['codeNAF']                                       	= $ds028PE3.a.a0.codeNAF;
cerfaFields['nomPrenomDenomination']                     		= $ds028PE3.a.a0.nomPrenomDenomination;

cerfaFields['adresse']                     						= $ds028PE3.a.a0.a00.adresseNumeroNomRue + ' ' + ($ds028PE3.a.a0.a00.adresseComplement != null ? ' ' + $ds028PE3.a.a0.a00.adresseComplement : '') + ' ' + ($ds028PE3.a.a0.a00.adresseCodePostal != null ? ' ' + $ds028PE3.a.a0.a00.adresseCodePostal : '') + ' ' + $ds028PE3.a.a0.a00.adresseCommune;
cerfaFields['adressePubliqueOui']   							= ($ds028PE3.a.a0.a00.ouiNon=='Oui');
cerfaFields['adressePubliqueNon']  	 							= ($ds028PE3.a.a0.a00.ouiNon=='Non');
cerfaFields['telephone']                        				= $ds028PE3.a.a0.a00.telephone;
cerfaFields['email']                            				= $ds028PE3.a.a0.a00.email;


// Caractéristiques de l'organisme
cerfaFields['exerciceComptableDu']                             	= $ds028PE3.b.b0.exerciceComptable.from;
cerfaFields['exerciceComptableAu']                             	= $ds028PE3.b.b0.exerciceComptable.to;


// Bilan financier - Produits
cerfaFields['totalProduitsRealises']                            = $ds028PE3.c.c0.totalProduitsRealises;
cerfaFields['caGlobal']                                			= $ds028PE3.c.c0.caGlobal;

cerfaFields['provenanceEntreprise']                             = $ds028PE3.c.c0.c00.c000.provenanceEntreprise;
cerfaFields['fondAssurance']                            		= $ds028PE3.c.c0.c00.c000.fondAssurance;
cerfaFields['formationAgents']                             		= $ds028PE3.c.c0.c00.c000.formationAgents;
cerfaFields['personnesIndiv']                          			= $ds028PE3.c.c0.c00.c000.personnesIndiv;
cerfaFields['orgFormation']                          			= $ds028PE3.c.c0.c00.c000.orgFormation;

cerfaFields['totalProduit']                                		= $ds028PE3.c.c0.c00.c000.c0000.totalProduit;
cerfaFields['contratPro']                                		= $ds028PE3.c.c0.c00.c000.c0000.contratPro;
cerfaFields['congeIndiv']                                		= $ds028PE3.c.c0.c00.c000.c0000.congeIndiv;
cerfaFields['comptePerso']                                		= $ds028PE3.c.c0.c00.c000.c0000.comptePerso;
cerfaFields['rechercheEmploi']                                	= $ds028PE3.c.c0.c00.c000.c0000.rechercheEmploi;
cerfaFields['autreDispositifs']                                	= $ds028PE3.c.c0.c00.c000.c0000.autreDispositifs;

cerfaFields['instanceEU']                                		= $ds028PE3.c.c0.c00.c000.c0001.instanceEU;
cerfaFields['etat']                                				= $ds028PE3.c.c0.c00.c000.c0001.etat;
cerfaFields['conseilsRegionaux']                               	= $ds028PE3.c.c0.c00.c000.c0001.conseilsRegionaux;
cerfaFields['poleEmploi']                                		= $ds028PE3.c.c0.c00.c000.c0001.poleEmploi;
cerfaFields['autresRessourcesPubliques']                        = $ds028PE3.c.c0.c00.c000.c0001.autresRessourcesPubliques;

cerfaFields['outilsPedagogiques']                             	= $ds028PE3.c.c0.c00.c001.outilsPedagogiques;
cerfaFields['autresProduits']                            		= $ds028PE3.c.c0.c00.c002.autresProduits;


// Bilan financier - Charges
cerfaFields['totalCharges']                             		= $ds028PE3.d.d0.totalCharges;
cerfaFields['salairesFormateurs']                               = $ds028PE3.d.d0.salairesFormateurs;
cerfaFields['achatsPrestation']                                	= $ds028PE3.d.d0.achatsPrestation;


// Personnes 
cerfaFields['nbrPersonnesFormation']                     		= $ds028PE3.e.e0.e00.nbrPersonnesFormation;
cerfaFields['nbrPersonnesSousTraitance']                       	= $ds028PE3.e.e0.e00.nbrPersonnesSousTraitance;
cerfaFields['nbrHeuresFormation']                       		= $ds028PE3.e.e0.e01.nbrHeuresFormation;
cerfaFields['nbrHeuresSousTraitance']                        	= $ds028PE3.e.e0.e01.nbrHeuresSousTraitance;


// Bilan pédagogique - partie F
cerfaFields['totalNbrStagiaires1']                           	= $ds028PE3.f.f0.f1.f10.totalNbrStagiaires1;
cerfaFields['nbrStagiaire1A']                        		 	= $ds028PE3.f.f0.f1.f10.nbrStagiaire1A;
cerfaFields['nbrStagiaire1B']                        			= $ds028PE3.f.f0.f1.f10.nbrStagiaire1B;
cerfaFields['nbrStagiaire1C']                          			= $ds028PE3.f.f0.f1.f10.nbrStagiaire1C;
cerfaFields['nbrStagiaire1D']                     				= $ds028PE3.f.f0.f1.f10.nbrStagiaire1D;
cerfaFields['nbrStagiaire1E']                          			= $ds028PE3.f.f0.f1.f10.nbrStagiaire1E;
cerfaFields['totalHeuresFormation1']                     		= $ds028PE3.f.f0.f1.f11.totalHeuresFormation1;
cerfaFields['nbrTotalHeures1A']                   				= $ds028PE3.f.f0.f1.f11.nbrTotalHeures1A;
cerfaFields['nbrTotalHeures1B']                  				= $ds028PE3.f.f0.f1.f11.nbrTotalHeures1B;
cerfaFields['nbrTotalHeures1C']                    				= $ds028PE3.f.f0.f1.f11.nbrTotalHeures1C;
cerfaFields['nbrTotalHeures1D']               					= $ds028PE3.f.f0.f1.f11.nbrTotalHeures1D;
cerfaFields['nbrTotalHeures1E']                    				= $ds028PE3.f.f0.f1.f11.nbrTotalHeures1E;

cerfaFields['totalNbrStagiaires2']                         		= $ds028PE3.f.f0.f2.f20.totalNbrStagiaires2;
cerfaFields['nbrStagiaire2A']                  					= $ds028PE3.f.f0.f2.f20.nbrStagiaire2A;
cerfaFields['nbrStagiaire2B']               		 			= $ds028PE3.f.f0.f2.f20.nbrStagiaire2B;
cerfaFields['totalHeuresFormation2']                   			= $ds028PE3.f.f0.f2.f21.totalHeuresFormation2;
cerfaFields['nbrTotalHeures2A']            						= $ds028PE3.f.f0.f2.f21.nbrTotalHeures2A;
cerfaFields['nbrTotalHeures2B']          						= $ds028PE3.f.f0.f2.f21.nbrTotalHeures2B;

cerfaFields['totalNbrStagiaires3']             = $ds028PE3.f.f0.f3.f30.totalNbrStagiaires3;
cerfaFields['nbrStagiaire3A']                  = $ds028PE3.f.f0.f3.f30.f300.nbrStagiaire3A;
cerfaFields['nbrStagiaire3B']                  = $ds028PE3.f.f0.f3.f30.f300.nbrStagiaire3B;
cerfaFields['nbrStagiaire3C']                  = $ds028PE3.f.f0.f3.f30.f300.nbrStagiaire3C;
cerfaFields['nbrStagiaire3D']                  = $ds028PE3.f.f0.f3.f30.f300.nbrStagiaire3D;
cerfaFields['nbrStagiaire3E']                  = $ds028PE3.f.f0.f3.f30.f300.nbrStagiaire3E;
cerfaFields['nbrStagiaire3F']                  = $ds028PE3.f.f0.f3.f30.f300.nbrStagiaire3F;
cerfaFields['nbrStagiaire3G']                  = $ds028PE3.f.f0.f3.f30.f300.nbrStagiaire3G;
cerfaFields['nbrStagiaire3H']                  = $ds028PE3.f.f0.f3.f30.f300.nbrStagiaire3H;
cerfaFields['nbrStagiaire3I']                  = $ds028PE3.f.f0.f3.f30.f300.nbrStagiaire3I;
cerfaFields['nbrStagiaire3J']                  = $ds028PE3.f.f0.f3.f30.f300.nbrStagiaire3J;
cerfaFields['totalHeuresFormation3'] 		   = $ds028PE3.f.f0.f3.f31.totalHeuresFormation3;
cerfaFields['nbrTotalHeures3A']                = $ds028PE3.f.f0.f3.f31.f301.nbrTotalHeures3A;
cerfaFields['nbrTotalHeures3B']                = $ds028PE3.f.f0.f3.f31.f301.nbrTotalHeures3B;
cerfaFields['nbrTotalHeures3C']                = $ds028PE3.f.f0.f3.f31.f301.nbrTotalHeures3C;
cerfaFields['nbrTotalHeures3D']                = $ds028PE3.f.f0.f3.f31.f301.nbrTotalHeures3D;
cerfaFields['nbrTotalHeures3E']                = $ds028PE3.f.f0.f3.f31.f301.nbrTotalHeures3E;
cerfaFields['nbrTotalHeures3F']                = $ds028PE3.f.f0.f3.f31.f301.nbrTotalHeures3F;
cerfaFields['nbrTotalHeures3G']                = $ds028PE3.f.f0.f3.f31.f301.nbrTotalHeures3G;
cerfaFields['nbrTotalHeures3H']                = $ds028PE3.f.f0.f3.f31.f301.nbrTotalHeures3H;
cerfaFields['nbrTotalHeures3I']                = $ds028PE3.f.f0.f3.f31.f301.nbrTotalHeures3I;
cerfaFields['nbrTotalHeures3J']                = $ds028PE3.f.f0.f3.f31.f301.nbrTotalHeures3J;

cerfaFields['formation1']                      = $ds028PE3.f.f0.f4.f40.formation1;
cerfaFields['formation2']                      = $ds028PE3.f.f0.f4.f40.formation2;
cerfaFields['formation3']                      = $ds028PE3.f.f0.f4.f40.formation3;
cerfaFields['formation4']                      = $ds028PE3.f.f0.f4.f40.formation4;
cerfaFields['formation5']                      = $ds028PE3.f.f0.f4.f40.formation5;
cerfaFields['precisionFormation']              = $ds028PE3.f.f0.f4.f40.precisionFormation;
cerfaFields['code1']                           = $ds028PE3.f.f0.f4.f40.formation1.getId();
cerfaFields['code2']                           = $ds028PE3.f.f0.f4.f40.formation2.getId();
cerfaFields['code3']                           = $ds028PE3.f.f0.f4.f40.formation3.getId();
cerfaFields['code4']                           = $ds028PE3.f.f0.f4.f40.formation4.getId();
cerfaFields['code5']                           = $ds028PE3.f.f0.f4.f40.formation5.getId();
cerfaFields['nbrStagiaire41']                  = $ds028PE3.f.f0.f4.f40.nbrStagiaire41;
cerfaFields['nbrStagiaire42']                  = $ds028PE3.f.f0.f4.f40.nbrStagiaire42;
cerfaFields['nbrStagiaire43']                  = $ds028PE3.f.f0.f4.f40.nbrStagiaire43;
cerfaFields['nbrStagiaire44']                  = $ds028PE3.f.f0.f4.f40.nbrStagiaire44;
cerfaFields['nbrStagiaire45']                  = $ds028PE3.f.f0.f4.f40.nbrStagiaire45;
cerfaFields['nbrStagiaireAutresSpecialites']   = $ds028PE3.f.f0.f4.f40.nbrStagiaireAutresSpecialites;
cerfaFields['nbrStagiaire4']                   = $ds028PE3.f.f0.f4.f40.f400.nbrStagiaire4;
cerfaFields['nbrTotalHeures41']                = $ds028PE3.f.f0.f4.f40.nbrTotalHeures41;
cerfaFields['nbrTotalHeures42']                = $ds028PE3.f.f0.f4.f40.nbrTotalHeures42;
cerfaFields['nbrTotalHeures43']                = $ds028PE3.f.f0.f4.f40.nbrTotalHeures43;
cerfaFields['nbrTotalHeures44']                = $ds028PE3.f.f0.f4.f40.nbrTotalHeures44;
cerfaFields['nbrTotalHeures45']                = $ds028PE3.f.f0.f4.f40.nbrTotalHeures45;
cerfaFields['nbrTotalHeuresAutresSpecialites'] = $ds028PE3.f.f0.f4.f40.nbrTotalHeuresAutresSpecialites;
cerfaFields['nbrTotalHeures4']                 = $ds028PE3.f.f0.f4.f40.f400.nbrTotalHeures4;


//Bilan pédagogique - partie G 
cerfaFields['nbrStagiaire5']                   = $ds028PE3.g.g0.nbrStagiaire5;
cerfaFields['nbrTotalHeures5']                 = $ds028PE3.g.g0.nbrTotalHeures5;

// Dirigeant
cerfaFields['nomPrenomDirigeant']              = $ds028PE3.h.h0.civilite + ' ' + $ds028PE3.h.h0.nom + ' ' + $ds028PE3.h.h0.prenom;
cerfaFields['qualite']                         = $ds028PE3.h.h0.qualite;


// Signature
cerfaFields['nomPrenomDeclarant']              = $ds028PE3.signatureGroup.signature.nom + ' ' + $ds028PE3.signatureGroup.signature.prenom;
cerfaFields['qualiteDeclarant']                = $ds028PE3.signatureGroup.signature.qualiteDeclarant;
cerfaFields['telephoneDeclarant']              = $ds028PE3.signatureGroup.signature.telephoneDeclarant;
cerfaFields['emailDeclarant']                  = $ds028PE3.signatureGroup.signature.emailDeclarant;
cerfaFields['lieuSignature']                   = $ds028PE3.signatureGroup.signature.lieuSignature;
cerfaFields['dateSignature']                   = $ds028PE3.signatureGroup.signature.dateSignature;
cerfaFields['signature']                       = "Je déclare sur l’honneur l'exactitude des informations de la formalité et signe la présente déclaration.";




/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $ds028PE3.signatureGroup.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $ds028PE3.signatureGroup.signature.dateSignature,
		autoriteHabilitee :"DIRRECTE",
		demandeContexte : "Bilan pédagogique et financier",
		civiliteNomPrenom : civNomPrenom
	});
	
var cerfaDoc = nash.doc //
    .load('models/Cerfa 10443_14.pdf') //
    .apply(cerfaFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjBilan);


var finalDocItem = finalDoc.save('Organisme_de_formation_professionnelle.pdf');


return spec.create({
    id : 'review',
   label : 'Organisme de formation professionnelle - Bilan pédagogique et financier.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Bilan pédagogique et financier.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});