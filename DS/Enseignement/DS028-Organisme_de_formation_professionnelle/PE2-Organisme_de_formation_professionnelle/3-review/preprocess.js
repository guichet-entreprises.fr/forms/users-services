var cerfaFields = {};

function pad(s) { return (s < 10) ? '0' + s : s; } 
var civNomPrenom = $ds028PE2.idDeclarant.idDeclarantFrance.denominationSocialeFrance;
// var region = $ds028PE2.signatureGroup.signature.regionExercice;

// Siège social
cerfaFields['organismeFrance']                               = true;
cerfaFields['organismeEtranger']                             = false;

// Identification du déclarant FRANCE
cerfaFields['numeroSIRET']                                   = $ds028PE2.idDeclarant.idDeclarantFrance.numeroSIRET;
cerfaFields['codeNAF']                                       = $ds028PE2.idDeclarant.idDeclarantFrance.codeNAF;
cerfaFields['denominationSocialeFrance']                     = $ds028PE2.idDeclarant.idDeclarantFrance.denominationSocialeFrance;
cerfaFields['adresseFranceNumeroNomRue']                     = $ds028PE2.idDeclarant.idDeclarantFrance.adresse.adresseFranceNumeroNomRue;
cerfaFields['adresseFranceComplement']                       = $ds028PE2.idDeclarant.idDeclarantFrance.adresse.adresseFranceComplement;
cerfaFields['adresseFranceCodePostal']                       = $ds028PE2.idDeclarant.idDeclarantFrance.adresse.adresseFranceCodePostal;
cerfaFields['adresseFranceCommune']                          = $ds028PE2.idDeclarant.idDeclarantFrance.adresse.adresseFranceCommune;
cerfaFields['publicationListeTravailGouvAdresseFranceOui']   = ($ds028PE2.idDeclarant.idDeclarantFrance.adresse.ouiNon=='Oui');
cerfaFields['publicationListeTravailGouvAdresseFranceNon']   = ($ds028PE2.idDeclarant.idDeclarantFrance.adresse.ouiNon=='Non');
cerfaFields['adresseFranceTelephone']                        = $ds028PE2.idDeclarant.idDeclarantFrance.adresse.adresseFranceTelephone;
cerfaFields['adresseFranceFax']                              = $ds028PE2.idDeclarant.idDeclarantFrance.adresse.adresseFranceFax;
cerfaFields['adresseFranceEmail']                            = $ds028PE2.idDeclarant.idDeclarantFrance.adresse.adresseFranceEmail;

cerfaFields['denominationSocialeAdressePostale']             = $ds028PE2.idDeclarant.idDeclarantFrance.adresse.idDeclarantFranceAutre.denominationSocialeAdressePostale;
cerfaFields['adressePostaleFranceNumeroNomRue']              = $ds028PE2.idDeclarant.idDeclarantFrance.adresse.idDeclarantFranceAutre.adressePostaleFranceNumeroNomRue;
cerfaFields['adressePostaleFranceComplement']                = $ds028PE2.idDeclarant.idDeclarantFrance.adresse.idDeclarantFranceAutre.adressePostaleFranceComplement;
cerfaFields['adressePostaleFranceCommune']                   = $ds028PE2.idDeclarant.idDeclarantFrance.adresse.idDeclarantFranceAutre.adressePostaleFranceCommune;
cerfaFields['adressePostaleFranceCodePostal']                = $ds028PE2.idDeclarant.idDeclarantFrance.adresse.idDeclarantFranceAutre.adressePostaleFranceCodePostal;

// Identification du déclarant ETRANGER
cerfaFields['denominationSocialeEtranger']                   = $ds028PE2.idDeclarant.idDeclarantEtranger.denominationSocialeEtranger;
cerfaFields['adresseEtrangerNumeroNomRue']                   = $ds028PE2.idDeclarant.idDeclarantEtranger.adresseEtrangerNumeroNomRue;
cerfaFields['adresseEtrangerComplement']                     = $ds028PE2.idDeclarant.idDeclarantEtranger.adresseEtrangerComplement;
cerfaFields['adresseEtrangerCommunePays']                    = $ds028PE2.idDeclarant.idDeclarantEtranger.adresseEtrangerCommune + ' ' + $ds028PE2.idDeclarant.idDeclarantEtranger.adresseEtrangerPays;
cerfaFields['publicationListeTravailGouvAdresseEtrangerOui'] = ($ds028PE2.idDeclarant.idDeclarantEtranger.ouiNon=='Oui');
cerfaFields['publicationListeTravailGouvAdresseEtrangerNon'] = ($ds028PE2.idDeclarant.idDeclarantEtranger.ouiNon=='Non');

// Activité du déclarant
if($ds028PE2.activiteDeclarant.activiteDeclarant0.dateSignatureContrat != null) {
	var dateTemp = new Date(parseInt ($ds028PE2.activiteDeclarant.activiteDeclarant0.dateSignatureContrat.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
	var year = dateTemp.getFullYear();
	cerfaFields['dateSignatureContratJour'] = day;
	cerfaFields['dateSignatureContratMois'] = month;
	cerfaFields['dateSignatureContratAnnee'] = year;
}

if($ds028PE2.activiteDeclarant.activiteDeclarant0.dateSignatureDebutExerciceComptable != null) {
var dateTemp = new Date(parseInt ($ds028PE2.activiteDeclarant.activiteDeclarant0.dateSignatureDebutExerciceComptable.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	cerfaFields['dateSignatureDebutExerciceComptableJour'] = day;
	cerfaFields['dateSignatureDebutExerciceComptableMois'] = month;
	cerfaFields['dateSignatureDebutExerciceComptableAnnee'] = year;
}

if($ds028PE2.activiteDeclarant.activiteDeclarant0.dateSignatureFinExerciceComptable != null) {
var dateTemp = new Date(parseInt ($ds028PE2.activiteDeclarant.activiteDeclarant0.dateSignatureFinExerciceComptable.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	cerfaFields['dateSignatureFinExerciceComptableJour'] = day;
	cerfaFields['dateSignatureFinExerciceComptableMois'] = month;
	cerfaFields['dateSignatureFinExerciceComptableAnnee'] = year;
}

cerfaFields['numeroActivitePrecedente']                      = $ds028PE2.activiteDeclarant.activiteDeclarant0.numeroActivitePrecedente;
cerfaFields['activitePrincipale']                            = $ds028PE2.activiteDeclarant.activiteDeclarant0.activitePrincipale;

// Statut de l'organisme

cerfaFields['statutTravailleurIndependant']                  = ($ds028PE2.statut.statut0.statutPrive=='Travailleur indépendant');
cerfaFields['statutEURL']                                    = ($ds028PE2.statut.statut0.statutPrive=='Entreprise unipersonnel à responsabilité limitée(EURL)');
cerfaFields['statutSARL']                                    = ($ds028PE2.statut.statut0.statutPrive=='Société à responsabilité limitée(SARL)');
cerfaFields['statutSA']                                      = ($ds028PE2.statut.statut0.statutPrive=='Société anonyme(SA)');
cerfaFields['statutSNC']                                     = ($ds028PE2.statut.statut0.statutPrive=='Société en nom collectif(SNC)');
cerfaFields['statutCommanditeSimple']                        = ($ds028PE2.statut.statut0.statutPrive=='Commandite simple');
cerfaFields['statutCommanditeActions']                       = ($ds028PE2.statut.statut0.statutPrive=='Commandite par actions');
cerfaFields['statutSocieteCivile']                           = ($ds028PE2.statut.statut0.statutPrive=='Société civile');
cerfaFields['statutAssociationLoi']                          = ($ds028PE2.statut.statut0.statutPrive=='Association loi de 1901 ou de 1908');
cerfaFields['statutAssociationSyndicale']                    = ($ds028PE2.statut.statut0.statutPrive=='Association syndicale (loi de 1884)');
cerfaFields['statutCooperative']                             = ($ds028PE2.statut.statut0.statutPrive=='Société coopérative');
cerfaFields['statutGIE']                                     = ($ds028PE2.statut.statut0.statutPrive=='Groupement d`intérêt économique');
cerfaFields['statutAutrePrive']                              = ($ds028PE2.statut.statut0.statutPrive=='Autres privés');
cerfaFields['statutAutrePriveLibelle']                       = $ds028PE2.statut.statut0.statutAutrePriveLibelle;

cerfaFields['organismeMinEdu']                               = ($ds028PE2.statut.statut0.statutPublic=='Organismes du ministère en charge de l`éducation nationale');
cerfaFields['greta']                                         = ($ds028PE2.statut.statut0.statutPublic=='Greta');
cerfaFields['horsGreta']                                     = ($ds028PE2.statut.statut0.statutPublic=='Hors Greta');
cerfaFields['superieur']                                     = ($ds028PE2.statut.statut0.statutPublic=='Supérieur');
cerfaFields['cnam']                                          = ($ds028PE2.statut.statut0.statutPublic=='CNAM');
cerfaFields['cnec']                                          = ($ds028PE2.statut.statut0.statutPublic=='CNEC');
cerfaFields['organismeMinSante']                             = ($ds028PE2.statut.statut0.statutPublic=='Organismes du ministère en charge de la santé');
cerfaFields['organismeMinAgriculture']                       = ($ds028PE2.statut.statut0.statutPublic=='Organismes du ministère en charge de l`agriculture');
cerfaFields['cci']                                           = ($ds028PE2.statut.statut0.statutPublic=='Organismes consulaires (CCI)');
cerfaFields['chambreMetiers']                                = ($ds028PE2.statut.statut0.statutPublic=='Organismes consulaires (chambres de métiers)');
cerfaFields['chambreAgriculture']                            = ($ds028PE2.statut.statut0.statutPublic=='Organismes consulaires (chambres d`agriculture)');
cerfaFields['autreOrganismePublic']                          = ($ds028PE2.statut.statut0.statutPublic=='Autres publics');
cerfaFields['autreOrganismePublicLibelle']                   = $ds028PE2.statut.statut0.autreOrganismePublicLibelle;

// Nombre de personnes dispensant des H de formation
cerfaFields['nombreTravailleursIndependants']                = $ds028PE2.nbPersonnes.nbFormateurs.nbFormateursInternes.nombreTravailleursIndependants;
cerfaFields['nombreSalariesCDI']                             = $ds028PE2.nbPersonnes.nbFormateurs.nbFormateursInternes.nombreSalariesCDI;
cerfaFields['nombreSalariesCDD']                             = $ds028PE2.nbPersonnes.nbFormateurs.nbFormateursInternes.nombreSalariesCDD;
cerfaFields['nombreSalariesOccasionnels']                    = $ds028PE2.nbPersonnes.nbFormateurs.nbFormateursInternes.nombreSalariesOccasionnels;
cerfaFields['nombreBenevoles']                               = $ds028PE2.nbPersonnes.nbFormateurs.nbFormateursInternes.nombreBenevoles;
cerfaFields['nombrePersonnesTotal']                          = $ds028PE2.nbPersonnes.nbFormateurs.nbFormateursInternes.nombrePersonnesTotal;
cerfaFields['nombrePersonnesExterieures']                    = $ds028PE2.nbPersonnes.nbFormateurs.nbExternes.nombrePersonnesExterieures;
cerfaFields['nombrePersonnesDispensantHeuresFormation']      = $ds028PE2.nbPersonnes.nbFormateurs.nbtotal.nombrePersonnesDispensantHeuresFormation;

// Spécialité de formation
cerfaFields['specialiteFormation1']                          = $ds028PE2.specialiteFormation.specialiteFormation0.specialiteFormation1;
cerfaFields['specialiteFormation2']                          = $ds028PE2.specialiteFormation.specialiteFormation0.specialiteFormation2;
cerfaFields['specialiteFormation3']                          = $ds028PE2.specialiteFormation.specialiteFormation0.specialiteFormation3;
cerfaFields['specialiteFormation4']                          = $ds028PE2.specialiteFormation.specialiteFormation0.specialiteFormation4;

cerfaFields['precisionFormation']                            = $ds028PE2.specialiteFormation.specialiteFormation0.precisionFormation;
cerfaFields['codeSpecialiteFormation1']                      = $ds028PE1.specialiteFormation.specialiteFormation0.specialiteFormation1.getId();
cerfaFields['codeSpecialiteFormation2']                      = $ds028PE1.specialiteFormation.specialiteFormation0.specialiteFormation2.getId();
cerfaFields['codeSpecialiteFormation3']                      = $ds028PE1.specialiteFormation.specialiteFormation0.specialiteFormation3.getId();
cerfaFields['codeSpecialiteFormation4']                      = $ds028PE1.specialiteFormation.specialiteFormation0.specialiteFormation4.getId();

// Personnes ayant une fonction de direction
cerfaFields['nomPrenomAdministratif1']                       = $ds028PE2.personnes.personnes0.nomPrenomAdministratif1;
cerfaFields['nomPrenomAdministratif2']                       = $ds028PE2.personnes.personnes0.nomPrenomAdministratif2;
cerfaFields['nomPrenomAdministratif3']                       = $ds028PE2.personnes.personnes0.nomPrenomAdministratif3;

// Signature
cerfaFields['nomPrenomSignataire']                           = $ds028PE2.etatCivil.identificationDeclarant.civilite + ' ' + $ds028PE2.etatCivil.identificationDeclarant.nomNaissance + ' ' + $ds028PE2.etatCivil.identificationDeclarant.prenom;
cerfaFields['qualiteSignataire']                             = $ds028PE2.etatCivil.identificationDeclarant.qualiteSignataire;
cerfaFields['lieuSignature']                                 = $ds028PE2.signatureGroup.signature.lieuSignature;
cerfaFields['dateSignature']                                 = $ds028PE2.signatureGroup.signature.dateSignature;
cerfaFields['signatureCoche']                                = $ds028PE2.signatureGroup.signature.signatureCoche;
cerfaFields['signatureTexte']                                = "Je déclare sur l’honneur l'exactitude des informations de la formalité et signe la présente déclaration.";

cerfaFields['libelleProfession']								  	  = "Organisme de formation professionnelle"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $ds028PE2.signatureGroup.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $ds028PE2.signatureGroup.signature.dateSignature,
		autoriteHabilitee :"Préfecture de région",
		demandeContexte : "Déclaration d’activité de prestataire de formation",
		civiliteNomPrenom : civNomPrenom
	});
	
var cerfaDoc = nash.doc //
    .load('models/Cerfa 10782_04.pdf') //
    .apply(cerfaFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjSIREN);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCasierJudiciaire);
appendPj($attachmentPreprocess.attachmentPreprocess.pjConvention);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificatifInscription);
appendPj($attachmentPreprocess.attachmentPreprocess.pjProgrammeFormation);


var finalDocItem = finalDoc.save('Organisme_de_formation_professionnelle.pdf');


return spec.create({
    id : 'review',
   label : 'Organisme de formation professionnelle - déclaration d’activité de prestataire de formation.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration d’activité de prestataire de formation.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});