function pad(s) { return (s < 10) ? '0' + s : s; } 
var cerfaFields = {};


var civNomPrenom = $ds053PE5.etatCivil.identificationDeclarant.civilite + ' ' + $ds053PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $ds053PE5.etatCivil.identificationDeclarant.prenomDeclarant;



cerfaFields['nomNaissanceDeclarant']        = $ds053PE5.etatCivil.identificationDeclarant.nomDeclarant;
cerfaFields['nomEpouseDeclarant']           = $ds053PE5.etatCivil.identificationDeclarant.nomEpouseDeclarant;
cerfaFields['nomUsageDeclarant']            = $ds053PE5.etatCivil.identificationDeclarant.nomUsageDeclarant;
cerfaFields['prenomsDeclarant']             = $ds053PE5.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissanceDeclarant']  = $ds053PE5.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ' ' +$ds053PE5.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']         = $ds053PE5.etatCivil.identificationDeclarant.nationaliteDeclarant;

if($ds053PE5.etatCivil.identificationDeclarant.dateNaissanceDeclarant != null) {
var dateTemp = new Date(parseInt ($ds053PE5.etatCivil.identificationDeclarant.dateNaissanceDeclarant.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
cerfaFields['dateNaissanceDeclarantJour']   = day;
cerfaFields['dateNaissanceDeclarantMois']   = month;
cerfaFields['dateNaissanceDeclarantAnnée']  = year;
}

cerfaFields['numeroNomRueAdresseDeclarant'] = $ds053PE5.adresse.adresseContact.numeroLibelleAdresseDeclarant + 
											 ($ds053PE5.adresse.adresseContact.complementAdresseDeclarant!= null ? ' ' +$ds053PE5.adresse.adresseContact.complementAdresseDeclarant: '');
cerfaFields['communeAdresseDeclarant']      = $ds053PE5.adresse.adresseContact.communeAdresseDeclarant;
cerfaFields['paysAdresseDeclarant']         = $ds053PE5.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['codePostalAdresseDeclarant']   = $ds053PE5.adresse.adresseContact.codePostalAdresseDeclarant;
cerfaFields['faxDeclarant']                 = $ds053PE5.adresse.adresseContact.faxDeclarant;
cerfaFields['courrielDeclarant']            = $ds053PE5.adresse.adresseContact.mailAdresseDeclarant;
cerfaFields['telephoneDeclarant']           = $ds053PE5.adresse.adresseContact.telephoneDeclarant;


cerfaFields['optionClassique']              = $ds053PE5.option1.option2.optionClassique;
cerfaFields['optionContemporaine']          = $ds053PE5.option1.option2.optionContemporaine;
cerfaFields['optionJazz']                   = $ds053PE5.option1.option2.optionJazz;

cerfaFields['dateSignature']                = $ds053PE5.signatureGroup.signature.dateSignature;
cerfaFields['signatureCoche']               = $ds053PE5.signatureGroup.signature.signatureCoche;
cerfaFields['lieuSignature']                = $ds053PE5.signatureGroup.signature.lieuSignature;
cerfaFields['signatureTexte']               = 'Je déclare sur l’honneur l’exactitude des informations de la formalité et signe la présente déclaration.';

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $ds053PE5.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		dateSignature: $ds053PE5.signatureGroup.signature.dateSignature,
		autoriteHabilitee :"Direction générale de la création artistique (DGCA)",
		demandeContexte : "Demande de dispense du diplôme d'état",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Cerfa n°10446_03.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCV);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustifDomicile);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificatifExperience);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Cerfa n°10446_03.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Professeur de danse - demande de dispense du diplôme d\'état',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de dispense du diplôme d\'état',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
