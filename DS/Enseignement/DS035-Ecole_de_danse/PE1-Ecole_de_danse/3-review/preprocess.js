var cerfaFields = {};

var civNomPrenom                                           = ($ds035PE1.identificationDeclarant.identificationDeclarant.declarantNom != null ? $ds035PE1.identificationDeclarant.identificationDeclarant.declarantNom : '')+" "+($ds035PE1.identificationDeclarant.identificationDeclarant.declarantPrenom != null ? $ds035PE1.identificationDeclarant.identificationDeclarant.declarantPrenom : ' ')+" "+($ds035PE1.identificationDeclarant.identificationDeclarant.declarantDenomination != null ? $ds035PE1.identificationDeclarant.identificationDeclarant.declarantDenomination : '')+" ";
var declarantDenom                                         = ($ds035PE1.identificationDeclarant.identificationDeclarant.declarantDenomination != null ? $ds035PE1.identificationDeclarant.identificationDeclarant.declarantDenomination : '')+" ";
var declarantNom                                           = $ds035PE1.identificationDeclarant.identificationDeclarant.declarantNom;
cerfaFields['declarantNomPrenom']                          = ($ds035PE1.identificationDeclarant.identificationDeclarant.declarantNom != null ? $ds035PE1.identificationDeclarant.identificationDeclarant.declarantNom : '')+" "+($ds035PE1.identificationDeclarant.identificationDeclarant.declarantPrenom != null ? $ds035PE1.identificationDeclarant.identificationDeclarant.declarantPrenom : ' ');
cerfaFields['declarantDenomination']                       = ($ds035PE1.identificationDeclarant.identificationDeclarant.declarantDenomination != null ? $ds035PE1.identificationDeclarant.identificationDeclarant.declarantDenomination : ' ');
cerfaFields['declarantAdresse']                            = $ds035PE1.identificationDeclarant.identificationDeclarant.declarantNumLibelle+" "+$ds035PE1.identificationDeclarant.identificationDeclarant.declarantComplementAdresse;
cerfaFields['declarantCodePostal']                         = $ds035PE1.identificationDeclarant.identificationDeclarant.declarantCodePostal;
cerfaFields['declarantCommune']                            = $ds035PE1.identificationDeclarant.identificationDeclarant.declarantCommune;
cerfaFields['declarantPays']                               = $ds035PE1.identificationDeclarant.identificationDeclarant.declarantPays;
cerfaFields['declarantTelephone']                          = $ds035PE1.identificationDeclarant.identificationDeclarant.declarantTelephone;

cerfaFields['localNomPrenomExploitant']                    = ($ds035PE1.caracteristiquesLocal.local.localNomExploitant != null ? $ds035PE1.caracteristiquesLocal.local.localNomExploitant : '')+" "+($ds035PE1.caracteristiquesLocal.local.localPrenomExploitant != null ? $ds035PE1.caracteristiquesLocal.local.localPrenomExploitant : '');
cerfaFields['localDenomination']                           = ($ds035PE1.caracteristiquesLocal.local.localDenomination != null ? $ds035PE1.caracteristiquesLocal.local.localDenomination : '');
cerfaFields['localAdresseNumeroNomRue']                    = $ds035PE1.caracteristiquesLocal.local.localNumLibelle+" "+$ds035PE1.caracteristiquesLocal.local.localComplementAdresse;
cerfaFields['localAdresseCodePostal']                      = $ds035PE1.caracteristiquesLocal.local.localAdresseCodePostal;
cerfaFields['localAdresseCommune']                         = $ds035PE1.caracteristiquesLocal.local.localAdresseCommune;
cerfaFields['localAdressePays']                            = $ds035PE1.caracteristiquesLocal.local.localAdressePays;
cerfaFields['localTelephone']                              = $ds035PE1.caracteristiquesLocal.local.localTelephone;

cerfaFields['localSurfaceTotale']                          = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.localSurfaceTotale;
cerfaFields['localNombreStudios']                          = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.localNombrestudios;









cerfaFields['localSuperficieStudio1']                      = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios1.localSuperficieStudio1;
cerfaFields['localHauteurStudio1']                         = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios1.localHauteurStudio1;
cerfaFields['localEquipementsTechniquesStudio1']           = "hauteur sous faux plafond : "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios1.localHauteurSousFauxPlafond1+"     Gaines ou luminaires : "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios1.localEquipementsTechniques1;
cerfaFields['localLongueurLargeurSolStudio1']              = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios1.localLongueurLargeurSolStudio1;
cerfaFields['localParquetOuiStudio1']                      = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios1.parquet).eq('localParquetOuiStudio1')? true : false;
cerfaFields['localParquetNonStudio1']                      = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios1.parquet).eq('localParquetNonStudio1')? true : false;
cerfaFields['localParquetLatteOuiStudio1']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios1.latte).eq('localParquetLatteOuiStudio1')? true : false;
cerfaFields['localParquetLatteNonStudio1']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios1.latte).eq('localParquetLatteNonStudio1')? true : false;
cerfaFields['localParquetTypeStudio1']                     = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios1.localParquetTypeStudio1;
cerfaFields['localSolPrecisionStudio1']                    = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios1.localSolPrecisionStudio1;
cerfaFields['localSolSupportLambourdageSimpleStudio1']     = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios1.lambourdage).eq('localSolSupportLambourdageSimpleStudio1')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio1']     = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios1.lambourdage).eq('localSolSupportLambourdageDoubleStudio1')? true : false;
cerfaFields['localSolSupportAutreStudio1']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios1.lambourdage).eq('localSolSupportAutreStudio1')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio1']         = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios1.localSolSupportAutrePreciserStudio1;
cerfaFields['localEclairageVentilationIndicationsStudio1'] = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios1.localEclairageIndicationsStudio1+" "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios1.localVentilationIndicationsStudio1;
cerfaFields['localDouchesNombreStudio1']                   = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios1.localDouchesNombreStudio1;
cerfaFields['localCabinetsNombreStudio1']                  = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios1.localCabinetsNombreStudio1;
cerfaFields['localLavabosNombreStudio1']                   = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios1.localLavabosNombreStudio1;


cerfaFields['localSuperficieStudio2']                      = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios2.localSuperficieStudio2;
cerfaFields['localHauteurStudio2']                         = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios2.localHauteurStudio2;
cerfaFields['localEquipementsTechniquesStudio2']           = "hauteur sous faux plafond : "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios2.localHauteurSousFauxPlafond2+"     Gaines ou luminaires : "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios2.localEquipementsTechniques2;
cerfaFields['localLongueurLargeurSolStudio2']              = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios2.localLongueurLargeurSolStudio2;
cerfaFields['localParquetOuiStudio2']                      = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios2.parquet).eq('localParquetOuiStudio2')? true : false;
cerfaFields['localParquetNonStudio2']                      = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios2.parquet).eq('localParquetNonStudio2')? true : false;
cerfaFields['localParquetLatteOuiStudio2']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios2.latte).eq('localParquetLatteOuiStudio2')? true : false;
cerfaFields['localParquetLatteNonStudio2']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios2.latte).eq('localParquetLatteNonStudio2')? true : false;
cerfaFields['localParquetTypeStudio2']                     = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios2.localParquetTypeStudio2;
cerfaFields['localSolPrecisionStudio2']                    = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios2.localSolPrecisionStudio2;
cerfaFields['localSolSupportLambourdageSimpleStudio2']     = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios2.lambourdage).eq('localSolSupportLambourdageSimpleStudio2')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio2']     = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios2.lambourdage).eq('localSolSupportLambourdageDoubleStudio2')? true : false;
cerfaFields['localSolSupportAutreStudio2']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios2.lambourdage).eq('localSolSupportAutreStudio2')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio2']         = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios2.localSolSupportAutrePreciserStudio2;
cerfaFields['localEclairageVentilationIndicationsStudio2'] = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios2.localEclairageIndicationsStudio2+" "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios2.localVentilationIndicationsStudio2;
cerfaFields['localDouchesNombreStudio2']                   = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios2.localDouchesNombreStudio2;
cerfaFields['localCabinetsNombreStudio2']                  = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios2.localCabinetsNombreStudio2;
cerfaFields['localLavabosNombreStudio2']                   = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios2.localLavabosNombreStudio2;


cerfaFields['localSuperficieStudio3']                      = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios3.localSuperficieStudio3;
cerfaFields['localHauteurStudio3']                         = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios3.localHauteurStudio3;
cerfaFields['localEquipementsTechniquesStudio3']           = "hauteur sous faux plafond : "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios3.localHauteurSousFauxPlafond3+"     Gaines ou luminaires : "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios3.localEquipementsTechniques3;
cerfaFields['localLongueurLargeurSolStudio3']              = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios3.localLongueurLargeurSolStudio3;
cerfaFields['localParquetOuiStudio3']                      = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios3.parquet).eq('localParquetOuiStudio3')? true : false;
cerfaFields['localParquetNonStudio3']                      = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios3.parquet).eq('localParquetNonStudio3')? true : false;
cerfaFields['localParquetLatteOuiStudio3']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios3.latte).eq('localParquetLatteOuiStudio3')? true : false;
cerfaFields['localParquetLatteNonStudio3']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios3.latte).eq('localParquetLatteNonStudio3')? true : false;
cerfaFields['localParquetTypeStudio3']                     = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios3.localParquetTypeStudio3;
cerfaFields['localSolPrecisionStudio3']                    = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios3.localSolPrecisionStudio3;
cerfaFields['localSolSupportLambourdageSimpleStudio3']     = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios3.lambourdage).eq('localSolSupportLambourdageSimpleStudio3')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio3']     = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios3.lambourdage).eq('localSolSupportLambourdageDoubleStudio3')? true : false;
cerfaFields['localSolSupportAutreStudio3']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios3.lambourdage).eq('localSolSupportAutreStudio3')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio3']         = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios3.localSolSupportAutrePreciserStudio3;
cerfaFields['localEclairageVentilationIndicationsStudio3'] = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios3.localEclairageIndicationsStudio3+" "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios3.localVentilationIndicationsStudio3;
cerfaFields['localDouchesNombreStudio3']                   = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios3.localDouchesNombreStudio3;
cerfaFields['localCabinetsNombreStudio3']                  = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios3.localCabinetsNombreStudio3;
cerfaFields['localLavabosNombreStudio3']                   = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios3.localLavabosNombreStudio3;


cerfaFields['localSuperficieStudio4']                      = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios4.localSuperficieStudio4;
cerfaFields['localHauteurStudio4']                         = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios4.localHauteurStudio4;
cerfaFields['localEquipementsTechniquesStudio4']           = "hauteur sous faux plafond : "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios4.localHauteurSousFauxPlafond4+"     Gaines ou luminaires : "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios4.localEquipementsTechniques4;
cerfaFields['localLongueurLargeurSolStudio4']              = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios4.localLongueurLargeurSolStudio4;
cerfaFields['localParquetOuiStudio4']                      = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios4.parquet).eq('localParquetOuiStudio4')? true : false;
cerfaFields['localParquetNonStudio4']                      = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios4.parquet).eq('localParquetNonStudio4')? true : false;
cerfaFields['localParquetLatteOuiStudio4']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios4.latte).eq('localParquetLatteOuiStudio4')? true : false;
cerfaFields['localParquetLatteNonStudio4']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios4.latte).eq('localParquetLatteNonStudio4')? true : false;
cerfaFields['localParquetTypeStudio4']                     = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios4.localParquetTypeStudio4;
cerfaFields['localSolPrecisionStudio4']                    = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios4.localSolPrecisionStudio4;
cerfaFields['localSolSupportLambourdageSimpleStudio4']     = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios4.lambourdage).eq('localSolSupportLambourdageSimpleStudio4')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio4']     = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios4.lambourdage).eq('localSolSupportLambourdageDoubleStudio4')? true : false;
cerfaFields['localSolSupportAutreStudio4']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios4.lambourdage).eq('localSolSupportAutreStudio4')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio4']         = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios4.localSolSupportAutrePreciserStudio4;
cerfaFields['localEclairageVentilationIndicationsStudio4'] = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios4.localEclairageIndicationsStudio4+" "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios4.localVentilationIndicationsStudio4;
cerfaFields['localDouchesNombreStudio4']                   = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios4.localDouchesNombreStudio4;
cerfaFields['localCabinetsNombreStudio4']                  = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios4.localCabinetsNombreStudio4;
cerfaFields['localLavabosNombreStudio4']                   = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios4.localLavabosNombreStudio4;


cerfaFields['localSuperficieStudio5']                      = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios5.localSuperficieStudio5;
cerfaFields['localHauteurStudio5']                         = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios5.localHauteurStudio5;
cerfaFields['localEquipementsTechniquesStudio5']           = "hauteur sous faux plafond : "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios5.localHauteurSousFauxPlafond5+"     Gaines ou luminaires : "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios5.localEquipementsTechniques5;
cerfaFields['localLongueurLargeurSolStudio5']              = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios5.localLongueurLargeurSolStudio5;
cerfaFields['localParquetOuiStudio5']                      = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios5.parquet).eq('localParquetOuiStudio5')? true : false;
cerfaFields['localParquetNonStudio5']                      = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios5.parquet).eq('localParquetNonStudio5')? true : false;
cerfaFields['localParquetLatteOuiStudio5']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios5.latte).eq('localParquetLatteOuiStudio5')? true : false;
cerfaFields['localParquetLatteNonStudio5']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios5.latte).eq('localParquetLatteNonStudio5')? true : false;
cerfaFields['localParquetTypeStudio5']                     = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios5.localParquetTypeStudio5;
cerfaFields['localSolPrecisionStudio5']                    = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios5.localSolPrecisionStudio5;
cerfaFields['localSolSupportLambourdageSimpleStudio5']     = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios5.lambourdage).eq('localSolSupportLambourdageSimpleStudio5')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio5']     = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios5.lambourdage).eq('localSolSupportLambourdageDoubleStudio5')? true : false;
cerfaFields['localSolSupportAutreStudio5']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios5.lambourdage).eq('localSolSupportAutreStudio5')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio5']         = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios5.localSolSupportAutrePreciserStudio5;
cerfaFields['localEclairageVentilationIndicationsStudio5'] = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios5.localEclairageIndicationsStudio5+" "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios5.localVentilationIndicationsStudio5;
cerfaFields['localDouchesNombreStudio5']                   = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios5.localDouchesNombreStudio5;
cerfaFields['localCabinetsNombreStudio5']                  = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios5.localCabinetsNombreStudio5;
cerfaFields['localLavabosNombreStudio5']                   = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios5.localLavabosNombreStudio5;



cerfaFields['localSuperficieStudio6']                      = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios6.localSuperficieStudio6;
cerfaFields['localHauteurStudio6']                         = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios6.localHauteurStudio6;
cerfaFields['localEquipementsTechniquesStudio6']           = "hauteur sous faux plafond : "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios6.localHauteurSousFauxPlafond6+"     Gaines ou luminaires : "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios6.localEquipementsTechniques6;
cerfaFields['localLongueurLargeurSolStudio6']              = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios6.localLongueurLargeurSolStudio6;
cerfaFields['localParquetOuiStudio6']                      = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios6.parquet).eq('localParquetOuiStudio6')? true : false;
cerfaFields['localParquetNonStudio6']                      = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios6.parquet).eq('localParquetNonStudio6')? true : false;
cerfaFields['localParquetLatteOuiStudio6']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios6.latte).eq('localParquetLatteOuiStudio6')? true : false;
cerfaFields['localParquetLatteNonStudio6']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios6.latte).eq('localParquetLatteNonStudio6')? true : false;
cerfaFields['localParquetTypeStudio6']                     = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios6.localParquetTypeStudio6;
cerfaFields['localSolPrecisionStudio6']                    = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios6.localSolPrecisionStudio6;
cerfaFields['localSolSupportLambourdageSimpleStudio6']     = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios6.lambourdage).eq('localSolSupportLambourdageSimpleStudio6')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio6']     = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios6.lambourdage).eq('localSolSupportLambourdageDoubleStudio6')? true : false;
cerfaFields['localSolSupportAutreStudio6']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios6.lambourdage).eq('localSolSupportAutreStudio6')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio6']         = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios6.localSolSupportAutrePreciserStudio6;
cerfaFields['localEclairageVentilationIndicationsStudio6']  = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios6.localEclairageIndicationsStudio6+" "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios6.localVentilationIndicationsStudio6;
cerfaFields['localDouchesNombreStudio6']                   = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios6.localDouchesNombreStudio6;
cerfaFields['localCabinetsNombreStudio6']                  = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios6.localCabinetsNombreStudio6;
cerfaFields['localLavabosNombreStudio6']                   = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios6.localLavabosNombreStudio6;


cerfaFields['localSuperficieStudio7']                      = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios7.localSuperficieStudio7;
cerfaFields['localHauteurStudio7']                         = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios7.localHauteurStudio7;
cerfaFields['localEquipementsTechniquesStudio7']           = "hauteur sous faux plafond : "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios7.localHauteurSousFauxPlafond7+"     Gaines ou luminaires : "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios7.localEquipementsTechniques7;
cerfaFields['localLongueurLargeurSolStudio7']              = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios7.localLongueurLargeurSolStudio7;
cerfaFields['localParquetOuiStudio7']                      = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios7.parquet).eq('localParquetOuiStudio7')? true : false;
cerfaFields['localParquetNonStudio7']                      = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios7.parquet).eq('localParquetNonStudio7')? true : false;
cerfaFields['localParquetLatteOuiStudio7']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios7.latte).eq('localParquetLatteOuiStudio7')? true : false;
cerfaFields['localParquetLatteNonStudio7']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios7.latte).eq('localParquetLatteNonStudio7')? true : false;
cerfaFields['localParquetTypeStudio7']                     = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios7.localParquetTypeStudio7;
cerfaFields['localSolPrecisionStudio7']                    = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios7.localSolPrecisionStudio7;
cerfaFields['localSolSupportLambourdageSimpleStudio7']     = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios7.lambourdage).eq('localSolSupportLambourdageSimpleStudio7')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio7']     = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios7.lambourdage).eq('localSolSupportLambourdageDoubleStudio7')? true : false;
cerfaFields['localSolSupportAutreStudio7']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios7.lambourdage).eq('localSolSupportAutreStudio7')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio7']         = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios7.localSolSupportAutrePreciserStudio7;
cerfaFields['localEclairageVentilationIndicationsStudio7'] = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios7.localEclairageIndicationsStudio7+" "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios7.localVentilationIndicationsStudio7;
cerfaFields['localDouchesNombreStudio7']                   = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios7.localDouchesNombreStudio7;
cerfaFields['localCabinetsNombreStudio7']                  = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios7.localCabinetsNombreStudio7;
cerfaFields['localLavabosNombreStudio7']                   = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios7.localLavabosNombreStudio7;


cerfaFields['localSuperficieStudio8']                      = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios8.localSuperficieStudio8;
cerfaFields['localHauteurStudio8']                         = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios8.localHauteurStudio8;
cerfaFields['localEquipementsTechniquesStudio8']           = "hauteur sous faux plafond : "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios8.localHauteurSousFauxPlafond8+"     Gaines ou luminaires : "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios8.localEquipementsTechniques8;
cerfaFields['localLongueurLargeurSolStudio8']              = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios8.localLongueurLargeurSolStudio8;
cerfaFields['localParquetOuiStudio8']                      = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios8.parquet).eq('localParquetOuiStudio8')? true : false;
cerfaFields['localParquetNonStudio8']                      = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios8.parquet).eq('localParquetNonStudio8')? true : false;
cerfaFields['localParquetLatteOuiStudio8']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios8.latte).eq('localParquetLatteOuiStudio8')? true : false;
cerfaFields['localParquetLatteNonStudio8']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios8.latte).eq('localParquetLatteNonStudio8')? true : false;
cerfaFields['localParquetTypeStudio8']                     = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios8.localParquetTypeStudio8;
cerfaFields['localSolPrecisionStudio8']                    = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios8.localSolPrecisionStudio8;
cerfaFields['localSolSupportLambourdageSimpleStudio8']     = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios8.lambourdage).eq('localSolSupportLambourdageSimpleStudio8')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio8']     = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios8.lambourdage).eq('localSolSupportLambourdageDoubleStudio8')? true : false;
cerfaFields['localSolSupportAutreStudio8']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios8.lambourdage).eq('localSolSupportAutreStudio8')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio8']         = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios8.localSolSupportAutrePreciserStudio8;
cerfaFields['localEclairageVentilationIndicationsStudio8'] = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios8.localEclairageIndicationsStudio8+" "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios8.localVentilationIndicationsStudio8;
cerfaFields['localDouchesNombreStudio8']                   = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios8.localDouchesNombreStudio8;
cerfaFields['localCabinetsNombreStudio8']                  = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios8.localCabinetsNombreStudio8;
cerfaFields['localLavabosNombreStudio8']                   = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios8.localLavabosNombreStudio8;


cerfaFields['localSuperficieStudio9']                      = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios9.localSuperficieStudio9;
cerfaFields['localHauteurStudio9']                         = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios9.localHauteurStudio9;
cerfaFields['localEquipementsTechniquesStudio9']           = "hauteur sous faux plafond : "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios9.localHauteurSousFauxPlafond9+"     Gaines ou luminaires : "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios9.localEquipementsTechniques9;
cerfaFields['localLongueurLargeurSolStudio9']              = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios9.localLongueurLargeurSolStudio9;
cerfaFields['localParquetOuiStudio9']                      = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios9.parquet).eq('localParquetOuiStudio9')? true : false;
cerfaFields['localParquetNonStudio9']                      = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios9.parquet).eq('localParquetNonStudio9')? true : false;
cerfaFields['localParquetLatteOuiStudio9']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios9.latte).eq('localParquetLatteOuiStudio9')? true : false;
cerfaFields['localParquetLatteNonStudio9']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios9.latte).eq('localParquetLatteNonStudio9')? true : false;
cerfaFields['localParquetTypeStudio9']                     = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios9.localParquetTypeStudio9;
cerfaFields['localSolPrecisionStudio9']                    = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios9.localSolPrecisionStudio9;
cerfaFields['localSolSupportLambourdageSimpleStudio9']     = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios9.lambourdage).eq('localSolSupportLambourdageSimpleStudio9')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio9']     = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios9.lambourdage).eq('localSolSupportLambourdageDoubleStudio9')? true : false;
cerfaFields['localSolSupportAutreStudio9']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios9.lambourdage).eq('localSolSupportAutreStudio9')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio9']         = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios9.localSolSupportAutrePreciserStudio9;
cerfaFields['localEclairageVentilationIndicationsStudio9'] = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios9.localEclairageIndicationsStudio9+" "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios9.localVentilationIndicationsStudio9;
cerfaFields['localDouchesNombreStudio9']                   = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios9.localDouchesNombreStudio9;
cerfaFields['localCabinetsNombreStudio9']                  = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios9.localCabinetsNombreStudio9;
cerfaFields['localLavabosNombreStudio9']                   = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios9.localLavabosNombreStudio9;


cerfaFields['localSuperficieStudio10']                      = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios10.localSuperficieStudio10;
cerfaFields['localHauteurStudio10']                         = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios10.localHauteurStudio10;
cerfaFields['localEquipementsTechniquesStudio10']           = "hauteur sous faux plafond : "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios10.localHauteurSousFauxPlafond10+"     Gaines ou luminaires : "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios10.localEquipementsTechniques10;
cerfaFields['localLongueurLargeurSolStudio10']              = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios10.localLongueurLargeurSolStudio10;
cerfaFields['localParquetOuiStudio10']                      = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios10.parquet).eq('localParquetOuiStudio10')? true : false;
cerfaFields['localParquetNonStudio10']                      = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios10.parquet).eq('localParquetNonStudio10')? true : false;
cerfaFields['localParquetLatteOuiStudio10']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios10.latte).eq('localParquetLatteOuiStudio10')? true : false;
cerfaFields['localParquetLatteNonStudio10']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios10.latte).eq('localParquetLatteNonStudio10')? true : false;
cerfaFields['localParquetTypeStudio10']                     = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios10.localParquetTypeStudio10;
cerfaFields['localSolPrecisionStudio10']                    = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios10.localSolPrecisionStudio10;
cerfaFields['localSolSupportLambourdageSimpleStudio10']     = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios10.lambourdage).eq('localSolSupportLambourdageSimpleStudio10')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio10']     = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios10.lambourdage).eq('localSolSupportLambourdageDoubleStudio10')? true : false;
cerfaFields['localSolSupportAutreStudio10']                 = Value('id').of($ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios10.lambourdage).eq('localSolSupportAutreStudio10')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio10']         = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios10.localSolSupportAutrePreciserStudio10;
cerfaFields['localEclairageVentilationIndicationsStudio10'] = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios10.localEclairageIndicationsStudio10+" "+$ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios10.localVentilationIndicationsStudio10;
cerfaFields['localDouchesNombreStudio10']                   = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios10.localDouchesNombreStudio10;
cerfaFields['localCabinetsNombreStudio10']                  = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios10.localCabinetsNombreStudio10;
cerfaFields['localLavabosNombreStudio10']                   = $ds035PE1.caracteristiquesLocal.caracteristiquesLocal.studios10.localLavabosNombreStudio10;






cerfaFields['nombreIndicatifEleves']                       = $ds035PE1.caracteristiquesLocal.nombreIndicatifEleves.nombreIndicatifEleves;

cerfaFields['typeDanseClassique']                          = $ds035PE1.caracteristiquesLocal.typeDanse.typeDanseClassique;
cerfaFields['typeDanseJazz']                               = $ds035PE1.caracteristiquesLocal.typeDanse.typeDanseJazz;
cerfaFields['typeDanseContemporaine']                      = $ds035PE1.caracteristiquesLocal.typeDanse.typeDanseContemporaine;
cerfaFields['typeDanseDeSociete']                          = $ds035PE1.caracteristiquesLocal.typeDanse.typeDanseDeSociete;
cerfaFields['typeDanseTraditionnelles']                    = $ds035PE1.caracteristiquesLocal.typeDanse.typeDanseTraditionnelles;
cerfaFields['typeDanseAutre']                              = $ds035PE1.caracteristiquesLocal.typeDanse.typeDanseAutre;
cerfaFields['typeDanseAutreNom']                              = $ds035PE1.caracteristiquesLocal.typeDanse.typeDanseAutreNom;
cerfaFields['datePrevueOuvertureLocal']                    = $ds035PE1.caracteristiquesLocal.datePrevueOuvertureLocal.datePrevueOuvertureLocal;



cerfaFields['signatureLieu']                               = $ds035PE1.signature.signature.lieuSignature;
cerfaFields['signature']                                   = $ds035PE1.signature.signature.signature;
cerfaFields['signatureDate']                               = $ds035PE1.signature.signature.dateSignature;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $ds035PE1.signature.signature.dateSignature,
		
		
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		dateSignature: $ds035PE1.signature.signature.dateSignature,
		autoriteHabilitee :"Direction régionale des affaires culturelles (DRAC)",
		demandeContexte : "Ecole de Danse - Déclaration d'ouverture",
		civiliteNomPrenom: civNomPrenom,
		declarantDenomination : declarantDenom,
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/cerfa 10452-03.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));

var cerfaDoc = nash.doc //
	.load('models/Cerfa 10452-03-Annexe.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));


function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjattestationResponsabiliteCivile);
appendPj($attachmentPreprocess.attachmentPreprocess.pjcopiePlanLocal);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Ecole de danse_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Ecole de danse - Demande d\'ouverture',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Ecole de danse - Demande d\'ouverture',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
