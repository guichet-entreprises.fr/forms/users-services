var cerfaFields = {};

var civNomPrenom                                           = ($ds035PE3.identificationDeclarant.identificationDeclarant.declarantNom != null ?  $ds035PE3.identificationDeclarant.identificationDeclarant.declarantNom : '')+" "+($ds035PE3.identificationDeclarant.identificationDeclarant.declarantPrenom != null ? $ds035PE3.identificationDeclarant.identificationDeclarant.declarantPrenom : '')+" "+($ds035PE3.identificationDeclarant.identificationDeclarant.declarantDenomination != null ?  $ds035PE3.identificationDeclarant.identificationDeclarant.declarantDenomination : '')+" ";
var declarantDenom                                         = ($ds035PE3.identificationDeclarant.identificationDeclarant.declarantDenomination != null ? $ds035PE3.identificationDeclarant.identificationDeclarant.declarantDenomination : '')+" ";
var declarantNom                                           = $ds035PE3.identificationDeclarant.identificationDeclarant.declarantNom;
cerfaFields['declarantNomPrenom']                          = ($ds035PE3.identificationDeclarant.identificationDeclarant.declarantNom != null ?  $ds035PE3.identificationDeclarant.identificationDeclarant.declarantNom : '')+" "+($ds035PE3.identificationDeclarant.identificationDeclarant.declarantPrenom != null ? $ds035PE3.identificationDeclarant.identificationDeclarant.declarantPrenom : '')+" ";
cerfaFields['declarantDenomination']                       = ($ds035PE3.identificationDeclarant.identificationDeclarant.declarantDenomination != null ?  $ds035PE3.identificationDeclarant.identificationDeclarant.declarantDenomination : '')+" ";
cerfaFields['declarantAdresse']                            = $ds035PE3.identificationDeclarant.identificationDeclarant.declarantNumLibelle+" "+$ds035PE3.identificationDeclarant.identificationDeclarant.declarantComplementAdresse;
cerfaFields['declarantCodePostal']                         = $ds035PE3.identificationDeclarant.identificationDeclarant.declarantCodePostal;
cerfaFields['declarantCommune']                            = $ds035PE3.identificationDeclarant.identificationDeclarant.declarantCommune;
cerfaFields['declarantPays']                               = $ds035PE3.identificationDeclarant.identificationDeclarant.declarantPays;
cerfaFields['declarantTelephone']                          = $ds035PE3.identificationDeclarant.identificationDeclarant.declarantTelephone;

cerfaFields['localNomPrenomExploitant']                    = ($ds035PE3.caracteristiquesLocal.local.localNomExploitant != null ? $ds035PE3.caracteristiquesLocal.local.localNomExploitant : '')+" "+($ds035PE3.caracteristiquesLocal.local.localPrenomExploitant != null ? $ds035PE3.caracteristiquesLocal.local.localPrenomExploitant :'');
cerfaFields['localDenomination']                           = ($ds035PE3.caracteristiquesLocal.local.localDenomination != null ? $ds035PE3.caracteristiquesLocal.local.localDenomination : '');
cerfaFields['localAdresseNumeroNomRue']                    = $ds035PE3.caracteristiquesLocal.local.localNumLibelle+" "+$ds035PE3.caracteristiquesLocal.local.localComplementAdresse;
cerfaFields['localAdresseCodePostal']                      = $ds035PE3.caracteristiquesLocal.local.localAdresseCodePostal;
cerfaFields['localAdresseCommune']                         = $ds035PE3.caracteristiquesLocal.local.localAdresseCommune;
cerfaFields['localAdressePays']                            = $ds035PE3.caracteristiquesLocal.local.localAdressePays;
cerfaFields['localTelephone']                              = $ds035PE3.caracteristiquesLocal.local.localTelephone;

cerfaFields['localSurfaceTotale']                          = $ds035PE3.caracteristiquesLocal.caracteristiquesLocal.localSurfaceTotale+" "+"m² ";
cerfaFields['localNombreStudios']                          = $ds035PE3.caracteristiquesLocal.caracteristiquesLocal.localNombreStudios;
cerfaFields['localSurfaceChaqueStudio']                    = ($ds035PE3.caracteristiquesLocal.caracteristiquesLocal.studios.studios1 != null ? '1: '+ $ds035PE3.caracteristiquesLocal.caracteristiquesLocal.studios.studios1 +'m², ' : '')+" "+($ds035PE3.caracteristiquesLocal.caracteristiquesLocal.studios.studios2 != null ? '2: '+ $ds035PE3.caracteristiquesLocal.caracteristiquesLocal.studios.studios2 +'m², ' : '')+" "+($ds035PE3.caracteristiquesLocal.caracteristiquesLocal.studios.studios3 != null ? '3: '+ $ds035PE3.caracteristiquesLocal.caracteristiquesLocal.studios.studios3 +'m², ' : '')+" "+($ds035PE3.caracteristiquesLocal.caracteristiquesLocal.studios.studios4 != null ? '4: '+ $ds035PE3.caracteristiquesLocal.caracteristiquesLocal.studios.studios4 +'m², ' : '')+" "+($ds035PE3.caracteristiquesLocal.caracteristiquesLocal.studios.studios5 != null ? '5: '+ $ds035PE3.caracteristiquesLocal.caracteristiquesLocal.studios.studios5 +'m², ' : '')+" "+($ds035PE3.caracteristiquesLocal.caracteristiquesLocal.studios.studios6 != null ? '6: '+ $ds035PE3.caracteristiquesLocal.caracteristiquesLocal.studios.studios6 +'m², ' : '')+" "+($ds035PE3.caracteristiquesLocal.caracteristiquesLocal.studios.studios7 != null ? '7: '+ $ds035PE3.caracteristiquesLocal.caracteristiquesLocal.studios.studios7 +'m², ' : '')+" "+($ds035PE3.caracteristiquesLocal.caracteristiquesLocal.studios.studios8 != null ? '8: '+ $ds035PE3.caracteristiquesLocal.caracteristiquesLocal.studios.studios8 +'m², ' : '')+" "+($ds035PE3.caracteristiquesLocal.caracteristiquesLocal.studios.studios9 != null ? '9: '+ $ds035PE3.caracteristiquesLocal.caracteristiquesLocal.studios.studios9 +'m², ' : '')+" "+($ds035PE3.caracteristiquesLocal.caracteristiquesLocal.studios.studios10 != null ? '10: '+ $ds035PE3.caracteristiquesLocal.caracteristiquesLocal.studios.studios10 +'m² ' : '')+" ";


cerfaFields['signatureLieu']                               = $ds035PE3.signature.signature.lieuSignature;
cerfaFields['signature']                                   = $ds035PE3.signature.signature.signature;
cerfaFields['signatureDate']                               = $ds035PE3.signature.signature.dateSignature;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $ds035PE3.signature.signature.dateSignature,
		
		
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		dateSignature: $ds035PE3.signature.signature.dateSignature,
		autoriteHabilitee :"Direction régionale des affaires culturelles (DRAC)",
		demandeContexte : "Ecole de Danse - Déclaration d'ouverture",
		civiliteNomPrenom: civNomPrenom,
		declarantDenomination : declarantDenom,
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Cerfa_10453_03.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));

// var cerfaDoc = nash.doc //
//	.load('models/Cerfa 10452-03-Annexe.pdf') //
//	.apply(cerfaFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));


function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);



/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Ecole de danse_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Ecole de danse - Déclaration de fermeture',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Ecole de danse - Déclaration de fermeture',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
