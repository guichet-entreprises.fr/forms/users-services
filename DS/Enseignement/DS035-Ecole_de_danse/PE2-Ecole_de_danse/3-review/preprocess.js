var cerfaFields = {};

var civNomPrenom                                                      = ($ds035PE2.identificationDeclarant.identificationDeclarant.declarantNom != null ? $ds035PE2.identificationDeclarant.identificationDeclarant.declarantNom : ' ')+" "+($ds035PE2.identificationDeclarant.identificationDeclarant.declarantPrenom != null ? $ds035PE2.identificationDeclarant.identificationDeclarant.declarantPrenom : ' ')+" "+($ds035PE2.identificationDeclarant.identificationDeclarant.declarantDenomination !=null ? $ds035PE2.identificationDeclarant.identificationDeclarant.declarantDenomination : ' ');
var declarantDenom                                                    = ($ds035PE2.identificationDeclarant.identificationDeclarant.declarantDenomination != null ? $ds035PE2.identificationDeclarant.identificationDeclarant.declarantDenomination : '')+" ";
var declarantNom                                                      = $ds035PE2.identificationDeclarant.identificationDeclarant.declarantNom;
cerfaFields['declarantNomPrenom']                                     = ($ds035PE2.identificationDeclarant.identificationDeclarant.declarantNom != null ? $ds035PE2.identificationDeclarant.identificationDeclarant.declarantNom : ' ')+" "+($ds035PE2.identificationDeclarant.identificationDeclarant.declarantPrenom != null ? $ds035PE2.identificationDeclarant.identificationDeclarant.declarantPrenom : ' ');
cerfaFields['declarantDenomination']                                  = ($ds035PE2.identificationDeclarant.identificationDeclarant.declarantDenomination !=null ? $ds035PE2.identificationDeclarant.identificationDeclarant.declarantDenomination : ' ');
cerfaFields['declarantAdresse']                                       = $ds035PE2.identificationDeclarant.identificationDeclarant.declarantNumLibelle+" "+$ds035PE2.identificationDeclarant.identificationDeclarant.declarantComplementAdresse;
cerfaFields['declarantCodePostal']                                    = $ds035PE2.identificationDeclarant.identificationDeclarant.declarantCodePostal;
cerfaFields['declarantCommune']                                       = $ds035PE2.identificationDeclarant.identificationDeclarant.declarantCommune;
cerfaFields['declarantPays']                                          = $ds035PE2.identificationDeclarant.identificationDeclarant.declarantPays;
cerfaFields['declarantTelephone']                                     = $ds035PE2.identificationDeclarant.identificationDeclarant.declarantTelephone;

cerfaFields['localNomPrenomExploitant']                               = ($ds035PE2.caracteristiquesLocal.local.localNomExploitant != null ? $ds035PE2.caracteristiquesLocal.local.localNomExploitant : '')+" "+($ds035PE2.caracteristiquesLocal.local.localPrenomExploitant != null ? $ds035PE2.caracteristiquesLocal.local.localPrenomExploitant : '');
cerfaFields['localDenomination']                                      = ($ds035PE2.caracteristiquesLocal.local.localDenomination != null ? $ds035PE2.caracteristiquesLocal.local.localDenomination : '');
cerfaFields['localAdresseNumeroNomRue']                               = $ds035PE2.caracteristiquesLocal.local.localNumLibelle+" "+$ds035PE2.caracteristiquesLocal.local.localComplementAdresse;
cerfaFields['localAdresseCodePostal']                                 = $ds035PE2.caracteristiquesLocal.local.localAdresseCodePostal;
cerfaFields['localAdresseCommune']                                    = $ds035PE2.caracteristiquesLocal.local.localAdresseCommune;
cerfaFields['localAdressePays']                                       = $ds035PE2.caracteristiquesLocal.local.localAdressePays;
cerfaFields['localTelephone']                                         = $ds035PE2.caracteristiquesLocal.local.localTelephone;

cerfaFields['localSurfaceTotale']                                     = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.localSurfaceTotale;
cerfaFields['localNombreStudios']                                     = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.localNombreStudios;



cerfaFields['localSuperficieStudio1']                                 = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios1Avant.localSuperficieStudio1Avant;
cerfaFields['localHauteurStudio1']                                    = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios1Avant.localHauteurStudio1Avant;
cerfaFields['localEquipementsTechniquesStudio1']           		      = "hauteur sous faux plafond :"+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios1Avant.localHauteurSousFauxPlafond1Avant+"  Gaines ou luminaires :"+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios1Avant.localEquipementsTechniques1Avant;
cerfaFields['localParquetOuiStudio1']                    	          = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios1Avant.parquetAvant).eq('localParquetOuiStudio1Avant')? true : false;
cerfaFields['localParquetNonStudio1']                    	          = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios1Avant.parquetAvant).eq('localParquetNonStudio1Avant')? true : false;
cerfaFields['localParquetLatteOuiStudio1']               	          = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios1Avant.latteAvant).eq('localParquetLatteOuiStudio1Avant')? true : false;
cerfaFields['localParquetLatteNonStudio1']                            = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios1Avant.latteAvant).eq('localParquetLatteNonStudio1Avant')? true : false;
cerfaFields['localParquetTypeStudio1']                                = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios1Avant.localParquetTypeStudio1Avant;
cerfaFields['localSolPrecisionStudio1']                               = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios1Avant.localSolPrecisionStudio1Avant;
cerfaFields['localSolSupportLambourdageSimpleStudio1']                = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios1Avant.lambourdageAvant).eq('localSolSupportLambourdageSimpleStudio1Avant')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio1']                = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios1Avant.lambourdageAvant).eq('localSolSupportLambourdageDoubleStudio1Avant')? true : false;
cerfaFields['localSolSupportAutreStudio1']                            = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios1Avant.lambourdageAvant).eq('localSolSupportAutreStudio1Avant')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio1']                    = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios1Avant.localSolSupportAutrePreciserStudio1Avant;
cerfaFields['localEclairageVentilationIndicationsStudio1']            = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios1Avant.localEclairageIndicationsStudio1Avant+" "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios1Avant.localVentilationIndicationsStudio1Avant;
cerfaFields['localDouchesNombreStudio1']                              = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios1Avant.localDouchesNombreStudio1Avant;
cerfaFields['localCabinetsNombreStudio1']                             = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios1Avant.localCabinetsNombreStudio1Avant;
cerfaFields['localLavabosNombreStudio1']                              = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios1Avant.localLavabosNombreStudio1Avant;




cerfaFields['localSuperficieStudio2Avant']                           = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios2Avant.localSuperficieStudio2Avant;
cerfaFields['localHauteurStudio2Avant']                              = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios2Avant.localHauteurStudio2Avant;
cerfaFields['localEquipementsTechniquesStudio2Avant']                = "hauteur sous faux plafond :"+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios2Avant.localHauteurSousFauxPlafond2Avant+"  Gaines ou luminaires : "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios2Avant.localEquipementsTechniques2Avant;

cerfaFields['localParquetOuiStudio2Avant']                           = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios2Avant.parquetAvant).eq('localParquetOuiStudio2Avant')? true : false;
cerfaFields['localParquetNonStudio2Avant']                           = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios2Avant.parquetAvant).eq('localParquetNonStudio2Avant')? true : false;
cerfaFields['localParquetLatteOuiStudio2Avant']                      = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios2Avant.latteAvant).eq('localParquetLatteOuiStudio2Avant')? true : false;
cerfaFields['localParquetLatteNonStudio2Avant']                      = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios2Avant.latteAvant).eq('localParquetLatteNonStudio2Avant')? true : false;
cerfaFields['localParquetTypeStudio2Avant']                          = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios2Avant.localParquetTypeStudio2Avant;
cerfaFields['localSolPrecisionStudio2Avant']                         = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios2Avant.localSolPrecisionStudio2Avant;
cerfaFields['localSolSupportLambourdageSimpleStudio2Avant']          = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios2Avant.lambourdageAvant).eq('localSolSupportLambourdageSimpleStudio2Avant')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio2Avant']          = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios2Avant.lambourdageAvant).eq('localSolSupportLambourdageDoubleStudio2Avant')? true : false;
cerfaFields['localSolSupportAutreStudio2Avant']                      = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios2Avant.lambourdageAvant).eq('localSolSupportAutreStudio2Avant')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio2Avant']              = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios2Avant.localSolSupportAutrePreciserStudio2Avant;
cerfaFields['localEclairageVentilationIndicationsStudio2Avant']      = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios2Avant.localEclairageIndicationsStudio2Avant+" "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios2Avant.localVentilationIndicationsStudio2Avant;
cerfaFields['localDouchesNombreStudio2Avant']                        = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios2Avant.localDouchesNombreStudio2Avant;
cerfaFields['localCabinetsNombreStudio2Avant']                       = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios2Avant.localCabinetsNombreStudio2Avant;
cerfaFields['localLavabosNombreStudio2Avant']                        = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios2Avant.localLavabosNombreStudio2Avant;


cerfaFields['localSuperficieStudio3Avant']                          = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios3Avant.localSuperficieStudio3Avant;
cerfaFields['localHauteurStudio3Avant']                             = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios3Avant.localHauteurStudio3Avant;
cerfaFields['localEquipementsTechniquesStudio3Avant']               = "hauteur sous faux plafond :"+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios3Avant.localHauteurSousFauxPlafond3Avant+"  Gaines ou luminaires : "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios3Avant.localEquipementsTechniques3Avant;

cerfaFields['localParquetOuiStudio3Avant']                          = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios3Avant.parquetAvant).eq('localParquetOuiStudio3Avant')? true : false;
cerfaFields['localParquetNonStudio3Avant']                          = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios3Avant.parquetAvant).eq('localParquetNonStudio3Avant')? true : false;
cerfaFields['localParquetLatteOuiStudio3Avant']                     = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios3Avant.latteAvant).eq('localParquetLatteOuiStudio3Avant')? true : false;
cerfaFields['localParquetLatteNonStudio3Avant']                     = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios3Avant.latteAvant).eq('localParquetLatteNonStudio3Avant')? true : false;
cerfaFields['localParquetTypeStudio3Avant']                         = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios3Avant.localParquetTypeStudio3Avant;
cerfaFields['localSolPrecisionStudio3Avant']                        = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios3Avant.localSolPrecisionStudio3Avant;
cerfaFields['localSolSupportLambourdageSimpleStudio3Avant']         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios3Avant.lambourdageAvant).eq('localSolSupportLambourdageSimpleStudio3Avant')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio3Avant']         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios3Avant.lambourdageAvant).eq('localSolSupportLambourdageDoubleStudio3Avant')? true : false;
cerfaFields['localSolSupportAutreStudio3Avant']                     = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios3Avant.lambourdageAvant).eq('localSolSupportAutreStudio3Avant')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio3Avant']             = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios3Avant.localSolSupportAutrePreciserStudio3Avant;
cerfaFields['localEclairageVentilationIndicationsStudio3Avant']     = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios3Avant.localEclairageIndicationsStudio3Avant+" "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios3Avant.localVentilationIndicationsStudio3Avant;
cerfaFields['localDouchesNombreStudio3Avant']                       = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios3Avant.localDouchesNombreStudio3Avant;
cerfaFields['localCabinetsNombreStudio3Avant']                      = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios3Avant.localCabinetsNombreStudio3Avant;
cerfaFields['localLavabosNombreStudio3Avant']                       = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios3Avant.localLavabosNombreStudio3Avant;


cerfaFields['localSuperficieStudio4Avant']                          = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios4Avant.localSuperficieStudio4Avant;
cerfaFields['localHauteurStudio4Avant']                             = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios4Avant.localHauteurStudio4Avant;
cerfaFields['localEquipementsTechniquesStudio4Avant']               = "hauteur sous faux plafond :"+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios4Avant.localHauteurSousFauxPlafond4Avant+"  Gaines ou luminaires : "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios4Avant.localEquipementsTechniques4Avant;

cerfaFields['localParquetOuiStudio4Avant']                          = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios4Avant.parquetAvant).eq('localParquetOuiStudio4Avant')? true : false;
cerfaFields['localParquetNonStudio4Avant']                          = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios4Avant.parquetAvant).eq('localParquetNonStudio4Avant')? true : false;
cerfaFields['localParquetLatteOuiStudio4Avant']                     = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios4Avant.latteAvant).eq('localParquetLatteOuiStudio4Avant')? true : false;
cerfaFields['localParquetLatteNonStudio4Avant']                     = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios4Avant.latteAvant).eq('localParquetLatteNonStudio4Avant')? true : false;
cerfaFields['localParquetTypeStudio4Avant']                         = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios4Avant.localParquetTypeStudio4Avant;
cerfaFields['localSolPrecisionStudio4Avant']                        = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios4Avant.localSolPrecisionStudio4Avant;
cerfaFields['localSolSupportLambourdageSimpleStudio4Avant']         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios4Avant.lambourdageAvant).eq('localSolSupportLambourdageSimpleStudio4Avant')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio4Avant']         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios4Avant.lambourdageAvant).eq('localSolSupportLambourdageDoubleStudio4Avant')? true : false;
cerfaFields['localSolSupportAutreStudio4Avant']                     = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios4Avant.lambourdageAvant).eq('localSolSupportAutreStudio4Avant')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio4Avant']       	    = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios4Avant.localSolSupportAutrePreciserStudio4Avant;
cerfaFields['localEclairageVentilationIndicationsStudio4Avant']     = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios4Avant.localEclairageIndicationsStudio4Avant+" "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios4Avant.localVentilationIndicationsStudio4Avant;
cerfaFields['localDouchesNombreStudio4Avant']                       = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios4Avant.localDouchesNombreStudio4Avant;
cerfaFields['localCabinetsNombreStudio4Avant']                      = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios4Avant.localCabinetsNombreStudio4Avant;
cerfaFields['localLavabosNombreStudio4Avant']                       = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios4Avant.localLavabosNombreStudio4Avant;


cerfaFields['localSuperficieStudio5Avant']                          = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios5Avant.localSuperficieStudio5Avant;
cerfaFields['localHauteurStudio5Avant']                             = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios5Avant.localHauteurStudio5Avant;
cerfaFields['localEquipementsTechniquesStudio5Avant']               = "hauteur sous faux plafond :"+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios5Avant.localHauteurSousFauxPlafond5Avant+"  Gaines ou luminaires : "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios5Avant.localEquipementsTechniques5Avant;

cerfaFields['localParquetOuiStudio5Avant']                          = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios5Avant.parquetAvant).eq('localParquetOuiStudio5Avant')? true : false;
cerfaFields['localParquetNonStudio5Avant']                          = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios5Avant.parquetAvant).eq('localParquetNonStudio5Avant')? true : false;
cerfaFields['localParquetLatteOuiStudio5Avant']                     = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios5Avant.latteAvant).eq('localParquetLatteOuiStudio5Avant')? true : false;
cerfaFields['localParquetLatteNonStudio5Avant']                     = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios5Avant.latteAvant).eq('localParquetLatteNonStudio5Avant')? true : false;
cerfaFields['localParquetTypeStudio5Avant']                         = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios5Avant.localParquetTypeStudio5Avant;
cerfaFields['localSolPrecisionStudio5Avant']                        = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios5Avant.localSolPrecisionStudio5Avant;
cerfaFields['localSolSupportLambourdageSimpleStudio5Avant']         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios5Avant.lambourdageAvant).eq('localSolSupportLambourdageSimpleStudio5Avant')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio5Avant']         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios5Avant.lambourdageAvant).eq('localSolSupportLambourdageDoubleStudio5Avant')? true : false;
cerfaFields['localSolSupportAutreStudio5Avant']                     = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios5Avant.lambourdageAvant).eq('localSolSupportAutreStudio5Avant')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio5Avant']             = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios5Avant.localSolSupportAutrePreciserStudio5Avant;
cerfaFields['localEclairageVentilationIndicationsStudio5Avant']     = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios5Avant.localEclairageIndicationsStudio5Avant+" "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios5Avant.localVentilationIndicationsStudio5Avant;
cerfaFields['localDouchesNombreStudio5Avant']                       = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios5Avant.localDouchesNombreStudio5Avant;
cerfaFields['localCabinetsNombreStudio5Avant']                      = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios5Avant.localCabinetsNombreStudio5Avant;
cerfaFields['localLavabosNombreStudio5Avant']                       = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios5Avant.localLavabosNombreStudio5Avant;



cerfaFields['localSuperficieStudio6Avant']                          = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios6Avant.localSuperficieStudio6Avant;
cerfaFields['localHauteurStudio6Avant']                             = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios6Avant.localHauteurStudio6Avant;
cerfaFields['localEquipementsTechniquesStudio6Avant']               = "hauteur sous faux plafond :"+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios6Avant.localHauteurSousFauxPlafond6Avant+"  Gaines ou luminaires : "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios6Avant.localEquipementsTechniques6Avant;

cerfaFields['localParquetOuiStudio6Avant']                          = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios6Avant.parquetAvant).eq('localParquetOuiStudio6Avant')? true : false;
cerfaFields['localParquetNonStudio6Avant']                          = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios6Avant.parquetAvant).eq('localParquetNonStudio6Avant')? true : false;
cerfaFields['localParquetLatteOuiStudio6Avant']                     = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios6Avant.latteAvant).eq('localParquetLatteOuiStudio6Avant')? true : false;
cerfaFields['localParquetLatteNonStudio6Avant']                     = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios6Avant.latteAvant).eq('localParquetLatteNonStudio6Avant')? true : false;
cerfaFields['localParquetTypeStudio6Avant']                         = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios6Avant.localParquetTypeStudio6Avant;
cerfaFields['localSolPrecisionStudio6Avant']                        = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios6Avant.localSolPrecisionStudio6Avant;
cerfaFields['localSolSupportLambourdageSimpleStudio6Avant']         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios6Avant.lambourdageAvant).eq('localSolSupportLambourdageSimpleStudio6Avant')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio6Avant']         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios6Avant.lambourdageAvant).eq('localSolSupportLambourdageDoubleStudio6Avant')? true : false;
cerfaFields['localSolSupportAutreStudio6Avant']                     = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios6Avant.lambourdageAvant).eq('localSolSupportAutreStudio6Avant')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio6Avant']             = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios6Avant.localSolSupportAutrePreciserStudio6Avant;
cerfaFields['localEclairageVentilationIndicationsStudio6Avant']     = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios6Avant.localEclairageIndicationsStudio6Avant+" "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios6Avant.localVentilationIndicationsStudio6Avant;
cerfaFields['localDouchesNombreStudio6Avant']                       = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios6Avant.localDouchesNombreStudio6Avant;
cerfaFields['localCabinetsNombreStudio6Avant']                      = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios6Avant.localCabinetsNombreStudio6Avant;
cerfaFields['localLavabosNombreStudio6Avant']                       = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios6Avant.localLavabosNombreStudio6Avant;


cerfaFields['localSuperficieStudio7Avant']                          = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios7Avant.localSuperficieStudio7Avant;
cerfaFields['localHauteurStudio7Avant']                             = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios7Avant.localHauteurStudio7Avant;
cerfaFields['localEquipementsTechniquesStudio7Avant']               = "hauteur sous faux plafond :"+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios7Avant.localHauteurSousFauxPlafond7Avant+"  Gaines ou luminaires : "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios7Avant.localEquipementsTechniques7Avant;

cerfaFields['localParquetOuiStudio7Avant']                          = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios7Avant.parquetAvant).eq('localParquetOuiStudio7Avant')? true : false;
cerfaFields['localParquetNonStudio7Avant']                          = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios7Avant.parquetAvant).eq('localParquetNonStudio7Avant')? true : false;
cerfaFields['localParquetLatteOuiStudio7Avant']                     = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios7Avant.latteAvant).eq('localParquetLatteOuiStudio7Avant')? true : false;
cerfaFields['localParquetLatteNonStudio7Avant']                     = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios7Avant.latteAvant).eq('localParquetLatteNonStudio7Avant')? true : false;
cerfaFields['localParquetTypeStudio7Avant']                         = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios7Avant.localParquetTypeStudio7Avant;
cerfaFields['localSolPrecisionStudio7Avant']                        = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios7Avant.localSolPrecisionStudio7Avant;
cerfaFields['localSolSupportLambourdageSimpleStudio7Avant']         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios7Avant.lambourdageAvant).eq('localSolSupportLambourdageSimpleStudio7Avant')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio7Avant']         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios7Avant.lambourdageAvant).eq('localSolSupportLambourdageDoubleStudio7Avant')? true : false;
cerfaFields['localSolSupportAutreStudio7Avant']                     = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios7Avant.lambourdageAvant).eq('localSolSupportAutreStudio7Avant')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio7Avant']             = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios7Avant.localSolSupportAutrePreciserStudio7Avant;
cerfaFields['localEclairageVentilationIndicationsStudio7Avant']     = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios7Avant.localEclairageIndicationsStudio7Avant+" "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios7Avant.localVentilationIndicationsStudio7Avant;
cerfaFields['localDouchesNombreStudio7Avant']                       = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios7Avant.localDouchesNombreStudio7Avant;
cerfaFields['localCabinetsNombreStudio7Avant']                      = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios7Avant.localCabinetsNombreStudio7Avant;
cerfaFields['localLavabosNombreStudio7Avant']                       = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios7Avant.localLavabosNombreStudio7Avant;


cerfaFields['localSuperficieStudio8Avant']                          = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios8Avant.localSuperficieStudio8Avant;
cerfaFields['localHauteurStudio8Avant']                             = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios8Avant.localHauteurStudio8Avant;
cerfaFields['localEquipementsTechniquesStudio8Avant']               = "hauteur sous faux plafond :"+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios8Avant.localHauteurSousFauxPlafond8Avant+"  Gaines ou luminaires : "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios8Avant.localEquipementsTechniques8Avant;

cerfaFields['localParquetOuiStudio8Avant']                          = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios8Avant.parquetAvant).eq('localParquetOuiStudio8Avant')? true : false;
cerfaFields['localParquetNonStudio8Avant']                          = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios8Avant.parquetAvant).eq('localParquetNonStudio8Avant')? true : false;
cerfaFields['localParquetLatteOuiStudio8Avant']                     = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios8Avant.latteAvant).eq('localParquetLatteOuiStudio8Avant')? true : false;
cerfaFields['localParquetLatteNonStudio8Avant']                     = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios8Avant.latteAvant).eq('localParquetLatteNonStudio8Avant')? true : false;
cerfaFields['localParquetTypeStudio8Avant']                         = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios8Avant.localParquetTypeStudio8Avant;
cerfaFields['localSolPrecisionStudio8Avant']                        = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios8Avant.localSolPrecisionStudio8Avant;
cerfaFields['localSolSupportLambourdageSimpleStudio8Avant']         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios8Avant.lambourdageAvant).eq('localSolSupportLambourdageSimpleStudio8Avant')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio8Avant']         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios8Avant.lambourdageAvant).eq('localSolSupportLambourdageDoubleStudio8Avant')? true : false;
cerfaFields['localSolSupportAutreStudio8Avant']                     = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios8Avant.lambourdageAvant).eq('localSolSupportAutreStudio8Avant')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio8Avant']             = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios8Avant.localSolSupportAutrePreciserStudio8Avant;
cerfaFields['localEclairageVentilationIndicationsStudio8Avant']     = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios8Avant.localEclairageIndicationsStudio8Avant+" "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios8Avant.localVentilationIndicationsStudio8Avant;
cerfaFields['localDouchesNombreStudio8Avant']                       = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios8Avant.localDouchesNombreStudio8Avant;
cerfaFields['localCabinetsNombreStudio8Avant']                      = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios8Avant.localCabinetsNombreStudio8Avant;
cerfaFields['localLavabosNombreStudio8Avant']                       = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios8Avant.localLavabosNombreStudio8Avant;


cerfaFields['localSuperficieStudio9Avant']                          = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios9Avant.localSuperficieStudio9Avant;
cerfaFields['localHauteurStudio9Avant']                             = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios9Avant.localHauteurStudio9Avant;
cerfaFields['localEquipementsTechniquesStudio9Avant']               = "hauteur sous faux plafond :"+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios9Avant.localHauteurSousFauxPlafond9Avant+"  Gaines ou luminaires : "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios9Avant.localEquipementsTechniques9Avant;

cerfaFields['localParquetOuiStudio9Avant']                          = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios9Avant.parquetAvant).eq('localParquetOuiStudio9Avant')? true : false;
cerfaFields['localParquetNonStudio9Avant']                          = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios9Avant.parquetAvant).eq('localParquetNonStudio9Avant')? true : false;
cerfaFields['localParquetLatteOuiStudio9Avant']                     = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios9Avant.latteAvant).eq('localParquetLatteOuiStudio9Avant')? true : false;
cerfaFields['localParquetLatteNonStudio9Avant']                     = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios9Avant.latteAvant).eq('localParquetLatteNonStudio9Avant')? true : false;
cerfaFields['localParquetTypeStudio9Avant']                         = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios9Avant.localParquetTypeStudio9Avant;
cerfaFields['localSolPrecisionStudio9Avant']                        = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios9Avant.localSolPrecisionStudio9Avant;
cerfaFields['localSolSupportLambourdageSimpleStudio9Avant']         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios9Avant.lambourdageAvant).eq('localSolSupportLambourdageSimpleStudio9Avant')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio9Avant']         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios9Avant.lambourdageAvant).eq('localSolSupportLambourdageDoubleStudio9Avant')? true : false;
cerfaFields['localSolSupportAutreStudio9Avant']                     = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios9Avant.lambourdageAvant).eq('localSolSupportAutreStudio9Avant')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio9Avant']             = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios9Avant.localSolSupportAutrePreciserStudio9Avant;
cerfaFields['localEclairageVentilationIndicationsStudio9Avant']     = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios9Avant.localEclairageIndicationsStudio9Avant+" "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios9Avant.localVentilationIndicationsStudio9Avant;
cerfaFields['localDouchesNombreStudio9Avant']                       = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios9Avant.localDouchesNombreStudio9Avant;
cerfaFields['localCabinetsNombreStudio9Avant']                      = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios9Avant.localCabinetsNombreStudio9Avant;
cerfaFields['localLavabosNombreStudio9Avant']                       = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios9Avant.localLavabosNombreStudio9Avant;


cerfaFields['localSuperficieStudio10Avant']                      = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios10Avant.localSuperficieStudio10Avant;
cerfaFields['localHauteurStudio10Avant']                         = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios10Avant.localHauteurStudio10Avant;
cerfaFields['localEquipementsTechniquesStudio10Avant']           = "hauteur sous faux plafond :"+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios10Avant.localHauteurSousFauxPlafond10Avant+"  Gaines ou luminaires : "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios10Avant.localEquipementsTechniques10Avant;

cerfaFields['localParquetOuiStudio10Avant']                      = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios10Avant.parquetAvant).eq('localParquetOuiStudio10Avant')? true : false;
cerfaFields['localParquetNonStudio10Avant']                      = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios10Avant.parquetAvant).eq('localParquetNonStudio10Avant')? true : false;
cerfaFields['localParquetLatteOuiStudio10Avant']                 = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios10Avant.latteAvant).eq('localParquetLatteOuiStudio10Avant')? true : false;
cerfaFields['localParquetLatteNonStudio10Avant']                 = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios10Avant.latteAvant).eq('localParquetLatteNonStudio10Avant')? true : false;
cerfaFields['localParquetTypeStudio10Avant']                     = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios10Avant.localParquetTypeStudio10Avant;
cerfaFields['localSolPrecisionStudio10Avant']                    = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios10Avant.localSolPrecisionStudio10Avant;
cerfaFields['localSolSupportLambourdageSimpleStudio10Avant']     = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios10Avant.lambourdageAvant).eq('localSolSupportLambourdageSimpleStudio10Avant')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio10Avant']     = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios10Avant.lambourdageAvant).eq('localSolSupportLambourdageDoubleStudio10Avant')? true : false;
cerfaFields['localSolSupportAutreStudio10Avant']                 = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios10Avant.lambourdageAvant).eq('localSolSupportAutreStudio10Avant')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio10Avant']         = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios10Avant.localSolSupportAutrePreciserStudio10Avant;
cerfaFields['localEclairageVentilationIndicationsStudio10Avant'] = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios10Avant.localEclairageIndicationsStudio10Avant+" "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios10Avant.localVentilationIndicationsStudio10Avant;
cerfaFields['localDouchesNombreStudio10Avant']                   = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios10Avant.localDouchesNombreStudio10Avant;
cerfaFields['localCabinetsNombreStudio10Avant']                  = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios10Avant.localCabinetsNombreStudio10Avant;
cerfaFields['localLavabosNombreStudio10Avant']                   = $ds035PE2.caracteristiquesLocal.caracteristiquesLocal.studios10Avant.localLavabosNombreStudio10Avant;









cerfaFields['localSurfaceTotaleApres']                             = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.localSurfaceTotaleApres;
cerfaFields['localNombreStudiosApres']                             = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.localNombrestudiosApres;



cerfaFields['localSuperficieStudio1Apres']                              = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios1Apres.localSuperficieStudio1Apres;
cerfaFields['localHauteurStudio1Apres']                                 = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios1Apres.localHauteurStudio1Apres;
cerfaFields['localEquipementsTechniquesStudio1Apres']                   = "hauteur sous faux plafond :"+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios1Apres.localHauteurSousFauxPlafond1Apres+"  Gaines ou luminaires : "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios1Apres.localEquipementsTechniques1Apres;

cerfaFields['localParquetOuiStudio1Apres']                              = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios1Apres.parquetApres).eq('localParquetOuiStudio1Apres')? true : false;
cerfaFields['localParquetNonStudio1Apres']                              = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios1Apres.parquetApres).eq('localParquetNonStudio1Apres')? true : false;
cerfaFields['localParquetLatteOuiStudio1Apres']                         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios1Apres.latteApres).eq('localParquetLatteOuiStudio1Apres')? true : false;
cerfaFields['localParquetLatteNonStudio1Apres']                         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios1Apres.latteApres).eq('localParquetLatteNonStudio1Apres')? true : false;
cerfaFields['localParquetTypeStudio1Apres']                             = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios1Apres.localParquetTypeStudio1Apres;
cerfaFields['localSolPrecisionStudio1Apres']                            = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios1Apres.localSolPrecisionStudio1Apres;
cerfaFields['localSolSupportLambourdageSimpleStudio1Apres']             = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios1Apres.lambourdageApres).eq('localSolSupportLambourdageSimpleStudio1Apres')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio1Apres']             = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios1Apres.lambourdageApres).eq('localSolSupportLambourdageDoubleStudio1Apres')? true : false;
cerfaFields['localSolSupportAutreStudio1Apres']                         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios1Apres.lambourdageApres).eq('localSolSupportAutreStudio1Apres')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio1Apres']                 = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios1Apres.localSolSupportAutrePreciserStudio1Apres;
cerfaFields['localEclairageVentilationIndicationsStudio1Apres']         = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios1Apres.localEclairageIndicationsStudio1Apres+" "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios1Apres.localVentilationIndicationsStudio1Apres;
cerfaFields['localDouchesNombreStudio1Apres']                           = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios1Apres.localDouchesNombreStudio1Apres;
cerfaFields['localCabinetsNombreStudio1Apres']                          = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios1Apres.localCabinetsNombreStudio1Apres;
cerfaFields['localLavabosNombreStudio1Apres']                           = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios1Apres.localLavabosNombreStudio1Apres;




cerfaFields['localSuperficieStudio2Apres']                         = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios2Apres.localSuperficieStudio2Apres;
cerfaFields['localHauteurStudio2Apres']                            = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios2Apres.localHauteurStudio2Apres;
cerfaFields['localEquipementsTechniquesStudio2Apres']              = "hauteur sous faux plafond :"+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios2Apres.localHauteurSousFauxPlafond2Apres+"  Gaines ou luminaires : "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios2Apres.localEquipementsTechniques2Apres;

cerfaFields['localParquetOuiStudio2Apres']                         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios2Apres.parquetApres).eq('localParquetOuiStudio2Apres')? true : false;
cerfaFields['localParquetNonStudio2Apres']                         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios2Apres.parquetApres).eq('localParquetNonStudio2Apres')? true : false;
cerfaFields['localParquetLatteOuiStudio2Apres']                    = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios2Apres.latteApres).eq('localParquetLatteOuiStudio2Apres')? true : false;
cerfaFields['localParquetLatteNonStudio2Apres']                    = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios2Apres.latteApres).eq('localParquetLatteNonStudio2Apres')? true : false;
cerfaFields['localParquetTypeStudio2Apres']                        = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios2Apres.localParquetTypeStudio2Apres;
cerfaFields['localSolPrecisionStudio2Apres']                       = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios2Apres.localSolPrecisionStudio2Apres;
cerfaFields['localSolSupportLambourdageSimpleStudio2Apres']        = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios2Apres.lambourdageApres).eq('localSolSupportLambourdageSimpleStudio2Apres')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio2Apres']        = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios2Apres.lambourdageApres).eq('localSolSupportLambourdageDoubleStudio2Apres')? true : false;
cerfaFields['localSolSupportAutreStudio2Apres']                    = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios2Apres.lambourdageApres).eq('localSolSupportAutreStudio2Apres')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio2Apres']            = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios2Apres.localSolSupportAutrePreciserStudio2Apres;
cerfaFields['localEclairageVentilationIndicationsStudio2Apres']    = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios2Apres.localEclairageIndicationsStudio2Apres+" "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios2Apres.localVentilationIndicationsStudio2Apres;
cerfaFields['localDouchesNombreStudio2Apres']                           = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios2Apres.localDouchesNombreStudio2Apres;
cerfaFields['localCabinetsNombreStudio2Apres']                          = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios2Apres.localCabinetsNombreStudio2Apres;
cerfaFields['localLavabosNombreStudio2Apres']                           = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios2Apres.localLavabosNombreStudio2Apres;


cerfaFields['localSuperficieStudio3Apres']                         = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios3Apres.localSuperficieStudio3Apres;
cerfaFields['localHauteurStudio3Apres']                            = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios3Apres.localHauteurStudio3Apres;
cerfaFields['localEquipementsTechniquesStudio3Apres']              = "hauteur sous faux plafond :"+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios3Apres.localHauteurSousFauxPlafond3Apres+"  Gaines ou luminaires : "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios3Apres.localEquipementsTechniques3Apres;

cerfaFields['localParquetOuiStudio3Apres']                         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios3Apres.parquetApres).eq('localParquetOuiStudio3Apres')? true : false;
cerfaFields['localParquetNonStudio3Apres']                         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios3Apres.parquetApres).eq('localParquetNonStudio3Apres')? true : false;
cerfaFields['localParquetLatteOuiStudio3Apres']                    = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios3Apres.latteApres).eq('localParquetLatteOuiStudio3Apres')? true : false;
cerfaFields['localParquetLatteNonStudio3Apres']                    = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios3Apres.latteApres).eq('localParquetLatteNonStudio3Apres')? true : false;
cerfaFields['localParquetTypeStudio3Apres']                        = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios3Apres.localParquetTypeStudio3Apres;
cerfaFields['localSolPrecisionStudio3Apres']                       = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios3Apres.localSolPrecisionStudio3Apres;
cerfaFields['localSolSupportLambourdageSimpleStudio3Apres']        = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios3Apres.lambourdageApres).eq('localSolSupportLambourdageSimpleStudio3Apres')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio3Apres']        = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios3Apres.lambourdageApres).eq('localSolSupportLambourdageDoubleStudio3Apres')? true : false;
cerfaFields['localSolSupportAutreStudio3Apres']                    = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios3Apres.lambourdageApres).eq('localSolSupportAutreStudio3Apres')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio3Apres']            = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios3Apres.localSolSupportAutrePreciserStudio3Apres;
cerfaFields['localEclairageVentilationIndicationsStudio3Apres']    = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios3Apres.localEclairageIndicationsStudio3Apres+" "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios3Apres.localVentilationIndicationsStudio3Apres;
cerfaFields['localDouchesNombreStudio3Apres']                      = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios3Apres.localDouchesNombreStudio3Apres;
cerfaFields['localCabinetsNombreStudio3Apres']                     = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios3Apres.localCabinetsNombreStudio3Apres;
cerfaFields['localLavabosNombreStudio3Apres']                      = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios3Apres.localLavabosNombreStudio3Apres;


cerfaFields['localSuperficieStudio4Apres']                         = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios4Apres.localSuperficieStudio4Apres;
cerfaFields['localHauteurStudio4Apres']                            = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios4Apres.localHauteurStudio4Apres;
cerfaFields['localEquipementsTechniquesStudio4Apres']              = "hauteur sous faux plafond :"+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios4Apres.localHauteurSousFauxPlafond4Apres+"  Gaines ou luminaires : "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios4Apres.localEquipementsTechniques4Apres;

cerfaFields['localParquetOuiStudio4Apres']                         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios4Apres.parquetApres).eq('localParquetOuiStudio4Apres')? true : false;
cerfaFields['localParquetNonStudio4Apres']                         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios4Apres.parquetApres).eq('localParquetNonStudio4Apres')? true : false;
cerfaFields['localParquetLatteOuiStudio4Apres']                    = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios4Apres.latteApres).eq('localParquetLatteOuiStudio4Apres')? true : false;
cerfaFields['localParquetLatteNonStudio4Apres']                    = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios4Apres.latteApres).eq('localParquetLatteNonStudio4Apres')? true : false;
cerfaFields['localParquetTypeStudio4Apres']                        = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios4Apres.localParquetTypeStudio4Apres;
cerfaFields['localSolPrecisionStudio4Apres']                       = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios4Apres.localSolPrecisionStudio4Apres;
cerfaFields['localSolSupportLambourdageSimpleStudio4Apres']        = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios4Apres.lambourdageApres).eq('localSolSupportLambourdageSimpleStudio4Apres')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio4Apres']        = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios4Apres.lambourdageApres).eq('localSolSupportLambourdageDoubleStudio4Apres')? true : false;
cerfaFields['localSolSupportAutreStudio4Apres']                    = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios4Apres.lambourdageApres).eq('localSolSupportAutreStudio4Apres')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio4Apres']            = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios4Apres.localSolSupportAutrePreciserStudio4Apres;
cerfaFields['localEclairageVentilationIndicationsStudio4Apres']    = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios4Apres.localEclairageIndicationsStudio4Apres+" "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios4Apres.localVentilationIndicationsStudio4Apres;
cerfaFields['localDouchesNombreStudio4Apres']                      = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios4Apres.localDouchesNombreStudio4Apres;
cerfaFields['localCabinetsNombreStudio4Apres']                     = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios4Apres.localCabinetsNombreStudio4Apres;
cerfaFields['localLavabosNombreStudio4Apres']                      = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios4Apres.localLavabosNombreStudio4Apres;


cerfaFields['localSuperficieStudio5Apres']                         = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios5Apres.localSuperficieStudio5Apres;
cerfaFields['localHauteurStudio5Apres']                            = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios5Apres.localHauteurStudio5Apres;
cerfaFields['localEquipementsTechniquesStudio5Apres']              = "hauteur sous faux plafond :"+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios5Apres.localHauteurSousFauxPlafond5Apres+"  Gaines ou luminaires : "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios5Apres.localEquipementsTechniques5Apres;

cerfaFields['localParquetOuiStudio5Apres']                         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios5Apres.parquetApres).eq('localParquetOuiStudio5Apres')? true : false;
cerfaFields['localParquetNonStudio5Apres']                         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios5Apres.parquetApres).eq('localParquetNonStudio5Apres')? true : false;
cerfaFields['localParquetLatteOuiStudio5Apres']                    = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios5Apres.latteApres).eq('localParquetLatteOuiStudio5Apres')? true : false;
cerfaFields['localParquetLatteNonStudio5Apres']                    = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios5Apres.latteApres).eq('localParquetLatteNonStudio5Apres')? true : false;
cerfaFields['localParquetTypeStudio5Apres']                        = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios5Apres.localParquetTypeStudio5Apres;
cerfaFields['localSolPrecisionStudio5Apres']                       = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios5Apres.localSolPrecisionStudio5Apres;
cerfaFields['localSolSupportLambourdageSimpleStudio5Apres']        = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios5Apres.lambourdageApres).eq('localSolSupportLambourdageSimpleStudio5Apres')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio5Apres']        = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios5Apres.lambourdageApres).eq('localSolSupportLambourdageDoubleStudio5Apres')? true : false;
cerfaFields['localSolSupportAutreStudio5Apres']               	   = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios5Apres.lambourdageApres).eq('localSolSupportAutreStudio5Apres')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio5Apres']            = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios5Apres.localSolSupportAutrePreciserStudio5Apres;
cerfaFields['localEclairageVentilationIndicationsStudio5Apres']    = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios5Apres.localEclairageIndicationsStudio5Apres+" "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios5Apres.localVentilationIndicationsStudio5Apres;
cerfaFields['localDouchesNombreStudio5Apres']                      = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios5Apres.localDouchesNombreStudio5Apres;
cerfaFields['localCabinetsNombreStudio5Apres']                     = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios5Apres.localCabinetsNombreStudio5Apres;
cerfaFields['localLavabosNombreStudio5Apres']                      = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios5Apres.localLavabosNombreStudio5Apres;



cerfaFields['localSuperficieStudio6Apres']                         = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios6Apres.localSuperficieStudio6Apres;
cerfaFields['localHauteurStudio6Apres']                            = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios6Apres.localHauteurStudio6Apres;
cerfaFields['localEquipementsTechniquesStudio6Apres']              = "hauteur sous faux plafond :"+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios6Apres.localHauteurSousFauxPlafond6Apres+"  Gaines ou luminaires : "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios6Apres.localEquipementsTechniques6Apres;

cerfaFields['localParquetOuiStudio6Apres']                         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios6Apres.parquetApres).eq('localParquetOuiStudio6Apres')? true : false;
cerfaFields['localParquetNonStudio6Apres']                         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios6Apres.parquetApres).eq('localParquetNonStudio6Apres')? true : false;
cerfaFields['localParquetLatteOuiStudio6Apres']                    = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios6Apres.latteApres).eq('localParquetLatteOuiStudio6Apres')? true : false;
cerfaFields['localParquetLatteNonStudio6Apres']                    = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios6Apres.latteApres).eq('localParquetLatteNonStudio6Apres')? true : false;
cerfaFields['localParquetTypeStudio6Apres']                        = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios6Apres.localParquetTypeStudio6Apres;
cerfaFields['localSolPrecisionStudio6Apres']                       = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios6Apres.localSolPrecisionStudio6Apres;
cerfaFields['localSolSupportLambourdageSimpleStudio6Apres']        = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios6Apres.lambourdageApres).eq('localSolSupportLambourdageSimpleStudio6Apres')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio6Apres']        = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios6Apres.lambourdageApres).eq('localSolSupportLambourdageDoubleStudio6Apres')? true : false;
cerfaFields['localSolSupportAutreStudio6Apres']                    = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios6Apres.lambourdageApres).eq('localSolSupportAutreStudio6Apres')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio6Apres']            = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios6Apres.localSolSupportAutrePreciserStudio6Apres;
cerfaFields['localEclairageVentilationIndicationsStudio6Apres']    = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios6Apres.localEclairageIndicationsStudio6Apres+" "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios6Apres.localVentilationIndicationsStudio6Apres;
cerfaFields['localDouchesNombreStudio6Apres']                      = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios6Apres.localDouchesNombreStudio6Apres;
cerfaFields['localCabinetsNombreStudio6Apres']                     = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios6Apres.localCabinetsNombreStudio6Apres;
cerfaFields['localLavabosNombreStudio6Apres']                      = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios6Apres.localLavabosNombreStudio6Apres;


cerfaFields['localSuperficieStudio7Apres']                         = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios7Apres.localSuperficieStudio7Apres;
cerfaFields['localHauteurStudio7Apres']                            = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios7Apres.localHauteurStudio7Apres;
cerfaFields['localEquipementsTechniquesStudio7Apres']              = "hauteur sous faux plafond :"+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios7Apres.localHauteurSousFauxPlafond7Apres+"  Gaines ou luminaires : "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios7Apres.localEquipementsTechniques7Apres;

cerfaFields['localParquetOuiStudio7Apres']                         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios7Apres.parquetApres).eq('localParquetOuiStudio7Apres')? true : false;
cerfaFields['localParquetNonStudio7Apres']                         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios7Apres.parquetApres).eq('localParquetNonStudio7Apres')? true : false;
cerfaFields['localParquetLatteOuiStudio7Apres']                    = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios7Apres.latteApres).eq('localParquetLatteOuiStudio7Apres')? true : false;
cerfaFields['localParquetLatteNonStudio7Apres']                    = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios7Apres.latteApres).eq('localParquetLatteNonStudio7Apres')? true : false;
cerfaFields['localParquetTypeStudio7Apres']                        = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios7Apres.localParquetTypeStudio7Apres;
cerfaFields['localSolPrecisionStudio7Apres']                       = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios7Apres.localSolPrecisionStudio7Apres;
cerfaFields['localSolSupportLambourdageSimpleStudio7Apres']        = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios7Apres.lambourdageApres).eq('localSolSupportLambourdageSimpleStudio7Apres')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio7Apres']        = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios7Apres.lambourdageApres).eq('localSolSupportLambourdageDoubleStudio7Apres')? true : false;
cerfaFields['localSolSupportAutreStudio7Apres']                    = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios7Apres.lambourdageApres).eq('localSolSupportAutreStudio7Apres')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio7Apres']            = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios7Apres.localSolSupportAutrePreciserStudio7Apres;
cerfaFields['localEclairageVentilationIndicationsStudio7Apres']    = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios7Apres.localEclairageIndicationsStudio7Apres+" "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios7Apres.localVentilationIndicationsStudio7Apres;
cerfaFields['localDouchesNombreStudio7Apres']                      = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios7Apres.localDouchesNombreStudio7Apres;
cerfaFields['localCabinetsNombreStudio7Apres']                     = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios7Apres.localCabinetsNombreStudio7Apres;
cerfaFields['localLavabosNombreStudio7Apres']                      = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios7Apres.localLavabosNombreStudio7Apres;


cerfaFields['localSuperficieStudio8Apres']                    	   = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios8Apres.localSuperficieStudio8Apres;
cerfaFields['localHauteurStudio8Apres']                            = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios8Apres.localHauteurStudio8Apres;
cerfaFields['localEquipementsTechniquesStudio8Apres']              = "hauteur sous faux plafond :"+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios8Apres.localHauteurSousFauxPlafond8Apres+"  Gaines ou luminaires : "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios8Apres.localEquipementsTechniques8Apres;

cerfaFields['localParquetOuiStudio8Apres']                         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios8Apres.parquetApres).eq('localParquetOuiStudio8Apres')? true : false;
cerfaFields['localParquetNonStudio8Apres']                         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios8Apres.parquetApres).eq('localParquetNonStudio8Apres')? true : false;
cerfaFields['localParquetLatteOuiStudio8Apres']                    = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios8Apres.latteApres).eq('localParquetLatteOuiStudio8Apres')? true : false;
cerfaFields['localParquetLatteNonStudio8Apres']                    = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios8Apres.latteApres).eq('localParquetLatteNonStudio8Apres')? true : false;
cerfaFields['localParquetTypeStudio8Apres']                        = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios8Apres.localParquetTypeStudio8Apres;
cerfaFields['localSolPrecisionStudio8Apres']                       = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios8Apres.localSolPrecisionStudio8Apres;
cerfaFields['localSolSupportLambourdageSimpleStudio8Apres']        = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios8Apres.lambourdageApres).eq('localSolSupportLambourdageSimpleStudio8Apres')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio8Apres']        = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios8Apres.lambourdageApres).eq('localSolSupportLambourdageDoubleStudio8Apres')? true : false;
cerfaFields['localSolSupportAutreStudio8Apres']                    = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios8Apres.lambourdageApres).eq('localSolSupportAutreStudio8Apres')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio8Apres']            = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios8Apres.localSolSupportAutrePreciserStudio8Apres;
cerfaFields['localEclairageVentilationIndicationsStudio8Apres']    = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios8Apres.localEclairageIndicationsStudio8Apres+" "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios8Apres.localVentilationIndicationsStudio8Apres;
cerfaFields['localDouchesNombreStudio8Apres']                      = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios8Apres.localDouchesNombreStudio8Apres;
cerfaFields['localCabinetsNombreStudio8Apres']                     = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios8Apres.localCabinetsNombreStudio8Apres;
cerfaFields['localLavabosNombreStudio8Apres']                      = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios8Apres.localLavabosNombreStudio8Apres;


cerfaFields['localSuperficieStudio9Apres']                         = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios9Apres.localSuperficieStudio9Apres;
cerfaFields['localHauteurStudio9Apres']                            = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios9Apres.localHauteurStudio9Apres;
cerfaFields['localEquipementsTechniquesStudio9Apres']              = "hauteur sous faux plafond :"+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios9Apres.localHauteurSousFauxPlafond9Apres+"  Gaines ou luminaires : "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios9Apres.localEquipementsTechniques9Apres;

cerfaFields['localParquetOuiStudio9Apres']                         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios9Apres.parquetApres).eq('localParquetOuiStudio9Apres')? true : false;
cerfaFields['localParquetNonStudio9Apres']                         = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios9Apres.parquetApres).eq('localParquetNonStudio9Apres')? true : false;
cerfaFields['localParquetLatteOuiStudio9Apres']                    = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios9Apres.latteApres).eq('localParquetLatteOuiStudio9Apres')? true : false;
cerfaFields['localParquetLatteNonStudio9Apres']                    = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios9Apres.latteApres).eq('localParquetLatteNonStudio9Apres')? true : false;
cerfaFields['localParquetTypeStudio9Apres']                        = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios9Apres.localParquetTypeStudio9Apres;
cerfaFields['localSolPrecisionStudio9Apres']                       = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios9Apres.localSolPrecisionStudio9Apres;
cerfaFields['localSolSupportLambourdageSimpleStudio9Apres']        = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios9Apres.lambourdageApres).eq('localSolSupportLambourdageSimpleStudio9Apres')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio9Apres']        = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios9Apres.lambourdageApres).eq('localSolSupportLambourdageDoubleStudio9Apres')? true : false;
cerfaFields['localSolSupportAutreStudio9Apres']                    = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios9Apres.lambourdageApres).eq('localSolSupportAutreStudio9Apres')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio9Apres']            = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios9Apres.localSolSupportAutrePreciserStudio9Apres;
cerfaFields['localEclairageVentilationIndicationsStudio9Apres']    = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios9Apres.localEclairageIndicationsStudio9Apres+" "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios9Apres.localVentilationIndicationsStudio9Apres;
cerfaFields['localDouchesNombreStudio9Apres']                      = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios9Apres.localDouchesNombreStudio9Apres;
cerfaFields['localCabinetsNombreStudio9Apres']                     = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios9Apres.localCabinetsNombreStudio9Apres;
cerfaFields['localLavabosNombreStudio9Apres']                      = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios9Apres.localLavabosNombreStudio9Apres;

 
cerfaFields['localSuperficieStudio10Apres']                             = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios10Apres.localSuperficieStudio10Apres;
cerfaFields['localHauteurStudio10Apres']                                = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios10Apres.localHauteurStudio10Apres;
cerfaFields['localEquipementsTechniquesStudio10Apres']                  = "hauteur sous faux plafond :"+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios10Apres.localHauteurSousFauxPlafond10Apres+"  Gaines ou luminaires : "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios10Apres.localEquipementsTechniques10Apres;

cerfaFields['localParquetOuiStudio10Apres']                             = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios10Apres.parquetApres).eq('localParquetOuiStudio10Apres')? true : false;
cerfaFields['localParquetNonStudio10Apres']                             = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios10Apres.parquet).eq('localParquetNonStudio10Apres')? true : false;
cerfaFields['localParquetLatteOuiStudio10Apres']                        = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios10Apres.latteApres).eq('localParquetLatteOuiStudio10Apres')? true : false;
cerfaFields['localParquetLatteNonStudio10Apres']                        = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios10Apres.latteApres).eq('localParquetLatteNonStudio10Apres')? true : false;
cerfaFields['localParquetTypeStudio10Apres']                            = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios10Apres.localParquetTypeStudio10Apres;
cerfaFields['localSolPrecisionStudio10Apres']                           = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios10Apres.localSolPrecisionStudio10Apres;
cerfaFields['localSolSupportLambourdageSimpleStudio10Apres']            = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios10Apres.lambourdageApres).eq('localSolSupportLambourdageSimpleStudio10Apres')? true : false;
cerfaFields['localSolSupportLambourdageDoubleStudio10Apres']            = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios10Apres.lambourdageApres).eq('localSolSupportLambourdageDoubleStudio10Apres')? true : false;
cerfaFields['localSolSupportAutreStudio10Apres']                        = Value('id').of($ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios10Apres.lambourdageApres).eq('localSolSupportAutreStudio10Apres')? true : false;
cerfaFields['localSolSupportAutrePreciserStudio10Apres']                = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios10Apres.localSolSupportAutrePreciserStudio10Apres;
cerfaFields['localEclairageVentilationIndicationsStudio10Apres']        = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios10Apres.localEclairageIndicationsStudio10Apres+" "+$ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios10Apres.localVentilationIndicationsStudio10Apres;
cerfaFields['localDouchesNombreStudio10Apres']                          = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios10Apres.localDouchesNombreStudio10Apres;
cerfaFields['localCabinetsNombreStudio10Apres']                         = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios10Apres.localCabinetsNombreStudio10Apres;
cerfaFields['localLavabosNombreStudio10Apres']                          = $ds035PE2.caracteristiquesLocal.caracteristiquesLocalApres.studios10Apres.localLavabosNombreStudio10Apres;



cerfaFields['typeDanseClassiqueAvant']                             = $ds035PE2.caracteristiquesLocal.typeDanseAvant.typeDanseClassiqueAvant;
cerfaFields['typeDanseJazzAvant']                                  = $ds035PE2.caracteristiquesLocal.typeDanseAvant.typeDanseJazzAvant;
cerfaFields['typeDanseContemporaineAvant']                         = $ds035PE2.caracteristiquesLocal.typeDanseAvant.typeDanseContemporaineAvant;
cerfaFields['typeDanseDeSocieteAvant']                             = $ds035PE2.caracteristiquesLocal.typeDanseAvant.typeDanseDeSocieteAvant;
cerfaFields['typeDanseTraditionnellesAvant']                       = $ds035PE2.caracteristiquesLocal.typeDanseAvant.typeDanseTraditionnellesAvant;
cerfaFields['typeDanseAutreAvant']                                 = $ds035PE2.caracteristiquesLocal.typeDanseAvant.typeDanseAutreAvant;
cerfaFields['typeDanseAutreLibelleAvant']                          = $ds035PE2.caracteristiquesLocal.typeDanseAvant.typeDanseAutreLibelleAvant;



cerfaFields['typeDanseClassiqueApres']                             = $ds035PE2.caracteristiquesLocal.typeDanseApres.typeDanseClassiqueApres;
cerfaFields['typeDanseJazzApres']                                  = $ds035PE2.caracteristiquesLocal.typeDanseApres.typeDanseJazzApres;
cerfaFields['typeDanseContemporaineApres']                         = $ds035PE2.caracteristiquesLocal.typeDanseApres.typeDanseContemporaineApres;
cerfaFields['typeDanseDeSocieteApres']                             = $ds035PE2.caracteristiquesLocal.typeDanseApres.typeDanseDeSocieteApres;
cerfaFields['typeDanseTraditionnellesApres']                       = $ds035PE2.caracteristiquesLocal.typeDanseApres.typeDanseTraditionnellesApres;
cerfaFields['typeDanseAutreApres']                                 = $ds035PE2.caracteristiquesLocal.typeDanseApres.typeDanseAutreApres;
cerfaFields['typeDanseAutreLibelleApres']                          = $ds035PE2.caracteristiquesLocal.typeDanseApres.typeDanseAutreLibelleApres;
 

 

cerfaFields['nombreElevesAvant']                               = $ds035PE2.caracteristiquesLocal.nombreIndicatifEleves.nombreElevesAvant;
cerfaFields['nombreElevesApres']                               = $ds035PE2.caracteristiquesLocal.nombreIndicatifEleves.nombreElevesApres;

cerfaFields['signatureLieu']                                       = $ds035PE2.signature.signature.lieuSignature;
cerfaFields['signature']                                           = $ds035PE2.signature.signature.signature;
cerfaFields['signatureDate']                                       = $ds035PE2.signature.signature.dateSignature;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $ds035PE2.signature.signature.dateSignature,
		
		
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		dateSignature: $ds035PE2.signature.signature.dateSignature,
		autoriteHabilitee :"Direction régionale des affaires culturelles (DRAC)",
		demandeContexte : "Ecole de Danse - Déclaration de modification",
		civiliteNomPrenom: civNomPrenom,
		declarantDenomination : declarantDenom,
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Cerfa_10454_03.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));

var cerfaDoc = nash.doc //
	.load('models/Cerfa_n_10454_03-Annexe.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));


function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);



/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Ecole de danse_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Ecole de danse - Déclaration de modification',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Ecole de danse - Déclaration de modification',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
