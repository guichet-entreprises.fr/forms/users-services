attachment('pjID', 'pjID');
attachment('pjJustifDomicile', 'pjJustifDomicile');
attachment('pjPhoto', 'pjPhoto');


if($ds026PE3.demandeurGroup.demandeur.demandeurNationaliteNaissance == false){
	attachment('pjCarteSejour', 'pjCarteSejour');
}

attachment('pjCapGestionEtab', 'pjCapGestionEtab');
attachment('pjJustifCotisFi', 'pjJustifCotisFi');
attachment('pjReactuSavoirsPro', 'pjReactuSavoirsPro');

attachment('pjProprieteBail', 'pjProprieteBail');
attachment('pjRespCivile', 'pjRespCivile');
attachment('pjDescriptLocal', 'pjDescriptLocal');
attachment('pjJustifVehicules', 'pjJustifVehicules');
attachment('pjAssuranceVehicule', 'pjAssuranceVehicule');
attachment('pjListeEnseignants', 'pjListeEnseignants');
attachment('pjAutorisationEnseigner', 'pjAutorisationEnseigner');
attachment('pjDomicile', 'pjDomicile');

if (Value('id').of($ds026PE3.etablissementGroup1.etablissementRenseignements.etablissementStatutJuridique).contains('etablissementStatutSARL')
	|| Value('id').of($ds026PE3.etablissementGroup1.etablissementRenseignements.etablissementStatutJuridique).contains('etablissementStatutAutre')){
	attachment('pjStatutsKbis', 'pjStatutsKbis');
}

if($ds026PE3.demandeurGroup.demandeur.demandeurBepecaser == false){
	attachment('pjNonBepecaser', 'pjNonBepecaser');
}