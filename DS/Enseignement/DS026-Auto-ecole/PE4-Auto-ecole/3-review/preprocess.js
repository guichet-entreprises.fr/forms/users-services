var formFields = {};

var civNomPrenom = $ds026PE4.demandeurGroup.demandeur.civiliteDemandeur + " " + ($ds026PE4.demandeurGroup.demandeur.demandeurNomUsage != null ? $ds026PE4.demandeurGroup.demandeur.demandeurNomUsage : $ds026PE4.demandeurGroup.demandeur.demandeurNomNaissance) + " " + $ds026PE4.demandeurGroup.demandeur.demandeurPrenoms;


// Renseignements sur le demandeur

formFields['civiliteMonsieur']                     = Value('id').of($ds026PE4.demandeurGroup.demandeur.civiliteDemandeur).eq('civiliteMonsieur');
formFields['civiliteMadame']                       = Value('id').of($ds026PE4.demandeurGroup.demandeur.civiliteDemandeur).eq('civiliteMadame');
formFields['demandeurPrenoms']                     = $ds026PE4.demandeurGroup.demandeur.demandeurPrenoms;
formFields['demandeurNomNaissance']                = $ds026PE4.demandeurGroup.demandeur.demandeurNomNaissance;
formFields['demandeurNomUsage']                    = $ds026PE4.demandeurGroup.demandeur.demandeurNomUsage;
formFields['demandeurVilleNaissance']              = $ds026PE4.demandeurGroup.demandeur.demandeurVilleNaissance;
formFields['demandeurNationalite']        		   = ($ds026PE4.demandeurGroup.demandeur.demandeurNationaliteNaissance == true ? 'Française' : $ds026PE4.demandeurGroup.demandeur.demandeurNationaliteEtranger);
formFields['demandeurDateNaissance']               = $ds026PE4.demandeurGroup.demandeur.demandeurDateNaissance;
formFields['demandeurLieuNaissance']               = $ds026PE4.demandeurGroup.demandeur.demandeurVilleNaissance;

formFields['demandeurAdresseNumeroVoie']           = ($ds026PE4.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurAdresseNumeroVoie != null ? $ds026PE4.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurAdresseNumeroVoie : '') + " " 
														+ ($ds026PE4.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurAdresseComplement != null ? $ds026PE4.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurAdresseComplement : '');
formFields['demandeurAdresseCPPays']               = Value('id').of($ds026PE4.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurPays).eq('FRXXXXX') ? 
													($ds026PE4.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurAdresseCP + " " 
													+ $ds026PE4.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurAdresseCommune) : 
													+ (($ds026PE4.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurAdresseCPBis != null ? $ds026PE4.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurAdresseCPBis : '')  
													+ ' ' + $ds026PE4.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurAdresseVille
													+ ' ' + $ds026PE4.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurPays);

formFields['demandeurTelephone']                   = $ds026PE4.demandeurGroup.demandeur.demandeurTelephone;
formFields['demandeurEmail']                       = $ds026PE4.demandeurGroup.demandeur.demandeurEmail;

// Catégories de permis

formFields['categorieA'] 						   = Value('id').of($ds026PE4.signatureGroup.declaration.categoriesPermis).contains('categoriePermiA');
formFields['categorieB'] 						   = Value('id').of($ds026PE4.signatureGroup.declaration.categoriesPermis).contains('categoriePermiB');
formFields['categorieBE'] 						   = Value('id').of($ds026PE4.signatureGroup.declaration.categoriesPermis).contains('categoriePermiBE');
formFields['categorieC'] 						   = Value('id').of($ds026PE4.signatureGroup.declaration.categoriesPermis).contains('categoriePermiC');


// Signature 

formFields['signatureCoche']                       = $ds026PE4.signatureGroup.signature.signatureCoche;
formFields['signatureLieu']                        = $ds026PE4.signatureGroup.signature.signatureLieu;
formFields['signatureDate']                        = $ds026PE4.signatureGroup.signature.signatureDate;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qpxxxPEx.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */


 
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var parisPC = Value('id').of($ds026PE4.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurPays).eq('FRXXXXX') ?
				(Value('id').of($ds026PE4.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurDepartement).contains('75') ? true : false
				|| Value('id').of($ds026PE4.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurDepartement).contains('92') ? true : false
				|| Value('id').of($ds026PE4.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurDepartement).contains('93') ? true : false
				|| Value('id').of($ds026PE4.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurDepartement).contains('94') ? true : false) : 
				(Value('id').of($ds026PE4.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurLieuExercice).contains('75') ? true : false
				|| Value('id').of($ds026PE4.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurLieuExercice).contains('92') ? true : false
				|| Value('id').of($ds026PE4.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurLieuExercice).contains('93') ? true : false
				|| Value('id').of($ds026PE4.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurLieuExercice).contains('94') ? true : false);
var autre = "Direction départementale des territoires (DDT) de la préfecture de " + (Value('id').of($ds026PE4.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurPays).eq('FRXXXXX') ? $ds026PE4.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurAdresseCommune : $ds026PE4.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurLieuExercice);
var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $ds026PE4.signatureGroup.signature.signatureDate,
		autoriteHabilitee : (parisPC == true ? 'Direction régionale et interdépartementale de l\'équipement et de l\'aménagement pour Paris et les départements de la petite couronne (DRIEA)' : autre),
		demandeContexte : "Demande d’autorisation d’enseigner la conduite de véhicules à moteur et la sécurité routière",
		civiliteNomPrenom : civNomPrenom
	});


/*
 * Ajout du formulaire de déclaration puis de l'attestation d'honorabilité
 */
var cerfaDoc = nash.doc //
	.load('models/Formulaire - DEMANDE D\'AUTORISATION D\'ENSEIGNER.pdf') //
	.apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));




function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustifDomicile);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPhoto);

if($ds026PE4.demandeurGroup.demandeur.demandeurNationaliteNaissance == false){
	appendPj($attachmentPreprocess.attachmentPreprocess.pjCarteSejour);
}

appendPj($attachmentPreprocess.attachmentPreprocess.pjBepecaser);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertificatMedical);
appendPj($attachmentPreprocess.attachmentPreprocess.pjQualif);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Auto_Ecole.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Auto-école - Demande d’autorisation d’enseigner la conduite de véhicules à moteur et la sécurité routière',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Auto-école - Demande d’autorisation d’enseigner la conduite de véhicules à moteur et la sécurité routière',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});