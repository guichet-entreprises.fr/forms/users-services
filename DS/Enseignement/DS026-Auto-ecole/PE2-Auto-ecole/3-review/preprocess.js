var formFields = {};

var civNomPrenom = $ds026PE2.demandeurGroup.demandeur.civiliteDemandeur + " " + $ds026PE2.demandeurGroup.demandeur.demandeurNomNaissance + " " + $ds026PE2.demandeurGroup.demandeur.demandeurPrenoms;

// Type de demande

formFields['transfert']                            = false;
formFields['renouvellement']                       = false;
formFields['demandeInitiale']                      = true;

// Renseignements sur le demandeur

formFields['demandeurPrenoms']                     = $ds026PE2.demandeurGroup.demandeur.demandeurPrenoms;
formFields['demandeurNomNaissance']                = $ds026PE2.demandeurGroup.demandeur.demandeurNomNaissance;
formFields['demandeurNomUsage']                    = $ds026PE2.demandeurGroup.demandeur.demandeurNomUsage;
formFields['demandeurVilleNaissance']              = $ds026PE2.demandeurGroup.demandeur.demandeurVilleNaissance;
formFields['demandeurDepartementNaissance']        = $ds026PE2.demandeurGroup.demandeur.demandeurDepartementNaissance;
formFields['demandeurNationaliteNaissance']        = ($ds026PE2.demandeurGroup.demandeur.demandeurNationaliteNaissance == true ? 'Française' : $ds026PE2.demandeurGroup.demandeur.demandeurNationaliteEtranger);
formFields['demandeurDateNaissance']               = $ds026PE2.demandeurGroup.demandeur.demandeurDateNaissance;

formFields['demandeurAdresseNumeroVoie']           = ($ds026PE2.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurAdresseNumeroVoie != null ? $ds026PE2.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurAdresseNumeroVoie : '') + " " 
														+ ($ds026PE2.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurAdresseComplement != null ? $ds026PE2.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurAdresseComplement : '');
formFields['demandeurAdresseCP']                   = $ds026PE2.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurAdresseCP;
formFields['demandeurAdresseVille']                = $ds026PE2.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurAdresseVille;

formFields['demandeurBepecaserOui']                = ($ds026PE1.demandeurGroup.demandeur.demandeurBepecaser) ? true : false;
formFields['demandeurBepecaserNon']                = ($ds026PE1.demandeurGroup.demandeur.demandeurBepecaser) ? false :true;
formFields['demandeurEmail']                       = $ds026PE2.demandeurGroup.demandeur.demandeurEmail;

// Renseignements concernant l'établissement

formFields['etablissementRaisonSociale']           = $ds026PE2.etablissementGroup1.etablissementRenseignements.etablissementRaisonSociale;
formFields['etablissementSiret']                   = ($ds026PE2.etablissementGroup1.etablissementRenseignements.etablissementSiret != null ? $ds026PE2.etablissementGroup1.etablissementRenseignements.etablissementSiret : $ds026PE2.etablissementGroup1.etablissementRenseignements.etablissementSiren);
// attention que faire pour le SIREN ?

formFields['etablissementStatutSARL']              = Value('id').of($ds026PE2.etablissementGroup1.etablissementRenseignements.etablissementStatutJuridique).eq('etablissementStatutSARL');
formFields['etablissementStatutPerso']             = false;
formFields['etablissementStatutAutre']             = Value('id').of($ds026PE2.etablissementGroup1.etablissementRenseignements.etablissementStatutJuridique).eq('etablissementStatutAutre');
formFields['etablissementSatutAutreDetails']       = $ds026PE2.etablissementGroup1.etablissementRenseignements.etablissementSatutAutreDetails;
formFields['etablissementEnseigne']                = $ds026PE2.etablissementGroup1.etablissementRenseignements.etablissementEnseigne;

formFields['etablissementAdresseNumeroVoie']       = ($ds026PE2.etablissementGroup1.etablissementRenseignements.etablissementAdresse.etablissementAdresseNumeroVoie != null ? $ds026PE2.etablissementGroup1.etablissementRenseignements.etablissementAdresse.etablissementAdresseNumeroVoie : '') + " " 
														+ ($ds026PE2.etablissementGroup1.etablissementRenseignements.etablissementAdresse.etablissementComplement != null ? $ds026PE2.etablissementGroup1.etablissementRenseignements.etablissementAdresse.etablissementComplement : '');
formFields['etablissementAdresseVille']            = $ds026PE2.etablissementGroup1.etablissementRenseignements.etablissementAdresse.etablissementAdresseVille;
formFields['etablissementAdresseCP']               = $ds026PE2.etablissementGroup1.etablissementRenseignements.etablissementAdresse.etablissementAdresseCP;

formFields['etablissementTelephoneFixe']           = $ds026PE2.etablissementGroup1.etablissementRenseignements.etablissementTelephoneFixe;
formFields['etablissementTelephonePortable']       = $ds026PE2.etablissementGroup1.etablissementRenseignements.etablissementTelephonePortable;
formFields['etablissementAgreeOui']                = ($ds026PE1.etablissementGroup1.etablissementRenseignements.etablissementAgree) ? true : false;
formFields['etablissementAgreeNon']                = ($ds026PE1.etablissementGroup1.etablissementRenseignements.etablissementAgree) ? false : true;
formFields['etablissementAncienExploitant']        = $ds026PE2.etablissementGroup1.etablissementRenseignements.etablissementAncienExploitant;
formFields['etablissementEmail']                   = $ds026PE2.etablissementGroup1.etablissementRenseignements.etablissementEmail;

// Caractéristiques de l'établissement

formFields['categoriePermiAM']                     = Value('id').of($ds026PE2.etablissementGroup2.etablissementCaracteristiques1.categoriesPermis).contains('categoriePermiAM');
formFields['categoriePermiA1']                     = Value('id').of($ds026PE2.etablissementGroup2.etablissementCaracteristiques1.categoriesPermis).contains('categoriePermiA1');
formFields['categoriePermiA2']                     = Value('id').of($ds026PE2.etablissementGroup2.etablissementCaracteristiques1.categoriesPermis).contains('categoriePermiA2');
formFields['categoriePermiA']                      = Value('id').of($ds026PE2.etablissementGroup2.etablissementCaracteristiques1.categoriesPermis).contains('categoriePermiA');
formFields['categoriePermiB96']                    = Value('id').of($ds026PE2.etablissementGroup2.etablissementCaracteristiques1.categoriesPermis).contains('categoriePermiB96');
formFields['categoriePermiC1']                     = Value('id').of($ds026PE2.etablissementGroup2.etablissementCaracteristiques1.categoriesPermis).contains('categoriePermiC1');
formFields['categoriePermiC1E']                    = Value('id').of($ds026PE2.etablissementGroup2.etablissementCaracteristiques1.categoriesPermis).contains('categoriePermiC1E');
formFields['categoriePermiC']                      = Value('id').of($ds026PE2.etablissementGroup2.etablissementCaracteristiques1.categoriesPermis).contains('categoriePermiC');
formFields['categoriePermiCE']                     = Value('id').of($ds026PE2.etablissementGroup2.etablissementCaracteristiques1.categoriesPermis).contains('categoriePermiCE');
formFields['categoriePermiD1']                     = Value('id').of($ds026PE2.etablissementGroup2.etablissementCaracteristiques1.categoriesPermis).contains('categoriePermiD1');
formFields['categoriePermiD1E']                    = Value('id').of($ds026PE2.etablissementGroup2.etablissementCaracteristiques1.categoriesPermis).contains('categoriePermiD1E');
formFields['categoriePermiD']                      = Value('id').of($ds026PE2.etablissementGroup2.etablissementCaracteristiques1.categoriesPermis).contains('categoriePermiD');
formFields['categoriePermiDE']                     = Value('id').of($ds026PE2.etablissementGroup2.etablissementCaracteristiques1.categoriesPermis).contains('categoriePermiDE');
formFields['categoriePermiBE']                     = Value('id').of($ds026PE2.etablissementGroup2.etablissementCaracteristiques1.categoriesPermis).contains('categoriePermiBE');
formFields['categoriePermiB']                      = Value('id').of($ds026PE2.etablissementGroup2.etablissementCaracteristiques1.categoriesPermis).contains('categoriePermiB');

formFields['etablissementNbVoitures']              = $ds026PE2.etablissementGroup2.etablissementCaracteristiques1.etablissementNbVoitures;
formFields['etablissementNb2Roues']                = $ds026PE2.etablissementGroup2.etablissementCaracteristiques1.etablissementNb2Roues;
formFields['etablissementNbPoidsLourds']           = $ds026PE2.etablissementGroup2.etablissementCaracteristiques1.etablissementNbPoidsLourds;
formFields['etablissementPlusieursNon']            = ($ds026PE1.etablissementGroup2.etablissementCaracteristiques1.etablissementPlusieurs) ? false : true;
formFields['etablissementPlusieursOui']            = ($ds026PE1.etablissementGroup2.etablissementCaracteristiques1.etablissementPlusieurs) ? true : false;
formFields['etablissementNbEnseignants']           = $ds026PE2.etablissementGroup2.etablissementCaracteristiques1.etablissementNbEnseignants;


for (var i = 0;i < $ds026PE2.etablissementGroup2.etablissementCaracteristiques1.etablissementAutre.size(); i++) {
formFields['etablissementAutreCoordonnees' + i]       = ($ds026PE2.etablissementGroup2.etablissementCaracteristiques1.etablissementAutre[i].etablissementAdresseNumeroVoie != null ? $ds026PE2.etablissementGroup2.etablissementCaracteristiques1.etablissementAutre[i].etablissementAdresseNumeroVoie : '') + " " 
														+ ($ds026PE2.etablissementGroup2.etablissementCaracteristiques1.etablissementAutre[i].etablissementComplement != null ? $ds026PE2.etablissementGroup2.etablissementCaracteristiques1.etablissementAutre[i].etablissementComplement : '') + " " 
														+ ($ds026PE2.etablissementGroup2.etablissementCaracteristiques1.etablissementAutre[i].etablissementAdresseVille != null ? $ds026PE2.etablissementGroup2.etablissementCaracteristiques1.etablissementAutre[i].etablissementAdresseVille : '') + " " 
														+ ($ds026PE2.etablissementGroup2.etablissementCaracteristiques1.etablissementAutre[i].etablissementAdresseCP != null ? $ds026PE2.etablissementGroup2.etablissementCaracteristiques1.etablissementAutre[i].etablissementAdresseCP : '');
}

formFields['etablissementMetrageSurfaceSalleCode'] = $ds026PE2.etablissementGroup2.etablissementCaracteristiques2.metrage.etablissementMetrageSurfaceSalleCode;
formFields['etablissementMetrageSurfaceAccueil']   = $ds026PE2.etablissementGroup2.etablissementCaracteristiques2.metrage.etablissementMetrageSurfaceAccueil;
formFields['etablissementMetrageSurfaceTotale']    = $ds026PE2.etablissementGroup2.etablissementCaracteristiques2.metrage.etablissementMetrageSurfaceTotale;

formFields['etablissementAmmenagementDebitAir']    = $ds026PE2.etablissementGroup2.etablissementCaracteristiques2.amenagement.etablissementAmmenagementDebitAir;

formFields['declarationAutresDemarches']           = $ds026PE2.signatureGroup.declaration.declarationAutresDemarches;

// Signature 

formFields['signatureCoche']                       = $ds026PE2.signatureGroup.signature.signatureCoche;
formFields['signatureLieu']                        = $ds026PE2.signatureGroup.signature.signatureLieu;
formFields['signatureDate']                        = $ds026PE2.signatureGroup.signature.signatureDate;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qpxxxPEx.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */


 
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var parisPC = Value('id').of($ds026PE2.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurDepartement).contains('75') ? true : false
				|| Value('id').of($ds026PE2.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurDepartement).contains('92') ? true : false
				|| Value('id').of($ds026PE2.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurDepartement).contains('93') ? true : false
				|| Value('id').of($ds026PE2.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurDepartement).contains('94') ? true : false;
var autre = "Direction départementale des territoires (DDT) de la préfecture de " + $ds026PE2.demandeurGroup.demandeur.demandeurAdresseDomicile.demandeurAdresseVille;
var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $ds026PE2.signatureGroup.signature.signatureDate,
		autoriteHabilitee : (parisPC == true ? 'Direction régionale et interdépartementale de l\'équipement et de l\'aménagement pour Paris et les départements de la petite couronne (DRIEA)' : autre),
		demandeContexte : "Demande d’agrément pour l'exploitation d’une auto-école par une personne morale",
		civiliteNomPrenom : civNomPrenom
	});


/*
 * Ajout du formulaire de déclaration puis de l'attestation d'honorabilité
 */
var cerfaDoc = nash.doc //
	.load('models/Formulaire - Demande d’agrément pour l’exploitation d\'une école de conduite.pdf') //
	.apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));




function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustifDomicile);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPhoto);

if($ds026PE2.demandeurGroup.demandeur.demandeurNationaliteNaissance == false){
	appendPj($attachmentPreprocess.attachmentPreprocess.pjCarteSejour);
}

appendPj($attachmentPreprocess.attachmentPreprocess.pjCapGestionEtab);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustifCotisFi);
appendPj($attachmentPreprocess.attachmentPreprocess.pjStatuts);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExtraitKbis);
appendPj($attachmentPreprocess.attachmentPreprocess.pjProprieteBail);
appendPj($attachmentPreprocess.attachmentPreprocess.pjRespCivile);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDescriptLocal);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustifVehicules);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssuranceVehicule);
appendPj($attachmentPreprocess.attachmentPreprocess.pjListeEnseignants);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAutorisationEnseigner);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDomicile);



if($ds026PE2.demandeurGroup.demandeur.demandeurBepecaser == false){
	appendPj($attachmentPreprocess.attachmentPreprocess.pjNonBepecaser);
}

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Auto_Ecole.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Auto-école - Demande d’agrément pour une personne morale',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Auto-école - Demande d’agrément pour une personne morale',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});