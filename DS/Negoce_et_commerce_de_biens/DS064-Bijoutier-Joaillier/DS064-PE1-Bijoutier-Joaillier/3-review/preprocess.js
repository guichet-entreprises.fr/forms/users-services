var formFields = {};

var chemiDeclaration = $ds064.declarationGroup.declaration;

formFields['nomPro']             = chemiDeclaration.nomPro;
formFields['activite']           = chemiDeclaration.activite;
formFields['adresseSiege']       = $ds064.declarationGroup.declaration.adresseVoie +", "+ ($ds064.declarationGroup.declaration.adresseVoieComplement ? $ds064.declarationGroup.declaration.adresseVoieComplement : ', ') +", "+ $ds064.declarationGroup.declaration.adresseCP +" "+ $ds064.declarationGroup.declaration.adresseVille +" "+ $ds064.declarationGroup.declaration.adressePays ;
formFields['courriel']           = chemiDeclaration.courriel;
formFields['telephone']          = chemiDeclaration.telephone;
formFields['objet']              = 'Déclaration de détention de métaux précieux';
//formFields['adresseExercice']    = chemiDeclaration.AdresseExercice;
if($ds064.declarationGroup.declaration.adressedifferenteQuestion){
   formFields['adresseExercice'] = $ds064.declarationGroup.declaration.adressedifferenteGroup.adressedifferenteVoie +", "+ ($ds064.declarationGroup.declaration.adressedifferenteGroup.adressedifferenteComplement ? $ds064.declarationGroup.declaration.adressedifferenteGroup.adressedifferenteComplement: ', ' )+" "+ $ds064.declarationGroup.declaration.adressedifferenteGroup.adressedifferenteCP +" "+ $ds064.declarationGroup.declaration.adressedifferenteGroup.adressedifferenteVille +" "+ $ds064.declarationGroup.declaration.adressedifferenteGroup.adressedifferentePays ;
}else {
   formFields['adresseExercice'] = '';
}

var chaminSignature = $ds064.signatureGroup.signature;

formFields['declarationHonneur'] = chaminSignature.declarationHonneur;
formFields['signature']          = $ds064.signatureGroup.signature.signatureNom +" "+  $ds064.signatureGroup.signature.signaturePrenom;
formFields['dateSignature']      = chaminSignature.dateSignature;
formFields['lieuSignature']      = chaminSignature.lieuSignature;

var cerfaDoc1 = nash.doc //
.load('models/Declaration_De_Detention_De_Metaux_Precieux.pdf') //
.apply (formFields);



/**********************
Pieces jointes
***********************/
function appendPj(fld) {
fld.forEach(function (elm) {
    cerfaDoc1.append(elm);
});
}	

appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificatifExercice);
appendPj($attachmentPreprocess.attachmentPreprocess.pjKKBis);
appendPj($attachmentPreprocess.attachmentPreprocess.pjBail); 




/*
* Chargement du courrier d'accompagnement à destination de l'AC
*/

var nomPrenom = $ds064.signatureGroup.signature.signatureNom +" "+  $ds064.signatureGroup.signature.signaturePrenom; 
var finalDoc = nash.doc //
.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
.apply({
	date: $ds064.signatureGroup.signature.signatureFaitLe ,
	autoriteHabilitee :"Direction Régionale des douanes et droits indirects – bureau de garantie" ,
	demandeContexte : "Déclaration de détention de métaux précieux.",
	civiliteNomPrenom : nomPrenom
});
finalDoc.append(cerfaDoc1.save('cerfa.pdf'));	


/*
* Enregistrement du fichier (en mémoire)
*/
var finalDocItem = finalDoc.save('Bijoutier_Joaillier.pdf');	


/*
* Persistance des données obtenues
*/

return spec.create({
id : 'review',
label : 'Bijoutier - Joaillier - Déclaration de détention de métaux précieux',
groups : [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : [ spec.createData({
        id : 'formulaire',
        label : "Déclaration de détenteur de métaux précieux.",
        description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
        type : 'FileReadOnly',
        value : [ finalDocItem ]
    }) ]
}) ]
});