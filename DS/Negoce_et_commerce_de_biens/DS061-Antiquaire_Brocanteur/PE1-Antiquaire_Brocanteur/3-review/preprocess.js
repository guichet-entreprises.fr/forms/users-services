function pad(s) { return (s < 10) ? '0' + s : s; }

var formFields = {};


var civNomPrenom = $ds061PE1.etatCivil.identificationDeclarant.civilite + ' ' + $ds061PE1.etatCivil.identificationDeclarant.nomNaissanceDeclarant + ' ' + $ds061PE1.etatCivil.identificationDeclarant.prenomDeclarant;


// Identification du déclarant
formFields['madame']								   = ($ds061PE1.etatCivil.identificationDeclarant.civilite=='Madame');
formFields['monsieur']                        		   = ($ds061PE1.etatCivil.identificationDeclarant.civilite=='Monsieur');


formFields['civilite']                     			   = $ds061PE1.etatCivil.identificationDeclarant.civilite;
formFields['nomEpouse']                         	   = $ds061PE1.etatCivil.identificationDeclarant.nomEpouse;
formFields['nomNaissanceDeclarant']                    = $ds061PE1.etatCivil.identificationDeclarant.nomNaissanceDeclarant;
formFields['prenomDeclarant']                          = $ds061PE1.etatCivil.identificationDeclarant.prenomDeclarant;

	if($ds061PE1.etatCivil.identificationDeclarant.dateNaissanceDeclarant != null) {
    var dateTemp = new Date(parseInt($ds061PE1.etatCivil.identificationDeclarant.dateNaissanceDeclarant.getTimeInMillis()));
    var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
    var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
    formFields['jourNaissanceDeclarant']         = day;
    formFields['moisNaissanceDeclarant']         = month;
    formFields['anneeNaissanceDeclarant']        = year;
}
	
formFields['dateNaissanceDeclarant']                   = $ds061PE1.etatCivil.identificationDeclarant.dateNaissanceDeclarant;
formFields['nationaliteDeclarant']                     = $ds061PE1.etatCivil.identificationDeclarant.nationaliteDeclarant;
formFields['villePaysNaissanceDeclarant']              = $ds061PE1.etatCivil.identificationDeclarant.villePaysNaissanceDeclarant;

// Adresse du déclarant

formFields['numeroNomTypeVoieDeclarant']               = $ds061PE1.coordonnes.adresseDeclarant.numeroNomTypeVoieDeclarant;
formFields['complementAdresseDeclarant']               = $ds061PE1.coordonnes.adresseDeclarant.complementAdresseDeclarant;
formFields['codePostalAdresseDeclarant']               = $ds061PE1.coordonnes.adresseDeclarant.codePostalAdresseDeclarant;
formFields['villeAdresseDeclarant']                    = $ds061PE1.coordonnes.adresseDeclarant.villeAdresseDeclarant;

// Adresse Société 
formFields['raisonSocialeEtablissement']               = $ds061PE1.informationsSociete.adresseSociete.raisonSocialeEtablissement;
formFields['statutEtablissement']               	   = $ds061PE1.informationsSociete.adresseSociete.statutEtablissement;
formFields['lieuPrincipalEtablissement']               = $ds061PE1.informationsSociete.adresseSociete.lieuPrincipalEtablissement;


// Lieu exercice habituel

formFields['adresseExercice']            			   = $ds061PE1.adresseExercice.lieuExercice.adresseExercice;
formFields['complementAdresseExercice']                = $ds061PE1.adresseExercice.lieuExercice.complementAdresseExercice;
formFields['codePostalExercice']               		   = $ds061PE1.adresseExercice.lieuExercice.codePostalExercice;
formFields['villeExercice']               			   = $ds061PE1.adresseExercice.lieuExercice.villeExercice;
formFields['paysExercice']               			   = $ds061PE1.adresseExercice.lieuExercice.paysExercice;

formFields['villePaysPrincipalEtablisement']           = $ds061PE1.adresseExercice.lieuExercice.adresseExercice + ", " + ($ds061PE1.adresseExercice.lieuExercice.complementAdresseExercice != null ? $ds061PE1.adresseExercice.lieuExercice.complementAdresseExercice + ", " :'') + $ds061PE1.adresseExercice.lieuExercice.villeExercice + ", " + $ds061PE1.adresseExercice.lieuExercice.paysExercice;


//Lieu d'établissements secondaires
formFields['villesEtablissementsSecondaires']          = $ds061PE1.adresseExercice.etablissementsSecondaires.villesEtablissementsSecondaires;

//Signature

formFields['signature']               			       = civNomPrenom;
formFields['lieuSignature']                    		   = $ds061PE1.signatureGroup.signature.lieuSignature;
formFields['signatureCoche']      					   = $ds061PE1.signatureGroup.signature.signatureCoche;
formFields['dateSignature']                            = $ds061PE1.signatureGroup.signature.dateSignature;



var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		date: $ds061PE1.signatureGroup.signature.dateSignature,
		autoriteHabilitee :"Préfet du département",
		demandeContexte : "Inscription au registre des revendeurs d'objets mobiliers.",
		civiliteNomPrenom : civNomPrenom
	});
	
var cerfaDoc = nash.doc //
    .load('models/cerfa-11733-01.pdf') //
    .apply(formFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJ
 */
 
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDeclaration);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCarteSejour);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificatifExercice);


var finalDocItem = finalDoc.save('Antiquaire_Brocanteur.pdf');


return spec.create({
    id : 'review',
   label : 'Antiquaire - Brocanteur - demande d\'inscription au registre des revendeurs d\'objets mobiliers',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande d\'inscription au registre des revendeurs d\'objets mobiliers.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});

