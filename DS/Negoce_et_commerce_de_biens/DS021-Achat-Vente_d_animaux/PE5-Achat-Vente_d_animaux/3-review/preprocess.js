var formFields = {};

formFields['nomPro']             = $ds021PE5.declarationGroup.identification.nomPro;
formFields['qualification']      = $ds021PE5.declarationGroup.identification.qualification;


formFields['telephone']          = $ds021PE5.declarationGroup.identification.siege.adresseTelephone;



formFields['objet']              = "Demande de certificat de capacité pour l'exploitant d'un établissement détenant des animaux non domestiques.";


formFields['adresseSiege']                          = $ds021PE5.declarationGroup.identification.siege.adresseNVoie +' '
														+ $ds021PE5.declarationGroup.identification.siege.adresseVoie + ' ' 
															+ ($ds021PE5.declarationGroup.identification.siege.adresseVoieComplement ? $ds021PE5.declarationGroup.identification.siege.adresseVoieComplement : '') +', '
														+ ($ds021PE5.declarationGroup.identification.siege.adresseCP ? $ds021PE5.declarationGroup.identification.siege.adresseCP : '') +' '
														+ $ds021PE5.declarationGroup.identification.siege.adresseVille +', '
														+ $ds021PE5.declarationGroup.identification.siege.adressePays;
formFields['courriel']           					=	$ds021PE5.declarationGroup.identification.siege.email;



var nomPrenom = $ds021PE5.signatureGroup.signature.signatureNom +' '+ $ds021PE5.signatureGroup.signature.signaturePrenom;
formFields['signatureNom']                         	= $ds021PE5.signatureGroup.signature.signatureNom +' '+ $ds021PE5.signatureGroup.signature.signaturePrenom;
formFields['declarationHonneur'] 					= $ds021PE5.signatureGroup.signature.declarationHonneur;
formFields['signatureFaitLe']                      	= $ds021PE5.signatureGroup.signature.signatureFaitLe;
formFields['signatureFaitA']                      	= $ds021PE5.signatureGroup.signature.signatureFaitA +', le ';
formFields['signature']          					= nomPrenom;



 var cerfaDoc1 = nash.doc //
	.load('models/courrier_libre_LE_DS.pdf') //
	.apply (formFields);



/**********************
Pieces jointes
***********************/
function appendPj(fld) {
	fld.forEach(function (elm) {
        cerfaDoc1.append(elm);
    });
}	
	
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCompetence);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAmenagement); 





/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		date: $ds021PE5.signatureGroup.signature.signatureFaitLe ,
		autoriteHabilitee :"La direction départementale de la protection des populations (DDPP)" ,
		demandeContexte : "Demande de certificat de capacité pour l'exploitant d'un établissement détenant des animaux non domestiques.",
		civiliteNomPrenom : nomPrenom
	});
	finalDoc.append(cerfaDoc1.save('cerfa.pdf'));	
	
	
/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Achat-Vente_animaux_Certificat_Capacite.pdf');	
	
	
/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Achat-Vente d\'animaux d\'agrément ou de compagnie - Demande de certificat de capacité pour l\'exploitant d\'un établissement détenant des animaux non domestiques',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : "Demande de certificat de capacité pour l'exploitant d'un établissement détenant des animaux non domestiques.",
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});