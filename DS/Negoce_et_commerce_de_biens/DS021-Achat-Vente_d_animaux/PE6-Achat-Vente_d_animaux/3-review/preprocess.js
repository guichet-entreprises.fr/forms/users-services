var cerfaFields = {};

var destinataire = Value('id').of($ds021PE6.signature.signature.departementInstallation).eq("974") ? "DEAL" : (Value('id').of($ds021PE6.signature.signature.departementInstallation).eq("21") ? "DDT" : (Value('id').of($ds021PE6.signature.signature.departementInstallation).eq("75") ? "DTPP" : "Prefecture du département"));
var signataire1 = ($ds021PE6.idDeclarant.idDeclarant1.nomSignataire !=null ? $ds021PE6.idDeclarant.idDeclarant1.nomSignataire : '') + ($ds021PE6.idDeclarant.idDeclarant1.prenomsSignataire !=null ? ' '+$ds021PE6.idDeclarant.idDeclarant1.prenomsSignataire : '' );
var signataire2 = ($ds021PE6.idDeclarant.idDeclarant1.nomDeclarant !=null ? $ds021PE6.idDeclarant.idDeclarant1.nomDeclarant : '');
 
//Identification Déclarant
cerfaFields['personneMorale']                 = Value('id').of($ds021PE6.idDeclarant.idDeclarant1.personne).eq("personneMorale") ? true : false;
cerfaFields['personnePhysique']               = Value('id').of($ds021PE6.idDeclarant.idDeclarant1.personne).eq("personnePhysique") ? true : false;
cerfaFields['madame']                         = Value('id').of($ds021PE6.idDeclarant.idDeclarant1.personne2).eq("madame") ? true : false;
cerfaFields['monsieur']                       = Value('id').of($ds021PE6.idDeclarant.idDeclarant1.personne2).eq("monsieur") ? true : false;
cerfaFields['nom']                 			 = ($ds021PE6.idDeclarant.idDeclarant1.nomMorale !=null ? $ds021PE6.idDeclarant.idDeclarant1.nomMorale : '') + ' ' + ($ds021PE6.idDeclarant.idDeclarant1.nomDeclarant !=null ? $ds021PE6.idDeclarant.idDeclarant1.nomDeclarant : '');
cerfaFields['formeJuridique']                 = $ds021PE6.idDeclarant.idDeclarant1.formeJuridique;
cerfaFields['siret']                          = $ds021PE6.idDeclarant.idDeclarant1.siret;
cerfaFields['numVoieAdresseDeclarant']        = $ds021PE6.idDeclarant.idDeclarant1.numVoieAdresseDeclarant;
cerfaFields['complementAdresseDeclarant']     = $ds021PE6.idDeclarant.idDeclarant1.complementAdresseDeclarant;
cerfaFields['codePostalDeclarant']            = $ds021PE6.idDeclarant.idDeclarant1.codePostalDeclarant;
cerfaFields['paysDeclarant']                  = $ds021PE6.idDeclarant.idDeclarant1.paysDeclarant;
cerfaFields['communeDeclarant']               = $ds021PE6.idDeclarant.idDeclarant1.communeDeclarant;
cerfaFields['regionDeclarant']                = $ds021PE6.idDeclarant.idDeclarant1.regionDeclarant;
cerfaFields['numTelDeclarant']                = $ds021PE6.idDeclarant.idDeclarant1.numTelDeclarant;
cerfaFields['numMobileDeclarant']             = $ds021PE6.idDeclarant.idDeclarant1.numMobileDeclarant;
cerfaFields['faxDeclarant']                   = $ds021PE6.idDeclarant.idDeclarant1.faxDeclarant
cerfaFields['emailDeclarant']                 = $ds021PE6.idDeclarant.idDeclarant1.emailDeclarant;

var civNomPrenom = signataire1 + signataire2;

//Signataire
cerfaFields['nomSignataire']                  = $ds021PE6.idDeclarant.idDeclarant1.nomSignataire;
cerfaFields['prenomsSignataire']              = $ds021PE6.idDeclarant.idDeclarant1.prenomsSignataire;
cerfaFields['qualiteSignataire']              = $ds021PE6.idDeclarant.idDeclarant1.qualiteSignataire;

//Infos générales
cerfaFields['siretInstallation']              = $ds021PE6.informations.informations.siretInstallation;
cerfaFields['enseigneSite']                   = $ds021PE6.informations.informations.enseigneSite;
cerfaFields['adresseIdentique']               =  Value('id').of($ds021PE6.informations.informations.adresseIdentique).eq("ouiAdresse") ? true : false;
cerfaFields['numVoieInstallation']            = $ds021PE6.informations.informations.numVoieInstallation;
cerfaFields['complementAdresseInstallation']  = $ds021PE6.informations.informations.complementAdresseInstallation;
cerfaFields['codePostalInstallation']         = $ds021PE6.informations.informations.codePostalInstallation;
cerfaFields['communeInstallation']            = $ds021PE6.informations.informations.communeInstallation;
cerfaFields['numTelInstallation']             = $ds021PE6.informations.informations.numTelInstallation;
cerfaFields['numMobileInstallation']          = $ds021PE6.informations.informations.numMobileInstallation;
cerfaFields['faxInstallation']                = $ds021PE6.informations.informations.faxInstallation;
cerfaFields['emailInstallation']             = $ds021PE6.informations.informations.emailInstallation;

//Description Installation
cerfaFields['descriptionInstallation']        = $ds021PE6.description.description.descriptionInstallation;
cerfaFields['nonAutorisation']                = Value('id').of($ds021PE6.description.description.autorisation).eq("nonAutorisation") ? true : false;
cerfaFields['ouiAutorisation']                = Value('id').of($ds021PE6.description.description.autorisation).eq("ouiAutorisation") ? true : false;
cerfaFields['ouiEnregistrement']              = Value('id').of($ds021PE6.description.description.enregistrement).eq("ouiEnregistrement") ? true : false;
cerfaFields['nonEnregistrement']              = Value('id').of($ds021PE6.description.description.enregistrement).eq("nonEnregistrement") ? true : false;
cerfaFields['nonDeclaration']                 = Value('id').of($ds021PE6.description.description.declaration).eq("nonDeclaration") ? true : false;
cerfaFields['ouiDeclaration']                 = Value('id').of($ds021PE6.description.description.declaration).eq("ouiDeclaration") ? true : false;

//Implantation installation
cerfaFields['numDepartements']                = $ds021PE6.implantation.cadastrePlan.numDepartements;
cerfaFields['ouiInstallationTerritoire']      = Value('id').of($ds021PE6.implantation.cadastrePlan.implantationTerritoire).eq("ouiInstallationTerritoire") ? true : false;
cerfaFields['nonInstallationTerritoire']      = Value('id').of($ds021PE6.implantation.cadastrePlan.implantationTerritoire).eq("nonInstallationTerritoire") ? true : false;
cerfaFields['nonInstallationCommune']         = Value('id').of($ds021PE6.implantation.cadastrePlan.implantationCommune).eq("nonInstallationCommune") ? true : false;
cerfaFields['nomCommunes']                    = $ds021PE6.implantation.cadastrePlan.nomCommunes;
cerfaFields['ouiInstallationCommune']         = Value('id').of($ds021PE6.implantation.cadastrePlan.implantationCommune).eq("ouiInstallationCommune") ? true : false;
cerfaFields['ouiPermis']                      = Value('id').of($ds021PE6.implantation.permis.permisConstruire).eq("ouiPermis") ? true : false;
cerfaFields['nonPermis']                      = Value('id').of($ds021PE6.implantation.permis.permisConstruire).eq("nonPermis") ? true : false;

//Nature et volume activité
cerfaFields['numRubrique1']                   = '';
cerfaFields['numRubrique2']                   = '';
cerfaFields['numRubrique3']                   = '';
cerfaFields['numRubrique4']                   = '';
cerfaFields['numRubrique5']                   = '';
cerfaFields['numRubrique6']                   = '';
cerfaFields['numRubrique7']                   = '';
cerfaFields['numRubrique8']                   = '';
cerfaFields['numRubrique9']                   = '';
cerfaFields['numRubrique10']                  = '';
cerfaFields['numRubrique0']                   = '';
cerfaFields['numRubrique11']                  = '';
cerfaFields['alinea1']                        = '';
cerfaFields['alinea2']                        = '';
cerfaFields['alinea3']                        = '';
cerfaFields['alinea4']                        = '';
cerfaFields['alinea5']                        = '';
cerfaFields['alinea6']                        = '';
cerfaFields['alinea7']                        = '';
cerfaFields['alinea8']                        = '';
cerfaFields['alinea9']                        = '';
cerfaFields['alinea10']                       = '';
cerfaFields['alinea0']                        = '';
cerfaFields['alinea11']                       = '';
cerfaFields['designation0']                   = '';
cerfaFields['designation1']                   = '';
cerfaFields['designation3']                   = '';
cerfaFields['designation4']                   = '';
cerfaFields['designation5']                   = '';
cerfaFields['designation6']                   = '';
cerfaFields['designation7']                   = '';
cerfaFields['designation8']                   = '';
cerfaFields['designation9']                   = '';
cerfaFields['designation10']                  = '';
cerfaFields['designation2']                   = '';
cerfaFields['designation11']                  = '';
cerfaFields['capacite1']                      = '';
cerfaFields['capacite2']                      = '';
cerfaFields['capacite3']                      = '';
cerfaFields['capacite4']                      = '';
cerfaFields['capacite5']                      = '';
cerfaFields['capacite6']                      = '';
cerfaFields['capacite7']                      = '';
cerfaFields['capacite8']                      = '';
cerfaFields['capacite9']                      = '';
cerfaFields['capacite10']                     = '';
cerfaFields['capacite0']                      = '';
cerfaFields['capacite11']                     = '';
cerfaFields['unite1']                         = '';
cerfaFields['unite2']                         = '';
cerfaFields['unite3']                         = '';
cerfaFields['unite4']                         = '';
cerfaFields['unite5']                         = '';
cerfaFields['unite6']                         = '';
cerfaFields['unite7']                         = '';
cerfaFields['unite8']                         = '';
cerfaFields['unite9']                         = '';
cerfaFields['unite10']                        = '';
cerfaFields['unite0']                         = '';
cerfaFields['unite11']                        = '';
cerfaFields['regime1']                        = '';
cerfaFields['regime2']                        = '';
cerfaFields['regime3']                        = '';
cerfaFields['regime4']                        = '';
cerfaFields['regime5']                        = '';
cerfaFields['regime6']                        = '';
cerfaFields['regime7']                        = '';
cerfaFields['regime8']                        = '';
cerfaFields['regime9']                        = '';
cerfaFields['regime10']                       = '';
cerfaFields['regime0']                        = '';
cerfaFields['regime11']                       = '';

for (var i= 0; i < $ds021PE6.natureVolumeActivite.natureVolumeActivite.size(); i++ ){
	cerfaFields['numRubrique'+i]             = $ds021PE6.natureVolumeActivite.natureVolumeActivite[i].numRubrique != null ? $ds021PE6.natureVolumeActivite.natureVolumeActivite[i].numRubrique : '' ;
	cerfaFields['alinea'+i]             	 = $ds021PE6.natureVolumeActivite.natureVolumeActivite[i].alinea != null ? $ds021PE6.natureVolumeActivite.natureVolumeActivite[i].alinea : '';
	cerfaFields['designation'+i]             = $ds021PE6.natureVolumeActivite.natureVolumeActivite[i].designation != null ? $ds021PE6.natureVolumeActivite.natureVolumeActivite[i].designation : '';
	cerfaFields['capacite'+i]             	 = $ds021PE6.natureVolumeActivite.natureVolumeActivite[i].capacite != null ? $ds021PE6.natureVolumeActivite.natureVolumeActivite[i].capacite : '';
	cerfaFields['unite'+i]             	     = $ds021PE6.natureVolumeActivite.natureVolumeActivite[i].unite != null ? $ds021PE6.natureVolumeActivite.natureVolumeActivite[i].unite : '';
	cerfaFields['regime'+i]             	     = $ds021PE6.natureVolumeActivite.natureVolumeActivite[i].regime != null ? $ds021PE6.natureVolumeActivite.natureVolumeActivite[i].regime : '';
}
cerfaFields['commentaireActivite']            = $ds021PE6.natureVolumeActivite.commentaires.commentaireActivite;

//Mode et conditions
cerfaFields['ouiPrelevement']                 = Value('id').of($ds021PE6.presentation1.modeCondition1.prelevementEau).eq("ouiPrelevement") ? true : false;
cerfaFields['nonPrelevement']                 = Value('id').of($ds021PE6.presentation1.modeCondition1.prelevementEau).eq("nonPrelevement") ? true : false;
cerfaFields['volumeReseauPublic']             = $ds021PE6.presentation1.modeCondition1.volumeReseauPublic;
cerfaFields['volumeMilieuNaturel']            = $ds021PE6.presentation1.modeCondition1.volumeMilieuNaturel;
cerfaFields['volumeForage']                   = $ds021PE6.presentation1.modeCondition1.volumeForage;
cerfaFields['ouiMilieuNaturel']               = Value('id').of($ds021PE6.presentation1.modeCondition1.modePrelevementEau).contains("ouiMilieuNaturel") ? true : false;
cerfaFields['ouiForageDixMetres']             = Value('id').of($ds021PE6.presentation1.modeCondition1.ouiForageDix).eq("ouiForageDixMetres") ? true : false;
cerfaFields['nonForageDixMetres']             = Value('id').of($ds021PE6.presentation1.modeCondition1.ouiForageDix).eq("nonForageDixMetres") ? true : false;
cerfaFields['ouiForage']                      = Value('id').of($ds021PE6.presentation1.modeCondition1.modePrelevementEau).contains("ouiForage") ? true : false;
cerfaFields['ouiAutres']                      = Value('id').of($ds021PE6.presentation1.modeCondition1.modePrelevementEau).contains("ouiAutres") ? true : false;
cerfaFields['autresModes']                    = $ds021PE6.presentation1.modeCondition1.autresModes;
cerfaFields['ouiReseauPublic']                = Value('id').of($ds021PE6.presentation1.modeCondition1.modePrelevementEau).contains("ouiReseauPublic") ? true : false;






//Rejet
cerfaFields['nonRejet']                       = Value('id').of($ds021PE6.presentation2.modeCondition2.rejetEau).eq("nonRejet") ? true : false;
cerfaFields['ouiRejet']                       = Value('id').of($ds021PE6.presentation2.modeCondition2.rejetEau).eq("ouiRejet") ? true : false;
cerfaFields['commentaireOrigine']             = $ds021PE6.presentation2.modeCondition2.commentaireOrigine;
cerfaFields['commentairesRejetEau']           = $ds021PE6.presentation2.modeCondition2.commentairesRejetEau;
cerfaFields['reseauAssainissement']           = Value('id').of($ds021PE6.presentation2.modeCondition2.exutoireEauResiduaires).eq("reseauAssainissement") ? true : false;
cerfaFields['precisionsTraitement']           = $ds021PE6.presentation2.modeCondition2.precisionsTraitement;
cerfaFields['volumeAnnuel']                   = $ds021PE6.presentation2.modeCondition2.volumeAnnuel;
cerfaFields['milieuNaturel']                  = Value('id').of($ds021PE6.presentation2.modeCondition2.exutoireEauResiduaires).eq("milieuNaturel") ? true : false;


//Epandage
cerfaFields['origineMatieresEpandues']        = $ds021PE6.presentation3.modeCondition3.origineMatieresEpandues;
cerfaFields['ilotPAC']                        = $ds021PE6.presentation3.modeCondition3.ilotPAC;
cerfaFields['surfacePlanEpandage']            = $ds021PE6.presentation3.modeCondition3.surfacePlanEpandage;
cerfaFields['quantiteAzoteInscrite']          = $ds021PE6.presentation3.modeCondition3.quantiteAzoteInscrite;
cerfaFields['azoteTerre']                     = $ds021PE6.presentation3.modeCondition3.azoteTerre;
cerfaFields['azoteTerreDispo']                = $ds021PE6.presentation3.modeCondition3.azoteTerreDispo;
cerfaFields['azoteInstallation']              = $ds021PE6.presentation3.modeCondition3.azoteInstallation;
cerfaFields['azoteTiers']                     = $ds021PE6.presentation3.modeCondition3.azoteTiers;
cerfaFields['capaciteStockage']               = $ds021PE6.presentation3.modeCondition3.capaciteStockage;
cerfaFields['ouiEpendage']                    = Value('id').of($ds021PE6.presentation3.modeCondition3.epandageDechets).eq("ouiEpendage") ? true : false;
cerfaFields['nonEpendage']                    = Value('id').of($ds021PE6.presentation3.modeCondition3.epandageDechets).eq("nonEpendage") ? true : false;

//Rejet atmosphere
cerfaFields['dispositifCapatation']           = $ds021PE6.presentation4.modeCondition4.dispositifCapatation;
cerfaFields['commentaireRejet']               = $ds021PE6.presentation4.modeCondition4.commentaireRejet;
cerfaFields['ouiRejetAtmosphere']             = Value('id').of($ds021PE6.presentation4.modeCondition4.rejetAtmosphere).eq("ouiRejetAtmosphere") ? true : false;
cerfaFields['nonRejetAtmosphere']             = Value('id').of($ds021PE6.presentation4.modeCondition4.rejetAtmosphere).eq("nonRejetAtmosphere") ? true : false;
cerfaFields['origineNatureDechet']            = $ds021PE6.presentation4.modeCondition4.origineNatureDechet;


//Elimination dechets
cerfaFields['typeDechetResidus']              = $ds021PE6.presentation5.presentation5.typeDechetResidus;
cerfaFields['ouiCollecte']                    = Value('id').of($ds021PE6.presentation5.presentation5.collecteDechets).eq("ouiCollecte") ? true : false;
cerfaFields['nonCollecte']                    = Value('id').of($ds021PE6.presentation5.presentation5.collecteDechets).eq("nonCollecte") ? true : false;


//disposition
cerfaFields['dispositionPrevues']             = $ds021PE6.presentation6.presentation6.dispositionPrevues;
cerfaFields['moyenSecours']                   = $ds021PE6.presentation6.presentation6.moyenSecours;
cerfaFields['priseEau']                       = Value('id').of($ds021PE6.presentation6.presentation6.capaciteEauLutte).eq("priseEau") ? true : false;
cerfaFields['autreCapaciteEau']               = Value('id').of($ds021PE6.presentation6.presentation6.capaciteEauLutte).eq("autreCapaciteEau") ? true : false;

//Demande agrément
cerfaFields['ouiTraitementDechets']           = Value('id').of($ds021PE6.demandeAgrement1.demandeAgrement.installationTraitement).eq("ouiTraitementDechets") ? true : false;
cerfaFields['nonTraitementDechets']           = Value('id').of($ds021PE6.demandeAgrement1.demandeAgrement.installationTraitement).eq("nonTraitementDechets") ? true : false;

//Dechets traiter
cerfaFields['nature0']                        = '';
cerfaFields['nature1']                        = '';
cerfaFields['nature2']                        = '';
cerfaFields['nature3']                        = '';
cerfaFields['nature4']                        = '';
cerfaFields['nature5']                        = '';
cerfaFields['nature6']                        = '';
cerfaFields['nature7']                        = '';
cerfaFields['nature8']                        = '';
cerfaFields['nature9']                        = '';
cerfaFields['nature10']                       = '';
cerfaFields['nature11']                       = '';
cerfaFields['nature12']                       = '';
cerfaFields['nature13']                       = '';
cerfaFields['nature14']                       = '';
cerfaFields['nature15']                       = '';
cerfaFields['nature16']                       = '';
cerfaFields['nature17']                       = '';
cerfaFields['nature18']                       = '';
cerfaFields['nature19']                       = '';
cerfaFields['nature20']                       = '';
cerfaFields['nature21']                       = '';
cerfaFields['nature22']                       = '';
cerfaFields['nature23']                       = '';
cerfaFields['nature24']                       = '';
cerfaFields['codification1']                  = '';
cerfaFields['codification2']                  = '';
cerfaFields['codification3']                  = '';
cerfaFields['codification4']                  = '';
cerfaFields['codification5']                  = '';
cerfaFields['codification6']                  = '';
cerfaFields['codification7']                  = '';
cerfaFields['codification8']                  = '';
cerfaFields['codification9']                  = '';
cerfaFields['codification10']                 = '';
cerfaFields['codification11']                 = '';
cerfaFields['codification12']                 = '';
cerfaFields['codification13']                 = '';
cerfaFields['codification14']                 = '';
cerfaFields['codification15']                 = '';
cerfaFields['codification16']                 = '';
cerfaFields['codification17']                 = '';
cerfaFields['codification18']                 = '';
cerfaFields['codification19']                 = '';
cerfaFields['codification20']                 = '';
cerfaFields['codification21']                 = '';
cerfaFields['codification22']                 = '';
cerfaFields['codification23']                 = '';
cerfaFields['codification0']                  = '';
cerfaFields['codification24']                 = '';
cerfaFields['type1']                          = '';
cerfaFields['type2']                          = '';
cerfaFields['type3']                          = '';
cerfaFields['type4']                          = '';
cerfaFields['type5']                          = '';
cerfaFields['type6']                          = '';
cerfaFields['type7']                          = '';
cerfaFields['type8']                          = '';
cerfaFields['type9']                          = '';
cerfaFields['type10']                         = '';
cerfaFields['type11']                         = '';
cerfaFields['type12']                         = '';
cerfaFields['type13']                         = '';
cerfaFields['type14']                         = '';
cerfaFields['type15']                         = '';
cerfaFields['type16']                         = '';
cerfaFields['type17']                         = '';
cerfaFields['type18']                         = '';
cerfaFields['type19']                         = '';
cerfaFields['type20']                         = '';
cerfaFields['type21']                         = '';
cerfaFields['type22']                         = '';
cerfaFields['type23']                         = '';
cerfaFields['type0']                          = '';
cerfaFields['type24']                         = '';
cerfaFields['codificationTraitement1']        = '';
cerfaFields['codificationTraitement2']        = '';
cerfaFields['codificationTraitement3']        = '';
cerfaFields['codificationTraitement4']        = '';
cerfaFields['codificationTraitement5']        = '';
cerfaFields['codificationTraitement6']        = '';
cerfaFields['codificationTraitement7']        = '';
cerfaFields['codificationTraitement8']        = '';
cerfaFields['codificationTraitement9']        = '';
cerfaFields['codificationTraitement10']       = '';
cerfaFields['codificationTraitement11']       = '';
cerfaFields['codificationTraitement12']       = '';
cerfaFields['codificationTraitement13']       = '';
cerfaFields['codificationTraitement14']       = '';
cerfaFields['codificationTraitement15']       = '';
cerfaFields['codificationTraitement16']       = '';
cerfaFields['codificationTraitement17']       = '';
cerfaFields['codificationTraitement18']       = '';
cerfaFields['codificationTraitement19']       = '';
cerfaFields['codificationTraitement20']       = '';
cerfaFields['codificationTraitement21']       = '';
cerfaFields['codificationTraitement22']       = '';
cerfaFields['codificationTraitement23']       = '';
cerfaFields['codificationTraitement0']        = '';
cerfaFields['codificationTraitement24']       = '';
cerfaFields['quantite1']                      = '';
cerfaFields['quantite2']                      = '';
cerfaFields['quantite3']                      = '';
cerfaFields['quantite4']                      = '';
cerfaFields['quantite5']                      = '';
cerfaFields['quantite6']                      = '';
cerfaFields['quantite7']                      = '';
cerfaFields['quantite8']                      = '';
cerfaFields['quantite9']                      = '';
cerfaFields['quantite10']                     = '';
cerfaFields['quantite11']                     = '';
cerfaFields['quantite12']                     = '';
cerfaFields['quantite13']                     = '';
cerfaFields['quantite14']                     = '';
cerfaFields['quantite15']                     = '';
cerfaFields['quantite16']                     = '';
cerfaFields['quantite17']                     = '';
cerfaFields['quantite18']                     = '';
cerfaFields['quantite19']                     = '';
cerfaFields['quantite20']                     = '';
cerfaFields['quantite21']                     = '';
cerfaFields['quantite22']                     = '';
cerfaFields['quantite23']                     = '';
cerfaFields['quantite0']                      = '';
cerfaFields['quantite24']                     = '';
cerfaFields['commentaireTraitementDechets']   = $ds021PE6.demandeAgrement1.demandeAgrement3.commentaireTraitementDechets;

for (var i= 0; i < $ds021PE6.demandeAgrement1.demandeAgrement2.size(); i++ ){
	cerfaFields['nature'+i]             		= $ds021PE6.demandeAgrement1.demandeAgrement2[i].nature != null ? $ds021PE6.demandeAgrement1.demandeAgrement2[i].nature : '' ;
	cerfaFields['codification'+i]             	= $ds021PE6.demandeAgrement1.demandeAgrement2[i].codification != null ? $ds021PE6.demandeAgrement1.demandeAgrement2[i].codification : '';
	cerfaFields['type'+i]                       = $ds021PE6.demandeAgrement1.demandeAgrement2[i].type != null ? $ds021PE6.demandeAgrement1.demandeAgrement2[i].type : '';
	cerfaFields['codificationTraitement'+i]     = $ds021PE6.demandeAgrement1.demandeAgrement2[i].codificationTraitement != null ? $ds021PE6.demandeAgrement1.demandeAgrement2[i].codificationTraitement : '';
	cerfaFields['quantite'+i]             	    = $ds021PE6.demandeAgrement1.demandeAgrement2[i].quantite != null ? $ds021PE6.demandeAgrement1.demandeAgrement2[i].quantite : '';
	
}

//Natura
cerfaFields['ouiNatura']                      = Value('id').of($ds021PE6.natura.natura.reference).eq("ouiNatura") ? true : false;
cerfaFields['nonNatura']                      = Value('id').of($ds021PE6.natura.natura.reference).eq("nonNatura") ? true : false;

//Prescription
cerfaFields['ouiPrescription']                = Value('id').of($ds021PE6.signature.signature.demandeModif).eq("ouiPrescription") ? true : false;
cerfaFields['nonPrescription']                = Value('id').of($ds021PE6.signature.signature.demandeModif).eq("nonPrescription") ? true : false;
cerfaFields['dateSignature']                  = $ds021PE6.signature.signature.dateSignature;
cerfaFields['villeSignature']                 = $ds021PE6.signature.signature.villeSignature;
cerfaFields['civNomPrenom']                   = civNomPrenom;




//var pdfModel = nash.doc.load('models/cerfa_11542-05.pdf').apply(formFields);

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $ds044PE1.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		date: $ds021PE6.signature.signature.dateSignature,
		autoriteHabilitee : destinataire,
		demandeContexte : "Achat-Vente d'animaux d'agrément ou de compagnie - Déclaration d’installation classée pour la protection de l’environnement.",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Cerfa n15271_02.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPlanCadastre);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPlanEnsemble);
appendPj($attachmentPreprocess.attachmentPreprocess.pjRubriques);
appendPj($attachmentPreprocess.attachmentPreprocess.pjEpurationEvacuation);



/*
 *PJ Natura : oui
 */
var pj=$ds021PE6.natura.natura.reference;
if(Value('id').of(pj).contains('ouiNatura')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjDossierEval);
}


/*
 *PJ Note interaction si coché : oui régime Autorisation
 */
var pj=$ds021PE6.description.description.autorisation;
if(Value('id').of(pj).contains('ouiAutorisation')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjregimeAutorisation);
}

/*
 *PJ Modif : oui
 */
var pj=$ds021PE6.signature.signature.demandeModif;
if(Value('id').of(pj).contains('ouiPrescription')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjModif);
}


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Achat-Vente-animaux_ICPE.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Garde de chiens et chats - Déclaration d\'installation classée pour la protection de l\'environnement.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Garde de chiens et chats - Déclaration d\'installation classée pour la protection de l\'environnement.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});