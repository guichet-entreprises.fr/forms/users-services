function pad(s) { return (s < 10) ? '0' + s : s; } 
var cerfaFields = {};

cerfaFields['actualisation']             = true;
cerfaFields['premiereDeclaration']       = false;
var civNomPrenom = $ds021PE2.idExploitant.idExploitant1.nomExploitant + ' ' + $ds021PE2.idExploitant.idExploitant1.prenomExploitant;

//identification declarant
cerfaFields['siret']                     = $ds021PE2.idDeclarant.idDeclarant1.siret;
cerfaFields['raisonSociale']             = $ds021PE2.idDeclarant.idDeclarant1.raisonSociale;
cerfaFields['adresseEtablissement']      = $ds021PE2.idDeclarant.idDeclarant1.adresseEtablissement;
cerfaFields['codePostal']                = $ds021PE2.idDeclarant.idDeclarant1.codePostal;
cerfaFields['commune']                   = $ds021PE2.idDeclarant.idDeclarant1.commune;

//identification exploitant
cerfaFields['nomExploitant']             = $ds021PE2.idExploitant.idExploitant1.nomExploitant;
cerfaFields['prenomExploitant']          = $ds021PE2.idExploitant.idExploitant1.prenomExploitant;
cerfaFields['numTel']                    = $ds021PE2.idExploitant.idExploitant1.numTel;
cerfaFields['numMobile']                 = $ds021PE2.idExploitant.idExploitant1.numMobile;
cerfaFields['email']                     = $ds021PE2.idExploitant.idExploitant1.email;

//Personnel

cerfaFields['nomPrenom0']                = '';
cerfaFields['nomPrenom1']                = '';
cerfaFields['nomPrenom2']                = '';
cerfaFields['nomPrenom3']                = '';
cerfaFields['nomPrenom4']                = '';
cerfaFields['fonctionAttribution0']      = '';
cerfaFields['fonctionAttribution1']      = '';
cerfaFields['fonctionAttribution2']      = '';
cerfaFields['fonctionAttribution3']      = '';
cerfaFields['fonctionAttribution4']      = '';

for (var i= 0; i < $ds021PE2.personnel.personnel.size(); i++ ){
	cerfaFields['nomPrenom'+i]             			 = $ds021PE2.personnel.personnel[i].nomPrenom != null ? $ds021PE2.personnel.personnel[i].nomPrenom : '' ;
	cerfaFields['fonctionAttribution'+i]             = $ds021PE2.personnel.personnel[i].fonctionAttribution != null ? $ds021PE2.personnel.personnel[i].fonctionAttribution : '';
	
}


//Activite de l'établissement

cerfaFields['vente']                     = Value('id').of($ds021PE2.activites.activites.activiteAnimal).contains("vente") ? true : false;
cerfaFields['presentationPublic']        = Value('id').of($ds021PE2.activites.activites.activiteAnimal).contains("presentationPublic") ? true : false;

cerfaFields['elevage']                   = Value('id').of($ds021PE2.activites.activites.activiteChienChats).contains("elevage") ? true : false;
cerfaFields['gestionFourriere']          = Value('id').of($ds021PE2.activites.activites.activiteChienChats).contains("gestionFourriere") ? true : false;
cerfaFields['gestionRefuge']             = Value('id').of($ds021PE2.activites.activites.activiteChienChats).contains("gestionRefuge") ? true : false;
cerfaFields['transit']                   = Value('id').of($ds021PE2.activites.activites.activiteChienChats).contains("transit") ? true : false;
cerfaFields['gardePension']              = Value('id').of($ds021PE2.activites.activites.activiteChienChats).contains("gardePension") ? true : false;
cerfaFields['education']                 = Value('id').of($ds021PE2.activites.activites.activiteChienChats).contains("education") ? true : false;
cerfaFields['dressage']                  = Value('id').of($ds021PE2.activites.activites.activiteChienChats).contains("dressage") ? true : false;

//especes hebergees
cerfaFields['capaciteChiens']            = $ds021PE2.especes.especesHebergees.capaciteChiens;
cerfaFields['capaciteChats']             = $ds021PE2.especes.especesHebergees.capaciteChats;
cerfaFields['capaciteFurets']            = $ds021PE2.especes.especesHebergees.capaciteFurets;
cerfaFields['capaciteLapins']            = $ds021PE2.especes.especesHebergees.capaciteLapins;
cerfaFields['capaciteRongeurs']          = $ds021PE2.especes.especesHebergees.capaciteRongeurs;
cerfaFields['capaciteOiseaux']           = $ds021PE2.especes.especesHebergees.capaciteOiseaux;
cerfaFields['capacitePoissons']          = $ds021PE2.especes.especesHebergees.capacitePoissons;

// Autres espèces hébergées
cerfaFields['especeHebergee0']      = '';
cerfaFields['especeHebergee1']      = '';
cerfaFields['especeHebergee2']      = '';
cerfaFields['especeHebergee3']      = '';
cerfaFields['capaciteAutres0']      = '';
cerfaFields['capaciteAutres1']      = '';
cerfaFields['capaciteAutres2']      = '';
cerfaFields['capaciteAutres3']      = '';


for (var i= 0; i < $ds021PE2.especes2.especes2.size(); i++ ){
	cerfaFields['especeHebergee'+i]             = $ds021PE2.especes2.especes2[i].especeHebergee != null ? $ds021PE2.especes2.especes2[i].especeHebergee : '' ;
	cerfaFields['capaciteAutres'+i]             = $ds021PE2.especes2.especes2[i].capaciteAutres != null ? $ds021PE2.especes2.especes2[i].capaciteAutres : '';
	
}

//Vétérinaire
cerfaFields['prenomVeterinaire']         = $ds021PE2.idVeterinaire.idVeterinaire.prenomVeterinaire;
cerfaFields['adresseVeterinaire']        = $ds021PE2.idVeterinaire.idVeterinaire.adresseVeterinaire;
cerfaFields['communeVeterinaire']        = $ds021PE2.idVeterinaire.idVeterinaire.communeVeterinaire;
cerfaFields['codePostalVeterinaire']     = $ds021PE2.idVeterinaire.idVeterinaire.codePostalVeterinaire;
cerfaFields['nomVeterinaire']            = $ds021PE2.idVeterinaire.idVeterinaire.nomVeterinaire;


//Signature
cerfaFields['certificationDeclarant']    = $ds021PE2.signature.signature.certificationDeclarant;
cerfaFields['certificationInformations'] = $ds021PE2.signature.signature.certificationInformations;
cerfaFields['engagementDirection']       = $ds021PE2.signature.signature.engagementDirection;
cerfaFields['reconnaissance1']           = $ds021PE2.signature.signature.reconnaissance1;
cerfaFields['information1']              = $ds021PE2.signature.signature.information1;
cerfaFields['civNomPrenom']              = civNomPrenom;


if($ds021PE2.signature.signature.date != null) {
var dateTemp = new Date(parseInt ($ds021PE2.signature.signature.date.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
cerfaFields['jour']   = day;
cerfaFields['mois']   = month;
cerfaFields['annee']  = year;
}




//var pdfModel = nash.doc.load('models/cerfa_11542-05.pdf').apply(formFields);

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $ds021PE2.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		date: $ds021PE2.signature.signature.date,
		autoriteHabilitee : 'Direction départementale de la cohésion sociale et de la protection des populations',
		demandeContexte : "Achat / Vente d'animaux d'agrément ou de compagnie -  Modification de la déclaration d'ouverture de l'établissement détenant des animaux domestiques.",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Cerfa n15045_02.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjEngagementVeterinaire);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Achat-Vente_animaux_modification.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Achat / Vente d\'animaux d\'agrément ou de compagnie - Modification de la déclaration d\'ouverture de l\'établissement détenant des animaux domestiques.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Achat / Vente d\'animaux d\'agrément ou de compagnie -  Modification de la déclaration d\'ouverture de l\'établissement détenant des animaux domestiques.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});