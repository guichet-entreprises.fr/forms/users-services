function pad(s) { return (s < 10) ? '0' + s : s; }

var cerfaFields = {};

var nomPrenom=$ds056PE4.identificationDemandeur.identificationDemandeur.civilite +' '+$ds056PE4.identificationDemandeur.identificationDemandeur.nomPrenomDemandeur;


/*Identification du demandeur*/

cerfaFields['nomPrenomDemandeur']  = $ds056PE4.identificationDemandeur.identificationDemandeur.nomPrenomDemandeur;

if($ds056PE4.identificationDemandeur.identificationDemandeur.dateNaissanceDemandeur != null) {
var dateTemp = new Date(parseInt ($ds056PE4.identificationDemandeur.identificationDemandeur.dateNaissanceDemandeur.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	cerfaFields['jourNaissance']  	= day;
	cerfaFields['moisNaissance']    = month;
	cerfaFields['anneeNaissance']   = year;
}
cerfaFields['lieuNaissance']                      = $ds056PE4.identificationDemandeur.identificationDemandeur.lieuNaissanceDemandeur;
cerfaFields['numeroAdresse']                      = ($ds056PE4.identificationDemandeur.identificationDemandeur.numeroAdresse != null ? $ds056PE4.identificationDemandeur.identificationDemandeur.numeroAdresse : '');
cerfaFields['codePostal']                         = ($ds056PE4.identificationDemandeur.identificationDemandeur.codePostalAdresse != null ? $ds056PE4.identificationDemandeur.identificationDemandeur.codePostalAdresse : '');
cerfaFields['voieAdresse']                        = $ds056PE4.identificationDemandeur.identificationDemandeur.voieAdresse;
cerfaFields['communeAdresse']                     = $ds056PE4.identificationDemandeur.identificationDemandeur.communeAdresse;

/*Activités pratiquées*/

cerfaFields['ouiDetentionSimple']                 = Value('id').of($ds056PE4.activitesPratiquees.activitesPratiquees.detentionSimple).eq('ouiDetentionSimple')? true : false;
cerfaFields['nonDetentionSimple']                 = Value('id').of($ds056PE4.activitesPratiquees.activitesPratiquees.detentionSimple).eq('nonDetentionSimple')? true : false;
cerfaFields['ouiUtilisation']                     = Value('id').of($ds056PE4.activitesPratiquees.activitesPratiquees.utilisation).eq('ouiUtilisation')? true : false;
cerfaFields['nonUtilisation']                     = Value('id').of($ds056PE4.activitesPratiquees.activitesPratiquees.utilisation).eq('nonUtilisation')? true : false;
cerfaFields['ouiTransport']                       = Value('id').of($ds056PE4.activitesPratiquees.activitesPratiquees.transport).eq('ouiTransport')? true : false;
cerfaFields['nonTransport']                       = Value('id').of($ds056PE4.activitesPratiquees.activitesPratiquees.transport).eq('nonTransport')? true : false;
cerfaFields['conditionsUtilisation']              = $ds056PE4.activitesPratiquees.activitesPratiquees.conditionsUtilisation;
cerfaFields['finsTransport']                      = $ds056PE4.activitesPratiquees.activitesPratiquees.finsTransport;


/*Alimentation*/

for (var i=$ds056PE4.especesAnimales.especesAnimales.size(); i < 5; i++ ){
	cerfaFields['nomVernaculaire'+i]         	= '';
	cerfaFields['nomScientifique'+i]        	= '';
	cerfaFields['specimenMale'+i]        		= '';
	cerfaFields['specimenFemelle'+i]       		= '';
	cerfaFields['specimenIndetermine'+i]        = '';
}

for (var i= 0; i < $ds056PE4.especesAnimales.especesAnimales.size(); i++ ){
	cerfaFields['nomVernaculaire'+i]         = $ds056PE4.especesAnimales.especesAnimales[i].nomVernaculaire;
	cerfaFields['nomScientifique'+i]       	 = $ds056PE4.especesAnimales.especesAnimales[i].nomScientifique;
	cerfaFields['specimenMale'+i]       	 = $ds056PE4.especesAnimales.especesAnimales[i].specimenMale;
	cerfaFields['specimenFemelle'+i]         = $ds056PE4.especesAnimales.especesAnimales[i].specimenFemelle;
	cerfaFields['specimenIndetermine'+i]     = $ds056PE4.especesAnimales.especesAnimales[i].specimenIndetermine;
}

/*Modalités d'acquisition des compétences*/

cerfaFields['formationInitiale']                  = $ds056PE4.modaliteAcquisition.modaliteAcquisition.formationInitiale;
cerfaFields['stageElevage']                       = $ds056PE4.modaliteAcquisition.modaliteAcquisition.stageElevage;
cerfaFields['activiteAssociativeProfessionnelle'] = $ds056PE4.modaliteAcquisition.modaliteAcquisition.activiteAssociativeProfessionnelle;
cerfaFields['bibliographie']                      = $ds056PE4.modaliteAcquisition.modaliteAcquisition.bibliographie;

cerfaFields['voieAdresseDetention']               = $ds056PE4.descritpionDetention.descritpionDetention.voieAdresseDetention;
cerfaFields['numeroAdresseDetention']             = $ds056PE4.descritpionDetention.descritpionDetention.numeroAdresseDetention;
cerfaFields['codePostalDetention']                = $ds056PE4.descritpionDetention.descritpionDetention.codePostalDetention;
cerfaFields['communeAdresseDetention']            = $ds056PE4.descritpionDetention.descritpionDetention.communeAdresseDetention;

/*Annexe*/

cerfaFields['nomScientifiqueEspece']              = $ds056PE4.groupeEspeces.groupeEspeces.nomScientifiqueEspece;
cerfaFields['cohabitationEspecesCoche']           = $ds056PE4.groupeEspeces.groupeEspeces.cohabitationEspecesCoche;
cerfaFields['cohabitationEspecesPrecision']       = $ds056PE4.groupeEspeces.groupeEspeces.cohabitationEspecesPrecision;
cerfaFields['origineGeographique']                = $ds056PE4.groupeEspeces.groupeEspeces.origineGeographique;
cerfaFields['biotope']                            = $ds056PE4.groupeEspeces.groupeEspeces.biotope;
cerfaFields['modeOrganisation']                   = $ds056PE4.groupeEspeces.groupeEspeces.modeOrganisation;
cerfaFields['dangerHomme']                        = $ds056PE4.groupeEspeces.groupeEspeces.dangerHomme;
cerfaFields['dangerFaune']                        = $ds056PE4.groupeEspeces.groupeEspeces.dangerFaune;
cerfaFields['statutJuridique']                    = $ds056PE4.groupeEspeces.groupeEspeces.statutJuridique;

/*Alimentation*/


for (var i=$ds056PE4.alimentation.alimentation.size (); i < 7; i++ ){
	cerfaFields['aliment'+i]         = '';
	cerfaFields['frequence'+i]       = '';
}

for (var i= 0; i < $ds056PE4.alimentation.alimentation.size(); i++ ){     
	cerfaFields['aliment'+i]         = $ds056PE4.alimentation.alimentation[i].aliment;
	cerfaFields['frequence'+i]       = $ds056PE4.alimentation.alimentation[i].frequence;
}


cerfaFields['autreParticularite']                 = $ds056PE4.alimentation1.alimentation1.autreParticularite;
cerfaFields['ouiAutreParticularite']              = $ds056PE4.alimentation1.alimentation1.ouiAutreParticularite;

/*Reproduction*/

cerfaFields['ageMaturite']                        = $ds056PE4.reproduction.reproduction.ageMaturite;
cerfaFields['saisonReproduction']                 = $ds056PE4.reproduction.reproduction.saisonReproduction;
cerfaFields['modaliteChoixReproducteur']          = $ds056PE4.reproduction.reproduction.modaliteChoixReproducteur;
cerfaFields['modaliteEntretienReproducteur']      = $ds056PE4.reproduction.reproduction.modaliteEntretienReproducteur;
cerfaFields['ouiDureeGestation']                  = $ds056PE4.reproduction.reproduction.ouiDureeGestation;
cerfaFields['dureeGestation']                     = $ds056PE4.reproduction.reproduction.dureeGestation;
cerfaFields['ouiIncubation']                      = $ds056PE4.reproduction.reproduction.ouiIncubation;
cerfaFields['dureeIncubation']                    = $ds056PE4.reproduction.reproduction.dureeIncubation;
cerfaFields['ouiIncubationNaturelle']             = $ds056PE4.reproduction.reproduction.ouiIncubationNaturelle;
cerfaFields['ouiIncubationArtificielle']          = $ds056PE4.reproduction.reproduction.ouiIncubationArtificielle;
cerfaFields['temperature']                        = $ds056PE4.reproduction.reproduction.temperature;
cerfaFields['retournementOeuf']                   = $ds056PE4.reproduction.reproduction.retournementOeuf;
cerfaFields['hygrometrie']                        = $ds056PE4.reproduction.reproduction.hygrometrie;
cerfaFields['nombreJeuneFemelle']                 = $ds056PE4.reproduction.reproduction.nombreJeuneFemelle;
cerfaFields['modaliteEntretienJeunes']            = $ds056PE4.reproduction.reproduction.modaliteEntretienJeunes;
cerfaFields['destinationJeunes']                  = $ds056PE4.reproduction.reproduction.destinationJeunes;
cerfaFields['ouiAutresParticularitesPrecautions'] = $ds056PE4.reproduction.reproduction.ouiAutresParticularitesPrecautions;
cerfaFields['autresParticularitesPrecautions']    = $ds056PE4.reproduction.reproduction.autresParticularitesPrecautions;

/*Installation d'hebergement des animaux*/

cerfaFields['installationCage']                   = Value('id').of($ds056PE4.hebergement.hebergement.natureInstallation).eq('installationCage')? true : false;
cerfaFields['installationEnclos']                 = Value('id').of($ds056PE4.hebergement.hebergement.natureInstallation).eq('installationEnclos')? true : false;
cerfaFields['installationVoliere']                = Value('id').of($ds056PE4.hebergement.hebergement.natureInstallation).eq('installationVoliere')? true : false;
cerfaFields['installationTerrarium']              = Value('id').of($ds056PE4.hebergement.hebergement.natureInstallation).eq('installationTerrarium')? true : false;
cerfaFields['installationBassin']                 = Value('id').of($ds056PE4.hebergement.hebergement.natureInstallation).eq('installationBassin')? true : false;
cerfaFields['installationAutre']                  = Value('id').of($ds056PE4.hebergement.hebergement.natureInstallation).eq('installationAutre')? true : false;
cerfaFields['autreCommentaire']                   = $ds056PE4.hebergement.hebergement.autreCommentaire;
cerfaFields['natureMateriaux2']                   = $ds056PE4.hebergement.hebergement.natureMateriaux2;
cerfaFields['nombreAnimaux']                  	  = $ds056PE4.hebergement.hebergement.nombreAnimaux;
cerfaFields['dimensionLongueur']                  = $ds056PE4.hebergement.hebergement.dimensionLongueur;
cerfaFields['dimensionLargeur']                   = $ds056PE4.hebergement.hebergement.dimensionLargeur;
cerfaFields['dimensionHauteur']                   = $ds056PE4.hebergement.hebergement.dimensionHauteur;
cerfaFields['materiauxParois']                    = $ds056PE4.hebergement.hebergement.materiauxParois;
cerfaFields['natureSol']                          = $ds056PE4.hebergement.hebergement.natureSol;
cerfaFields['ouiClotures']                        = $ds056PE4.hebergement.hebergement.ouiClotures;
cerfaFields['barrierePlus']                       = $ds056PE4.hebergement.hebergement.barrierePlus;
cerfaFields['natureMateriaux']                    = $ds056PE4.hebergement.hebergement.natureMateriaux;
cerfaFields['ouiEcartementPoteaux']               = $ds056PE4.hebergement.hebergement.ouiEcartementPoteaux;
cerfaFields['ecartementMateriaux']                = $ds056PE4.hebergement.hebergement.ecartementMateriaux;
cerfaFields['ouiEcartementBarreaux']              = $ds056PE4.hebergement.hebergement.ouiEcartementBarreaux;
cerfaFields['ecartementBarreaux']                 = $ds056PE4.hebergement.hebergement.ecartementBarreaux;
cerfaFields['ouiMaille']                          = $ds056PE4.hebergement.hebergement.ouiMaille;
cerfaFields['mailleGrillage']                     = $ds056PE4.hebergement.hebergement.mailleGrillage;
cerfaFields['hauteurSol']                         = $ds056PE4.hebergement.hebergement.hauteurSol;
cerfaFields['profondeurSol']                      = $ds056PE4.hebergement.hebergement.profondeurSol;
cerfaFields['ouiBarrieresPlus']                   = $ds056PE4.hebergement.hebergement.ouiBarrieresPlus;
cerfaFields['barrierePlus2']                      = $ds056PE4.hebergement.hebergement.barrierePlus2;
cerfaFields['ouiAbris']                           = $ds056PE4.hebergement.hebergement.ouiAbris;
cerfaFields['natureMateriaux']                    = $ds056PE4.hebergement.hebergement.natureMateriaux;
cerfaFields['largeurMateriaux']                   = $ds056PE4.hebergement.hebergement.largeurMateriaux;
cerfaFields['longueurMateriaux']                  = $ds056PE4.hebergement.hebergement.longueurMateriaux;
cerfaFields['hauteurMateriaux']                   = $ds056PE4.hebergement.hebergement.hauteurMateriaux;
cerfaFields['ouiChauffage']                       = $ds056PE4.hebergement.hebergement.ouiChauffage;
cerfaFields['chauffageTypeTemp']                  = $ds056PE4.hebergement.hebergement.chauffageTypeTemp;
cerfaFields['ouiEclairementArtificiel']           = $ds056PE4.hebergement.hebergement.ouiEclairementArtificiel;
cerfaFields['eclairementArtificiel']              = $ds056PE4.hebergement.hebergement.eclairementArtificiel;
cerfaFields['ouiSystemeVentilation']              = $ds056PE4.hebergement.hebergement.ouiSystemeVentilation;
cerfaFields['systemeVentilation']                 = $ds056PE4.hebergement.hebergement.systemeVentilation;
cerfaFields['ouiTauxHygrometrie']                 = $ds056PE4.hebergement.hebergement.ouiTauxHygrometrie;
cerfaFields['tauxHygrométrie'] 					  = $ds056PE4.hebergement.hebergement.tauxHygrométrie; 
cerfaFields['ouiMoyenFermeture']                  = $ds056PE4.hebergement.hebergement.ouiMoyenFermeture;
cerfaFields['moyenFermetureInstallation']         = $ds056PE4.hebergement.hebergement.moyenFermetureInstallation;
cerfaFields['ouiAccessoireAmenagement']           = $ds056PE4.hebergement.hebergement.ouiAccessoireAmenagement;
cerfaFields['ouiBassin']                          = $ds056PE4.hebergement.hebergement.ouiBassin;
cerfaFields['bassinDimension']                    = $ds056PE4.hebergement.hebergement.bassinDimension;
cerfaFields['modaliteMaitriseEau']                = $ds056PE4.hebergement.hebergement.modaliteMaitriseEau;
cerfaFields['ouiArbres']                          = $ds056PE4.hebergement.hebergement.ouiArbres;
cerfaFields['planteArbre']                        = $ds056PE4.hebergement.hebergement.planteArbre;
cerfaFields['ouiLitiere']                         = $ds056PE4.hebergement.hebergement.ouiLitiere;
cerfaFields['litiere']                            = $ds056PE4.hebergement.hebergement.litiere;
cerfaFields['ouiAutres']                          = $ds056PE4.hebergement.hebergement.ouiAutres;
cerfaFields['autresAccessoires']                  = $ds056PE4.hebergement.hebergement.autresAccessoires;
cerfaFields['ouiMaterielCapture']                 = $ds056PE4.hebergement.hebergement.ouiMaterielCapture;
cerfaFields['materielCapture']                    = $ds056PE4.hebergement.hebergement.materielCapture;
cerfaFields['ouiLocalQuarantaine']                = $ds056PE4.hebergement.hebergement.ouiLocalQuarantaine;
cerfaFields['localQuarantaine']                   = $ds056PE4.hebergement.hebergement.localQuarantaine;
cerfaFields['ouiAutresParticulariteInstallation'] = $ds056PE4.hebergement.hebergement.ouiAutresParticulariteInstallation;
cerfaFields['particularite']                      = $ds056PE4.hebergement.hebergement.particularite;
cerfaFields['ouiDescriptionTransport']            = $ds056PE4.hebergement.hebergement.ouiDescriptionTransport;
cerfaFields['descriptionMoyenTransport']          = $ds056PE4.hebergement.hebergement.descriptionMoyenTransport;

/*Mesure d'hygiéne*/

cerfaFields['ouiNettoyage']                       = $ds056PE4.mesurehygiene.mesurehygiene.ouiNettoyage;
cerfaFields['methodeNettoyage']                   = $ds056PE4.mesurehygiene.mesurehygiene.methodeNettoyage;
cerfaFields['frequenceNettoyage']                 = $ds056PE4.mesurehygiene.mesurehygiene.frequenceNettoyage;
cerfaFields['produitEmployesNettoyage']           = $ds056PE4.mesurehygiene.mesurehygiene.produitEmployesNettoyage;
cerfaFields['ouiDesinfection']                    = $ds056PE4.mesurehygiene.mesurehygiene.ouiDesinfection;
cerfaFields['methodeDesinfection']                = $ds056PE4.mesurehygiene.mesurehygiene.methodeDesinfection;
cerfaFields['frequenceDesinfection']              = $ds056PE4.mesurehygiene.mesurehygiene.frequenceDesinfection;
cerfaFields['produitEmployesDesinfection']        = $ds056PE4.mesurehygiene.mesurehygiene.produitEmployesDesinfection;
cerfaFields['ouiEvacuationDechets']               = $ds056PE4.mesurehygiene.mesurehygiene.ouiEvacuationDechets;
cerfaFields['traitementDechet']                   = $ds056PE4.mesurehygiene.mesurehygiene.traitementDechet;
cerfaFields['destinationDechet']                  = $ds056PE4.mesurehygiene.mesurehygiene.destinationDechet;
cerfaFields['methodeEvacuation']                  = $ds056PE4.mesurehygiene.mesurehygiene.methodeEvacuation;
cerfaFields['frequenceEvacuation']                = $ds056PE4.mesurehygiene.mesurehygiene.frequenceEvacuation;
cerfaFields['ouiAutresMesures']                   = $ds056PE4.mesurehygiene.mesurehygiene.ouiAutresMesures;
cerfaFields['autreMesures']                       = $ds056PE4.mesurehygiene.mesurehygiene.autreMesures;

cerfaFields['principalesMaladies']                = $ds056PE4.preventionMaladies.preventionMaladies.principalesMaladies;
cerfaFields['ouiMesuresSanitaires']               = $ds056PE4.preventionMaladies.preventionMaladies.ouiMesuresSanitaires;
cerfaFields['mesuresSanitaires']                  = $ds056PE4.preventionMaladies.preventionMaladies.mesuresSanitaires;
cerfaFields['ouiMesuresPermanente']               = $ds056PE4.preventionMaladies.preventionMaladies.ouiMesuresPermanente;
cerfaFields['mesuresSanitairesPermanentes']       = $ds056PE4.preventionMaladies.preventionMaladies.mesuresSanitairesPermanentes;
cerfaFields['ouiConcoursVeterinaire']             = $ds056PE4.preventionMaladies.preventionMaladies.ouiConcoursVeterinaire;
cerfaFields['nomAdresseConcours']                 = $ds056PE4.preventionMaladies.preventionMaladies.nomAdresseConcours;
cerfaFields['ouiMesuresProphylaxie']              = $ds056PE4.preventionMaladies.preventionMaladies.ouiMesuresProphylaxie;
cerfaFields['mesuresProphylaxie']                 = $ds056PE4.preventionMaladies.preventionMaladies.mesuresProphylaxie;
cerfaFields['ouiAutresMesures3']                  = $ds056PE4.preventionMaladies.preventionMaladies.ouiAutresMesures3;
cerfaFields['autreMesureVeterinaire']             = $ds056PE4.preventionMaladies.preventionMaladies.autreMesureVeterinaire;





cerfaFields['respectAutorisation']                = $ds056PE4.signature.signature.signatureDeclarant;
cerfaFields['visitesHeures']                      = Value('id').of($ds056PE4.signature.signature.signatureDeclarant2).contains('visitesHeures')? true : false;
cerfaFields['visitePresence']                     = Value('id').of($ds056PE4.signature.signature.signatureDeclarant2).contains('visitePresence')? true : false;
cerfaFields['visiteLieu']                         = Value('id').of($ds056PE4.signature.signature.signatureDeclarant2).contains('visiteLieu')? true : false;
cerfaFields['lieuSignature']                      = $ds056PE4.signature.signature.lieuSignature;
cerfaFields['dateSignature']                      = $ds056PE4.signature.signature.dateSignature;
cerfaFields['signatureDemandeur']                 = $ds056PE4.signature.signature.signatureDemandeur;









/* var destinataire = Value('id').of($ds021PE6.signatureGroup.signature.departementInstallation).eq("974") ? "DEAL" : (Value('id').of($ds021PE6.signatureGroup.signature.departementInstallation).eq("21") ? "DDT" : (Value('id').of($ds021PE6.signatureGroup.signature.departementInstallation).eq("75") ? "DTPP" : "Prefecture du département"));
 */

 var cerfaDoc1 = nash.doc //
	.load('models/Cerfa 12447_01.pdf') //
	.apply (cerfaFields);



/**********************
Pieces jointes
***********************/
function appendPj(fld) {
	fld.forEach(function (elm) {
        cerfaDoc1.append(elm);
    });
}	
	
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPlanInstallations);
appendPj($attachmentPreprocess.attachmentPreprocess.pjListeEspece);
appendPj($attachmentPreprocess.attachmentPreprocess.pjNotice); 
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertificatCapacite); 




/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		date: $ds056PE4.signature.signature.dateSignature ,
		autoriteHabilitee : "Prefecture du département" ,  /* destinataire , */
		demandeContexte : "Demande d'autorisation d'ouverture d'un établissement détenant des animaux non domestiques de 2ème catégorie.",
		civiliteNomPrenom : nomPrenom
	});
	finalDoc.append(cerfaDoc1.save('cerfa.pdf'));	
	
	
/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Achat-Vente_animaux_1ere-categorie.pdf');	
	
	
/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Achat-Vente d\'animaux d\'agrément ou de compagnie - demande d\'autorisation d\'ouverture d\'un établissement détenant des animaux non domestiques de 2ème catégorie',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : "Demande d'autorisation d'ouverture d'un établissement détenant des animaux non domestiques de 2ème catégorie.",
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});