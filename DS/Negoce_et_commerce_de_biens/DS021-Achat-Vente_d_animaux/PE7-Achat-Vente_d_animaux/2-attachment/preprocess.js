// attachment('attachmentType', 'outputFileFieldId', { label: $pageId.fieldId });

var nomPrenom = $ds021PE7.signatureGroup.signature.signatureNom +' '+ $ds021PE7.signatureGroup.signature.signaturePrenom;
attachment('pjID', 'pjID', {label: nomPrenom, mandatory:"true"});	

attachment('pjAtestationAutorite', 'pjAtestationAutorite', {mandatory:"true"});	
attachment('pjQualificationsProfessionnelles', 'pjQualificationsProfessionnelles', {mandatory:"true"});	
attachment('pjPiecesUtiles', 'pjPiecesUtiles', {mandatory:"true"});		
attachment('pjAssurance', 'pjAssurance', {mandatory:"true"});	
attachment('pjDenomination', 'pjDenomination', {mandatory:"true"});	
	
	
attachment('pjPreuveActivite', 'pjPreuveActivite', {mandatory:"false"});