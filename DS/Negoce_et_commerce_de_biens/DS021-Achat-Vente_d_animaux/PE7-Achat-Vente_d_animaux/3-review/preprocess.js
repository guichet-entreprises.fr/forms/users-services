var formFields = {};

formFields['nomPro']             = $ds021PE7.declarationGroup.identification.nomPro;


formFields['telephone']          = $ds021PE7.declarationGroup.identification.siege.adresseTelephone;



formFields['objet']              = "Déclaration préalable à une libre prestation de services d'achat/vente d'animaux d'agrément ou de compagnie.";


formFields['adresseSiege']                          = $ds021PE7.declarationGroup.identification.siege.adresseNVoie +' '
														+ $ds021PE7.declarationGroup.identification.siege.adresseVoie + ' ' 
															+ ($ds021PE7.declarationGroup.identification.siege.adresseVoieComplement ? $ds021PE7.declarationGroup.identification.siege.adresseVoieComplement : '') +', '
														+ ($ds021PE7.declarationGroup.identification.siege.adresseCP ? $ds021PE7.declarationGroup.identification.siege.adresseCP : '') +' '
														+ $ds021PE7.declarationGroup.identification.siege.adresseVille +', '
														+ $ds021PE7.declarationGroup.identification.siege.adressePays;
formFields['courriel']           					=	$ds021PE7.declarationGroup.identification.siege.email;



var nomPrenom = $ds021PE7.signatureGroup.signature.signatureNom +' '+ $ds021PE7.signatureGroup.signature.signaturePrenom;
formFields['signatureNom']                         	= $ds021PE7.signatureGroup.signature.signatureNom +' '+ $ds021PE7.signatureGroup.signature.signaturePrenom;
formFields['declarationHonneur'] 					= $ds021PE7.signatureGroup.signature.declarationHonneur;
formFields['signatureFaitLe']                      	= $ds021PE7.signatureGroup.signature.signatureFaitLe;
formFields['signatureFaitA']                      	= $ds021PE7.signatureGroup.signature.signatureFaitA +', le ';
formFields['signature']          					= nomPrenom;

// var destinataire = Value('id').of($ds021PE6.signatureGroup.signature.departementInstallation).eq("974") ? "DEAL" : (Value('id').of($ds021PE6.signatureGroup.signature.departementInstallation).eq("21") ? "DDT" : (Value('id').of($ds021PE6.signatureGroup.signature.departementInstallation).eq("75") ? "DTPP" : "Prefecture du département"));


 var cerfaDoc1 = nash.doc //
	.load('models/courrier_libre_LPS_DS.pdf') //
	.apply (formFields);



/**********************
Pieces jointes
***********************/
function appendPj(fld) {
	fld.forEach(function (elm) {
        cerfaDoc1.append(elm);
    });
}	
	
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAtestationAutorite);
appendPj($attachmentPreprocess.attachmentPreprocess.pjQualificationsProfessionnelles);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles); 
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveActivite); 
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurance); 
appendPj($attachmentPreprocess.attachmentPreprocess.pjDenomination); 



/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		date: $ds021PE7.signatureGroup.signature.signatureFaitLe ,
		autoriteHabilitee :"Prefecture du département" ,  /* destinataire , */
		demandeContexte : "Déclaration préalable à une libre prestation de services d'achat/vente d'animaux d'agrément ou de compagnie.",
		civiliteNomPrenom : nomPrenom
	});
	finalDoc.append(cerfaDoc1.save('cerfa.pdf'));	
	
	
/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Achat-Vente_animaux_LPS.pdf');	
	
	
/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Achat-Vente d\'animaux d\'agrément ou de compagnie - Déclaration préalable à une libre prestation de services d\'achat/vente d\'animaux d\'agrément ou de compagnie',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : "Déclaration préalable à une libre prestation de services d'achat/vente d'animaux d'agrément ou de compagnie.",
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});