function pad(s) { return (s < 10) ? '0' + s : s; }

var cerfaFields = {};

var nomPrenom=$ds021PE3.identificationDemandeur.identificationDemandeur.civilite +' '+$ds021PE3.identificationDemandeur.identificationDemandeur.nomPrenomDemandeur;


/*Identification du demandeur*/

cerfaFields['nomPrenomDemandeur']  = $ds021PE3.identificationDemandeur.identificationDemandeur.nomDemandeur + " " + $ds021PE3.identificationDemandeur.identificationDemandeur.prenomDemandeur;

if($ds021PE3.identificationDemandeur.identificationDemandeur.dateNaissanceDemandeur != null) {
var dateTemp = new Date(parseInt ($ds021PE3.identificationDemandeur.identificationDemandeur.dateNaissanceDemandeur.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	cerfaFields['jourNaissance']  	= day;
	cerfaFields['moisNaissance']    = month;
	cerfaFields['anneeNaissance']   = year;
}
cerfaFields['lieuNaissance']                      = $ds021PE3.identificationDemandeur.identificationDemandeur.lieuNaissanceDemandeur;
cerfaFields['numeroAdresse']                      = ($ds021PE3.identificationDemandeur.identificationDemandeur.numeroAdresse != null ? $ds021PE3.identificationDemandeur.identificationDemandeur.numeroAdresse : '');
cerfaFields['codePostal']                         = ($ds021PE3.identificationDemandeur.identificationDemandeur.codePostalAdresse != null ? $ds021PE3.identificationDemandeur.identificationDemandeur.codePostalAdresse : '');
cerfaFields['voieAdresse']                        = $ds021PE3.identificationDemandeur.identificationDemandeur.voieAdresse;
cerfaFields['communeAdresse']                     = $ds021PE3.identificationDemandeur.identificationDemandeur.communeAdresse;

/*Activités pratiquées*/

cerfaFields['ouiDetentionSimple']                 = Value('id').of($ds021PE3.activitesPratiquees.activitesPratiquees.detentionSimple).eq('ouiDetentionSimple')? true : false;
cerfaFields['nonDetentionSimple']                 = Value('id').of($ds021PE3.activitesPratiquees.activitesPratiquees.detentionSimple).eq('nonDetentionSimple')? true : false;
cerfaFields['ouiUtilisation']                     = Value('id').of($ds021PE3.activitesPratiquees.activitesPratiquees.utilisation).eq('ouiUtilisation')? true : false;
cerfaFields['nonUtilisation']                     = Value('id').of($ds021PE3.activitesPratiquees.activitesPratiquees.utilisation).eq('nonUtilisation')? true : false;
cerfaFields['ouiTransport']                       = Value('id').of($ds021PE3.activitesPratiquees.activitesPratiquees.transport).eq('ouiTransport')? true : false;
cerfaFields['nonTransport']                       = Value('id').of($ds021PE3.activitesPratiquees.activitesPratiquees.transport).eq('nonTransport')? true : false;

cerfaFields['finsTransport']                      = $ds021PE3.activitesPratiquees.activitesPratiquees.finsTransport;

for (var i= 0; i <$ds021PE3.activitesPratiquees.activitesPratiquees.conditionsUtilisationGroup.size(); i++ ){
cerfaFields['conditionsUtilisation' +i]              = $ds021PE3.activitesPratiquees.activitesPratiquees.conditionsUtilisationGroup[i].utilisationDetails + ", " + $ds021PE3.activitesPratiquees.activitesPratiquees.conditionsUtilisationGroup[i].conditionsDetails;
}
for (var i=$ds021PE3.activitesPratiquees.activitesPratiquees.conditionsUtilisationGroup.size();  i < 3; i++ ){
cerfaFields['conditionsUtilisation' +i]              = '';
}

/*Alimentation*/

for (var i=$ds021PE3.especesAnimales.especesAnimales.size(); i < 5; i++ ){
	cerfaFields['nomVernaculaire'+i]         	= '';
	cerfaFields['nomScientifique'+i]        	= '';
	cerfaFields['specimenMale'+i]        		= '';
	cerfaFields['specimenFemelle'+i]       		= '';
	cerfaFields['specimenIndetermine'+i]        = '';
}

for (var i= 0; i < $ds021PE3.especesAnimales.especesAnimales.size(); i++ ){
	cerfaFields['nomVernaculaire'+i]         = $ds021PE3.especesAnimales.especesAnimales[i].nomVernaculaire;
	cerfaFields['nomScientifique'+i]       	 = $ds021PE3.especesAnimales.especesAnimales[i].nomScientifique;
	cerfaFields['specimenMale'+i]       	 = $ds021PE3.especesAnimales.especesAnimales[i].specimenMale;
	cerfaFields['specimenFemelle'+i]         = $ds021PE3.especesAnimales.especesAnimales[i].specimenFemelle;
	cerfaFields['specimenIndetermine'+i]     = $ds021PE3.especesAnimales.especesAnimales[i].specimenIndetermine;
}

/*Modalités d'acquisition des compétences*/

cerfaFields['formationInitiale']                  = $ds021PE3.modaliteAcquisition.modaliteAcquisition.formationInitiale;
cerfaFields['stageElevage']                       = $ds021PE3.modaliteAcquisition.modaliteAcquisition.stageElevage;
cerfaFields['activiteAssociativeProfessionnelle'] = $ds021PE3.modaliteAcquisition.modaliteAcquisition.activiteAssociativeProfessionnelle;
cerfaFields['bibliographie']                      = $ds021PE3.modaliteAcquisition.modaliteAcquisition.bibliographie;

cerfaFields['voieAdresseDetention']               = $ds021PE3.descritpionDetention.descritpionDetention.voieAdresseDetention;
cerfaFields['numeroAdresseDetention']             = $ds021PE3.descritpionDetention.descritpionDetention.numeroAdresseDetention;
cerfaFields['codePostalDetention']                = $ds021PE3.descritpionDetention.descritpionDetention.codePostalDetention;
cerfaFields['communeAdresseDetention']            = $ds021PE3.descritpionDetention.descritpionDetention.communeAdresseDetention;

/*Annexe*/

cerfaFields['nomScientifiqueEspece']              = $ds021PE3.groupeEspeces.groupeEspeces.nomScientifiqueEspece;
cerfaFields['cohabitationEspecesCoche']           = $ds021PE3.groupeEspeces.groupeEspeces.cohabitationEspecesCoche;
cerfaFields['cohabitationEspecesPrecision']       = $ds021PE3.groupeEspeces.groupeEspeces.cohabitationEspecesPrecision;
cerfaFields['origineGeographique']                = $ds021PE3.groupeEspeces.groupeEspeces.origineGeographique;
cerfaFields['biotope']                            = $ds021PE3.groupeEspeces.groupeEspeces.biotope;
cerfaFields['modeOrganisation']                   = $ds021PE3.groupeEspeces.groupeEspeces.modeOrganisation;
cerfaFields['dangerHomme']                        = $ds021PE3.groupeEspeces.groupeEspeces.dangerHomme;
cerfaFields['dangerFaune']                        = $ds021PE3.groupeEspeces.groupeEspeces.dangerFaune;
cerfaFields['statutJuridique']                    = $ds021PE3.groupeEspeces.groupeEspeces.statutJuridique;

/*Alimentation*/


for (var i=$ds021PE3.alimentation.alimentation.size (); i < 7; i++ ){
	cerfaFields['aliment'+i]         = '';
	cerfaFields['frequence'+i]       = '';
}

for (var i= 0; i < $ds021PE3.alimentation.alimentation.size(); i++ ){     
	cerfaFields['aliment'+i]         = $ds021PE3.alimentation.alimentation[i].aliment;
	cerfaFields['frequence'+i]       = $ds021PE3.alimentation.alimentation[i].frequence;
}


cerfaFields['autreParticularite']                 = $ds021PE3.alimentation1.alimentation1.autreParticularite;
cerfaFields['ouiAutreParticularite']              = $ds021PE3.alimentation1.alimentation1.ouiAutreParticularite;

/*Reproduction*/

cerfaFields['ageMaturite']                        = $ds021PE3.reproduction.reproduction.ageMaturite;
cerfaFields['saisonReproduction']                 = $ds021PE3.reproduction.reproduction.saisonReproduction;
cerfaFields['modaliteChoixReproducteur']          = $ds021PE3.reproduction.reproduction.modaliteChoixReproducteur;
cerfaFields['modaliteEntretienReproducteur']      = $ds021PE3.reproduction.reproduction.modaliteEntretienReproducteur;
cerfaFields['ouiDureeGestation']                  = Value('id').of($ds021PE3.reproduction.reproduction.gestationOuIncubation).eq('ouiDureeGestation')? true : false;
cerfaFields['dureeGestation']                     = $ds021PE3.reproduction.reproduction.dureeGestation;
cerfaFields['ouiIncubation']                      = Value('id').of($ds021PE3.reproduction.reproduction.gestationOuIncubation).eq('ouiIncubation')? true : false;
cerfaFields['dureeIncubation']                    = $ds021PE3.reproduction.reproduction.dureeIncubation;
cerfaFields['ouiIncubationNaturelle']             = Value('id').of($ds021PE3.reproduction.reproduction.typeIncubation).eq('ouiIncubationNaturelle')? true : false;
cerfaFields['ouiIncubationArtificielle']          = Value('id').of($ds021PE3.reproduction.reproduction.typeIncubation).eq('ouiIncubationArtificielle')? true : false;
cerfaFields['temperature']                        = $ds021PE3.reproduction.reproduction.incubArtificielleDetails.temperature;
cerfaFields['retournementOeuf']                   = $ds021PE3.reproduction.reproduction.incubArtificielleDetails.retournementOeuf;
cerfaFields['hygrometrie']                        = $ds021PE3.reproduction.reproduction.incubArtificielleDetails.hygrometrie;
cerfaFields['nombreJeuneFemelle']                 = $ds021PE3.reproduction.reproduction.nombreJeuneFemelle;
cerfaFields['modaliteEntretienJeunes']            = $ds021PE3.reproduction.reproduction.modaliteEntretienJeunes;
cerfaFields['destinationJeunes']                  = $ds021PE3.reproduction.reproduction.destinationJeunes;
cerfaFields['ouiAutresParticularitesPrecautions'] = $ds021PE3.reproduction.reproduction.ouiAutresParticularitesPrecautions;
cerfaFields['autresParticularitesPrecautions']    = $ds021PE3.reproduction.reproduction.autresParticularitesPrecautions;

/*Installation d'hebergement des animaux*/

cerfaFields['installationCage']                   = Value('id').of($ds021PE3.hebergement.hebergement.natureInstallation).contains('installationCage')? true : false;
cerfaFields['installationEnclos']                 = Value('id').of($ds021PE3.hebergement.hebergement.natureInstallation).contains('installationEnclos')? true : false;
cerfaFields['installationVoliere']                = Value('id').of($ds021PE3.hebergement.hebergement.natureInstallation).contains('installationVoliere')? true : false;
cerfaFields['installationTerrarium']              = Value('id').of($ds021PE3.hebergement.hebergement.natureInstallation).contains('installationTerrarium')? true : false;
cerfaFields['installationBassin']                 = Value('id').of($ds021PE3.hebergement.hebergement.natureInstallation).contains('installationBassin')? true : false;
cerfaFields['installationAutre']                  = Value('id').of($ds021PE3.hebergement.hebergement.natureInstallation).contains('installationAutre')? true : false;
cerfaFields['autreCommentaire']                   = $ds021PE3.hebergement.hebergement.autreCommentaire;
cerfaFields['natureMateriaux2']                   = $ds021PE3.hebergement.hebergement.natureMateriaux2;
cerfaFields['nombreAnimaux']                  	  = $ds021PE3.hebergement.hebergement.nombreAnimaux;
cerfaFields['dimensionLongueur']                  = $ds021PE3.hebergement.hebergement.dimensionLongueur;
cerfaFields['dimensionLargeur']                   = $ds021PE3.hebergement.hebergement.dimensionLargeur;
cerfaFields['dimensionHauteur']                   = $ds021PE3.hebergement.hebergement.dimensionHauteur;
cerfaFields['materiauxParois']                    = $ds021PE3.hebergement.hebergement.materiauxParois;
cerfaFields['natureSol']                          = $ds021PE3.hebergement.hebergement.natureSol;
cerfaFields['ouiClotures']                        = $ds021PE3.hebergement.hebergement.ouiClotures;
cerfaFields['barrierePlus']                       = $ds021PE3.hebergement.hebergement.barrierePlus;
cerfaFields['natureMateriaux']                    = $ds021PE3.hebergement.hebergement.natureMateriaux;
cerfaFields['ouiEcartementPoteaux']               = $ds021PE3.hebergement.hebergement.ouiEcartementPoteaux;
cerfaFields['ecartementMateriaux']                = $ds021PE3.hebergement.hebergement.ecartementMateriaux;
cerfaFields['ouiEcartementBarreaux']              = $ds021PE3.hebergement.hebergement.ouiEcartementBarreaux;
cerfaFields['ecartementBarreaux']                 = $ds021PE3.hebergement.hebergement.ecartementBarreaux;
cerfaFields['ouiMaille']                          = $ds021PE3.hebergement.hebergement.ouiMaille;
cerfaFields['mailleGrillage']                     = $ds021PE3.hebergement.hebergement.mailleGrillage;
cerfaFields['hauteurSol']                         = $ds021PE3.hebergement.hebergement.hauteurSol;
cerfaFields['profondeurSol']                      = $ds021PE3.hebergement.hebergement.profondeurSol;
cerfaFields['ouiBarrieresPlus']                   = $ds021PE3.hebergement.hebergement.ouiBarrieresPlus;
cerfaFields['barrierePlus2']                      = $ds021PE3.hebergement.hebergement.barrierePlus2;
cerfaFields['ouiAbris']                           = $ds021PE3.hebergement.hebergement.ouiAbris;
cerfaFields['natureMateriaux']                    = $ds021PE3.hebergement.hebergement.natureMateriaux;
cerfaFields['largeurMateriaux']                   = $ds021PE3.hebergement.hebergement.largeurMateriaux;
cerfaFields['longueurMateriaux']                  = $ds021PE3.hebergement.hebergement.longueurMateriaux;
cerfaFields['hauteurMateriaux']                   = $ds021PE3.hebergement.hebergement.hauteurMateriaux;
cerfaFields['ouiChauffage']                       = $ds021PE3.hebergement.hebergement.ouiChauffage;
cerfaFields['chauffageTypeTemp']                  = $ds021PE3.hebergement.hebergement.chauffageTypeTemp;
cerfaFields['ouiEclairementArtificiel']           = $ds021PE3.hebergement.hebergement.ouiEclairementArtificiel;
cerfaFields['eclairementArtificiel']              = $ds021PE3.hebergement.hebergement.eclairementArtificiel;
cerfaFields['ouiSystemeVentilation']              = $ds021PE3.hebergement.hebergement.ouiSystemeVentilation;
cerfaFields['systemeVentilation']                 = $ds021PE3.hebergement.hebergement.systemeVentilation;
cerfaFields['ouiTauxHygrometrie']                 = $ds021PE3.hebergement.hebergement.ouiTauxHygrometrie;
cerfaFields['tauxHygrométrie'] 					  = $ds021PE3.hebergement.hebergement.tauxHygrométrie; 
cerfaFields['ouiMoyenFermeture']                  = $ds021PE3.hebergement.hebergement.ouiMoyenFermeture;
cerfaFields['moyenFermetureInstallation']         = $ds021PE3.hebergement.hebergement.moyenFermetureInstallation;
cerfaFields['ouiAccessoireAmenagement']           = $ds021PE3.hebergement.hebergement.ouiAccessoireAmenagement;
cerfaFields['ouiBassin']                          = $ds021PE3.hebergement.hebergement.ouiBassin;
cerfaFields['bassinDimension']                    = $ds021PE3.hebergement.hebergement.bassinDimension;
cerfaFields['modaliteMaitriseEau']                = $ds021PE3.hebergement.hebergement.modaliteMaitriseEau;
cerfaFields['ouiArbres']                          = $ds021PE3.hebergement.hebergement.ouiArbres;
cerfaFields['planteArbre']                        = $ds021PE3.hebergement.hebergement.planteArbre;
cerfaFields['ouiLitiere']                         = $ds021PE3.hebergement.hebergement.ouiLitiere;
cerfaFields['litiere']                            = $ds021PE3.hebergement.hebergement.litiere;
cerfaFields['ouiAutres']                          = $ds021PE3.hebergement.hebergement.ouiAutres;
cerfaFields['autresAccessoires']                  = $ds021PE3.hebergement.hebergement.autresAccessoires;
cerfaFields['ouiMaterielCapture']                 = $ds021PE3.hebergement.hebergement.ouiMaterielCapture;
cerfaFields['materielCapture']                    = $ds021PE3.hebergement.hebergement.materielCapture;
cerfaFields['ouiLocalQuarantaine']                = $ds021PE3.hebergement.hebergement.ouiLocalQuarantaine;
cerfaFields['localQuarantaine']                   = $ds021PE3.hebergement.hebergement.localQuarantaine;
cerfaFields['ouiAutresParticulariteInstallation'] = $ds021PE3.hebergement.hebergement.ouiAutresParticulariteInstallation;
cerfaFields['particularite']                      = $ds021PE3.hebergement.hebergement.particularite;
cerfaFields['ouiDescriptionTransport']            = $ds021PE3.hebergement.hebergement.ouiDescriptionTransport;
cerfaFields['descriptionMoyenTransport']          = $ds021PE3.hebergement.hebergement.descriptionMoyenTransport;

/*Mesure d'hygiéne*/

cerfaFields['ouiNettoyage']                       = $ds021PE3.mesurehygiene.mesurehygiene.ouiNettoyage;
cerfaFields['methodeNettoyage']                   = $ds021PE3.mesurehygiene.mesurehygiene.methodeNettoyage;
cerfaFields['frequenceNettoyage']                 = $ds021PE3.mesurehygiene.mesurehygiene.frequenceNettoyage;
cerfaFields['produitEmployesNettoyage']           = $ds021PE3.mesurehygiene.mesurehygiene.produitEmployesNettoyage;
cerfaFields['ouiDesinfection']                    = $ds021PE3.mesurehygiene.mesurehygiene.ouiDesinfection;
cerfaFields['methodeDesinfection']                = $ds021PE3.mesurehygiene.mesurehygiene.methodeDesinfection;
cerfaFields['frequenceDesinfection']              = $ds021PE3.mesurehygiene.mesurehygiene.frequenceDesinfection;
cerfaFields['produitEmployesDesinfection']        = $ds021PE3.mesurehygiene.mesurehygiene.produitEmployesDesinfection;
cerfaFields['ouiEvacuationDechets']               = $ds021PE3.mesurehygiene.mesurehygiene.ouiEvacuationDechets;
cerfaFields['traitementDechet']                   = $ds021PE3.mesurehygiene.mesurehygiene.traitementDechet;
cerfaFields['destinationDechet']                  = $ds021PE3.mesurehygiene.mesurehygiene.destinationDechet;
cerfaFields['methodeEvacuation']                  = $ds021PE3.mesurehygiene.mesurehygiene.methodeEvacuation;
cerfaFields['frequenceEvacuation']                = $ds021PE3.mesurehygiene.mesurehygiene.frequenceEvacuation;
cerfaFields['ouiAutresMesures']                   = $ds021PE3.mesurehygiene.mesurehygiene.ouiAutresMesures;
cerfaFields['autreMesures']                       = $ds021PE3.mesurehygiene.mesurehygiene.autreMesures;

cerfaFields['principalesMaladies']                = $ds021PE3.preventionMaladies.preventionMaladies.principalesMaladies;
cerfaFields['ouiMesuresSanitaires']               = $ds021PE3.preventionMaladies.preventionMaladies.ouiMesuresSanitaires;
cerfaFields['mesuresSanitaires']                  = $ds021PE3.preventionMaladies.preventionMaladies.mesuresSanitaires;
cerfaFields['ouiMesuresPermanente']               = $ds021PE3.preventionMaladies.preventionMaladies.ouiMesuresPermanente;
cerfaFields['mesuresSanitairesPermanentes']       = $ds021PE3.preventionMaladies.preventionMaladies.mesuresSanitairesPermanentes;
cerfaFields['ouiConcoursVeterinaire']             = $ds021PE3.preventionMaladies.preventionMaladies.ouiConcoursVeterinaire;
cerfaFields['nomAdresseConcours']                 = $ds021PE3.preventionMaladies.preventionMaladies.nomAdresseConcours;
cerfaFields['ouiMesuresProphylaxie']              = $ds021PE3.preventionMaladies.preventionMaladies.ouiMesuresProphylaxie;
cerfaFields['mesuresProphylaxie']                 = $ds021PE3.preventionMaladies.preventionMaladies.mesuresProphylaxie;
cerfaFields['ouiAutresMesures3']                  = $ds021PE3.preventionMaladies.preventionMaladies.ouiAutresMesures3;
cerfaFields['autreMesureVeterinaire']             = $ds021PE3.preventionMaladies.preventionMaladies.autreMesureVeterinaire;





cerfaFields['respectAutorisation']                = $ds021PE3.signature.signature.signatureDeclarant;
cerfaFields['visitesHeures']                      = Value('id').of($ds021PE3.signature.signature.signatureDeclarant2).contains('visitesHeures')? true : false;
cerfaFields['visitePresence']                     = Value('id').of($ds021PE3.signature.signature.signatureDeclarant2).contains('visitePresence')? true : false;
cerfaFields['visiteLieu']                         = Value('id').of($ds021PE3.signature.signature.signatureDeclarant2).contains('visiteLieu')? true : false;
cerfaFields['lieuSignature']                      = $ds021PE3.signature.signature.lieuSignature;
cerfaFields['dateSignature']                      = $ds021PE3.signature.signature.dateSignature;
cerfaFields['signatureDemandeur']                 = $ds021PE3.signature.signature.signatureDemandeur;









/* var destinataire = Value('id').of($ds021PE6.signatureGroup.signature.departementInstallation).eq("974") ? "DEAL" : (Value('id').of($ds021PE6.signatureGroup.signature.departementInstallation).eq("21") ? "DDT" : (Value('id').of($ds021PE6.signatureGroup.signature.departementInstallation).eq("75") ? "DTPP" : "Prefecture du département"));
 */

 var cerfaDoc1 = nash.doc //
	.load('models/Cerfa 12447_01.pdf') //
	.apply (cerfaFields);



/**********************
Pieces jointes
***********************/
function appendPj(fld) {
	fld.forEach(function (elm) {
        cerfaDoc1.append(elm);
    });
}	
	
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPlanInstallations);
appendPj($attachmentPreprocess.attachmentPreprocess.pjListeEspece);
appendPj($attachmentPreprocess.attachmentPreprocess.pjNotice); 
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertificatCapacite); 




/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		date: $ds021PE3.signature.signature.dateSignature ,
		autoriteHabilitee : "Prefecture du département" ,  /* destinataire , */
		demandeContexte : "Demande d'autorisation d'ouverture d'un établissement détenant des animaux non domestiques de 1ère catégorie.",
		civiliteNomPrenom : nomPrenom
	});
	finalDoc.append(cerfaDoc1.save('cerfa.pdf'));	
	
	
/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Achat-Vente_animaux_1ere-categorie.pdf');	
	
	
/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Achat-Vente d\'animaux d\'agrément ou de compagnie - Demande d\'autorisation d\'ouverture d\'un établissement détenant des animaux non domestiques de 1ère catégorie',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : "Demande d'autorisation d'ouverture d'un établissement détenant des animaux non domestiques de 1ère catégorie.",
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});