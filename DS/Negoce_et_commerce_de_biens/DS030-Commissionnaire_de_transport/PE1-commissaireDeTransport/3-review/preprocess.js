function pad(s) { return (s < 10) ? '0' + s : s; }

var formFields = {};

var civNomPrenom = $ds030PE1.etatCivilPage.etatCivilGroupe.civilite +' '+ $ds030PE1.etatCivilPage.etatCivilGroupe.nom +' '+ $ds030PE1.etatCivilPage.etatCivilGroupe.prenom;


/*******************************************************************************
 * Type attestations
 ******************************************************************************/
formFields['attestationCommissaireTransport']              = true;


/*******************************************************************************
 * Etat civil
 ******************************************************************************/
var cheminEtatCivil = $ds030PE1.etatCivilPage.etatCivilGroupe;

formFields['civiliteMadame']                     	       = (cheminEtatCivil.civilite=='Madame');
formFields['civiliteMonsieur']                   		   = (cheminEtatCivil.civilite=='Monsieur');
formFields['nom']                                          = cheminEtatCivil.nom;
formFields['prenom']                                       = cheminEtatCivil.prenom;
formFields['nomMarital']                                   = cheminEtatCivil.nomMarital != null ? cheminEtatCivil.nomMarital : ' ';
formFields['dateNaissance']                                = cheminEtatCivil.dateNaissance;
formFields['lieuNaissance']                                = cheminEtatCivil.lieuNaissance;
formFields['numDeptFrance']                                = cheminEtatCivil.numDeptFrance;
formFields['nomEtatEtranger']                              = cheminEtatCivil.nomEtatEtranger != null ? cheminEtatCivil.nomEtatEtranger : ' ';
formFields['nationalite']                                  = cheminEtatCivil.nationalite;


/*******************************************************************************
 * Coordonnées
 ******************************************************************************/
var cheminCoordonnes = $ds030PE1.coordonneesPage.coordonneGroupe;


formFields['numVoie']                                      = cheminCoordonnes.numVoie;
formFields['extention']                                    = cheminCoordonnes.extention;
formFields['typeVoie']                                     = cheminCoordonnes.typeVoie;
formFields['nomVoie']                                      = cheminCoordonnes.nomVoie;
formFields['lieuDitBP']                                    = cheminCoordonnes.lieuDitBP;
formFields['codePostal']                                   = cheminCoordonnes.codePostal;
formFields['localite']                                     = cheminCoordonnes.communeVille;
formFields['pays']                                         = cheminCoordonnes.residerEnFranceOuiNon ? "France" : cheminCoordonnes.pays;
formFields['numTel']                                       = cheminCoordonnes.numTel;
formFields['numTelecopie']                                 = cheminCoordonnes.numTelecopie;
formFields['email']                                        = cheminCoordonnes.email;


/*******************************************************************************
 * Attestations de capacité
 ******************************************************************************/
var cheminSituationPage = $ds030PE1.situationPage.situationGroupe;


formFields['examenEcrit']                                  = true;
formFields['dateExamenEcrit']                              = cheminSituationPage.dateExamenEcrit;

/*******************************************************************************
 * Signature
 ******************************************************************************/
var cheminSignature = $ds030PE1.signaturePage.signatureGroupe;


formFields['declarationHonneur']                           = cheminSignature.declarationHonneur;
formFields['signatureDeclarant']                  		   = civNomPrenom;

formFields['lieuSignature']                                = cheminSignature.lieuSignature;
if(cheminSignature.dateSignature != null) {
	var dateTemp = new Date(parseInt(cheminSignature.dateSignature.getTimeInMillis()));
	var monthTemp = dateTemp.getMonth() + 1;
	var month = pad(monthTemp.toString());
	var day =  pad(dateTemp.getDate().toString());
	var year = dateTemp.getFullYear();
	formFields['jourSignature'] 	    = day;
	formFields['moisSignature']	    	= month;
	formFields['anneeSignature']	    = year;
}


/*******************************************************************************
 * Données du représentant légal du mineur
 ******************************************************************************/

formFields['signatureRepresentantMineur']                  = (cheminEtatCivil.representantMineurGroupe.civiliteRepresentant != null ? cheminEtatCivil.representantMineurGroupe.civiliteRepresentant : ' ' )+' '+ (cheminEtatCivil.representantMineurGroupe.nomRepresentant != null ? cheminEtatCivil.representantMineurGroupe.nomRepresentant : ' ') +' '+ (cheminEtatCivil.representantMineurGroupe.prenomRepresentant != null ? cheminEtatCivil.representantMineurGroupe.prenomRepresentant :' ' );
formFields['qualiteRepresentantMineur']                    = cheminEtatCivil.representantMineurGroupe.qualiteRepresentantMineur;

var cheminCoordonneesRepresentant = $ds030PE1.coordonneesRepresentantPage.coordonneesRepresentantGroupe;

formFields['coordonnesRepresentantMineur']                 = (cheminCoordonneesRepresentant.adresse != null ? cheminCoordonneesRepresentant.adresse : ' ') +' '+ (cheminCoordonneesRepresentant.codePostal != null ? cheminCoordonneesRepresentant.codePostal : ' ') +' '+ (cheminCoordonneesRepresentant.communeVille != null ? cheminCoordonneesRepresentant.communeVille : ' ') +' '+ (cheminCoordonneesRepresentant.pays != null ? cheminCoordonneesRepresentant.pays : ' ');
formFields['numTelRepresentantMineur'] 					   = cheminCoordonneesRepresentant.numTelRepresentant;
formFields['emailRepresentantMineur'] 					   = cheminCoordonneesRepresentant.emailRepresentant;



if (Value('id').of(cheminSituationPage.lieuJury).eq('metropole') || Value('id').of(cheminSituationPage.regionResidence).eq('metropole')){
	var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		dateSignature: cheminSignature.dateSignature,
		autoriteHabilitee :"Direction régionale de l'environnement, de l'aménagement et du logement (DREAL).",
		demandeContexte : "Demande d’attestation de capacité professionnelle.",
		civiliteNomPrenom : civNomPrenom
	});	
}

if (Value('id').of(cheminSituationPage.lieuJury).eq('idf') || Value('id').of(cheminSituationPage.regionResidence).eq('idf')){
	var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		dateSignature: cheminSignature.dateSignature,
		autoriteHabilitee :"Direction régionale et interdépartementale de l'équipement et de l'aménagement (DRIEA).",
		demandeContexte : "Demande d’attestation de capacité professionnelle.",
		civiliteNomPrenom : civNomPrenom
	});	
}

if (Value('id').of(cheminSituationPage.lieuJury).eq('dom') || Value('id').of(cheminSituationPage.regionResidence).eq('dom')){
	var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		dateSignature: cheminSignature.dateSignature,
		autoriteHabilitee :"Direction départementale de l'équipement (DDE).",
		demandeContexte : "Demande d'attestation de capacité professionnelle pour une personne ayant satisfait à un examen écrit.",
		civiliteNomPrenom : civNomPrenom
	});	
}


var cerfaDoc = nash.doc //
.load('models/Cerfa_11414_05.pdf') //
.apply(formFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));

/*******************************************************************************
 * Ajout des PJs
 ******************************************************************************/

function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}


appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
if (cheminCoordonnes.residerEnFranceOuiNon) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjJustifDomicile);
}

if (cheminCoordonnes.residerEnFranceOuiNon == false) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificatifNonResident);
}
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustifEtude);
appendPj($attachmentPreprocess.attachmentPreprocess.pjServiceNational);



var finalDocItem = finalDoc.save('Commissionnaire_de_transport.pdf');


return spec.create({
id : 'review',
label : 'Commissionnaire de transport - Demande d\'attestation de capacité professionnelle pour une personne ayant satisfait à un examen écrit.',
groups : [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : [ spec.createData({
        id : 'formulaire',
        label : 'Demande d\'attestation de capacité professionnelle.',
        description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
        type : 'FileReadOnly',
        value : [ finalDocItem ]
    }) ]
}) ]
});