attachment('pjID', 'pjID');

var cheminCoordonnes = $ds030PE1.coordonneesPage.coordonneGroupe;
if (cheminCoordonnes.residerEnFranceOuiNon) {
	attachment('pjJustifDomicile', 'pjJustifDomicile');
}

if (cheminCoordonnes.residerEnFranceOuiNon == false) {
	attachment('pjJustificatifNonResident', 'pjJustificatifNonResident');
}
attachment('pjJustifEtude', 'pjJustifEtude', {
	mandatory : "false"
});
attachment('pjServiceNational', 'pjServiceNational', {
	mandatory : "false"
});