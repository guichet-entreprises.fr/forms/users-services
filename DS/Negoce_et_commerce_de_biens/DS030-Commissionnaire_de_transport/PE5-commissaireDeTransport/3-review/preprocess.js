var formFields = {};

var civNomPrenom = $ds030PE5.representantLegalPage.representantLegalGroupe.nomResponsableLegal +' '+ $ds030PE5.representantLegalPage.representantLegalGroupe.prenomResponsableLegal;

/*******************************************************************************
 * Type attestations
 ******************************************************************************/

formFields['commisionnaireTransport']                          = true;

/*******************************************************************************
 * Identification de l'entreprise
 ******************************************************************************/
var cheminIdEntreprise = $ds030PE5.idEntreprisePage.idEntrepriseGroupe;


formFields['raisonSocialeEntreprise']                          = cheminIdEntreprise.raisonSocialeEntreprise;
formFields['formeJuridiqueEntreprise']                         = cheminIdEntreprise.formeJuridiqueEntreprise;
formFields['nomCommercial']                                    = cheminIdEntreprise.nomCommercial;
formFields['sirenEntreprise']                                  = cheminIdEntreprise.sirenEntreprise;
formFields['nic']                                              = cheminIdEntreprise.nic;
formFields['codeNAFPrincipal']                                 = cheminIdEntreprise.codeNAFPrincipal;

/*******************************************************************************
 * Coordonnées entreprises
 ******************************************************************************/
var chemincoordonneeEntreprise = $ds030PE5.coordonneeEntreprisePage.typeEntrepriseGroupe;

formFields['locauxHabitationSiege']                            = chemincoordonneeEntreprise.locauxHabitationSiege;
formFields['numVoieSiege']                                     = chemincoordonneeEntreprise.NumVoieSiege;
formFields['typeVoieSiege']                                    = chemincoordonneeEntreprise.typeVoieSiege;
formFields['nomVoieSiege']                                     = chemincoordonneeEntreprise.nomVoieSiege;
formFields['boitePostaleSiege']                                = chemincoordonneeEntreprise.boitePostaleSiege;
formFields['codePostaleSiege']                                 = chemincoordonneeEntreprise.codePostaleSiege;
formFields['localiteSiege']                                    = chemincoordonneeEntreprise.localiteSiege;
formFields['numTelSiege']                                      = chemincoordonneeEntreprise.numTelSiege;
formFields['numFaxSiege']                                      = chemincoordonneeEntreprise.numFaxSiege;
formFields['emailSiege']                                       = chemincoordonneeEntreprise.emailSiege;

/*******************************************************************************
 * Coordonnées du siège à l'étranger
 ******************************************************************************/
var  cheminCoordonneeEntrepriseEtranger = $ds030PE5.coordonneeEntrepriseEtrangerPage.coordonneeEntrepriseEtrangerGroupe;

formFields['numVoieSiegeEntranger']                            = cheminCoordonneeEntrepriseEtranger.numVoieSiegeEntranger;
formFields['typeVoieSiegeEntranger']                           = cheminCoordonneeEntrepriseEtranger.typeVoieSiegeEntranger;
formFields['nomVoieSiegeEntranger']                            = cheminCoordonneeEntrepriseEtranger.nomVoieSiegeEntranger;
formFields['boitePostaleSiegeEntranger']                       = cheminCoordonneeEntrepriseEtranger.boitePostaleSiegeEntranger;
formFields['codePostaleSiegeEntranger']                        = cheminCoordonneeEntrepriseEtranger.codePostaleSiegeEntranger;
formFields['localiteSiegeEntranger']                           = cheminCoordonneeEntrepriseEtranger.localiteSiegeEntranger;
formFields['numTelSiegeEntranger']                             = cheminCoordonneeEntrepriseEtranger.numTelSiegeEntranger;
formFields['numFaxSiegeEntranger']                             = cheminCoordonneeEntrepriseEtranger.numFaxSiegeEntranger;
formFields['emailSiegeEntranger']                              = cheminCoordonneeEntrepriseEtranger.emailSiegeEntranger;


/*******************************************************************************
 * Identification de l'établissement secondaire
 ******************************************************************************/
var cheminEtablissemementSecondaire = $ds030PE5.etablissemementSecondairePage.etablissemementSecondaireGroupe;

formFields['nomPrenomResponsableSecondaire']                   = cheminEtablissemementSecondaire.nomResponsableSecondaire != null ? cheminEtablissemementSecondaire.nomResponsableSecondaire : '' +' '+ cheminEtablissemementSecondaire.prenomResponsableSecondaire !=null ? cheminEtablissemementSecondaire.prenomResponsableSecondaire : ' ';
formFields['nomCommercialEtablissementSecondaire']             = cheminEtablissemementSecondaire.nomCommercialEtablissementSecondaire;
formFields['nicEtablissementSecondaire']                       = cheminEtablissemementSecondaire.nicEtablissementSecondaire;

/*******************************************************************************
 * Coordonnées de l'établissement secondaire
 ******************************************************************************/
var cheminCoordonneesEtablissemementSecondaire = $ds030PE5.coordonneesEtablissemementSecondairePage.coordonneesEtablissemementSecondaireGroupe;

formFields['numVoieEtablissementSecondaire']                     = cheminCoordonneesEtablissemementSecondaire.numVoieEtablissementSecondaire;
formFields['typeVoieEtablissementSecondaire']                    = cheminCoordonneesEtablissemementSecondaire.typeVoieEtablissementSecondaire;
formFields['nomVoieEtablissementSecondaire']                     = cheminCoordonneesEtablissemementSecondaire.nomVoieEtablissementSecondaire;
formFields['boitePostaleEtablissementSecondaire']                = cheminCoordonneesEtablissemementSecondaire.boitePostaleEtablissementSecondaire;
formFields['codePostalEtablissementSecondaire']                  = cheminCoordonneesEtablissemementSecondaire.codePostalEtablissementSecondaire;
formFields['localiteEtablissementSecondaire']                    = cheminCoordonneesEtablissemementSecondaire.localiteEtablissementSecondaire;
formFields['numTelEtablissementSecondaire']                      = cheminCoordonneesEtablissemementSecondaire.numTelEtablissementSecondaire;
formFields['numFaxEtablissementSecondaire']                      = cheminCoordonneesEtablissemementSecondaire.numFaxEtablissementSecondaire;
formFields['emailEtablissementSecondaire']                       = cheminCoordonneesEtablissemementSecondaire.emailEtablissementSecondaire;


/*******************************************************************************
 * Identification de l'établissement secondaire 2
 ******************************************************************************/
var cheminEtablissemementSecondaire2 = $ds030PE5.etablissemementSecondaire2Page.etablissemementSecondaire2Groupe;

formFields['nomPrenomEtablissementSecondaire2']                  = cheminEtablissemementSecondaire2.nomResponsableSecondaire2 != null ? cheminEtablissemementSecondaire2.nomResponsableSecondaire2 : '' +' '+ cheminEtablissemementSecondaire2.prenomResponsableSecondaire2 !=null ? cheminEtablissemementSecondaire2.prenomResponsableSecondaire2 : ' ';
formFields['nomCommercialEtablissementSecondaire2']              = cheminEtablissemementSecondaire2.nomCommercialEtablissementSecondaire2;
formFields['nicEtablissementSecondaire2']                        = cheminEtablissemementSecondaire2.nicEtablissementSecondaire2;


/*******************************************************************************
 * Coordonnées de l'établissement secondaire 2
 ******************************************************************************/
var cheminCoordonneesEtablissemementSecondaire2 = $ds030PE5.coordonneesEtablissemementSecondaire2Page.coordonneesEtablissemementSecondaire2Groupe;

formFields['numVoieEtablissementSecondaire2']                    = cheminCoordonneesEtablissemementSecondaire2.numVoieEtablissementSecondaire2;
formFields['typeVoieEtablissementSecondaire2']                   = cheminCoordonneesEtablissemementSecondaire2.typeVoieEtablissementSecondaire2;
formFields['nomVoieEtablissementSecondaire2']                    = cheminCoordonneesEtablissemementSecondaire2.nomVoieEtablissementSecondaire2;
formFields['boitePostaleEtablissementSecondaire2']               = cheminCoordonneesEtablissemementSecondaire2.boitePostaleEtablissementSecondaire2;
formFields['codePostalEtablissementSecondaire2']                 = cheminCoordonneesEtablissemementSecondaire2.codePostalEtablissementSecondaire2;
formFields['localiteEtablissementSecondaire2']                   = cheminCoordonneesEtablissemementSecondaire2.localiteEtablissementSecondaire2;
formFields['numTelEtablissementSecondaire2']                     = cheminCoordonneesEtablissemementSecondaire2.numTelEtablissementSecondaire2;
formFields['numFaxEtablissementSecondaire2']                     = cheminCoordonneesEtablissemementSecondaire2.numFaxEtablissementSecondaire2;
formFields['emailEtablissementSecondaire2']                      = cheminCoordonneesEtablissemementSecondaire2.emailEtablissementSecondaire2;



/*******************************************************************************
 * Identification de l'établissement secondaire 3
 ******************************************************************************/
var cheminEtablissemementSecondaire3 = $ds030PE5.etablissemementSecondaire3Page.etablissemementSecondaire3Groupe;

formFields['nomPrenomEtablissementSecondaire3']                  = cheminEtablissemementSecondaire3.nomResponsableSecondaire3 != null ? cheminEtablissemementSecondaire3.nomResponsableSecondaire3 : '' +' '+ cheminEtablissemementSecondaire3.prenomResponsableSecondaire3 !=null ? cheminEtablissemementSecondaire3.prenomResponsableSecondaire3 : ' ';
formFields['nomCommercialEtablissementSecondaire3']              = cheminEtablissemementSecondaire3.nomCommercialEtablissementSecondaire3;
formFields['nicEtablissementSecondaire3']                        = cheminEtablissemementSecondaire3.nicEtablissementSecondaire3;

/*******************************************************************************
 * Coordonnées de l'établissement secondaire 3
 ******************************************************************************/
var cheminCoordonneesEtablissemementSecondaire3 = $ds030PE5.coordonneesEtablissemementSecondaire3Page.coordonneesEtablissemementSecondaire3Groupe;

formFields['numVoieEtablissementSecondaire3']                    = cheminCoordonneesEtablissemementSecondaire3.numVoieEtablissementSecondaire3;
formFields['typeVoieEtablissementSecondaire3']                   = cheminCoordonneesEtablissemementSecondaire3.typeVoieEtablissementSecondaire3;
formFields['nomVoieEtablissementSecondaire3']                    = cheminCoordonneesEtablissemementSecondaire3.nomVoieEtablissementSecondaire3;
formFields['boitePostaleEtablissementSecondaire3']               = cheminCoordonneesEtablissemementSecondaire3.boitePostaleEtablissementSecondaire3;
formFields['codePostalEtablissementSecondaire3']                 = cheminCoordonneesEtablissemementSecondaire3.codePostalEtablissementSecondaire3;
formFields['localiteEtablissementSecondaire3']                   = cheminCoordonneesEtablissemementSecondaire3.localiteEtablissementSecondaire3;
formFields['numTelEtablissementSecondaire3']                     = cheminCoordonneesEtablissemementSecondaire3.nuTelEtablissementSecondaire3;
formFields['numFaxEtablissementSecondaire3']                     = cheminCoordonneesEtablissemementSecondaire3.numFaxEtablissementSecondaire3;
formFields['emailEtablissementSecondaire3']                      = cheminCoordonneesEtablissemementSecondaire3.emailEtablissementSecondaire3;

/*******************************************************************************
 * Etat civil du représentant légal
 ******************************************************************************/
var cheminRepresentantLegal = $ds030PE5.representantLegalPage.representantLegalGroupe;


formFields['locauxHabitation']                                 = cheminRepresentantLegal.locauxHabitation;
formFields['mensieurResponsableLegal']                         = (cheminRepresentantLegal.civiliteRepresentant=='Monsieur');
formFields['madameResponsableLegal']                           = (cheminRepresentantLegal.civiliteRepresentant=='Madame');
formFields['nomResponsableLegal']                              = cheminRepresentantLegal.nomResponsableLegal;
formFields['nomMaritalResponsableLegal']                       = cheminRepresentantLegal.nomMaritalResponsableLegal;
formFields['prenomResponsableLegal']                           = cheminRepresentantLegal.prenomResponsableLegal;
formFields['dateNaissanceResponsableLegal']                    = cheminRepresentantLegal.dateNaissanceResponsableLegal;
formFields['lieuNaissanceResponsableLegal']                    = cheminRepresentantLegal.lieuNaissanceResponsableLegal;
formFields['deptNaissanceResponsableLegal']                    = cheminRepresentantLegal.deptNaissanceResponsableLegal;
formFields['nationaliteResponsableLegal']                      = cheminRepresentantLegal.nationaliteResponsableLegal;


/*******************************************************************************
 * Coordonnées du représentant légal
 ******************************************************************************/
var cheminCoordoneesResponsableLegal = $ds030PE5.coordoneesResponsableLegalPage.coordoneesResponsableLegalGroup;

formFields['numVoieResponsableLegal']                          = cheminCoordoneesResponsableLegal.numVoieResponsableLegal;
formFields['typeVoieResponsableLegal']                         = cheminCoordoneesResponsableLegal.typeVoieResponsableLegal;
formFields['nomVoieResponsableLegal']                          = cheminCoordoneesResponsableLegal.nomVoieResponsableLegal;
formFields['codePostalResponsableLegal']                       = cheminCoordoneesResponsableLegal.codePostalResponsableLegal;
formFields['numTelResponsableLegal']                           = cheminCoordoneesResponsableLegal.numTelResponsableLegal;
formFields['numFaxResponsableLegal']                           = cheminCoordoneesResponsableLegal.numFaxResponsableLegal;
formFields['emailResponsableLegal']                            = cheminCoordoneesResponsableLegal.emailResponsableLegal;


/*******************************************************************************
 * Etat civil du représentant légal 2
 ******************************************************************************/
var cheminRepresentantLegal2 = $ds030PE5.representantLegal2Page.representantLegal2Groupe;

formFields['locauxHabitation2']                                = cheminRepresentantLegal2.locauxHabitation2;
formFields['mensieurResponsableLegal2']                        = cheminRepresentantLegal2.mensieurResponsableLegal2;
formFields['madameResponsableLegal2']                          = cheminRepresentantLegal2.madameResponsableLegal2;
formFields['nomResponsableLegal2']                             = cheminRepresentantLegal2.nomResponsableLegal2;
formFields['prenomResponsableLegal2']                          = cheminRepresentantLegal2.nomResponsableLegal2;
formFields['nomMaritalResponsableLegal2']                      = cheminRepresentantLegal2.nomMaritalResponsableLegal2;
formFields['dateNaissanceResponsableLegal2']                   = cheminRepresentantLegal2.dateNaissanceResponsableLegal2;
formFields['lieuNaissanceResponsableLegal2']                   = cheminRepresentantLegal2.lieuNaissanceResponsableLegal2;
formFields['deptNaissanceResponsableLegal2']                   = cheminRepresentantLegal2.deptNaissanceResponsableLegal2;
formFields['nationaliteResponsableLegal2']                     = cheminRepresentantLegal2.nationaliteResponsableLegal2;


/*******************************************************************************
 * Coordonnées du représentant légal 2
 ******************************************************************************/
var cheminCoordoneesResponsableLegal2 = $ds030PE5.coordoneesResponsableLegal2Page.coordoneesResponsableLegal2Groupe;

formFields['numVoieResponsableLegal2']                         = cheminCoordoneesResponsableLegal2.numVoieResponsableLegal2;
formFields['typeVoieResponsableLegal2']                        = cheminCoordoneesResponsableLegal2.typeVoieResponsableLegal2;
formFields['nomVoieResponsableLegal2']                         = cheminCoordoneesResponsableLegal2.nomVoieResponsableLegal2;
formFields['codePostalResponsableLegal2']                      = cheminCoordoneesResponsableLegal2.codePostalResponsableLegal2;
formFields['localiteResponsableLegal']                         = cheminCoordoneesResponsableLegal2.localiteResponsableLegal;
formFields['localiteResponsableLegal2']                        = cheminCoordoneesResponsableLegal2.localiteResponsableLegal2;
formFields['numFaxResponsableLegal2']                          = cheminCoordoneesResponsableLegal2.numFaxResponsableLegal2;
formFields['numTelResponsableLegal2']                          = cheminCoordoneesResponsableLegal2.numTelResponsableLegal2;
formFields['emailResponsableLegal2']                            = cheminCoordoneesResponsableLegal2.emailResponsableLegal2;


/*******************************************************************************
 * Coordonnées de l'entrepôt des documents administratifs et de contrôle
 ******************************************************************************/
var cheminDocAdmin = $ds030PE5.docAdminPage.docAdminGroupe;

formFields['locauxHabitationDocAdministratif']                 = cheminDocAdmin.locauxHabitationDocAdministratif;
formFields['nomEtablissementDocAdministratif']                 = cheminDocAdmin.nomEtablissementDocAdministratif;
formFields['numVoieDocAdministratif']                          = cheminDocAdmin.numVoieDocAdministratif;
formFields['typeVoieDocAdministratif']                         = cheminDocAdmin.typeVoieDocAdministratif;
formFields['nomVoieDocAdministratif']                          = cheminDocAdmin.nomVoieDocAdministratif;
formFields['boitePostaleDocAdministratif']                     = cheminDocAdmin.boitePostaleDocAdministratif;
formFields['codePostalDocAdministratif']                       = cheminDocAdmin.codePostalDocAdministratif;
formFields['localiteDocAdministratif']                         = cheminDocAdmin.localiteDocAdministratif;
formFields['numTelDocAdministratif']                           = cheminDocAdmin.numTelDocAdministratif;
formFields['numFaxDocAdministratif']                           = cheminDocAdmin.numFaxDocAdministratif;
formFields['emailDocAdministratif']                            = cheminDocAdmin.emailDocAdministratif;


/*************************************************************************************************
 * Coordonnées des locaux abritant les équipements administratifs et les installations techniques
 *************************************************************************************************/
var cheminInstallationTech = $ds030PE5.installationTechPage.installationTechGroupe;

formFields['locauxHabitationInstallTech']                      = cheminInstallationTech.locauxHabitationInstallTech;
formFields['nomEtablissementInstallTech']                      = cheminInstallationTech.nomEtablissementInstallTech;
formFields['numVoieInstallTech']                               = cheminInstallationTech.numVoieInstallTech;
formFields['typeVoieInstallTech']                              = cheminInstallationTech.typeVoieInstallTech;
formFields['nomVoieInstallTech']                               = cheminInstallationTech.nomVoieInstallTech;
formFields['boitePostaleInstallTech']                          = cheminInstallationTech.boitePostaleInstallTech;
formFields['codePostalInstallTech']                            = cheminInstallationTech.codePostalInstallTech;
formFields['localiteInstallTech']                              = cheminInstallationTech.localiteInstallTech;
formFields['numTelInstallTech']                                = cheminInstallationTech.numTelInstallTech;
formFields['numFaxInstallTech']                                = cheminInstallationTech.numFaxInstallTech;
formFields['emailInstallTech']                                 = cheminInstallationTech.emailInstallTech;

/****************************************************************************************************************
 * Coordonnées des locaux abritant les équipements administratifs et les installations techniques - sous traitant
 ****************************************************************************************************************/
var cheminInstallTechSousTrait = $ds030PE5.installTechSousTraitPage.installTechSousTraitGroupe;

formFields['nomEtablissementInstallTechSousTrait']             = cheminInstallTechSousTrait.nomEtablissementInstallTechSousTrait;
formFields['numVoieInstallTechSousTrait']                      = cheminInstallTechSousTrait.numVoieInstallTechSousTrait;
formFields['typeVoieInstallTechSousTrait']                     = cheminInstallTechSousTrait.typeVoieInstallTechSousTrait;
formFields['nomVoieInstallTechSousTrait']                      = cheminInstallTechSousTrait.nomVoieInstallTechSousTrait;
formFields['boitePostaleInstallTechSousTrait']                 = cheminInstallTechSousTrait.boitePostaleInstallTechSousTrait;
formFields['localiteInstallTechSousTrait']                     = cheminInstallTechSousTrait.localiteInstallTechSousTrait;
formFields['codePostalInstallTechSousTrait']                   = cheminInstallTechSousTrait.codePostalInstallTechSousTrait;
formFields['numTelInstallTechSousTrait']                       = cheminInstallTechSousTrait.numTelInstallTechSousTrait;
formFields['numFaxInstallTechSousTrait']                       = cheminInstallTechSousTrait.numFaxInstallTechSousTrait;
formFields['emailInstallTechSousTrait']                        = cheminInstallTechSousTrait.emailInstallTechSousTrait;


/*******************************************************************************
 * Parc de vehicules
 ******************************************************************************/
var cheminTransportMarchandises = $ds030PE5.parcVehiculesPage.transportMarchandisesGroupe;
var cheminTransportPersonnes = $ds030PE5.parcVehiculesPage.transportPersonneGroupe;

/////////Transport de marchandises -> véhicule moins de 3.5 tonnes
formFields['marchandisePleineProprioMoins35Tonnes']           = cheminTransportMarchandises.marchandiseMoins3.marchandisePleineProprioMoins35Tonnes;
formFields['marchandiseLocationMoins35Tonnes']                = cheminTransportMarchandises.marchandiseMoins3.marchandiseLocationMoins35Tonnes;
formFields['marchandiseCreditBailMoins35Tonnes']              = cheminTransportMarchandises.marchandiseMoins3.marchandiseCreditBailMoins35Tonnes;

/////////Transport de marchandises -> véhicule plus de 3.5 tonnes
formFields['marchandisePleineProprioPlus35Tonnes']            = cheminTransportMarchandises.marchandisePlus3.marchandisePleineProprioPlus35Tonnes;
formFields['marchandiseLocationPlus35Tonnes']                 = cheminTransportMarchandises.marchandisePlus3.marchandiseLocationPlus35Tonnes;
formFields['marchandiseCreditBailPlus35Tonnes']               = cheminTransportMarchandises.marchandisePlus3.marchandiseCreditBailPlus35Tonnes;

/////////Transport de personnes -> véhicule de 4 à 9 personnes
formFields['personnePleinProprio4a9']                         = cheminTransportPersonnes.personne4A9.personnePleinProprio4a9;
formFields['personneLocation4a9']                             = cheminTransportPersonnes.personne4A9.personneLocation4a9;
formFields['personneCreditBail4a9']                           = cheminTransportPersonnes.personne4A9.personneCreditBail4a9;

/////////Transport de personnes -> véhicule plus de 9 personnes
formFields['personnePleineProprioPlus9']                      = cheminTransportPersonnes.personnePlus9.personnePleineProprioPlus9;
formFields['personneLocationPlus9']                           = cheminTransportPersonnes.personnePlus9.personneLocationPlus9;
formFields['personneCreditBailplus9']                         = cheminTransportPersonnes.personnePlus9.personneCreditBailplus9;


/*******************************************************************************************************************************
 * Etat civil du gestionnaire de transport titulaire de l'attestation de capacité professionnelle pour le transport routier
 *******************************************************************************************************************************/
var cheminEtatCivilTitulaireAttestation = $ds030PE5.etatCivilTitulaireAttestationPage.etatCivilTitulaireAttestationGroupe;

formFields['gestionnaireInterne']                              = cheminEtatCivilTitulaireAttestation.gestionnaireInterne;
formFields['gestionnairePresta']                               = cheminEtatCivilTitulaireAttestation.gestionnairePresta;
formFields['madameGestionnairetitulaire']                      = cheminEtatCivilTitulaireAttestation.madameGestionnairetitulaire;
formFields['monsieurGestionnairetitulaire']                    = cheminEtatCivilTitulaireAttestation.monsieurGestionnairetitulaire;
formFields['prenomGestionnaireTitulaire']                      = cheminEtatCivilTitulaireAttestation.prenomGestionnaireTitulaire;
formFields['nomMaritalGestionnaireTitulaire']                  = cheminEtatCivilTitulaireAttestation.nomMaritalGestionnaireTitulaire;
formFields['dateNaissanceGestionnaireTitulaire']               = cheminEtatCivilTitulaireAttestation.dateNaissanceGestionnaireTitulaire;
formFields['lieuNaissanceGestionnaireTitulaire']               = cheminEtatCivilTitulaireAttestation.lieuNaissanceGestionnaireTitulaire;
formFields['deptNaissanceGestionnaireTitulaire']               = cheminEtatCivilTitulaireAttestation.deptNaissanceGestionnaireTitulaire;
formFields['nationaliteGestionnaireTitulaire']                 = cheminEtatCivilTitulaireAttestation.nationaliteGestionnaireTitulaire;


/***************************************************************************************************************************
 * Coordonnées du gestionnaire de transport titulaire de l'attestation de capacité professionnelle pour le transport routier
 ***************************************************************************************************************************/
var cheminCoordonnesTitulaireAttestaion = $ds030PE5.coordonnesTitulaireAttestaionPage.coordonnesTitulaireAttestaionGroupe;

formFields['numVoieGestionnaireTitulaire']                     = cheminCoordonnesTitulaireAttestaion.numVoieGestionnaireTitulaire;
formFields['typeVoieGestionnaireTitulaire']                    = cheminCoordonnesTitulaireAttestaion.typeVoieGestionnaireTitulaire;
formFields['nomVoieGestionnaireTitulaire']                     = cheminCoordonnesTitulaireAttestaion.nomVoieGestionnaireTitulaire;
formFields['codePostalGestionnaireTitulaire']                  = cheminCoordonnesTitulaireAttestaion.codePostalGestionnaireTitulaire;
formFields['localiteGestionnaireTitulaire']                    = cheminCoordonnesTitulaireAttestaion.localiteGestionnaireTitulaire;
formFields['numTelGestionnaireTitulaire']                      = cheminCoordonnesTitulaireAttestaion.numTelGestionnaireTitulaire;
formFields['numFaxGestionnaireTitulaire']                      = cheminCoordonnesTitulaireAttestaion.numFaxGestionnaireTitulaire;
formFields['emailGestionnaireTitulaire']                       = cheminCoordonnesTitulaireAttestaion.emailGestionnaireTitulaire;


/*******************************************************************************
 * Type de l'attestation du transport de marchandises
 ******************************************************************************/
var cheminAttestationTransportMarchandises = $ds030PE5.attestationTransportMarchandisePage.attestationTransportMarchandiseGroupe;

formFields['attestationcapacitePlus35']                       = cheminAttestationTransportMarchandises.attestationcapacitePlus35;
formFields['attestationcapaciteMois35']                       = cheminAttestationTransportMarchandises.attestationcapaciteMois35;
formFields['numAttestationGestionnaireTitulaire']             = cheminAttestationTransportMarchandises.numAttestationGestionnaireTitulaire;
formFields['dateGestionnaireTitulaire']                       = cheminAttestationTransportMarchandises.dateGestionnaireTitulaire;
formFields['regionGestionnaireTitulaire']                     = cheminAttestationTransportMarchandises.regionGestionnaireTitulaire;
formFields['payesGestionnaireTitulaire']                      = cheminAttestationTransportMarchandises.payesGestionnaireTitulaire;
formFields['aucuneAutreActTransMarchandise']                  = cheminAttestationTransportMarchandises.aucuneAutreActTransMarchandise;
formFields['exercerAutreEntrepriseMarchandise']               = cheminAttestationTransportMarchandises.exercerAutreEntrepriseMarchandise;
formFields['heurePresenceGestionnaireTitulaire']              = cheminAttestationTransportMarchandises.heurePresenceGestionnaireTitulaire;
formFields['jourPresenceGestionnaireTitulaire']               = cheminAttestationTransportMarchandises.jourPresenceGestionnaireTitulaire;


/*******************************************************************************
 * Coordonnées de l'entreprise A - Transport de marchandises
 ******************************************************************************/
var cheminIdentificationEntrepriseA = $ds030PE5.identificationEntrepriseAPage.identificationEntrepriseAGroupe;

formFields['sirenGestionnaireTitulaireA']                      = cheminIdentificationEntrepriseA.sirenGestionnaireTitulaireA;
formFields['denominationGestionnaireTitulaireA']               = cheminIdentificationEntrepriseA.denominationGestionnaireTitulaireA;
formFields['adresseGestionnaireTitulaireA']                    = cheminIdentificationEntrepriseA.adresseGestionnaireTitulaireA;
formFields['complementAdresseGestionnaireTitulaireA']          = cheminIdentificationEntrepriseA.complementAdresseGestionnaireTitulaireA;
formFields['codePostalGestionnaireTitulaireA']                 = cheminIdentificationEntrepriseA.codePostalGestionnaireTitulaireA;
formFields['communeVilleGestionnaireTitulaireA']               = cheminIdentificationEntrepriseA.communeVilleGestionnaireTitulaireA;
formFields['qualiteGestionnaireTitulaireA']                    = cheminIdentificationEntrepriseA.qualiteGestionnaireTitulaireA;
formFields['remunirationGestionnaireTitulaireA']               = cheminIdentificationEntrepriseA.remunirationGestionnaireTitulaireA;
formFields['nbrHeureMoisGestionnaireTitulaireA']               = cheminIdentificationEntrepriseA.nbrHeureMoisGestionnaireTitulaireA;
formFields['nbrVehiculeGestionnaireTitulaireA']                = cheminIdentificationEntrepriseA.nbrVehiculeGestionnaireTitulaireA;


/*******************************************************************************
 * Coordonnées de l'entreprise  - Transport de marchandises
 ******************************************************************************/
var cheminIdentificationEntrepriseB = $ds030PE5.identificationEntrepriseBPage.identificationEntrepriseBGroupe;

formFields['denominationGestionnaireTitulaireB']               = cheminIdentificationEntrepriseB.denominationGestionnaireTitulaireB;
formFields['sirenGestionnaireTitulaireB']                      = cheminIdentificationEntrepriseB.sirenGestionnaireTitulaireB;
formFields['adresseGestionnaireTitulaireB']                    = cheminIdentificationEntrepriseB.adresseGestionnaireTitulaireB;
formFields['complementAdresseGestionnaireTitulaireB']          = cheminIdentificationEntrepriseB.complementAdresseGestionnaireTitulaireB;
formFields['codePostalGestionnaireTitulaireB']                 = cheminIdentificationEntrepriseB.codePostalGestionnaireTitulaireB;
formFields['communeVilleGestionnaireTitulaireB']               = cheminIdentificationEntrepriseB.communeVilleGestionnaireTitulaireB;
formFields['qualiteGestionnaireTitulaireB']                    = cheminIdentificationEntrepriseB.qualiteGestionnaireTitulaireB;
formFields['remunirationGestionnaireTitulaireB']               = cheminIdentificationEntrepriseB.remunirationGestionnaireTitulaireB;
formFields['nbrHeureMoisGestionnaireTitulaireB']               = cheminIdentificationEntrepriseB.nbrHeureMoisGestionnaireTitulaireB;
formFields['nbrVehiculeGestionnaireTitulaireB']                = cheminIdentificationEntrepriseB.nbrVehiculeGestionnaireTitulaireB;


/*************************************************************************************************
 * Etat civil du titulaire de l'attestation de capacité professionnelle pour le transport routier
 *************************************************************************************************/
var cheminEtatCivilPersonneTitulaire = $ds030PE5.etatCivilPersonneTitulairePage.etatCivilPersonneTitulaireGroupe;

formFields['personneInterneTitulaire']                         = cheminEtatCivilPersonneTitulaire.personneInterneTitulaire;
formFields['personnePrestaTitulaire']                          = cheminEtatCivilPersonneTitulaire.personnePrestaTitulaire;
formFields['madamePersonne']                                   = cheminEtatCivilPersonneTitulaire.madamePersonne;
formFields['monsieurPersonne']                                 = cheminEtatCivilPersonneTitulaire.monsieurPersonne;
formFields['nomPersonneTitulaire']                             = cheminEtatCivilPersonneTitulaire.nomPersonneTitulaire;
formFields['prenomPersonneTitulaire']                          = cheminEtatCivilPersonneTitulaire.prenomPersonneTitulaire;
formFields['nomMaritalPersonneTitulaire']                      = cheminEtatCivilPersonneTitulaire.nomMaritalPersonneTitulaire;
formFields['dateNaissancePersonneTitulaire']                   = cheminEtatCivilPersonneTitulaire.dateNaissancePersonneTitulaire;
formFields['lieuNaissancePersonneTitulaire']                   = cheminEtatCivilPersonneTitulaire.lieuNaissancePersonneTitulaire;
formFields['deptNaissancePersonneTitulaire']                   = cheminEtatCivilPersonneTitulaire.deptNaissancePersonneTitulaire;
formFields['nationalitePersonneTitulaire']                     = cheminEtatCivilPersonneTitulaire.nationalitePersonneTitulaire;


/**************************************************************************************************
 * Coordonnées du titulaire de l'attestation de capacité professionnelle pour le transport routier
 **************************************************************************************************/
var cheminCoordonnesPersonnesTitulaire = $ds030PE5.coordonnesPersonnesTitulairePage.coordonnesPersonnesTitulaireGroupe;

formFields['numVoiePersonneTitulaire']                         = cheminCoordonnesPersonnesTitulaire.numVoiePersonneTitulaire;
formFields['typeVoiePersonneTitulaire']                        = cheminCoordonnesPersonnesTitulaire.typeVoiePersonneTitulaire;
formFields['nomVoiePersonneTitulaire']                         = cheminCoordonnesPersonnesTitulaire.nomVoiePersonneTitulaire;
formFields['codePostalPersonneTitulaire']                      = cheminCoordonnesPersonnesTitulaire.codePostalPersonneTitulaire;
formFields['localitePersonneTitulaire']                        = cheminCoordonnesPersonnesTitulaire.localitePersonneTitulaire;
formFields['numTelPersonneTitulaire']                          = cheminCoordonnesPersonnesTitulaire.numTelPersonneTitulaire;
formFields['numFaxPersonneTitulaire']                          = cheminCoordonnesPersonnesTitulaire.numFaxPersonneTitulaire;
formFields['emailPersonneTitulaire']                           = cheminCoordonnesPersonnesTitulaire.emailPersonneTitulaire;


/*******************************************************************************
 * Type de l'attestation du transport de personnes
 ******************************************************************************/
var cheminAttestationTransportPersonne = $ds030PE5.attestationTransportPersonnePage.attestationTransportPersonneGroupe;

formFields['attestationPersonneTitulairePlus9']                = cheminAttestationTransportPersonne.attestationPersonneTitulairePlus9;
formFields['attestationPersonneTitulaireMoins9']               = cheminAttestationTransportPersonne.attestationPersonneTitulaireMoins9;
formFields['numAttestationPersonneTitulaire']                  = cheminAttestationTransportPersonne.numAttestationPersonneTitulaire;
formFields['datePersonneTitulaire']                            = cheminAttestationTransportPersonne.datePersonneTitulaire;
formFields['regionPersonneTitulaire']                          = cheminAttestationTransportPersonne.regionPersonneTitulaire;
formFields['paysPersonneTitulaire']                            = cheminAttestationTransportPersonne.paysPersonneTitulaire;
formFields['dispense4A']                            		   = cheminAttestationTransportPersonne.dispense4A;
formFields['dispense4B']                            		   = cheminAttestationTransportPersonne.dispense4B;
formFields['dispense4C']                            		   = cheminAttestationTransportPersonne.dispense4C;
formFields['dispense4D']                            		   = cheminAttestationTransportPersonne.dispense4D;
formFields['dispense5']                            		   	   = cheminAttestationTransportPersonne.dispense5;
formFields['aucuneActivitePersonneTitulaire'] 				   = cheminAttestationTransportPersonne.aucuneActivitePersonneTitulaire;
formFields['nbrHeurePresencePersonneTitulaire']                = cheminAttestationTransportPersonne.nbrHeurePresencePersonneTitulaire;
formFields['nbrJourPresencePersonneTitulaire']                 = cheminAttestationTransportPersonne.nbrJourPresencePersonneTitulaire;
formFields['exercerAutreEntreprisePersonneTitulaire']          = cheminAttestationTransportPersonne.exercerAutreEntreprisePersonneTitulaire;


/*******************************************************************************
 * Coordonnées de l'entreprise A - Transport de personnes
 ******************************************************************************/
var cheminIdentificationEntreprisePersonneA = $ds030PE5.identificationEntreprisePersonneAPage.identificationEntrepriseAGroupe;

formFields['sirenPersonneTitulaireA']                          = cheminIdentificationEntreprisePersonneA.sirenPersonneTitulaireA;
formFields['denominationPersonneTitulaireA']                   = cheminIdentificationEntreprisePersonneA.denominationPersonneTitulaireA;
formFields['sirenPersonneTitulaireA']                          = cheminIdentificationEntreprisePersonneA.sirenPersonneTitulaireA;
formFields['nbrVehiculePersonneTitulaireA']                    = cheminIdentificationEntreprisePersonneA.nbrVehiculePersonneTitulaireA;
formFields['adressePersonneTitulaireA']                        = cheminIdentificationEntreprisePersonneA.adressePersonneTitulaireA;
formFields['communePersonneTitulaireA']                        = cheminIdentificationEntreprisePersonneA.communePersonneTitulaireA;
formFields['qualitePersonneTitulaireA']                        = cheminIdentificationEntreprisePersonneA.qualitePersonneTitulaireA;
formFields['remunirationPersonneTitulaireA']                   = cheminIdentificationEntreprisePersonneA.remunirationPersonneTitulaireA;
formFields['nbrHeureMoisPersonneTitulaireA']                   = cheminIdentificationEntreprisePersonneA.nbrHeureMoisPersonneTitulaireA;
formFields['codePostalPersonneTitulaireA']                     = cheminIdentificationEntreprisePersonneA.codePostalPersonneTitulaireA;


/*******************************************************************************
 * Coordonnées de l'entreprise B - Transport de personnes
 ******************************************************************************/
var cheminIdentificationEntreprisePersonneB = $ds030PE5.identificationEntreprisePersonneBPage.identificationEntrepriseBGroupe;

formFields['sirenPersonneTitulaireB']                          = cheminIdentificationEntreprisePersonneB.sirenPersonneTitulaireB;
formFields['denominationPersonneTitulaireB']                   = cheminIdentificationEntreprisePersonneB.denominationPersonneTitulaireB;
formFields['adressePersonneTitulaireB']                        = cheminIdentificationEntreprisePersonneB.adressePersonneTitulaireB;
formFields['codePostalPersonneTitulaireB']                     = cheminIdentificationEntreprisePersonneB.codePostalPersonneTitulaireB;
formFields['communePersonneTitulaireB']                        = cheminIdentificationEntreprisePersonneB.communePersonneTitulaireB;
formFields['qualitePersonneTitulaireB']                        = cheminIdentificationEntreprisePersonneB.qualitePersonneTitulaireB;
formFields['remunirationPersonneTitulaireB']                   = cheminIdentificationEntreprisePersonneB.remunirationPersonneTitulaireB;
formFields['nbrHeureMoisPersonneTitulaireB']                   = cheminIdentificationEntreprisePersonneB.nbrHeureMoisPersonneTitulaireB;
formFields['nbrVehiculePersonneTitulaireB']                    = cheminIdentificationEntreprisePersonneB.nbrVehiculePersonneTitulaireB;


/**************************************************************************************************************
 * Etat civil de l'attestataire de capacité professionnelle pour une entreprise de commissionnaire de transport
 **************************************************************************************************************/
var cheminIdAttestEntreprise = $ds030PE5.idAttestEntreprisePage.idAttestEntrepriseGroupe;

formFields['madameCommissionnaire']                            = cheminIdAttestEntreprise.madameCommissionnaire;
formFields['monsieurCommissionnaire']                          = cheminIdAttestEntreprise.monsieurCommissionnaire;
formFields['nomCommissionnaire']                               = cheminIdAttestEntreprise.nomCommissionnaire;
formFields['nomMaritalCommissionnaire']                        = cheminIdAttestEntreprise.nomMaritalCommissionnaire;
formFields['prenomMaritalCommissionnaire']                     = cheminIdAttestEntreprise.prenomMaritalCommissionnaire;
formFields['dateNaissanceCommissionnaire']                     = cheminIdAttestEntreprise.dateNaissanceCommissionnaire;
formFields['lieuNaissnceCommissionnaire']                      = cheminIdAttestEntreprise.lieuNaissnceCommissionnaire;
formFields['deptNaissanceCommissionnaire']                     = cheminIdAttestEntreprise.deptNaissanceCommissionnaire;
formFields['nationaliteCommissionnaire']                       = cheminIdAttestEntreprise.nationaliteCommissionnaire;


/****************************************************************************************************************
 * Coordonnées de l'attestataire de capacité professionnelle pour une entreprise de commissionnaire de transport
 ****************************************************************************************************************/
var cheminCoordonnesAttestEntreprise = $ds030PE5.coordonnesAttestEntreprisePage.coordonnesPersonnesTitulaireGroupe;

formFields['numVoieCommissionnaire']                           = cheminCoordonnesAttestEntreprise.numVoieCommissionnaire;
formFields['typeVoieCommissionnaire']                          = cheminCoordonnesAttestEntreprise.typeVoieCommissionnaire;
formFields['nomVoieCommissionnaire']                           = cheminCoordonnesAttestEntreprise.nomVoieCommissionnaire;
formFields['codePostalCommissionnaire']                        = cheminCoordonnesAttestEntreprise.codePostalCommissionnaire;
formFields['localiteCommissionnaire']                          = cheminCoordonnesAttestEntreprise.localiteCommissionnaire;
formFields['numTelCommissionnaire']                            = cheminCoordonnesAttestEntreprise.numTelCommissionnaire;
formFields['numFaxCommissionnaire']                            = cheminCoordonnesAttestEntreprise.numFaxCommissionnaire;
formFields['emailCommissionnaire']                             = cheminCoordonnesAttestEntreprise.emailCommissionnaire;


/*******************************************************************************
 * Identification de l'attestation de capacité professionnelle
 ******************************************************************************/
var cheminAttestationTransportAttestataire = $ds030PE5.attestationTransportAttestatairePage.attestationTransportAttestataireGroupe;

formFields['numAttestationCommissionnaire']                    = cheminAttestationTransportAttestataire.numAttestationCommissionnaire;
formFields['dateCommissionnaire']                              = cheminAttestationTransportAttestataire.dateCommissionnaire;
formFields['regionCommissionnaire']                            = cheminAttestationTransportAttestataire.regionCommissionnaire;
formFields['paysCommissionnaire']                              = cheminAttestationTransportAttestataire.paysCommissionnaire;
formFields['exercerAutreActiviteCommissionnaire']              = cheminAttestationTransportAttestataire.exercerAutreActiviteCommissionnaire;
formFields['autreActiviteCommissionnaire']              	   = cheminAttestationTransportAttestataire.autreActiviteCommissionnaire;
formFields['nbrHeureCommissionnaire']                          = cheminAttestationTransportAttestataire.nbrHeureCommissionnaire;
formFields['nbrJourCommissionnaire']                           = cheminAttestationTransportAttestataire.nbrJourCommissionnaire;

/*******************************************************************************
 * Coordonnées de l'entreprise A --> Attestataire de capacité pro
 ******************************************************************************/
var cheminAutreActiviteAttestataireA = $ds030PE5.autreActiviteAttestataireAPage.autreActiviteAttestataireAGroupe;

formFields['denominationAutreActCommissionnaire']              = cheminAutreActiviteAttestataireA.denominationAutreActCommissionnaire;
formFields['sirenAutreActCommissionnaire']              	   = cheminAutreActiviteAttestataireA.sirenAutreActCommissionnaire;
formFields['adresseAutreActCommissionnaire']                   = cheminAutreActiviteAttestataireA.adresseAutreActCommissionnaire;
formFields['codePostalAutreActCommissionnaire']                = cheminAutreActiviteAttestataireA.codePostalAutreActCommissionnaire;
formFields['communeAutreActCommissionnaire']                   = cheminAutreActiviteAttestataireA.communeAutreActCommissionnaire;
formFields['qualiteAutreActCommissionnaire']                   = cheminAutreActiviteAttestataireA.qualiteAutreActCommissionnaire;
formFields['remunirationAutreActCommissionnaire']              = cheminAutreActiviteAttestataireA.remunirationAutreActCommissionnaire;
formFields['nbrHeureAutreActCommissionnaire']                  = cheminAutreActiviteAttestataireA.nbrHeureAutreActCommissionnaire;


/*******************************************************************************
 * Coordonnées de l'entreprise B --> Attestataire de capacité pro
 ******************************************************************************/
var cheminAutreActiviteAttestataireB = $ds030PE5.autreActiviteAttestataireBPage.autreActiviteAttestataireBGroupe;

formFields['denominationAutreActCommissionnaireB']             = cheminAutreActiviteAttestataireB.denominationAutreActCommissionnaireB;
formFields['sirenAutreActCommissionnaireB']              	   = cheminAutreActiviteAttestataireB.sirenAutreActCommissionnaireB;
formFields['adresseAutreActCommissionnaireB']                  = cheminAutreActiviteAttestataireB.adresseAutreActCommissionnaireB;
formFields['codePostalAutreActCommissionnaireB']               = cheminAutreActiviteAttestataireB.codePostalAutreActCommissionnaireB;
formFields['communeAutreActCommissionnaireB']                  = cheminAutreActiviteAttestataireB.communeAutreActCommissionnaireB;
formFields['qualiteAutreActCommissionnaireB']                  = cheminAutreActiviteAttestataireB.qualiteAutreActCommissionnaireB;
formFields['remunirationAutreActCommissionnaireB']             = cheminAutreActiviteAttestataireB.remunirationAutreActCommissionnaireB;
formFields['nbrHeureAutreActCommissionnaireB']                 = cheminAutreActiviteAttestataireB.nbrHeureAutreActCommissionnaireB;


/*******************************************************************************
 * Identification des personnes devant satisfaire à l'exigence d'honorabilité
 ******************************************************************************/
var cheminExigenceHonorabilite = $ds030PE5.exigenceHonorabilitePage.exigenceHonorabiliteGroup;

formFields['civiliteHonorabilite']                     	       = (cheminEtatCivil.civilite=='madameHonorabilite');
formFields['civiliteHonorabilite']                   		   = (cheminEtatCivil.civilite=='monsieurHonorabilite');
formFields['nomHonorabilite']                                  = cheminExigenceHonorabilite.nomHonorabilite;
formFields['nomMaritalHonorabilite']                           = cheminExigenceHonorabilite.nomMaritalHonorabilite;
formFields['prenomHonorabilite']                               = cheminExigenceHonorabilite.prenomHonorabilite;
formFields['dateNaissanceHonorabilite']                        = cheminExigenceHonorabilite.dateNaissanceHonorabilite;
formFields['deptNaissanceHonorabilite']                        = cheminExigenceHonorabilite.deptNaissanceHonorabilite;
formFields['natinaliteHonorabilite']                           = cheminExigenceHonorabilite.natinaliteHonorabilite;
formFields['dateEntreeEnFontionHonorabilite']                  = cheminExigenceHonorabilite.dateEntreeEnFontionHonorabilite;
formFields['attestaHonneur5ans']                  			   = cheminExigenceHonorabilite.attestaHonneur5ans ? true : false;

 ////////////////////////////////////////// Adresse de résidence des 5 dernières années ///////////////////////////////

formFields['dateResidence']                                    = cheminExigenceHonorabilite.coordonnesPersonnesTitulaireGroupe.dateResidence;
formFields['numVoieHonorabilite']                              = cheminExigenceHonorabilite.coordonnesPersonnesTitulaireGroupe.numVoieHonorabilite;
formFields['typeVoieHonorabilite']                             = cheminExigenceHonorabilite.coordonnesPersonnesTitulaireGroupe.typeVoieHonorabilite;
formFields['nomVoieHonorabilite']                        	   = cheminExigenceHonorabilite.coordonnesPersonnesTitulaireGroupe.nomVoieHonorabilite;
formFields['codePostalHonorabilite']                           = cheminExigenceHonorabilite.coordonnesPersonnesTitulaireGroupe.codePostalHonorabilite;
formFields['localiteHonorabilite']                             = cheminExigenceHonorabilite.coordonnesPersonnesTitulaireGroupe.localiteHonorabilite;
formFields['numTelHonorabilite']                  			   = cheminExigenceHonorabilite.coordonnesPersonnesTitulaireGroupe.numTelHonorabilite;
formFields['numFaxHonorabilite']                           	   = cheminExigenceHonorabilite.coordonnesPersonnesTitulaireGroupe.numFaxHonorabilite;
formFields['emailHonorabilite']                  			   = cheminExigenceHonorabilite.coordonnesPersonnesTitulaireGroupe.emailHonorabilite;

/*******************************************************************************
 * Identification des personnes devant satisfaire à l'exigence d'honorabilité BIS
 ******************************************************************************/
var cheminExigenceHonorabiliteBis = $ds030PE5.exigenceHonorabilitePageBis.exigenceHonorabiliteGroupBis;

formFields['civiliteHonorabiliteBis']                     	   = (cheminExigenceHonorabiliteBis.civilite=='madameHonorabiliteBis');
formFields['civiliteHonorabiliteBis']                   	   = (cheminExigenceHonorabiliteBis.civilite=='monsieurHonorabiliteBis');
formFields['nomHonorabiliteBis']                               = cheminExigenceHonorabiliteBis.nomHonorabiliteBis;
formFields['nomMaritalHonorabiliteBis']                        = cheminExigenceHonorabiliteBis.nomMaritalHonorabiliteBis;
formFields['prenomHonorabiliteBis']                            = cheminExigenceHonorabiliteBis.prenomHonorabiliteBis;
formFields['dateNaissanceHonorabiliteBis']                     = cheminExigenceHonorabiliteBis.dateNaissanceHonorabiliteBis;
formFields['deptNaissanceHonorabiliteBis']                     = cheminExigenceHonorabiliteBis.deptNaissanceHonorabiliteBis;
formFields['natinaliteHonorabiliteBis']                        = cheminExigenceHonorabiliteBis.natinaliteHonorabiliteBis;
formFields['dateEntreeEnFontionHonorabiliteBis']               = cheminExigenceHonorabiliteBis.dateEntreeEnFontionHonorabiliteBis;

 ////////////////////////////////////////// Adresse de résidence des 5 dernières années BIS///////////////////////////////

formFields['dateResidenceBis']                                 = cheminExigenceHonorabiliteBis.coordonnesPersonnesTitulaireGroupeBis.dateResidenceBis;
formFields['numVoieHonorabiliteBis']                           = cheminExigenceHonorabiliteBis.coordonnesPersonnesTitulaireGroupeBis.numVoieHonorabiliteBis;
formFields['typeVoieHonorabiliteBis']                          = cheminExigenceHonorabiliteBis.coordonnesPersonnesTitulaireGroupeBis.typeVoieHonorabiliteBis;
formFields['nomVoieHonorabiliteBis']                           = cheminExigenceHonorabiliteBis.coordonnesPersonnesTitulaireGroupeBis.nomVoieHonorabiliteBis;
formFields['codePostalHonorabiliteBis']                        = cheminExigenceHonorabiliteBis.coordonnesPersonnesTitulaireGroupeBis.codePostalHonorabiliteBis;
formFields['localiteHonorabiliteBis']                          = cheminExigenceHonorabiliteBis.coordonnesPersonnesTitulaireGroupeBis.localiteHonorabiliteBis;
formFields['numTelHonorabiliteBis']                  		   = cheminExigenceHonorabiliteBis.coordonnesPersonnesTitulaireGroupeBis.numTelHonorabiliteBis;
formFields['numFaxHonorabiliteBis']                            = cheminExigenceHonorabiliteBis.coordonnesPersonnesTitulaireGroupeBis.numFaxHonorabiliteBis;
formFields['emailHonorabiliteBis']                  		   = cheminExigenceHonorabiliteBis.coordonnesPersonnesTitulaireGroupeBis.emailHonorabiliteBis;


/*******************************************************************************
 * Signature
 ******************************************************************************/
var cheminSignature = $ds030PE5.signaturePage.signatureGroupe;

formFields['declarationHonneur']                         	   = cheminSignature.declarationHonneur;
formFields['declarationHonneurRespectCond']                    = cheminSignature.declarationHonneurRespectCond;
formFields['declarationHonneurRespectDsiposition']             = cheminSignature.declarationHonneurRespectDsiposition;
formFields['declarationHonneurRegistreCommissionnaire']        = cheminSignature.declarationHonneurRegistreCommissionnaire;
formFields['declarationHonneurSignaler']                       = cheminSignature.declarationHonneurSignaler;
formFields['dateSignature']                         		   = cheminSignature.dateSignature;



var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		dateSignature : cheminSignature.dateSignature,
		autoriteHabilitee :"Soumis à conditions ;) .",
		demandeContexte : "Demande d'inscription au registre des commissionnaires de transport."
//		civiliteNomPrenom : civNomPrenom
});	

	
var cerfaDoc = nash.doc //
.load('models/Cerfa_11414_05.pdf') //
.apply(formFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));

/*******************************************************************************
 * Ajout des PJs
 ******************************************************************************/

function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}


//appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
//appendPj($attachmentPreprocess.attachmentPreprocess.pjJustifDomicile);
//appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificatifNonResident);
//appendPj($attachmentPreprocess.attachmentPreprocess.pjJustifEtude);
//appendPj($attachmentPreprocess.attachmentPreprocess.pjServiceNational);



var finalDocItem = finalDoc.save('Commissionnaire_de_transport.pdf');


return spec.create({
id : 'review',
label : 'Commissionnaire de transport - Demande d\'inscription au registre des commissionnaires de transport.',
groups : [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : [ spec.createData({
        id : 'formulaire',
        label : 'Demande d\'inscription au registre des commissionnaires de transport.',
        description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
        type : 'FileReadOnly',
        value : [ finalDocItem ]
    }) ]
}) ]
});