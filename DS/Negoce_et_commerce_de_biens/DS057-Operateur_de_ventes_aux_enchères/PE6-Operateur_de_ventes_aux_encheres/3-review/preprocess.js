function pad(s) { return (s < 10) ? '0' + s : s; } 
var cerfaFields = {};
var adresseVar = Value('id').of($ds057PE6.etatCivil.typePersonne.typeDeclarant).eq("personnePhysique") ? $ds057PE6.adresse.adresseContact : $ds057PE6.adresse.adresseContact2;
var civNomPrenom = Value('id').of($ds057PE6.etatCivil.typePersonne.typeDeclarant).eq("personnePhysique") ? 
	$ds057PE6.etatCivil.identificationDeclarant1.civilite + ' ' + $ds057PE6.etatCivil.identificationDeclarant1.nomDeclarant + ' ' + $ds057PE6.etatCivil.identificationDeclarant1.prenomDeclarant 
	: $ds057PE6.etatCivil.identificationDeclarant2.representantLegal;
var abrev2 = $ds057PE6.etatCivil.identificationDeclarant2;

//adresse
cerfaFields['adresse'] 						= adresseVar.numeroLibelleAdresseDeclarant + (adresseVar.complementAdresseDeclarant != null ? ', ' + adresseVar.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       				= (adresseVar.codePostalAdresseDeclarant != null ? adresseVar.codePostalAdresseDeclarant + ' ' : '') + adresseVar.villeAdresseDeclarant + ', ' + adresseVar.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      		= adresseVar.telephoneMobileAdresseDeclarant != null ? "Téléphone mobile : "+adresseVar.telephoneMobileAdresseDeclarant : "Représentant légal : "+abrev2.representantLegal; 
cerfaFields['telephoneFixe']          		= adresseVar.telephoneAdresseDeclarant != null ? "Téléphone fixe : "+adresseVar.telephoneAdresseDeclarant : "SIREN : "+abrev2.siren;
cerfaFields['courriel']          			= adresseVar.mailAdresseDeclarant != null ? "Courriel: "+adresseVar.mailAdresseDeclarant : "Courriel: "+ abrev2.mailAdresseDeclarant;

//signature
cerfaFields['date']                			= $ds057PE6.signature.signature.dateSignature;
cerfaFields['signature']           			= $ds057PE6.signature.signature.signature;
cerfaFields['lieuSignature']                = $ds057PE6.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']	= civNomPrenom;
cerfaFields['libelleProfession']			= "Opérateur de ventes aux enchères";	

	
//etatCivil
if (Value('id').of($ds057PE6.etatCivil.typePersonne.typeDeclarant).eq("personnePhysique")) {
	var abrev1 = $ds057PE6.etatCivil.identificationDeclarant1;
	var dateTemp = new Date(parseInt (abrev1.dateNaissanceDeclarant.getTimeInMillis()));
	var monthTemp = dateTemp.getMonth() + 1;
	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
	var year = dateTemp.getFullYear().toString();
	
	cerfaFields['civiliteNomPrenom']          	= civNomPrenom;
	cerfaFields['villePaysNaissance']  			= "A: "+ abrev1.lieuNaissanceDeclarant + ', ' + abrev1.paysNaissanceDeclarant;
	cerfaFields['nationalite']                	= "Nationalité: "+ abrev1.nationaliteDeclarant;
	cerfaFields['dateNaissance']             	= "Né(e) le: "+ day +"/"+ month +"/"+ year;
}

if (Value('id').of($ds057PE6.etatCivil.typePersonne.typeDeclarant).eq("personneMorale")) {
	cerfaFields['civiliteNomPrenom']          	= "Raison sociale : "+abrev2.raisonSociale;
	cerfaFields['dateNaissance']             	= "Tél: " + abrev2.telephoneMobileAdresseDeclarant;
}

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $ds057PE6.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
.load('models/Courrier au premier dossier v2.1 GE.pdf') //
.apply({
	dateSignature: $ds057PE6.signature.signature.dateSignature ,
	autoriteHabilitee :"Conseil des ventes volontaires de meubles aux enchères publiques" ,
	demandeContexte : "Déclaration préalable en vue d'une libre prestation de services pour l'exercice de la profession d'opérateur de ventes aux enchères.",
	civiliteNomPrenom : civNomPrenom
});	

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier_libre_LPS_V4.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExercice);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjIndication);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustification);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurance);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Opérateur de ventes aux enchères_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Opérateur de ventes aux enchères - déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession d\'opérateur de ventes aux enchères',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
