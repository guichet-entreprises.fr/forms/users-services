var formFields = {};
function pad(s) { return (s < 10) ? '0' + s : s; }	


formFields['raisonSociale']                     = $ds057PE4.declarationGroup.identification.raisonSociale;
formFields['denominationCom']                 = $ds057PE4.declarationGroup.identification.denominationCom;
formFields['dateCreation']                         = $ds057PE4.declarationGroup.identification.dateCreation;

var dateTmp = new Date(parseInt($ds057PE4.declarationGroup.identification.dateCreation.getTimeInMillis()));
var date = pad(dateTmp.getDate().toString());
var month = dateTmp.getMonth() + 1;
date = date.concat(pad(month.toString()));
date = date.concat(dateTmp.getFullYear().toString());
formFields['dateCreation'] = date;





formFields['formeJuridique']                       = $ds057PE4.declarationGroup.identification.formeJuridique;
formFields['codeAPENAF']                       	   = $ds057PE4.declarationGroup.identification.codeAPENAF;
formFields['activitePrincipale']    				= $ds057PE4.declarationGroup.identification.activitePrincipale;
formFields['numeroSIRET']                          = $ds057PE4.declarationGroup.identification.numeroSIRET;
formFields['adresseNVoie']                          = $ds057PE4.declarationGroup.identification.adresseNVoie ;
formFields['adresseVoie']                          = $ds057PE4.declarationGroup.identification.adresseVoie + ' ' 
															+ ($ds057PE4.declarationGroup.identification.adresseVoieComplement ? $ds057PE4.declarationGroup.identification.adresseVoieComplement : '');
formFields['adresseCP']                            = $ds057PE4.declarationGroup.identification.adresseCP;
formFields['adresseVille']                         = $ds057PE4.declarationGroup.identification.adresseVille;
formFields['email']                                = $ds057PE4.declarationGroup.identification.email;
if($ds057PE4.declarationGroup.identification.adressedifferenteQuestion){
	formFields['adresseDifferenteNom']                     = $ds057PE4.declarationGroup.identification.adressedifferenteGroup.adresseDifferenteNom ;
	formFields['adresseDifferenteNVoie']                    = $ds057PE4.declarationGroup.identification.adressedifferenteGroup.adressedifferenteNVoie ;
	formFields['adresseDifferenteVoie']                    =  $ds057PE4.declarationGroup.identification.adressedifferenteGroup.adressedifferenteVoie + ' ' 
															+ ($ds057PE4.declarationGroup.identification.adressedifferenteGroup.adressedifferenteComplement ? $ds057PE4.declarationGroup.identification.adressedifferenteGroup.adressedifferenteComplement : '');

	formFields['adresseDifferenteCP']                  = $ds057PE4.declarationGroup.identification.adressedifferenteGroup.adressedifferenteCP;
	formFields['adresseDifferenteVille']               = $ds057PE4.declarationGroup.identification.adressedifferenteGroup.adressedifferenteVille;
}else {
	formFields['adresseDifferenteNom']                    = '';
	formFields['adresseDifferenteNVoie']                    = '';
	formFields['adresseDifferenteVoie']                    = '';
	formFields['adresseDifferenteCP']                  = '';
	formFields['adresseDifferenteVille']               = '';
}

formFields['representantLegal']                    = $ds057PE4.declarationGroup.identification.representantLegal;
formFields['representantLegal1']     = '';


formFields['signatureNom']                         = $ds057PE4.signatureGroup.signature.signatureNom +' '+ $ds057PE4.signatureGroup.signature.signaturePrenom;
formFields['signatureTelephone']                     = $ds057PE4.signatureGroup.signature.signatureTelephone;
formFields['signatureLe']                      = $ds057PE4.signatureGroup.signature.signatureFaitLe;
var date2Tmp2 = new Date(parseInt($ds057PE4.signatureGroup.signature.signatureFaitLe.getTimeInMillis()));
var date2 = pad(date2Tmp2.getDate().toString());
var month2 = date2Tmp2.getMonth() + 1;
date2 = date2.concat(pad(month2.toString()));
date2 = date2.concat(date2Tmp2.getFullYear().toString());
formFields['signatureLe'] = date2;





formFields['signature']                            = "Cette déclaration respecte les attendus de l’article 1367 du code civil." ;



 var cerfaDoc1 = nash.doc //
	.load('models/declaration-d-existence-agessa.pdf') //
	.apply (formFields);



/**********************
Pieces jointes
***********************/
function appendPj(fld) {
	fld.forEach(function (elm) {
        cerfaDoc1.append(elm);
    });
}	
	
appendPj($attachmentPreprocess.attachmentPreprocess.pjID); 




/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $ds057PE4.signatureGroup.signature.signatureFaitLe ,
		autoriteHabilitee :"Agessa – Service diffuseurs" ,
		demandeContexte : "Déclaration d’existence auprès de l’Agessa.",
		civiliteNomPrenom : nomPrenom
	});
	finalDoc.append(cerfaDoc1.save('cerfa.pdf'));	
	
	
/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Opérateur de ventes aux enchères_Agessa.pdf');	
	
	
/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Opérateur de ventes aux enchères - Déclaration d’existence auprès de l’Agessa',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : "Dossier de déclaration d’existence du diffuseur.",
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});