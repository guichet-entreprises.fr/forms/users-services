var cerfaFields = {};
//etatCivil

var civNomPrenom = $ds057PE2.etatCivil.identificationDeclarant.civilite + ' ' + $ds057PE2.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $ds057PE2.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['nomDeclarant']             = $ds057PE2.etatCivil.identificationDeclarant.nomDeclarant;
cerfaFields['prenomDeclarant']          = $ds057PE2.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['lieuNaissanceDeclarant']   = $ds057PE2.etatCivil.identificationDeclarant.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']     = $ds057PE2.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['telephoneFixeDeclarant']   = $ds057PE2.etatCivil.identificationDeclarant.telephoneAdresseDeclarant;
cerfaFields['telephoneMobileDeclarant'] = $ds057PE2.etatCivil.identificationDeclarant.telephoneMobileAdresseDeclarant;
cerfaFields['dateNaissanceDeclarant']   = $ds057PE2.etatCivil.identificationDeclarant.dateNaissanceDeclarant;
cerfaFields['courrielDeclarant']        = $ds057PE2.etatCivil.identificationDeclarant.mailAdresseDeclarant;
cerfaFields['exerciceActivite']         = $ds057PE2.adresse.adresseContact1.exerciceActivite;
cerfaFields['adresseEtablissement1']    = $ds057PE2.adresse.adresseContact1.numeroLibelleAdresseDeclarant 
										  + ($ds057PE2.adresse.adresseContact1.complementAdresseDeclarant != null ? ', ' + $ds057PE2.adresse.adresseContact1.complementAdresseDeclarant : '') 
										  + ($ds057PE2.adresse.adresseContact1.codePostalAdresseDeclarant != null ? ', ' + $ds057PE2.adresse.adresseContact1.codePostalAdresseDeclarant : '')
										  + ' ' + $ds057PE2.adresse.adresseContact1.villeAdresseDeclarant
										  + ', ' + $ds057PE2.adresse.adresseContact1.paysAdresseDeclarant;
cerfaFields['adresseEtablissement2']    = $ds057PE2.adresse.adresseContact2.numeroLibelleAdresseDeclarant 
										  + ($ds057PE2.adresse.adresseContact2.complementAdresseDeclarant != null ? ', ' + $ds057PE2.adresse.adresseContact2.complementAdresseDeclarant : '') 
										  + ($ds057PE2.adresse.adresseContact2.codePostalAdresseDeclarant != null ? ', ' + $ds057PE2.adresse.adresseContact2.codePostalAdresseDeclarant : '')
										  + ' ' + $ds057PE2.adresse.adresseContact2.villeAdresseDeclarant
										  + ', ' + $ds057PE2.adresse.adresseContact2.paysAdresseDeclarant;
cerfaFields['civNomPrenom']             = civNomPrenom;
cerfaFields['activite']                 = 'Opérateur de ventes aux enchères';
/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $ds057PE2.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		dateSignature: $ds057PE2.signature.signature.dateSignature,
		autoriteHabilitee :"Conseil des ventes volontaires de meubles aux enchères publiques",
		demandeContexte : "Déclaration d’activité",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier_libre_LE_V4.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID1);
appendPj($attachmentPreprocess.attachmentPreprocess.pjID2);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDocuments1);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDocuments2);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDocuments3);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDocuments4);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDocuments5);
appendPj($attachmentPreprocess.attachmentPreprocess.pjStatut);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPropriete);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDirigeants);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Opérateur_de_ventes_aux_enchères_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Opérateur de ventes aux enchères - déclaration d’activité',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de déclaration d’activité pour l\'exercice de la profession d\'opérateur de ventes aux enchères.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
