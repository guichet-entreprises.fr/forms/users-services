var cerfaFields = {};
//etatCivil

var civNomPrenom = $ds057PE1.etatCivil.identificationDeclarant.civilite + ' ' + $ds057PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $ds057PE1.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['civiliteNomPrenom']          	    = $ds057PE1.etatCivil.identificationDeclarant.civilite + ' ' + $ds057PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $ds057PE1.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			 	= $ds057PE1.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $ds057PE1.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                		= $ds057PE1.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             		= $ds057PE1.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 							= $ds057PE1.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($ds057PE1.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $ds057PE1.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       					= ($ds057PE1.adresse.adresseContact.codePostalAdresseDeclarant != null ? $ds057PE1.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $ds057PE1.adresse.adresseContact.villeAdresseDeclarant + ', ' + $ds057PE1.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      			= $ds057PE1.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          			= $ds057PE1.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          				= $ds057PE1.adresse.adresseContact.mailAdresseDeclarant;


//signature
cerfaFields['date']                				= $ds057PE1.signature.signature.dateSignature;
cerfaFields['signature']           				= $ds057PE1.signature.signature.signature;
cerfaFields['lieuSignature']                  	= $ds057PE1.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']		= $ds057PE1.etatCivil.identificationDeclarant.civilite + ' ' + $ds057PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $ds057PE1.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['libelleProfession']				= "Opérateur de ventes aux enchères"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $ds057PE1.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		dateSignature: $ds057PE1.signature.signature.dateSignature,
		autoriteHabilitee :"Conseil des ventes volontaires de meubles aux enchères publiques",
		demandeContexte : "Déclaration d’activité",
		civiliteNomPrenom : civNomPrenom
	});

var finalDoc2 = nash.doc //
	.load('models/Declaration_sur_l_honneur.pdf') //
	.apply({
		dateSignature: $ds057PE1.signature.signature.dateSignature,
		nomPrenomDeclarant : civNomPrenom,
		lieuNaissanceDeclarant : $ds057PE1.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $ds057PE1.etatCivil.identificationDeclarant.paysNaissanceDeclarant,
		dateNaissanceDeclarant : $ds057PE1.etatCivil.identificationDeclarant.dateNaissanceDeclarant,
		adresseDeclarant	   : $ds057PE1.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($ds057PE1.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' 
								+ $ds057PE1.adresse.adresseContact.complementAdresseDeclarant : ' ') 
								+ ($ds057PE1.adresse.adresseContact.codePostalAdresseDeclarant != null ? $ds057PE1.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') 
								+ $ds057PE1.adresse.adresseContact.villeAdresseDeclarant + ', ' + $ds057PE1.adresse.adresseContact.paysAdresseDeclarant
	});
	
//finalDoc.append(accompDoc.save('courrier.pdf'));

/*generated
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier_libre_LE_V4.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));

finalDoc.append(finalDoc2.save('cerfa2.pdf'));


function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDocuments);
appendPj($attachmentPreprocess.attachmentPreprocess.pjBail);
appendPj($attachmentPreprocess.attachmentPreprocess.pjOuverture);
appendPj($attachmentPreprocess.attachmentPreprocess.pjSouscription);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurance);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Opérateur_de_ventes_aux_enchères_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Opérateur de ventes aux enchères - déclaration d’activité',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de déclaration d’activité pour l\'exercice de la profession d\'opérateur de ventes aux enchères.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
