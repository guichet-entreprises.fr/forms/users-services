var formFields = {};

var chemiDeclaration = $ds057PE5.declarationGroup.declaration;

formFields['nomPro']             = chemiDeclaration.nomPro;
formFields['activite']           = chemiDeclaration.activite;
formFields['adresseSiege']       = $ds057PE5.declarationGroup.declaration.adresseVoie +", "+ ($ds057PE5.declarationGroup.declaration.adresseVoieComplement ? $ds057PE5.declarationGroup.declaration.adresseVoieComplement : ', ') +", "+ $ds057PE5.declarationGroup.declaration.adresseCP +" "+ $ds057PE5.declarationGroup.declaration.adresseVille +" "+ $ds057PE5.declarationGroup.declaration.adressePays ;
formFields['courriel']           = chemiDeclaration.courriel;
formFields['telephone']          = chemiDeclaration.telephone;
formFields['objet']              = 'Opérateur de ventes aux enchères';
//formFields['adresseExercice']    = chemiDeclaration.AdresseExercice;
if($ds057PE5.declarationGroup.declaration.adressedifferenteQuestion){
   formFields['adresseExercice'] = $ds057PE5.declarationGroup.declaration.adressedifferenteGroup.adressedifferenteVoie +", "+ ($ds057PE5.declarationGroup.declaration.adressedifferenteGroup.adressedifferenteComplement ? $ds057PE5.declarationGroup.declaration.adressedifferenteGroup.adressedifferenteComplement: ', ' )+" "+ $ds057PE5.declarationGroup.declaration.adressedifferenteGroup.adressedifferenteCP +" "+ $ds057PE5.declarationGroup.declaration.adressedifferenteGroup.adressedifferenteVille +" "+ $ds057PE5.declarationGroup.declaration.adressedifferenteGroup.adressedifferentePays ;
}else {
   formFields['adresseExercice'] = '';
}

var chaminSignature = $ds057PE5.signatureGroup.signature;

formFields['declarationHonneur'] = chaminSignature.declarationHonneur;
formFields['signature']          = $ds057PE5.signatureGroup.signature.signatureNom +" "+  $ds057PE5.signatureGroup.signature.signaturePrenom;
formFields['dateSignature']      = chaminSignature.dateSignature;
formFields['lieuSignature']      = chaminSignature.lieuSignature;


var nomPrenom = $ds057PE5.signatureGroup.signature.signatureNom +" "+  $ds057PE5.signatureGroup.signature.signaturePrenom; 
var finalDoc = nash.doc //
.load('models/Courrier au premier dossier v2.1 GE.pdf') //
.apply({
	dateSignature: $ds057PE5.signatureGroup.signature.dateSignature ,
	autoriteHabilitee :"Direction Régionale des douanes et droits indirects – bureau de garantie" ,
	demandeContexte : "Opérateur de ventes aux enchères.",
	civiliteNomPrenom : nomPrenom
});	


var cerfaDoc1 = nash.doc //
.load('models/model.pdf') //
.apply (formFields);



/**********************
Pieces jointes
***********************/
function appendPj(fld) {
fld.forEach(function (elm) {
    cerfaDoc1.append(elm);
});
}	

appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificatifExercice);
appendPj($attachmentPreprocess.attachmentPreprocess.pjKKBis);
appendPj($attachmentPreprocess.attachmentPreprocess.pjBail); 


finalDoc.append(cerfaDoc1.save('cerfa.pdf'));


/*
* Chargement du courrier d'accompagnement à destination de l'AC
*/




/*
* Enregistrement du fichier (en mémoire)
*/
var finalDocItem = finalDoc.save('Opérateur_de_ventes_aux_enchères_LE.pdf');	


/*
* Persistance des données obtenues
*/

return spec.create({
id : 'review',
label : 'Opérateur de ventes aux enchères - Déclaration libre de détention de métaux précieux',
groups : [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : [ spec.createData({
        id : 'formulaire',
        label : "Déclaration libre de détention de métaux précieux.",
        description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
        type : 'FileReadOnly',
        value : [ finalDocItem ]
    }) ]
}) ]
});