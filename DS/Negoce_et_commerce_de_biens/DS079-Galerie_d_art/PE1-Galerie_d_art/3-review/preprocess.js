var formFields = {};


formFields['raisonSocialeNom']                     = $ds079PE1.declarationGroup.identification.raisonSocialeNom;
formFields['denominationEnseigne']                 = $ds079PE1.declarationGroup.identification.denominationEnseigne;
formFields['dateCreation']                         = $ds079PE1.declarationGroup.identification.dateCreation;
formFields['formeJuridique']                       = $ds079PE1.declarationGroup.identification.formeJuridique;
formFields['codeNAF']                       	   = $ds079PE1.declarationGroup.identification.codeNAF;
formFields['numeroSiret']                          = $ds079PE1.declarationGroup.identification.numeroSiret;
formFields['adresseVoie']                          = $ds079PE1.declarationGroup.identification.adresseNVoie +' '
															+ $ds079PE1.declarationGroup.identification.adresseVoie + ' ' 
															+ ($ds079PE1.declarationGroup.identification.adresseVoieComplement ? $ds079PE1.declarationGroup.identification.adresseVoieComplement : '');
formFields['adresseCP']                            = $ds079PE1.declarationGroup.identification.adresseCP;
formFields['adresseVille']                         = $ds079PE1.declarationGroup.identification.adresseVille;
formFields['email']                                = $ds079PE1.declarationGroup.identification.email;
if($ds079PE1.declarationGroup.identification.adressedifferenteQuestion){
	formFields['adressedifferente']                    = $ds079PE1.declarationGroup.identification.adressedifferenteGroup.adressedifferenteNVoie +' '
															+ $ds079PE1.declarationGroup.identification.adressedifferenteGroup.adressedifferenteVoie + ' ' 
															+ ($ds079PE1.declarationGroup.identification.adressedifferenteGroup.adressedifferenteComplement ? $ds079PE1.declarationGroup.identification.adressedifferenteGroup.adressedifferenteComplement : '');
	formFields['adresseDifferenteCP']                  = $ds079PE1.declarationGroup.identification.adressedifferenteGroup.adressedifferenteCP;
	formFields['adresseDifferenteVille']               = $ds079PE1.declarationGroup.identification.adressedifferenteGroup.adressedifferenteVille;
}else {
	formFields['adressedifferente']                    = '';
	formFields['adresseDifferenteCP']                  = '';
	formFields['adresseDifferenteVille']               = '';
}

formFields['representantLegal']                    = $ds079PE1.declarationGroup.identification.representantLegal;


formFields['contactNom']                           = $ds079PE1.contactGroup.contact.contactNom;
formFields['contactPrenom']                        = $ds079PE1.contactGroup.contact.contactPrenom;
formFields['contactEmail']                         = $ds079PE1.contactGroup.contact.contactEmail;
formFields['contactTelephone']                     = $ds079PE1.contactGroup.contact.contactTelephone;


formFields['activiteGalerieDArt']                  = Value('id').of($ds079PE1.page2Group.activite.questionStructure).eq('activiteGalerieDArt')					? true : false	;
formFields['activiteOperateurDeVentesVolontaires'] = Value('id').of($ds079PE1.page2Group.activite.questionStructure).eq('activiteOperateurDeVentesVolontaires')	? true : false	;
formFields['activiteVenteEnLigne']                 = Value('id').of($ds079PE1.page2Group.activite.questionStructure).eq('activiteVenteEnLigne')					? true : false	;
formFields['activiteEditeurDArt']                  = Value('id').of($ds079PE1.page2Group.activite.questionStructure).eq('activiteEditeurDArt')					? true : false	;
formFields['activiteMusee']                        = Value('id').of($ds079PE1.page2Group.activite.questionStructure).eq('activiteMusee')							? true : false	;
formFields['activiteAntiquaire']                   = Value('id').of($ds079PE1.page2Group.activite.questionStructure).eq('activiteAntiquaire')						? true : false	;
formFields['activiteBrocanteur']                   = Value('id').of($ds079PE1.page2Group.activite.questionStructure).eq('activiteBrocanteur')						? true : false	;
formFields['activiteAutre']                        = Value('id').of($ds079PE1.page2Group.activite.questionStructure).eq('activiteAutre')							? true : false	; 
formFields['activiteAutrePrecision']               = $ds079PE1.page2Group.activite.activiteAutrePrecision;    
                                                                  
formFields['activiteExclusif']                     = Value('id').of($ds079PE1.page2Group.activite.questionExploitation).eq('activiteExclusif')		? true : false;
formFields['activitePrincipale']                   = Value('id').of($ds079PE1.page2Group.activite.questionExploitation).eq('activitePrincipale')		? true : false;
formFields['activiteAccessoire']                   = Value('id').of($ds079PE1.page2Group.activite.questionExploitation).eq('activiteAccessoire')		? true : false;
formFields['activiteDate']                         = $ds079PE1.page2Group.activite.activiteDate;

formFields['ca2013']                               =$ds079PE1.page2Group.caGroup.ca2013 ? parseFloat($ds079PE1.page2Group.caGroup.ca2013).toFixed(2) :'';
formFields['ca2014']                               =$ds079PE1.page2Group.caGroup.ca2014 ? parseFloat($ds079PE1.page2Group.caGroup.ca2014).toFixed(2) :'';
formFields['ca2015']                               =$ds079PE1.page2Group.caGroup.ca2015 ? parseFloat($ds079PE1.page2Group.caGroup.ca2015).toFixed(2) :'';
formFields['ca2016']                               =$ds079PE1.page2Group.caGroup.ca2016 ? parseFloat($ds079PE1.page2Group.caGroup.ca2016).toFixed(2) :'';
                                                                                                                                                     
/* formFields['ca2013Bis']                            = $ds079PE1.page2Group.caGroup.ca2013Bis;
formFields['ca2014Bis']                            = $ds079PE1.page2Group.caGroup.ca2014Bis;
formFields['ca2015Bis']                            = $ds079PE1.page2Group.caGroup.ca2015Bis;
formFields['ca2016Bis']                            = $ds079PE1.page2Group.caGroup.ca2016Bis;  */



formFields['commissions2013']                      = $ds079PE1.page2Group.commissionsGroup.commissions2013 ? parseFloat($ds079PE1.page2Group.commissionsGroup.commissions2013).toFixed(2) : '';
formFields['commissions2014']                      = $ds079PE1.page2Group.commissionsGroup.commissions2014 ? parseFloat($ds079PE1.page2Group.commissionsGroup.commissions2014).toFixed(2) : '';
formFields['commissions2015']                      = $ds079PE1.page2Group.commissionsGroup.commissions2015 ? parseFloat($ds079PE1.page2Group.commissionsGroup.commissions2015).toFixed(2) : '';
formFields['commissions2016']                      = $ds079PE1.page2Group.commissionsGroup.commissions2016 ? parseFloat($ds079PE1.page2Group.commissionsGroup.commissions2016).toFixed(2) : '';





/*  formFields['commissions2013Bis']                   = $ds079PE1.page2Group.commissionsGroup.commissions2013Bis; 
formFields['commissions2014Bis']                   = $ds079PE1.page2Group.commissionsGroup.commissions2014Bis;
formFields['commissions2015Bis']                   = $ds079PE1.page2Group.commissionsGroup.commissions2015Bis;
formFields['commissions2016Bis']                   = $ds079PE1.page2Group.commissionsGroup.commissions2016Bis; */

formFields['signatureNom']                         = $ds079PE1.signatureGroup.signature.signatureNom +' '+ $ds079PE1.signatureGroup.signature.signaturePrenom;
formFields['signatureQualite']                     = $ds079PE1.signatureGroup.signature.signatureQualite;
formFields['signatureFaitA']                       = $ds079PE1.signatureGroup.signature.signatureFaitA;
formFields['signatureFaitLe']                      = $ds079PE1.signatureGroup.signature.signatureFaitLe;
formFields['signature']                            = "Cette déclaration respecte les attendus de l’article 1367 du code civil." ;




 var cerfaDoc1 = nash.doc //
	.load('models/declaration-d-existence-Art-OVV.pdf') //
	.apply (formFields);



/**********************
Pieces jointes
***********************/
function appendPj(fld) {
	fld.forEach(function (elm) {
        cerfaDoc1.append(elm);
    });
}	
	
appendPj($attachmentPreprocess.attachmentPreprocess.pjID); 
appendPj($attachmentPreprocess.attachmentPreprocess.pjKbis); 




/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $ds079PE1.signatureGroup.signature.signatureFaitLe ,
		autoriteHabilitee :"La maison des artistes – Service diffuseurs" ,
		demandeContexte : "Déclaration d'existence auprès de la Maison des Artistes.",
		civiliteNomPrenom : nomPrenom
	});
	finalDoc.append(cerfaDoc1.save('cerfa.pdf'));	
	
	
/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Galerie-d-art_Art-et-OVV.pdf');	
	
	
/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Galerie d\'art - Déclaration d\'existence auprès de la Maison des Artistes',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : "Dossier de déclaration d’existence du diffuseur réservée aux commerces d’art et OVV.",
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});