var formFields = {};
function pad(s) { return (s < 10) ? '0' + s : s; }	


formFields['raisonSociale']                     = $ds079PE2.declarationGroup.identification.raisonSociale;
formFields['denominationCom']                 = $ds079PE2.declarationGroup.identification.denominationCom;
formFields['dateCreation']                         = $ds079PE2.declarationGroup.identification.dateCreation;

var dateTmp = new Date(parseInt($ds079PE2.declarationGroup.identification.dateCreation.getTimeInMillis()));
var date = pad(dateTmp.getDate().toString());
var month = dateTmp.getMonth() + 1;
date = date.concat(pad(month.toString()));
date = date.concat(dateTmp.getFullYear().toString());
formFields['dateCreation'] = date;





formFields['formeJuridique']                       = $ds079PE2.declarationGroup.identification.formeJuridique;
formFields['codeAPENAF']                       	   = $ds079PE2.declarationGroup.identification.codeAPENAF;
formFields['activitePrincipale']    				= $ds079PE2.declarationGroup.identification.activitePrincipale;
formFields['numeroSIRET']                          = $ds079PE2.declarationGroup.identification.numeroSIRET;
formFields['adresseNVoie']                          = $ds079PE2.declarationGroup.identification.adresseNVoie ;
formFields['adresseVoie']                          = $ds079PE2.declarationGroup.identification.adresseVoie + ' ' 
															+ ($ds079PE2.declarationGroup.identification.adresseVoieComplement ? $ds079PE2.declarationGroup.identification.adresseVoieComplement : '');
formFields['adresseCP']                            = $ds079PE2.declarationGroup.identification.adresseCP;
formFields['adresseVille']                         = $ds079PE2.declarationGroup.identification.adresseVille;
formFields['email']                                = $ds079PE2.declarationGroup.identification.email;
if($ds079PE2.declarationGroup.identification.adressedifferenteQuestion){
	formFields['adresseDifferenteNom']                     = $ds079PE2.declarationGroup.identification.adressedifferenteGroup.adresseDifferenteNom ;
	formFields['adresseDifferenteNVoie']                    = $ds079PE2.declarationGroup.identification.adressedifferenteGroup.adressedifferenteNVoie ;
	formFields['adresseDifferenteVoie']                    =  $ds079PE2.declarationGroup.identification.adressedifferenteGroup.adressedifferenteVoie + ' ' 
															+ ($ds079PE2.declarationGroup.identification.adressedifferenteGroup.adressedifferenteComplement ? $ds079PE2.declarationGroup.identification.adressedifferenteGroup.adressedifferenteComplement : '');

	formFields['adresseDifferenteCP']                  = $ds079PE2.declarationGroup.identification.adressedifferenteGroup.adressedifferenteCP;
	formFields['adresseDifferenteVille']               = $ds079PE2.declarationGroup.identification.adressedifferenteGroup.adressedifferenteVille;
}else {
	formFields['adresseDifferenteNom']                    = '';
	formFields['adresseDifferenteNVoie']                    = '';
	formFields['adresseDifferenteVoie']                    = '';
	formFields['adresseDifferenteCP']                  = '';
	formFields['adresseDifferenteVille']               = '';
}

formFields['representantLegal']                    = $ds079PE2.declarationGroup.identification.representantLegal;
formFields['representantLegal1']     = '';


formFields['signatureNom']                         = $ds079PE2.signatureGroup.signature.signatureNom +' '+ $ds079PE2.signatureGroup.signature.signaturePrenom;
formFields['signatureTelephone']                     = $ds079PE2.signatureGroup.signature.signatureTelephone;
formFields['signatureLe']                      = $ds079PE2.signatureGroup.signature.signatureFaitLe;
var date2Tmp2 = new Date(parseInt($ds079PE2.signatureGroup.signature.signatureFaitLe.getTimeInMillis()));
var date2 = pad(date2Tmp2.getDate().toString());
var month2 = date2Tmp2.getMonth() + 1;
date2 = date2.concat(pad(month2.toString()));
date2 = date2.concat(date2Tmp2.getFullYear().toString());
formFields['signatureLe'] = date2;





formFields['signature']                            = "Cette déclaration respecte les attendus de l’article 1367 du code civil." ;



 var cerfaDoc1 = nash.doc //
	.load('models/declaration-d-existence-agessa.pdf') //
	.apply (formFields);



/**********************
Pieces jointes
***********************/
function appendPj(fld) {
	fld.forEach(function (elm) {
        cerfaDoc1.append(elm);
    });
}	
	
appendPj($attachmentPreprocess.attachmentPreprocess.pjID); 




/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $ds079PE2.signatureGroup.signature.signatureFaitLe ,
		autoriteHabilitee :"Agessa – Service diffuseurs" ,
		demandeContexte : "Déclaration d’existence auprès de l’Agessa.",
		civiliteNomPrenom : nomPrenom
	});
	finalDoc.append(cerfaDoc1.save('cerfa.pdf'));	
	
	
/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Galerie-d-art_Agessa.pdf');	
	
	
/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Galerie d\'art - Déclaration d’existence auprès de l’Agessa',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : "Dossier de déclaration d’existence du diffuseur.",
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});