var formFields = {};

formFields['nomPro']             = $ds079PE3.declarationGroup.identification.nomPro;
formFields['activite']             = $ds079PE3.declarationGroup.identification.designationActivite;

formFields['telephone']          = $ds079PE3.declarationGroup.identification.siege.adresseTelephone;



formFields['objet']              = "Déclaration de détenteur de métaux précieux. ";


formFields['adresseSiege']                          = $ds079PE3.declarationGroup.identification.siege.adresseNVoie +' '
														+ $ds079PE3.declarationGroup.identification.siege.adresseVoie + ' ' 
															+ ($ds079PE3.declarationGroup.identification.siege.adresseVoieComplement ? $ds079PE3.declarationGroup.identification.siege.adresseVoieComplement : '') +', '
														+ ($ds079PE3.declarationGroup.identification.siege.adresseCP ? $ds079PE3.declarationGroup.identification.siege.adresseCP : '') +' '
														+ $ds079PE3.declarationGroup.identification.siege.adresseVille +', '
														+ $ds079PE3.declarationGroup.identification.siege.adressePays;
formFields['courriel']           					=	$ds079PE3.declarationGroup.identification.siege.email;




if($ds079PE3.declarationGroup.identification.adressedifferenteQuestion){
	formFields['adresseExercice']                    = $ds079PE3.declarationGroup.identification.adressedifferenteGroup.adressedifferenteNVoie +' '
																+  $ds079PE3.declarationGroup.identification.adressedifferenteGroup.adressedifferenteVoie + ' ' 
																+ ($ds079PE3.declarationGroup.identification.adressedifferenteGroup.adressedifferenteComplement ? $ds079PE3.declarationGroup.identification.adressedifferenteGroup.adressedifferenteComplement : '') +', '
																+ ($ds079PE3.declarationGroup.identification.adressedifferenteGroup.adressedifferenteCP ? $ds079PE3.declarationGroup.identification.adressedifferenteGroup.adressedifferenteCP :'') +' '
																+ $ds079PE3.declarationGroup.identification.adressedifferenteGroup.adressedifferenteVille +', '
																+ $ds079PE3.declarationGroup.identification.adressedifferenteGroup.adressedifferentePays;
}else {
	formFields['adresseExercice']                    = '';
}

var nomPrenom = $ds079PE3.signatureGroup.signature.signatureNom +' '+ $ds079PE3.signatureGroup.signature.signaturePrenom;
formFields['signatureNom']                         	= $ds079PE3.signatureGroup.signature.signatureNom +' '+ $ds079PE3.signatureGroup.signature.signaturePrenom;
formFields['declarationHonneur'] 					= $ds079PE3.signatureGroup.signature.declarationHonneur;
formFields['signatureFaitLe']                      	= $ds079PE3.signatureGroup.signature.signatureFaitLe;
formFields['signatureFaitA']                      	= $ds079PE3.signatureGroup.signature.signatureFaitA +', le ';
formFields['signature']          					= nomPrenom;


 var cerfaDoc1 = nash.doc //
	.load('models/courrier_libre_LE_DS.pdf') //
	.apply (formFields);



/**********************
Pieces jointes
***********************/
function appendPj(fld) {
	fld.forEach(function (elm) {
        cerfaDoc1.append(elm);
    });
}	
	
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjEnregistrement);
appendPj($attachmentPreprocess.attachmentPreprocess.pjKKBis);
appendPj($attachmentPreprocess.attachmentPreprocess.pjBail); 




/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $ds079PE3.signatureGroup.signature.signatureFaitLe ,
		autoriteHabilitee :"Direction Régionale des douanes et droits indirects – bureau de garantie" ,
		demandeContexte : "Déclaration de détenteur de métaux précieux.",
		civiliteNomPrenom : nomPrenom
	});
	finalDoc.append(cerfaDoc1.save('cerfa.pdf'));	
	
	
/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Galerie-d-art_Libre.pdf');	
	
	
/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Galerie d\'art - Déclaration de détenteur de métaux précieux',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : "Déclaration de détenteur de métaux précieux.",
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});