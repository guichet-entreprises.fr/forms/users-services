var cerfaFields = {};

var civNomPrenom = ($ds009PE1.personnePhysique.personnePhysique0.civilite != null ? $ds009PE1.personnePhysique.personnePhysique0.civilite + ' ' : '') + ($ds009PE1.personnePhysique.personnePhysique0.nom != null ? $ds009PE1.personnePhysique.personnePhysique0.nom + ' ' : '') + ($ds009PE1.personnePhysique.personnePhysique0.prenom != null ? $ds009PE1.personnePhysique.personnePhysique0.prenom + ' ' : '');
/* var region = $ds009PE1.signatureGroup.signature.regionExercice; */
var denomination = $ds009PE1.personneMorale.personneMorale0.denomination;
var civiliteNomPrenom = denomination != null ? denomination : civNomPrenom;

// Statut demande 
cerfaFields['demandeInitiale']  						= true;
cerfaFields['renouvellement']                			= false;
cerfaFields['modification']             				= false;

//Personne physique
cerfaFields['nom']  							= $ds009PE1.personnePhysique.personnePhysique0.nom;
cerfaFields['nomUsage']                			= $ds009PE1.personnePhysique.personnePhysique0.nomUsage;
cerfaFields['prenom']             				= $ds009PE1.personnePhysique.personnePhysique0.prenom;
cerfaFields['dateNaissance']  					= $ds009PE1.personnePhysique.personnePhysique0.dateNaissance;
cerfaFields['lieuNaissance']                	= $ds009PE1.personnePhysique.personnePhysique0.lieuNaissance;
cerfaFields['nationalite']             			= $ds009PE1.personnePhysique.personnePhysique0.nationalite;
cerfaFields['villePP']                			= $ds009PE1.personnePhysique.personnePhysique0.villePP;
cerfaFields['activitePP']             			= $ds009PE1.personnePhysique.personnePhysique0.activitePP;

//Personne morale
cerfaFields['denomination']  					= $ds009PE1.personneMorale.personneMorale0.denomination;
cerfaFields['sigle']  							= $ds009PE1.personneMorale.personneMorale0.sigle;
cerfaFields['activitePM']                		= $ds009PE1.personneMorale.personneMorale0.activitePM;
cerfaFields['siegeSocial']             			= ($ds009PE1.personneMorale.personneMorale0.numRue != null ? $ds009PE1.personneMorale.personneMorale0.numRue : ' ') + ($ds009PE1.personneMorale.personneMorale0.complement != null ? ', ' + $ds009PE1.personneMorale.personneMorale0.complement : ' ') + ' ' + ($ds009PE1.personneMorale.personneMorale0.codePostal != null ? $ds009PE1.personneMorale.personneMorale0.codePostal + ' ' : '') + ' ' + ($ds009PE1.personneMorale.personneMorale0.ville != null ? $ds009PE1.personneMorale.personneMorale0.ville + ' ' : '');


cerfaFields['nomRP']  							= $ds009PE1.personneMorale.personneMorale0.rp.nomRP;
cerfaFields['nomUsageRP']                		= $ds009PE1.personneMorale.personneMorale0.rp.nomUsageRP;
cerfaFields['prenomRP']             			= $ds009PE1.personneMorale.personneMorale0.rp.prenomRP;
cerfaFields['dateNaissanceRP']  				= $ds009PE1.personneMorale.personneMorale0.rp.dateNaissanceRP;
cerfaFields['lieuNaissanceRP']                	= $ds009PE1.personneMorale.personneMorale0.rp.lieuNaissanceRP;
cerfaFields['nationaliteRP']             		= $ds009PE1.personneMorale.personneMorale0.rp.nationaliteRP;
cerfaFields['numRueComplement']             	= ($ds009PE1.personneMorale.personneMorale0.rp.adresse.numRue != null ? $ds009PE1.personneMorale.personneMorale0.rp.adresse.numRue : ' ') + ($ds009PE1.personneMorale.personneMorale0.rp.adresse.complement != null ? ', ' + $ds009PE1.personneMorale.personneMorale0.rp.adresse.complement : ' ');
cerfaFields['cpVille']       					= ($ds009PE1.personneMorale.personneMorale0.rp.adresse.codePostal != null ? $ds009PE1.personneMorale.personneMorale0.rp.adresse.codePostal + ' ' : '') + ($ds009PE1.personneMorale.personneMorale0.rp.adresse.ville != null ? $ds009PE1.personneMorale.personneMorale0.rp.adresse.ville + ' ' : '');


//Signature
cerfaFields['dateSignature']                	= $ds009PE1.signature.signature.dateSignature;
cerfaFields['signature']           				= "Je déclare sur l’honneur l’exactitude des informations de la formalité et signe la présente déclaration.";
cerfaFields['lieuSignature']                    = $ds009PE1.signature.signature.lieuSignature;
cerfaFields['civNomPrenom']       				= ($ds009PE1.personnePhysique.personnePhysique0.civilite != null ? $ds009PE1.personnePhysique.personnePhysique0.civilite + ' ' : '') + ' ' + ($ds009PE1.personnePhysique.personnePhysique0.nom != null ? $ds009PE1.personnePhysique.personnePhysique0.nom + ' ' : '') + ' ' + ($ds009PE1.personnePhysique.personnePhysique0.prenom != null ? $ds009PE1.personnePhysique.personnePhysique0.prenom + ' ' : '');
cerfaFields['denominationSignature']  			= $ds009PE1.personneMorale.personneMorale0.denomination;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //0
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $ds009PE1.signatureGroup.signature.dateSignature,
		civiliteNomPrenom: civiliteNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		dateSignature : $ds009PE1.signature.signature.dateSignature,
		autoriteHabilitee :" ",
		demandeContexte : "Déclaration préalable d’une activité commerciale ou artisanale ambulante",
		civiliteNomPrenom : civiliteNomPrenom
	});
	
var cerfaDoc = nash.doc //
    .load('models/Cerfa 14022_02.pdf') //
    .apply(cerfaFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPhoto);

var finalDocItem = finalDoc.save('Commercant_ambulant.pdf');


return spec.create({
    id : 'review',
   label : 'Commerçant ambulant - Déclaration préalable d’une activité commerciale ou artisanale ambulante.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable d’une activité commerciale ou artisanale ambulante.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});