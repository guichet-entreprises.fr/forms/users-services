var cerfaFields = {};
//etatCivil

var civNomPrenom = $ds071PE1.etatCivil.identificationDeclarant.civilite + ' ' + $ds071PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $ds071PE1.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['civiliteNomPrenom']          	    = $ds071PE1.etatCivil.identificationDeclarant.civilite + ' ' + $ds071PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $ds071PE1.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			 	= $ds071PE1.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $ds071PE1.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                		= $ds071PE1.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             		= $ds071PE1.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 							= $ds071PE1.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($ds071PE1.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $ds071PE1.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       					= ($ds071PE1.adresse.adresseContact.codePostalAdresseDeclarant != null ? $ds071PE1.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $ds071PE1.adresse.adresseContact.villeAdresseDeclarant + ', ' + $ds071PE1.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      			= $ds071PE1.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          			= $ds071PE1.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          				= $ds071PE1.adresse.adresseContact.mailAdresseDeclarant;


//signature
cerfaFields['date']                				= $ds071PE1.signature.signature.dateSignature;
cerfaFields['signature']           				= $ds071PE1.signature.signature.signature;
cerfaFields['lieuSignature']                  	= $ds071PE1.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']		= $ds071PE1.etatCivil.identificationDeclarant.civilite + ' ' + $ds071PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $ds071PE1.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['libelleProfession']				= "Courtier en vins et spiritueux"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $ds071PE1.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $ds071PE1.signature.signature.dateSignature,
		autoriteHabilitee :"La chambre de commerce et d'industrie",
		demandeContexte : "Demande de carte professionnelle pour exercer en tant que courtier en vins et spiritueux",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier_libre_LE_V4.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPhoto);
appendPj($attachmentPreprocess.attachmentPreprocess.pjInscription);
appendPj($attachmentPreprocess.attachmentPreprocess.pjActivite);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Courtier_en_vins_et_spiritueux_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Courtier en vins et spiritueux - demande de carte professionnelle pour exercer en tant que courtier en vins et spiritueux',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'demande de carte professionnelle pour exercer en tant que courtier en vins et spiritueux',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
