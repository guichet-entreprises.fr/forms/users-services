var formFields = {};

civNomPrenom = $ds069PE1.identificationDeclarant.identificationPersonnePhysique.civilite + ' ' + $ds069PE1.identificationDeclarant.identificationPersonnePhysique.nomNaissance + ' ' + $ds069PE1.identificationDeclarant.identificationPersonnePhysique.prenoms;



//Identification Déclarant

formFields['personnePhysique']                        = Value('id').of($ds069PE1.identificationDeclarant.personnePhysiqueMorale.identite).eq("personnePhysique") ? true : false;
formFields['personneMorale']                          = Value('id').of($ds069PE1.identificationDeclarant.personnePhysiqueMorale.identite).eq("personneMorale") ? true : false;

//Identification personne physique
formFields['madame']							  = ($ds069PE1.identificationDeclarant.identificationPersonnePhysique.civilite=='Madame');
formFields['monsieur']                        	  = ($ds069PE1.identificationDeclarant.identificationPersonnePhysique.civilite=='Monsieur');
formFields['nomUsage']                            = $ds069PE1.identificationDeclarant.identificationPersonnePhysique.nomUsage;
formFields['nomNaissance']                        = $ds069PE1.identificationDeclarant.identificationPersonnePhysique.nomNaissance;
formFields['prenoms']                             = $ds069PE1.identificationDeclarant.identificationPersonnePhysique.prenoms;


//Identification personne morale
formFields['raisonSociale']                       = $ds069PE1.identificationDeclarant.identificationPersonneMorale.raisonSociale;
formFields['nomCommercial']                       = $ds069PE1.identificationDeclarant.identificationPersonneMorale.nomCommercial;
formFields['nSiret']                              = $ds069PE1.identificationDeclarant.identificationPersonneMorale.nSiret;
formFields['nSiren']                              = $ds069PE1.identificationDeclarant.identificationPersonneMorale.nSiren;


//Coordonnées du demandeur

formFields['numeroRue']              			  = $ds069PE1.coordonneesDemandeur.coordonnees.numeroRue;
formFields['adresse']              	              = $ds069PE1.coordonneesDemandeur.coordonnees.adressePersonne;
formFields['complementAdresse']                   = $ds069PE1.coordonneesDemandeur.coordonnees.complementAdresse;
formFields['codePostal']                          = $ds069PE1.coordonneesDemandeur.coordonnees.codePostalPersonne;
formFields['commune']                             = $ds069PE1.coordonneesDemandeur.coordonnees.communePersonne;
formFields['telephone']                           = $ds069PE1.coordonneesDemandeur.coordonnees.telephone;
formFields['telecopie']                           = $ds069PE1.coordonneesDemandeur.coordonnees.telecopie;
formFields['couriel']                             = $ds069PE1.coordonneesDemandeur.coordonnees.couriel;

//Description des surfaces
formFields['surfaceVenteGlobale']         		  = $ds069PE1.surfaces.descriptionSurfaces.surfaceVenteGlobale;
formFields['surfaceVenteProduits']   			  = $ds069PE1.surfaces.descriptionSurfaces.surfaceVenteProduits;
formFields['surfaceReserves'] 					  = $ds069PE1.surfaces.descriptionSurfaces.surfaceReserves;
formFields['surfacesStationnementClients']        = $ds069PE1.surfaces.descriptionSurfaces.surfacesStationnementClients;
formFields['surfaceFournisseursLivraison']       = $ds069PE1.surfaces.descriptionSurfaces.surfaceFournisseursLivraison;
formFields['surfacesEquippements']                = $ds069PE1.surfaces.descriptionSurfaces.surfacesEquippements;
formFields['surfacesLocauxAdministratifs']        = $ds069PE1.surfaces.descriptionSurfaces.surfacesLocauxAdministratifs;
formFields['contraintesTechniques']               = $ds069PE1.surfaces.descriptionSurfaces.contraintesTechniques;
formFields['formeJuridique']                      = $ds069PE1.surfaces.descriptionSurfaces.formeJuridique;


//Identification personne morale
formFields['plageHoraire0']                       = $ds069PE1.livraisonVehicules.types.plageHoraire0;
formFields['typeVehiculeLivraison0']              = $ds069PE1.livraisonVehicules.types.TypeVehiculeLivraison0;
formFields['typeVehiculeAcheteurs0']              = $ds069PE1.livraisonVehicules.types.TypeVehiculeAcheteurs0;
formFields['augmentationTraficPrevue0']           = $ds069PE1.livraisonVehicules.types.AugmentationTraficPrevue0;

formFields['plageHoraire1']                       = $ds069PE1.livraisonVehicules.types.PlageHoraire1;
formFields['typeVehiculeAcheteurs1']              = $ds069PE1.livraisonVehicules.types.TypeVehiculeAcheteurs1;
formFields['typeVehiculeLivraison1']              = $ds069PE1.livraisonVehicules.types.TypeVehiculeLivraison1;
formFields['augmentationTraficPrevue1']           = $ds069PE1.livraisonVehicules.types.AugmentationTraficPrevue1;

formFields['plageHoraire2']                       = $ds069PE1.livraisonVehicules.types.PlageHoraire2;
formFields['typeVehiculeAcheteurs2']              = $ds069PE1.livraisonVehicules.types.TypeVehiculeAcheteurs2;
formFields['typeVehiculeLivraison2']              = $ds069PE1.livraisonVehicules.types.TypeVehiculeLivraison2;
formFields['augmentationTraficPrevue2']           = $ds069PE1.livraisonVehicules.types.AugmentationTraficPrevue2;

formFields['plageHoraire3']                       = $ds069PE1.livraisonVehicules.types.PlageHoraire3;
formFields['typeVehiculeAcheteurs3']              = $ds069PE1.livraisonVehicules.types.TypeVehiculeAcheteurs3;
formFields['typeVehiculeLivraison3']              = $ds069PE1.livraisonVehicules.types.TypeVehiculeLivraison3;
formFields['augmentationTraficPrevue3']           = $ds069PE1.livraisonVehicules.types.AugmentationTraficPrevue3;

formFields['plageHoraire4']                       = $ds069PE1.livraisonVehicules.types.PlageHoraire4;
formFields['typeVehiculeLivraison4']              = $ds069PE1.livraisonVehicules.types.TypeVehiculeLivraison4;
formFields['typeVehiculeAcheteurs4']              = $ds069PE1.livraisonVehicules.types.TypeVehiculeAcheteurs4;
formFields['augmentationTraficPrevue4']           = $ds069PE1.livraisonVehicules.types.AugmentationTraficPrevue4;

formFields['plageHoraire5']                       = $ds069PE1.livraisonVehicules.types.PlageHoraire5;
formFields['typeVehiculeLivraison5']              = $ds069PE1.livraisonVehicules.types.TypeVehiculeLivraison5;
formFields['typeVehiculeAcheteurs5']              = $ds069PE1.livraisonVehicules.types.TypeVehiculeAcheteurs5;
formFields['augmentationTraficPrevue5']           = $ds069PE1.livraisonVehicules.types.AugmentationTraficPrevue5;

formFields['plageHoraire6']                       = $ds069PE1.livraisonVehicules.types.PlageHoraire6;
formFields['typeVehiculeLivraison6']              = $ds069PE1.livraisonVehicules.types.TypeVehiculeLivraison6;
formFields['typeVehiculeLivraison6']              = $ds069PE1.livraisonVehicules.types.TypeVehiculeLivraison6;
formFields['augmentationTraficPrevue6']           = $ds069PE1.livraisonVehicules.types.AugmentationTraficPrevue6;

formFields['plageHoraire7']                       = $ds069PE1.livraisonVehicules.types.PlageHoraire7;
formFields['typeVehiculeAcheteurs7']              = $ds069PE1.livraisonVehicules.types.TypeVehiculeAcheteurs7;
formFields['typeVehiculeLivraison7']              = $ds069PE1.livraisonVehicules.types.TypeVehiculeLivraison7;
formFields['augmentationTraficPrevue7']           = $ds069PE1.livraisonVehicules.types.AugmentationTraficPrevue7;

for (var i= 0; i < $ds069PE1.livraisonVehicules.types.size(); i++ ){
formFields['plageHoraire'+i]             		  = $ds069PE1.livraisonVehicules.types[i].plageHoraire != null ? $ds069PE1.livraisonVehicules.types[i].plageHoraire :'' ;
formFields['typeVehiculeAcheteurs'+i]             = $ds069PE1.livraisonVehicules.types[i].typeVehiculeAcheteurs != null ? $ds069PE1.livraisonVehicules.types[i].typeVehiculeAcheteurs :'' ;
formFields['typeVehiculeLivraison'+i]             = $ds069PE1.livraisonVehicules.types[i].typeVehiculeLivraison != null ? $ds069PE1.livraisonVehicules.types[i].typeVehiculeLivraison :'' ;
formFields['augmentationTraficPrevue'+i]          = $ds069PE1.livraisonVehicules.types[i].augmentationTraficPrevue != null ? $ds069PE1.livraisonVehicules.types[i].augmentationTraficPrevue :'' ;
	
}

//Modalités

formFields['modalitesTraitementsDechets']        		= $ds069PE1.traitementDechetsEaux.traitementDechetsEaux.modalitesTraitementsDechets;
formFields['consommationsEnergetiquesDirectes']         = '';
formFields['consommationsEnergetiquesIndirectes']       = $ds069PE1.consommationsEnergetiques.consommationsEnergetiquesIndirectes.consommationsEnergetiquesIndirectes;
formFields['contraintesTecgniquesEventuelles']          = $ds069PE1.contraintesTechniques.contraintesTechniques.contraintesTecgniquesEventuelles;
formFields['activitesExerceesLocal']        		    = $ds069PE1.indicationsLocal.indicationsLocal.activitesExerceesLocal;

formFields['nomPrenomsQualiteDemandeur']       			= $ds069PE1.signatureGroup.signature.nomPrenomsQualiteDemandeur;
formFields['dateSignature']           					= $ds069PE1.signatureGroup.signature.dateSignature;
formFields['civiliteNomPrenom']       					= "civNomPrenom";



var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		date: $ds069PE1.signatureGroup.signature.dateSignature,
		autoriteHabilitee :"Préfet",
		demandeContexte : "Demande d’autorisation d’installation dans le périmètre d’un marché d’intérêt national",
		civiliteNomPrenom : civNomPrenom,
	});
	
var cerfaDoc = nash.doc //
    .load('models/autorisation MIN.pdf')//
    .apply(formFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));

function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJ
 */
 
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);


var finalDocItem = finalDoc.save('Commerce_de_gros_interentreprises.pdf');

return spec.create({
    id : 'review',
   label : 'Commerce de gros - demande d\’autorisation d\’installation dans le périmètre d\’un marché d’intérêt national',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande d\’autorisation d\’installation dans le périmètre d\’un marché d’intérêt national.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});