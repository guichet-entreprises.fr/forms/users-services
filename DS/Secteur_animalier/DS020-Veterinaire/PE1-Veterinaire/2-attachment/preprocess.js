// attachment('attachmentType', 'outputFileFieldId', { label: $pageId.fieldId });


var nomPrenom = ($ds020PE1.veterinaire.inscriptionEtatCivil.etatCivilNomUsage ? $ds020PE1.veterinaire.inscriptionEtatCivil.etatCivilNomUsage : $ds020PE1.veterinaire.inscriptionEtatCivil.etatCivilNomNaissance)
				+ ' ' +
				$ds020PE1.veterinaire.inscriptionEtatCivil.etatCivilPrenom;

attachment('pjID', 'pjID', {label: nomPrenom, mandatory:"true"});	
attachment('pjDiplomes', 'pjDiplomes', { mandatory:"true"});	
attachment('pjCasier', 'pjCasier', { mandatory:"true"});	
attachment('pjJustifDomicile', 'pjJustifDomicile', { mandatory:"true"});		
attachment('pjPhoto', 'pjPhoto', { mandatory:"true"});	
attachment('pjAnnexe2', 'pjAnnexe2', { mandatory:"true"});

if($ds020PE1.veterinaire.veterinaireChoix.pjContratEtablis){
	attachment('pjContrat', 'pjContrat', { mandatory:"true"});
}	
//<!-- Pour les vétérinaires non titulaires du titre de Docteur Vétérinaire d’une Ecole Française	 -->
if(not $ds020PE1.veterinaire.scolarite.pjAnnexe1){
	attachment('pjAnnexe1', 'pjAnnexe1', { mandatory:"true"});
}
	
	
/* <!-- Pour les vétérinaires responsables (ou vétérinaires responsables intérimaires) et les vétérinaires
délégués (ou vétérinaires délégués intérimaires) au sein d’une entreprise comportant au moins un
établissement pharmaceutique  --> */
if(Value('id').of($ds020PE1.veterinaire.veterinaireChoix.questionDeclaration).eq('veterinaireResponsables')) {
	attachment('pjContratEtablissementPharma', 'pjContratEtablissementPharma', { mandatory:"true"});
	attachment('pjJustificationExperience', 'pjJustificationExperience', { mandatory:"true"});
	attachment('pjOrganeSocial', 'pjOrganeSocial', { mandatory:"true"});
	
}


/* <!-- Pour les vétérinaires adjoints au sein d’une entreprise ou d’un établissement pharmaceutique --> */
if(Value('id').of($ds020PE1.veterinaire.veterinaireChoix.questionDeclaration).eq('veterinaireAdjoint')) {
	attachment('pjPiecesUtiles', 'pjPiecesUtiles', { mandatory:"true"});
}
/* <!-- Pour les vétérinaires au sein d’un établissement fabriquant, important, distribuant des aliments
médicamenteux --> */
if(Value('id').of($ds020PE1.veterinaire.veterinaireChoix.questionDeclaration).eq('veterinaireMedicamenteux')) {
	attachment('pjConvention', 'pjConvention', { mandatory:"true"});
}



/* <!-- NON OBLIGATOIRE : ANNEXE 4&5 --> */
attachment('annexe45', 'annexe45', {mandatory:"false"});

