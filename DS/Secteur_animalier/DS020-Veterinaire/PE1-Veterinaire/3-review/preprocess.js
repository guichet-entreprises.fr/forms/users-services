var formFields = {};
function pad(s) { return (s < 10) ? '0' + s : s; }	

var nomPrenom = ($ds020PE1.veterinaire.inscriptionEtatCivil.etatCivilNomUsage ? $ds020PE1.veterinaire.inscriptionEtatCivil.etatCivilNomUsage : $ds020PE1.veterinaire.inscriptionEtatCivil.etatCivilNomNaissance)
				+ ' ' +
				$ds020PE1.veterinaire.inscriptionEtatCivil.etatCivilPrenom;

//Page 1
formFields['crovRegion']                           = $ds020PE1.veterinaire.veterinaireChoix.crov;

//Page 2
formFields['veterinaireResponsables']              = Value('id').of($ds020PE1.veterinaire.veterinaireChoix.questionDeclaration).eq('veterinaireResponsables')  ? true : false;
formFields['veterinaireAdjoint']                   = Value('id').of($ds020PE1.veterinaire.veterinaireChoix.questionDeclaration).eq('veterinaireAdjoint')       ? true : false;
formFields['veterinaireMedicamenteux']             = Value('id').of($ds020PE1.veterinaire.veterinaireChoix.questionDeclaration).eq('veterinaireMedicamenteux') ? true : false;

//Page4
//Etat Civil
formFields['etatCivilNomNaissance']                = $ds020PE1.veterinaire.inscriptionEtatCivil.etatCivilNomNaissance;
formFields['etatCivilNomUsage']                    = $ds020PE1.veterinaire.inscriptionEtatCivil.etatCivilNomUsage ? $ds020PE1.veterinaire.inscriptionEtatCivil.etatCivilNomUsage : $ds020PE1.veterinaire.inscriptionEtatCivil.etatCivilNomNaissance ;
formFields['etatCivilPrenom']                      = $ds020PE1.veterinaire.inscriptionEtatCivil.etatCivilPrenom;
formFields['etatCivilNationalite']                 = $ds020PE1.veterinaire.inscriptionEtatCivil.etatCivilNationalite;
formFields['etatCivilPays']                        = $ds020PE1.veterinaire.inscriptionEtatCivil.etatCivilPays;
formFields['etatCivilDateNaissance']               = $ds020PE1.veterinaire.inscriptionEtatCivil.etatCivilDateNaissance;
formFields['etatCivilLieuNaissance']               = $ds020PE1.veterinaire.inscriptionEtatCivil.etatCivilLieuNaissance;

//Adresse Inscription
formFields['adresseInscriptionDPA']                = '';

formFields['adresseInscriptionNVoie']              = $ds020PE1.veterinaire.inscriptionAdresse.adresseInscriptionNVoie;
formFields['adresseInscriptionVoie']               = $ds020PE1.veterinaire.inscriptionAdresse.adresseInscriptionVoie +' '+($ds020PE1.veterinaire.inscriptionAdresse.adresseInscriptionVoieComplement != null ?  $ds020PE1.veterinaire.inscriptionAdresse.adresseInscriptionVoieComplement : '')
formFields['adresseInscriptionCP']                 = $ds020PE1.veterinaire.inscriptionAdresse.adresseInscriptionCP;
formFields['adresseInscriptionVille']              = $ds020PE1.veterinaire.inscriptionAdresse.adresseInscriptionVille +' '+($ds020PE1.veterinaire.inscriptionAdresse.adresseInscriptionPays != null ?  $ds020PE1.veterinaire.inscriptionAdresse.adresseInscriptionPays : '') ;
formFields['adresseInscriptionTelephone']          = $ds020PE1.veterinaire.inscriptionAdresse.adresseInscriptionTelephone;
formFields['adresseInscriptionEmail']              = $ds020PE1.veterinaire.inscriptionAdresse.adresseInscriptionEmail;


//Adresse Personnelle
formFields['adressePersonnelleACbox']              = Value('id').of($ds020PE1.veterinaire.adresseCorrespondanceGroup.adresseCorrespondance).eq('adressePersonnelleACbox')  ? true : false;
formFields['adressePersonnelleAC']                 = '';
formFields['adressePersonnelleNVoie']              = $ds020PE1.veterinaire.adressePersonnelle.adressePersonnelleNVoie ;
formFields['adressePersonnelleVoie']               = $ds020PE1.veterinaire.adressePersonnelle.adressePersonnelleVoie +' '+($ds020PE1.veterinaire.adressePersonnelle.adressePersonnelleVoieComplement != null ?  $ds020PE1.veterinaire.adressePersonnelle.adressePersonnelleVoieComplement : '')
formFields['adressePersonnelleCP']                 = $ds020PE1.veterinaire.adressePersonnelle.adressePersonnelleCP;
formFields['adressePersonnelleVille']              = $ds020PE1.veterinaire.adressePersonnelle.adressePersonnelleVille +' '+($ds020PE1.veterinaire.adressePersonnelle.adressePersonnellePays != null ?  $ds020PE1.veterinaire.adressePersonnelle.adressePersonnellePays : '') ;
formFields['adressePersonnelleTelephone']          = $ds020PE1.veterinaire.adressePersonnelle.adressePersonnelleTelephone;
formFields['adressePersonnelleEmail']              = $ds020PE1.veterinaire.adressePersonnelle.adressePersonnelleEmail;

//Domicile Professionnel d'exercice 0
formFields['domicileProACbox0']              = Value('id').of($ds020PE1.veterinaire.adresseCorrespondanceGroup.adresseCorrespondance).eq('domicileProACbox0')  ? true : false;
formFields['domicileProAC0']                 = $ds020PE1.veterinaire.domicilePro0.domicileProAC ;
formFields['domicileProNVoie0']              = $ds020PE1.veterinaire.domicilePro0.domicileProNVoie ;
formFields['domicileProVoie0']               = $ds020PE1.veterinaire.domicilePro0.domicileProVoie +' '+($ds020PE1.veterinaire.domicilePro0.domicileProVoieComplement != null ?  $ds020PE1.veterinaire.domicilePro0.domicileProVoieComplement : '')
formFields['domicileProCP0']                 = $ds020PE1.veterinaire.domicilePro0.domicileProCP;
formFields['domicileProVille0']              = $ds020PE1.veterinaire.domicilePro0.domicileProVille +' '+($ds020PE1.veterinaire.domicilePro0.domicileProPays != null ?  $ds020PE1.veterinaire.domicilePro0.domicileProPays : '') ;
formFields['domicileProTelephone0']          = $ds020PE1.veterinaire.domicilePro0.domicileProTelephone;
formFields['domicileProEmail0']              = $ds020PE1.veterinaire.domicilePro0.domicileProEmail;

//Page5
//Scolarite
formFields['scolariteEtablissement']               = $ds020PE1.veterinaire.scolarite.scolariteEtablissement;
formFields['scolariteDateFin']                     = $ds020PE1.veterinaire.scolarite.scolariteDateFin;
formFields['scolariteDESV']                        = $ds020PE1.veterinaire.scolarite.autresDiplomes.scolariteDESV;
formFields['scolariteCEAV']                        = $ds020PE1.veterinaire.scolarite.autresDiplomes.scolariteCEAV;
formFields['scolariteCES']                         = $ds020PE1.veterinaire.scolarite.autresDiplomes.scolariteCES;
formFields['scolariteCollegeEuro']                 = $ds020PE1.veterinaire.scolarite.autresDiplomes.scolariteCollegeEuro;
formFields['scolariteDeDie']                       = $ds020PE1.veterinaire.scolarite.autresDiplomes.scolariteDeDie;
formFields['scolariteAutres']                      = $ds020PE1.veterinaire.scolarite.autresDiplomes.scolariteAutres;
formFields['scolariteDateDESV']                    = $ds020PE1.veterinaire.scolarite.autresDiplomes.scolariteDateDESV;
formFields['scolariteDateCEAV']                    = $ds020PE1.veterinaire.scolarite.autresDiplomes.scolariteDateCEAV;
formFields['scolariteDateCES']                     = $ds020PE1.veterinaire.scolarite.autresDiplomes.scolariteDateCES;
formFields['scolariteDateCollegeEuro']             = $ds020PE1.veterinaire.scolarite.autresDiplomes.scolariteDateCollegeEuro;
formFields['scolariteDateDeDie']                   = $ds020PE1.veterinaire.scolarite.autresDiplomes.scolariteDateDeDie;
formFields['scolariteDateAutres']                  = $ds020PE1.veterinaire.scolarite.autresDiplomes.scolariteDateAutres;


//Motif de la demande
formFields['motifTableauOrdreVeto']                = Value('id').of($ds020PE1.veterinaire.veterinaireChoix.motifDemande).eq('motifTableauOrdreVeto')  ? true : false;
formFields['motifTableauOrdreVetoActivite']        = $ds020PE1.veterinaire.veterinaireChoix.motifTableauOrdreVetoActivite ;
formFields['motifVolontaire']                      = Value('id').of($ds020PE1.veterinaire.veterinaireChoix.motifDemande).eq('motifVolontaire')  ? true : false;

//Statut envisagé
formFields['statutEnvisageLiberal']                = Value('id').of($ds020PE1.veterinaire.veterinaireChoix.questionStatut).eq('statutEnvisageLiberal')  ? true : false;
formFields['statutEnvisageSalarie']                = Value('id').of($ds020PE1.veterinaire.veterinaireChoix.questionStatut).eq('statutEnvisageSalarie')  ? true : false;
formFields['statutEnvisageIndetermine']            = Value('id').of($ds020PE1.veterinaire.veterinaireChoix.questionStatut).eq('statutEnvisageIndetermine')  ? true : false;

//Page 6 - Signature
formFields['signatureFaitA']                       = $ds020PE1.signatureGroup.signature.signatureFaitA ;
formFields['signatureFaitLe']                      = $ds020PE1.signatureGroup.signature.signatureFaitLe ;
formFields['signature']                            = "Cette déclaration respecte les attendus de l’article 1367 du code civil."


/************************************************************************************************************************************************************
 * Création du dossier Demande d’inscription au tableau de l’ordre 
*************************************************************************************************************************************************************/
var cerfaDoc1 = nash.doc //
	.load('models/veterinaire_sans-annexe.pdf') //
	.apply(formFields);


//ANNEXE 3 DEMANDE ENREGISTREMENT DIPLOME
if($ds020PE1.veterinaire.scolarite.autresDiplomes.questionDESV and not $ds020PE1.veterinaire.scolarite.autresDiplomes.questionEnregistrementDESV){
	formFields = {};
	formFields['annexe3Soussigne']                     = nomPrenom;
	formFields['annexe3Diplome']                       = $ds020PE1.veterinaire.scolarite.autresDiplomes.annexe3DiplomeDESV;
	formFields['annexe3Organisme']                     = $ds020PE1.veterinaire.scolarite.autresDiplomes.annexe3OrganismeDESV;
	formFields['annexe3ConseilRegional']               = $ds020PE1.veterinaire.veterinaireChoix.crov;
	formFields['annexe3signatureFaitLe']				= $ds020PE1.signatureGroup.signature.signatureFaitLe ;
	formFields['annexe3signatureFaitA']					= $ds020PE1.signatureGroup.signature.signatureFaitA ;
	formFields['annexe3signature']						= "Cette déclaration respecte les attendus de l’article 1367 du code civil."

/*****************************************************************************************************************************************************
 * Création du dossier Demande d’inscription au tableau de l’ordre Annexe 3
 *****************************************************************************************************************************************************/

 var cerfaDoc2 = nash.doc //
	.load('models/annexe3_veterinaire.pdf') //
	.apply (formFields);
	cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));
	
}

//ANNEXE 3 DEMANDE ENREGISTREMENT DIPLOME
if($ds020PE1.veterinaire.scolarite.autresDiplomes.questionCEAV and not $ds020PE1.veterinaire.scolarite.autresDiplomes.questionEnregistrementCEAV){
	formFields = {};
	formFields['annexe3Soussigne']                     = nomPrenom;
	formFields['annexe3Diplome']                       = $ds020PE1.veterinaire.scolarite.autresDiplomes.annexe3DiplomeCEAV;
	formFields['annexe3Organisme']                     = $ds020PE1.veterinaire.scolarite.autresDiplomes.annexe3OrganismeCEAV;
	formFields['annexe3ConseilRegional']               = $ds020PE1.veterinaire.veterinaireChoix.crov;
	formFields['annexe3signatureFaitLe']				= $ds020PE1.signatureGroup.signature.signatureFaitLe ;
	formFields['annexe3signatureFaitA']					= $ds020PE1.signatureGroup.signature.signatureFaitA ;
	formFields['annexe3signature']						= "Cette déclaration respecte les attendus de l’article 1367 du code civil."

	/*****************************************************************************************************************************************************
 * Création du dossier Demande d’inscription au tableau de l’ordre Annexe 3
 *****************************************************************************************************************************************************/

 var cerfaDoc2 = nash.doc //
	.load('models/annexe3_veterinaire.pdf') //
	.apply (formFields);
	cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));
	
}
//ANNEXE 3 DEMANDE ENREGISTREMENT DIPLOME
if($ds020PE1.veterinaire.scolarite.autresDiplomes.questionCES and not $ds020PE1.veterinaire.scolarite.autresDiplomes.questionEnregistrementCES){
		formFields = {};
	formFields['annexe3Soussigne']                     = nomPrenom;
	formFields['annexe3Diplome']                       = $ds020PE1.veterinaire.scolarite.autresDiplomes.annexe3DiplomeCES;
	formFields['annexe3Organisme']                     = $ds020PE1.veterinaire.scolarite.autresDiplomes.annexe3OrganismeCES;
	formFields['annexe3ConseilRegional']               = $ds020PE1.veterinaire.veterinaireChoix.crov;
	formFields['annexe3signatureFaitLe']				= $ds020PE1.signatureGroup.signature.signatureFaitLe ;
	formFields['annexe3signatureFaitA']					= $ds020PE1.signatureGroup.signature.signatureFaitA ;
	formFields['annexe3signature']						= "Cette déclaration respecte les attendus de l’article 1367 du code civil."

		/*****************************************************************************************************************************************************
 * Création du dossier Demande d’inscription au tableau de l’ordre Annexe 3
 *****************************************************************************************************************************************************/

 var cerfaDoc2 = nash.doc //
	.load('models/annexe3_veterinaire.pdf') //
	.apply (formFields);
	cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));
	
}
//ANNEXE 3 DEMANDE ENREGISTREMENT DIPLOME
if($ds020PE1.veterinaire.scolarite.autresDiplomes.questionCollegeEuro and not $ds020PE1.veterinaire.scolarite.autresDiplomes.questionEnregistrementCollegeEuro){
		formFields = {};
	formFields['annexe3Soussigne']                     = nomPrenom;
	formFields['annexe3Diplome']                       = $ds020PE1.veterinaire.scolarite.autresDiplomes.annexe3DiplomeCollegeEuro;
	formFields['annexe3Organisme']                     = $ds020PE1.veterinaire.scolarite.autresDiplomes.annexe3OrganismeCollegeEuro;
	formFields['annexe3ConseilRegional']               = $ds020PE1.veterinaire.veterinaireChoix.crov;
	formFields['annexe3signatureFaitLe']				= $ds020PE1.signatureGroup.signature.signatureFaitLe ;
	formFields['annexe3signatureFaitA']					= $ds020PE1.signatureGroup.signature.signatureFaitA ;
	formFields['annexe3signature']						= "Cette déclaration respecte les attendus de l’article 1367 du code civil."

		/*****************************************************************************************************************************************************
 * Création du dossier Demande d’inscription au tableau de l’ordre Annexe 3
 *****************************************************************************************************************************************************/

 var cerfaDoc2 = nash.doc //
	.load('models/annexe3_veterinaire.pdf') //
	.apply (formFields);
	cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));
	
}
//ANNEXE 3 DEMANDE ENREGISTREMENT DIPLOME
if($ds020PE1.veterinaire.scolarite.autresDiplomes.questionDeDie and not $ds020PE1.veterinaire.scolarite.autresDiplomes.questionEnregistrementDeDie){
		formFields = {};
	formFields['annexe3Soussigne']                     = nomPrenom;
	formFields['annexe3Diplome']                       = $ds020PE1.veterinaire.scolarite.autresDiplomes.annexe3DiplomeDeDie;
	formFields['annexe3Organisme']                     = $ds020PE1.veterinaire.scolarite.autresDiplomes.annexe3OrganismeDeDie;
	formFields['annexe3ConseilRegional']               = $ds020PE1.veterinaire.veterinaireChoix.crov;
	formFields['annexe3signatureFaitLe']				= $ds020PE1.signatureGroup.signature.signatureFaitLe ;
	formFields['annexe3signatureFaitA']					= $ds020PE1.signatureGroup.signature.signatureFaitA ;
	formFields['annexe3signature']						= "Cette déclaration respecte les attendus de l’article 1367 du code civil."

		/*****************************************************************************************************************************************************
 * Création du dossier Demande d’inscription au tableau de l’ordre Annexe 3
 *****************************************************************************************************************************************************/

 var cerfaDoc2 = nash.doc //
	.load('models/annexe3_veterinaire.pdf') //
	.apply (formFields);
	cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));
	
}
//ANNEXE 3 DEMANDE ENREGISTREMENT DIPLOME
if($ds020PE1.veterinaire.scolarite.autresDiplomes.questionAutres and not $ds020PE1.veterinaire.scolarite.autresDiplomes.questionEnregistrementAutres){
		formFields = {};
	formFields['annexe3Soussigne']                     = nomPrenom;
	formFields['annexe3Diplome']                       = $ds020PE1.veterinaire.scolarite.autresDiplomes.annexe3DiplomeAutres;
	formFields['annexe3Organisme']                     = $ds020PE1.veterinaire.scolarite.autresDiplomes.annexe3OrganismeAutres;
	formFields['annexe3ConseilRegional']               = $ds020PE1.veterinaire.veterinaireChoix.crov;
	formFields['annexe3signatureFaitLe']				= $ds020PE1.signatureGroup.signature.signatureFaitLe ;
	formFields['annexe3signatureFaitA']					= $ds020PE1.signatureGroup.signature.signatureFaitA ;
	formFields['annexe3signature']						= "Cette déclaration respecte les attendus de l’article 1367 du code civil."

		/*****************************************************************************************************************************************************
 * Création du dossier Demande d’inscription au tableau de l’ordre Annexe 3
 *****************************************************************************************************************************************************/

 var cerfaDoc2 = nash.doc //
	.load('models/annexe3_veterinaire.pdf') //
	.apply (formFields);
	cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));
	
}


//ANNEXE 6 - 1 
if($ds020PE1.veterinaire.domicilePro0.questionDPE){
		formFields = {};
	//Domicile Professionnel d'exercice 1
	formFields['annexe6domicileProACbox1']              = Value('id').of($ds020PE1.veterinaire.adresseCorrespondanceGroup.adresseCorrespondance).eq('domicileProACbox1')  ? true : false;
	formFields['annexe6domicileProAC1']                 = $ds020PE1.veterinaire.domicilePro1.domicileProAC ;
	formFields['annexe6domicileProNVoie1']              = $ds020PE1.veterinaire.domicilePro1.domicileProNVoie ;
	formFields['annexe6domicileProVoie1']               = $ds020PE1.veterinaire.domicilePro1.domicileProVoie +' '+($ds020PE1.veterinaire.domicilePro1.domicileProVoieComplement != null ?  $ds020PE1.veterinaire.domicilePro1.domicileProVoieComplement : '')
	formFields['annexe6domicileProCP1']                 = $ds020PE1.veterinaire.domicilePro1.domicileProCP;
	formFields['annexe6domicileProVille1']              = $ds020PE1.veterinaire.domicilePro1.domicileProVille +' '+($ds020PE1.veterinaire.domicilePro1.domicileProPays != null ?  $ds020PE1.veterinaire.domicilePro1.domicileProPays : '') ;
	formFields['annexe6domicileProTelephone1']          = $ds020PE1.veterinaire.domicilePro1.domicileProTelephone;
	formFields['annexe6domicileProEmail1']              = $ds020PE1.veterinaire.domicilePro1.domicileProEmail;

	if($ds020PE1.veterinaire.domicilePro1.questionDPE){	
		//Domicile Professionnel d'exercice 2
		formFields['annexe6domicileProACbox2']              = Value('id').of($ds020PE1.veterinaire.adresseCorrespondanceGroup.adresseCorrespondance).eq('domicileProACbox2')  ? true : false;
		formFields['annexe6domicileProAC2']                 = $ds020PE1.veterinaire.domicilePro2.domicileProAC ;
		formFields['annexe6domicileProNVoie2']              = $ds020PE1.veterinaire.domicilePro2.domicileProNVoie ;
		formFields['annexe6domicileProVoie2']               = $ds020PE1.veterinaire.domicilePro2.domicileProVoie +' '+($ds020PE1.veterinaire.domicilePro2.domicileProVoieComplement != null ?  $ds020PE1.veterinaire.domicilePro2.domicileProVoieComplement : '')
		formFields['annexe6domicileProCP2']                 = $ds020PE1.veterinaire.domicilePro2.domicileProCP;
		formFields['annexe6domicileProVille2']              = $ds020PE1.veterinaire.domicilePro2.domicileProVille +' '+($ds020PE1.veterinaire.domicilePro2.domicileProPays != null ?  $ds020PE1.veterinaire.domicilePro2.domicileProPays : '') ;
		formFields['annexe6domicileProTelephone2']          = $ds020PE1.veterinaire.domicilePro2.domicileProTelephone;
		formFields['annexe6domicileProEmail2']              = $ds020PE1.veterinaire.domicilePro2.domicileProEmail;	

		
	} else {
		formFields['annexe6domicileProACbox2']              = '';
		formFields['annexe6domicileProAC2']              	= '';
		formFields['annexe6domicileProNVoie2']              = '';
		formFields['annexe6domicileProVoie2']               = '';
		formFields['annexe6domicileProCP2']                 = '';
		formFields['annexe6domicileProVille2']              = '';
		formFields['annexe6domicileProTelephone2']          = '';
		formFields['annexe6domicileProEmail2']              = '';
	}
	
		/*****************************************************************************************************************************************************
 * Création du dossier Demande d’inscription au tableau de l’ordre Annexe 6
 *****************************************************************************************************************************************************/

 var cerfaDoc2 = nash.doc //
	.load('models/annexe6_veterinaire.pdf') //
	.apply (formFields);
	cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));
}


//ANNEXE 6 - 2 
if($ds020PE1.veterinaire.domicilePro2.questionDPE){
		formFields = {};
	//Domicile Professionnel d'exercice 3
	formFields['annexe6domicileProACbox1']              = Value('id').of($ds020PE1.veterinaire.adresseCorrespondanceGroup.adresseCorrespondance).eq('domicileProACbox3')  ? true : false;
	formFields['annexe6domicileProAC1']                 = $ds020PE1.veterinaire.domicilePro3.domicileProAC ;
	formFields['annexe6domicileProNVoie1']              = $ds020PE1.veterinaire.domicilePro3.domicileProNVoie ;
	formFields['annexe6domicileProVoie1']               = $ds020PE1.veterinaire.domicilePro3.domicileProVoie +' '+($ds020PE1.veterinaire.domicilePro3.domicileProVoieComplement != null ?  $ds020PE1.veterinaire.domicilePro3.domicileProVoieComplement : '')
	formFields['annexe6domicileProCP1']                 = $ds020PE1.veterinaire.domicilePro3.domicileProCP;
	formFields['annexe6domicileProVille1']              = $ds020PE1.veterinaire.domicilePro3.domicileProVille +' '+($ds020PE1.veterinaire.domicilePro3.domicileProPays != null ?  $ds020PE1.veterinaire.domicilePro3.domicileProPays : '') ;
	formFields['annexe6domicileProTelephone1']          = $ds020PE1.veterinaire.domicilePro3.domicileProTelephone;
	formFields['annexe6domicileProEmail1']              = $ds020PE1.veterinaire.domicilePro3.domicileProEmail;

	if($ds020PE1.veterinaire.domicilePro3.questionDPE){	
		//Domicile Professionnel d'exercice 4
		formFields['annexe6domicileProACbox2']              = Value('id').of($ds020PE1.veterinaire.adresseCorrespondanceGroup.adresseCorrespondance).eq('domicileProACbox4')  ? true : false;
		formFields['annexe6domicileProAC2']                 = $ds020PE1.veterinaire.domicilePro4.domicileProAC ;
		formFields['annexe6domicileProNVoie2']              = $ds020PE1.veterinaire.domicilePro4.domicileProNVoie ;
		formFields['annexe6domicileProVoie2']               = $ds020PE1.veterinaire.domicilePro4.domicileProVoie +' '+($ds020PE1.veterinaire.domicilePro4.domicileProVoieComplement != null ?  $ds020PE1.veterinaire.domicilePro4.domicileProVoieComplement : '')
		formFields['annexe6domicileProCP2']                 = $ds020PE1.veterinaire.domicilePro4.domicileProCP;
		formFields['annexe6domicileProVille2']              = $ds020PE1.veterinaire.domicilePro4.domicileProVille +' '+($ds020PE1.veterinaire.domicilePro4.domicileProPays != null ?  $ds020PE1.veterinaire.domicilePro4.domicileProPays : '') ;
		formFields['annexe6domicileProTelephone2']          = $ds020PE1.veterinaire.domicilePro4.domicileProTelephone;
		formFields['annexe6domicileProEmail2']              = $ds020PE1.veterinaire.domicilePro4.domicileProEmail;
	} else {
		//Domicile Professionnel d'exercice 4
		formFields['annexe6domicileProACbox2']              = '';
		formFields['annexe6domicileProAC2']       			= ''; 
		formFields['annexe6domicileProNVoie2']              = '';
		formFields['annexe6domicileProVoie2']               = '';
		formFields['annexe6domicileProCP2']                 = '';
		formFields['annexe6domicileProVille2']              = '';
		formFields['annexe6domicileProTelephone2']          = '';
		formFields['annexe6domicileProEmail2']              = '';
		
		
		
		
	}

	/*****************************************************************************************************************************************************
 * Création du dossier Demande d’inscription au tableau de l’ordre Annexe 6
 *****************************************************************************************************************************************************/

 var cerfaDoc2 = nash.doc //
	.load('models/annexe6_veterinaire.pdf') //
	.apply (formFields);
	cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));
}	


				

function appendPj(fld) {
	fld.forEach(function (elm) {
        cerfaDoc1.append(elm);
    });
}

 
/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID); 
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes); 
appendPj($attachmentPreprocess.attachmentPreprocess.pjCasier); 
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustifDomicile); 
appendPj($attachmentPreprocess.attachmentPreprocess.pjPhoto); 
appendPj($attachmentPreprocess.attachmentPreprocess.pjAnnexe2); 

if($ds020PE1.veterinaire.veterinaireChoix.pjContratEtablis){
	appendPj($attachmentPreprocess.attachmentPreprocess.pjContrat); 
}	
//<!-- Pour les vétérinaires non titulaires du titre de Docteur Vétérinaire d’une Ecole Française	 -->
if(not $ds020PE1.veterinaire.scolarite.pjAnnexe1){
	appendPj($attachmentPreprocess.attachmentPreprocess.pjAnnexe1); 
}
	
	
/* <!-- Pour les vétérinaires responsables (ou vétérinaires responsables intérimaires) et les vétérinaires
délégués (ou vétérinaires délégués intérimaires) au sein d’une entreprise comportant au moins un
établissement pharmaceutique  --> */
if(Value('id').of($ds020PE1.veterinaire.veterinaireChoix.questionDeclaration).eq('veterinaireResponsables')) {
		appendPj($attachmentPreprocess.attachmentPreprocess.pjContratEtablissementPharma); 
		appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificationExperience); 
		appendPj($attachmentPreprocess.attachmentPreprocess.pjOrganeSocial); 	
}


/* <!-- Pour les vétérinaires adjoints au sein d’une entreprise ou d’un établissement pharmaceutique --> */
if(Value('id').of($ds020PE1.veterinaire.veterinaireChoix.questionDeclaration).eq('veterinaireAdjoint')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles); 
}
/* <!-- Pour les vétérinaires au sein d’un établissement fabriquant, important, distribuant des aliments
médicamenteux --> */
if(Value('id').of($ds020PE1.veterinaire.veterinaireChoix.questionDeclaration).eq('veterinaireMedicamenteux')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjConvention);
}

/* <!-- NON OBLIGATOIRE : ANNEXE 4&5 --> */
appendPj($attachmentPreprocess.attachmentPreprocess.annexe45);


/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $ds020PE1.signatureGroup.signature.signatureFaitLe ,
		autoriteHabilitee :"Ordre national des vétérinaires" ,
		demandeContexte : "Demande d’inscription au tableau de l’ordre.",
		civiliteNomPrenom : nomPrenom
	});
	finalDoc.append(cerfaDoc1.save('cerfa.pdf'));

/*
 * Enregistrement du fichier (en mémoire)
 */
/* var finalDocItem = finalDoc.append(cerfaDoc1.save('veterinaire_DS.pdf')); */
var finalDocItem = finalDoc.save('veterinaire_DS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Vétérinaire - Demande d’inscription au tableau de l’ordre',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : "Dossier de demande d'inscription au tableau de l'ordre d'un vétérinaire.",
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});