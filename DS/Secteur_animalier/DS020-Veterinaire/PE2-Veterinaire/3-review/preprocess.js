var formFields = {};


formFields['crov']                             = $ds020PE2.veterinaire.veterinaireChoix.crov;

//forme juridique
formFields['formeJuridiqueSCP']                = Value('id').of($ds020PE2.veterinaire.veterinaireChoix.questionDeclaration).eq('formeJuridiqueSCP')	? true : false	;
formFields['formeJuridiqueSELARL']             = Value('id').of($ds020PE2.veterinaire.veterinaireChoix.questionDeclaration).eq('formeJuridiqueSELARL')? true : false	;
formFields['formeJuridiqueSELURL']             = Value('id').of($ds020PE2.veterinaire.veterinaireChoix.questionDeclaration).eq('formeJuridiqueSELURL')? true : false	;
formFields['formeJuridiqueSELAS']              = Value('id').of($ds020PE2.veterinaire.veterinaireChoix.questionDeclaration).eq('formeJuridiqueSELAS')	? true : false	;
formFields['formeJuridiqueSELASU']             = Value('id').of($ds020PE2.veterinaire.veterinaireChoix.questionDeclaration).eq('formeJuridiqueSELASU')? true : false	;
formFields['formeJuridiqueSARL']               = Value('id').of($ds020PE2.veterinaire.veterinaireChoix.questionDeclaration).eq('formeJuridiqueSARL')	? true : false	;
formFields['formeJuridiqueEURL']               = Value('id').of($ds020PE2.veterinaire.veterinaireChoix.questionDeclaration).eq('formeJuridiqueEURL')	? true : false	;
formFields['formeJuridiqueSAS']                = Value('id').of($ds020PE2.veterinaire.veterinaireChoix.questionDeclaration).eq('formeJuridiqueSAS')	? true : false	;
formFields['formeJuridiqueSASU']               = Value('id').of($ds020PE2.veterinaire.veterinaireChoix.questionDeclaration).eq('formeJuridiqueSASU')	? true : false	;
formFields['formeJuridiqueAutre']              = Value('id').of($ds020PE2.veterinaire.veterinaireChoix.questionDeclaration).eq('formeJuridiqueAutre')	? true : false	;
formFields['formeJuridiqueAutrePrecision']     = Value('id').of($ds020PE2.veterinaire.veterinaireChoix.questionDeclaration).eq('formeJuridiqueAutre') ? $ds020PE2.veterinaire.veterinaireChoix.formeJuridiqueAutrePrecision : '';

formFields['denomitationSociale']              = $ds020PE2.veterinaire.veterinaireChoix.denomitationSociale;

formFields['nomActivite']                      = $ds020PE2.veterinaire.veterinaireChoix.nomActivite;

//Adresse siege social
formFields['AdresseSiegeSociete']              = $ds020PE2.veterinaire.veterinaireChoix.adresseSiegeSocial.adresseSiegeSociete	;
formFields['AdresseSiegeAdresse']              = $ds020PE2.veterinaire.veterinaireChoix.adresseSiegeSocial.adresseSiegeAdresseNVoie +' '
													+ $ds020PE2.veterinaire.veterinaireChoix.adresseSiegeSocial.adresseSiegeAdresseVoie +' '
													+ ($ds020PE2.veterinaire.veterinaireChoix.adresseSiegeSocial.adresseSiegeAdresseComplement ? $ds020PE2.veterinaire.veterinaireChoix.adresseSiegeSocial.adresseSiegeAdresseComplement :'');
formFields['AdresseSiegeCP']                   = $ds020PE2.veterinaire.veterinaireChoix.adresseSiegeSocial.adresseSiegeCP		;
formFields['AdresseSiegeVille']                = $ds020PE2.veterinaire.veterinaireChoix.adresseSiegeSocial.adresseSiegeVille +' '
													+ ($ds020PE2.veterinaire.veterinaireChoix.adresseSiegeSocial.adresseSiegePays ? $ds020PE2.veterinaire.veterinaireChoix.adresseSiegeSocial.adresseSiegePays :'') ;
formFields['AdresseSiegeTelephone']            = $ds020PE2.veterinaire.veterinaireChoix.adresseSiegeSocial.adresseSiegeTelephone;
formFields['AdresseSiegeEmail']                = $ds020PE2.veterinaire.veterinaireChoix.adresseSiegeSocial.adresseSiegeEmail	;
formFields['AdresseSiegeRCS']                  = $ds020PE2.veterinaire.veterinaireChoix.adresseSiegeSocial.adresseSiegeRCS		;

//Adresse Correspondance
formFields['adresseCorrespondanceSociete']     = $ds020PE2.veterinaire.veterinaireChoix.adresseCorrespondance.adresseCorrespondanceSociete				;
formFields['adresseCorrespondanceNVoie']       = $ds020PE2.veterinaire.veterinaireChoix.adresseCorrespondance.adresseCorrespondanceNVoie				;
formFields['adresseCorrespondanceVoie']        = $ds020PE2.veterinaire.veterinaireChoix.adresseCorrespondanceQuestion ? (
													$ds020PE2.veterinaire.veterinaireChoix.adresseCorrespondance.adresseCorrespondanceVoie	+' '
													+ ($ds020PE2.veterinaire.veterinaireChoix.adresseCorrespondance.adresseCorrespondanceComplement ? $ds020PE2.veterinaire.veterinaireChoix.adresseCorrespondance.adresseCorrespondanceComplement : '')
												) : '';
formFields['adresseCorrespondanceCP']          = $ds020PE2.veterinaire.veterinaireChoix.adresseCorrespondance.adresseCorrespondanceCP					;
formFields['adresseCorrespondanceVille']       = $ds020PE2.veterinaire.veterinaireChoix.adresseCorrespondanceQuestion ? (
													$ds020PE2.veterinaire.veterinaireChoix.adresseCorrespondance.adresseCorrespondanceVille + ' '
													+ ($ds020PE2.veterinaire.veterinaireChoix.adresseCorrespondance.adresseCorrespondancePays ? $ds020PE2.veterinaire.veterinaireChoix.adresseCorrespondance.adresseCorrespondancePays :'') 
												) : '';
//Adresse Juridique
formFields['conseilJuridiqueDe']               = $ds020PE2.veterinaire.veterinaireChoix.crov									;
formFields['conseilJuridiqueNom']              = $ds020PE2.veterinaire.veterinaireChoix.adresseConseilJuridiqueGroup.conseilJuridiqueNom						;
formFields['conseilJuridiqueSociete']          = $ds020PE2.veterinaire.veterinaireChoix.adresseConseilJuridiqueGroup.conseilJuridiqueSociete					;
formFields['conseilJuridiqueAdresse']          = $ds020PE2.veterinaire.veterinaireChoix.adresseConseilJuridiqueGroup.conseilJuridiqueAdresse +' '
													+ ($ds020PE2.veterinaire.veterinaireChoix.adresseConseilJuridiqueGroup.adresseSiegeAdresseComplement ? $ds020PE2.veterinaire.veterinaireChoix.adresseConseilJuridiqueGroup.adresseSiegeAdresseComplement :'')  ;
formFields['conseilJuridiqueCP']               = $ds020PE2.veterinaire.veterinaireChoix.adresseConseilJuridiqueGroup.conseilJuridiqueCP						;
formFields['conseilJuridiqueVille']            = $ds020PE2.veterinaire.veterinaireChoix.adresseConseilJuridiqueGroup.conseilJuridiqueVille + ' '
													+ ($ds020PE2.veterinaire.veterinaireChoix.adresseConseilJuridiqueGroup.conseilJuridiquePays ? $ds020PE2.veterinaire.veterinaireChoix.adresseConseilJuridiqueGroup.conseilJuridiquePays : '') ;
formFields['conseilJuridiqueTelephone']        = $ds020PE2.veterinaire.veterinaireChoix.adresseConseilJuridiqueGroup.conseilJuridiqueTelephone				;
formFields['conseilJuridiqueEmail']            = $ds020PE2.veterinaire.veterinaireChoix.adresseConseilJuridiqueGroup.conseilJuridiqueEmail					;

//Pré-remplissage à vide pour éviter les champs non rempli dans le PDF final
formFields['representantLegalNom0']        =  '';   formFields['representantLegalNom1']         =  '';   formFields['representantLegalNom2']       =  '';    formFields['representantLegalNom3']       =  '';   formFields['representantLegalNom4']       =  '';  formFields['representantLegalNom5']       =  '';
formFields['representantLegalPrenom0']     =  '';   formFields['representantLegalPrenom1']      =  '';   formFields['representantLegalPrenom2']    =  '';    formFields['representantLegalPrenom3']    =  '';   formFields['representantLegalPrenom4']    =  '';  formFields['representantLegalPrenom5']    =  '';
formFields['representantLegalFonction0']   =  '';   formFields['representantLegalFonction1']    =  '';   formFields['representantLegalFonction2']  =  '';    formFields['representantLegalFonction3']  =  '';   formFields['representantLegalFonction4']  =  '';  formFields['representantLegalFonction5']  =  '';
formFields['representantLegalNOrdre0']     =  '';   formFields['representantLegalNOrdre1']      =  '';   formFields['representantLegalNOrdre2']    =  '';    formFields['representantLegalNOrdre3']    =  '';   formFields['representantLegalNOrdre4']    =  '';  formFields['representantLegalNOrdre5']    =  '';
//FIN Pré-remplissage 


//Representants légaux
for (var i= 0; i <  $ds020PE2.representantLegalGroup.representantLegal.size(); i++ ){
	formFields['representantLegalNom'+i]             = $ds020PE2.representantLegalGroup.representantLegal[i].representantLegalNom			!= null ?      $ds020PE2.representantLegalGroup.representantLegal[i].representantLegalNom		:'' ;
	formFields['representantLegalPrenom'+i]          = $ds020PE2.representantLegalGroup.representantLegal[i].representantLegalPrenom        != null ?      $ds020PE2.representantLegalGroup.representantLegal[i].representantLegalPrenom    :'' ;
	formFields['representantLegalFonction'+i]        = $ds020PE2.representantLegalGroup.representantLegal[i].representantLegalFonction      != null ?      $ds020PE2.representantLegalGroup.representantLegal[i].representantLegalFonction  :'' ;
	formFields['representantLegalNOrdre'+i]          = $ds020PE2.representantLegalGroup.representantLegal[i].representantLegalNOrdre        != null ?      $ds020PE2.representantLegalGroup.representantLegal[i].representantLegalNOrdre    :'' ;
}



//Capital Social
formFields['capitalSociete']                   = $ds020PE2.capitalSocieteGroup.capitalSociete.capitalSociete;
formFields['capitalSocieteNbPart']             = $ds020PE2.capitalSocieteGroup.capitalSociete.capitalSocieteNbPart;
formFields['capitalSocieteValeurParts']        = $ds020PE2.capitalSocieteGroup.capitalSociete.capitalSocieteValeurParts;
formFields['capitalSocieteRepartition']        = $ds020PE2.capitalSocieteGroup.capitalSociete.capitalSocieteRepartition;



//Pré-remplissage à vide pour éviter les champs non rempli dans le PDF final
formFields['nomPrenomVeto0']   	= '';  				formFields['nomPrenomVeto1']   = ''; 				formFields['nomPrenomVeto2']   	= ''; 				formFields['nomPrenomVeto3']   	= '';  				formFields['nomPrenomVeto4']   = ''; 				formFields['nomPrenomVeto5']  	= ''; 				formFields['nomPrenomVeto6']  	= '';
formFields['numeroInscriptionOrdinal0']    	= ''; 	formFields['numeroInscriptionOrdinal1']    	= ''; 	formFields['numeroInscriptionOrdinal2']    	= ''; 	formFields['numeroInscriptionOrdinal3']    	= ''; 	formFields['numeroInscriptionOrdinal4']    	= '';	formFields['numeroInscriptionOrdinal5']    	= '';	formFields['numeroInscriptionOrdinal6']    	= '';
formFields['qualite0']        				= '';	formFields['qualite1']        				= '';	formFields['qualite2']        				= '';	formFields['qualite3']        				= '';	formFields['qualite4']        				= '';	formFields['qualite5']        				= '';	formFields['qualite6']        				= '';
formFields['nombrePartsDetenues0']         	= '';	formFields['nombrePartsDetenues1']         	= '';	formFields['nombrePartsDetenues2']         	= '';	formFields['nombrePartsDetenues3']         	= '';	formFields['nombrePartsDetenues4']         	= '';	formFields['nombrePartsDetenues5']         	= '';	formFields['nombrePartsDetenues6']         	= '';
formFields['pourcentCapitalDetenu0']       	= '';	formFields['pourcentCapitalDetenu1']       	= '';	formFields['pourcentCapitalDetenu2']       	= '';	formFields['pourcentCapitalDetenu3']       	= '';	formFields['pourcentCapitalDetenu4']       	= '';	formFields['pourcentCapitalDetenu5']       	= '';   formFields['pourcentCapitalDetenu6']       	= '';
formFields['pourcentDroitsVote0']          	= '';	formFields['pourcentDroitsVote1']          	= '';	formFields['pourcentDroitsVote2']          	= '';	formFields['pourcentDroitsVote3']          	= '';	formFields['pourcentDroitsVote4']          	= '';	formFields['pourcentDroitsVote5']          	= '';   formFields['pourcentDroitsVote6']          	= '';


formFields['societeVeterinaireDenomination0']               ='';	formFields['societeVeterinaireDenomination1']               ='';	formFields['societeVeterinaireDenomination2']               ='';	
formFields['societeVeterinaireNumeroInscriptionOrdinal0']   ='';	formFields['societeVeterinaireNumeroInscriptionOrdinal1']   ='';	formFields['societeVeterinaireNumeroInscriptionOrdinal2']   ='';	
formFields['societeVeterinaireObjetSocial0']        		='';	formFields['societeVeterinaireObjetSocial1']        		='';	formFields['societeVeterinaireObjetSocial2']        		='';	
formFields['societeVeterinaireNombrePartsDetenues0']        ='';	formFields['societeVeterinaireNombrePartsDetenues1']        ='';	formFields['societeVeterinaireNombrePartsDetenues2']        ='';
formFields['societeVeterinairePourcentCapitalDetenu0']      ='';	formFields['societeVeterinairePourcentCapitalDetenu1']      ='';	formFields['societeVeterinairePourcentCapitalDetenu2']      ='';
formFields['soceieteVeterinairePourcentDroitsVote0']        ='';	formFields['soceieteVeterinairePourcentDroitsVote1']        ='';	formFields['soceieteVeterinairePourcentDroitsVote2']        ='';

formFields['associeNomPrenomDenomination0']  = '';	formFields['associeNomPrenomDenomination1']  = '';	formFields['associeNomPrenomDenomination2']  = '';	formFields['associeNomPrenomDenomination3']  = '';
formFields['associeActivitePro0']    		= '';	formFields['associeActivitePro1']    		= '';	formFields['associeActivitePro2']    		= '';	formFields['associeActivitePro3']    		= '';
formFields['associeObjetSocial0']        	= '';	formFields['associeObjetSocial1']        	= '';	formFields['associeObjetSocial2']        	= '';	formFields['associeObjetSocial3']        	= '';
formFields['associeNombrePartsDetenues0']    = '';	formFields['associeNombrePartsDetenues1']    = '';	formFields['associeNombrePartsDetenues2']    = '';	formFields['associeNombrePartsDetenues3']    = '';
formFields['associePourcentCapitalDetenu0']  = '';	formFields['associePourcentCapitalDetenu1']  = '';	formFields['associePourcentCapitalDetenu2']  = '';	formFields['associePourcentCapitalDetenu3']  = '';
formFields['associePourcentDroitsVote0']     = '';	formFields['associePourcentDroitsVote1']     = '';	formFields['associePourcentDroitsVote2']     = '';	formFields['associePourcentDroitsVote3']     = '';
//Fin pré-remplissage



if($ds020PE2.capitalSocieteGroup.capitalSociete.capitalPhysiqueChoix){
//Personne Physique Capital Social
for (var i= 0; i <  $ds020PE2.capitalSocieteGroup.capitalSociete.capitalPhysique.size(); i++ ){
	formFields['nomPrenomVeto'+i]             	= $ds020PE2.capitalSocieteGroup.capitalSociete.capitalPhysique[i].nomPrenomVeto				!= null ?      		$ds020PE2.capitalSocieteGroup.capitalSociete.capitalPhysique[i].nomPrenomVeto			:'' ;
	formFields['numeroInscriptionOrdinal'+i]    = $ds020PE2.capitalSocieteGroup.capitalSociete.capitalPhysique[i].numeroInscriptionOrdinal   	!= null ?      	$ds020PE2.capitalSocieteGroup.capitalSociete.capitalPhysique[i].numeroInscriptionOrdinal   :'' ;
	formFields['qualite'+i]        				= $ds020PE2.capitalSocieteGroup.capitalSociete.capitalPhysique[i].qualite      				!= null ?      		$ds020PE2.capitalSocieteGroup.capitalSociete.capitalPhysique[i].qualite      			  :'' ;
	formFields['nombrePartsDetenues'+i]         = $ds020PE2.capitalSocieteGroup.capitalSociete.capitalPhysique[i].nombrePartsDetenues       	!= null ?      	$ds020PE2.capitalSocieteGroup.capitalSociete.capitalPhysique[i].nombrePartsDetenues        :'' ;
	formFields['pourcentCapitalDetenu'+i]       = $ds020PE2.capitalSocieteGroup.capitalSociete.capitalPhysique[i].pourcentCapitalDetenu     	!= null ?      	$ds020PE2.capitalSocieteGroup.capitalSociete.capitalPhysique[i].pourcentCapitalDetenu      :'' ;
	formFields['pourcentDroitsVote'+i]          = $ds020PE2.capitalSocieteGroup.capitalSociete.capitalPhysique[i].pourcentDroitsVote       	!= null ?      	$ds020PE2.capitalSocieteGroup.capitalSociete.capitalPhysique[i].pourcentDroitsVote         :'' ;
	}
}
	
	
if($ds020PE2.capitalSocieteGroup.capitalSociete.capitalMoraleChoix){
//Personne Morale Capital Social
for (var i= 0; i <  $ds020PE2.capitalSocieteGroup.capitalSociete.capitalMorale.size(); i++ ){
	formFields['societeVeterinaireDenomination'+i]             	= $ds020PE2.capitalSocieteGroup.capitalSociete.capitalMorale[i].societeVeterinaireDenomination					!= null ?     $ds020PE2.capitalSocieteGroup.capitalSociete.capitalMorale[i].societeVeterinaireDenomination				:'' ;
	formFields['societeVeterinaireNumeroInscriptionOrdinal'+i]    = $ds020PE2.capitalSocieteGroup.capitalSociete.capitalMorale[i].societeVeterinaireNumeroInscriptionOrdinal   	!= null ?     $ds020PE2.capitalSocieteGroup.capitalSociete.capitalMorale[i].societeVeterinaireNumeroInscriptionOrdinal    :'' ;
	formFields['societeVeterinaireObjetSocial'+i]        				= $ds020PE2.capitalSocieteGroup.capitalSociete.capitalMorale[i].societeVeterinaireObjetSocial      			!= null ? $ds020PE2.capitalSocieteGroup.capitalSociete.capitalMorale[i].societeVeterinaireObjetSocial      		  :'' ;
	formFields['societeVeterinaireNombrePartsDetenues'+i]         = $ds020PE2.capitalSocieteGroup.capitalSociete.capitalMorale[i].societeVeterinaireNombrePartsDetenues       	!= null ?     $ds020PE2.capitalSocieteGroup.capitalSociete.capitalMorale[i].societeVeterinaireNombrePartsDetenues         :'' ;
	formFields['societeVeterinairePourcentCapitalDetenu'+i]       = $ds020PE2.capitalSocieteGroup.capitalSociete.capitalMorale[i].societeVeterinairePourcentCapitalDetenu     	!= null ?     $ds020PE2.capitalSocieteGroup.capitalSociete.capitalMorale[i].societeVeterinairePourcentCapitalDetenu       :'' ;
	formFields['soceieteVeterinairePourcentDroitsVote'+i]          = $ds020PE2.capitalSocieteGroup.capitalSociete.capitalMorale[i].soceieteVeterinairePourcentDroitsVote       	!= null ?     $ds020PE2.capitalSocieteGroup.capitalSociete.capitalMorale[i].soceieteVeterinairePourcentDroitsVote         :'' ;
	}
}

if($ds020PE2.capitalSocieteGroup.capitalSociete.capitalAssocieChoix){
//Personne Associée Capital Social
for (var i= 0; i <  $ds020PE2.capitalSocieteGroup.capitalSociete.capitalAssocie.size(); i++ ){
	formFields['associeNomPrenomDenomination'+i]             	= $ds020PE2.capitalSocieteGroup.capitalSociete.capitalAssocie[i].associeNomPrenomDenomination		!= null ?    $ds020PE2.capitalSocieteGroup.capitalSociete.capitalAssocie[i].associeNomPrenomDenomination	:'' ;
	formFields['associeActivitePro'+i]    						= $ds020PE2.capitalSocieteGroup.capitalSociete.capitalAssocie[i].associeActivitePro   				!= null ?    $ds020PE2.capitalSocieteGroup.capitalSociete.capitalAssocie[i].associeActivitePro   			   :'' ;
	formFields['associeObjetSocial'+i]        					= $ds020PE2.capitalSocieteGroup.capitalSociete.capitalAssocie[i].associeObjetSocial      			!= null ?    $ds020PE2.capitalSocieteGroup.capitalSociete.capitalAssocie[i].associeObjetSocial      		  :'' ;
	formFields['associeNombrePartsDetenues'+i]         			= $ds020PE2.capitalSocieteGroup.capitalSociete.capitalAssocie[i].associeNombrePartsDetenues       	!= null ?    $ds020PE2.capitalSocieteGroup.capitalSociete.capitalAssocie[i].associeNombrePartsDetenues       :'' ;
	formFields['associePourcentCapitalDetenu'+i]       			= $ds020PE2.capitalSocieteGroup.capitalSociete.capitalAssocie[i].associePourcentCapitalDetenu     	!= null ?    $ds020PE2.capitalSocieteGroup.capitalSociete.capitalAssocie[i].associePourcentCapitalDetenu     :'' ;
	formFields['associePourcentDroitsVote'+i]          			= $ds020PE2.capitalSocieteGroup.capitalSociete.capitalAssocie[i].associePourcentDroitsVote       	!= null ?    $ds020PE2.capitalSocieteGroup.capitalSociete.capitalAssocie[i].associePourcentDroitsVote        :'' ;
	}
}

//Signature
var nomPrenom = $ds020PE2.signatureGroup.signature.signatureNom + ' '+ $ds020PE2.signatureGroup.signature.signaturePrenom + '  Qualité : ' + $ds020PE2.signatureGroup.signature.signatureQualite;
formFields['signatureNom']                  = $ds020PE2.signatureGroup.signature.signatureNom + ' '+ $ds020PE2.signatureGroup.signature.signaturePrenom + '  Qualité : ' + $ds020PE2.signatureGroup.signature.signatureQualite;
formFields['signatureFaitLe']                  = $ds020PE2.signatureGroup.signature.signatureFaitLe;
formFields['signatureFaitA']                   = $ds020PE2.signatureGroup.signature.signatureFaitA;
formFields['signature']                        = "Cette déclaration respecte les attendus de l’article 1367 du code civil." ;



/************************************************************************************************************************************************************
 * Création du dossier Demande d’inscription au tableau de l’ordre 
*************************************************************************************************************************************************************/
var cerfaDoc1 = nash.doc //
	.load('models/DOSSIER-INSCRIP-SOCIETES.pdf') //
	.apply(formFields);
	
	

//Annexe 1
for (var i= 0; i <  $ds020PE2.annexe1Group.annexe1.size(); i++ ){
	formFields = {};
	formFields['annexe1DPENumero']                			 = $ds020PE2.annexe1Group.annexe1[i].annexe1DPENumero					!= null ?  $ds020PE2.annexe1Group.annexe1[i].annexe1DPENumero	 : ''					;
	formFields['annexe1DPENom']                   			 = $ds020PE2.annexe1Group.annexe1[i].annexe1DPENom						!= null ?  $ds020PE2.annexe1Group.annexe1[i].annexe1DPENom		: ''						;
	formFields['annexe1CabinetVeterinaire']       			 = Value('id').of($ds020PE2.annexe1Group.annexe1[i].categoriesChoix).eq('annexe1CabinetVeterinaire')		 ? true : false ;
	formFields['annexe1CliniqueVeterinaire']      			 = Value('id').of($ds020PE2.annexe1Group.annexe1[i].categoriesChoix).eq('annexe1CliniqueVeterinaire')		 ? true : false ;
	formFields['annexe1CentreHospiVeterinaire']   			 = Value('id').of($ds020PE2.annexe1Group.annexe1[i].categoriesChoix).eq('annexe1CentreHospiVeterinaire')	 ? true : false ;
	formFields['annexe1CabinetVeterinaireMedico'] 			 = Value('id').of($ds020PE2.annexe1Group.annexe1[i].categoriesChoix).eq('annexe1CabinetVeterinaireMedico')	 ? true : false ;
	formFields['annexe1CentreVeterinaireSpe']     			 = Value('id').of($ds020PE2.annexe1Group.annexe1[i].categoriesChoix).eq('annexe1CentreVeterinaireSpe')		 ? true : false ;
	formFields['annexe1Autre']                    			 = Value('id').of($ds020PE2.annexe1Group.annexe1[i].categoriesChoix).eq('annexe1Autre')						 ? true : false ;
	formFields['annexe1AutrePrecision']           			 = $ds020PE2.annexe1Group.annexe1[i].annexe1AutrePrecision				!= null ?  $ds020PE2.annexe1Group.annexe1[i].annexe1AutrePrecision			: ''			;
	formFields['annexe1Adresse']                  			 = $ds020PE2.annexe1Group.annexe1[i].annexe1Adresse						!= null ?  $ds020PE2.annexe1Group.annexe1[i].annexe1Adresse					: ''		;
	formFields['annexe1CP']                       			 = $ds020PE2.annexe1Group.annexe1[i].annexe1CP							!= null ?  $ds020PE2.annexe1Group.annexe1[i].annexe1CP						: ''			;
	formFields['annexe1Ville']                    			 = $ds020PE2.annexe1Group.annexe1[i].annexe1Ville						!= null ?  $ds020PE2.annexe1Group.annexe1[i].annexe1Ville					: ''			;
	formFields['annexe1Telephone']                			 = $ds020PE2.annexe1Group.annexe1[i].annexe1Telephone					!= null ?  $ds020PE2.annexe1Group.annexe1[i].annexe1Telephone				: ''			;
	formFields['annexe1NomPrenomVeto']            			 = $ds020PE2.annexe1Group.annexe1[i].annexe1NomPrenomVeto				!= null ?  $ds020PE2.annexe1Group.annexe1[i].annexe1NomPrenomVeto			: ''			;
	formFields['annexe1Interlocuteur']            			 = $ds020PE2.annexe1Group.annexe1[i].annexe1Interlocuteur				!= null ?  $ds020PE2.annexe1Group.annexe1[i].annexe1Interlocuteur			: ''			;
	formFields['annexe1Email']                    			 = $ds020PE2.annexe1Group.annexe1[i].annexe1Email						!= null ?  $ds020PE2.annexe1Group.annexe1[i].annexe1Email					: ''			;
	formFields['annexe1AnimauxCompagnie']         			 = Value('id').of($ds020PE2.annexe1Group.annexe1[i].especeTraiteesChoix).eq('annexe1AnimauxCompagnie')	 ?  true : false ;
	formFields['annexe1AnimauxRente']             			 = Value('id').of($ds020PE2.annexe1Group.annexe1[i].especeTraiteesChoix).eq('annexe1AnimauxRente')		 ?  true : false ;
	formFields['annexe1AnimauxEleves']            			 = Value('id').of($ds020PE2.annexe1Group.annexe1[i].especeTraiteesChoix).eq('annexe1AnimauxEleves')		 ?  true : false ;
	formFields['annexe1NAC']                      			 = Value('id').of($ds020PE2.annexe1Group.annexe1[i].especeTraiteesChoix).eq('annexe1NAC')				 ?  true : false ;
	formFields['annexe1Equides']                  			 = Value('id').of($ds020PE2.annexe1Group.annexe1[i].especeTraiteesChoix).eq('annexe1Equides')			 ?  true : false ;
	formFields['annexe1EspeceAutre']              			 = Value('id').of($ds020PE2.annexe1Group.annexe1[i].especeTraiteesChoix).eq('annexe1EspeceAutre')		 ? true : false ;
	formFields['annexe1EspeceAutrePrecision']     			 = $ds020PE2.annexe1Group.annexe1[i].annexe1EspeceAutrePrecision		!= null ?  $ds020PE2.annexe1Group.annexe1[i].annexe1EspeceAutrePrecision		:''	;   

/*****************************************************************************************************************************************************
 * Création du dossier Demande d’inscription au tableau de l’ordre Annexe 3
 *****************************************************************************************************************************************************/

 var cerfaDoc2 = nash.doc //
	.load('models/DOSSIER-INSCRIP-SOCIETES - Annexe 1.pdf') //
	.apply (formFields);
	cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));
	}


	

function appendPj(fld) {
	fld.forEach(function (elm) {
        cerfaDoc1.append(elm);
    });
}	
	
appendPj($attachmentPreprocess.attachmentPreprocess.pjID); 
appendPj($attachmentPreprocess.attachmentPreprocess.pjStatutSigne); 
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustifDomicile); 
appendPj($attachmentPreprocess.attachmentPreprocess.pjReglementInterieur); 
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveLiberation); 
appendPj($attachmentPreprocess.attachmentPreprocess.pjKbis); 


if(Value('id').of($ds020PE2.veterinaire.veterinaireChoix.questionDeclaration).eq('formeJuridiqueSCP')){
	appendPj($attachmentPreprocess.attachmentPreprocess.pjSCP); 

}
		
	
	
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $ds020PE2.signatureGroup.signature.signatureFaitLe ,
		autoriteHabilitee :"Ordre national des vétérinaires" ,
		demandeContexte : "Demande d’inscription au tableau de l’ordre.",
		civiliteNomPrenom : nomPrenom
	});
	finalDoc.append(cerfaDoc1.save('cerfa.pdf'));	
	
	
/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('veterinaire_PM.pdf');	
	
	
/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Vétérinaire - Demande d’inscription au tableau de l’ordre',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : "Dossier de demande d'inscription au tableau de l'ordre des vétérinaies d'une société d'exercice vétérinaire.",
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});