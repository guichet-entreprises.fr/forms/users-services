// attachment('attachmentType', 'outputFileFieldId', { label: $pageId.fieldId });

var nomPrenom = $ds020PE2.signatureGroup.signature.signatureNom + ' '+ $ds020PE2.signatureGroup.signature.signaturePrenom 
attachment('pjID', 'pjID', {label: nomPrenom, mandatory:"true"});	
attachment('pjStatutSigne', 'pjStatutSigne', { mandatory:"true"});		
attachment('pjJustifDomicile', 'pjJustifDomicile', { mandatory:"true"});	
attachment('pjReglementInterieur', 'pjReglementInterieur', { mandatory:"false"});	
attachment('pjPreuveLiberation', 'pjPreuveLiberation', { mandatory:"true"});	
attachment('pjKbis', 'pjKbis', { mandatory:"true"});

if(Value('id').of($ds020PE2.veterinaire.veterinaireChoix.questionDeclaration).eq('formeJuridiqueSCP')){
	attachment('pjSCP', 'pjSCP', { mandatory:"true"});
}
		