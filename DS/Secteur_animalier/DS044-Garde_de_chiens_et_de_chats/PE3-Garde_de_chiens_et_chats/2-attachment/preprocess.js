// attachment('attachmentType', 'outputFileFieldId', { label: $pageId.fieldId });
attachment('pjID', 'pjID');
attachment('pjPlanCadastre', 'pjPlanCadastre');
attachment('pjPlanEnsemble', 'pjPlanEnsemble');
attachment('pjRubriques', 'pjRubriques');
attachment('pjPresentation', 'pjPresentation');


//regime Autorisation
var pj=$ds044PE3.description.description.autorisation;
if(Value('id').of(pj).contains('ouiAutorisation')) {
    attachment('pjregimeAutorisation', 'pjregimeAutorisation', { mandatory:"true"});
}

//Natrura
var pj=$ds044PE3.natura.natura.reference;
if(Value('id').of(pj).contains('ouiNatura')) {
    attachment('pjDossierEval', 'pjDossierEval', { mandatory:"true"});
}
//Natrura
var pj=$ds044PE3.signature.signature.demandeModif;
if(Value('id').of(pj).contains('ouiPrescription')) {
    attachment('pjModif', 'pjModif', { mandatory:"true"});
}