function pad(s) { return (s < 10) ? '0' + s : s; }

var cerfaFields = {};

var civNomPrenom = $ds007PE4.cadre1NatureActivite.identificationEtablissement.exploitantEtablissement.civilite + ' ' + $ds007PE4.cadre1NatureActivite.identificationEtablissement.exploitantEtablissement.etablissementExploitantNom + ' ' + $ds007PE4.cadre1NatureActivite.identificationEtablissement.exploitantEtablissement.etablissementExploitantPrenom;

cerfaFields['numeroSiret']                                                  = $ds007PE4.cadre1NatureActivite.identificationEtablissement.etablissementSiren.split(' ').join('');
cerfaFields['raisonSociale']                                         	    = $ds007PE4.cadre1NatureActivite.identificationEtablissement.etablissementRaisonSociale;

//Adresse etablissement
cerfaFields['numeroNomVoie']                                         		= ($ds007PE4.cadre1NatureActivite.identificationEtablissement.adresseEtablissement.adresseEtablissementNumNomVoie != null ? $ds007PE4.cadre1NatureActivite.identificationEtablissement.adresseEtablissement.adresseEtablissementNumNomVoie : '') + ' ' + ($ds007PE4.cadre1NatureActivite.identificationEtablissement.adresseEtablissement.adresseEtablissementComplement != null ? $ds007PE4.cadre1NatureActivite.identificationEtablissement.adresseEtablissement.adresseEtablissementComplement : '');
cerfaFields['commune']                                                  	= $ds007PE4.cadre1NatureActivite.identificationEtablissement.adresseEtablissement.etablissementCommune;
cerfaFields['codePostal']                                               	= $ds007PE4.cadre1NatureActivite.identificationEtablissement.adresseEtablissement.etablissementCodePostal;

//Exploitant de l'établissement
cerfaFields['nomExploitant']                                            	= $ds007PE4.cadre1NatureActivite.identificationEtablissement.exploitantEtablissement.etablissementExploitantNom;
cerfaFields['prenomExploitant']                                      		= $ds007PE4.cadre1NatureActivite.identificationEtablissement.exploitantEtablissement.etablissementExploitantPrenom;
cerfaFields['telephoneFixe']                                  				= $ds007PE4.cadre1NatureActivite.identificationEtablissement.exploitantEtablissement.etablissementExploitantTelephoneFixe != null ? $ds007PE4.cadre1NatureActivite.identificationEtablissement.exploitantEtablissement.etablissementExploitantTelephoneFixe : '';
cerfaFields['telephoneMobile']                               				= $ds007PE4.cadre1NatureActivite.identificationEtablissement.exploitantEtablissement.etablissementExploitantTelephoneMobile;
cerfaFields['courriel']                                   					= $ds007PE4.cadre1NatureActivite.identificationEtablissement.exploitantEtablissement.etablissementExploitantCourriel;

cerfaFields['premiereDeclaration']                                    		= (Value('id').of($ds007PE4.cadre1NatureActivite.identificationEtablissement.questionDeclaration).eq('etablissementPremiereDeclaration') != false ? true : false);
cerfaFields['actualisation']                                           		= (Value('id').of($ds007PE4.cadre1NatureActivite.identificationEtablissement.questionDeclaration).eq('etablissementActualisation') != false ? true : false);

//Nature et quantité des produits cédés
cerfaFields['quantitéViandeFraiche']             							= $ds007PE4.natureQuantiteProduitsCedes.categoriesProduits.viandesFraichesQP;
cerfaFields['quantiteHebdoViandeFraiche']									= $ds007PE4.natureQuantiteProduitsCedes.categoriesProduits.viandesFraichesQC;
cerfaFields['rapport1']        												= $ds007PE4.natureQuantiteProduitsCedes.categoriesProduits.viandesFraichesRapport;

cerfaFields['quantiteProduitsBaseViande']                    				= $ds007PE4.natureQuantiteProduitsCedes.categoriesProduits.produitsQP;
cerfaFields['quantiteHebdoProduitsBaseViande']                   			= $ds007PE4.natureQuantiteProduitsCedes.categoriesProduits.produitsQC;
cerfaFields['rapport2']              										= $ds007PE4.natureQuantiteProduitsCedes.categoriesProduits.produitsRapport;

cerfaFields['quantitéLaitsTraites']                       					= $ds007PE4.natureQuantiteProduitsCedes.categoriesProduits.laitsQP;
cerfaFields['quantiteHebdoLaitsTraites']                        			= $ds007PE4.natureQuantiteProduitsCedes.categoriesProduits.laitsQC;
cerfaFields['rapport3']                  									= $ds007PE4.natureQuantiteProduitsCedes.categoriesProduits.laitsRapport;

cerfaFields['quantiteproduitslaitiers']             						= $ds007PE4.natureQuantiteProduitsCedes.categoriesProduits.produitslaitiersQP;
cerfaFields['quantiteHebdoProduitsLaitiers']            					= $ds007PE4.natureQuantiteProduitsCedes.categoriesProduits.produitslaitiersQC;
cerfaFields['rapport4']       												= $ds007PE4.natureQuantiteProduitsCedes.categoriesProduits.produitslaitiersRapport;

cerfaFields['quantitePreparationbaseOeuf']                					= $ds007PE4.natureQuantiteProduitsCedes.categoriesProduits.preparationsQP;
cerfaFields['quantiteHebdoPreparationbaseOeuf']               				= $ds007PE4.natureQuantiteProduitsCedes.categoriesProduits.preparationsQC;
cerfaFields['rapport5']           											= $ds007PE4.natureQuantiteProduitsCedes.categoriesProduits.preparationsRapport;

cerfaFields['quantiteProduitsTransformes']      							= $ds007PE4.natureQuantiteProduitsCedes.categoriesProduits.produitsNonTransformesQP;
cerfaFields['quantiteHebdoProduitstransformes'] 							= $ds007PE4.natureQuantiteProduitsCedes.categoriesProduits.produitsNonTransformesRapport;
cerfaFields['rapport6']         											= $ds007PE4.natureQuantiteProduitsCedes.categoriesProduits.produitsNonTransformesQC;

cerfaFields['quantiteProduitsNontransformes']         						= $ds007PE4.natureQuantiteProduitsCedes.categoriesProduits.produitsTransformesQP;
cerfaFields['quantiteHebdoProduitNontransformes']        					= $ds007PE4.natureQuantiteProduitsCedes.categoriesProduits.produitsTransformesQC;
cerfaFields['rapport7']     												= $ds007PE4.natureQuantiteProduitsCedes.categoriesProduits.produitsTransformesRapport;

cerfaFields['quantiteEscargots']											= $ds007PE4.natureQuantiteProduitsCedes.categoriesProduits.quantiteEscargots
cerfaFields['quantiteHebdoEscargots'] 										= $ds007PE4.natureQuantiteProduitsCedes.categoriesProduits.quantiteHebdoEscargots
cerfaFields['rapport8'] 													= $ds007PE4.natureQuantiteProduitsCedes.categoriesProduits.escargotRapport

cerfaFields['quantiteRepasPreparation']										= $ds007PE4.natureQuantiteProduitsCedes.categoriesProduits.quantiteRepasPreparation
cerfaFields['quantiteHebdoRepasPreparation']								= $ds007PE4.natureQuantiteProduitsCedes.categoriesProduits.quantiteHebdoRepasPreparation
cerfaFields['rapport9']														= $ds007PE4.natureQuantiteProduitsCedes.categoriesProduits.repasPreparationRapport


//Liste des établissements de commerce déstinataire livrés régulièrement

cerfaFields['etablissement0']                     = '';
cerfaFields['etablissement1']                     = '';
cerfaFields['etablissement2']                     = '';
cerfaFields['etablissement3']                     = '';
cerfaFields['etablissement4']                     = '';
cerfaFields['etablissement5']                     = '';
cerfaFields['etablissement6']                     = '';
cerfaFields['etablissement7']                     = '';
cerfaFields['etablissement8']                     = '';
cerfaFields['etablissement9']                     = '';
cerfaFields['etablissement10']                    = '';
cerfaFields['etablissement11']                    = '';
cerfaFields['adresse0']                           = '';
cerfaFields['adresse1']                           = '';
cerfaFields['adresse2']                           = '';
cerfaFields['adresse3']                           = '';
cerfaFields['adresse4']                           = '';
cerfaFields['adresse5']                           = '';
cerfaFields['adresse6']                           = '';
cerfaFields['adresse7']                           = '';
cerfaFields['adresse8']                           = '';
cerfaFields['adresse9']                           = '';
cerfaFields['adresse10']                          = '';
cerfaFields['adresse11']                          = '';
cerfaFields['distance0']                          = '';
cerfaFields['distance1']                          = '';
cerfaFields['distance2']                          = '';
cerfaFields['distance3']                          = '';
cerfaFields['distance4']                          = '';
cerfaFields['distance5']                          = '';
cerfaFields['distance6']                          = '';
cerfaFields['distance7']                          = '';
cerfaFields['distance8']                          = '';
cerfaFields['distance9']                          = '';
cerfaFields['distance10']                         = '';
cerfaFields['distance11']                         = '';
cerfaFields['categorie0']                         = '';
cerfaFields['categorie1']                         = '';
cerfaFields['categorie2']                         = '';
cerfaFields['categorie3']                         = '';
cerfaFields['categorie4']                         = '';
cerfaFields['categorie5']                         = '';
cerfaFields['categorie6']                         = '';
cerfaFields['categorie7']                         = '';
cerfaFields['categorie8']                         = '';
cerfaFields['categorie9']                         = '';
cerfaFields['categorie10']                        = '';
cerfaFields['categorie11']                        = '';


for (var i= 0; i < $ds007PE4.listeEtablissementLivres.listeEtablissementCommerceLivres.size(); i++ ){
	cerfaFields['etablissement'+i]             = $ds007PE4.listeEtablissementLivres.listeEtablissementCommerceLivres[i].nomEtablissement != null ? $ds007PE4.listeEtablissementLivres.listeEtablissementCommerceLivres[i].nomEtablissement : '' ;
	cerfaFields['adresse'+i]				   = $ds007PE4.listeEtablissementLivres.listeEtablissementCommerceLivres[i].numeroLibelleAdresseEtablissement 
												+ ($ds007PE4.listeEtablissementLivres.listeEtablissementCommerceLivres[i].complementAdresseEtablissement != null ? ' '+$ds007PE4.listeEtablissementLivres.listeEtablissementCommerceLivres[i].complementAdresseEtablissement: '')
												+ ($ds007PE4.listeEtablissementLivres.listeEtablissementCommerceLivres[i].codePostalAdresseEtablissement != null ? ' '+$ds007PE4.listeEtablissementLivres.listeEtablissementCommerceLivres[i].codePostalAdresseEtablissement: '')
												+ ($ds007PE4.listeEtablissementLivres.listeEtablissementCommerceLivres[i].villeAdresseEtablissement != null ? ' '+$ds007PE4.listeEtablissementLivres.listeEtablissementCommerceLivres[i].villeAdresseEtablissement: '')
	cerfaFields['distance'+i]					   = $ds007PE4.listeEtablissementLivres.listeEtablissementCommerceLivres[i].distanceEtablissement != null ? $ds007PE4.listeEtablissementLivres.listeEtablissementCommerceLivres[i].distanceEtablissement : '' ;
	cerfaFields['categorie'+i]				   = $ds007PE4.listeEtablissementLivres.listeEtablissementCommerceLivres[i].categoriesProduitsLivres != null ? $ds007PE4.listeEtablissementLivres.listeEtablissementCommerceLivres[i].categoriesProduitsLivres : '' ;
}

cerfaFields['declarationHonneur']              = $ds007PE4.cadre1NatureActivite.identificationEtablissement.exploitantEtablissement.etablissementExploitantNom + ' ' + $ds007PE4.cadre1NatureActivite.identificationEtablissement.exploitantEtablissement.etablissementExploitantPrenom;
cerfaFields['signatureCoche']                  = $ds007PE4.signature.cadre4SignatureEngagement.signatureDeclarant;
//cerfaFields['dateSignature']                                                          = $ds007PE4.cadre4.cadre4SignatureEngagement.dateSignature;

if($ds007PE4.signature.cadre4SignatureEngagement.dateSignature != null) {
var dateTemp = new Date(parseInt ($ds007PE4.signature.cadre4SignatureEngagement.dateSignature.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
	cerfaFields['jourSignature'] = day;
	cerfaFields['moisSignature'] = month;
	cerfaFields['anneeSignature'] = year;
}

/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
 var finalDoc = nash.doc 
	.load('models/Courrier au premier dossier v2.1 GE.pdf')
	.apply({
		date: $ds007PE4.signature.cadre4SignatureEngagement.dateSignature,
		autoriteHabilitee :" ",
		demandeContexte : "Déclaration concernant les établissements préparant, transformant, manipulant, exposant, mettant en vente, entreposant ou transportant des denrées animales ou d'origine animale",
		civiliteNomPrenom : civNomPrenom
	});
	
var cerfaDoc = nash.doc
    .load('models/Cerfa n°13982_05.pdf')
    .apply(cerfaFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);



var finalDocItem = finalDoc.save('Cerfa n°13982_05.pdf');


return spec.create({
    id : 'review',
   label : 'Déclaration concernant les établissements préparant, transformant, manipulant, exposant, mettant en vente, entreposant ou transportant des denrées animales ou d\'origine animale.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration concernant les établissements préparant, transformant, manipulant, exposant, mettant en vente, entreposant ou transportant des denrées animales ou d\'origine animale.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});