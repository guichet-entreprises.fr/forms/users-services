function pad(s) { return (s < 10) ? '0' + s : s; }
var formFields = {};

var civNomPrenom = $ds033PE02.identificationGroup.identificationDeclarant.civilite + " " + $ds033PE02.identificationGroup.identificationDeclarant.nomDeclarant + " " + $ds033PE02.identificationGroup.identificationDeclarant.prenomDeclarant + ', ';


//Identification de la collectivite ou de l'association
formFields['nomDeLaCollectiviteOuAssociation'] = $ds033PE02.identificationGroup.identificationCollectiviteAssociation.nomDeLaCollectiviteOuAssociation;
formFields['numeroDeTelephone']                = $ds033PE02.identificationGroup.identificationCollectiviteAssociation.numeroDeTelephone;
formFields['fax']                              = $ds033PE02.identificationGroup.identificationCollectiviteAssociation.fax;
formFields['adresseMail']                      = $ds033PE02.identificationGroup.identificationCollectiviteAssociation.adresseMail;

//identification du declarant
formFields['civilite']                         = $ds033PE02.identificationGroup.identificationDeclarant.civilite;
formFields['nomDeclarant']                     = $ds033PE02.identificationGroup.identificationDeclarant.nomDeclarant;
formFields['prenomDeclarant']                  = $ds033PE02.identificationGroup.identificationDeclarant.prenomDeclarant;
formFields['nomCollectiviteAssociation']       = $ds033PE02.identificationGroup.identificationDeclarant.nomCollectiviteAssociation;
formFields['adresse']                          = $ds033PE02.identificationGroup.identificationDeclarant.numeroDeVoieEtLibbele + " " + $ds033PE02.identificationGroup.identificationDeclarant.complementAdresse + " " + $ds033PE02.identificationGroup.identificationDeclarant.codePostal + " " +  $ds033PE02.identificationGroup.identificationDeclarant.commune;
formFields['fonction']                         = $ds033PE02.identificationGroup.identificationDeclarant.fonction;
formFields['dateAvisCommissaire']              = $ds033PE02.identificationGroup.identificationDeclarant.dateAvisCommissaire;
formFields['nomPersonneAvis']                  = $ds033PE02.identificationGroup.identificationDeclarant.nomPersonneAvis;
formFields['qualitePersonneAvis']              = $ds033PE02.identificationGroup.identificationDeclarant.qualitePersonneAvis;

//Information de la manifestation
formFields['lieuInstallationDebitTemp']        = $ds033PE02.identificationManifGroup.identificationManif.lieuInstallationDebitTemp;
formFields['dateDebutManifestation']           = $ds033PE02.identificationManifGroup.identificationManif.dateDebutManifestation;
formFields['minutesDebutManif']                = $ds033PE02.identificationManifGroup.identificationManif.minutesDebutManif;
formFields['heureDebutManif']                  = $ds033PE02.identificationManifGroup.identificationManif.heureDebutManif;
formFields['dateFinManif']                     = $ds033PE02.identificationManifGroup.identificationManif.dateFinManif;
formFields['heurefinManif']                    = $ds033PE02.identificationManifGroup.identificationManif.heurefinManif;
formFields['minutesFinManif']                  = $ds033PE02.identificationManifGroup.identificationManif.minutesFinManif;
formFields['natureManif']                      = $ds033PE02.identificationManifGroup.identificationManif.natureManif;
formFields['nomEtatCollecAssoc']               = $ds033PE02.identificationManifGroup.identificationManif.nomEtatCollecAssoc;

//Signature

var signChemin = $ds033PE02.signaturegroup.signature;
var civNomPrenomSign = $ds033PE02.identificationGroup.identificationDeclarant.civilite + " " + $ds033PE02.identificationGroup.identificationDeclarant.nomDeclarant + " " + $ds033PE02.identificationGroup.identificationDeclarant.prenomDeclarant + ",";


formFields['civiliteNomPrenom']                = civNomPrenom;
formFields['certifieExactitude']               = $ds033PE02.signaturegroup.signature.certifieExactitude;
formFields['engagement']                       = $ds033PE02.signaturegroup.signature.engagement;
formFields['dateSignature']                    = $ds033PE02.signaturegroup.signature.dateSignature;
formFields['lieuSignature']                    = $ds033PE02.signaturegroup.signature.lieuSignature;

if($ds033PE02.signaturegroup.signature.dateSignature != null) {
	var dateTmp = new Date(parseInt($ds033PE02.signaturegroup.signature.dateSignature.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['dateSignature'] = date;
}

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */

var finalDoc = nash.doc 
	.load('models/Courrier au premier dossier v2.1 GQ.pdf')
	.apply({
		date: $ds033PE02.signaturegroup.signature.dateSignature,
		autoriteHabilitee :"La mairie de Champs-Sur-Marne",
		demandeContexte : "Déclarer en mairie l'ouverture d'un débit temporaire dans le cadre d'une exposition, ou d'une foire organisée par l'état, les collectivités publiques ou les associations reconnues d'utilité publique",
		civiliteNomPrenom : civNomPrenom
	});
	
var cerfaDoc = nash.doc
    .load('models/model.pdf')
    .apply(formFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAvisConformeCommissaire);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Debiteur_De_Boisson.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Déclaration d\'ouverture d\'un débit de boisson temporaire.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration d\'ouverture d\'un débit de boisson temporaire dans le cadre d\'une exposition ou d\'une foire organisée par l\'Etat .',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]                                                                                                                                                                   
        }) ]
    }) ]
});