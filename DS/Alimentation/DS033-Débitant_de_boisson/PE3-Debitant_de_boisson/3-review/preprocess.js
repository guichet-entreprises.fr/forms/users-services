var formFields = {};

//identification du demandeur

var cheminDemandeur = $ds033PE03.identificationDemandeurGroup.identificationDemandeur;

formFields['civilite']            = cheminDemandeur.civilite;
formFields['nomDemandeur']        = cheminDemandeur.nomDemandeur;
formFields['prenomDemandeur']     = cheminDemandeur.prenomDemandeur;
formFields['professionDemandeur'] = cheminDemandeur.professionDemandeur;
formFields['adresseDemandeur']    = cheminDemandeur.numeroLibelleAdresseDeclarant +" "+ cheminDemandeur.complementAdresseDeclarant + " " + cheminDemandeur.codePostal + " " + cheminDemandeur.commune;

//Identification de l'établissement

var cheminEtablissement = $ds033PE03.identificationEtablissementGroup.identificationEtablissement;

formFields['lieuEtablissement']   = cheminEtablissement.lieuEtablissement;
formFields['dateDebut']           = cheminEtablissement.dateDebut;
formFields['dateFin']             = cheminEtablissement.dateFin;
formFields['occasion']            = cheminEtablissement.occasion;
formFields['motif']               = cheminEtablissement.motif;
formFields['lieuOuverture']       = cheminEtablissement.lieuOuverture;
formFields['dateOuverture']       = cheminEtablissement.dateOuverture;
formFields['heureOuverture']      = cheminEtablissement.heureOuverture;

//Signature
 
var civNomPrenom = $ds033PE03.identificationDemandeurGroup.identificationDemandeur.civilite +" "+ $ds033PE03.identificationDemandeurGroup.identificationDemandeur.nomDemandeur + " " + $ds033PE03.identificationDemandeurGroup.identificationDemandeur.prenomDemandeur + " ,";
var cheminSignature = $ds033PE03.signaturegroup.signature; 

formFields['civiliteNomPrenom']   = civNomPrenom;
formFields['certifieExactitude']  = cheminSignature.certifieExactitude;
formFields['engagement']  		  = cheminSignature.engagement;
formFields['dateSignature']       = cheminSignature.dateSignature;
formFields['lieuSignature']       = cheminSignature.lieuSignature;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */

var finalDoc = nash.doc 
	.load('models/Courrier au premier dossier v2.1 GQ.pdf')
	.apply({
		date: $ds033PE03.signaturegroup.signature.dateSignature,
		autoriteHabilitee :"Préfecture de paris",
		demandeContexte : "Demander une autorisation municipale en cas d'ouverture d'un débit temporaire dans le cadre d'une foire, d'une vente ou d'une fête publique",
		civiliteNomPrenom : civNomPrenom
	});
	
var cerfaDoc = nash.doc
    .load('models/model.pdf')
    .apply(formFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Autorisation_ouverture_debit_boisson_temporaire.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Demande d’autorisation d\'un débit de boisson temporaire.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration d\'ouverture d\'un débit de boisson temporaire dans le cadre d\'une foire, vente ou fête publique.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]                                                                                                                                                                   
        }) ]
    }) ]
});