var formFields = {};


formFields['declarationOuverture_11542']                   = Value('id').of($ds033PE01.declarationGroup.declaration.declarationChoix).eq('declarationOuverture11542') ? true : false;
formFields['declarationMutation_11542']                    = Value('id').of($ds033PE01.declarationGroup.declaration.declarationChoix).eq('declarationMutation11542') ? true : false;
formFields['declarationTranslation_11542']                 = Value('id').of($ds033PE01.declarationGroup.declaration.declarationChoix).eq('declarationTranslation11542') ? true : false;

//cadre Catégorie de licence
formFields['categorieLicence3_11542']                      =   Value('id').of($ds033PE01.declarationGroup.categorieDeLicence.licenceDebitSurPlace).eq('categorieLicence311542')     ? true : false ;
formFields['categorieLicence4_11542']                      =   Value('id').of($ds033PE01.declarationGroup.categorieDeLicence.licenceDebitSurPlace).eq('categorieLicence411542')     ? true : false ;
formFields['categorieLicenceRestaurant_11542']             =   Value('id').of($ds033PE01.declarationGroup.categorieDeLicence.licenceRestaurant).eq('categoriePetiteLicenceRestautant11542')        ? true : false ;
formFields['categoriePetiteLicenceRestautant_11542']       =   Value('id').of($ds033PE01.declarationGroup.categorieDeLicence.licenceRestaurant).eq('categorieLicenceRestaurant11542')        ? true : false ;
formFields['categoriePetiteLicenceEmporter_11542']         =   Value('id').of($ds033PE01.declarationGroup.categorieDeLicence.licenceDebitEmporter).eq('categoriePetiteLicenceEmporter11542')     ? true : false ;
formFields['categorieLicenceEmporter_11542']               =   Value('id').of($ds033PE01.declarationGroup.categorieDeLicence.licenceDebitEmporter).eq('categorieLicenceEmporter11542')     ? true : false ;

//formFields['categorieLicence3_11542']                      = $ds033PE01.declarationGroup.categorieDeLicence.licenceDebitSurPlace.categorieLicence311542 ? true : false;
//formFields['categorieLicence4_11542']                      = $ds033PE01.declarationGroup.categorieDeLicence.licenceDebitSurPlace.categorieLicence411542 ? true : false;
//formFields['categorieLicenceRestaurant_11542']             = $ds033PE01.declarationGroup.categorieDeLicence.licenceRestaurant.categorieLicenceRestaurant11542 ? true : false;
//formFields['categoriePetiteLicenceRestautant_11542']       = $ds033PE01.declarationGroup.categorieDeLicence.licenceRestaurant.categoriePetiteLicenceRestautant11542 ? true : false;
//formFields['categoriePetiteLicenceEmporter_11542']         = $ds033PE01.declarationGroup.categorieDeLicence.licenceDebitEmporter.categoriePetiteLicenceEmporter11542 ? true : false;
//formFields['categorieLicenceEmporter_11542']               = $ds033PE01.declarationGroup.categorieDeLicence.licenceDebitEmporter.categorieLicenceEmporter11542 ? true : false;
                                                                                        

//cadre Le débit de boissons
formFields['enseigneDebitBoisson_11542']                   = $ds033PE01.declarationGroup.debitBoissons.enseigneDebitBoisson11542;
formFields['numeroTelephoneDebitBoisson_11542']            = $ds033PE01.declarationGroup.debitBoissons.telephoneDebitBoisson11542;
formFields['adresseDebitBoisson_11542']                    = $ds033PE01.declarationGroup.debitBoissons.numeroDebitBoisson11542 +' '
															+ ($ds033PE01.declarationGroup.debitBoissons.complementDebitBoisson11542 ? $ds033PE01.declarationGroup.debitBoissons.complementDebitBoisson11542 : '')  +' '
															+ ($ds033PE01.declarationGroup.debitBoissons.codePostalDebitBoisson11542 ? $ds033PE01.declarationGroup.debitBoissons.codePostalDebitBoisson11542 :'')  +' '
															+ $ds033PE01.declarationGroup.debitBoissons.communeDebitBoisson11542 +' ' ;


//cadre Propriétaires du fonds de commerce

// Proprietaire Personne physique 1
formFields['proprietaireNomNaissance1_11542']              = $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique1.proprietaireNomNaissance111542;
formFields['proprietaireNomUsage1_11542']                  = $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique1.proprietaireNomUsage111542;
formFields['proprietairePrenom1_11542']                    = $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique1.proprietairePrenom111542;
formFields['proprietaireProfession1_11542']                = $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique1.proprietaireProfession111542;
formFields['proprietaireAdresse1_11542']                   = ($ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique1.proprietairenumero111542 ? 
																($ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique1.proprietairenumero111542 +' ' 
																+ ($ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique1.proprietairecomplement111542 ? $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique1.proprietairecomplement111542 :'') +' '
																+ ($ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique1.proprietairecodePostal111542 ? $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique1.proprietairecodePostal111542 :'') +' '
																+ $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique1.proprietairecommune111542 +' '
																+ $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique1.proprietairePays111542 
																): '');
formFields['proprietaireTelephone1_11542']                 = $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique1.proprietaireTelephone111542 ;
formFields['proprietaireEmail1_11542']                     = $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique1.proprietaireEmail111542;

// Proprietaire Personne physique 2
formFields['proprietaireNomNaissance2_11542']              = $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique2.proprietaireNomNaissance211542;
formFields['proprietaireNomUsage2_11542']                  = $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique2.proprietaireNomUsage211542;
formFields['proprietairePrenom2_11542']                    = $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique2.proprietairePrenom211542;
formFields['proprietaireProfession2_11542']                = $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique2.proprietaireProfession211542;
formFields['proprietaireAdresse2_11542']                   = ($ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique2.proprietairenumero211542 ?
																($ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique2.proprietairenumero211542+' ' 
																+ ($ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique2.proprietairecomplement211542 ? $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique2.proprietairecomplement211542 +' ' :'')
																+ ($ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique2.proprietairecodePostal211542 ? $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique2.proprietairecodePostal211542 +' ' :'') 
																+ $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique2.proprietairecommune211542 +' '
																+ $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique2.proprietairePays211542 
																): '');
formFields['proprietaireTelephone2_11542']                 = $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique2.proprietaireTelephone211542 ;
formFields['proprietaireEmail2_11542']                     = $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique2.proprietaireEmail211542;

// Proprietaire Personne physique 3
formFields['proprietaireNomNaissance3_11542']              = $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique3.proprietaireNomNaissance311542;
formFields['proprietaireNomUsage3_11542']                  = $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique3.proprietaireNomUsage311542;
formFields['proprietairePrenom3_11542']                    = $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique3.proprietairePrenom311542;
formFields['proprietaireProfession3_11542']                = $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique3.proprietaireProfession311542;
formFields['proprietaireAdresse3_11542']                   = ($ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique3.proprietairenumero311542 ? 
																($ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique3.proprietairenumero311542 +' ' 
																+ ($ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique3.proprietairecomplement311542 ? $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique3.proprietairecomplement311542 :'') +' '
																+ ($ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique3.proprietairecodePostal311542 ? $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique3.proprietairecodePostal311542 :'') +' '
																+ $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique3.proprietairecommune311542 +' '
																+ $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique3.proprietairePays311542 
																): '');
formFields['proprietaireTelephone3_11542']                 = $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique3.proprietaireTelephone311542 ;
formFields['proprietaireEmail3_11542']                     = $ds033PE01.declarationGroup.proprietaireFondsCommerce.personnePhysique3.proprietaireEmail311542;


//Personne morale

formFields['proprietairePersonneMoraleDenomination_11542'] = $ds033PE01.declarationGroup.proprietaireFondsCommerce.proprietairePersonneMorale.proprietairePersonneMoraleDenomination11542;
formFields['proprietairePersonneMoraleAdresseSiege_11542'] = ($ds033PE01.declarationGroup.proprietaireFondsCommerce.proprietairePersonneMorale.proprietaireMoralenumero11542 ? 
																($ds033PE01.declarationGroup.proprietaireFondsCommerce.proprietairePersonneMorale.proprietaireMoralenumero11542  +' '
																+ ($ds033PE01.declarationGroup.proprietaireFondsCommerce.proprietairePersonneMorale.proprietaireMoralecomplement11542 ? $ds033PE01.declarationGroup.proprietaireFondsCommerce.proprietaireMoralecomplement11542 :'') +' '
																+ ($ds033PE01.declarationGroup.proprietaireFondsCommerce.proprietairePersonneMorale.proprietaireMoralecodePostal11542 ? $ds033PE01.declarationGroup.proprietaireFondsCommerce.proprietaireMoralecodePostal11542 :'') +' '
																+ $ds033PE01.declarationGroup.proprietaireFondsCommerce.proprietairePersonneMorale.proprietaireMoralecommune11542 +' '
																+ $ds033PE01.declarationGroup.proprietaireFondsCommerce.proprietairePersonneMorale.proprietaireMoralePays11542 +' '
																): '');
formFields['proprietairePersonneMoraleTelephone_11542']    = $ds033PE01.declarationGroup.proprietaireFondsCommerce.proprietairePersonneMorale.proprietaireMoraleTelephone11542;
															

//Exploitant 1
var exploitantPrincipal = (Value('id').of($ds033PE01.declarationGroup.exploitants.exploitant1.exploitantCivilite111542).eq('exploitantCiviliteMme111542') ? 'Mme' : 'Mr' ) 
							+ ' ' 
							+ ($ds033PE01.declarationGroup.exploitants.exploitant1.exploitantNomUsage111542 ? $ds033PE01.declarationGroup.exploitants.exploitant1.exploitantNomUsage111542 : $ds033PE01.declarationGroup.exploitants.exploitant1.exploitantNomNaissance111542 ) +' '
							+ $ds033PE01.declarationGroup.exploitants.exploitant1.exploitantPrenom111542;
formFields['exploitantCiviliteMme1_11542']                 = Value('id').of($ds033PE01.declarationGroup.exploitants.exploitant1.exploitantCivilite111542).eq('exploitantCiviliteMme111542') ? true : false;
formFields['exploitantCiviliteMr1_11542']                  = Value('id').of($ds033PE01.declarationGroup.exploitants.exploitant1.exploitantCivilite111542).eq('exploitantCiviliteMr111542') ? true : false;
formFields['exploitantNomNaissance1_11542']                = $ds033PE01.declarationGroup.exploitants.exploitant1.exploitantNomNaissance111542;
formFields['exploitantNomUsage1_11542']                    = $ds033PE01.declarationGroup.exploitants.exploitant1.exploitantNomUsage111542;
formFields['exploitantPrenom1_11542']                      = $ds033PE01.declarationGroup.exploitants.exploitant1.exploitantPrenom111542;
formFields['exploitantDateNaissance1_11542']               = $ds033PE01.declarationGroup.exploitants.exploitant1.exploitantDateNaissance111542;
formFields['exploitantLieuNaissance1_11542']               = $ds033PE01.declarationGroup.exploitants.exploitant1.exploitantLieuNaissance111542 + ' / '
															 + $ds033PE01.declarationGroup.exploitants.exploitant1.exploitantPaysNaissance111542  ;
formFields['exploitantNationalite1_11542']                 = $ds033PE01.declarationGroup.exploitants.exploitant1.exploitantNationalite111542;
formFields['exploitantTelephone1_11542']                   = $ds033PE01.declarationGroup.exploitants.exploitant1.exploitantTelephone111542;
formFields['exploitantEmail1_11542']                       = $ds033PE01.declarationGroup.exploitants.exploitant1.exploitantEmail111542;

formFields['exploitantQualiteProprietaire1_11542']         = Value('id').of($ds033PE01.declarationGroup.exploitants.exploitant1.exploitantQualite111542).eq('exploitantQualiteProprietaire111542') ? true : false;
formFields['exploitantQualiteLocataire1_11542']            = Value('id').of($ds033PE01.declarationGroup.exploitants.exploitant1.exploitantQualite111542).eq('exploitantQualiteLocataire111542') ? true : false;
formFields['exploitantQualiteRepresentant1_11542']         = Value('id').of($ds033PE01.declarationGroup.exploitants.exploitant1.exploitantQualite111542).eq('exploitantQualiteRepresentant111542') ? true : false;

formFields['exploitantPermisExploitation1_11542']          = $ds033PE01.declarationGroup.exploitants.exploitant1.exploitantPermisExploitation111542 ? true : false;
formFields['exploitantPermisVenteBoissonNumero1_11542']    = $ds033PE01.declarationGroup.exploitants.exploitant1.exploitantPermisVenteBoissonNumero111542;
formFields['exploitantPermisVenteBoissonDate1_11542']      = $ds033PE01.declarationGroup.exploitants.exploitant1.exploitantPermisVenteBoissonDate111542;
formFields['exploitantPermisVenteBoisson1_11542']          = $ds033PE01.declarationGroup.exploitants.exploitant1.exploitantPermisVenteBoisson111542 ? true : false;
formFields['exploitantPermisExploitationNumero1_11542']    = $ds033PE01.declarationGroup.exploitants.exploitant1.exploitantPermisExploitationNumero111542;
formFields['exploitantPermisExploitationDate1_11542']      = $ds033PE01.declarationGroup.exploitants.exploitant1.exploitantPermisExploitationDate111542;

//Exploitant 2
formFields['exploitantCiviliteMr2_11542']                  = Value('id').of($ds033PE01.declarationGroup.exploitants.exploitant2.exploitantCivilite211542).eq('exploitantCiviliteMr211542') ? true : false;
formFields['exploitantCiviliteMme2_11542']                 = Value('id').of($ds033PE01.declarationGroup.exploitants.exploitant2.exploitantCivilite211542).eq('exploitantCiviliteMme211542') ? true : false;
formFields['exploitantNomNaissance2_11542']                = $ds033PE01.declarationGroup.exploitants.exploitant2.exploitantNomNaissance211542;
formFields['exploitantNomUsage2_11542']                    = $ds033PE01.declarationGroup.exploitants.exploitant2.exploitantNomUsage211542;
formFields['exploitantPrenom2_11542']                      = $ds033PE01.declarationGroup.exploitants.exploitant2.exploitantPrenom211542;
formFields['exploitantDateNaissance2_11542']               = $ds033PE01.declarationGroup.exploitants.exploitant2.exploitantDateNaissance211542;
formFields['exploitantLieuNaissance2_11542']               = ($ds033PE01.declarationGroup.exploitants.exploitant2.exploitantLieuNaissance211542 ? $ds033PE01.declarationGroup.exploitants.exploitant2.exploitantLieuNaissance211542 :'') 
															 + ($ds033PE01.declarationGroup.exploitants.exploitant2.exploitantPaysNaissance211542 ? ' / ' +$ds033PE01.declarationGroup.exploitants.exploitant2.exploitantPaysNaissance211542 :'') ;
formFields['exploitantNationalite2_11542']                 = $ds033PE01.declarationGroup.exploitants.exploitant2.exploitantNationalite211542;
formFields['exploitantTelephone2_11542']                   = $ds033PE01.declarationGroup.exploitants.exploitant2.exploitantTelephone211542;
formFields['exploitantEmail2_11542']                       = $ds033PE01.declarationGroup.exploitants.exploitant2.exploitantEmail211542;

formFields['exploitantQualiteProprietaire2_11542']         = Value('id').of($ds033PE01.declarationGroup.exploitants.exploitant2.exploitantQualite211542).eq('exploitantQualiteProprietaire211542') ? true : false;
formFields['exploitantQualiteLocataire2_11542']            = Value('id').of($ds033PE01.declarationGroup.exploitants.exploitant2.exploitantQualite211542).eq('exploitantQualiteLocataire211542') ? true : false;
formFields['exploitantQualiteRepresentant2_11542']         = Value('id').of($ds033PE01.declarationGroup.exploitants.exploitant2.exploitantQualite211542).eq('exploitantQualiteRepresentant211542') ? true : false;

formFields['exploitantPermisExploitation2_11542']          = $ds033PE01.declarationGroup.exploitants.exploitant2.exploitantPermisExploitation211542 ? true : false;
formFields['exploitantPermisExploitationDate2_11542']      = $ds033PE01.declarationGroup.exploitants.exploitant2.exploitantPermisExploitationDate211542;
formFields['exploitantPermisExploitationNumero2_11542']    = $ds033PE01.declarationGroup.exploitants.exploitant2.exploitantPermisExploitationNumero211542;
formFields['exploitantPermisVenteBoisson2_11542']          = $ds033PE01.declarationGroup.exploitants.exploitant2.exploitantPermisVenteBoisson211542 ? true : false;
formFields['exploitantPermisVenteBoissonNumero2_11542']    = $ds033PE01.declarationGroup.exploitants.exploitant2.exploitantPermisVenteBoissonNumero211542;
formFields['exploitantPermisVenteBoissonDate2_11542']      = $ds033PE01.declarationGroup.exploitants.exploitant2.exploitantPermisVenteBoissonDate211542;





formFields['declarationDate_11542']                        = $ds033PE01.declarationFinale.signature.declarationDateOuverture11542 ? $ds033PE01.declarationFinale.signature.declarationDateOuverture11542 : ($ds033PE01.declarationFinale.signature.declarationDateMutation11542 ? $ds033PE01.declarationFinale.signature.declarationDateMutation11542 : ( $ds033PE01.declarationFinale.signature.declarationDateTranslation11542 ? $ds033PE01.declarationFinale.signature.declarationDateTranslation11542 :'')) ;
formFields['declarationFaitA_11542']                       = $ds033PE01.declarationFinale.signature.declarationFaitA11542;
formFields['declarationLe_11542']                      	   = $ds033PE01.declarationFinale.signature.declarationLe11542;
formFields['signatureDeclarant_11542']                     = "Cette déclaration respecte les attendus de l’article 1367 du code civil.";

//var pdfModel = nash.doc.load('models/cerfa_11542-05.pdf').apply(formFields);

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qpxxxPEx.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $ds033PE01.declarationFinale.signature.declarationLe11542,
		autoriteHabilitee :"Mairie de " + $ds033PE01.declarationGroup.debitBoissons.communeDebitBoisson11542 ,
		demandeContexte : "Déclaration d'ouverture d'un débit de boisson",
		civiliteNomPrenom : exploitantPrincipal
	});


//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/cerfa_11542-05.pdf') //
	.apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjIDExploitant1);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPermisExploitationDebitBoissonsExploitant1) ;
if($ds033PE01.declarationGroup.exploitants.exploitant1.exploitantPermisVenteBoisson111542){appendPj($attachmentPreprocess.attachmentPreprocess.pjPermisExploitationLaNuitExploitant1)} ;

if($ds033PE01.declarationGroup.exploitants.exploitant1.exploitant2OUI){appendPj($attachmentPreprocess.attachmentPreprocess.pjIDExploitant2)} ;
if($ds033PE01.declarationGroup.exploitants.exploitant2.exploitantPermisExploitation211542){appendPj($attachmentPreprocess.attachmentPreprocess.pjPermisExploitationDebitBoissonsExploitant2)} ;
if($ds033PE01.declarationGroup.exploitants.exploitant2.exploitantPermisVenteBoisson211542){appendPj($attachmentPreprocess.attachmentPreprocess.pjPermisExploitationLaNuitExploitant2)} ;

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Debit_De_Boisson.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Débitant de boisson - Déclaration d\'ouverture d\'un débit de boisson',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Débitant de boisson - Déclaration d\'ouverture d\'un débit de boisson',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
