function pad(s) { return (s < 10) ? '0' + s : s; }

var cerfaFields = {};

var civNomPrenom = $ds052PE3.cadre1NatureActivite.identificationEtablissement.exploitantEtablissement.civilite + ' ' + $ds052PE3.cadre1NatureActivite.identificationEtablissement.exploitantEtablissement.etablissementExploitantNom + ' ' + $ds052PE3.cadre1NatureActivite.identificationEtablissement.exploitantEtablissement.etablissementExploitantPrenom;

cerfaFields['numeroSiret']                                                  = $ds052PE3.cadre1NatureActivite.identificationEtablissement.etablissementSiren.split(' ').join('');
cerfaFields['raisonSociale']                                         	    = $ds052PE3.cadre1NatureActivite.identificationEtablissement.etablissementRaisonSociale;

//Adresse etablissement
cerfaFields['numeroNomVoie']                                         		= ($ds052PE3.cadre1NatureActivite.identificationEtablissement.adresseEtablissement.adresseEtablissementNumNomVoie != null ? $ds052PE3.cadre1NatureActivite.identificationEtablissement.adresseEtablissement.adresseEtablissementNumNomVoie : '') + ' ' + ($ds052PE3.cadre1NatureActivite.identificationEtablissement.adresseEtablissement.adresseEtablissementComplement != null ? $ds052PE3.cadre1NatureActivite.identificationEtablissement.adresseEtablissement.adresseEtablissementComplement : '');
cerfaFields['commune']                                                  	= $ds052PE3.cadre1NatureActivite.identificationEtablissement.adresseEtablissement.etablissementCommune;
cerfaFields['codePostal']                                               	= $ds052PE3.cadre1NatureActivite.identificationEtablissement.adresseEtablissement.etablissementCodePostal;

//Exploitant de l'établissement
cerfaFields['nomExploitant']                                            	= $ds052PE3.cadre1NatureActivite.identificationEtablissement.exploitantEtablissement.etablissementExploitantNom;
cerfaFields['prenomExploitant']                                      		= $ds052PE3.cadre1NatureActivite.identificationEtablissement.exploitantEtablissement.etablissementExploitantPrenom;
cerfaFields['telephoneFixe']                                  				= $ds052PE3.cadre1NatureActivite.identificationEtablissement.exploitantEtablissement.etablissementExploitantTelephoneFixe != null ? $ds052PE3.cadre1NatureActivite.identificationEtablissement.exploitantEtablissement.etablissementExploitantTelephoneFixe : '';
cerfaFields['telephoneMobile']                               				= $ds052PE3.cadre1NatureActivite.identificationEtablissement.exploitantEtablissement.etablissementExploitantTelephoneMobile;
cerfaFields['courriel']                                   					= $ds052PE3.cadre1NatureActivite.identificationEtablissement.exploitantEtablissement.etablissementExploitantCourriel;

cerfaFields['premiereDeclaration']                                    		= (Value('id').of($ds052PE3.cadre1NatureActivite.identificationEtablissement.questionDeclaration).eq('etablissementPremiereDeclaration') != false ? true : false);
cerfaFields['actualisation']                                           		= (Value('id').of($ds052PE3.cadre1NatureActivite.identificationEtablissement.questionDeclaration).eq('etablissementActualisation') != false ? true : false);

//Nature et quantité des produits cédés
cerfaFields['quantitéViandeFraiche']             							= $ds052PE3.natureQuantiteProduitsCedes.categoriesProduits.viandesFraichesQP;
cerfaFields['quantiteHebdoViandeFraiche']									= $ds052PE3.natureQuantiteProduitsCedes.categoriesProduits.viandesFraichesQC;
cerfaFields['rapport1']        												= $ds052PE3.natureQuantiteProduitsCedes.categoriesProduits.viandesFraichesRapport;

cerfaFields['quantiteProduitsBaseViande']                    				= $ds052PE3.natureQuantiteProduitsCedes.categoriesProduits.produitsQP;
cerfaFields['quantiteHebdoProduitsBaseViande']                   			= $ds052PE3.natureQuantiteProduitsCedes.categoriesProduits.produitsQC;
cerfaFields['rapport2']              										= $ds052PE3.natureQuantiteProduitsCedes.categoriesProduits.produitsRapport;

cerfaFields['quantitéLaitsTraites']                       					= $ds052PE3.natureQuantiteProduitsCedes.categoriesProduits.laitsQP;
cerfaFields['quantiteHebdoLaitsTraites']                        			= $ds052PE3.natureQuantiteProduitsCedes.categoriesProduits.laitsQC;
cerfaFields['rapport3']                  									= $ds052PE3.natureQuantiteProduitsCedes.categoriesProduits.laitsRapport;

cerfaFields['quantiteproduitslaitiers']             						= $ds052PE3.natureQuantiteProduitsCedes.categoriesProduits.produitslaitiersQP;
cerfaFields['quantiteHebdoProduitsLaitiers']            					= $ds052PE3.natureQuantiteProduitsCedes.categoriesProduits.produitslaitiersQC;
cerfaFields['rapport4']       												= $ds052PE3.natureQuantiteProduitsCedes.categoriesProduits.produitslaitiersRapport;

cerfaFields['quantitePreparationbaseOeuf']                					= $ds052PE3.natureQuantiteProduitsCedes.categoriesProduits.preparationsQP;
cerfaFields['quantiteHebdoPreparationbaseOeuf']               				= $ds052PE3.natureQuantiteProduitsCedes.categoriesProduits.preparationsQC;
cerfaFields['rapport5']           											= $ds052PE3.natureQuantiteProduitsCedes.categoriesProduits.preparationsRapport;

cerfaFields['quantiteProduitsTransformes']      							= $ds052PE3.natureQuantiteProduitsCedes.categoriesProduits.produitsNonTransformesQP;
cerfaFields['quantiteHebdoProduitstransformes'] 							= $ds052PE3.natureQuantiteProduitsCedes.categoriesProduits.produitsNonTransformesRapport;
cerfaFields['rapport6']         											= $ds052PE3.natureQuantiteProduitsCedes.categoriesProduits.produitsNonTransformesQC;

cerfaFields['quantiteProduitsNontransformes']         						= $ds052PE3.natureQuantiteProduitsCedes.categoriesProduits.produitsTransformesQP;
cerfaFields['quantiteHebdoProduitNontransformes']        					= $ds052PE3.natureQuantiteProduitsCedes.categoriesProduits.produitsTransformesQC;
cerfaFields['rapport7']     												= $ds052PE3.natureQuantiteProduitsCedes.categoriesProduits.produitsTransformesRapport;

cerfaFields['quantiteEscargots']											= $ds052PE3.natureQuantiteProduitsCedes.categoriesProduits.quantiteEscargots
cerfaFields['quantiteHebdoEscargots'] 										= $ds052PE3.natureQuantiteProduitsCedes.categoriesProduits.quantiteHebdoEscargots
cerfaFields['rapport8'] 													= $ds052PE3.natureQuantiteProduitsCedes.categoriesProduits.escargotRapport

cerfaFields['quantiteRepasPreparation']										= $ds052PE3.natureQuantiteProduitsCedes.categoriesProduits.quantiteRepasPreparation
cerfaFields['quantiteHebdoRepasPreparation']								= $ds052PE3.natureQuantiteProduitsCedes.categoriesProduits.quantiteHebdoRepasPreparation
cerfaFields['rapport9']														= $ds052PE3.natureQuantiteProduitsCedes.categoriesProduits.repasPreparationRapport


//Liste des établissements de commerce déstinataire livrés régulièrement

cerfaFields['etablissement0']                     = '';
cerfaFields['etablissement1']                     = '';
cerfaFields['etablissement2']                     = '';
cerfaFields['etablissement3']                     = '';
cerfaFields['etablissement4']                     = '';
cerfaFields['etablissement5']                     = '';
cerfaFields['etablissement6']                     = '';
cerfaFields['etablissement7']                     = '';
cerfaFields['etablissement8']                     = '';
cerfaFields['etablissement9']                     = '';
cerfaFields['etablissement10']                    = '';
cerfaFields['etablissement11']                    = '';
cerfaFields['adresse0']                           = '';
cerfaFields['adresse1']                           = '';
cerfaFields['adresse2']                           = '';
cerfaFields['adresse3']                           = '';
cerfaFields['adresse4']                           = '';
cerfaFields['adresse5']                           = '';
cerfaFields['adresse6']                           = '';
cerfaFields['adresse7']                           = '';
cerfaFields['adresse8']                           = '';
cerfaFields['adresse9']                           = '';
cerfaFields['adresse10']                          = '';
cerfaFields['adresse11']                          = '';
cerfaFields['distance0']                          = '';
cerfaFields['distance1']                          = '';
cerfaFields['distance2']                          = '';
cerfaFields['distance3']                          = '';
cerfaFields['distance4']                          = '';
cerfaFields['distance5']                          = '';
cerfaFields['distance6']                          = '';
cerfaFields['distance7']                          = '';
cerfaFields['distance8']                          = '';
cerfaFields['distance9']                          = '';
cerfaFields['distance10']                         = '';
cerfaFields['distance11']                         = '';
cerfaFields['categorie0']                         = '';
cerfaFields['categorie1']                         = '';
cerfaFields['categorie2']                         = '';
cerfaFields['categorie3']                         = '';
cerfaFields['categorie4']                         = '';
cerfaFields['categorie5']                         = '';
cerfaFields['categorie6']                         = '';
cerfaFields['categorie7']                         = '';
cerfaFields['categorie8']                         = '';
cerfaFields['categorie9']                         = '';
cerfaFields['categorie10']                        = '';
cerfaFields['categorie11']                        = '';


for (var i= 0; i < $ds052PE3.listeEtablissementLivres.listeEtablissementCommerceLivres.size(); i++ ){
	cerfaFields['etablissement'+i]             = $ds052PE3.listeEtablissementLivres.listeEtablissementCommerceLivres[i].nomEtablissement != null ? $ds052PE3.listeEtablissementLivres.listeEtablissementCommerceLivres[i].nomEtablissement : '' ;
	cerfaFields['adresse'+i]				   = $ds052PE3.listeEtablissementLivres.listeEtablissementCommerceLivres[i].numeroLibelleAdresseEtablissement 
												+ ($ds052PE3.listeEtablissementLivres.listeEtablissementCommerceLivres[i].complementAdresseEtablissement != null ? ' '+$ds052PE3.listeEtablissementLivres.listeEtablissementCommerceLivres[i].complementAdresseEtablissement: '')
												+ ($ds052PE3.listeEtablissementLivres.listeEtablissementCommerceLivres[i].codePostalAdresseEtablissement != null ? ' '+$ds052PE3.listeEtablissementLivres.listeEtablissementCommerceLivres[i].codePostalAdresseEtablissement: '')
												+ ($ds052PE3.listeEtablissementLivres.listeEtablissementCommerceLivres[i].villeAdresseEtablissement != null ? ' '+$ds052PE3.listeEtablissementLivres.listeEtablissementCommerceLivres[i].villeAdresseEtablissement: '')
	cerfaFields['distance'+i]					   = $ds052PE3.listeEtablissementLivres.listeEtablissementCommerceLivres[i].distanceEtablissement != null ? $ds052PE3.listeEtablissementLivres.listeEtablissementCommerceLivres[i].distanceEtablissement : '' ;
	cerfaFields['categorie'+i]				   = $ds052PE3.listeEtablissementLivres.listeEtablissementCommerceLivres[i].categoriesProduitsLivres != null ? $ds052PE3.listeEtablissementLivres.listeEtablissementCommerceLivres[i].categoriesProduitsLivres : '' ;
}

cerfaFields['declarationHonneur']              = $ds052PE3.cadre1NatureActivite.identificationEtablissement.exploitantEtablissement.etablissementExploitantNom + ' ' + $ds052PE3.cadre1NatureActivite.identificationEtablissement.exploitantEtablissement.etablissementExploitantPrenom;
cerfaFields['signatureCoche']                  = $ds052PE3.signature.cadre4SignatureEngagement.signatureDeclarant;
//cerfaFields['dateSignature']                                                          = $ds052PE3.cadre4.cadre4SignatureEngagement.dateSignature;

if($ds052PE3.signature.cadre4SignatureEngagement.dateSignature != null) {
var dateTemp = new Date(parseInt ($ds052PE3.signature.cadre4SignatureEngagement.dateSignature.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
	cerfaFields['jourSignature'] = day;
	cerfaFields['moisSignature'] = month;
	cerfaFields['anneeSignature'] = year;
}

/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
 var finalDoc = nash.doc 
	.load('models/Courrier au premier dossier v2.1 GE.pdf')
	.apply({
		dateSignature: $ds052PE3.signature.cadre4SignatureEngagement.dateSignature,
		autoriteHabilitee :" ",
		demandeContexte : "Demande de derogation à l’obligation d’agrément sanitaire",
		civiliteNomPrenom : civNomPrenom
	});
	
var cerfaDoc = nash.doc
    .load('models/Cerfa n°13982_05.pdf')
    .apply(cerfaFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);



var finalDocItem = finalDoc.save('Cerfa n°13982_05.pdf');


return spec.create({
    id : 'review',
   label : 'Demande de dérogation à l’obligation d’agrément sanitaire.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de dérogation à l’obligation d’agrément sanitaire.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});