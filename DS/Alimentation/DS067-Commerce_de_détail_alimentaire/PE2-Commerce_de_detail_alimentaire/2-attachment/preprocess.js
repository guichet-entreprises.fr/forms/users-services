attachment('pjID', 'pjID');
attachment('pjPlanIndicatif', 'pjPlanIndicatif');
attachment('pjLocalisation', 'pjLocalisation');
attachment('pjCartePlan', 'pjCartePlan');
attachment('pjDesserte', 'pjDesserte');
attachment('pjVoies', 'pjVoies');
attachment('pjEnvironnement', 'pjEnvironnement');
attachment('pjChalandise', 'pjChalandise');
attachment('pjPresentation', 'pjPresentation');


// pj conditions

var pj=$ds067PE2.autresRenseignements.autresRenseignements.projetZoneCommercial;
if(Value('id').of(pj).contains('ouiZoneEnsembleCommercial')) {
    attachment('pjZoneCommerciale', 'pjZoneCommerciale', { mandatory:"true"});
}

/*
 *Extension si extension ou changement
 */
var pj1=$ds067PE2.descriptionProjet.surfaceDeVente.projetExtension;


var pj2=$ds067PE2.descriptionProjet.projetChangement.projetChangementSecteur;
if(Value('id').of(pj2).contains('ouiChangement') || Value('id').of(pj1).contains('ouiExtension')) {
	attachment('pjExtension', 'pjExtension', { mandatory:"true"});
}
