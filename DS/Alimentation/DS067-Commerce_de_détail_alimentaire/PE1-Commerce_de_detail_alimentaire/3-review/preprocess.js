var cerfaFields = {};
var formFields = {};
var moralFields = {};
var annexeFields = {};
var ficheFields = {};
//etatCivil

var civNomPrenom = ($ds067PE1.etatCivil.identificationDeclarant.civilite != null ? $ds067PE1.etatCivil.identificationDeclarant.civilite :'') + ' ' + $ds067PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $ds067PE1.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['civNomPrenom']          	        = $ds067PE1.etatCivil.identificationDeclarant.civilite + ' ' + $ds067PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $ds067PE1.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			 	= $ds067PE1.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $ds067PE1.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                		= $ds067PE1.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissanceDeclarant']           = $ds067PE1.etatCivil.identificationDeclarant.dateNaissanceDeclarant;
cerfaFields['nomDeclarant']          		    = $ds067PE1.etatCivil.identificationDeclarant.nomDeclarant;
cerfaFields['prenomDeclarant']          		= $ds067PE1.etatCivil.identificationDeclarant.prenomDeclarant;
//adresse personnelle
cerfaFields['adresse'] 							= $ds067PE1.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($ds067PE1.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $ds067PE1.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       					= ($ds067PE1.adresse.adresseContact.codePostalAdresseDeclarant != null ? $ds067PE1.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $ds067PE1.adresse.adresseContact.villeAdresseDeclarant + ', ' + $ds067PE1.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      			= $ds067PE1.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          			= $ds067PE1.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courrielDeclarant']          		= $ds067PE1.adresse.adresseContact.courrielDeclarant;
//Adresse Etablissement
cerfaFields['adresseEtablissement1'] 			= $ds067PE1.lieuExercice.lieuExercice.numeroLibelleAdresseEtablissement1 
												+ ' ' + ($ds067PE1.lieuExercice.lieuExercice.complementAdresseEtablissement1 != null ? ', ' + $ds067PE1.lieuExercice.lieuExercice.complementAdresseEtablissement1 : ' ')
												+ ' ' + ($ds067PE1.lieuExercice.lieuExercice.codePostalAdresseEtablissement1 != null ? $ds067PE1.lieuExercice.lieuExercice.codePostalAdresseEtablissement1 + ' ' : '') 
												+' ' + $ds067PE1.lieuExercice.lieuExercice.villeAdresseEtablissement1 
												+ ', ' + $ds067PE1.lieuExercice.lieuExercice.paysAdresseEtablissement1;
cerfaFields['adresseEtablissement2'] 			= ($ds067PE1.lieuExercice.lieuExercice.numeroLibelleAdresseEtablissement2 != null ? $ds067PE1.lieuExercice.lieuExercice.numeroLibelleAdresseEtablissement2 :'')
												+ ' ' + ($ds067PE1.lieuExercice.lieuExercice.complementAdresseEtablissement2 != null ? $ds067PE1.lieuExercice.lieuExercice.complementAdresseEtablissement2 :'')
												+ ' ' + ($ds067PE1.lieuExercice.lieuExercice.codePostalAdresseEtablissement2 != null ? $ds067PE1.lieuExercice.lieuExercice.codePostalAdresseEtablissement2  : '') 
												+' ' + ($ds067PE1.lieuExercice.lieuExercice.villeAdresseEtablissement2 != null ? $ds067PE1.lieuExercice.lieuExercice.villeAdresseEtablissement2 :'')
												+ ' ' + ($ds067PE1.lieuExercice.lieuExercice.paysAdresseEtablissement2 != null ? $ds067PE1.lieuExercice.lieuExercice.paysAdresseEtablissement2 :'');
cerfaFields['exerciceActivite']          		= $ds067PE1.infoExercice.infoExercice.exerciceActivite;

//signature
cerfaFields['date']                				= $ds067PE1.signature.signature.dateSignature;
cerfaFields['signature']           				= $ds067PE1.signature.signature.signature;
cerfaFields['lieuSignature']                  	= $ds067PE1.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenom']		        = ($ds067PE1.etatCivil.identificationDeclarant.civilite != null ? $ds067PE1.etatCivil.identificationDeclarant.civilite :'') + ' ' + $ds067PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $ds067PE1.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['activiteExercee']                	= 'Commerce de détail alimentaire';

// Formulaire personne physique
formFields['nomDeclarant']                      = $ds067PE1.etatCivil.identificationDeclarant.nomDeclarant;
formFields['prenomDeclarant']                   = $ds067PE1.etatCivil.identificationDeclarant.prenomDeclarant;
formFields['adresseDeclarant']                  = $ds067PE1.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($ds067PE1.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $ds067PE1.adresse.adresseContact.complementAdresseDeclarant : ' ');
formFields['telephoneDeclarant']      			= $ds067PE1.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
formFields['telecopieurDeclarant']      	    = $ds067PE1.adresse.adresseContact.telecopieurDeclarant;
formFields['emailDeclarant']          		    = $ds067PE1.adresse.adresseContact.courrielDeclarant;

	//Autres demandeurs
formFields['nomDemandeur2']                     = $ds067PE1.autresDemandeurs.autresDemandeurs.nomDemandeur2;
formFields['prenomDemandeur2']                  = $ds067PE1.autresDemandeurs.autresDemandeurs.prenomDemandeur2;
formFields['adresseDemandeur2']                 = ($ds067PE1.autresDemandeurs.autresDemandeurs.numeroLibelleAdresseDemandeur2 != null ? $ds067PE1.autresDemandeurs.autresDemandeurs.numeroLibelleAdresseDemandeur2 : '')  
													+ ' '+ ($ds067PE1.adresse.adresseContact.complementAdresseDemandeur2 != null ? ', ' + $ds067PE1.adresse.adresseContact.complementAdresseDemandeur2 : ' ') 
													+ ' ' + ($ds067PE1.autresDemandeurs.autresDemandeurs.codePostalAdresseDemandeur2 != null ? $ds067PE1.autresDemandeurs.autresDemandeurs.codePostalAdresseDemandeur2 + ' ' : '') 
													+ ' '+ ($ds067PE1.autresDemandeurs.autresDemandeurs.villeAdresseDemandeur2 != null ? $ds067PE1.autresDemandeurs.autresDemandeurs.villeAdresseDemandeur2 : '' );
formFields['telephoneDemandeur2']      			= $ds067PE1.autresDemandeurs.autresDemandeurs.telephoneDemandeur2; 
formFields['telecopieurDemandeur2']      	    = $ds067PE1.autresDemandeurs.autresDemandeurs.telecopieurDemandeur2;
formFields['emailDemandeur2']          			= $ds067PE1.autresDemandeurs.autresDemandeurs.emailDemandeur2;
formFields['nomDemandeur3']                     = $ds067PE1.autresDemandeurs.autresDemandeurs.nomDemandeur3;
formFields['prenomDemandeur3']                  = $ds067PE1.autresDemandeurs.autresDemandeurs.prenomDemandeur3;
formFields['adresseDemandeur3']                 = ($ds067PE1.autresDemandeurs.autresDemandeurs.numeroLibelleAdresseDemandeur3 != null ? $ds067PE1.autresDemandeurs.autresDemandeurs.numeroLibelleAdresseDemandeur3 : '')  
													+ ' '+ ($ds067PE1.adresse.adresseContact.complementAdresseDemandeur3 != null ? ', ' + $ds067PE1.adresse.adresseContact.complementAdresseDemandeur3 : ' ') 
													+ ' ' + ($ds067PE1.autresDemandeurs.autresDemandeurs.codePostalAdresseDemandeur3 != null ? $ds067PE1.autresDemandeurs.autresDemandeurs.codePostalAdresseDemandeur3 + ' ' : '') 
													+ ' '+ ($ds067PE1.autresDemandeurs.autresDemandeurs.villeAdresseDemandeur3 != null ? $ds067PE1.autresDemandeurs.autresDemandeurs.villeAdresseDemandeur3 : '' );
formFields['telephoneDemandeur3']      			= $ds067PE1.autresDemandeurs.autresDemandeurs.telephoneDemandeur3; 
formFields['telecopieurDemandeur3']      	    = $ds067PE1.autresDemandeurs.autresDemandeurs.telecopieurDemandeur3;
formFields['emailDemandeur3']          			= $ds067PE1.autresDemandeurs.autresDemandeurs.emailDemandeur3;
											+ ', ' + $ds067PE1.lieuExercice.lieuExercice.paysAdresseProjet;                   
	//Qualité
formFields['exploitantFutur']                  = Value('id').of($ds067PE1.qualiteDemandeur.qualiteDemandeur.qualite).eq("exploitantFutur") ? true : false;
formFields['proprietaireFutur']                = Value('id').of($ds067PE1.qualiteDemandeur.qualiteDemandeur.qualite).eq("proprietaireFutur") ? true : false;
formFields['promoteur']                        = Value('id').of($ds067PE1.qualiteDemandeur.qualiteDemandeur.qualite).eq("promoteur") ? true : false;

	//présentation Projet
formFields['adresseProjet']                     = $ds067PE1.presentationProjet.presentationProjet.numeroLibelleAdresseProjet 
												+ ' ' + ($ds067PE1.presentationProjet.presentationProjet.complementAdresseProjet != null ? ', ' + $ds067PE1.presentationProjet.presentationProjet.complementAdresseProjet : ' ')
												+ ' ' + ($ds067PE1.presentationProjet.presentationProjet.codePostalAdresseProjet != null ? $ds067PE1.presentationProjet.presentationProjet.codePostalAdresseProjet + ' ' : '')
												+' ' + $ds067PE1.presentationProjet.presentationProjet.villeAdresseProjet;
	//Description
formFields['surfaceVente']	                    = $ds067PE1.descriptionProjet.descriptionProjet.surfaceVente != null ? $ds067PE1.descriptionProjet.descriptionProjet.surfaceVente : '';
formFields['surfaceVenteSecteurActivite']	    = $ds067PE1.descriptionProjet.descriptionProjet.surfaceVenteSecteurActivite != null ? $ds067PE1.descriptionProjet.descriptionProjet.surfaceVenteSecteurActivite : '';
formFields['nombreMagasinSurfaceVente']	        = $ds067PE1.descriptionProjet.descriptionProjet.nombreMagasinSurfaceVente != null ? $ds067PE1.descriptionProjet.descriptionProjet.nombreMagasinSurfaceVente : '';
formFields['descriptionActiviteAnnexe']	        = $ds067PE1.descriptionProjet.descriptionProjet.descriptionActiviteAnnexe != null ? $ds067PE1.descriptionProjet.descriptionProjet.descriptionActiviteAnnexe : '';
formFields['surfaceVenteSecteurActivite2']	    = $ds067PE1.descriptionProjet.surfaceDeVente.surfaceVenteSecteurActivite2 != null ? $ds067PE1.descriptionProjet.surfaceDeVente.surfaceVenteSecteurActivite2 : '';
formFields['rappelSurfaceExistante']	        = $ds067PE1.descriptionProjet.surfaceDeVente.rappelSurfaceExistante != null ? $ds067PE1.descriptionProjet.surfaceDeVente.rappelSurfaceExistante : '';
formFields['rappelSurfacedemandee']	            = $ds067PE1.descriptionProjet.surfaceDeVente.rappelSurfacedemandee != null ? $ds067PE1.descriptionProjet.surfaceDeVente.rappelSurfacedemandee : '';
formFields['surfaceEnvisagee']	                = $ds067PE1.descriptionProjet.surfaceDeVente.surfaceEnvisagee != null ? $ds067PE1.descriptionProjet.surfaceDeVente.surfaceEnvisagee : '';
formFields['dateOuverture']	                    = $ds067PE1.descriptionProjet.surfaceDeVente.dateOuverture != null ? $ds067PE1.descriptionProjet.surfaceDeVente.dateOuverture : '';
formFields['surfaceVenteInitiale']	            = $ds067PE1.descriptionProjet.surfaceDeVente.surfaceVenteInitiale != null ? $ds067PE1.descriptionProjet.surfaceDeVente.surfaceVenteInitiale : '';
formFields['indicationExtension']	            = $ds067PE1.descriptionProjet.surfaceDeVente.indicationExtension != null ? $ds067PE1.descriptionProjet.surfaceDeVente.indicationExtension : '';

formFields['surfaceVenteMagasinSecteur']	    = $ds067PE1.descriptionProjet.projetChangement.surfaceVenteMagasinSecteur != null ? $ds067PE1.descriptionProjet.projetChangement.surfaceVenteMagasinSecteur : '';
formFields['surfaceVenteSecteur2']	            = $ds067PE1.descriptionProjet.projetChangement.surfaceVenteSecteur2 != null ? $ds067PE1.descriptionProjet.projetChangement.surfaceVenteSecteur2 : '';
formFields['descriptionProjetAutorise']	        = $ds067PE1.descriptionProjet.projetModifProjet.descriptionProjetAutorise != null ? $ds067PE1.descriptionProjet.projetModifProjet.descriptionProjetAutorise : '';
formFields['modifEnvisagees']	                = $ds067PE1.descriptionProjet.projetModifProjet.modifEnvisagees != null ? $ds067PE1.descriptionProjet.projetModifProjet.modifEnvisagees : '';
formFields['projetApresModif']	                = $ds067PE1.descriptionProjet.projetModifProjet.projetApresModif != null ? $ds067PE1.descriptionProjet.projetModifProjet.projetApresModif : '';
formFields['listeMagasin']	                    = $ds067PE1.autresRenseignements.autresRenseignements.listeMagasin != null ? $ds067PE1.autresRenseignements.autresRenseignements.listeMagasin : '';
formFields['mentionEnseigne']	                = $ds067PE1.autresRenseignements.autresRenseignements.mentionEnseigne != null ? $ds067PE1.autresRenseignements.autresRenseignements.mentionEnseigne : '';
formFields['parcStationnement']	                = $ds067PE1.autresRenseignements.autresRenseignements.parcStationnement != null ? $ds067PE1.autresRenseignements.autresRenseignements.parcStationnement : '';
formFields['activiteAnnexe']	                = $ds067PE1.autresRenseignements.autresRenseignements.activiteAnnexe != null ? $ds067PE1.autresRenseignements.autresRenseignements.activiteAnnexe : '';

//Condition réalisation projet
formFields['indicationParcelle']	            = $ds067PE1.conditionRealisationProjet.conditionRealisationProjet.indicationParcelle != null ? $ds067PE1.conditionRealisationProjet.conditionRealisationProjet.indicationParcelle : '';
formFields['identificationLocal']	            = $ds067PE1.conditionRealisationProjet.conditionRealisationProjet.identificationLocal != null ? $ds067PE1.conditionRealisationProjet.conditionRealisationProjet.identificationLocal : '';
	


	
// Formulaire personne morale
moralFields['raisonSociale']                     = $ds067PE1.etatCivil.identificationDeclarant.raisonSociale;
moralFields['formeJuridique']                    = $ds067PE1.etatCivil.identificationDeclarant.formeJuridique;
moralFields['adresseDeclarant']                  = $ds067PE1.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($ds067PE1.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $ds067PE1.adresse.adresseContact.complementAdresseDeclarant : ' ');
moralFields['telephoneDeclarant']      			= $ds067PE1.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
moralFields['telecopieurDeclarant']      	    = $ds067PE1.adresse.adresseContact.telecopieurDeclarant;
moralFields['courriel']          		        = $ds067PE1.adresse.adresseContact.courrielDeclarant;

moralFields['nomDemandeur2']                  = $ds067PE1.autresDemandeurs.autresDemandeurs.nomDemandeur2;
moralFields['prenomDemandeur2']                  = $ds067PE1.autresDemandeurs.autresDemandeurs.prenomDemandeur2;
moralFields['adresseDemandeur2']                 = ($ds067PE1.autresDemandeurs.autresDemandeurs.numeroLibelleAdresseDemandeur2 != null ? $ds067PE1.autresDemandeurs.autresDemandeurs.numeroLibelleAdresseDemandeur2 : '')  
													+ ' '+ ($ds067PE1.adresse.adresseContact.complementAdresseDemandeur2 != null ? ', ' + $ds067PE1.adresse.adresseContact.complementAdresseDemandeur2 : ' ') 
													+ ' ' + ($ds067PE1.autresDemandeurs.autresDemandeurs.codePostalAdresseDemandeur2 != null ? $ds067PE1.autresDemandeurs.autresDemandeurs.codePostalAdresseDemandeur2 + ' ' : '') 
													+ ' '+ ($ds067PE1.autresDemandeurs.autresDemandeurs.villeAdresseDemandeur2 != null ? $ds067PE1.autresDemandeurs.autresDemandeurs.villeAdresseDemandeur2 : '' );
moralFields['telephoneDemandeur2']      		 = $ds067PE1.autresDemandeurs.autresDemandeurs.telephoneDemandeur2; 
moralFields['telecopieurDemandeur2']      	     = $ds067PE1.autresDemandeurs.autresDemandeurs.telecopieurDemandeur2;
moralFields['emailDemandeur2']          		 = $ds067PE1.autresDemandeurs.autresDemandeurs.emailDemandeur2;

moralFields['nomDemandeur3']                     = $ds067PE1.autresDemandeurs.autresDemandeurs.nomDemandeur3;
moralFields['prenomDemandeur3']                  = $ds067PE1.autresDemandeurs.autresDemandeurs.prenomDemandeur3;
moralFields['adresseDemandeur3']                 = ($ds067PE1.autresDemandeurs.autresDemandeurs.numeroLibelleAdresseDemandeur3 != null ? $ds067PE1.autresDemandeurs.autresDemandeurs.numeroLibelleAdresseDemandeur3 : '')  
													+ ' '+ ($ds067PE1.adresse.adresseContact.complementAdresseDemandeur3 != null ? ', ' + $ds067PE1.adresse.adresseContact.complementAdresseDemandeur3 : ' ') 
													+ ' ' + ($ds067PE1.autresDemandeurs.autresDemandeurs.codePostalAdresseDemandeur3 != null ? $ds067PE1.autresDemandeurs.autresDemandeurs.codePostalAdresseDemandeur3 + ' ' : '') 
													+ ' '+ ($ds067PE1.autresDemandeurs.autresDemandeurs.villeAdresseDemandeur3 != null ? $ds067PE1.autresDemandeurs.autresDemandeurs.villeAdresseDemandeur3 : '' );
moralFields['telephoneDemandeur3']      		= $ds067PE1.autresDemandeurs.autresDemandeurs.telephoneDemandeur3; 
moralFields['telecopieurDemandeur3']      	    = $ds067PE1.autresDemandeurs.autresDemandeurs.telecopieurDemandeur3;
moralFields['emailDemandeur3']          		= $ds067PE1.autresDemandeurs.autresDemandeurs.emailDemandeur3;
	//Qualité
moralFields['exploitantFutur']                  = Value('id').of($ds067PE1.qualiteDemandeur.qualiteDemandeur.qualite).eq("exploitantFutur") ? true : false;
moralFields['proprietaireFutur']                = Value('id').of($ds067PE1.qualiteDemandeur.qualiteDemandeur.qualite).eq("proprietaireFutur") ? true : false;
moralFields['promoteur']                        = Value('id').of($ds067PE1.qualiteDemandeur.qualiteDemandeur.qualite).eq("promoteur") ? true : false;

	//présentation Projet
moralFields['adresseProjet']                     = $ds067PE1.presentationProjet.presentationProjet.numeroLibelleAdresseProjet 
												+ ' ' + ($ds067PE1.presentationProjet.presentationProjet.complementAdresseProjet != null ? ', ' + $ds067PE1.presentationProjet.presentationProjet.complementAdresseProjet : ' ')
												+ ' ' + ($ds067PE1.presentationProjet.presentationProjet.codePostalAdresseProjet != null ? $ds067PE1.presentationProjet.presentationProjet.codePostalAdresseProjet + ' ' : '') 
												+' ' + $ds067PE1.presentationProjet.presentationProjet.villeAdresseProjet 
												+ ', ' + $ds067PE1.presentationProjet.presentationProjet.paysAdresseProjet;

	//Description
moralFields['surfaceVente']	                    = $ds067PE1.descriptionProjet.descriptionProjet.surfaceVente != null ? $ds067PE1.descriptionProjet.descriptionProjet.surfaceVente : '';
moralFields['surfaceVenteSecteurActivite']	    = $ds067PE1.descriptionProjet.descriptionProjet.surfaceVenteSecteurActivite != null ? $ds067PE1.descriptionProjet.descriptionProjet.surfaceVenteSecteurActivite : '';
moralFields['nombreMagasinSurfaceVente']	    = $ds067PE1.descriptionProjet.descriptionProjet.nombreMagasinSurfaceVente != null ? $ds067PE1.descriptionProjet.descriptionProjet.nombreMagasinSurfaceVente : '';
moralFields['descriptionActiviteAnnexe']	    = $ds067PE1.descriptionProjet.descriptionProjet.descriptionActiviteAnnexe != null ? $ds067PE1.descriptionProjet.descriptionProjet.descriptionActiviteAnnexe : '';
moralFields['surfaceVenteSecteurActivite2']	    = $ds067PE1.descriptionProjet.surfaceDeVente.surfaceVenteSecteurActivite2 != null ? $ds067PE1.descriptionProjet.surfaceDeVente.surfaceVenteSecteurActivite2 : '';
moralFields['rappelSurfaceExistante']	        = $ds067PE1.descriptionProjet.surfaceDeVente.rappelSurfaceExistante != null ? $ds067PE1.descriptionProjet.surfaceDeVente.rappelSurfaceExistante : '';
moralFields['rappelSurfacedemandee']	        = $ds067PE1.descriptionProjet.surfaceDeVente.rappelSurfacedemandee != null ? $ds067PE1.descriptionProjet.surfaceDeVente.rappelSurfacedemandee : '';
moralFields['surfaceEnvisagee']	                = $ds067PE1.descriptionProjet.surfaceDeVente.surfaceEnvisagee != null ? $ds067PE1.descriptionProjet.surfaceDeVente.surfaceEnvisagee : '';
moralFields['dateOuverture']	                = $ds067PE1.descriptionProjet.surfaceDeVente.dateOuverture != null ? $ds067PE1.descriptionProjet.surfaceDeVente.dateOuverture : '';
moralFields['surfaceVenteInitiale']	            = $ds067PE1.descriptionProjet.surfaceDeVente.surfaceVenteInitiale != null ? $ds067PE1.descriptionProjet.surfaceDeVente.surfaceVenteInitiale : '';
moralFields['indicationExtension']	            = $ds067PE1.descriptionProjet.surfaceDeVente.indicationExtension != null ? $ds067PE1.descriptionProjet.surfaceDeVente.indicationExtension : '';

moralFields['surfaceVenteMagasinSecteur']	    = $ds067PE1.descriptionProjet.projetChangement.surfaceVenteMagasinSecteur != null ? $ds067PE1.descriptionProjet.projetChangement.surfaceVenteMagasinSecteur : '';
moralFields['surfaceVenteSecteur2']	            = $ds067PE1.descriptionProjet.projetChangement.surfaceVenteSecteur2 != null ? $ds067PE1.descriptionProjet.projetChangement.surfaceVenteSecteur2 : '';
moralFields['descriptionProjetAutorise']	    = $ds067PE1.descriptionProjet.projetModifProjet.descriptionProjetAutorise != null ? $ds067PE1.descriptionProjet.projetModifProjet.descriptionProjetAutorise : '';
moralFields['modifEnvisagees']	                = $ds067PE1.descriptionProjet.projetModifProjet.modifEnvisagees != null ? $ds067PE1.descriptionProjet.projetModifProjet.modifEnvisagees : '';
moralFields['projetApresModif']	                = $ds067PE1.descriptionProjet.projetModifProjet.projetApresModif != null ? $ds067PE1.descriptionProjet.projetModifProjet.projetApresModif : '';

moralFields['listeMagasin']	                    = $ds067PE1.autresRenseignements.autresRenseignements.listeMagasin != null ? $ds067PE1.autresRenseignements.autresRenseignements.listeMagasin : '';
moralFields['mentionEnseigne']	                = $ds067PE1.autresRenseignements.autresRenseignements.mentionEnseigne != null ? $ds067PE1.autresRenseignements.autresRenseignements.mentionEnseigne : '';
moralFields['parcStationnement']	            = $ds067PE1.autresRenseignements.autresRenseignements.parcStationnement != null ? $ds067PE1.autresRenseignements.autresRenseignements.parcStationnement : '';
moralFields['activiteAnnexe']	                = $ds067PE1.autresRenseignements.autresRenseignements.activiteAnnexe != null ? $ds067PE1.autresRenseignements.autresRenseignements.activiteAnnexe : '';

//Condition réalisation projet
moralFields['indicationParcelle']	            = $ds067PE1.conditionRealisationProjet.conditionRealisationProjet.indicationParcelle != null ? $ds067PE1.conditionRealisationProjet.conditionRealisationProjet.indicationParcelle : '';
moralFields['identificationLocal']	            = $ds067PE1.conditionRealisationProjet.conditionRealisationProjet.identificationLocal != null ? $ds067PE1.conditionRealisationProjet.conditionRealisationProjet.identificationLocal : '';

//Annexe2
annexeFields['populationRecensement']           = $ds067PE1.zoneChalandise.zoneChalandise.populationRecensement;
annexeFields['populationLegale']                = $ds067PE1.zoneChalandise.zoneChalandise.populationLegale;
annexeFields['populationAuthentifiee']          = $ds067PE1.zoneChalandise.zoneChalandise.populationAuthentifiee;
annexeFields['tauxEvolution']                   = $ds067PE1.zoneChalandise.zoneChalandise.tauxEvolution;
annexeFields['listeExhaustive1']                = $ds067PE1.listeExhaustive.listeExhaustive.listeExhaustive1;
annexeFields['localisationActivite']            = $ds067PE1.presentationEffet.presentationEffet.localisationActivite;
annexeFields['fluxDeplacements']                = $ds067PE1.presentationEffet.presentationEffet.fluxDeplacements;
annexeFields['projetAmenagement']               = $ds067PE1.presentationEffet.presentationEffet.projetAmenagement;

annexeFields['reductionPollution']               = $ds067PE1.presentationEffet2.presentationEffet2.reductionPollution;
annexeFields['solutionVegetale']                 = $ds067PE1.presentationEffet2.presentationEffet2.solutionVegetale;
annexeFields['inscriptionHarmonieuse']           = $ds067PE1.presentationEffet2.presentationEffet2.inscriptionHarmonieuse;
annexeFields['traitementFiche']                  = $ds067PE1.presentationEffet2.presentationEffet2.traitementFiche;
annexeFields['siteNatura']                       = $ds067PE1.presentationEffet2.presentationEffet2.siteNatura;
annexeFields['protectionParticuliere']           = $ds067PE1.presentationEffet2.presentationEffet2.protectionParticuliere;
annexeFields['inscriptionHarmonieuse']           = $ds067PE1.presentationEffet2.presentationEffet2.inscriptionHarmonieuse;
annexeFields['situationRisque']                  = $ds067PE1.presentationEffet2.presentationEffet2.situationRisque;

//Annexe3
ficheFields['adresseProjet']                     =$ds067PE1.presentationProjet.presentationProjet.numeroLibelleAdresseProjet 
												+ ' ' + ($ds067PE1.presentationProjet.presentationProjet.complementAdresseProjet != null ? ', ' + $ds067PE1.presentationProjet.presentationProjet.complementAdresseProjet : ' ')
												+ ' ' + ($ds067PE1.presentationProjet.presentationProjet.codePostalAdresseProjet != null ? $ds067PE1.presentationProjet.presentationProjet.codePostalAdresseProjet + ' ' : '') 
												+' ' + $ds067PE1.presentationProjet.presentationProjet.villeAdresseProjet 
												+ ', ' + $ds067PE1.presentationProjet.presentationProjet.paysAdresseProjet;
ficheFields['categorieProjet']                   =$ds067PE1.identificationProjet.identificationProjet.categorieProjet;
ficheFields['descriptionProjet1']                =$ds067PE1.identificationProjet.identificationProjet.descriptionProjet1;
ficheFields['dateCreation']                      =$ds067PE1.identificationProjet.identificationProjet.dateCreation;
ficheFields['commerceAlimentaire']               =$ds067PE1.identificationProjet.identificationProjet.commerceAlimentaire;
ficheFields['autresCommerces']                   =$ds067PE1.identificationProjet.identificationProjet.autresCommerces;
ficheFields['commerceAlimentaire2']              =$ds067PE1.identificationProjet.identificationProjet.commerceAlimentaire;
ficheFields['autresCommerces2']                  =$ds067PE1.identificationProjet.identificationProjet.autresCommerces;

ficheFields['decisionAvis']                      =$ds067PE1.historiqueProjet.historiqueProjet.decisionAvis;
ficheFields['maitriseFonciere']                  =$ds067PE1.historiqueProjet.historiqueProjet.maitriseFonciere;

//identite
ficheFields['nomDeclarant']                      = $ds067PE1.etatCivil.identificationDeclarant.nomDeclarant;
ficheFields['prenomDeclarant']                   = $ds067PE1.etatCivil.identificationDeclarant.prenomDeclarant;
ficheFields['adresseDeclarant']                  = $ds067PE1.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($ds067PE1.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $ds067PE1.adresse.adresseContact.complementAdresseDeclarant : ' ');
ficheFields['telephoneDeclarant']      			 = $ds067PE1.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
ficheFields['telecopieurDeclarant']      	     = $ds067PE1.adresse.adresseContact.telecopieurDeclarant;
ficheFields['emailDeclarant']          		     = $ds067PE1.adresse.adresseContact.courrielDeclarant;

//positionnement général
ficheFields['ouiSecteurAgglomere']               =Value('id').of($ds067PE1.situationProjet1.positionnementGeneral.secteurAgglomere).eq("ouiSecteurAgglomere") ? true : false;
ficheFields['nonSecteurAgglomere']               =Value('id').of($ds067PE1.situationProjet1.positionnementGeneral.secteurAgglomere).eq("nonSecteurAgglomere") ? true : false;
ficheFields['sansObjetSecteurAgglomere']         =Value('id').of($ds067PE1.situationProjet1.positionnementGeneral.secteurAgglomere).eq("sansObjetSecteurAgglomere") ? true : false;
ficheFields['observationSecteurAgglomere']       =$ds067PE1.situationProjet1.positionnementGeneral.observationSecteurAgglomere;
ficheFields['ouiEntreeVille']                    =Value('id').of($ds067PE1.situationProjet1.positionnementGeneral.entreeVille).eq("ouiEntreeVille") ? true : false;
ficheFields['nonEntreeVille']                    =Value('id').of($ds067PE1.situationProjet1.positionnementGeneral.entreeVille).eq("nonEntreeVille") ? true : false;
ficheFields['sansObjetEntreeVille']              =Value('id').of($ds067PE1.situationProjet1.positionnementGeneral.entreeVille).eq("sansObjetEntreeVille") ? true : false;
ficheFields['observationEntreeVille']            =$ds067PE1.situationProjet1.positionnementGeneral.observationEntreeVille;
ficheFields['ouiCentreVille']                    =Value('id').of($ds067PE1.situationProjet1.positionnementGeneral.centreVille).eq("ouiCentreVille") ? true : false;
ficheFields['nonCentreVille']                    =Value('id').of($ds067PE1.situationProjet1.positionnementGeneral.centreVille).eq("nonCentreVille") ? true : false;
ficheFields['sansObjetCentreVille']              =Value('id').of($ds067PE1.situationProjet1.positionnementGeneral.centreVille).eq("sansObjetCentreVille") ? true : false;
ficheFields['observationCentreVille']            =$ds067PE1.situationProjet1.positionnementGeneral.observationCentreVille;
ficheFields['ouiQuartierHabitation']             =Value('id').of($ds067PE1.situationProjet1.positionnementGeneral.quartierHabitation).eq("ouiQuartierHabitation") ? true : false;
ficheFields['nonQuartierHabitation']             =Value('id').of($ds067PE1.situationProjet1.positionnementGeneral.quartierHabitation).eq("nonQuartierHabitation") ? true : false;
ficheFields['sansObjetQuartierHabitation']       =Value('id').of($ds067PE1.situationProjet1.positionnementGeneral.quartierHabitation).eq("sansObjetQuartierHabitation") ? true : false;
ficheFields['observationQuartierHabitation']     =$ds067PE1.situationProjet1.positionnementGeneral.observationQuartierHabitation;

//positionnement construction
ficheFields['ouiZoneCommerciale']                        =Value('id').of($ds067PE1.situationProjet2.positionnementConstruction.zoneCommerciale).eq("ouiZoneCommerciale") ? true : false;
ficheFields['nonZoneCommerciale']                        =Value('id').of($ds067PE1.situationProjet2.positionnementConstruction.zoneCommerciale).eq("nonZoneCommerciale") ? true : false;
ficheFields['sansObjetZoneCommerciale']                  =Value('id').of($ds067PE1.situationProjet2.positionnementConstruction.zoneCommerciale).eq("sansObjetZoneCommerciale") ? true : false;
ficheFields['observationZoneCommerciale']                =$ds067PE1.situationProjet2.positionnementConstruction.observationZoneCommerciale;
ficheFields['ouiZoneCommercialeNouvelle']                =Value('id').of($ds067PE1.situationProjet2.positionnementConstruction.zoneCommercialeNouvelle).eq("ouiZoneCommercialeNouvelle") ? true : false;
ficheFields['nonZoneCommercialeNouvelle']                =Value('id').of($ds067PE1.situationProjet2.positionnementConstruction.zoneCommercialeNouvelle).eq("nonZoneCommercialeNouvelle") ? true : false;
ficheFields['sansObjetZoneCommercialeNouvelle']          =Value('id').of($ds067PE1.situationProjet2.positionnementConstruction.zoneCommercialeNouvelle).eq("sansObjetZoneCommercialeNouvelle") ? true : false;
ficheFields['observationZoneCommercialeNouvelle']        =$ds067PE1.situationProjet2.positionnementConstruction.observationZoneCommercialeNouvelle;
ficheFields['ouiZoneCommercialeCreer']                   =Value('id').of($ds067PE1.situationProjet2.positionnementConstruction.zoneCommercialeCreer).eq("ouiZoneCommercialeCreer") ? true : false;
ficheFields['nonZoneCommercialeCreer']                   =Value('id').of($ds067PE1.situationProjet2.positionnementConstruction.zoneCommercialeCreer).eq("nonZoneCommercialeCreer") ? true : false;
ficheFields['sansObjetZoneCommercialeCreer']             =Value('id').of($ds067PE1.situationProjet2.positionnementConstruction.zoneCommercialeCreer).eq("sansObjetZoneCommercialeCreer") ? true : false;
ficheFields['observationZoneCommercialeCreer']           =$ds067PE1.situationProjet2.positionnementConstruction.observationZoneCommercialeCreer;
ficheFields['ouiZoneHabitationExistante']                   =Value('id').of($ds067PE1.situationProjet2.positionnementConstruction.zoneHabitationExistante).eq("ouiZoneHabitationExistante") ? true : false;
ficheFields['nonZoneHabitationExistante']                =Value('id').of($ds067PE1.situationProjet2.positionnementConstruction.zoneHabitationExistante).eq("nonZoneHabitationExistante") ? true : false;
ficheFields['sansObjetZoneHabitationExistante']          =Value('id').of($ds067PE1.situationProjet2.positionnementConstruction.zoneHabitationExistante).eq("sansObjetZoneHabitationExistante") ? true : false;
ficheFields['observationZoneHabitationExistante']        =$ds067PE1.situationProjet2.positionnementConstruction.observationZoneHabitationExistante;
ficheFields['ouiZoneHabitationNouvelle']                =Value('id').of($ds067PE1.situationProjet2.positionnementConstruction.zoneHabitationNouvelle).eq("ouiZoneHabitationNouvelle") ? true : false;
ficheFields['nonZoneHabitationNouvelle']                =Value('id').of($ds067PE1.situationProjet2.positionnementConstruction.zoneHabitationNouvelle).eq("nonZoneHabitationNouvelle") ? true : false;
ficheFields['sansObjetZoneHabitationNouvelle']          =Value('id').of($ds067PE1.situationProjet2.positionnementConstruction.zoneHabitationNouvelle).eq("sansObjetZoneHabitationNouvelle") ? true : false;
ficheFields['observationZoneHabitationNouvelle']        =$ds067PE1.situationProjet2.positionnementConstruction.observationZoneHabitationNouvelle;
ficheFields['ouiZoneHabitationCreer']                  =Value('id').of($ds067PE1.situationProjet2.positionnementConstruction.zoneHabitationCreer).eq("ouiZoneHabitationCreer") ? true : false;
ficheFields['nonZoneHabitationCreer']                  =Value('id').of($ds067PE1.situationProjet2.positionnementConstruction.zoneHabitationCreer).eq("nonZoneHabitationCreer") ? true : false;
ficheFields['sansObjetZoneHabitationCreer']            =Value('id').of($ds067PE1.situationProjet2.positionnementConstruction.zoneHabitationCreer).eq("sansObjetZoneHabitationCreer") ? true : false;
ficheFields['observationZoneHabitationCreer']          =$ds067PE1.situationProjet2.positionnementConstruction.observationZoneHabitationCreer;
ficheFields['ouiZoneUrbaniseeExistante']                =Value('id').of($ds067PE1.situationProjet2.positionnementConstruction.zoneUrbaniseeExistante).eq("ouiZoneUrbaniseeExistante") ? true : false;
ficheFields['nonZoneUrbaniseeExistante']                =Value('id').of($ds067PE1.situationProjet2.positionnementConstruction.zoneUrbaniseeExistante).eq("nonZoneUrbaniseeExistante") ? true : false;
ficheFields['sansObjetZoneUrbaniseeExistante']          =Value('id').of($ds067PE1.situationProjet2.positionnementConstruction.zoneUrbaniseeExistante).eq("sansObjetZoneUrbaniseeExistante") ? true : false;
ficheFields['observationZoneUrbaniseeExistante']        =$ds067PE1.situationProjet2.positionnementConstruction.observationZoneUrbaniseeExistante;
ficheFields['ouiZoneUrbaniseeNouvelle']                =Value('id').of($ds067PE1.situationProjet2.positionnementConstruction.zoneUrbaniseeNouvelle).eq("ouiZoneUrbaniseeNouvelle") ? true : false;
ficheFields['nonZoneUrbaniseeNouvelle']                =Value('id').of($ds067PE1.situationProjet2.positionnementConstruction.zoneUrbaniseeNouvelle).eq("nonZoneUrbaniseeNouvelle") ? true : false;
ficheFields['sansObjetZoneUrbaniseeNouvelle']          =Value('id').of($ds067PE1.situationProjet2.positionnementConstruction.zoneUrbaniseeNouvelle).eq("sansObjetZoneUrbaniseeNouvelle") ? true : false;
ficheFields['observationZoneUrbaniseeNouvelle']        =$ds067PE1.situationProjet2.positionnementConstruction.observationZoneUrbaniseeNouvelle;
ficheFields['ouiZoneUrbaniseeCreer']                   =Value('id').of($ds067PE1.situationProjet2.positionnementConstruction.zoneUrbaniseeCreer).eq("ouiZoneUrbaniseeCreer") ? true : false;
ficheFields['nonZoneUrbaniseeCreer']                   =Value('id').of($ds067PE1.situationProjet2.positionnementConstruction.zoneUrbaniseeCreer).eq("nonZoneUrbaniseeCreer") ? true : false;
ficheFields['sansObjetZoneUrbaniseeCreer']             =Value('id').of($ds067PE1.situationProjet2.positionnementConstruction.zoneUrbaniseeCreer).eq("sansObjetZoneUrbaniseeCreer") ? true : false;
ficheFields['observationZoneUrbaniseeCreer']           =$ds067PE1.situationProjet2.positionnementConstruction.observationZoneUrbaniseeCreer;

//positionnement Disponibilites Foncieres
ficheFields['ouiOffreFonciereCentre']                     =Value('id').of($ds067PE1.situationProjet3.positionnementDisponibilitesFoncieres.offreFonciereCentre).eq("ouiOffreFonciereCentre") ? true : false;
ficheFields['nonOffreFonciereCentre']                     =Value('id').of($ds067PE1.situationProjet3.positionnementDisponibilitesFoncieres.offreFonciereCentre).eq("nonOffreFonciereCentre") ? true : false;
ficheFields['sansObjetOffreFonciereCentre']               =Value('id').of($ds067PE1.situationProjet3.positionnementDisponibilitesFoncieres.offreFonciereCentre).eq("sansObjetOffreFonciereCentre") ? true : false;
ficheFields['observationOffreFonciereCentre']             =$ds067PE1.situationProjet3.positionnementDisponibilitesFoncieres.observationOffreFonciereCentre;
ficheFields['ouiOffreFonciereClientele']                =Value('id').of($ds067PE1.situationProjet3.positionnementDisponibilitesFoncieres.offreFonciereClientele).eq("ouiOffreFonciereClientele") ? true : false;
ficheFields['nonOffreFonciereClientele']                =Value('id').of($ds067PE1.situationProjet3.positionnementDisponibilitesFoncieres.offreFonciereClientele).eq("nonOffreFonciereClientele") ? true : false;
ficheFields['sansObjetOffreFonciereClientele']          =Value('id').of($ds067PE1.situationProjet3.positionnementDisponibilitesFoncieres.offreFonciereClientele).eq("sansObjetOffreFonciereClientele") ? true : false;
ficheFields['observationOffreFonciereClientele']        =$ds067PE1.situationProjet3.positionnementDisponibilitesFoncieres.observationOffreFonciereClientele;
ficheFields['ouiImpossibiliteOffreCentre']                =Value('id').of($ds067PE1.situationProjet3.positionnementDisponibilitesFoncieres.impossibiliteOffreCentre).eq("ouiImpossibiliteOffreCentre") ? true : false;
ficheFields['nonImpossibiliteOffreCentre']                =Value('id').of($ds067PE1.situationProjet3.positionnementDisponibilitesFoncieres.impossibiliteOffreCentre).eq("nonImpossibiliteOffreCentre") ? true : false;
ficheFields['sansObjetImpossibiliteOffreCentre']          =Value('id').of($ds067PE1.situationProjet3.positionnementDisponibilitesFoncieres.impossibiliteOffreCentre).eq("sansObjetImpossibiliteOffreCentre") ? true : false;
ficheFields['observationImpossibiliteOffreCentre']        =$ds067PE1.situationProjet3.positionnementDisponibilitesFoncieres.observationImpossibiliteOffreCentre;

//complementarite Activites
ficheFields['ouiExistencePietonnierActivite']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.existencePietonnierActivite).eq("ouiExistencePietonnierActivite") ? true : false;
ficheFields['nonExistencePietonnierActivite']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.existencePietonnierActivite).eq("nonExistencePietonnierActivite") ? true : false;
ficheFields['sansObjetExistencePietonnierActivite']          =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.existencePietonnierActivite).eq("sansObjetExistencePietonnierActivite") ? true : false;
ficheFields['observationExistencePietonnierActivite']        =$ds067PE1.examenPrecision1.complementatiriteActivite.observationExistencePietonnierActivite;
ficheFields['ouiExistencePietonnierCommerceProximite']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.existencePietonnierCommerceProximite).eq("ouiExistencePietonnierCommerceProximite") ? true : false;
ficheFields['nonExistencePietonnierCommerceProximite']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.existencePietonnierCommerceProximite).eq("nonExistencePietonnierCommerceProximite") ? true : false;
ficheFields['sansObjetExistencePietonnierCommerceProximite']          =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.existencePietonnierCommerceProximite).eq("sansObjetExistencePietonnierCommerceProximite") ? true : false;
ficheFields['observationExistencePietonnierCommerceProximite']        =$ds067PE1.examenPrecision1.complementatiriteActivite.observationExistencePietonnierCommerceProximite;
ficheFields['ouiExistencePietonnierAutreCommerce']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.existencePietonnierAutreCommerce).eq("ouiExistencePietonnierAutreCommerce") ? true : false;
ficheFields['nonExistencePietonnierAutreCommerce']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.existencePietonnierAutreCommerce).eq("nonExistencePietonnierAutreCommerce") ? true : false;
ficheFields['sansObjetExistencePietonnierAutreCommerce']          =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.existencePietonnierAutreCommerce).eq("sansObjetExistencePietonnierAutreCommerce") ? true : false;
ficheFields['observationExistencePietonnierAutreCommerce']        =$ds067PE1.examenPrecision1.complementatiriteActivite.observationExistencePietonnierAutreCommerce;
ficheFields['ouiExistencePietonnierLieuxVie']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.existencePietonnierLieuxVie).eq("ouiExistencePietonnierLieuxVie") ? true : false;
ficheFields['nonExistencePietonnierLieuxVie']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.existencePietonnierLieuxVie).eq("nonExistencePietonnierLieuxVie") ? true : false;
ficheFields['sansObjetExistencePietonnierLieuxVie']          =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.existencePietonnierLieuxVie).eq("sansObjetExistencePietonnierLieuxVie") ? true : false;
ficheFields['observationExistencePietonnierLieuxVie']        =$ds067PE1.examenPrecision1.complementatiriteActivite.observationExistencePietonnierLieuxVie;
ficheFields['ouiExistencePietonnierProximite']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.existencePietonnierProximite).eq("ouiExistencePietonnierProximite") ? true : false;
ficheFields['nonExistencePietonnierProximite']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.existencePietonnierProximite).eq("nonExistencePietonnierProximite") ? true : false;
ficheFields['sansObjetExistencePietonnierProximite']          =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.existencePietonnierProximite).eq("sansObjetExistencePietonnierProximite") ? true : false;
ficheFields['observationExistencePietonnierProximite']        =$ds067PE1.examenPrecision1.complementatiriteActivite.observationExistencePietonnierProximite;
ficheFields['ouiExistencePietonnierPublic']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.existencePietonnierPublic).eq("ouiExistencePietonnierPublic") ? true : false;
ficheFields['nonExistencePietonnierPublic']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.existencePietonnierPublic).eq("nonExistencePietonnierPublic") ? true : false;
ficheFields['sansObjetExistencePietonnierPublic']          =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.existencePietonnierPublic).eq("sansObjetExistencePietonnierPublic") ? true : false;
ficheFields['observationExistencePietonnierPublic']        =$ds067PE1.examenPrecision1.complementatiriteActivite.observationExistencePietonnierPublic;

ficheFields['ouiFaciliteHalle']                      =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.faciliteHalle).eq("ouiFaciliteHalle") ? true : false;
ficheFields['nonFaciliteHalle']                      =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.faciliteHalle).eq("nonFaciliteHalle") ? true : false;
ficheFields['sansObjetFaciliteHalle']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.faciliteHalle).eq("sansObjetFaciliteHalle") ? true : false;
ficheFields['observationFaciliteHalle']              =$ds067PE1.examenPrecision1.complementatiriteActivite.observationFaciliteHalle;
ficheFields['ouiReequilibrageCentre']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.reequilibrageCentre).eq("ouiReequilibrageCentre") ? true : false;
ficheFields['nonReequilibrageCentre']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.reequilibrageCentre).eq("nonReequilibrageCentre") ? true : false;
ficheFields['sansObjetReequilibrageCentre']          =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.reequilibrageCentre).eq("sansObjetReequilibrageCentre") ? true : false;
ficheFields['observationReequilibrageCentre']        =$ds067PE1.examenPrecision1.complementatiriteActivite.observationReequilibrageCentre;
ficheFields['ouiReequilibrageSpatial']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.reequilibrageSpatial).eq("ouiReequilibrageSpatial") ? true : false;
ficheFields['nonReequilibrageSpatial']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.reequilibrageSpatial).eq("nonReequilibrageSpatial") ? true : false;
ficheFields['sansObjetReequilibrageSpatial']          =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.reequilibrageSpatial).eq("sansObjetReequilibrageSpatial") ? true : false;
ficheFields['observationReequilibrageSpatial']        =$ds067PE1.examenPrecision1.complementatiriteActivite.observationReequilibrageSpatial;
ficheFields['ouiReequilibrageSpatialCommun']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.reequilibrageSpatialCommun).eq("ouiReequilibrageSpatialCommun") ? true : false;
ficheFields['nonReequilibrageSpatialCommun']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.reequilibrageSpatialCommun).eq("nonReequilibrageSpatialCommun") ? true : false;
ficheFields['sansObjetReequilibrageSpatialCommun']          =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.reequilibrageSpatialCommun).eq("sansObjetReequilibrageSpatialCommun") ? true : false;
ficheFields['observationReequilibrageSpatialCommun']        =$ds067PE1.examenPrecision1.complementatiriteActivite.observationReequilibrageSpatialCommun;
ficheFields['ouiReequilibrageSpatialMotorisee']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.reequilibrageSpatialMotorisee).eq("ouiReequilibrageSpatialMotorisee") ? true : false;
ficheFields['nonReequilibrageSpatialMotorisee']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.reequilibrageSpatialMotorisee).eq("nonReequilibrageSpatialMotorisee") ? true : false;
ficheFields['sansObjetReequilibrageSpatialMotorisee']          =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.reequilibrageSpatialMotorisee).eq("sansObjetReequilibrageSpatialMotorisee") ? true : false;
ficheFields['observationReequilibrageSpatialMotorisee']        =$ds067PE1.examenPrecision1.complementatiriteActivite.observationReequilibrageSpatialMotorisee;

ficheFields['ouiRenforcementCommercialPolitique']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.renforcementCommercialPolitique).eq("ouiRenforcementCommercialPolitique") ? true : false;
ficheFields['nonRenforcementCommercialPolitique']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.renforcementCommercialPolitique).eq("nonRenforcementCommercialPolitique") ? true : false;
ficheFields['sansObjetRenforcementCommercialPolitique']          =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.renforcementCommercialPolitique).eq("sansObjetRenforcementCommercialPolitique") ? true : false;
ficheFields['observationRenforcementCommercialPolitique']        =$ds067PE1.examenPrecision1.complementatiriteActivite.observationRenforcementCommercialPolitique;

ficheFields['ouiRenforcementCommercialMontagne']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.renforcementCommercialMontagne).eq("ouiRenforcementCommercialMontagne") ? true : false;
ficheFields['nonRenforcementCommercialMontagne']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.renforcementCommercialMontagne).eq("nonRenforcementCommercialMontagne") ? true : false;
ficheFields['sansObjetRenforcementCommercialMontagne']          =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.renforcementCommercialMontagne).eq("sansObjetRenforcementCommercialMontagne") ? true : false;
ficheFields['observationRenforcementCommercialMontagne']        =$ds067PE1.examenPrecision1.complementatiriteActivite.observationRenforcementCommercialMontagne;
ficheFields['ouiRenforcementCommercialRural']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.renforcementCommercialRural).eq("ouiRenforcementCommercialRural") ? true : false;
ficheFields['nonRenforcementCommercialRural']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.renforcementCommercialRural).eq("nonRenforcementCommercialRural") ? true : false;
ficheFields['sansObjetRenforcementCommercialRural']          =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.renforcementCommercialRural).eq("sansObjetRenforcementCommercialRural") ? true : false;
ficheFields['observationRenforcementCommercialRural']        =$ds067PE1.examenPrecision1.complementatiriteActivite.observationRenforcementCommercialRural;
ficheFields['ouiRenforcementCommercialPetit']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.renforcementCommercialPetit).eq("ouiRenforcementCommercialPetit") ? true : false;
ficheFields['nonRenforcementCommercialPetit']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.renforcementCommercialPetit).eq("nonRenforcementCommercialPetit") ? true : false;
ficheFields['sansObjetRenforcementCommercialPetit']          =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.renforcementCommercialPetit).eq("sansObjetRenforcementCommercialPetit") ? true : false;
ficheFields['observationRenforcementCommercialPetit']        =$ds067PE1.examenPrecision1.complementatiriteActivite.observationRenforcementCommercialPetit;
ficheFields['ouiRenforcementCommercialRue']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.renforcementCommercialRue).eq("ouiRenforcementCommercialRue") ? true : false;
ficheFields['nonRenforcementCommercialRue']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.renforcementCommercialRue).eq("nonRenforcementCommercialRue") ? true : false;
ficheFields['sansObjetRenforcementCommercialRue']          =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.renforcementCommercialRue).eq("sansObjetRenforcementCommercialRue") ? true : false;
ficheFields['observationRenforcementCommercialRue']        =$ds067PE1.examenPrecision1.complementatiriteActivite.observationRenforcementCommercialRue;
ficheFields['ouiRenforcementCommercialQuartier']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.renforcementCommercialQuartier).eq("ouiRenforcementCommercialQuartier") ? true : false;
ficheFields['nonRenforcementCommercialQuartier']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.renforcementCommercialQuartier).eq("nonRenforcementCommercialQuartier") ? true : false;
ficheFields['sansObjetRenforcementCommercialQuartier']          =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.renforcementCommercialQuartier).eq("sansObjetRenforcementCommercialQuartier") ? true : false;
ficheFields['observationRenforcementCommercialQuartier']        =$ds067PE1.examenPrecision1.complementatiriteActivite.observationRenforcementCommercialQuartier;

ficheFields['ouiRenforcementCommercialCentre']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.renforcementCommercialCentre).eq("ouiRenforcementCommercialCentre") ? true : false;
ficheFields['nonRenforcementCommercialCentre']                =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.renforcementCommercialCentre).eq("nonRenforcementCommercialCentre") ? true : false;
ficheFields['sansObjetRenforcementCommercialCentre']          =Value('id').of($ds067PE1.examenPrecision1.complementatiriteActivite.renforcementCommercialCentre).eq("sansObjetRenforcementCommercialCentre") ? true : false;
ficheFields['observationRenforcementCommercialCentre']        =$ds067PE1.examenPrecision1.complementatiriteActivite.observationRenforcementCommercialCentre;

//Maitrise et limitation
ficheFields['ouiDimensionnementClientele']                =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.dimensionnementClientele).eq("ouiDimensionnementClientele") ? true : false;
ficheFields['nonDimensionnementClientele']                =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.dimensionnementClientele).eq("nonDimensionnementClientele") ? true : false;
ficheFields['sansObjetDimensionnementClientele']          =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.dimensionnementClientele).eq("sansObjetDimensionnementClientele") ? true : false;
ficheFields['observationDimensionnementClientele']        =$ds067PE1.examenPrecision2.maitriseLimitation.observationDimensionnementClientele;
ficheFields['ouiDimensionnementLivraison']                =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.dimensionnementLivraison).eq("ouiDimensionnementLivraison") ? true : false;
ficheFields['nonDimensionnementLivraison']                =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.dimensionnementLivraison).eq("nonDimensionnementLivraison") ? true : false;
ficheFields['sansObjetDimensionnementLivraison']          =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.dimensionnementLivraison).eq("sansObjetDimensionnementLivraison") ? true : false;
ficheFields['observationDimensionnementLivraison']        =$ds067PE1.examenPrecision2.maitriseLimitation.observationDimensionnementLivraison;
ficheFields['ouiEmplacementHandicape']                =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.emplacementHandicape).eq("ouiEmplacementHandicape") ? true : false;
ficheFields['nonEmplacementHandicape']                =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.emplacementHandicape).eq("nonEmplacementHandicape") ? true : false;
ficheFields['sansObjetEmplacementHandicape']          =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.emplacementHandicape).eq("sansObjetEmplacementHandicape") ? true : false;
ficheFields['observationEmplacementHandicape']        =$ds067PE1.examenPrecision2.maitriseLimitation.observationEmplacementHandicape;
ficheFields['ouiExistenceTC']                         =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.existenceTC).eq("ouiExistenceTC") ? true : false;
ficheFields['nonExistenceTC']                         =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.existenceTC).eq("nonExistenceTC") ? true : false;
ficheFields['sansObjetExistenceTC']                   =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.existenceTC).eq("sansObjetExistenceTC") ? true : false;
ficheFields['observationExistenceTC']                 =$ds067PE1.examenPrecision2.maitriseLimitation.observationExistenceTC;
ficheFields['ouiLienTcActivite']                         =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.lienTcActivite).eq("ouiLienTcActivite") ? true : false;
ficheFields['nonLienTcActivite']                         =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.lienTcActivite).eq("nonLienTcActivite") ? true : false;
ficheFields['sansObjetLienTcActivite']                   =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.lienTcActivite).eq("sansObjetLienTcActivite") ? true : false;
ficheFields['observationLienTcActivite']                 =$ds067PE1.examenPrecision2.maitriseLimitation.observationLienTcActivite;
ficheFields['ouiLienTcLieuVie']                         =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.lienTcLieuVie).eq("ouiLienTcLieuVie") ? true : false;
ficheFields['nonLienTcLieuVie']                         =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.lienTcLieuVie).eq("nonLienTcLieuVie") ? true : false;
ficheFields['sansObjetLienTcLieuVie']                   =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.lienTcLieuVie).eq("sansObjetLienTcLieuVie") ? true : false;
ficheFields['observationLienTcLieuVie']                 =$ds067PE1.examenPrecision2.maitriseLimitation.observationLienTcLieuVie;
ficheFields['ouiLienTcProximite']                         =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.lienTcProximite).eq("ouiLienTcProximite") ? true : false;
ficheFields['nonLienTcProximite']                         =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.lienTcProximite).eq("nonLienTcProximite") ? true : false;
ficheFields['sansObjetLienTcProximite']                   =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.lienTcProximite).eq("sansObjetLienTcProximite") ? true : false;
ficheFields['observationLienTcProximite']                 =$ds067PE1.examenPrecision2.maitriseLimitation.observationLienTcProximite;
ficheFields['ouiLienTcPublic']                         =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.lienTcPublic).eq("ouiLienTcPublic") ? true : false;
ficheFields['nonLienTcPublic']                         =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.lienTcPublic).eq("nonLienTcPublic") ? true : false;
ficheFields['sansObjetLienTcPublic']                   =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.lienTcPublic).eq("sansObjetLienTcPublic") ? true : false;
ficheFields['observationLienTcPublic']                 =$ds067PE1.examenPrecision2.maitriseLimitation.observationLienTcPublic;
ficheFields['ouiPlace']                         =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.place).eq("ouiPlace") ? true : false;
ficheFields['nonPlace']                         =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.place).eq("nonPlace") ? true : false;
ficheFields['sansObjetPlace']                   =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.place).eq("sansObjetPlace") ? true : false;
ficheFields['observationPlace']                 =$ds067PE1.examenPrecision2.maitriseLimitation.observationPlace;
ficheFields['ouiMesureDeplacement']                         =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.mesureDeplacement).eq("ouiMesureDeplacement") ? true : false;
ficheFields['nonMesureDeplacement']                         =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.mesureDeplacement).eq("nonMesureDeplacement") ? true : false;
ficheFields['sansObjetMesureDeplacement']                   =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.mesureDeplacement).eq("sansObjetMesureDeplacement") ? true : false;
ficheFields['observationMesureDeplacement']                 =$ds067PE1.examenPrecision2.maitriseLimitation.observationMesureDeplacement;
ficheFields['ouiMesureSecurisationClientele']                         =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.mesureSecurisationClientele).eq("ouiMesureSecurisationClientele") ? true : false;
ficheFields['nonMesureSecurisationClientele']                         =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.mesureSecurisationClientele).eq("nonMesureSecurisationClientele") ? true : false;
ficheFields['sansObjetMesureSecurisationClientele']                   =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.mesureSecurisationClientele).eq("sansObjetMesureSecurisationClientele") ? true : false;
ficheFields['observationMesureSecurisationClientele']                 =$ds067PE1.examenPrecision2.maitriseLimitation.observationMesureSecurisationClientele;
ficheFields['ouiMesureSecurisationLivraison']                         =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.mesureSecurisationLivraison).eq("ouiMesureSecurisationLivraison") ? true : false;
ficheFields['nonMesureSecurisationLivraison']                         =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.mesureSecurisationLivraison).eq("nonMesureSecurisationLivraison") ? true : false;
ficheFields['sansObjetMesureSecurisationLivraison']                   =Value('id').of($ds067PE1.examenPrecision2.maitriseLimitation.mesureSecurisationLivraison).eq("sansObjetMesureSecurisationLivraison") ? true : false;
ficheFields['observationMesureSecurisationLivraison']                 =$ds067PE1.examenPrecision2.maitriseLimitation.observationMesureSecurisationLivraison;

//reduction pollution
ficheFields['ouiDispositionBatiment']                         =Value('id').of($ds067PE1.examenPrecision3.reductionPollution.dispositionBatiment).eq("ouiDispositionBatiment") ? true : false;
ficheFields['nonDispositionBatiment']                         =Value('id').of($ds067PE1.examenPrecision3.reductionPollution.dispositionBatiment).eq("nonDispositionBatiment") ? true : false;
ficheFields['sansObjetDispositionBatiment']                   =Value('id').of($ds067PE1.examenPrecision3.reductionPollution.dispositionBatiment).eq("sansObjetDispositionBatiment") ? true : false;
ficheFields['observationDispositionBatiment']                 =$ds067PE1.examenPrecision3.reductionPollution.observationDispositionBatiment;
ficheFields['ouiTraitementStationnement']                         =Value('id').of($ds067PE1.examenPrecision3.reductionPollution.traitementStationnement).eq("ouiTraitementStationnement") ? true : false;
ficheFields['nonTraitementStationnement']                         =Value('id').of($ds067PE1.examenPrecision3.reductionPollution.traitementStationnement).eq("nonTraitementStationnement") ? true : false;
ficheFields['sansObjetTraitementStationnement']                   =Value('id').of($ds067PE1.examenPrecision3.reductionPollution.traitementStationnement).eq("sansObjetTraitementStationnement") ? true : false;
ficheFields['observationTraitementStationnement']                 =$ds067PE1.examenPrecision3.reductionPollution.observationTraitementStationnement;
ficheFields['ouiTraitementDechet']                         =Value('id').of($ds067PE1.examenPrecision3.reductionPollution.traitementDechet).eq("ouiTraitementDechet") ? true : false;
ficheFields['nonTraitementDechet']                         =Value('id').of($ds067PE1.examenPrecision3.reductionPollution.traitementDechet).eq("nonTraitementDechet") ? true : false;
ficheFields['sansObjetTraitementDechet']                   =Value('id').of($ds067PE1.examenPrecision3.reductionPollution.traitementDechet).eq("sansObjetTraitementDechet") ? true : false;
ficheFields['observationTraitementDechet']                 =$ds067PE1.examenPrecision3.reductionPollution.observationTraitementDechet;
ficheFields['ouiMesureMaitriseEnvironnement']                         =Value('id').of($ds067PE1.examenPrecision3.reductionPollution.mesureMaitriseEnvironnement).eq("ouiMesureMaitriseEnvironnement") ? true : false;
ficheFields['nonMesureMaitriseEnvironnement']                         =Value('id').of($ds067PE1.examenPrecision3.reductionPollution.mesureMaitriseEnvironnement).eq("nonMesureMaitriseEnvironnement") ? true : false;
ficheFields['sansObjetMesureMaitriseEnvironnement']                   =Value('id').of($ds067PE1.examenPrecision3.reductionPollution.mesureMaitriseEnvironnement).eq("sansObjetMesureMaitriseEnvironnement") ? true : false;
ficheFields['observationMesureMaitriseEnvironnement']                 =$ds067PE1.examenPrecision3.reductionPollution.observationMesureMaitriseEnvironnement;
ficheFields['ouiModaliteStructurelles']                         =Value('id').of($ds067PE1.examenPrecision3.reductionPollution.modaliteStructurelles).eq("ouiModaliteStructurelles") ? true : false;
ficheFields['nonModaliteStructurelles']                         =Value('id').of($ds067PE1.examenPrecision3.reductionPollution.modaliteStructurelles).eq("nonModaliteStructurelles") ? true : false;
ficheFields['sansObjetModaliteStructurelles']                   =Value('id').of($ds067PE1.examenPrecision3.reductionPollution.modaliteStructurelles).eq("sansObjetModaliteStructurelles") ? true : false;
ficheFields['observationModaliteStructurelles']                 =$ds067PE1.examenPrecision3.reductionPollution.observationModaliteStructurelles;

//maitrise consommation energie
ficheFields['ouiDispositifChauffage']                         =Value('id').of($ds067PE1.examenPrecision4.maitriseConsommation.dispositifChauffage).eq("ouiDispositifChauffage") ? true : false;
ficheFields['nonDispositifChauffage']                         =Value('id').of($ds067PE1.examenPrecision4.maitriseConsommation.dispositifChauffage).eq("nonDispositifChauffage") ? true : false;
ficheFields['sansObjetDispositifChauffage']                   =Value('id').of($ds067PE1.examenPrecision4.maitriseConsommation.dispositifChauffage).eq("sansObjetDispositifChauffage") ? true : false;
ficheFields['observationDispositifChauffage']                 =$ds067PE1.examenPrecision4.maitriseConsommation.observationDispositifChauffage;
ficheFields['ouiDispositifEclairage']                         =Value('id').of($ds067PE1.examenPrecision4.maitriseConsommation.dispositifEclairage).eq("ouiDispositifEclairage") ? true : false;
ficheFields['nonDispositifEclairage']                          =Value('id').of($ds067PE1.examenPrecision4.maitriseConsommation.dispositifEclairage).eq("nonDispositifEclairage") ? true : false;
ficheFields['sansObjetDispositifEclairage']                   =Value('id').of($ds067PE1.examenPrecision4.maitriseConsommation.dispositifEclairage).eq("sansObjetDispositifEclairage") ? true : false;
ficheFields['observationDispositifEclairage']                 =$ds067PE1.examenPrecision4.maitriseConsommation.observationDispositifEclairage;
ficheFields['ouiDispositifEnergieRenouvelable']                         =Value('id').of($ds067PE1.examenPrecision4.maitriseConsommation.dispositifEnergieRenouvelable).eq("ouiDispositifEnergieRenouvelable") ? true : false;
ficheFields['nonDispositifEnergieRenouvelable']                         =Value('id').of($ds067PE1.examenPrecision4.maitriseConsommation.dispositifEnergieRenouvelable).eq("nonDispositifEnergieRenouvelable") ? true : false;
ficheFields['sansObjetDispositifEnergieRenouvelable']                   =Value('id').of($ds067PE1.examenPrecision4.maitriseConsommation.dispositifEnergieRenouvelable).eq("sansObjetDispositifEnergieRenouvelable") ? true : false;
ficheFields['observationDispositifEnergieRenouvelable']                 =$ds067PE1.examenPrecision4.maitriseConsommation.observationDispositifEnergieRenouvelable;

//mise en oeuvre accompagnement végétal
ficheFields['ouiCompensationBatimente']                         =Value('id').of($ds067PE1.examenPrecision5.accompagnementVegetal.compensationBatimente).eq("ouiCompensationBatimente") ? true : false;
ficheFields['nonCompensationBatimente']                         =Value('id').of($ds067PE1.examenPrecision5.accompagnementVegetal.compensationBatimente).eq("nonCompensationBatimente") ? true : false;
ficheFields['sansObjetCompensationBatimente']                   =Value('id').of($ds067PE1.examenPrecision5.accompagnementVegetal.compensationBatimente).eq("sansObjetCompensationBatimente") ? true : false;
ficheFields['observationCompensationBatimente']                 =$ds067PE1.examenPrecision5.accompagnementVegetal.observationCompensationBatimente;
ficheFields['ouiEmplacementVegetalise']                         =Value('id').of($ds067PE1.examenPrecision5.accompagnementVegetal.emplacementVegetalise).eq("ouiEmplacementVegetalise") ? true : false;
ficheFields['nonEmplacementVegetalise']                         =Value('id').of($ds067PE1.examenPrecision5.accompagnementVegetal.emplacementVegetalise).eq("nonEmplacementVegetalise") ? true : false;
ficheFields['sansObjetEmplacementVegetalise']                   =Value('id').of($ds067PE1.examenPrecision5.accompagnementVegetal.emplacementVegetalise).eq("sansObjetEmplacementVegetalise") ? true : false;
ficheFields['observationEmplacementVegetalise']                 =$ds067PE1.examenPrecision5.accompagnementVegetal.observationEmplacementVegetalise;
ficheFields['ouiCompensationStationnement']                     =Value('id').of($ds067PE1.examenPrecision5.accompagnementVegetal.compensationStationnement).eq("ouiCompensationStationnement") ? true : false;
ficheFields['nonCompensationStationnement']                     =Value('id').of($ds067PE1.examenPrecision5.accompagnementVegetal.compensationStationnement).eq("nonCompensationStationnement") ? true : false;
ficheFields['sansObjetCompensationStationnement']               =Value('id').of($ds067PE1.examenPrecision5.accompagnementVegetal.compensationStationnement).eq("sansObjetCompensationStationnement") ? true : false;
ficheFields['observationCompensationStationnement']             =$ds067PE1.examenPrecision5.accompagnementVegetal.observationCompensationStationnement;
ficheFields['ouiPlantationArbre']                               =Value('id').of($ds067PE1.examenPrecision5.accompagnementVegetal.plantationArbre).eq("ouiPlantationArbre") ? true : false;
ficheFields['nonPlantationArbre']                               =Value('id').of($ds067PE1.examenPrecision5.accompagnementVegetal.plantationArbre).eq("nonPlantationArbre") ? true : false;
ficheFields['sansObjetPlantationArbre']                         =Value('id').of($ds067PE1.examenPrecision5.accompagnementVegetal.plantationArbre).eq("sansObjetPlantationArbre") ? true : false;
ficheFields['observationPlantationArbre']                       =$ds067PE1.examenPrecision5.accompagnementVegetal.observationPlantationArbre;

//inscription urbain paysager
ficheFields['ouiContributionUrbaine']                               =Value('id').of($ds067PE1.examenPrecision6.inscriptionProjet.contributionUrbaine).eq("ouiContributionUrbaine") ? true : false;
ficheFields['nonContributionUrbaine']                               =Value('id').of($ds067PE1.examenPrecision6.inscriptionProjet.contributionUrbaine).eq("nonContributionUrbaine") ? true : false;
ficheFields['sansObjetContributionUrbaine']                         =Value('id').of($ds067PE1.examenPrecision6.inscriptionProjet.contributionUrbaine).eq("sansObjetContributionUrbaine") ? true : false;
ficheFields['observationContributionUrbaine']                       =$ds067PE1.examenPrecision6.inscriptionProjet.observationContributionUrbaine;
ficheFields['ouiStationnementBatiment']                               =Value('id').of($ds067PE1.examenPrecision6.inscriptionProjet.stationnementBatiment).eq("ouiStationnementBatiment") ? true : false;
ficheFields['nonStationnementBatiment']                               =Value('id').of($ds067PE1.examenPrecision6.inscriptionProjet.stationnementBatiment).eq("nonStationnementBatiment") ? true : false;
ficheFields['sansObjetStationnementBatiment']                         =Value('id').of($ds067PE1.examenPrecision6.inscriptionProjet.stationnementBatiment).eq("sansObjetStationnementBatiment") ? true : false;
ficheFields['observationStationnementBatiment']                       =$ds067PE1.examenPrecision6.inscriptionProjet.observationStationnementBatiment;
ficheFields['ouiRehabilitationFriche']                               =Value('id').of($ds067PE1.examenPrecision6.inscriptionProjet.rehabilitationFriche).eq("ouiRehabilitationFriche") ? true : false;
ficheFields['nonRehabilitationFriche']                               =Value('id').of($ds067PE1.examenPrecision6.inscriptionProjet.rehabilitationFriche).eq("nonRehabilitationFriche") ? true : false;
ficheFields['sansObjetRehabilitationFriche']                         =Value('id').of($ds067PE1.examenPrecision6.inscriptionProjet.rehabilitationFriche).eq("sansObjetRehabilitationFriche") ? true : false;
ficheFields['observationRehabilitationFriche']                       =$ds067PE1.examenPrecision6.inscriptionProjet.observationRehabilitationFriche;
ficheFields['ouiRehabilitationBatimentCommercial']                               =Value('id').of($ds067PE1.examenPrecision6.inscriptionProjet.rehabilitationBatimentCommercial).eq("ouiRehabilitationBatimentCommercial") ? true : false;
ficheFields['nonRehabilitationBatimentCommercial']                               =Value('id').of($ds067PE1.examenPrecision6.inscriptionProjet.rehabilitationBatimentCommercial).eq("nonRehabilitationBatimentCommercial") ? true : false;
ficheFields['sansObjetRehabilitationBatimentCommercial']                         =Value('id').of($ds067PE1.examenPrecision6.inscriptionProjet.rehabilitationBatimentCommercial).eq("sansObjetRehabilitationBatimentCommercial") ? true : false;
ficheFields['observationRehabilitationBatimentCommercial']                       =$ds067PE1.examenPrecision6.inscriptionProjet.observationRehabilitationBatimentCommercial;

//complementarite plans locaux
ficheFields['ouiCompatibiliteRisquesIndus']                               =Value('id').of($ds067PE1.examenPrecision7.complementaritePlan.compatibiliteRisquesIndus).eq("ouiCompatibiliteRisquesIndus") ? true : false;
ficheFields['nonCompatibiliteRisquesIndus']                               =Value('id').of($ds067PE1.examenPrecision7.complementaritePlan.compatibiliteRisquesIndus).eq("nonCompatibiliteRisquesIndus") ? true : false;
ficheFields['sansObjetCompatibiliteRisquesIndus']                         =Value('id').of($ds067PE1.examenPrecision7.complementaritePlan.compatibiliteRisquesIndus).eq("sansObjetCompatibiliteRisquesIndus") ? true : false;
ficheFields['observationCompatibiliteRisquesIndus']                       =$ds067PE1.examenPrecision7.complementaritePlan.observationCompatibiliteRisquesIndus;
ficheFields['ouiCompatibiliteRisquesNaturels']                               =Value('id').of($ds067PE1.examenPrecision7.complementaritePlan.compatibiliteRisquesNaturels).eq("ouiCompatibiliteRisquesNaturels") ? true : false;
ficheFields['nonCompatibiliteRisquesNaturels']                               =Value('id').of($ds067PE1.examenPrecision7.complementaritePlan.compatibiliteRisquesNaturels).eq("nonCompatibiliteRisquesNaturels") ? true : false;
ficheFields['sansObjetCompatibiliteRisquesNaturels']                         =Value('id').of($ds067PE1.examenPrecision7.complementaritePlan.compatibiliteRisquesNaturels).eq("sansObjetCompatibiliteRisquesNaturels") ? true : false;
ficheFields['observationCompatibiliteRisquesNaturels']                       =$ds067PE1.examenPrecision7.complementaritePlan.observationCompatibiliteRisquesNaturels;


//Maitrise externalite projet
ficheFields['ouiRealisationTravauxPublic']                               =Value('id').of($ds067PE1.examenPrecision8.maitriseExternalite.realisationTravauxPublic).eq("ouiRealisationTravauxPublic") ? true : false;
ficheFields['nonRealisationTravauxPublic']                               =Value('id').of($ds067PE1.examenPrecision8.maitriseExternalite.realisationTravauxPublic).eq("nonRealisationTravauxPublic") ? true : false;
ficheFields['sansObjetRealisationTravauxPublic']                         =Value('id').of($ds067PE1.examenPrecision8.maitriseExternalite.realisationTravauxPublic).eq("sansObjetRealisationTravauxPublic") ? true : false;
ficheFields['observationRealisationTravauxPublic']                       =$ds067PE1.examenPrecision8.maitriseExternalite.observationRealisationTravauxPublic;
ficheFields['ouiPriseChargeTravaux']                               =Value('id').of($ds067PE1.examenPrecision8.maitriseExternalite.priseChargeTravaux).eq("ouiPriseChargeTravaux") ? true : false;
ficheFields['nonPriseChargeTravaux']                               =Value('id').of($ds067PE1.examenPrecision8.maitriseExternalite.priseChargeTravaux).eq("nonPriseChargeTravaux") ? true : false;
ficheFields['sansObjetPriseChargeTravaux']                         =Value('id').of($ds067PE1.examenPrecision8.maitriseExternalite.priseChargeTravaux).eq("sansObjetPriseChargeTravaux") ? true : false;
ficheFields['observationPriseChargeTravaux']                       =$ds067PE1.examenPrecision8.maitriseExternalite.observationPriseChargeTravaux;
ficheFields['ouiDemandeurTravaux']                               =Value('id').of($ds067PE1.examenPrecision8.maitriseExternalite.demandeurTravaux).eq("ouiDemandeurTravaux") ? true : false;
ficheFields['nonDemandeurTravaux']                               =Value('id').of($ds067PE1.examenPrecision8.maitriseExternalite.demandeurTravaux).eq("nonDemandeurTravaux") ? true : false;
ficheFields['sansObjetDemandeurTravaux']                         =Value('id').of($ds067PE1.examenPrecision8.maitriseExternalite.demandeurTravaux).eq("sansObjetDemandeurTravaux") ? true : false;
ficheFields['observationDemandeurTravaux']                       =$ds067PE1.examenPrecision8.maitriseExternalite.observationDemandeurTravaux;
ficheFields['ouiTravauxPaysage']                               =Value('id').of($ds067PE1.examenPrecision8.maitriseExternalite.travauxPaysage).eq("ouiTravauxPaysage") ? true : false;
ficheFields['nonTravauxPaysage']                               =Value('id').of($ds067PE1.examenPrecision8.maitriseExternalite.travauxPaysage).eq("nonTravauxPaysage") ? true : false;
ficheFields['sansObjetTravauxPaysage']                         =Value('id').of($ds067PE1.examenPrecision8.maitriseExternalite.travauxPaysage).eq("sansObjetTravauxPaysage") ? true : false;
ficheFields['observationTravauxPaysage']                       =$ds067PE1.examenPrecision8.maitriseExternalite.observationTravauxPaysage;
ficheFields['ouiPriseChargeElus']                               =Value('id').of($ds067PE1.examenPrecision8.maitriseExternalite.priseChargeElus).eq("ouiPriseChargeElus") ? true : false;
ficheFields['nonPriseChargeElus']                               =Value('id').of($ds067PE1.examenPrecision8.maitriseExternalite.priseChargeElus).eq("nonPriseChargeElus") ? true : false;
ficheFields['sansObjetPriseChargeElus']                         =Value('id').of($ds067PE1.examenPrecision8.maitriseExternalite.priseChargeElus).eq("sansObjetPriseChargeElus") ? true : false;
ficheFields['observationPriseChargeElus']                       =$ds067PE1.examenPrecision8.maitriseExternalite.observationPriseChargeElus;
ficheFields['ouiDemandeurElu']                               =Value('id').of($ds067PE1.examenPrecision8.maitriseExternalite.demandeurElu).eq("ouiDemandeurElu") ? true : false;
ficheFields['nonDemandeurElu']                               =Value('id').of($ds067PE1.examenPrecision8.maitriseExternalite.demandeurElu).eq("nonDemandeurElu") ? true : false;
ficheFields['sansObjetDemandeurElu']                         =Value('id').of($ds067PE1.examenPrecision8.maitriseExternalite.demandeurElu).eq("sansObjetDemandeurElu") ? true : false;
ficheFields['observationDemandeurElu']                       =$ds067PE1.examenPrecision8.maitriseExternalite.observationDemandeurElu;
ficheFields['ouiFinancementAmenagement']                               =Value('id').of($ds067PE1.examenPrecision8.maitriseExternalite.financementAmenagement).eq("ouiFinancementAmenagement") ? true : false;
ficheFields['nonFinancementAmenagement']                               =Value('id').of($ds067PE1.examenPrecision8.maitriseExternalite.financementAmenagement).eq("nonFinancementAmenagement") ? true : false;
ficheFields['sansObjetFinancementAmenagement']                         =Value('id').of($ds067PE1.examenPrecision8.maitriseExternalite.financementAmenagement).eq("sansObjetFinancementAmenagement") ? true : false;
ficheFields['observationFinancementAmenagement']                       =$ds067PE1.examenPrecision8.maitriseExternalite.observationFinancementAmenagement;
ficheFields['ouiPriseChargeTravauxElu']                               =Value('id').of($ds067PE1.examenPrecision8.maitriseExternalite.priseChargeTravauxElu).eq("ouiPriseChargeTravauxElu") ? true : false;
ficheFields['nonPriseChargeTravauxElu']                               =Value('id').of($ds067PE1.examenPrecision8.maitriseExternalite.priseChargeTravauxElu).eq("nonPriseChargeTravauxElu") ? true : false;
ficheFields['sansObjetPriseChargeTravauxElu']                         =Value('id').of($ds067PE1.examenPrecision8.maitriseExternalite.priseChargeTravauxElu).eq("sansObjetPriseChargeTravauxElu") ? true : false;
ficheFields['observationPriseChargeTravauxElu']                       =$ds067PE1.examenPrecision8.maitriseExternalite.observationPriseChargeTravauxElu;

//Respect Règles
ficheFields['ouiRespectOrientation']                               =Value('id').of($ds067PE1.examenPrecision9.respectRegle.respectOrientation).eq("ouiRespectOrientation") ? true : false;
ficheFields['nonRespectOrientation']                               =Value('id').of($ds067PE1.examenPrecision9.respectRegle.respectOrientation).eq("nonRespectOrientation") ? true : false;
ficheFields['sansObjetRespectOrientation']                         =Value('id').of($ds067PE1.examenPrecision9.respectRegle.respectOrientation).eq("sansObjetRespectOrientation") ? true : false;
ficheFields['observationRespectOrientation']                       =$ds067PE1.examenPrecision9.respectRegle.observationRespectOrientation;
ficheFields['ouiPointVerifie']                               =Value('id').of($ds067PE1.examenPrecision9.respectRegle.pointVerifie).eq("ouiPointVerifie") ? true : false;
ficheFields['nonPointVerifie']                               =Value('id').of($ds067PE1.examenPrecision9.respectRegle.pointVerifie).eq("nonPointVerifie") ? true : false;
ficheFields['sansObjetPointVerifie']                         =Value('id').of($ds067PE1.examenPrecision9.respectRegle.pointVerifie).eq("sansObjetPointVerifie") ? true : false;
ficheFields['observationPointVerifie']                       =$ds067PE1.examenPrecision9.respectRegle.observationPointVerifie;
ficheFields['ouiLocalisationZone']                               =Value('id').of($ds067PE1.examenPrecision9.respectRegle.localisationZone).eq("ouiLocalisationZone") ? true : false;
ficheFields['nonLocalisationZone']                               =Value('id').of($ds067PE1.examenPrecision9.respectRegle.localisationZone).eq("nonLocalisationZone") ? true : false;
ficheFields['sansObjetLocalisationZone']                         =Value('id').of($ds067PE1.examenPrecision9.respectRegle.localisationZone).eq("sansObjetLocalisationZone") ? true : false;
ficheFields['observationLocalisationZone']                       =$ds067PE1.examenPrecision9.respectRegle.observationLocalisationZone;


//autres infos
ficheFields['ouiExistenceInformation']                               =Value('id').of($ds067PE1.autreInfos.autreInfos.existenceInformation).eq("ouiExistenceInformation") ? true : false;
ficheFields['nonExistenceInformation']                               =Value('id').of($ds067PE1.autreInfos.autreInfos.existenceInformation).eq("nonExistenceInformation") ? true : false;
ficheFields['sansObjetExistenceInformation']                         =Value('id').of($ds067PE1.autreInfos.autreInfos.existenceInformation).eq("sansObjetExistenceInformation") ? true : false;
ficheFields['observationExistenceInformation']                       =$ds067PE1.autreInfos.autreInfos.observationExistenceInformation;
ficheFields['ouiExistenceEtude']                               =Value('id').of($ds067PE1.autreInfos.autreInfos.existenceEtude).eq("ouiExistenceEtude") ? true : false;
ficheFields['nonExistenceEtude']                               =Value('id').of($ds067PE1.autreInfos.autreInfos.existenceEtude).eq("nonExistenceEtude") ? true : false;
ficheFields['sansObjetExistenceEtude']                         =Value('id').of($ds067PE1.autreInfos.autreInfos.existenceEtude).eq("sansObjetExistenceEtude") ? true : false;
ficheFields['observationExistenceEtude']                       =$ds067PE1.autreInfos.autreInfos.observationExistenceEtude;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $ds067PE1.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		dateSignature: $ds067PE1.signature.signature.dateSignature,
		autoriteHabilitee :"CDAC",
		demandeContexte : "Demande d’autorisation d’exploitation commerciale sans permis de construire",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier_libre_LE_V4.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));


/*
 * Ajout du formulaire personne physique
 */
if (Value('id').of($ds067PE1.etatCivil.identificationDeclarant.personne).eq("personnePhysique")) {
 var cerfaDoc = nash.doc //
	.load('models/DS067 PE1 Personne physique.pdf') //
	.apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));
}
/*
 * Ajout du formulaire personne morale
 */
 if (Value('id').of($ds067PE1.etatCivil.identificationDeclarant.personne).eq("personneMorale")) {
var cerfaDoc = nash.doc //
	.load('models/DS067-PE1-Personne morale.pdf') //
	.apply(moralFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));
}
/*
 * Ajout du formulaire avec les renseignements de l'annexe 2
 */
var cerfaDoc = nash.doc //
	.load('models/Annexe2.pdf') //
	.apply(annexeFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));

/*
 * Ajout du du formulaire avec les renseignements de l'annexe 3
 */
var cerfaDoc = nash.doc //
	.load('models/fiche_technique1.pdf') //
	.apply(ficheFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjMandat);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPlanIndicatif);
appendPj($attachmentPreprocess.attachmentPreprocess.pjLocalisation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCartePlan);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDesserte);
appendPj($attachmentPreprocess.attachmentPreprocess.pjVoies);
appendPj($attachmentPreprocess.attachmentPreprocess.pjEnvironnement);
appendPj($attachmentPreprocess.attachmentPreprocess.pjChalandise);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPresentation);

/*
 *PJ Construction : oui
 */
var pj=$ds067PE1.conditionRealisationProjet.conditionRealisationProjet.projetConstruction;
if(Value('id').of(pj).contains('ouiConstruction')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjConstruction);
}
/*
 *PJ Local Existant : oui
 */
var pj=$ds067PE1.conditionRealisationProjet.conditionRealisationProjet.projetLocalExistant;
if(Value('id').of(pj).contains('ouiChangement')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjLocalExistant);
}
/*
 *PJ Changement : oui
 */
var pj=$ds067PE1.descriptionProjet.projetChangement.projetChangementSecteur;
if(Value('id').of(pj).contains('ouiLocal')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjChangement);
}

/*
 *Extension si extension ou changement
 */
var pj1=$ds067PE1.descriptionProjet.surfaceDeVente.projetExtension;


var pj2=$ds067PE1.descriptionProjet.projetChangement.projetChangementSecteur;
if(Value('id').of(pj2).contains('ouiChangement') || Value('id').of(pj1).contains('ouiExtension')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjExtension);
}

/*
 *PJ Zone commerciale : Oui
 */
var pj=$ds067PE1.autresRenseignements.autresRenseignements.projetZoneCommercial;
if(Value('id').of(pj).contains('ouiZoneEnsembleCommercial')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjZoneCommerciale);
}



/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Commerce_Detail_Alimentaire_Sans_Permis.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Commerce de détail alimentaire - Demande d’autorisation d’exploitation commerciale sans permis de construire',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande d’autorisation d’exploitation commerciale sans permis de construire',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
