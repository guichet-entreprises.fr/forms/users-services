attachment('pjID', 'pjID');
attachment('pjExtraitImmatriculation', 'pjExtraitImmatriculation');
attachment('pjMandat', 'pjMandat', { mandatory : "false"});


attachment('pjPlanIndicatif', 'pjPlanIndicatif');
attachment('pjLocalisation', 'pjLocalisation');
attachment('pjCartePlan', 'pjCartePlan');
attachment('pjDesserte', 'pjDesserte');
attachment('pjVoies', 'pjVoies');
attachment('pjEnvironnement', 'pjEnvironnement');
attachment('pjChalandise', 'pjChalandise');
attachment('pjPresentation', 'pjPresentation');


// pj conditions

var pj=$ds067PE1.conditionRealisationProjet.conditionRealisationProjet.projetConstruction;
if(Value('id').of(pj).contains('ouiConstruction')) {
    attachment('pjConstruction', 'pjConstruction', { mandatory:"true"});
}

var pj=$ds067PE1.conditionRealisationProjet.conditionRealisationProjet.projetLocalExistant;
if(Value('id').of(pj).contains('ouiLocal')) {
    attachment('pjLocalExistant', 'pjLocalExistant', { mandatory:"true"});
}

var pj=$ds067PE1.descriptionProjet.projetChangement.projetChangementSecteur;
if(Value('id').of(pj).contains('ouiChangement')) {
    attachment('pjChangement', 'pjChangement', { mandatory:"true"});
}

var pj=$ds067PE1.autresRenseignements.autresRenseignements.projetZoneCommercial;
if(Value('id').of(pj).contains('ouiZoneEnsembleCommercial')) {
    attachment('pjZoneCommerciale', 'pjZoneCommerciale', { mandatory:"true"});
}

/*
 *Extension si extension ou changement
 */
var pj1=$ds067PE1.descriptionProjet.surfaceDeVente.projetExtension;


var pj2=$ds067PE1.descriptionProjet.projetChangement.projetChangementSecteur;
if(Value('id').of(pj2).contains('ouiChangement') || Value('id').of(pj1).contains('ouiExtension')) {
	attachment('pjExtension', 'pjExtension', { mandatory:"true"});
}
