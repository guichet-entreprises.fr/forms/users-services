// attachment('attachmentType', 'outputFileFieldId', { label: $pageId.fieldId });

//Exploitant 1
attachment('pjIDExploitant1', 'pjIDExploitant1', 
		{ label : ($ds067PE6.declarationGroup.exploitants.exploitant1.exploitantNomUsage111542 ?
					$ds067PE6.declarationGroup.exploitants.exploitant1.exploitantNomUsage111542 : $ds067PE6.declarationGroup.exploitants.exploitant1.exploitantNomNaissance111542 ) +' '
					+ $ds067PE6.declarationGroup.exploitants.exploitant1.exploitantPrenom111542
,mandatory:"true"});



	attachment('pjPermisExploitationDebitBoissonsExploitant1', 'pjPermisExploitationDebitBoissonsExploitant1', 
		{ label : ($ds067PE6.declarationGroup.exploitants.exploitant1.exploitantNomUsage111542 ?
					$ds067PE6.declarationGroup.exploitants.exploitant1.exploitantNomUsage111542 : $ds067PE6.declarationGroup.exploitants.exploitant1.exploitantNomNaissance111542 ) +' '
					+ $ds067PE6.declarationGroup.exploitants.exploitant1.exploitantPrenom111542
,mandatory:"true"});


if($ds067PE6.declarationGroup.exploitants.exploitant1.exploitantPermisVenteBoisson111542){
	attachment('pjPermisExploitationLaNuitExploitant1', 'pjPermisExploitationLaNuitExploitant1', 
		{ label : ($ds067PE6.declarationGroup.exploitants.exploitant1.exploitantNomUsage111542 ?
					$ds067PE6.declarationGroup.exploitants.exploitant1.exploitantNomUsage111542 : $ds067PE6.declarationGroup.exploitants.exploitant1.exploitantNomNaissance111542 ) +' '
					+ $ds067PE6.declarationGroup.exploitants.exploitant1.exploitantPrenom111542
,mandatory:"true"})};


//exploitant 2
if($ds067PE6.declarationGroup.exploitants.exploitant1.exploitant2OUI){
attachment('pjIDExploitant2', 'pjIDExploitant2', 
		{ label : ($ds067PE6.declarationGroup.exploitants.exploitant2.exploitantNomUsage211542 ?
					$ds067PE6.declarationGroup.exploitants.exploitant2.exploitantNomUsage211542 : $ds067PE6.declarationGroup.exploitants.exploitant2.exploitantNomNaissance211542 ) +' '
					+ $ds067PE6.declarationGroup.exploitants.exploitant2.exploitantPrenom211542
, mandatory:"true"})};


if($ds067PE6.declarationGroup.exploitants.exploitant2.exploitantPermisExploitation211542){
	attachment('pjPermisExploitationDebitBoissonsExploitant2', 'pjPermisExploitationDebitBoissonsExploitant2', 
		{ label : ($ds067PE6.declarationGroup.exploitants.exploitant2.exploitantNomUsage211542 ?
					$ds067PE6.declarationGroup.exploitants.exploitant2.exploitantNomUsage211542 : $ds067PE6.declarationGroup.exploitants.exploitant2.exploitantNomNaissance211542 ) +' '
					+ $ds067PE6.declarationGroup.exploitants.exploitant2.exploitantPrenom211542
, mandatory:"true"})};



if($ds067PE6.declarationGroup.exploitants.exploitant2.exploitantPermisVenteBoisson211542){
	attachment('pjPermisExploitationLaNuitExploitant2', 'pjPermisExploitationLaNuitExploitant2', 
		{ label : ($ds067PE6.declarationGroup.exploitants.exploitant2.exploitantNomUsage211542 ?
					$ds067PE6.declarationGroup.exploitants.exploitant2.exploitantNomUsage211542 : $ds067PE6.declarationGroup.exploitants.exploitant2.exploitantNomNaissance211542 ) +' '
					+ $ds067PE6.declarationGroup.exploitants.exploitant2.exploitantPrenom211542
, mandatory:"true"})};
