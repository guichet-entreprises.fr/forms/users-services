var formFields = {};

formFields['premiereDeclaration']                         = (Value('id').of($ds004PE1.declarationGroup.declaration.actual).eq('premiereDemande') ? true : false);
formFields['actualisation']                         	  = (Value('id').of($ds004PE1.declarationGroup.declaration.actual).eq('actualisation') ? true : false);

//Identification établissement

var identEtab = $ds004PE1.identificationGroup.identificationEtablissement;
var identDeclar = $ds004PE1.identificationGroup.identificationDeclarant;

formFields['numeroSiret']                                 = identEtab.numeroSiret;
formFields['raisonSociale']                               = identEtab.raisonSociale;
formFields['adresseEtablissement']                        = identEtab.numeroLibelleAdresseDeclarant + (identEtab.complementAdresseDeclarant != null ? ', ' + identEtab.complementAdresseDeclarant : '');
formFields['codePostal']                                  = identEtab.codePostal;
formFields['commune']                                     = identEtab.commune;
formFields['nom']                                         = identDeclar.nom;
formFields['prenom']                                      = identDeclar.prenom;
formFields['telephoneFixe']                               = identDeclar.telephoneFixe;
formFields['telephoneMobile']                             = identDeclar.telephoneMobile;
formFields['adresseMail']                                 = identDeclar.adresseMail;

//Nature de l'activité

var natActChemin = $ds004PE1.activite.natureActivite;

formFields['restaurationColl']                            = natActChemin.restaurationColl;
formFields['restCollNBrepasPrep']                         = natActChemin.restCollGroup.restCollNBrepasPrep;
formFields['restCollNBrepasServ']                         = natActChemin.restCollGroup.restCollNBrepasServ;
formFields['coordonneesCuisineCentrale']                  = natActChemin.restCollGroup.coordonneesCuisineCentrale;
formFields['restaurationComm']                            = natActChemin.restaurationComm;
formFields['restCommNbPlacesAssises']                     = natActChemin.restCommGroup.restCommNbPlacesAssises;
formFields['artisan']                        			  = natActChemin.artisan;
formFields['artisanMetiersBouche']                        = natActChemin.artisanMetiersBoucheGroup.artisanMetiersBouche;
formFields['laboSansLocalVente']                          = (natActChemin.artisan ? (natActChemin.artisanMetiersBoucheGroup.laboLocalVente ? false : true) : null);
formFields['laboAvecLocalVente']                          = (natActChemin.artisan ? (natActChemin.artisanMetiersBoucheGroup.laboLocalVente ? true : false) : null);
formFields['pointVente']                                  = natActChemin.pointVente;
formFields['pointVenteTypeComm']                          = natActChemin.pointVenteGroup.pointVenteTypeComm;
formFields['producteur']                                  = natActChemin.producteur;
formFields['producteurFiliere']                           = natActChemin.producteurGroup.producteurFiliere;
formFields['marches']                                     = natActChemin.marches;
formFields['lieuxImplentation']                           = natActChemin.marchesGroup.lieuxImplentation;
formFields['vehiculesBoutiques']                          = natActChemin.vehiculesBoutiques;
formFields['vehicules']                                   = (natActChemin.vehiculesBoutiquesGroup.vehiculesNombre != null ? natActChemin.vehiculesBoutiquesGroup.vehiculesNombre + " véhicules, " : '')
															+ (natActChemin.vehiculesBoutiquesGroup.vehiculesLieux != null ? natActChemin.vehiculesBoutiquesGroup.vehiculesLieux : '');
formFields['distributeurAuto']                            = natActChemin.distributeurAuto;
formFields['produitDistribAuto']                          = natActChemin.distributeurAutoGroup.produitDistribAuto;
formFields['entreposage']                                 = natActChemin.entreposage;
formFields['denreesEntreposees']                          = natActChemin.entreposageGroup.denreesEntreposees;
formFields['transport']                                   = natActChemin.transport;
formFields['precisionTransport']                          = natActChemin.transportGroup.precisionTransport;
formFields['autre']                                       = natActChemin.autre;
formFields['autreActivite']                               = natActChemin.autreActiviteGroup.autreActivite;
formFields['venteLigneOui']                               = (natActChemin.venteLigne ? true : false);
formFields['venteLigneNon']                               = (natActChemin.venteLigne ? false : true);

//Procédés mis en oeuvre

var procedChemin = $ds004PE1.procedesGroup.procedes;

formFields['pasteurisation']                               = procedChemin.pasteurisation;
formFields['fumaison']                                    = procedChemin.fumaison;
formFields['cuissonVide']                                 = procedChemin.cuissonVide;
formFields['cuissonBasseTemp']                            = procedChemin.cuissonBasseTemp;
formFields['congelation']                                 = procedChemin.congelation;
formFields['decongelation']                               = procedChemin.decongelation;
formFields['recongelation']                               = procedChemin.recongelation;
formFields['fromagesLaitCru']                             = procedChemin.fromagesLaitCru;
formFields['fromagesAffines']                             = procedChemin.fromagesAffines;
formFields['fabricationViandeHE']                         = procedChemin.fabricationViandeHE;
formFields['abattageVolailles']                           = procedChemin.abattageVolailles;
formFields['decoupeVolaillesOui']                         = (procedChemin.abattageVolailles ? (procedChemin.abattageVolaillesGroup.decoupeVolailles ? true : false) : null);
formFields['decoupeVolaillesNon']                         = (procedChemin.abattageVolailles ? (procedChemin.abattageVolaillesGroup.decoupeVolailles ? false : true) : null);
formFields['transfoVolaillesOui']                         = (procedChemin.abattageVolailles ? (procedChemin.abattageVolaillesGroup.transfoVolailles ? true : false) : null);
formFields['transfoVolaillesNon']                         = (procedChemin.abattageVolailles ? (procedChemin.abattageVolaillesGroup.transfoVolailles ? false : true) : null);
formFields['livraisonOui']                                = (procedChemin.abattageVolailles ? (procedChemin.abattageVolaillesGroup.livraison ? true : false) : null);
formFields['livraisonNon']                                = (procedChemin.abattageVolailles ? (procedChemin.abattageVolaillesGroup.livraison ? false : true) : null);
formFields['listeEtablissementsLivres']                   = procedChemin.abattageVolaillesGroup.listeEtablissementsLivres;

//Nature des produits

var natProdChemin = $ds004PE1.natureProduitsGroup.natureProduits;

formFields['viandeBoucherie']                             = natProdChemin.viandeBoucherie;
formFields['viandeVolailles']                             = natProdChemin.viandeVolailles;
formFields['viandeLapins']                                = natProdChemin.viandeLapins;
formFields['viandePetitGibier']                           = natProdChemin.viandePetitGibier;
formFields['viandeGrosGibier']                            = natProdChemin.viandeGrosGibier;
formFields['viandeHE']                                    = natProdChemin.viandeHE;
formFields['poissons']                                    = natProdChemin.poissons;
formFields['produitsTransfoViande']                       = natProdChemin.produitsTransfoViande;
formFields['produitsTransfoPoissons']                     = natProdChemin.produitsTransfoPoissons;
formFields['coquillages']                                 = natProdChemin.coquillages;
formFields['laitCru']                                     = natProdChemin.laitCru;
formFields['fromages']                                    = natProdChemin.fromages;
formFields['oeufs']                                       = natProdChemin.oeufs;
formFields['patisseries']                                 = natProdChemin.patisseries;
formFields['platsCuisines']                               = natProdChemin.platsCuisines;
formFields['autresProduitsComm']                          = natProdChemin.autresProduitsComm;
formFields['listeAutresProduitsComm']                     = natProdChemin.autresProduitsCommGroup.listeAutresProduitsComm;

//Signature

var signChemin = $ds004PE1.signaturegroup.signature;
var civNomPrenomSign = $ds004PE1.identificationGroup.identificationSignataire.civiliteSignataire + " " + $ds004PE1.identificationGroup.identificationSignataire.nomSignataire + ' ' + $ds004PE1.identificationGroup.identificationSignataire.prenomSignataire + ',';
var civNomPrenomDeclar = identDeclar.civilite + " " + identDeclar.nom + ' ' + identDeclar.prenom + ',';
var civNomPrenom = (Value('id').of($ds004PE1.declarationGroup.identification.choixSignataire).eq('representant') ? civNomPrenomSign : civNomPrenomDeclar);

formFields['civiliteNomPrenom']                           = civNomPrenom;
formFields['dateSignature']                               = signChemin.dateSignature;
formFields['certifieExactitude']                          = signChemin.certifieExactitude;
formFields['certifiePouvoir']                             = signChemin.certifiePouvoir;
formFields['engagement']                                  = signChemin.engagement;



var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		dateSignature: signChemin.dateSignature,
		autoriteHabilitee :"DDCSPP ",
		demandeContexte : "Déclaration pour la vente de denrées animales ou d'origine animale.",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
    .load('models/cerfa 13984-3.pdf') //
    .apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustifDomicile);


function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Boucher_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Boucher - Déclaration pour  la vente de denrées animales ou d\'origine animale ',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du courrier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration pour  la vente de denrées animales ou d\'origine animale ',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});

