var formFields = {};
//etatCivil

var civNomPrenom = $ds004PE2.etatCivil.identificationDeclarant.nom + ' ' + $ds004PE2.etatCivil.identificationDeclarant.prenom;


//etat civil
formFields['civiliteMadame']				 = Value('id').of($ds004PE2.etatCivil.identificationDeclarant.civilite).eq('madame') ? "======" : '';
formFields['civiliteMonsieur']				 = Value('id').of($ds004PE2.etatCivil.identificationDeclarant.civilite).eq('monsieur') ? "===========" : '';
formFields['nomPrenomDeclarant']             = $ds004PE2.etatCivil.identificationDeclarant.nom + ' ' + $ds004PE2.etatCivil.identificationDeclarant.prenom;
formFields['civiliteMadameRJ']				 = Value('id').of($ds004PE2.etatCivil.identificationDeclarant.identificationDeclarant2.civiliteRJ).eq('madameRJ') ? "======" : '';
formFields['civiliteMonsieurRJ']			 = Value('id').of($ds004PE2.etatCivil.identificationDeclarant.identificationDeclarant2.civiliteRJ).eq('monsieurRJ') ? "===========" : '';
formFields['nomPrenomResponsableJuridique']  = ($ds004PE2.etatCivil.identificationDeclarant.identificationDeclarant2.nom1 != null ? $ds004PE2.etatCivil.identificationDeclarant.identificationDeclarant2.nom1 : '') + ' ' + ($ds004PE2.etatCivil.identificationDeclarant.identificationDeclarant2.prenom1 != null ? $ds004PE2.etatCivil.identificationDeclarant.identificationDeclarant2.prenom1 : '') ;

//coordonnees

formFields['numeroNomVoieDeclarant']         = $ds004PE2.etatCivil.identificationDeclarant.coordonnees.numeroLibelleAdresseDeclarant;
formFields['codePostalCommuneDeclarant']     = ($ds004PE2.etatCivil.identificationDeclarant.coordonnees.codePostal != null ? $ds004PE2.etatCivil.identificationDeclarant.coordonnees.codePostal + ' ' : '')
												+ $ds004PE2.etatCivil.identificationDeclarant.coordonnees.commune;
formFields['telephoneDeclarant']             = $ds004PE2.etatCivil.identificationDeclarant.coordonnees.telephone;


//etablissement
formFields['raisonSociale']                  = $ds004PE2.identificationEtablissement.identificationEtablissement.raisonSociale;
formFields['siretEtablissement']             = $ds004PE2.identificationEtablissement.identificationEtablissement.numeroSiret;
formFields['numeroNomVoieEtablissement']     = $ds004PE2.identificationEtablissement.identificationEtablissement.numeroLibelleAdresseEtablissement 
											 +' '+ ($ds004PE2.identificationEtablissement.identificationEtablissement.complementAdresseEtablissement != null ? $ds004PE2.identificationEtablissement.identificationEtablissement.complementAdresseEtablissement : '');
formFields['codePostalCommuneEtablissement'] = ($ds004PE2.identificationEtablissement.identificationEtablissement.codePostalEtablissement !=null ? $ds004PE2.identificationEtablissement.identificationEtablissement.codePostalEtablissement + ' ' : '') 
											   + $ds004PE2.identificationEtablissement.identificationEtablissement.communeEtablissement;
formFields['telephoneEtablissement']         = $ds004PE2.identificationEtablissement.identificationEtablissement.telephonefixeEtablissement;



// signature
formFields['lieuSignature']                  = $ds004PE2.signaturegroup.signature.lieuSignature;
formFields['dateSignature']                  = $ds004PE2.signaturegroup.signature.dateSignature;
formFields['signatureCoche']                 = $ds004PE2.signaturegroup.signature.signatureCoche;
formFields['signatureCocheResponsable']      = $ds004PE2.signaturegroup.signature.signaturegroupResponsable.signatureResponsable;
formFields['NomPrenomResponsableSignature']  = ($ds004PE2.etatCivil.identificationDeclarant.identificationDeclarant2.civiliteRJ != null ? $ds004PE2.etatCivil.identificationDeclarant.identificationDeclarant2.civiliteRJ + ' ' : '') 
												+ ($ds004PE2.etatCivil.identificationDeclarant.identificationDeclarant2.nom1 != null ? $ds004PE2.etatCivil.identificationDeclarant.identificationDeclarant2.nom1 : '') 
												+' ' + ($ds004PE2.etatCivil.identificationDeclarant.identificationDeclarant2.prenom1 != null ? $ds004PE2.etatCivil.identificationDeclarant.identificationDeclarant2.prenom1 : '');
formFields['signatureCoche1']                 = $ds004PE2.signaturegroup.signature.engagement;
formFields['signatureCoche2']                 = $ds004PE2.signaturegroup.signature.engagement2;




/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
 
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp085PE3.signature.signature.lieuSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		date: $ds004PE2.signaturegroup.signature.dateSignature,
		autoriteHabilitee :"DDCSPP ",
		demandeContexte : "Demande d'autorisation pour le désossage de colonne vertébrale de bovins de plus de 30 mois.",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
    .load('models/Formulairedesossage.pdf') //
    .apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Boucher_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Boucher - Demande d\'autorisation pour le désossage de colonne vertébrale de bovins de plus de 30 mois',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du courrier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande d\'autorisation pour le désossage de colonne vertébrale de bovins de plus de 30 mois ',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});


