function pad(s) { return (s < 10) ? '0' + s : s; }

var cerfaFields = {};

var civNomPrenom = $ds046PE2.identificationEntreprise.identificationEntreprise.representantLegal.civilite+ ' ' +$ds046PE2.identificationEntreprise.identificationEntreprise.representantLegal.nomRepresentantLegalEntreprise+" "+$ds046PE2.identificationEntreprise.identificationEntreprise.representantLegal.prenomRepresentantLegalEntreprise;


cerfaFields['civiliteNomPrenom']                 = $ds046PE2.identificationEntreprise.identificationEntreprise.civilite+" "+$ds046PE2.identificationEntreprise.identificationEntreprise.representantLegal.nomRepresentantLegalEntreprise+" "+$ds046PE2.identificationEntreprise.identificationEntreprise.representantLegal.prenomRepresentantLegalEntreprise;
cerfaFields['denominationEntreprise']            = $ds046PE2.identificationEntreprise.identificationEntreprise.denominationEntreprise;
cerfaFields['sa']                                = Value('id').of($ds046PE2.identificationEntreprise.identificationEntreprise.formeJuridiqueEntreprise).eq('sa') ? true:false;
cerfaFields['sarl']                              = Value('id').of($ds046PE2.identificationEntreprise.identificationEntreprise.formeJuridiqueEntreprise).eq('sarl') ? true:false;
cerfaFields['sas']                               = Value('id').of($ds046PE2.identificationEntreprise.identificationEntreprise.formeJuridiqueEntreprise).eq('sas') ? true:false;
cerfaFields['eurl']                              = Value('id').of($ds046PE2.identificationEntreprise.identificationEntreprise.formeJuridiqueEntreprise).eq('eurl') ? true:false;
cerfaFields['autreEntreprise']                   = $ds046PE2.identificationEntreprise.identificationEntreprise.autreEntreprise;
cerfaFields['numeroSiretEntreprise']             = $ds046PE2.identificationEntreprise.identificationEntreprise.numeroSiretEntreprise;
cerfaFields['codeApe']                           = $ds046PE2.identificationEntreprise.identificationEntreprise.codeApe;
cerfaFields['mmeEntreprise']                     = Value('id').of($ds046PE2.identificationEntreprise.identificationEntreprise.representantLegal.civilite).eq('mmeEntreprise') ? true:false;
cerfaFields['monsieurEntreprise']                = Value('id').of($ds046PE2.identificationEntreprise.identificationEntreprise.representantLegal.civilite).eq('monsieurEntreprise') ? true:false;
cerfaFields['nomRepresentantLegalEntreprise']    = $ds046PE2.identificationEntreprise.identificationEntreprise.representantLegal.nomRepresentantLegalEntreprise;
cerfaFields['prenomRepresentantLegalEntreprise'] = $ds046PE2.identificationEntreprise.identificationEntreprise.representantLegal.prenomRepresentantLegalEntreprise;
cerfaFields['adresseSiegeSocialEntreprise']      = $ds046PE2.identificationEntreprise.identificationEntreprise.adresseSiegeSocialEntreprise.numeroLibelleVoieEntreprise+" "+$ds046PE2.identificationEntreprise.identificationEntreprise.adresseSiegeSocialEntreprise.complementAdresseEntreprise;
cerfaFields['codePostalEntreprise']              = $ds046PE2.identificationEntreprise.identificationEntreprise.adresseSiegeSocialEntreprise.codePostalEntreprise;
cerfaFields['communeEntreprise']                 = $ds046PE2.identificationEntreprise.identificationEntreprise.adresseSiegeSocialEntreprise.communeEntreprise;

cerfaFields['mmeExploitant']                     = Value('id').of($ds046PE2.identificationExploitant.identificationExploitant.civiliteExploitant).eq('mmeExploitant') ? true:false;
cerfaFields['monsieurExploitant']                = Value('id').of($ds046PE2.identificationExploitant.identificationExploitant.civiliteExploitant).eq('monsieurExploitant') ? true:false;
cerfaFields['nomExploitant']                     = $ds046PE2.identificationExploitant.identificationExploitant.nomExploitant;           
cerfaFields['prenomExploitant']                  = $ds046PE2.identificationExploitant.identificationExploitant.prenomExploitant;
cerfaFields['statutExploitant']                  = $ds046PE2.identificationExploitant.identificationExploitant.statutExploitant;
cerfaFields['courrielExploitant']                = $ds046PE2.identificationExploitant.identificationExploitant.courrielExploitant;
cerfaFields['adresseExploitant']                 = $ds046PE2.identificationExploitant.identificationExploitant.adresseExploitant.numeroLibelleVoieExploitant+" "+$ds046PE2.identificationExploitant.identificationExploitant.adresseExploitant.complementAdresseExploitant+" ";
cerfaFields['codePostalExploitant']              = $ds046PE2.identificationExploitant.identificationExploitant.adresseExploitant.codePostalExploitant;
cerfaFields['communeExploitant']                 = $ds046PE2.identificationExploitant.identificationExploitant.adresseExploitant.communeExploitant;


cerfaFields['numeroIdentifiantEtablissement']          = $ds046PE2.identificationEtablissement.identificationEtablissement.numeroIdentifiantEtablissement;
cerfaFields['nomCommercialEtablissement']              = $ds046PE2.identificationEtablissement.identificationEtablissement.nomCommercialEtablissement;
cerfaFields['adresseEtablissement']                    = $ds046PE2.identificationEtablissement.identificationEtablissement.adresseEtablissement.numeroLibelleVoieEtablissement+" "+$ds046PE2.identificationEtablissement.identificationEtablissement.adresseEtablissement.complementAdresseEtablissement+" ";
cerfaFields['codePostalEtablissement']                 = $ds046PE2.identificationEtablissement.identificationEtablissement.adresseEtablissement.codePostalEtablissement;
cerfaFields['communeEtablissement']                    = $ds046PE2.identificationEtablissement.identificationEtablissement.adresseEtablissement.communeEtablissement;
cerfaFields['telephoneEtablissement']                  = $ds046PE2.identificationEtablissement.identificationEtablissement.telephoneEtablissement;
cerfaFields['telecopieEtablissement']                  = $ds046PE2.identificationEtablissement.identificationEtablissement.telecopieEtablissement;
cerfaFields['courrielEtablissement']                   = $ds046PE2.identificationEtablissement.identificationEtablissement.courrielEtablissement;
cerfaFields['siteInternetEtablissement']               = $ds046PE2.identificationEtablissement.identificationEtablissement.siteInternetEtablissement;

cerfaFields['classementActuelEtoiles']                 = $ds046PE2.natureDemande.natureDemande.classementActuelEtoiles;
cerfaFields['classementDemandeEtoiles']                = $ds046PE2.natureDemande.natureDemande.classementDemandeEtoiles;
cerfaFields['nonClasseEtoiles']                        = Value('id').of($ds046PE2.natureDemande.natureDemande.classementActuel).eq('nonClasseEtoiles');

cerfaFields['dateConstructionJour']    = '';

cerfaFields['dateConstructionMois']     = '';

cerfaFields['dateConstructionAnnee']    = '';





if($ds046PE2.descriptionEtablissement.descriptionEtablissement.dateConstruction != null) {

var dateTemp = new Date(parseInt ($ds046PE2.descriptionEtablissement.descriptionEtablissement.dateConstruction.getTimeInMillis()));

      var monthTemp = dateTemp.getMonth() + 1;

       var month = pad(monthTemp.toString());

      var day =  pad(dateTemp.getDate().toString());

       var year = dateTemp.getFullYear();

    cerfaFields['dateConstructionJour']    =  day;

    cerfaFields['dateConstructionMois']     = month;

    cerfaFields['dateConstructionAnnee']    =  year;

}





cerfaFields['etablissementPermanentOui']               = Value('id').of($ds046PE2.descriptionEtablissement.descriptionEtablissement.etablissementPermanent).eq('etablissementPermanentOui');
cerfaFields['etablissementPermanentNon']               = Value('id').of($ds046PE2.descriptionEtablissement.descriptionEtablissement.etablissementPermanent).eq('etablissementPermanentNon');
cerfaFields['etablissementSaisonnierOui']              = Value('id').of($ds046PE2.descriptionEtablissement.descriptionEtablissement.etablissementSaisonnier).eq('etablissementSaisonnierOui');
cerfaFields['etablissementSaisonnierNon']              = Value('id').of($ds046PE2.descriptionEtablissement.descriptionEtablissement.etablissementSaisonnier).eq('etablissementSaisonnierNon');
cerfaFields['nombreDeChambres']                        = $ds046PE2.descriptionEtablissement.descriptionEtablissement.nombreDeChambres;
cerfaFields['nombrePersonnesAcceuillis']               = $ds046PE2.descriptionEtablissement.descriptionEtablissement.nombrePersonnesAcceuillis;
cerfaFields['nombreEmployesEnFonction']                = $ds046PE2.descriptionEtablissement.descriptionEtablissement.nombreEmployesEnFonction;
cerfaFields['periodesOuverture']                       = $ds046PE2.descriptionEtablissement.descriptionEtablissement.periodesOuverture;
cerfaFields['affiliationChaineVolontaireOui']          = Value('id').of($ds046PE2.descriptionEtablissement.descriptionEtablissement.affiliationChaineIntegree2).eq('AffiliationChaineIntegreeOui');
cerfaFields['affiliationChaineVolontaireNon']          = Value('id').of($ds046PE2.descriptionEtablissement.descriptionEtablissement.affiliationChaineIntegree2).eq('AffiliationChaineIntegreeNon');
cerfaFields['AffiliationChaineIntegreeOui']            = Value('id').of($ds046PE2.descriptionEtablissement.descriptionEtablissement.affiliationChaineIntegree).eq('AffiliationChaineIntegreeOui');
cerfaFields['AffiliationChaineIntegreeNon']            = Value('id').of($ds046PE2.descriptionEtablissement.descriptionEtablissement.affiliationChaineIntegree).eq('AffiliationChaineIntegreeNon');
cerfaFields['marqueOuLabel']                           = $ds046PE2.descriptionEtablissement.descriptionEtablissement.marqueOuLabel;

cerfaFields['chambreStandard']                         = Value('id').of($ds046PE2.typologieDesChambres.typologieDesChambres.etablissementPermanent).contains('chambreStandard');
cerfaFields['chambreClassique']                        = Value('id').of($ds046PE2.typologieDesChambres.typologieDesChambres.etablissementPermanent).contains('chambreClassique');
cerfaFields['chambreTradition']                        = Value('id').of($ds046PE2.typologieDesChambres.typologieDesChambres.etablissementPermanent).contains('chambreTradition');
cerfaFields['chambreExecutive']                        = Value('id').of($ds046PE2.typologieDesChambres.typologieDesChambres.etablissementPermanent).contains('chambreExecutive');
cerfaFields['chambreSuperieure']                       = Value('id').of($ds046PE2.typologieDesChambres.typologieDesChambres.etablissementPermanent).contains('chambreSuperieure');
cerfaFields['chambrePrivilege']                        = Value('id').of($ds046PE2.typologieDesChambres.typologieDesChambres.etablissementPermanent).contains('chambrePrivilege');
cerfaFields['chambreFamiliale']                        = Value('id').of($ds046PE2.typologieDesChambres.typologieDesChambres.etablissementPermanent).contains('chambreFamiliale');
cerfaFields['chambreLuxe']                             = Value('id').of($ds046PE2.typologieDesChambres.typologieDesChambres.etablissementPermanent).contains('chambreLuxe');
cerfaFields['suiteJunior']                             = Value('id').of($ds046PE2.typologieDesChambres.typologieDesChambres.etablissementPermanent).contains('suiteJunior');
cerfaFields['suiteClassique']                          = Value('id').of($ds046PE2.typologieDesChambres.typologieDesChambres.etablissementPermanent).contains('suiteClassique');
cerfaFields['suiteSuperieure']                         = Value('id').of($ds046PE2.typologieDesChambres.typologieDesChambres.etablissementPermanent).contains('suiteSuperieure');
cerfaFields['suiteExecutive']                          = Value('id').of($ds046PE2.typologieDesChambres.typologieDesChambres.etablissementPermanent).contains('suiteExecutive');
cerfaFields['suiteDeLuxe']                             = Value('id').of($ds046PE2.typologieDesChambres.typologieDesChambres.etablissementPermanent).contains('suiteDeLuxe');
cerfaFields['suitePresidentielle']                     = Value('id').of($ds046PE2.typologieDesChambres.typologieDesChambres.etablissementPermanent).contains('suitePresidentielle');
cerfaFields['autreChambre']                            = Value('id').of($ds046PE2.typologieDesChambres.typologieDesChambres.etablissementPermanent).contains('autreChambre');

cerfaFields['nomOrganismeEvaluateur']                    = $ds046PE2.identificationOrganismeEvaluateur.identificationOrganismeEvaluateur.nomOrganismeEvaluateur;
cerfaFields['numeroAccreditationOrganismeEvaluateur']    = $ds046PE2.identificationOrganismeEvaluateur.identificationOrganismeEvaluateur.numeroAccreditationOrganismeEvaluateur; 



cerfaFields['departementPrefecture']                     = $ds046PE2.signature.signature.departementPrefecture;
cerfaFields['faitA']                                     = $ds046PE2.signature.signature.faitA;
cerfaFields['faitLe']                                    = $ds046PE2.signature.signature.faitLe;
cerfaFields['signature']                                 = $ds046PE2.signature.signature.signature;




















/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $ds046PE2.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier_au_premier_dossier_v2.1_GE.pdf') //
	.apply({
		dateSignature: $ds046PE2.signature.signature.faitLe,
		autoriteHabilitee :"Atout France",
		demandeContexte : "Demande préfectorale pour le classement de l'hôtel",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
 
 
 
var cerfaDoc = nash.doc //
	.load('models/cerfa 12000-03.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.RapportControlConformeDispositionsReglementaires);
appendPj($attachmentPreprocess.attachmentPreprocess.GrilleConformeDispositionsReglementaires);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('demande_classement_prefectoral.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Demande préfectorale pour le classement de l\'hôtel',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande préfectorale pour le classement de l\'hôtel',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
