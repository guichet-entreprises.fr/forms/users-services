attachment('pjID', 'pjID');
attachment('pjExtraitRegistre', 'pjExtraitRegistre');
attachment('pjCopiePropriete', 'pjCopiePropriete');

var pj=$ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireFond.questionPersonnalite;
if(Value('id').of(pj).contains('personneMorale')) {
    attachment('pjPVAssemble', 'pjPVAssemble', { mandatory:"true"});
}

var pj=$ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireFond.questionProprietaire;
if(pj == true) {
    attachment('pjContratLocation', 'pjContratLocation', { mandatory:"true"});
}

var pj=$ds046PE01.cadre1.cadre1DeclarationHotel.etatCivil1.declarantExploitant;
if(pj == true) {
    attachment('pjDelegationPouvoir', 'pjDelegationPouvoir', { mandatory:"true"});
}