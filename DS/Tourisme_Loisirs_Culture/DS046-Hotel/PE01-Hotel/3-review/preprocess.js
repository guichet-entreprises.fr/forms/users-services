var formFields = {};

var civNomPrenom = $ds046PE01.cadre1.cadre1DeclarationHotel.etatCivil1.etatCivilNomPrenomDeclarant1;



//Etat civil du ou des déclarants
formFields['etatCivil_nomPrenomDeclarant1']                        = $ds046PE01.cadre1.cadre1DeclarationHotel.etatCivil1.etatCivilNomPrenomDeclarant1;
formFields['etatCivilDateNaissanceDeclarant1']                	   = $ds046PE01.cadre1.cadre1DeclarationHotel.etatCivil1.dateNaissanceDeclarant1;
formFields['etatCivilLieuNaissanceDeclarant1']                	   = $ds046PE01.cadre1.cadre1DeclarationHotel.etatCivil1.lieuNaissanceDeclarant1;
formFields['etatCivil_nationaliteDeclarant1']                      = $ds046PE01.cadre1.cadre1DeclarationHotel.etatCivil1.etatCivilNationaliteDeclarant1;
formFields['etatCivil_adressePersonnelleDeclarant1']               = ($ds046PE01.cadre1.cadre1DeclarationHotel.etatCivil1.adresse.numNomVoie != null ? $ds046PE01.cadre1.cadre1DeclarationHotel.etatCivil1.adresse.numNomVoie:'') + ' ' + ($ds046PE01.cadre1.cadre1DeclarationHotel.etatCivil1.adresse.complement != null ? $ds046PE01.cadre1.cadre1DeclarationHotel.etatCivil1.adresse.complement:'') + ' ' + ($ds046PE01.cadre1.cadre1DeclarationHotel.etatCivil1.adresse.codePostal != null ? $ds046PE01.cadre1.cadre1DeclarationHotel.etatCivil1.adresse.codePostal:'') + ' ' + ($ds046PE01.cadre1.cadre1DeclarationHotel.etatCivil1.adresse.commune != null ? $ds046PE01.cadre1.cadre1DeclarationHotel.etatCivil1.adresse.commune:'');
formFields['etatCivil_qualiteDeclarant1']                          = $ds046PE01.cadre1.cadre1DeclarationHotel.etatCivil1.etatCivilQualiteDeclarant1;
formFields['etatCivil_qualiteDeclarant2']                          = $ds046PE01.cadre1.cadre1DeclarationHotel.etatCivil2.etatCivilQualiteDeclarant2;
formFields['etatCivil_adressePersonnelleDeclarant2']               = ($ds046PE01.cadre1.cadre1DeclarationHotel.etatCivil2.adresse.numNomVoie != null ? $ds046PE01.cadre1.cadre1DeclarationHotel.etatCivil2.adresse.numNomVoie:'') + ' ' + ($ds046PE01.cadre1.cadre1DeclarationHotel.etatCivil2.adresse.complement != null ? $ds046PE01.cadre1.cadre1DeclarationHotel.etatCivil2.adresse.complement:'') + ' ' + ($ds046PE01.cadre1.cadre1DeclarationHotel.etatCivil2.adresse.codePostal != null ? $ds046PE01.cadre1.cadre1DeclarationHotel.etatCivil2.adresse.codePostal:'') + ' ' + ($ds046PE01.cadre1.cadre1DeclarationHotel.etatCivil2.adresse.commune != null ? $ds046PE01.cadre1.cadre1DeclarationHotel.etatCivil2.adresse.commune:'');
formFields['etatCivil_nationaliteDeclarant2']                      = $ds046PE01.cadre1.cadre1DeclarationHotel.etatCivil2.etatCivilNationaliteDeclarant2;
formFields['etatCivilDateNaissanceDeclarant2']                     = $ds046PE01.cadre1.cadre1DeclarationHotel.etatCivil2.dateNaissanceDeclarant2;
formFields['etatCivilLieuNaissanceDeclarant2']                     = $ds046PE01.cadre1.cadre1DeclarationHotel.etatCivil2.lieuNaissanceDeclarant2;
formFields['etatCivil_nomPrenomDeclarant2']                        = $ds046PE01.cadre1.cadre1DeclarationHotel.etatCivil2.etatCivilNomPrenomDeclarant2;

//Propriétaire du fond de commerce et d'immeuble
formFields['etatCivil_nomPrenom_proprietaireFond']                 = ($ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireFond.etatCivilNomPrenomProprietaireFond1 != null ? $ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireFond.etatCivilNomPrenomProprietaireFond1:'') + '' + ($ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireFond.etatCivilNomPrenomProprietaireFond2 != null ? $ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireFond.etatCivilNomPrenomProprietaireFond2:'');
formFields['etatCivil_nomPrenom_proprietaireImmeuble']             = ($ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireImmeuble.etatCivilNomPrenomProprietaireImmeuble1 != null ? $ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireImmeuble.etatCivilNomPrenomProprietaireImmeuble1:'') + '' + ($ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireImmeuble.etatCivilNomPrenomProprietaireImmeuble2 != null ? $ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireImmeuble.etatCivilNomPrenomProprietaireImmeuble2:'');
formFields['etatCivil_adresse_proprietaireFond']                   = ($ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireFond.adresse.numNomVoie != null ? $ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireFond.adresse.numNomVoie:'') + ' ' + ($ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireFond.adresse.complement != null ? $ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireFond.adresse.complement:'') + ' ' + ($ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireFond.adresse.codePostal != null ? $ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireFond.adresse.codePostal:'') + ' ' + ($ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireFond.adresse.commune != null ? $ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireFond.adresse.commune:'');
formFields['etatCivil_adresse_proprietaireImmeuble']               = ($ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireImmeuble.adresse.numNomVoie != null ? $ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireImmeuble.adresse.numNomVoie:'') + ' ' + ($ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireImmeuble.adresse.complement != null ? $ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireImmeuble.adresse.complement:'') + ' ' + ($ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireImmeuble.adresse.codePostal != null ? $ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireImmeuble.adresse.codePostal:'') + ' ' + ($ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireImmeuble.adresse.commune != null ? $ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireImmeuble.adresse.commune:'');
formFields['etatCivil_nomPrenomRepresentant_proprietaireImmeuble'] = $ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireImmeuble.etatCivilNomPrenomRepresentantProprietaireImmeuble;
formFields['etatCivil_nomPrenomRepresentant_proprietaireFond']     = $ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireFond.etatCivilNomPrenomRepresentantProprietaireFond;

formFields['etablissement_enseigne']                               = $ds046PE01.cadre1.cadre1DeclarationHotel.renseignementEtablissement.etablissementEnseigne;
formFields['etablissement_nbreTotalChambres']                      = $ds046PE01.cadre1.cadre1DeclarationHotel.renseignementEtablissement.etablissementNbreTotalChambres;
formFields['etablissement_adresseHotel']                           = ($ds046PE01.cadre1.cadre1DeclarationHotel.renseignementEtablissement.adresse.numNomVoie != null ? $ds046PE01.cadre1.cadre1DeclarationHotel.renseignementEtablissement.adresse.numNomVoie:'') + ' ' + ($ds046PE01.cadre1.cadre1DeclarationHotel.renseignementEtablissement.adresse.complement != null ? $ds046PE01.cadre1.cadre1DeclarationHotel.renseignementEtablissement.adresse.complement:'') + ' ' + ($ds046PE01.cadre1.cadre1DeclarationHotel.renseignementEtablissement.adresse.codePostal != null ? $ds046PE01.cadre1.cadre1DeclarationHotel.renseignementEtablissement.adresse.codePostal:'') + ' ' + ($ds046PE01.cadre1.cadre1DeclarationHotel.renseignementEtablissement.adresse.commune != null ? $ds046PE01.cadre1.cadre1DeclarationHotel.renseignementEtablissement.adresse.commune:'');
formFields['etablissement_nomPrenomResponsable']                   = $ds046PE01.cadre1.cadre1DeclarationHotel.renseignementEtablissement.etablissementNomPrenomResponsable;
formFields['etablissement_telephoneFixe']                          = $ds046PE01.cadre1.cadre1DeclarationHotel.renseignementEtablissement.etablissementTelephoneFixe;
formFields['etablissement_telephonePortable']                      = $ds046PE01.cadre1.cadre1DeclarationHotel.renseignementEtablissement.etablissementTelephonePortable;
formFields['etablissement_courrielPersonnel']                      = $ds046PE01.cadre1.cadre1DeclarationHotel.renseignementEtablissement.etablissementCourrielPersonnel;
formFields['etablissement_courrielHotel']                          = $ds046PE01.cadre1.cadre1DeclarationHotel.renseignementEtablissement.etablissementCourrielHotel;


formFields['hotelCoche']                                           = true;
formFields['prefecture']                                           = $ds046PE01.cadre1.cadre1DeclarationHotel.renseignementEtablissement.adresse.commune;
formFields['lieuSignature']                                        = $ds046PE01.cadre2.etatDesLieux.etatDesLieuxSignature.lieuSignature;
formFields['dateSignature']                                        = $ds046PE01.cadre2.etatDesLieux.etatDesLieuxSignature.dateSignature;
formFields['signatureDeclarant']                                   = 'Cette déclaration respecte les attendus de l’article 1367 du code civil.';


formFields['etage1']                                               = $ds046PE01.cadre2.etatDesLieux.chambre1.etage;
formFields['etage2']                                               = $ds046PE01.cadre2.etatDesLieux.chambre2.etage;
formFields['etage3']                                               = $ds046PE01.cadre2.etatDesLieux.chambre3.etage;
formFields['etage4']                                               = $ds046PE01.cadre2.etatDesLieux.chambre4.etage;
formFields['etage5']                                               = $ds046PE01.cadre2.etatDesLieux.chambre5.etage;
formFields['etage6']                                               = $ds046PE01.cadre2.etatDesLieux.chambre6.etage;
formFields['etage7']                                               = $ds046PE01.cadre2.etatDesLieux.chambre7.etage;
formFields['etage8']                                               = $ds046PE01.cadre2.etatDesLieux.chambre8.etage;
formFields['etage9']                                               = $ds046PE01.cadre2.etatDesLieux.chambre9.etage;
formFields['etage10']                                              = $ds046PE01.cadre2.etatDesLieux.chambre10.etage;
formFields['etage11']                                              = $ds046PE01.cadre2.etatDesLieux.chambre11.etage;
formFields['etage12']                                              = $ds046PE01.cadre2.etatDesLieux.chambre12.etage;
formFields['etage13']                                              = $ds046PE01.cadre2.etatDesLieux.chambre13.etage;
formFields['etage14']                                              = $ds046PE01.cadre2.etatDesLieux.chambre14.etage;
formFields['etage15']                                              = $ds046PE01.cadre2.etatDesLieux.chambre15.etage;
formFields['etage16']                                              = $ds046PE01.cadre2.etatDesLieux.chambre16.etage;
formFields['etage17']                                              = $ds046PE01.cadre2.etatDesLieux.chambre17.etage;
formFields['etage18']                                              = $ds046PE01.cadre2.etatDesLieux.chambre18.etage;
formFields['etage19']                                              = $ds046PE01.cadre2.etatDesLieux.chambre19.etage;
formFields['etage20']                                              = $ds046PE01.cadre2.etatDesLieux.chambre20.etage;
formFields['etage21']                                              = $ds046PE01.cadre2.etatDesLieux.chambre21.etage;
formFields['etage22']                                              = $ds046PE01.cadre2.etatDesLieux.chambre22.etage;
formFields['etage23']                                              = $ds046PE01.cadre2.etatDesLieux.chambre23.etage;
formFields['etage24']                                              = $ds046PE01.cadre2.etatDesLieux.chambre24.etage;
formFields['etage25']                                              = $ds046PE01.cadre2.etatDesLieux.chambre25.etage;
formFields['etage26']                                              = $ds046PE01.cadre2.etatDesLieux.chambre26.etage;
formFields['etage27']                                              = $ds046PE01.cadre2.etatDesLieux.chambre27.etage;
formFields['etage28']                                              = $ds046PE01.cadre2.etatDesLieux.chambre28.etage;
formFields['numeroChambre1']                                       = $ds046PE01.cadre2.etatDesLieux.chambre1.numeroChambre;
formFields['numeroChambre2']                                       = $ds046PE01.cadre2.etatDesLieux.chambre2.numeroChambre;
formFields['numeroChambre3']                                       = $ds046PE01.cadre2.etatDesLieux.chambre3.numeroChambre;
formFields['numeroChambre4']                                       = $ds046PE01.cadre2.etatDesLieux.chambre4.numeroChambre;
formFields['numeroChambre5']                                       = $ds046PE01.cadre2.etatDesLieux.chambre5.numeroChambre;
formFields['numeroChambre6']                                       = $ds046PE01.cadre2.etatDesLieux.chambre6.numeroChambre;
formFields['numeroChambre7']                                       = $ds046PE01.cadre2.etatDesLieux.chambre7.numeroChambre;
formFields['numeroChambre8']                                       = $ds046PE01.cadre2.etatDesLieux.chambre8.numeroChambre;
formFields['numeroChambre9']                                       = $ds046PE01.cadre2.etatDesLieux.chambre9.numeroChambre;
formFields['numeroChambre10']                                      = $ds046PE01.cadre2.etatDesLieux.chambre10.numeroChambre;
formFields['numeroChambre11']                                      = $ds046PE01.cadre2.etatDesLieux.chambre11.numeroChambre;
formFields['numeroChambre12']                                      = $ds046PE01.cadre2.etatDesLieux.chambre12.numeroChambre;
formFields['numeroChambre13']                                      = $ds046PE01.cadre2.etatDesLieux.chambre13.numeroChambre;
formFields['numeroChambre14']                                      = $ds046PE01.cadre2.etatDesLieux.chambre14.numeroChambre;
formFields['numeroChambre15']                                      = $ds046PE01.cadre2.etatDesLieux.chambre15.numeroChambre;
formFields['numeroChambre16']                                      = $ds046PE01.cadre2.etatDesLieux.chambre16.numeroChambre;
formFields['numeroChambre17']                                      = $ds046PE01.cadre2.etatDesLieux.chambre17.numeroChambre;
formFields['numeroChambre18']                                      = $ds046PE01.cadre2.etatDesLieux.chambre18.numeroChambre;
formFields['numeroChambre19']                                      = $ds046PE01.cadre2.etatDesLieux.chambre19.numeroChambre;
formFields['numeroChambre20']                                      = $ds046PE01.cadre2.etatDesLieux.chambre20.numeroChambre;
formFields['numeroChambre21']                                      = $ds046PE01.cadre2.etatDesLieux.chambre21.numeroChambre;
formFields['numeroChambre22']                                      = $ds046PE01.cadre2.etatDesLieux.chambre22.numeroChambre;
formFields['numeroChambre23']                                      = $ds046PE01.cadre2.etatDesLieux.chambre23.numeroChambre;
formFields['numeroChambre24']                                      = $ds046PE01.cadre2.etatDesLieux.chambre24.numeroChambre;
formFields['numeroChambre25']                                      = $ds046PE01.cadre2.etatDesLieux.chambre25.numeroChambre;
formFields['numeroChambre26']                                      = $ds046PE01.cadre2.etatDesLieux.chambre26.numeroChambre;
formFields['numeroChambre27']                                      = $ds046PE01.cadre2.etatDesLieux.chambre27.numeroChambre;
formFields['numeroChambre28']                                      = $ds046PE01.cadre2.etatDesLieux.chambre28.numeroChambre;
formFields['superficieChambre1']                                   = $ds046PE01.cadre2.etatDesLieux.chambre1.superficieChambre;
formFields['superficieChambre2']                                   = $ds046PE01.cadre2.etatDesLieux.chambre2.superficieChambre;
formFields['superficieChambre3']                                   = $ds046PE01.cadre2.etatDesLieux.chambre3.superficieChambre;
formFields['superficieChambre4']                                   = $ds046PE01.cadre2.etatDesLieux.chambre4.superficieChambre;
formFields['superficieChambre5']                                   = $ds046PE01.cadre2.etatDesLieux.chambre5.superficieChambre;
formFields['superficieChambre6']                                   = $ds046PE01.cadre2.etatDesLieux.chambre6.superficieChambre;
formFields['superficieChambre7']                                   = $ds046PE01.cadre2.etatDesLieux.chambre7.superficieChambre;
formFields['superficieChambre8']                                   = $ds046PE01.cadre2.etatDesLieux.chambre8.superficieChambre;
formFields['superficieChambre9']                                   = $ds046PE01.cadre2.etatDesLieux.chambre9.superficieChambre;
formFields['superficieChambre10']                                  = $ds046PE01.cadre2.etatDesLieux.chambre10.superficieChambre;
formFields['superficieChambre11']                                  = $ds046PE01.cadre2.etatDesLieux.chambre11.superficieChambre;
formFields['superficieChambre12']                                  = $ds046PE01.cadre2.etatDesLieux.chambre12.superficieChambre;
formFields['superficieChambre13']                                  = $ds046PE01.cadre2.etatDesLieux.chambre13.superficieChambre;
formFields['superficieChambre14']                                  = $ds046PE01.cadre2.etatDesLieux.chambre14.superficieChambre;
formFields['superficieChambre15']                                  = $ds046PE01.cadre2.etatDesLieux.chambre15.superficieChambre;
formFields['superficieChambre16']                                  = $ds046PE01.cadre2.etatDesLieux.chambre16.superficieChambre;
formFields['superficieChambre17']                                  = $ds046PE01.cadre2.etatDesLieux.chambre17.superficieChambre;
formFields['superficieChambre18']                                  = $ds046PE01.cadre2.etatDesLieux.chambre18.superficieChambre;
formFields['superficieChambre19']                                  = $ds046PE01.cadre2.etatDesLieux.chambre19.superficieChambre;
formFields['superficieChambre20']                                  = $ds046PE01.cadre2.etatDesLieux.chambre20.superficieChambre;
formFields['superficieChambre21']                                  = $ds046PE01.cadre2.etatDesLieux.chambre21.superficieChambre;
formFields['superficieChambre22']                                  = $ds046PE01.cadre2.etatDesLieux.chambre22.superficieChambre;
formFields['superficieChambre23']                                  = $ds046PE01.cadre2.etatDesLieux.chambre23.superficieChambre;
formFields['superficieChambre24']                                  = $ds046PE01.cadre2.etatDesLieux.chambre24.superficieChambre;
formFields['superficieChambre25']                                  = $ds046PE01.cadre2.etatDesLieux.chambre25.superficieChambre;
formFields['superficieChambre26']                                  = $ds046PE01.cadre2.etatDesLieux.chambre26.superficieChambre;
formFields['superficieChambre27']                                  = $ds046PE01.cadre2.etatDesLieux.chambre27.superficieChambre;
formFields['superficieChambre28']                                  = $ds046PE01.cadre2.etatDesLieux.chambre28.superficieChambre;
formFields['longueur1']                                            = $ds046PE01.cadre2.etatDesLieux.chambre1.longueur;
formFields['longueur2']                                            = $ds046PE01.cadre2.etatDesLieux.chambre2.longueur;
formFields['longueur3']                                            = $ds046PE01.cadre2.etatDesLieux.chambre3.longueur;
formFields['longueur4']                                            = $ds046PE01.cadre2.etatDesLieux.chambre4.longueur;
formFields['longueur5']                                            = $ds046PE01.cadre2.etatDesLieux.chambre5.longueur;
formFields['longueur6']                                            = $ds046PE01.cadre2.etatDesLieux.chambre6.longueur;
formFields['longueur7']                                            = $ds046PE01.cadre2.etatDesLieux.chambre7.longueur;
formFields['longueur8']                                            = $ds046PE01.cadre2.etatDesLieux.chambre8.longueur;
formFields['longueur9']                                            = $ds046PE01.cadre2.etatDesLieux.chambre9.longueur;
formFields['longueur10']                                           = $ds046PE01.cadre2.etatDesLieux.chambre10.longueur;
formFields['longueur11']                                           = $ds046PE01.cadre2.etatDesLieux.chambre11.longueur;
formFields['longueur12']                                           = $ds046PE01.cadre2.etatDesLieux.chambre12.longueur;
formFields['longueur13']                                           = $ds046PE01.cadre2.etatDesLieux.chambre13.longueur;
formFields['longueur14']                                           = $ds046PE01.cadre2.etatDesLieux.chambre14.longueur;
formFields['longueur15']                                           = $ds046PE01.cadre2.etatDesLieux.chambre15.longueur;
formFields['longueur16']                                           = $ds046PE01.cadre2.etatDesLieux.chambre16.longueur;
formFields['longueur17']                                           = $ds046PE01.cadre2.etatDesLieux.chambre17.longueur;
formFields['longueur18']                                           = $ds046PE01.cadre2.etatDesLieux.chambre18.longueur;
formFields['longueur19']                                           = $ds046PE01.cadre2.etatDesLieux.chambre19.longueur;
formFields['longueur20']                                           = $ds046PE01.cadre2.etatDesLieux.chambre20.longueur;
formFields['longueur21']                                           = $ds046PE01.cadre2.etatDesLieux.chambre21.longueur;
formFields['longueur22']                                           = $ds046PE01.cadre2.etatDesLieux.chambre22.longueur;
formFields['longueur23']                                           = $ds046PE01.cadre2.etatDesLieux.chambre23.longueur;
formFields['longueur24']                                           = $ds046PE01.cadre2.etatDesLieux.chambre24.longueur;
formFields['longueur25']                                           = $ds046PE01.cadre2.etatDesLieux.chambre25.longueur;
formFields['longueur26']                                           = $ds046PE01.cadre2.etatDesLieux.chambre26.longueur;
formFields['longueur27']                                           = $ds046PE01.cadre2.etatDesLieux.chambre27.longueur;
formFields['longueur28']                                           = $ds046PE01.cadre2.etatDesLieux.chambre28.longueur;
formFields['largeur1']                                             = $ds046PE01.cadre2.etatDesLieux.chambre1.largeur;
formFields['largeur2']                                             = $ds046PE01.cadre2.etatDesLieux.chambre2.largeur;
formFields['largeur3']                                             = $ds046PE01.cadre2.etatDesLieux.chambre3.largeur;
formFields['largeur4']                                             = $ds046PE01.cadre2.etatDesLieux.chambre4.largeur;
formFields['largeur5']                                             = $ds046PE01.cadre2.etatDesLieux.chambre5.largeur;
formFields['largeur6']                                             = $ds046PE01.cadre2.etatDesLieux.chambre6.largeur;
formFields['largeur7']                                             = $ds046PE01.cadre2.etatDesLieux.chambre7.largeur;
formFields['largeur8']                                             = $ds046PE01.cadre2.etatDesLieux.chambre8.largeur;
formFields['largeur9']                                             = $ds046PE01.cadre2.etatDesLieux.chambre9.largeur;
formFields['largeur10']                                            = $ds046PE01.cadre2.etatDesLieux.chambre10.largeur;
formFields['largeur11']                                            = $ds046PE01.cadre2.etatDesLieux.chambre11.largeur;
formFields['largeur12']                                            = $ds046PE01.cadre2.etatDesLieux.chambre12.largeur;
formFields['largeur13']                                            = $ds046PE01.cadre2.etatDesLieux.chambre13.largeur;
formFields['largeur14']                                            = $ds046PE01.cadre2.etatDesLieux.chambre14.largeur;
formFields['largeur15']                                            = $ds046PE01.cadre2.etatDesLieux.chambre15.largeur;
formFields['largeur16']                                            = $ds046PE01.cadre2.etatDesLieux.chambre16.largeur;
formFields['largeur17']                                            = $ds046PE01.cadre2.etatDesLieux.chambre17.largeur;
formFields['largeur18']                                            = $ds046PE01.cadre2.etatDesLieux.chambre18.largeur;
formFields['largeur19']                                            = $ds046PE01.cadre2.etatDesLieux.chambre19.largeur;
formFields['largeur20']                                            = $ds046PE01.cadre2.etatDesLieux.chambre20.largeur;
formFields['largeur21']                                            = $ds046PE01.cadre2.etatDesLieux.chambre21.largeur;
formFields['largeur22']                                            = $ds046PE01.cadre2.etatDesLieux.chambre22.largeur;
formFields['largeur23']                                            = $ds046PE01.cadre2.etatDesLieux.chambre23.largeur;
formFields['largeur24']                                            = $ds046PE01.cadre2.etatDesLieux.chambre24.largeur;
formFields['largeur25']                                            = $ds046PE01.cadre2.etatDesLieux.chambre25.largeur;
formFields['largeur26']                                            = $ds046PE01.cadre2.etatDesLieux.chambre26.largeur;
formFields['largeur27']                                            = $ds046PE01.cadre2.etatDesLieux.chambre27.largeur;
formFields['largeur28']                                            = $ds046PE01.cadre2.etatDesLieux.chambre28.largeur;
formFields['hauteur1']                                             = $ds046PE01.cadre2.etatDesLieux.chambre1.hauteur;
formFields['hauteur2']                                             = $ds046PE01.cadre2.etatDesLieux.chambre2.hauteur;
formFields['hauteur3']                                             = $ds046PE01.cadre2.etatDesLieux.chambre3.hauteur;
formFields['hauteur4']                                             = $ds046PE01.cadre2.etatDesLieux.chambre4.hauteur;
formFields['hauteur5']                                             = $ds046PE01.cadre2.etatDesLieux.chambre5.hauteur;
formFields['hauteur6']                                             = $ds046PE01.cadre2.etatDesLieux.chambre6.hauteur;
formFields['hauteur7']                                             = $ds046PE01.cadre2.etatDesLieux.chambre7.hauteur;
formFields['hauteur8']                                             = $ds046PE01.cadre2.etatDesLieux.chambre8.hauteur;
formFields['hauteur9']                                             = $ds046PE01.cadre2.etatDesLieux.chambre9.hauteur;
formFields['hauteur10']                                            = $ds046PE01.cadre2.etatDesLieux.chambre10.hauteur;
formFields['hauteur11']                                            = $ds046PE01.cadre2.etatDesLieux.chambre11.hauteur;
formFields['hauteur12']                                            = $ds046PE01.cadre2.etatDesLieux.chambre12.hauteur;
formFields['hauteur13']                                            = $ds046PE01.cadre2.etatDesLieux.chambre13.hauteur;
formFields['hauteur14']                                            = $ds046PE01.cadre2.etatDesLieux.chambre14.hauteur;
formFields['hauteur15']                                            = $ds046PE01.cadre2.etatDesLieux.chambre15.hauteur;
formFields['hauteur16']                                            = $ds046PE01.cadre2.etatDesLieux.chambre16.hauteur;
formFields['hauteur17']                                            = $ds046PE01.cadre2.etatDesLieux.chambre17.hauteur;
formFields['hauteur18']                                            = $ds046PE01.cadre2.etatDesLieux.chambre18.hauteur;
formFields['hauteur19']                                            = $ds046PE01.cadre2.etatDesLieux.chambre19.hauteur;
formFields['hauteur20']                                            = $ds046PE01.cadre2.etatDesLieux.chambre20.hauteur;
formFields['hauteur21']                                            = $ds046PE01.cadre2.etatDesLieux.chambre21.hauteur;
formFields['hauteur22']                                            = $ds046PE01.cadre2.etatDesLieux.chambre22.hauteur;
formFields['hauteur23']                                            = $ds046PE01.cadre2.etatDesLieux.chambre23.hauteur;
formFields['hauteur24']                                            = $ds046PE01.cadre2.etatDesLieux.chambre24.hauteur;
formFields['hauteur25']                                            = $ds046PE01.cadre2.etatDesLieux.chambre25.hauteur;
formFields['hauteur26']                                            = $ds046PE01.cadre2.etatDesLieux.chambre26.hauteur;
formFields['hauteur27']                                            = $ds046PE01.cadre2.etatDesLieux.chambre27.hauteur;
formFields['hauteur28']                                            = $ds046PE01.cadre2.etatDesLieux.chambre28.hauteur;
formFields['nbrePersonsonnesParChambre1']                          = $ds046PE01.cadre2.etatDesLieux.chambre1.nbrePersonsonnesParChambre;
formFields['nbrePersonsonnesParChambre2']                          = $ds046PE01.cadre2.etatDesLieux.chambre2.nbrePersonsonnesParChambre;
formFields['nbrePersonsonnesParChambre3']                          = $ds046PE01.cadre2.etatDesLieux.chambre3.nbrePersonsonnesParChambre;
formFields['nbrePersonsonnesParChambre4']                          = $ds046PE01.cadre2.etatDesLieux.chambre4.nbrePersonsonnesParChambre;
formFields['nbrePersonsonnesParChambre5']                          = $ds046PE01.cadre2.etatDesLieux.chambre5.nbrePersonsonnesParChambre;
formFields['nbrePersonsonnesParChambre6']                          = $ds046PE01.cadre2.etatDesLieux.chambre6.nbrePersonsonnesParChambre;
formFields['nbrePersonsonnesParChambre7']                          = $ds046PE01.cadre2.etatDesLieux.chambre7.nbrePersonsonnesParChambre;
formFields['nbrePersonsonnesParChambre8']                          = $ds046PE01.cadre2.etatDesLieux.chambre8.nbrePersonsonnesParChambre;
formFields['nbrePersonsonnesParChambre9']                          = $ds046PE01.cadre2.etatDesLieux.chambre9.nbrePersonsonnesParChambre;
formFields['nbrePersonsonnesParChambre10']                         = $ds046PE01.cadre2.etatDesLieux.chambre10.nbrePersonsonnesParChambre;
formFields['nbrePersonsonnesParChambre11']                         = $ds046PE01.cadre2.etatDesLieux.chambre11.nbrePersonsonnesParChambre;
formFields['nbrePersonsonnesParChambre12']                         = $ds046PE01.cadre2.etatDesLieux.chambre12.nbrePersonsonnesParChambre;
formFields['nbrePersonsonnesParChambre13']                         = $ds046PE01.cadre2.etatDesLieux.chambre13.nbrePersonsonnesParChambre;
formFields['nbrePersonsonnesParChambre14']                         = $ds046PE01.cadre2.etatDesLieux.chambre14.nbrePersonsonnesParChambre;
formFields['nbrePersonsonnesParChambre15']                         = $ds046PE01.cadre2.etatDesLieux.chambre15.nbrePersonsonnesParChambre;
formFields['nbrePersonsonnesParChambre16']                         = $ds046PE01.cadre2.etatDesLieux.chambre16.nbrePersonsonnesParChambre;
formFields['nbrePersonsonnesParChambre17']                         = $ds046PE01.cadre2.etatDesLieux.chambre17.nbrePersonsonnesParChambre;
formFields['nbrePersonsonnesParChambre18']                         = $ds046PE01.cadre2.etatDesLieux.chambre18.nbrePersonsonnesParChambre;
formFields['nbrePersonsonnesParChambre19']                         = $ds046PE01.cadre2.etatDesLieux.chambre19.nbrePersonsonnesParChambre;
formFields['nbrePersonsonnesParChambre20']                         = $ds046PE01.cadre2.etatDesLieux.chambre20.nbrePersonsonnesParChambre;
formFields['nbrePersonsonnesParChambre21']                         = $ds046PE01.cadre2.etatDesLieux.chambre21.nbrePersonsonnesParChambre;
formFields['nbrePersonsonnesParChambre22']                         = $ds046PE01.cadre2.etatDesLieux.chambre22.nbrePersonsonnesParChambre;
formFields['nbrePersonsonnesParChambre23']                         = $ds046PE01.cadre2.etatDesLieux.chambre23.nbrePersonsonnesParChambre;
formFields['nbrePersonsonnesParChambre24']                         = $ds046PE01.cadre2.etatDesLieux.chambre24.nbrePersonsonnesParChambre;
formFields['nbrePersonsonnesParChambre25']                         = $ds046PE01.cadre2.etatDesLieux.chambre25.nbrePersonsonnesParChambre;
formFields['nbrePersonsonnesParChambre26']                         = $ds046PE01.cadre2.etatDesLieux.chambre26.nbrePersonsonnesParChambre;
formFields['nbrePersonsonnesParChambre27']                         = $ds046PE01.cadre2.etatDesLieux.chambre27.nbrePersonsonnesParChambre;
formFields['nbrePersonsonnesParChambre28']                         = $ds046PE01.cadre2.etatDesLieux.chambre28.nbrePersonsonnesParChambre;
                                                                                                                                        
var formFieldsPage2 = {};

formFieldsPage2['etage1']                                               = $ds046PE01.cadre2.etatDesLieux.chambre29.etage;
formFieldsPage2['etage2']                                               = $ds046PE01.cadre2.etatDesLieux.chambre30.etage;
formFieldsPage2['etage3']                                               = $ds046PE01.cadre2.etatDesLieux.chambre31.etage;
formFieldsPage2['etage4']                                               = $ds046PE01.cadre2.etatDesLieux.chambre32.etage;
formFieldsPage2['etage5']                                               = $ds046PE01.cadre2.etatDesLieux.chambre33.etage;
formFieldsPage2['etage6']                                               = $ds046PE01.cadre2.etatDesLieux.chambre34.etage;
formFieldsPage2['etage7']                                               = $ds046PE01.cadre2.etatDesLieux.chambre35.etage;
formFieldsPage2['etage8']                                               = $ds046PE01.cadre2.etatDesLieux.chambre36.etage;
formFieldsPage2['etage9']                                               = $ds046PE01.cadre2.etatDesLieux.chambre37.etage;
formFieldsPage2['etage10']                                              = $ds046PE01.cadre2.etatDesLieux.chambre38.etage;
formFieldsPage2['etage11']                                              = $ds046PE01.cadre2.etatDesLieux.chambre39.etage;
formFieldsPage2['etage12']                                              = $ds046PE01.cadre2.etatDesLieux.chambre40.etage;
formFieldsPage2['etage13']                                              = $ds046PE01.cadre2.etatDesLieux.chambre41.etage;
formFieldsPage2['etage14']                                              = $ds046PE01.cadre2.etatDesLieux.chambre42.etage;
formFieldsPage2['etage15']                                              = $ds046PE01.cadre2.etatDesLieux.chambre43.etage;
formFieldsPage2['etage16']                                              = $ds046PE01.cadre2.etatDesLieux.chambre44.etage;
formFieldsPage2['etage17']                                              = $ds046PE01.cadre2.etatDesLieux.chambre45.etage;
formFieldsPage2['etage18']                                              = $ds046PE01.cadre2.etatDesLieux.chambre46.etage;
formFieldsPage2['etage19']                                              = $ds046PE01.cadre2.etatDesLieux.chambre47.etage;
formFieldsPage2['etage20']                                              = $ds046PE01.cadre2.etatDesLieux.chambre48.etage;
formFieldsPage2['etage21']                                              = $ds046PE01.cadre2.etatDesLieux.chambre49.etage;
formFieldsPage2['etage22']                                              = $ds046PE01.cadre2.etatDesLieux.chambre50.etage;
formFieldsPage2['etage23']                                              = $ds046PE01.cadre2.etatDesLieux.chambre51.etage;
formFieldsPage2['etage24']                                              = $ds046PE01.cadre2.etatDesLieux.chambre52.etage;
formFieldsPage2['etage25']                                              = $ds046PE01.cadre2.etatDesLieux.chambre53.etage;
formFieldsPage2['etage26']                                              = $ds046PE01.cadre2.etatDesLieux.chambre54.etage;
formFieldsPage2['etage27']                                              = $ds046PE01.cadre2.etatDesLieux.chambre55.etage;
formFieldsPage2['etage28']                                              = $ds046PE01.cadre2.etatDesLieux.chambre56.etage;
formFieldsPage2['numeroChambre1']                                       = $ds046PE01.cadre2.etatDesLieux.chambre29.numeroChambre;
formFieldsPage2['numeroChambre2']                                       = $ds046PE01.cadre2.etatDesLieux.chambre30.numeroChambre;
formFieldsPage2['numeroChambre3']                                       = $ds046PE01.cadre2.etatDesLieux.chambre31.numeroChambre;
formFieldsPage2['numeroChambre4']                                       = $ds046PE01.cadre2.etatDesLieux.chambre32.numeroChambre;
formFieldsPage2['numeroChambre5']                                       = $ds046PE01.cadre2.etatDesLieux.chambre33.numeroChambre;
formFieldsPage2['numeroChambre6']                                       = $ds046PE01.cadre2.etatDesLieux.chambre34.numeroChambre;
formFieldsPage2['numeroChambre7']                                       = $ds046PE01.cadre2.etatDesLieux.chambre35.numeroChambre;
formFieldsPage2['numeroChambre8']                                       = $ds046PE01.cadre2.etatDesLieux.chambre36.numeroChambre;
formFieldsPage2['numeroChambre9']                                       = $ds046PE01.cadre2.etatDesLieux.chambre37.numeroChambre;
formFieldsPage2['numeroChambre10']                                      = $ds046PE01.cadre2.etatDesLieux.chambre38.numeroChambre;
formFieldsPage2['numeroChambre11']                                      = $ds046PE01.cadre2.etatDesLieux.chambre39.numeroChambre;
formFieldsPage2['numeroChambre12']                                      = $ds046PE01.cadre2.etatDesLieux.chambre40.numeroChambre;
formFieldsPage2['numeroChambre13']                                      = $ds046PE01.cadre2.etatDesLieux.chambre41.numeroChambre;
formFieldsPage2['numeroChambre14']                                      = $ds046PE01.cadre2.etatDesLieux.chambre42.numeroChambre;
formFieldsPage2['numeroChambre15']                                      = $ds046PE01.cadre2.etatDesLieux.chambre43.numeroChambre;
formFieldsPage2['numeroChambre16']                                      = $ds046PE01.cadre2.etatDesLieux.chambre44.numeroChambre;
formFieldsPage2['numeroChambre17']                                      = $ds046PE01.cadre2.etatDesLieux.chambre45.numeroChambre;
formFieldsPage2['numeroChambre18']                                      = $ds046PE01.cadre2.etatDesLieux.chambre46.numeroChambre;
formFieldsPage2['numeroChambre19']                                      = $ds046PE01.cadre2.etatDesLieux.chambre47.numeroChambre;
formFieldsPage2['numeroChambre20']                                      = $ds046PE01.cadre2.etatDesLieux.chambre48.numeroChambre;
formFieldsPage2['numeroChambre21']                                      = $ds046PE01.cadre2.etatDesLieux.chambre49.numeroChambre;
formFieldsPage2['numeroChambre22']                                      = $ds046PE01.cadre2.etatDesLieux.chambre50.numeroChambre;
formFieldsPage2['numeroChambre23']                                      = $ds046PE01.cadre2.etatDesLieux.chambre51.numeroChambre;
formFieldsPage2['numeroChambre24']                                      = $ds046PE01.cadre2.etatDesLieux.chambre52.numeroChambre;
formFieldsPage2['numeroChambre25']                                      = $ds046PE01.cadre2.etatDesLieux.chambre53.numeroChambre;
formFieldsPage2['numeroChambre26']                                      = $ds046PE01.cadre2.etatDesLieux.chambre54.numeroChambre;
formFieldsPage2['numeroChambre27']                                      = $ds046PE01.cadre2.etatDesLieux.chambre55.numeroChambre;
formFieldsPage2['numeroChambre28']                                      = $ds046PE01.cadre2.etatDesLieux.chambre56.numeroChambre;
formFieldsPage2['superficieChambre1']                                   = $ds046PE01.cadre2.etatDesLieux.chambre29.superficieChambre;
formFieldsPage2['superficieChambre2']                                   = $ds046PE01.cadre2.etatDesLieux.chambre30.superficieChambre;
formFieldsPage2['superficieChambre3']                                   = $ds046PE01.cadre2.etatDesLieux.chambre31.superficieChambre;
formFieldsPage2['superficieChambre4']                                   = $ds046PE01.cadre2.etatDesLieux.chambre32.superficieChambre;
formFieldsPage2['superficieChambre5']                                   = $ds046PE01.cadre2.etatDesLieux.chambre33.superficieChambre;
formFieldsPage2['superficieChambre6']                                   = $ds046PE01.cadre2.etatDesLieux.chambre34.superficieChambre;
formFieldsPage2['superficieChambre7']                                   = $ds046PE01.cadre2.etatDesLieux.chambre35.superficieChambre;
formFieldsPage2['superficieChambre8']                                   = $ds046PE01.cadre2.etatDesLieux.chambre36.superficieChambre;
formFieldsPage2['superficieChambre9']                                   = $ds046PE01.cadre2.etatDesLieux.chambre37.superficieChambre;
formFieldsPage2['superficieChambre10']                                  = $ds046PE01.cadre2.etatDesLieux.chambre38.superficieChambre;
formFieldsPage2['superficieChambre11']                                  = $ds046PE01.cadre2.etatDesLieux.chambre39.superficieChambre;
formFieldsPage2['superficieChambre12']                                  = $ds046PE01.cadre2.etatDesLieux.chambre40.superficieChambre;
formFieldsPage2['superficieChambre13']                                  = $ds046PE01.cadre2.etatDesLieux.chambre41.superficieChambre;
formFieldsPage2['superficieChambre14']                                  = $ds046PE01.cadre2.etatDesLieux.chambre42.superficieChambre;
formFieldsPage2['superficieChambre15']                                  = $ds046PE01.cadre2.etatDesLieux.chambre43.superficieChambre;
formFieldsPage2['superficieChambre16']                                  = $ds046PE01.cadre2.etatDesLieux.chambre44.superficieChambre;
formFieldsPage2['superficieChambre17']                                  = $ds046PE01.cadre2.etatDesLieux.chambre45.superficieChambre;
formFieldsPage2['superficieChambre18']                                  = $ds046PE01.cadre2.etatDesLieux.chambre46.superficieChambre;
formFieldsPage2['superficieChambre19']                                  = $ds046PE01.cadre2.etatDesLieux.chambre47.superficieChambre;
formFieldsPage2['superficieChambre20']                                  = $ds046PE01.cadre2.etatDesLieux.chambre48.superficieChambre;
formFieldsPage2['superficieChambre21']                                  = $ds046PE01.cadre2.etatDesLieux.chambre49.superficieChambre;
formFieldsPage2['superficieChambre22']                                  = $ds046PE01.cadre2.etatDesLieux.chambre50.superficieChambre;
formFieldsPage2['superficieChambre23']                                  = $ds046PE01.cadre2.etatDesLieux.chambre51.superficieChambre;
formFieldsPage2['superficieChambre24']                                  = $ds046PE01.cadre2.etatDesLieux.chambre52.superficieChambre;
formFieldsPage2['superficieChambre25']                                  = $ds046PE01.cadre2.etatDesLieux.chambre53.superficieChambre;
formFieldsPage2['superficieChambre26']                                  = $ds046PE01.cadre2.etatDesLieux.chambre54.superficieChambre;
formFieldsPage2['superficieChambre27']                                  = $ds046PE01.cadre2.etatDesLieux.chambre55.superficieChambre;
formFieldsPage2['superficieChambre28']                                  = $ds046PE01.cadre2.etatDesLieux.chambre56.superficieChambre;
formFieldsPage2['longueur1']                                            = $ds046PE01.cadre2.etatDesLieux.chambre29.longueur;
formFieldsPage2['longueur2']                                            = $ds046PE01.cadre2.etatDesLieux.chambre30.longueur;
formFieldsPage2['longueur3']                                            = $ds046PE01.cadre2.etatDesLieux.chambre31.longueur;
formFieldsPage2['longueur4']                                            = $ds046PE01.cadre2.etatDesLieux.chambre32.longueur;
formFieldsPage2['longueur5']                                            = $ds046PE01.cadre2.etatDesLieux.chambre33.longueur;
formFieldsPage2['longueur6']                                            = $ds046PE01.cadre2.etatDesLieux.chambre34.longueur;
formFieldsPage2['longueur7']                                            = $ds046PE01.cadre2.etatDesLieux.chambre35.longueur;
formFieldsPage2['longueur8']                                            = $ds046PE01.cadre2.etatDesLieux.chambre36.longueur;
formFieldsPage2['longueur9']                                            = $ds046PE01.cadre2.etatDesLieux.chambre37.longueur;
formFieldsPage2['longueur10']                                           = $ds046PE01.cadre2.etatDesLieux.chambre38.longueur;
formFieldsPage2['longueur11']                                           = $ds046PE01.cadre2.etatDesLieux.chambre39.longueur;
formFieldsPage2['longueur12']                                           = $ds046PE01.cadre2.etatDesLieux.chambre40.longueur;
formFieldsPage2['longueur13']                                           = $ds046PE01.cadre2.etatDesLieux.chambre41.longueur;
formFieldsPage2['longueur14']                                           = $ds046PE01.cadre2.etatDesLieux.chambre42.longueur;
formFieldsPage2['longueur15']                                           = $ds046PE01.cadre2.etatDesLieux.chambre43.longueur;
formFieldsPage2['longueur16']                                           = $ds046PE01.cadre2.etatDesLieux.chambre44.longueur;
formFieldsPage2['longueur17']                                           = $ds046PE01.cadre2.etatDesLieux.chambre45.longueur;
formFieldsPage2['longueur18']                                           = $ds046PE01.cadre2.etatDesLieux.chambre46.longueur;
formFieldsPage2['longueur19']                                           = $ds046PE01.cadre2.etatDesLieux.chambre47.longueur;
formFieldsPage2['longueur20']                                           = $ds046PE01.cadre2.etatDesLieux.chambre48.longueur;
formFieldsPage2['longueur21']                                           = $ds046PE01.cadre2.etatDesLieux.chambre49.longueur;
formFieldsPage2['longueur22']                                           = $ds046PE01.cadre2.etatDesLieux.chambre50.longueur;
formFieldsPage2['longueur23']                                           = $ds046PE01.cadre2.etatDesLieux.chambre51.longueur;
formFieldsPage2['longueur24']                                           = $ds046PE01.cadre2.etatDesLieux.chambre52.longueur;
formFieldsPage2['longueur25']                                           = $ds046PE01.cadre2.etatDesLieux.chambre53.longueur;
formFieldsPage2['longueur26']                                           = $ds046PE01.cadre2.etatDesLieux.chambre54.longueur;
formFieldsPage2['longueur27']                                           = $ds046PE01.cadre2.etatDesLieux.chambre55.longueur;
formFieldsPage2['longueur28']                                           = $ds046PE01.cadre2.etatDesLieux.chambre56.longueur;
formFieldsPage2['largeur1']                                             = $ds046PE01.cadre2.etatDesLieux.chambre29.largeur;
formFieldsPage2['largeur2']                                             = $ds046PE01.cadre2.etatDesLieux.chambre30.largeur;
formFieldsPage2['largeur3']                                             = $ds046PE01.cadre2.etatDesLieux.chambre31.largeur;
formFieldsPage2['largeur4']                                             = $ds046PE01.cadre2.etatDesLieux.chambre32.largeur;
formFieldsPage2['largeur5']                                             = $ds046PE01.cadre2.etatDesLieux.chambre33.largeur;
formFieldsPage2['largeur6']                                             = $ds046PE01.cadre2.etatDesLieux.chambre34.largeur;
formFieldsPage2['largeur7']                                             = $ds046PE01.cadre2.etatDesLieux.chambre35.largeur;
formFieldsPage2['largeur8']                                             = $ds046PE01.cadre2.etatDesLieux.chambre36.largeur;
formFieldsPage2['largeur9']                                             = $ds046PE01.cadre2.etatDesLieux.chambre37.largeur;
formFieldsPage2['largeur10']                                            = $ds046PE01.cadre2.etatDesLieux.chambre38.largeur;
formFieldsPage2['largeur11']                                            = $ds046PE01.cadre2.etatDesLieux.chambre39.largeur;
formFieldsPage2['largeur12']                                            = $ds046PE01.cadre2.etatDesLieux.chambre40.largeur;
formFieldsPage2['largeur13']                                            = $ds046PE01.cadre2.etatDesLieux.chambre41.largeur;
formFieldsPage2['largeur14']                                            = $ds046PE01.cadre2.etatDesLieux.chambre42.largeur;
formFieldsPage2['largeur15']                                            = $ds046PE01.cadre2.etatDesLieux.chambre43.largeur;
formFieldsPage2['largeur16']                                            = $ds046PE01.cadre2.etatDesLieux.chambre44.largeur;
formFieldsPage2['largeur17']                                            = $ds046PE01.cadre2.etatDesLieux.chambre45.largeur;
formFieldsPage2['largeur18']                                            = $ds046PE01.cadre2.etatDesLieux.chambre46.largeur;
formFieldsPage2['largeur19']                                            = $ds046PE01.cadre2.etatDesLieux.chambre47.largeur;
formFieldsPage2['largeur20']                                            = $ds046PE01.cadre2.etatDesLieux.chambre48.largeur;
formFieldsPage2['largeur21']                                            = $ds046PE01.cadre2.etatDesLieux.chambre49.largeur;
formFieldsPage2['largeur22']                                            = $ds046PE01.cadre2.etatDesLieux.chambre50.largeur;
formFieldsPage2['largeur23']                                            = $ds046PE01.cadre2.etatDesLieux.chambre51.largeur;
formFieldsPage2['largeur24']                                            = $ds046PE01.cadre2.etatDesLieux.chambre52.largeur;
formFieldsPage2['largeur25']                                            = $ds046PE01.cadre2.etatDesLieux.chambre53.largeur;
formFieldsPage2['largeur26']                                            = $ds046PE01.cadre2.etatDesLieux.chambre54.largeur;
formFieldsPage2['largeur27']                                            = $ds046PE01.cadre2.etatDesLieux.chambre55.largeur;
formFieldsPage2['largeur28']                                            = $ds046PE01.cadre2.etatDesLieux.chambre56.largeur;
formFieldsPage2['hauteur1']                                             = $ds046PE01.cadre2.etatDesLieux.chambre29.hauteur;
formFieldsPage2['hauteur2']                                             = $ds046PE01.cadre2.etatDesLieux.chambre30.hauteur;
formFieldsPage2['hauteur3']                                             = $ds046PE01.cadre2.etatDesLieux.chambre31.hauteur;
formFieldsPage2['hauteur4']                                             = $ds046PE01.cadre2.etatDesLieux.chambre32.hauteur;
formFieldsPage2['hauteur5']                                             = $ds046PE01.cadre2.etatDesLieux.chambre33.hauteur;
formFieldsPage2['hauteur6']                                             = $ds046PE01.cadre2.etatDesLieux.chambre34.hauteur;
formFieldsPage2['hauteur7']                                             = $ds046PE01.cadre2.etatDesLieux.chambre35.hauteur;
formFieldsPage2['hauteur8']                                             = $ds046PE01.cadre2.etatDesLieux.chambre36.hauteur;
formFieldsPage2['hauteur9']                                             = $ds046PE01.cadre2.etatDesLieux.chambre37.hauteur;
formFieldsPage2['hauteur10']                                            = $ds046PE01.cadre2.etatDesLieux.chambre38.hauteur;
formFieldsPage2['hauteur11']                                            = $ds046PE01.cadre2.etatDesLieux.chambre39.hauteur;
formFieldsPage2['hauteur12']                                            = $ds046PE01.cadre2.etatDesLieux.chambre40.hauteur;
formFieldsPage2['hauteur13']                                            = $ds046PE01.cadre2.etatDesLieux.chambre41.hauteur;
formFieldsPage2['hauteur14']                                            = $ds046PE01.cadre2.etatDesLieux.chambre42.hauteur;
formFieldsPage2['hauteur15']                                            = $ds046PE01.cadre2.etatDesLieux.chambre43.hauteur;
formFieldsPage2['hauteur16']                                            = $ds046PE01.cadre2.etatDesLieux.chambre44.hauteur;
formFieldsPage2['hauteur17']                                            = $ds046PE01.cadre2.etatDesLieux.chambre45.hauteur;
formFieldsPage2['hauteur18']                                            = $ds046PE01.cadre2.etatDesLieux.chambre46.hauteur;
formFieldsPage2['hauteur19']                                            = $ds046PE01.cadre2.etatDesLieux.chambre47.hauteur;
formFieldsPage2['hauteur20']                                            = $ds046PE01.cadre2.etatDesLieux.chambre48.hauteur;
formFieldsPage2['hauteur21']                                            = $ds046PE01.cadre2.etatDesLieux.chambre49.hauteur;
formFieldsPage2['hauteur22']                                            = $ds046PE01.cadre2.etatDesLieux.chambre50.hauteur;
formFieldsPage2['hauteur23']                                            = $ds046PE01.cadre2.etatDesLieux.chambre51.hauteur;
formFieldsPage2['hauteur24']                                            = $ds046PE01.cadre2.etatDesLieux.chambre52.hauteur;
formFieldsPage2['hauteur25']                                            = $ds046PE01.cadre2.etatDesLieux.chambre53.hauteur;
formFieldsPage2['hauteur26']                                            = $ds046PE01.cadre2.etatDesLieux.chambre54.hauteur;
formFieldsPage2['hauteur27']                                            = $ds046PE01.cadre2.etatDesLieux.chambre55.hauteur;
formFieldsPage2['hauteur28']                                            = $ds046PE01.cadre2.etatDesLieux.chambre56.hauteur;
formFieldsPage2['nbrePersonsonnesParChambre1']                          = $ds046PE01.cadre2.etatDesLieux.chambre29.nbrePersonsonnesParChambre;
formFieldsPage2['nbrePersonsonnesParChambre2']                          = $ds046PE01.cadre2.etatDesLieux.chambre30.nbrePersonsonnesParChambre;
formFieldsPage2['nbrePersonsonnesParChambre3']                          = $ds046PE01.cadre2.etatDesLieux.chambre31.nbrePersonsonnesParChambre;
formFieldsPage2['nbrePersonsonnesParChambre4']                          = $ds046PE01.cadre2.etatDesLieux.chambre32.nbrePersonsonnesParChambre;
formFieldsPage2['nbrePersonsonnesParChambre5']                          = $ds046PE01.cadre2.etatDesLieux.chambre33.nbrePersonsonnesParChambre;
formFieldsPage2['nbrePersonsonnesParChambre6']                          = $ds046PE01.cadre2.etatDesLieux.chambre34.nbrePersonsonnesParChambre;
formFieldsPage2['nbrePersonsonnesParChambre7']                          = $ds046PE01.cadre2.etatDesLieux.chambre35.nbrePersonsonnesParChambre;
formFieldsPage2['nbrePersonsonnesParChambre8']                          = $ds046PE01.cadre2.etatDesLieux.chambre36.nbrePersonsonnesParChambre;
formFieldsPage2['nbrePersonsonnesParChambre9']                          = $ds046PE01.cadre2.etatDesLieux.chambre37.nbrePersonsonnesParChambre;
formFieldsPage2['nbrePersonsonnesParChambre10']                         = $ds046PE01.cadre2.etatDesLieux.chambre38.nbrePersonsonnesParChambre;
formFieldsPage2['nbrePersonsonnesParChambre11']                         = $ds046PE01.cadre2.etatDesLieux.chambre39.nbrePersonsonnesParChambre;
formFieldsPage2['nbrePersonsonnesParChambre12']                         = $ds046PE01.cadre2.etatDesLieux.chambre40.nbrePersonsonnesParChambre;
formFieldsPage2['nbrePersonsonnesParChambre13']                         = $ds046PE01.cadre2.etatDesLieux.chambre41.nbrePersonsonnesParChambre;
formFieldsPage2['nbrePersonsonnesParChambre14']                         = $ds046PE01.cadre2.etatDesLieux.chambre42.nbrePersonsonnesParChambre;
formFieldsPage2['nbrePersonsonnesParChambre15']                         = $ds046PE01.cadre2.etatDesLieux.chambre43.nbrePersonsonnesParChambre;
formFieldsPage2['nbrePersonsonnesParChambre16']                         = $ds046PE01.cadre2.etatDesLieux.chambre44.nbrePersonsonnesParChambre;
formFieldsPage2['nbrePersonsonnesParChambre17']                         = $ds046PE01.cadre2.etatDesLieux.chambre45.nbrePersonsonnesParChambre;
formFieldsPage2['nbrePersonsonnesParChambre18']                         = $ds046PE01.cadre2.etatDesLieux.chambre46.nbrePersonsonnesParChambre;
formFieldsPage2['nbrePersonsonnesParChambre19']                         = $ds046PE01.cadre2.etatDesLieux.chambre47.nbrePersonsonnesParChambre;
formFieldsPage2['nbrePersonsonnesParChambre20']                         = $ds046PE01.cadre2.etatDesLieux.chambre48.nbrePersonsonnesParChambre;
formFieldsPage2['nbrePersonsonnesParChambre21']                         = $ds046PE01.cadre2.etatDesLieux.chambre49.nbrePersonsonnesParChambre;
formFieldsPage2['nbrePersonsonnesParChambre22']                         = $ds046PE01.cadre2.etatDesLieux.chambre50.nbrePersonsonnesParChambre;
formFieldsPage2['nbrePersonsonnesParChambre23']                         = $ds046PE01.cadre2.etatDesLieux.chambre51.nbrePersonsonnesParChambre;
formFieldsPage2['nbrePersonsonnesParChambre24']                         = $ds046PE01.cadre2.etatDesLieux.chambre52.nbrePersonsonnesParChambre;
formFieldsPage2['nbrePersonsonnesParChambre25']                         = $ds046PE01.cadre2.etatDesLieux.chambre53.nbrePersonsonnesParChambre;
formFieldsPage2['nbrePersonsonnesParChambre26']                         = $ds046PE01.cadre2.etatDesLieux.chambre54.nbrePersonsonnesParChambre;
formFieldsPage2['nbrePersonsonnesParChambre27']                         = $ds046PE01.cadre2.etatDesLieux.chambre55.nbrePersonsonnesParChambre;
formFieldsPage2['nbrePersonsonnesParChambre28']                         = $ds046PE01.cadre2.etatDesLieux.chambre56.nbrePersonsonnesParChambre;


var formFieldsPage3 = {};

formFieldsPage3['etage1']                                               = $ds046PE01.cadre2.etatDesLieux.chambre57.etage;
formFieldsPage3['etage2']                                               = $ds046PE01.cadre2.etatDesLieux.chambre58.etage;
formFieldsPage3['etage3']                                               = $ds046PE01.cadre2.etatDesLieux.chambre59.etage;
formFieldsPage3['etage4']                                               = $ds046PE01.cadre2.etatDesLieux.chambre60.etage;
formFieldsPage3['etage5']                                               = $ds046PE01.cadre2.etatDesLieux.chambre61.etage;
formFieldsPage3['etage6']                                               = $ds046PE01.cadre2.etatDesLieux.chambre62.etage;
formFieldsPage3['etage7']                                               = $ds046PE01.cadre2.etatDesLieux.chambre63.etage;
formFieldsPage3['etage8']                                               = $ds046PE01.cadre2.etatDesLieux.chambre64.etage;
formFieldsPage3['etage9']                                               = $ds046PE01.cadre2.etatDesLieux.chambre65.etage;
formFieldsPage3['etage10']                                              = $ds046PE01.cadre2.etatDesLieux.chambre66.etage;
formFieldsPage3['etage11']                                              = $ds046PE01.cadre2.etatDesLieux.chambre67.etage;
formFieldsPage3['etage12']                                              = $ds046PE01.cadre2.etatDesLieux.chambre68.etage;
formFieldsPage3['etage13']                                              = $ds046PE01.cadre2.etatDesLieux.chambre69.etage;
formFieldsPage3['etage14']                                              = $ds046PE01.cadre2.etatDesLieux.chambre70.etage;
formFieldsPage3['etage15']                                              = $ds046PE01.cadre2.etatDesLieux.chambre71.etage;
formFieldsPage3['etage16']                                              = $ds046PE01.cadre2.etatDesLieux.chambre72.etage;
formFieldsPage3['etage17']                                              = $ds046PE01.cadre2.etatDesLieux.chambre73.etage;
formFieldsPage3['etage18']                                              = $ds046PE01.cadre2.etatDesLieux.chambre74.etage;
formFieldsPage3['etage19']                                              = $ds046PE01.cadre2.etatDesLieux.chambre75.etage;
formFieldsPage3['etage20']                                              = $ds046PE01.cadre2.etatDesLieux.chambre76.etage;
formFieldsPage3['etage21']                                              = $ds046PE01.cadre2.etatDesLieux.chambre77.etage;
formFieldsPage3['etage22']                                              = $ds046PE01.cadre2.etatDesLieux.chambre78.etage;
formFieldsPage3['etage23']                                              = $ds046PE01.cadre2.etatDesLieux.chambre79.etage;
formFieldsPage3['etage24']                                              = $ds046PE01.cadre2.etatDesLieux.chambre80.etage;
formFieldsPage3['etage25']                                              = $ds046PE01.cadre2.etatDesLieux.chambre81.etage;
formFieldsPage3['etage26']                                              = $ds046PE01.cadre2.etatDesLieux.chambre82.etage;
formFieldsPage3['etage27']                                              = $ds046PE01.cadre2.etatDesLieux.chambre83.etage;
formFieldsPage3['etage28']                                              = $ds046PE01.cadre2.etatDesLieux.chambre84.etage;
formFieldsPage3['numeroChambre1']                                       = $ds046PE01.cadre2.etatDesLieux.chambre57.numeroChambre;
formFieldsPage3['numeroChambre2']                                       = $ds046PE01.cadre2.etatDesLieux.chambre58.numeroChambre;
formFieldsPage3['numeroChambre3']                                       = $ds046PE01.cadre2.etatDesLieux.chambre59.numeroChambre;
formFieldsPage3['numeroChambre4']                                       = $ds046PE01.cadre2.etatDesLieux.chambre60.numeroChambre;
formFieldsPage3['numeroChambre5']                                       = $ds046PE01.cadre2.etatDesLieux.chambre61.numeroChambre;
formFieldsPage3['numeroChambre6']                                       = $ds046PE01.cadre2.etatDesLieux.chambre62.numeroChambre;
formFieldsPage3['numeroChambre7']                                       = $ds046PE01.cadre2.etatDesLieux.chambre63.numeroChambre;
formFieldsPage3['numeroChambre8']                                       = $ds046PE01.cadre2.etatDesLieux.chambre64.numeroChambre;
formFieldsPage3['numeroChambre9']                                       = $ds046PE01.cadre2.etatDesLieux.chambre65.numeroChambre;
formFieldsPage3['numeroChambre10']                                      = $ds046PE01.cadre2.etatDesLieux.chambre66.numeroChambre;
formFieldsPage3['numeroChambre11']                                      = $ds046PE01.cadre2.etatDesLieux.chambre67.numeroChambre;
formFieldsPage3['numeroChambre12']                                      = $ds046PE01.cadre2.etatDesLieux.chambre68.numeroChambre;
formFieldsPage3['numeroChambre13']                                      = $ds046PE01.cadre2.etatDesLieux.chambre69.numeroChambre;
formFieldsPage3['numeroChambre14']                                      = $ds046PE01.cadre2.etatDesLieux.chambre70.numeroChambre;
formFieldsPage3['numeroChambre15']                                      = $ds046PE01.cadre2.etatDesLieux.chambre71.numeroChambre;
formFieldsPage3['numeroChambre16']                                      = $ds046PE01.cadre2.etatDesLieux.chambre72.numeroChambre;
formFieldsPage3['numeroChambre17']                                      = $ds046PE01.cadre2.etatDesLieux.chambre73.numeroChambre;
formFieldsPage3['numeroChambre18']                                      = $ds046PE01.cadre2.etatDesLieux.chambre74.numeroChambre;
formFieldsPage3['numeroChambre19']                                      = $ds046PE01.cadre2.etatDesLieux.chambre75.numeroChambre;
formFieldsPage3['numeroChambre20']                                      = $ds046PE01.cadre2.etatDesLieux.chambre76.numeroChambre;
formFieldsPage3['numeroChambre21']                                      = $ds046PE01.cadre2.etatDesLieux.chambre77.numeroChambre;
formFieldsPage3['numeroChambre22']                                      = $ds046PE01.cadre2.etatDesLieux.chambre78.numeroChambre;
formFieldsPage3['numeroChambre23']                                      = $ds046PE01.cadre2.etatDesLieux.chambre79.numeroChambre;
formFieldsPage3['numeroChambre24']                                      = $ds046PE01.cadre2.etatDesLieux.chambre80.numeroChambre;
formFieldsPage3['numeroChambre25']                                      = $ds046PE01.cadre2.etatDesLieux.chambre81.numeroChambre;
formFieldsPage3['numeroChambre26']                                      = $ds046PE01.cadre2.etatDesLieux.chambre82.numeroChambre;
formFieldsPage3['numeroChambre27']                                      = $ds046PE01.cadre2.etatDesLieux.chambre83.numeroChambre;
formFieldsPage3['numeroChambre28']                                      = $ds046PE01.cadre2.etatDesLieux.chambre84.numeroChambre;
formFieldsPage3['superficieChambre1']                                   = $ds046PE01.cadre2.etatDesLieux.chambre57.superficieChambre;
formFieldsPage3['superficieChambre2']                                   = $ds046PE01.cadre2.etatDesLieux.chambre58.superficieChambre;
formFieldsPage3['superficieChambre3']                                   = $ds046PE01.cadre2.etatDesLieux.chambre59.superficieChambre;
formFieldsPage3['superficieChambre4']                                   = $ds046PE01.cadre2.etatDesLieux.chambre60.superficieChambre;
formFieldsPage3['superficieChambre5']                                   = $ds046PE01.cadre2.etatDesLieux.chambre61.superficieChambre;
formFieldsPage3['superficieChambre6']                                   = $ds046PE01.cadre2.etatDesLieux.chambre62.superficieChambre;
formFieldsPage3['superficieChambre7']                                   = $ds046PE01.cadre2.etatDesLieux.chambre63.superficieChambre;
formFieldsPage3['superficieChambre8']                                   = $ds046PE01.cadre2.etatDesLieux.chambre64.superficieChambre;
formFieldsPage3['superficieChambre9']                                   = $ds046PE01.cadre2.etatDesLieux.chambre65.superficieChambre;
formFieldsPage3['superficieChambre10']                                  = $ds046PE01.cadre2.etatDesLieux.chambre66.superficieChambre;
formFieldsPage3['superficieChambre11']                                  = $ds046PE01.cadre2.etatDesLieux.chambre67.superficieChambre;
formFieldsPage3['superficieChambre12']                                  = $ds046PE01.cadre2.etatDesLieux.chambre68.superficieChambre;
formFieldsPage3['superficieChambre13']                                  = $ds046PE01.cadre2.etatDesLieux.chambre69.superficieChambre;
formFieldsPage3['superficieChambre14']                                  = $ds046PE01.cadre2.etatDesLieux.chambre70.superficieChambre;
formFieldsPage3['superficieChambre15']                                  = $ds046PE01.cadre2.etatDesLieux.chambre71.superficieChambre;
formFieldsPage3['superficieChambre16']                                  = $ds046PE01.cadre2.etatDesLieux.chambre72.superficieChambre;
formFieldsPage3['superficieChambre17']                                  = $ds046PE01.cadre2.etatDesLieux.chambre73.superficieChambre;
formFieldsPage3['superficieChambre18']                                  = $ds046PE01.cadre2.etatDesLieux.chambre74.superficieChambre;
formFieldsPage3['superficieChambre19']                                  = $ds046PE01.cadre2.etatDesLieux.chambre75.superficieChambre;
formFieldsPage3['superficieChambre20']                                  = $ds046PE01.cadre2.etatDesLieux.chambre76.superficieChambre;
formFieldsPage3['superficieChambre21']                                  = $ds046PE01.cadre2.etatDesLieux.chambre77.superficieChambre;
formFieldsPage3['superficieChambre22']                                  = $ds046PE01.cadre2.etatDesLieux.chambre78.superficieChambre;
formFieldsPage3['superficieChambre23']                                  = $ds046PE01.cadre2.etatDesLieux.chambre79.superficieChambre;
formFieldsPage3['superficieChambre24']                                  = $ds046PE01.cadre2.etatDesLieux.chambre80.superficieChambre;
formFieldsPage3['superficieChambre25']                                  = $ds046PE01.cadre2.etatDesLieux.chambre81.superficieChambre;
formFieldsPage3['superficieChambre26']                                  = $ds046PE01.cadre2.etatDesLieux.chambre82.superficieChambre;
formFieldsPage3['superficieChambre27']                                  = $ds046PE01.cadre2.etatDesLieux.chambre83.superficieChambre;
formFieldsPage3['superficieChambre28']                                  = $ds046PE01.cadre2.etatDesLieux.chambre84.superficieChambre;
formFieldsPage3['longueur1']                                            = $ds046PE01.cadre2.etatDesLieux.chambre57.longueur;
formFieldsPage3['longueur2']                                            = $ds046PE01.cadre2.etatDesLieux.chambre58.longueur;
formFieldsPage3['longueur3']                                            = $ds046PE01.cadre2.etatDesLieux.chambre59.longueur;
formFieldsPage3['longueur4']                                            = $ds046PE01.cadre2.etatDesLieux.chambre60.longueur;
formFieldsPage3['longueur5']                                            = $ds046PE01.cadre2.etatDesLieux.chambre61.longueur;
formFieldsPage3['longueur6']                                            = $ds046PE01.cadre2.etatDesLieux.chambre62.longueur;
formFieldsPage3['longueur7']                                            = $ds046PE01.cadre2.etatDesLieux.chambre63.longueur;
formFieldsPage3['longueur8']                                            = $ds046PE01.cadre2.etatDesLieux.chambre64.longueur;
formFieldsPage3['longueur9']                                            = $ds046PE01.cadre2.etatDesLieux.chambre65.longueur;
formFieldsPage3['longueur10']                                           = $ds046PE01.cadre2.etatDesLieux.chambre66.longueur;
formFieldsPage3['longueur11']                                           = $ds046PE01.cadre2.etatDesLieux.chambre67.longueur;
formFieldsPage3['longueur12']                                           = $ds046PE01.cadre2.etatDesLieux.chambre68.longueur;
formFieldsPage3['longueur13']                                           = $ds046PE01.cadre2.etatDesLieux.chambre69.longueur;
formFieldsPage3['longueur14']                                           = $ds046PE01.cadre2.etatDesLieux.chambre70.longueur;
formFieldsPage3['longueur15']                                           = $ds046PE01.cadre2.etatDesLieux.chambre71.longueur;
formFieldsPage3['longueur16']                                           = $ds046PE01.cadre2.etatDesLieux.chambre72.longueur;
formFieldsPage3['longueur17']                                           = $ds046PE01.cadre2.etatDesLieux.chambre73.longueur;
formFieldsPage3['longueur18']                                           = $ds046PE01.cadre2.etatDesLieux.chambre74.longueur;
formFieldsPage3['longueur19']                                           = $ds046PE01.cadre2.etatDesLieux.chambre75.longueur;
formFieldsPage3['longueur20']                                           = $ds046PE01.cadre2.etatDesLieux.chambre76.longueur;
formFieldsPage3['longueur21']                                           = $ds046PE01.cadre2.etatDesLieux.chambre77.longueur;
formFieldsPage3['longueur22']                                           = $ds046PE01.cadre2.etatDesLieux.chambre78.longueur;
formFieldsPage3['longueur23']                                           = $ds046PE01.cadre2.etatDesLieux.chambre79.longueur;
formFieldsPage3['longueur24']                                           = $ds046PE01.cadre2.etatDesLieux.chambre80.longueur;
formFieldsPage3['longueur25']                                           = $ds046PE01.cadre2.etatDesLieux.chambre81.longueur;
formFieldsPage3['longueur26']                                           = $ds046PE01.cadre2.etatDesLieux.chambre82.longueur;
formFieldsPage3['longueur27']                                           = $ds046PE01.cadre2.etatDesLieux.chambre83.longueur;
formFieldsPage3['longueur28']                                           = $ds046PE01.cadre2.etatDesLieux.chambre84.longueur;
formFieldsPage3['largeur1']                                             = $ds046PE01.cadre2.etatDesLieux.chambre57.largeur;
formFieldsPage3['largeur2']                                             = $ds046PE01.cadre2.etatDesLieux.chambre58.largeur;
formFieldsPage3['largeur3']                                             = $ds046PE01.cadre2.etatDesLieux.chambre59.largeur;
formFieldsPage3['largeur4']                                             = $ds046PE01.cadre2.etatDesLieux.chambre60.largeur;
formFieldsPage3['largeur5']                                             = $ds046PE01.cadre2.etatDesLieux.chambre61.largeur;
formFieldsPage3['largeur6']                                             = $ds046PE01.cadre2.etatDesLieux.chambre62.largeur;
formFieldsPage3['largeur7']                                             = $ds046PE01.cadre2.etatDesLieux.chambre63.largeur;
formFieldsPage3['largeur8']                                             = $ds046PE01.cadre2.etatDesLieux.chambre64.largeur;
formFieldsPage3['largeur9']                                             = $ds046PE01.cadre2.etatDesLieux.chambre65.largeur;
formFieldsPage3['largeur10']                                            = $ds046PE01.cadre2.etatDesLieux.chambre66.largeur;
formFieldsPage3['largeur11']                                            = $ds046PE01.cadre2.etatDesLieux.chambre67.largeur;
formFieldsPage3['largeur12']                                            = $ds046PE01.cadre2.etatDesLieux.chambre68.largeur;
formFieldsPage3['largeur13']                                            = $ds046PE01.cadre2.etatDesLieux.chambre69.largeur;
formFieldsPage3['largeur14']                                            = $ds046PE01.cadre2.etatDesLieux.chambre70.largeur;
formFieldsPage3['largeur15']                                            = $ds046PE01.cadre2.etatDesLieux.chambre71.largeur;
formFieldsPage3['largeur16']                                            = $ds046PE01.cadre2.etatDesLieux.chambre72.largeur;
formFieldsPage3['largeur17']                                            = $ds046PE01.cadre2.etatDesLieux.chambre73.largeur;
formFieldsPage3['largeur18']                                            = $ds046PE01.cadre2.etatDesLieux.chambre74.largeur;
formFieldsPage3['largeur19']                                            = $ds046PE01.cadre2.etatDesLieux.chambre75.largeur;
formFieldsPage3['largeur20']                                            = $ds046PE01.cadre2.etatDesLieux.chambre76.largeur;
formFieldsPage3['largeur21']                                            = $ds046PE01.cadre2.etatDesLieux.chambre77.largeur;
formFieldsPage3['largeur22']                                            = $ds046PE01.cadre2.etatDesLieux.chambre78.largeur;
formFieldsPage3['largeur23']                                            = $ds046PE01.cadre2.etatDesLieux.chambre79.largeur;
formFieldsPage3['largeur24']                                            = $ds046PE01.cadre2.etatDesLieux.chambre80.largeur;
formFieldsPage3['largeur25']                                            = $ds046PE01.cadre2.etatDesLieux.chambre81.largeur;
formFieldsPage3['largeur26']                                            = $ds046PE01.cadre2.etatDesLieux.chambre82.largeur;
formFieldsPage3['largeur27']                                            = $ds046PE01.cadre2.etatDesLieux.chambre83.largeur;
formFieldsPage3['largeur28']                                            = $ds046PE01.cadre2.etatDesLieux.chambre84.largeur;
formFieldsPage3['hauteur1']                                             = $ds046PE01.cadre2.etatDesLieux.chambre57.hauteur;
formFieldsPage3['hauteur2']                                             = $ds046PE01.cadre2.etatDesLieux.chambre58.hauteur;
formFieldsPage3['hauteur3']                                             = $ds046PE01.cadre2.etatDesLieux.chambre59.hauteur;
formFieldsPage3['hauteur4']                                             = $ds046PE01.cadre2.etatDesLieux.chambre60.hauteur;
formFieldsPage3['hauteur5']                                             = $ds046PE01.cadre2.etatDesLieux.chambre61.hauteur;
formFieldsPage3['hauteur6']                                             = $ds046PE01.cadre2.etatDesLieux.chambre62.hauteur;
formFieldsPage3['hauteur7']                                             = $ds046PE01.cadre2.etatDesLieux.chambre63.hauteur;
formFieldsPage3['hauteur8']                                             = $ds046PE01.cadre2.etatDesLieux.chambre64.hauteur;
formFieldsPage3['hauteur9']                                             = $ds046PE01.cadre2.etatDesLieux.chambre65.hauteur;
formFieldsPage3['hauteur10']                                            = $ds046PE01.cadre2.etatDesLieux.chambre66.hauteur;
formFieldsPage3['hauteur11']                                            = $ds046PE01.cadre2.etatDesLieux.chambre67.hauteur;
formFieldsPage3['hauteur12']                                            = $ds046PE01.cadre2.etatDesLieux.chambre68.hauteur;
formFieldsPage3['hauteur13']                                            = $ds046PE01.cadre2.etatDesLieux.chambre69.hauteur;
formFieldsPage3['hauteur14']                                            = $ds046PE01.cadre2.etatDesLieux.chambre70.hauteur;
formFieldsPage3['hauteur15']                                            = $ds046PE01.cadre2.etatDesLieux.chambre71.hauteur;
formFieldsPage3['hauteur16']                                            = $ds046PE01.cadre2.etatDesLieux.chambre72.hauteur;
formFieldsPage3['hauteur17']                                            = $ds046PE01.cadre2.etatDesLieux.chambre73.hauteur;
formFieldsPage3['hauteur18']                                            = $ds046PE01.cadre2.etatDesLieux.chambre74.hauteur;
formFieldsPage3['hauteur19']                                            = $ds046PE01.cadre2.etatDesLieux.chambre75.hauteur;
formFieldsPage3['hauteur20']                                            = $ds046PE01.cadre2.etatDesLieux.chambre76.hauteur;
formFieldsPage3['hauteur21']                                            = $ds046PE01.cadre2.etatDesLieux.chambre77.hauteur;
formFieldsPage3['hauteur22']                                            = $ds046PE01.cadre2.etatDesLieux.chambre78.hauteur;
formFieldsPage3['hauteur23']                                            = $ds046PE01.cadre2.etatDesLieux.chambre79.hauteur;
formFieldsPage3['hauteur24']                                            = $ds046PE01.cadre2.etatDesLieux.chambre80.hauteur;
formFieldsPage3['hauteur25']                                            = $ds046PE01.cadre2.etatDesLieux.chambre81.hauteur;
formFieldsPage3['hauteur26']                                            = $ds046PE01.cadre2.etatDesLieux.chambre82.hauteur;
formFieldsPage3['hauteur27']                                            = $ds046PE01.cadre2.etatDesLieux.chambre83.hauteur;
formFieldsPage3['hauteur28']                                            = $ds046PE01.cadre2.etatDesLieux.chambre84.hauteur;
formFieldsPage3['nbrePersonsonnesParChambre1']                          = $ds046PE01.cadre2.etatDesLieux.chambre57.nbrePersonsonnesParChambre;
formFieldsPage3['nbrePersonsonnesParChambre2']                          = $ds046PE01.cadre2.etatDesLieux.chambre58.nbrePersonsonnesParChambre;
formFieldsPage3['nbrePersonsonnesParChambre3']                          = $ds046PE01.cadre2.etatDesLieux.chambre59.nbrePersonsonnesParChambre;
formFieldsPage3['nbrePersonsonnesParChambre4']                          = $ds046PE01.cadre2.etatDesLieux.chambre60.nbrePersonsonnesParChambre;
formFieldsPage3['nbrePersonsonnesParChambre5']                          = $ds046PE01.cadre2.etatDesLieux.chambre61.nbrePersonsonnesParChambre;
formFieldsPage3['nbrePersonsonnesParChambre6']                          = $ds046PE01.cadre2.etatDesLieux.chambre62.nbrePersonsonnesParChambre;
formFieldsPage3['nbrePersonsonnesParChambre7']                          = $ds046PE01.cadre2.etatDesLieux.chambre63.nbrePersonsonnesParChambre;
formFieldsPage3['nbrePersonsonnesParChambre8']                          = $ds046PE01.cadre2.etatDesLieux.chambre64.nbrePersonsonnesParChambre;
formFieldsPage3['nbrePersonsonnesParChambre9']                          = $ds046PE01.cadre2.etatDesLieux.chambre65.nbrePersonsonnesParChambre;
formFieldsPage3['nbrePersonsonnesParChambre10']                         = $ds046PE01.cadre2.etatDesLieux.chambre66.nbrePersonsonnesParChambre;
formFieldsPage3['nbrePersonsonnesParChambre11']                         = $ds046PE01.cadre2.etatDesLieux.chambre67.nbrePersonsonnesParChambre;
formFieldsPage3['nbrePersonsonnesParChambre12']                         = $ds046PE01.cadre2.etatDesLieux.chambre68.nbrePersonsonnesParChambre;
formFieldsPage3['nbrePersonsonnesParChambre13']                         = $ds046PE01.cadre2.etatDesLieux.chambre69.nbrePersonsonnesParChambre;
formFieldsPage3['nbrePersonsonnesParChambre14']                         = $ds046PE01.cadre2.etatDesLieux.chambre70.nbrePersonsonnesParChambre;
formFieldsPage3['nbrePersonsonnesParChambre15']                         = $ds046PE01.cadre2.etatDesLieux.chambre71.nbrePersonsonnesParChambre;
formFieldsPage3['nbrePersonsonnesParChambre16']                         = $ds046PE01.cadre2.etatDesLieux.chambre72.nbrePersonsonnesParChambre;
formFieldsPage3['nbrePersonsonnesParChambre17']                         = $ds046PE01.cadre2.etatDesLieux.chambre73.nbrePersonsonnesParChambre;
formFieldsPage3['nbrePersonsonnesParChambre18']                         = $ds046PE01.cadre2.etatDesLieux.chambre74.nbrePersonsonnesParChambre;
formFieldsPage3['nbrePersonsonnesParChambre19']                         = $ds046PE01.cadre2.etatDesLieux.chambre75.nbrePersonsonnesParChambre;
formFieldsPage3['nbrePersonsonnesParChambre20']                         = $ds046PE01.cadre2.etatDesLieux.chambre76.nbrePersonsonnesParChambre;
formFieldsPage3['nbrePersonsonnesParChambre21']                         = $ds046PE01.cadre2.etatDesLieux.chambre77.nbrePersonsonnesParChambre;
formFieldsPage3['nbrePersonsonnesParChambre22']                         = $ds046PE01.cadre2.etatDesLieux.chambre78.nbrePersonsonnesParChambre;
formFieldsPage3['nbrePersonsonnesParChambre23']                         = $ds046PE01.cadre2.etatDesLieux.chambre79.nbrePersonsonnesParChambre;
formFieldsPage3['nbrePersonsonnesParChambre24']                         = $ds046PE01.cadre2.etatDesLieux.chambre80.nbrePersonsonnesParChambre;
formFieldsPage3['nbrePersonsonnesParChambre25']                         = $ds046PE01.cadre2.etatDesLieux.chambre81.nbrePersonsonnesParChambre;
formFieldsPage3['nbrePersonsonnesParChambre26']                         = $ds046PE01.cadre2.etatDesLieux.chambre82.nbrePersonsonnesParChambre;
formFieldsPage3['nbrePersonsonnesParChambre27']                         = $ds046PE01.cadre2.etatDesLieux.chambre83.nbrePersonsonnesParChambre;
formFieldsPage3['nbrePersonsonnesParChambre28']                         = $ds046PE01.cadre2.etatDesLieux.chambre84.nbrePersonsonnesParChambre;
																																		
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
 var finalDoc = nash.doc 
	.load('models/Courrier au premier dossier v2.1 GE.pdf')
	.apply({
		date: $ds046PE01.cadre2.etatDesLieux.etatDesLieuxSignature.dateSignature,
		autoriteHabilitee :"Préfecture de "+ $ds046PE01.cadre1.cadre1DeclarationHotel.renseignementEtablissement.adresse.commune,
		demandeContexte : "Déclaration exploitation hôtel",
		civiliteNomPrenom : civNomPrenom
	});
	
	function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Création du dossier avec la première d'état des lieux
 */
var cerfaDeclaration = nash.doc //
	.load('models/declaration_exploitation_hotel.pdf') //
	.apply(formFields);
finalDoc.append(cerfaDeclaration.save('cerfa.pdf'));

if ($ds046PE01.cadre1.cadre1DeclarationHotel.renseignementEtablissement.etablissementNbreTotalChambres > 28)
{
var cerfaPage2 = nash.doc //
	.load('models/declaration_exploitation_hotel_2.pdf') //
	.apply(formFieldsPage2);
finalDoc.append(cerfaPage2.save('cerfa.pdf'));
}
if ($ds046PE01.cadre1.cadre1DeclarationHotel.renseignementEtablissement.etablissementNbreTotalChambres > 56)
{
var cerfaPage3 = nash.doc //
	.load('models/declaration_exploitation_hotel_3.pdf') //
	.apply(formFieldsPage3);
finalDoc.append(cerfaPage3.save('cerfa.pdf'));
}
/* Remplissage des chambres */
/* var chambreSize = $ds046PE01.cadre2.etatDesLieux.chambre.size(); //2

var nbEtages = 28;
var nbChambresPdf = 0;
if ((chambreSize % nbEtages) == 0) {
	nbChambresPdf = Math.round(chambreSize/28);
} else {
	nbChambresPdf = Math.round(chambreSize/28) + 1; //1
}

for ( i = 1; i <= nbChambresPdf; i++ ) {
	var chambrePdf = nash.doc.load('models/declaration_exploitation_hotel_2.pdf');
	var	formFields = {};
	for ( indexChambre = 1; indexChambre <= chambreSize; indexChambre++ ) {
		formFields['etage' + indexChambre ] = $ds046PE01.cadre2.etatDesLieux.chambre[indexChambre-1].etage ? $ds046PE01.cadre2.etatDesLieux.chambre[indexChambre-1].etage : '';
		formFields['numeroChambre' + indexChambre ] = $ds046PE01.cadre2.etatDesLieux.chambre[indexChambre-1].numeroChambre ? $ds046PE01.cadre2.etatDesLieux.chambre[indexChambre-1].numeroChambre : '';
		formFields['superficieChambre' + indexChambre ] = $ds046PE01.cadre2.etatDesLieux.chambre[indexChambre-1].superficieChambre ? $ds046PE01.cadre2.etatDesLieux.chambre[indexChambre-1].superficieChambre : '';
		formFields['longueur' + indexChambre ] = $ds046PE01.cadre2.etatDesLieux.chambre[indexChambre-1].longueur ? $ds046PE01.cadre2.etatDesLieux.chambre[indexChambre-1].longueur : '';
		formFields['largeur' + indexChambre ] = $ds046PE01.cadre2.etatDesLieux.chambre[indexChambre-1].largeur ? $ds046PE01.cadre2.etatDesLieux.chambre[indexChambre-1].largeur : '';
		formFields['hauteur' + indexChambre ] = $ds046PE01.cadre2.etatDesLieux.chambre[indexChambre-1].hauteur ? $ds046PE01.cadre2.etatDesLieux.chambre[indexChambre-1].hauteur : '';
		formFields['nbrePersonsonnesParChambre' + indexChambre ] = $ds046PE01.cadre2.etatDesLieux.chambre[indexChambre-1].nbrePersonsonnesParChambre ? $ds046PE01.cadre2.etatDesLieux.chambre[indexChambre-1].nbrePersonsonnesParChambre : '';
	}
	
	chambrePdf.apply(formFields);
	finalDoc.append(chambrePdf.save('chambre.pdf'));
} */





/*
 * Création du dossier sans option fiscale : ajout du cerfa sans option fiscale
 */
 
/*  var cerfaDoc2 = nash.doc //
	.load('models/cerfa_13959-4_mSAS_sans_option_fiscale.pdf') //
	.apply (formFields);
	finalDoc.append(cerfaDoc2.save('cerfa.pdf'));

 */
/*
 * Ajout des PJs
 */
 //Identification de la société
/* appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExtraitRegistre);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCopiePropriete);

var pj=$ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireFond.questionPersonnalite;
if(Value('id').of(pj).contains('personneMorale')) {
    appendPj($attachmentPreprocess.attachmentPreprocess.pjPVAssemble);
}

var pj=$ds046PE01.cadre1.cadre1DeclarationHotel.proprietaireFond.questionProprietaire;
if(pj == true) {
    appendPj($attachmentPreprocess.attachmentPreprocess.pjContratLocation);
}

var pj=$ds046PE01.cadre1.cadre1DeclarationHotel.etatCivil1.declarantExploitant;
if(pj == true) {
    appendPj($attachmentPreprocess.attachmentPreprocess.pjDelegationPouvoir);
}
 */


/*
 * Enregistrement du fichier (en mémoire)
 */
 
var finalDocItem = finalDoc.save('declaration_exploitation_hotel_DS.pdf');

var data = [ spec.createData({
    id : 'record',
    label : 'Déclaration exploitation hôtel',
    description : 'Voici le formulaire obtenu à partir des données saisies.',
    type : 'FileReadOnly',
    value : [ finalDocItem ]
}) ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Déclaration exploitation hôtel',
    groups : groups
});