function pad(s) { return (s < 10) ? '0' + s : s; }

var formFields = {};

var civNomPrenom = $ds006PE5.cadre1.cadre1.civilite + ' ' + $ds006PE5.cadre1.cadre1.nomNaissance + ' ' + $ds006PE5.cadre1.cadre1.prenomDeclarant;


/// Dossier de déclaration

formFields['declarationInitiale']                     = true;
formFields['renouvellement']                          = false;
formFields['departementExercice']                     = $ds006PE5.declaration.declaration.departementexercice;


//Etat Civil
formFields['civiliteMadame']                           = ($ds006PE5.cadre1.cadre1.civilite=='Madame');
formFields['civiliteMonsieur']                         = ($ds006PE5.cadre1.cadre1.civilite=='Monsieur');
formFields['nomNaissance']                             = $ds006PE5.cadre1.cadre1.nomNaissance;
formFields['nomUsage']                                 = $ds006PE5.cadre1.cadre1.nomUsage;
formFields['prenom']                                   = $ds006PE5.cadre1.cadre1.prenomDeclarant;
formFields['nomPere']                                  = $ds006PE5.cadre1.cadre1.nomPereDeclarant;
formFields['nomMere']                                  = $ds006PE5.cadre1.cadre1.nomMereDeclarant;
formFields['prenomPere']                               = $ds006PE5.cadre1.cadre1.prenomPereDeclarant;
formFields['prenomMere']                               = $ds006PE5.cadre1.cadre1.prenomMereDeclarant;


if($ds006PE5.cadre1.cadre1.dateNaissanceDeclarant != null) {
var dateTemp = new Date(parseInt ($ds006PE5.cadre1.cadre1.dateNaissanceDeclarant.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['dateNaissanceJour'] = day;
	formFields['dateNaissanceMois'] = month;
	formFields['dateNaissanceAnnee'] = year;
}


formFields['villeNaissance']                           = $ds006PE5.cadre1.cadre1.lieuNaissanceDeclarant;
formFields['paysNaissance']                            = $ds006PE5.cadre1.cadre1.paysNaissanceDeclarant;
formFields['arrondissementNaissance']                  = $ds006PE5.cadre1.cadre1.arrondissement;
formFields['departementNaissance']                     = $ds006PE5.cadre1.cadre1.departementNaissance;
formFields['nationalite']                              = $ds006PE5.cadre1.cadre1.nationaliteDeclarant;
formFields['nationaliteAutre']                         = $ds006PE5.cadre1.cadre1.autrenationaliteDeclarant;



//Coordonnées

formFields['adresseDeclarantPays']                     = $ds006PE5.adresse.adressePersonnelle.adresseDeclarantPays;
formFields['adresseDeclarantVille']                    = $ds006PE5.adresse.adressePersonnelle.adresseDeclarantVille;
formFields['adresseDeclarantComplement2']              = $ds006PE5.adresse.adressePersonnelle.adressecomplement2;
formFields['adresseDeclarantComplement1']              = $ds006PE5.adresse.adressePersonnelle.adressecomplement1;
formFields['telephoneFixeDeclarant']                   = $ds006PE5.adresse.adressePersonnelle.telephoneFixeDeclarant;
formFields['telephoneMobileDeclarant']                 = $ds006PE5.adresse.adressePersonnelle.telephoneMobileDeclarant;
formFields['adresseDeclarantNumeroNomRue']             = $ds006PE5.adresse.adressePersonnelle.adresseDeclarantNumeroNomRue;
formFields['courrielDeclarant']                        = $ds006PE5.adresse.adressePersonnelle.courrielDeclarant;
formFields['adresseDeclarantCodePostal']               = $ds006PE5.adresse.adressePersonnelle.adresseDeclarantCodePostal;



//Titulaire de qualification française

//qualification2
formFields['qualificationJourObtention1']              ='';
formFields['qualificationMoisObtention1']              ='';
formFields['qualificationAnneeObtention1']              ='';
formFields['qualificationType1']                       = '';
formFields['qualificationActivite1']                   = '';
formFields['qualificationNumeroDiplome1']              = '';
formFields['qualificationLieuObtention1']              = '';
formFields['qualificationJourRecyclage1']              ='';
formFields['qualificationMoisRecyclage1']              ='';
formFields['qualificationAnneeRecyclage1']              ='';
formFields['qualificationJourObtention2']              ='';
formFields['qualificationMoisObtention2']              ='';
formFields['qualificationAnneeObtention2']              ='';
formFields['qualificationType2']                       = '';
formFields['qualificationActivite2']                   = '';
formFields['qualificationNumeroDiplome2']              = '';
formFields['qualificationLieuObtention2']              = '';
formFields['qualificationJourRecyclage2']              ='';
formFields['qualificationMoisRecyclage2']              ='';
formFields['qualificationAnneeRecyclage2']              ='';
formFields['qualificationJourObtention3']              ='';
formFields['qualificationMoisObtention3']              ='';
formFields['qualificationAnneeObtention3']              ='';
formFields['qualificationType3']                       = '';
formFields['qualificationActivite3']                   = '';
formFields['qualificationNumeroDiplome3']              = '';
formFields['qualificationLieuObtention3']              = '';
formFields['qualificationJourRecyclage3']              ='';
formFields['qualificationMoisRecyclage3']              ='';
formFields['qualificationAnneeRecyclage3']              ='';
formFields['qualificationJourObtention4']              ='';
formFields['qualificationMoisObtention4']              ='';
formFields['qualificationAnneeObtention4']              ='';
formFields['qualificationType4']                       = '';
formFields['qualificationActivite4']                   = '';
formFields['qualificationNumeroDiplome4']              = '';
formFields['qualificationLieuObtention4']              = '';
formFields['qualificationJourRecyclage4']              ='';
formFields['qualificationMoisRecyclage4']              ='';
formFields['qualificationAnneeRecyclage4']              ='';
formFields['qualificationJourObtention5']              ='';
formFields['qualificationMoisObtention5']              ='';
formFields['qualificationAnneeObtention5']              ='';
formFields['qualificationType5']                       = '';
formFields['qualificationActivite5']                   = '';
formFields['qualificationNumeroDiplome5']              = '';
formFields['qualificationLieuObtention5']              = '';
formFields['qualificationJourRecyclage5']              ='';
formFields['qualificationMoisRecyclage5']              ='';
formFields['qualificationAnneeRecyclage5']              ='';
formFields['qualificationJourObtention6']              ='';
formFields['qualificationMoisObtention6']              ='';
formFields['qualificationAnneeObtention6']              ='';
formFields['qualificationType6']                       = '';
formFields['qualificationActivite6']                   = '';
formFields['qualificationNumeroDiplome6']              = '';
formFields['qualificationLieuObtention6']              = '';
formFields['qualificationJourRecyclage6']              ='';
formFields['qualificationMoisRecyclage6']              ='';
formFields['qualificationAnneeRecyclage6']              ='';
formFields['qualificationJourObtention7']              ='';
formFields['qualificationMoisObtention7']              ='';
formFields['qualificationAnneeObtention7']              ='';
formFields['qualificationType7']                       = '';
formFields['qualificationActivite7']                   = '';
formFields['qualificationNumeroDiplome7']              = '';
formFields['qualificationLieuObtention7']              = '';
formFields['qualificationJourRecyclage7']              ='';
formFields['qualificationMoisRecyclage7']              ='';
formFields['qualificationAnneeRecyclage7']              ='';
formFields['qualificationJourObtention8']              ='';
formFields['qualificationMoisObtention8']              ='';
formFields['qualificationAnneeObtention8']              ='';
formFields['qualificationType8']                       = '';
formFields['qualificationActivite8']                   = '';
formFields['qualificationNumeroDiplome8']              = '';
formFields['qualificationLieuObtention8']              = '';
formFields['qualificationJourRecyclage8']              ='';
formFields['qualificationMoisRecyclage8']              ='';
formFields['qualificationAnneeRecyclage8']              ='';
formFields['qualificationJourObtention9']              ='';
formFields['qualificationMoisObtention9']              ='';
formFields['qualificationAnneeObtention9']              ='';
formFields['qualificationType9']                       = '';
formFields['qualificationActivite9']                   = '';
formFields['qualificationNumeroDiplome9']              = '';
formFields['qualificationLieuObtention9']              = '';
formFields['qualificationJourRecyclage9']              ='';
formFields['qualificationMoisRecyclage9']              ='';
formFields['qualificationAnneeRecyclage9']              ='';
	

//Equivalence diplôme
formFields['equivalenceType1']               = '';
formFields['equivalenceType2']               = '';
formFields['equivalenceType3']               = '';
formFields['equivalenceActivite1']           = '';
formFields['equivalenceActivite2']           = '';
formFields['equivalenceActivite3']           = '';
formFields['equivalenceNumero1']             = '';
formFields['equivalenceNumero2']             = '';
formFields['equivalenceNumero3']             = '';
formFields['equivalenceJourDelivrance1']              ='';
formFields['equivalenceJourDelivrance2']              ='';
formFields['equivalenceJourDelivrance3']              ='';
formFields['equivalenceMoisDelivrance1']              ='';
formFields['equivalenceMoisDelivrance2']              ='';
formFields['equivalenceMoisDelivrance3']              ='';
formFields['equivalenceAnneeDelivrance1']              ='';
formFields['equivalenceAnneeDelivrance2']              ='';
formFields['equivalenceAnneeDelivrance3']              ='';

//beneficiaire equivalence diplôme
formFields['equivalenceType6']                       ='';
formFields['equivalenceType7']                       ='';
formFields['equivalenceType8']                       ='';
formFields['equivalenceType9']                       ='';
formFields['equivalenceActivite6']                   ='';
formFields['equivalenceActivite7']                   ='';
formFields['equivalenceActivite8']                   ='';
formFields['equivalenceActivite9']                   ='';
formFields['equivalenceNumero6']                     ='';
formFields['equivalenceNumero7']                     ='';
formFields['equivalenceNumero8']                     ='';
formFields['equivalenceNumero9']                     ='';
formFields['equivalenceJourDelivrance6']             ='';
formFields['equivalenceJourDelivrance7']             ='';
formFields['equivalenceJourDelivrance8']             ='';
formFields['equivalenceJourDelivrance9']             ='';
formFields['equivalenceMoisDelivrance6']             ='';
formFields['equivalenceMoisDelivrance7']             ='';
formFields['equivalenceMoisDelivrance8']             ='';
formFields['equivalenceMoisDelivrance9']             ='';
formFields['equivalenceAnneeDelivrance6']             ='';
formFields['equivalenceAnneeDelivrance7']             ='';
formFields['equivalenceAnneeDelivrance8']             ='';
formFields['equivalenceAnneeDelivrance9']             ='';

//personnes en formation (préparation diplome)
formFields['intituleDiplomePreparation']              ='';
formFields['formationType']                           ='';
formFields['formationActivite']                       ='';

formFields['stageNomEtablissement1']                  ='';
formFields['stageNomEtablissement2']                  ='';
formFields['stageNomEtablissement3']                  ='';
formFields['stageNomEtablissement4']                  ='';
formFields['stageNumeroNomRueAdresseEtablissement1']        ='';
formFields['stageNumeroNomRueAdresseEtablissement2']        ='';
formFields['stageNumeroNomRueAdresseEtablissement3']        ='';
formFields['stageNumeroNomRueAdresseEtablissement4']        ='';
formFields['stageVillePaysAdresseEtablissement1']           ='';
formFields['stageVillePaysAdresseEtablissement2']           ='';
formFields['stageVillePaysAdresseEtablissement3']           ='';
formFields['stageVillePaysAdresseEtablissement4']           ='';
formFields['stageJourDebut1']              ='';
formFields['stageJourDebut2']              ='';
formFields['stageJourDebut3']              ='';
formFields['stageJourDebut4']              ='';
formFields['stageMoisDebut1']              ='';
formFields['stageMoisDebut2']              ='';
formFields['stageMoisDebut3']              ='';
formFields['stageMoisDebut4']              ='';
formFields['stageAnneeDebut1']             ='';
formFields['stageAnneeDebut2']             ='';
formFields['stageAnneeDebut3']             ='';
formFields['stageAnneeDebut4']             ='';
formFields['stageJourFin1']              ='';
formFields['stageJourFin2']              ='';
formFields['stageJourFin3']              ='';
formFields['stageJourFin4']              ='';
formFields['stageMoisFin1']              ='';
formFields['stageMoisFin2']              ='';
formFields['stageMoisFin3']              ='';
formFields['stageMoisFin4']              ='';
formFields['stageAnneeFin1']             ='';
formFields['stageAnneeFin2']             ='';
formFields['stageAnneeFin3']             ='';
formFields['stageAnneeFin4']             ='';
formFields['stageNomTuteur1']             ='';
formFields['stageNomTuteur2']             ='';
formFields['stageNomTuteur3']             ='';
formFields['stageNomTuteur4']             ='';
formFields['autorisationExerciceJourDelivrance']           ='';
formFields['autorisationExerciceMoisDelivrance']           ='';
formFields['autorisationExerciceAnneeDelivrance']           ='';

//Equivalences

if($ds006PE5.autorisationexercice.autorisationexercice.autorisation != null) {
var dateTemp = new Date(parseInt ($ds006PE5.autorisationexercice.autorisationexercice.autorisation.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['autorisationExerciceJourDelivrance'] = day;
	formFields['autorisationExerciceMoisDelivrance'] = month;


//personnes en formation (préparation diplome) (volet complémentaire)
formFields['intituleDiplomePreparation1']              ='';
formFields['formationType1']                           ='';
formFields['formationActivite1']                       ='';

formFields['stageNomEtablissement5']                  ='';
formFields['stageNomEtablissement6']                  ='';
formFields['stageNomEtablissement7']                  ='';
formFields['stageNomEtablissement8']                  ='';
formFields['stageNumeroNomRueAdresseEtablissement5']        ='';
formFields['stageNumeroNomRueAdresseEtablissement6']        ='';
formFields['stageNumeroNomRueAdresseEtablissement7']        ='';
formFields['stageNumeroNomRueAdresseEtablissement8']        ='';
formFields['stageVillePaysAdresseEtablissement5']           ='';
formFields['stageVillePaysAdresseEtablissement6']           ='';
formFields['stageVillePaysAdresseEtablissement7']           ='';
formFields['stageVillePaysAdresseEtablissement8']           ='';
formFields['stageJourDebut5']              ='';
formFields['stageJourDebut6']              ='';
formFields['stageJourDebut7']              ='';
formFields['stageJourDebut8']              ='';
formFields['stageMoisDebut5']              ='';
formFields['stageMoisDebut6']              ='';
formFields['stageMoisDebut7']              ='';
formFields['stageMoisDebut8']              ='';
formFields['stageAnneeDebut5']             ='';
formFields['stageAnneeDebut6']             ='';
formFields['stageAnneeDebut7']             ='';
formFields['stageAnneeDebut8']             ='';
formFields['stageJourFin5']              ='';
formFields['stageJourFin6']              ='';
formFields['stageJourFin7']              ='';
formFields['stageJourFin8']              ='';
formFields['stageMoisFin5']              ='';
formFields['stageMoisFin6']              ='';
formFields['stageMoisFin7']              ='';
formFields['stageMoisFin8']              ='';
formFields['stageAnneeFin5']             ='';
formFields['stageAnneeFin6']             ='';
formFields['stageAnneeFin7']             ='';
formFields['stageAnneeFin8']             ='';
formFields['stageNomTuteur5']             ='';
formFields['stageNomTuteur6']             ='';
formFields['stageNomTuteur7']             ='';
formFields['stageNomTuteur8']             ='';
formFields['autorisationExerciceJourDelivrance2']           ='';
formFields['autorisationExerciceMoisDelivrance2']           ='';
formFields['autorisationExerciceAnneeDelivrance2']           ='';
	formFields['autorisationExerciceAnneeDelivrance'] = year;
}
// activités physiques ou sportives encadrées

formFields['metierIndependantPrincipal']               = ($ds006PE5.activites.activites.metierEducateur=='Principal');
formFields['metierIndependantSecondaire']              = ($ds006PE5.activites.activites.metierEducateur=='Secondaire');

formFields['raisonSocialeIndependant']                 = $ds006PE5.activites.activites.raisonsociale;
formFields['formeEURL']                                = ($ds006PE5.activites.activites.naturejuridique=='EURL');
formFields['formeIndividuelle']                        = ($ds006PE5.activites.activites.naturejuridique=='Entreprise individuelle');
formFields['numeroSiretIndependants']                  =$ds006PE5.activites.activites.numerosiret;

formFields['adresseDeclarantNumeroNomRueIndependants'] = $ds006PE5.activites.activites.adresseDeclarantNumeroNomRueIndependant;
formFields['adresseDeclarantCodePostalIndependants']   = $ds006PE5.activites.activites.adresseDeclarantCodePostalIndependant;
formFields['adresseDeclarantVilleIndependants']        = $ds006PE5.activites.activites.adresseDeclarantVilleIndependant;
formFields['telephoneFixeDeclarantIndependant']        = $ds006PE5.activites.activites.telephoneFixeDeclarantIndependant;
formFields['telephoneMobileDeclarantIndependant']      = $ds006PE5.activites.activites.telephoneMobileDeclarantIndependant;
formFields['courrielDeclarantIndependant']             = $ds006PE5.activites.activites.courrielDeclarantIndependant;
formFields['siteInternetDeclarantIndependant']         = $ds006PE5.activites.activites.siteinternet;

// Activite encadrée independant
/// 1

formFields['activiteEncadreeIndependant']              = $ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.activiteencadreeindependant1;
formFields['disciplineIndependant']                    = $ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.disciplineIndependant1;
formFields['lieuExerciceIndependant']                  = $ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.lieuexerciceIndependant1;
formFields['numeroNomVoieLieuExerciceIndependant']     = $ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.adresseExerciceIndependant1;
formFields['departementLieuExerciceIndependant']       = $ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.departementIndependant1;
formFields['codePostalLieuExerciceIndependant']        = $ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.codePostalIndependant1;
formFields['communeLieuExerciceIndependant']           = $ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.communeIndependant1;


if($ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant1 != null) {
var dateTemp = new Date(parseInt ($ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant1.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourDebutExerciceIndependant'] = day;
	formFields['moisDebutExerciceIndependant'] = month;
	formFields['anneeDebutExerciceIndependant'] = year;
}

///2 

formFields['activiteEncadreeIndependant2']              = $ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.activiteencadreeindependant2;
formFields['disciplineIndependant2']                    = $ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.disciplineIndependant2;
formFields['lieuExerciceIndependant2']                  = $ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.lieuexerciceIndependant2;
formFields['numeroNomVoieLieuExerciceIndependant2']     = $ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.adresseExerciceIndependant2;
formFields['departementLieuExerciceIndependant2']       = $ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.departementIndependant2;
formFields['codePostalLieuExerciceIndependant2']        = $ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.codePostalIndependant2;
formFields['communeLieuExerciceIndependant2']           = $ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.communeIndependant2;

formFields['jourDebutExerciceIndependant2']             ='';
formFields['moisDebutExerciceIndependant2']             ='';
formFields['anneeDebutExerciceIndependant2']            ='';
if($ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant2 != null) {
var dateTemp = new Date(parseInt ($ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant2.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourDebutExerciceIndependant2'] = day;
	formFields['moisDebutExerciceIndependant2'] = month;
	formFields['anneeDebutExerciceIndependant2'] = year;
}


///3

formFields['activiteEncadreeIndependant3']              = $ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.activiteencadreeindependant3;
formFields['disciplineIndependant3']                    = $ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.disciplineIndependant3;
formFields['lieuExerciceIndependant3']                  = $ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.lieuexerciceIndependant3;
formFields['numeroNomVoieLieuExerciceIndependant3']     = $ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.adresseExerciceIndependant3;
formFields['departementLieuExerciceIndependant3']       = $ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.departementIndependant3;
formFields['codePostalLieuExerciceIndependant3']        = $ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.codePostalIndependant3;
formFields['communeLieuExerciceIndependant3']           = $ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.communeIndependant3;

formFields['jourDebutExerciceIndependant3']             ='';
formFields['moisDebutExerciceIndependant3']             ='';
formFields['anneeDebutExerciceIndependant3']            ='';
if($ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant3 != null) {
var dateTemp = new Date(parseInt ($ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant3.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourDebutExerciceIndependant3'] = day;
	formFields['moisDebutExerciceIndependant3'] = month;
	formFields['anneeDebutExerciceIndependant3'] = year;
}


///4

formFields['activiteEncadreeIndependant4']              = $ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.activiteencadreeindependant4;
formFields['disciplineIndependant4']                    = $ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.disciplineIndependant4;
formFields['lieuExerciceIndependant4']                  = $ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.lieuexerciceIndependant4;
formFields['numeroNomVoieLieuExerciceIndependant4']     = $ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.adresseExerciceIndependant4;
formFields['departementLieuExerciceIndependant4']       = $ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.departementIndependant4;
formFields['codePostalLieuExerciceIndependant4']        = $ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.codePostalIndependant4;
formFields['communeLieuExerciceIndependant4']           = $ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.communeIndependant4;

formFields['jourDebutExerciceIndependant4']             ='';
formFields['moisDebutExerciceIndependant4']             ='';
formFields['anneeDebutExerciceIndependant4']            ='';
if($ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant4 != null) {
var dateTemp = new Date(parseInt ($ds006PE5.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant4.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourDebutExerciceIndependant4'] = day;
	formFields['moisDebutExerciceIndependant4'] = month;
	formFields['anneeDebutExerciceIndependant4'] = year;
}




//Signature



formFields['nomPrenomSignature']                       = $ds006PE5.cadre1.cadre1.nomNaissance + ' ' + $ds006PE5.cadre1.cadre1.prenomDeclarant;
formFields['Signature']                                = $ds006PE5.signature.signature.signatureCoche;
formFields['dateSignature']                            = $ds006PE5.signature.signature.faitLe;



/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
 
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $ds006PE5.signature.signature.faitLe,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		dateSignature: $ds006PE5.signature.signature.faitLe,
		autoriteHabilitee :"DDCSPP",
		demandeContexte : "Bénéficiaire d'une autorisation d'exercice délivrée par la Commission nationale de l'enseignement  des activités physiques et sportives",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
    .load('models/cerfa1269903.pdf') //
    .apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
 
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertificatMedical);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPhoto);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAutorExercice);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Centre_equestre_assimiles_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
   label : 'Centre équestre et assimilés  - Bénéficiaire d\'une autorisation d\'exercice délivrée par la Commission nationale de l\'enseignement  des activités physiques et sportives',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Bénéficiaire d\'une autorisation d\'exercice délivrée par la Commission nationale de l\'enseignement  des activités physiques et sportives',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});