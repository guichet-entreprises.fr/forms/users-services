function pad(s) { return (s < 10) ? '0' + s : s; }

var formFields = {};

var civNomPrenom = ($ds006PE7.cadre1.cadre1.civilite != null ? $ds006PE7.cadre1.cadre1.civilite :'') +' '+($ds006PE7.cadre1.cadre1.nomNaissanceDeclarant != null ? $ds006PE7.cadre1.cadre1.nomNaissanceDeclarant :'')+ ' ' +($ds006PE7.cadre1.cadre1.prenomDeclarant != null ? $ds006PE7.cadre1.cadre1.prenomDeclarant:'') + '' + ($ds006PE7.cadre1.cadre1.denominationSociale != null ? $ds006PE7.cadre1.cadre1.denominationSociale:'') + '' + ($ds006PE7.cadre1.cadre1.civilite2 !=null ? $ds006PE7.cadre1.cadre1.civilite2 :'') +' '+  ($ds006PE7.cadre1.cadre1.nomNaissanceDeclarant2 != null ? $ds006PE7.cadre1.cadre1.nomNaissanceDeclarant2 :'') +' '+ ($ds006PE7.cadre1.cadre1.prenomDeclarant2 != null ? $ds006PE7.cadre1.cadre1.prenomDeclarant2 :'');



//identificationResponsable

//Pour les particuliers 

formFields['Madame']                           = ($ds006PE7.cadre1.cadre1.civilite=='Madame');
formFields['Monsieur']                         = ($ds006PE7.cadre1.cadre1.civilite=='Monsieur');
formFields['Mademoiselle']                     = ($ds006PE7.cadre1.cadre1.civilite=='Mademoiselle');
formFields['nomNaissanceDeclarant']            = $ds006PE7.cadre1.cadre1.nomNaissanceDeclarant;
formFields['nomUsageDeclarant']                = $ds006PE7.cadre1.cadre1.nomUsageDeclarant;
formFields['prenomDeclarant']                  = $ds006PE7.cadre1.cadre1.prenomDeclarant;



	formFields['dateNaissanceJoursDeclarant']    =  '';
	formFields['dateNaissanceMoisDeclarant']     =  '';
	formFields['dateNaissanceAnneeDeclarant']    =  '';

if($ds006PE7.cadre1.cadre1.dateNaissanceDeclarant != null) {
var dateTemp = new Date(parseInt ($ds006PE7.cadre1.cadre1.dateNaissanceDeclarant.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['dateNaissanceJoursDeclarant']    =  day;
	formFields['dateNaissanceMoisDeclarant']     = month;
	formFields['dateNaissanceAnneeDeclarant']    =  year;
}

formFields['paysNaissanceDeclarant']          = $ds006PE7.cadre1.cadre1.paysNaissanceDeclarant;
formFields['departementNaissanceDeclarant']   = $ds006PE7.cadre1.cadre1.departementNaissanceDeclarant;
formFields['numagritDeclarant']               = $ds006PE7.cadre1.cadre1.numagritDeclarant;

//Pour les sociétés  

formFields['numeroSiret']                    = $ds006PE7.cadre1.cadre1.numeroSiret;
formFields['denominationSociale']            = $ds006PE7.cadre1.cadre1.denominationSociale;
formFields['statutJuridique']                = $ds006PE7.cadre1.cadre1.statutJuridique;
formFields['codeApe1']                       = $ds006PE7.cadre1.cadre1.codeApe1;

//Entreprise en nom propre
formFields['madameEntreprise']               = Value('id').of($ds006PE7.cadre1.cadre1.civilite2).eq('madameEntreprise')? true : false;
formFields['monsieurEntreprise']             = Value('id').of($ds006PE7.cadre1.cadre1.civilite2).eq('monsieurEntreprise') ? true : false;
formFields['mlleEntreprise']                 = Value('id').of($ds006PE7.cadre1.cadre1.civilite2).eq('mlleEntreprise') ? true : false;
formFields['numeroSiret2']                   = $ds006PE7.cadre1.cadre1.numeroSiret2;
formFields['prenomDeclarant2']               = $ds006PE7.cadre1.cadre1.prenomDeclarant2;
formFields['nomUsageDeclarant2']             = $ds006PE7.cadre1.cadre1.nomUsageDeclarant2;
formFields['nomNaissanceDeclarant2']         = $ds006PE7.cadre1.cadre1.nomNaissanceDeclarant2;
formFields['codeApe2']                       = $ds006PE7.cadre1.cadre1.codeApe2;

//Adresse postale du détenteur 

formFields['complementAdresseDeclarant']     = $ds006PE7.adresse.adresse.complementAdresseDeclarant;
formFields['adresseDeclarantNumNomRue']      = $ds006PE7.adresse.adresse.adresseDeclarantNumNomRue;
formFields['codePostalAdresseDeclarant']     = $ds006PE7.adresse.adresse.codePostalAdresseDeclarant;
formFields['communeAdresseDeclarant']        = $ds006PE7.adresse.adresse.communeAdresseDeclarant;


//Coordonnées du lieu de stationnement 

formFields['denominationSocialeLieuEquides'] = $ds006PE7.lieuEquides.lieuEquides.denominationSocialeLieuEquides;
formFields['adresseLieuEquides']             = $ds006PE7.lieuEquides.lieuEquides.adresseLieuEquides;
formFields['complementAdresseLieuEquides']   = $ds006PE7.lieuEquides.lieuEquides.complementAdresseLieuEquides;
formFields['codePostalAdresseLieuEquides']   = $ds006PE7.lieuEquides.lieuEquides.codePostalAdresseLieuEquides;
formFields['communeAdresseLieuEquides']      = $ds006PE7.lieuEquides.lieuEquides.communeAdresseLieuEquides;


//Contact sur place 

formFields['mlleContact']                    = Value('id').of($ds006PE7.contactSurPlace.contactSurPlace.civilite3).eq('mlleContact') ? true : false;
formFields['monsieurContact']                = Value('id').of($ds006PE7.contactSurPlace.contactSurPlace.civilite3).eq('monsieurContact') ? true : false;
formFields['madameContact']                  = Value('id').of($ds006PE7.contactSurPlace.contactSurPlace.civilite3).eq('madameContact') ? true : false;
formFields['prenomContact']                  = $ds006PE7.contactSurPlace.contactSurPlace.prenomContact;
formFields['telephoneMobileContact']         = $ds006PE7.contactSurPlace.contactSurPlace.telephoneMobileContact;
formFields['courrielContact']                = $ds006PE7.contactSurPlace.contactSurPlace.courrielContact;
formFields['telephoneFixeContact']           = $ds006PE7.contactSurPlace.contactSurPlace.telephoneFixeContact;
formFields['faxContact']                     = $ds006PE7.contactSurPlace.contactSurPlace.faxContact;
formFields['commentaireContact']             = $ds006PE7.contactSurPlace.contactSurPlace.commentaireContact;
formFields['nomContact']                     = $ds006PE7.contactSurPlace.contactSurPlace.nomContact;


//Informations complémentaires 

formFields['nombreEquidesPresent']           = $ds006PE7.informationsComplementaires.informationsComplementaires.nombreEquidesPresent;
formFields['surfaceUtiliseeEquides']         = $ds006PE7.informationsComplementaires.informationsComplementaires.surfaceUtiliseeEquides;
formFields['typeElevage']                    = $ds006PE7.informationsComplementaires.typeActivite.typeElevage;
formFields['typeEnseignement']               = $ds006PE7.informationsComplementaires.typeActivite.typeEnseignement;
formFields['typeEntrainement']               = $ds006PE7.informationsComplementaires.typeActivite.typeEntrainement;
formFields['typePension']                    = $ds006PE7.informationsComplementaires.typeActivite.typePension;
formFields['typeAucune']                     = $ds006PE7.informationsComplementaires.typeActivite.typeAucune;
formFields['typeAutre']                      = $ds006PE7.informationsComplementaires.typeActivite.typeAutre;

//Signature 

formFields['lieuSiganture']                  = $ds006PE7.signature.signature.lieuSignature;
formFields['dateSignature']                  = $ds006PE7.signature.signature.dateSignature;





/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
 
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp085PE3.signature.signature.lieuSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		dateSignature: $ds006PE7.signature.signature.dateSignature,
		autoriteHabilitee :"Institut Français du Cheval et de l’Equitation (IFCE)",
		demandeContexte : "Titulaire d'un diplôme, titre de formation ou certificat ",
		civiliteNomPrenom : civNomPrenom,
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	
    .load('models/DECLARATION D’UN LIEU DE STATIONNEMENT D’EQUIDES.pdf') //
    .apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
 
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);



/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Centre_equestre_assimiles.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
   label : 'Centre équestre et assimilés - Déclaration du lieu de détention d\'équidés permettant de leur attribuer ainsi d\'être identifié par un numéro national unique.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration du lieu de détention d\'équidés permettant de leur attribuer ainsi d\'être identifié par un numéro national unique',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});