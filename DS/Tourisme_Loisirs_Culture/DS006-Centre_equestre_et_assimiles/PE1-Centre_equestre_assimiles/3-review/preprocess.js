var formFields = {};


//Identification du demandeur


formFields['nSiret']                        					 = $ds006PE1.identificationDemandeur.identificationExploitant.nSiret;
formFields['nPacage']                        					 = $ds006PE1.identificationDemandeur.identificationExploitant.nPacage;

formFields['nomPrenomsRaisonSociale']                           = $ds006PE1.identificationDemandeur.identificationExploitant.nomPrenomsRaisonSociale;

formFields['adressePostale']                                    = $ds006PE1.identificationDemandeur.coordonneesDemandeur.adressePostale;
formFields['paysPersonne']                              	    = $ds006PE1.identificationDemandeur.coordonneesDemandeur.paysPersonne;
formFields['codePostal']                              		    = $ds006PE1.identificationDemandeur.coordonneesDemandeur.codePostal;
formFields['commune']                              			    = $ds006PE1.identificationDemandeur.coordonneesDemandeur.commune;
formFields['telephoneFixe']                              	    = $ds006PE1.identificationDemandeur.coordonneesDemandeur.telephoneFixe;
formFields['telephoneMobile']                                   = $ds006PE1.identificationDemandeur.coordonneesDemandeur.telephoneMobile;
formFields['courriel']                                          = $ds006PE1.identificationDemandeur.coordonneesDemandeur.courriel;


//Identification des membres de l'exploitation individelle (membre 1)

formFields['nomPrenomsMembre1']                                 = $ds006PE1.membre1.declarationMembre1.nomPrenomsMembre1;
formFields['dateNaissanceMembre1']                              = $ds006PE1.membre1.declarationMembre1.dateNaissanceMembre1;
formFields['nationaliteMembre1']                                = $ds006PE1.membre1.declarationMembre1.nationaliteMembre1;


formFields['nomPrenomsConjointMembre1']                         = $ds006PE1.membre1.conjointAssocieMembre1.nomPrenomsConjointMembre1;
formFields['dateNaissanceConjointMembre1']                      = $ds006PE1.membre1.conjointAssocieMembre1.dateNaissanceConjointMembre1;
formFields['conjointOuiCollaborateurMembre1']                   = Value('id').of($ds006PE1.membre1.conjointAssocieMembre1.conjointCollaborateurMembre1).eq("conjointOuiCollaborateurMembre1") ? true : false;
formFields['conjointNonCollaborateurMembre1']                  	= Value('id').of($ds006PE1.membre1.conjointAssocieMembre1.conjointCollaborateurMembre1).eq("conjointNonCollaborateurMembre1") ? true : false;
formFields['professionConjointMembre1']                         = $ds006PE1.membre1.conjointAssocieMembre1.professionConjointMembre1;
formFields['exploitationSepareeOuiConjoint1']                   = Value('id').of($ds006PE1.membre1.conjointAssocieMembre1.exploitationConjointMembre1).eq("exploitationSepareeOuiConjoint1") ? true : false;
formFields['exploitationSepareeNonConjoint1']                   = Value('id').of($ds006PE1.membre1.conjointAssocieMembre1.exploitationConjointMembre1).eq("exploitationSepareeNonConjoint1") ? true : false;
formFields['superficieExploitationMembre1']                     = $ds006PE1.membre1.conjointAssocieMembre1.superficieExploitationMembre1;


formFields['nombreEnfantsMembre1']                          	= $ds006PE1.membre1.enfantAssocieMembre1.nombreEnfantsMembre1;
formFields['projetAgriculteurEnfantMembre1']                    = Value('id').of($ds006PE1.membre1.enfantAssocieMembre1.projetEnfantAgriculteurMembre1).eq("projetAgriculteurEnfantMembre1") ? true : false;
formFields['ageEnfantProjetAgriculteurMembre1']                 = $ds006PE1.membre1.enfantAssocieMembre1.ageEnfantProjetAgriculteurMembre1;
formFields['projetNonAgriculteurEnfantMembre1']                 = Value('id').of($ds006PE1.membre1.enfantAssocieMembre1.projetEnfantAgriculteurMembre1).eq("projetNonAgriculteurEnfantMembre1") ? true : false;
formFields['anneeInstallationEnfantMembre1']                    = $ds006PE1.membre1.enfantAssocieMembre1.anneeInstallationEnfantMembre1;
formFields['enfantExploitantMembre1']                           = Value('id').of($ds006PE1.membre1.enfantAssocieMembre1.enfantExploitant1).eq("enfantExploitantMembre1") ? true : false;
formFields['enfantNonExploitantMembre1']                        = Value('id').of($ds006PE1.membre1.enfantAssocieMembre1.enfantExploitant1).eq("enfantNonExploitantMembre1") ? true : false;
formFields['enfantAideExploitationMembre1']                    	= Value('id').of($ds006PE1.membre1.enfantAssocieMembre1.enfantAideExploitation1).eq("enfantAideExploitationMembre1") ? true : false;
formFields['enfantAideNonExploitationMembre1']                  = Value('id').of($ds006PE1.membre1.enfantAssocieMembre1.enfantAideExploitation1).eq("enfantAideNonExploitationMembre1") ? true : false;
formFields['activiteRemunereeMembre1']                     		= $ds006PE1.membre1.capaciteProfessionnelle1.activiteRemunereeMembre1;



formFields['capaciteAgricoleMembre1']                           = Value('id').of($ds006PE1.membre1.capaciteProfessionnelle1.capaciteAgricole1).eq("capaciteAgricoleMembre1") ? true : false;
formFields['capaciteNonAgricoleMembre1']                        = Value('id').of($ds006PE1.membre1.capaciteProfessionnelle1.capaciteAgricole1).eq("capaciteNonAgricoleMembre1") ? true : false;
formFields['autreActiviteRemunereeMembre1']                     = Value('id').of($ds006PE1.membre1.capaciteProfessionnelle1.statutActiviteRemuneree1).eq("autreActiviteRemunereeMembre1") ? true : false;
formFields['pasAutreActiviteRemunereeMembre1']                  = Value('id').of($ds006PE1.membre1.capaciteProfessionnelle1.statutActiviteRemuneree1).eq("pasAutreActiviteRemunereeMembre1") ? true : false;
formFields['gerantSocieteMembre1']                              = Value('id').of($ds006PE1.membre1.capaciteProfessionnelle1.gerantSociete1).eq("gerantSocieteMembre1") ? true : false;
formFields['pasGerantSocieteMembre1']                           = Value('id').of($ds006PE1.membre1.capaciteProfessionnelle1.gerantSociete1).eq("pasGerantSocieteMembre1") ? true : false;
formFields['associeExploitantMembre1']                          = Value('id').of($ds006PE1.membre1.capaciteProfessionnelle1.associeExploitant1).eq("associeExploitantMembre1") ? true : false;
formFields['pasAssocieExploitantMembre1']                       = Value('id').of($ds006PE1.membre1.capaciteProfessionnelle1.associeExploitant1).eq("pasAssocieExploitantMembre1") ? true : false;
formFields['associeAutreSocieteMembre1']                        = Value('id').of($ds006PE1.membre1.capaciteProfessionnelle1.associeAutresSocietes1).eq("associeAutreSocieteMembre1") ? true : false;
formFields['pasAssocieAutreSocieteMembre1']                     = Value('id').of($ds006PE1.membre1.capaciteProfessionnelle1.associeAutresSocietes1).eq("pasAssocieAutreSocieteMembre1") ? true : false;
formFields['exploitantIndividuelMembre1']                       = Value('id').of($ds006PE1.membre1.capaciteProfessionnelle1.exploitantIndividuel1).eq("exploitantIndividuelMembre1") ? true : false;
formFields['nonExploitantIndividuelMembre1']                    = Value('id').of($ds006PE1.membre1.capaciteProfessionnelle1.exploitantIndividuel1).eq("nonExploitantIndividuelMembre1") ? true : false;



//Identification des membres de l'exploitation individelle (membre 2)

formFields['nomPrenomsMembre2']                                 = $ds006PE1.membre2.declarationMembre2.nomPrenomsMembre2;
formFields['dateNaissanceMembre2']                              = $ds006PE1.membre2.declarationMembre2.dateNaissanceMembre2;
formFields['nationaliteMembre2']                                = $ds006PE1.membre2.declarationMembre2.nomPrenomsMembre2;


formFields['nomPrenomsConjointMembre2']                         = $ds006PE1.membre2.conjointAssocieMembre2.nomPrenomsConjointMembre2;
formFields['dateNaissanceConjointMembre2']                      = $ds006PE1.membre2.conjointAssocieMembre2.dateNaissanceConjointMembre2;
formFields['conjointOuiCollaborateurMembre2']                   = Value('id').of($ds006PE1.membre2.conjointAssocieMembre2.conjointCollaborateurMembre2).eq("conjointOuiCollaborateurMembre2") ? true : false;
formFields['conjointNonCollaborateurMembre2']                  	= Value('id').of($ds006PE1.membre2.conjointAssocieMembre2.conjointCollaborateurMembre2).eq("conjointNonCollaborateurMembre2") ? true : false;
formFields['professionConjointMembre2']                         = $ds006PE1.membre2.conjointAssocieMembre2.professionConjointMembre2;
formFields['exploitationSepareeOuiConjoint2']                  	= Value('id').of($ds006PE1.membre2.conjointAssocieMembre2.exploitationConjointMembre2).eq("exploitationSepareeOuiConjoint2") ? true : false;
formFields['exploitationSepareeNonConjoint2']                   = Value('id').of($ds006PE1.membre2.conjointAssocieMembre2.exploitationConjointMembre2).eq("exploitationSepareeNonConjoint2") ? true : false;
formFields['superficieExploitationMembre2']                     = $ds006PE1.membre2.conjointAssocieMembre2.superficieExploitationMembre2;




formFields['nombreEnfantsMembre2']                          	= $ds006PE1.membre2.enfantAssocieMembre2.nombreEnfantsMembre2;
formFields['projetAgriculteurEnfantMembre2']                    = Value('id').of($ds006PE1.membre2.enfantAssocieMembre2.projetEnfantAgriculteurMembre2).eq("projetAgriculteurEnfantMembre2") ? true : false;
formFields['projetNonAgriculteurEnfantMembre2']                 = Value('id').of($ds006PE1.membre2.enfantAssocieMembre2.projetEnfantAgriculteurMembre2).eq("projetNonAgriculteurEnfantMembre2") ? true : false;
formFields['ageEnfantProjetAgriculteurMembre2']                 = $ds006PE1.membre2.enfantAssocieMembre2.ageEnfantProjetAgriculteurMembre2;
formFields['anneeInstallationEnfantMembre2']                    = $ds006PE1.membre2.enfantAssocieMembre2.anneeInstallationEnfantMembre2;
formFields['enfantExploitantMembre2']                           = Value('id').of($ds006PE1.membre2.enfantAssocieMembre2.enfantExploitant2).eq("enfantExploitantMembre2") ? true : false;
formFields['enfantNonExploitantMembre2']                        = Value('id').of($ds006PE1.membre2.enfantAssocieMembre2.enfantExploitant2).eq("enfantNonExploitantMembre2") ? true : false;
formFields['enfantAideExploitationMembre2']                      = Value('id').of($ds006PE1.membre2.enfantAssocieMembre2.enfantAideExploitation2).eq("enfantAideExploitationMembre2") ? true : false;
formFields['enfantAideNonExploitationMembre2']                  = Value('id').of($ds006PE1.membre2.enfantAssocieMembre2.enfantAideExploitation2).eq("enfantAideNonExploitationMembre2") ? true : false;
formFields['activiteRemunereeMembre2']                     		= $ds006PE1.membre2.capaciteProfessionnelle2.activiteRemunereeMembre2;



formFields['capaciteAgricoleMembre2']                           = Value('id').of($ds006PE1.membre2.capaciteProfessionnelle2.capaciteAgricole2).eq("capaciteAgricoleMembre2") ? true : false;
formFields['capaciteNonAgricoleMembre2']                        = Value('id').of($ds006PE1.membre2.capaciteProfessionnelle2.capaciteAgricole2).eq("capaciteNonAgricoleMembre2") ? true : false;
formFields['autreActiviteRemunereeMembre2']                     = Value('id').of($ds006PE1.membre2.capaciteProfessionnelle2.statutActiviteRemuneree2).eq("autreActiviteRemunereeMembre2") ? true : false;
formFields['pasAutreActiviteRemunereeMembre2']                  = Value('id').of($ds006PE1.membre2.capaciteProfessionnelle2.statutActiviteRemuneree2).eq("pasAutreActiviteRemunereeMembre2") ? true : false;
formFields['gerantSocieteMembre2']                              = Value('id').of($ds006PE1.membre2.capaciteProfessionnelle2.gerantSociete2).eq("gerantSocieteMembre2") ? true : false;
formFields['pasGerantSocieteMembre2']                           = Value('id').of($ds006PE1.membre2.capaciteProfessionnelle2.gerantSociete2).eq("pasGerantSocieteMembre2") ? true : false;
formFields['associeExploitantMembre2']                          = Value('id').of($ds006PE1.membre2.capaciteProfessionnelle2.associeExploitant2).eq("associeExploitantMembre2") ? true : false;
formFields['pasAssocieExploitantMembre2']                       = Value('id').of($ds006PE1.membre2.capaciteProfessionnelle2.associeExploitant2).eq("pasAssocieExploitantMembre2") ? true : false;
formFields['associeAutreSocieteMembre2']                        = Value('id').of($ds006PE1.membre2.capaciteProfessionnelle2.associeAutresSocietes2).eq("associeAutreSocieteMembre2") ? true : false;
formFields['pasAssocieAutreSocieteMembre2']                    	= Value('id').of($ds006PE1.membre2.capaciteProfessionnelle2.associeAutresSocietes2).eq("pasAssocieAutreSocieteMembre2") ? true : false;
formFields['exploitantIndividuelMembre2']                       = Value('id').of($ds006PE1.membre2.capaciteProfessionnelle2.exploitantIndividuel2).eq("exploitantIndividuelMembre2") ? true : false;
formFields['nonExploitantIndividuelMembre2']                    = Value('id').of($ds006PE1.membre2.capaciteProfessionnelle2.exploitantIndividuel2).eq("nonExploitantIndividuelMembre2") ? true : false;




//Identification des membres de l'exploitation individelle (membre 3)

formFields['nomPrenomsMembre3']                                 = $ds006PE1.membre3.declarationMembre3.nomPrenomsMembre3;
formFields['dateNaissanceMembre3']                              = $ds006PE1.membre3.declarationMembre3.dateNaissanceMembre3;
formFields['nationaliteMembre3']                                = $ds006PE1.membre3.declarationMembre3.nationaliteMembre3;

formFields['nomPrenomsConjointMembre3']                         = $ds006PE1.membre3.conjointAssocieMembre3.nomPrenomsConjointMembre3;
formFields['dateNaissanceConjointMembre3']                      = $ds006PE1.membre3.conjointAssocieMembre3.dateNaissanceConjointMembre3;
formFields['conjointOuiCollaborateurMembre3']                   = Value('id').of($ds006PE1.membre3.conjointAssocieMembre3.conjointCollaborateurMembre3).eq("conjointOuiCollaborateurMembre3") ? true : false;
formFields['conjointNonCollaborateurMembre3']                  	= Value('id').of($ds006PE1.membre3.conjointAssocieMembre3.conjointCollaborateurMembre3).eq("conjointNonCollaborateurMembre3") ? true : false;
formFields['professionConjointMembre3']                         = $ds006PE1.membre3.conjointAssocieMembre3.professionConjointMembre3;
formFields['exploitationSepareeOuiConjoint3']                   = Value('id').of($ds006PE1.membre3.conjointAssocieMembre3.exploitationConjointMembre3).eq("exploitationSepareeOuiConjoint3") ? true : false;
formFields['exploitationSepareeNonConjoint3']                   = Value('id').of($ds006PE1.membre3.conjointAssocieMembre3.exploitationConjointMembre3).eq("exploitationSepareeNonConjoint3") ? true : false;
formFields['superficieExploitationMembre3']                     = $ds006PE1.membre3.conjointAssocieMembre3.superficieExploitationMembre3;




formFields['nombreEnfantsMembre3']                          	= $ds006PE1.membre3.enfantAssocieMembre3.nombreEnfantsMembre3;
formFields['projetAgriculteurEnfantMembre3']                    = Value('id').of($ds006PE1.membre3.enfantAssocieMembre3.projetEnfantAgriculteurMembre3).eq("projetAgriculteurEnfantMembre3") ? true : false;
formFields['projetNonAgriculteurEnfantMembre3']                 = Value('id').of($ds006PE1.membre3.enfantAssocieMembre3.projetEnfantAgriculteurMembre3).eq("projetNonAgriculteurEnfantMembre3") ? true : false;
formFields['ageEnfantProjetAgriculteurMembre3']                 = $ds006PE1.membre3.enfantAssocieMembre3.ageEnfantProjetAgriculteurMembre3;
formFields['anneeInstallationEnfantMembre3']                    = $ds006PE1.membre3.enfantAssocieMembre3.anneeInstallationEnfantMembre3;
formFields['enfantExploitantMembre3']                           = Value('id').of($ds006PE1.membre3.enfantAssocieMembre3.enfantExploitant3).eq("enfantExploitantMembre3") ? true : false;
formFields['enfantNonExploitantMembre3']                        = Value('id').of($ds006PE1.membre3.enfantAssocieMembre3.enfantExploitant3).eq("enfantNonExploitantMembre3") ? true : false;
formFields['enfantAideExploitationMembre3']                      = Value('id').of($ds006PE1.membre3.enfantAssocieMembre3.enfantAideExploitation3).eq("enfantAideExploitationMembre3") ? true : false;
formFields['enfantAideNonExploitationMembre3']                  = Value('id').of($ds006PE1.membre3.enfantAssocieMembre3.enfantAideExploitation3).eq("enfantAideNonExploitationMembre3") ? true : false;
formFields['activiteRemunereeMembre3']                     		= $ds006PE1.membre3.capaciteProfessionnelle3.activiteRemunereeMembre3;




formFields['capaciteAgricoleMembre3']                           = Value('id').of($ds006PE1.membre3.capaciteProfessionnelle3.capaciteAgricole3).eq("capaciteAgricoleMembre3") ? true : false;
formFields['capaciteNonAgricoleMembre3']                        = Value('id').of($ds006PE1.membre3.capaciteProfessionnelle3.capaciteAgricole3).eq("capaciteNonAgricoleMembre3") ? true : false;
formFields['autreActiviteRemunereeMembre3']                     = Value('id').of($ds006PE1.membre3.capaciteProfessionnelle3.statutActiviteRemuneree3).eq("autreActiviteRemunereeMembre3") ? true : false;
formFields['pasAutreActiviteRemunereeMembre3']                  = Value('id').of($ds006PE1.membre3.capaciteProfessionnelle3.statutActiviteRemuneree3).eq("pasAutreActiviteRemunereeMembre3") ? true : false;
formFields['gerantSocieteMembre3']                              = Value('id').of($ds006PE1.membre3.capaciteProfessionnelle3.gerantSociete3).eq("gerantSocieteMembre3") ? true : false;
formFields['pasGerantSocieteMembre3']                           = Value('id').of($ds006PE1.membre3.capaciteProfessionnelle3.gerantSociete3).eq("pasGerantSocieteMembre3") ? true : false;
formFields['associeExploitantMembre3']                          = Value('id').of($ds006PE1.membre3.capaciteProfessionnelle3.associeExploitant3).eq("associeExploitantMembre3") ? true : false;
formFields['pasAssocieExploitantMembre3']                       = Value('id').of($ds006PE1.membre3.capaciteProfessionnelle3.associeExploitant3).eq("pasAssocieExploitantMembre3") ? true : false;
formFields['associeAutreSocieteMembre3']                        = Value('id').of($ds006PE1.membre3.capaciteProfessionnelle3.associeAutresSocietes3).eq("associeAutreSocieteMembre3") ? true : false;
formFields['pasAssocieAutreSocieteMembre3']                     = Value('id').of($ds006PE1.membre3.capaciteProfessionnelle3.associeAutresSocietes3).eq("pasAssocieAutreSocieteMembre3") ? true : false;
formFields['exploitantIndividuelMembre3']                       = Value('id').of($ds006PE1.membre3.capaciteProfessionnelle3.exploitantIndividuel3).eq("exploitantIndividuelMembre3") ? true : false;
formFields['nonExploitantIndividuelMembre3']                    = Value('id').of($ds006PE1.membre3.capaciteProfessionnelle3.exploitantIndividuel3).eq("nonExploitantIndividuelMembre3") ? true : false;


//Identification des membres de l'exploitation individelle (membre 4)

formFields['nomPrenomsMembre4']                                 = $ds006PE1.membre4.declarationMembre4.nomPrenomsMembre4;
formFields['dateNaissanceMembre4']                              = $ds006PE1.membre4.declarationMembre4.dateNaissanceMembre4;
formFields['nationaliteMembre4']                                = $ds006PE1.membre4.declarationMembre4.nationaliteMembre4;

formFields['nomPrenomsConjointMembre4']                         = $ds006PE1.membre4.conjointAssocieMembre4.nomPrenomsConjointMembre4;
formFields['dateNaissanceConjointMembre4']                      = $ds006PE1.membre4.conjointAssocieMembre4.dateNaissanceConjointMembre4;
formFields['conjointOuiCollaborateurMembre4']                   = Value('id').of($ds006PE1.membre4.conjointAssocieMembre4.conjointCollaborateurMembre4).eq("conjointOuiCollaborateurMembre4") ? true : false;
formFields['conjointNonCollaborateurMembre4']                  	= Value('id').of($ds006PE1.membre4.conjointAssocieMembre4.conjointCollaborateurMembre4).eq("conjointNonCollaborateurMembre4") ? true : false;
formFields['professionConjointMembre4']                         = $ds006PE1.membre4.conjointAssocieMembre4.professionConjointMembre4;
formFields['exploitationSepareeOuiConjoint4']                   = Value('id').of($ds006PE1.membre4.conjointAssocieMembre4.exploitationConjointMembre4).eq("exploitationSepareeOuiConjoint4") ? true : false;
formFields['exploitationSepareeNonConjoint4']                   = Value('id').of($ds006PE1.membre4.conjointAssocieMembre4.exploitationConjointMembre4).eq("exploitationSepareeNonConjoint4") ? true : false;
formFields['superficieExploitationMembre4']                     = $ds006PE1.membre4.conjointAssocieMembre4.superficieExploitationMembre4;



formFields['nombreEnfantsMembre4']                          	= $ds006PE1.membre4.enfantAssocieMembre4.nombreEnfantsMembre4;
formFields['projetAgriculteurEnfantMembre4']                    = Value('id').of($ds006PE1.membre4.enfantAssocieMembre4.projetEnfantAgriculteurMembre3).eq("projetAgriculteurEnfantMembre4") ? true : false;
formFields['projetNonAgriculteurEnfantMembre4']                 = Value('id').of($ds006PE1.membre4.enfantAssocieMembre4.projetEnfantAgriculteurMembre4).eq("projetNonAgriculteurEnfantMembre4") ? true : false;
formFields['ageEnfantProjetAgriculteurMembre4']                 = $ds006PE1.membre4.enfantAssocieMembre4.ageEnfantProjetAgriculteurMembre4;
formFields['anneeInstallationEnfantMembre4']                    = $ds006PE1.membre4.enfantAssocieMembre4.anneeInstallationEnfantMembre4;
formFields['enfantExploitantMembre4']                           = Value('id').of($ds006PE1.membre4.enfantAssocieMembre4.enfantExploitant4).eq("enfantExploitantMembre4") ? true : false;
formFields['enfantNonExploitantMembre4']                        = Value('id').of($ds006PE1.membre4.enfantAssocieMembre4.enfantExploitant4).eq("enfantNonExploitantMembre4") ? true : false;
formFields['enfantAideExploitationMembre4']                     = Value('id').of($ds006PE1.membre4.enfantAssocieMembre4.enfantAideExploitation4).eq("enfantAideExploitationMembre4") ? true : false;
formFields['enfantAideNonExploitationMembre4']                  = Value('id').of($ds006PE1.membre4.enfantAssocieMembre4.enfantAideExploitation4).eq("enfantAideNonExploitationMembre4") ? true : false;
formFields['activiteRemunereeMembre4']                     		= $ds006PE1.membre4.capaciteProfessionnelle4.activiteRemunereeMembre4;



formFields['capaciteAgricoleMembre4']                           = Value('id').of($ds006PE1.membre4.capaciteProfessionnelle4.capaciteAgricole4).eq("capaciteAgricoleMembre4") ? true : false;
formFields['capaciteNonAgricoleMembre4']                        = Value('id').of($ds006PE1.membre4.capaciteProfessionnelle4.capaciteAgricole4).eq("capaciteNonAgricoleMembre4") ? true : false;
formFields['autreActiviteRemunereeMembre4']                     = Value('id').of($ds006PE1.membre4.capaciteProfessionnelle4.statutActiviteRemuneree4).eq("autreActiviteRemunereeMembre4") ? true : false;
formFields['pasAutreActiviteRemunereeMembre4']                  = Value('id').of($ds006PE1.membre4.capaciteProfessionnelle4.statutActiviteRemuneree4).eq("pasAutreActiviteRemunereeMembre4") ? true : false;
formFields['gerantSocieteMembre4']                              = Value('id').of($ds006PE1.membre4.capaciteProfessionnelle4.gerantSociete4).eq("gerantSocieteMembre4") ? true : false;
formFields['pasGerantSocieteMembre4']                           = Value('id').of($ds006PE1.membre4.capaciteProfessionnelle4.gerantSociete4).eq("pasGerantSocieteMembre4") ? true : false;
formFields['associeExploitantMembre4']                          = Value('id').of($ds006PE1.membre4.capaciteProfessionnelle4.associeExploitant4).eq("associeExploitantMembre4") ? true : false;
formFields['pasAssocieExploitantMembre4']                       = Value('id').of($ds006PE1.membre4.capaciteProfessionnelle4.associeExploitant4).eq("pasAssocieExploitantMembre4") ? true : false;
formFields['associeAutreSocieteMembre4']                        = Value('id').of($ds006PE1.membre4.capaciteProfessionnelle4.associeAutresSocietes4).eq("associeAutreSocieteMembre4") ? true : false;
formFields['pasAssocieAutreSocieteMembre4']                     = Value('id').of($ds006PE1.membre4.capaciteProfessionnelle4.associeAutresSocietes4).eq("pasAssocieAutreSocieteMembre4") ? true : false;
formFields['exploitantIndividuelMembre4']                       = Value('id').of($ds006PE1.membre4.capaciteProfessionnelle4.exploitantIndividuel4).eq("exploitantIndividuelMembre4") ? true : false;
formFields['nonExploitantIndividuelMembre4']                    = Value('id').of($ds006PE1.membre4.capaciteProfessionnelle4.exploitantIndividuel4).eq("nonExploitantIndividuelMembre4") ? true : false;



//Circonstances de la demande

formFields['demandeSpontanee']                          		= $ds006PE1.objectifsDemande.circonstancesDemande.demandeSpontanee;
formFields['demandePublicite']                             		= $ds006PE1.objectifsDemande.circonstancesDemande.demandePublicite;
formFields['numeroDemandeDossier']                              = $ds006PE1.objectifsDemande.circonstancesDemande.numeroDemandeDossier;
formFields['dateEnregistrementDossier']                         = $ds006PE1.objectifsDemande.circonstancesDemande.dateEnregistrementDossier;
formFields['demandeConcurence']                             	= $ds006PE1.objectifsDemande.circonstancesDemande.demandeConcurence;
formFields['demandeConcurrenceParBox']                          = Value('id').of($ds006PE1.objectifsDemande.circonstancesDemande.motifDemande).eq("demandeConcurrenceParBox") ? true : false;
formFields['demandeConcurrencesSurfacesLibereesBox']            = Value('id').of($ds006PE1.objectifsDemande.circonstancesDemande.motifDemande).eq("demandeConcurrencesSurfacesLibereesBox") ? true : false;
formFields['demandeConcurrencePar']                             = $ds006PE1.objectifsDemande.circonstancesDemande.demandeConcurrencePar;
formFields['demandeConcurrencesSurfacesLiberees']               = $ds006PE1.objectifsDemande.circonstancesDemande.demandeConcurrencesSurfacesLiberees;



//Nature de l'opération


formFields['titreIndividuel']                                    = Value('id').of($ds006PE1.natureOperation.typeOperations.operationInstallation).eq("titreIndividuel") ? true : false;
formFields['premiereInstallation']                               = Value('id').of($ds006PE1.natureOperation.typeOperations.operationInstallation).eq("premiereInstallation") ? true : false;
formFields['constitutionSociete']                                = Value('id').of($ds006PE1.natureOperation.typeOperations.operationInstallation).eq("constitutionSociete") ? true : false;
formFields['agrandissementSociete']                              = Value('id').of($ds006PE1.natureOperation.typeOperations.operationAgrandissement).eq("agrandissementSociete") ? true : false;
formFields['deuxExploitations']                                  = Value('id').of($ds006PE1.natureOperation.typeOperations.operationAgrandissement).eq("deuxExploitations") ? true : false;
formFields['participationAutreExploitation']                     = Value('id').of($ds006PE1.natureOperation.typeOperations.operationAgrandissement).eq("participationAutreExploitation") ? true : false;
formFields['miseDisposition']                                    = Value('id').of($ds006PE1.natureOperation.typeOperations.operationAgrandissement).eq("miseDisposition") ? true : false;
formFields['creationAtelierHorsSol']                             = Value('id').of($ds006PE1.natureOperation.typeOperations.creation).eq("creationAtelierHorsSol") ? true : false;
formFields['repriseAtelierHorsSol']                              = Value('id').of($ds006PE1.natureOperation.typeOperations.creation).eq("repriseAtelierHorsSol") ? true : false;
formFields['extensionAtelierHorsSol']                            = Value('id').of($ds006PE1.natureOperation.typeOperations.creation).eq("extensionAtelierHorsSol") ? true : false;



//Ajout des Annexes 

//Annexe 1
formFields['biensAgricolesTerre']                                = Value('id').of($ds006PE1.annexe1.biensAgricoles.biensAgricoles).eq("biensAgricolesTerre") ? true : false;
formFields['biensAgricolesHorsSols']                             = Value('id').of($ds006PE1.annexe1.biensAgricoles.biensAgricoles).eq("biensAgricolesHorsSols") ? true : false;
formFields['operationAchat']                                     = Value('id').of($ds006PE1.annexe1.natureOperation.natureOperationTransfert).eq("operationAchat") ? true : false;
formFields['operationBail']                                      = Value('id').of($ds006PE1.annexe1.natureOperation.natureOperationTransfert).eq("operationBail") ? true : false;
formFields['operationDonation']                                  = Value('id').of($ds006PE1.annexe1.natureOperation.natureOperationTransfert).eq("operationDonation") ? true : false;
formFields['operationReprise']                              	 = Value('id').of($ds006PE1.annexe1.natureOperation.natureOperationTransfert).eq("operationReprise") ? true : false;
formFields['autreOperation']                              	 	 = Value('id').of($ds006PE1.annexe1.natureOperation.natureOperationTransfert).eq("autreOperation") ? true : false;


formFields['dateTransfert']                                      = $ds006PE1.annexe1.natureOperation.dateTransfert;
formFields['superficieHa']                                	 	 = $ds006PE1.annexe1.terres.superficieHa;
formFields['superficieA']                                	 	 = $ds006PE1.annexe1.terres.superficieA;
formFields['superficieCa']                                	 	 = $ds006PE1.annexe1.terres.superficieCa;



formFields['transfertBatimentsOui']                              = Value('id').of($ds006PE1.annexe1.batimentsNonHorsSols.transfertBatiments).eq("transfertBatimentsOui") ? true : false;
formFields['transfertBatimentsNon']                              = Value('id').of($ds006PE1.annexe1.batimentsNonHorsSols.transfertBatiments).eq("transfertBatimentsNon") ? true : false;


formFields['batimentsHabitation']                              	 = Value('id').of($ds006PE1.annexe1.batimentsNonHorsSols.typeBatimentsHorsSolsProd).eq("batimentsHabitation") ? true : false;
formFields['batimentsExploitation']                              = Value('id').of($ds006PE1.annexe1.batimentsNonHorsSols.typeBatimentshorsHorsProd).eq("batimentsExploitation") ? true : false;



formFields['nouveauBatimentsExploitation']                       = Value('id').of($ds006PE1.annexe1.batimentsHorsSols.typesBatimentsProd).eq("nouveauBatimentsExploitation") ? true : false;
formFields['anciensBatimentsExploitation']                       = Value('id').of($ds006PE1.annexe1.batimentsHorsSols.typesBatimentsProd).eq("anciensBatimentsExploitation") ? true : false;
formFields['typesBatiments']                       				 = $ds006PE1.annexe1.batimentsHorsSols.typeBatiments;
formFields['dateDepot']                       					 = $ds006PE1.annexe1.batimentsHorsSols.dateDepot;
formFields['enqueteProcedureOui']                       		 = Value('id').of($ds006PE1.annexe1.batimentsHorsSols.objetProcedure).eq("enqueteProcedureOui") ? true : false;
formFields['enqueteProcedureNon']                       		 = Value('id').of($ds006PE1.annexe1.batimentsHorsSols.objetProcedure).eq("enqueteProcedureNon") ? true : false;
formFields['dateClotureEnquete']                       			 = $ds006PE1.annexe1.batimentsHorsSols.dateClotureEnquete;
formFields['dateDepotPermisConstruction']                        = $ds006PE1.annexe1.batimentsHorsSols.dateDepotPermisConstruction;



formFields['nSiret1']                        					 = $ds006PE1.annexe1bis.identificationExploitantAnterieur.nSiret1;
formFields['nPacage1']                        					 = $ds006PE1.annexe1bis.identificationExploitantAnterieur.nPacage1;

formFields['nomPrenomsRaisonSociale1']                        	 = $ds006PE1.annexe1bis.adresseExploitantAnterieur.nomPrenomsRaisonSociale1;

formFields['adressePostale1']                         			 = ($ds006PE1.annexe1bis.adresseExploitantAnterieur.adresse1 !=null ? $ds006PE1.annexe1bis.adresseExploitantAnterieur.adresse1 + ", " :'') + ($ds006PE1.annexe1bis.adresseExploitantAnterieur.complementAdresse1 !=null ? $ds006PE1.annexe1bis.adresseExploitantAnterieur.complementAdresse1 + ", " :'') + ($ds006PE1.annexe1bis.adresseExploitantAnterieur.codePostal1 !=null ? $ds006PE1.annexe1bis.adresseExploitantAnterieur.codePostal1 + ", " :'') + ($ds006PE1.annexe1bis.adresseExploitantAnterieur.commune1 !=null ? $ds006PE1.annexe1bis.adresseExploitantAnterieur.commune1 + ", " :'') + ($ds006PE1.annexe1bis.adresseExploitantAnterieur.pays1 !=null ? $ds006PE1.annexe1bis.adresseExploitantAnterieur.pays1 + ", " :'');


formFields['codePostal1']                        				= $ds006PE1.annexe1bis.adresseExploitantAnterieur.codePostal1;
formFields['complementAdresse1']                        	    = $ds006PE1.annexe1bis.adresseExploitantAnterieur.complementAdresse1;
formFields['commune1']                        					= $ds006PE1.annexe1bis.adresseExploitantAnterieur.commune1;
formFields['adresse1']                        					= $ds006PE1.annexe1bis.adresseExploitantAnterieur.adresse1;
formFields['pays1']                        						= $ds006PE1.annexe1bis.adresseExploitantAnterieur.pays1;

formFields['telephoneFixe1']                        			= $ds006PE1.annexe1bis.adresseExploitantAnterieur.telephoneFixe1;
formFields['telephoneMobile1']                        			= $ds006PE1.annexe1bis.adresseExploitantAnterieur.telephoneMobile1;


formFields['repriseOui']                        				= Value('id').of($ds006PE1.annexe1bis.adresseExploitantAnterieur.accordReprise).eq("repriseOui") ? true : false;
formFields['repriseNon']                        				= Value('id').of($ds006PE1.annexe1bis.adresseExploitantAnterieur.accordReprise).eq("repriseNon") ? true : false;
formFields['repriseInconue']                        			= Value('id').of($ds006PE1.annexe1bis.adresseExploitantAnterieur.accordReprise).eq("repriseInconue") ? true : false;


formFields['natureHorsSol0']                        			= '';
formFields['effectifReelSol0']                        			= '';

formFields['natureHorsSol1']                        			= '';
formFields['effectifReelSol1']                        			= '';

formFields['natureHorsSol2']                        			= '';
formFields['effectifReelSol2']                        			= '';

formFields['natureHorsSol3']                        			= '';
formFields['effectifReelSol3']                        			= '';

formFields['natureHorsSol4']                        			= '';
formFields['effectifReelSol4']                        			= '';

formFields['natureHorsSol5']                        			= '';
formFields['effectifReelSol5']                        			= '';

formFields['natureHorsSol6']                        			= '';
formFields['effectifReelSol6']                        			= '';

formFields['natureHorsSol7']                        			= '';
formFields['effectifReelSol7']                        			= '';

formFields['natureHorsSol8']                        			= '';
formFields['effectifReelSol8']                        			= '';

formFields['natureHorsSol9']                        			= '';
formFields['effectifReelSol9']                        			= '';

formFields['natureHorsSol10']                        			= '';
formFields['effectifReelSol10']                        			= '';

formFields['natureHorsSol11']                        			= '';
formFields['effectifReelSol11']                        			= '';

formFields['natureHorsSol12']                        			= '';
formFields['effectifReelSol12']                        			= '';

formFields['natureHorsSol13']                        			= '';
formFields['effectifReelSol13']                        			= '';

formFields['natureHorsSol14']                        			= '';
formFields['effectifReelSol14']                        			= '';

formFields['natureHorsSol15']                        			= '';
formFields['effectifReelSol15']                        			= '';

formFields['natureHorsSol16']                        			= '';
formFields['effectifReelSol16']                        			= '';

formFields['natureHorsSol17']                        			= '';
formFields['effectifReelSol17']                        			= '';

formFields['natureHorsSol18']                        			= '';
formFields['effectifReelSol18']                        			= '';

formFields['natureHorsSol19']                        			= '';
formFields['effectifReelSol19']                        			= '';

formFields['natureHorsSol20']                        			= '';
formFields['effectifReelSol20']                        			= '';

formFields['natureHorsSol21']                        			= '';
formFields['effectifReelSol21']                        			= '';

formFields['natureHorsSol22']                        			= '';
formFields['effectifReelSol22']                        			= '';

formFields['natureHorsSol23']                        			= '';
formFields['effectifReelSol23']                        			= '';

formFields['natureHorsSol24']                        			= '';
formFields['effectifReelSol24']                        			= '';

formFields['natureHorsSol25']                        			= '';
formFields['effectifReelSol25']                        			= '';

formFields['natureHorsSol26']                        			= '';
formFields['effectifReelSol26']                        			= '';

formFields['natureHorsSol27']                        			= '';
formFields['effectifReelSol27']                        			= '';

formFields['natureHorsSol28']                        			= '';
formFields['effectifReelSol28']                        			= '';

formFields['natureHorsSol29']                        			= '';
formFields['effectifReelSol29']                        			= '';

formFields['natureHorsSol30']                        			= '';
formFields['effectifReelSol30']                        			= '';

formFields['natureHorsSol31']                        			= '';
formFields['effectifReelSol31']                        			= '';

for (var i= 0; i < $ds006PE1.batimentsExploitation.productionsHorsSol.size(); i++ ){
formFields['natureHorsSol'+i]             = $ds006PE1.batimentsExploitation.productionsHorsSol[i].natureHorsSol != null ? $ds006PE1.batimentsExploitation.productionsHorsSol[i].natureHorsSol :'' ;
formFields['effectifReelSol'+i]             = $ds006PE1.batimentsExploitation.productionsHorsSol[i].effectifReelSol != null ? $ds006PE1.batimentsExploitation.productionsHorsSol[i].effectifReelSol :'' ;
	
	
}

//Annexe 2

formFields['superficieHaSurface1']                        			= $ds006PE1.annexe2.caracteristiquesSurface1.superficieHaSurface1;
formFields['superficieASurface1']                        			= $ds006PE1.annexe2.caracteristiquesSurface1.superficieASurface1;
formFields['superficieCaSurface1']                        			= $ds006PE1.annexe2.caracteristiquesSurface1.superficieCaSurface1;
formFields['natureCultureSurface1']                        			= $ds006PE1.annexe2.caracteristiquesSurface1.natureCultureSurface1;
formFields['referenceCadastraleSurface1']                           = $ds006PE1.annexe2.caracteristiquesSurface1.referenceCadastraleSurface1;
formFields['communeSurface1']                          		        = $ds006PE1.annexe2.caracteristiquesSurface1.communeSurface1;
formFields['siegeSurface1']                                         = $ds006PE1.annexe2.caracteristiquesSurface1.siegeSurface1;
formFields['parcelleProcheSurface1']                          		= $ds006PE1.annexe2.caracteristiquesSurface1.parcelleProcheSurface1;

formFields['nomProprietaireSurface1']                        	    = $ds006PE1.annexe2.coordoonneesProprietaireSurface1.nomProprietaireSurface1;
formFields['prenomProprietaireSurface1']                        	= $ds006PE1.annexe2.coordoonneesProprietaireSurface1.prenomProprietaireSurface1;
formFields['adresseProprietaireSurface1']                           = $ds006PE1.annexe2.coordoonneesProprietaireSurface1.adresseProprietaireSurface1;
formFields['complementProprietaireSurface1']                        = $ds006PE1.annexe2.coordoonneesProprietaireSurface1.complementProprietaireSurface1;
formFields['codePostalProprietaireSurface1']                        = $ds006PE1.annexe2.coordoonneesProprietaireSurface1.codePostalProprietaireSurface1;
formFields['communeProprietaireSurface1']                        	= $ds006PE1.annexe2.coordoonneesProprietaireSurface1.communeProprietaireSurface1;
formFields['paysProprietaireSurface1']                              = $ds006PE1.annexe2.coordoonneesProprietaireSurface1.paysProprietaireSurface1;

formFields['nomPrenomAdresseProprietaireSurface1']                  = ($ds006PE1.annexe2.coordoonneesProprietaireSurface1.nomProprietaireSurface1 != null ? $ds006PE1.annexe2.coordoonneesProprietaireSurface1.nomProprietaireSurface1 + ", " :'') + ($ds006PE1.annexe2.coordoonneesProprietaireSurface1.prenomProprietaireSurface1 != null ? $ds006PE1.annexe2.coordoonneesProprietaireSurface1.prenomProprietaireSurface1 + ", " :'') + ($ds006PE1.annexe2.coordoonneesProprietaireSurface1.adresseProprietaireSurface1 != null ? $ds006PE1.annexe2.coordoonneesProprietaireSurface1.adresseProprietaireSurface1 + ", " :'') + ($ds006PE1.annexe2.coordoonneesProprietaireSurface1.complementProprietaireSurface1 != null ? $ds006PE1.annexe2.coordoonneesProprietaireSurface1.complementProprietaireSurface1 + ", " :'') + ($ds006PE1.annexe2.coordoonneesProprietaireSurface1.communeProprietaireSurface1 != null ? $ds006PE1.annexe2.coordoonneesProprietaireSurface1.communeProprietaireSurface1 + ", " :'') + ($ds006PE1.annexe2.coordoonneesProprietaireSurface1.communeProprietaireSurface1 != null ? $ds006PE1.annexe2.coordoonneesProprietaireSurface1.communeProprietaireSurface1 + ", " :'');

formFields['superficieHaSurface2']                        			= $ds006PE1.annexe2Bis.caracteristiquesSurface2.superficieHaSurface2;
formFields['superficieASurface2']                        			= $ds006PE1.annexe2Bis.caracteristiquesSurface2.superficieASurface2;
formFields['superficieCaSurface2']                        			= $ds006PE1.annexe2Bis.caracteristiquesSurface2.superficieCaSurface2;
formFields['natureCultureSurface2']                        			= $ds006PE1.annexe2Bis.caracteristiquesSurface2.natureCultureSurface2;
formFields['referenceCadastraleSurface2']                           = $ds006PE1.annexe2Bis.caracteristiquesSurface2.referenceCadastraleSurface2;
formFields['communeSurface2']                          		        = $ds006PE1.annexe2Bis.caracteristiquesSurface2.communeSurface2;
formFields['siegeSurface2']                                         = $ds006PE1.annexe2Bis.caracteristiquesSurface2.siegeSurface2;
formFields['parcelleProcheSurface2']                          		= $ds006PE1.annexe2Bis.caracteristiquesSurface2.parcelleProcheSurface2;

formFields['nomProprietaireSurface2']                        	    = $ds006PE1.annexe2Bis.coordoonneesProprietaireSurface2.nomProprietaireSurface2;
formFields['prenomProprietaireSurface2']                        	= $ds006PE1.annexe2Bis.coordoonneesProprietaireSurface2.prenomProprietaireSurface2;
formFields['adresseProprietaireSurface2']                           = $ds006PE1.annexe2Bis.coordoonneesProprietaireSurface2.adresseProprietaireSurface2;
formFields['complementProprietaireSurface2']                        = $ds006PE1.annexe2Bis.coordoonneesProprietaireSurface2.complementProprietaireSurface2;
formFields['codePostalProprietaireSurface2']                        = $ds006PE1.annexe2Bis.coordoonneesProprietaireSurface2.codePostalProprietaireSurface2;
formFields['communeProprietaireSurface2']                        	= $ds006PE1.annexe2Bis.coordoonneesProprietaireSurface2.communeProprietaireSurface2;
formFields['paysProprietaireSurface2']                              = $ds006PE1.annexe2Bis.coordoonneesProprietaireSurface2.paysProprietaireSurface2;

formFields['nomPrenomAdresseProprietaireSurface2']                  = ($ds006PE1.annexe2Bis.coordoonneesProprietaireSurface2.nomProprietaireSurface2 != null ? $ds006PE1.annexe2Bis.coordoonneesProprietaireSurface2.nomProprietaireSurface2 + ", " :'') + ($ds006PE1.annexe2Bis.coordoonneesProprietaireSurface2.prenomProprietaireSurface2 != null ? $ds006PE1.annexe2Bis.coordoonneesProprietaireSurface2.prenomProprietaireSurface2 + ", " :'') + ($ds006PE1.annexe2Bis.coordoonneesProprietaireSurface2.adresseProprietaireSurface2 != null ? $ds006PE1.annexe2Bis.coordoonneesProprietaireSurface2.adresseProprietaireSurface2 + ", " :'') + ($ds006PE1.annexe2Bis.coordoonneesProprietaireSurface2.complementProprietaireSurface2 != null ? $ds006PE1.annexe2Bis.coordoonneesProprietaireSurface2.complementProprietaireSurface2 + ", " :'') + ($ds006PE1.annexe2Bis.coordoonneesProprietaireSurface2.communeProprietaireSurface2 != null ? $ds006PE1.annexe2Bis.coordoonneesProprietaireSurface2.communeProprietaireSurface2 + ", " :'') + ($ds006PE1.annexe2Bis.coordoonneesProprietaireSurface2.paysProprietaireSurface2 != null ? $ds006PE1.annexe2Bis.coordoonneesProprietaireSurface2.paysProprietaireSurface2 + ", " :'');


formFields['superficieHaSurface3']                        			= $ds006PE1.annexe2Bis1.caracteristiquesSurface3.superficieHaSurface3;
formFields['superficieASurface3']                        			= $ds006PE1.annexe2Bis1.caracteristiquesSurface3.superficieASurface3;
formFields['superficieCaSurface3']                        			= $ds006PE1.annexe2Bis1.caracteristiquesSurface3.superficieCaSurface3;
formFields['natureCultureSurface3']                        			= $ds006PE1.annexe2Bis1.caracteristiquesSurface3.natureCultureSurface3;
formFields['referenceCadastraleSurface3']                           = $ds006PE1.annexe2Bis1.caracteristiquesSurface3.referenceCadastraleSurface3;
formFields['communeSurface3']                          		        = $ds006PE1.annexe2Bis1.caracteristiquesSurface3.communeSurface3;
formFields['siegeSurface3']                                         = $ds006PE1.annexe2Bis1.caracteristiquesSurface3.siegeSurface3;
formFields['parcelleProcheSurface3']                          		= $ds006PE1.annexe2Bis1.caracteristiquesSurface3.parcelleProcheSurface3;

formFields['nomProprietaireSurface3']                        	    = $ds006PE1.annexe2Bis1.coordoonneesProprietaireSurface3.nomProprietaireSurface3;
formFields['prenomProprietaireSurface3']                        	= $ds006PE1.annexe2Bis1.coordoonneesProprietaireSurface3.prenomProprietaireSurface3;
formFields['adresseProprietaireSurface3']                           = $ds006PE1.annexe2Bis1.coordoonneesProprietaireSurface3.adresseProprietaireSurface3;
formFields['complementProprietaireSurface3']                        = $ds006PE1.annexe2Bis1.coordoonneesProprietaireSurface3.complementProprietaireSurface3;
formFields['codePostalProprietaireSurface3']                        = $ds006PE1.annexe2Bis1.coordoonneesProprietaireSurface3.codePostalProprietaireSurface3;
formFields['communeProprietaireSurface3']                        	= $ds006PE1.annexe2Bis1.coordoonneesProprietaireSurface3.communeProprietaireSurface3;
formFields['paysProprietaireSurface3']                              = $ds006PE1.annexe2Bis1.coordoonneesProprietaireSurface3.paysProprietaireSurface3;

formFields['nomPrenomAdresseProprietaireSurface3']                  = ($ds006PE1.annexe2Bis1.coordoonneesProprietaireSurface3.nomProprietaireSurface3 != null ? $ds006PE1.annexe2Bis1.coordoonneesProprietaireSurface3.nomProprietaireSurface3 + ", " :'') + ($ds006PE1.annexe2Bis1.coordoonneesProprietaireSurface3.prenomProprietaireSurface3 != null ? $ds006PE1.annexe2Bis1.coordoonneesProprietaireSurface3.prenomProprietaireSurface3 + ", " :'') + ($ds006PE1.annexe2Bis1.coordoonneesProprietaireSurface3.adresseProprietaireSurface3 != null ? $ds006PE1.annexe2Bis1.coordoonneesProprietaireSurface3.adresseProprietaireSurface3 + ", " :'') + ($ds006PE1.annexe2Bis1.coordoonneesProprietaireSurface3.complementProprietaireSurface3 != null ? $ds006PE1.annexe2Bis1.coordoonneesProprietaireSurface3.complementProprietaireSurface3 + ", " :'') + ($ds006PE1.annexe2Bis1.coordoonneesProprietaireSurface3.communeProprietaireSurface3 != null ? $ds006PE1.annexe2Bis1.coordoonneesProprietaireSurface3.communeProprietaireSurface3 + ", " :'') + ($ds006PE1.annexe2Bis1.coordoonneesProprietaireSurface3.paysProprietaireSurface3 != null ? $ds006PE1.annexe2Bis1.coordoonneesProprietaireSurface3.paysProprietaireSurface3 :'');


formFields['superficieHaSurface4']                        			= $ds006PE1.annexe2Bis2.caracteristiquesSurface4.superficieHaSurface4;
formFields['superficieASurface4']                        			= $ds006PE1.annexe2Bis2.caracteristiquesSurface4.superficieASurface4;
formFields['superficieCaSurface4']                        			= $ds006PE1.annexe2Bis2.caracteristiquesSurface4.superficieCaSurface4;
formFields['natureCultureSurface4']                        			= $ds006PE1.annexe2Bis2.caracteristiquesSurface4.natureCultureSurface4;
formFields['referenceCadastraleSurface4']                           = $ds006PE1.annexe2Bis2.caracteristiquesSurface4.referenceCadastraleSurface4;
formFields['communeSurface4']                          		        = $ds006PE1.annexe2Bis2.caracteristiquesSurface4.communeSurface4;
formFields['siegeSurface4']                                         = $ds006PE1.annexe2Bis2.caracteristiquesSurface4.siegeSurface4;
formFields['parcelleProcheSurface4']                          		= $ds006PE1.annexe2Bis2.caracteristiquesSurface4.parcelleProcheSurface4;

formFields['nomProprietaireSurface4']                        	    = $ds006PE1.annexe2Bis2.coordoonneesProprietaireSurface4.nomProprietaireSurface4;
formFields['prenomProprietaireSurface4']                        	= $ds006PE1.annexe2Bis2.coordoonneesProprietaireSurface4.prenomProprietaireSurface4;
formFields['adresseProprietaireSurface4']                           = $ds006PE1.annexe2Bis2.coordoonneesProprietaireSurface4.adresseProprietaireSurface4;
formFields['complementProprietaireSurface4']                        = $ds006PE1.annexe2Bis2.coordoonneesProprietaireSurface4.complementProprietaireSurface4;
formFields['codePostalProprietaireSurface4']                        = $ds006PE1.annexe2Bis2.coordoonneesProprietaireSurface4.codePostalProprietaireSurface4;
formFields['communeProprietaireSurface4']                        	= $ds006PE1.annexe2Bis2.coordoonneesProprietaireSurface4.communeProprietaireSurface4;
formFields['paysProprietaireSurface4']                              = $ds006PE1.annexe2Bis2.coordoonneesProprietaireSurface4.paysProprietaireSurface4;


formFields['nomPrenomAdresseProprietaireSurface4']                  = ($ds006PE1.annexe2Bis2.coordoonneesProprietaireSurface4.nomProprietaireSurface4 != null ? $ds006PE1.annexe2Bis2.coordoonneesProprietaireSurface4.nomProprietaireSurface4 + ", " :'') + ($ds006PE1.annexe2Bis2.coordoonneesProprietaireSurface4.prenomProprietaireSurface4 != null ? $ds006PE1.annexe2Bis2.coordoonneesProprietaireSurface4.prenomProprietaireSurface4 + ", " :'') + ($ds006PE1.annexe2Bis2.coordoonneesProprietaireSurface4.adresseProprietaireSurface4 != null ? $ds006PE1.annexe2Bis2.coordoonneesProprietaireSurface4.adresseProprietaireSurface4 + ", " :'') + ($ds006PE1.annexe2Bis2.coordoonneesProprietaireSurface4.complementProprietaireSurface4 != null ? $ds006PE1.annexe2Bis2.coordoonneesProprietaireSurface4.complementProprietaireSurface4 + ", " :'') + ($ds006PE1.annexe2Bis2.coordoonneesProprietaireSurface4.communeProprietaireSurface4 != null ? $ds006PE1.annexe2Bis2.coordoonneesProprietaireSurface4.communeProprietaireSurface4 + ", " :'') + ($ds006PE1.annexe2Bis2.coordoonneesProprietaireSurface4.paysProprietaireSurface4 != null ? $ds006PE1.annexe2Bis2.coordoonneesProprietaireSurface4.paysProprietaireSurface4 :'');



formFields['superficieHaSurface5']                        			= $ds006PE1.annexe2Bis3.caracteristiquesSurface5.superficieHaSurface5;
formFields['superficieASurface5']                        			= $ds006PE1.annexe2Bis3.caracteristiquesSurface5.superficieASurface5;
formFields['superficieCaSurface5']                        			= $ds006PE1.annexe2Bis3.caracteristiquesSurface5.superficieCaSurface5;
formFields['natureCultureSurface5']                        			= $ds006PE1.annexe2Bis3.caracteristiquesSurface5.natureCultureSurface5;
formFields['referenceCadastraleSurface5']                           = $ds006PE1.annexe2Bis3.caracteristiquesSurface5.referenceCadastraleSurface5;
formFields['communeSurface5']                          		        = $ds006PE1.annexe2Bis3.caracteristiquesSurface5.communeSurface5;
formFields['siegeSurface5']                                         = $ds006PE1.annexe2Bis3.caracteristiquesSurface5.siegeSurface5;
formFields['parcelleProcheSurface5']                          		= $ds006PE1.annexe2Bis3.caracteristiquesSurface5.parcelleProcheSurface5;

formFields['nomProprietaireSurface5']                        	    = $ds006PE1.annexe2Bis3.coordoonneesProprietaireSurface5.nomProprietaireSurface5;
formFields['prenomProprietaireSurface5']                        	= $ds006PE1.annexe2Bis3.coordoonneesProprietaireSurface5.prenomProprietaireSurface5;
formFields['adresseProprietaireSurface5']                           = $ds006PE1.annexe2Bis3.coordoonneesProprietaireSurface5.adresseProprietaireSurface5;
formFields['complementProprietaireSurface5']                        = $ds006PE1.annexe2Bis3.coordoonneesProprietaireSurface5.complementProprietaireSurface5;
formFields['codePostalProprietaireSurface5']                        = $ds006PE1.annexe2Bis3.coordoonneesProprietaireSurface5.codePostalProprietaireSurface5;
formFields['communeProprietaireSurface5']                        	= $ds006PE1.annexe2Bis3.coordoonneesProprietaireSurface5.communeProprietaireSurface5;
formFields['paysProprietaireSurface5']                              = $ds006PE1.annexe2Bis3.coordoonneesProprietaireSurface5.paysProprietaireSurface5;


formFields['nomPrenomAdresseProprietaireSurface5']                  = ($ds006PE1.annexe2Bis3.coordoonneesProprietaireSurface5.nomProprietaireSurface5 != null ? $ds006PE1.annexe2Bis3.coordoonneesProprietaireSurface5.nomProprietaireSurface5 + ", " :'') + ($ds006PE1.annexe2Bis3.coordoonneesProprietaireSurface5.prenomProprietaireSurface5 != null ? $ds006PE1.annexe2Bis3.coordoonneesProprietaireSurface5.prenomProprietaireSurface5 + ", " :'') + ($ds006PE1.annexe2Bis3.coordoonneesProprietaireSurface5.adresseProprietaireSurface5 != null ? $ds006PE1.annexe2Bis3.coordoonneesProprietaireSurface5.adresseProprietaireSurface5 + ", " :'') + ($ds006PE1.annexe2Bis3.coordoonneesProprietaireSurface5.complementProprietaireSurface5 != null ? $ds006PE1.annexe2Bis3.coordoonneesProprietaireSurface5.complementProprietaireSurface5 + ", " :'') + ($ds006PE1.annexe2Bis3.coordoonneesProprietaireSurface5.communeProprietaireSurface5 != null ? $ds006PE1.annexe2Bis3.coordoonneesProprietaireSurface5.communeProprietaireSurface5 + ", " :'') + ($ds006PE1.annexe2Bis3.coordoonneesProprietaireSurface5.paysProprietaireSurface5 != null ? $ds006PE1.annexe2Bis3.coordoonneesProprietaireSurface5.paysProprietaireSurface5 :'');



formFields['superficieHaSurface6']                        			= $ds006PE1.annexe2Bis4.caracteristiquesSurface6.superficieHaSurface6;
formFields['superficieASurface6']                        			= $ds006PE1.annexe2Bis4.caracteristiquesSurface6.superficieASurface6;
formFields['superficieCaSurface6']                        			= $ds006PE1.annexe2Bis4.caracteristiquesSurface6.superficieCaSurface6;
formFields['natureCultureSurface6']                        			= $ds006PE1.annexe2Bis4.caracteristiquesSurface6.natureCultureSurface6;
formFields['referenceCadastraleSurface6']                           = $ds006PE1.annexe2Bis4.caracteristiquesSurface6.referenceCadastraleSurface6;
formFields['communeSurface6']                          		        = $ds006PE1.annexe2Bis4.caracteristiquesSurface6.communeSurface6;
formFields['siegeSurface6']                                         = $ds006PE1.annexe2Bis4.caracteristiquesSurface6.siegeSurface7;
formFields['parcelleProcheSurface6']                          		= $ds006PE1.annexe2Bis4.caracteristiquesSurface6.parcelleProcheSurface6;

formFields['nomProprietaireSurface6']                        	    = $ds006PE1.annexe2Bis4.coordoonneesProprietaireSurface6.nomProprietaireSurface6;
formFields['prenomProprietaireSurface6']                        	= $ds006PE1.annexe2Bis4.coordoonneesProprietaireSurface6.prenomProprietaireSurface6;
formFields['adresseProprietaireSurface6']                           = $ds006PE1.annexe2Bis4.coordoonneesProprietaireSurface6.adresseProprietaireSurface6;
formFields['complementProprietaireSurface6']                        = $ds006PE1.annexe2Bis4.coordoonneesProprietaireSurface6.complementProprietaireSurface6;
formFields['codePostalProprietaireSurface6']                        = $ds006PE1.annexe2Bis4.coordoonneesProprietaireSurface6.codePostalProprietaireSurface6;
formFields['communeProprietaireSurface6']                        	= $ds006PE1.annexe2Bis4.coordoonneesProprietaireSurface6.communeProprietaireSurface6;
formFields['paysProprietaireSurface6']                              = $ds006PE1.annexe2Bis4.coordoonneesProprietaireSurface6.paysProprietaireSurface6;

formFields['nomPrenomAdresseProprietaireSurface6']                  = ($ds006PE1.annexe2Bis4.coordoonneesProprietaireSurface6.nomProprietaireSurface6 != null ? $ds006PE1.annexe2Bis4.coordoonneesProprietaireSurface6.nomProprietaireSurface6 + ", " :'') + ($ds006PE1.annexe2Bis4.coordoonneesProprietaireSurface6.prenomProprietaireSurface6 != null ? $ds006PE1.annexe2Bis4.coordoonneesProprietaireSurface6.prenomProprietaireSurface6 + ", " :'') + ($ds006PE1.annexe2Bis4.coordoonneesProprietaireSurface6.adresseProprietaireSurface6 != null ? $ds006PE1.annexe2Bis4.coordoonneesProprietaireSurface6.adresseProprietaireSurface6 + ", " :'') + ($ds006PE1.annexe2Bis4.coordoonneesProprietaireSurface6.complementProprietaireSurface6 != null ? $ds006PE1.annexe2Bis4.coordoonneesProprietaireSurface6.complementProprietaireSurface6 + ", " :'') + ($ds006PE1.annexe2Bis4.coordoonneesProprietaireSurface6.communeProprietaireSurface6 != null ? $ds006PE1.annexe2Bis4.coordoonneesProprietaireSurface6.communeProprietaireSurface6 + ", " :'') + ($ds006PE1.annexe2Bis4.coordoonneesProprietaireSurface6.paysProprietaireSurface6 != null ? $ds006PE1.annexe2Bis4.coordoonneesProprietaireSurface6.paysProprietaireSurface6 :'');



formFields['superficieHaSurface7']                        			= $ds006PE1.annexe2Bis5.caracteristiquesSurface7.superficieHaSurface7;
formFields['superficieASurface7']                        			= $ds006PE1.annexe2Bis5.caracteristiquesSurface7.superficieASurface7;
formFields['superficieCaSurface7']                        			= $ds006PE1.annexe2Bis5.caracteristiquesSurface7.superficieCaSurface7;
formFields['natureCultureSurface7']                        			= $ds006PE1.annexe2Bis5.caracteristiquesSurface7.natureCultureSurface7;
formFields['referenceCadastraleSurface7']                           = $ds006PE1.annexe2Bis5.caracteristiquesSurface7.referenceCadastraleSurface7;
formFields['communeSurface7']                          		        = $ds006PE1.annexe2Bis5.caracteristiquesSurface7.communeSurface7;
formFields['siegeSurface7']                                         = $ds006PE1.annexe2Bis5.caracteristiquesSurface7.siegeSurface7;
formFields['parcelleProcheSurface7']                          		= $ds006PE1.annexe2Bis5.caracteristiquesSurface7.parcelleProcheSurface7;

formFields['nomProprietaireSurface7']                        	    = $ds006PE1.annexe2Bis5.coordoonneesProprietaireSurface7.nomProprietaireSurface7;
formFields['prenomProprietaireSurface7']                        	= $ds006PE1.annexe2Bis5.coordoonneesProprietaireSurface7.prenomProprietaireSurface7;
formFields['adresseProprietaireSurface7']                           = $ds006PE1.annexe2Bis5.coordoonneesProprietaireSurface7.adresseProprietaireSurface7;
formFields['complementProprietaireSurface7']                        = $ds006PE1.annexe2Bis5.coordoonneesProprietaireSurface7.complementProprietaireSurface7;
formFields['codePostalProprietaireSurface7']                        = $ds006PE1.annexe2Bis5.coordoonneesProprietaireSurface7.codePostalProprietaireSurface7;
formFields['communeProprietaireSurface7']                        	= $ds006PE1.annexe2Bis5.coordoonneesProprietaireSurface7.communeProprietaireSurface7;
formFields['paysProprietaireSurface7']                              = $ds006PE1.annexe2Bis5.coordoonneesProprietaireSurface7.paysProprietaireSurface7;

formFields['nomPrenomAdresseProprietaireSurface7']                  = ($ds006PE1.annexe2Bis5.coordoonneesProprietaireSurface7.nomProprietaireSurface7 != null ? $ds006PE1.annexe2Bis5.coordoonneesProprietaireSurface7.nomProprietaireSurface7 + ", " :'') + ($ds006PE1.annexe2Bis5.coordoonneesProprietaireSurface7.prenomProprietaireSurface7 != null ? $ds006PE1.annexe2Bis5.coordoonneesProprietaireSurface7.prenomProprietaireSurface7 + ", " :'') + ($ds006PE1.annexe2Bis5.coordoonneesProprietaireSurface7.adresseProprietaireSurface7 != null ? $ds006PE1.annexe2Bis5.coordoonneesProprietaireSurface7.adresseProprietaireSurface7 + ", " :'') + ($ds006PE1.annexe2Bis5.coordoonneesProprietaireSurface7.complementProprietaireSurface7 != null ? $ds006PE1.annexe2Bis5.coordoonneesProprietaireSurface7.complementProprietaireSurface7 + ", " :'') + ($ds006PE1.annexe2Bis5.coordoonneesProprietaireSurface7.communeProprietaireSurface7 != null ? $ds006PE1.annexe2Bis5.coordoonneesProprietaireSurface7.communeProprietaireSurface7 + ", " :'') + ($ds006PE1.annexe2Bis5.coordoonneesProprietaireSurface7.paysProprietaireSurface7 != null ? $ds006PE1.annexe2Bis5.coordoonneesProprietaireSurface7.paysProprietaireSurface7 :'');



//Annexe 3

formFields['nSiret3']                                       	= $ds006PE1.annexe3.identificationExploitationDemandeur.nSiret3; 
formFields['nPacage3']                                       	= $ds006PE1.annexe3.identificationExploitationDemandeur.nPacage3; 
formFields['nomPrenomsRaisonSociale3']                          = $ds006PE1.annexe3.civilite1.nomPrenomsRaisonSociale3; 
formFields['adresseExploitation']                               = ($ds006PE1.annexe3.civilite1.adressePostale3 != null ? $ds006PE1.annexe3.civilite1.adressePostale3 + ', ' :'') + ($ds006PE1.annexe3.civilite1.complementAdresse3 != null ? $ds006PE1.annexe3.civilite1.complementAdresse3 + ', ' :'') + ($ds006PE1.annexe3.civilite1.codePostal3 != null ? $ds006PE1.annexe3.civilite1.codePostal3 + ', ' :'') + ($ds006PE1.annexe3.civilite1.commune3 != null ? $ds006PE1.annexe3.civilite1.commune3 + ', ' :'') + ($ds006PE1.annexe3.civilite1.paysPersonne4 != null ? $ds006PE1.annexe3.civilite1.paysPersonne4 + ', ' :'');


formFields['haExploitationTerres']                              = $ds006PE1.annexe3bis.descriptionExploitation.haExploitationTerres;
formFields['aExploitationTerres']                              	= $ds006PE1.annexe3bis.descriptionExploitation.aExploitationTerres;
formFields['caExploitationTerres']                              = $ds006PE1.annexe3bis.descriptionExploitation.caExploitationTerres;

formFields['haExploitationTerres1']                             = $ds006PE1.annexe3bis.superficiePropriete.haExploitationTerres1;
formFields['aExploitationTerres1']                              = $ds006PE1.annexe3bis.superficiePropriete.aExploitationTerres1;
formFields['caExploitationTerres1']                             = $ds006PE1.annexe3bis.superficiePropriete.caExploitationTerres1;
formFields['haExploitationTerres2']                             = $ds006PE1.annexe3bis.superficieFermage.haExploitationTerres2;
formFields['aExploitationTerres2']                              = $ds006PE1.annexe3bis.superficieFermage.aExploitationTerres2;
formFields['caExploitationTerres2']                             = $ds006PE1.annexe3bis.superficieFermage.caExploitationTerres2;
formFields['haExploitationTerres3']                             = $ds006PE1.annexe3bis.superficieAutre.haExploitationTerres3;
formFields['aExploitationTerres3']                              = $ds006PE1.annexe3bis.superficieAutre.aExploitationTerres3;
formFields['caExploitationTerres3']                             = $ds006PE1.annexe3bis.superficieAutre.caExploitationTerres3;

formFields['natureCultureSols0']                        		= '';
formFields['haExploitationSols0']                        		= '';
formFields['aExploitationSols0']                        		= '';
formFields['caExploitationnSols0']                        		= '';

formFields['natureCultureSols1']                        		= '';
formFields['haExploitationSols1']                        		= '';
formFields['aExploitationSols1']                        		= '';
formFields['caExploitationnSols1']                        		= '';

formFields['natureCultureSols2']                        		= '';
formFields['haExploitationSols2']                        		= '';
formFields['aExploitationSols2']                        		= '';
formFields['caExploitationnSols2']                        		= '';

formFields['natureCultureSols3']                        		= '';
formFields['haExploitationSols3']                        		= '';
formFields['aExploitationSols3']                        		= '';
formFields['caExploitationnSols3']                        		= '';

formFields['natureCultureSols4']                        		= '';
formFields['haExploitationSols4']                        		= '';
formFields['aExploitationSols4']                        		= '';
formFields['caExploitationnSols4']                        		= '';

formFields['natureCultureSols5']                        		= '';
formFields['haExploitationSols5']                        		= '';
formFields['aExploitationSols5']                        		= '';
formFields['caExploitationnSols5']                        		= '';

formFields['natureCultureSols6']                        		= '';
formFields['haExploitationSols6']                        		= '';
formFields['aExploitationSols6']                        		= '';
formFields['caExploitationnSols6']                        		= '';

formFields['natureCultureSols7']                        		= '';
formFields['haExploitationSols7']                        		= '';
formFields['aExploitationSols7']                        		= '';
formFields['caExploitationnSols7']                        		= '';

formFields['natureCultureSols8']                        		= '';
formFields['haExploitationSols8']                        		= '';
formFields['aExploitationSols8']                        		= '';
formFields['caExploitationnSols8']                        		= '';

formFields['natureCultureSols9']                        		= '';
formFields['haExploitationSols9']                        		= '';
formFields['aExploitationSols9']                        		= '';
formFields['caExploitationnSols9']                        		= '';

formFields['natureCultureSols10']                        		= '';
formFields['haExploitationSols10']                        		= '';
formFields['aExploitationSols10']                        		= '';
formFields['caExploitationnSols10']                        		= '';

formFields['natureCultureSols11']                        		= '';
formFields['haExploitationSols11']                        		= '';
formFields['aExploitationSols11']                        		= '';
formFields['caExploitationnSols11']                        		= '';

formFields['natureCultureSols12']                        		= '';
formFields['haExploitationSols12']                        		= '';
formFields['aExploitationSols12']                        		= '';
formFields['caExploitationnSols12']                        		= '';

formFields['natureCultureSols13']                        		= '';
formFields['haExploitationSols13']                        		= '';
formFields['aExploitationSols13']                        		= '';
formFields['caExploitationnSols13']                        		= '';

formFields['natureCultureSols14']                        		= '';
formFields['haExploitationSols14']                        		= '';
formFields['aExploitationSols14']                        		= '';
formFields['caExploitationnSols14']                        		= '';

formFields['natureCultureSols15']                        		= '';
formFields['haExploitationSols15']                        		= '';
formFields['aExploitationSols15']                        		= '';
formFields['caExploitationnSols15']                        		= '';


for (var i= 0; i < $ds006PE1.annexe3Bis2.occupationSols.size(); i++ ){
	
formFields['natureCultureSols'+i]         						= $ds006PE1.annexe3Bis2.occupationSols[i].natureCultureSols != null ? $ds006PE1.annexe3Bis2.occupationSols[i].natureCultureSols :'' ;
formFields['haExploitationSols'+i]         						= $ds006PE1.annexe3Bis2.occupationSols[i].haExploitationSols != null ? $ds006PE1.annexe3Bis2.occupationSols[i].haExploitationSols :'' ;
formFields['aExploitationSols'+i]         						= $ds006PE1.annexe3Bis2.occupationSols[i].aExploitationSols != null ? $ds006PE1.annexe3Bis2.occupationSols[i].aExploitationSols :'' ;
formFields['caExploitationnSols'+i]         					= $ds006PE1.annexe3Bis2.occupationSols[i].caExploitationnSols != null ? $ds006PE1.annexe3Bis2.occupationSols[i].caExploitationnSols :'' ;
}

formFields['elevageHorsSol0']                        			= '';
formFields['effectifReelHorsSol0']                        		= '';

formFields['elevageHorsSol1']                        			= '';
formFields['effectifReelHorsSol1']                        		= '';

formFields['elevageHorsSol2']                        			= '';
formFields['effectifReelHorsSol2']                        		= '';

formFields['elevageHorsSol3']                        			= '';
formFields['effectifReelHorsSol3']                        		= '';

formFields['elevageHorsSol4']                        			= '';
formFields['effectifReelHorsSol4']                        		= '';

formFields['elevageHorsSol5']                        			= '';
formFields['effectifReelHorsSol5']                        		= '';

formFields['elevageHorsSol6']                        			= '';
formFields['effectifReelHorsSol6']                        		= '';

formFields['elevageHorsSol7']                        			= '';
formFields['effectifReelHorsSol7']                        		= '';

formFields['elevageHorsSol8']                        			= '';
formFields['effectifReelHorsSol8']                        		= '';

formFields['elevageHorsSol9']                        			= '';
formFields['effectifReelHorsSol9']                        		= '';

formFields['elevageHorsSol10']                        			= '';
formFields['effectifReelHorsSol10']                        		= '';

formFields['elevageHorsSol11']                        			= '';
formFields['effectifReelHorsSol11']                        		= '';

formFields['elevageHorsSol12']                        			= '';
formFields['effectifReelHorsSol12']                        		= '';

formFields['elevageHorsSol13']                        			= '';
formFields['effectifReelHorsSol13']                        		= '';

formFields['elevageHorsSol14']                        			= '';
formFields['effectifReelHorsSol14']                        		= '';

formFields['elevageHorsSol15']                        			= '';
formFields['effectifReelHorsSol15']                        		= '';

formFields['elevageHorsSol16']                        			= '';
formFields['effectifReelHorsSol16']                        		= '';

formFields['elevageHorsSol17']                        			= '';
formFields['effectifReelHorsSol17']                        		= '';

formFields['elevageHorsSol18']                        			= '';
formFields['effectifReelHorsSol18']                        		= '';

formFields['elevageHorsSol19']                        			= '';
formFields['effectifReelHorsSol19']                        		= '';

formFields['elevageHorsSol20']                        			= '';
formFields['effectifReelHorsSol20']                        		= '';

formFields['elevageHorsSol21']                        			= '';
formFields['effectifReelHorsSol21']                        		= '';

formFields['elevageHorsSol22']                        			= '';
formFields['effectifReelHorsSol22']                        		= '';

formFields['elevageHorsSol23']                        			= '';
formFields['effectifReelHorsSol23']                        		= '';

formFields['elevageHorsSol24']                        			= '';
formFields['effectifReelHorsSol24']                        		= '';

formFields['elevageHorsSol25']                        			= '';
formFields['effectifReelHorsSol25']                        	    = '';

	
for (var i= 0; i < $ds006PE1.annexe3bis3.horsSol.size(); i++ ){
	
formFields['elevageHorsSol'+i]         							= $ds006PE1.annexe3bis3.horsSol[i].elevageHorsSol != null ? $ds006PE1.annexe3bis3.horsSol[i].elevageHorsSol :'';
formFields['effectifReelHorsSol'+i]         					= $ds006PE1.annexe3bis3.horsSol[i].effectifReelHorsSol != null ? $ds006PE1.annexe3bis3.horsSol[i].effectifReelHorsSol :'';
}

formFields['typesBatimentsHorsSols']                            = $ds006PE1.annexe3Bis4.typesBatimentsHorsSols.typesBatimentsHorsSols;
formFields['nombreEmploisNonSalaries']                          = $ds006PE1.annexe3Bis4.maindoeuvre.nombreEmploisNonSalaries;
formFields['nombreSalariesEmploiPermanent']                     = $ds006PE1.annexe3Bis4.maindoeuvre.nombreSalariesEmploiPermanent;
formFields['nombreSalariesEmploiSaisonnier']                    = $ds006PE1.annexe3Bis4.maindoeuvre.nombreSalariesEmploiSaisonnier;

//Annexe 4

formFields['criteresEconomiques']                               = $ds006PE1.annexe4.criteresEconomiques.criteresEconomiques;
formFields['criteresSociaux']                                   = $ds006PE1.annexe4.criteresSociaux.criteresSociaux;
formFields['criteresEnvironnementaux']                          = $ds006PE1.annexe4.criteresEnvironnementaux.criteresEnvironnementaux;
formFields['autresCriteres']                                  = $ds006PE1.annexe4.autresCriteres.autresCriteres;

//Lettre de motivation 

formFields['lettreMotivation']                                  = $ds006PE1.lettre.lettreMotivation.lettreMotivation;


//Signature

formFields['nomPrenomsQualiteDemandeur']                   	= $ds006PE1.signatureGroup.signature.nomPrenomsQualiteDemandeur;
formFields['declarationHonneur1']                               = $ds006PE1.signatureGroup.signature.declarationHonneur1;
formFields['declarationHonneur2']                               = $ds006PE1.signatureGroup.signature.declarationHonneur2;
formFields['dateSignature']                                     = $ds006PE1.signatureGroup.signature.dateSignature;
formFields['Signature']                                         = $ds006PE1.signatureGroup.signature.Signature;



/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $ds006PE1.signatureGroup.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		date: $ds006PE1.signatureGroup.signature.dateSignature,
		autoriteHabilitee :"Préfet de la région",
		demandeContexte : "Demande d'autorisation d'exploiter un centre équestre.",
		civiliteNomPrenom : $ds006PE1.signatureGroup.signature.nomPrenomsQualiteDemandeur,
	});
	
var cerfaDoc = nash.doc //
    .load('models/Cerfa_11534-04.pdf')//
    .apply(formFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));

var cerfaDoc = nash.doc //
    .load('models/annexe_11534-04-01.pdf')//
    .apply(formFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));

var cerfaDoc = nash.doc //
    .load('models/annexe_11534-04-02.pdf')//
    .apply(formFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));

var cerfaDoc = nash.doc //
    .load('models/annexe_11534-04-03.pdf')//
    .apply(formFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));

var cerfaDoc = nash.doc //
    .load('models/annexe_11534-04-04.pdf')//
    .apply(formFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));


function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJ
 */
 
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);


var finalDocItem = finalDoc.save('Centre_equestre_et_assimiles.pdf');

return spec.create({
    id : 'review',
   label : 'Centre équestre et assimilés - Demande d\'autorisation d\'exploiter un centre équestre',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande d\'autorisation d\'exploiter un centre équestre.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});