var cerfaFields = {};
//etatCivil

var civNomPrenom = $ds015PE1.etatCivil.identificationDeclarant.civilite + ' ' + $ds015PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $ds015PE1.etatCivil.identificationDeclarant.prenomDeclarant;

// Etat civil
cerfaFields['nomFamille']          	= $ds015PE1.etatCivil.identificationDeclarant.nomDeclarant;
cerfaFields['nomUsage']          	= $ds015PE1.etatCivil.identificationDeclarant.nomUsage;
cerfaFields['prenom']          		= $ds015PE1.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['dateNaissance']        = $ds015PE1.etatCivil.identificationDeclarant.dateNaissanceDeclarant;
cerfaFields['lieuNaissance']        = $ds015PE1.etatCivil.identificationDeclarant.lieuNaissanceDeclarant;
cerfaFields['nationalite']          = $ds015PE1.etatCivil.identificationDeclarant.nationaliteDeclarant;

// Adresse
cerfaFields['adresseNumRue'] 		= $ds015PE1.adresse.adresseContact.numeroLibelleAdresseDeclarant + ' ' + ($ds015PE1.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $ds015PE1.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['adresseCPVille'] 		= ($ds015PE1.adresse.adresseContact.codePostalAdresseDeclarant != null ? $ds015PE1.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + ' ' + $ds015PE1.adresse.adresseContact.villeAdresseDeclarant;
cerfaFields['mail']          		= $ds015PE1.adresse.adresseContact.mailAdresseDeclarant;

// Profession 
cerfaFields['lieuEtablissement']     = $ds015PE1.profession.profession0.lieuEtablissement;
cerfaFields['mentionsParticulieres'] = $ds015PE1.profession.profession0.mentionsParticulieres;


//signature
cerfaFields['dateSignature']        = $ds015PE1.signature.signature.dateSignature;
cerfaFields['signature']           	= 'Je déclare sur l’honneur l’exactitude des informations de la formalité et signe la présente déclaration.';
cerfaFields['lieuSignature']        = $ds015PE1.signature.signature.lieuSignature;
cerfaFields['civNomPrenom']			= $ds015PE1.etatCivil.identificationDeclarant.civilite + ' ' + $ds015PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $ds015PE1.etatCivil.identificationDeclarant.prenomDeclarant;



/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $ds015PE1.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $ds015PE1.signature.signature.dateSignature,
		autoriteHabilitee :"Préfecture du département",
		demandeContexte : "Demande de carte professionnelle de guide-conférencier",
		civiliteNomPrenom : civNomPrenom
	});


// finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Formulaire guide-conférencier.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPhoto);
appendPj($attachmentPreprocess.attachmentPreprocess.pjLieuActivite);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificatif);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitreSejour);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Guide-conférencier.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Guide-conférencier - demande de carte professionnelle de guide-conférencier',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de carte professionnelle de guide-conférencier pour l\'exercice de la profession de guide-conférencier.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
