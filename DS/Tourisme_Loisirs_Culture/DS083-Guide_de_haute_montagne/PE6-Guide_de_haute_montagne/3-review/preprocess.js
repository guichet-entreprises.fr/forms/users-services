function pad(s) { return (s < 10) ? '0' + s : s; }

var cerfaFields = {};

var civNomPrenom = $ds083PE6.cadre.cadre1.civilite + ' ' + $ds083PE6.cadre.cadre1.nomDeclarant + ' ' + $ds083PE6.cadre.cadre1.prenomDeclarant;


/// Dossier de déclaration

cerfaFields['nomDeclarant']          = $ds083PE6.cadre.cadre1.nomDeclarant;
cerfaFields['prenomDeclarant']       = $ds083PE6.cadre.cadre1.prenomDeclarant;
cerfaFields['adresseDeclarant']      = $ds083PE6.adresse.adresse.adresseNumeroNomRue + ' ' + ($ds083PE6.adresse.adresse.adressecomplement != null ? $ds083PE6.adresse.adresse.adressecomplement : '') 
										+ ' ' + $ds083PE6.adresse.adresse.adresseCodePostal + ' ' + $ds083PE6.adresse.adresse.adresseVille 
										+ ' ' + ($ds083PE6.adresse.adresse.adressePays != null ? $ds083PE6.adresse.adresse.adressePays : '') ;
cerfaFields['nationaliteDeclarant']  = $ds083PE6.cadre.cadre1.nationaliteDeclarant;
cerfaFields['activiteEncadree']      = $ds083PE6.activiteEncadre.activiteEncadre.activiteEncadree;
cerfaFields['fonctionExercee']       = $ds083PE6.activiteEncadre.activiteEncadre.fonctionExercee;
cerfaFields['lieuExerciceEnvisage']  = $ds083PE6.activiteEncadre.activiteEncadre.lieuExerciceEnvisage;
cerfaFields['etablissementExercice'] = $ds083PE6.activiteEncadre.activiteEncadre.etablissementExercice;
cerfaFields['signatureCoche']        = $ds083PE6.signature.signature.signatureCoche;
cerfaFields['lieuSignature']         = $ds083PE6.signature.signature.lieuSignature;
cerfaFields['dateSignature']         = $ds083PE6.signature.signature.dateSignature;
cerfaFields['nomPrenom']             = $ds083PE6.cadre.cadre1.nomDeclarant + ' ' + $ds083PE6.cadre.cadre1.prenomDeclarant;


//Signature




/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
 
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp085PE3.signature.signature.lieuSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		dateSignature: $ds083PE6.signature.signature.dateSignature,
		autoriteHabilitee :"Préfet du département de l'Isère",
		demandeContexte : "Déclaration préalable d’activité pour les ressortissants européens exerçant une activité permanente",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
    .load('models/Formulaires_déclaration_activité_permanente.pdf') //
    .apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
 
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertificatMedical);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPhoto);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExperienceProfessionnelle);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDetailDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationEquivalence);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCasier);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Guide_Haute_Montagne_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
   label : 'Guide de haute montagne - Déclaration préalable d’activité pour les ressortissants européens exerçant une activité permanente',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable d’activité pour les ressortissants européens exerçant une activité permanente',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});