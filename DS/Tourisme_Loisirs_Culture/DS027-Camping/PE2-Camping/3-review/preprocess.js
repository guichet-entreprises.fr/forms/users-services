var formFields = {};

if ($ds027PE2.etatCivil.identificationDeclarant.personnePhysiqueMorale == 'un particulier') {
var civNomPrenom = $ds027PE2.etatCivil.identificationDeclarant.personnePhysiqueGroup.civilite + " " + $ds027PE2.etatCivil.identificationDeclarant.personnePhysiqueGroup.nomDeclarant + " " + $ds027PE2.etatCivil.identificationDeclarant.personnePhysiqueGroup.prenomDeclarant;
}
else {
var civNomPrenom = $ds027PE2.etatCivil.identificationDeclarant.personneMoraleGroup.representantLegal.civiliteRL + " " + $ds027PE2.etatCivil.identificationDeclarant.personneMoraleGroup.representantLegal.nomRL + " " + $ds027PE2.etatCivil.identificationDeclarant.personneMoraleGroup.representantLegal.prenomRL;
}

//Cadre 1 
//PP
formFields['cadre1PPMadame']                           = Value('id').of($ds027PE2.etatCivil.identificationDeclarant.personnePhysiqueGroup.civilite).eq('madame') ? true : false;
formFields['cadre1PPMonsieur']                         = Value('id').of($ds027PE2.etatCivil.identificationDeclarant.personnePhysiqueGroup.civilite).eq('monsieur') ? true : false;
formFields['cadre1PPNom']                              = $ds027PE2.etatCivil.identificationDeclarant.personnePhysiqueGroup.nomDeclarant;
formFields['cadre1PPPrenom']                           = $ds027PE2.etatCivil.identificationDeclarant.personnePhysiqueGroup.prenomDeclarant;
formFields['cadre1Date']                               = $ds027PE2.etatCivil.identificationDeclarant.personnePhysiqueGroup.dateNaissanceDeclarant;
formFields['cadre1Commune']                            = $ds027PE2.etatCivil.identificationDeclarant.personnePhysiqueGroup.lieuNaissanceDeclarant;
formFields['cadre1Departement']                        = $ds027PE2.etatCivil.identificationDeclarant.personnePhysiqueGroup.departementDeclarant;
formFields['cadre1Pays']                               = $ds027PE2.etatCivil.identificationDeclarant.personnePhysiqueGroup.paysNaissanceDeclarant;

//PM
formFields['cadre1Denomination']                       = $ds027PE2.etatCivil.identificationDeclarant.personneMoraleGroup.denominationDeclarant;
formFields['cadre1RaisonSociale']                      = $ds027PE2.etatCivil.identificationDeclarant.personneMoraleGroup.raisonSocialeDeclarant;
formFields['cadre1SIRET']                              = $ds027PE2.etatCivil.identificationDeclarant.personneMoraleGroup.siretDeclarant;
formFields['cadre1TypeSociete']                        = $ds027PE2.etatCivil.identificationDeclarant.personneMoraleGroup.formeJuridiqueDeclarant;

formFields['cadre1PMMadame']                           = Value('id').of($ds027PE2.etatCivil.identificationDeclarant.personneMoraleGroup.representantLegal.civiliteRL).eq('madame') ? true : false;
formFields['cadre1PMMonsieur']                         = Value('id').of($ds027PE2.etatCivil.identificationDeclarant.personneMoraleGroup.representantLegal.civiliteRL).eq('monsieur') ? true : false;
formFields['cadre1PMNom']                              = $ds027PE2.etatCivil.identificationDeclarant.personneMoraleGroup.representantLegal.nomRL;
formFields['cadre1PMPrenom']                           = $ds027PE2.etatCivil.identificationDeclarant.personneMoraleGroup.representantLegal.prenomRL;

//Cadre 2
formFields['cadre2Numero']                             = $ds027PE2.adresse.adresseContact.numeroAdresseDeclarant;
formFields['cadre2Voie']                               = ($ds027PE2.adresse.adresseContact.libelleAdresseDeclarant ? $ds027PE2.adresse.adresseContact.libelleAdresseDeclarant : '') + ' '
															+ ($ds027PE2.adresse.adresseContact.complementAdresseDeclarant ? $ds027PE2.adresse.adresseContact.complementAdresseDeclarant : '')  ;
formFields['cadre2LieuDit']                            = $ds027PE2.adresse.adresseContact.lieuAdresseDeclarant;
formFields['cadre2Localite']                           = $ds027PE2.adresse.adresseContact.villeAdresseDeclarant;
formFields['cadre2CP']                                 = $ds027PE2.adresse.adresseContact.codePostalAdresseDeclarant;
formFields['cadre2BP']                                 = $ds027PE2.adresse.adresseContact.boitePostalAdresseDeclarant;
formFields['cadre2Cedex']                              = $ds027PE2.adresse.adresseContact.cedexAdresseDeclarant;
formFields['cadre2Telephone']                          = $ds027PE2.adresse.adresseContact.telephoneAdresseDeclarant;
formFields['cadre2TelephoneIndicatif']                 = ''; /* $ds027PE2.adresse.adresseContact.Cadre2Telephone; */
formFields['cadre2PaysEtranger']                       = $ds027PE2.adresse.adresseContact.paysEtrangerAdresseDeclarant;
formFields['cadre2Division']                           = $ds027PE2.adresse.adresseContact.divEtrangerAdresseDeclarant;

//Si courrier administration

formFields['cadre2CourriersMadame']                    = Value('id').of($ds027PE2.adresse.adresseContact.adresseContact2.civiliteContact).eq('madame');
formFields['cadre2CourriersMonsieur']                  = Value('id').of($ds027PE2.adresse.adresseContact.adresseContact2.civiliteContact).eq('monsieur');
formFields['cadre2CourriersPM']                        = Value('id').of($ds027PE2.adresse.adresseContact.adresseContact2.personnePhysiqueMoraleContact).eq('personneMoraleContact');
formFields['cadre2CourriersNom']                       = $ds027PE2.adresse.adresseContact.adresseContact2.nomContact;
formFields['cadre2CourriersPrenom']                    = $ds027PE2.adresse.adresseContact.adresseContact2.prenomContact;
formFields['cadre2CourriersRaisonSociale']             = $ds027PE2.adresse.adresseContact.adresseContact2.raisonSocialeContact;
formFields['cadre2CourriersNumero']                    = $ds027PE2.adresse.adresseContact.adresseContact2.numeroAdresse2;
formFields['cadre2CourriersVoie']                      = ($ds027PE2.adresse.adresseContact.adresseContact2.libelleAdresse2 ? $ds027PE2.adresse.adresseContact.adresseContact2.libelleAdresse2 : '')
															+' '+ ($ds027PE2.adresse.adresseContact.adresseContact2.complementAdresse2 ? $ds027PE2.adresse.adresseContact.adresseContact2.complementAdresse2 :'');
formFields['cadre2CourriersLieuDit']                   = $ds027PE2.adresse.adresseContact.adresseContact2.lieuAdresse2;
formFields['cadre2CourriersLocalite']                  = $ds027PE2.adresse.adresseContact.adresseContact2.villeAdresse2;
formFields['cadre2CourriersCP']                        = $ds027PE2.adresse.adresseContact.adresseContact2.codePostalAdresse2;
formFields['cadre2CourriersBP']                        = $ds027PE2.adresse.adresseContact.adresseContact2.boitePostalAdresse2;
formFields['cadre2CourriersCedex']                     = $ds027PE2.adresse.adresseContact.adresseContact2.cedexAdresse2;
formFields['cadre2CourriersPaysEtranger']              = $ds027PE2.adresse.adresseContact.adresseContact2.paysEtrangerAdresse2;
formFields['cadre2CourriersDivision']                  = $ds027PE2.adresse.adresseContact.adresseContact2.divEtrangerAdresse2;
formFields['cadre2CourriersTelephone']                 = $ds027PE2.adresse.adresseContact.adresseContact2.telephoneAdresse2;
formFields['cadre2CourriersIndicatif']                 = '' ; /* $ds027PE2.adresse.adresseContact.adresseContact2.Cadre2CourriersIndicatif; */



//Cadre 3
formFields['cadre3Numero']                             = $ds027PE2.terrainGroup.terrain.terrainAdresse.numeroAdresseTerrain;
formFields['cadre3Voie']                               = ($ds027PE2.terrainGroup.terrain.terrainAdresse.libelleAdresseTerrain ? $ds027PE2.terrainGroup.terrain.terrainAdresse.libelleAdresseTerrain : '') +' '
															+ ($ds027PE2.terrainGroup.terrain.terrainAdresse.complementAdresseTerrain ? $ds027PE2.terrainGroup.terrain.terrainAdresse.complementAdresseTerrain :'');
formFields['cadre3LieuDit']                            = $ds027PE2.terrainGroup.terrain.terrainAdresse.lieuAdresseTerrain;
formFields['cadre3Localite']                           = ($ds027PE2.terrainGroup.terrain.terrainAdresse.villeAdresseTerrain ? $ds027PE2.terrainGroup.terrain.terrainAdresse.villeAdresseTerrain : '')
															+ ( $ds027PE2.terrainGroup.terrain.terrainAdresse.paysAdresseTerrain ? $ds027PE2.terrainGroup.terrain.terrainAdresse.paysAdresseTerrain : '') ;
formFields['cadre3CP']                                 = $ds027PE2.terrainGroup.terrain.terrainAdresse.codePostalAdresseTerrain;
formFields['cadre3BP']                                 = $ds027PE2.terrainGroup.terrain.terrainAdresse.boitePostalAdresseTerrain;
formFields['cadre3Cedex']                              = $ds027PE2.terrainGroup.terrain.terrainAdresse.cedexAdresseTerrain;

formFields['cadre3Prefixe']                            = $ds027PE2.terrainGroup.terrain.terrainRefCadastrales.terrainPrefixe;
formFields['cadre3Sexion']                             = $ds027PE2.terrainGroup.terrain.terrainRefCadastrales.terrainSection;
formFields['cadre3NumeroCadastre']                     = $ds027PE2.terrainGroup.terrain.terrainRefCadastrales.terrainNumero;
formFields['cadre3Superficie']                         = $ds027PE2.terrainGroup.terrain.terrainRefCadastrales.terrainSuperficie;

//Références cadastrales : fiche complémentaire

//remplissage à vide pour le visuel du formulaire final
for (var i= $ds027PE2.terrainGroup.terrain.terrainRefCadastrales.refCadastrales.size(); i < 20; i++ ){
	formFields['cadastreSuperficie'+i]                      =   '';
	formFields['cadastreNumero'+i]                          =   '';
	formFields['cadastreSection'+i]                         =   '';
	formFields['cadastrePrefixe'+i]                         =   '';
}

//Remplissage cardinalité
for (var i= 0; i < $ds027PE2.terrainGroup.terrain.terrainRefCadastrales.refCadastrales.size(); i++ ){
	formFields['cadastreSuperficie'+i]                      = $ds027PE2.terrainGroup.terrain.terrainRefCadastrales.refCadastrales[i].terrainSuperficie ;
	formFields['cadastreNumero'+i]                          = $ds027PE2.terrainGroup.terrain.terrainRefCadastrales.refCadastrales[i].terrainNumero;
	formFields['cadastreSection'+i]                         = $ds027PE2.terrainGroup.terrain.terrainRefCadastrales.refCadastrales[i].terrainSection;
	formFields['cadastrePrefixe'+i]                         = $ds027PE2.terrainGroup.terrain.terrainRefCadastrales.refCadastrales[i].terrainPrefixe;
}

formFields['cadastreSuperficieTotale']                 = $ds027PE2.terrainGroup.terrain.terrainRefCadastrales.terrainRefCadastralesTotal;

formFields['cadre32CertificatUrbanismeOUI']            = Value('id').of($ds027PE2.terrainGroup2.terrain2.cas1).eq('oui1') ? true : false;
formFields['cadre32CertificatUrbanismeNON']            = Value('id').of($ds027PE2.terrainGroup2.terrain2.cas1).eq('non1') ? true : false;
formFields['cadre32CertificatUrbanismeJSP']            = Value('id').of($ds027PE2.terrainGroup2.terrain2.cas1).eq('na1') ? true : false;

formFields['cadre32LotissementOUI']                    = Value('id').of($ds027PE2.terrainGroup2.terrain2.cas2).eq('oui2') ? true : false;
formFields['cadre32LotissementNON']                    = Value('id').of($ds027PE2.terrainGroup2.terrain2.cas2).eq('non2') ? true : false;
formFields['cadre32LotissementJSP']                    = Value('id').of($ds027PE2.terrainGroup2.terrain2.cas2).eq('na2') ? true : false;

formFields['cadre32ZACOUI']                            = Value('id').of($ds027PE2.terrainGroup2.terrain2.cas3).eq('oui3') ? true : false;
formFields['cadre32ZACNON']                            = Value('id').of($ds027PE2.terrainGroup2.terrain2.cas3).eq('non3') ? true : false;
formFields['cadre32ZACJSP']                            = Value('id').of($ds027PE2.terrainGroup2.terrain2.cas3).eq('na3') ? true : false;

formFields['cadre32AFUOUI']                            = Value('id').of($ds027PE2.terrainGroup2.terrain2.cas4).eq('oui4') ? true : false;
formFields['cadre32AFUNON']                            = Value('id').of($ds027PE2.terrainGroup2.terrain2.cas4).eq('non4') ? true : false;
formFields['cadre32AFUJSP']                            = Value('id').of($ds027PE2.terrainGroup2.terrain2.cas4).eq('na4') ? true : false;


formFields['cadre32PUPOUI']                            = Value('id').of($ds027PE2.terrainGroup2.terrain2.cas5).eq('oui5') ? true : false;
formFields['cadre32PUPNON']                            = Value('id').of($ds027PE2.terrainGroup2.terrain2.cas5).eq('non5') ? true : false;
formFields['cadre32PUPJSP']                            = Value('id').of($ds027PE2.terrainGroup2.terrain2.cas5).eq('na5') ? true : false;


formFields['cadre32OINOUI']                            = Value('id').of($ds027PE2.terrainGroup2.terrain2.cas6).eq('oui6') ? true : false;
formFields['cadre32OINNON']                            = Value('id').of($ds027PE2.terrainGroup2.terrain2.cas6).eq('non6') ? true : false;
formFields['cadre32OINJSP']                            = Value('id').of($ds027PE2.terrainGroup2.terrain2.cas6).eq('na6') ? true : false;

formFields['cadre32Precision']                         = $ds027PE2.terrainGroup2.terrain2.detailsCas;




//Cadre 4.1 
formFields['cadre41Lotissement']                       = $ds027PE2.projetAmenagementGroup.projetAmenagement.toutSecteur.travaux1			? true : false ;
formFields['cadre41Remembrement']                      = $ds027PE2.projetAmenagementGroup.projetAmenagement.toutSecteur.travaux2			? true : false ;
formFields['cadre41Camping']                           = $ds027PE2.projetAmenagementGroup.projetAmenagement.toutSecteur.travaux3				? true : false ;
formFields['cadre41Parc']                              = $ds027PE2.projetAmenagementGroup.projetAmenagement.toutSecteur.travaux4					? true : false ;		
formFields['cadre41AmenagementSports']                 = $ds027PE2.projetAmenagementGroup.projetAmenagement.toutSecteur.travaux5	? true : false ;
formFields['cadre41AmenagementParc']                   = $ds027PE2.projetAmenagementGroup.projetAmenagement.toutSecteur.travaux6		? true : false ;
formFields['cadre41AmenagementGolf']                   = $ds027PE2.projetAmenagementGroup.projetAmenagement.toutSecteur.travaux7		? true : false ;
formFields['cadre41Aires']                             = $ds027PE2.projetAmenagementGroup.projetAmenagement.toutSecteur.travaux8				? true : false ;
formFields['cadre41AiresContenance']                   = $ds027PE2.projetAmenagementGroup.projetAmenagement.toutSecteur.detailsTravaux8 ;
formFields['cadre41Travaux']                           = $ds027PE2.projetAmenagementGroup.projetAmenagement.toutSecteur.travaux9				? true : false ;
formFields['cadre41TravauxSuperficie']                 = $ds027PE2.projetAmenagementGroup.projetAmenagement.toutSecteur.detailsTravaux91 ;
formFields['cadre41TravauxProfondeur']                 = $ds027PE2.projetAmenagementGroup.projetAmenagement.toutSecteur.detailsTravaux92 ;
formFields['cadre41TravauxHauteur']                    = $ds027PE2.projetAmenagementGroup.projetAmenagement.toutSecteur.detailsTravaux93	 ;
formFields['cadre41AmenagementTerrain']                = $ds027PE2.projetAmenagementGroup.projetAmenagement.toutSecteur.travaux10	? true : false ;
formFields['cadre41AmenagementAire']                   = $ds027PE2.projetAmenagementGroup.projetAmenagement.toutSecteur.travaux11		? true : false ;
                                                                  
formFields['cadre41Chemin']                            = $ds027PE2.projetAmenagementGroup.projetAmenagement.secteurProteges.travaux12					? true : false;
formFields['cadre41AmenagementNecessaire']             = $ds027PE2.projetAmenagementGroup.projetAmenagement.secteurProteges.travaux13	? true : false;
formFields['cadre41CreationVoie']                      = $ds027PE2.projetAmenagementGroup.projetAmenagement.secteurProteges.travaux15				? true : false;
formFields['cadre41TravauxModification']               = $ds027PE2.projetAmenagementGroup.projetAmenagement.secteurProteges.travaux16		? true : false;
formFields['cadre41CreationEspace']                    = $ds027PE2.projetAmenagementGroup.projetAmenagement.secteurProteges.travaux17			? true : false;

formFields['cadre41CreationEspacePublic']              = $ds027PE2.projetAmenagementGroup.projetAmenagement.secteurProteges.travaux19		? true : false;
                                                                   

formFields['cadre41Description']                       = $ds027PE2.projetAmenagementGroup2.descriptionTravauxGroup.descriptionTravaux;
formFields['cadre41Superficie']                        = $ds027PE2.projetAmenagementGroup2.descriptionTravauxGroup.superficieTerrain;
formFields['cadre41TravauxTranches']                   = $ds027PE2.projetAmenagementGroup2.descriptionTravauxGroup.jalonsTravaux;

//Cadre 4.2
formFields['cadre42NBMaxLots']                         = $ds027PE2.projetAmenagementGroup2.projetLotissementGroup.lotissementNbLots;
formFields['cadre42SurfacePlancher']                   = $ds027PE2.projetAmenagementGroup2.projetLotissementGroup.lotissementSurfacePlancher;
formFields['cadre42Application']                       = Value('id').of($ds027PE2.projetAmenagementGroup2.projetLotissementGroup.lotissementConstructibilite).eq('constructibilite1') ? true : false;
formFields['cadre42Conformement']                      = Value('id').of($ds027PE2.projetAmenagementGroup2.projetLotissementGroup.lotissementConstructibilite).eq('constructibilite2') ? true : false;
formFields['cadre42Constructibilite']                  = Value('id').of($ds027PE2.projetAmenagementGroup2.projetLotissementGroup.lotissementConstructibilite).eq('constructibilite3') ? true : false;
formFields['cadre42TravauxOUI']                        = $ds027PE2.projetAmenagementGroup2.projetLotissementGroup.lotissementDemandeDifferes	? true : false;
formFields['cadre42TravauxNON']                        = $ds027PE2.projetAmenagementGroup2.projetLotissementGroup.lotissementDemandeDifferes	? false : true;
formFields['cadre42Consignation']                      = Value('id').of($ds027PE2.projetAmenagementGroup2.projetLotissementGroup.lotissementGarantie).eq('lotissementGarantie1') ? true : false;
formFields['cadre42Garantie']                          = Value('id').of($ds027PE2.projetAmenagementGroup2.projetLotissementGroup.lotissementGarantie).eq('lotissementGarantie2') ? true : false;
formFields['cadre42VenteOUI']                          = $ds027PE2.projetAmenagementGroup2.projetLotissementGroup.lotissementVenteLocation		? true : false;
formFields['cadre42VenteNON']                          = $ds027PE2.projetAmenagementGroup2.projetLotissementGroup.lotissementVenteLocation		? false : true;

//Cadre 4.3
formFields['cadre43NBMaxEmplacements']                 = $ds027PE2.projetAmenagementGroup2.projetCampingGroup.campingNbEmplacements;
formFields['cadre43NBMaxPersonnes']                    = $ds027PE2.projetAmenagementGroup2.projetCampingGroup.campingNbPersonnes;
formFields['cadre43NBEmplacementsHLL']                 = $ds027PE2.projetAmenagementGroup2.projetCampingGroup.campingHLL.campingHLLNbEmplacements;
formFields['cadre43SurfaceHLL']                        = $ds027PE2.projetAmenagementGroup2.projetCampingGroup.campingHLL.campingHLLSurfacePlancher;
formFields['cadre43TerrainSaisonnier']                 = $ds027PE2.projetAmenagementGroup2.projetCampingGroup.campingHLL.campingHLLSaisonnier;
formFields['cadre43AgrandissementOUI']                 = $ds027PE2.projetAmenagementGroup2.projetCampingGroup.campingHLL.campingHLLAgrandissement	? true : false;
formFields['cadre43AgrandissementNON']                 = $ds027PE2.projetAmenagementGroup2.projetCampingGroup.campingHLL.campingHLLAgrandissement	? false : true;

//Cadre 5.1
formFields['cadre51ArchitecteOUI']                     = $ds027PE2.projetConstructionGroup.projetConstruction.architecte	? true : false;
formFields['cadre51ArchitecteNON']                     = $ds027PE2.projetConstructionGroup.projetConstruction.architecte	? false : true;
formFields['cadre51ArchitecteNom']                     = $ds027PE2.projetConstructionGroup.projetConstruction.architecteNom;
formFields['cadre51ArchitectePrenom']                  = $ds027PE2.projetConstructionGroup.projetConstruction.architectePrenom;
formFields['cadre51ArchitecteNumero']                  = $ds027PE2.projetConstructionGroup.projetConstruction.architecteAdresse.numeroAdresseArchitecte;
formFields['cadre51ArchitecteVoie']                    = ($ds027PE2.projetConstructionGroup.projetConstruction.architecteAdresse.libelleAdresseArchitecte ? $ds027PE2.projetConstructionGroup.projetConstruction.architecteAdresse.libelleAdresseArchitecte : '') + ' '
															+ ($ds027PE2.projetConstructionGroup.projetConstruction.architecteAdresse.complementAdresseArchitecte ? $ds027PE2.projetConstructionGroup.projetConstruction.architecteAdresse.complementAdresseArchitecte :'');
formFields['cadre51ArchitecteLieuDit']                 = $ds027PE2.projetConstructionGroup.projetConstruction.architecteAdresse.lieuAdresseArchitecte;
formFields['cadre51ArchitecteLocalite']                = $ds027PE2.projetConstructionGroup.projetConstruction.architecteAdresse.villeAdresseArchitecte;
formFields['cadre51ArchitecteCP']                      = $ds027PE2.projetConstructionGroup.projetConstruction.architecteAdresse.codePostalAdresseArchitecte;
formFields['cadre51ArchitecteBP']                      = $ds027PE2.projetConstructionGroup.projetConstruction.architecteAdresse.boitePostalAdresseArchitecte;
formFields['cadre51ArchitecteCedex']                   = $ds027PE2.projetConstructionGroup.projetConstruction.architecteAdresse.cedexAdresseArchitecte;
formFields['cadre51ArchitecteNumeroInscription']       = $ds027PE2.projetConstructionGroup.projetConstruction.architecteInscription;
formFields['cadre51ArchitecteConseilRegion']           = $ds027PE2.projetConstructionGroup.projetConstruction.architecteConseilRegional;
formFields['cadre51ArchitecteTelephone']               = $ds027PE2.projetConstructionGroup.projetConstruction.architecteAdresse.telephoneArchitecte;
formFields['cadre51ArchitecteTelecopie']               = $ds027PE2.projetConstructionGroup.projetConstruction.architecteAdresse.telecopieArchitecte;
formFields['cadre51ArchitecteEmail']                   = $ds027PE2.projetConstructionGroup.projetConstruction.architecteAdresse.mailArchitecte;
formFields['cadre51ArchitecteEmailBis']                = ''; /* $ds027PE2.projetConstructionGroup.projetConstruction.architecteAdresse.Cadre51ArchitecteEmailBis */;

formFields['cadre51ArchitecteSignature']               = ''; //SIGNATURE 
formFields['cadre51ArchitecteCachet']                  =  ''; //SIGNATURE

formFields['cadre51ArchitecteAucun']                   = $ds027PE2.projetConstructionGroup.projetConstruction.aucunArchitecte;

//Cadre 5.2
formFields['cadre52NouvelleConstruction']              = Value('id').of($ds027PE2.projetConstructionGroup.natureProjet.natureConstruction).eq('nouvelleConstruction');
formFields['cadre52Travaux']                           = Value('id').of($ds027PE2.projetConstructionGroup.natureProjet.natureConstruction).eq('existantConstruction');
formFields['cadre52Terrain']                           = $ds027PE2.projetConstructionGroup.natureProjet.terrainDivise			? true : false;
formFields['cadre52Description']                       = $ds027PE2.projetConstructionGroup.natureProjet.descriptionProjet;
formFields['cadre52PuissanceElectrique']               = $ds027PE2.projetConstructionGroup.natureProjet.projetPuissanceElectrique;

//Cadre 5.3
formFields['cadre53NBLogement']                        = $ds027PE2.projetConstructionGroup2.infosComplementaires.nbLogements;
formFields['cadre53NBIndividuel']                      = $ds027PE2.projetConstructionGroup2.infosComplementaires.nbLogementsIndividuel;
formFields['cadre53NBCollectif']                       = $ds027PE2.projetConstructionGroup2.infosComplementaires.nbLogementsCollectif;
formFields['cadre53LocatifSocial']                     = $ds027PE2.projetConstructionGroup2.infosComplementaires.logementsRepartition.nbLogementsLocatifSocial;
formFields['cadre53AccessionSociale']                  = $ds027PE2.projetConstructionGroup2.infosComplementaires.logementsRepartition.nbLogementsAccessionSociale;
formFields['cadre53PretTauxZ']                         = $ds027PE2.projetConstructionGroup2.infosComplementaires.logementsRepartition.nbLogementsPretTauxZero;
formFields['cadre53AutreFinancement']                  = $ds027PE2.projetConstructionGroup2.infosComplementaires.logementsRepartition.nbLogementsAutresFinancements ? true : false;
formFields['cadre53AutreFinancementPrecision']         = $ds027PE2.projetConstructionGroup2.infosComplementaires.logementsRepartition.nbLogementsAutresFinancements;

formFields['cadre53Occupation']                        = $ds027PE2.projetConstructionGroup2.infosComplementaires.logementUtilisation.utilisationOccupationPersonnelle;
formFields['cadre53Vente']                             = $ds027PE2.projetConstructionGroup2.infosComplementaires.logementUtilisation.utilisationVente;
formFields['cadre53Location']                          = $ds027PE2.projetConstructionGroup2.infosComplementaires.logementUtilisation.utilisationLocation;
formFields['cadre53ResidencePrincipale']               = Value('id').of($ds027PE2.projetConstructionGroup2.infosComplementaires.logementUtilisation.utilisationResidencePrincipaleSecondaire).eq('utilisationResidencePrincipale') ? true : false ;
formFields['cadre53ResidenceSecondaire']               = Value('id').of($ds027PE2.projetConstructionGroup2.infosComplementaires.logementUtilisation.utilisationResidencePrincipaleSecondaire).eq('utilisationResidenceSecondaire') ? true : false ;
formFields['cadre53Piscine']                           = Value('id').of($ds027PE2.projetConstructionGroup2.infosComplementaires.logementUtilisation.utilisationAnnexe).contains('piscine') ? true : false ;
formFields['cadre53Garage']                            = Value('id').of($ds027PE2.projetConstructionGroup2.infosComplementaires.logementUtilisation.utilisationAnnexe).contains('garage') ? true : false ;
formFields['cadre53Veranda']                           = Value('id').of($ds027PE2.projetConstructionGroup2.infosComplementaires.logementUtilisation.utilisationAnnexe).contains('veranda') ? true : false ;
formFields['cadre53AbriJardin']                        = Value('id').of($ds027PE2.projetConstructionGroup2.infosComplementaires.logementUtilisation.utilisationAnnexe).contains('abriJardin') ? true : false ;
formFields['cadre53AutreAnnexe']                       = Value('id').of($ds027PE2.projetConstructionGroup2.infosComplementaires.logementUtilisation.utilisationAnnexe).contains('autreAnnexe') ? true : false ;
formFields['cadre53AutreAnnexePrecision']              = $ds027PE2.projetConstructionGroup2.infosComplementaires.logementUtilisation.autresAnnexes;

formFields['cadre53PersonnesAgees']                    = Value('id').of($ds027PE2.projetConstructionGroup2.infosComplementaires.utilisationResidenceType).contains('personnesAgees') ? true : false;
formFields['cadre53Etudiants']                         = Value('id').of($ds027PE2.projetConstructionGroup2.infosComplementaires.utilisationResidenceType).contains('etudiants') ? true : false;
formFields['cadre53Tourisme']                          = Value('id').of($ds027PE2.projetConstructionGroup2.infosComplementaires.utilisationResidenceType).contains('tourisme') ? true : false;
formFields['cadre53Hotel']                             = Value('id').of($ds027PE2.projetConstructionGroup2.infosComplementaires.utilisationResidenceType).contains('hotelier') ? true : false;
formFields['cadre53Sociale']                           = Value('id').of($ds027PE2.projetConstructionGroup2.infosComplementaires.utilisationResidenceType).contains('social') ? true : false;
formFields['cadre53Handicap']                          = Value('id').of($ds027PE2.projetConstructionGroup2.infosComplementaires.utilisationResidenceType).contains('handicap') ? true : false;
formFields['cadre53ResidenceAutre']                    = Value('id').of($ds027PE2.projetConstructionGroup2.infosComplementaires.utilisationResidenceType).contains('autre') ? true : false;
formFields['cadre53ResidenceAutrePrecision']           = $ds027PE2.projetConstructionGroup2.infosComplementaires.autresResidence;

formFields['cadre53ResidenceAutreNB']                  = $ds027PE2.projetConstructionGroup2.infosComplementaires.chambreFoyer;
formFields['cadre53Repartition1piece']                 = $ds027PE2.projetConstructionGroup2.infosComplementaires.repartitionLogementPieces.p1;
formFields['cadre53Repartition2pieces']                = $ds027PE2.projetConstructionGroup2.infosComplementaires.repartitionLogementPieces.p2;
formFields['cadre53Repartition3pieces']                = $ds027PE2.projetConstructionGroup2.infosComplementaires.repartitionLogementPieces.p3;
formFields['cadre53Repartition4pieces']                = $ds027PE2.projetConstructionGroup2.infosComplementaires.repartitionLogementPieces.p4;
formFields['cadre53Repartition5pieces']                = $ds027PE2.projetConstructionGroup2.infosComplementaires.repartitionLogementPieces.p5;
formFields['cadre53Repartition6pieces']                = $ds027PE2.projetConstructionGroup2.infosComplementaires.repartitionLogementPieces.p6;
formFields['cadre53NBNiveau']                          = $ds027PE2.projetConstructionGroup2.infosComplementaires.niveauxNb;
formFields['cadre53Extension']                         = Value('id').of($ds027PE2.projetConstructionGroup2.infosComplementaires.travauxTypes).contains('extension') ? true : false;
formFields['cadre53Surelevation']                      = Value('id').of($ds027PE2.projetConstructionGroup2.infosComplementaires.travauxTypes).contains('Surélévation') ? true : false;
formFields['cadre53Creation']                          = Value('id').of($ds027PE2.projetConstructionGroup2.infosComplementaires.travauxTypes).contains('creationNiveauxSup') ? true : false;
formFields['cadre53Transport']                         = Value('id').of($ds027PE2.projetConstructionGroup2.infosComplementaires.constructionsFutures).contains('transport') ? true : false ;
formFields['cadre53Enseignement']                      = Value('id').of($ds027PE2.projetConstructionGroup2.infosComplementaires.constructionsFutures).contains('enseignement') ? true : false ;
formFields['cadre53Action']                            = Value('id').of($ds027PE2.projetConstructionGroup2.infosComplementaires.constructionsFutures).contains('actionSociale') ? true : false ;
formFields['cadre53Ouvrage']                           = Value('id').of($ds027PE2.projetConstructionGroup2.infosComplementaires.constructionsFutures).contains('ouvrageSpecial') ? true : false ;
formFields['cadre53Sante']                             = Value('id').of($ds027PE2.projetConstructionGroup2.infosComplementaires.constructionsFutures).contains('sante') ? true : false ;
formFields['cadre53Culture']                           = Value('id').of($ds027PE2.projetConstructionGroup2.infosComplementaires.constructionsFutures).contains('cultureLoisir') ? true : false ;

//Cadre 5.4
formFields['cadre54Periode']                           = $ds027PE2.projetConstructionGroup2.infosComplementaires.constructionPeriodique.periodeAnnee;

//Cadre 5.5
formFields['cadre55HabitationA']                       = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationHabitation.a;
formFields['cadre55HabitationB']                       = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationHabitation.b;
formFields['cadre55HabitationC']                       = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationHabitation.c;
formFields['cadre55HabitationD']                       = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationHabitation.d;
formFields['cadre55HabitationE']                       = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationHabitation.e;
formFields['cadre55HabitationABCDE']                   = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationHabitation.f;
                                                                   
formFields['cadre55HebergementA']                      = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationHebergementHotelier.a;
formFields['cadre55HebergementB']                      = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationHebergementHotelier.b;
formFields['cadre55HebergementC']                      = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationHebergementHotelier.c;
formFields['cadre55HebergementD']                      = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationHebergementHotelier.d;
formFields['cadre55HebergementE']                      = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationHebergementHotelier.e;
formFields['cadre55HebergementABCDE']                  = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationHebergementHotelier.f;
                                                                   
formFields['cadre55BureauA']                           = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationBureaux.a;
formFields['cadre55BureauB']                           = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationBureaux.b;
formFields['cadre55BureauC']                           = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationBureaux.c;
formFields['cadre55BureauD']                           = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationBureaux.d;
formFields['cadre55BureauE']                           = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationBureaux.e;
formFields['cadre55BureauABCDE']                       = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationBureaux.f;
                                                                
formFields['cadre55CommerceA']                         = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationCommerce.a;
formFields['cadre55CommerceB']                         = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationCommerce.b;
formFields['cadre55CommerceC']                         = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationCommerce.c;
formFields['cadre55CommerceD']                         = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationCommerce.d;
formFields['cadre55CommerceE']                         = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationCommerce.e;
formFields['cadre55CommerceABCDE']                     = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationCommerce.f;
                                                                  
formFields['cadre55ArtisanatA']                        = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationArtisanat.a;
formFields['cadre55ArtisanatB']                        = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationArtisanat.b;
formFields['cadre55ArtisanatC']                        = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationArtisanat.c;
formFields['cadre55ArtisanatD']                        = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationArtisanat.d;
formFields['cadre55ArtisanatE']                        = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationArtisanat.e;
formFields['cadre55ArtisanatABCDE']                    = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationArtisanat.f;
                                                              
formFields['cadre55IndustrieA']                        = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationIndustrie.a;
formFields['cadre55IndustrieB']                        = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationIndustrie.b;
formFields['cadre55IndustrieC']                        = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationIndustrie.c;
formFields['cadre55IndustrieD']                        = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationIndustrie.d;
formFields['cadre55IndustrieE']                        = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationIndustrie.e;
formFields['cadre55IndustrieABCDE']                    = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationIndustrie.f;
                                                                
formFields['cadre55ExploitationA']                     = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationExploitationAgricoleForestiere.a;
formFields['cadre55ExploitationB']                     = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationExploitationAgricoleForestiere.b;
formFields['cadre55ExploitationC']                     = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationExploitationAgricoleForestiere.c;
formFields['cadre55ExploitationD']                     = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationExploitationAgricoleForestiere.d;
formFields['cadre55ExploitationE']                     = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationExploitationAgricoleForestiere.e;
formFields['cadre55ExploitationABCDE']                 = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationExploitationAgricoleForestiere.f;
                                                                
formFields['cadre55EntrepotA']                         = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationEntrepot.a;
formFields['cadre55EntrepotB']                         = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationEntrepot.b;
formFields['cadre55EntrepotC']                         = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationEntrepot.c;
formFields['cadre55EntrepotD']                         = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationEntrepot.d;
formFields['cadre55EntrepotE']                         = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationEntrepot.e;
formFields['cadre55EntrepotABCDE']                     = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationEntrepot.f;
                                                   
formFields['cadre55ServicePublicA']                    = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationServicePublic.a;
formFields['cadre55ServicePublicB']                    = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationServicePublic.b;
formFields['cadre55ServicePublicC']                    = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationServicePublic.c;
formFields['cadre55ServicePublicD']                    = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationServicePublic.d;
formFields['cadre55ServicePublicE']                    = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationServicePublic.e;
formFields['cadre55ServicePublicABCDE']                = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationServicePublic.f;
                                                             
formFields['cadre55SurfaceTotaleA']                    = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationSurfacesTotales.a;
formFields['cadre55SurfaceTotaleB']                    = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationSurfacesTotales.b;
formFields['cadre55SurfaceTotaleC']                    = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationSurfacesTotales.c;
formFields['cadre55SurfaceTotaleD']                    = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationSurfacesTotales.d;
formFields['cadre55SurfaceTotaleE']                    = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationSurfacesTotales.e;
formFields['cadre55SurfaceTotaleABCDE']                = $ds027PE2.projetConstructionGroup3.projetConstruction3.destinationSurfacesTotales.f;


//Cadre 5.6
formFields['cadre56AgricoleA']                         = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationExploitAgricoleForestiere.destinationExploitationAgricole.a;
formFields['cadre56AgricoleB']                         = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationExploitAgricoleForestiere.destinationExploitationAgricole.b;
formFields['cadre56AgricoleC']                         = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationExploitAgricoleForestiere.destinationExploitationAgricole.c;
formFields['cadre56AgricoleD']                         = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationExploitAgricoleForestiere.destinationExploitationAgricole.d;
formFields['cadre56AgricoleE']                         = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationExploitAgricoleForestiere.destinationExploitationAgricole.e;
formFields['cadre56AgricoleABCDE']                     = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationExploitAgricoleForestiere.destinationExploitationAgricole.f;

formFields['cadre56ForestiereA']                       = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationExploitAgricoleForestiere.destinationExploitationForestiere.a;
formFields['cadre56ForestiereB']                       = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationExploitAgricoleForestiere.destinationExploitationForestiere.b;
formFields['cadre56ForestiereC']                       = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationExploitAgricoleForestiere.destinationExploitationForestiere.c;
formFields['cadre56ForestiereD']                       = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationExploitAgricoleForestiere.destinationExploitationForestiere.d;
formFields['cadre56ForestiereE']                       = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationExploitAgricoleForestiere.destinationExploitationForestiere.e;
formFields['cadre56ForestiereABCDE']                   = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationExploitAgricoleForestiere.destinationExploitationForestiere.f;

formFields['cadre56LogementA']                         = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationHabitation.destinationHabitationLogement.a;
formFields['cadre56LogementB']                         = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationHabitation.destinationHabitationLogement.b;
formFields['cadre56LogementC']                         = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationHabitation.destinationHabitationLogement.c;
formFields['cadre56LogementD']                         = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationHabitation.destinationHabitationLogement.d;
formFields['cadre56LogementE']                         = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationHabitation.destinationHabitationLogement.e;
formFields['cadre56LogementABCDE']                     = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationHabitation.destinationHabitationLogement.f;

formFields['cadre56HebergementA']                      = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationHabitation.destinationHabitationHebergement.a;
formFields['cadre56HebergementB']                      = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationHabitation.destinationHabitationHebergement.b;
formFields['cadre56HebergementC']                      = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationHabitation.destinationHabitationHebergement.c;
formFields['cadre56HebergementD']                      = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationHabitation.destinationHabitationHebergement.d;
formFields['cadre56HebergementE']                      = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationHabitation.destinationHabitationHebergement.e;
formFields['cadre56HebergementABCDE']                  = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationHabitation.destinationHabitationHebergement.f;

formFields['cadre56ArtisanatA']                        = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceArtisanat.a;
formFields['cadre56ArtisanatB']                        = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceArtisanat.b;
formFields['cadre56ArtisanatC']                        = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceArtisanat.c;
formFields['cadre56ArtisanatD']                        = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceArtisanat.d;
formFields['cadre56ArtisanatE']                        = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceArtisanat.e;
formFields['cadre56ArtisanatABCDE']                    = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceArtisanat.f;

formFields['cadre56RestaurationA']                     = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceRestauration.a;
formFields['cadre56RestaurationB']                     = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceRestauration.b;
formFields['cadre56RestaurationC']                     = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceRestauration.c;
formFields['cadre56RestaurationD']                     = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceRestauration.d;
formFields['cadre56RestaurationE']                     = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceRestauration.e;
formFields['cadre56RestaurationABCDE']                 = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceRestauration.f;

formFields['cadre56CommerceA']                         = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommmerceDeGros.a;
formFields['cadre56CommerceB']                         = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommmerceDeGros.b;
formFields['cadre56CommerceC']                         = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommmerceDeGros.c;
formFields['cadre56CommerceD']                         = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommmerceDeGros.d;
formFields['cadre56CommerceE']                         = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommmerceDeGros.e;
formFields['cadre56CommerceABCDE']                     = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommmerceDeGros.f;

formFields['cadre56ActiviteA']                         = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceActivites.a;
formFields['cadre56ActiviteB']                         = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceActivites.b;
formFields['cadre56ActiviteC']                         = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceActivites.c;
formFields['cadre56ActiviteD']                         = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceActivites.d;
formFields['cadre56ActiviteE']                         = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceActivites.e;
formFields['cadre56ActiviteABCDE']                     = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceActivites.f;

formFields['cadre56HotelA']                            = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceHebergement.a;
formFields['cadre56HotelB']                            = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceHebergement.b;
formFields['cadre56HotelC']                            = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceHebergement.c;
formFields['cadre56HotelD']                            = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceHebergement.d;
formFields['cadre56HotelE']                            = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceHebergement.e;
formFields['cadre56HotelABCDE']                        = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceHebergement.f;

formFields['cadre56CinemaA']                           = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceCinema.a;
formFields['cadre56CinemaB']                           = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceCinema.b;
formFields['cadre56CinemaC']                           = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceCinema.c;
formFields['cadre56CinemaD']                           = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceCinema.d;
formFields['cadre56CinemaE']                           = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceCinema.e;
formFields['cadre56CinemaABCDE']                       = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationCommerce.destinationCommerceCinema.f;

formFields['cadre56LocauxBureauxA']                    = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementLocauxBureaux.a;
formFields['cadre56LocauxBureauxB']                    = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementLocauxBureaux.b;
formFields['cadre56LocauxBureauxC']                    = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementLocauxBureaux.c;
formFields['cadre56LocauxBureauxD']                    = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementLocauxBureaux.d;
formFields['cadre56LocauxBureauxE']                    = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementLocauxBureaux.e;
formFields['cadre56LocauxBureauxABCDE']                = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementLocauxBureaux.f;

formFields['cadre56LocauxTechniqueA']                  = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementLocauxTechniques.a;
formFields['cadre56LocauxTechniqueB']                  = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementLocauxTechniques.b;
formFields['cadre56LocauxTechniqueC']                  = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementLocauxTechniques.c;
formFields['cadre56LocauxTechniqueD']                  = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementLocauxTechniques.d;
formFields['cadre56LocauxTechniqueE']                  = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementLocauxTechniques.e;
formFields['cadre56LocauxTechniqueABCDE']              = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementLocauxTechniques.f;

formFields['cadre56EnseignementA']                     = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementEtablissement.a;
formFields['cadre56EnseignementB']                     = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementEtablissement.b;
formFields['cadre56EnseignementC']                     = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementEtablissement.c;
formFields['cadre56EnseignementD']                     = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementEtablissement.d;
formFields['cadre56EnseignementE']                     = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementEtablissement.e;
formFields['cadre56EnseignementABCDE']                 = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementEtablissement.f;

formFields['cadre56ArtA']                              = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementSalles.a;
formFields['cadre56ArtB']                              = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementSalles.b;
formFields['cadre56ArtC']                              = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementSalles.c;
formFields['cadre56ArtD']                              = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementSalles.d;
formFields['cadre56ArtE']                              = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementSalles.e;
formFields['cadre56ArtABCDE']                          = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementSalles.f;

formFields['cadre56EquipementSportifA']                = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementSportifs.a;
formFields['cadre56EquipementSportifB']                = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementSportifs.b;
formFields['cadre56EquipementSportifC']                = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementSportifs.c;
formFields['cadre56EquipementSportifD']                = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementSportifs.d;
formFields['cadre56EquipementSportifE']                = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementSportifs.e;
formFields['cadre56EquipementSportifABCDE']            = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementSportifs.f;

formFields['cadre56AutresA']                           = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementAutres.a;
formFields['cadre56AutresB']                           = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementAutres.b;
formFields['cadre56AutresC']                           = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementAutres.c;
formFields['cadre56AutresD']                           = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementAutres.d;
formFields['cadre56AutresE']                           = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementAutres.e;
formFields['cadre56AutresABCDE']                       = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationEquipement.destinationEquipementAutres.f;

formFields['cadre56IndustrieA']                        = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationAutre.destinationAutresIndustrie.a;
formFields['cadre56IndustrieB']                        = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationAutre.destinationAutresIndustrie.b;
formFields['cadre56IndustrieC']                        = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationAutre.destinationAutresIndustrie.c;
formFields['cadre56IndustrieD']                        = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationAutre.destinationAutresIndustrie.d;
formFields['cadre56IndustrieE']                        = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationAutre.destinationAutresIndustrie.e;
formFields['cadre56IndustrieABCDE']                    = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationAutre.destinationAutresIndustrie.f;

formFields['cadre56EntrepotA']                         = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationAutre.destinationAutresEntrepot.a;
formFields['cadre56EntrepotB']                         = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationAutre.destinationAutresEntrepot.b;
formFields['cadre56EntrepotC']                         = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationAutre.destinationAutresEntrepot.c;
formFields['cadre56EntrepotD']                         = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationAutre.destinationAutresEntrepot.d;
formFields['cadre56EntrepotE']                         = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationAutre.destinationAutresEntrepot.e;
formFields['cadre56EntrepotABCDE']                     = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationAutre.destinationAutresEntrepot.f;

formFields['cadre56BureauA']                           = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationAutre.destinationAutresBureau.a;
formFields['cadre56BureauB']                           = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationAutre.destinationAutresBureau.b;
formFields['cadre56BureauC']                           = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationAutre.destinationAutresBureau.c;
formFields['cadre56BureauD']                           = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationAutre.destinationAutresBureau.d;
formFields['cadre56BureauE']                           = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationAutre.destinationAutresBureau.e;
formFields['cadre56BureauABCDE']                       = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationAutre.destinationAutresBureau.f;

formFields['cadre56CentreA']                           = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationAutre.destinationAutresCentre.a;
formFields['cadre56CentreB']                           = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationAutre.destinationAutresCentre.b;
formFields['cadre56CentreC']                           = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationAutre.destinationAutresCentre.c;
formFields['cadre56CentreD']                           = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationAutre.destinationAutresCentre.d;
formFields['cadre56CentreE']                           = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationAutre.destinationAutresCentre.e;
formFields['cadre56CentreABCDE']                       = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationAutre.destinationAutresCentre.f;

formFields['cadre56TotaleA']                           = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationSurfacesTotales2.a;
formFields['cadre56TotaleB']                           = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationSurfacesTotales2.b;
formFields['cadre56TotaleC']                           = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationSurfacesTotales2.c;
formFields['cadre56TotaleD']                           = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationSurfacesTotales2.d;
formFields['cadre56TotaleE']                           = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationSurfacesTotales2.e;
formFields['cadre56TotaleABCDE']                       = $ds027PE2.projetConstructionGroup3.projetConstruction4.destinationSurfacesTotales2.f;




//Cadre 5.8
formFields['cadre58AvantRealisation']                  = $ds027PE2.projetConstructionGroup4.projetConstruction4.stationnementNbAvant;
formFields['cadre58ApresRealisation']                  = $ds027PE2.projetConstructionGroup4.projetConstruction4.stationnementNbApres;
formFields['cadre58Adresse']                           = $ds027PE2.projetConstructionGroup4.projetConstruction4.stationnementAdresse;
formFields['cadre58NBPlaces']                          = $ds027PE2.projetConstructionGroup4.projetConstruction4.stationnementPlaces;
formFields['cadre58SurfaceTotale']                     = $ds027PE2.projetConstructionGroup4.projetConstruction4.stationnementSurface;
formFields['cadre58SurfaceTotaleBatie']                = $ds027PE2.projetConstructionGroup4.projetConstruction4.stationnementSurfaceBatie;
formFields['cadre58Emprise']                           = $ds027PE2.projetConstructionGroup4.projetConstruction4.stationnementEntreprise;

//Cadre 6
formFields['cadre6Date']                               = $ds027PE2.projetDemolitionsGroup.projetDemolitions.demolitionsDate;
formFields['cadre6DemolitionTotale']                   = Value('id').of($ds027PE2.projetDemolitionsGroup.projetDemolitions.demolitionType).eq('totale') ? true : false ;
formFields['cadre6DemolitionPartielle']                = Value('id').of($ds027PE2.projetDemolitionsGroup.projetDemolitions.demolitionType).eq('partielle') ? true : false ;
formFields['cadre6DemolitionPartielleDesciption']      = $ds027PE2.projetDemolitionsGroup.projetDemolitions.demolitionPartielle;
formFields['cadre6DemolitionNb']                       = $ds027PE2.projetDemolitionsGroup.projetDemolitions.demolitionNBlogements;

//Cadre 7

if ($ds027PE2.participationVoirieReseauxGroup.participationVoirieReseaux.contactPVR == true) {
	
formFields['cadre7Madame']                             = '' ;
formFields['cadre7Monsieur']                           = '' ;
formFields['cadre7PM']                                 = '' ;
formFields['cadre7Nom']                                = '' ;
formFields['cadre7Prenom']                             = '' ;
formFields['cadre7RaisonSociale']                      = '' ;
formFields['cadre7Numero']                             = '' ;
formFields['cadre7Voie']                               = '' ;
formFields['cadre7LieuDit']                            = '' ;
formFields['cadre7Localite']                           = '' ;
formFields['cadre7CP']                                 = '' ;
formFields['cadre7BP']                                 = '' ;
formFields['cadre7Cedex']                              = '' ;
formFields['cadre7PaysEtranger']                       = '' ;
formFields['cadre7Division']                           = '' ;

}

else {
	
formFields['cadre7Madame']                             = Value('id').of($ds027PE2.participationVoirieReseauxGroup.participationVoirieReseaux.proprietaireCoordonnees.civiliteProprio).eq('madame') ? true : false ;
formFields['cadre7Monsieur']                           = Value('id').of($ds027PE2.participationVoirieReseauxGroup.participationVoirieReseaux.proprietaireCoordonnees.civiliteProprio).eq('monsieur') ? true : false ;
formFields['cadre7PM']                                 = Value('id').of($ds027PE2.participationVoirieReseauxGroup.participationVoirieReseaux.proprietaireCoordonnees.proprietairePhysiqueMorale).eq('personneMorale') ? true : false ;
formFields['cadre7Nom']                                = $ds027PE2.participationVoirieReseauxGroup.participationVoirieReseaux.proprietaireCoordonnees.nomProprio;
formFields['cadre7Prenom']                             = $ds027PE2.participationVoirieReseauxGroup.participationVoirieReseaux.proprietaireCoordonnees.prenomProprio;
formFields['cadre7RaisonSociale']                      = $ds027PE2.participationVoirieReseauxGroup.participationVoirieReseaux.proprietaireCoordonnees.raisonSocialeProprio;
formFields['cadre7Numero']                             = $ds027PE2.participationVoirieReseauxGroup.participationVoirieReseaux.proprietaireCoordonnees.numeroAdresseProprio;
formFields['cadre7Voie']                               = ($ds027PE2.participationVoirieReseauxGroup.participationVoirieReseaux.proprietaireCoordonnees.libelleAdresseProrio ? $ds027PE2.participationVoirieReseauxGroup.participationVoirieReseaux.proprietaireCoordonnees.libelleAdresseProrio : '')
														+' '+ ($ds027PE2.participationVoirieReseauxGroup.participationVoirieReseaux.proprietaireCoordonnees.complementAdresseProprio ? $ds027PE2.participationVoirieReseauxGroup.participationVoirieReseaux.proprietaireCoordonnees.complementAdresseProprio : '') ;
formFields['cadre7LieuDit']                            = $ds027PE2.participationVoirieReseauxGroup.participationVoirieReseaux.proprietaireCoordonnees.lieuAdresseProprio;
formFields['cadre7Localite']                           = $ds027PE2.participationVoirieReseauxGroup.participationVoirieReseaux.proprietaireCoordonnees.villeAdresseProprio;
formFields['cadre7CP']                                 = $ds027PE2.participationVoirieReseauxGroup.participationVoirieReseaux.proprietaireCoordonnees.codePostalAdresseProprio;
formFields['cadre7BP']                                 = $ds027PE2.participationVoirieReseauxGroup.participationVoirieReseaux.proprietaireCoordonnees.boitePostalAdresseProprio;
formFields['cadre7Cedex']                              = $ds027PE2.participationVoirieReseauxGroup.participationVoirieReseaux.proprietaireCoordonnees.cedexAdresseProprio;
formFields['cadre7PaysEtranger']                       = $ds027PE2.participationVoirieReseauxGroup.participationVoirieReseaux.proprietaireCoordonnees.paysEtrangerAdresseProprio;
formFields['cadre7Division']                           = $ds027PE2.participationVoirieReseauxGroup.participationVoirieReseaux.proprietaireCoordonnees.divEtrangerAdresseProprio;

}

//Cadre 8
formFields['cadre8Installation']                       = $ds027PE2.infosLegislationConnexeGroup.infosLegislationConnexe1.infosConx1 ? true : false;
formFields['cadre8Travaux']                            = $ds027PE2.infosLegislationConnexeGroup.infosLegislationConnexe1.infosConx2 ? true : false;
formFields['cadre8Derogation']                         = $ds027PE2.infosLegislationConnexeGroup.infosLegislationConnexe1.infosConx3 ? true : false;
formFields['cadre8SituePerimetre']                     = $ds027PE2.infosLegislationConnexeGroup.infosLegislationConnexe2.infosConx4 ? true : false;
formFields['cadre8SitueAbords']                        = $ds027PE2.infosLegislationConnexeGroup.infosLegislationConnexe2.infosConx5 ? true : false;

//Cadre 9
formFields['cadre9SignatureA']                         = $ds027PE2.signatureGroup.signatureGroup1.lieuSignature;
formFields['cadre9SignatureLe']                        = $ds027PE2.signatureGroup.signatureGroup1.dateSignature;

formFields['cadre2CourriersElectroniqueOUI']           = $ds027PE2.signatureGroup.signatureGroup1.dematerialisationEchanges;
formFields['cadre2CourriersElectronique']              = $ds027PE2.signatureGroup.signatureGroup1.dematEchangesEmail
formFields['cadre2CourriersElectroniqueFin']           = ''; /* $ds027PE2.adresse.adresseContact.adresseContact2.Cadre2CourriersElectroniqueFin; */

                                                                                  
formFields['cadre9SignatureDemandeur']                 ='';  /* $ds027PE2.signatureGroup.signatureGroup1.Cadre9SignatureDemandeur ; */
formFields['cadre9SignatureDemandeur1']                ='';  /* $ds027PE2.signatureGroup.signatureGroup1.Cadre9SignatureDemandeur1; */
formFields['cadre9SignatureDemandeur2']                ='';  /* $ds027PE2.signatureGroup.signatureGroup1.Cadre9SignatureDemandeur2; */
formFields['cadre9SignaturePersonnes2']                =''; /*  $ds027PE2.signatureGroup.signatureGroup1.Cadre9SignaturePersonnes2; */
formFields['cadre9SignaturePersonnes1']                ='';  /* $ds027PE2.signatureGroup.signatureGroup1.Cadre9SignaturePersonnes1; */

formFields['page8Opposition']                          = $ds027PE2.signatureGroup.signature7;


//PJ
formFields['pjPA1']                                    = "" ; /* $ds027PE2.info11.PjPA1; */
formFields['pjPA2']                                    = "" ; /* $ds027PE2.info11.PjPA2; */
formFields['pjPA3']                                    = "" ; /* $ds027PE2.info11.PjPA3; */
formFields['pjPA4']                                    = "" ; /* $ds027PE2.info11.PjPA4; */
formFields['pjPA41']                                   = "" ; /* $ds027PE2.info11.PjPA41; */
formFields['pjPA5']                                    = "" ; /* $ds027PE2.info11.PjPA5; */
formFields['pjPA6']                                    = "" ; /* $ds027PE2.info11.PjPA6; */
formFields['pjPA7']                                    = "" ; /* $ds027PE2.info11.PjPA7; */
formFields['pjPA8']                                    = "" ; /* $ds027PE2.info11.PjPA8; */
formFields['pjPA9']                                    = "" ; /* $ds027PE2.info11.PjPA9; */
formFields['pjPA10']                                   = "" ; /* $ds027PE2.info11.PjPA10; */
formFields['pjPA11']                                   = "" ; /* $ds027PE2.info11.PjPA11; */
formFields['pjPA12']                                   = "" ; /* $ds027PE2.info12.PjPA12; */
formFields['pjPA121']                                  = "" ; /* $ds027PE2.info12.PjPA121; */
formFields['pjPA122']                                  = "" ; /* $ds027PE2.info12.PjPA122; */
formFields['pjPA13']                                   = "" ; /* $ds027PE2.info12.PjPA13; */
formFields['pjPA14']                                   = "" ; /* $ds027PE2.info12.PjPA14; */
formFields['pjPA141']                                  = "" ; /* $ds027PE2.info12.PjPA141; */
formFields['pjPA151']                                  = "" ; /* $ds027PE2.info12.PjPA151; */
formFields['pjPA152']                                  = "" ; /* $ds027PE2.info12.PjPA152; */
formFields['pjPA153']                                  = "" ; /* $ds027PE2.info12.PjPA153; */
formFields['pjPA16']                                   = "" ; /* $ds027PE2.info12.PjPA16; */
formFields['pjPA161']                                  = "" ; /* $ds027PE2.info12.PjPA161; */
formFields['pjPA162']                                  = "" ; /* $ds027PE2.info12.PjPA162; */
formFields['pjPA17']                                   = "" ; /* $ds027PE2.info12.PjPA17; */
formFields['pjPA18']                                   = "" ; /* $ds027PE2.info12.PjPA18; */
formFields['pjPA19']                                   = "" ; /* $ds027PE2.info12.PjPA19; */
formFields['pjPA20']                                   = "" ; /* $ds027PE2.info13.PjPA20; */
formFields['pjPA21']                                   = "" ; /* $ds027PE2.info13.PjPA21; */
formFields['pjPA22']                                   = "" ; /* $ds027PE2.info13.PjPA22; */
formFields['pjPA23']                                   = "" ; /* $ds027PE2.info13.PjPA23; */
formFields['pjPA231']                                  = "" ; /* $ds027PE2.info13.PjPA231; */
formFields['pjPA232']                                  = "" ; /* $ds027PE2.info13.PjPA232; */
formFields['pjPA233']                                  = "" ; /* $ds027PE2.info13.PjPA233; */
formFields['pjPA24']                                   = "" ; /* $ds027PE2.info13.PjPA24; */
formFields['pjPA25']                                   = "" ; /* $ds027PE2.info13.PjPA25; */
formFields['pjPA26']                                   = "" ; /* $ds027PE2.info13.PjPA26; */
formFields['pjPA27']                                   = "" ; /* $ds027PE2.info13.PjPA27; */
formFields['pjPA28']                                   = "" ; /* $ds027PE2.info13.PjPA28; */
formFields['pjPA281']                                  = "" ; /* $ds027PE2.info13.PjPA281; */
formFields['pjPA282']                                  = "" ; /* $ds027PE2.info13.PjPA282; */
formFields['pjPA283']                                  = "" ; /* $ds027PE2.info13.PjPA283; */
formFields['pjPA284']                                  = "" ; /* $ds027PE2.info13.PjPA284; */
formFields['pjPA29']                                   = "" ; /* $ds027PE2.info14.PjPA29; */
formFields['pjPA291']                                  = "" ; /* $ds027PE2.info14.PjPA291; */
formFields['pjPA30']                                   = "" ; /* $ds027PE2.info14.PjPA30; */
formFields['pjPA31']                                   = "" ; /* $ds027PE2.info14.PjPA31; */
formFields['pjPA32']                                   = "" ; /* $ds027PE2.info14.PjPA32; */
formFields['pjPA33']                                   = "" ; /* $ds027PE2.info14.PjPA33; */
formFields['pjPA34']                                   = "" ; /* $ds027PE2.info14.PjPA34; */
formFields['pjPA35']                                   = "" ; /* $ds027PE2.info14.PjPA35; */
formFields['pjPA36']                                   = "" ; /* $ds027PE2.info14.PjPA36; */
formFields['pjPA37']                                   = "" ; /* $ds027PE2.info14.PjPA37; */
formFields['pjPA38']                                   = "" ; /* $ds027PE2.info14.PjPA38; */
formFields['pjPA39']                                   = "" ; /* $ds027PE2.info14.PjPA39; */
formFields['pjPA40']                                   = "" ; /* $ds027PE2.info14.PjPA40; */
formFields['pjPAA1']                                   = "" ; /* $ds027PE2.info14.PjPAA1; */
formFields['pjPAA411']                                 = "" ; /* $ds027PE2.info14.PjPAA411; */
formFields['pjPA42']                                   = "" ; /* $ds027PE2.info15.PjPA42; */
formFields['pjPA43']                                   = "" ; /* $ds027PE2.info15.PjPA43; */
formFields['pjPA44']                                   = "" ; /* $ds027PE2.info15.PjPA44; */
formFields['pjPA45']                                   = "" ; /* $ds027PE2.info15.PjPA45; */
formFields['pjPA46']                                   = "" ; /* $ds027PE2.info15.PjPA46; */
formFields['pjPA47']                                   = "" ; /* $ds027PE2.info15.PjPA47; */
formFields['pjPA48']                                   = "" ; /* $ds027PE2.info15.PjPA48; */
formFields['pjPA49']                                   = "" ; /* $ds027PE2.info15.PjPA49; */
formFields['pjPA50']                                   = "" ; /* $ds027PE2.info15.PjPA50; */
formFields['pjPA51']                                   = "" ; /* $ds027PE2.info15.PjPA51; */
formFields['pjPA52']                                   = "" ; /* $ds027PE2.info15.PjPA52; */
formFields['pjPA521']                                  = "" ; /* $ds027PE2.info15.PjPA521; */
formFields['pjPA53']                                   = "" ; /* $ds027PE2.info15.PjPA53; */
formFields['pjPA54']                                   = "" ; /* $ds027PE2.info15.PjPA54; */
formFields['pjPA58']                                   = "" ; /* $ds027PE2.info15.PjPA58; */

//Annexe
formFields['annexePjA1']                               = "" ; /* $ds027PE2.info16.AnnexePjA1; */
formFields['annexePjA2']                               = "" ; /* $ds027PE2.info16.AnnexePjA2; */
formFields['annexePjA3']                               = "" ; /* $ds027PE2.info16.AnnexePjA3; */
formFields['annexePjA4']                               = "" ; /* $ds027PE2.info16.AnnexePjA4; */
formFields['annexePjA5']                               = "" ; /* $ds027PE2.info16.AnnexePjA5; */
formFields['annexePjA6']                               = "" ; /* $ds027PE2.info16.AnnexePjA6; */
formFields['annexePjA7']                               = "" ; /* $ds027PE2.info16.AnnexePjA7; */
formFields['annexePjA8']                               = "" ; /* $ds027PE2.info16.AnnexePjA8; */

//Annexe - Déclaration des éléments nécessaires au calcul des impositions

formFields['annexe1Cadre1Surface1']                    = $ds027PE2.projetConstructionAmenagementGroup.projetConstructionAmenagement1.surfaceTaxable1;
formFields['annexe1Cadre1Surface2']                    = $ds027PE2.projetConstructionAmenagementGroup.projetConstructionAmenagement1.surfaceTaxable2;

formFields['annexe1C121A1']                            = $ds027PE2.projetConstructionAmenagementGroup.projetConstructionAmenagement2.creationLocaux.locauxHabitationPrincipale.cas1.logementCrees;
formFields['annexe1C121A2']                            = $ds027PE2.projetConstructionAmenagementGroup.projetConstructionAmenagement2.creationLocaux.locauxHabitationPrincipale.cas2.logementCrees;
formFields['annexe1C121A3']                            = $ds027PE2.projetConstructionAmenagementGroup.projetConstructionAmenagement2.creationLocaux.locauxHabitationPrincipale.cas3.logementCrees;
formFields['annexe1C121A4']                            = $ds027PE2.projetConstructionAmenagementGroup.projetConstructionAmenagement2.creationLocaux.locauxHabitationPrincipale.cas4.logementCrees;
formFields['annexe1C121A5']                            = $ds027PE2.projetConstructionAmenagementGroup.projetConstructionAmenagement2.creationLocaux.locauxHabitationSecondaire.logementCrees;
formFields['annexe1C121A9']                            = $ds027PE2.projetConstructionAmenagementGroup.projetConstructionAmenagement2.creationLocaux.nbTotalLogementCrees;
                                                       
formFields['annexe1C121B1']                            = $ds027PE2.projetConstructionAmenagementGroup.projetConstructionAmenagement2.creationLocaux.locauxHabitationPrincipale.cas1.surfaceCrees;
formFields['annexe1C121B2']                            = $ds027PE2.projetConstructionAmenagementGroup.projetConstructionAmenagement2.creationLocaux.locauxHabitationPrincipale.cas2.surfaceCrees;
formFields['annexe1C121B3']                            = $ds027PE2.projetConstructionAmenagementGroup.projetConstructionAmenagement2.creationLocaux.locauxHabitationPrincipale.cas3.surfaceCrees;
formFields['annexe1C121B4']                            = $ds027PE2.projetConstructionAmenagementGroup.projetConstructionAmenagement2.creationLocaux.locauxHabitationPrincipale.cas4.surfaceCrees;
formFields['annexe1C121B5']                            = $ds027PE2.projetConstructionAmenagementGroup.projetConstructionAmenagement2.creationLocaux.locauxHabitationSecondaire.surfaceCrees;
formFields['annexe1C121B6']                            = $ds027PE2.projetConstructionAmenagementGroup.projetConstructionAmenagement2.creationLocaux.locauxHergement.cas1.surfaceCrees;
formFields['annexe1C121B7']                            = $ds027PE2.projetConstructionAmenagementGroup.projetConstructionAmenagement2.creationLocaux.locauxHergement.cas2.surfaceCrees;
formFields['annexe1C121B8']                            = $ds027PE2.projetConstructionAmenagementGroup.projetConstructionAmenagement2.creationLocaux.locauxHergement.cas3.surfaceCrees;
                                                                 
formFields['annexe1C121C1']                            = $ds027PE2.projetConstructionAmenagementGroup.projetConstructionAmenagement2.creationLocaux.locauxHabitationPrincipale.cas1.surfaceStatCrees;
formFields['annexe1C121C2']                            = $ds027PE2.projetConstructionAmenagementGroup.projetConstructionAmenagement2.creationLocaux.locauxHabitationPrincipale.cas2.surfaceStatCrees;
formFields['annexe1C121C3']                            = $ds027PE2.projetConstructionAmenagementGroup.projetConstructionAmenagement2.creationLocaux.locauxHabitationPrincipale.cas3.surfaceStatCrees;
formFields['annexe1C121C4']                            = $ds027PE2.projetConstructionAmenagementGroup.projetConstructionAmenagement2.creationLocaux.locauxHabitationPrincipale.cas4.surfaceStatCrees;
formFields['annexe1C121C5']                            = $ds027PE2.projetConstructionAmenagementGroup.projetConstructionAmenagement2.creationLocaux.locauxHabitationSecondaire.surfaceStatCrees;
formFields['annexe1C121C6']                            = $ds027PE2.projetConstructionAmenagementGroup.projetConstructionAmenagement2.creationLocaux.locauxHergement.cas1.surfaceStatCrees;
formFields['annexe1C121C7']                            = $ds027PE2.projetConstructionAmenagementGroup.projetConstructionAmenagement2.creationLocaux.locauxHergement.cas2.surfaceStatCrees;
formFields['annexe1C121C8']                            = $ds027PE2.projetConstructionAmenagementGroup.projetConstructionAmenagement2.creationLocaux.locauxHergement.cas3.surfaceStatCrees;

formFields['annexe1C122OUI']                           = $ds027PE2.projetConstructionAmenagementGroup2.extensionHabitationPrincipale.pretAide ? true : false;
formFields['annexe1C122NON']                           = $ds027PE2.projetConstructionAmenagementGroup2.extensionHabitationPrincipale.pretAide ? false : true;
formFields['annexe1C122OUIPrecision']                  = $ds027PE2.projetConstructionAmenagementGroup2.extensionHabitationPrincipale.pretAideDetail;
formFields['annexe1C122Surface']                       = $ds027PE2.projetConstructionAmenagementGroup2.extensionHabitationPrincipale.surfaceTotaleTaxable;
formFields['annexe1C122NBLogement']                    = $ds027PE2.projetConstructionAmenagementGroup2.extensionHabitationPrincipale.nbLogementsExistants;

formFields['annexe1C123A1']                            = $ds027PE2.projetConstructionAmenagementGroup2.creationExtensionLocaux.nbCommerces.nbCommercesCrees;

formFields['annexe1C123B2']                            = $ds027PE2.projetConstructionAmenagementGroup2.creationExtensionLocaux.totalSurfacesAnnexesInclus.surfacesCommercesCrees;
formFields['annexe1C123C2']                            = $ds027PE2.projetConstructionAmenagementGroup2.creationExtensionLocaux.totalSurfacesAnnexesInclus.surfacesStatCommerceCrees;

formFields['annexe1C123B3']                            = $ds027PE2.projetConstructionAmenagementGroup2.creationExtensionLocaux.locauxIndustrielsAnnexes.surfacesIndustrielCrees;
formFields['annexe1C123C3']                            = $ds027PE2.projetConstructionAmenagementGroup2.creationExtensionLocaux.locauxIndustrielsAnnexes.surfacesStatIndustrielCrees;

formFields['annexe1C123B4']                            = $ds027PE2.projetConstructionAmenagementGroup2.creationExtensionLocaux.locauxArtisanauxAnnexes.surfacesArtisanalCrees;
formFields['annexe1C123C4']                            = $ds027PE2.projetConstructionAmenagementGroup2.creationExtensionLocaux.locauxArtisanauxAnnexes.surfacesStatArtisanalCrees;

formFields['annexe1C123B5']                            = $ds027PE2.projetConstructionAmenagementGroup2.creationExtensionLocaux.locauxEntrepotsAnnexes.surfacesEntrepotCrees;
formFields['annexe1C123C5']                            = $ds027PE2.projetConstructionAmenagementGroup2.creationExtensionLocaux.locauxEntrepotsAnnexes.surfacesStatEntrepotCrees;

formFields['annexe1C123B6']                            = $ds027PE2.projetConstructionAmenagementGroup2.creationExtensionLocaux.locauxExploitations.surfacesExploitationCrees;
formFields['annexe1C123C6']                            = $ds027PE2.projetConstructionAmenagementGroup2.creationExtensionLocaux.locauxExploitations.surfacesStatExploitationCrees;

formFields['annexe1C123B7']                            = $ds027PE2.projetConstructionAmenagementGroup2.creationExtensionLocaux.locauxCentresEquestres.surfacesCentreEquestreCrees;
formFields['annexe1C123C7']                            = $ds027PE2.projetConstructionAmenagementGroup2.creationExtensionLocaux.locauxCentresEquestres.surfacesStatCentreEquestreCrees;


formFields['annexe1C123BC8']                           = $ds027PE2.projetConstructionAmenagementGroup2.creationExtensionLocaux.locauxSurfacesCrees.parcStatExploitCo;

formFields['annexe1C13NBPlaces']                       = $ds027PE2.projetConstructionAmenagementGroup3.autresElementsCrees.autresPlacesStat;
formFields['annexe1C13Superficie']                     = $ds027PE2.projetConstructionAmenagementGroup3.autresElementsCrees.autresSuperficiePiscine;
formFields['annexe1C13NBTentes']                       = $ds027PE2.projetConstructionAmenagementGroup3.autresElementsCrees.autresEmplacements;
formFields['annexe1C13NBHabitations']                  = $ds027PE2.projetConstructionAmenagementGroup3.autresElementsCrees.autresEmplacementsLoisirs;
formFields['annexe1C13NBEoliennes']                    = $ds027PE2.projetConstructionAmenagementGroup3.autresElementsCrees.autresEoliennes;
formFields['annexe1C13SuperficiePanneaux']             = $ds027PE2.projetConstructionAmenagementGroup3.autresElementsCrees.autresPanneauxPhoto;

formFields['annexe1C14Locaux']                         = $ds027PE2.projetConstructionAmenagementGroup3.redevanceArcheologie.redevanceLocaux;
formFields['annexe1C14Piscine']                        = $ds027PE2.projetConstructionAmenagementGroup3.redevanceArcheologie.redevancePiscine;
formFields['annexe1C14Stationnement']                  = $ds027PE2.projetConstructionAmenagementGroup3.redevanceArcheologie.redevanceEmplacementsStat;
formFields['annexe1C14Tentes']                         = $ds027PE2.projetConstructionAmenagementGroup3.redevanceArcheologie.redevanceEmplacementsTente;
formFields['annexe1C14Habitations']                    = $ds027PE2.projetConstructionAmenagementGroup3.redevanceArcheologie.redevanceEmplacementsLoisirs;

formFields['annexe1C15MiniersOUI']                     = $ds027PE2.projetConstructionAmenagementGroup3.casParticuliers.casParticuliers1 ? true : false ;
formFields['annexe1C15MiniersNON']                     = $ds027PE2.projetConstructionAmenagementGroup3.casParticuliers.casParticuliers1 ? false : true ;
formFields['annexe1C15MonumentsOUI']                   = $ds027PE2.projetConstructionAmenagementGroup3.casParticuliers.casParticuliers2 ?  true : false  ;
formFields['annexe1C15MonumentsNON']                   = $ds027PE2.projetConstructionAmenagementGroup3.casParticuliers.casParticuliers2 ? false : true ;

formFields['annexe1C2OUI']                             = $ds027PE2.projetConstructionAmenagementGroup3.versementsSousDensites.vsd ? true : false  ;
formFields['annexe1C2NON']                             = $ds027PE2.projetConstructionAmenagementGroup3.versementsSousDensites.vsd ? false  : true  ;
formFields['annexe1C2SuperficieFonciere']              = $ds027PE2.projetConstructionAmenagementGroup3.versementsSousDensites.vsdSuperficie;
formFields['annexe1C2SuperficieFonciereConstructible'] = $ds027PE2.projetConstructionAmenagementGroup3.versementsSousDensites.vsdSuperficieConstruc;
formFields['annexe1C2Valeur']                          = $ds027PE2.projetConstructionAmenagementGroup3.versementsSousDensites.vsdValeurTerrain;
formFields['annexe1C2Surface']                         = $ds027PE2.projetConstructionAmenagementGroup3.versementsSousDensites.vsdSurfacesPlancher;
formFields['annexe1C2Date']                            = $ds027PE2.projetConstructionAmenagementGroup3.versementsSousDensites.vsdRescritFiscalDate;

formFields['annexe1C3F1']                              = '' ; /* $ds027PE2.info18.Annexe1C3F1; */
formFields['annexe1C3F2']                              = '' ;/* $ds027PE2.info18.Annexe1C3F2; */
formFields['annexe1C4F3']                              = '' ;/* $ds027PE2.info19.Annexe1C4F3; */
formFields['annexe1C4F4']                              = '' ;/* $ds027PE2.info19.Annexe1C4F4; */
formFields['annexe1C4F5']                              = '' ;/* $ds027PE2.info19.Annexe1C4F5; */
formFields['annexe1C4F6']                              = '' ;/* $ds027PE2.info19.Annexe1C4F6; */
formFields['annexe1C4F7']                              = '' ;/* ds027PE2.info19.Annexe1C4F7; */
formFields['annexe1C5Informations']                    = '' ;/* $ds027PE2.info19.Annexe1C5Informations; */
formFields['annexe1C5Date']                            = $ds027PE2.signatureGroup.signatureGroup1.dateSignature;
formFields['annexe1C5SignatureDeclarantNom']           = civNomPrenom;
formFields['annexe1C5Signature']                       = $ds027PE2.signatureGroup.signatureGroup1.signature2;

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qpxxxPEx.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */

 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		date: $ds027PE2.signatureGroup.signatureGroup1.dateSignature,
		autoriteHabilitee : " ",
		demandeContexte : "Demande de délivrance du permis d'aménager et/ou du permis de construire",
		civiliteNomPrenom : civNomPrenom
	});


/*
 * Ajout du formulaire de déclaration puis de l'attestation d'honorabilité
 */
var cerfaDoc = nash.doc //
	.load('models/model.pdf') //
	.apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Camping_amenager_construire.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Camping - Demande de délivrance du permis d\'aménager et/ou du permis de construire',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Camping - Demande de délivrance du permis d\'aménager et/ou du permis de construire',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});