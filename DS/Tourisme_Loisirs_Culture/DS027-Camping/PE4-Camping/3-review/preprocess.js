function pad(s) { return (s < 10) ? '0' + s : s; }

var cerfaFields = {};
var civNomPrenom    = ($ds027PE4.identificationDeclarant1.identificationDeclarant.nomDeclarant != null ? $ds027PE4.identificationDeclarant1.identificationDeclarant.nomDeclarant : ' ')+" "+($ds027PE4.identificationDeclarant1.identificationDeclarant.prenomDeclarant != null ? $ds027PE4.identificationDeclarant1.identificationDeclarant.prenomDeclarant : ' ')+" "+($ds027PE4.identificationDeclarant1.identificationDeclarant.denominationSociale !=null ? $ds027PE4.identificationDeclarant1.identificationDeclarant.denominationSociale : ' ');





/// Désignation du permis ou de la déclaration préalable   ($ds027PE4.permis.permis1.permisConstruireOuDeclration=='Madame');

cerfaFields['permisConstruireCoche']             = Value('id').of($ds027PE4.permis.permis1.permisConstruireOuDeclration).eq('permisConstruireCoche')? true : false;
cerfaFields['permisAmenagerCoche']               = Value('id').of($ds027PE4.permis.permis1.permisConstruireOuDeclration).eq('permisAmenagerCoche')? true : false;
cerfaFields['declarationPrealableCoche']         = Value('id').of($ds027PE4.permis.permis1.permisConstruireOuDeclration).eq('declarationPrealableCoche')? true : false;
cerfaFields['numeroPermisAmenagement']           = $ds027PE4.permis.permis1.numeroPermisAmenagement;
cerfaFields['amenagementCocheoui']               = Value('id').of($ds027PE4.permis.permis1.permisAmenagerTravauxCoche).eq('amenagementCocheoui')? true : false;
cerfaFields['amenagementCocheNon']               = Value('id').of($ds027PE4.permis.permis1.permisAmenagerTravauxCoche).eq('amenagementCocheNon')? true : false;

	cerfaFields['finitionJourDate']                  = '';
	cerfaFields['finitionMoisDate']                  = '';
	cerfaFields['finitionAnneeDate']                 = '';

if($ds027PE4.permis.permis1.dateFinitionVoiries != null) {
var dateTemp = new Date(parseInt ($ds027PE4.permis.permis1.dateFinitionVoiries.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	cerfaFields['finitionJourDate'] = day;
	cerfaFields['finitionMoisDate'] = month;
	cerfaFields['finitionAnneeDate'] = year;
}


cerfaFields['numeroDeclarationPrealable']        = $ds027PE4.permis.permis1.numeroDeclarationPrealable;
cerfaFields['numeroPermisConstruire']            = $ds027PE4.permis.permis1.numeroPermisConstruire;

Adresse: 

//Identification Déclarant


cerfaFields['madame']                           = ($ds027PE4.identificationDeclarant1.identificationDeclarant.civilite=='Madame');
cerfaFields['monsieur']                         = ($ds027PE4.identificationDeclarant1.identificationDeclarant.civilite=='Monsieur');
cerfaFields['nomDeclarant']                     =  $ds027PE4.identificationDeclarant1.identificationDeclarant.nomDeclarant;
cerfaFields['prenomDeclarant']                  =  $ds027PE4.identificationDeclarant1.identificationDeclarant.prenomDeclarant;
cerfaFields['denominationSociale']              = $ds027PE4.identificationDeclarant1.identificationDeclarant.denominationSociale;
cerfaFields['raisonSociale']                    = $ds027PE4.identificationDeclarant1.identificationDeclarant.raisonSociale;
cerfaFields['numeroSiret']                      = $ds027PE4.identificationDeclarant1.identificationDeclarant.numeroSiret;  
cerfaFields['typeSociete']                      = $ds027PE4.identificationDeclarant1.identificationDeclarant.typeSociete;
cerfaFields['madameRepresentant']               = Value('id').of($ds027PE4.identificationDeclarant1.identificationDeclarant.civilitePersonneMorale).eq('madameRepresentant')? true : false;
cerfaFields['monsieurRepresentant']             = Value('id').of($ds027PE4.identificationDeclarant1.identificationDeclarant.civilitePersonneMorale).eq('monsieurRepresentant')? true : false;
cerfaFields['nomRepresentant']                  = $ds027PE4.identificationDeclarant1.identificationDeclarant.nomRepresentant;
cerfaFields['prenomRepresentant']               = $ds027PE4.identificationDeclarant1.identificationDeclarant.prenomRepresentant;


//Coordonnées du déclarant 

cerfaFields['adresseLieuDit']                    = $ds027PE4.adresseDeclarant.adresseDeclarant.adresseLieuDit;
cerfaFields['adresseLocalite']                   = $ds027PE4.adresseDeclarant.adresseDeclarant.adresseLocalite;
cerfaFields['adresseCodePostal']                 = $ds027PE4.adresseDeclarant.adresseDeclarant.adresseCodePostal;
cerfaFields['adresseBoitePostale']               = $ds027PE4.adresseDeclarant.adresseDeclarant.adresseBoitePostale;
cerfaFields['adresseCedex']                      = $ds027PE4.adresseDeclarant.adresseDeclarant.adresseCedex;
cerfaFields['adressePays']                       = $ds027PE4.adresseDeclarant.adresseDeclarant.adressePays;
cerfaFields['adresseDivisionTerritoriale']       = $ds027PE4.adresseDeclarant.adresseDeclarant.adresseDivisionTerritoriale;
cerfaFields['adresseNumeroVoie']                 = $ds027PE4.adresseDeclarant.adresseDeclarant.adresseNumeroVoie;
cerfaFields['adresseNomTypeVoie']                = $ds027PE4.adresseDeclarant.adresseDeclarant.adresseNomTypeVoie;
cerfaFields['telephoneDeclarant']                = $ds027PE4.adresseDeclarant.adresseDeclarant.telephoneDeclarant;
cerfaFields['indicatifPays']                     = $ds027PE4.adresseDeclarant.adresseDeclarant.indicatifPays;
cerfaFields['receptionMailcoche']                = $ds027PE4.adresseDeclarant.adresseDeclarant.receptionMailcoche;

var adresse = $ds027PE4.adresseDeclarant.adresseDeclarant.adresseMailDeclarant;
	cerfaFields['adresseMailDeclarant']              = '';
	cerfaFields['siteMailDeclarant']                 = '';
if(adresse != null) {
	var adresseSplite = adresse.split("@");
	cerfaFields['adresseMailDeclarant']              = adresseSplite[0];
	cerfaFields['siteMailDeclarant']                 = adresseSplite[1];
}

//Achévement des travaux 


cerfaFields['chantierAcheveJourDate']            = '';
cerfaFields['chantierAcheveeMoisDate']           = '';
cerfaFields['chantierAcheveAnneeDate']           = '';

if($ds027PE4.achevementTravaux.achevementTravaux.chantierAcheveDate != null) {
var dateTemp = new Date(parseInt ($ds027PE4.achevementTravaux.achevementTravaux.chantierAcheveDate.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	cerfaFields['chantierAcheveJourDate'] = day;
	cerfaFields['chantierAcheveeMoisDate'] = month;
	cerfaFields['chantierAcheveAnneeDate'] = year;
}




cerfaFields['changementJourDate']                = '';
cerfaFields['changementMoisDate']                = '';
cerfaFields['changementAnneeDate']               = '';

if($ds027PE4.achevementTravaux.achevementTravaux.changementDate != null) {
var dateTemp = new Date(parseInt ($ds027PE4.achevementTravaux.achevementTravaux.changementDate.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	cerfaFields['changementJourDate'] = day;
	cerfaFields['changementMoisDate'] = month;
	cerfaFields['changementAnneeDate'] = year;
}
cerfaFields['amenagementTrancheTravaux']         = $ds027PE4.achevementTravaux.achevementTravaux.amenagementTrancheTravaux;
cerfaFields['travauxTotaliteCoche']        		 = Value('id').of($ds027PE4.achevementTravaux.achevementTravaux.travaux).eq('travauxTotaliteCoche')? true : false;
cerfaFields['partielTravauxCoche']         		 = Value('id').of($ds027PE4.achevementTravaux.achevementTravaux.travaux).eq('partielTravauxCoche')? true : false;
cerfaFields['surfaceCree']				   		 =  $ds027PE4.achevementTravaux.achevementTravaux.surfaceCree; 
cerfaFields['nombreLogementTermines']      		 =  $ds027PE4.achevementTravaux.achevementTravaux.nombreLogementTermines;       
cerfaFields['nombreLogementTerminesIndividuels'] = $ds027PE4.achevementTravaux.achevementTravaux.nombreLogementTerminesIndividuels;
cerfaFields['nombreLogementterminesCollectifs']  = $ds027PE4.achevementTravaux.achevementTravaux.nombreLogementterminesCollectifs;
cerfaFields['nombreLogement1']                   = $ds027PE4.achevementTravaux.repartitionNombreLogement.nombreLogement1;
cerfaFields['nombreLogement2']                   = $ds027PE4.achevementTravaux.repartitionNombreLogement.nombreLogement2;
cerfaFields['nombreLogement3']                   = $ds027PE4.achevementTravaux.repartitionNombreLogement.nombreLogement3;
cerfaFields['nombreLogement4']                   = $ds027PE4.achevementTravaux.repartitionNombreLogement.nombreLogement4;
cerfaFields['logementLocatifSocialCoche']        = $ds027PE4.achevementTravaux.repartitionNombreLogement.logementLocatifSocialCoche;
cerfaFields['accessionSocialeCoche']             = $ds027PE4.achevementTravaux.repartitionNombreLogement.accessionSocialeCoche;
cerfaFields['pretTauxZeroCoche']                 = $ds027PE4.achevementTravaux.repartitionNombreLogement.pretTauxZeroCoche;
cerfaFields['autreFinancementCoche']             = $ds027PE4.achevementTravaux.repartitionNombreLogement.autreFinancementCoche;

//Signature

cerfaFields['lieuSignaturedeclarant']            = $ds027PE4.signature.signature.lieuSignaturedeclarant;
cerfaFields['dateSignatureDeclarant']            = $ds027PE4.signature.signature.dateSignatureDeclarant;
cerfaFields['lieuSignatureArchitecte']           = $ds027PE4.signature.signature.lieuSignatureArchitecte;
cerfaFields['dateSignatureArchitecte']           = $ds027PE4.signature.signature.dateSignatureArchitecte;
cerfaFields['droitInformationCoche']             = $ds027PE4.signature.droitInformation.droitInformationCoche;





/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
 
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp085PE3.signature.signature.lieuSignature,

	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		dateSignature: ($ds027PE4.signature.signature.dateSignatureDeclarant != null ? $ds027PE4.signature.signature.dateSignatureDeclarant : $ds027PE4.signature.signature.dateSignatureArchitecte) ,
		autoriteHabilitee :" ",
		demandeContexte : "Déclaration d'achèvement et de conformité des travaux",
		civiliteNomPrenom : civNomPrenom,
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
    .load('models/Cerfa 13408-04.pdf') //
    .apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
 
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAT1);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAT2);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Guide_Haute_Montagne_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
   label : 'Camping - déclaration d\'achèvement et de conformité des travaux',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration d\'achèvement et de conformité des travaux',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});