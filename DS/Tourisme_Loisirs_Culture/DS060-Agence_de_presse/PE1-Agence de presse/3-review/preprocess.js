function pad(s) { return (s < 10) ? '0' + s : s; } 
var cerfaFields = {};

cerfaFields['premiereDemande']                                                               = Value('id').of($ds060PE1.demandeAgrement.agencePresse.typeDemande).eq('premiereDemande') ? true : false;
cerfaFields['renouvellement']                                                                = Value('id').of($ds060PE1.demandeAgrement.agencePresse.typeDemande).eq('renouvellement') ? true : false;
cerfaFields['nouvelExamen']                                                                  = Value('id').of($ds060PE1.demandeAgrement.agencePresse.typeDemande).eq('nouvelExamen') ? true : false;

if($ds060PE1.demandeAgrement.agencePresse.dateEcheancePrecedentAgrement != null) {
var dateTemp = new Date(parseInt ($ds060PE1.demandeAgrement.agencePresse.dateEcheancePrecedentAgrement.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
cerfaFields['datePrecedentAgrementJour']   = day;
cerfaFields['datePrecedentAgrementMois']   = month;
cerfaFields['datePrecedentAgrementAnnee']  = year;
}

if($ds060PE1.demandeAgrement.agencePresse.datePrecedentRefus != null) {
var dateTemp = new Date(parseInt ($ds060PE1.demandeAgrement.agencePresse.datePrecedentRefus.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
cerfaFields['datePrecedentRefusJour']   = day;
cerfaFields['datePrecedentRefusMois']   = month;
cerfaFields['datePrecedentRefusAnnee']  = year;
}

cerfaFields['nomEntreprise']                                                                 = $ds060PE1.demandeAgrement.agencePresse.nomEntreprise;

if($ds060PE1.demandeAgrement.agencePresse.dateCreation != null) {
var dateTemp = new Date(parseInt ($ds060PE1.demandeAgrement.agencePresse.dateCreation.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
cerfaFields['dateCreationJour']   = day;
cerfaFields['dateCreationMois']   = month;
cerfaFields['dateCreationAnnee']  = year;
}
if($ds060PE1.demandeAgrement.agencePresse.dateDebutActivite != null) {
var dateTemp = new Date(parseInt ($ds060PE1.demandeAgrement.agencePresse.dateDebutActivite.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
cerfaFields['dateDebutActiviteJour']   = day;
cerfaFields['dateDebutActiviteMois']   = month;
cerfaFields['dateDebutActiviteAnnee']  = year;
}

cerfaFields['denominationRS']                                                                = $ds060PE1.demandeAgrement.agencePresse.denominationRS;
cerfaFields['statutJuridique']                                                               = $ds060PE1.demandeAgrement.agencePresse.statutJuridique;
cerfaFields['capitalSocial']                                                                 = $ds060PE1.demandeAgrement.agencePresse.capitalSocial;
cerfaFields['adresseSiegeSocial']                                                            = $ds060PE1.demandeAgrement.adresseSiegeSocial.numeroLibelleAdresseSiege 
																							   +' '+ ($ds060PE1.demandeAgrement.adresseSiegeSocial.complementAdresseSiege != null ? $ds060PE1.demandeAgrement.adresseSiegeSocial.complementAdresseSiege : '') 
																							   +' '+ ($ds060PE1.demandeAgrement.adresseSiegeSocial.codePostalAdresseSiege != null ? $ds060PE1.demandeAgrement.adresseSiegeSocial.codePostalAdresseSiege : '')
																							   +' '+ $ds060PE1.demandeAgrement.adresseSiegeSocial.villeAdresseSiege 
																							   +' '+ $ds060PE1.demandeAgrement.adresseSiegeSocial.paysAdresseSiege;
cerfaFields['adresseBureaux']                                                                = ($ds060PE1.demandeAgrement.adresseBureaux.numeroLibelleAdresseBureaux != null ? $ds060PE1.demandeAgrement.adresseBureaux.numeroLibelleAdresseBureaux : '')
																							   +' '+ ($ds060PE1.demandeAgrement.adresseBureaux.complementAdresseBureaux != null ? $ds060PE1.demandeAgrement.adresseBureaux.complementAdresseBureaux : '') 
																							   +' '+ ($ds060PE1.demandeAgrement.adresseBureaux.codePostalAdresseBureaux != null ? $ds060PE1.demandeAgrement.adresseBureaux.codePostalAdresseBureaux : '')
																							   +' '+ ($ds060PE1.demandeAgrement.adresseBureaux.villeAdresseBureaux != null ? $ds060PE1.demandeAgrement.adresseBureaux.villeAdresseBureaux : '')
																							   +' '+ ($ds060PE1.demandeAgrement.adresseBureaux.paysAdresseBureaux != null ? $ds060PE1.demandeAgrement.adresseBureaux.paysAdresseBureaux : '');
cerfaFields['numSIREN']                                                                      = $ds060PE1.demandeAgrement.agencePresse.numSIREN;
cerfaFields['codeNAF']                                                                       = $ds060PE1.demandeAgrement.agencePresse.codeNAF;

cerfaFields['nomDirecteur']                                                                  = $ds060PE1.autreInfos1.directeurRepresentant.nomDirecteur;
cerfaFields['nomRepresentant']                                                               = $ds060PE1.autreInfos1.directeurRepresentant.nomRepresentant != null ? $ds060PE1.autreInfos1.directeurRepresentant.nomRepresentant : '';

cerfaFields['responsableDossier']                                                            = $ds060PE1.autreInfos1.directeurRepresentant.responsable.responsableDossier;
var numero = $ds060PE1.autreInfos1.directeurRepresentant.responsable.numTelephoneMobile;
cerfaFields['telephoneMobile']                                                               = numero != null ? numero.toString().replace("+33 ", "0") : '';
numero = $ds060PE1.autreInfos1.directeurRepresentant.responsable.numTelephone;
cerfaFields['telephoneFixe']                                                                 = numero != null ? numero.toString().replace("+33 ", "0") : '';
numero = $ds060PE1.autreInfos1.directeurRepresentant.responsable.numFax;
cerfaFields['fax']                 			                                                 = numero != null ? numero.toString().replace("+33 ", "0") : '';
cerfaFields['email']                         		                                         = $ds060PE1.autreInfos1.directeurRepresentant.responsable.adresseMail;

cerfaFields['presseEcrite']                                                                  = Value('id').of($ds060PE1.autreInfos1.directeurRepresentant.activiteAgence).eq('presseEcrite') ? true : false;
cerfaFields['photographie']                                                                  = Value('id').of($ds060PE1.autreInfos1.directeurRepresentant.activiteAgence).eq('photographie') ? true : false;
cerfaFields['radioTv']                                                                       = Value('id').of($ds060PE1.autreInfos1.directeurRepresentant.activiteAgence).eq('radioTV') ? true : false;
cerfaFields['servicesEnLigne']                                                               = Value('id').of($ds060PE1.autreInfos1.directeurRepresentant.activiteAgence).eq('servicesEnLigne') ? true : false;
cerfaFields['infographie']                                                                   = Value('id').of($ds060PE1.autreInfos1.directeurRepresentant.activiteAgence).eq('infographie') ? true : false;
cerfaFields['activitePrincipaleAutres']                                                      = Value('id').of($ds060PE1.autreInfos1.directeurRepresentant.activiteAgence).eq('autres') ? true : false;
cerfaFields['activitePrincipalePrecision']                                                   = $ds060PE1.autreInfos1.directeurRepresentant.activiteAgencePrecision != null ? $ds060PE1.autreInfos1.directeurRepresentant.activiteAgencePrecision : '';

cerfaFields['chiffreAffaires']                                                               = $ds060PE1.autreInfos2.chiffreAffaires.chiffreAffaires;

var periodeConsidereeDu = $ds060PE1.autreInfos2.chiffreAffaires.periodeConsidereeDu.from
if(periodeConsidereeDu != null) {
var dateTemp = new Date(parseInt (periodeConsidereeDu.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
cerfaFields['periodeConsidereeDuJour']   = day;
cerfaFields['periodeConsidereeDuMois']   = month;
cerfaFields['periodeConsidereeDuAnnee']  = year;
}

var periodeConsidereeDu = $ds060PE1.autreInfos2.chiffreAffaires.periodeConsidereeDu.to
if(periodeConsidereeDu != null) {
var dateTemp = new Date(parseInt (periodeConsidereeDu.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
cerfaFields['periodeConsidereeAuJour']   = day;
cerfaFields['periodeConsidereeAuMois']   = month;
cerfaFields['periodeConsidereeAuAnnee']  = year;
}

cerfaFields['pourcentage']																	 = $ds060PE1.autreInfos2.chiffreAffaires.pourcentage != null ? $ds060PE1.autreInfos2.chiffreAffaires.pourcentage : '';
cerfaFields['principauxClients']                                                             = $ds060PE1.autreInfos2.chiffreAffaires.clients != null ? $ds060PE1.autreInfos2.chiffreAffaires.clients : '';

cerfaFields['nbrSalaries']                                                                   = $ds060PE1.autreInfos2.chiffreAffaires.personnel.nbrTotalSalaries;
cerfaFields['nbrJournalistes']                                                               = $ds060PE1.autreInfos2.chiffreAffaires.personnel.nbrJournalistes;
cerfaFields['nbrPermanents']                                                                 = $ds060PE1.autreInfos2.chiffreAffaires.personnel.nbrJournalistesPermanents != null ? $ds060PE1.autreInfos2.chiffreAffaires.personnel.nbrJournalistesPermanents : '';
cerfaFields['nbrPigistes']                                                                   = $ds060PE1.autreInfos2.chiffreAffaires.personnel.nbrJournalistesPigistes != null ? $ds060PE1.autreInfos2.chiffreAffaires.personnel.nbrJournalistesPigistes : '';
cerfaFields['nbrAdministratifs']                                                             = $ds060PE1.autreInfos2.chiffreAffaires.personnel.nbrAdministratifs;
cerfaFields['nbrTechniciens']                                                                = $ds060PE1.autreInfos2.chiffreAffaires.personnel.nbrTechniciens;

var variable = $ds060PE1.autreInfos2.chiffreAffaires.masseSalarialeAnnee;
cerfaFields['annee']                                                                         = variable.substring(2, 4);
cerfaFields['masseSalarialeAnnuelle']                                                        = $ds060PE1.autreInfos2.chiffreAffaires.masseSalarialeAnnuelle != null ? $ds060PE1.autreInfos2.chiffreAffaires.masseSalarialeAnnuelle : '';
cerfaFields['masseSalarialeMensuelle']                                                       = $ds060PE1.autreInfos2.chiffreAffaires.masseSalarialeMensuelle != null ? $ds060PE1.autreInfos2.chiffreAffaires.masseSalarialeMensuelle : '';

variable = $ds060PE1.autreInfos2.chiffreAffaires.masseSalarialeMois
if( variable != null) {
var dateTemp = new Date(parseInt (variable.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
    var year = dateTemp.getFullYear();
cerfaFields['moisConsidere']   = month;
cerfaFields['AnneeConsidere']  = year.toString().substring(2, 4);;
}

cerfaFields['surfaceBureaux']                                                                = $ds060PE1.autreInfos2.chiffreAffaires.surfaceBureaux;
cerfaFields['conventionOccupation']                                                          = $ds060PE1.autreInfos2.chiffreAffaires.conventionOccupation;

cerfaFields['signatureBox1']                                                                 = true;
cerfaFields['signatureBox2']                                                                 = true;
cerfaFields['signatureBox3']                                                                 = true;
cerfaFields['lieuSignature']                                                                 = $ds060PE1.signature.signature.lieuSignature;
cerfaFields['dateSignature']                                                                 = $ds060PE1.signature.signature.dateSignature;
cerfaFields['signature']                                                                     = $ds060PE1.autreInfos1.directeurRepresentant.nomRepresentant != null ? $ds060PE1.autreInfos1.directeurRepresentant.nomRepresentant : $ds060PE1.autreInfos1.directeurRepresentant.nomDirecteur;

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $ds060PE1.signature.signature.dateSignature,
		civiliteNomPrenom: $ds060PE1.autreInfos1.directeurRepresentant.nomRepresentant != null ? $ds060PE1.autreInfos1.directeurRepresentant.nomRepresentant : $ds060PE1.autreInfos1.directeurRepresentant.nomDirecteur
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $ds060PE1.signature.signature.dateSignature,
		autoriteHabilitee :"Commission paritaire des publications et des agences de presse (CPPAP)",
		demandeContexte : "Demande d’inscription sur la liste d’agence de presses par la commission paritaire des publications et des agences de presse (CPPAP)",
		civiliteNomPrenom : $ds060PE1.autreInfos1.directeurRepresentant.nomRepresentant != null ? $ds060PE1.autreInfos1.directeurRepresentant.nomRepresentant : $ds060PE1.autreInfos1.directeurRepresentant.nomDirecteur
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Cerfa 12405_03.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjStatus);
appendPj($attachmentPreprocess.attachmentPreprocess.pjKbis);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCA);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPresentation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjResultat);
appendPj($attachmentPreprocess.attachmentPreprocess.pjBulletin);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Agence_de_presse.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Agence de presse - Demande d’inscription sur la liste d’agence de presses par la commission paritaire des publications et des agences de presse',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Agence de presse - Demande d’inscription sur la liste d’agence de presses par la commission paritaire des publications et des agences de presse',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
