var formFields = {};

//Cadre A
var declarant13566 = (Value('id').of($ds029PE1.declarationGroup.cadreA.declarantCivilite13566).eq('madame') ? 'Mme' :'Mr') +' '+ $ds029PE1.declarationGroup.cadreA.cadreANom13566 +' ' + $ds029PE1.declarationGroup.cadreA.cadreApPrenom13566;
formFields['cadreANom_13566']                = $ds029PE1.declarationGroup.cadreA.cadreANom13566;
formFields['cadreApPrenom_13566']            = $ds029PE1.declarationGroup.cadreA.cadreApPrenom13566;
formFields['cadreAAdresse_13566']            = $ds029PE1.declarationGroup.cadreA.cadreAAdresseNumero13566 +' ' 
													+ ($ds029PE1.declarationGroup.cadreA.cadreAAdresseComplement13566 ? $ds029PE1.declarationGroup.cadreA.cadreAAdresseComplement13566 :'') +' '
													;

formFields['cadreACP_13566']                 = $ds029PE1.declarationGroup.cadreA.cadreACP13566 ;
formFields['cadreACommune_13566']            = $ds029PE1.declarationGroup.cadreA.cadreACommune13566;
formFields['cadreATelephone_13566']          = $ds029PE1.declarationGroup.cadreA.cadreATelephone13566 ;

formFields['cadreAAdresseDifferente_13566']  =   $ds029PE1.declarationGroup.cadreA.cadreAAdresseDifferenteOUINON13566 ?
												($ds029PE1.declarationGroup.cadreA.adresseDifferenteOUI.cadreAAdresseDifferenteNumero13566 +' ' 
													+ ($ds029PE1.declarationGroup.cadreA.adresseDifferenteOUI.cadreAAdresseDifferenteComplement13566 ? $ds029PE1.declarationGroup.cadreA.adresseDifferenteOUI.cadreAAdresseDifferenteComplement13566 :'') +' '
													) : '';

formFields['cadreACPDifferent_13566']        = $ds029PE1.declarationGroup.cadreA.adresseDifferenteOUI.cadreACPDifferent13566;
formFields['cadreACommuneDifferente_13566']  = $ds029PE1.declarationGroup.cadreA.cadreAAdresseDifferenteOUINON13566 ? ($ds029PE1.declarationGroup.cadreA.adresseDifferenteOUI.cadreACommuneDifferente13566 + ' / ' + $ds029PE1.declarationGroup.cadreA.adresseDifferenteOUI.cadreAPaysDifferente13566) : '';
formFields['cadreATelephoneDifferent_13566'] = $ds029PE1.declarationGroup.cadreA.adresseDifferenteOUI.cadreATelephoneDifferent13566 ;

//Cadre B
formFields['cadreBMaison_13566']             = Value('id').of($ds029PE1.declarationGroup.cadreB.identificationChambres).eq('maisonIndiv') ? true : false ;
formFields['cadreBAppartement_13566']        = $ds029PE1.declarationGroup.cadreB.cadreBAppartement13566 ? ('N°'+ $ds029PE1.declarationGroup.cadreB.cadreBAppartement13566) : '' ;
formFields['cadreBEtage_13566']              = $ds029PE1.declarationGroup.cadreB.cadreBEtage13566  ? ('N°' + $ds029PE1.declarationGroup.cadreB.cadreBEtage13566 ): '' ;
formFields['cadreBNBChambres_13566']         = $ds029PE1.declarationGroup.cadreB.cadreBNBChambres13566;
formFields['cadreBNBPersonnes_13566']        = $ds029PE1.declarationGroup.cadreB.cadreBNBPersonnes13566;

//Cadre C
formFields['cadreCPeriodeAnnee_13566']       = Value('id').of($ds029PE1.declarationGroup.cadreC.periodePrevisionelle13566).eq('annee') ? 'OUI' :'NON';
formFields['cadreCAutrePeriode_13566']       = ($ds029PE1.declarationGroup.cadreC.cadreCAutre1Periode13566 ? $ds029PE1.declarationGroup.cadreC.cadreCAutre1Periode13566 :'') ;
											
//Signature
formFields['signatureFaitA_13566']           = $ds029PE1.signatureGroup.signatureFaitA13566;
formFields['signatureDate_13566']            = $ds029PE1.signatureGroup.signatureDate13566;
formFields['signature_13566']                = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";


//var pdfModel = nash.doc.load('models/cerfa_11542-05.pdf').apply(formFields);

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qpxxxPEx.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $ds029PE1.signatureGroup.signatureDate13566,
		autoriteHabilitee :"Mairie de " + $ds029PE1.declarationGroup.cadreA.cadreACommune13566 ,
		demandeContexte : "Déclaration de location de chambres meublées pour touristes.",
		civiliteNomPrenom : declarant13566
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/cerfa_13566-02.pdf') //
	.apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Chambre_D_Hotes_Declaration.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Chambre d\'hôtes - Déclaration de location de chambres meublées pour touristes.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Chambre d\'hôtes - Déclaration de location de chambres meublées pour touristes.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});