function pad(s) { return (s < 10) ? '0' + s : s; }

var formFields = {};

var civNomPrenom = $ds055PE3.cadre1.cadre1.civilite + ' ' + $ds055PE3.cadre1.cadre1.nomNaissance + ' ' + $ds055PE3.cadre1.cadre1.prenomDeclarant;


formFields['declarationInitiale']                     = true
formFields['renouvellement']                          = false

/// Dossier de déclaration

formFields['departementExercice']                     = $ds055PE3.declaration.declaration.departementexercice;


//Etat Civil
formFields['civiliteMadame']                           = ($ds055PE3.cadre1.cadre1.civilite=='Madame');
formFields['civiliteMonsieur']                         = ($ds055PE3.cadre1.cadre1.civilite=='Monsieur');
formFields['nomNaissance']                             = $ds055PE3.cadre1.cadre1.nomNaissance;
formFields['nomUsage']                                 = $ds055PE3.cadre1.cadre1.nomUsage;
formFields['prenom']                                   = $ds055PE3.cadre1.cadre1.prenomDeclarant;
formFields['nomPere']                                  = $ds055PE3.cadre1.cadre1.nomPereDeclarant;
formFields['nomMere']                                  = $ds055PE3.cadre1.cadre1.nomMereDeclarant;
formFields['prenomPere']                               = $ds055PE3.cadre1.cadre1.prenomPereDeclarant;
formFields['prenomMere']                               = $ds055PE3.cadre1.cadre1.prenomMereDeclarant;


if($ds055PE3.cadre1.cadre1.dateNaissanceDeclarant != null) {
var dateTemp = new Date(parseInt ($ds055PE3.cadre1.cadre1.dateNaissanceDeclarant.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['dateNaissanceJour'] = day;
	formFields['dateNaissanceMois'] = month;
	formFields['dateNaissanceAnnee'] = year;
}


formFields['villeNaissance']                           = $ds055PE3.cadre1.cadre1.lieuNaissanceDeclarant;
formFields['paysNaissance']                            = $ds055PE3.cadre1.cadre1.paysNaissanceDeclarant;
formFields['arrondissementNaissance']                  = $ds055PE3.cadre1.cadre1.arrondissement;
formFields['departementNaissance']                     = $ds055PE3.cadre1.cadre1.departementNaissance;
formFields['nationalite']                              = $ds055PE3.cadre1.cadre1.nationaliteDeclarant;
formFields['nationaliteAutre']                         = $ds055PE3.cadre1.cadre1.autrenationaliteDeclarant;



//Coordonnées

formFields['adresseDeclarantPays']                     = $ds055PE3.adresse.adressePersonnelle.adresseDeclarantPays;
formFields['adresseDeclarantVille']                    = $ds055PE3.adresse.adressePersonnelle.adresseDeclarantVille;
formFields['adresseDeclarantComplement2']              = $ds055PE3.adresse.adressePersonnelle.adressecomplement2;
formFields['adresseDeclarantComplement1']              = $ds055PE3.adresse.adressePersonnelle.adressecomplement1;
formFields['telephoneFixeDeclarant']                   = $ds055PE3.adresse.adressePersonnelle.telephoneFixeDeclarant;
formFields['telephoneMobileDeclarant']                 = $ds055PE3.adresse.adressePersonnelle.telephoneMobileDeclarant;
formFields['adresseDeclarantNumeroNomRue']             = $ds055PE3.adresse.adressePersonnelle.adresseDeclarantNumeroNomRue;
formFields['courrielDeclarant']                        = $ds055PE3.adresse.adressePersonnelle.courrielDeclarant;
formFields['adresseDeclarantCodePostal']               = $ds055PE3.adresse.adressePersonnelle.adresseDeclarantCodePostal;

//Qualifications

///qualification1

formFields['qualificationType1']                       = $ds055PE3.qualification.qualification.typequalification1;
formFields['qualificationActivite1']                   = $ds055PE3.qualification.qualification.activitequalification1;
formFields['qualificationNumeroDiplome1']              = $ds055PE3.qualification.qualification.numerodiplome1;

if($ds055PE3.qualification.qualification.dateobtentiondiplome1 != null) {
var dateTemp = new Date(parseInt ($ds055PE3.qualification.qualification.dateobtentiondiplome1.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourObtention1'] = day;
	formFields['qualificationMoisObtention1'] = month;
	formFields['qualificationAnneeObtention1'] = year;
}

formFields['qualificationLieuObtention1']              = $ds055PE3.qualification.qualification.lieuobtention1;

formFields['qualificationJourRecyclage1']              ='';
formFields['qualificationMoisRecyclage1']              ='';
formFields['qualificationAnneeRecyclage1']              ='';

if($ds055PE3.qualification.qualification.daterecyclage1 != null) {
var dateTemp = new Date(parseInt ($ds055PE3.qualification.qualification.daterecyclage1.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourRecyclage1'] = day;
	formFields['qualificationMoisRecyclage1'] = month;
	formFields['qualificationAnneeRecyclage1'] = year;
	}
	
///qualification2

formFields['qualificationJourObtention2']              ='';
formFields['qualificationMoisObtention2']              ='';
formFields['qualificationAnneeObtention2']              ='';
if($ds055PE3.qualification.qualification.dateobtentiondiplome2 != null) {
var dateTemp = new Date(parseInt ($ds055PE3.qualification.qualification.dateobtentiondiplome2.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourObtention2'] = day;
	formFields['qualificationMoisObtention2'] = month;
	formFields['qualificationAnneeObtention2'] = year;
}
formFields['qualificationType2']                       = $ds055PE3.qualification.qualification.typequalification2;
formFields['qualificationActivite2']                   = $ds055PE3.qualification.qualification.activitequalification2;
formFields['qualificationNumeroDiplome2']              = $ds055PE3.qualification.qualification.numerodiplome2;

formFields['qualificationLieuObtention2']              = $ds055PE3.qualification.qualification.lieuobtention2;

formFields['qualificationJourRecyclage2']              ='';
formFields['qualificationMoisRecyclage2']              ='';
formFields['qualificationAnneeRecyclage2']              ='';
if($ds055PE3.qualification.qualification.daterecyclage2 != null) {
var dateTemp = new Date(parseInt ($ds055PE3.qualification.qualification.daterecyclage2.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourRecyclage2'] = day;
	formFields['qualificationMoisRecyclage2'] = month;
	formFields['qualificationAnneeRecyclage2'] = year;
	}
	
///qualification3

formFields['qualificationType3']                       = $ds055PE3.qualification.qualification.typequalification3;
formFields['qualificationActivite3']                   = $ds055PE3.qualification.qualification.activitequalification3;
formFields['qualificationNumeroDiplome3']              = $ds055PE3.qualification.qualification.numerodiplome3;

formFields['qualificationJourObtention3']              ='';
formFields['qualificationMoisObtention3']              ='';
formFields['qualificationAnneeObtention3']              ='';
if($ds055PE3.qualification.qualification.dateobtentiondiplome3 != null) {
var dateTemp = new Date(parseInt ($ds055PE3.qualification.qualification.dateobtentiondiplome3.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourObtention3'] = day;
	formFields['qualificationMoisObtention3'] = month;
	formFields['qualificationAnneeObtention3'] = year;
}


formFields['qualificationLieuObtention3']              = $ds055PE3.qualification.qualification.lieuobtention3;
formFields['qualificationJourRecyclage3']              ='';
formFields['qualificationMoisRecyclage3']              ='';
formFields['qualificationAnneeRecyclage3']              ='';
if($ds055PE3.qualification.qualification.daterecyclage3 != null) {
var dateTemp = new Date(parseInt ($ds055PE3.qualification.qualification.daterecyclage3.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourRecyclage3'] = day;
	formFields['qualificationMoisRecyclage3'] = month;
	formFields['qualificationAnneeRecyclage3'] = year;
	}
	
	///qualification 4
	
formFields['qualificationType4']                       = $ds055PE3.qualification.qualification.typequalification4;
formFields['qualificationActivite4']                   = $ds055PE3.qualification.qualification.activitequalification4;
formFields['qualificationNumeroDiplome4']              = $ds055PE3.qualification.qualification.numerodiplome4;

formFields['qualificationJourObtention4']              ='';
formFields['qualificationMoisObtention4']              ='';
formFields['qualificationAnneeObtention4']              ='';
if($ds055PE3.qualification.qualification.dateobtentiondiplome4 != null) {
var dateTemp = new Date(parseInt ($ds055PE3.qualification.qualification.dateobtentiondiplome4.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourObtention4'] = day;
	formFields['qualificationMoisObtention4'] = month;
	formFields['qualificationAnneeObtention4'] = year;
}


formFields['qualificationLieuObtention4']              = $ds055PE3.qualification.qualification.lieuobtention4;

formFields['qualificationJourRecyclage4']              ='';
formFields['qualificationMoisRecyclage4']              ='';
formFields['qualificationAnneeRecyclage4']              ='';
if($ds055PE3.qualification.qualification.daterecyclage4 != null) {
var dateTemp = new Date(parseInt ($ds055PE3.qualification.qualification.daterecyclage4.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourRecyclage4'] = day;
	formFields['qualificationMoisRecyclage4'] = month;
	formFields['qualificationAnneeRecyclage4'] = year;
	}
	
	///qualification 5
	
	
formFields['qualificationType5']                       = $ds055PE3.qualification.qualification.typequalification5;
formFields['qualificationActivite5']                   = $ds055PE3.qualification.qualification.activitequalification5;
formFields['qualificationNumeroDiplome5']              = $ds055PE3.qualification.qualification.numerodiplome5;
formFields['qualificationJourObtention5']              ='';
formFields['qualificationMoisObtention5']              ='';
formFields['qualificationAnneeObtention5']             ='';

if($ds055PE3.qualification.qualification.dateobtentiondiplome5 != null) {
var dateTemp = new Date(parseInt ($ds055PE3.qualification.qualification.dateobtentiondiplome5.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourObtention5'] = day;
	formFields['qualificationMoisObtention5'] = month;
	formFields['qualificationAnneeObtention5'] = year;
}

formFields['qualificationLieuObtention5']              = $ds055PE3.qualification.qualification.lieuobtention5;

formFields['qualificationJourRecyclage5']              ='';
formFields['qualificationMoisRecyclage5']              ='';
formFields['qualificationAnneeRecyclage5']              ='';
if($ds055PE3.qualification.qualification.daterecyclage5 != null) {
var dateTemp = new Date(parseInt ($ds055PE3.qualification.qualification.daterecyclage5.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourRecyclage5'] = day;
	formFields['qualificationMoisRecyclage5'] = month;
	formFields['qualificationAnneeRecyclage5'] = year;
	}
	
	///qualification 6
	
formFields['qualificationType6']                       = $ds055PE3.qualification.qualification.typequalification6;
formFields['qualificationActivite6']                   = $ds055PE3.qualification.qualification.activitequalification6;
formFields['qualificationNumeroDiplome6']              = $ds055PE3.qualification.qualification.numerodiplome6;

formFields['qualificationJourObtention6']              ='';
formFields['qualificationMoisObtention6']              ='';
formFields['qualificationAnneeObtention6']             ='';
if($ds055PE3.qualification.qualification.dateobtentiondiplome6 != null) {
var dateTemp = new Date(parseInt ($ds055PE3.qualification.qualification.dateobtentiondiplome6.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourObtention6'] = day;
	formFields['qualificationMoisObtention6'] = month;
	formFields['qualificationAnneeObtention6'] = year;
}

formFields['qualificationLieuObtention6']              = $ds055PE3.qualification.qualification.lieuobtention6;

formFields['qualificationJourRecyclage6']              ='';
formFields['qualificationMoisRecyclage6']              ='';
formFields['qualificationAnneeRecyclage6']              ='';
if($ds055PE3.qualification.qualification.daterecyclage6 != null) {
var dateTemp = new Date(parseInt ($ds055PE3.qualification.qualification.daterecyclage6.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourRecyclage6'] = day;
	formFields['qualificationMoisRecyclage6'] = month;
	formFields['qualificationAnneeRecyclage6'] = year;
	
	}
	
	///qualification 7

formFields['qualificationType7']                       = $ds055PE3.qualification.qualification.typequalification7;
formFields['qualificationActivite7']                   = $ds055PE3.qualification.qualification.activitequalification7;
formFields['qualificationNumeroDiplome7']              = $ds055PE3.qualification.qualification.numerodiplome7;
formFields['qualificationJourObtention7']              ='';
formFields['qualificationMoisObtention7']              ='';
formFields['qualificationAnneeObtention7']             ='';
if($ds055PE3.qualification.qualification.dateobtentiondiplome7 != null) {
var dateTemp = new Date(parseInt ($ds055PE3.qualification.qualification.dateobtentiondiplome7.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourObtention7'] = day;
	formFields['qualificationMoisObtention7'] = month;
	formFields['qualificationAnneeObtention7'] = year;
}

formFields['qualificationLieuObtention7']              = $ds055PE3.qualification.qualification.lieuobtention7;

formFields['qualificationJourRecyclage7']              ='';
formFields['qualificationMoisRecyclage7']              ='';
formFields['qualificationAnneeRecyclage7']              ='';
if($ds055PE3.qualification.qualification.daterecyclage7 != null) {
var dateTemp = new Date(parseInt ($ds055PE3.qualification.qualification.daterecyclage7.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourRecyclage7'] = day;
	formFields['qualificationMoisRecyclage7'] = month;
	formFields['qualificationAnneeRecyclage7'] = year;
	
	}
	
	///qualification 8
	
formFields['qualificationType8']                       = $ds055PE3.qualification.qualification.typequalification8;
formFields['qualificationActivite8']                   = $ds055PE3.qualification.qualification.activitequalification8;
formFields['qualificationNumeroDiplome8']              = $ds055PE3.qualification.qualification.numerodiplome8;

formFields['qualificationJourObtention8']              ='';
formFields['qualificationMoisObtention8']              ='';
formFields['qualificationAnneeObtention8']             ='';
if($ds055PE3.qualification.qualification.dateobtentiondiplome8 != null) {
var dateTemp = new Date(parseInt ($ds055PE3.qualification.qualification.dateobtentiondiplome8.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourObtention8'] = day;
	formFields['qualificationMoisObtention8'] = month;
	formFields['qualificationAnneeObtention8'] = year;
}

formFields['qualificationLieuObtention8']              = $ds055PE3.qualification.qualification.lieuobtention8;

formFields['qualificationJourRecyclage8']              ='';
formFields['qualificationMoisRecyclage8']              ='';
formFields['qualificationAnneeRecyclage8']              ='';
if($ds055PE3.qualification.qualification.daterecyclage8 != null) {
var dateTemp = new Date(parseInt ($ds055PE3.qualification.qualification.daterecyclage8.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourRecyclage8'] = day;
	formFields['qualificationMoisRecyclage8'] = month;
	formFields['qualificationAnneeRecyclage8'] = year;
	
	}
	
	
	///qualification 9

formFields['qualificationJourObtention9']              ='';
formFields['qualificationMoisObtention9']              ='';
formFields['qualificationAnneeObtention9']             ='';	
formFields['qualificationType9']                       = $ds055PE3.qualification.qualification.typequalification9;
formFields['qualificationActivite9']                   = $ds055PE3.qualification.qualification.activitequalification9;
formFields['qualificationNumeroDiplome9']              = $ds055PE3.qualification.qualification.numerodiplome9;

if($ds055PE3.qualification.qualification.dateobtentiondiplome8 != null) {
var dateTemp = new Date(parseInt ($ds055PE3.qualification.qualification.dateobtentiondiplome9.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourObtention9'] = day;
	formFields['qualificationMoisObtention9'] = month;
	formFields['qualificationAnneeObtention9'] = year;
}

formFields['qualificationLieuObtention9']              = $ds055PE3.qualification.qualification.lieuobtention9;

formFields['qualificationJourRecyclage9']              ='';
formFields['qualificationMoisRecyclage9']              ='';
formFields['qualificationAnneeRecyclage9']              ='';
if($ds055PE3.qualification.qualification.daterecyclage9 != null) {
var dateTemp = new Date(parseInt ($ds055PE3.qualification.qualification.daterecyclage9.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['qualificationJourRecyclage9'] = day;
	formFields['qualificationMoisRecyclage9'] = month;
	formFields['qualificationAnneeRecyclage9'] = year;
	
	}
//Equivalence diplôme
formFields['equivalenceType1']               = '';
formFields['equivalenceType2']               = '';
formFields['equivalenceType3']               = '';
formFields['equivalenceActivite1']           = '';
formFields['equivalenceActivite2']           = '';
formFields['equivalenceActivite3']           = '';
formFields['equivalenceNumero1']             = '';
formFields['equivalenceNumero2']             = '';
formFields['equivalenceNumero3']             = '';
formFields['equivalenceJourDelivrance1']              ='';
formFields['equivalenceJourDelivrance2']              ='';
formFields['equivalenceJourDelivrance3']              ='';
formFields['equivalenceMoisDelivrance1']              ='';
formFields['equivalenceMoisDelivrance2']              ='';
formFields['equivalenceMoisDelivrance3']              ='';
formFields['equivalenceAnneeDelivrance1']              ='';
formFields['equivalenceAnneeDelivrance2']              ='';
formFields['equivalenceAnneeDelivrance3']              ='';

//beneficiaire equivalence diplôme
formFields['equivalenceType6']                       ='';
formFields['equivalenceType7']                       ='';
formFields['equivalenceType8']                       ='';
formFields['equivalenceType9']                       ='';
formFields['equivalenceActivite6']                   ='';
formFields['equivalenceActivite7']                   ='';
formFields['equivalenceActivite8']                   ='';
formFields['equivalenceActivite9']                   ='';
formFields['equivalenceNumero6']                     ='';
formFields['equivalenceNumero7']                     ='';
formFields['equivalenceNumero8']                     ='';
formFields['equivalenceNumero9']                     ='';
formFields['equivalenceJourDelivrance6']             ='';
formFields['equivalenceJourDelivrance7']             ='';
formFields['equivalenceJourDelivrance8']             ='';
formFields['equivalenceJourDelivrance9']             ='';
formFields['equivalenceMoisDelivrance6']             ='';
formFields['equivalenceMoisDelivrance7']             ='';
formFields['equivalenceMoisDelivrance8']             ='';
formFields['equivalenceMoisDelivrance9']             ='';
formFields['equivalenceAnneeDelivrance6']             ='';
formFields['equivalenceAnneeDelivrance7']             ='';
formFields['equivalenceAnneeDelivrance8']             ='';
formFields['equivalenceAnneeDelivrance9']             ='';

//personnes en formation (préparation diplome)
formFields['intituleDiplomePreparation']              ='';
formFields['formationType']                           ='';
formFields['formationActivite']                       ='';

formFields['stageNomEtablissement1']                  ='';
formFields['stageNomEtablissement2']                  ='';
formFields['stageNomEtablissement3']                  ='';
formFields['stageNomEtablissement4']                  ='';
formFields['stageNumeroNomRueAdresseEtablissement1']        ='';
formFields['stageNumeroNomRueAdresseEtablissement2']        ='';
formFields['stageNumeroNomRueAdresseEtablissement3']        ='';
formFields['stageNumeroNomRueAdresseEtablissement4']        ='';
formFields['stageVillePaysAdresseEtablissement1']           ='';
formFields['stageVillePaysAdresseEtablissement2']           ='';
formFields['stageVillePaysAdresseEtablissement3']           ='';
formFields['stageVillePaysAdresseEtablissement4']           ='';
formFields['stageJourDebut1']              ='';
formFields['stageJourDebut2']              ='';
formFields['stageJourDebut3']              ='';
formFields['stageJourDebut4']              ='';
formFields['stageMoisDebut1']              ='';
formFields['stageMoisDebut2']              ='';
formFields['stageMoisDebut3']              ='';
formFields['stageMoisDebut4']              ='';
formFields['stageAnneeDebut1']             ='';
formFields['stageAnneeDebut2']             ='';
formFields['stageAnneeDebut3']             ='';
formFields['stageAnneeDebut4']             ='';
formFields['stageJourFin1']              ='';
formFields['stageJourFin2']              ='';
formFields['stageJourFin3']              ='';
formFields['stageJourFin4']              ='';
formFields['stageMoisFin1']              ='';
formFields['stageMoisFin2']              ='';
formFields['stageMoisFin3']              ='';
formFields['stageMoisFin4']              ='';
formFields['stageAnneeFin1']             ='';
formFields['stageAnneeFin2']             ='';
formFields['stageAnneeFin3']             ='';
formFields['stageAnneeFin4']             ='';
formFields['stageNomTuteur1']             ='';
formFields['stageNomTuteur2']             ='';
formFields['stageNomTuteur3']             ='';
formFields['stageNomTuteur4']             ='';
formFields['autorisationExerciceJourDelivrance']           ='';
formFields['autorisationExerciceMoisDelivrance']           ='';
formFields['autorisationExerciceAnneeDelivrance']           ='';

//personnes en formation (préparation diplome) (volet complémentaire)
formFields['intituleDiplomePreparation1']              ='';
formFields['formationType1']                           ='';
formFields['formationActivite1']                       ='';

formFields['stageNomEtablissement5']                  ='';
formFields['stageNomEtablissement6']                  ='';
formFields['stageNomEtablissement7']                  ='';
formFields['stageNomEtablissement8']                  ='';
formFields['stageNumeroNomRueAdresseEtablissement5']        ='';
formFields['stageNumeroNomRueAdresseEtablissement6']        ='';
formFields['stageNumeroNomRueAdresseEtablissement7']        ='';
formFields['stageNumeroNomRueAdresseEtablissement8']        ='';
formFields['stageVillePaysAdresseEtablissement5']           ='';
formFields['stageVillePaysAdresseEtablissement6']           ='';
formFields['stageVillePaysAdresseEtablissement7']           ='';
formFields['stageVillePaysAdresseEtablissement8']           ='';
formFields['stageJourDebut5']              ='';
formFields['stageJourDebut6']              ='';
formFields['stageJourDebut7']              ='';
formFields['stageJourDebut8']              ='';
formFields['stageMoisDebut5']              ='';
formFields['stageMoisDebut6']              ='';
formFields['stageMoisDebut7']              ='';
formFields['stageMoisDebut8']              ='';
formFields['stageAnneeDebut5']             ='';
formFields['stageAnneeDebut6']             ='';
formFields['stageAnneeDebut7']             ='';
formFields['stageAnneeDebut8']             ='';
formFields['stageJourFin5']              ='';
formFields['stageJourFin6']              ='';
formFields['stageJourFin7']              ='';
formFields['stageJourFin8']              ='';
formFields['stageMoisFin5']              ='';
formFields['stageMoisFin6']              ='';
formFields['stageMoisFin7']              ='';
formFields['stageMoisFin8']              ='';
formFields['stageAnneeFin5']             ='';
formFields['stageAnneeFin6']             ='';
formFields['stageAnneeFin7']             ='';
formFields['stageAnneeFin8']             ='';
formFields['stageNomTuteur5']             ='';
formFields['stageNomTuteur6']             ='';
formFields['stageNomTuteur7']             ='';
formFields['stageNomTuteur8']             ='';
formFields['autorisationExerciceJourDelivrance2']           ='';
formFields['autorisationExerciceMoisDelivrance2']           ='';
formFields['autorisationExerciceAnneeDelivrance2']           ='';

// activités physiques ou sportives encadrées

formFields['metierIndependantPrincipal']               = ($ds055PE3.activites.activites.metierEducateur=='Principal');
formFields['metierIndependantSecondaire']              = ($ds055PE3.activites.activites.metierEducateur=='Secondaire');

formFields['raisonSocialeIndependant']                 = $ds055PE3.activites.activites.raisonsociale;
formFields['formeEURL']                                = ($ds055PE3.activites.activites.naturejuridique=='EURL');
formFields['formeIndividuelle']                        = ($ds055PE3.activites.activites.naturejuridique=='Entreprise individuelle');
formFields['numeroSiretIndependants']                  =$ds055PE3.activites.activites.numerosiret;

formFields['adresseDeclarantNumeroNomRueIndependants'] = $ds055PE3.activites.activites.adresseDeclarantNumeroNomRueIndependant;
formFields['adresseDeclarantCodePostalIndependants']   = $ds055PE3.activites.activites.adresseDeclarantCodePostalIndependant;
formFields['adresseDeclarantVilleIndependants']        = $ds055PE3.activites.activites.adresseDeclarantVilleIndependant;
formFields['telephoneFixeDeclarantIndependant']        = $ds055PE3.activites.activites.telephoneFixeDeclarantIndependant;
formFields['telephoneMobileDeclarantIndependant']      = $ds055PE3.activites.activites.telephoneMobileDeclarantIndependant;
formFields['courrielDeclarantIndependant']             = $ds055PE3.activites.activites.courrielDeclarantIndependant;
formFields['siteInternetDeclarantIndependant']         = $ds055PE3.activites.activites.siteinternet;

// Activite encadrée independant
/// 1

formFields['activiteEncadreeIndependant']              = $ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.activiteencadreeindependant1;
formFields['disciplineIndependant']                    = $ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.disciplineIndependant1;
formFields['lieuExerciceIndependant']                  = $ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.lieuexerciceIndependant1;
formFields['numeroNomVoieLieuExerciceIndependant']     = $ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.adresseExerciceIndependant1;
formFields['departementLieuExerciceIndependant']       = $ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.departementIndependant1;
formFields['codePostalLieuExerciceIndependant']        = $ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.codePostalIndependant1;
formFields['communeLieuExerciceIndependant']           = $ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.communeIndependant1;



if($ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant1 != null) {
var dateTemp = new Date(parseInt ($ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant1.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourDebutExerciceIndependant'] = day;
	formFields['moisDebutExerciceIndependant'] = month;
	formFields['anneeDebutExerciceIndependant'] = year;
}

///2 

formFields['activiteEncadreeIndependant2']              = $ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.activiteencadreeindependant2;
formFields['disciplineIndependant2']                    = $ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.disciplineIndependant2;
formFields['lieuExerciceIndependant2']                  = $ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.lieuexerciceIndependant2;
formFields['numeroNomVoieLieuExerciceIndependant2']     = $ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.adresseExerciceIndependant2;
formFields['departementLieuExerciceIndependant2']       = $ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.departementIndependant2;
formFields['codePostalLieuExerciceIndependant2']        = $ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.codePostalIndependant2;
formFields['communeLieuExerciceIndependant2']           = $ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.communeIndependant2;

formFields['jourDebutExerciceIndependant2']             ='';
formFields['moisDebutExerciceIndependant2']             ='';
formFields['anneeDebutExerciceIndependant2']            ='';
if($ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant2 != null) {
var dateTemp = new Date(parseInt ($ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant2.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourDebutExerciceIndependant2'] = day;
	formFields['moisDebutExerciceIndependant2'] = month;
	formFields['anneeDebutExerciceIndependant2'] = year;
}


///3

formFields['activiteEncadreeIndependant3']              = $ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.activiteencadreeindependant3;
formFields['disciplineIndependant3']                    = $ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.disciplineIndependant3;
formFields['lieuExerciceIndependant3']                  = $ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.lieuexerciceIndependant3;
formFields['numeroNomVoieLieuExerciceIndependant3']     = $ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.adresseExerciceIndependant3;
formFields['departementLieuExerciceIndependant3']       = $ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.departementIndependant3;
formFields['codePostalLieuExerciceIndependant3']        = $ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.codePostalIndependant3;
formFields['communeLieuExerciceIndependant3']           = $ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.communeIndependant3;

formFields['jourDebutExerciceIndependant3']             ='';
formFields['moisDebutExerciceIndependant3']             ='';
formFields['anneeDebutExerciceIndependant3']            ='';
if($ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant3 != null) {
var dateTemp = new Date(parseInt ($ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant3.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourDebutExerciceIndependant3'] = day;
	formFields['moisDebutExerciceIndependant3'] = month;
	formFields['anneeDebutExerciceIndependant3'] = year;
}


///4

formFields['activiteEncadreeIndependant4']              = $ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.activiteencadreeindependant4;
formFields['disciplineIndependant4']                    = $ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.disciplineIndependant4;
formFields['lieuExerciceIndependant4']                  = $ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.lieuexerciceIndependant4;
formFields['numeroNomVoieLieuExerciceIndependant4']     = $ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.adresseExerciceIndependant4;
formFields['departementLieuExerciceIndependant4']       = $ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.departementIndependant4;
formFields['codePostalLieuExerciceIndependant4']        = $ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.codePostalIndependant4;
formFields['communeLieuExerciceIndependant4']           = $ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.communeIndependant4;

formFields['jourDebutExerciceIndependant4']             ='';
formFields['moisDebutExerciceIndependant4']             ='';
formFields['anneeDebutExerciceIndependant4']            ='';
if($ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant4 != null) {
var dateTemp = new Date(parseInt ($ds055PE3.activitesencadreesindenpendant.activitesencadreesindenpendant.dateIndependant4.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourDebutExerciceIndependant4'] = day;
	formFields['moisDebutExerciceIndependant4'] = month;
	formFields['anneeDebutExerciceIndependant4'] = year;
}



//Signature
formFields['nomPrenomSignature']                       = $ds055PE3.cadre1.cadre1.nomNaissance + ' ' + $ds055PE3.cadre1.cadre1.prenomDeclarant;
formFields['Signature']                                = $ds055PE3.signature.signature.signatureCoche;
formFields['dateSignature']                            = $ds055PE3.signature.signature.faitLe;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
 
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp085PE3.signature.signature.lieuSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		dateSignature: $ds055PE3.signature.signature.faitLe,
		autoriteHabilitee :"Direction départementale de la cohésion sociale et de la protection des populations (DDCSPP)",
		demandeContexte : "Titulaire d'un diplôme, titre de formation ou certificat ",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
    .load('models/cerfa1269903.pdf') //
    .apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
 
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertificatMedical);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationEquivlence);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPhoto);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Activite_educateur_sportif.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
   label : 'Déclaration d’exercice de l’activité d’éducteur sportif - Titulaire d\'un diplôme, titre de formation ou certificat.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Titulaire d\'un diplôme, titre de formation ou certificat.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});