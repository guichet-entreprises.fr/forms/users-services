function pad(s) { return (s < 10) ? '0' + s : s; }

var formFields = {};

var civNomPrenom = $ds055PE1.cadre10.cadre10.civiliteSignataire + ' ' + $ds055PE1.cadre10.cadre10.nomDeclarant + ' ' + $ds055PE1.cadre10.cadre10.prenomDeclarant;


/*******************************************************************************
 * Objet de la déclaration
 ******************************************************************************/

formFields['creationEquipement']              = Value('id').of($ds055PE1.cadre1.cadre1.declaration).eq('creationequipementSportif') ? true : false ;
formFields['modificationEquipement']          = Value('id').of($ds055PE1.cadre1.cadre1.declaration).eq('modificationequipementsportif');
formFields['structurePrincipale']             = Value('id').of($ds055PE1.cadre1.cadre1.cadre11.precisionmodification).eq('structureprincipale') ? true : false ;
formFields['sol']                             = Value('id').of($ds055PE1.cadre1.cadre1.cadre11.precisionmodification).eq('sol') ? true : false ;
formFields['eclairage']                       = Value('id').of($ds055PE1.cadre1.cadre1.cadre11.precisionmodification).eq('eclairage') ? true : false ;
formFields['isolation']                       = Value('id').of($ds055PE1.cadre1.cadre1.cadre11.precisionmodification).eq('isolationchauffage') ? true : false ;
formFields['accoustique']                     = Value('id').of($ds055PE1.cadre1.cadre1.cadre11.precisionmodification).eq('acoustique') ? true : false ;
formFields['annexes']                         = Value('id').of($ds055PE1.cadre1.cadre1.cadre11.precisionmodification).eq('annexe') ? true : false ;
formFields['cessionEquipement']               = Value('id').of($ds055PE1.cadre1.cadre1.declaration).eq('cessionequipementsportif') ? true : false ;

formFields['nomCessionnaire']                 = $ds055PE1.cadre1.cadre1.cadre12.nom;
formFields['prenomCessionnaire']              = $ds055PE1.cadre1.cadre1.cadre12.prenom;
formFields['libelleVoieCessionnaire']         = $ds055PE1.cadre1.cadre1.cadre12.numeroLibelleAdresseDeclarant1;
formFields['complementAdresseCessionnaire']   = $ds055PE1.cadre1.cadre1.cadre12.declarantAdressePersoComplementCessionnaire;
formFields['numeroVoieCessionnaire']          = $ds055PE1.cadre1.cadre1.cadre12.numeroLibelleAdresseDeclarant;
formFields['codePostalCessionnaire']          = $ds055PE1.cadre1.cadre1.cadre12.codePostal;
formFields['villeCessionnaire']               = $ds055PE1.cadre1.cadre1.cadre12.commune;




formFields['suppresionEquipement']            = Value('id').of($ds055PE1.cadre1.cadre1.declaration).eq('suppresionequipementsportif') ? true : false ;


formFields['changementAffectationEquipement'] = Value('id').of($ds055PE1.cadre1.cadre1.declaration).eq('changemntaffectationequipementsportif') ? true : false ;
formFields['saisPas']                         = Value('id').of($ds055PE1.cadre1.cadre1.cadre13.nouvelleaffectation).eq('non') ? true : false ;
formFields['precisionNouvelleAffectation']   = $ds055PE1.cadre1.cadre1.cadre13.precisionnouvelleaffecation;

/*******************************************************************************
 * type proprietaire
 ******************************************************************************/

formFields['asso']                            = Value('id').of($ds055PE1.cadre2.cadre2.declarationproprietaire).eq('association') ? true : false ;
formFields['etat']                            = Value('id').of($ds055PE1.cadre2.cadre2.declarationproprietaire).eq('etat') ? true : false ;
formFields['region']                          = Value('id').of($ds055PE1.cadre2.cadre2.declarationproprietaire).eq('region') ? true : false ;
formFields['departement']                     = Value('id').of($ds055PE1.cadre2.cadre2.declarationproprietaire).eq('departement') ? true : false ;
formFields['commune']                         = Value('id').of($ds055PE1.cadre2.cadre2.declarationproprietaire).eq('commune') ? true : false ;
formFields['epci']                            = Value('id').of($ds055PE1.cadre2.cadre2.declarationproprietaire).eq('epci') ? true : false ;
formFields['autrePublic']                     = Value('id').of($ds055PE1.cadre2.cadre2.declarationproprietaire).eq('autreetablipublic') ? true : false ;
formFields['priveNonCommercial']              = Value('id').of($ds055PE1.cadre2.cadre2.declarationproprietaire).eq('prive') ? true : false ;
formFields['etablissementEnseignementPrive']  = Value('id').of($ds055PE1.cadre2.cadre2.declarationproprietaire).eq('etablissementenseiprive') ? true : false ;
formFields['etablissementPriveCommercial']    = Value('id').of($ds055PE1.cadre2.cadre2.declarationproprietaire).eq('etablissementenseiprivecommercial') ? true : false ;

formFields['etat1']                           = Value('id').of($ds055PE1.cadre2.cadre2.cadre3.declarationsecondaire).eq('etatsecondaire') ? true : false ;
formFields['region1']                         = Value('id').of($ds055PE1.cadre2.cadre2.cadre3.declarationsecondaire).eq('regionsecondaire') ? true : false ;
formFields['departement1']                    = Value('id').of($ds055PE1.cadre2.cadre2.cadre3.declarationsecondaire).eq('departementsecondaire') ? true : false ;
formFields['commune1']                        = Value('id').of($ds055PE1.cadre2.cadre2.cadre3.declarationsecondaire).eq('communesecondaire') ? true : false ;
formFields['asso1']                           = Value('id').of($ds055PE1.cadre2.cadre2.cadre3.declarationsecondaire).eq('associationsecondaire') ? true : false ;
formFields['autrePublic1']                    = Value('id').of($ds055PE1.cadre2.cadre2.cadre3.declarationsecondaire).eq('autreetablipublicsecondaire') ? true : false ;
formFields['priveNonCommercial1']             = Value('id').of($ds055PE1.cadre2.cadre2.cadre3.declarationsecondaire).eq('privesecondaire') ? true : false ;
formFields['etablissementPriveCommercial1']   = Value('id').of($ds055PE1.cadre2.cadre2.cadre3.declarationsecondaire).eq('etablissementenseiprivesecondaire') ? true : false ;
formFields['etablissementEnseignementPrive1'] = Value('id').of($ds055PE1.cadre2.cadre2.cadre3.declarationsecondaire).eq('etablissementenseiprivecommercialsecondaire') ? true : false ;
formFields['epci1']                           = Value('id').of($ds055PE1.cadre2.cadre2.cadre3.declarationsecondaire).eq('epcisecondaire') ? true : false ;

/*******************************************************************************
 * Identite du proprietaire
 ******************************************************************************/
formFields['nomProprietaire']                 = $ds055PE1.cadre2.cadre2.nomproprietaireprincipal;
formFields['prenomProprietaire']              = $ds055PE1.cadre2.cadre2.prenomproprietaireprincipal;
formFields['numeroVoieProprietaire']          = $ds055PE1.cadre2.cadre2.numeroLibelleAdresseDeclarantproprietaireprincipal;
formFields['libelleVoieProprietaire']         = $ds055PE1.cadre2.cadre2.numeroLibelleAdresseDeclarant1proprietaireprincipal;
formFields['complementAdresseProprietaire']   = $ds055PE1.cadre2.cadre2.declarantAdressePersoComplementProprietairePrincipal;
formFields['villeProprietaire']               = $ds055PE1.cadre2.cadre2.communeproprietaireprincipal;
formFields['codePostalProprietaire']          = $ds055PE1.cadre2.cadre2.codePostalproprietaireprincipal;




formFields['nomProprietaire1']                = $ds055PE1.cadre2.cadre2.cadre3.nomproprietairesecondaire;
formFields['prenomProprietaire1']             = $ds055PE1.cadre2.cadre2.cadre3.prenomproprietairesecondaire;
formFields['libelleVoieProprietaire1']        = $ds055PE1.cadre2.cadre2.cadre3.numeroLibelleAdresseDeclarant1proprietairesecondaire;
formFields['numeroVoieProprietaire1']         = $ds055PE1.cadre2.cadre2.cadre3.numeroLibelleAdresseDeclarantproprietairesecondaire;
formFields['complementAdresseProprietaire1']  = $ds055PE1.cadre2.cadre2.cadre3.declarantAdressePersoComplementProprietaire;
formFields['villeProprietaire1']              = $ds055PE1.cadre2.cadre2.cadre3.communeproprietairesecondaire;
formFields['codePostalProprietaire1']         = $ds055PE1.cadre2.cadre2.cadre3.codePostalproprietairesecondaire;

/*******************************************************************************
 * caractéristiques générales de l'équipement
 ******************************************************************************/
formFields['nomEquipement']                   = $ds055PE1.cadre4.cadre4.nomequipement;

formFields['typeEquipement']                  = $ds055PE1.cadre4.cadre4.typeequipement;
formFields['villeEquipement']                 = $ds055PE1.cadre4.cadre4.communeequipement;
formFields['codePostalEquipement']            = $ds055PE1.cadre4.cadre4.typeequipement;
formFields['libelleVoieEquipement']           = $ds055PE1.cadre4.cadre4.nomvoieEquipement;
formFields['complementVoieEquipement']        = $ds055PE1.cadre4.cadre4.complementAdresseEquipement;
formFields['numeroEquipement']                = $ds055PE1.cadre4.cadre4.numeroadresseequipement;




formFields['superficieAire']                  = $ds055PE1.cadre4.cadre4.superficieaire;
formFields['longueurAire']                    = $ds055PE1.cadre4.cadre4.longueureaire;
formFields['largeurAire']                     = $ds055PE1.cadre4.cadre4.largeureaire;




formFields['interieur']                       = Value('id').of($ds055PE1.cadre4.cadre4.cadre5.natureequipement).eq('interieur') ? true : false ;
formFields['exterieur']                       = Value('id').of($ds055PE1.cadre4.cadre4.cadre5.natureequipement).eq('exterieurcouvert') ? true : false ;
formFields['decouvert']                       = Value('id').of($ds055PE1.cadre4.cadre4.cadre5.natureequipement).eq('decouvert') ? true : false ;
formFields['decouvrable']                     = Value('id').of($ds055PE1.cadre4.cadre4.cadre5.natureequipement).eq('decouvrable') ? true : false ;
formFields['siteartificiel']                  = Value('id').of($ds055PE1.cadre4.cadre4.cadre5.natureequipement).eq('siteartificiel') ? true : false ;
formFields['sitenaturel']                     = Value('id').of($ds055PE1.cadre4.cadre4.cadre5.natureequipement).eq('sitenaturelprotege') ? true : false ;

formFields['avant1945']                       = Value('id').of($ds055PE1.cadre4.cadre4.cadre15.datenonpreciseservice).eq('avant1945') ? true : false ;
formFields['1964']                            = Value('id').of($ds055PE1.cadre4.cadre4.cadre15.datenonpreciseservice).eq('1945') ? true : false ;
formFields['1974']                            = Value('id').of($ds055PE1.cadre4.cadre4.cadre15.datenonpreciseservice).eq('1965') ? true : false ;
formFields['1984']                            = Value('id').of($ds055PE1.cadre4.cadre4.cadre15.datenonpreciseservice).eq('1975') ? true : false ;
formFields['1994']                            = Value('id').of($ds055PE1.cadre4.cadre4.cadre15.datenonpreciseservice).eq('1985') ? true : false ;
formFields['2004']                            = Value('id').of($ds055PE1.cadre4.cadre4.cadre15.datenonpreciseservice).eq('1995') ? true : false ;


formFields['jour1']              ='';
formFields['mois1']              ='';
formFields['annee1']             ='';


if($ds055PE1.cadre4.cadre4.cadre15.datepreciseservice != null) {
var dateTemp = new Date(parseInt ($ds055PE1.cadre4.cadre4.cadre15.datepreciseservice.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jour1'] = day;
	formFields['mois1'] = month;
	formFields['annee1'] = year;
}







formFields['individuel']                      = $ds055PE1.cadre6.cadre6.cadre7.individuelfamille;
formFields['scolaire']                        = $ds055PE1.cadre6.cadre6.cadre7.scolaireuniversite;
formFields['clubSportif']                     = $ds055PE1.cadre6.cadre6.cadre7.clubssportifs;
formFields['autreAsso']                       = $ds055PE1.cadre6.cadre6.cadre7.autreasso;

formFields['nombreLits']                      = $ds055PE1.cadre8.cadre8.locauxHebergement;
formFields['nombreVestiairesSportifs']        = $ds055PE1.cadre8.cadre8.nombreVestiaires;
formFields['nombreVestiairesArbitres']        = $ds055PE1.cadre8.cadre8.nombreVestiairesenseignants;
formFields['nombresPlacesGradins']            = $ds055PE1.cadre8.cadre8.nombrePlaces;



formFields['oui']                    = Value('id').of($ds055PE1.cadre8.cadre8.questionouverture).eq('oui') ? true : false ;
formFields['non']                    = Value('id').of($ds055PE1.cadre8.cadre8.questionouverture).eq('non') ? true : false ;

formFields['adresseInternet']                 = $ds055PE1.cadre8.cadre8.adresseinternet;
formFields['actvite1']                        = $ds055PE1.cadre9.cadre9.activitessportives;
formFields['actvite3']                        = $ds055PE1.cadre9.cadre9.activitessportives2;
formFields['actvite2']                        = $ds055PE1.cadre9.cadre9.activitessportives1;
formFields['actvite4']                        = $ds055PE1.cadre9.cadre9.activitessportives3;
formFields['actvite5']                        = $ds055PE1.cadre9.cadre9.activitessportives4;
formFields['noteActivite2']                   = $ds055PE1.cadre9.cadre9.niveaucompetition1;
formFields['noteActivite3']                   = $ds055PE1.cadre9.cadre9.niveaucompetition2;
formFields['noteActivite1']                   = $ds055PE1.cadre9.cadre9.niveaucompetition;
formFields['noteActivite4']                   = $ds055PE1.cadre9.cadre9.niveaucompetition3;
formFields['noteActivite5']                   = $ds055PE1.cadre9.cadre9.niveaucompetition4;

formFields['nomDeclaration']                  = $ds055PE1.cadre10.cadre10.nomDeclarant;
formFields['prenomDeclaration']               = $ds055PE1.cadre10.cadre10.prenomDeclarant;
formFields['numeroNomVoieDeclaration']        = $ds055PE1.cadre10.cadre10.numeroLibelleAdresseDeclarant1Declarant;
formFields['complementAdresseDeclaration']    = $ds055PE1.cadre10.cadre10.complementAdresseDeclarant12;
formFields['numeroDeclaration']               = $ds055PE1.cadre10.cadre10.numeroLibelleAdresseDeclarantDeclarant;
formFields['codePostalDeclaration']           = $ds055PE1.cadre10.cadre10.codePostalDeclarant;
formFields['villeDeclaration']                = $ds055PE1.cadre10.cadre10.communeDeclarant;


formFields['faitLe']                          = $ds055PE1.signature.signature.faita;




formFields['jourDeclaration']              ='';
formFields['moisDeclaration']              ='';
formFields['anneeDeclaration']             ='';

if($ds055PE1.signature.signature.faitLe != null) {
var dateTemp = new Date(parseInt ($ds055PE1.signature.signature.faitLe.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourDeclaration'] = day;
	formFields['moisDeclaration'] = month;
	formFields['anneeDeclaration'] = year;
}


formFields['courriel']                         = $ds055PE1.cadre10.cadre10.mail;


/*******************************************************************************
 * Chargement du courrier d'accompagnement à destination de l'AC
 ******************************************************************************/

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		dateSignature: $ds055PE1.signature.signature.faitLe,
		autoriteHabilitee :"Direction départementale de la cohésion sociale et de la protection des populations (DDCSPP)",
		demandeContexte : "Déclaration de recensement d\'équipement sportif",
		civiliteNomPrenom : civNomPrenom
	});




/*******************************************************************************
 * Ajout du cerfa
 ******************************************************************************/
//finalDoc.append(accompDoc.save('courrier.pdf'));
var cerfaDoc = nash.doc //
    .load('models/cerfan13436_02.pdf') //
    .apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}




/*******************************************************************************
 * Ajout des PJs
 ******************************************************************************/
 
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);




/*******************************************************************************
 * Enregistrement du fichier (en mémoire)
 ******************************************************************************/
var finalDocItem = finalDoc.save('Etablissement_Activite_physiques_et_sportives.pdf');



/*******************************************************************************
 * Persistance des données obtenues
 ******************************************************************************/

return spec.create({
    id : 'review',
   label : 'Etablissement d’activités physiques et sportives (EAPS) à vocation commerciale - Déclaration d’un équipement sportif',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration d\'équipement sportif en vue d\'un recencsement des équipements sportifs',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});