var formFields = {};
function pad(s) { return (s < 10) ? '0' + s : s; }
//Cadre A
//<!--DECLARANT Physique-->
var declarant14004 = $ds045PE1.declarationGroup.cadreA.cadreADenomination14004 ? $ds045PE1.declarationGroup.cadreA.cadreADenomination14004 : ((Value('id').of($ds045PE1.declarationGroup.cadreA.declarantCivilite14004).eq('madame') ? 'Mme' :'Mr') +' '+ $ds045PE1.declarationGroup.cadreA.cadreANom14004 +' ' + $ds045PE1.declarationGroup.cadreA.cadreAPrenom14004);


var nomlength = new String($ds045PE1.declarationGroup.cadreA.cadreANom14004) ; // 20 caractères
if(nomlength.length > 20){
	formFields['cadreANom_14004']                = $ds045PE1.declarationGroup.cadreA.cadreANom14004.substring(0, 20);
	formFields['cadreANomSuite_14004']           = $ds045PE1.declarationGroup.cadreA.cadreANom14004.substring(20,nomlength.length);
} else {
	formFields['cadreANom_14004']                = $ds045PE1.declarationGroup.cadreA.cadreANom14004;
	formFields['cadreANomSuite_14004']           = '';	
}
var nomPage2length = new String($ds045PE1.declarationGroup.cadreA.cadreANom14004) ; // 18 caractères
if(nomPage2length.length > 18){
	formFields['cadreANomPage2_14004']                = $ds045PE1.declarationGroup.cadreA.cadreANom14004.substring(0, 18);
	formFields['cadreANomPage2Suite_14004']           = $ds045PE1.declarationGroup.cadreA.cadreANom14004.substring(18,nomPage2length.length);
} else {
	formFields['cadreANomPage2_14004']                = $ds045PE1.declarationGroup.cadreA.cadreANom14004;
	formFields['cadreANomPage2Suite_14004']           = '';	
}

var prenomlength = new String($ds045PE1.declarationGroup.cadreA.cadreAPrenom14004) ; //19 caractères
if(prenomlength.length > 19){
	formFields['cadreAPrenom_14004']            = $ds045PE1.declarationGroup.cadreA.cadreAPrenom14004.substring(0, 19);
	formFields['cadreAPrenomSuite_14004']            = $ds045PE1.declarationGroup.cadreA.cadreAPrenom14004.substring(19,prenomlength.length);
} else {
	formFields['cadreAPrenom_14004']            = $ds045PE1.declarationGroup.cadreA.cadreAPrenom14004;
	formFields['cadreAPrenomSuite_14004']            = '';	
	
}
formFields['cadreAPrenomPage2_14004']            = $ds045PE1.declarationGroup.cadreA.cadreAPrenom14004;

//<!--DECLARANT Morale-->

var denominationlength = new String($ds045PE1.declarationGroup.cadreA.cadreADenomination14004) ; //19 caractères
if(denominationlength.length > 19){
	formFields['cadreADenomination_14004']       = $ds045PE1.declarationGroup.cadreA.cadreADenomination14004.substring(0, 19);
	formFields['cadreADenominationSuite_14004']       = $ds045PE1.declarationGroup.cadreA.cadreADenomination14004.substring(19,denominationlength.length);
} else {
	formFields['cadreADenomination_14004']       = $ds045PE1.declarationGroup.cadreA.cadreADenomination14004;
	formFields['cadreADenominationSuite_14004']       = '';	
	
}
var denominationPage2length = new String($ds045PE1.declarationGroup.cadreA.cadreADenomination14004) ; //18 caractères
if(denominationPage2length.length > 18){
	formFields['cadreADenominationPage2_14004']       = $ds045PE1.declarationGroup.cadreA.cadreADenomination14004.substring(0, 18);
	formFields['cadreADenominationPage2Suite_14004']       = $ds045PE1.declarationGroup.cadreA.cadreADenomination14004.substring(18,denominationPage2length.length);
} else {
	formFields['cadreADenominationPage2_14004']       = $ds045PE1.declarationGroup.cadreA.cadreADenomination14004;
	formFields['cadreADenominationPage2Suite_14004']       = '';	
	
}
formFields['cadreANumeroSiren_14004']        = $ds045PE1.declarationGroup.cadreA.cadreANumeroSiren14004;
//<!--DECLARANT Adresse-->
formFields['cadreAAdresse_14004']            = $ds045PE1.declarationGroup.cadreA.cadreAAdresseNumero14004  
													+ ($ds045PE1.declarationGroup.cadreA.cadreAAdresseComplement14004 ? ', '+$ds045PE1.declarationGroup.cadreA.cadreAAdresseComplement14004 :'') 
													;

formFields['cadreACP_14004']                 = $ds045PE1.declarationGroup.cadreA.cadreACP14004 ;

var communelength = new String($ds045PE1.declarationGroup.cadreA.cadreACommune14004) ; //16 caractères
if(communelength.length > 16){
	formFields['cadreACommune_14004']            = $ds045PE1.declarationGroup.cadreA.cadreACommune14004.substring(0, 16);
	formFields['cadreACommuneSuite_14004']            = $ds045PE1.declarationGroup.cadreA.cadreACommune14004.substring(16,communelength.length);
} else {
	formFields['cadreACommune_14004']            = $ds045PE1.declarationGroup.cadreA.cadreACommune14004;
	formFields['cadreACommuneSuite_14004']            = '';
}
var communePage2length = new String($ds045PE1.declarationGroup.cadreA.cadreACommune14004) ; //11 caractères
if(communePage2length.length > 11){
	formFields['cadreACommunePage2_14004']            = $ds045PE1.declarationGroup.cadreA.cadreACommune14004.substring(0, 11);
	formFields['cadreACommunePage2Suite_14004']            = $ds045PE1.declarationGroup.cadreA.cadreACommune14004.substring(11,communePage2length.length);
} else {
	formFields['cadreACommunePage2_14004']            = $ds045PE1.declarationGroup.cadreA.cadreACommune14004;
	formFields['cadreACommunePage2Suite_14004']            = '';
}

var payslength = new String($ds045PE1.declarationGroup.cadreA.cadreAPays14004) ; //19 caractères
if(payslength.length > 12){
	formFields['cadreAPays_14004']            	 = $ds045PE1.declarationGroup.cadreA.cadreAPays14004.substring(0, 12);
	formFields['cadreAPaysSuite_14004']            	 = $ds045PE1.declarationGroup.cadreA.cadreAPays14004.substring(12,payslength.length);
} else {
	formFields['cadreAPays_14004']            	 = $ds045PE1.declarationGroup.cadreA.cadreAPays14004;
	formFields['cadreAPaysSuite_14004']            	 = '';
	
}
var paysPage2length = new String($ds045PE1.declarationGroup.cadreA.cadreAPays14004) ; //5 caractères
if(paysPage2length.length > 5){
	formFields['cadreAPaysPage2_14004']            	 = $ds045PE1.declarationGroup.cadreA.cadreAPays14004.substring(0, 5);
	formFields['cadreAPaysPage2Suite_14004']            	 = $ds045PE1.declarationGroup.cadreA.cadreAPays14004.substring(5,paysPage2length.length);
} else {
	formFields['cadreAPaysPage2_14004']            	 = $ds045PE1.declarationGroup.cadreA.cadreAPays14004;
	formFields['cadreAPaysPage2Suite_14004']            	 = '';
	
}
formFields['cadreATelephone_14004']          = $ds045PE1.declarationGroup.cadreA.cadreATelephone14004 ;
formFields['cadreACourriel_14004']           = $ds045PE1.declarationGroup.cadreA.cadreACourriel14004 ;

//<!--ADRESSE MEUBLE DE TOURISME -->
formFields['cadreAAdresseMeubleTourisme_14004']  =   $ds045PE1.declarationGroup.cadreA.cadreAAdresseMeubleTourismeNumero14004 
													+ ($ds045PE1.declarationGroup.cadreA.cadreAAdresseMeubleTourismeComplement14004 ? ', '+$ds045PE1.declarationGroup.cadreA.cadreAAdresseMeubleTourismeComplement14004 :'') 
													;

formFields['cadreACPMeubleTourisme_14004']        = $ds045PE1.declarationGroup.cadreA.cadreACPMeubleTourisme14004 ;

var communeMeublelength = new String($ds045PE1.declarationGroup.cadreA.cadreACommuneMeubleTourisme14004) ; //16 caractères
if(communeMeublelength.length > 16){
	formFields['cadreACommuneMeubleTourisme_14004']   = $ds045PE1.declarationGroup.cadreA.cadreACommuneMeubleTourisme14004.substring(0, 16);
	formFields['cadreACommuneMeubleTourismeSuite_14004']   = $ds045PE1.declarationGroup.cadreA.cadreACommuneMeubleTourisme14004.substring(16,communeMeublelength.length);
} else {
	formFields['cadreACommuneMeubleTourisme_14004']   = $ds045PE1.declarationGroup.cadreA.cadreACommuneMeubleTourisme14004;
	formFields['cadreACommuneMeubleTourismeSuite_14004']   = '';
	
}

formFields['cadreACommuneMeubleTourismeEnTete_14004']   = $ds045PE1.declarationGroup.cadreA.cadreACommuneMeubleTourisme14004; 
	
var communeMeublePage2length = new String($ds045PE1.declarationGroup.cadreA.cadreACommuneMeubleTourisme14004) ; //11 caractères PAGE 2
if(communeMeublePage2length.length > 11){
	formFields['cadreACommuneMeubleTourismePage2_14004']   = $ds045PE1.declarationGroup.cadreA.cadreACommuneMeubleTourisme14004.substring(0, 11);
	formFields['cadreACommuneMeubleTourismePage2Suite_14004']   = $ds045PE1.declarationGroup.cadreA.cadreACommuneMeubleTourisme14004.substring(11,communeMeublePage2length.length);
} else {
	formFields['cadreACommuneMeubleTourismePage2_14004']   = $ds045PE1.declarationGroup.cadreA.cadreACommuneMeubleTourisme14004;
	formFields['cadreACommuneMeubleTourismePage2Suite_14004']   = '';
	
}
//Cadre B
formFields['cadreBMaisonIndividuelle_14004']        = Value('id').of($ds045PE1.declarationGroup.cadreB.identificationChambres).eq('maisonIndiv') ? true : false ;
formFields['cadreBAppartement_14004']           	= Value('id').of($ds045PE1.declarationGroup.cadreB.identificationChambres).eq('appartement') ? true : false ;
formFields['cadreBNumeroAppartement_14004']        	= $ds045PE1.declarationGroup.cadreB.cadreBNumeroAppartement14004 ? ('N°'+ $ds045PE1.declarationGroup.cadreB.cadreBNumeroAppartement14004) : '' ;
formFields['cadreBNumeroEtage_14004']             	= $ds045PE1.declarationGroup.cadreB.cadreBNumeroEtage14004  ? ('N°' + $ds045PE1.declarationGroup.cadreB.cadreBNumeroEtage14004 ): '' ;
formFields['cadreBNBPieces_14004']         			= $ds045PE1.declarationGroup.cadreB.cadreBNBPieces14004;
formFields['cadreBNBPersonnes_14004']        		= $ds045PE1.declarationGroup.cadreB.cadreBNBPersonnes14004;
formFields['cadreBDateClassement_14004']         	= $ds045PE1.declarationGroup.cadreB.cadreBDateClassement14004;
formFields['cadreBClassement_14004']        		= $ds045PE1.declarationGroup.cadreB.cadreBClassement14004;



//Cadre C
formFields['cadreCTouteAnnee_14004']       = Value('id').of($ds045PE1.declarationGroup.cadreC.periodePrevisionelle14004).eq('annee') ? true : false ;
formFields['cadreCPeriode_14004']       = ($ds045PE1.declarationGroup.cadreC.cadreCPeriode14004 ? $ds045PE1.declarationGroup.cadreC.cadreCPeriode14004 :'') ;
											
//Signature
formFields['signatureFaitA_14004']           = $ds045PE1.signatureGroup.signatureFaitA14004;
formFields['signatureLe_14004']            = $ds045PE1.signatureGroup.signatureDate14004;
formFields['signature_14004']                = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";


//var pdfModel = nash.doc.load('models/cerfa_11542-05.pdf').apply(formFields);

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qpxxxPEx.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $ds045PE1.signatureGroup.signatureDate14004,
		autoriteHabilitee :"Mairie de " + $ds045PE1.declarationGroup.cadreA.cadreACommuneMeubleTourisme14004 ,
		demandeContexte : "Déclaration de meublé de tourisme.",
		civiliteNomPrenom : declarant14004
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/cerfa_14004-03.pdf') //
	.apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Gite_rural_Declaration.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Gîte rural - Déclaration de meublé de tourisme',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Gîte rural - Déclaration de meublé de tourisme.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});

