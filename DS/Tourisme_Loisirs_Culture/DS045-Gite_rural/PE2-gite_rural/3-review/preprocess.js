var formFields = {};
//Cadre 1
formFields['cadre1Denomination_11819']     = $ds045PE2.declarationGroup.cadre1.cadre1Denomination11819;
formFields['cadre1AdresseMeuble_11819']    = $ds045PE2.declarationGroup.cadre1.cadre1AdresseMeubleNumero11819
												+ ($ds045PE2.declarationGroup.cadre1.cadre1AdresseMeubleComplement11819 ? ', ' + $ds045PE2.declarationGroup.cadre1.cadre1AdresseMeubleComplement11819 :'' );
formFields['cadre1CPMeuble_11819']         = $ds045PE2.declarationGroup.cadre1.cadre1CPMeuble11819;
formFields['cadre1CommmuneMeuble_11819']   = $ds045PE2.declarationGroup.cadre1.cadre1CommmuneMeuble11819;
formFields['cadre1TelephoneMeuble_11819']  = $ds045PE2.declarationGroup.cadre1.cadre1TelephoneMeuble11819;

//Cadre 2
var declarant11819 =  (Value('id').of($ds045PE2.declarationGroup.cadre2.declarantCivilite11819).eq('madame') ? 'Mme' :'Mr') +' '+ $ds045PE2.declarationGroup.cadre2.cadre2Nom11819 +' ' + $ds045PE2.declarationGroup.cadre2.cadre2Prenom11819;
formFields['cadre2Madame_11819']           = Value('id').of($ds045PE2.declarationGroup.cadre2.declarantCivilite11819).eq('madame') ? true : false;
formFields['cadre2Mademoiselle_11819']     = '' //$ds045PE2.declarationGroup.cadre2.cadre2Mademoiselle11819;
formFields['cadre2Monsieur_11819']         = Value('id').of($ds045PE2.declarationGroup.cadre2.declarantCivilite11819).eq('monsieur') ? true : false;
formFields['cadre2Nom_11819']              = $ds045PE2.declarationGroup.cadre2.cadre2Nom11819;
formFields['cadre2Prenom_11819']           = $ds045PE2.declarationGroup.cadre2.cadre2Prenom11819;
formFields['cadre2Statut_11819']           = $ds045PE2.declarationGroup.cadre2.cadre2Statut11819;

formFields['cadre2Adresse_11819']          = $ds045PE2.declarationGroup.cadre2.cadre2AdresseNumero11819
												+ ($ds045PE2.declarationGroup.cadre2.cadre2AdresseComplement11819 ? ', ' + $ds045PE2.declarationGroup.cadre2.cadre2AdresseComplement11819 :'') ;

formFields['cadre2CP_11819']               = $ds045PE2.declarationGroup.cadre2.cadre2CP11819;
formFields['cadre2Commune11819']           = $ds045PE2.declarationGroup.cadre2.cadre2Commune11819;
formFields['cadre2Pays_11819']             = $ds045PE2.declarationGroup.cadre2.cadre2Pays11819;
formFields['cadre2Telephone1_11819']       = $ds045PE2.declarationGroup.cadre2.cadre2Telephone111819;
formFields['cadre2Telephone2_11819']       = $ds045PE2.declarationGroup.cadre2.cadre2Telephone211819;
formFields['cadre2Courriel_11819']         = $ds045PE2.declarationGroup.cadre2.cadre2Courriel11819;

//Cadre 3
formFields['cadre3NonClasse_11819']        =  Value('id').of($ds045PE2.declarationGroup.cadre3.cadre3Etoile11819).eq('nonClasse') ? true : false ;
formFields['cadre3Etoile_11819']           = Value('id').of($ds045PE2.declarationGroup.cadre3.cadre3Etoile11819).eq('1etoile') ? '1' :  
												(Value('id').of($ds045PE2.declarationGroup.cadre3.cadre3Etoile11819).eq('2etoiles') ? '2' :
													(Value('id').of($ds045PE2.declarationGroup.cadre3.cadre3Etoile11819).eq('3etoiles') ? '3' : 
														(Value('id').of($ds045PE2.declarationGroup.cadre3.cadre3Etoile11819).eq('4etoiles') ? '4' : 
															(Value('id').of($ds045PE2.declarationGroup.cadre3.cadre3Etoile11819).eq('5etoiles') ? '5' : '')))) ;
formFields['cadre3EtoileDemande_11819']    = Value('id').of($ds045PE2.declarationGroup.cadre3.cadre3EtoileDemande11819).eq('1etoile') ? '1' :  
												(Value('id').of($ds045PE2.declarationGroup.cadre3.cadre3EtoileDemande11819).eq('2etoiles') ? '2' : 
													(Value('id').of($ds045PE2.declarationGroup.cadre3.cadre3EtoileDemande11819).eq('3etoiles') ? '3' : 
														(Value('id').of($ds045PE2.declarationGroup.cadre3.cadre3EtoileDemande11819).eq('4etoiles') ? '4' : 
															(Value('id').of($ds045PE2.declarationGroup.cadre3.cadre3EtoileDemande11819).eq('5etoiles') ? '5' :'' ))));
formFields['cadre3Capacite_11819']         = $ds045PE2.declarationGroup.cadre3.cadre3Capacite11819;

//Cadre4
formFields['cadre4Superficie_11819']       = $ds045PE2.declarationGroup.cadre4.cadre4Superficie11819;
formFields['cadre4NBPieces_11819']         = $ds045PE2.declarationGroup.cadre4.cadre4NBPieces11819;
formFields['cadre4Neuve_11819']            = Value('id').of($ds045PE2.declarationGroup.cadre4.cadre4Construction11819).eq('neuve') ? true : false ;
formFields['cadre4Recente_11819']          = Value('id').of($ds045PE2.declarationGroup.cadre4.cadre4Construction11819).eq('recente') ? true : false ;
formFields['cadre4Ancienne_11819']         = Value('id').of($ds045PE2.declarationGroup.cadre4.cadre4Construction11819).eq('ancienne') ? true : false ;
formFields['cadre4Appartement_11819']      = Value('id').of($ds045PE2.declarationGroup.cadre4.cadre4TypeLogement11819).eq('appartement') ? true : false ;
formFields['cadre4Studio_11819']           = Value('id').of($ds045PE2.declarationGroup.cadre4.cadre4TypeLogement11819).eq('studio') ? true : false ;
formFields['cadre4Villa_11819']            = Value('id').of($ds045PE2.declarationGroup.cadre4.cadre4TypeLogement11819).eq('villa') ? true : false ;
formFields['cadre4Autre_11819']            = Value('id').of($ds045PE2.declarationGroup.cadre4.cadre4TypeLogement11819).eq('autre') ? true : false ;
formFields['cadre4AutrePrecision_11819']   = $ds045PE2.declarationGroup.cadre4.cadre4AutrePrecision11819;
formFields['cadre4AnneeOUI_11819']         = $ds045PE2.declarationGroup.cadre4.cadre4Annee11819 ? true : false;
formFields['cadre4AnneeNON_11819']         = $ds045PE2.declarationGroup.cadre4.cadre4Annee11819 ? false : true;

//Cadre 5
formFields['cadre5NomOrganise_11819']      = $ds045PE2.declarationGroup.cadre5.cadre5NomOrganise11819;
formFields['cadre5AdresseOrganisme_11819'] = $ds045PE2.declarationGroup.cadre5.cadre5AdresseOrganismeNumero11819
												+ ($ds045PE2.declarationGroup.cadre5.cadre5AdresseOrganismeComplement11819 ? ', ' + $ds045PE2.declarationGroup.cadre5.cadre5AdresseOrganismeComplement11819 :'')
												+ ($ds045PE2.declarationGroup.cadre5.cadre5AdresseOrganismeCP11819 ? ' ' + $ds045PE2.declarationGroup.cadre5.cadre5AdresseOrganismeCP11819 :'')
												+ ', ' +$ds045PE2.declarationGroup.cadre5.cadre5AdresseOrganismeCommune11819 
												+ ($ds045PE2.declarationGroup.cadre5.cadre5AdresseOrganismePays11819 ? ' / ' + $ds045PE2.declarationGroup.cadre5.cadre5AdresseOrganismePays11819 :'')
												+ ($ds045PE2.declarationGroup.cadre5.cadre5AdresseOrganismeTelephone111819 ? ' ' + $ds045PE2.declarationGroup.cadre5.cadre5AdresseOrganismeTelephone111819 :'' );
formFields['cadre5AdresseOrganismeCourriel_11819']  = $ds045PE2.declarationGroup.cadre5.cadre5AdresseOrganismeCourriel111819;

//Signature
formFields['signatureFaitA_11819']           = $ds045PE2.signatureGroup.signatureFaitA11819;
formFields['signatureLe_11819']           	 = $ds045PE2.signatureGroup.signatureDate11819;
formFields['signature_11819']                = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";


//var pdfModel = nash.doc.load('models/cerfa_11542-05.pdf').apply(formFields);

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qpxxxPEx.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $ds045PE2.signatureGroup.signatureDate11819,
		autoriteHabilitee :"Organismes accrédités par le Comité français d'accréditation ou organismes agréés",
		demandeContexte : "Classement du gîte.",
		civiliteNomPrenom : declarant11819
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/cerfa_11819-03.pdf') //
	.apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Gite_rural_Classement.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Gîte rural -  Classement du gîte',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Gîte rural -  Classement du gîte.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
