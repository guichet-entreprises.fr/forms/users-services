// PJ JQPA

var pjactivite1=$jqpa.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle1.jqpaSituation;
var pjactivite2=$jqpa.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle2.jqpaSituation;
var pjactivite3=$jqpa.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle3.jqpaSituation;
var pjactivite4=$jqpa.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle4.jqpaSituation;

if(Value('id').of(pjactivite1).eq('JqpaSituationDiplome')) {
	attachment('pjDiplomes1', 'pjDiplomes1', { mandatory:"true"});
}
if(Value('id').of(pjactivite2).eq('JqpaSituationDiplome')) {
	attachment('pjDiplomes2', 'pjDiplomes2', { mandatory:"true"});
}
if(Value('id').of(pjactivite3).eq('JqpaSituationDiplome')) {
	attachment('pjDiplomes3', 'pjDiplomes3', { mandatory:"true"});
}
if(Value('id').of(pjactivite4).eq('JqpaSituationDiplome')) {
	attachment('pjDiplomes4', 'pjDiplomes4', { mandatory:"true"});
}


if(Value('id').of(pjactivite1).eq('JqpaSituationExperience')) {
	attachment('pjPiecesUtiles1', 'pjPiecesUtiles1', { mandatory:"true"});
}																									
if(Value('id').of(pjactivite2).eq('JqpaSituationExperience')) {
	attachment('pjPiecesUtiles2', 'pjPiecesUtiles2', { mandatory:"true"});
}
if(Value('id').of(pjactivite3).eq('JqpaSituationExperience')) {
	attachment('pjPiecesUtiles3', 'pjPiecesUtiles3', { mandatory:"true"});
}
if(Value('id').of(pjactivite4).eq('JqpaSituationExperience')) {
	attachment('pjPiecesUtiles4', 'pjPiecesUtiles4', { mandatory:"true"});
}


var pjSignataire=$jqpa.cadreSignatureGroup.cadreSignature.soussigne;
/**  Si le signataire c'est le déclarant  **/
if(Value('id').of(pjSignataire).eq('FormaliteSignataireQualiteDeclarant')) {
	attachment('pjIDDeclarantSignataire', 'pjIDDeclarantSignataire', { mandatory:"true"});
}else if(Value('id').of(pjSignataire).eq('FormaliteSignataireQualiteMandataire')) {

/**  Si le signataire c'est le Mandataire **/
	attachment('pjIDMandataireSignataire', 'pjIDMandataireSignataire', { mandatory:"true"});
	attachment('pjPouvoir', 'pjPouvoir', { mandatory:"true"});

}else if(Value('id').of(pjSignataire).eq('FormaliteSignataireQualiteRepresentant')) {
	/** Si le signataire c'est le Representant  **/
	attachment('pjIDRepresentantSignataire', 'pjIDRepresentantSignataire', { mandatory:"true"});
}