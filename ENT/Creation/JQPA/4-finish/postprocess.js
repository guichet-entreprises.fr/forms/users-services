// prepare info to send to pushingBox
pushingbox();	

// prepare info to send


var cerfa= "/3-review/generated/generated.record-1-cerfa.pdf";
var identitePersonne = $jqpa.identitePersonne;
var adresse = identitePersonne.cadreAdresseProfessionnelle;

_log.info("algo is {}",algo);
var codeCommune = adresse.communeAdresseSiege.getId();
_log.info("codeCommune is {}",codeCommune);
var algo = "trouver_rm";


	
/** Other files */

function findUserAttachments() {
	var values = [];
	var metas = nash.record.meta();

	log.debug('meta list => {}', null == metas ? null : metas.metas);

	if (null != metas && null != metas.metas) {
		var valueMap = {};
		metas.metas.forEach(function (meta) {
			log.debug('  ---> meta {} : {}', meta.name, meta.value);
			if ('userAttachments' == meta.name) {
				valueMap[meta.value] = true;
			}
		})
		Object.keys(valueMap).forEach(function (elm) {
			values.push({
				id: elm.replaceAll('^(.*/)([^/]+)$', '$2'), //
				label: elm //
			});
		});
	}

	return values;
}

var data = nash.instance.load("output.xml");

data.bind("parameters",{
	"algo" : {
		"algo" : algo,
		"params" : [{
			"key" : "codeCommune",
			"value" : codeCommune
		}]
	},
	"attachment" : {
		"cerfa" : cerfa,
		"others" : findUserAttachments()
	}
});

data.bind("result",{
	"funcId" : ""
})
/**
data.bind("result", {
	"denomination" : denomination
});  **/