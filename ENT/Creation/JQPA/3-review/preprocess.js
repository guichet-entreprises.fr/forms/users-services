var formFieldsPers1 = {};
var formFieldsPers2 = {};
var formFieldsPers3 = {};
var formFieldsPers4 = {};

var identiteJuridique = $jqpa.identitePersonne.personnaliteJuridiqueDirigeant.personnaliteJuridique;
var identitePhysique = $jqpa.identitePersonne.personnePhysiqueRappelIdentite;
var identiteMorale = $jqpa.identitePersonne.personneMoraleRappelIdentite;

var justificationQP1 = $jqpa.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle1;
var justificationQP2 = $jqpa.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle2;
var justificationQP3 = $jqpa.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle3;
var justificationQP4 = $jqpa.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle4;
var observationJqpa = $jqpa.cadreRensCompGroup.cadreRensComp;
var signatureJqpa = $jqpa.cadreSignatureGroup.cadreSignature;
var signatureAdresseMandataireJqpa=signatureJqpa.adresseMandataire
function pad(s) { return (s < 10) ? '0' + s : s; }

formFieldsPers1['jqpa_formulaire']                              = true;
if (Value('id').of(justificationQP1.autresActiviteADeclarer).eq('autresActiviteADeclarerOui')) {
    formFieldsPers2['jqpa_intercalaire']                        = true;
    if (Value('id').of(justificationQP2.autresActiviteADeclarer).eq('autresActiviteADeclarerOui')) {
        formFieldsPers3['jqpa_intercalaire']                    = true;
        if (Value('id').of(justificationQP3.autresActiviteADeclarer).eq('autresActiviteADeclarerOui')) {
            formFieldsPers4['jqpa_intercalaire']                = true;
        }
    }
}

 /**Cerfa 1**/
    
if(Value('id').of(identiteJuridique).eq('personnePhysique')){
    formFieldsPers1['entreprise_dirigeants_ppNomNaissance']         = identitePhysique.personnePhysiqueNomNaissance;
    formFieldsPers1['entreprise_dirigeants_ppNomUsage']             = identitePhysique.personnePhysiqueNomUsage;
    var prenoms=[];
    for ( i = 0; i < identitePhysique.personnePhysiquePrenom.size() ; i++ ){prenoms.push(identitePhysique.personnePhysiquePrenom[i]);}
    formFieldsPers1['entreprise_dirigeants_ppPrenom']               = prenoms.toString();
    
    if (identitePhysique.personnePhysiqueDateNaissance !== null ) {
        var dateTmp = new Date(parseInt(identitePhysique.personnePhysiqueDateNaissance.getTimeInMillis()));
        var dateModif = pad(dateTmp.getDate().toString());
        var month = dateTmp.getMonth() + 1;
        dateModif = dateModif.concat(pad(month.toString()));
        dateModif = dateModif.concat(dateTmp.getFullYear().toString());
        formFieldsPers1['entreprise_dirigeants_ppDateNaissance']        = dateModif;
    }
    
    formFieldsPers1['entreprise_siren']                             = identitePhysique.sirenPhysique.split(' ').join('');
    formFieldsPers1['jqpa_activiteJqpa_PP']                         = justificationQP1.jqpaActiviteJqpaPP;
    formFieldsPers1['jqpa_aqpaSituationPP_diplome']                 = (Value('id').of(justificationQP1.jqpaSituation).eq('JqpaSituationDiplome') || Value('id').of(justificationQP1.jqpaSituation).eq('JqpaSituationExperience') ) ? true : false;
    formFieldsPers1['jqpa_qualitePersonneQualifieePP_declarant']    = (Value('id').of(justificationQP1.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPDeclarant')) ? true : false;
    formFieldsPers1['jqpa_qualitePersonneQualifieePP_conjoint']     = (Value('id').of(justificationQP1.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPConjoint')) ? true : false;
    formFieldsPers1['jqpa_qualitePersonneQualifieePP_salarie']      = (Value('id').of(justificationQP1.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPSalarie')) ? true : false;
    formFieldsPers1['jqpa_qualitePersonneQualifieePP_autre']        = (Value('id').of(justificationQP1.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPAutre')) ? true : false;
    
    if((Value('id').of(justificationQP1.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPAutre'))|| (Value('id').of(justificationQP1.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPSalarie'))){
    	
	    formFieldsPers1['jqpa_qualitePersonneQualifieePPAutre']         = justificationQP1.qualitePersonneQualifieePP.jqpaQualitePersonneQualifieePPChampAutre;
	    
	    formFieldsPers1['jqpa_identitePPNomNaissance']                  = justificationQP1.identitePersonneQualifiee.jqpaIdentitePPNomNaissance;
	    formFieldsPers1['jqpa_identitePPNomUsage']                      = justificationQP1.identitePersonneQualifiee.jqpaIdentitePPNomUsage;
	    var prenoms=[];
	    for ( i = 0; i < justificationQP1.identitePersonneQualifiee.jqpaIdentitePPPrenom.size() ; i++ ){
	        prenoms.push(justificationQP1.identitePersonneQualifiee.jqpaIdentitePPPrenom[i]);}   
	    formFieldsPers1['jqpa_identitePPPrenom']                        = prenoms.toString();
	    
	    
	    if (justificationQP1.identitePersonneQualifiee.jqpaIdentitePPDateNaissance !== null ) {
	        var dateTmp = new Date(parseInt(justificationQP1.identitePersonneQualifiee.jqpaIdentitePPDateNaissance.getTimeInMillis()));
	        var dateModif = pad(dateTmp.getDate().toString());
	        var month = dateTmp.getMonth() + 1;
	        dateModif = dateModif.concat(pad(month.toString()));
	        dateModif = dateModif.concat(dateTmp.getFullYear().toString());
		    formFieldsPers1['jqpa_identitePPDateNaissance']                 = dateModif;

	    }
	    
	    
	    formFieldsPers1['jqpa_identitePPLieuNaissancePays']             = (justificationQP1.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune != null ? justificationQP1.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune : justificationQP1.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceVille)+"/"+justificationQP1.identitePersonneQualifiee.jqpaIdentitePPLieuNaissancePays;
	    formFieldsPers1['jqpa_identitePPDepartement']                   = justificationQP1.identitePersonneQualifiee.jqpaIdentitePPDepartement !=null ? justificationQP1.identitePersonneQualifiee.jqpaIdentitePPDepartement.getId() : "";
	    
    }
    
    /**Cerfa 2**/
    if (Value('id').of(justificationQP1.autresActiviteADeclarer).eq('autresActiviteADeclarerOui')) {

        formFieldsPers2['entreprise_dirigeants_ppNomNaissance']         = identitePhysique.personnePhysiqueNomNaissance;
        formFieldsPers2['entreprise_dirigeants_ppNomUsage']             = identitePhysique.personnePhysiqueNomUsage;
        var prenoms=[];
        for ( i = 0; i < identitePhysique.personnePhysiquePrenom.size() ; i++ ){prenoms.push(identitePhysique.personnePhysiquePrenom[i]);}
        formFieldsPers2['entreprise_dirigeants_ppPrenom']               = prenoms.toString();
        
        if (identitePhysique.personnePhysiqueDateNaissance !== null ) {
            var dateTmp = new Date(parseInt(identitePhysique.personnePhysiqueDateNaissance.getTimeInMillis()));
            var dateModif = pad(dateTmp.getDate().toString());
            var month = dateTmp.getMonth() + 1;
            dateModif = dateModif.concat(pad(month.toString()));
            dateModif = dateModif.concat(dateTmp.getFullYear().toString());
            formFieldsPers2['entreprise_dirigeants_ppDateNaissance']        = dateModif;
        }

        formFieldsPers2['entreprise_siren']                             = identitePhysique.sirenPhysique.split(' ').join('');
        
        formFieldsPers2['jqpa_activiteJqpa_PP']                         = justificationQP2.jqpaActiviteJqpaPP;
        formFieldsPers2['jqpa_aqpaSituationPP_diplome']                 = (Value('id').of(justificationQP2.jqpaSituation).eq('JqpaSituationDiplome') || Value('id').of(justificationQP2.jqpaSituation).eq('JqpaSituationExperience') ) ? true : false;
        formFieldsPers2['jqpa_qualitePersonneQualifieePP_declarant']    = (Value('id').of(justificationQP2.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPDeclarant')) ? true : false;
        formFieldsPers2['jqpa_qualitePersonneQualifieePP_conjoint']     = (Value('id').of(justificationQP2.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPConjoint')) ? true : false;
        formFieldsPers2['jqpa_qualitePersonneQualifieePP_salarie']      = (Value('id').of(justificationQP2.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPSalarie')) ? true : false;
        formFieldsPers2['jqpa_qualitePersonneQualifieePP_autre']        = (Value('id').of(justificationQP2.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPAutre')) ? true : false;

        if((Value('id').of(justificationQP2.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPAutre'))|| (Value('id').of(justificationQP2.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPSalarie'))){

	        formFieldsPers2['jqpa_qualitePersonneQualifieePPAutre']         = justificationQP2.qualitePersonneQualifieePP.jqpaQualitePersonneQualifieePPChampAutre;
	        formFieldsPers2['jqpa_identitePPNomNaissance']                  = justificationQP2.identitePersonneQualifiee.jqpaIdentitePPNomNaissance;
	        formFieldsPers2['jqpa_identitePPNomUsage']                      = justificationQP2.identitePersonneQualifiee.jqpaIdentitePPNomUsage;
	        var prenoms=[];
	        for ( i = 0; i < justificationQP2.identitePersonneQualifiee.jqpaIdentitePPPrenom.size() ; i++ ){
	            prenoms.push(justificationQP2.identitePersonneQualifiee.jqpaIdentitePPPrenom[i]);}   
	        formFieldsPers2['jqpa_identitePPPrenom']                        = prenoms.toString();
	        
		    if (justificationQP2.identitePersonneQualifiee.jqpaIdentitePPDateNaissance !== null ) {
		        var dateTmp = new Date(parseInt(justificationQP2.identitePersonneQualifiee.jqpaIdentitePPDateNaissance.getTimeInMillis()));
		        var dateModif = pad(dateTmp.getDate().toString());
		        var month = dateTmp.getMonth() + 1;
		        dateModif = dateModif.concat(pad(month.toString()));
		        dateModif = dateModif.concat(dateTmp.getFullYear().toString());
			    formFieldsPers2['jqpa_identitePPDateNaissance']                 = dateModif;

		    }
	        
	        formFieldsPers2['jqpa_identitePPLieuNaissancePays']             = (justificationQP2.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune != null ? justificationQP2.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune : justificationQP2.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceVille)+"/"+justificationQP2.identitePersonneQualifiee.jqpaIdentitePPLieuNaissancePays;
	        formFieldsPers2['jqpa_identitePPDepartement']                   = justificationQP2.identitePersonneQualifiee.jqpaIdentitePPDepartement !=null ? justificationQP2.identitePersonneQualifiee.jqpaIdentitePPDepartement.getId() : "";
        }

        /**Cerfa 3**/
        if (Value('id').of(justificationQP2.autresActiviteADeclarer).eq('autresActiviteADeclarerOui')) {

            formFieldsPers3['entreprise_dirigeants_ppNomNaissance']         = identitePhysique.personnePhysiqueNomNaissance;
            formFieldsPers3['entreprise_dirigeants_ppNomUsage']             = identitePhysique.personnePhysiqueNomUsage;
            var prenoms=[];
            for ( i = 0; i < identitePhysique.personnePhysiquePrenom.size() ; i++ ){prenoms.push(identitePhysique.personnePhysiquePrenom[i]);}
            formFieldsPers3['entreprise_dirigeants_ppPrenom']               = prenoms.toString();
           
            if (identitePhysique.personnePhysiqueDateNaissance !== null ) {
                var dateTmp = new Date(parseInt(identitePhysique.personnePhysiqueDateNaissance.getTimeInMillis()));
                var dateModif = pad(dateTmp.getDate().toString());
                var month = dateTmp.getMonth() + 1;
                dateModif = dateModif.concat(pad(month.toString()));
                dateModif = dateModif.concat(dateTmp.getFullYear().toString());
                formFieldsPers3['entreprise_dirigeants_ppDateNaissance']        = dateModif;
            }
            
            formFieldsPers3['entreprise_siren']                             = identitePhysique.sirenPhysique.split(' ').join('');

            formFieldsPers3['jqpa_activiteJqpa_PP']                         = justificationQP3.jqpaActiviteJqpaPP;
            formFieldsPers3['jqpa_aqpaSituationPP_diplome']                 = (Value('id').of(justificationQP3.jqpaSituation).eq('JqpaSituationDiplome') || Value('id').of(justificationQP3.jqpaSituation).eq('JqpaSituationExperience') ) ? true : false;
            formFieldsPers3['jqpa_qualitePersonneQualifieePP_declarant']    = (Value('id').of(justificationQP3.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPDeclarant')) ? true : false;
            formFieldsPers3['jqpa_qualitePersonneQualifieePP_conjoint']     = (Value('id').of(justificationQP3.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPConjoint')) ? true : false;
            formFieldsPers3['jqpa_qualitePersonneQualifieePP_salarie']      = (Value('id').of(justificationQP3.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPSalarie')) ? true : false;
            formFieldsPers3['jqpa_qualitePersonneQualifieePP_autre']        = (Value('id').of(justificationQP3.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPAutre')) ? true : false;
            
            if((Value('id').of(justificationQP3.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPAutre'))|| (Value('id').of(justificationQP3.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPSalarie'))){
	            formFieldsPers3['jqpa_qualitePersonneQualifieePPAutre']         = justificationQP3.qualitePersonneQualifieePP.jqpaQualitePersonneQualifieePPChampAutre;
	            formFieldsPers3['jqpa_identitePPNomNaissance']                  = justificationQP3.identitePersonneQualifiee.jqpaIdentitePPNomNaissance;
	            formFieldsPers3['jqpa_identitePPNomUsage']                      = justificationQP3.identitePersonneQualifiee.jqpaIdentitePPNomUsage;
	            var prenoms=[];
	            for ( i = 0; i < justificationQP3.identitePersonneQualifiee.jqpaIdentitePPPrenom.size() ; i++ ){
	                prenoms.push(justificationQP3.identitePersonneQualifiee.jqpaIdentitePPPrenom[i]);}   
	
	            formFieldsPers3['jqpa_identitePPPrenom']                        = prenoms.toString();
	            
	    	    if (justificationQP3.identitePersonneQualifiee.jqpaIdentitePPDateNaissance !== null ) {
	    	        var dateTmp = new Date(parseInt(justificationQP3.identitePersonneQualifiee.jqpaIdentitePPDateNaissance.getTimeInMillis()));
	    	        var dateModif = pad(dateTmp.getDate().toString());
	    	        var month = dateTmp.getMonth() + 1;
	    	        dateModif = dateModif.concat(pad(month.toString()));
	    	        dateModif = dateModif.concat(dateTmp.getFullYear().toString());
	    		    formFieldsPers3['jqpa_identitePPDateNaissance']                 = dateModif;

	    	    }
	            formFieldsPers3['jqpa_identitePPLieuNaissancePays']             = (justificationQP3.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune != null ? justificationQP3.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune : justificationQP3.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceVille)+"/"+justificationQP3.identitePersonneQualifiee.jqpaIdentitePPLieuNaissancePays;
	            formFieldsPers3['jqpa_identitePPDepartement']                   = justificationQP3.identitePersonneQualifiee.jqpaIdentitePPDepartement !=null ? justificationQP3.identitePersonneQualifiee.jqpaIdentitePPDepartement.getId() : "";
            }
            
            /**Cerfa 4**/
            if (Value('id').of(justificationQP3.autresActiviteADeclarer).eq('autresActiviteADeclarerOui')) {
               
                formFieldsPers4['entreprise_dirigeants_ppNomNaissance']         = identitePhysique.personnePhysiqueNomNaissance;
                formFieldsPers4['entreprise_dirigeants_ppNomUsage']             = identitePhysique.personnePhysiqueNomUsage;
                var prenoms=[];
                for ( i = 0; i < identitePhysique.personnePhysiquePrenom.size() ; i++ ){prenoms.push(identitePhysique.personnePhysiquePrenom[i]);}
                formFieldsPers4['entreprise_dirigeants_ppPrenom']               = prenoms.toString();                
                if (identitePhysique.personnePhysiqueDateNaissance !== null ) {
                    var dateTmp = new Date(parseInt(identitePhysique.personnePhysiqueDateNaissance.getTimeInMillis()));
                    var dateModif = pad(dateTmp.getDate().toString());
                    var month = dateTmp.getMonth() + 1;
                    dateModif = dateModif.concat(pad(month.toString()));
                    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
                    formFieldsPers3['entreprise_dirigeants_ppDateNaissance']        = dateModif;
                }
                formFieldsPers4['entreprise_siren']                             = identitePhysique.sirenPhysique.split(' ').join('');

                formFieldsPers4['jqpa_activiteJqpa_PP']                         = justificationQP4.jqpaActiviteJqpaPP;
                formFieldsPers4['jqpa_aqpaSituationPP_diplome']                 = (Value('id').of(justificationQP4.jqpaSituation).eq('JqpaSituationDiplome') || Value('id').of(justificationQP4.jqpaSituation).eq('JqpaSituationExperience') ) ? true : false;
                formFieldsPers4['jqpa_qualitePersonneQualifieePP_declarant']    = (Value('id').of(justificationQP4.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPDeclarant')) ? true : false;
                formFieldsPers4['jqpa_qualitePersonneQualifieePP_conjoint']     = (Value('id').of(justificationQP4.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPConjoint')) ? true : false;
                formFieldsPers4['jqpa_qualitePersonneQualifieePP_salarie']      = (Value('id').of(justificationQP4.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPSalarie')) ? true : false;
                formFieldsPers4['jqpa_qualitePersonneQualifieePP_autre']        = (Value('id').of(justificationQP4.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPAutre')) ? true : false;
                
                if((Value('id').of(justificationQP4.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPAutre'))|| (Value('id').of(justificationQP4.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPSalarie'))){

	                formFieldsPers4['jqpa_qualitePersonneQualifieePPAutre']         = justificationQP4.qualitePersonneQualifieePP.jqpaQualitePersonneQualifieePPChampAutre;
	                formFieldsPers4['jqpa_identitePPNomNaissance']                  = justificationQP4.identitePersonneQualifiee.jqpaIdentitePPNomNaissance;
	                formFieldsPers4['jqpa_identitePPNomUsage']                      = justificationQP4.identitePersonneQualifiee.jqpaIdentitePPNomUsage;
	                var prenoms=[];
	                for ( i = 0; i < justificationQP4.identitePersonneQualifiee.jqpaIdentitePPPrenom.size() ; i++ ){
	                    prenoms.push(justificationQP4.identitePersonneQualifiee.jqpaIdentitePPPrenom[i]);}   
	
	                formFieldsPers4['jqpa_identitePPPrenom']                        = prenoms.toString();
	                
	        	    if (justificationQP4.identitePersonneQualifiee.jqpaIdentitePPDateNaissance !== null ) {
	        	        var dateTmp = new Date(parseInt(justificationQP4.identitePersonneQualifiee.jqpaIdentitePPDateNaissance.getTimeInMillis()));
	        	        var dateModif = pad(dateTmp.getDate().toString());
	        	        var month = dateTmp.getMonth() + 1;
	        	        dateModif = dateModif.concat(pad(month.toString()));
	        	        dateModif = dateModif.concat(dateTmp.getFullYear().toString());
	        		    formFieldsPers4['jqpa_identitePPDateNaissance']                 = dateModif;

	        	    }
	                
	                formFieldsPers4['jqpa_identitePPLieuNaissancePays']             = justificationQP4.identitePersonneQualifiee.jqpaIdentitePPLieuNaissancePays+"/"+(justificationQP4.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune != null ? justificationQP4.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune : justificationQP4.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceVille);
	                formFieldsPers4['jqpa_identitePPDepartement']                   = justificationQP4.identitePersonneQualifiee.jqpaIdentitePPDepartement !=null ? justificationQP4.identitePersonneQualifiee.jqpaIdentitePPDepartement.getId() : "";
                }
            }

        }

    }






}else if(Value('id').of(identiteJuridique).eq('personneMorale')){
    /**Cerfa 1**/
    formFieldsPers1['jqpa_entreprise_denomination']                 = identiteMorale.personneMoraleDenomination;
    formFieldsPers1['jqpa_entreprise_formeJuridique']               = identiteMorale.personneMoraleFormeJuridique;
    formFieldsPers1['jqpa_entreprise_siren']                        = identiteMorale.sirenMorale.split(' ').join('');


    formFieldsPers1['jqpa_pm_activite']                             = justificationQP1.jqpaActiviteJqpaPP;
    formFieldsPers1['jqpa_aqpaSituationPM_diplome']                 = (Value('id').of(justificationQP1.jqpaSituation).eq('JqpaSituationDiplome') || Value('id').of(justificationQP1.jqpaSituation).eq('JqpaSituationExperience') ) ? true : false;


    formFieldsPers1['jqpa_qualitePersonneQualifieePM_representant'] = (Value('id').of(justificationQP1.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPRepresentantLegal')) ? true : false;
    formFieldsPers1['jqpa_qualitePersonneQualifieePM_conjoint']     = (Value('id').of(justificationQP1.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPConjoint')) ? true : false;
    formFieldsPers1['jqpa_qualitePersonneQualifieePM_salarie']      = (Value('id').of(justificationQP1.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPSalarie')) ? true : false;
    formFieldsPers1['jqpa_qualitePersonneQualifieePM_autre']        = (Value('id').of(justificationQP1.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPAutre')) ? true : false;
    formFieldsPers1['jqpa_qualitePersonneQualifieePMAutre']         = justificationQP1.qualitePersonneQualifieePP.jqpaQualitePersonneQualifieePPChampAutre;

    if(Value('id').of(justificationQP1.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPAutre') || Value('id').of(justificationQP1.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPSalarie')){
	    formFieldsPers1['jqpa_identitePMNomNaissance']                  = justificationQP1.identitePersonneQualifiee.jqpaIdentitePPNomNaissance;
	    formFieldsPers1['jqpa_identitePMNomUsage']                      = justificationQP1.identitePersonneQualifiee.jqpaIdentitePPNomUsage;
	    var prenoms=[];
	    for ( i = 0; i < justificationQP1.identitePersonneQualifiee.jqpaIdentitePPPrenom.size() ; i++ ){prenoms.push(justificationQP1.identitePersonneQualifiee.jqpaIdentitePPPrenom[i]);}                            
	    formFieldsPers1['jqpa_identitePMPrenom']                        = prenoms.toString();
	
	    if (justificationQP1.identitePersonneQualifiee.jqpaIdentitePPDateNaissance !== null ) {
	        var dateTmp = new Date(parseInt(justificationQP1.identitePersonneQualifiee.jqpaIdentitePPDateNaissance.getTimeInMillis()));
	        var dateModif = pad(dateTmp.getDate().toString());
	        var month = dateTmp.getMonth() + 1;
	        dateModif = dateModif.concat(pad(month.toString()));
	        dateModif = dateModif.concat(dateTmp.getFullYear().toString());
		    formFieldsPers1['jqpa_identitePMDateNaissance']                 = dateModif;
	
	    }
	    
	    formFieldsPers1['jqpa_identitePMLieuNaissancePays']             = (justificationQP1.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune != null ? justificationQP1.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune : justificationQP1.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceVille)+"/"+justificationQP1.identitePersonneQualifiee.jqpaIdentitePPLieuNaissancePays;
	    formFieldsPers1['jqpa_identitePMDepartement']					= justificationQP1.identitePersonneQualifiee.jqpaIdentitePPDepartement !=null ? justificationQP1.identitePersonneQualifiee.jqpaIdentitePPDepartement.getId() : "";
	
	    //cerfaFields['jqpa_aqpaSituationPM_engagement']              = false;
    }
    /**Cerfa 2**/
    if (Value('id').of(justificationQP1.autresActiviteADeclarer).eq('autresActiviteADeclarerOui')) {
        
        formFieldsPers2['jqpa_entreprise_denomination']                 = identiteMorale.personneMoraleDenomination;
        formFieldsPers2['jqpa_entreprise_formeJuridique']               = identiteMorale.personneMoraleFormeJuridique;
        formFieldsPers2['jqpa_entreprise_siren']                        = identiteMorale.sirenMorale.split(' ').join('');

        formFieldsPers2['jqpa_pm_activite']                             = justificationQP2.jqpaActiviteJqpaPP;
        formFieldsPers2['jqpa_aqpaSituationPM_diplome']                 = (Value('id').of(justificationQP2.jqpaSituation).eq('JqpaSituationDiplome') || Value('id').of(justificationQP2.jqpaSituation).eq('JqpaSituationExperience') ) ? true : false;


        formFieldsPers2['jqpa_qualitePersonneQualifieePM_representant'] = (Value('id').of(justificationQP2.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPRepresentantLegal')) ? true : false;
        formFieldsPers2['jqpa_qualitePersonneQualifieePM_conjoint']     = (Value('id').of(justificationQP2.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPConjoint')) ? true : false;
        formFieldsPers2['jqpa_qualitePersonneQualifieePM_salarie']      = (Value('id').of(justificationQP2.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPSalarie')) ? true : false;
        formFieldsPers2['jqpa_qualitePersonneQualifieePM_autre']        = (Value('id').of(justificationQP2.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPAutre')) ? true : false;
        formFieldsPers2['jqpa_qualitePersonneQualifieePMAutre']         = justificationQP2.qualitePersonneQualifieePP.jqpaQualitePersonneQualifieePPChampAutre;
        
        if(Value('id').of(justificationQP2.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPAutre') || Value('id').of(justificationQP2.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPSalarie')){
	        formFieldsPers2['jqpa_identitePMNomNaissance']                  = justificationQP2.identitePersonneQualifiee.jqpaIdentitePPNomNaissance;
	        formFieldsPers2['jqpa_identitePMNomUsage']                      = justificationQP2.identitePersonneQualifiee.jqpaIdentitePPNomUsage;
	        var prenoms=[];
	        for ( i = 0; i < justificationQP2.identitePersonneQualifiee.jqpaIdentitePPPrenom.size() ; i++ ){prenoms.push(justificationQP2.identitePersonneQualifiee.jqpaIdentitePPPrenom[i]);}                            
	        formFieldsPers2['jqpa_identitePMPrenom']                        = prenoms.toString();
	        
		    if (justificationQP2.identitePersonneQualifiee.jqpaIdentitePPDateNaissance !== null ) {
		        var dateTmp = new Date(parseInt(justificationQP2.identitePersonneQualifiee.jqpaIdentitePPDateNaissance.getTimeInMillis()));
		        var dateModif = pad(dateTmp.getDate().toString());
		        var month = dateTmp.getMonth() + 1;
		        dateModif = dateModif.concat(pad(month.toString()));
		        dateModif = dateModif.concat(dateTmp.getFullYear().toString());
			    formFieldsPers2['jqpa_identitePMDateNaissance']                 = dateModif;

		    }
	        
		    formFieldsPers2['jqpa_identitePMLieuNaissancePays']       = (justificationQP2.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune != null ? justificationQP2.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune : justificationQP2.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceVille)+"/"+justificationQP2.identitePersonneQualifiee.jqpaIdentitePPLieuNaissancePays;
	        formFieldsPers2['jqpa_identitePMDepartement']             = justificationQP2.identitePersonneQualifiee.jqpaIdentitePPDepartement !=null ? justificationQP2.identitePersonneQualifiee.jqpaIdentitePPDepartement.getId() : "";
        }
        
        /**Cerfa 3**/
        if (Value('id').of(justificationQP2.autresActiviteADeclarer).eq('autresActiviteADeclarerOui')) {
            
            formFieldsPers3['jqpa_entreprise_denomination']                 = identiteMorale.personneMoraleDenomination;
            formFieldsPers3['jqpa_entreprise_formeJuridique']               = identiteMorale.personneMoraleFormeJuridique;
            formFieldsPers3['jqpa_entreprise_siren']                        = identiteMorale.sirenMorale.split(' ').join('');

            formFieldsPers3['jqpa_pm_activite']                             = justificationQP3.jqpaActiviteJqpaPP;
            formFieldsPers3['jqpa_aqpaSituationPM_diplome']                 = (Value('id').of(justificationQP3.jqpaSituation).eq('JqpaSituationDiplome') || Value('id').of(justificationQP3.jqpaSituation).eq('JqpaSituationExperience') ) ? true : false;


            formFieldsPers3['jqpa_qualitePersonneQualifieePM_representant'] = (Value('id').of(justificationQP3.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPRepresentantLegal')) ? true : false;
            formFieldsPers3['jqpa_qualitePersonneQualifieePM_conjoint']     = (Value('id').of(justificationQP3.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPConjoint')) ? true : false;
            formFieldsPers3['jqpa_qualitePersonneQualifieePM_salarie']      = (Value('id').of(justificationQP3.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPSalarie')) ? true : false;
            formFieldsPers3['jqpa_qualitePersonneQualifieePM_autre']        = (Value('id').of(justificationQP3.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPAutre')) ? true : false;
            
            if((Value('id').of(justificationQP3.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPAutre'))|| Value('id').of(justificationQP3.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPSalarie') ){
            	
	            formFieldsPers3['jqpa_qualitePersonneQualifieePMAutre']         = justificationQP3.qualitePersonneQualifieePP.jqpaQualitePersonneQualifieePPChampAutre;
	            formFieldsPers3['jqpa_identitePMNomNaissance']                  = justificationQP3.identitePersonneQualifiee.jqpaIdentitePPNomNaissance;
	            formFieldsPers3['jqpa_identitePMNomUsage']                      = justificationQP3.identitePersonneQualifiee.jqpaIdentitePPNomUsage;
	            var prenoms=[];
	            for ( i = 0; i < justificationQP3.identitePersonneQualifiee.jqpaIdentitePPPrenom.size() ; i++ ){prenoms.push(justificationQP3.identitePersonneQualifiee.jqpaIdentitePPPrenom[i]);}                            
	            formFieldsPers3['jqpa_identitePMPrenom']                        = prenoms.toString();
	            
	            
	    	    if (justificationQP3.identitePersonneQualifiee.jqpaIdentitePPDateNaissance !== null ) {
	    	        var dateTmp = new Date(parseInt(justificationQP3.identitePersonneQualifiee.jqpaIdentitePPDateNaissance.getTimeInMillis()));
	    	        var dateModif = pad(dateTmp.getDate().toString());
	    	        var month = dateTmp.getMonth() + 1;
	    	        dateModif = dateModif.concat(pad(month.toString()));
	    	        dateModif = dateModif.concat(dateTmp.getFullYear().toString());
	    		    formFieldsPers3['jqpa_identitePMDateNaissance']                 = dateModif;

	    	    }
	            
	            formFieldsPers3['jqpa_identitePMLieuNaissancePays']             = (justificationQP3.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune != null ? justificationQP3.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune : justificationQP3.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceVille)+"/"+justificationQP3.identitePersonneQualifiee.jqpaIdentitePPLieuNaissancePays;
	            formFieldsPers3['jqpa_identitePMDepartement']           	= justificationQP3.identitePersonneQualifiee.jqpaIdentitePPDepartement !=null ? justificationQP3.identitePersonneQualifiee.jqpaIdentitePPDepartement.getId() : "";
            }
             /**Cerfa 4**/
             if (Value('id').of(justificationQP3.autresActiviteADeclarer).eq('autresActiviteADeclarerOui')) {
                
                formFieldsPers4['jqpa_entreprise_denomination']                 = identiteMorale.personneMoraleDenomination;
                formFieldsPers4['jqpa_entreprise_formeJuridique']               = identiteMorale.personneMoraleFormeJuridique;
                formFieldsPers4['jqpa_entreprise_siren']                        = identiteMorale.sirenMorale.split(' ').join('');

                formFieldsPers4['jqpa_pm_activite']                             = justificationQP4.jqpaActiviteJqpaPP;
                formFieldsPers4['jqpa_aqpaSituationPM_diplome']                 = (Value('id').of(justificationQP4.jqpaSituation).eq('JqpaSituationDiplome') || Value('id').of(justificationQP4.jqpaSituation).eq('JqpaSituationExperience') ) ? true : false;


                formFieldsPers4['jqpa_qualitePersonneQualifieePM_representant'] = (Value('id').of(justificationQP4.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPRepresentantLegal')) ? true : false;
                formFieldsPers4['jqpa_qualitePersonneQualifieePM_conjoint']     = (Value('id').of(justificationQP4.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPConjoint')) ? true : false;
                formFieldsPers4['jqpa_qualitePersonneQualifieePM_salarie']      = (Value('id').of(justificationQP4.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPSalarie')) ? true : false;
                formFieldsPers4['jqpa_qualitePersonneQualifieePM_autre']        = (Value('id').of(justificationQP4.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPAutre')) ? true : false;
               
                if((Value('id').of(justificationQP4.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPAutre'))|| Value('id').of(justificationQP4.qualitePersonneQualifieePP.jqpaQualitePersonneQualifiee).eq('JqpaQualitePersonneQualifieePPSalarie') ){

	                formFieldsPers4['jqpa_qualitePersonneQualifieePMAutre']         = justificationQP4.qualitePersonneQualifieePP.jqpaQualitePersonneQualifieePPChampAutre;
	                formFieldsPers4['jqpa_identitePMNomNaissance']                  = justificationQP4.identitePersonneQualifiee.jqpaIdentitePPNomNaissance;
	                formFieldsPers4['jqpa_identitePMNomUsage']                      = justificationQP4.identitePersonneQualifiee.jqpaIdentitePPNomUsage;
	                var prenoms=[];
	                for ( i = 0; i < justificationQP4.identitePersonneQualifiee.jqpaIdentitePPPrenom.size() ; i++ ){prenoms.push(justificationQP4.identitePersonneQualifiee.jqpaIdentitePPPrenom[i]);}                            
	                formFieldsPers4['jqpa_identitePMPrenom']                        = prenoms.toString();
	                
	        	    if (justificationQP4.identitePersonneQualifiee.jqpaIdentitePPDateNaissance !== null ) {
	        	        var dateTmp = new Date(parseInt(justificationQP4.identitePersonneQualifiee.jqpaIdentitePPDateNaissance.getTimeInMillis()));
	        	        var dateModif = pad(dateTmp.getDate().toString());
	        	        var month = dateTmp.getMonth() + 1;
	        	        dateModif = dateModif.concat(pad(month.toString()));
	        	        dateModif = dateModif.concat(dateTmp.getFullYear().toString());
	        		    formFieldsPers4['jqpa_identitePMDateNaissance']                 = dateModif;

	        	    }
	                formFieldsPers4['jqpa_identitePMLieuNaissancePays']                   = (justificationQP4.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune != null ? justificationQP4.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune : justificationQP4.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceVille)+"/"+justificationQP4.identitePersonneQualifiee.jqpaIdentitePPLieuNaissancePays;
	                formFieldsPers4['jqpa_identitePMDepartement']            = justificationQP4.identitePersonneQualifiee.jqpaIdentitePPDepartement !=null ? justificationQP4.identitePersonneQualifiee.jqpaIdentitePPDepartement.getId() : "";
                }
                
            }

        
        }

    }

}

/** Observation **/

/**Cerfa 1**/
formFieldsPers1['jqpa_formalite_observations']                  = observationJqpa.formaliteObservations;
/**Cerfa 2**/
if (Value('id').of(justificationQP1.autresActiviteADeclarer).eq('autresActiviteADeclarerOui')) {
    formFieldsPers2['jqpa_formalite_observations']                  = observationJqpa.formaliteObservations;
    
    /**Cerfa 3**/
    if (Value('id').of(justificationQP2.autresActiviteADeclarer).eq('autresActiviteADeclarerOui')) {
        formFieldsPers3['jqpa_formalite_observations']                  = observationJqpa.formaliteObservations;
        
        /**Cerfa 4**/
        if (Value('id').of(justificationQP3.autresActiviteADeclarer).eq('autresActiviteADeclarerOui')) {
            formFieldsPers4['jqpa_formalite_observations']                  = observationJqpa.formaliteObservations;
        }

    }
}


/** Signature **/
/**Cerfa 1**/

formFieldsPers1['jqpa_formalite_declarant']                     = (Value('id').of(signatureJqpa.soussigne).eq('FormaliteSignataireQualiteDeclarant')) ? true : false; 
formFieldsPers1['jqpa_formalite_representant']                  = (Value('id').of(signatureJqpa.soussigne).eq('FormaliteSignataireQualiteRepresentant')) ? true : false;
formFieldsPers1['jqpa_formalite_mandataire']                    = (Value('id').of(signatureJqpa.soussigne).eq('FormaliteSignataireQualiteMandataire')) ? true : false;
if (Value('id').of(signatureJqpa.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
    formFieldsPers1['jqpa_formalite_mandataireNom']                 = signatureAdresseMandataireJqpa.nomPrenomDenominationMandataire;
	formFieldsPers1['jqpa_formalite_mandataireInfo']                = (signatureAdresseMandataireJqpa.numeroVoieMandataire != null ? signatureAdresseMandataireJqpa.numeroVoieMandataire : '')
																	+ ' ' + (signatureAdresseMandataireJqpa.indiceVoieMandataire != null ? signatureAdresseMandataireJqpa.indiceVoieMandataire : '')
																	+ ' ' + (signatureAdresseMandataireJqpa.typeVoieMandataire != null ? signatureAdresseMandataireJqpa.typeVoieMandataire : '')
																	+ ' ' + (signatureAdresseMandataireJqpa.nomVoieMandataire != null ? signatureAdresseMandataireJqpa.nomVoieMandataire : '')
																	+ ' ' + (signatureAdresseMandataireJqpa.complementVoieMandataire != null ? signatureAdresseMandataireJqpa.complementVoieMandataire : '')
																	+ ' ' + (signatureAdresseMandataireJqpa.voieDistributionSpecialeAdresseMandataire != null ? signatureAdresseMandataireJqpa.voieDistributionSpecialeAdresseMandataire : '')
																	+ ' ' + (signatureAdresseMandataireJqpa.dataCodePostalMandataire != null ? signatureAdresseMandataireJqpa.dataCodePostalMandataire : '')
																	+ ' ' + (signatureAdresseMandataireJqpa.villeAdresseMandataire != null ? signatureAdresseMandataireJqpa.villeAdresseMandataire : '');
}
formFieldsPers1['jqpa_formalite_lieu']                          = signatureJqpa.formaliteSignatureLieu;
formFieldsPers1['jqpa_formalite_date']                          = signatureJqpa.formaliteSignatureDate;

/**Cerfa 2**/
if (Value('id').of(justificationQP1.autresActiviteADeclarer).eq('autresActiviteADeclarerOui')) {

    formFieldsPers2['jqpa_formalite_declarant']                     = (Value('id').of(signatureJqpa.soussigne).eq('FormaliteSignataireQualiteDeclarant')) ? true : false; 
    formFieldsPers2['jqpa_formalite_representant']                  = (Value('id').of(signatureJqpa.soussigne).eq('FormaliteSignataireQualiteRepresentant')) ? true : false;
    formFieldsPers2['jqpa_formalite_mandataire']                    = (Value('id').of(signatureJqpa.soussigne).eq('FormaliteSignataireQualiteMandataire')) ? true : false;
    if (Value('id').of(signatureJqpa.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
    formFieldsPers2['jqpa_formalite_mandataireNom']                 = signatureAdresseMandataireJqpa.nomPrenomDenominationMandataire;
	formFieldsPers2['jqpa_formalite_mandataireInfo']                = (signatureAdresseMandataireJqpa.numeroVoieMandataire != null ? signatureAdresseMandataireJqpa.numeroVoieMandataire : '')
																+ ' ' + (signatureAdresseMandataireJqpa.numeroVoieMandataire != null ? signatureAdresseMandataireJqpa.numeroVoieMandataire : '')
																	+ ' ' + (signatureAdresseMandataireJqpa.indiceVoieMandataire != null ? signatureAdresseMandataireJqpa.indiceVoieMandataire : '')
																	+ ' ' + (signatureAdresseMandataireJqpa.typeVoieMandataire != null ? signatureAdresseMandataireJqpa.typeVoieMandataire : '')
																	+ ' ' + (signatureAdresseMandataireJqpa.nomVoieMandataire != null ? signatureAdresseMandataireJqpa.nomVoieMandataire : '')
																	+ ' ' + (signatureAdresseMandataireJqpa.complementVoieMandataire != null ? signatureAdresseMandataireJqpa.complementVoieMandataire : '')
																	+ ' ' + (signatureAdresseMandataireJqpa.voieDistributionSpecialeAdresseMandataire != null ? signatureAdresseMandataireJqpa.voieDistributionSpecialeAdresseMandataire : '')
																	+ ' ' + (signatureAdresseMandataireJqpa.dataCodePostalMandataire != null ? signatureAdresseMandataireJqpa.dataCodePostalMandataire : '')
																	+ ' ' + (signatureAdresseMandataireJqpa.villeAdresseMandataire != null ? signatureAdresseMandataireJqpa.villeAdresseMandataire : '');
}
formFieldsPers2['jqpa_formalite_lieu']                          = signatureJqpa.formaliteSignatureLieu;
    formFieldsPers2['jqpa_formalite_date']                          = signatureJqpa.formaliteSignatureDate;
    
    /**Cerfa 3**/
    if (Value('id').of(justificationQP2.autresActiviteADeclarer).eq('autresActiviteADeclarerOui')) {
        formFieldsPers3['jqpa_formalite_declarant']                     = (Value('id').of(signatureJqpa.soussigne).eq('FormaliteSignataireQualiteDeclarant')) ? true : false; 
        formFieldsPers3['jqpa_formalite_representant']                  = (Value('id').of(signatureJqpa.soussigne).eq('FormaliteSignataireQualiteRepresentant')) ? true : false;
        formFieldsPers3['jqpa_formalite_mandataire']                    = (Value('id').of(signatureJqpa.soussigne).eq('FormaliteSignataireQualiteMandataire')) ? true : false;
        if (Value('id').of(signatureJqpa.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
		formFieldsPers3['jqpa_formalite_mandataireNom']                 = signatureAdresseMandataireJqpa.nomPrenomDenominationMandataire;
		formFieldsPers3['jqpa_formalite_mandataireInfo']                = (signatureAdresseMandataireJqpa.numeroVoieMandataire != null ? signatureAdresseMandataireJqpa.numeroVoieMandataire : '')
																+ ' ' + (signatureAdresseMandataireJqpa.indiceVoieMandataire != null ? signatureAdresseMandataireJqpa.indiceVoieMandataire : '')
																	+ ' ' + (signatureAdresseMandataireJqpa.typeVoieMandataire != null ? signatureAdresseMandataireJqpa.typeVoieMandataire : '')
																	+ ' ' + (signatureAdresseMandataireJqpa.nomVoieMandataire != null ? signatureAdresseMandataireJqpa.nomVoieMandataire : '')
																	+ ' ' + (signatureAdresseMandataireJqpa.complementVoieMandataire != null ? signatureAdresseMandataireJqpa.complementVoieMandataire : '')
																	+ ' ' + (signatureAdresseMandataireJqpa.voieDistributionSpecialeAdresseMandataire != null ? signatureAdresseMandataireJqpa.voieDistributionSpecialeAdresseMandataire : '')
																	+ ' ' + (signatureAdresseMandataireJqpa.dataCodePostalMandataire != null ? signatureAdresseMandataireJqpa.dataCodePostalMandataire : '')
																	+ ' ' + (signatureAdresseMandataireJqpa.villeAdresseMandataire != null ? signatureAdresseMandataireJqpa.villeAdresseMandataire : '');
		}
formFieldsPers3['jqpa_formalite_lieu']                          = signatureJqpa.formaliteSignatureLieu;
        formFieldsPers3['jqpa_formalite_date']                          = signatureJqpa.formaliteSignatureDate;
        
       /**Cerfa 4**/
        if (Value('id').of(justificationQP3.autresActiviteADeclarer).eq('autresActiviteADeclarerOui')) {
            formFieldsPers4['jqpa_formalite_declarant']                     = (Value('id').of(signatureJqpa.soussigne).eq('FormaliteSignataireQualiteDeclarant')) ? true : false; 
            formFieldsPers4['jqpa_formalite_representant']                  = (Value('id').of(signatureJqpa.soussigne).eq('FormaliteSignataireQualiteRepresentant')) ? true : false;
            formFieldsPers4['jqpa_formalite_mandataire']                    = (Value('id').of(signatureJqpa.soussigne).eq('FormaliteSignataireQualiteMandataire')) ? true : false;
            if (Value('id').of(signatureJqpa.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
    formFieldsPers4['jqpa_formalite_mandataireNom']                 = signatureAdresseMandataireJqpa.nomPrenomDenominationMandataire;
	formFieldsPers4['jqpa_formalite_mandataireInfo']                = (signatureAdresseMandataireJqpa.numeroVoieMandataire != null ? signatureAdresseMandataireJqpa.numeroVoieMandataire : '')
																	+ ' ' + (signatureAdresseMandataireJqpa.numeroVoieMandataire != null ? signatureAdresseMandataireJqpa.numeroVoieMandataire : '')
																	+ ' ' + (signatureAdresseMandataireJqpa.indiceVoieMandataire != null ? signatureAdresseMandataireJqpa.indiceVoieMandataire : '')
																	+ ' ' + (signatureAdresseMandataireJqpa.typeVoieMandataire != null ? signatureAdresseMandataireJqpa.typeVoieMandataire : '')
																	+ ' ' + (signatureAdresseMandataireJqpa.nomVoieMandataire != null ? signatureAdresseMandataireJqpa.nomVoieMandataire : '')
																	+ ' ' + (signatureAdresseMandataireJqpa.complementVoieMandataire != null ? signatureAdresseMandataireJqpa.complementVoieMandataire : '')
																	+ ' ' + (signatureAdresseMandataireJqpa.voieDistributionSpecialeAdresseMandataire != null ? signatureAdresseMandataireJqpa.voieDistributionSpecialeAdresseMandataire : '')
																	+ ' ' + (signatureAdresseMandataireJqpa.dataCodePostalMandataire != null ? signatureAdresseMandataireJqpa.dataCodePostalMandataire : '')
																	+ ' ' + (signatureAdresseMandataireJqpa.villeAdresseMandataire != null ? signatureAdresseMandataireJqpa.villeAdresseMandataire : '');
}
formFieldsPers4['jqpa_formalite_lieu']                          = signatureJqpa.formaliteSignatureLieu;
            formFieldsPers4['jqpa_formalite_date']                          = signatureJqpa.formaliteSignatureDate;
        }

    }
}

/**Merge du cerfa **/

var cerfaDoc0 = nash.doc //
    .load('models/cerfaJQPA.pdf') //
    .apply(formFieldsPers1);

if (Value('id').of(justificationQP1.autresActiviteADeclarer).eq('autresActiviteADeclarerOui')) {
    var cerfaDoc2 = nash.doc //
        .load('models/cerfaJQPA.pdf') //
        .apply (formFieldsPers2);
    cerfaDoc0.append(cerfaDoc2.save('cerfa.pdf'));
    if (Value('id').of(justificationQP2.autresActiviteADeclarer).eq('autresActiviteADeclarerOui')) {
        var cerfaDoc3 = nash.doc //
            .load('models/cerfaJQPA.pdf') //
            .apply (formFieldsPers3);
        cerfaDoc0.append(cerfaDoc3.save('cerfa.pdf'));
        if (Value('id').of(justificationQP3.autresActiviteADeclarer).eq('autresActiviteADeclarerOui')) {
            var cerfaDoc4 = nash.doc //
                .load('models/cerfaJQPA.pdf') //
                .apply (formFieldsPers4);
            cerfaDoc0.append(cerfaDoc4.save('cerfa.pdf'));
        }
    }
}

/** Affichage cerfa dans le review **/
var pjUser = [];
var metas = [];

function pushPjPreview(fld) {
    fld.forEach(function (elm) {
        pjUser.push(elm);
        _log.info('elm.getAbsolutePath() = >'+elm.getAbsolutePath());
		metas.push({'name':'userAttachments', 'value': '/'+elm.getAbsolutePath()});
		metas.push({'name':'document', 'value': '/'+elm.getAbsolutePath()});
    });
}
var pjactivite1=$jqpa.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle1.jqpaSituation;
var pjactivite2=$jqpa.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle2.jqpaSituation;
var pjactivite3=$jqpa.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle3.jqpaSituation;
var pjactivite4=$jqpa.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle4.jqpaSituation;

if(Value('id').of(pjactivite1).eq('JqpaSituationDiplome')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDiplomes1);
}
if(Value('id').of(pjactivite2).eq('JqpaSituationDiplome')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDiplomes2);
}
if(Value('id').of(pjactivite3).eq('JqpaSituationDiplome')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDiplomes3);
}
if(Value('id').of(pjactivite4).eq('JqpaSituationDiplome')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDiplomes4);
}


if(Value('id').of(pjactivite1).eq('JqpaSituationExperience')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles1);
}                                                                                                   
if(Value('id').of(pjactivite2).eq('JqpaSituationExperience')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles2);
}
if(Value('id').of(pjactivite3).eq('JqpaSituationExperience')) {
   pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles3);
}
if(Value('id').of(pjactivite4).eq('JqpaSituationExperience')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles4);
}


var pjSignataire=$jqpa.cadreSignatureGroup.cadreSignature.soussigne;
/**  Si le signataire c'est le déclarant  **/
if(Value('id').of(pjSignataire).eq('FormaliteSignataireQualiteDeclarant')) {
     pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantSignataire);

}else if(Value('id').of(pjSignataire).eq('FormaliteSignataireQualiteMandataire')) {

/**  Si le signataire c'est le Mandataire **/
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire);
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPouvoir);

}else if(Value('id').of(pjSignataire).eq('FormaliteSignataireQualiteRepresentant')) {
    /** Si le signataire c'est le Representant  **/
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDRepresentantSignataire);
}



//Remove old metas before insert
var metasToDelete = [];
var recordMetas = nash.record.meta() != null ? nash.record.meta().metas : null;
var recordMetasSize = recordMetas != null ? recordMetas.size() : 0;

for (var i = 0; i < recordMetasSize; i++) {
    if (recordMetas.get(i).name == 'document') {
        metasToDelete.push({'name':'document', 'value': recordMetas.get(i).value});
    }
}
nash.record.removeMeta(metasToDelete);


//Insert new metas
nash.record.meta(metas);

var data = [ spec.createData({
    id : 'record',
    label : 'Justificatif de Qualification Professionnelle Artisanale',
    description : 'Voici le formulaire obtenu à partir des données saisies.',
    type : 'FileReadOnly',
    value : [ cerfaDoc0.save('cerfa.pdf') ]
    }),  
    spec.createData({
    id : 'attachments',
    label : 'Pièces jointes',
    description : 'Pièces jointes ajoutées au formulaire.',
    type : 'FileReadOnly',
    value : pjUser
})

 ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Review',
    groups : groups
});