function pad(s) { return (s < 10) ? '0' + s : s; }
var formFields = {};

// Cadres 1 - Activité non salariée

var identite = $p0cmb.cadre1IdentiteGroup.cadre1Identite;

formFields['formalite_evenement_oui']                                                   = identite.formaliteEvenementOui ? true : false;
formFields['formalite_evenement_non']                                                   = identite.formaliteEvenementOui ? false : true;
formFields['voletSocial_activiteExerceeAnterieurementSIREN']                            = identite.voletSocialActiviteExerceeAnterieurementSIREN != null ? identite.voletSocialActiviteExerceeAnterieurementSIREN.split(' ').join('') : '';

// Cadres 2A - EI

formFields['nonEIRL']                                                                   = $p0cmb.cadreEIRLGroup.cadreEIRL.estEIRL ? false : true;

// Cadres 2B - EIRL

formFields['estEIRL']                                                                   = $p0cmb.cadreEIRLGroup.cadreEIRL.estEIRL ? true : false;

// Cadres 3 - Identité

formFields['personneLiee_personnePhysique_nomNaissance']                                = identite.personneLieePersonnePhysiqueNomNaissance;
formFields['personneLiee_personnePhysique_nomUsage']                                    = identite.personneLieePersonnePhysiqueNomUsage;
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFields['personneLiee_personnePhysique_prenom1']                                                   = prenoms.toString();
formFields['personneLiee_personnePhysique_pseudonyme']                                  = identite.personneLieePersonnePhysiquePseudonyme;
formFields['personneLiee_personnePhysique_nationalite']                                 = identite.personneLieePersonnePhysiqueNationalite;
formFields['personneLiee_personnePhysique_civiliteMasculin']                            = Value('id').of(identite.civilite).eq('PersonneLieePersonnePhysiqueCiviliteMasculin') ? true : false;
formFields['personneLiee_personnePhysique_civiliteFeminin']                             = Value('id').of(identite.civilite).eq('PersonneLieePersonnePhysiqueCiviliteFeminin') ? true : false;
if(identite.personneLieePersonnePhysiqueDateNaissance != null) {
	var dateTmp = new Date(parseInt(identite.personneLieePersonnePhysiqueDateNaissance.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['personneLiee_personnePhysique_dateNaissance'] = date;
}
formFields['personneLiee_personnePhysique_lieuNaissanceDepartement']                    = identite.personneLieePersonnePhysiqueLieuNaissanceDepartement != null ? identite.personneLieePersonnePhysiqueLieuNaissanceDepartement.getId() : '';
formFields['personneLiee_personnePhysique_lieuNaissanceCommune']                        = identite.personneLieePersonnePhysiqueLieuNaissanceCommune != null ? identite.personneLieePersonnePhysiqueLieuNaissanceCommune : identite.personneLieePersonnePhysiqueLieuNaissanceVille;
formFields['personneLiee_personnePhysique_lieuNaissancePays']                           = identite.personneLieePersonnePhysiqueLieuNaissancePays;
formFields['personneLiee_personnePhysique_mineurEmancipe']                              = identite.personneLieePersonnePhysiqueMineurEmancipe ? true : false;

var adresse = $p0cmb.cadre1IdentiteGroup.cadre1AdresseDeclarant;

formFields['personneLiee_adresse_voie1']                                                = (adresse.personneLieePersonnePhysiqueAdresserueNumeroVoie != null ? adresse.personneLieePersonnePhysiqueAdresserueNumeroVoie : '') 
																						+ ' ' + (adresse.personneLieePersonnePhysiqueAdresseIndiceVoie != null ? adresse.personneLieePersonnePhysiqueAdresseIndiceVoie : '') 
																						+ ' ' + (adresse.personneLieePersonnePhysiqueAdresseTypeVoie != null ? adresse.personneLieePersonnePhysiqueAdresseTypeVoie : '') 
																						+ ' ' + (adresse.personneLieePersonnePhysiqueAdresseNomVoie !=null ? adresse.personneLieePersonnePhysiqueAdresseNomVoie : '');           
formFields['personneLiee_adresse_voie2']                                                = (adresse.personneLieePersonnePhysiquerueAdresseComplementAdresse != null ? adresse.personneLieePersonnePhysiquerueAdresseComplementAdresse : '') 
																						+ ' ' + (adresse.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie != null ? adresse.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie :'');
formFields['personneLiee_adresse_codePostal']                                           = adresse.personneLieePersonnePhysiqueAdresseCodePostal;
formFields['personneLiee_adresse_commune']                                              = Value('id').of(adresse.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX') ? adresse.personneLieePersonnePhysiqueAdresseCommune : adresse.personneLieeAdresseVille;
formFields['personneLiee_adresse_pays']                                                 = adresse.personneLieePersonnePhysiqueAdressePays;
formFields['personneLiee_adresse_communeAncienne']                                      = adresse.personneLieePersonnePhysiqueAdresseCommuneAncienne != null ? adresse.personneLieePersonnePhysiqueAdresseCommuneAncienne : '';

// Cadre 4 - Statut Conjoint

var conjoint = $p0cmb.cadre2conjointGroup.cadre2Conjoint;
formFields['personneLiee_conjointActiviteEntreprise_oui']                   			= conjoint.conjointRole ? true : false;
formFields['personneLiee_conjointActiviteEntreprise_non']                  				= conjoint.conjointRole ? false : true;
formFields['personneLiee_conjointCollaborateurSalarie_collaborateur']                   = Value('id').of(conjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieCollaborateur') ? true : false;
formFields['personneLiee_conjointCollaborateurSalarie_salarie']                         = Value('id').of(conjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieSalarie') ? true : false;

// Cadre 5 - Déclaration relative à l'insaisissabilité
var accre = $p0cmb.cadre3InformationsComplementairesGroup.cadre3InformationsComplementaires;

var insaisissabilite = $p0cmb.cadre3InformationsComplementairesGroup.cadre3InformationsComplementaires.cadre3DeclarationInsaisissabilite;

formFields['entreprise_insaisissabiliteRenonciationDeclarationAutresBiens']             = insaisissabilite.entrepriseInsaisissabiliteRenonciationDeclarationAutresBiens ? true : false;
formFields['entreprise_insaisissabilitePublicationRenonciationDeclarationAutresBiens']  = insaisissabilite.entrepriseInsaisissabilitePublicationRenonciationDeclarationAutresBiens;
formFields['entreprise_insaisissabiliteDeclarationAutresBiens']                         = insaisissabilite.entrepriseInsaisissabiliteDeclarationAutresBiens ? true : false;
if (insaisissabilite.entrepriseInsaisissabiliteDeclarationAutresBiens) {
var declarationAutresBiens=[];
for ( i = 0; i < insaisissabilite.entrepriseInsaisissabilitePublicationDeclarationAutresBiens.size() ; i++ ){declarationAutresBiens.push(insaisissabilite.entrepriseInsaisissabilitePublicationDeclarationAutresBiens[i]);}                            
formFields['entreprise_insaisissabilitePublicationDeclarationAutresBiens']                      = declarationAutresBiens.toString();
}

// Cadre 6 - Contrat Appui

formFields['entreprise_contratAppuiPresence']                                           = accre.entrepriseContratAppuiPresence ? true : false;
if(accre.contratAppuiDateFin != null) {
	var dateTmp = new Date(parseInt(accre.contratAppuiDateFin.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['contratAppuiDateFin'] = date;
}

// Information sur l'entreprise accompagnante sur l'intercalaire P0'


// Cadres 7 - Autre Etblissement UE

formFields['entreprise_autreEtablissementUE']                                           = accre.entrepriseAutreEtablissementUE ? true : false;

// Etablissements UE sur l'intercalaire P0'

formFields['etablissementUE1_infos']                                                    = accre.entrepriseAutreEtablissementUE ? (accre.cadre3AutreEtablissementUE1.numeroImmatriculationAutreEtablissementUE +' '+ accre.cadre3AutreEtablissementUE1.lieuImmatriculationAutreEtablissementUE +' / '+ "Activité(s) :" +' '+ accre.cadre3AutreEtablissementUE1.activiteAutreEtablissementUE) : '';
formFields['etablissementUE1_adresse']                                                  = accre.entrepriseAutreEtablissementUE ? 
																						((accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.rueNumeroAdresseAutreEtablissementUE != null ? accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.rueNumeroAdresseAutreEtablissementUE : '')
																						+' '+ (accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.indiceVoieAdresseAutreEtablissementUE != null ? accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.indiceVoieAdresseAutreEtablissementUE : '')
																						+' '+ (accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.typeVoieAdresseAutreEtablissementUE != null ? accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.typeVoieAdresseAutreEtablissementUE  : '')
																						+' '+ accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.nomVoieAdresseAutreEtablissementUE 
																						+' '+ (accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.rueComplementAdresseAutreEtablissementUE != null ? accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.rueComplementAdresseAutreEtablissementUE : '') 
																						+' '+ (accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.distributionSpecialeAdresseAutreEtablissementUE != null ? accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.distributionSpecialeAdresseAutreEtablissementUE : '')
																						+' '+ (accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.codePostalAdresseAutreEtablissementUE != null ? accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.codePostalAdresseAutreEtablissementUE : '') 
																						+' '+ accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.communeAdresseAutreEtablissementUECommune  
																						+ ' / ' + accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.paysAdresseAutreEtablissementUE) : '';
formFields['etablissementUE2_infos']                                                    = accre.cadre3AutreEtablissementUE1.declarationAutreEtablissementUE ? (accre.cadre3AutreEtablissementUE2.numeroImmatriculationAutreEtablissementUE +' '+ accre.cadre3AutreEtablissementUE2.lieuImmatriculationAutreEtablissementUE +' / '+ "Activité(s) :" +' '+ accre.cadre3AutreEtablissementUE2.activiteAutreEtablissementUE) : '';
formFields['etablissementUE2_adresse']                                                  = accre.cadre3AutreEtablissementUE1.declarationAutreEtablissementUE ? 
																						((accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.rueNumeroAdresseAutreEtablissementUE != null ? accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.rueNumeroAdresseAutreEtablissementUE : '')
																						+' '+ (accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.indiceVoieAdresseAutreEtablissementUE != null ? accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.indiceVoieAdresseAutreEtablissementUE : '')
																						+' '+ (accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.typeVoieAdresseAutreEtablissementUE != null ? accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.typeVoieAdresseAutreEtablissementUE  : '')
																						+' '+ accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.nomVoieAdresseAutreEtablissementUE 
																						+' '+ (accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.rueComplementAdresseAutreEtablissementUE != null ? accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.rueComplementAdresseAutreEtablissementUE : '') 
																						+' '+ (accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.distributionSpecialeAdresseAutreEtablissementUE != null ? accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.distributionSpecialeAdresseAutreEtablissementUE : '')
																						+' '+ (accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.codePostalAdresseAutreEtablissementUE != null ? accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.codePostalAdresseAutreEtablissementUE : '') 
																						+' '+ accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.communeAdresseAutreEtablissementUECommune  
																						+ ' / ' + accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.paysAdresseAutreEtablissementUE) : '';

// Cadre 8A - Adresse de l'entreprise

var adressePro = $p0cmb.cadre4AdresseActiviteGroup.cadre4AdresseActivite;

formFields['etablissement_domiciliation_non']                                           = Value('id').of(adressePro.activiteLieu).eq('etablissementDomiciliationNon') ? true : false;
formFields['etablissement_domiciliation_oui']                                           = Value('id').of(adressePro.activiteLieu).eq('etablissementDomiciliationOui') ? true : false;
formFields['entreprise_adresseEntreprise_domicile']                                     = Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? true : false;

//Cadre 8B - Ambulant UE

var activite = $p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite;

formFields['etablissement_estAmbulantUE']                                               = activite.etablissementEstAmbulantUE ? true : false;
formFields['personneLiee_adresse_codePostal_ambulantUE']                                = activite.etablissementEstAmbulantUE ? activite.personneLieeAdresseCodePostalAmbulantUE : '';
formFields['personneLiee_adresse_commune_ambulantUE']                                   = activite.etablissementEstAmbulantUE ? activite.personneLieeAdresseCommuneAmbulantUE : '';

//Cadre 9 - Adresse de l'établissement

formFields['entrepriseLiee_adresse_voie']                                               = (adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseNumRue != null ? adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseNumRue : '') 
																						+ ' ' + (adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseIndiceVoieAdresse != null ? adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseIndiceVoieAdresse : '') 
																						+ ' ' + (adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseTypeVoieAdresse != null ? adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseTypeVoieAdresse : '') 
																						+ ' ' + (adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseNomVoieAdresse != null ? adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseNomVoieAdresse : '')
																				        + ' ' + (adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseRueComplementAdresse != null ? adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseRueComplementAdresse : '') 
																						+ ' ' + (adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseAdresseDistriutionSpecialeVoie != null ? adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseAdresseDistriutionSpecialeVoie : '');
formFields['entrepriseLiee_adresse_codePostal']                                         = adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseCodePostal;
formFields['entrepriseLiee_adresse_commune']                                            = adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseCommune != null ? adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseCommune : '';
formFields['entrepriseLiee_adresse_communeAncienne']                                    = adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseCommuneAncienne != null ?adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseCommuneAncienne : '';
formFields['entrepriseLiee_siren']                                                      = adressePro.entrepriseLieeSiren != null ? adressePro.entrepriseLieeSiren.split(' ').join('') : '';
formFields['entrepriseLiee_nom']                                                        = adressePro.entrepriseLieeNom;

// Cadre 10 - Activité

if(activite.etablissementDateDebutActivite != null) {
	var dateTmp = new Date(parseInt(activite.etablissementDateDebutActivite.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_dateDebutActivite'] = date;
}	
formFields['entreprise_activitePermanenteSaisonniere_permanente']                       = Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonnierePermanente') ? true : false;
formFields['entreprise_activitePermanenteSaisonniere_saisonniere']                      = Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonniereSaisonniere') ? true : false;
formFields['etablissment_nonSedentariteQualite_nonSedentaire']                          = activite.etablissmentNonSedentariteQualiteNonSedentaire ? true : false;
formFields['etablissement_activitesPrincipales2']                                       = (activite.etablissementActivitesPrincipales.length > 90) ? (activite.etablissementActivitesPrincipales.substring(0, 75) + ' ' + "(suite sur le P0')") : activite.etablissementActivitesPrincipales;
formFields['etablissement_activitesAutres']                                             = activite.etablissementActivitesAutres;
formFields['etablissement_activiteLieuExercice_magasin']                                = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMagasin') ? true : false;
formFields['etablissement_activiteLieuExercice_marche']                                 = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMarche') ? true : false;
formFields['etablissement_activiteNature_commerceGros']                                 = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceGros') ? true : false;
formFields['etablissement_activiteNature_batTravauxPublics']                            = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureBatTravauxPublics') ? true : false;
formFields['etablissement_activiteLieuExerciceMagasin']                                 = activite.etablissementActiviteLieuExerciceMagasinSurface;
formFields['etablissement_activiteNature_commerceDetail']                               = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceDetail') ? true : false;
formFields['etablissement_activiteNature_fabricationProduction']                        = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureFabricationProduction') ? true : false;
formFields['etablissement_activiteNature_cocheAutre']                                   = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCocheAutre') ? true : false;
formFields['etablissement_activiteNature_autre']                                        = activite.etablissementActiviteNatureAutre;

// Cadre 11 - Nom commercial

formFields['etablissement_nomCommercialProfessionnel']                                  = activite.etablissementNomCommercialProfessionnel;
formFields['etablissement_enseigne']                                                    = activite.etablissementEnseigne;

// Cadre 12 - Origine Fonds

var origine = $p0cmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds;

formFields['etablissement_origineFonds_creation']                                       = Value('id').of($p0cmb.cadre4AdresseActiviteGroup.cadre4AdresseActivite.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? true : (Value('id').of($p0cmb.cadre4AdresseActiviteGroup.cadre4AdresseActivite.activiteLieu).eq('etablissementDomiciliationOui') ? true : (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation') ? true : false));
formFields['etablissement_origineFonds_locationGerance']                                = Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsLocationGerance') ? true : false;
formFields['etablissement_origineFonds_geranceMandat']                                  = Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsGeranceMandat') ? true : false;
formFields['etablissement_origineFonds_achat']                                          = Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsAchat') ? true : false;
formFields['etablissement_origineFonds_cocheAutre']                                     = Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCocheAutre') ? true : false;
formFields['etablissement_origineFonds_autre']                                          = origine.etablissementOrigineFondsAutre;
formFields['entrepriseLiee_siren_precedentExploitant']                                  = origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant.split(' ').join('') : '';
formFields['entrepriseLiee_entreprisePP_nomNaissance_precedentExploitant']              = origine.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant : (origine.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant : '');
formFields['entrepriseLiee_entreprisePP_nomUsage_precedentExploitant']                  = origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant : '';
formFields['entrepriseLiee_entreprisePP_prenom1_precedentExploitant']                   = origine.precedentExploitant.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant : '';
if(origine.geranceMandat.etablissementLoueurMandantDuFondsDebutContrat != null) {
	var dateTmp = new Date(parseInt(origine.geranceMandat.etablissementLoueurMandantDuFondsDebutContrat.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_loueurMandantDuFondsDebutContrat'] = date;
}
if(origine.geranceMandat.etablissementLoueurMandantDuFondsFinContrat != null) {
	var dateTmp = new Date(parseInt(origine.geranceMandat.etablissementLoueurMandantDuFondsFinContrat.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_loueurMandantDuFondsFinContrat'] = date;
}
formFields['etablissement_loueurMandantDuFondsRenouvellementTaciteReconduction_oui']    = (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsLocationGerance') or  Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsGeranceMandat')) ? (origine.geranceMandat.etablissementLoueurMandantDuFondsRenouvellementTaciteReconductionOui ? true : false) : false;
formFields['etablissement_loueurMandantDuFondsRenouvellementTaciteReconduction_non']    = (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsLocationGerance') or  Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsGeranceMandat')) ? (origine.geranceMandat.etablissementLoueurMandantDuFondsRenouvellementTaciteReconductionOui ? false : true) : false;
formFields['entrepriseLiee_entreprisePM_nomNaissance_loueurMandantDuFonds']             = origine.geranceMandat.entrepriseLieeEntreprisePMDenominationLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeEntreprisePMDenominationLoueurMandantDuFonds : (origine.geranceMandat.entrepriseLieeEntreprisePMNomNaissanceLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeEntreprisePMNomNaissanceLoueurMandantDuFonds : '');
formFields['entrepriseLiee_entreprisePP_nomUsage_loueurMandantDuFonds']                 = origine.geranceMandat.entrepriseLieeEntreprisePPNomUsageLoueurMandantDuFonds;
formFields['entrepriseLiee_entreprisePP_prenom1_loueurMandantDuFonds']                  = origine.geranceMandat.entrepriseLieeEntreprisePPPrenom1LoueurMandantDuFonds;
formFields['entrepriseLiee_adresse_voie_loueurMandantDuFonds']                          = (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.numRueAdresseLoueurMandantDufonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.numRueAdresseLoueurMandantDufonds : '') 
																						+ ' ' + (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.indiceVoieLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.indiceVoieLoueurMandantDuFonds : '') 
																						+ ' ' + (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.typeVoieLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.typeVoieLoueurMandantDuFonds : '') 
																						+ ' ' + (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.nomVoieLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.nomVoieLoueurMandantDuFonds : '')                                                                                                                              
																				        + ' ' + (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.rueComplementAdresseLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.rueComplementAdresseLoueurMandantDuFonds : '') 
																						+ ' ' + (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.distributionSpecialeLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.distributionSpecialeLoueurMandantDuFonds : '');
formFields['entrepriseLiee_adresse_codePostal_loueurMandantDuFonds']                    = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.codePostalLoueurMandantDuFonds;
formFields['entrepriseLiee_adresse_commune_loueurMandantDuFonds']                       = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.communeLoueurMandantDuFonds;
formFields['entrepriseLiee_siren_geranceMandat']                                        = origine.geranceMandat.entrepriseLieeSirenGeranceMandat != null ? origine.geranceMandat.entrepriseLieeSirenGeranceMandat.split(' ').join('') : '';
formFields['entrepriseLiee_greffeImmatriculation']                                      = origine.geranceMandat.entrepriseLieeGreffeImmatriculation;
if(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParution != null) {
	var dateTmp = new Date(parseInt(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParution.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_journalAnnoncesLegalesDateParution'] = date;
}
formFields['etablissement_journalAnnoncesLegalesNom']                                   = origine.precedentExploitant.etablissementJournalAnnoncesLegalesNom;

// Cadre 13 - Effectif salarié

formFields['etablissement_effectifSalariePresence_non']                                 = activite.etablissementEffectifSalariePresenceOui ? false : true;
formFields['etablissement_effectifSalariePresence_oui']                                 = activite.etablissementEffectifSalariePresenceOui ? true : false;
formFields['etablissement_effectifSalarieNombre']                                       = activite.cadre5EffectifSalarie.etablissementEffectifSalarieNombre;
formFields['etablissement_effectifSalarieApprentis']                                    = activite.cadre5EffectifSalarie.etablissementEffectifSalarieApprentis;
formFields['etablissement_effectifSalarieVRP']                                          = activite.cadre5EffectifSalarie.etablissementEffectifSalarieVRP;
formFields['etablissement_effectifSalarieEmbauchePremierSalarie_oui']                   = activite.cadre5EffectifSalarie.etablissementEffectifSalarieEmbauchePremierSalarieOui ? true : false;
formFields['etablissement_effectifSalarieEmbauchePremierSalarie_non']                   = activite.etablissementEffectifSalariePresenceOui ? (activite.cadre5EffectifSalarie.etablissementEffectifSalarieEmbauchePremierSalarieOui ? false : true) : false;

// Cadre 14 - Conjoint collaborateur

var conjointInfos = $p0cmb.cadre2conjointGroup.cadre2Conjoint.cadre2InfosConjoint;
if (conjoint.conjointRole) {
formFields['personneLiee_personnePhysique_nomNaissance_conjointPacse']                  = conjointInfos.personneLieePersonnePhysiqueNomNaissanceConjointPacse;
formFields['personneLiee_personnePhysique_nomUsage_conjointPacse']                      = conjointInfos.personneLieePersonnePhysiqueNomUsageConjointPacse;
var prenomsConjoint=[];
for ( i = 0; i < conjointInfos.personneLieePersonnePhysiquePrenom1ConjointPacse.size() ; i++ ){prenomsConjoint.push(conjointInfos.personneLieePersonnePhysiquePrenom1ConjointPacse[i]);}                            
formFields['personneLiee_personnePhysique_prenom1_conjointPacse']                       = prenomsConjoint.toString();
formFields['personneLiee_personnePhysique_nationalite_conjointPacse']                   = conjointInfos.personneLieePersonnePhysiqueNationaliteConjointPacse;

if(conjointInfos.personneLieePersonnePhysiqueDateNaissanceConjointPacse != null) {
	var dateTmp = new Date(parseInt(conjointInfos.personneLieePersonnePhysiqueDateNaissanceConjointPacse.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['personneLiee_personnePhysique_dateNaissance_conjointPacse'] = date;
}

formFields['personneLiee_personnePhysique_lieuNaissanceDepartement_conjointPacse']      = conjointInfos.personneLieePersonnePhysiqueLieuNaissanceDepartementConjointPacse != null ? conjointInfos.personneLieePersonnePhysiqueLieuNaissanceDepartementConjointPacse.getId() : '';
formFields['personneLiee_personnePhysique_lieuNaissanceCommune_conjointPacse']          = Value('id').of(conjointInfos.personneLieePersonnePhysiquePaysNaissanceConjointPacse).eq('FRXXXXX') ? conjointInfos.personneLieePersonnePhysiqueLieuNaissanceCommuneConjointPacse : conjointInfos.personneLieePersonnePhysiqueLieuNaissanceVilleConjointPacse;
formFields['personneLiee_personnePhysique_lieuNaissancePays_conjointPacse']             = conjointInfos.personneLieePersonnePhysiquePaysNaissanceConjointPacse;
}

// Cadre 15 - Personne pouvoir

var personnePouvoir = $p0cmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir;

formFields['personneLiee_personneLieeEtablissementQualite_pouvoirEngagerEtablissement'] = Value('id').of($p0cmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement');
formFields['personneLiee_personneLieeEtablissementQualite_proprietaireIndivis']         = Value('id').of($p0cmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualiteProprietaireIndivis');
formFields['personneLiee_personnePhysique_nomNaissance_personnePouvoir']                = personnePouvoir.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
formFields['personneLiee_personnePhysique_nomUsage_personnePouvoir']                    = personnePouvoir.personneLieePersonnePhysiqueNomUsagePersonnePouvoir;
var prenomsPouvoir1=[];
for ( i = 0; i < personnePouvoir.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenomsPouvoir1.push(personnePouvoir.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFields['personneLiee_personnePhysique_prenom1_personnePouvoir']                       = prenomsPouvoir1.toString();
formFields['personneLiee_adresse_voie_personnePouvoir']                                 = (personnePouvoir.cadre6AdressePersonnePouvoir.rueNumeroAdressePersonnePouvoir != null ? personnePouvoir.cadre6AdressePersonnePouvoir.rueNumeroAdressePersonnePouvoir : '')
																						+ ' ' + (personnePouvoir.cadre6AdressePersonnePouvoir.indiceVoiePersonnePouvoir != null ? personnePouvoir.cadre6AdressePersonnePouvoir.indiceVoiePersonnePouvoir : '')
																						+ ' ' + (personnePouvoir.cadre6AdressePersonnePouvoir.typeVoiePersonnePouvoir != null ? personnePouvoir.cadre6AdressePersonnePouvoir.typeVoiePersonnePouvoir : '') 
																						+ ' ' + (personnePouvoir.cadre6AdressePersonnePouvoir.nomVoiePersonnePouvoir != null ? personnePouvoir.cadre6AdressePersonnePouvoir.nomVoiePersonnePouvoir : '')
																				        + ' ' + (personnePouvoir.cadre6AdressePersonnePouvoir.rueComplementAdressePersonnePouvoir != null ? personnePouvoir.cadre6AdressePersonnePouvoir.rueComplementAdressePersonnePouvoir : '')
																						+ ' ' + (personnePouvoir.cadre6AdressePersonnePouvoir.distriutionSpecialePersonnePouvoir != null ? personnePouvoir.cadre6AdressePersonnePouvoir.distriutionSpecialePersonnePouvoir : '');
formFields['personneLiee_adresse_codePostal_personnePouvoir']                           = personnePouvoir.cadre6AdressePersonnePouvoir.personneLieeAdresseCodePostalPersonnePouvoir;
formFields['personneLiee_adresse_communepersonnePouvoir']                               = personnePouvoir.cadre6AdressePersonnePouvoir.personneLieeAdresseCommunepersonnePouvoir;
if(personnePouvoir.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir != null) {
	var dateTmp = new Date(parseInt(personnePouvoir.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['personneLiee_personnePhysique_dateNaissance_personnePouvoir'] = date;
}
formFields['personneLiee_personnePhysique_lieuNaissanceDepartement_personnePouvoir']    = personnePouvoir.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir != null ? personnePouvoir.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir.getId() : '';
formFields['personneLiee_personnePhysique_lieuNaissanceCommune_personnePouvoir']        = Value('id').of($p0cmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement') ? (Value('id').of(personnePouvoir.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX') ? personnePouvoir.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir : personnePouvoir.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir) : '';
formFields['personneLiee_personnePhysique_lieuNaissancePays_personnePouvoir']           = Value('id').of($p0cmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement') ? personnePouvoir.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir : '';
formFields['personneLiee_personnePhysique_nationalite_personnePouvoir']                 = personnePouvoir.personneLieePersonnePhysiqueNationalitePersonnePouvoir;

// Deuxième fondé de pouvoir à indiquer sur le PO' intercalaire

var personnePouvoir2 = $p0cmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir2;

formFields['personneLiee_personneLieeEtablissementQualite_pouvoirEngagerEtablissement2'] = Value('id').of(personnePouvoir2.personneLieePersonneLieeEtablissementQualite2).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement2');
formFields['personneLiee_personneLieeEtablissementQualite_proprietaireIndivis2']         = Value('id').of(personnePouvoir2.personneLieePersonneLieeEtablissementQualite2).eq('PersonneLieePersonneLieeEtablissementQualiteProprietaireIndivis2');
formFields['personneLiee_personnePhysique_nomNaissance_personnePouvoir2']                = personnePouvoir2.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir2;
formFields['personneLiee_personnePhysique_nomUsage_personnePouvoir2']                    = personnePouvoir2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir2;
var prenomsPouvoir2=[];
for ( i = 0; i < personnePouvoir2.personneLieePersonnePhysiquePrenom1PersonnePouvoir2.size() ; i++ ){prenomsPouvoir2.push(personnePouvoir2.personneLieePersonnePhysiquePrenom1PersonnePouvoir2[i]);}                            
formFields['personneLiee_personnePhysique_prenom1_personnePouvoir2']                       = prenomsPouvoir2.toString();
formFields['personneLiee_adresse_nomVoie_personnePouvoir2']                              = (personnePouvoir2.cadre6AdressePersonnePouvoir2.rueNumeroAdressePersonnePouvoir2 != null ? personnePouvoir2.cadre6AdressePersonnePouvoir2.rueNumeroAdressePersonnePouvoir2 : '')
																						+ ' ' + (personnePouvoir2.cadre6AdressePersonnePouvoir2.indiceVoiePersonnePouvoir2 != null ? personnePouvoir2.cadre6AdressePersonnePouvoir2.indiceVoiePersonnePouvoir2 : '')
																						+ ' ' + (personnePouvoir2.cadre6AdressePersonnePouvoir2.typeVoiePersonnePouvoir2 != null ? personnePouvoir2.cadre6AdressePersonnePouvoir2.typeVoiePersonnePouvoir2 : '')
																						+ ' ' + (personnePouvoir2.cadre6AdressePersonnePouvoir2.nomVoiePersonnePouvoir2 != null ? personnePouvoir2.cadre6AdressePersonnePouvoir2.nomVoiePersonnePouvoir2 : '')
																						+ ' ' + (personnePouvoir2.cadre6AdressePersonnePouvoir2.rueComplementAdressePersonnePouvoir2 != null ? personnePouvoir2.cadre6AdressePersonnePouvoir2.rueComplementAdressePersonnePouvoir2 : '')
																						+ ' ' + (personnePouvoir2.cadre6AdressePersonnePouvoir2.distriutionSpecialePersonnePouvoir2 != null ? personnePouvoir2.cadre6AdressePersonnePouvoir2.distriutionSpecialePersonnePouvoir2 : '');
formFields['personneLiee_adresse_codePostal_personnePouvoir2']                           = personnePouvoir2.cadre6AdressePersonnePouvoir2.personneLieeAdresseCodePostalPersonnePouvoir2;
formFields['personneLiee_adresse_commune_personnePouvoir2']                              = personnePouvoir2.cadre6AdressePersonnePouvoir2.personneLieeAdresseCommunepersonnePouvoir2;
if(personnePouvoir2.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir2 != null) {
	var dateTmp = new Date(parseInt(personnePouvoir2.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir2.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['personneLiee_personnePhysique_dateNaissance_personnePouvoir2'] = date;
}
formFields['personneLiee_personnePhysique_lieuNaissanceDepartement_personnePouvoir2']    = personnePouvoir2.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir2 != null ? personnePouvoir2.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir2.getId() : '';
formFields['personneLiee_personnePhysique_lieuNaissanceCommune_personnePouvoir2']        = Value('id').of(personnePouvoir2.personneLieePersonneLieeEtablissementQualite2).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement2') ? (Value('id').of(personnePouvoir2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir2).eq('FRXXXXX') ? personnePouvoir2.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir2 : personnePouvoir2.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir) : '';
formFields['personneLiee_personnePhysique_lieuNaissancePays_personnePouvoir2']           = Value('id').of(personnePouvoir2.personneLieePersonneLieeEtablissementQualite2).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement2') ? personnePouvoir2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir2 : '';
formFields['personneLiee_personnePhysique_nationalite_personnePouvoir2']                 = personnePouvoir2.personneLieePersonnePhysiqueNationalitePersonnePouvoir2;

// Troisième fondé de pouvoir

var personnePouvoir3 = $p0cmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir3;

formFields['personneLiee_personneLieeEtablissementQualite_pouvoirEngagerEtablissement3'] = Value('id').of(personnePouvoir3.personneLieePersonneLieeEtablissementQualite3).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement3');
formFields['personneLiee_personneLieeEtablissementQualite_proprietaireIndivis3']         = Value('id').of(personnePouvoir3.personneLieePersonneLieeEtablissementQualite3).eq('PersonneLieePersonneLieeEtablissementQualiteProprietaireIndivis3');
formFields['personneLiee_personnePhysique_nomNaissance_personnePouvoir3']                = personnePouvoir3.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir3;
formFields['personneLiee_personnePhysique_nomUsage_personnePouvoir3']                    = personnePouvoir3.personneLieePersonnePhysiqueNomUsagePersonnePouvoir3;
var prenomsPouvoir3=[];
for ( i = 0; i < personnePouvoir3.personneLieePersonnePhysiquePrenom1PersonnePouvoir3.size() ; i++ ){prenomsPouvoir3.push(personnePouvoir3.personneLieePersonnePhysiquePrenom1PersonnePouvoir3[i]);}                            
formFields['personneLiee_personnePhysique_prenom1_personnePouvoir3']                     = prenomsPouvoir3.toString();

formFields['personneLiee_adresse_nomVoie_personnePouvoir3']                              = (personnePouvoir3.cadre6AdressePersonnePouvoir3.rueNumeroAdressePersonnePouvoir3 != null ? personnePouvoir3.cadre6AdressePersonnePouvoir3.rueNumeroAdressePersonnePouvoir3 : '')
																						+ ' ' + (personnePouvoir3.cadre6AdressePersonnePouvoir3.indiceVoiePersonnePouvoir3 != null ? personnePouvoir3.cadre6AdressePersonnePouvoir3.indiceVoiePersonnePouvoir3 : '')
																						+ ' ' + (personnePouvoir3.cadre6AdressePersonnePouvoir3.typeVoiePersonnePouvoir3 != null ? personnePouvoir3.cadre6AdressePersonnePouvoir3.typeVoiePersonnePouvoir3 : '') 
																						+ ' ' + (personnePouvoir3.cadre6AdressePersonnePouvoir3.nomVoiePersonnePouvoir3 != null ? personnePouvoir3.cadre6AdressePersonnePouvoir3.nomVoiePersonnePouvoir3: '')
																						+ ' ' + (personnePouvoir3.cadre6AdressePersonnePouvoir3.rueComplementAdressePersonnePouvoir3 != null ? personnePouvoir3.cadre6AdressePersonnePouvoir3.rueComplementAdressePersonnePouvoir3 : '') 
																						+ ' ' + (personnePouvoir3.cadre6AdressePersonnePouvoir3.distriutionSpecialePersonnePouvoir3 != null ? personnePouvoir3.cadre6AdressePersonnePouvoir3.distriutionSpecialePersonnePouvoir3: '');
formFields['personneLiee_adresse_codePostal_personnePouvoir3']                           = personnePouvoir3.cadre6AdressePersonnePouvoir3.personneLieeAdresseCodePostalPersonnePouvoir3;
formFields['personneLiee_adresse_commune_personnePouvoir3']                              = personnePouvoir3.cadre6AdressePersonnePouvoir3.personneLieeAdresseCommunepersonnePouvoir3;
if(personnePouvoir3.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir3 != null) {
	var dateTmp = new Date(parseInt(personnePouvoir3.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir3.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['personneLiee_personnePhysique_dateNaissance_personnePouvoir3'] = date;
}
formFields['personneLiee_personnePhysique_lieuNaissanceDepartement_personnePouvoir3']    = personnePouvoir3.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir3 != null ? personnePouvoir3.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir3.getId() : '';
formFields['personneLiee_personnePhysique_lieuNaissanceCommune_personnePouvoir3']        = Value('id').of(personnePouvoir3.personneLieePersonneLieeEtablissementQualite3).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement3') ? (Value('id').of(personnePouvoir3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir3).eq('FRXXXXX') ? personnePouvoir3.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir3 : personnePouvoir3.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir3) : '';
formFields['personneLiee_personnePhysique_lieuNaissancePays_personnePouvoir3']           = Value('id').of(personnePouvoir3.personneLieePersonneLieeEtablissementQualite3).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement3') ? personnePouvoir3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir3 : '';
formFields['personneLiee_personnePhysique_nationalite_personnePouvoir3']                 = personnePouvoir3.personneLieePersonnePhysiqueNationalitePersonnePouvoir3;

// Cadre 16 - Volet Social

var social = $p0cmb.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale;

var nirDeclarant = social.voletSocialNumeroSecuriteSociale;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFields['voletSocial_numeroSecuriteSociale']                = nirDeclarant.substring(0, 13);
    formFields['voletSocial_numeroSecuriteSocialeCle']             = nirDeclarant.substring(13, 15);
}
formFields['voletSocial_titreSejour_numero']                                            = social.voletSocialTitreSejourNumero;
formFields['voletSocial_titreSejour_lieuDelivranceCommune']                             = social.voletSocialTitreSejourLieuDelivranceCommune;
if(social.voletSocialTitreSejourDateExpiration != null) {
	var dateTmp = new Date(parseInt(social.voletSocialTitreSejourDateExpiration.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['voletSocial_titreSejour_dateExpiration'] = date;
}
formFields['voletSocial_activiteAutreQueDeclareeStatut_oui']                            = social.voletSocialActiviteAutreQueDeclareeStatutOui ? true : false;
formFields['voletSocial_activiteAutreQueDeclareeStatut_non']                            = social.voletSocialActiviteAutreQueDeclareeStatutOui ? false : true;
formFields['voletSocial_activiteAutreQueDeclareeStatut_salarie']                        = Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutSalarie') ? true : false;
formFields['voletSocial_activiteAutreQueDeclareeStatut_salarieAgricole']                = Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutSalarieAgricole') ? true : false;
formFields['voletSocial_activiteAutreQueDeclareeStatut_retraitePensionne']              = Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutRetraitePensionne') ? true : false;
formFields['voletSocial_activiteAutreQueDeclareeStatut_cocheAutre'] 					= Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutCocheAutre') ? true : false;
formFields['voletSocial_activiteAutreQueDeclareeStatutAutre']                           = social.voletSocialActiviteAutreQueDeclareeStatutAutre;
formFields['voletSocial_regimePraticien_oui']                                           = social.voletSocialRegimePraticienOui != null ? (Value('id').of(social.voletSocialRegimePraticienOui).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false) : false;
formFields['voletSocial_regimePraticien_non']                                           = social.voletSocialRegimePraticienOui != null ? (Value('id').of(social.voletSocialRegimePraticienOui).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false) : false;
formFields['marinProfessionnel']                                                        = social.voletSocialRegimeMarin ? true : false;
formFields['voletSocial_conjointCouvertAssuranceMaladie_oui']                           = Value('id').of($p0cmb.cadre2conjointGroup.cadre2Conjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieCollaborateur') ? (conjointInfos.voletSocialConjointCouvertAssuranceMaladieOui ? true : false) : false;
formFields['voletSocial_conjointCouvertAssuranceMaladie_non']                           = Value('id').of($p0cmb.cadre2conjointGroup.cadre2Conjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieCollaborateur') ? (conjointInfos.voletSocialConjointCouvertAssuranceMaladieOui ? false : true) : false;

var nirConjoint = conjointInfos.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(Value('id').of($p0cmb.cadre2conjointGroup.cadre2Conjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieCollaborateur') and nirConjoint != null) {
    nirConjoint = nirConjoint.replace(/ /g, "");
    formFields['voletSocial_conjointCollaborateurNumeroSecuriteSociale']    = nirConjoint.substring(0, 13);
    formFields['voletSocial_conjointCollaborateurNumeroSecuriteSocialeCle'] = nirConjoint.substring(13, 15);
}

//Cadre 17 - Options fiscales

var declarationAffectation = $p0cmb.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine;
var optionsFiscales = $p0cmb.cadre9optFiscHorsEIRLGroup.cadre9optFiscHorsEIRL

if (declarationAffectation.declarationPatrimoine.objetPartiel or not $p0cmb.cadreEIRLGroup.cadreEIRL.estEIRL) {
formFields['regimeFiscal_regimeImpositionBenefices_mBIC']                               = Value('id').of(optionsFiscales.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('regimeFiscalRegimeImpositionBeneficesMBIC') ? true : false;
formFields['regimeFiscal_regimeImpositionBenefices_rsBIC']                              = Value('id').of(optionsFiscales.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('regimeFiscalRegimeImpositionBeneficesRsBIC') ? true : false;
formFields['regimeFiscal_regimeImpositionBenefices_rnBIC']                              = Value('id').of(optionsFiscales.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('regimeFiscalRegimeImpositionBeneficesRnBIC') ? true : false;
if (optionsFiscales.impositionBenefices.regimeFiscalDateClotureExerciceComptable !== null) {
    var dateTmp = new Date(parseInt(optionsFiscales.impositionBenefices.regimeFiscalDateClotureExerciceComptable.getTimeInMillis()));
    var dateClotureEc = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateClotureEc = dateClotureEc.concat(pad(month.toString()));
    formFields['regimeFiscal_dateClotureExerciceComptable']          = dateClotureEc;
}
formFields['regimeFiscal_regimeImpositionTVA_rfTVA']                                    = Value('id').of(optionsFiscales.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('regimeFiscalRegimeImpositionBeneficesMBIC') ? true : 
																						((Value('id').of(optionsFiscales.optionsTVA.regimeTVA1).eq('regimeFiscalRegimeImpositionTVARfTVA') or Value('id').of(optionsFiscales.optionsTVA.regimeTVA2).eq('regimeFiscalRegimeImpositionTVARfTVA')) ? true : false); 
formFields['regimeFiscal_regimeImpositionTVA_rsTVA']                                    = Value('id').of(optionsFiscales.optionsTVA.regimeTVA1).eq('regimeFiscalRegimeImpositionTVARsTVA') ? true : false;
formFields['regimeFiscal_regimeImpositionTVA_mrTVA']                                    = Value('id').of(optionsFiscales.optionsTVA.regimeTVA1).eq('regimeFiscalRegimeImpositionTVAMrTVA') ? true : false;
formFields['regimeFiscal_regimeImpositionTVA_rnTVA']                                    = Value('id').of(optionsFiscales.optionsTVA.regimeTVA2).eq('regimeFiscalRegimeImpositionTVARnTVA') ? true : false;
formFields['regimeFiscal_regimeImpositionTVAOptionsParticulieres2']                     = optionsFiscales.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres2 ? true : false;
formFields['regimeFiscal_regimeImpositionTVAOptionsParticulieres1']                     = optionsFiscales.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres1 ? true : false;
}

//Cadre 18 - Observations

var correspondance = $p0cmb.cadre10RensCompGroup.cadre10RensComp ;

formFields['formalite_observations']                                                    = correspondance.formaliteObservations;

// Cadre 19 - Adresse de correspondance

formFields['adresseCorrespondanceDeclaree']                                             = (Value('id').of(correspondance.adresseCorrespond).eq('domi') or Value('id').of(correspondance.adresseCorrespond).eq('prof')) ? true : false;
formFields['adresseDeclareeCadre']                                                      = Value('id').of(correspondance.adresseCorrespond).eq('domi') ? "3" : (Value('id').of(correspondance.adresseCorrespond).eq('prof') ? "9" : ' ');
formFields['formalite_correspondance']                                                  = Value('id').of(correspondance.adresseCorrespond).eq('autre') ? true : false;
formFields['formalite_correspondanceAdresse_voie1']                                     = Value('id').of(correspondance.adresseCorrespond).eq('autre') ?
																						((correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance != null ? correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance : '')
																						+ ' ' + (correspondance.adresseCorrespondance.numeroRueAdresseCorrespondance != null ? correspondance.adresseCorrespondance.numeroRueAdresseCorrespondance : '')
																						+ ' ' + (correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance : '')
																						+ ' ' + (correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance : '')
																						+ ' ' + (correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance : '')) : '';
formFields['formalite_correspondanceAdresse_voie2']                                     = (correspondance.adresseCorrespondance.rueComplementAdresseCorrespondance != null ? correspondance.adresseCorrespondance.rueComplementAdresseCorrespondance : '')
																						+ ' ' + (correspondance.adresseCorrespondance.distriutionSpecialeVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.distriutionSpecialeVoieAdresseCorrespondance : '');
formFields['formalite_correspondanceAdresse_codePostal']                                = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal;
formFields['formalite_correspondanceAdresse_commune']									= correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune != null ? correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune : '';
formFields['formalite_telephone1']                                                      = correspondance.infosSup.formaliteTelephone1 != null ? correspondance.infosSup.formaliteTelephone1 : '';
formFields['formalite_telephone2']                                                      = correspondance.infosSup.formaliteTelephone2;
formFields['formalite_fax_courriel']                                                    = correspondance.infosSup.formaliteCourriel != null ? correspondance.infosSup.formaliteCourriel : correspondance.infosSup.telecopie;

// Cadre 20 - Non diffusion

var signataire = $p0cmb.cadre11SignatureGroup.cadre11Signature;

formFields['formalite_nonDiffusionInformation']                                         = signataire.formaliteNonDiffusionInformation ? true : false;

// Cadres 21 - Signature

var jqpa = $p0cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle;
var jqpa1 = $p0cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification1;
var jqpa2 = $p0cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification2;
var jqpa3 = $p0cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification3;

formFields['formalite_signataireQualite_declarant']                                     = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFields['formalite_signataireQualite_mandataire']                                    = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
formFields['nomPrenomDenominationMandataire']											= Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? signataire.adresseMandataire.nomPrenomDenominationMandataire : '';	
formFields['formalite_signataire_adresseVoie']                                          = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? 
																						((signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																						+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																						+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																						+ ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																						+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																						+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '')) : '';
formFields['formalite_signataire_adresseCP']                                            = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ?
																						(signataire.adresseMandataire.codePostalMandataire != null ? signataire.adresseMandataire.codePostalMandataire : '') : '';
formFields['formalite_signataire_adresseCommune']                                       = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ?
 																						(signataire.adresseMandataire.villeAdresseMandataire != null ? signataire.adresseMandataire.villeAdresseMandataire : '') : '';
formFields['formalite_signatureLieu']                                                   = signataire.formaliteSignatureLieu;
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['formalite_signatureDate'] = date;
}
formFields['estEIRL_oui']                                                               = $p0cmb.cadreEIRLGroup.cadreEIRL.estEIRL ? true : false;
formFields['estEIRL_non']                                                               = $p0cmb.cadreEIRLGroup.cadreEIRL.estEIRL ? false : true;
formFields['formalite_nombreActivitesSoumisQualification']                              = (jqpa3.jqpaSituation != null ? "3" : (jqpa2.jqpaSituation != null ? "2" : (jqpa1.jqpaSituation != null ? "1" : "0")));
// A conditionner selon l'activité
formFields['formalite_nombreP0prime']                                                   = (accre.entrepriseAutreEtablissementUE or personnePouvoir.autreDeclarationPersonneLiee or accre.entrepriseContratAppuiPresence or (activite.etablissementActivitesPrincipales.length > 90)) ? "1" : "0";
formFields['ndi_oui']                                                                   = activite.etablissementNomDomaine != null ? true : false;
formFields['ndi_non']                                                                   = activite.etablissementNomDomaine != null ? false : true;
formFields['signature']                                                                 = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce";


//P0' Intercalaire

formFields['complete_p0pl']                                                              = false;
formFields['complete_aco']                                                               = false;
formFields['complete_p0agr']                                                             = false;
formFields['complete_p0cmb']                                                             = true;
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFields['personneLiee_personnePhysique_nomNaissance_prenom']                          = identite.personneLieePersonnePhysiqueNomNaissance + ' ' + prenoms.toString();
formFields['personneLiee_personnePhysique_dateNaissance_P0Prime']                        = identite.personneLieePersonnePhysiqueDateNaissance;
formFields['numero_intercalaire']                                                        = "1";

if (accre.entrepriseContratAppuiPresence) {
formFields['complete_cadre']                                                            = "5";
formFields['entrepriseLieeContratAppui_denomination']                                   = accre.cadre3ContratAppuiInfos.contratAppuiDenomination 
																						+ ' / ' + accre.cadre3ContratAppuiInfos.contratAppuiSiren 
																						+ ' / ' + (accre.cadre3ContratAppuiInfos.contratAppuiAdresse.numeroAdresseContratAppui != null ? accre.cadre3ContratAppuiInfos.contratAppuiAdresse.numeroAdresseContratAppui : '')
																						+ ' ' + (accre.cadre3ContratAppuiInfos.contratAppuiAdresse.indiceAdresseContratAppui != null ? accre.cadre3ContratAppuiInfos.contratAppuiAdresse.indiceAdresseContratAppui : '')
																						+ ' ' + (accre.cadre3ContratAppuiInfos.contratAppuiAdresse.typeAdresseContratAppui	!= null ? accre.cadre3ContratAppuiInfos.contratAppuiAdresse.typeAdresseContratAppui : '')
																						+ ' ' + accre.cadre3ContratAppuiInfos.contratAppuiAdresse.voieAdresseContratAppui
																						+ ' ' + (accre.cadre3ContratAppuiInfos.contratAppuiAdresse.complementAdresseContratAppui != null ? accre.cadre3ContratAppuiInfos.contratAppuiAdresse.complementAdresseContratAppui : '')	
																						+ ' ' + (accre.cadre3ContratAppuiInfos.contratAppuiAdresse.distributionSpecialeAdresseContratAppui != null ? accre.cadre3ContratAppuiInfos.contratAppuiAdresse.distributionSpecialeAdresseContratAppui : '')
																						+ ' ' +  accre.cadre3ContratAppuiInfos.contratAppuiAdresse.codePostalAdresseContratAppui 
																						+ ' ' + accre.cadre3ContratAppuiInfos.contratAppuiAdresse.communeAdresseContratAppui;
formFields['complete_cerfa']                                                            = "P0 CM";
if (activite.etablissementActivitesPrincipales.length > 90) {
formFields['complete_cadre2']                                                           = "10";
formFields['entrepriseLieeContratAppui_adresseVoie']                                    = "Suite activité :" + ' ' + activite.etablissementActivitesPrincipales.substring(75, 200); 
if (activite.etablissementActivitesPrincipales.length > 200) {
formFields['entrepriseLieeContratAppui_adresseCommune']   								= activite.etablissementActivitesPrincipales.substring(200, 340); 
}}
} else if (not accre.entrepriseContratAppuiPresence and (activite.etablissementActivitesPrincipales.length > 90))  {
formFields['complete_cadre']                                                           = "10";
formFields['complete_cerfa']                                                            = "P0 CM";
formFields['entrepriseLieeContratAppui_denomination']                                    = "Suite activité :" + ' ' + activite.etablissementActivitesPrincipales.substring(75, 200); 
if (activite.etablissementActivitesPrincipales.length > 200) {
formFields['entrepriseLieeContratAppui_adresseVoie']   								= activite.etablissementActivitesPrincipales.substring(200, 340); 
}}


// PEIRL CMB

//Cadre 1 PEIRL CMB - Déclaration ou modification d'affectation de patrimoine

formFields['eirl_complete_p2cmb']                                                           = false;
formFields['eirl_complete_p4cmb']                                                           = false;
formFields['eirl_complete_p0cmb']                                                           = true;
formFields['eirl_complete_p0me']                                                            = false;
formFields['declaration_initiale']															= true;
formFields['declaration_modification']                                                      = false;

// Cadre 2 PEIRL CMB- Rappel d'identification

formFields['eirl_siren']                                                                    = '';
formFields['eirl_nomNaissance']                                                             = $p0cmb.cadre1IdentiteGroup.cadre1Identite.personneLieePersonnePhysiqueNomNaissance;
formFields['eirl_nomUsage']                                                                 = identite.personneLieePersonnePhysiqueNomUsage;
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFields['eirl_prenom']                                                   = prenoms.toString();

// Cadre 3 - Déclaration d'affectation de patrimoine

var declarationAffectation = $p0cmb.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine;

formFields['eirl_statutEIRL_declarationInitialeSansDepot']                                  = Value('id').of(declarationAffectation.eirlDepot).eq('eirlSansDepot') ? true : false;
formFields['eirl_statutEIRL_declarationInitialeAvecDepot']                                  = Value('id').of(declarationAffectation.eirlDepot).eq('eirlAvecDepot') ? true : false;
formFields['eirl_statutEIRL_reprise']                                                       = Value('id').of(declarationAffectation.eirlStatut).eq('EirlStatutEIRLReprise') ? true : false;
formFields['eirl_denomination']                                                             = declarationAffectation.declarationPatrimoine.eirlDenomination;
if (declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable !== null) {
    var dateTmp = new Date(parseInt(declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable.getTimeInMillis()));
    var dateClotureEc = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateClotureEc = dateClotureEc.concat(pad(month.toString()));
    formFields['eirl_dateClotureExerciceComptable']          = dateClotureEc;
}
formFields['eirl_objet']                                                                    = declarationAffectation.declarationPatrimoine.objetPartiel ? declarationAffectation.declarationPatrimoine.eirlObjet : ($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.etablissementActivitesPrincipales);
formFields['eirl_depotRCS']                                                                 = Value('id').of(declarationAffectation.declarationPatrimoine.eirlChoixDepotRegistre).eq('EirlDepotRCS') ? true : false;
formFields['eirl_depotRM']                                                                  = Value('id').of(declarationAffectation.declarationPatrimoine.eirlChoixDepotRegistre).eq('EirlDepotRM') ? true : false;
formFields['eirl_precedentEIRLDenomination']                                                = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLDenomination;
formFields['eirl_precedentEIRLRegistre_rcs']                                                = Value('id').of(declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLRegistre).eq('EirlPrecedentEIRLRegistreRcs') ? true : false;
formFields['eirl_precedentEIRLRegistre_rm']                                                 = Value('id').of(declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLRegistre).eq('EirlPrecedentEIRLRegistreRm') ? true : false;
formFields['eirl_precedentEIRLRegistre_rseirl']                                             = Value('id').of(declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLRegistre).eq('EirlPrecedentEIRLRegistreRseirl') ? true : false;
formFields['eirl_precedentEIRLLieuImmatriculation']                                         = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLLieuImmatriculation;
formFields['eirl_precedentEIRLSIREN']                                                       = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN != null ? (declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN.split(' ').join('')) : '';

// Cadre 7 - Options fiscales

formFields['eirl_regimeFiscal_regimeImpositionBenefices_mBIC']                    = Value('id').of(declarationAffectation.cadre3OptionsFiscalesEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('EirlRegimeFiscalRegimeImpositionBeneficesMBIC') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rsBIC']                   = (Value('id').of(declarationAffectation.cadre3OptionsFiscalesEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('EirlRegimeFiscalRegimeImpositionBeneficesReelBIC') and Value('id').of(declarationAffectation.cadre3OptionsFiscalesEIRL.impositionBeneficesEIRL.regimeFiscalReel).eq('EirlRegimeFiscalRegimeImpositionBeneficesRs')) ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rnBIC']                   = (Value('id').of(declarationAffectation.cadre3OptionsFiscalesEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('EirlRegimeFiscalRegimeImpositionBeneficesReelBIC') and Value('id').of(declarationAffectation.cadre3OptionsFiscalesEIRL.impositionBeneficesEIRL.regimeFiscalReel).eq('EirlRegimeFiscalRegimeImpositionBeneficesRn')) ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_optionIS']                = Value('id').of(declarationAffectation.cadre3OptionsFiscalesEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('EirlRegimeFiscalRegimeImpositionBeneficesOptionIS') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rsIS']                    = (Value('id').of(declarationAffectation.cadre3OptionsFiscalesEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('EirlRegimeFiscalRegimeImpositionBeneficesOptionIS') and Value('id').of(declarationAffectation.cadre3OptionsFiscalesEIRL.impositionBeneficesEIRL.regimeFiscalReel).eq('EirlRegimeFiscalRegimeImpositionBeneficesRs')) ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rnIS']                    = (Value('id').of(declarationAffectation.cadre3OptionsFiscalesEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('EirlRegimeFiscalRegimeImpositionBeneficesOptionIS') and Value('id').of(declarationAffectation.cadre3OptionsFiscalesEIRL.impositionBeneficesEIRL.regimeFiscalReel).eq('EirlRegimeFiscalRegimeImpositionBeneficesRn')) ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVA_rfTVA']                         = Value('id').of(declarationAffectation.cadre3OptionsFiscalesEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('EirlRegimeFiscalRegimeImpositionBeneficesMBIC') ? true : ((Value('id').of(declarationAffectation.cadre3OptionsFiscalesEIRL.optionsTVA.regimeTVA1).eq('EirlRegimeFiscalRegimeImpositionTVARfTVA') or Value('id').of(declarationAffectation.cadre3OptionsFiscalesEIRL.optionsTVA.regimeTVA2).eq('EirlRegimeFiscalRegimeImpositionTVARfTVA')) ? true : false);
formFields['eirl_regimeFiscal_regimeImpositionTVA_rsTVA']                         = Value('id').of(declarationAffectation.cadre3OptionsFiscalesEIRL.optionsTVA.regimeTVA1).eq('EirlRegimeFiscalRegimeImpositionTVARsTVA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVA_mrTVA']                         = Value('id').of(declarationAffectation.cadre3OptionsFiscalesEIRL.optionsTVA.regimeTVA1).eq('EirlRegimeFiscalRegimeImpositionTVAMrTVA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVA_rnTVA']                         = Value('id').of(declarationAffectation.cadre3OptionsFiscalesEIRL.optionsTVA.regimeTVA2).eq('EirlRegimeFiscalRegimeImpositionTVARnTVA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVAOptionsParticulieres2']          = declarationAffectation.cadre3OptionsFiscalesEIRL.optionsTVA.eirlRegimeFiscalRegimeImpositionTVAOptionsParticulieres2 ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVAOptionsParticulieres1']          = declarationAffectation.cadre3OptionsFiscalesEIRL.optionsTVA.eirlregimeFiscalRegimeImpositionTVAOptionsParticulieres1 ? true : false;

// NDI

formFields['complete_immatriculation_ndi']                                 = true;
formFields['complete_modification_ndi']                                    = false;
formFields['nomNaissance_ndi']                                             = identite.personneLieePersonnePhysiqueNomNaissance;
formFields['nomUsage_ndi']                                                 = identite.personneLieePersonnePhysiqueNomUsage;
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFields['prenom_ndi']                                                   = prenoms.toString();
if(activite.etablissementDateDebutActivite != null) {
	var dateTmp = new Date(parseInt(activite.etablissementDateDebutActivite.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['dateDebutActivite_ndi'] = date;
}	
formFields['adresseEtablissement_voie_ndi']                                             = Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? 
																						((adresse.personneLieePersonnePhysiqueAdresserueNumeroVoie != null ? adresse.personneLieePersonnePhysiqueAdresserueNumeroVoie : '') 
																						+ ' ' + (adresse.personneLieePersonnePhysiqueAdresseIndiceVoie != null ? adresse.personneLieePersonnePhysiqueAdresseIndiceVoie : '') 
																						+ ' ' + (adresse.personneLieePersonnePhysiqueAdresseTypeVoie != null ? adresse.personneLieePersonnePhysiqueAdresseTypeVoie : '') 
																						+ ' ' + (adresse.personneLieePersonnePhysiqueAdresseNomVoie !=null ? adresse.personneLieePersonnePhysiqueAdresseNomVoie : '')           
																						+ ' ' + (adresse.personneLieePersonnePhysiquerueAdresseComplementAdresse != null ? adresse.personneLieePersonnePhysiquerueAdresseComplementAdresse : '') 
																						+ ' ' + (adresse.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie != null ? adresse.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie :''))
																						: ((adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseNumRue != null ? adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseNumRue : '') 
																						+ ' ' + (adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseIndiceVoieAdresse != null ? adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseIndiceVoieAdresse : '') 
																						+ ' ' + (adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseTypeVoieAdresse != null ? adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseTypeVoieAdresse : '') 
																						+ ' ' + (adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseNomVoieAdresse != null ? adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseNomVoieAdresse : '')
																				        + ' ' + (adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseRueComplementAdresse != null ? adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseRueComplementAdresse : '') 
																						+ ' ' + (adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseAdresseDistriutionSpecialeVoie != null ? adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseAdresseDistriutionSpecialeVoie : ''));

formFields['adresseEtablissement_codePostal_ndi']                                       = Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? 
																						adresse.personneLieePersonnePhysiqueAdresseCodePostal : adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseCodePostal;
formFields['adresseEtablissement_commune_ndi']                                            = Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? 
																						adresse.personneLieePersonnePhysiqueAdresseCommune : adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseCommune;

formFields['nomDomaineEtablissement_ndi']                                               = activite.etablissementNomDomaine;
formFields['declarationInitialeNomDomaineEtablissement_ndi']                            = true;



// JQPA 1

var formFieldsPers1 = {};

// Cadre 1
formFieldsPers1['jqpa_intercalaire']                         = true;
formFieldsPers1['jqpa_formulaire']                           = false;


// Cadre 2
formFieldsPers1['entreprise_dirigeants_ppNomUsage']          = identite.personneLieePersonnePhysiqueNomUsage != null ? identite.personneLieePersonnePhysiqueNomUsage : ''  ;
formFieldsPers1['entreprise_dirigeants_ppNomNaissance']      = identite.personneLieePersonnePhysiqueNomNaissance;
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFieldsPers1['entreprise_dirigeants_ppPrenom']            = prenoms.toString();
if(identite.personneLieePersonnePhysiqueDateNaissance != null) {
	var dateTmp = new Date(parseInt(identite.personneLieePersonnePhysiqueDateNaissance.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers1['entreprise_dirigeants_ppDateNaissance'] = date;
}

 // Cadre 3A
formFieldsPers1['jqpa_activiteJqpa_PP']                      = jqpa1.jqpaActiviteJqpaPP;

//Cadre 3B
formFieldsPers1['jqpa_aqpaSituationPP_diplome']              = Value('id').of(jqpa1.jqpaSituation).eq('jqpaSituationDiplome') or Value('id').of(jqpa1.jqpaSituation).eq('jqpaSituationExperience') ? true : false;

// Cadre 3C
formFieldsPers1['jqpa_qualitePersonneQualifieePP_declarant'] = Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPDeclarant') ? true : false;
formFieldsPers1['jqpa_qualitePersonneQualifieePP_conjoint']  = Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPConjoint') ? true : false;
formFieldsPers1['jqpa_qualitePersonneQualifieePP_salarie']   = Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') ? true : false;
formFieldsPers1['jqpa_qualitePersonneQualifieePP_autre']     = Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre') ? true : false;
formFieldsPers1['jqpa_qualitePersonneQualifieePPAutre']      = jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifieePPChampAutre;

formFieldsPers1['jqpa_identitePPNomNaissance']               = jqpa1.identitePersonneQualifiee.jqpaIdentitePPNomNaissance != null ? jqpa1.identitePersonneQualifiee.jqpaIdentitePPNomNaissance : '';
formFieldsPers1['jqpa_identitePPNomUsage']                   = jqpa1.identitePersonneQualifiee.jqpaIdentitePPNomUsage != null ? jqpa1.identitePersonneQualifiee.jqpaIdentitePPNomUsage : '';
var prenoms=[];
for ( i = 0; i < jqpa1.identitePersonneQualifiee.jqpaIdentitePPPrenom.size() ; i++ ){prenoms.push(jqpa1.identitePersonneQualifiee.jqpaIdentitePPPrenom[i]);}                            
formFieldsPers1['jqpa_identitePPPrenom']            = prenoms.toString();

if (jqpa1.identitePersonneQualifiee.jqpaIdentitePPDateNaissance !== null) {
	var dateTmp = new Date(parseInt(jqpa1.identitePersonneQualifiee.jqpaIdentitePPDateNaissance.getTimeInMillis()));
	var dateNaissance = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	dateNaissance = dateNaissance.concat(pad(month.toString()));
	dateNaissance = dateNaissance.concat(dateTmp.getFullYear().toString());
	formFieldsPers1['jqpa_identitePPDateNaissance']          = dateNaissance;
}
formFieldsPers1['jqpa_identitePPDepartement']                = jqpa1.identitePersonneQualifiee.jqpaIdentitePPDepartement != null ? jqpa1.identitePersonneQualifiee.jqpaIdentitePPDepartement.getId() : '';
formFieldsPers1['jqpa_identitePPLieuNaissancePays']          = (Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') or Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre')) ? ((jqpa1.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune != null ? jqpa1.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune : jqpa1.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceVille) + ' / ' + jqpa1.identitePersonneQualifiee.jqpaIdentitePPLieuNaissancePays) : '';

// Cadre 3C
formFieldsPers1['jqpa_aqpaSituationPP_engagement']           = Value('id').of(jqpa1.jqpaSituation).eq('jqpaSituationRecrutement') ? true : false;

/* A NE GENERER QUE SI FORMULAIRE ET NON EN TANT QU'INTERCALAIRE
// Cadre 6
formFieldsPers1['jqpa_formalite_declarant']                  = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFieldsPers1['jqpa_formalite_representant']               = false;
formFieldsPers1['jqpa_formalite_mandataire']                 = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
formFieldsPers1['jqpa_formalite_mandataireInfo']             = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? signataire.adresseMandataire.nomPrenomDenominationMandataire : '';	
formFieldsPers1['jqpa_formalite_lieu']                       = signataire.formaliteSignatureLieu;
formFieldsPers1['jqpa_formalite_date']                       = signataire.formaliteSignatureDate;
formFieldsPers1['jqpa_formalite_signature']                  = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce";
*/

// JQPA 2

var formFieldsPers2 = {};

// Cadre 1
formFieldsPers2['jqpa_intercalaire']                         = true;
formFieldsPers2['jqpa_formulaire']                           = false;


// Cadre 2
formFieldsPers2['entreprise_dirigeants_ppNomUsage']          = identite.personneLieePersonnePhysiqueNomUsage != null ? identite.personneLieePersonnePhysiqueNomUsage : ''  ;
formFieldsPers2['entreprise_dirigeants_ppNomNaissance']      = identite.personneLieePersonnePhysiqueNomNaissance;
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFieldsPers2['entreprise_dirigeants_ppPrenom']            = prenoms.toString();
if(identite.personneLieePersonnePhysiqueDateNaissance != null) {
	var dateTmp = new Date(parseInt(identite.personneLieePersonnePhysiqueDateNaissance.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers2['entreprise_dirigeants_ppDateNaissance'] = date;
}

 var jqpa2 = $p0cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification2;

 // Cadre 3A
formFieldsPers2['jqpa_activiteJqpa_PP']                      = jqpa2.jqpaActiviteJqpaPP;

//Cadre 3B
formFieldsPers2['jqpa_aqpaSituationPP_diplome']              = Value('id').of(jqpa2.jqpaSituation).eq('jqpaSituationDiplome') or Value('id').of(jqpa2.jqpaSituation).eq('jqpaSituationExperience') ? true : false;

// Cadre 3C
formFieldsPers2['jqpa_qualitePersonneQualifieePP_declarant'] = Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPDeclarant') ? true : false;
formFieldsPers2['jqpa_qualitePersonneQualifieePP_conjoint']  = Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPConjoint') ? true : false;
formFieldsPers2['jqpa_qualitePersonneQualifieePP_salarie']   = Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') ? true : false;
formFieldsPers2['jqpa_qualitePersonneQualifieePP_autre']     = Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre') ? true : false;
formFieldsPers2['jqpa_qualitePersonneQualifieePPAutre']      = jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifieePPChampAutre;

formFieldsPers2['jqpa_identitePPNomNaissance']               = jqpa2.identitePersonneQualifiee.jqpaIdentitePPNomNaissance != null ? jqpa2.identitePersonneQualifiee.jqpaIdentitePPNomNaissance : '';
formFieldsPers2['jqpa_identitePPNomUsage']                   = jqpa2.identitePersonneQualifiee.jqpaIdentitePPNomUsage != null ? jqpa2.identitePersonneQualifiee.jqpaIdentitePPNomUsage : '';
var prenoms=[];
for ( i = 0; i < jqpa2.identitePersonneQualifiee.jqpaIdentitePPPrenom.size() ; i++ ){prenoms.push(jqpa2.identitePersonneQualifiee.jqpaIdentitePPPrenom[i]);}                            
formFieldsPers2['jqpa_identitePPPrenom']            = prenoms.toString();

if (jqpa2.identitePersonneQualifiee.jqpaIdentitePPDateNaissance !== null) {
	var dateTmp = new Date(parseInt(jqpa2.identitePersonneQualifiee.jqpaIdentitePPDateNaissance.getTimeInMillis()));
	var dateNaissance = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	dateNaissance = dateNaissance.concat(pad(month.toString()));
	dateNaissance = dateNaissance.concat(dateTmp.getFullYear().toString());
	formFieldsPers2['jqpa_identitePPDateNaissance']          = dateNaissance;
}
formFieldsPers2['jqpa_identitePPDepartement']                = jqpa2.identitePersonneQualifiee.jqpaIdentitePPDepartement != null ? jqpa2.identitePersonneQualifiee.jqpaIdentitePPDepartement.getId() : '';
formFieldsPers2['jqpa_identitePPLieuNaissancePays']          = (Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') or Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre')) ? ((jqpa2.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune != null ? jqpa2.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune : jqpa2.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceVille) + ' / ' + jqpa2.identitePersonneQualifiee.jqpaIdentitePPLieuNaissancePays) : '';

// Cadre 3C
formFieldsPers2['jqpa_aqpaSituationPP_engagement']           = Value('id').of(jqpa2.jqpaSituation).eq('jqpaSituationRecrutement') ? true : false;


// JQPA 3

var formFieldsPers3 = {};

// Cadre 1
formFieldsPers3['jqpa_intercalaire']                         = true;
formFieldsPers3['jqpa_formulaire']                           = false;


// Cadre 2
formFieldsPers3['entreprise_dirigeants_ppNomUsage']          = identite.personneLieePersonnePhysiqueNomUsage != null ? identite.personneLieePersonnePhysiqueNomUsage : ''  ;
formFieldsPers3['entreprise_dirigeants_ppNomNaissance']      = identite.personneLieePersonnePhysiqueNomNaissance;
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFieldsPers3['entreprise_dirigeants_ppPrenom']            = prenoms.toString();
if(identite.personneLieePersonnePhysiqueDateNaissance != null) {
	var dateTmp = new Date(parseInt(identite.personneLieePersonnePhysiqueDateNaissance.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers3['entreprise_dirigeants_ppDateNaissance'] = date;
}

 var jqpa3 = $p0cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification3;

 // Cadre 3A
formFieldsPers3['jqpa_activiteJqpa_PP']                      = jqpa3.jqpaActiviteJqpaPP;

//Cadre 3B
formFieldsPers3['jqpa_aqpaSituationPP_diplome']              = Value('id').of(jqpa3.jqpaSituation).eq('jqpaSituationDiplome') or Value('id').of(jqpa3.jqpaSituation).eq('jqpaSituationExperience') ? true : false;

// Cadre 3C
formFieldsPers3['jqpa_qualitePersonneQualifieePP_declarant'] = Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPDeclarant') ? true : false;
formFieldsPers3['jqpa_qualitePersonneQualifieePP_conjoint']  = Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPConjoint') ? true : false;
formFieldsPers3['jqpa_qualitePersonneQualifieePP_salarie']   = Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') ? true : false;
formFieldsPers3['jqpa_qualitePersonneQualifieePP_autre']     = Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre') ? true : false;
formFieldsPers3['jqpa_qualitePersonneQualifieePPAutre']      = jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifieePPChampAutre;

formFieldsPers3['jqpa_identitePPNomNaissance']               = jqpa3.identitePersonneQualifiee.jqpaIdentitePPNomNaissance != null ? jqpa3.identitePersonneQualifiee.jqpaIdentitePPNomNaissance : '';
formFieldsPers3['jqpa_identitePPNomUsage']                   = jqpa3.identitePersonneQualifiee.jqpaIdentitePPNomUsage != null ? jqpa3.identitePersonneQualifiee.jqpaIdentitePPNomUsage : '';
var prenoms=[];
for ( i = 0; i < jqpa3.identitePersonneQualifiee.jqpaIdentitePPPrenom.size() ; i++ ){prenoms.push(jqpa3.identitePersonneQualifiee.jqpaIdentitePPPrenom[i]);}                            
formFieldsPers3['jqpa_identitePPPrenom']            = prenoms.toString();

if (jqpa3.identitePersonneQualifiee.jqpaIdentitePPDateNaissance !== null) {
	var dateTmp = new Date(parseInt(jqpa3.identitePersonneQualifiee.jqpaIdentitePPDateNaissance.getTimeInMillis()));
	var dateNaissance = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	dateNaissance = dateNaissance.concat(pad(month.toString()));
	dateNaissance = dateNaissance.concat(dateTmp.getFullYear().toString());
	formFieldsPers3['jqpa_identitePPDateNaissance']          = dateNaissance;
}
formFieldsPers3['jqpa_identitePPDepartement']                = jqpa3.identitePersonneQualifiee.jqpaIdentitePPDepartement != null ? jqpa3.identitePersonneQualifiee.jqpaIdentitePPDepartement.getId() : '';
formFieldsPers3['jqpa_identitePPLieuNaissancePays']          = (Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') or Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre')) ? ((jqpa3.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune != null ? jqpa3.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune : jqpa3.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceVille) + ' / ' + jqpa3.identitePersonneQualifiee.jqpaIdentitePPLieuNaissancePays) : '';

// Cadre 3C
formFieldsPers3['jqpa_aqpaSituationPP_engagement']           = Value('id').of(jqpa3.jqpaSituation).eq('jqpaSituationRecrutement') ? true : false;


/*
 * Création du dossier avec volet social : ajout du cerfa avec volet social
 */
var cerfaDoc1 = nash.doc //
	.load('models/cerfa_11676-10_p0cmbSaufME_avec_volet_social.pdf') //
	.apply(formFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));

/*
 * Création du dossier sans volet social : ajout du cerfa sans volet social
 */
 
 var cerfaDoc2 = nash.doc //
	.load('models/cerfa_11676-10_p0cmbSaufME_sans_volet_social.pdf') //
	.apply (formFields);
	cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));


/*
 * Ajout de l'intercalaire P0 Prime avec volet social
 */
 
 if ((accre.entrepriseAutreEtablissementUE == true) or (personnePouvoir.autreDeclarationPersonneLiee == true) or (accre.entrepriseContratAppuiPresence) or (activite.etablissementActivitesPrincipales.length > 90))
{
	var p0PrimeDoc1 = nash.doc //
		.load('models/cerfa_11771-04_P0_Prime.pdf') //
		.apply (formFields);
	cerfaDoc1.append(p0PrimeDoc1.save('cerfa.pdf'));
}


/*
 * Ajout de l'intercalaire PEIRL avec option fiscale
 */
if ($p0cmb.cadreEIRLGroup.cadreEIRL.estEIRL == true)
{
	var peirlDoc1 = nash.doc //
		.load('models/cerfa_14215-04_peirlcmb_avec_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(peirlDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire PEIRL sans option fiscale
 */
if ($p0cmb.cadreEIRLGroup.cadreEIRL.estEIRL == true)
{
	var peirlDoc2 = nash.doc //
		.load('models/cerfa_14215-04_peirlcmb_sans_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(peirlDoc2.save('cerfa.pdf'));
}

 /*
 * Ajout de l'intercalaire NDI 1er exemplaire
 */
 
 if (activite.etablissementNomDomaine != null)
{
	var p0NDIDoc1 = nash.doc //
		.load('models/cerfa_14943-01_NDI_volet1.pdf') //
		.apply (formFields);
	cerfaDoc1.append(p0NDIDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire NDI 2ème exemplaire
 */
 
 if (activite.etablissementNomDomaine != null)
{
	var p0NDIDoc2 = nash.doc //
		.load('models/cerfa_14943-01_NDI_volet2.pdf') //
		.apply (formFields);
	cerfaDoc1.append(p0NDIDoc2.save('cerfa.pdf'));
}
 
 
 /*
 * Ajout de l'intercalaire JQPA 1
 */
 
  if (jqpa1.jqpaActiviteJqpaPP != null) {

	var p0JQPADoc1 = nash.doc //
		.load('models/cerfa_14077-02_JQPA.pdf') //
		.apply (formFieldsPers1);
	cerfaDoc1.append(p0JQPADoc1.save('cerfa.pdf'));
}
/*
 * Ajout de l'intercalaire JQPA 2
 */
 
  if (jqpa2.jqpaActiviteJqpaPP != null) {
	var p0JQPADoc2 = nash.doc //
		.load('models/cerfa_14077-02_JQPA.pdf') //
		.apply (formFieldsPers2);
	cerfaDoc1.append(p0JQPADoc2.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire JQPA 3
 */
 
  if (jqpa3.jqpaActiviteJqpaPP != null) {

	var p0JQPADoc3 = nash.doc //
		.load('models/cerfa_14077-02_JQPA.pdf') //
		.apply (formFieldsPers3);
	cerfaDoc1.append(p0JQPADoc3.save('cerfa.pdf'));
}  



/*
 * Ajout des PJs
 */
var pjUser = [];
var metas = [];

function pushPjPreview(fld) {
	fld.forEach(function (elm) {
        pjUser.push(elm);
        metas.push({'name':'document', 'value': '/'+elm.getAbsolutePath()});
    });
}

// PJ Déclarant

pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDeclarant);
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDActiviteReglementee);

var pj=$p0cmb.cadre11SignatureGroup.cadre11Signature.soussigne;

if(Value('id').of(pj).contains('FormaliteSignataireQualiteDeclarant')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantSignataire);
}

if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantNonSignataire);
} 

// PJ Mandataire

if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPouvoir);
}

if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire);
}

// PJ Conjoint

var pj=$p0cmb.cadre2conjointGroup.cadre2Conjoint.statutConjointCollaborateur ;
if(Value('id').of(pj).contains('PersonneLieeConjointCollaborateurSalarieCollaborateur')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDConjoint);
}

var pj=$p0cmb.cadre2conjointGroup.cadre2Conjoint ;
if(pj.conjointCommunauteBiens and pj.conjoint) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjInformationConjoint);
}

var pj=$p0cmb.cadre2conjointGroup.cadre2Conjoint.statutConjointCollaborateur ;
if(Value('id').of(pj).contains('PersonneLieeConjointCollaborateurSalarieCollaborateur') or Value('id').of(pj).contains('PersonneLieeConjointCollaborateurSalarieSalarie')) {
 	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjChoixStatutConjoint);
}

// PJ Fondé de pouvoir 1

var pj=$p0cmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.personneLieePersonneLieeEtablissementQualite ;
if(Value('id').of(pj).contains('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement') and $p0cmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.personneLieePersonneLieeEtablissement) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCPersonnePouvoir);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDPersonnePouvoir);
}

// PJ Fondé de pouvoir 2

var pj=$p0cmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir2.personneLieePersonneLieeEtablissementQualite2 ;
if(Value('id').of(pj).contains('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement2') and $p0cmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir.autreDeclarationPersonneLiee) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCPersonnePouvoir2);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDPersonnePouvoir2);
}

// PJ Fondé de pouvoir 3

var pj=$p0cmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir3.personneLieePersonneLieeEtablissementQualite3 ;
if(Value('id').of(pj).contains('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement3') and $p0cmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir2.autreDeclarationPersonneLiee2) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCPersonnePouvoir3);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDPersonnePouvoir3);
}

// PJ Contrat Appui

var pj=$p0cmb.cadre3InformationsComplementairesGroup.cadre3InformationsComplementaires ;
if(pj.entrepriseContratAppuiPresence) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjContratAppui);
}

// PJ mineur émancipé

var pj=$p0cmb.cadre1IdentiteGroup.cadre1Identite ;
if(pj.personneLieePersonnePhysiqueMineurEmancipe) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDMineurEmancipe);
}

// PJ Insaisissabilité

var pj=$p0cmb.cadre3InformationsComplementairesGroup.cadre3InformationsComplementaires ;
if(pj.cadre3DeclarationInsaisissabilite.entrepriseInsaisissabiliteDeclarationAutresBiens) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjInsaisissabilite);
}

// PJ Etablissement

var pj=$p0cmb.cadre4AdresseActiviteGroup.cadre4AdresseActivite.activiteLieu ;
if(Value('id').of(pj).contains('entrepriseAdresseEntrepriseDomicile')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDomicile);
}

var pj=$p0cmb.cadre4AdresseActiviteGroup.cadre4AdresseActivite.activiteLieu ;
if(Value('id').of(pj).contains('etablissementDomiciliationOui')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjContratDomiciliation);
}

var pj=$p0cmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsCreation') or ((Value('id').of(pj).contains('EtablissementOrigineFondsAchat')) and $p0cmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds.precedentExploitant.fondsArtisanal) or (Value('id').of(pj).contains('EtablissementOrigineFondsCocheAutre'))) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjEtablissementCreation);
}

var pj=$p0cmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsLocationGerance')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjLocationGerance);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjLocationGerancePublication);
}

var pj=$p0cmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsGeranceMandat')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjGeranceMandat);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjGeranceMandatPublication);
}

var pj=$p0cmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds ;
if((Value('id').of(pj).contains('EtablissementOrigineFondsAchat')) and $p0cmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds.precedentExploitant.cessionFonds) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPlanCession);
}

var pj=$p0cmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds ;
if((Value('id').of(pj).contains('EtablissementOrigineFondsAchat')) and ($p0cmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds.precedentExploitant.etablissementJournalAnnoncesLegalesNom != null)) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjAchat);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjAchatPublication);
}

// PJ Eirl

var pj=$p0cmb.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine;
if($p0cmb.cadreEIRLGroup.cadreEIRL.estEIRL and not Value('id').of(pj.eirlDepot).eq('eirlSansDepot')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAP);
}

var pj=$p0cmb.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine;
if(Value('id').of(pj.cadreEIRLInformationsComplementaires.contenuDAP).contains('bienImmo') or Value('id').of(pj.cadreEIRLInformationsComplementairesBis.contenuDAPBis).contains('bienImmo')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAPActeNotarie);
}

if(Value('id').of(pj.cadreEIRLInformationsComplementaires.contenuDAP).contains('bienCommunIndivis') or Value('id').of(pj.cadreEIRLInformationsComplementairesBis.contenuDAPBis).contains('bienCommunIndivis')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAPAccordTiers);
}

var pj=$p0cmb.cadre1IdentiteGroup.cadre1Identite ;
if(not(pj.personneLieePersonnePhysiqueMineurEmancipe) and $p0cmb.cadreEIRLGroup.cadreEIRL.estEIRL) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjAutorisationMineur);
}

// PJ JQPA

var pj=$p0cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification1.jqpaSituation ;
if(Value('id').of(pj).eq('jqpaSituationDiplome')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDiplomes1);
}

var pj=$p0cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification1.jqpaSituation ;
if(Value('id').of(pj).eq('jqpaSituationExperience')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles1);
}

var pj=$p0cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification2.jqpaSituation ;
																																
if(Value('id').of(pj).eq('jqpaSituationDiplome')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDiplomes2);
}

var pj=$p0cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification2.jqpaSituation ;
if(Value('id').of(pj).eq('jqpaSituationExperience')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles2);
}

var pj=$p0cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification3.jqpaSituation ;
if(Value('id').of(pj).eq('jqpaSituationDiplome')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDiplomes3);
}

var pj=$p0cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification3.jqpaSituation ;
if(Value('id').of(pj).eq('jqpaSituationExperience')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles3);
}


/*
 * Enregistrement du fichier (en mémoire)
 */

var finalDoc = cerfaDoc1.save('PE_PO_CMB_Creation.pdf');

metas.push({'name':'document', 'value': '/3-review/generated/generated.record-1-PE_PO_CMB_Creation.pdf'});

//Remove old metas before insert
var metasToDelete = [];
var recordMetas = nash.record.meta() != null ? nash.record.meta().metas : null;
var recordMetasSize = recordMetas != null ? recordMetas.size() : 0;

for (var i = 0; i < recordMetasSize; i++) {
   if (recordMetas.get(i).name == 'document' && !recordMetas.get(i).value.contains("proxy")) {
       metasToDelete.push({'name':'document', 'value': recordMetas.get(i).value});
   }
}
nash.record.removeMeta(metasToDelete);

nash.record.meta(metas);


var data = [ spec.createData({
    id : 'record',
    label : 'Déclaration de début d\'activité d\'une activité commerciale et/ou artisanale hors micro-entrepreneur',
    description : 'Voici le formulaire obtenu à partir des données saisies.',
    type : 'FileReadOnly',
    value : [ finalDoc ]
}),  spec.createData({
    id : 'attachments',
    label : 'Pièces jointes',
    description : 'Pièces jointes ajoutées au formulaire.',
    type : 'FileReadOnly',
    value : pjUser
}) ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Déclaration de début d\'activité d\'une activité commerciale et/ou artisanale hors micro-entrepreneur',
    groups : groups
});
