// prepare info to send

var adresse = $p0cmb.cadre1IdentiteGroup.cadre1AdresseDeclarant;
var adressePro = $p0cmb.cadre4AdresseActiviteGroup.cadre4AdresseActivite.cadre4AdresseProfessionnelle;
var cheminLieuActivite = $p0cmb.cadre4AdresseActiviteGroup.cadre4AdresseActivite;

var algo = "trouver destinataire";
var secteur2 = '';
if ((($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA != null
	and not Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup1.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	or ($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA != null
	and not Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup2.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	or ($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA != null
	and not Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup3.jqpaDomaineTransport).eq('jqpaTransportAutre')))
	and not $p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.optionCMACCI) 	{
var secteur1 =  "ARTISANAL";
} else { var secteur1 =  "COMMERCIAL"; }

if (Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
	or Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
	or Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
	or Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup3.jqpaDomaineTransport).eq('jqpaTransportAutre')
	or Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre')
	or Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA).eq('nafaAutre')
	or Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineAlimentation).eq('alimentationAutre')
	or Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineAutresServices).eq('servicesAutre')
	or Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup2.jqpaDomaineTransport).eq('jqpaTransportAutre')
	or Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineProduction).eq('productionAutre')
	or Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA).eq('nafaAutre')
	or Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineAlimentation).eq('alimentationAutre')
	or Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineAutresServices).eq('servicesAutre')
	or Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineProduction).eq('productionAutre')
	or Value('id').of($p0cmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup3.jqpaDomaineTransport).eq('jqpaTransportAutre')) {
	
	secteur2 =  "COMMERCIAL";
}
var typePersonne = "PP";

var formJuridique = $p0cmb.cadreEIRLGroup.cadreEIRL.estEIRL ? "EIRL" : "EI";

var optionCMACCI = "NON";


var codeCommune = '';
_log.info("lieu activite, il devrait pas être nulle --> {}", cheminLieuActivite.activiteLieu);
_log.info("commune domicile {}", adresse.personneLieePersonnePhysiqueAdresseCommune);
_log.info("commune adresse pro {} ",adressePro.entrepriseLieeAdresseCommune);

if (Value('id').of(cheminLieuActivite.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adresse.personneLieePersonnePhysiqueAdresseCommune != null){
	_log.info("adresse activite domicile");
	codeCommune = adresse.personneLieePersonnePhysiqueAdresseCommune.getId();
}else if ((Value('id').of(cheminLieuActivite.activiteLieu).eq('etablissementDomiciliationNon') or Value('id').of(cheminLieuActivite.activiteLieu).eq('etablissementDomiciliationOui')) and adressePro.entrepriseLieeAdresseCommune != null ){
	_log.info("adresse activite entreprise");
	codeCommune = adressePro.entrepriseLieeAdresseCommune.getId();
}


var attachement = "/3-review/generated/generated.record-1-PE_PO_CMB_Creation.pdf";

return spec.create({
	id : 'prepareSend',
	label : "Préparation de la recherche du destinataire",
	groups : [ spec.createGroup({
		id : 'view',
		label : "Informations",
		data : [ spec.createData({
			id : 'algo',
			label : "Algo",
			type : 'String',
			mandatory : true,
			value : algo
		}), spec.createData({
			id : 'secteur1',
			label : "Secteur",
			type : 'String',
			value : secteur1
		}), spec.createData({
			id : 'typePersonne',
			label : "Type personne",
			type : 'String',
			value : typePersonne
		}), spec.createData({
			id : 'formJuridique',
			label : "Forme juridique",
			type : 'String',
			value : formJuridique
		}), spec.createData({
			id : 'optionCMACCI',
			label : "Option CMACCI",
			type : 'String',
			value : optionCMACCI
		}), spec.createData({
			id : 'codeCommune',
			label : "Code commune",
			type : 'String',
			value : codeCommune
		}), spec.createData({
			id : 'attachement',
			label : "Pièce jointe",
			type : 'String',
			value : attachement
		}) ]
	}) ]
});