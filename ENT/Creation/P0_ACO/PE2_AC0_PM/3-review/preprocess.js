function pad(s) { return (s < 10) ? '0' + s : s; }
var formFields = {};

formFields['formulaire_dependance_personnePhysique']                                        = false;
formFields['formulaire_dependance_personneMorale']                                          = true;
formFields['formulaire_dependance_autoEntrepreneur']                                        = false;

// Cadres 1 - Activité non salariée
formFields['formalite_evenement_oui']                                                   = '';
formFields['formalite_evenement_non']                                                   = '';
formFields['voletSocial_activiteExerceeAnterieurementSIREN']                            = '';
// Cadres 2 - Identité
formFields['personneLiee_PP_nomNaissance']                                = '';
formFields['personneLiee_PP_nomUsage']                                    = '';
formFields['personneLiee_PP_prenom1']                                     = '';
formFields['personneLiee_PP_nationalite']                                 = '';
formFields['personneLiee_PP_civiliteMasculin']                            = '';
formFields['personneLiee_PP_civiliteFeminin']                             = '';
formFields['personneLiee_PP_dateNaissance'] 							  = '';
formFields['personneLiee_PP_lieuNaissanceDepartement']                    = '';
formFields['personneLiee_PP_lieuNaissanceCommune']                        = '';
formFields['personneLiee_adresse_nomVoie']                                = '';
formFields['personneLiee_adresse_complementVoie']                         = '';
formFields['personneLiee_adresse_codePostal']                             = '';
formFields['personneLiee_adresse_commune']                                = '';
// Cadre 3 - Demande d'Accre
formFields['entreprise_demandeACCRE']                                     = '';
// Cadre 4 - Statut Conjoint
formFields['personneLiee_conjointCollaborateurSalarie_collaborateur']     = '';
formFields['personneLiee_conjointCollaborateurSalarie_salarie']           = '';
formFields['personneLiee_PP_nomNaissance_conjointPacse']                  = '';
formFields['personneLiee_PP_nomUsage_conjointPacse']                      = '';
formFields['personneLiee_personnePhysique_prenom1_conjointPacse']         = '';
formFields['personneLiee_PP_dateNaissance_conjointPacse'] 				  = '';
formFields['personneLiee_PP_lieuNaissanceDepartement_conjointPacse']      = '';
formFields['personneLiee_PP_lieuNaissanceCommune_conjointPacse']          = '';
// Cadre 5 - Déclaration relative à l'insaisissabilité
formFields['entreprise_insaisissabiliteRenonciationRP']             		= '';
formFields['entreprise_insaisissabilitePublicationRenonciationRP'] 			= '';
formFields['entreprise_insaisissabiliteDeclarationAutresBiens']           = '';
formFields['entreprise_insaisissabilitePublicationDeclarationAutresBiens']  = '';
// Cadre 5 bis - EIRL
formFields['formalite_optionEIRL']                                      = '';
// Cadre 6 - Contrat Appui
formFields['entreprise_contratAppuiPresence']                           = '';
formFields['contratAppuiDateFin']									 	= '';
// Cadre 12 - Volet Social
formFields['voletSocial_numeroSecuriteSociale']                = '';
formFields['voletSocial_numeroSecuriteSocialeCle']             = '';
formFields['voletSocial_titreSejour_numero']                                            = '';
formFields['voletSocial_titreSejour_lieuDelivranceCommune']                             = '';
formFields['voletSocial_titreSejour_dateExpiration'] 									= '';
formFields['voletSocial_Autre_activité_oui']                                            = '';
formFields['voletSocial_Autre_activité_non']                                            = '';
formFields['voletSocial_activiteAutreQueDeclareeStatut_salarie']                        = '';
formFields['voletSocial_activiteAutreQueDeclareeStatut_salarieAgricole']                = '';
formFields['voletSocial_activiteAutreQueDeclareeStatut_retraitePensionne']              = '';
formFields['voletSocial_activiteAutreQueDeclareeStatut_autre'] 				        	= '';
formFields['voletSocial_activiteAutreQueDeclareeStatutAutre']                           = '';
formFields['voletSocial_organismeConventionneLibelle']                                  = '';
formFields['voletSocial_organismeConventionneCode']                                     = '';
formFields['voletSocial_conjointCouvertAssuranceMaladie_oui']                           = '';
formFields['voletSocial_conjointCouvertAssuranceMaladie_non']                           = '';
formFields['voletSocial_conjointCollaborateurNumeroSecuriteSociale']               		 = '';
formFields['voletSocial_conjointCollaborateurNumeroSecuriteSocialeCle']             	= ''; 
formFields['ayantDroit_identite_nomprenom1']                                            = '';
formFields['ayantDroit_identite_nomprenom2']                                            = '';
formFields['ayantDroit_identite_nomprenom3']                                            = '';
formFields['ayantDroit_identite_nomprenom4']            								= '';
formFields['ayantDroit_numeroSecuriteSociale1']                                         = '';
formFields['ayantDroit_numeroSecuriteSociale2']                                         = '';
formFields['ayantDroit_numeroSecuriteSociale3']                                         = '';
formFields['ayantDroit_numeroSecuriteSociale4']  										= '';
formFields['ayantDroit_dateNaissance1']                                                 = '';
formFields['ayantDroit_dateNaissance2']                                                 = '';
formFields['ayantDroit_dateNaissance3']                                                 = '';
formFields['ayantDroit_dateNaissance4']													= '';
formFields['ayantDroit_lieuNaissance1']                                                 = '';
formFields['ayantDroit_lieuNaissance2']                                                 = '';
formFields['ayantDroit_lieuNaissance3']                                                 = '';
formFields['ayantDroit_lieuNaissance4']													= '';
formFields['ayantDroit_lienParente1']                                                   = '';
formFields['ayantDroit_lienParente2']                                                   = '';
formFields['ayantDroit_lienParente3']                                                   = '';
formFields['ayantDroit_lienParente4']													= '';
formFields['ayantDroit_enfantScolarise_oui1']                                           = '';
formFields['ayantDroit_enfantScolarise_non1']                                           = '';
formFields['ayantDroit_enfantScolarise_oui2']                                           = '';
formFields['ayantDroit_enfantScolarise_non2']                                           = '';
formFields['ayantDroit_enfantScolarise_oui3']                                           = '';
formFields['ayantDroit_enfantScolarise_non3']                                           = '';
formFields['ayantDroit_enfantScolarise_oui4']                                           = '';
formFields['ayantDroit_enfantScolarise_non4']                                           = '';
formFields['ayantDroit_nationalite1']                                                   = '';
formFields['ayantDroit_nationalite2']                                                   = '';
formFields['ayantDroit_nationalite3']                                                   = '';
formFields['ayantDroit_nationalite4']                                                   = '';
formFields['voletSocial_optionMicroSocialVersement_mensuel']                  = '';
formFields['voletSocial_optionMicroSocialVersement_trimestriel']              = '';
formFields['regimeFiscal_regimeImpositionBeneficesVersementLiberatoire_oui']  = '';
// Cadre 13 - Régime fiscal
formFields['regimeFiscal_regimeImpositionBeneficesOptionsParticulieres_optionCreanceDette'] = '';
formFields['regimeFiscal_regimeImpositionBenefices_dcBNC']                                  = '';
formFields['regimeFiscal_regimeImpositionBenefices_rsBNC']                                  = '';
formFields['regimeFiscal_regimeImpositionTVA_rfTVA']                                        = '';
formFields['regimeFiscal_regimeImpositionTVA_rsTVA']                                        = '';
formFields['regimeFiscal_regimeImpositionTVA_rnTVA']                                        = '';
formFields['regimeFiscal_regimeImpositionTVAOptionsParticulieres1']                         = '';





// Cadre 7
formFields['agent_commercial_denomination']                                                 = $ac0pm.cadre7PMGroup.cadre7PM.denominationPM;
formFields['agent_commercial_sigle']                                                        = $ac0pm.cadre7PMGroup.cadre7PM.siglePM;
formFields['agent_commercial_siren']                                                        = $ac0pm.cadre7PMGroup.cadre7PM.numeroUniqueIdentificationPM.replace(/ /g, "");
formFields['agent_commercial_adresse_numVoie']                                              = $ac0pm.cadre7PMGroup.cadre7PM.adresseSiege.cadre7PMNumVoie +' '+ ($ac0pm.cadre7PMGroup.cadre7PM.adresseSiege.cadre7PMComplement != null ? $ac0pm.cadre7PMGroup.cadre7PM.adresseSiege.cadre7PMComplement :'') ;
formFields['agent_commercial_adresse_codePostal']                                           = $ac0pm.cadre7PMGroup.cadre7PM.adresseSiege.cadre7PMCP != null ? $ac0pm.cadre7PMGroup.cadre7PM.adresseSiege.cadre7PMCP :'' ;
formFields['agent_commercial_communePays']                                                  = $ac0pm.cadre7PMGroup.cadre7PM.adresseSiege.cadre7PMCommune2 + '/' +$ac0pm.cadre7PMGroup.cadre7PM.adresseSiege.cadre7PMPays ;


// Cadre 8
formFields['agent_commercial_formeJuridique']                                               = $ac0pm.cadre7PMGroup.cadre7PM.formeJuridiquePM;
formFields['agent_commercial_nomNaissance']                                                 = $ac0pm.cadre7PMGroup.cadre8PM.cadre8PMDirigeant.cadre8PMNomNaissance;
formFields['agent_commercial_prenoms']                                                      = $ac0pm.cadre7PMGroup.cadre8PM.cadre8PMDirigeant.cadre8PMPrenom1;

var nirPMAC0 = $ac0pm.cadre7PMGroup.cadre8PM.cadre8PMDirigeant.cadre8PMSecuriteSociale;
	if(nirPMAC0 != null) {
		nirPMAC0 = nirPMAC0.replace(/ /g, "");
		formFields['agent_commercial_securiteSocialeNumero']                = nirPMAC0.substring(0, 13);
		formFields['agent_commercial_securiteSocialeClef']             = nirPMAC0.substring(13, 15);
	} else {
		formFields['agent_commercial_securiteSocialeNumero']                = '';
		formFields['agent_commercial_securiteSocialeClef']             = '';
	}



// Cadres 9 - Activité

var activite = $ac0pm.cadre4AdresseActiviteGroup.cadre4EtablissementActivite

if(activite.etablissementDateDebutActivite != null) {
	var dateTmp = new Date(parseInt(activite.etablissementDateDebutActivite.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_dateDebutActivite'] = date;
}	
formFields['etablissement_secteursActivites']                                         ='';
formFields['etablissement_secteursActivites_suite']                                         = activite.etablissementSecteursActivites;
formFields['etablissement_activitePlusImportante']                                          = activite.etablissementActivitePlusImportante;

//Cadres 10 - Effectif salarié

formFields['etablissement_effectifSalariePresence_non']                                     = activite.etablissementEffectifSalarieEmbauchePremierSalarieOui ? false : true;
formFields['etablissement_effectifSalariePresence_oui']                                     = activite.etablissementEffectifSalarieEmbauchePremierSalarieOui ? true : false;
formFields['etablissement_effectifSalarieNombre']                                           = activite.etablissementEffectifSalarieNombre;
formFields['etablissement_effectifSalarieEmbauchePremierSalarie_oui']                       = activite.etablissementEffectifSalarieEmbauchePremierSalarieOui ? true : false;
formFields['etablissement_effectifSalarieEmbauchePremierSalarie_non']                       = activite.etablissementEffectifSalarieEmbauchePremierSalarieOui ? false : true;

//Cadre 11 - Adresse de l'établissement

var adressePro = $ac0pm.cadre4AdresseActiviteGroup.cadre4AdresseActivite;

formFields['etablissement_adresseProfessionnelle']                                      = (adressePro.cadre4AdresseProfessionnelle.numRueAdresseActivite != null ? adressePro.cadre4AdresseProfessionnelle.numRueAdresseActivite : '')
formFields['etablissement_adresseProfessionnelle_suite']								= (adressePro.cadre4AdresseProfessionnelle.rueComplementAdresseActivite != null ? adressePro.cadre4AdresseProfessionnelle.rueComplementAdresseActivite : '');
formFields['etablissement_adresseProfessionnelle_codePostal']                           = adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseCodePostal;
formFields['etablissement_adresseProfessionnelle_commune']                              = adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseCommune;



//Cadre 14 - Observations

var correspondance = $ac0pm.cadre10RensCompGroup.cadre10RensComp ;

formFields['formalite_observations']                                                    = correspondance.formaliteObservations;

// Cadre 15 - Adresse de correspondance

formFields['formalite_DeclarerCadre']                                                   = (Value('id').of(correspondance.adresseCorrespond).eq('domi') or Value('id').of(correspondance.adresseCorrespond).eq('prof')) ? true : false;
formFields['formalite_DeclarerCadreNumero']                                             = Value('id').of(correspondance.adresseCorrespond).eq('domi') ? "7" : (Value('id').of(correspondance.adresseCorrespond).eq('prof') ? "11" : ' ');
formFields['formalite_correspondanceAdresse']                                           = Value('id').of(correspondance.adresseCorrespond).eq('autre') ? true : false;
formFields['formalite_correspondanceAdresse_nomVoie']                                   = correspondance.adresseCorrespondance.numeroRueAdresseCorrespondance;
formFields['formalite_correspondanceAdresse_complementVoie']                            = correspondance.adresseCorrespondance.rueComplementAdresseCorrespondance;
formFields['formalite_correspondanceAdresse_codePostal']                                = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal;
formFields['formalite_correspondanceAdresse_commune']									= correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune;
formFields['telephone1']                                                                = correspondance.infosSup.formaliteTelephone1;
formFields['telephone2']                                                                = correspondance.infosSup.formaliteTelephone2;
formFields['fax_mail']                                                    = correspondance.infosSup.formaliteFaxCourriel != null ? correspondance.infosSup.formaliteFaxCourriel : correspondance.infosSup.telecopie;

// Cadre 16 - Non diffusion

var signataire = $ac0pm.cadre11SignatureGroup.cadre11Signature ;

formFields['formalite_nonDiffusionInformation']                                         = signataire.formaliteNonDiffusionInformation ? true : false;

// Cadres 20 - Signature

formFields['formalite_signataireQualite_declarant']                                     =  false;
formFields['formalite_signataireQualite_representant']									= Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentant') ? true : false;
formFields['formalite_signataireQualite_mandataire']                                    = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
formFields['formalite_signataireNom']										        	= Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') or Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentant') ? signataire.adresseMandataire.nomPrenomDenominationMandataire : '';	
formFields['formalite_signataireAdresse']                                               = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') or Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentant') ? (signataire.adresseMandataire.numeroNomRueMandataire + ' ' + (signataire.adresseMandataire.dataCodePostalMandataire != null ? signataire.adresseMandataire.dataCodePostalMandataire :'') + ' ' + signataire.adresseMandataire.villeAdresseMandataire) : '';
formFields['formalite_signatureLieu']                                                   = signataire.formaliteSignatureLieu;
formFields['formalite_signatureDate']                                                   = signataire.formaliteSignatureDate;
formFields['formalite_peirl_oui']                                                       = false;
formFields['formalite_peirl_non']                                                       = true;
formFields['formalite_ACCRE_oui']                                                       = false;
formFields['formalite_ACCRE_non']                                                       = true;
// A conditionner selon l'activité
formFields['formalite_nb_intercalaires']                                                =  "0";
formFields['signature']                                                                 = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce";







/*
 * Création du dossier avec volet social : ajout du cerfa avec volet social
 */
var cerfaDoc1 = nash.doc //
	.load('models/cerfa_13847-05_P0_ACO_avec_volet_social.pdf') //
	.apply(formFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));

/*
 * Création du dossier sans volet social : ajout du cerfa sans volet social
 */
 
 var cerfaDoc2 = nash.doc //
	.load('models/cerfa_13847-05_P0_ACO_sans_volet_social.pdf') //
	.apply (formFields);
	cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));




function appendPj(fld) {
	fld.forEach(function (elm) {
        cerfaDoc1.append(elm);
    });
}

/*
 * Ajout des PJs
 */

// PJ Déclarant

var pj=$ac0pm.cadre11SignatureGroup.cadre11Signature.soussigne;

appendPj($attachmentPreprocess.attachmentPreprocess.pjContratAgentCo);
appendPj($attachmentPreprocess.attachmentPreprocess.pjkbis);


if(Value('id').of(pj).contains('FormaliteSignataireQualiteRepresentant')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjIDRepresentantSignataire);
}

// PJ Mandataire

if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjPouvoir);
}

if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire);
}






	

/*
 * Enregistrement du fichier (en mémoire)
 */

var finalDoc = cerfaDoc1.save('PE_AC0_Creation.pdf');

var data = [ spec.createData({
    id : 'record',
    label : 'Déclaration de début d\'activite d\'un agent commercial personne morale',
    description : 'Voici le formulaire obtenu à partir des données saisies.',
    type : 'FileReadOnly',
    value : [ finalDoc ]
}) ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Déclaration de début d\'activite d\'un agent commercial personne morale',
    groups : groups
});