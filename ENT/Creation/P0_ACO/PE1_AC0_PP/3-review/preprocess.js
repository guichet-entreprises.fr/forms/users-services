function pad(s) { return (s < 10) ? '0' + s : s; }
var formFields = {};



var identite = $ac0PE1.cadre1IdentiteGroup.cadre1Identite;

formFields['formulaire_dependance_personnePhysique']                                        = true;
formFields['formulaire_dependance_personneMorale']                                          = false;
formFields['formulaire_dependance_autoEntrepreneur']                                        = identite.microEntrepreneurOuiNon ?  true : false;


// Cadres 1 - Activité non salariée

formFields['formalite_evenement_oui']                                                   = identite.formaliteEvenementOui ? true : false;
formFields['formalite_evenement_non']                                                   = identite.formaliteEvenementOui ? false : true;
formFields['voletSocial_activiteExerceeAnterieurementSIREN']                            = identite.voletSocialActiviteExerceeAnterieurementSIREN != null ? identite.voletSocialActiviteExerceeAnterieurementSIREN.split(' ').join('') : '';

// Cadres 2A - EI

formFields['nonEIRL']                                                                   = $ac0PE1.cadreEIRLGroup.cadreEIRL.estEIRL ? false : true;

// Cadres 2B - EIRL

formFields['estEIRL']                                                                   = $ac0PE1.cadreEIRLGroup.cadreEIRL.estEIRL ? true : false;

// Cadres 3 - Identité

formFields['personneLiee_personnePhysique_nomNaissance']                  = identite.personneLieePersonnePhysiqueNomNaissance;
formFields['personneLiee_personnePhysique_nomUsage']                      = identite.personneLieePersonnePhysiqueNomUsage;
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFields['personneLiee_personnePhysique_prenom1']                       = prenoms.toString();
formFields['personneLiee_PP_nationalite']                                 = identite.personneLieePersonnePhysiqueNationalite;
formFields['personneLiee_PP_civiliteMasculin']                            = Value('id').of(identite.civilite).eq('PersonneLieePersonnePhysiqueCiviliteMasculin') ? true : false;
formFields['personneLiee_PP_civiliteFeminin']                             = Value('id').of(identite.civilite).eq('PersonneLieePersonnePhysiqueCiviliteFeminin') ? true : false;
if(identite.personneLieePersonnePhysiqueDateNaissance != null) {
	var dateTmp = new Date(parseInt(identite.personneLieePersonnePhysiqueDateNaissance.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['personneLiee_PP_dateNaissance'] = date;
}
formFields['personneLiee_PP_lieuNaissanceDepartement']                    = identite.personneLieePersonnePhysiqueLieuNaissanceDepartement != null ? identite.personneLieePersonnePhysiqueLieuNaissanceDepartement.getId() : '';
formFields['personneLiee_PP_lieuNaissanceCommune']                        = identite.personneLieePersonnePhysiqueLieuNaissanceCommune != null ? identite.personneLieePersonnePhysiqueLieuNaissanceCommune : identite.personneLieePersonnePhysiqueLieuNaissanceVille;
formFields['personneLiee_personnePhysique_lieuNaissancePays']             = identite.personneLieePersonnePhysiqueLieuNaissancePays;

var adresse = $ac0PE1.cadre1IdentiteGroup.cadre1AdresseDeclarant;

formFields['personneLiee_adresse_nomVoie']                                                = (adresse.personneLieePersonnePhysiqueAdresserueNumeroVoie != null ? adresse.personneLieePersonnePhysiqueAdresserueNumeroVoie : '') 
																						+ ' ' + (adresse.personneLieePersonnePhysiqueAdresseIndiceVoie != null ? adresse.personneLieePersonnePhysiqueAdresseIndiceVoie : '') 
																						+ ' ' + (adresse.personneLieePersonnePhysiqueAdresseTypeVoie != null ? adresse.personneLieePersonnePhysiqueAdresseTypeVoie : '') 
																						+ ' ' + (adresse.personneLieePersonnePhysiqueAdresseNomVoie !=null ? adresse.personneLieePersonnePhysiqueAdresseNomVoie : '');           
formFields['personneLiee_adresse_complementVoie']                                                = (adresse.personneLieePersonnePhysiquerueAdresseComplementAdresse != null ? adresse.personneLieePersonnePhysiquerueAdresseComplementAdresse : '') 
																						+ ' ' + (adresse.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie != null ? adresse.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie :'');
formFields['personneLiee_adresse_codePostal']                                           = adresse.personneLieePersonnePhysiqueAdresseCodePostal;
formFields['personneLiee_adresse_commune']                                              = Value('id').of(adresse.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX') ? adresse.personneLieePersonnePhysiqueAdresseCommune : adresse.personneLieeAdresseVille;
formFields['personneLiee_adresse_pays']                                                 = adresse.personneLieePersonnePhysiqueAdressePays;
formFields['personneLiee_adresse_communeAncienne']                                      = adresse.personneLieePersonnePhysiqueAdresseCommuneAncienne != null ? adresse.personneLieePersonnePhysiqueAdresseCommuneAncienne : '';

var accre = $ac0PE1.cadre3InformationsComplementairesGroup.cadre3InformationsComplementaires;

// Cadre 4 - Contrat Appui

formFields['entreprise_contratAppuiPresence']                                           = accre.entrepriseContratAppuiPresence ? true : false;
if(accre.contratAppuiDateFin != null) {
	var dateTmp = new Date(parseInt(accre.contratAppuiDateFin.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['contratAppuiDateFin'] = date;
}

// Information sur l'entreprise accompagnante sur l'intercalaire P0'

if (accre.entrepriseContratAppuiPresence) {
formFields['complete_cadre']                                                            = "4";
formFields['entrepriseLieeContratAppui_denomination']                                   = accre.cadre3ContratAppuiInfos.contratAppuiDenomination 
																						+ ' / ' + accre.cadre3ContratAppuiInfos.contratAppuiSiren; 
formFields['entrepriseLieeContratAppui_adresseVoie']   									= (accre.cadre3ContratAppuiInfos.contratAppuiAdresse.numeroAdresseContratAppui != null ? accre.cadre3ContratAppuiInfos.contratAppuiAdresse.numeroAdresseContratAppui : '')
																						+ '  ' + (accre.cadre3ContratAppuiInfos.contratAppuiAdresse.indiceAdresseContratAppui != null ? accre.cadre3ContratAppuiInfos.contratAppuiAdresse.indiceAdresseContratAppui : '')
																						+ '  ' + (accre.cadre3ContratAppuiInfos.contratAppuiAdresse.typeAdresseContratAppui	!= null ? accre.cadre3ContratAppuiInfos.contratAppuiAdresse.typeAdresseContratAppui : '')
																						+ '  ' + accre.cadre3ContratAppuiInfos.contratAppuiAdresse.voieAdresseContratAppui
																						+ '  ' + (accre.cadre3ContratAppuiInfos.contratAppuiAdresse.complementAdresseContratAppui != null ? accre.cadre3ContratAppuiInfos.contratAppuiAdresse.complementAdresseContratAppui : '')	
																						+ '  ' + (accre.cadre3ContratAppuiInfos.contratAppuiAdresse.distributionSpecialeAdresseContratAppui != null ? accre.cadre3ContratAppuiInfos.contratAppuiAdresse.distributionSpecialeAdresseContratAppui : ''); 
formFields['entrepriseLieeContratAppui_adresseCommune']									= accre.cadre3ContratAppuiInfos.contratAppuiAdresse.codePostalAdresseContratAppui 
																						+ '  ' + accre.cadre3ContratAppuiInfos.contratAppuiAdresse.communeAdresseContratAppui;
formFields['complete_cerfa']                                                            = "AC0";
}

// Cadre 5 - Déclaration relative à l'insaisissabilité


var insaisissabilite = $ac0PE1.cadre3InformationsComplementairesGroup.cadre3InformationsComplementaires.cadre3DeclarationInsaisissabilite;

formFields['entreprise_insaisissabiliteRenonciationRP']            				= insaisissabilite.entrepriseInsaisissabiliteRenonciationDeclarationAutresBiens ? true : false;
formFields['entreprise_insaisissabilitePublicationRenonciationRP']  			= insaisissabilite.entrepriseInsaisissabilitePublicationRenonciationDeclarationAutresBiens;
formFields['entreprise_insaisissabiliteDeclarationAutresBiens']                 = insaisissabilite.entrepriseInsaisissabiliteDeclarationAutresBiens ? true : false;
if (insaisissabilite.entrepriseInsaisissabiliteDeclarationAutresBiens) {
var declarationAutresBiens=[];
for ( i = 0; i < insaisissabilite.entrepriseInsaisissabilitePublicationDeclarationAutresBiens.size() ; i++ ){declarationAutresBiens.push(insaisissabilite.entrepriseInsaisissabilitePublicationDeclarationAutresBiens[i]);}                            
formFields['entreprise_insaisissabilitePublicationDeclarationAutresBiens']                      = declarationAutresBiens.toString();
}

// Cadre 6 - Statut Conjoint

var conjoint = $ac0PE1.cadre2conjointGroup.cadre2Conjoint;

formFields['personneLiee_conjointActiviteEntreprise_oui']                   			= conjoint.conjointRole ? true : false;
formFields['personneLiee_conjointActiviteEntreprise_non']                  				= conjoint.conjointRole ? false : true;
if (conjoint.conjointRole) {
formFields['personneLiee_conjointCollaborateurSalarie_collaborateur']                   = Value('id').of(conjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieCollaborateur') ? true : false;
formFields['personneLiee_conjointCollaborateurSalarie_salarie']                         = Value('id').of(conjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieSalarie') ? true : false;

var conjointInfos = $ac0PE1.cadre2conjointGroup.cadre2Conjoint.cadre2InfosConjoint;

formFields['personneLiee_PP_nomNaissance_conjointPacse']                  = conjointInfos.personneLieePersonnePhysiqueNomNaissanceConjointPacse;
formFields['personneLiee_PP_nomUsage_conjointPacse']                      = conjointInfos.personneLieePersonnePhysiqueNomUsageConjointPacse;
var prenomsConjoint=[];
for ( i = 0; i < conjointInfos.personneLieePersonnePhysiquePrenom1ConjointPacse.size() ; i++ ){prenomsConjoint.push(conjointInfos.personneLieePersonnePhysiquePrenom1ConjointPacse[i]);}                            
formFields['personneLiee_personnePhysique_prenom1_conjointPacse']                                                   = prenomsConjoint.toString();
if(conjointInfos.personneLieePersonnePhysiqueDateNaissanceConjointPacse != null) {
	var dateTmp = new Date(parseInt(conjointInfos.personneLieePersonnePhysiqueDateNaissanceConjointPacse.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['personneLiee_PP_dateNaissance_conjointPacse'] = date;
}
formFields['personneLiee_PP_lieuNaissanceDepartement_conjointPacse']      = conjointInfos.personneLieePersonnePhysiqueLieuNaissanceDepartementConjointPacse != null ? conjointInfos.personneLieePersonnePhysiqueLieuNaissanceDepartementConjointPacse.getId() : '';
formFields['personneLiee_PP_lieuNaissanceCommune_conjointPacse']          = conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueLieuNaissanceCommuneConjointPacse != null ? conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueLieuNaissanceCommuneConjointPacse : conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueLieuNaissanceVilleConjointPacse;
formFields['personneLiee_personnePhysique_lieuNaissancePays_conjointPacse']         = conjoint.cadre2InfosConjoint.personneLieePersonnePhysiquePaysNaissanceConjointPacse;
if (conjoint.cadre2InfosConjoint.adresseConjointDifferente ) {
formFields['adresseConjointVoie']                                       	            = (conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointNumeroVoie != null ? conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointNumeroVoie : '')
																						+ '  ' + (conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointIndiceVoie != null ? conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointIndiceVoie : '')
																						+ '  ' + (conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointTypeVoie  != null ? conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointTypeVoie : '')
																						+ '  ' + (conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointNomVoie  != null ? conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointNomVoie : '')
																						+ '  ' + (conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointComplementVoie != null ? conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointComplementVoie :'')
																						+ '  ' + (conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null ? conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie :'');
formFields['adresseConjointCodePostal']                                                 = conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointCodePostal;
formFields['adresseConjointCommune']                                                    = conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointCommune;
}
}

// Cadres 9 - Activité

var activite = $ac0PE1.cadre4AdresseActiviteGroup.cadreEtablissementActivite;

if(activite.etablissementDateDebutActivite != null) {
	var dateTmp = new Date(parseInt(activite.etablissementDateDebutActivite.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_dateDebutActivite'] = date;
}	
formFields['etablissement_secteursActivites_suite']                                         = activite.etablissementSecteursActivites;
formFields['etablissement_activitePlusImportante']                                          = activite.etablissementActivitePlusImportante;

//Cadres 10 - Effectif salarié

formFields['etablissement_effectifSalariePresence_non']                                 = activite.etablissementEffectifSalariePresenceOui ? false : true;
formFields['etablissement_effectifSalariePresence_oui']                                 = activite.etablissementEffectifSalariePresenceOui ? true : false;
if (activite.etablissementEffectifSalariePresenceOui) {
formFields['etablissement_effectifSalarieNombre']                                       = activite.etablissementEffectifSalarieNombre;
formFields['etablissement_effectifSalarieEmbauchePremierSalarie_oui']                   = activite.etablissementEffectifSalarieEmbauchePremierSalarieOui ? true : false;
formFields['etablissement_effectifSalarieEmbauchePremierSalarie_non']                   = activite.etablissementEffectifSalarieEmbauchePremierSalarieOui ? false : true;
}

//Cadre 11 - Adresse de l'établissement

var adressePro = $ac0PE1.cadre4AdresseActiviteGroup.cadreAdresseActivite;

formFields['etablissement_adresseProfessionnelle']                                      = (adressePro.cadreAdresseProfessionnelle.etablissementAdresseNumeroVoie != null ? adressePro.cadreAdresseProfessionnelle.etablissementAdresseNumeroVoie : '') 
																						+ ' ' + (adressePro.cadreAdresseProfessionnelle.etablissementAdresseIndiceVoie != null ? adressePro.cadreAdresseProfessionnelle.etablissementAdresseIndiceVoie : '') 
																						+ ' ' + (adressePro.cadreAdresseProfessionnelle.etablissementAdresseTypeVoie != null ? adressePro.cadreAdresseProfessionnelle.etablissementAdresseTypeVoie : '') 
																						+ ' ' + (adressePro.cadreAdresseProfessionnelle.etablissementAdresseNomVoie != null ? adressePro.cadreAdresseProfessionnelle.etablissementAdresseNomVoie : '');
formFields['etablissement_adresseProfessionnelle_suite']								= (adressePro.cadreAdresseProfessionnelle.etablissementAdresseComplementVoie != null ? adressePro.cadreAdresseProfessionnelle.etablissementAdresseComplementVoie : '') 
																						+ ' ' + (adressePro.cadreAdresseProfessionnelle.etablissementAdresseDistriutionSpecialeVoie != null ? adressePro.cadreAdresseProfessionnelle.etablissementAdresseDistriutionSpecialeVoie : '');
formFields['etablissement_adresseProfessionnelle_codePostal']                           = adressePro.cadreAdresseProfessionnelle.etablissementAdresseCodePostal;
formFields['etablissement_adresseProfessionnelle_commune']                              = adressePro.cadreAdresseProfessionnelle.etablissementAdresseCommune != null ? adressePro.cadreAdresseProfessionnelle.etablissementAdresseCommune : '';
formFields['etablissement_adresseProfessionnelle_communeAncienne']                      = adressePro.cadreAdresseProfessionnelle.etablissementAdresseCommuneAncienne != null ? adressePro.cadreAdresseProfessionnelle.etablissementAdresseCommuneAncienne : '';


// Cadre 12 - Volet Social

var social = $ac0PE1.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale;

var nirDeclarant = social.voletSocialNumeroSecuriteSociale;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFields['voletSocial_numeroSecuriteSociale']                = nirDeclarant.substring(0, 13);
    formFields['voletSocial_numeroSecuriteSocialeCle']             = nirDeclarant.substring(13, 15);
}
formFields['voletSocial_titreSejour_numero']                                            = social.voletSocialTitreSejourNumero;
formFields['voletSocial_titreSejour_lieuDelivranceCommune']                             = social.voletSocialTitreSejourLieuDelivranceCommune;
if(social.voletSocialTitreSejourDateExpiration != null) {
	var dateTmp = new Date(parseInt(social.voletSocialTitreSejourDateExpiration.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['voletSocial_titreSejour_dateExpiration'] = date;
}
formFields['voletSocial_Autre_activité_oui']                            = social.voletSocialActiviteAutreQueDeclareeStatutOui ? true : false;
formFields['voletSocial_Autre_activité_non']                                            = social.voletSocialActiviteAutreQueDeclareeStatutOui ? false : true;
formFields['voletSocial_activiteAutreQueDeclareeStatut_salarie']                        = Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutSalarie') ? true : false;
formFields['voletSocial_activiteAutreQueDeclareeStatut_salarieAgricole']                = Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutSalarieAgricole') ? true : false;
formFields['voletSocial_activiteAutreQueDeclareeStatut_retraitePensionne']              = Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutRetraitePensionne') ? true : false;
formFields['voletSocial_activiteAutreQueDeclareeStatut_autre'] 				        	= Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutCocheAutre') ? true : false;
formFields['voletSocial_activiteAutreQueDeclareeStatutAutre']                           = social.voletSocialActiviteAutreQueDeclareeStatutAutre;
formFields['voletSocial_regimePraticien_oui']                                           = social.voletSocialRegimePraticienOui != null ? (Value('id').of(social.voletSocialRegimePraticienOui).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false) : false;
formFields['voletSocial_regimePraticien_non']                                           = social.voletSocialRegimePraticienOui != null ? (Value('id').of(social.voletSocialRegimePraticienOui).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false) : false;
if (conjoint.conjointRole) {
formFields['voletSocial_conjointCouvertAssuranceMaladie_oui']                           = conjointInfos.voletSocialConjointCouvertAssuranceMaladieOui ? true : false;
formFields['voletSocial_conjointCouvertAssuranceMaladie_non']                           = conjointInfos.voletSocialConjointCouvertAssuranceMaladieOui ? false : true;
var nirConjoint = conjointInfos.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(nirConjoint != null) {
    nirConjoint = nirConjoint.replace(/ /g, "");
    formFields['voletSocial_conjointCollaborateurNumeroSecuriteSociale']    = nirConjoint.substring(0, 13);
    formFields['voletSocial_conjointCollaborateurNumeroSecuriteSocialeCle'] = nirConjoint.substring(13, 15);
}
}

// Cadre 12 bis : régime fiscal et social du micro-entrepreneur

var optionsFiscalesME = $ac0PE1.cadre9optFiscHorsEIRLGroup.cadre9optFiscHorsEIRLME;
var declarationAffectation = $ac0PE1.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine;

if(identite.microEntrepreneurOuiNon) {
formFields['voletSocial_optionMicroSocialVersement_trimestriel']                 = Value('id').of(social.optionMicroSocialSimplifie).eq('VoletSocialOptionMicroSocialVersementTrimestriel') ? true : false;
formFields['voletSocial_optionMicroSocialVersement_mensuel']                     = Value('id').of(social.optionMicroSocialSimplifie).eq('VoletSocialOptionMicroSocialVersementMensuel') ? true : false;
if (declarationAffectation.declarationPatrimoine.objetPartiel or not $ac0PE1.cadreEIRLGroup.cadreEIRL.estEIRL) {
formFields['regimeFiscal_regimeImpositionBeneficesVersementLiberatoire_oui']     = optionsFiscalesME.regimeFiscalRegimeImpositionBeneficesVersementLiberatoireOui ? true : false;
}
}

// Cadre 13 - Régime fiscal

var optionsFiscales = $ac0PE1.cadre9optFiscHorsEIRLGroup.cadre9optFiscHorsEIRLnonME;

if ((declarationAffectation.declarationPatrimoine.objetPartiel or not $ac0PE1.cadreEIRLGroup.cadreEIRL.estEIRL) and not identite.microEntrepreneurOuiNon) {
formFields['regimeFiscal_regimeImpositionBeneficesOptionsParticulieres_optionCreanceDette'] = optionsFiscales.impositionBenefices.regimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionCreanceDette ? true : false;
formFields['regimeFiscal_regimeImpositionBenefices_dcBNC']                                  = Value('id').of(optionsFiscales.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesDcBNC') ? true : false;
formFields['regimeFiscal_regimeImpositionBenefices_rsBNC']                                  = Value('id').of(optionsFiscales.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesRsBNC') ? true : false;
formFields['regimeFiscal_regimeImpositionTVA_rfTVA']                                        = Value('id').of(optionsFiscales.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesRsBNC') ? true : (Value('id').of(optionsFiscales.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARfTVA') ? true : false);
formFields['regimeFiscal_regimeImpositionTVA_rsTVA']                                        = Value('id').of(optionsFiscales.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARsTVA') ? true : false;
formFields['regimeFiscal_regimeImpositionTVA_rnTVA']                                        = Value('id').of(optionsFiscales.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARnTVA') ? true : false;
formFields['regimeFiscal_regimeImpositionTVAOptionsParticulieres1']                         = optionsFiscales.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres1 ? true : false;
}

//Cadre 14 - Observations

var correspondance = $ac0PE1.cadre10RensCompGroup.cadre10RensComp ;

formFields['formalite_observations']                                                    = correspondance.formaliteObservations;


// Cadre 15 - Adresse de correspondance

formFields['formalite_DeclarerCadre']                                                   = (Value('id').of(correspondance.adresseCorrespond).eq('domi') or Value('id').of(correspondance.adresseCorrespond).eq('prof')) ? true : false;
formFields['formalite_DeclarerCadreNumero']                                             = Value('id').of(correspondance.adresseCorrespond).eq('domi') ? "3" : (Value('id').of(correspondance.adresseCorrespond).eq('prof') ? "11" : '');
formFields['formalite_correspondanceAdresse']                                           = Value('id').of(correspondance.adresseCorrespond).eq('autre') ? true : false;
formFields['formalite_correspondanceAdresse_nom']                                       = correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance != null ? correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance : '';
formFields['formalite_correspondanceAdresse_voie']                                      = (correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance : '')
																						+ ' ' + (correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance : '')
																						+ ' ' + (correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance : '')
																						+ ' ' + (correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance : '')
																						+ ' ' + (correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance != null ? correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance : '')
																						+ ' ' + (correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance != null ? correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance : '');
formFields['formalite_correspondanceAdresse_codePostal']                                = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal;
formFields['formalite_correspondanceAdresse_commune']									= correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune;
formFields['telephone1']                                                			    = correspondance.infosSup.formaliteTelephone1 != null ? correspondance.infosSup.formaliteTelephone1 : '';
formFields['telephone2']                                          			            = correspondance.infosSup.formaliteTelephone2;
formFields['fax_mail']                                             					    = correspondance.infosSup.formaliteFaxCourriel != null ? correspondance.infosSup.formaliteFaxCourriel : correspondance.infosSup.telecopie;


// Cadre 16 - Non diffusion

var signataire = $ac0PE1.cadre11SignatureGroup.cadre11Signature;

formFields['formalite_nonDiffusionInformation']                                         = signataire.formaliteNonDiffusionInformation ? true : false;


// Cadres 20 - Signature

formFields['formalite_signataireQualite_declarant']                                     = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFields['formalite_signataireQualite_mandataire']                                    = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFields['formalite_signataireNom']								        			= signataire.adresseMandataire.nomPrenomDenominationMandataire;	
formFields['formalite_signataireAdresse_voie']                                          = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																						+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																						+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																						+ ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																						+ ' ' + (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '')
																						+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '');
formFields['formalite_signataire_adresseCP']											= signataire.adresseMandataire.dataCodePostalMandataire;
formFields['formalite_signataire_adresseCommune']										= signataire.adresseMandataire.villeAdresseMandataire;
}
formFields['formalite_signatureLieu']                                                   = signataire.formaliteSignatureLieu;
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['formalite_signatureDate'] = date;
}	
formFields['formalite_peirl_oui']                                                       = $ac0PE1.cadreEIRLGroup.cadreEIRL.estEIRL ? true : false;
formFields['formalite_peirl_non']                                                       = $ac0PE1.cadreEIRLGroup.cadreEIRL.estEIRL ? false : true;
// A conditionner selon l'activité
formFields['formalite_nb_intercalaires']                                                = accre.entrepriseContratAppuiPresence ? "1" : "0";
formFields['signature']                                                                 = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce";


//P0' Intercalaire

formFields['complete_p0pl']                                                              = false;
formFields['complete_aco']                                                               = true;
formFields['complete_p0agr']                                                             = false;
formFields['complete_p0cmb']                                                             = false;
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFields['personneLiee_personnePhysique_nomNaissance_prenom']                          = identite.personneLieePersonnePhysiqueNomNaissance + ' ' + prenoms.toString();
formFields['personneLiee_personnePhysique_dateNaissance_P0Prime']                        = identite.personneLieePersonnePhysiqueDateNaissance;
formFields['numero_intercalaire']                                                        = "1";


// PEIRL CMB

//Cadre 1 PEIRL CMB - Déclaration ou modification d'affectation de patrimoine

formFields['formulaire_dependance_P0PL']                                                    = false;
formFields['formulaire_dependance_P0PLME']                                                  = false;
formFields['formulaire_dependance_P2PL']                                                    = false;
formFields['formulaire_dependance_P4PL']                                                    = false;
formFields['formulaire_dependance_AC0']                                                     = true;
formFields['formulaire_dependance_AC2']                                                     = false;
formFields['formulaire_dependance_AC4']     												= false;
formFields['declaration_initiale']                                                          = true;
formFields['declaration_modification']                                                      = false;

// Cadre 2 - Rappel d'identification

formFields['eirl_nomNaissance']                                              = identite.personneLieePersonnePhysiqueNomNaissance;
formFields['eirl_nomUsage']                                                  = identite.personneLieePersonnePhysiqueNomUsage;
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFields['eirl_prenom']                                                   = prenoms.toString();

// Cadre 3 - Déclaration d'affectation de patrimoine

var declarationAffectation = $ac0PE1.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine;

formFields['eirl_statutEIRL_declarationInitialeSansDepot']                                  = Value('id').of(declarationAffectation.eirlDepot).eq('eirlSansDepot') ? true : false;
formFields['eirl_statutEIRL_declarationInitialeAvecDepot']                                  = Value('id').of(declarationAffectation.eirlDepot).eq('eirlAvecDepot') ? true : false;
formFields['eirl_statutEIRL_reprise']                                                       = Value('id').of(declarationAffectation.eirlStatut).eq('EirlStatutEIRLReprise') ? true : false;
formFields['eirl_denomination']                                                             = declarationAffectation.declarationPatrimoine.eirlDenomination;
if (declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable !== null) {
    var dateTmp = new Date(parseInt(declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable.getTimeInMillis()));
    var dateClotureEc = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateClotureEc = dateClotureEc.concat(pad(month.toString()));
    formFields['eirl_dateClotureExerciceComptable']          = dateClotureEc;
}
formFields['eirl_objet']                                                                    = declarationAffectation.declarationPatrimoine.objetPartiel ? declarationAffectation.declarationPatrimoine.eirlObjet : ($ac0PE1.cadre4AdresseActiviteGroup.cadreEtablissementActivite.etablissementSecteursActivites);
if ( Value('id').of(declarationAffectation.eirlStatut).eq('EirlStatutEIRLReprise')) {
formFields['eirl_precedentEIRLDenomination']                                                = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLDenomination;
formFields['eirl_precedentEIRLRegistre_rseirl']                                             = Value('id').of(declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLRegistre).eq('EirlPrecedentEIRLRegistreRseirl') ? true : false;
formFields['eirl_precedentEIRLRegistre_rsac']                                               = Value('id').of(declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLRegistre).eq('EirlPrecedentEIRLRegistreRsac') ? true : false;
formFields['eirl_precedentEIRLLieuImmatriculation']                                         = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLLieuImmatriculation;
formFields['eirl_precedentEIRLSIREN']                                                       = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN != null ? (declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN.split(' ').join('')) : '';
}

// Cadre 7 - Options fiscales

if(identite.microEntrepreneurOuiNon) {
formFields['eirl_regimeFiscal_regimeImpositionBeneficesVersementLiberatoire']                    = declarationAffectation.cadreOptionsFiscalesEIRLME.regimeFiscalRegimeImpositionBeneficesVersementLiberatoire ? true : false;
}

formFields['eirl_regimeFiscal_regimeImpositionBenefices_rsBNC']                                  = identite.microEntrepreneurOuiNon ? true : (Value('id').of(declarationAffectation.cadreOptionsFiscalesEIRLnonME.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesRsBNC') ? true : false);
formFields['eirl_regimeFiscal_regimeImpositionBenefices_dcBNC']                                  = Value('id').of(declarationAffectation.cadreOptionsFiscalesEIRLnonME.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesDcBNC') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBeneficesOptionsParticulieres_optionCreanceDette'] = declarationAffectation.cadreOptionsFiscalesEIRLnonME.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionCreanceDette ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBeneficesOptionsParticulieres_optionIS']           = Value('id').of(declarationAffectation.cadreOptionsFiscalesEIRLnonME.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionIS') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rsIS']                                   = Value('id').of(declarationAffectation.cadreOptionsFiscalesEIRLnonME.impositionBeneficesEIRL.regimeFiscalIS).eq('RegimeFiscalRegimeImpositionBeneficesRsIS') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rnIS']                                   = Value('id').of(declarationAffectation.cadreOptionsFiscalesEIRLnonME.impositionBeneficesEIRL.regimeFiscalIS).eq('RegimeFiscalRegimeImpositionBeneficesRnIS') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVA_rfTVA']                                        = identite.microEntrepreneurOuiNon ? true : ((Value('id').of(declarationAffectation.cadreOptionsFiscalesEIRLnonME.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesRsBNC') or Value('id').of(declarationAffectation.cadreOptionsFiscalesEIRLnonME.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARfTVA')) ? true : false);
formFields['eirl_regimeFiscal_regimeImpositionTVA_rsTVA']                                        = Value('id').of(declarationAffectation.cadreOptionsFiscalesEIRLnonME.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARsTVA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVA_rnTVA']                                        = Value('id').of(declarationAffectation.cadreOptionsFiscalesEIRLnonME.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARnTVA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVAOptionsParticulieres1']                         = declarationAffectation.cadreOptionsFiscalesEIRLnonME.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres1DC ? true : false;


/*
 * Création du dossier avec volet social : ajout du cerfa avec volet social
 */
var cerfaDoc1 = nash.doc //
	.load('models/cerfa_13847-08_P0_ACO_avec_volet_social.pdf') //
	.apply(formFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));

/*
 * Création du dossier sans volet social : ajout du cerfa sans volet social
 */
 
 var cerfaDoc2 = nash.doc //
	.load('models/cerfa_13847-08_P0_ACO_sans_volet_social.pdf') //
	.apply (formFields);
	cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));


/*
 * Ajout de l'intercalaire P0 Prime
 */
 
 if (accre.entrepriseContratAppuiPresence)
{
	var p0PrimeDoc1 = nash.doc //
		.load('models/cerfa_11771-04_P0_Prime.pdf') //
		.apply (formFields);
	cerfaDoc1.append(p0PrimeDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire PEIRL PL avec option fiscale
 */
if ($ac0PE1.cadreEIRLGroup.cadreEIRL.estEIRL)
{
	var peirlMEDoc1 = nash.doc //
		.load('models/cerfa1_14218-03_peirl_PL_AC_avec_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(peirlMEDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire PEIRL PL sans option fiscale
 */
if ($ac0PE1.cadreEIRLGroup.cadreEIRL.estEIRL)
{
	var peirlMEDoc2 = nash.doc //
		.load('models/cerfa1_14218-03_peirl_PL_AC_sans_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(peirlMEDoc2.save('cerfa.pdf'));
}

/*
 * Ajout des PJs
 */

// PJ Déclarant

var pj=$ac0PE1.cadre11SignatureGroup.cadre11Signature.soussigne;
var pjUser = [];
var metas = [];

function pushPjPreview(fld) {
	fld.forEach(function (elm) {
        pjUser.push(elm);
		metas.push({'name':'document', 'value': '/'+elm.getAbsolutePath()});
    });
}
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDeclarant);

var pj=$ac0PE1.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).eq('FormaliteSignataireQualiteDeclarant')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantSignataire);
}

var pj=$ac0PE1.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantNonSignataire);
}


// PJ Mandataire

var pj=$ac0PE1.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPouvoir);
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire);
}

// PJ agent co

pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjContratAgentCo);

// PJ Conjoint

var pj=$ac0PE1.cadre2conjointGroup.cadre2Conjoint.statutConjointCollaborateur ;
if(Value('id').of(pj).contains('PersonneLieeConjointCollaborateurSalarieCollaborateur')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDConjoint);
}

var pj=$ac0PE1.cadre2conjointGroup.cadre2Conjoint ;
if(pj.conjointCommunauteBiens and pj.conjoint) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjInformationConjoint);
}

// PJ Insaisissabilité

var pj=$ac0PE1.cadre3InformationsComplementairesGroup.cadre3InformationsComplementaires ;
if(pj.cadre3DeclarationInsaisissabilite.entrepriseInsaisissabiliteDeclarationAutresBiens) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjInsaisissabilite);
}

// PJ Contrat Appui

var pj=$ac0PE1.cadre3InformationsComplementairesGroup.cadre3InformationsComplementaires ;
if(pj.entrepriseContratAppuiPresence) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjContratAppui);
}

// PJ EIRL

var pj=$ac0PE1.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine;
if($ac0PE1.cadreEIRLGroup.cadreEIRL.estEIRL and not Value('id').of(pj.eirlDepot).eq('eirlSansDepot')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAP);
}

if(Value('id').of(pj.cadreEIRLInformationsComplementaires.contenuDAP).contains('bienImmo') or Value('id').of(pj.cadreEIRLInformationsComplementairesBis.contenuDAPBis).contains('bienImmo')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAPActeNotarie);
}

if(Value('id').of(pj.cadreEIRLInformationsComplementaires.contenuDAP).contains('bienCommunIndivis') or Value('id').of(pj.cadreEIRLInformationsComplementairesBis.contenuDAPBis).contains('bienCommunIndivis')) {
 pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAPAccordTiers);
}

var pj=$ac0PE1.cadreEIRLGroup.cadreEIRL;
if(pj.estEIRL) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjAutorisationMineur);
}
	

/*
 * Enregistrement du fichier (en mémoire)
 */

var finalDoc = cerfaDoc1.save('AC0_Creation.pdf');
// Remove old metas before insert
var metasToDelete = [];
var recordMetas = nash.record.meta() != null ? nash.record.meta().metas : null;
var recordMetasSize = recordMetas != null ? recordMetas.size() : 0;

for (var i = 0; i < recordMetasSize; i++) {
   if (recordMetas.get(i).name == 'document' && !recordMetas.get(i).value.contains("proxy")) {
       metasToDelete.push({'name':'document', 'value': recordMetas.get(i).value});
   }
}
nash.record.removeMeta(metasToDelete);

nash.record.meta(metas);

var data = [ spec.createData({
    id : 'record',
    label : 'Déclaration de début d\'activite d\'un agent commercial',
    description : 'Voici le formulaire obtenu à partir des données saisies.',
    type : 'FileReadOnly',
    value : [ finalDoc ]
}),  spec.createData({
    id : 'attachments',
    label : 'Pièces jointes',
    description : 'Pièces jointes ajoutées au formulaire.',
    type : 'FileReadOnly',
    value : pjUser
}) ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Déclaration de début d\'activite d\'un agent commercial',
    groups : groups
});