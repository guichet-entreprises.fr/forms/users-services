//prepare info to send 

var adresse = $ac0PE1.cadre1IdentiteGroup.cadre1AdresseDeclarant;
var adressePro = $ac0PE1.cadre4AdresseActiviteGroup.cadreAdresseActivite;

var algo = "trouver destinataire";

var secteur1 = "AGENT COMMERCIAL";

var typePersonne = "PP";

var formJuridique = $ac0PE1.cadreEIRLGroup.cadreEIRL.estEIRL ? "EIRL" : "EI";;

var optionCMACCI = "NON";

var codeCommune = Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? adresse.personneLieePersonnePhysiqueAdresseCommune.getId() : adressePro.cadreAdresseProfessionnelle.etablissementAdresseCommune.getId();

var attachement= "/3-review/generated/generated.record-1-AC0_Creation.pdf";

return spec.create({
		id: 'prepareSend',
		label: "Préparation de la recherche du destinataire",
		groups: [spec.createGroup({
				id: 'view',
				label: "Informations",
				data: [spec.createData({
						id: 'algo',
						label: "Algo",
						type: 'String',
						mandatory: true,
						value: algo
					}), spec.createData({
						id: 'secteur1',
						label: "Secteur",
						type: 'String',
						value: secteur1
					}), spec.createData({
						id: 'typePersonne',
						label: "Type personne",
						type: 'String',
						value: typePersonne
					}), spec.createData({
						id: 'formJuridique',
						label: "Forme juridique",
						type: 'String',
						value: formJuridique
					}),spec.createData({
						id: 'optionCMACCI',
						label: "Option CMACCI",
						type: 'String',
						value: optionCMACCI
					}),spec.createData({
						id: 'codeCommune',
						label: "Code commune",
						type: 'String',
						value: codeCommune
					}) ,spec.createData({
					    id: 'attachement',
					    label: "Pièce jointe",
					    type: 'String',
					    value: attachement
					})
				]
			})]
});