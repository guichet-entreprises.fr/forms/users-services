//Fichier de mapping des champs Regent pour :
//Version : V2008.11 
//Evenements : 05P,  
//Fichier généré le : 2018-07-16_111210 

function pad(s) { return (s < 10) ? '0' + s : s; }

var regentVersion = "V2008.11";  // (V2008.11, V2016.02)
var eventRegent = $ac0PE1.cadre1IdentiteGroup.cadre1Identite.formaliteEvenementOui ? "05P" : "01P"; 
var authorityType = "CFE"; // (CFE, TDR)
var authorityId = _INPUT_.dataGroup.codeEDI; // (code EDI)

//Ajout du type de la deuxième autorité
var authorityType2 = "TDR"; // (CFE, TDR) 
//Ajout du code de la deuxième autorité
var authorityId2 = _INPUT_.dataGroup.codeEDI2; // (code EDI) 
 
var regEx = new RegExp('[0-9]{5}'); 
var identite = $ac0PE1.cadre1IdentiteGroup.cadre1Identite;
var conjointInfos = $ac0PE1.cadre2conjointGroup.cadre2Conjoint.cadre2InfosConjoint;
var adresse = $ac0PE1.cadre1IdentiteGroup.cadre1AdresseDeclarant;
var adressePro = $ac0PE1.cadre4AdresseActiviteGroup.cadreAdresseActivite;
var correspondance = $ac0PE1.cadre10RensCompGroup.cadre10RensComp;
var accre = $ac0PE1.cadre3InformationsComplementairesGroup.cadre3InformationsComplementaires;
var conjoint = $ac0PE1.cadre2conjointGroup.cadre2Conjoint;
var insaisissabilite = $ac0PE1.cadre3InformationsComplementairesGroup.cadre3InformationsComplementaires.cadre3DeclarationInsaisissabilite;
var activite = $ac0PE1.cadre4AdresseActiviteGroup.cadreEtablissementActivite;
var social = $ac0PE1.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale;
var declarationAffectation = $ac0PE1.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine;
var signataire = $ac0PE1.cadre11SignatureGroup.cadre11Signature;
var optionsFiscalesME = $ac0PE1.cadre9optFiscHorsEIRLGroup.cadre9optFiscHorsEIRLME;
var optionsFiscales = $ac0PE1.cadre9optFiscHorsEIRLGroup.cadre9optFiscHorsEIRLnonME;
var optionEIRL = $ac0PE1.cadreEIRLGroup.cadreEIRL;
var fiscalEirlNonME = $ac0PE1.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine.cadreOptionsFiscalesEIRLnonME;
var fiscalEirlME = $ac0PE1.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine.cadreOptionsFiscalesEIRLME;


var regentFields = {}; 
regentFields['/REGENT-XML/Emetteur']= "Z1611";

/*******************************************************************************
 * À récuperer depuis directory
 ******************************************************************************/
regentFields['/REGENT-XML/Destinataire']= authorityId;


/*******************************************************************************
 * Complété par destiny
 ******************************************************************************/
regentFields['/REGENT-XML/DateHeureEmission']=null;
regentFields['/REGENT-XML/VersionMessage']=null;
regentFields['/REGENT-XML/VersionNorme']=null;
regentFields['/REGENT-XML/ServiceApplicatif/Specification/NomService']=null;
regentFields['/REGENT-XML/ServiceApplicatif/Specification/VersionService']=null;


/*******************************************************************************
 * Groupe GDF : Groupe données de service Sous groupe IDF : Identification de la
 * formalité
 ******************************************************************************/
// Généré par destiny
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C01']=null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02']=null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C03']= "0";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C04']=null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05']= "R";


regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.1']= identite.formaliteEvenementOui ? "05P" : "01P";

if(activite.etablissementDateDebutActivite !== null) {
    var dateTemp = new Date(parseInt(activite.etablissementDateDebutActivite.getTimeInMillis()));
    var year = dateTemp.getFullYear();
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
    var date = year + "-" + month + "-" + day ;
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2']= date;
}

/*******************************************************************************
 * Sous groupe DMF : Destinataire de la formalité
 ******************************************************************************/
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[1]']= authorityId;
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[2]']= authorityId2;


/*******************************************************************************
 * Sous groupe ADF : Adresse de correspondance
 ******************************************************************************/


regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C36']= correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance != null ? correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance : (identite.personneLieePersonnePhysiqueNomUsage != null ? (identite.personneLieePersonnePhysiqueNomUsage + ' ' + identite.personneLieePersonnePhysiquePrenom1[0]) : (identite.personneLieePersonnePhysiqueNomNaissance + ' ' + identite.personneLieePersonnePhysiquePrenom1[0]));;

if (Value('id').of(adresse.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX') and Value('id').of(correspondance.adresseCorrespond).eq('domi')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']=adresse.personneLieePersonnePhysiqueAdresseCommune.getId();
}
else if (not Value('id').of(adresse.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX') and Value('id').of(correspondance.adresseCorrespond).eq('domi')) {
		var idPays = adresse.personneLieePersonnePhysiqueAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']= result;
}
else if (Value('id').of(correspondance.adresseCorrespond).eq('prof')) {
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']=adressePro.cadreAdresseProfessionnelle.etablissementAdresseCommune.getId();
}
else if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']=correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune.getId();
}
																	
if ((Value('id').of(correspondance.adresseCorrespond).eq('domi') and adresse.personneLieePersonnePhysiqueAdresserueNumeroVoie != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adressePro.cadreAdresseProfessionnelle.etablissementAdresseNumeroVoie != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.5']= Value('id').of(correspondance.adresseCorrespond).eq('domi') ? adresse.personneLieePersonnePhysiqueAdresserueNumeroVoie :
																			(Value('id').of(correspondance.adresseCorrespond).eq('prof') ? adressePro.cadreAdresseProfessionnelle.etablissementAdresseNumeroVoie : 
																			correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance);
	}
	
if (Value('id').of(correspondance.adresseCorrespond).eq('domi') and adresse.personneLieePersonnePhysiqueAdresseIndiceVoie !== null) {
   var monId1 = Value('id').of(adresse.personneLieePersonnePhysiqueAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId1;  
} 
else if (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adressePro.cadreAdresseProfessionnelle.etablissementAdresseIndiceVoie !== null) {
   var monId2 = Value('id').of(adressePro.cadreAdresseProfessionnelle.etablissementAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId2;
}  
else if (correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance !== null) {
   var monId3 = Value('id').of(correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId3;
}

if ((Value('id').of(correspondance.adresseCorrespond).eq('domi') and adresse.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adressePro.cadreAdresseProfessionnelle.etablissementAdresseDistriutionSpecialeVoie != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.7']= Value('id').of(correspondance.adresseCorrespond).eq('domi') ? adresse.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie : 
																				(Value('id').of(correspondance.adresseCorrespond).eq('prof') ? adressePro.cadreAdresseProfessionnelle.etablissementAdresseDistriutionSpecialeVoie : 
																				correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance);
}

if ((Value('id').of(correspondance.adresseCorrespond).eq('domi') and adresse.personneLieePersonnePhysiqueAdresseCodePostal != null and Value('id').of(adresse.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX'))
	or (Value('id').of(correspondance.adresseCorrespond).eq('domi') and not Value('id').of(adresse.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX'))
	or (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adressePro.cadreAdresseProfessionnelle.etablissementAdresseCodePostal != null)) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8']= Value('id').of(correspondance.adresseCorrespond).eq('domi') ? (adresse.personneLieePersonnePhysiqueAdresseCodePostal != null ? adresse.personneLieePersonnePhysiqueAdresseCodePostal : ".") : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('prof') ? adressePro.cadreAdresseProfessionnelle.etablissementAdresseCodePostal : 
																			correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal);
}

/*
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.9']= null;
*/

if ((Value('id').of(correspondance.adresseCorrespond).eq('domi') and adresse.personneLieePersonnePhysiquerueAdresseComplementAdresse != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adressePro.cadreAdresseProfessionnelle.etablissementAdresseComplementVoie != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.10']= Value('id').of(correspondance.adresseCorrespond).eq('domi') ? adresse.personneLieePersonnePhysiquerueAdresseComplementAdresse : 
																				(Value('id').of(correspondance.adresseCorrespond).eq('prof') ? adressePro.cadreAdresseProfessionnelle.etablissementAdresseComplementVoie : 
																				correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance);
}

if (Value('id').of(correspondance.adresseCorrespond).eq('domi') and adresse.personneLieePersonnePhysiqueAdresseTypeVoie !== null) {
   var monId1 = Value('id').of(adresse.personneLieePersonnePhysiqueAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId1;
} 
else if (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adressePro.cadreAdresseProfessionnelle.etablissementAdresseTypeVoie !== null) {
   var monId2 = Value('id').of(adressePro.cadreAdresseProfessionnelle.etablissementAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId2;
}  
else if (correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance !== null) {
   var monId3 = Value('id').of(correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId3;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12']= Value('id').of(correspondance.adresseCorrespond).eq('domi') ? adresse.personneLieePersonnePhysiqueAdresseNomVoie : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('prof') ? adressePro.cadreAdresseProfessionnelle.etablissementAdresseNomVoie : 
																			correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance);
																			
if 	(Value('id').of(correspondance.adresseCorrespond).eq('domi') and Value('id').of(adresse.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {																		
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= adresse.personneLieePersonnePhysiqueAdresseCommune.getLabel();
}
else if (Value('id').of(correspondance.adresseCorrespond).eq('domi') and not Value('id').of(adresse.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= adresse.personneLieeAdresseVille;
}
else if (Value('id').of(correspondance.adresseCorrespond).eq('prof')) {
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= adressePro.cadreAdresseProfessionnelle.etablissementAdresseCommune.getLabel();
}
else if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune.getLabel();
}

if (Value('id').of(correspondance.adresseCorrespond).eq('domi') and not Value('id').of(adresse.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.14']= adresse.personneLieePersonnePhysiqueAdressePays.getLabel();
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.1[1]']= correspondance.infosSup.formaliteTelephone2.e164.replace('+', '00');

if (correspondance.infosSup.formaliteTelephone1 != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.1[2]']= correspondance.infosSup.formaliteTelephone1.e164.replace('+', '00');
}

if (correspondance.infosSup.telecopie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.2']= correspondance.infosSup.telecopie.e164.replace('+', '00');
}

if (correspondance.infosSup.formaliteFaxCourriel != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.3']= correspondance.infosSup.formaliteFaxCourriel;
}


/*******************************************************************************
 * Signature de la Formalité
 ******************************************************************************/

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.1']= Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? signataire.adresseMandataire.nomPrenomDenominationMandataire : (identite.personneLieePersonnePhysiqueNomUsage != null ? (identite.personneLieePersonnePhysiqueNomUsage + ' ' + identite.personneLieePersonnePhysiquePrenom1[0]) : (identite.personneLieePersonnePhysiqueNomNaissance + ' ' + identite.personneLieePersonnePhysiquePrenom1[0]));

if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.2']= "Mandataire";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.3']= signataire.adresseMandataire.villeAdresseMandataire.getId();

/*
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.4']= null;
*/

if (signataire.adresseMandataire.numeroVoieMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.5']= signataire.adresseMandataire.numeroVoieMandataire;
}

if (signataire.adresseMandataire.indiceVoieMandataire !== null) {
   var monId = Value('id').of(signataire.adresseMandataire.indiceVoieMandataire)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.6']          = monId;
}

if (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.7']= signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.8']= signataire.adresseMandataire.dataCodePostalMandataire;

/*
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.9']= null;
*/

if (signataire.adresseMandataire.complementVoieMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.10']= signataire.adresseMandataire.complementVoieMandataire;
}

if (signataire.adresseMandataire.typeVoieMandataire !== null) {
   var monId = Value('id').of(signataire.adresseMandataire.typeVoieMandataire)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.11']          = monId;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.12']= signataire.adresseMandataire.nomVoieMandataire;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.13']= signataire.adresseMandataire.villeAdresseMandataire.getLabel();

/*
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.14']= signataire.adresseMandataire.paysAdresseMandataire != null ? signataire.adresseMandataire.paysAdresseMandataire : null;
*/
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C41']= signataire.formaliteSignatureLieu;

if(signataire.formaliteSignatureDate !== null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C42']= date;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C43']= (signataire.formaliteNonDiffusionInformation ? "C45=N!" : "C45=0!") 
+ '' + (insaisissabilite.entrepriseInsaisissabiliteRenonciationDeclarationAutresBiens ? ("P90.1=A!" +''+ insaisissabilite.entrepriseInsaisissabilitePublicationRenonciationDeclarationAutresBiens +''+ "!") : '') 
+ '' + ((insaisissabilite.entrepriseInsaisissabiliteDeclarationAutresBiens and insaisissabilite.entrepriseInsaisissabilitePublicationDeclarationAutresBiens[0].length > 0) ? 
(("P90.2=C1!"+insaisissabilite.entrepriseInsaisissabilitePublicationDeclarationAutresBiens[0]+"!")
+''+ ((insaisissabilite.entrepriseInsaisissabilitePublicationDeclarationAutresBiens.size() > 1) ? ("C2" +''+ insaisissabilite.entrepriseInsaisissabilitePublicationDeclarationAutresBiens[1] +''+ "!") : '')
+''+ ((insaisissabilite.entrepriseInsaisissabilitePublicationDeclarationAutresBiens.size() > 2) ? ("C3" +''+ insaisissabilite.entrepriseInsaisissabilitePublicationDeclarationAutresBiens[2] +''+ "!") : '')) : '')
+ ' ' + (correspondance.formaliteObservations != null ? correspondance.formaliteObservations : '') ;


/*******************************************************************************
 * Identification Complète de la personne Physique
 ******************************************************************************/
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.1']= Value('id').of(identite.civilite).eq('PersonneLieePersonnePhysiqueCiviliteFeminin') ? "2" : "1";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.2']= identite.personneLieePersonnePhysiqueNomNaissance;

var prenoms=[];
for (i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){
	prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);
}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.3']= prenoms;

if(identite.personneLieePersonnePhysiqueNomUsage != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P02/P02.1']= identite.personneLieePersonnePhysiqueNomUsage;
}

if(identite.personneLieePersonnePhysiqueDateNaissance !== null) {
    var dateTemp = new Date(parseInt(identite.personneLieePersonnePhysiqueDateNaissance.getTimeInMillis()));
    var year = dateTemp.getFullYear();
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
    var date = year + "-" + month + "-" + day ;
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.1']= date;
}

if (not Value('id').of(identite.personneLieePersonnePhysiqueLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = identite.personneLieePersonnePhysiqueLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.2']= result;
}
else if (Value('id').of(identite.personneLieePersonnePhysiqueLieuNaissancePays).eq('FRXXXXX')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.2'] = identite.personneLieePersonnePhysiqueLieuNaissanceCommune.getId();
}																						 
																					 
if (not Value('id').of(identite.personneLieePersonnePhysiqueLieuNaissancePays).eq('FRXXXXX')) {																					 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.3'] = identite.personneLieePersonnePhysiqueLieuNaissancePays.getLabel();
}

if (Value('id').of(identite.personneLieePersonnePhysiqueLieuNaissancePays).eq('FRXXXXX')) {	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.4']= identite.personneLieePersonnePhysiqueLieuNaissanceCommune.getLabel();
}


/*******************************************************************************
 * Domicile personnel ou commune de rattachement pour les forains
 ******************************************************************************/
 
if (Value('id').of(adresse.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.3']= adresse.personneLieePersonnePhysiqueAdresseCommune.getId();
}
else if (not Value('id').of(adresse.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {
	var idPays = adresse.personneLieePersonnePhysiqueAdressePays.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.3']= result;
}

if (adresse.personneLieePersonnePhysiqueAdresserueNumeroVoie !=null) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.5']= adresse.personneLieePersonnePhysiqueAdresserueNumeroVoie;
}
if (adresse.personneLieePersonnePhysiqueAdresseIndiceVoie !==null) {
	   var monId = Value('id').of(adresse.personneLieePersonnePhysiqueAdresseIndiceVoie)._eval();
	   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.6']          = monId;
}
if (adresse.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie !=null) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.7']= adresse.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.8']  = adresse.personneLieePersonnePhysiqueAdresseCodePostal != null ? adresse.personneLieePersonnePhysiqueAdresseCodePostal : ".";

if (adresse.personneLieePersonnePhysiquerueAdresseComplementAdresse !=null)  {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.10'] = adresse.personneLieePersonnePhysiquerueAdresseComplementAdresse;
}
if (adresse.personneLieePersonnePhysiqueAdresseTypeVoie !==null) {
   var monId = Value('id').of(adresse.personneLieePersonnePhysiqueAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.11']          = monId;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.12'] = adresse.personneLieePersonnePhysiqueAdresseNomVoie;

if(Value('id').of(adresse.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.13'] = adresse.personneLieePersonnePhysiqueAdresseCommune.getLabel();
}
else{
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.13'] = adresse.personneLieeAdresseVille;
}

if (not Value('id').of(adresse.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')){
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.14']= adresse.personneLieePersonnePhysiqueAdressePays.getLabel();
}

// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P05']=null;


// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.1']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.2']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.3']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.5']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.6']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.7']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.8']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.10']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.11']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.12']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.13']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.14']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.4']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.5']=null;



/*******************************************************************************
 * Ancienne Identification de la personne physique
 ******************************************************************************/

 if(identite.voletSocialActiviteExerceeAnterieurementSIREN != null){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P12']= identite.voletSocialActiviteExerceeAnterieurementSIREN.split(' ').join('');
}

/*******************************************************************************
 * Nationalité de la personne physique
 ******************************************************************************/

 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/NAP/P21']= identite.personneLieePersonnePhysiqueNationalite;

/*******************************************************************************
 * Mineur Emancipé
 ******************************************************************************/
// non concerné

/*******************************************************************************
 * Insaisissabilité de la résidence principale
 ******************************************************************************/
// non concerné

/*******************************************************************************
 * Déclaration d'affectation du patrimoine (si option pour l'EIRL)
 ******************************************************************************/
if (optionEIRL.estEIRL) {
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P70']= Value('id').of(declarationAffectation.eirlStatut).eq('EirlStatutEIRLReprise') ? "R" : "O";
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P71']= declarationAffectation.declarationPatrimoine.eirlDenomination;

if (declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable !== null) {
		   var dateTmp = new Date(parseInt(declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable.getTimeInMillis()));
		   var dateClotureEc = pad(dateTmp.getDate().toString());
		   var month = dateTmp.getMonth() + 1;
		   dateClotureEc = dateClotureEc.concat(pad(month.toString()));
		   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P72']          = dateClotureEc;
		}
	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P73']= declarationAffectation.declarationPatrimoine.objetPartiel ? declarationAffectation.declarationPatrimoine.eirlObjet : activite.etablissementSecteursActivites;
	
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P75']= "1";
	
if (Value('id').of(declarationAffectation.eirlStatut).eq('EirlStatutEIRLReprise')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P76/P76.1']= declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLDenomination;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P76/P76.2']= Value('id').of(declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLRegistre).eq('EirlPrecedentEIRLRegistreRseirl') ? "3" : "4";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P76/P76.3']= declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLLieuImmatriculation;
if (declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN != null){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P76/P76.4']= declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN.split(' ').join('');
}
}
}

/*******************************************************************************
 * Situation Matrimoniale de la personne Physique
 ******************************************************************************/

if (Value('id').of(conjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieCollaborateur')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P25/P25.2']= conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueNomNaissanceConjointPacse;

prenoms=[];
for (i = 0; i < conjoint.cadre2InfosConjoint.personneLieePersonnePhysiquePrenom1ConjointPacse.size() ; i++ ){
	prenoms.push(conjoint.cadre2InfosConjoint.personneLieePersonnePhysiquePrenom1ConjointPacse[i]);     
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P25/P25.3']= prenoms;

if (conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueNomUsageConjointPacse != null)  {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P25/P25.4']= conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueNomUsageConjointPacse;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P40']= "O";
if(conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueDateNaissanceConjointPacse !== null) {
	    var dateTemp = new Date(parseInt(conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueDateNaissanceConjointPacse.getTimeInMillis()));
	    var year = dateTemp.getFullYear();
	    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	    var date = year + "-" + month + "-" + day ;
	    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.1'] = date;
}
	
if	(Value('id').of(conjointInfos.personneLieePersonnePhysiquePaysNaissanceConjointPacse).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.2']= conjointInfos.personneLieePersonnePhysiqueLieuNaissanceCommuneConjointPacse.getId();
}
else if (not Value('id').of(conjointInfos.personneLieePersonnePhysiquePaysNaissanceConjointPacse).eq('FRXXXXX')) {
		var idPays = conjointInfos.personneLieePersonnePhysiquePaysNaissanceConjointPacse.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.2']= result;
}	
																						  
if (not Value('id').of(conjoint.cadre2InfosConjoint.personneLieePersonnePhysiquePaysNaissanceConjointPacse).eq('FRXXXXX')) {																					 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.3'] = conjoint.cadre2InfosConjoint.personneLieePersonnePhysiquePaysNaissanceConjointPacse.getLabel();
}

if (Value('id').of(conjoint.cadre2InfosConjoint.personneLieePersonnePhysiquePaysNaissanceConjointPacse).eq('FRXXXXX')){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.4'] = conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueLieuNaissanceCommuneConjointPacse.getLabel();
}
if (conjointInfos.adresseConjointDifferente) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.3']= conjointInfos.adresseDomicileConjoint.adresseConjointCommune.getId();
if (conjointInfos.adresseDomicileConjoint.adresseConjointNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.5']= conjointInfos.adresseDomicileConjoint.adresseConjointNumeroVoie;
}
if (conjointInfos.adresseDomicileConjoint.adresseConjointIndiceVoie !== null) {
   var monId = Value('id').of(conjointInfos.adresseDomicileConjoint.adresseConjointIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.6']          = monId;
}
if (conjointInfos.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.7']= conjointInfos.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.8']= conjointInfos.adresseDomicileConjoint.adresseConjointCodePostal;
if(conjointInfos.adresseDomicileConjoint.adresseConjointComplementVoie != null) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.10']= conjointInfos.adresseDomicileConjoint.adresseConjointComplementVoie;
}
if (conjointInfos.adresseDomicileConjoint.adresseConjointTypeVoie !== null) {
   var monId = Value('id').of(conjointInfos.adresseDomicileConjoint.adresseConjointTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.11']          = monId;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.12']= conjointInfos.adresseDomicileConjoint.adresseConjointNomVoie;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.13']= conjointInfos.adresseDomicileConjoint.adresseConjointCommune.getLabel();
}
}

if (Value('id').of(conjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieSalarie')){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P45'] = "O";
}

/*******************************************************************************
 * Justification pour le RCS des déclarations relatives à la personne Physique
 ******************************************************************************/
// non concerné

/*******************************************************************************
 * Immatriculation Sécurité Sociale du travailleur non salarié
 ******************************************************************************/
var nirDeclarant = social.voletSocialNumeroSecuriteSociale;

if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A10/A10.1']  = nirDeclarant.substring(0, 13);
}
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A10/A10.2'] = nirDeclarant.substring(13, 15);
}


/*******************************************************************************
 * Situation du travailleur Non Salarié
 ******************************************************************************/

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A23']= "O";

if (social.voletSocialActiviteAutreQueDeclareeStatutOui) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A24/A24.2']= Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutSalarie') ? "1" :
																						(Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutSalarieAgricole') ? "2" : 
																						(Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutRetraitePensionne') ? "8" : "9" ));
if (Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutCocheAutre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A24/A24.3']= social.voletSocialActiviteAutreQueDeclareeStatutAutre;
}
}

/*******************************************************************************
 * Justification du droit d’exercice
 ******************************************************************************/

if (social.ressortissantHorsUE == true) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/JES/A30/A30.1']= social.voletSocialTitreSejourLieuDelivranceCommune.getId() ;
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/JES/A30/A30.2']= social.voletSocialTitreSejourLieuDelivranceCommune.getLabel();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/JES/A30/A30.3']= social.voletSocialTitreSejourNumero;
	
	if(social.voletSocialTitreSejourDateExpiration !== null) {
	    var dateTemp = new Date(parseInt(social.voletSocialTitreSejourDateExpiration.getTimeInMillis()));
	    var year = dateTemp.getFullYear();
	    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	    var date = year + "-" + month + "-" + day ;
	    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/JES/A30/A30.4'] = date;
	}
}

/*******************************************************************************
 * Choix de l’organisme d’Assurance maladie du TNS - MSA
 ******************************************************************************/

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/CAS/A42/A42.1']= ".";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/CAS/A42/A42.2']= ".";

/*******************************************************************************
 * Situation du Conjoint vis-à-vis de la sécurité Sociale
 ******************************************************************************/

if (Value('id').of(conjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieCollaborateur')) {
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A53/A53.4']= conjointInfos.voletSocialConjointCouvertAssuranceMaladieOui ? "O" : "N";

var nirDeclarant = conjointInfos.voletSocialConjointCollaborateurNumeroSecuriteSociale;

if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A55/A55.1'] = nirDeclarant.substring(0, 13);
}
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A55/A55.2'] = nirDeclarant.substring(13, 15);
}
}

/*******************************************************************************
 * Situation du conjoint Associé du TNS
 ******************************************************************************/
// non concerné

/*******************************************************************************
 * régime Micro-Social du TNS
 ******************************************************************************/

if (identite.microEntrepreneurOuiNon) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/MSS/A31/A31.1']= "O";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/MSS/A31/A31.2']= Value('id').of(social.optionMicroSocialSimplifie).eq('VoletSocialOptionMicroSocialVersementMensuel') ? "M" : "T";
}

/*******************************************************************************
 * Siège de l'entreprise (adresse de l'entreprise individuelle)
 ******************************************************************************/

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U16']="O";
}

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.3']= adresse.personneLieePersonnePhysiqueAdresseCommune.getId();
}
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.3']= adressePro.cadreAdresseProfessionnelle.etablissementAdresseCommune.getId();
}		
	
if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')  and adresse.personneLieePersonnePhysiqueAdresserueNumeroVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.5']= adresse.personneLieePersonnePhysiqueAdresserueNumeroVoie;
}
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadreAdresseProfessionnelle.etablissementAdresseNumeroVoie != null){	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.5']= adressePro.cadreAdresseProfessionnelle.etablissementAdresseNumeroVoie;
}	

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adresse.personneLieePersonnePhysiqueAdresseIndiceVoie !== null) {
   var monId1 = Value('id').of(adresse.personneLieePersonnePhysiqueAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.6']          = monId1;
} else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadreAdresseProfessionnelle.etablissementAdresseIndiceVoie !== null) {
   var monId2 = Value('id').of(adressePro.cadreAdresseProfessionnelle.etablissementAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.6']          = monId2;
}

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')  and adresse.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.7']= adresse.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie;
}
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadreAdresseProfessionnelle.etablissementAdresseDistriutionSpecialeVoie != null){	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.7']= adressePro.cadreAdresseProfessionnelle.etablissementAdresseDistriutionSpecialeVoie;
}	
	
if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.8']= adresse.personneLieePersonnePhysiqueAdresseCodePostal;
}
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.8']= adressePro.cadreAdresseProfessionnelle.etablissementAdresseCodePostal;
}		

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')  and adresse.personneLieePersonnePhysiquerueAdresseComplementAdresse != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.10']= adresse.personneLieePersonnePhysiquerueAdresseComplementAdresse;
}
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadreAdresseProfessionnelle.etablissementAdresseComplementVoie != null){	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.10']= adressePro.cadreAdresseProfessionnelle.etablissementAdresseComplementVoie;
}

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adresse.personneLieePersonnePhysiqueAdresseTypeVoie !== null) {
   var monId1 = Value('id').of(adresse.personneLieePersonnePhysiqueAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.11']          = monId1;
} else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadreAdresseProfessionnelle.etablissementAdresseTypeVoie !== null) {
   var monId2 = Value('id').of(adressePro.cadreAdresseProfessionnelle.etablissementAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.11']          = monId2;
}

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.12']= adresse.personneLieePersonnePhysiqueAdresseNomVoie;
}
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.12']= adressePro.cadreAdresseProfessionnelle.etablissementAdresseNomVoie;
}		

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.13']= adresse.personneLieePersonnePhysiqueAdresseCommune.getLabel();
}
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.13']= adressePro.cadreAdresseProfessionnelle.etablissementAdresseCommune.getLabel();
}	
	

/*******************************************************************************
 * Caractéristiques de l’Entreprise
 ******************************************************************************/

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U21']= activite.etablissementSecteursActivites;

if (activite.etablissementEffectifSalariePresenceOui) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U22']= activite.etablissementEffectifSalarieNombre;
}

/*******************************************************************************
 * Contrat d’appui
 ******************************************************************************/

if (accre.entrepriseContratAppuiPresence) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.1']= "D";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.1']=accre.cadre3ContratAppuiInfos.contratAppuiDenomination;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.2/U40.2.2.3'] = accre.cadre3ContratAppuiInfos.contratAppuiAdresse.communeAdresseContratAppui.getId();

if (accre.cadre3ContratAppuiInfos.contratAppuiAdresse.numeroAdresseContratAppui != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.2/U40.2.2.5']=accre.cadre3ContratAppuiInfos.contratAppuiAdresse.numeroAdresseContratAppui;
}

if (accre.cadre3ContratAppuiInfos.contratAppuiAdresse.indiceAdresseContratAppui != null !== null) {
   var monId1 = Value('id').of(accre.cadre3ContratAppuiInfos.contratAppuiAdresse.indiceAdresseContratAppui)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.2/U40.2.2.6'] = monId1;
}

if (accre.cadre3ContratAppuiInfos.contratAppuiAdresse.distributionSpecialeAdresseContratAppui != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.2/U40.2.2.7']=accre.cadre3ContratAppuiInfos.contratAppuiAdresse.distributionSpecialeAdresseContratAppui;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.2/U40.2.2.8']=accre.cadre3ContratAppuiInfos.contratAppuiAdresse.codePostalAdresseContratAppui;

if (accre.cadre3ContratAppuiInfos.contratAppuiAdresse.complementAdresseContratAppui != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.2/U40.2.2.10']=accre.cadre3ContratAppuiInfos.contratAppuiAdresse.complementAdresseContratAppui;
}

if (accre.cadre3ContratAppuiInfos.contratAppuiAdresse.typeAdresseContratAppui != null !== null) {
   var monId1 = Value('id').of(accre.cadre3ContratAppuiInfos.contratAppuiAdresse.typeAdresseContratAppui)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.2/U40.2.2.11'] = monId1;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.2/U40.2.2.12']=accre.cadre3ContratAppuiInfos.contratAppuiAdresse.voieAdresseContratAppui;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.2/U40.2.2.13']=accre.cadre3ContratAppuiInfos.contratAppuiAdresse.communeAdresseContratAppui.getLabel();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.4']=accre.cadre3ContratAppuiInfos.contratAppuiSiren.split(' ').join('');

if(accre.contratAppuiDateFin !== null) {
	    var dateTemp = new Date(parseInt(accre.contratAppuiDateFin.getTimeInMillis()));
	    var year = dateTemp.getFullYear();
	    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	    var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.5'] = date;
}
}

/*******************************************************************************
 * Attestation de qualification professionnelle
 ******************************************************************************/
// non concerné

/*******************************************************************************
 * Etablissements situés dans l'UE
 ******************************************************************************/
// non concerné

/*******************************************************************************
 * Régime Fiscal de l’Entreprise
 ******************************************************************************/

if ($ac0PE1.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine.declarationPatrimoine.objetPartiel or not $ac0PE1.cadreEIRLGroup.cadreEIRL.estEIRL) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U31']= (identite.microEntrepreneurOuiNon or Value('id').of(optionsFiscales.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesRsBNC')) ? "110" : "111";

if (optionsFiscales.impositionBenefices.regimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionCreanceDette) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U32']= "210";
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.1']= (identite.microEntrepreneurOuiNon 
																				or Value('id').of(optionsFiscales.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesRsBNC')
																				or Value('id').of(optionsFiscales.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARfTVA')) ? "310" :
																				(Value('id').of(optionsFiscales.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARsTVA') ? "311" : "312");

if (optionsFiscales.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres1) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.2']= "410";
}
}

/*******************************************************************************
 * Option fiscale EIRL
 ******************************************************************************/
 
if ($ac0PE1.cadreEIRLGroup.cadreEIRL.estEIRL) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U70']= (identite.microEntrepreneurOuiNon
																		or Value('id').of(fiscalEirlNonME.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesRsBNC')) ? "110" : 
																		(Value('id').of(fiscalEirlNonME.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesDcBNC') ? "111" : 
																		((Value('id').of(fiscalEirlNonME.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionIS') and Value('id').of(fiscalEirlNonME.impositionBeneficesEIRL.regimeFiscalIS).eq('RegimeFiscalRegimeImpositionBeneficesRsIS')) ? "114" : "115"));

if (Value('id').of(fiscalEirlNonME.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionIS') or fiscalEirlNonME.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionCreanceDette) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U71']= fiscalEirlNonME.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionCreanceDette ? "210" : "211";
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U72/U72.1']= (identite.microEntrepreneurOuiNon or
																				Value('id').of(fiscalEirlNonME.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesRsBNC')
																				or Value('id').of(fiscalEirlNonME.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARfTVA')) ? "310" : 
																				(Value('id').of(fiscalEirlNonME.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARsTVA') ? "311" : "312");
																				
if (fiscalEirlNonME.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres1DC) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U72/U72.2']= "410";	
}
																			
// option pour le versement liberatoire
if (identite.microEntrepreneurOuiNon) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U74']= fiscalEirlME.regimeFiscalRegimeImpositionBeneficesVersementLiberatoire ? "O" : "N"; 
}
}	
		
/*******************************************************************************
 * Option pour le Versement libératoire de l’impôt sur le revenu de l'entreprise
 ******************************************************************************/

if (($ac0PE1.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine.declarationPatrimoine.objetPartiel or not $ac0PE1.cadreEIRLGroup.cadreEIRL.estEIRL) and identite.microEntrepreneurOuiNon) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OVU/U42']= optionsFiscalesME.regimeFiscalRegimeImpositionBeneficesVersementLiberatoireOui ? "O" :"N";
}

/*******************************************************************************
 * Justification pour le RCS des déclarations relatives à l'entreprise
 ******************************************************************************/
// non concerné

/*******************************************************************************
 * Justification pour le RM des déclarations relatives à l'entreprise
 ******************************************************************************/
// non concerné

/*******************************************************************************
 * Identification Complète de l’Etablissement
 ******************************************************************************/
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E01']= "1";

 _log.info("adresse domicile value is {}",adresse.entrepriseAdresseEntrepriseDomicile );
 _log.info("adresse domicile id is {}",Value('id').of(adressePro.activiteLieu));

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.3']= adresse.personneLieePersonnePhysiqueAdresseCommune.getId();
}
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.3']= adressePro.cadreAdresseProfessionnelle.etablissementAdresseCommune.getId();
}		
	
if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')  and adresse.personneLieePersonnePhysiqueAdresserueNumeroVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5']= adresse.personneLieePersonnePhysiqueAdresserueNumeroVoie;
}
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadreAdresseProfessionnelle.etablissementAdresseNumeroVoie != null){	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5']= adressePro.cadreAdresseProfessionnelle.etablissementAdresseNumeroVoie;
}	

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adresse.personneLieePersonnePhysiqueAdresseIndiceVoie !== null) {
   var monId1 = Value('id').of(adresse.personneLieePersonnePhysiqueAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.6']          = monId1;
} else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadreAdresseProfessionnelle.etablissementAdresseIndiceVoie !== null) {
   var monId2 = Value('id').of(adressePro.cadreAdresseProfessionnelle.etablissementAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.6']          = monId2;
}

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')  and adresse.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.7']= adresse.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie;
}
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadreAdresseProfessionnelle.etablissementAdresseDistriutionSpecialeVoie != null){	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.7']= adressePro.cadreAdresseProfessionnelle.etablissementAdresseDistriutionSpecialeVoie;
}	
	
if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.8']= adresse.personneLieePersonnePhysiqueAdresseCodePostal;
}
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.8']= adressePro.cadreAdresseProfessionnelle.etablissementAdresseCodePostal;
}		

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')  and adresse.personneLieePersonnePhysiquerueAdresseComplementAdresse != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.10']= adresse.personneLieePersonnePhysiquerueAdresseComplementAdresse;
}
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadreAdresseProfessionnelle.etablissementAdresseComplementVoie != null){	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.10']= adressePro.cadreAdresseProfessionnelle.etablissementAdresseComplementVoie;
}

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adresse.personneLieePersonnePhysiqueAdresseTypeVoie !== null) {
   var monId1 = Value('id').of(adresse.personneLieePersonnePhysiqueAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.11']          = monId1;
} else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadreAdresseProfessionnelle.etablissementAdresseTypeVoie !== null) {
   var monId2 = Value('id').of(adressePro.cadreAdresseProfessionnelle.etablissementAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.11']          = monId2;
}

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.12']= adresse.personneLieePersonnePhysiqueAdresseNomVoie;
}
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.12']= adressePro.cadreAdresseProfessionnelle.etablissementAdresseNomVoie;
}		

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.13']= adresse.personneLieePersonnePhysiqueAdresseCommune.getLabel();
}
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.13']= adressePro.cadreAdresseProfessionnelle.etablissementAdresseCommune.getLabel();
}	

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E04']="3";
	
/*******************************************************************************
 * Origine de l’Etablissement
 ******************************************************************************/

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.1']= "1";

/*******************************************************************************
 * Précédent Exploitant de l’Etablissement
 ******************************************************************************/
// non concerné

/*******************************************************************************
 * Location Gérance de l’Etablissement ou gérance-mandat
 ******************************************************************************/
// non concerné

/*******************************************************************************
 * Activité de l’Etablissement
 ******************************************************************************/

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E70'] = activite.etablissementSecteursActivites;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E71'] = activite.etablissementActivitePlusImportante;

/*******************************************************************************
 * Compléments sur l’Activité de l’Etablissement
 ******************************************************************************/
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E73/E73.1']= "P";

/*******************************************************************************
 * SAlariés de l’Etablissement
 ******************************************************************************/

if (activite.etablissementEffectifSalariePresenceOui){
if (activite.etablissementEffectifSalarieEmbauchePremierSalarieOui) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E83/E83.1']="1";
}	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E84']=activite.etablissementEffectifSalarieNombre;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E86']= activite.etablissementEffectifSalariePresenceOui ? "O" : "N";


/*******************************************************************************
 * Identification de la personne liée à l’établissement
 ******************************************************************************/
// non concerné

/*******************************************************************************
 * Justification pour le RCS des déclarations relatives à l’Etablissement
 ******************************************************************************/
// non concerné
 
_log.info("U21 value is {}", regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U21']);

_log.info("P12 value is {}", regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P12']);

// Call to the XML-REGENT generation WS 
var response = nash.service.request('${regent.baseUrl}/private/v1/xml-regent/generate/{regentVersion}/{authorityType}/{authorityId}', regentVersion, authorityType, authorityId) 
.dataType('application/json') // 
.accept('json') // 
.param('listTypeEvenement',eventRegent) 
//-->MINE-263 : Permettre à l'équipe FF de voir les problèmes de validation XSD et le regent sur studio
.continueOnError(true) //
.post(JSON.stringify(regentFields)); 

// Record the generated XML-REGENT 
//MOD déplacement du test de la réponse du premier ws
if (response != null && response.status == 200) { 
    var xmlRegentStr = response.asBytes(); 
    nash.record.saveFile("XML_REGENT.xml",xmlRegentStr); 

	
//debut de l'ajout
 if (authorityId2 != null) {
//MOD extraction du numéro de liasse du premeir regent généré
var numeroLiasse = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02');
numeroLiasse = JSON.parse(numeroLiasse);
_log.info('extracted numero de liasse is {}', numeroLiasse);
 regentFields['/REGENT-XML/Destinataire']= authorityId2;

// Filtre regent greffe
var undefined;
// Groupe GCS/ISS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A10/A10.1']  = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A10/A10.2'] = undefined;
// Groupe GCS/SNS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A23'] = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A24/A24.2']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A24/A24.3']= undefined;
// Groupe GCS/JES
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/JES/A30/A30.1']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/JES/A30/A30.2']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/JES/A30/A30.3']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/JES/A30/A30.4'] = undefined;
// Groupe GCS/CAS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/CAS/A42/A42.1']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/CAS/A42/A42.2']= undefined;
// Groupe GCS/SCS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A53/A53.4']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A55/A55.1'] = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A55/A55.2'] = undefined;
// Groupe GCS/MSS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/MSS/A31/A31.1']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/MSS/A31/A31.2']= undefined;
// Groupe RFU
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U31']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.1']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.2']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U37']= undefined;
// Groupe OFU
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U70']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U71']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U72/U72.1']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U72/U72.2']= undefined;
// Groupe SAE
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E83/E83.1']=undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E84']=undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E85/E85.8']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E86']= undefined;

 //MOD modification de l'appel pour la génération du deuxième régent afin de prendre en compte le numéro de liasse du premier
 var response2 = nash.service.request('${regent.baseUrl}/private/v1/xml-regent/generate/{regentVersion}/{authorityType}/{authorityId}', regentVersion, authorityType2, authorityId2) 
.dataType('application/json') // 
.accept('json') // 
.param('listTypeEvenement',eventRegent) 
.param('liasseNumber', numeroLiasse.C02) 
.continueOnError(true)
.post(JSON.stringify(regentFields)); 


	//Début de l'ajout
	//MOD modifier reponse en response2
	if (response2 != null && response2.status == 200) { 
		var xmlRegentStr = response2.asBytes(); 
		nash.record.saveFile("XML_REGENT2.xml",xmlRegentStr);
	}else{
			var returnedResponse = response2.asObject();
		_log.info("Call ws regent returned errors  {}", returnedResponse);
		
		
		var regent2 = returnedResponse['regent'];
		var errors2 = returnedResponse['errors']; 
		nash.record.saveFile("XML_REGENT2.xml",regent2.getBytes()); 
		nash.record.saveFile("XML_VALIDATION_ERRORS_2.xml",errors2.getBytes()); 
  
//-->MINE-263 : Permettre à l'équipe FF de voir les problèmes de validation XSD et le regent sur studio
		
		return spec.create({
		id : 'xmlGenerationConfirmation',
		label : "Xml Regent confirmation message",
		groups : [ spec.createGroup({
				id : 'Regent ',
				description : "Une erreur s'est produite lors de la génération du XML Regent.",
				data : [
				spec.createData({
					id: 'regent',
					label: "XML regent généré",
					type: 'Text',
					mandatory: true,
					value: regent2
				}),spec.createData({
					id: 'errors',
					label: "Erreurs de validations xsd",
					type: 'Text',
					mandatory: false,
					value: errors2
				}),spec.createData({
					id: 'blockingField',
					label: "xsd erroné",
					help:"Ce champs sert à arrêter le process du dossier pour que le support puisse le consulter",
					type: 'StringReadOnly',
					mandatory: true
				})]
			})]
		});
	}	
}	

	return spec.create({
	id : 'xmlGenerationConfirmation',
	label : "Xml Regent confirmation message",
	groups : [ spec.createGroup({
		id : 'confirmationMessageOk',
		description : "Le fichier XML Regent a été généré et ajouté au dossier.",
		data : []
		}) ]
	});
}else{
	
	var returnedResponse = response.asObject();
	_log.info("Call ws regent returned errors  {}", returnedResponse);
	
	
	var regent = returnedResponse['regent'];
	var errors = returnedResponse['errors']; 
	nash.record.saveFile("XML_REGENT.xml",regent.getBytes()); 
	nash.record.saveFile("XML_VALIDATION_ERRORS.xml",errors.getBytes()); 
	
	
	return spec.create({
	id : 'xmlGenerationConfirmation',
	label : "Xml Regent confirmation message",
	groups : [ spec.createGroup({
		id : 'Regent ',
		description : "Une erreur s'est produite lors de la génération du XML Regent de la première autorité.",
		data : [
				spec.createData({
					id: 'regent',
					label: "XML regent généré",
					type: 'Text',
					mandatory: true,
					value: regent
				}),spec.createData({
					id: 'errors',
					label: "Erreurs de validations xsd",
					type: 'Text',
					mandatory: false,
					value: errors
				}),spec.createData({
					id: 'blockingField',
					label: "xsd erroné",
					help:"Ce champs sert à arrêter le process du dossier pour que le support puisse le consulter",
					type: 'StringReadOnly',
					mandatory: true
			})]
		}) ]
	});
}	