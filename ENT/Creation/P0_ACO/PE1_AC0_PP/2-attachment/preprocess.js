// PJ Déclarant

var userDeclarant;
if ($ac0PE1.cadre1IdentiteGroup.cadre1Identite.personneLieePersonnePhysiqueNomUsage != null) {
    var userDeclarant = $ac0PE1.cadre1IdentiteGroup.cadre1Identite.personneLieePersonnePhysiqueNomUsage + '  ' + $ac0PE1.cadre1IdentiteGroup.cadre1Identite.personneLieePersonnePhysiquePrenom1[0] ;
} else {
    var userDeclarant = $ac0PE1.cadre1IdentiteGroup.cadre1Identite.personneLieePersonnePhysiqueNomNaissance + '  '+ $ac0PE1.cadre1IdentiteGroup.cadre1Identite.personneLieePersonnePhysiquePrenom1[0] ;
}
attachment('pjDNCDeclarant', 'pjDNCDeclarant',{ label: userDeclarant, mandatory:"true"}) ;


var pj=$ac0PE1.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteDeclarant')) {
    attachment('pjIDDeclarantSignataire', 'pjIDDeclarantSignataire', { label: userDeclarant, mandatory:"true"});
}

var pj=$ac0PE1.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
    attachment('pjIDDeclarantNonSignataire', 'pjIDDeclarantNonSignataire', { label: userDeclarant, mandatory:"true"});
}

// // PJ Mandataire

var pj=$ac0PE1.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
    attachment('pjPouvoir', 'pjPouvoir', { mandatory:"true"});
}

var userMandataire=$ac0PE1.cadre11SignatureGroup.cadre11Signature.adresseMandataire

var pj=$ac0PE1.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
    attachment('pjIDMandataireSignataire', 'pjIDMandataireSignataire', {label: userMandataire.nomPrenomDenominationMandataire, mandatory:"true"});
}

// PJ agent co

attachment('pjContratAgentCo', 'pjContratAgentCo', { mandatory:"true"});

// PJ Conjoint

var pj=$ac0PE1.cadre2conjointGroup.cadre2Conjoint.statutConjointCollaborateur ;
if(Value('id').of(pj).contains('PersonneLieeConjointCollaborateurSalarieCollaborateur')) {
    attachment('pjIDConjoint', 'pjIDConjoint', { mandatory:"true"});
}

var pj=$ac0PE1.cadre2conjointGroup.cadre2Conjoint ;
if(pj.conjointCommunauteBiens and pj.conjoint) {
    attachment('pjInformationConjoint', 'pjInformationConjoint', { mandatory:"true"});
}


// PJ Insaisissabilité

var pj=$ac0PE1.cadre3InformationsComplementairesGroup.cadre3InformationsComplementaires ;
if(pj.cadre3DeclarationInsaisissabilite.entrepriseInsaisissabiliteDeclarationAutresBiens) {
    attachment('pjInsaisissabilite', 'pjInsaisissabilite', { mandatory:"true"});
}

// PJ Contrat d'appui

var pj=$ac0PE1.cadre3InformationsComplementairesGroup.cadre3InformationsComplementaires ;
if(pj.entrepriseContratAppuiPresence) {
    attachment('pjContratAppui', 'pjContratAppui', { mandatory:"true"});
}

// PJ EIRL

var pj=$ac0PE1.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine;
if($ac0PE1.cadreEIRLGroup.cadreEIRL.estEIRL and not Value('id').of(pj.eirlDepot).eq('eirlSansDepot')) {
    attachment('pjDAP', 'pjDAP', { mandatory:"true"});
}

if(Value('id').of(pj.cadreEIRLInformationsComplementaires.contenuDAP).contains('bienImmo') or Value('id').of(pj.cadreEIRLInformationsComplementairesBis.contenuDAPBis).contains('bienImmo')) {
    attachment('pjDAPActeNotarie', 'pjDAPActeNotarie', { mandatory:"true"});
}

if(Value('id').of(pj.cadreEIRLInformationsComplementaires.contenuDAP).contains('bienCommunIndivis') or Value('id').of(pj.cadreEIRLInformationsComplementairesBis.contenuDAPBis).contains('bienCommunIndivis')) {
    attachment('pjDAPAccordTiers', 'pjDAPAccordTiers', { mandatory:"true"});
}

var pj=$ac0PE1.cadreEIRLGroup.cadreEIRL;
if(pj.estEIRL) {
    attachment('pjAutorisationMineur', 'pjAutorisationMineur', { mandatory:"false"});
}
   



