// PJ Déclarant

var userDeclarant;
if ($p0cmbme.cadre1IdentiteGroup.cadre1Identite.personneLieePersonnePhysiqueNomUsage != null) {
    var userDeclarant = $p0cmbme.cadre1IdentiteGroup.cadre1Identite.personneLieePersonnePhysiqueNomUsage + '  ' + $p0cmbme.cadre1IdentiteGroup.cadre1Identite.personneLieePersonnePhysiquePrenom1[0] ;
} else {
    var userDeclarant = $p0cmbme.cadre1IdentiteGroup.cadre1Identite.personneLieePersonnePhysiqueNomNaissance + '  '+ $p0cmbme.cadre1IdentiteGroup.cadre1Identite.personneLieePersonnePhysiquePrenom1[0] ;
}
attachment('pjDNCDeclarant', 'pjDNCDeclarant',{ label: userDeclarant, mandatory:"true"}) ;
attachment('pjIDActiviteReglementee', 'pjIDActiviteReglementee',{mandatory:"false"}) ;

var pj=$p0cmbme.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteDeclarant')) {
    attachment('pjIDDeclarantSignataire', 'pjIDDeclarantSignataire', { label: userDeclarant, mandatory:"true"});
}

var pj=$p0cmbme.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
    attachment('pjIDDeclarantNonSignataire', 'pjIDDeclarantNonSignataire', { label: userDeclarant, mandatory:"true"});
}

// // PJ Mandataire

var pj=$p0cmbme.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
    attachment('pjPouvoir', 'pjPouvoir', { mandatory:"true"});
}

var userMandataire=$p0cmbme.cadre11SignatureGroup.cadre11Signature.adresseMandataire

var pj=$p0cmbme.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
    attachment('pjIDMandataireSignataire', 'pjIDMandataireSignataire', {label: userMandataire.nomPrenomDenominationMandataire, mandatory:"true"});
}

// // PJ Conjoint

var pj=$p0cmbme.cadre2conjointGroup.cadre2Conjoint.statutConjointCollaborateur ;
if(Value('id').of(pj).contains('PersonneLieeConjointCollaborateurSalarieCollaborateur')) {
    attachment('pjIDConjoint', 'pjIDConjoint', { mandatory:"true"});
}

var pj=$p0cmbme.cadre2conjointGroup.cadre2Conjoint ;
if(pj.conjointCommunauteBiens and pj.conjoint) {
    attachment('pjInformationConjoint', 'pjInformationConjoint', { mandatory:"true"});
}

var pj=$p0cmbme.cadre2conjointGroup.cadre2Conjoint.statutConjointCollaborateur ;
if(Value('id').of(pj).contains('PersonneLieeConjointCollaborateurSalarieCollaborateur') or Value('id').of(pj).contains('PersonneLieeConjointCollaborateurSalarieSalarie')) {
    attachment('pjChoixStatutConjoint', 'pjChoixStatutConjoint', { mandatory:"true"});
}

// // PJ Fondé de pouvoir 1

var user=$p0cmbme.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir

var pj=$p0cmbme.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.personneLieePersonneLieeEtablissementQualite ;
if(Value('id').of(pj).contains('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement') and $p0cmbme.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.personneLieePersonneLieeEtablissement) {
    attachment('pjDNCPersonnePouvoir', 'pjDNCPersonnePouvoir', { label : (user.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? user.personneLieePersonnePhysiqueNomUsagePersonnePouvoir : user.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir) + ' '+user.personneLieePersonnePhysiquePrenom1PersonnePouvoir[0], mandatory:"true"});
    attachment('pjIDPersonnePouvoir', 'pjIDPersonnePouvoir', { label : (user.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? user.personneLieePersonnePhysiqueNomUsagePersonnePouvoir : user.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir) + ' '+user.personneLieePersonnePhysiquePrenom1PersonnePouvoir[0], mandatory:"true"});
}

// // PJ Fondé de pouvoir 2

var userFondePouvoir2=$p0cmbme.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir2

var pj=$p0cmbme.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir2.personneLieePersonneLieeEtablissementQualite2 ;
if(Value('id').of(pj).contains('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement2') and $p0cmbme.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir.autreDeclarationPersonneLiee) {
    attachment('pjDNCPersonnePouvoir2', 'pjDNCPersonnePouvoir2', { label : (userFondePouvoir2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir2 != null ? userFondePouvoir2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir2 : userFondePouvoir2.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir2) + ' '+userFondePouvoir2.personneLieePersonnePhysiquePrenom1PersonnePouvoir2[0], mandatory:"true"});
    attachment('pjIDPersonnePouvoir2', 'pjIDPersonnePouvoir2', { label : (userFondePouvoir2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir2 != null ? userFondePouvoir2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir2 : userFondePouvoir2.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir2) + ' '+userFondePouvoir2.personneLieePersonnePhysiquePrenom1PersonnePouvoir2[0], mandatory:"true"});
}

// // PJ Fondé de pouvoir 3

var userFondePouvoir3=$p0cmbme.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir3

var pj=$p0cmbme.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir3.personneLieePersonneLieeEtablissementQualite3 ;
if(Value('id').of(pj).contains('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement3') and $p0cmbme.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir2.autreDeclarationPersonneLiee2) {
    attachment('pjDNCPersonnePouvoir3', 'pjDNCPersonnePouvoir3', { label : (userFondePouvoir3.personneLieePersonnePhysiqueNomUsagePersonnePouvoir3 != null ? userFondePouvoir3.personneLieePersonnePhysiqueNomUsagePersonnePouvoir3 : userFondePouvoir3.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir3) + ' '+userFondePouvoir3.personneLieePersonnePhysiquePrenom1PersonnePouvoir3[0], mandatory:"true"});
    attachment('pjIDPersonnePouvoir3', 'pjIDPersonnePouvoir3', { label : (userFondePouvoir3.personneLieePersonnePhysiqueNomUsagePersonnePouvoir3 != null ? userFondePouvoir3.personneLieePersonnePhysiqueNomUsagePersonnePouvoir3 : userFondePouvoir3.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir3) + ' '+userFondePouvoir3.personneLieePersonnePhysiquePrenom1PersonnePouvoir3[0], mandatory:"true"});
}

// // PJ mineur émancipé

var pj=$p0cmbme.cadre1IdentiteGroup.cadre1Identite ;
if(pj.personneLieePersonnePhysiqueMineurEmancipe) {
    attachment('pjIDMineurEmancipe', 'pjIDMineurEmancipe', { mandatory:"true"});
}

// // PJ Insaisissabilité

var pj=$p0cmbme.cadre3InformationsComplementairesGroup.cadre3InformationsComplementaires ;
if(pj.cadre3DeclarationInsaisissabilite.entrepriseInsaisissabiliteDeclarationAutresBiens) {
    attachment('pjInsaisissabilite', 'pjInsaisissabilite', { mandatory:"true"});
}

// // PJ Etablissement

var pj=$p0cmbme.cadre4AdresseActiviteGroup.cadre4AdresseActivite.activiteLieu ;
if(Value('id').of(pj).contains('entrepriseAdresseEntrepriseDomicile')) {
    attachment('pjDomicile', 'pjDomicile', { mandatory:"true"});
}

var pj=$p0cmbme.cadre4AdresseActiviteGroup.cadre4AdresseActivite.activiteLieu ;
if(Value('id').of(pj).contains('etablissementDomiciliationOui')) {
    attachment('pjContratDomiciliation', 'pjContratDomiciliation', { mandatory:"true"});
}

var pj=$p0cmbme.cadre4AdresseActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsCreation') or ((Value('id').of(pj).contains('EtablissementOrigineFondsAchat')) and $p0cmbme.cadre4AdresseActiviteGroup.cadre4OrigineFonds.precedentExploitant.fondsArtisanal) or (Value('id').of(pj).contains('EtablissementOrigineFondsCocheAutre'))) {
    attachment('pjEtablissementCreation', 'pjEtablissementCreation', { mandatory:"true"});
}

var pj=$p0cmbme.cadre4AdresseActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsLocationGerance')) {
    attachment('pjLocationGerance', 'pjLocationGerance', { mandatory:"true"});
    attachment('pjLocationGerancePublication', 'pjLocationGerancePublication', { mandatory:"true"});
}


var pj=$p0cmbme.cadre4AdresseActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsGeranceMandat')) {
    attachment('pjGeranceMandat', 'pjGeranceMandat', { mandatory:"true"});
    attachment('pjGeranceMandatPublication', 'pjGeranceMandatPublication', { mandatory:"true"});
}


var pj=$p0cmbme.cadre4AdresseActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds ;
if((Value('id').of(pj).contains('EtablissementOrigineFondsAchat')) and $p0cmbme.cadre4AdresseActiviteGroup.cadre4OrigineFonds.precedentExploitant.cessionFonds) {
    attachment('pjPlanCession', 'pjPlanCession', { mandatory:"true"});
}

var pj=$p0cmbme.cadre4AdresseActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds ;
if((Value('id').of(pj).contains('EtablissementOrigineFondsAchat')) and ($p0cmbme.cadre4AdresseActiviteGroup.cadre4OrigineFonds.precedentExploitant.etablissementJournalAnnoncesLegalesNom != null)) {
    attachment('pjAchat', 'pjAchat', { mandatory:"true"});
   attachment('pjAchatPublication', 'pjAchatPublication', { mandatory:"true"});
}


// // PJ EIRL

var pj=$p0cmbme.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine;
if($p0cmbme.cadreEIRLGroup.cadreEIRL.estEIRL and not Value('id').of(pj.eirlDepot).eq('eirlSansDepot')) {
    attachment('pjDAP', 'pjDAP', { mandatory:"true"});
}

var pj=$p0cmbme.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine;
if(Value('id').of(pj.cadreEIRLInformationsComplementaires.contenuDAP).contains('bienImmo') or Value('id').of(pj.cadreEIRLInformationsComplementairesBis.contenuDAPBis).contains('bienImmo')) {
    attachment('pjDAPActeNotarie', 'pjDAPActeNotarie', { mandatory:"true"});
}

if(Value('id').of(pj.cadreEIRLInformationsComplementaires.contenuDAP).contains('bienCommunIndivis') or Value('id').of(pj.cadreEIRLInformationsComplementairesBis.contenuDAPBis).contains('bienCommunIndivis')) {
    attachment('pjDAPAccordTiers', 'pjDAPAccordTiers', { mandatory:"true"});
}

var dateJour = new Date()
var dateNaissance = $p0cmbme.cadre1IdentiteGroup.cadre1Identite.personneLieePersonnePhysiqueDateNaissance;
var pj=$p0cmbme.cadre1IdentiteGroup.cadre1Identite ;
if($p0cmbme.cadreEIRLGroup.cadreEIRL.estEIRL and not $p0cmbme.cadre1IdentiteGroup.cadre1Identite.personneLieePersonnePhysiqueMineurEmancipe ) {
    attachment('pjAutorisationMineur', 'pjAutorisationMineur', { mandatory:"false"});
}

// PJ JQPA

var pj=$p0cmbme.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification1.jqpaSituation ;
if(Value('id').of(pj).eq('jqpaSituationDiplome')) {
	attachment('pjDiplomes1', 'pjDiplomes1', { mandatory:"true"});
}

var pj=$p0cmbme.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification1.jqpaSituation ;
if(Value('id').of(pj).eq('jqpaSituationExperience')) {
	attachment('pjPiecesUtiles1', 'pjPiecesUtiles1', { mandatory:"true"});
}

var pj=$p0cmbme.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification2.jqpaSituation ;
																									
if(Value('id').of(pj).eq('jqpaSituationDiplome')) {
	attachment('pjDiplomes2', 'pjDiplomes2', { mandatory:"true"});
}

var pj=$p0cmbme.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification2.jqpaSituation ;
if(Value('id').of(pj).eq('jqpaSituationExperience')) {
	attachment('pjPiecesUtiles2', 'pjPiecesUtiles2', { mandatory:"true"});
}

var pj=$p0cmbme.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification3.jqpaSituation ;
if(Value('id').of(pj).eq('jqpaSituationDiplome')) {
	attachment('pjDiplomes3', 'pjDiplomes3', { mandatory:"true"});
}

var pj=$p0cmbme.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification3.jqpaSituation ;
if(Value('id').of(pj).eq('jqpaSituationExperience')) {
	attachment('pjPiecesUtiles3', 'pjPiecesUtiles3', { mandatory:"true"});
}

