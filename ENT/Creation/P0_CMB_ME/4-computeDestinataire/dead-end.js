function getCityName (city) {
	var url = "${directory.baseUrl}/v1/authority/{entityId}";
	var searchResult;
	try {
		searchResult = nash.service.request(url, city)
			.connectionTimeout(10000)
			.receiveTimeout(10000)
			.accept('json')
			.get();
	} catch (e) {
		_log.error("error while searching authority on directory : {} ", e);
		throw e;
	}
	return searchResult.asObject().label;
};
function getAuthority (authorityId, debug) {
	if (debug === undefined) {
		debug = {};
	}


	var url = "${directory.baseUrl}/v1/authority/{entityId}";
	debug.url = url;
	var searchResult;
	try {
		searchResult = nash.service.request(url, authorityId)
			.connectionTimeout(10000)
			.receiveTimeout(10000)
			.accept('json')
			.get();
	} catch (e) {
		_log.error("error while searching authority on directory : {} ", e);
		throw e;
	} finally {
		debug.status = searchResult.status;
	}

	var response = searchResult.asObject();
	_log.info("response is {}", response);

	return response;
};
function getStringAuthorityInfo(authority, libelleIntro){
	_log.info("authority is  {}", authority);
	var result = [];
	
	if(authority.details.profile.url != null && authority.details.profile.url != ""){
		url = "";
		if(!authority.details.profile.url.startsWith('http://')){
			url = "http://" + authority.details.profile.url;
		}else{
			url = authority.details.profile.url;
		}
		result.push("\n<b>Site internet : </b><" + url + ">");
	}	
	if(authority.details.profile.email != null && authority.details.profile.email != ""){
		result.push("\n<b>Email : </b><" + authority.details.profile.email + ">");
	}
	if(authority.details.profile.tel != null && authority.details.profile.tel != ""){
		result.push("\n<b>Tel : </b>" + authority.details.profile.tel);
	}
	if(authority.details.profile.fax != null && authority.details.profile.fax != ""){
		result.push("\n<b>Fax : </b>" + authority.details.profile.fax);
	}	

	return result;
};
function getStringAuthorityAddress(authority, libelleIntro){
	_log.info("authority is  {}", authority);
	var address = "";
		
	if(authority.details.profile.address != null && authority.details.profile.address != ""){
		address = "";
		address += authority.details.profile.address.recipientName + "<BR>";
		address += authority.details.profile.address.addressNameCompl + "<BR>";
		if(authority.details.profile.address.special != ""){
			address += authority.details.profile.address.special + " - ";
		}
		
		address += authority.details.profile.address.postalCode + " - ";
		address += getCityName(authority.details.profile.address.cityNumber);
	}		

	return address;
};
_log.info("dead-end-display.xml");

var authorityCode = _input.dataGroup.destFuncId;
var authority = getAuthority(authorityCode);

var deadEndSave = nash.instance.load("dead-end-display.xml");
var nbDest = 1;
deadEndSave.bind("generated.group.infoCfeGroup",{ infoCfe : getStringAuthorityAddress(authority)});
if(authority.details.profile.observation != null && authority.details.profile.observation != ""){
	deadEndSave.bind("generated.group.infoCfeGroup", {observation : authority.details.profile.observation});
}


var authorityCode2 = _input.dataGroup.destFuncId2;
if(_input.dataGroup.destFuncId2 != null){
	var authority2 = getAuthority(authorityCode2);
	deadEndSave.bind("generated.group.infoCfeGroup2",{ infoCfe2 : getStringAuthorityAddress(authority2)});
	if(authority2.details.profile.observation != null && authority2.details.profile.observation != ""){
		deadEndSave.bind("generated.group.infoCfeGroup2", {observation2 : authority2.details.profile.observation});
	}
	nbDest = 2;
}

var wordingSingulier = "L'envoi de formalités dématérialisées vers les tribunaux d'instance et les tribunaux de grande instance est indisponible.<BR/>Nous vous invitons à imprimer et envoyer le formulaire Cerfa et les pièces jointes que vous trouverez ci-dessous à l'adresse suivante :";
var wordingPluriel = "L'envoi de formalités dématérialisées vers les tribunaux d'instance et les tribunaux de grande instance est indisponible.<BR/>Nous vous invitons à imprimer et envoyer le formulaire Cerfa et les pièces jointes que vous trouverez ci-dessous aux adresses suivantes (un dossier complet par envoi) :";

if(nbDest == 1){
	deadEndSave.bind("generated.group", {infoCfeDescription : wordingSingulier});
}else{
	deadEndSave.bind("generated.group", {infoCfeDescription : wordingPluriel});
}

var paiementDesc = "";
if(_record.listFrais.fraisInformation.flagFormalitePayante.flagPayment == false){
	paiementDesc = "pas de paiement";
}else{
	for( i = 0; i < _record.computeDestinataire.information.fraisDestinations.size() ; i++ ){
		montant = _record.computeDestinataire.information.fraisDestinations[i].fraisPrix;
		var label = _record.computeDestinataire.information.fraisDestinations[i].authorityLabel;
		paiementDesc += "<BR/>Veuillez également joindre un chèque d'un montant total de " + montant + " à votre courrier à destination de : " + label;
	}
	
}
deadEndSave.bind("generated.group",{ paiementDescription : paiementDesc});

var listFiles = [];
var attachementsGroup = _record.attachmentPreprocess.attachmentPreprocess;

var index = 0;
for (var cle in attachementsGroup) {
    var pjGroup = attachementsGroup[cle] ; 
    _log.info("pjGroup length {} content  is {}", pjGroup.length, pjGroup);

    if (null != pjGroup && 0 != pjGroup.length) {
        var filePath = "/2-attachment/generated/attachmentPreprocess." + cle +"-1-postprocess.pdf";
        listFiles.push({id : index , label : filePath});
	}
	index += 1;
}

listFiles.push({id : index , label : "/3-review/generated/generated.record-1-PE_PO_CMB_ME_Creation.pdf"});

deadEndSave.bind("generated", {record : listFiles});
