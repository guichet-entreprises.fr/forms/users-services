function getAuthorityParameters (authorityId, authorityDetails, debug) {
	if (debug === undefined) {
		debug = {};
	}
	if (authorityDetails === undefined) {
		authorityDetails = {};
	}

	var url = "${directory.baseUrl}/v1/authority/{entityId}";
	debug.url = url;
	var searchResult;
	try {
		searchResult = nash.service.request(url, authorityId)
			.connectionTimeout(10000)
			.receiveTimeout(10000)
			.accept('json')
			.get();
	} catch (e) {
		_log.error("error while searching authority on directory : {} ", e);
		throw e;
	} finally {
		debug.status = searchResult.status;
	}

	var response = searchResult.asObject();
	_log.info("response is {}", response);

	var parameters = !response.details.parameters ? null : response.details.parameters;
	_log.info("parameters is {}", parameters);
	authorityDetails.label = response.label
	return parameters;
};
		
_log.info("identify dead end");

var isDeadEnd = false;

var authority = _input.dataGroup.destFuncId;
var authority2 = _input.dataGroup.destFuncId2;
_log.info("authority : {}", authority);

var parameters = getAuthorityParameters(authority);
_log.info("parameters : {}", parameters);
if(parameters != null && parameters['sendAllToAddress'] == "on"){
	_log.info("isDeadEnd set to true");
	isDeadEnd = true;
}

if(authority2 != null){
	var parameters2 = getAuthorityParameters(authority2);
	if(parameters2 != null && parameters2['sendAllToAddress'] == "on"){
		_log.info("isDeadEnd set to true");
		isDeadEnd = true;
	}
}

_log.info("isDeadEnd is : {}", isDeadEnd);
var deadEndSave = nash.instance.load("is-dead-end.xml");
deadEndSave.bind("deadEndPage", { deadEndGroup : { deadEnd : isDeadEnd} });
