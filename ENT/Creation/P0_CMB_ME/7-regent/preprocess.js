//Fichier de mapping des champs Regent pour :
//Version : V2008.11 
//Evenements : 05P,  
//Fichier généré le : 2018-07-16_111210 

function pad(s) { return (s < 10) ? '0' + s : s; }
function sanitize(s){return s.replaceAll("’","'").replaceAll("–","-").replaceAll("­","");}

var regentVersion = "V2008.11";  // (V2008.11, V2016.02)
var eventRegent = $p0cmbme.cadre1IdentiteGroup.cadre1Identite.formaliteEvenementOui ? "05P" : "01P"; 
var authorityType = "CFE"; // (CFE, TDR)
var authorityId = _INPUT_.dataGroup.codeEDI; // (code EDI)

//Ajout du type de la deuxième autorité
var authorityType2 = "TDR"; // (CFE, TDR) 
//Ajout du code de la deuxième autorité
var authorityId2 = _INPUT_.dataGroup.codeEDI2; // (code EDI) 
 
var regEx = new RegExp('[0-9]{5}'); 
var identite = $p0cmbme.cadre1IdentiteGroup.cadre1Identite;
var conjointInfos = $p0cmbme.cadre2conjointGroup.cadre2Conjoint.cadre2InfosConjoint;
var adresse = $p0cmbme.cadre1IdentiteGroup.cadre1AdresseDeclarant;
var adressePro = $p0cmbme.cadre4AdresseActiviteGroup.cadre4AdresseActivite;
var correspondance = $p0cmbme.cadre10RensCompGroup.cadre10RensComp;
var accre = $p0cmbme.cadre3InformationsComplementairesGroup.cadre3InformationsComplementaires;
var conjoint = $p0cmbme.cadre2conjointGroup.cadre2Conjoint;
var insaisissabilite = $p0cmbme.cadre3InformationsComplementairesGroup.cadre3InformationsComplementaires.cadre3DeclarationInsaisissabilite;
var activite = $p0cmbme.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite;
var origine = $p0cmbme.cadre4AdresseActiviteGroup.cadre4OrigineFonds;
var personnePouvoir = $p0cmbme.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir;
var personnePouvoir2 = $p0cmbme.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir2;
var personnePouvoir3 = $p0cmbme.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir3;
var social = $p0cmbme.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale;
var declarationAffectation = $p0cmbme.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine;
var signataire = $p0cmbme.cadre11SignatureGroup.cadre11Signature;
var optionEIRL = $p0cmbme.cadreEIRLGroup.cadreEIRL;
var jqpa = $p0cmbme.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle;
var jqpa1 = $p0cmbme.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification1;
var jqpa2 = $p0cmbme.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification2;
var jqpa3 = $p0cmbme.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification3;



var regentFields = {}; 
regentFields['/REGENT-XML/Emetteur']= "Z1611";

/*******************************************************************************
 * À récuperer depuis directory
 ******************************************************************************/
regentFields['/REGENT-XML/Destinataire']= authorityId;


/*******************************************************************************
 * Complété par destiny
 ******************************************************************************/
regentFields['/REGENT-XML/DateHeureEmission']=null;
regentFields['/REGENT-XML/VersionMessage']=null;
regentFields['/REGENT-XML/VersionNorme']=null;
regentFields['/REGENT-XML/ServiceApplicatif/Specification/NomService']=null;
regentFields['/REGENT-XML/ServiceApplicatif/Specification/VersionService']=null;


/*******************************************************************************
 * Groupe GDF : Groupe données de service Sous groupe IDF : Identification de la
 * formalité
 ******************************************************************************/
// Généré par destiny
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C01']=null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02']=null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C03']= "0";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C04']=null;
if ((($p0cmbme.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA != null
	and not Value('id').of($p0cmbme.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($p0cmbme.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($p0cmbme.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($p0cmbme.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($p0cmbme.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup1.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	or ($p0cmbme.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA != null
	and not Value('id').of($p0cmbme.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($p0cmbme.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($p0cmbme.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($p0cmbme.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($p0cmbme.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup2.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	or ($p0cmbme.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA != null
	and not Value('id').of($p0cmbme.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($p0cmbme.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($p0cmbme.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($p0cmbme.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($p0cmbme.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.nouvelleActiviteGroup3.jqpaDomaineTransport).eq('jqpaTransportAutre')))
	and not $p0cmbme.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.optionCMACCI) 	{
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05']= "M";
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05']= "C";
}	

// quand profil, à compléter si activité artisanale à titre principal 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C06']= "O";



regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.1']= identite.formaliteEvenementOui ? "05P" : "01P";

if(activite.etablissementDateDebutActivite !== null) {
    var dateTemp = new Date(parseInt(activite.etablissementDateDebutActivite.getTimeInMillis()));
    var year = dateTemp.getFullYear();
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
    var date = year + "-" + month + "-" + day ;
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2']= date;
}

/*******************************************************************************
 * Sous groupe DMF : Destinataire de la formalité
 ******************************************************************************/
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[1]']= authorityId;
if (authorityId != "" and authorityId2 != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[2]']= authorityId2;
}

/*******************************************************************************
 * Sous groupe ADF : Adresse de correspondance
 ******************************************************************************/


regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C36']= correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance != null ? correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance : (identite.personneLieePersonnePhysiqueNomUsage != null ? (identite.personneLieePersonnePhysiqueNomUsage + ' ' + identite.personneLieePersonnePhysiquePrenom1[0]) : (identite.personneLieePersonnePhysiqueNomNaissance + ' ' + identite.personneLieePersonnePhysiquePrenom1[0]));;

if (Value('id').of(adresse.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX') and Value('id').of(correspondance.adresseCorrespond).eq('domi')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']=adresse.personneLieePersonnePhysiqueAdresseCommune.getId();
}
else if (not Value('id').of(adresse.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX') and Value('id').of(correspondance.adresseCorrespond).eq('domi')) {
		var idPays = adresse.personneLieePersonnePhysiqueAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']= result;
}
else if (Value('id').of(correspondance.adresseCorrespond).eq('prof')) {
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']=adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseCommune.getId();
}
else if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']=correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune.getId();
}
																	
if ((Value('id').of(correspondance.adresseCorrespond).eq('domi') and adresse.personneLieePersonnePhysiqueAdresserueNumeroVoie != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseNumRue != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.numeroRueAdresseCorrespondance != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.5']= Value('id').of(correspondance.adresseCorrespond).eq('domi') ? adresse.personneLieePersonnePhysiqueAdresserueNumeroVoie :
																			(Value('id').of(correspondance.adresseCorrespond).eq('prof') ? adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseNumRue : 
																			correspondance.adresseCorrespondance.numeroRueAdresseCorrespondance);
	}
	
if (Value('id').of(correspondance.adresseCorrespond).eq('domi') and adresse.personneLieePersonnePhysiqueAdresseIndiceVoie !== null) {
   var monId1 = Value('id').of(adresse.personneLieePersonnePhysiqueAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId1;  
} 
else if (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseIndiceVoieAdresse !== null) {
   var monId2 = Value('id').of(adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseIndiceVoieAdresse)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId2;
}  
else if (correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance !== null) {
   var monId3 = Value('id').of(correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId3;
}

if ((Value('id').of(correspondance.adresseCorrespond).eq('domi') and adresse.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseAdresseDistriutionSpecialeVoie != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.distriutionSpecialeVoieAdresseCorrespondance != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.7']= Value('id').of(correspondance.adresseCorrespond).eq('domi') ? adresse.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie : 
																				(Value('id').of(correspondance.adresseCorrespond).eq('prof') ? adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseAdresseDistriutionSpecialeVoie : 
																				correspondance.adresseCorrespondance.distriutionSpecialeVoieAdresseCorrespondance);
}

if ((Value('id').of(correspondance.adresseCorrespond).eq('domi') and adresse.personneLieePersonnePhysiqueAdresseCodePostal != null and Value('id').of(adresse.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX'))
	or (Value('id').of(correspondance.adresseCorrespond).eq('domi') and not Value('id').of(adresse.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX'))
	or (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseCodePostal != null)) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8']= Value('id').of(correspondance.adresseCorrespond).eq('domi') ? (adresse.personneLieePersonnePhysiqueAdresseCodePostal != null ? adresse.personneLieePersonnePhysiqueAdresseCodePostal : ".") : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('prof') ? adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseCodePostal : 
																			correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal);
}

/*
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.9']= null;
*/

if ((Value('id').of(correspondance.adresseCorrespond).eq('domi') and adresse.personneLieePersonnePhysiquerueAdresseComplementAdresse != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseRueComplementAdresse != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.rueComplementAdresseCorrespondance != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.10']= Value('id').of(correspondance.adresseCorrespond).eq('domi') ? sanitize(adresse.personneLieePersonnePhysiquerueAdresseComplementAdresse.substring(0,38)) : 
																				(Value('id').of(correspondance.adresseCorrespond).eq('prof') ? sanitize(adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseRueComplementAdresse.substring(0,38)) : 
																				sanitize(correspondance.adresseCorrespondance.rueComplementAdresseCorrespondance.substring(0,38)));
}

if (Value('id').of(correspondance.adresseCorrespond).eq('domi') and adresse.personneLieePersonnePhysiqueAdresseTypeVoie !== null) {
   var monId1 = Value('id').of(adresse.personneLieePersonnePhysiqueAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId1;
} 
else if (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseTypeVoieAdresse !== null) {
   var monId2 = Value('id').of(adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseTypeVoieAdresse)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId2;
}  
else if (correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance !== null) {
   var monId3 = Value('id').of(correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId3;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12']= Value('id').of(correspondance.adresseCorrespond).eq('domi') ? sanitize(adresse.personneLieePersonnePhysiqueAdresseNomVoie) : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('prof') ? sanitize(adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseNomVoieAdresse) : 
																			sanitize(correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance));
																			
if 	(Value('id').of(correspondance.adresseCorrespond).eq('domi') and Value('id').of(adresse.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {																		
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= adresse.personneLieePersonnePhysiqueAdresseCommune.getLabel().substring(0, 32);
}
else if (Value('id').of(correspondance.adresseCorrespond).eq('domi') and not Value('id').of(adresse.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= adresse.personneLieeAdresseVille.substring(0, 32);
}
else if (Value('id').of(correspondance.adresseCorrespond).eq('prof')) {
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseCommune.getLabel().substring(0, 32);
}
else if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune.getLabel().substring(0, 32);
}

if (Value('id').of(correspondance.adresseCorrespond).eq('domi') and not Value('id').of(adresse.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.14']= adresse.personneLieePersonnePhysiqueAdressePays.getLabel().substring(0, 38);
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.1[1]']= correspondance.infosSup.formaliteTelephone2.e164.replace('+', '00').replaceAll(" ","");

if (correspondance.infosSup.formaliteTelephone1 != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.1[2]']= correspondance.infosSup.formaliteTelephone1.e164.replace('+', '00').replaceAll(" ","");
}

if (correspondance.infosSup.telecopie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.2']= correspondance.infosSup.telecopie.e164.replace('+', '00').replaceAll(" ","");
}

if (correspondance.infosSup.formaliteCourriel != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.3']= correspondance.infosSup.formaliteCourriel;
}


/*******************************************************************************
 * Signature de la Formalité
 ******************************************************************************/

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.1']= Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? signataire.adresseMandataire.nomPrenomDenominationMandataire : (identite.personneLieePersonnePhysiqueNomUsage != null ? (identite.personneLieePersonnePhysiqueNomUsage + ' ' + identite.personneLieePersonnePhysiquePrenom1[0]) : (identite.personneLieePersonnePhysiqueNomNaissance + ' ' + identite.personneLieePersonnePhysiquePrenom1[0]));

if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.2']= "Mandataire";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.3']= signataire.adresseMandataire.villeAdresseMandataire.getId();

/*
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.4']= null;
*/

if (signataire.adresseMandataire.numeroVoieMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.5']= signataire.adresseMandataire.numeroVoieMandataire;
}

if (signataire.adresseMandataire.indiceVoieMandataire !== null) {
   var monId = Value('id').of(signataire.adresseMandataire.indiceVoieMandataire)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.6']          = monId;
}

if (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.7']= signataire.adresseMandataire.voieDistributionSpecialeMandataire;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.8']= signataire.adresseMandataire.codePostalMandataire;

/*
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.9']= null;
*/

if (signataire.adresseMandataire.voieComplementMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.10']= signataire.adresseMandataire.voieComplementMandataire;
}

if (signataire.adresseMandataire.typeVoieMandataire !== null) {
   var monId = Value('id').of(signataire.adresseMandataire.typeVoieMandataire)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.11']          = monId;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.12']= signataire.adresseMandataire.nomVoieMandataire;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.13']= signataire.adresseMandataire.villeAdresseMandataire.getLabel();

/*
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.14']= signataire.adresseMandataire.paysAdresseMandataire != null ? signataire.adresseMandataire.paysAdresseMandataire : null;
*/
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C41']= signataire.formaliteSignatureLieu.substring(0, 32);

if(signataire.formaliteSignatureDate !== null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C42']= date;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C43']= "ME=I!" + '' + 
(signataire.formaliteNonDiffusionInformation ? "C45=N!" : "C45=0!") 
+''+ (jqpa3.jqpaSituation != null ? "JQPA=3!" : (jqpa2.jqpaSituation != null ? "JQPA=2!" : 
(jqpa1.jqpaSituation != null ? "JQPA=1!" : '')))
+ '' + (insaisissabilite.entrepriseInsaisissabiliteRenonciationDeclarationAutresBiens ? ("P90.1=A!" +''+ insaisissabilite.entrepriseInsaisissabilitePublicationRenonciationDeclarationAutresBiens +''+ "!") : '') 
+ '' + ((insaisissabilite.entrepriseInsaisissabiliteDeclarationAutresBiens and insaisissabilite.entrepriseInsaisissabilitePublicationDeclarationAutresBiens[0].length > 0) ? 
(("P90.2=C1!"+insaisissabilite.entrepriseInsaisissabilitePublicationDeclarationAutresBiens[0]+"!")
+''+ ((insaisissabilite.entrepriseInsaisissabilitePublicationDeclarationAutresBiens.size() > 1) ? ("C2" +''+ insaisissabilite.entrepriseInsaisissabilitePublicationDeclarationAutresBiens[1] +''+ "!") : '')
+''+ ((insaisissabilite.entrepriseInsaisissabilitePublicationDeclarationAutresBiens.size() > 2) ? ("C3" +''+ insaisissabilite.entrepriseInsaisissabilitePublicationDeclarationAutresBiens[2] +''+ "!") : '')) : '')
+ '' + (correspondance.formaliteObservations != null ? sanitize(correspondance.formaliteObservations) : '') ;



/*******************************************************************************
 * Identification Complète de la personne Physique
 ******************************************************************************/
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.1']= Value('id').of(identite.civilite).eq('PersonneLieePersonnePhysiqueCiviliteFeminin') ? "2" : "1";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.2']= identite.personneLieePersonnePhysiqueNomNaissance;

var prenoms=[];
for (i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){
	prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);
}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.3']= prenoms;

if(identite.personneLieePersonnePhysiqueNomUsage != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P02/P02.1']= identite.personneLieePersonnePhysiqueNomUsage;
}

if(identite.personneLieePersonnePhysiquePseudonyme != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P02/P02.2']= identite.personneLieePersonnePhysiquePseudonyme;
}

if(identite.personneLieePersonnePhysiqueDateNaissance !== null) {
    var dateTemp = new Date(parseInt(identite.personneLieePersonnePhysiqueDateNaissance.getTimeInMillis()));
    var year = dateTemp.getFullYear();
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
    var date = year + "-" + month + "-" + day ;
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.1']= date;
}

if (not Value('id').of(identite.personneLieePersonnePhysiqueLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = identite.personneLieePersonnePhysiqueLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.2']= result;
}
else if (Value('id').of(identite.personneLieePersonnePhysiqueLieuNaissancePays).eq('FRXXXXX')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.2'] = identite.personneLieePersonnePhysiqueLieuNaissanceCommune.getId();
}																						 
																					 
if (not Value('id').of(identite.personneLieePersonnePhysiqueLieuNaissancePays).eq('FRXXXXX')) {																					 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.3'] = identite.personneLieePersonnePhysiqueLieuNaissancePays.getLabel().substring(0, 38);
}

if (Value('id').of(identite.personneLieePersonnePhysiqueLieuNaissancePays).eq('FRXXXXX')) {	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.4']= identite.personneLieePersonnePhysiqueLieuNaissanceCommune.getLabel().substring(0, 32);
}


/*******************************************************************************
 * Domicile personnel ou commune de rattachement pour les forains
 ******************************************************************************/
 
if (Value('id').of(adresse.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.3']= adresse.personneLieePersonnePhysiqueAdresseCommune.getId();
}
else if (not Value('id').of(adresse.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {
	var idPays = adresse.personneLieePersonnePhysiqueAdressePays.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.3']= result;
}

if (adresse.personneLieePersonnePhysiqueAdresserueNumeroVoie !=null) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.5']= adresse.personneLieePersonnePhysiqueAdresserueNumeroVoie;
}
if (adresse.personneLieePersonnePhysiqueAdresseIndiceVoie !==null) {
	   var monId = Value('id').of(adresse.personneLieePersonnePhysiqueAdresseIndiceVoie)._eval();
	   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.6']          = monId;
}
if (adresse.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie !=null) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.7']= adresse.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.8']  = adresse.personneLieePersonnePhysiqueAdresseCodePostal != null ? adresse.personneLieePersonnePhysiqueAdresseCodePostal : ".";

if (adresse.personneLieePersonnePhysiquerueAdresseComplementAdresse !=null)  {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.10'] = sanitize(adresse.personneLieePersonnePhysiquerueAdresseComplementAdresse.substring(0,38));
}
if (adresse.personneLieePersonnePhysiqueAdresseTypeVoie !==null) {
   var monId = Value('id').of(adresse.personneLieePersonnePhysiqueAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.11']          = monId;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.12'] = sanitize(adresse.personneLieePersonnePhysiqueAdresseNomVoie);

if(Value('id').of(adresse.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.13'] = adresse.personneLieePersonnePhysiqueAdresseCommune.getLabel().substring(0, 32);
}
else{
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.13'] = adresse.personneLieeAdresseVille.substring(0, 32);
}

if (not Value('id').of(adresse.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')){
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.14']= adresse.personneLieePersonnePhysiqueAdressePays.getLabel().substring(0, 38);
}

// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P05']=null;

if (activite.etablissmentNonSedentariteQualiteNonSedentaire and activite.etablissementEstAmbulantUE){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P06']= "E";
}else if(activite.etablissmentNonSedentariteQualiteNonSedentaire and not activite.etablissementEstAmbulantUE) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P06']= "A";
}


// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.1']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.2']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.3']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.5']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.6']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.7']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.8']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.10']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.11']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.12']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.13']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.14']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.4']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.5']=null;



/*******************************************************************************
 * Ancienne Identification de la personne physique
 ******************************************************************************/
if(identite.voletSocialActiviteExerceeAnterieurementSIREN != null){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P12']= identite.voletSocialActiviteExerceeAnterieurementSIREN.split(' ').join('');
}
/*******************************************************************************
 * Nationalité de la personne physique
 ******************************************************************************/
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/NAP/P21']= identite.personneLieePersonnePhysiqueNationalite.substring(0, 26);


/*******************************************************************************
 * Mineur Emancipé
 ******************************************************************************/
if (identite.personneLieePersonnePhysiqueMineurEmancipe){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/MEP/P22']= "O";
}


/*******************************************************************************
 * Insaisissabilité de la résidence principale
 ******************************************************************************/
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ISP']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ISP/P90']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ISP/P91']=null;


/*******************************************************************************
 * Déclaration d'affectation du patrimoine (si option pour l'EIRL)
 ******************************************************************************/
if (optionEIRL.estEIRL) {
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P70']= Value('id').of(declarationAffectation.eirlStatut).eq('EirlStatutEIRLReprise') ? "R" : "O";
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P71']= declarationAffectation.declarationPatrimoine.eirlDenomination;

if (declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable !== null) {
		   var dateTmp = new Date(parseInt(declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable.getTimeInMillis()));
		   var dateClotureEc = pad(dateTmp.getDate().toString());
		   var month = dateTmp.getMonth() + 1;
		   dateClotureEc = dateClotureEc.concat(pad(month.toString()));
		   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P72']          = dateClotureEc;
		}
	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P73']= declarationAffectation.declarationPatrimoine.objetPartiel ? declarationAffectation.declarationPatrimoine.eirlObjet : $p0cmbme.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.etablissementActivites;

/*
	// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.3']=null;
	// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.5']=null;
	// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.6']=null;
	// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.7']=null;
	// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.8']=null;
	// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.10']=null;
	// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.11']=null;
	// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.12']=null;
	// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.13']=null;
	// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.14']=null;
*/
	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P75']= Value('id').of(declarationAffectation.declarationPatrimoine.eirlChoixDepotRegistre).eq('EirlDepotRCS') ? "1" : "2";
	
if (Value('id').of(declarationAffectation.eirlStatut).eq('EirlStatutEIRLReprise')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P76/P76.1']= declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLDenomination;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P76/P76.2']= Value('id').of(declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLRegistre).eq('EirlPrecedentEIRLRegistreRcs') ? "1" : 
																					(Value('id').of(declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLRegistre).eq('EirlPrecedentEIRLRegistreRm') ? "2" : "3");
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P76/P76.3']= declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLLieuImmatriculation;
if (declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN != null){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P76/P76.4']= declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN.split(' ').join('');
}
}
}


/*******************************************************************************
 * Situation Matrimoniale de la personne Physique
 ******************************************************************************/

if (Value('id').of(conjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieCollaborateur')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P25/P25.2']= conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueNomNaissanceConjointPacse;

prenoms=[];
for (i = 0; i < conjoint.cadre2InfosConjoint.personneLieePersonnePhysiquePrenom1ConjointPacse.size() ; i++ ){
	prenoms.push(conjoint.cadre2InfosConjoint.personneLieePersonnePhysiquePrenom1ConjointPacse[i]);     
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P25/P25.3']= prenoms;

if (conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueNomUsageConjointPacse != null)  {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P25/P25.4']= conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueNomUsageConjointPacse;
}


regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P40']= "O";
if(conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueDateNaissanceConjointPacse !== null) {
	    var dateTemp = new Date(parseInt(conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueDateNaissanceConjointPacse.getTimeInMillis()));
	    var year = dateTemp.getFullYear();
	    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	    var date = year + "-" + month + "-" + day ;
	    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.1'] = date;
}
	
if	(Value('id').of(conjointInfos.personneLieePersonnePhysiquePaysNaissanceConjointPacse).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.2']= conjointInfos.personneLieePersonnePhysiqueLieuNaissanceCommuneConjointPacse.getId();
}
else if (not Value('id').of(conjointInfos.personneLieePersonnePhysiquePaysNaissanceConjointPacse).eq('FRXXXXX')) {
		var idPays = conjointInfos.personneLieePersonnePhysiquePaysNaissanceConjointPacse.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.2']= result;
}	
																						  
if (not Value('id').of(conjoint.cadre2InfosConjoint.personneLieePersonnePhysiquePaysNaissanceConjointPacse).eq('FRXXXXX')) {																					 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.3'] = conjoint.cadre2InfosConjoint.personneLieePersonnePhysiquePaysNaissanceConjointPacse.getLabel();
}

if (Value('id').of(conjoint.cadre2InfosConjoint.personneLieePersonnePhysiquePaysNaissanceConjointPacse).eq('FRXXXXX')){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.4'] = conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueLieuNaissanceCommuneConjointPacse.getLabel();
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P42']= conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueNationaliteConjointPacse;
}
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.3']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.5']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.6']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.7']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.8']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.10']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.11']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.12']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.13']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.14']=null;

if (Value('id').of(conjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieSalarie')){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P45'] = "O";
}

/*******************************************************************************
 * Justification pour le RCS des déclarations relatives à la personne Physique
 ******************************************************************************/
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/JGP/J00']=null;
	


/*******************************************************************************
 * Immatriculation Sécurité Sociale du travailleur non salarié
 ******************************************************************************/
var nirDeclarant = social.voletSocialNumeroSecuriteSociale;

if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A10/A10.1']  = nirDeclarant.substring(0, 13);
}
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A10/A10.2'] = nirDeclarant.substring(13, 15);
}


/*******************************************************************************
 * Situation du travailleur Non Salarié
 ******************************************************************************/

// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A21/A21.2']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A21/A21.3']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A21/A21.4']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A21/A21.5']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A21/A21.6']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A22/A22.3']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A22/A22.4']=null;


regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A23']= "O";


if (social.voletSocialActiviteAutreQueDeclareeStatutOui) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A24/A24.2']= Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutSalarie') ? "1" :
																						(Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutSalarieAgricole') ? "2" : 
																						(Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutRetraitePensionne') ? "8" : "9" ));
if (Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutCocheAutre')) {
	if(social.voletSocialActiviteAutreQueDeclareeStatutAutre != null){
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A24/A24.3']= social.voletSocialActiviteAutreQueDeclareeStatutAutre.substring(0,40);
	}
}
/*
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A24/A24.5']= "France";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A24/A24.6']=null;
*/
}

// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A27']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A28']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A29']=null;

/*******************************************************************************
 * Justification du droit d’exercice
 ******************************************************************************/

if (social.ressortissantHorsUE == true) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/JES/A30/A30.1']= social.voletSocialTitreSejourLieuDelivranceCommune.getId() ;
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/JES/A30/A30.2']= social.voletSocialTitreSejourLieuDelivranceCommune.getLabel();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/JES/A30/A30.3']= social.voletSocialTitreSejourNumero.substring(0,10);
	
	if(social.voletSocialTitreSejourDateExpiration !== null) {
	    var dateTemp = new Date(parseInt(social.voletSocialTitreSejourDateExpiration.getTimeInMillis()));
	    var year = dateTemp.getFullYear();
	    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	    var date = year + "-" + month + "-" + day ;
	    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/JES/A30/A30.4'] = date;
	}
}

/*******************************************************************************
 * Choix de l’organisme d’Assurance maladie du TNS - MSA
 ******************************************************************************/

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/CAS/A42/A42.1']= ".";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/CAS/A42/A42.2']= ".";

// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/CAS/A44']=null;


/*******************************************************************************
 * Situation du Conjoint vis-à-vis de la sécurité Sociale
 ******************************************************************************/

if (Value('id').of(conjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieCollaborateur')) {
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A53/A53.4']= conjointInfos.voletSocialConjointCouvertAssuranceMaladieOui ? "O" : "N";

var nirDeclarant = conjointInfos.voletSocialConjointCollaborateurNumeroSecuriteSociale;

if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A55/A55.1'] = nirDeclarant.substring(0, 13);
}
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A55/A55.2'] = nirDeclarant.substring(13, 15);
}
}

/*******************************************************************************
 * Situation du conjoint Associé du TNS
 ******************************************************************************/
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SAS/A68/A68.2']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SAS/A68/A68.3']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SAS/A68/A68.4']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SAS/A69/A69.1']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SAS/A69/A69.2']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SAS/A69/A69.3']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SAS/A69/A69.4']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SAS/A70']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SAS/A71/A71.6']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SAS/A71/A71.7']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SAS/A71/A71.8']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SAS/A71/A71.10']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SAS/A71/A71.11']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SAS/A71/A71.12']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SAS/A71/A71.13']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SAS/A71/A71.14']=null;


/*******************************************************************************
 * régime Micro-Social du TNS
 ******************************************************************************/
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/MSS/A31/A31.1']= "O";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/MSS/A31/A31.2']= Value('id').of(social.optionMicroSocialSimplifie).eq('VoletSocialOptionMicroSocialVersementMensuel') ? "M" : "T";


/*******************************************************************************
 * Siège de l'entreprise (adresse de l'entreprise individuelle)
 ******************************************************************************/

if (Value('id').of(adressePro.activiteLieu).eq('etablissementDomiciliationOui')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U10']= adressePro.entrepriseLieeNom;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U15'] = adressePro.entrepriseLieeSiren.split(' ').join('');
}

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U16']="O";
}

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.3']= adresse.personneLieePersonnePhysiqueAdresseCommune.getId();
}
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.3']= adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseCommune.getId();
}		
	
if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')  and adresse.personneLieePersonnePhysiqueAdresserueNumeroVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.5']= adresse.personneLieePersonnePhysiqueAdresserueNumeroVoie;
}
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseNumRue != null){	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.5']= adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseNumRue;
}	

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adresse.personneLieePersonnePhysiqueAdresseIndiceVoie !== null) {
   var monId1 = Value('id').of(adresse.personneLieePersonnePhysiqueAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.6']          = monId1;
} else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseIndiceVoieAdresse !== null) {
   var monId2 = Value('id').of(adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseIndiceVoieAdresse)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.6']          = monId2;
}

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')  and adresse.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.7']= adresse.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie;
}
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseAdresseDistriutionSpecialeVoie != null){	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.7']= adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseAdresseDistriutionSpecialeVoie;
}	
	
if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.8']= adresse.personneLieePersonnePhysiqueAdresseCodePostal;
}
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.8']= adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseCodePostal;
}		

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')  and adresse.personneLieePersonnePhysiquerueAdresseComplementAdresse != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.10']= sanitize(adresse.personneLieePersonnePhysiquerueAdresseComplementAdresse.substring(0,38));
}
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseRueComplementAdresse != null){	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.10']= adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseRueComplementAdresse;
}

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adresse.personneLieePersonnePhysiqueAdresseTypeVoie !== null) {
   var monId1 = Value('id').of(adresse.personneLieePersonnePhysiqueAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.11']          = monId1;
} else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseTypeVoieAdresse !== null) {
   var monId2 = Value('id').of(adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseTypeVoieAdresse)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.11']          = monId2;
}

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.12']= sanitize(adresse.personneLieePersonnePhysiqueAdresseNomVoie);
}
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.12']= adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseNomVoieAdresse;
}		

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.13']= adresse.personneLieePersonnePhysiqueAdresseCommune.getLabel().substring(0, 32);
}
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.13']= adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseCommune.getLabel().substring(0, 32);
}	
	

/*******************************************************************************
 * Caractéristiques de l’Entreprise
 ******************************************************************************/

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U21']= sanitize(activite.etablissementActivites);

if (activite.etablissementEffectifSalariePresenceOui) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U22']= activite.cadre5EffectifSalarie.etablissementEffectifSalarieNombre;
}

// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U24']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U27']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U28']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U29']=null;



/*******************************************************************************
 * Contrat d’appui
 ******************************************************************************/
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.1']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.1']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.2']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.2/U40.2.2.3']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.2/U40.2.2.5']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.2/U40.2.2.6']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.2/U40.2.2.7']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.2/U40.2.2.8']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.2/U40.2.2.10']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.2/U40.2.2.11']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.2/U40.2.2.12']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.2/U40.2.2.13']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.2/U40.2.2.14']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.4']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.5']=null;


/*******************************************************************************
 * Attestation de qualification professionnelle
 ******************************************************************************/
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/AQU/U60']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/AQU/U61']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/AQU/U62']=null;


/*******************************************************************************
 * Etablissements situés dans l'UE
 ******************************************************************************/

if (accre.entrepriseAutreEtablissementUE) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1]/U41.1'] = accre.cadre3AutreEtablissementUE1.lieuImmatriculationAutreEtablissementUE;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1]/U41.2'] = accre.cadre3AutreEtablissementUE1.numeroImmatriculationAutreEtablissementUE;

if (accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.paysAdresseAutreEtablissementUE != null) {
	var idPays = accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.paysAdresseAutreEtablissementUE.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1]/U41.3/U41.3.3']= result;
}
if (accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.rueNumeroAdresseAutreEtablissementUE != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1]/U41.3/U41.3.5'] = accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.rueNumeroAdresseAutreEtablissementUE;
}
if (accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.indiceVoieAdresseAutreEtablissementUE != null) {
   var monId1 = Value('id').of(accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.indiceVoieAdresseAutreEtablissementUE)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1]/U41.3/U41.3.6'] = monId1;
}
if (accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.distributionSpecialeAdresseAutreEtablissementUE != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1]/U41.3/U41.3.7'] = accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.distributionSpecialeAdresseAutreEtablissementUE;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1]/U41.3/U41.3.8'] = accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.codePostalAdresseAutreEtablissementUE != null ? accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.codePostalAdresseAutreEtablissementUE : "." ;
if (accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.rueComplementAdresseAutreEtablissementUE != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1]/U41.3/U41.3.10'] = accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.rueComplementAdresseAutreEtablissementUE;
}
if (accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.typeVoieAdresseAutreEtablissementUE != null) {
   var monId1 = Value('id').of(accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.typeVoieAdresseAutreEtablissementUE)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1]/U41.3/U41.3.11'] = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1]/U41.3/U41.3.12'] = accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.nomVoieAdresseAutreEtablissementUE ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1]/U41.3/U41.3.13'] = accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.communeAdresseAutreEtablissementUECommune ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1]/U41.3/U41.3.14'] = accre.cadre3AutreEtablissementUE1.adresseAutreEtablissementUE.paysAdresseAutreEtablissementUE.getLabel();

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1]/U41.4'] = accre.cadre3AutreEtablissementUE1.activiteAutreEtablissementUE;
}


if (accre.cadre3AutreEtablissementUE1.declarationAutreEtablissementUE) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2]/U41.1'] = accre.cadre3AutreEtablissementUE2.lieuImmatriculationAutreEtablissementUE;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2]/U41.2'] = accre.cadre3AutreEtablissementUE2.numeroImmatriculationAutreEtablissementUE;

if (accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.paysAdresseAutreEtablissementUE != null) {
	var idPays = accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.paysAdresseAutreEtablissementUE.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2]/U41.3/U41.3.3']= result;
}
if (accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.rueNumeroAdresseAutreEtablissementUE != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2]/U41.3/U41.3.5'] = accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.rueNumeroAdresseAutreEtablissementUE;
}
if (accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.indiceVoieAdresseAutreEtablissementUE != null) {
   var monId1 = Value('id').of(accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.indiceVoieAdresseAutreEtablissementUE)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2]/U41.3/U41.3.6'] = monId1;
}
if (accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.distributionSpecialeAdresseAutreEtablissementUE != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2]/U41.3/U41.3.7'] = accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.distributionSpecialeAdresseAutreEtablissementUE;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2]/U41.3/U41.3.8'] = accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.codePostalAdresseAutreEtablissementUE != null ? accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.codePostalAdresseAutreEtablissementUE : "." ;
if (accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.rueComplementAdresseAutreEtablissementUE != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2]/U41.3/U41.3.10'] = accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.rueComplementAdresseAutreEtablissementUE;
}
if (accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.typeVoieAdresseAutreEtablissementUE != null) {
   var monId1 = Value('id').of(accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.typeVoieAdresseAutreEtablissementUE)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2]/U41.3/U41.3.11'] = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2]/U41.3/U41.3.12'] = accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.nomVoieAdresseAutreEtablissementUE ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2]/U41.3/U41.3.13'] = accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.communeAdresseAutreEtablissementUECommune ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2]/U41.3/U41.3.14'] = accre.cadre3AutreEtablissementUE2.adresseAutreEtablissementUE.paysAdresseAutreEtablissementUE.getLabel();

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2]/U41.4'] = accre.cadre3AutreEtablissementUE2.activiteAutreEtablissementUE;
}



/*******************************************************************************
 * Régime Fiscal de l’Entreprise
 ******************************************************************************/

 if ($p0cmbme.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine.declarationPatrimoine.objetPartiel or not $p0cmbme.cadreEIRLGroup.cadreEIRL.estEIRL) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U31']= "116";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.1']= "310";
}
 

/*******************************************************************************
 * Option fiscale EIRL
 ******************************************************************************/
if ($p0cmbme.cadreEIRLGroup.cadreEIRL.estEIRL == true) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U70']= "116";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U72/U72.1']= "310";

// option pour le versement liberatoire
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U74']= declarationAffectation.impositionBenefices.regimeFiscalRegimeImpositionBeneficesVersementLiberatoire ? "O" : "N"; 
}	
		
/*******************************************************************************
 * Option pour le Versement libératoire de l’impôt sur le revenu de l'entreprise
 ******************************************************************************/

 if ($p0cmbme.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine.declarationPatrimoine.objetPartiel or not $p0cmbme.cadreEIRLGroup.cadreEIRL.estEIRL) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OVU/U42']= $p0cmbme.cadre9optFiscHorsEIRLGroup.cadre9optFiscHorsEIRL.regimeFiscalRegimeImpositionBeneficesVersementLiberatoireOui ? "O" :"N";
}
else if (not $p0cmbme.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine.declarationPatrimoine.objetPartiel and $p0cmbme.cadreEIRLGroup.cadreEIRL.estEIRL) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OVU/U42']= declarationAffectation.impositionBenefices.regimeFiscalRegimeImpositionBeneficesVersementLiberatoire ? "O" : "N";
}
		

/*******************************************************************************
 * Justification pour le RCS des déclarations relatives à l'entreprise
 ******************************************************************************/
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/JGU/J00']=null;


/*******************************************************************************
 * Justification pour le RM des déclarations relatives à l'entreprise
 ******************************************************************************/
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/JMU/J00']=null;


/*******************************************************************************
 * Identification Complète de l’Etablissement
 ******************************************************************************/
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E01']= "1";

 _log.info("adresse domicile value is {}",adresse.entrepriseAdresseEntrepriseDomicile );
 _log.info("adresse domicile id is {}",Value('id').of(adressePro.activiteLieu));

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.3']= adresse.personneLieePersonnePhysiqueAdresseCommune.getId();
}
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.3']= adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseCommune.getId();
}		
	
if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')  and adresse.personneLieePersonnePhysiqueAdresserueNumeroVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5']= adresse.personneLieePersonnePhysiqueAdresserueNumeroVoie;
}
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseNumRue != null){	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5']= adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseNumRue;
}	

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adresse.personneLieePersonnePhysiqueAdresseIndiceVoie !== null) {
   var monId1 = Value('id').of(adresse.personneLieePersonnePhysiqueAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.6']          = monId1;
} else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseIndiceVoieAdresse !== null) {
   var monId2 = Value('id').of(adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseIndiceVoieAdresse)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.6']          = monId2;
}

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')  and adresse.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.7']= adresse.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie;
}
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseAdresseDistriutionSpecialeVoie != null){	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.7']= adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseAdresseDistriutionSpecialeVoie;
}	
	
if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.8']= adresse.personneLieePersonnePhysiqueAdresseCodePostal;
}
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.8']= adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseCodePostal;
}		

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')  and adresse.personneLieePersonnePhysiquerueAdresseComplementAdresse != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.10']= sanitize(adresse.personneLieePersonnePhysiquerueAdresseComplementAdresse.substring(0,38));
}
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseRueComplementAdresse != null){	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.10']= sanitize(adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseRueComplementAdresse);
}

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adresse.personneLieePersonnePhysiqueAdresseTypeVoie !== null) {
   var monId1 = Value('id').of(adresse.personneLieePersonnePhysiqueAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.11']          = monId1;
} else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseTypeVoieAdresse !== null) {
   var monId2 = Value('id').of(adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseTypeVoieAdresse)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.11']          = monId2;
}

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.12']= sanitize(adresse.personneLieePersonnePhysiqueAdresseNomVoie);
}
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.12']= sanitize(adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseNomVoieAdresse);
}		

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.13']= adresse.personneLieePersonnePhysiqueAdresseCommune.getLabel().substring(0, 32);
}
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')){	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.13']= adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseCommune.getLabel().substring(0, 32);
}	

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E04']="3";

if (activite.etablissementEnseigne != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E05']=sanitize(activite.etablissementEnseigne);
}

if (activite.etablissementNomCommercialProfessionnel) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E09']=sanitize(activite.etablissementNomCommercialProfessionnel);
}		
	
/*******************************************************************************
 * Origine de l’Etablissement
 ******************************************************************************/
if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation') or not Value('id').of(adressePro.activiteLieu).eq('etablissementDomiciliationNon')){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.1']= "1";
}else if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsLocationGerance')){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.1']= "6";
}else if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsGeranceMandat')){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.1']= "F";
}else if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsAchat')){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.1']= "3";
}else if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCocheAutre')){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.1']= "9";
}

if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCocheAutre')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.2']= origine.etablissementOrigineFondsAutre;
}

if(origine.precedentExploitant.etablissementJournalAnnoncesLegalesNom != null){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E22/E22.1'] = origine.precedentExploitant.etablissementJournalAnnoncesLegalesNom;
}

if(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParution !== null) {
    var dateTemp = new Date(parseInt(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParution.getTimeInMillis()));
    var year = dateTemp.getFullYear();
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
    var date = year + "-" + month + "-" + day ;
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E22/E22.2'] = date;
}


/*******************************************************************************
 * Précédent Exploitant de l’Etablissement
 ******************************************************************************/

if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsAchat') or Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsLocationGerance')) { 
if (origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant != null){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.1']= origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant.split(' ').join('');
}
if (Value('id').of(origine.precedentExploitant.typePersonnePrecedentExploitant).eq('typePersonnePrecedentExploitantPP')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.2']= origine.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant;
if (origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.3']=  origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.4']= origine.precedentExploitant.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant;
}	
if (Value('id').of(origine.precedentExploitant.typePersonnePrecedentExploitant).eq('typePersonnePrecedentExploitantPM')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.7']= origine.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant;
}
}


/*******************************************************************************
 * Location Gérance de l’Etablissement ou gérance-mandat
 ******************************************************************************/

if(Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsLocationGerance') or Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsGeranceMandat')){
	
if(origine.geranceMandat.etablissementLoueurMandantDuFondsDebutContrat !== null) {
	var dateTemp = new Date(parseInt(origine.geranceMandat.etablissementLoueurMandantDuFondsDebutContrat.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ;
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E61/E61.1'] = date;
}
if(origine.geranceMandat.etablissementLoueurMandantDuFondsFinContrat !== null) {
	var dateTemp = new Date(parseInt(origine.geranceMandat.etablissementLoueurMandantDuFondsFinContrat.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ;
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E61/E61.2'] = date;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E61/E61.3'] = origine.geranceMandat.etablissementLoueurMandantDuFondsRenouvellementTaciteReconductionOui ? "O" : "N";

if (Value('id').of(origine.geranceMandat.typePersonneLoueurMandant).eq('typePersonneLoueurMandantPP')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E62'] = origine.geranceMandat.entrepriseLieeEntreprisePMNomNaissanceLoueurMandantDuFonds;
if (origine.geranceMandat.entrepriseLieeEntreprisePPNomUsageLoueurMandantDuFonds != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E64'] = origine.geranceMandat.entrepriseLieeEntreprisePPNomUsageLoueurMandantDuFonds;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E65']= origine.geranceMandat.entrepriseLieeEntreprisePPPrenom1LoueurMandantDuFonds;
}
if (Value('id').of(origine.geranceMandat.typePersonneLoueurMandant).eq('typePersonneLoueurMandantPM')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E66']=origine.geranceMandat.entrepriseLieeEntreprisePMDenominationLoueurMandantDuFonds;
}
	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.3'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.communeLoueurMandantDuFonds.getId();
if (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.numRueAdresseLoueurMandantDufonds != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.5'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.numRueAdresseLoueurMandantDufonds; 
}
if (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.indiceVoieLoueurMandantDuFonds != null) {																							
	var monId1 = Value('id').of(origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.indiceVoieLoueurMandantDuFonds)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.6']=monId1;
}
if (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.distributionSpecialeLoueurMandantDuFonds != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.7'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.distributionSpecialeLoueurMandantDuFonds;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.8'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.codePostalLoueurMandantDuFonds;
if (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.rueComplementAdresseLoueurMandantDuFonds != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.10'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.rueComplementAdresseLoueurMandantDuFonds;
}
if (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.typeVoieLoueurMandantDuFonds != null) {			
	var typeVoie = Value('id').of(origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.typeVoieLoueurMandantDuFonds)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.11']=typeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.12'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.nomVoieLoueurMandantDuFonds;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.13'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.communeLoueurMandantDuFonds.getLabel();
}

if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsGeranceMandat')){
	if (origine.geranceMandat.entrepriseLieeSirenGeranceMandat != null){
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E67']=origine.geranceMandat.entrepriseLieeSirenGeranceMandat.split(' ').join('');
	}
	//Récupérer le EntityId du greffe
	var greffeId = $p0cmbme.cadre4AdresseActiviteGroup.cadre4OrigineFonds.geranceMandat.entrepriseLieeGreffeImmatriculation.id ;
	//Appeler Directory pour récupérer l'autorité
	_log.info('l entityId du greffe selectionne est {}', greffeId);
	var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', greffeId) //
	           .connectionTimeout(10000) //
	           .receiveTimeout(10000) //
	           .accept('json') //
	           .get();
	//result
	var receiverInfo = response.asObject();
	var contactInfo = !receiverInfo.details ? null : receiverInfo.details;
	//Récupérer le code EDI
	var codeEDIGreffe = !contactInfo.ediCode ? null : contactInfo.ediCode;
	_log.info('le code edi du greffe selectionne est {}', codeEDIGreffe);
	// À modifier quand Destiny fourni le réferentiel
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E68']=codeEDIGreffe;
}






/*******************************************************************************
 * Activité de l’Etablissement
 ******************************************************************************/

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E70'] = sanitize(activite.etablissementActivites);
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E71'] = sanitize(activite.etablissementActivitePrincipale.substring(0, 140));

// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E76/E76.1']=null;


/*******************************************************************************
 * Compléments sur l’Activité de l’Etablissement
 ******************************************************************************/

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E73/E73.1']= Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonnierePermanente') ? 'P' : 'S';

if (activite.etablissmentNonSedentariteQualiteNonSedentaire and activite.etablissementEstAmbulantUE){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E73/E73.2'] = "E";
}else if(activite.etablissmentNonSedentariteQualiteNonSedentaire and not activite.etablissementEstAmbulantUE) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E73/E73.2'] = "A";
}

if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMagasin') or Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMarche') or Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceDetail')) { 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.1']= "10";
}
else if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceGros')){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.1']= "09";
}
else if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureFabricationProduction')){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.1']= "04";
}
else if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureBatTravauxPublics')){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.1']= "14";
}
else if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCocheAutre')){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.1']= "99";
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.2']= sanitize(activite.etablissementActiviteNatureAutre.substring(0, 50));
}

if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMagasin')) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E77/E77.1']= "05";
}
else if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMarche')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E77/E77.1']= "92";
}
else if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceDetail')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E77/E77.1']= "99";
}

if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceDetail')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E77/E77.2']= "sur internet";
}

if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMagasin')) {
function lpad(str, padString, length) {
  var ch = str;
  while (ch.length < length) {
      ch = padString + ch;
  }
  log.info("lpad : "+ch);
  return ch;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E77/E77.3']= lpad(activite.etablissementActiviteLieuExerciceMagasinSurface.toString(), "0", 5).substring(0,5);
}



/*******************************************************************************
 * SAlariés de l’Etablissement
 ******************************************************************************/

if (activite.etablissementEffectifSalariePresenceOui){
if (activite.cadre5EffectifSalarie.etablissementEffectifSalarieEmbauchePremierSalarieOui) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E83/E83.1']="1";
}	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E84']=activite.cadre5EffectifSalarie.etablissementEffectifSalarieNombre;
if (activite.cadre5EffectifSalarie.etablissementEffectifSalarieApprentis != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E85/E85.8']= activite.cadre5EffectifSalarie.etablissementEffectifSalarieApprentis;
}
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E86']= activite.etablissementEffectifSalariePresenceOui ? "O" : "N";


/*******************************************************************************
 * Identification de la personne liée à l’établissement
 ******************************************************************************/

// 1er fondé de pouvoir
 
if ($p0cmbme.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.personneLieePersonneLieeEtablissement) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E91/E91.1'] = "1";
if(activite.etablissementDateDebutActivite !== null) {
    var dateTemp = new Date(parseInt(activite.etablissementDateDebutActivite.getTimeInMillis()));
    var year = dateTemp.getFullYear();
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
    var date = year + "-" + month + "-" + day ;
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E91/E91.2']= date;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E92/E92.1'] = Value('id').of($p0cmbme.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement') ? "P" : "N";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E93/E93.2'] = personnePouvoir.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
if(personnePouvoir.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E93/E93.4'] = personnePouvoir.personneLieePersonnePhysiqueNomUsagePersonnePouvoir;
}
var prenoms=[];
for (i = 0; i < personnePouvoir.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){
	prenoms.push(personnePouvoir.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);
}    
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E93/E93.3'] = prenoms;

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.3'] = personnePouvoir.cadre6AdressePersonnePouvoir.personneLieeAdresseCommunepersonnePouvoir.getId();
if (personnePouvoir.cadre6AdressePersonnePouvoir.rueNumeroAdressePersonnePouvoir != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.5'] = personnePouvoir.cadre6AdressePersonnePouvoir.rueNumeroAdressePersonnePouvoir;
}
if (personnePouvoir.cadre6AdressePersonnePouvoir.indiceVoiePersonnePouvoir != null) {
   var monId1 = Value('id').of(personnePouvoir.cadre6AdressePersonnePouvoir.indiceVoiePersonnePouvoir)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.6'] = monId1;
}
if (personnePouvoir.cadre6AdressePersonnePouvoir.distriutionSpecialePersonnePouvoir != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.7'] = personnePouvoir.cadre6AdressePersonnePouvoir.distriutionSpecialePersonnePouvoir;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.8'] = personnePouvoir.cadre6AdressePersonnePouvoir.personneLieeAdresseCodePostalPersonnePouvoir;
if (personnePouvoir.cadre6AdressePersonnePouvoir.rueComplementAdressePersonnePouvoir != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.10'] = personnePouvoir.cadre6AdressePersonnePouvoir.rueComplementAdressePersonnePouvoir;
}
if (personnePouvoir.cadre6AdressePersonnePouvoir.typeVoiePersonnePouvoir != null) {
   var monId1 = Value('id').of(personnePouvoir.cadre6AdressePersonnePouvoir.typeVoiePersonnePouvoir)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.11'] = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.12'] = personnePouvoir.cadre6AdressePersonnePouvoir.nomVoiePersonnePouvoir;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.13'] = personnePouvoir.cadre6AdressePersonnePouvoir.personneLieeAdresseCommunepersonnePouvoir.getLabel();

if (Value('id').of($p0cmbme.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement')) {
if(personnePouvoir.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir !== null) {
    var dateTemp = new Date(parseInt(personnePouvoir.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
    var year = dateTemp.getFullYear();
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
    var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E95/E95.1'] = date;
}
if (not Value('id').of(personnePouvoir.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
	var idPays = personnePouvoir.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E95/E95.2'] = result;
}
else if (Value('id').of(personnePouvoir.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E95/E95.2'] = personnePouvoir.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getId();
}	
if (not Value('id').of(personnePouvoir.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E95/E95.3'] = personnePouvoir.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getLabel();
}
if (Value('id').of(personnePouvoir.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E95/E95.4'] = personnePouvoir.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E96']= personnePouvoir.personneLieePersonnePhysiqueNationalitePersonnePouvoir;
}
}

// 2ème fondé de pouvoir

if (personnePouvoir.autreDeclarationPersonneLiee) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E91/E91.1'] = "1";
if(activite.etablissementDateDebutActivite !== null) {
    var dateTemp = new Date(parseInt(activite.etablissementDateDebutActivite.getTimeInMillis()));
    var year = dateTemp.getFullYear();
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
    var date = year + "-" + month + "-" + day ;
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E91/E91.2']= date;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E92/E92.1'] = Value('id').of(personnePouvoir2.personneLieePersonneLieeEtablissementQualite2).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement2') ? "P" : "N";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E93/E93.2'] = personnePouvoir2.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir2;
if(personnePouvoir2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir2 != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E93/E93.4'] = personnePouvoir2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir2;
}
var prenoms=[];
for (i = 0; i < personnePouvoir2.personneLieePersonnePhysiquePrenom1PersonnePouvoir2.size() ; i++ ){
	prenoms.push(personnePouvoir2.personneLieePersonnePhysiquePrenom1PersonnePouvoir2[i]);
}    
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E93/E93.3'] = prenoms;

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.3'] = personnePouvoir2.cadre6AdressePersonnePouvoir2.personneLieeAdresseCommunepersonnePouvoir2.getId();
if (personnePouvoir2.cadre6AdressePersonnePouvoir2.rueNumeroAdressePersonnePouvoir2 != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.5'] = personnePouvoir2.cadre6AdressePersonnePouvoir2.rueNumeroAdressePersonnePouvoir2;
}
if (personnePouvoir2.cadre6AdressePersonnePouvoir2.indiceVoiePersonnePouvoir2 != null) {
   var monId1 = Value('id').of(personnePouvoir2.cadre6AdressePersonnePouvoir2.indiceVoiePersonnePouvoir2)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.6'] = monId1;
}
if (personnePouvoir2.cadre6AdressePersonnePouvoir2.distriutionSpecialePersonnePouvoir2 != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.7'] = personnePouvoir2.cadre6AdressePersonnePouvoir2.distriutionSpecialePersonnePouvoir2;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.8'] = personnePouvoir2.cadre6AdressePersonnePouvoir2.personneLieeAdresseCodePostalPersonnePouvoir2;
if (personnePouvoir2.cadre6AdressePersonnePouvoir2.rueComplementAdressePersonnePouvoir2 != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.10'] = personnePouvoir2.cadre6AdressePersonnePouvoir2.rueComplementAdressePersonnePouvoir2;
}
if (personnePouvoir2.cadre6AdressePersonnePouvoir2.typeVoiePersonnePouvoir2 != null) {
   var monId1 = Value('id').of(personnePouvoir2.cadre6AdressePersonnePouvoir2.typeVoiePersonnePouvoir2)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.11'] = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.12'] = personnePouvoir2.cadre6AdressePersonnePouvoir2.nomVoiePersonnePouvoir2;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.13'] = personnePouvoir2.cadre6AdressePersonnePouvoir2.personneLieeAdresseCommunepersonnePouvoir2.getLabel();

if (Value('id').of(personnePouvoir2.personneLieePersonneLieeEtablissementQualite2).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement2')) {
if(personnePouvoir2.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir2 !== null) {
    var dateTemp = new Date(parseInt(personnePouvoir2.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir2.getTimeInMillis()));
    var year = dateTemp.getFullYear();
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
    var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E95/E95.1'] = date;
}
if (not Value('id').of(personnePouvoir2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir2).eq('FRXXXXX')) {
	var idPays = personnePouvoir2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir2.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E95/E95.2'] = result;
}
else if (Value('id').of(personnePouvoir2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir2).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E95/E95.2'] = personnePouvoir2.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir2.getId();
}	
if (not Value('id').of(personnePouvoir2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir2).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E95/E95.3'] = personnePouvoir2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir2.getLabel();
}
if (Value('id').of(personnePouvoir2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir2).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E95/E95.4'] = personnePouvoir2.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir2.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E96']= personnePouvoir2.personneLieePersonnePhysiqueNationalitePersonnePouvoir2;
}
}

// 3ème fondé de pouvoir

if (personnePouvoir2.autreDeclarationPersonneLiee2) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E91/E91.1'] = "1";
if(activite.etablissementDateDebutActivite !== null) {
    var dateTemp = new Date(parseInt(activite.etablissementDateDebutActivite.getTimeInMillis()));
    var year = dateTemp.getFullYear();
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
    var date = year + "-" + month + "-" + day ;
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E91/E91.2']= date;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E92/E92.1'] = Value('id').of(personnePouvoir3.personneLieePersonneLieeEtablissementQualite3).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement3') ? "P" : "N";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E93/E93.2'] = personnePouvoir3.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir3;
if(personnePouvoir3.personneLieePersonnePhysiqueNomUsagePersonnePouvoir3 != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E93/E93.4'] = personnePouvoir3.personneLieePersonnePhysiqueNomUsagePersonnePouvoir3;
}
var prenoms=[];
for (i = 0; i < personnePouvoir3.personneLieePersonnePhysiquePrenom1PersonnePouvoir3.size() ; i++ ){
	prenoms.push(personnePouvoir3.personneLieePersonnePhysiquePrenom1PersonnePouvoir3[i]);
}    
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E93/E93.3'] = prenoms;

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.3'] = personnePouvoir3.cadre6AdressePersonnePouvoir3.personneLieeAdresseCommunepersonnePouvoir3.getId();
if (personnePouvoir3.cadre6AdressePersonnePouvoir3.rueNumeroAdressePersonnePouvoir3 != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.5'] = personnePouvoir3.cadre6AdressePersonnePouvoir3.rueNumeroAdressePersonnePouvoir3;
}
if (personnePouvoir3.cadre6AdressePersonnePouvoir3.indiceVoiePersonnePouvoir3 != null) {
   var monId1 = Value('id').of(personnePouvoir3.cadre6AdressePersonnePouvoir3.indiceVoiePersonnePouvoir3)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.6'] = monId1;
}
if (personnePouvoir3.cadre6AdressePersonnePouvoir3.distriutionSpecialePersonnePouvoir3 != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.7'] = personnePouvoir3.cadre6AdressePersonnePouvoir3.distriutionSpecialePersonnePouvoir3;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.8'] = personnePouvoir3.cadre6AdressePersonnePouvoir3.personneLieeAdresseCodePostalPersonnePouvoir3;
if (personnePouvoir3.cadre6AdressePersonnePouvoir3.rueComplementAdressePersonnePouvoir3 != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.10'] = personnePouvoir3.cadre6AdressePersonnePouvoir3.rueComplementAdressePersonnePouvoir3;
}
if (personnePouvoir3.cadre6AdressePersonnePouvoir3.typeVoiePersonnePouvoir3 != null) {
   var monId1 = Value('id').of(personnePouvoir3.cadre6AdressePersonnePouvoir3.typeVoiePersonnePouvoir3)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.11'] = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.12'] = personnePouvoir3.cadre6AdressePersonnePouvoir3.nomVoiePersonnePouvoir3;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.13'] = personnePouvoir3.cadre6AdressePersonnePouvoir3.personneLieeAdresseCommunepersonnePouvoir3.getLabel();

if (Value('id').of(personnePouvoir3.personneLieePersonneLieeEtablissementQualite3).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement3')) {
if(personnePouvoir3.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir3 !== null) {
    var dateTemp = new Date(parseInt(personnePouvoir3.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir3.getTimeInMillis()));
    var year = dateTemp.getFullYear();
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
    var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E95/E95.1'] = date;
}
if (not Value('id').of(personnePouvoir3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir3).eq('FRXXXXX')) {
	var idPays = personnePouvoir3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir3.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E95/E95.2'] = result;
}
else if (Value('id').of(personnePouvoir3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir3).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E95/E95.2'] = personnePouvoir3.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir3.getId();
}	
if (not Value('id').of(personnePouvoir3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir3).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E95/E95.3'] = personnePouvoir3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir3.getLabel();
}
if (Value('id').of(personnePouvoir3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir3).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E95/E95.4'] = personnePouvoir3.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir3.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E96']= personnePouvoir3.personneLieePersonnePhysiqueNationalitePersonnePouvoir3;
}
}


/*******************************************************************************
 * Justification pour le RCS des déclarations relatives à l’Etablissement
 ******************************************************************************/
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/JGE/J00']=null;
 
_log.info("U21 value is {}", regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U21']);

_log.info("P12 value is {}", regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P12']);

// Call to the XML-REGENT generation WS 
var response = nash.service.request('${regent.baseUrl}/private/v1/xml-regent/generate/{regentVersion}/{authorityType}/{authorityId}', regentVersion, authorityType, authorityId) 
.dataType('application/json') // 
.accept('json') // 
.param('listTypeEvenement',eventRegent) 
//-->MINE-263 : Permettre à l'équipe FF de voir les problèmes de validation XSD et le regent sur studio
.continueOnError(true) //
.post(JSON.stringify(regentFields)); 

// Record the generated XML-REGENT 
//MOD déplacement du test de la réponse du premier ws
if (response != null && response.status == 200) { 
    var xmlRegentStr = response.asBytes(); 
    nash.record.saveFile("XML_REGENT.xml",xmlRegentStr); 

//debut de l'ajout
 if (authorityId2 != null) {
//MOD extraction du numéro de liasse du premeir regent généré
var numeroLiasse = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02');
numeroLiasse = JSON.parse(numeroLiasse);
_log.info('extracted numero de liasse is {}', numeroLiasse);
 regentFields['/REGENT-XML/Destinataire']= authorityId2;
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05']= "C";
 
 // Filtre regent greffe
var undefined;
// Groupe GCS/ISS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A10/A10.1']  = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A10/A10.2'] = undefined;
// Groupe GCS/SNS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A23'] = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A24/A24.2']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A24/A24.3']= undefined;
// Groupe GCS/JES
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/JES/A30/A30.1']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/JES/A30/A30.2']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/JES/A30/A30.3']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/JES/A30/A30.4'] = undefined;
// Groupe GCS/CAS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/CAS/A42/A42.1']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/CAS/A42/A42.2']= undefined;
// Groupe GCS/SCS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A53/A53.4']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A55/A55.1'] = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A55/A55.2'] = undefined;
// Groupe GCS/MSS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/MSS/A31/A31.1']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/MSS/A31/A31.2']= undefined;
// Groupe RFU
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U31']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.1']= undefined;
// Groupe OFU
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U70']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U72/U72.1']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U74']= undefined; 
// Groupe OVU
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OVU/U42']= undefined;
// Groupe SAE
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E83/E83.1']=undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E84']=undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E85/E85.8']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E86']= undefined;

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS']  = undefined;


 //MOD modification de l'appel pour la génération du deuxième régent afi de prendre e compte le numéro de liasse du premier
 var response2 = nash.service.request('${regent.baseUrl}/private/v1/xml-regent/generate/{regentVersion}/{authorityType}/{authorityId}', regentVersion, authorityType2, authorityId2) 
.dataType('application/json') // 
.accept('json') // 
.param('listTypeEvenement',eventRegent) 
.param('liasseNumber', numeroLiasse.C02) 
.continueOnError(true)
.post(JSON.stringify(regentFields)); 

	//Début de l'ajout
	//MOD modifier reponse en response2
	if (response2 != null && response2.status == 200) { 
		var xmlRegentStr = response2.asBytes(); 
		nash.record.saveFile("XML_REGENT2.xml",xmlRegentStr);
	}else{
		
		var returnedResponse = response2.asObject();
		_log.info("Call ws regent returned errors  {}", returnedResponse);
		
		
		var regent2 = returnedResponse['regent'];
		var errors2 = returnedResponse['errors']; 
		nash.record.saveFile("XML_REGENT2.xml",regent2.getBytes()); 
		nash.record.saveFile("XML_VALIDATION_ERRORS_2.xml",errors2.getBytes()); 
		
		//-->MINE-263 : Permettre à l'équipe FF de voir les problèmes de validation XSD et le regent sur studio
		
		return spec.create({
		id : 'xmlGenerationConfirmation',
		label : "Xml Regent confirmation message",
		groups : [ spec.createGroup({
				id : 'Regent ',
				description : "Une erreur s'est produite lors de la génération du XML Regent.",
				data : [
				spec.createData({
					id: 'regent',
					label: "XML regent généré",
					type: 'Text',
					mandatory: true,
					value: regent2
				}),spec.createData({
					id: 'errors',
					label: "Erreurs de validations xsd",
					type: 'Text',
					mandatory: false,
					value: errors2
				}),spec.createData({
					id: 'blockingField',
					label: "xsd erroné",
					help:"Ce champs sert à arrêter le process du dossier pour que le support puisse le consulter",
					type: 'StringReadOnly',
					mandatory: true
				})]
			})]
		});
	}	
}
	return spec.create({
	id : 'xmlGenerationConfirmation',
	label : "Xml Regent confirmation message",
	groups : [ spec.createGroup({
		id : 'confirmationMessageOk',
		description : "Le fichier XML Regent a été généré et ajouté au dossier.",
		data : []
		}) ]
	});
}else{
	
	var returnedResponse = response.asObject();
	_log.info("Call ws regent returned errors  {}", returnedResponse);
	
	
	var regent = returnedResponse['regent'];
	var errors = returnedResponse['errors']; 
	nash.record.saveFile("XML_REGENT.xml",regent.getBytes()); 
	nash.record.saveFile("XML_VALIDATION_ERRORS.xml",errors.getBytes()); 
	
	
	return spec.create({
	id : 'xmlGenerationConfirmation',
	label : "Xml Regent confirmation message",
	groups : [ spec.createGroup({
		id : 'Regent ',
		description : "Une erreur s'est produite lors de la génération du XML Regent de la première autorité.",
		data : [
				spec.createData({
					id: 'regent',
					label: "XML regent généré",
					type: 'Text',
					mandatory: true,
					value: regent
				}),spec.createData({
					id: 'errors',
					label: "Erreurs de validations xsd",
					type: 'Text',
					mandatory: false,
					value: errors
				}),spec.createData({
					id: 'blockingField',
					label: "xsd erroné",
					help:"Ce champs sert à arrêter le process du dossier pour que le support puisse le consulter",
					type: 'StringReadOnly',
					mandatory: true
			})]
		}) ]
	});
}	