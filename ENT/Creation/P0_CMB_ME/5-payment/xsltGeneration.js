//transform xml regent to data.xml with xslt
var recordUid = nash.record.description().recordUid; 
var proxyResult = _INPUT_.proxyResult;
_log.info("_INPUT_ is {}", _INPUT_);
_log.info("proxyResult is {}", proxyResult);

var nameToCheck = recordUid+"_resume.xml";
_log.info("nameToCheck is {}", nameToCheck);
var checkIfFileExists = false ;
for(var i=0; i<proxyResult.files.length; i++){
var indice = i+1;
_log.info("label is {}", proxyResult.files[i].label);
_log.info("proxyResult {} element is {}", i, proxyResult.files[i]);
	if(proxyResult.files[i].label.indexOf(nameToCheck)!=-1){
		var	fileName = "proxyResult.files-"+indice+"-"+proxyResult.files[i].label;
		checkIfFileExists = true ;
	}
}
if(checkIfFileExists){
nash.xml.transform("/5-payment/generated/"+fileName,"xslt_resume.xslt","/8-xmlTc/generated/infoPayment.xml");
}
