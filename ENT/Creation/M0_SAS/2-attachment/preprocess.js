// PJ Dirigeant personne physique 1

var societe = $m0SAS.cadre1SocieteGroup.cadre1Identite;
var siege = $m0SAS.cadre2AdresseSiegeGroup.cadre2AdresseSiege; 

var dirigeant1 = $m0SAS.dirigeant1.cadreIdentiteDirigeant;
var userDeclarant1;
if (dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant1 = dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant1 = dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant1 = dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  '+ dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant1 = dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  '+ dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

var pj=$m0SAS.cadre11SignatureGroup.cadre11Signature.soussigne;

if((Value('id').of(dirigeant1.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant1.personneLieeQualite).eq('66')
	or Value('id').of(dirigeant1.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant1.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant1.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant1.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant1.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant1.personneLieeQualite).eq('63'))
	or ((Value('id').of(dirigeant1.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant1.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant1.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant1.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant1.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant1.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))
	and not Value('id').of(dirigeant1.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeant1.personneLieeQualite).eq('72')) {
attachment('pjDNCDirigeant1', 'pjDNCDirigeant1',{ label: userDeclarant1, mandatory:"true"}) ;
	
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and (not dirigeant1.autreDirigeant 
	or Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant1'))) {
		attachment('pjIDDirigeant1Signataire', 'pjIDDirigeant1Signataire', { label: userDeclarant1, mandatory:"true"});
	} else if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or (dirigeant1.autreDirigeant
	and not Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant1') 
	and Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal'))) {
		attachment('pjIDDirigeant1NonSignataire', 'pjIDDirigeant1NonSignataire', { label: userDeclarant1, mandatory:"true"});
	}
}

// PJ Dirigeant personne physique 2

var dirigeant2 = $m0SAS.dirigeant2.cadreIdentiteDirigeant;
var userDeclarant2;
if (dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant2 = dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant2 = dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant2 = dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  '+ dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant2 = dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  '+ dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of(dirigeant2.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant2.personneLieeQualite).eq('66')
	or Value('id').of(dirigeant2.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant2.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant2.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant2.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant2.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant2.personneLieeQualite).eq('63'))
	or ((Value('id').of(dirigeant2.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant2.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant2.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant2.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant2.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant2.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))
	and not Value('id').of(dirigeant2.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeant2.personneLieeQualite).eq('72')) {
attachment('pjDNCDirigeant2', 'pjDNCDirigeant2',{ label: userDeclarant2, mandatory:"true"}) ;
	
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant2')) {
		attachment('pjIDDirigeant2Signataire', 'pjIDDirigeant2Signataire', { label: userDeclarant2, mandatory:"true"});
	} else if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or (not Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant2') 
	and Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal'))) {
		attachment('pjIDDirigeant2NonSignataire', 'pjIDDirigeant2NonSignataire', { label: userDeclarant2, mandatory:"true"});
	}
}

//PJ Dirigeant personne physique 3

var dirigeant3 = $m0SAS.dirigeant3.cadreIdentiteDirigeant;
var userDeclarant3;
if (dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant3 = dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant3 = dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant3 = dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  '+ dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant3 = dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  '+ dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of(dirigeant3.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant3.personneLieeQualite).eq('66')
	or Value('id').of(dirigeant3.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant3.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant3.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant3.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant3.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant3.personneLieeQualite).eq('63'))
	or ((Value('id').of(dirigeant3.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant3.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant3.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant3.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant3.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant3.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))
	and not Value('id').of(dirigeant3.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeant3.personneLieeQualite).eq('72')) {
attachment('pjDNCDirigeant3', 'pjDNCDirigeant3',{ label: userDeclarant3, mandatory:"true"}) ;
	
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant3')) {
		attachment('pjIDDirigeant3Signataire', 'pjIDDirigeant3Signataire', { label: userDeclarant3, mandatory:"true"});
	} else if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or (not Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant3') 
	and Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal'))) {
		attachment('pjIDDirigeant3NonSignataire', 'pjIDDirigeant3NonSignataire', { label: userDeclarant3, mandatory:"true"});
	}
}

//PJ Dirigeant personne physique 4

var dirigeant4 = $m0SAS.dirigeant4.cadreIdentiteDirigeant;
var userDeclarant4;
if (dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant4 = dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant4 = dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant4 = dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  '+ dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant4 = dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  '+ dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of(dirigeant4.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant4.personneLieeQualite).eq('66')
	or Value('id').of(dirigeant4.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant4.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant4.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant4.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant4.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant4.personneLieeQualite).eq('63'))
	or ((Value('id').of(dirigeant4.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant4.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant4.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant4.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant4.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant4.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))
	and not Value('id').of(dirigeant4.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeant4.personneLieeQualite).eq('72')) {
attachment('pjDNCDirigeant4', 'pjDNCDirigeant4',{ label: userDeclarant4, mandatory:"true"}) ;
	
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant4')) {
		attachment('pjIDDirigeant4Signataire', 'pjIDDirigeant4Signataire', { label: userDeclarant4, mandatory:"true"});
	} else if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or (not Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant4') 
	and Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal'))) {
		attachment('pjIDDirigeant4NonSignataire', 'pjIDDirigeant4NonSignataire', { label: userDeclarant4, mandatory:"true"});
	}
}

//PJ Dirigeant personne physique 5

var dirigeant5 = $m0SAS.dirigeant5.cadreIdentiteDirigeant;
var userDeclarant5;
if (dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant5 = dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant5 = dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant5 = dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  '+ dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant5 = dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  '+ dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of(dirigeant5.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant5.personneLieeQualite).eq('66')
	or Value('id').of(dirigeant5.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant5.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant5.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant5.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant5.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant5.personneLieeQualite).eq('63'))
	or ((Value('id').of(dirigeant5.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant5.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant5.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant5.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant5.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant5.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))
	and not Value('id').of(dirigeant5.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeant5.personneLieeQualite).eq('72')) {
attachment('pjDNCDirigeant5', 'pjDNCDirigeant5',{ label: userDeclarant5, mandatory:"true"}) ;
	
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant5')) {
		attachment('pjIDDirigeant5Signataire', 'pjIDDirigeant5Signataire', { label: userDeclarant5, mandatory:"true"});
	} else if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or (not Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant5') 
	and Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal'))) {
		attachment('pjIDDirigeant5NonSignataire', 'pjIDDirigeant5NonSignataire', { label: userDeclarant5, mandatory:"true"});
	}
}

//PJ Dirigeant personne physique 6

var dirigeant6 = $m0SAS.dirigeant6.cadreIdentiteDirigeant;
var userDeclarant6;
if (dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant6 = dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant6 = dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant6 = dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  '+ dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant6 = dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  '+ dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of(dirigeant6.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant6.personneLieeQualite).eq('66')
	or Value('id').of(dirigeant6.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant6.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant6.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant6.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant6.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant6.personneLieeQualite).eq('63'))
	or ((Value('id').of(dirigeant6.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant6.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant6.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant6.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant6.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant6.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))
	and not Value('id').of(dirigeant6.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeant6.personneLieeQualite).eq('72')) {
attachment('pjDNCDirigeant6', 'pjDNCDirigeant6',{ label: userDeclarant6, mandatory:"true"}) ;
	
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant6')) {
		attachment('pjIDDirigeant6Signataire', 'pjIDDirigeant6Signataire', { label: userDeclarant6, mandatory:"true"});
	} else if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or (not Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant6') 
	and Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal'))) {
		attachment('pjIDDirigeant6NonSignataire', 'pjIDDirigeant6NonSignataire', { label: userDeclarant6, mandatory:"true"});
	}
}

//PJ Dirigeant personne physique 7

var dirigeant7 = $m0SAS.dirigeant7.cadreIdentiteDirigeant;
var userDeclarant7;
if (dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant7 = dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant7 = dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant7 = dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  '+ dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant7 = dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  '+ dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of(dirigeant7.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant7.personneLieeQualite).eq('66')
	or Value('id').of(dirigeant7.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant7.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant7.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant7.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant7.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant7.personneLieeQualite).eq('63'))
	or ((Value('id').of(dirigeant7.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant7.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant7.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant7.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant7.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant7.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))
	and not Value('id').of(dirigeant7.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeant7.personneLieeQualite).eq('72')) {
attachment('pjDNCDirigeant7', 'pjDNCDirigeant7',{ label: userDeclarant7, mandatory:"true"}) ;
	
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant7')) {
		attachment('pjIDDirigeant7Signataire', 'pjIDDirigeant7Signataire', { label: userDeclarant7, mandatory:"true"});
	} else if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or (not Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant7') 
	and Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal'))) {
		attachment('pjIDDirigeant7NonSignataire', 'pjIDDirigeant7NonSignataire', { label: userDeclarant7, mandatory:"true"});
	}
}

//PJ Dirigeant personne physique 8

var dirigeant8 = $m0SAS.dirigeant8.cadreIdentiteDirigeant;
var userDeclarant8;
if (dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant8 = dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant8 = dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant8 = dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  '+ dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant8 = dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  '+ dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of(dirigeant8.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant8.personneLieeQualite).eq('66')
	or Value('id').of(dirigeant8.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant8.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant8.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant8.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant8.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant8.personneLieeQualite).eq('63'))
	or ((Value('id').of(dirigeant8.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant8.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant8.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant8.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant8.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant8.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))
	and not Value('id').of(dirigeant8.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeant8.personneLieeQualite).eq('72')) {
attachment('pjDNCDirigeant8', 'pjDNCDirigeant8',{ label: userDeclarant8, mandatory:"true"}) ;
	
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant8')) {
		attachment('pjIDDirigeant8Signataire', 'pjIDDirigeant8Signataire', { label: userDeclarant8, mandatory:"true"});
	} else if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or (not Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant8') 
	and Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal'))) {
		attachment('pjIDDirigeant8NonSignataire', 'pjIDDirigeant8NonSignataire', { label: userDeclarant8, mandatory:"true"});
	}
}

//PJ Dirigeant personne physique 9

var dirigeant9 = $m0SAS.dirigeant9.cadreIdentiteDirigeant;
var userDeclarant9;
if (dirigeant9.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant9 = dirigeant9.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant9 = dirigeant9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant9 = dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  '+ dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant9 = dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  '+ dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of(dirigeant9.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant9.personneLieeQualite).eq('66')
	or Value('id').of(dirigeant9.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant9.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant9.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant9.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant9.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant9.personneLieeQualite).eq('63'))
	or ((Value('id').of(dirigeant9.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant9.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant9.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant9.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant9.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant9.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))
	and not Value('id').of(dirigeant9.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeant9.personneLieeQualite).eq('72')) {
attachment('pjDNCDirigeant9', 'pjDNCDirigeant9',{ label: userDeclarant9, mandatory:"true"}) ;
	
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant9')) {
		attachment('pjIDDirigeant9Signataire', 'pjIDDirigeant9Signataire', { label: userDeclarant9, mandatory:"true"});
	} else if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or (not Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant9') 
	and Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal'))) {
		attachment('pjIDDirigeant9NonSignataire', 'pjIDDirigeant9NonSignataire', { label: userDeclarant9, mandatory:"true"});
	}
}

//PJ Dirigeant personne physique 10

var dirigeant10 = $m0SAS.dirigeant10.cadreIdentiteDirigeant;
var userDeclarant10;
if (dirigeant10.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant10 = dirigeant10.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant10 = dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant10 = dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  '+ dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant10 = dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  '+ dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of(dirigeant10.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant10.personneLieeQualite).eq('66')
	or Value('id').of(dirigeant10.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant10.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant10.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant10.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant10.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant10.personneLieeQualite).eq('63'))
	or ((Value('id').of(dirigeant10.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant10.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant10.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant10.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant10.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant10.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))
	and not Value('id').of(dirigeant10.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeant10.personneLieeQualite).eq('72')) {
attachment('pjDNCDirigeant10', 'pjDNCDirigeant10',{ label: userDeclarant10, mandatory:"true"}) ;
	
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant10')) {
		attachment('pjIDDirigeant10Signataire', 'pjIDDirigeant10Signataire', { label: userDeclarant10, mandatory:"true"});
	} else if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or (not Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant10') 
	and Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal'))) {
		attachment('pjIDDirigeant10NonSignataire', 'pjIDDirigeant10NonSignataire', { label: userDeclarant10, mandatory:"true"});
	}
}

//PJ Dirigeant personne physique 11

var dirigeant11 = $m0SAS.dirigeant11.cadreIdentiteDirigeant;
var userDeclarant11;
if (dirigeant11.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant11 = dirigeant11.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant11 = dirigeant11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant11 = dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  '+ dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant11 = dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  '+ dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of(dirigeant11.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant11.personneLieeQualite).eq('66')
	or Value('id').of(dirigeant11.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant11.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant11.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant11.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant11.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant11.personneLieeQualite).eq('63'))
	or ((Value('id').of(dirigeant11.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant11.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant11.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant11.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant11.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant11.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))
	and not Value('id').of(dirigeant11.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeant11.personneLieeQualite).eq('72')) {
attachment('pjDNCDirigeant11', 'pjDNCDirigeant11',{ label: userDeclarant11, mandatory:"true"}) ;
	
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant11')) {
		attachment('pjIDDirigeant11Signataire', 'pjIDDirigeant11Signataire', { label: userDeclarant11, mandatory:"true"});
	} else if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or (not Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant11') 
	and Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal'))) {
		attachment('pjIDDirigeant11NonSignataire', 'pjIDDirigeant11NonSignataire', { label: userDeclarant11, mandatory:"true"});
	}
}

//PJ Dirigeant personne physique 12

var dirigeant12 = $m0SAS.dirigeant12.cadreIdentiteDirigeant;
var userDeclarant12;
if (dirigeant12.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant12 = dirigeant12.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant12 = dirigeant12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant12 = dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  '+ dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant12 = dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  '+ dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of(dirigeant12.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant12.personneLieeQualite).eq('66')
	or Value('id').of(dirigeant12.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant12.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant12.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant12.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant12.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant12.personneLieeQualite).eq('63'))
	or ((Value('id').of(dirigeant12.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant12.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant12.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant12.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant12.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant12.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))
	and not Value('id').of(dirigeant12.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeant12.personneLieeQualite).eq('72')) {
attachment('pjDNCDirigeant12', 'pjDNCDirigeant12',{ label: userDeclarant12, mandatory:"true"}) ;
	
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant12')) {
		attachment('pjIDDirigeant12Signataire', 'pjIDDirigeant12Signataire', { label: userDeclarant12, mandatory:"true"});
	} else if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or (not Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant12') 
	and Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal'))) {
		attachment('pjIDDirigeant12NonSignataire', 'pjIDDirigeant12NonSignataire', { label: userDeclarant12, mandatory:"true"});
	}
}

//PJ Dirigeant personne physique 13

var dirigeant13 = $m0SAS.dirigeant13.cadreIdentiteDirigeant;
var userDeclarant13;
if (dirigeant13.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant13 = dirigeant13.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeant13.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant13.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant13 = dirigeant13.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ dirigeant13.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant13 = dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  '+ dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant13 = dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  '+ dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of(dirigeant13.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant13.personneLieeQualite).eq('66')
	or Value('id').of(dirigeant13.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant13.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant13.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant13.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant13.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant13.personneLieeQualite).eq('63'))
	or ((Value('id').of(dirigeant13.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant13.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant13.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant13.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant13.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant13.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))
	and not Value('id').of(dirigeant13.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeant13.personneLieeQualite).eq('72')) {
attachment('pjDNCDirigeant13', 'pjDNCDirigeant13',{ label: userDeclarant13, mandatory:"true"}) ;
	
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant13')) {
		attachment('pjIDDirigeant13Signataire', 'pjIDDirigeant13Signataire', { label: userDeclarant13, mandatory:"true"});
	} else if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or (not Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant13') 
	and Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal'))) {
		attachment('pjIDDirigeant13NonSignataire', 'pjIDDirigeant13NonSignataire', { label: userDeclarant13, mandatory:"true"});
	}
}

//PJ Dirigeant personne physique 14

var dirigeant14 = $m0SAS.dirigeant14.cadreIdentiteDirigeant;
var userDeclarant14;
if (dirigeant14.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant14 = dirigeant14.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeant14.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant14.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant14 = dirigeant14.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ dirigeant14.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant14 = dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  '+ dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant14 = dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  '+ dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of(dirigeant14.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant14.personneLieeQualite).eq('66')
	or Value('id').of(dirigeant14.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant14.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant14.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant14.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant14.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant14.personneLieeQualite).eq('63'))
	or ((Value('id').of(dirigeant14.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant14.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant14.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant14.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant14.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant14.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))
	and not Value('id').of(dirigeant14.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeant14.personneLieeQualite).eq('72')) {
attachment('pjDNCDirigeant14', 'pjDNCDirigeant14',{ label: userDeclarant14, mandatory:"true"}) ;
	
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant14')) {
		attachment('pjIDDirigeant14Signataire', 'pjIDDirigeant14Signataire', { label: userDeclarant14, mandatory:"true"});
	} else if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or (not Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant14') 
	and Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal'))) {
		attachment('pjIDDirigeant14NonSignataire', 'pjIDDirigeant14NonSignataire', { label: userDeclarant14, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 1 

var userMorale1= dirigeant1.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of(dirigeant1.personnaliteJuridique).eq('personneMorale') 
	and not Value('id').of(dirigeant1.personneLieeQualite).eq('71') 
	and not Value('id').of(dirigeant1.personneLieeQualite).eq('72'))	{
    attachment('pjExtraitImmatriculationRCS1', 'pjExtraitImmatriculationRCS1',{ label: userMorale1, mandatory:"true"}) ;

	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and (not dirigeant1.autreDirigeant 
	or Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant1'))) {
		attachment('pjPersonneMoraleSignataire1', 'pjPersonneMoraleSignataire1', { label: userMorale1, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 2 

var userMorale2= dirigeant2.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of(dirigeant2.personnaliteJuridique).eq('personneMorale') 
	and not Value('id').of(dirigeant2.personneLieeQualite).eq('71') 
	and not Value('id').of(dirigeant2.personneLieeQualite).eq('72'))	{
    attachment('pjExtraitImmatriculationRCS2', 'pjExtraitImmatriculationRCS2',{ label: userMorale2, mandatory:"true"}) ;

	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant2')) {
		attachment('pjPersonneMoraleSignataire2', 'pjPersonneMoraleSignataire2', { label: userMorale2, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 3 

var userMorale3= dirigeant3.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of(dirigeant3.personnaliteJuridique).eq('personneMorale') 
	and not Value('id').of(dirigeant3.personneLieeQualite).eq('71') 
	and not Value('id').of(dirigeant3.personneLieeQualite).eq('72'))	{
    attachment('pjExtraitImmatriculationRCS3', 'pjExtraitImmatriculationRCS3',{ label: userMorale3, mandatory:"true"}) ;

	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant3')) {
		attachment('pjPersonneMoraleSignataire3', 'pjPersonneMoraleSignataire3', { label: userMorale3, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 4 

var userMorale4= dirigeant4.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of(dirigeant4.personnaliteJuridique).eq('personneMorale') 
	and not Value('id').of(dirigeant4.personneLieeQualite).eq('71') 
	and not Value('id').of(dirigeant4.personneLieeQualite).eq('72'))	{
    attachment('pjExtraitImmatriculationRCS4', 'pjExtraitImmatriculationRCS4',{ label: userMorale4, mandatory:"true"}) ;

	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant4')) {
		attachment('pjPersonneMoraleSignataire4', 'pjPersonneMoraleSignataire4', { label: userMorale4, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 5 

var userMorale5= dirigeant5.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of(dirigeant5.personnaliteJuridique).eq('personneMorale') 
	and not Value('id').of(dirigeant5.personneLieeQualite).eq('71') 
	and not Value('id').of(dirigeant5.personneLieeQualite).eq('72'))	{
    attachment('pjExtraitImmatriculationRCS5', 'pjExtraitImmatriculationRCS5',{ label: userMorale5, mandatory:"true"}) ;

	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant5')) {
		attachment('pjPersonneMoraleSignataire5', 'pjPersonneMoraleSignataire5', { label: userMorale5, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 6 

var userMorale6= dirigeant6.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of(dirigeant6.personnaliteJuridique).eq('personneMorale') 
	and not Value('id').of(dirigeant6.personneLieeQualite).eq('71') 
	and not Value('id').of(dirigeant6.personneLieeQualite).eq('72'))	{
    attachment('pjExtraitImmatriculationRCS6', 'pjExtraitImmatriculationRCS6',{ label: userMorale6, mandatory:"true"}) ;

	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant6')) {
		attachment('pjPersonneMoraleSignataire6', 'pjPersonneMoraleSignataire6', { label: userMorale6, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 7 

var userMorale7= dirigeant7.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of(dirigeant7.personnaliteJuridique).eq('personneMorale') 
	and not Value('id').of(dirigeant7.personneLieeQualite).eq('71') 
	and not Value('id').of(dirigeant7.personneLieeQualite).eq('72'))	{
    attachment('pjExtraitImmatriculationRCS7', 'pjExtraitImmatriculationRCS7',{ label: userMorale7, mandatory:"true"}) ;

	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant7')) {
		attachment('pjPersonneMoraleSignataire7', 'pjPersonneMoraleSignataire7', { label: userMorale7, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 8 

var userMorale8= dirigeant8.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of(dirigeant8.personnaliteJuridique).eq('personneMorale') 
	and not Value('id').of(dirigeant8.personneLieeQualite).eq('71') 
	and not Value('id').of(dirigeant8.personneLieeQualite).eq('72'))	{
    attachment('pjExtraitImmatriculationRCS8', 'pjExtraitImmatriculationRCS8',{ label: userMorale8, mandatory:"true"}) ;

	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant8')) {
		attachment('pjPersonneMoraleSignataire8', 'pjPersonneMoraleSignataire8', { label: userMorale8, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 9 

var userMorale9= dirigeant9.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of(dirigeant9.personnaliteJuridique).eq('personneMorale') 
	and not Value('id').of(dirigeant9.personneLieeQualite).eq('71') 
	and not Value('id').of(dirigeant9.personneLieeQualite).eq('72'))	{
    attachment('pjExtraitImmatriculationRCS9', 'pjExtraitImmatriculationRCS9',{ label: userMorale9, mandatory:"true"}) ;

	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant9')) {
		attachment('pjPersonneMoraleSignataire9', 'pjPersonneMoraleSignataire9', { label: userMorale9, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 10 

var userMorale10= dirigeant10.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of(dirigeant10.personnaliteJuridique).eq('personneMorale') 
	and not Value('id').of(dirigeant10.personneLieeQualite).eq('71') 
	and not Value('id').of(dirigeant10.personneLieeQualite).eq('72'))	{
    attachment('pjExtraitImmatriculationRCS10', 'pjExtraitImmatriculationRCS10',{ label: userMorale10, mandatory:"true"}) ;

	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant10')) {
		attachment('pjPersonneMoraleSignataire10', 'pjPersonneMoraleSignataire10', { label: userMorale10, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 11 

var userMorale11= dirigeant11.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of(dirigeant11.personnaliteJuridique).eq('personneMorale') 
	and not Value('id').of(dirigeant11.personneLieeQualite).eq('71') 
	and not Value('id').of(dirigeant11.personneLieeQualite).eq('72'))	{
    attachment('pjExtraitImmatriculationRCS11', 'pjExtraitImmatriculationRCS11',{ label: userMorale11, mandatory:"true"}) ;

	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant11')) {
		attachment('pjPersonneMoraleSignataire11', 'pjPersonneMoraleSignataire11', { label: userMorale11, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 12 

var userMorale12= dirigeant12.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of(dirigeant12.personnaliteJuridique).eq('personneMorale') 
	and not Value('id').of(dirigeant12.personneLieeQualite).eq('71') 
	and not Value('id').of(dirigeant12.personneLieeQualite).eq('72'))	{
    attachment('pjExtraitImmatriculationRCS12', 'pjExtraitImmatriculationRCS12',{ label: userMorale12, mandatory:"true"}) ;

	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant12')) {
		attachment('pjPersonneMoraleSignataire12', 'pjPersonneMoraleSignataire12', { label: userMorale12, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 13 

var userMorale13= dirigeant13.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of(dirigeant13.personnaliteJuridique).eq('personneMorale') 
	and not Value('id').of(dirigeant13.personneLieeQualite).eq('71') 
	and not Value('id').of(dirigeant13.personneLieeQualite).eq('72'))	{
    attachment('pjExtraitImmatriculationRCS13', 'pjExtraitImmatriculationRCS13',{ label: userMorale13, mandatory:"true"}) ;

	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant13')) {
		attachment('pjPersonneMoraleSignataire13', 'pjPersonneMoraleSignataire13', { label: userMorale13, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 14 

var userMorale14= dirigeant14.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of(dirigeant14.personnaliteJuridique).eq('personneMorale') 
	and not Value('id').of(dirigeant14.personneLieeQualite).eq('71') 
	and not Value('id').of(dirigeant14.personneLieeQualite).eq('72'))	{
    attachment('pjExtraitImmatriculationRCS14', 'pjExtraitImmatriculationRCS14',{ label: userMorale14, mandatory:"true"}) ;

	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant14')) {
		attachment('pjPersonneMoraleSignataire14', 'pjPersonneMoraleSignataire14', { label: userMorale14, mandatory:"true"});
	}
}

// Nomination dirigeants

if(not dirigeant1.statutPresenceDirigeant 
	or (dirigeant1.autreDirigeant and not dirigeant2.statutPresenceDirigeant) 
	or (dirigeant2.autreDirigeant and not dirigeant3.statutPresenceDirigeant) 
	or (dirigeant3.autreDirigeant and not dirigeant4.statutPresenceDirigeant) 
	or (dirigeant4.autreDirigeant and not dirigeant5.statutPresenceDirigeant) 
	or (dirigeant5.autreDirigeant and not dirigeant6.statutPresenceDirigeant) 
	or (dirigeant6.autreDirigeant and not dirigeant7.statutPresenceDirigeant) 
	or (dirigeant7.autreDirigeant and not dirigeant8.statutPresenceDirigeant) 
	or (dirigeant8.autreDirigeant and not dirigeant9.statutPresenceDirigeant) 
	or (dirigeant9.autreDirigeant and not dirigeant10.statutPresenceDirigeant) 
	or (dirigeant10.autreDirigeant and not dirigeant11.statutPresenceDirigeant)
	or (dirigeant11.autreDirigeant and not dirigeant12.statutPresenceDirigeant) 
	or (dirigeant12.autreDirigeant and not dirigeant13.statutPresenceDirigeant) 
	or (dirigeant13.autreDirigeant and not dirigeant14.statutPresenceDirigeant)) {
    attachment('pjNominationDirigeant', 'pjNominationDirigeant', { mandatory:"true"});
}

// PJ Représentant Permanent 1

var userRepresentant1;
if (dirigeant1.civiliteRepresentant.personneLieePPNomUsage != null) {
    var userRepresentant1 = dirigeant1.civiliteRepresentant.personneLieePPNomUsage + '  ' + dirigeant1.civiliteRepresentant.personneLieePPPrenom[0] ;
} else {
    var userRepresentant1 = dirigeant1.civiliteRepresentant.personneLieePPNomNaissance + '  '+ dirigeant1.civiliteRepresentant.personneLieePPPrenom[0] ;
}

if(dirigeant1.civiliteRepresentant.personneLieePPNomNaissance != null){
	attachment('pjDNCRepresentant1', 'pjDNCRepresentant1',{ label: userRepresentant1, mandatory:"true"}) ;
    attachment('pjIDRepresentant1NonSignataire', 'pjIDRepresentant1NonSignataire', { label: userRepresentant1, mandatory:"true"});
}

// PJ Représentant Permanent 2

var userRepresentant2;
if (dirigeant2.civiliteRepresentant.personneLieePPNomUsage != null) {
    var userRepresentant2 = dirigeant2.civiliteRepresentant.personneLieePPNomUsage + '  ' + dirigeant2.civiliteRepresentant.personneLieePPPrenom[0] ;
} else {
    var userRepresentant2 = dirigeant2.civiliteRepresentant.personneLieePPNomNaissance + '  '+ dirigeant2.civiliteRepresentant.personneLieePPPrenom[0] ;
}

if(dirigeant2.civiliteRepresentant.personneLieePPNomNaissance != null){
	attachment('pjDNCRepresentant2', 'pjDNCRepresentant2',{ label: userRepresentant2, mandatory:"true"}) ;
    attachment('pjIDRepresentant2NonSignataire', 'pjIDRepresentant2NonSignataire', { label: userRepresentant2, mandatory:"true"});
}

// PJ Représentant Permanent 3

var userRepresentant3;
if (dirigeant3.civiliteRepresentant.personneLieePPNomUsage != null) {
    var userRepresentant3 = dirigeant3.civiliteRepresentant.personneLieePPNomUsage + '  ' + dirigeant3.civiliteRepresentant.personneLieePPPrenom[0] ;
} else {
    var userRepresentant3 = dirigeant3.civiliteRepresentant.personneLieePPNomNaissance + '  '+ dirigeant3.civiliteRepresentant.personneLieePPPrenom[0] ;
}

if(dirigeant3.civiliteRepresentant.personneLieePPNomNaissance != null){
	attachment('pjDNCRepresentant3', 'pjDNCRepresentant3',{ label: userRepresentant3, mandatory:"true"}) ;
    attachment('pjIDRepresentant3NonSignataire', 'pjIDRepresentant3NonSignataire', { label: userRepresentant3, mandatory:"true"});
}

// PJ Représentant Permanent 4

var userRepresentant4;
if (dirigeant4.civiliteRepresentant.personneLieePPNomUsage != null) {
    var userRepresentant4 = dirigeant4.civiliteRepresentant.personneLieePPNomUsage + '  ' + dirigeant4.civiliteRepresentant.personneLieePPPrenom[0] ;
} else {
    var userRepresentant4 = dirigeant4.civiliteRepresentant.personneLieePPNomNaissance + '  '+ dirigeant4.civiliteRepresentant.personneLieePPPrenom[0] ;
}

if(dirigeant4.civiliteRepresentant.personneLieePPNomNaissance != null){
	attachment('pjDNCRepresentant4', 'pjDNCRepresentant4',{ label: userRepresentant4, mandatory:"true"}) ;
    attachment('pjIDRepresentant4NonSignataire', 'pjIDRepresentant4NonSignataire', { label: userRepresentant4, mandatory:"true"});
}

// PJ Représentant Permanent 5

var userRepresentant5;
if (dirigeant5.civiliteRepresentant.personneLieePPNomUsage != null) {
    var userRepresentant5 = dirigeant5.civiliteRepresentant.personneLieePPNomUsage + '  ' + dirigeant5.civiliteRepresentant.personneLieePPPrenom[0] ;
} else {
    var userRepresentant5 = dirigeant5.civiliteRepresentant.personneLieePPNomNaissance + '  '+ dirigeant5.civiliteRepresentant.personneLieePPPrenom[0] ;
}

if(dirigeant5.civiliteRepresentant.personneLieePPNomNaissance != null){
	attachment('pjDNCRepresentant5', 'pjDNCRepresentant5',{ label: userRepresentant5, mandatory:"true"}) ;
    attachment('pjIDRepresentant5NonSignataire', 'pjIDRepresentant5NonSignataire', { label: userRepresentant5, mandatory:"true"});
}

// PJ Représentant Permanent 6

var userRepresentant6;
if (dirigeant6.civiliteRepresentant.personneLieePPNomUsage != null) {
    var userRepresentant6 = dirigeant6.civiliteRepresentant.personneLieePPNomUsage + '  ' + dirigeant6.civiliteRepresentant.personneLieePPPrenom[0] ;
} else {
    var userRepresentant6 = dirigeant6.civiliteRepresentant.personneLieePPNomNaissance + '  '+ dirigeant6.civiliteRepresentant.personneLieePPPrenom[0] ;
}

if(dirigeant6.civiliteRepresentant.personneLieePPNomNaissance != null){
	attachment('pjDNCRepresentant6', 'pjDNCRepresentant6',{ label: userRepresentant6, mandatory:"true"}) ;
    attachment('pjIDRepresentant6NonSignataire', 'pjIDRepresentant6NonSignataire', { label: userRepresentant6, mandatory:"true"});
}

// PJ Représentant Permanent 7

var userRepresentant7;
if (dirigeant7.civiliteRepresentant.personneLieePPNomUsage != null) {
    var userRepresentant7 = dirigeant7.civiliteRepresentant.personneLieePPNomUsage + '  ' + dirigeant7.civiliteRepresentant.personneLieePPPrenom[0] ;
} else {
    var userRepresentant7 = dirigeant7.civiliteRepresentant.personneLieePPNomNaissance + '  '+ dirigeant7.civiliteRepresentant.personneLieePPPrenom[0] ;
}

if(dirigeant7.civiliteRepresentant.personneLieePPNomNaissance != null){
	attachment('pjDNCRepresentant7', 'pjDNCRepresentant7',{ label: userRepresentant7, mandatory:"true"}) ;
    attachment('pjIDRepresentant7NonSignataire', 'pjIDRepresentant7NonSignataire', { label: userRepresentant7, mandatory:"true"});
}

// PJ Représentant Permanent 8

var userRepresentant8;
if (dirigeant8.civiliteRepresentant.personneLieePPNomUsage != null) {
    var userRepresentant8 = dirigeant8.civiliteRepresentant.personneLieePPNomUsage + '  ' + dirigeant8.civiliteRepresentant.personneLieePPPrenom[0] ;
} else {
    var userRepresentant8 = dirigeant8.civiliteRepresentant.personneLieePPNomNaissance + '  '+ dirigeant8.civiliteRepresentant.personneLieePPPrenom[0] ;
}

if(dirigeant8.civiliteRepresentant.personneLieePPNomNaissance != null){
	attachment('pjDNCRepresentant8', 'pjDNCRepresentant8',{ label: userRepresentant8, mandatory:"true"}) ;
    attachment('pjIDRepresentant8NonSignataire', 'pjIDRepresentant8NonSignataire', { label: userRepresentant8, mandatory:"true"});
}

// PJ Représentant Permanent 9

var userRepresentant9;
if (dirigeant9.civiliteRepresentant.personneLieePPNomUsage != null) {
    var userRepresentant9 = dirigeant9.civiliteRepresentant.personneLieePPNomUsage + '  ' + dirigeant9.civiliteRepresentant.personneLieePPPrenom[0] ;
} else {
    var userRepresentant9 = dirigeant9.civiliteRepresentant.personneLieePPNomNaissance + '  '+ dirigeant9.civiliteRepresentant.personneLieePPPrenom[0] ;
}

if(dirigeant9.civiliteRepresentant.personneLieePPNomNaissance != null){
	attachment('pjDNCRepresentant9', 'pjDNCRepresentant9',{ label: userRepresentant9, mandatory:"true"}) ;
    attachment('pjIDRepresentant9NonSignataire', 'pjIDRepresentant9NonSignataire', { label: userRepresentant9, mandatory:"true"});
}

// PJ Représentant Permanent 10

var userRepresentant10;
if (dirigeant10.civiliteRepresentant.personneLieePPNomUsage != null) {
    var userRepresentant10 = dirigeant10.civiliteRepresentant.personneLieePPNomUsage + '  ' + dirigeant10.civiliteRepresentant.personneLieePPPrenom[0] ;
} else {
    var userRepresentant10 = dirigeant10.civiliteRepresentant.personneLieePPNomNaissance + '  '+ dirigeant10.civiliteRepresentant.personneLieePPPrenom[0] ;
}

if(dirigeant10.civiliteRepresentant.personneLieePPNomNaissance != null){
	attachment('pjDNCRepresentant10', 'pjDNCRepresentant10',{ label: userRepresentant10, mandatory:"true"}) ;
    attachment('pjIDRepresentant10NonSignataire', 'pjIDRepresentant10NonSignataire', { label: userRepresentant10, mandatory:"true"});
}

// PJ Représentant Permanent 11

var userRepresentant11;
if (dirigeant11.civiliteRepresentant.personneLieePPNomUsage != null) {
    var userRepresentant11 = dirigeant11.civiliteRepresentant.personneLieePPNomUsage + '  ' + dirigeant11.civiliteRepresentant.personneLieePPPrenom[0] ;
} else {
    var userRepresentant11 = dirigeant11.civiliteRepresentant.personneLieePPNomNaissance + '  '+ dirigeant11.civiliteRepresentant.personneLieePPPrenom[0] ;
}

if(dirigeant11.civiliteRepresentant.personneLieePPNomNaissance != null){
	attachment('pjDNCRepresentant11', 'pjDNCRepresentant11',{ label: userRepresentant11, mandatory:"true"}) ;
    attachment('pjIDRepresentant11NonSignataire', 'pjIDRepresentant11NonSignataire', { label: userRepresentant11, mandatory:"true"});
}

// PJ Représentant Permanent 12

var userRepresentant12;
if (dirigeant12.civiliteRepresentant.personneLieePPNomUsage != null) {
    var userRepresentant12 = dirigeant12.civiliteRepresentant.personneLieePPNomUsage + '  ' + dirigeant12.civiliteRepresentant.personneLieePPPrenom[0] ;
} else {
    var userRepresentant12 = dirigeant12.civiliteRepresentant.personneLieePPNomNaissance + '  '+ dirigeant12.civiliteRepresentant.personneLieePPPrenom[0] ;
}

if(dirigeant12.civiliteRepresentant.personneLieePPNomNaissance != null){
	attachment('pjDNCRepresentant12', 'pjDNCRepresentant12',{ label: userRepresentant12, mandatory:"true"}) ;
    attachment('pjIDRepresentant12NonSignataire', 'pjIDRepresentant12NonSignataire', { label: userRepresentant12, mandatory:"true"});
}

// PJ Représentant Permanent 13

var userRepresentant13;
if (dirigeant13.civiliteRepresentant.personneLieePPNomUsage != null) {
    var userRepresentant13 = dirigeant13.civiliteRepresentant.personneLieePPNomUsage + '  ' + dirigeant13.civiliteRepresentant.personneLieePPPrenom[0] ;
} else {
    var userRepresentant13 = dirigeant13.civiliteRepresentant.personneLieePPNomNaissance + '  '+ dirigeant13.civiliteRepresentant.personneLieePPPrenom[0] ;
}

if(dirigeant13.civiliteRepresentant.personneLieePPNomNaissance != null){
	attachment('pjDNCRepresentant13', 'pjDNCRepresentant13',{ label: userRepresentant13, mandatory:"true"}) ;
    attachment('pjIDRepresentant13NonSignataire', 'pjIDRepresentant13NonSignataire', { label: userRepresentant13, mandatory:"true"});
}

// PJ Représentant Permanent 14

var userRepresentant14;
if (dirigeant14.civiliteRepresentant.personneLieePPNomUsage != null) {
    var userRepresentant14 = dirigeant14.civiliteRepresentant.personneLieePPNomUsage + '  ' + dirigeant14.civiliteRepresentant.personneLieePPPrenom[0] ;
} else {
    var userRepresentant14 = dirigeant14.civiliteRepresentant.personneLieePPNomNaissance + '  '+ dirigeant14.civiliteRepresentant.personneLieePPPrenom[0] ;
}

if(dirigeant14.civiliteRepresentant.personneLieePPNomNaissance != null){
	attachment('pjDNCRepresentant14', 'pjDNCRepresentant14',{ label: userRepresentant14, mandatory:"true"}) ;
    attachment('pjIDRepresentant14NonSignataire', 'pjIDRepresentant14NonSignataire', { label: userRepresentant14, mandatory:"true"});
}

// Pj CAC Titulaire 

if(Value('id').of(dirigeant1.personneLieeQualite).eq('71')
or Value('id').of(dirigeant2.personneLieeQualite).eq('71')
or Value('id').of(dirigeant3.personneLieeQualite).eq('71')
or Value('id').of(dirigeant4.personneLieeQualite).eq('71')
or Value('id').of(dirigeant5.personneLieeQualite).eq('71')
or Value('id').of(dirigeant6.personneLieeQualite).eq('71')
or Value('id').of(dirigeant7.personneLieeQualite).eq('71')
or Value('id').of(dirigeant8.personneLieeQualite).eq('71')
or Value('id').of(dirigeant9.personneLieeQualite).eq('71')
or Value('id').of(dirigeant10.personneLieeQualite).eq('71')
or Value('id').of(dirigeant11.personneLieeQualite).eq('71')
or Value('id').of(dirigeant12.personneLieeQualite).eq('71')
or Value('id').of(dirigeant13.personneLieeQualite).eq('71')
or Value('id').of(dirigeant14.personneLieeQualite).eq('71')) {
    attachment('pjJustificatifInscriptionCACTitulaire', 'pjJustificatifInscriptionCACTitulaire', { mandatory:"false"});
	attachment('pjLettreAcceptationCACTitulaire', 'pjLettreAcceptationCACTitulaire', { mandatory:"true"});
}

// Pj CAC Suppléant

if(Value('id').of(dirigeant1.personneLieeQualite).eq('72')
or Value('id').of(dirigeant2.personneLieeQualite).eq('72')
or Value('id').of(dirigeant3.personneLieeQualite).eq('72')
or Value('id').of(dirigeant4.personneLieeQualite).eq('72')
or Value('id').of(dirigeant5.personneLieeQualite).eq('72')
or Value('id').of(dirigeant6.personneLieeQualite).eq('72')
or Value('id').of(dirigeant7.personneLieeQualite).eq('72')
or Value('id').of(dirigeant8.personneLieeQualite).eq('72')
or Value('id').of(dirigeant9.personneLieeQualite).eq('72')
or Value('id').of(dirigeant10.personneLieeQualite).eq('72')
or Value('id').of(dirigeant11.personneLieeQualite).eq('72')
or Value('id').of(dirigeant12.personneLieeQualite).eq('72')
or Value('id').of(dirigeant13.personneLieeQualite).eq('72')
or Value('id').of(dirigeant14.personneLieeQualite).eq('72')) {
   attachment('pjJustificatifInscriptionCACSuppleant', 'pjJustificatifInscriptionCACSuppleant', { mandatory:"false"});
   attachment('pjLettreAcceptationCACSuppleant', 'pjLettreAcceptationCACSuppleant', { mandatory:"true"});
}

// PJ Mandataire

var userMandataire=$m0SAS.cadre11SignatureGroup.cadre11Signature.adresseMandataire

var pj=$m0SAS.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire')) {
    attachment('pjPouvoir', 'pjPouvoir', { mandatory:"true"});
	attachment('pjIDMandataireSignataire', 'pjIDMandataireSignataire', {label: userMandataire.nomPrenomDenominationMandataire, mandatory:"true"});
}

//PJ Société

if(not Value('id').of(societe.entrepriseFormeJuridique).eq('3110') and not Value('id').of(societe.entrepriseFormeJuridique).eq('3120') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5800')) {
	attachment('pjStatuts', 'pjStatuts', { mandatory:"true"});

	if(not Value('id').of(societe.entrepriseFormeJuridique).eq('5202') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5203') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5306') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5307')) {
		attachment('pjCertificatDepositaireFonds', 'pjCertificatDepositaireFonds', { mandatory:"true"});
		attachment('pjCertificatListeSouscripteurs', 'pjCertificatListeSouscripteurs', { mandatory:"true"});
		attachment('pjAnnonceLegaleConstitution', 'pjAnnonceLegaleConstitution', { mandatory:"true"});
		if (societe.entrepriseCapitalApportNature and ((not Value('id').of(societe.entrepriseFormeJuridique).eq('5720')
			and not Value('id').of(societe.entrepriseFormeJuridique).eq('5710') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5785'))
			or (societe.entrepriseCapitalApportMoitierCapital or societe.entrepriseCapitalApport30000))) {
		    attachment('pjRapportCAA', 'pjRapportCAA', { mandatory:"true"});
		}
	}
	if(Value('id').of(societe.entrepriseFormeJuridique).eq('5202') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5203') or Value('id').of(societe.entrepriseFormeJuridique).eq('5306') or Value('id').of(societe.entrepriseFormeJuridique).eq('5307')) {
		attachment('pjJournalConstitution', 'pjJournalConstitution', { mandatory:"true"});
	}
}	

if(Value('id').of(societe.entrepriseFormeJuridique).eq('3110') or Value('id').of(societe.entrepriseFormeJuridique).eq('3120') or Value('id').of(societe.entrepriseFormeJuridique).eq('5800')) {
	attachment('pjStatutsEtr', 'pjStatutsEtr', { mandatory:"true"});
}

// PJ Etablissement

// Création ou constitution sans activité

var origine = $m0SAS.cadre4OrigineFondsGroup.cadre4OrigineFonds ;

if(Value('id').of(siege.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')) {
    attachment('pjDomicile', 'pjDomicile', { mandatory:"true"});
}

if(Value('id').of(siege.activiteLieu).eq('etablissementDomiciliationOui')) {
    attachment('pjContratDomiciliation', 'pjContratDomiciliation', { mandatory:"true"});
}

if (Value('id').of(siege.activiteLieu).eq('etablissementDomiciliationNon')
 and (siege.etablissementPrincipalDifferentSiege or not siege.formaliteCreationAvecActivite 
 or (not siege.etablissementPrincipalDifferentSiege 
 and (Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsCreation') 
 or Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsCocheAutre')
 or ((Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsAchat')
 or Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsApport')) 
 and origine.precedentExploitant.fondsArtisanal)
 or Value('id').of(origine.etablissementOrigineFondsLiberal).eq('EtablissementOrigineFondsCreation')
 or Value('id').of(origine.etablissementOrigineFondsLiberal).eq('EtablissementOrigineFondsRepriseActiviteLiberale'))))
 and not Value('id').of(societe.entrepriseFormeJuridique).eq('3110') 
 and not Value('id').of(societe.entrepriseFormeJuridique).eq('3120') 
 and not Value('id').of(societe.entrepriseFormeJuridique).eq('5800')) {
    attachment('pjSocieteCreation', 'pjSocieteCreation', { mandatory:"true"});
}

if((siege.etablissementPrincipalDifferentSiege or Value('id').of(societe.entrepriseFormeJuridique).eq('3110') 
 or Value('id').of(societe.entrepriseFormeJuridique).eq('3120') 
 or Value('id').of(societe.entrepriseFormeJuridique).eq('5800'))
and (Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsCreation') 
 or Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsCocheAutre')
 or ((Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsAchat')
 or Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsApport')) 
 and origine.precedentExploitant.fondsArtisanal)
 or Value('id').of(origine.etablissementOrigineFondsLiberal).eq('EtablissementOrigineFondsCreation')
 or Value('id').of(origine.etablissementOrigineFondsLiberal).eq('EtablissementOrigineFondsRepriseActiviteLiberale'))) {
    attachment('pjEtablissementCreation', 'pjEtablissementCreation', { mandatory:"true"});
}

// Location-gérance

if(Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsLocationGerance')) {
    attachment('pjLocationGerance', 'pjLocationGerance', { mandatory:"true"});
	attachment('pjLocationGerancePublication', 'pjLocationGerancePublication', { mandatory:"true"});
}

// Gérance-mandat

if(Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsGeranceMandat')) {
    attachment('pjGeranceMandat', 'pjGeranceMandat', { mandatory:"true"});
	attachment('pjGeranceMandatPublication', 'pjGeranceMandatPublication', { mandatory:"true"});
}

// Achat

if(Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsAchat')
	and origine.precedentExploitant.cessionFonds) {
    attachment('pjPlanCession', 'pjPlanCession', { mandatory:"true"});
}

if(Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsAchat') 
	and origine.precedentExploitant.etablissementJournalAnnoncesLegalesNom != null) {
    attachment('pjAchat', 'pjAchat', { mandatory:"true"});
	attachment('pjAchatPublication', 'pjAchatPublication', { mandatory:"true"});
}

// Apport

if(Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsApport')) {
    attachment('pjApport', 'pjApport', { mandatory:"true"});
	attachment('pjApportPublication', 'pjApportPublication', { mandatory:"true"});
}

// PJ JQPA

var jqpa1 = $m0SAS.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification1;
if(Value('id').of(jqpa1.jqpaSituation).eq('jqpaSituationDiplome')) {
	attachment('pjDiplomes1', 'pjDiplomes1', { label : jqpa1.jqpaActiviteJqpaPP, mandatory:"true"});
}
if(Value('id').of(jqpa1.jqpaSituation).eq('jqpaSituationExperience')) {
	attachment('pjPiecesUtiles1', 'pjPiecesUtiles1', { label : jqpa1.jqpaActiviteJqpaPP,mandatory:"true"});
}

var jqpa2 = $m0SAS.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification2;
if(Value('id').of(jqpa2.jqpaSituation).eq('jqpaSituationDiplome')) {
	attachment('pjDiplomes2', 'pjDiplomes2', { label : jqpa2.jqpaActiviteJqpaPP, mandatory:"true"});
}
if(Value('id').of(jqpa2.jqpaSituation).eq('jqpaSituationExperience')) {
	attachment('pjPiecesUtiles2', 'pjPiecesUtiles2', { label : jqpa2.jqpaActiviteJqpaPP, mandatory:"true"});
}

var jqpa3 = $m0SAS.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification3;
if(Value('id').of(jqpa3.jqpaSituation).eq('jqpaSituationDiplome')) {
	attachment('pjDiplomes3', 'pjDiplomes3', { label : jqpa3.jqpaActiviteJqpaPP, mandatory:"true"});
}
if(Value('id').of(jqpa3.jqpaSituation).eq('jqpaSituationExperience')) {
	attachment('pjPiecesUtiles3', 'pjPiecesUtiles3', { label : jqpa3.jqpaActiviteJqpaPP, mandatory:"true"});
}

// PJ MBE	
var signataire = $m0SAS.cadre11SignatureGroup.cadre11Signature ;

attachment('formulaireBE1', 'formulaireBE1', {mandatory:"true"});

if (not Value('id').of(signataire.nombreMBE).eq('un')) {
attachment('formulaireBE2', 'formulaireBE2', {mandatory:"true"});
}

if (not Value('id').of(signataire.nombreMBE).eq('un') and not Value('id').of(signataire.nombreMBE).eq('deux')) {
attachment('formulaireBE3', 'formulaireBE3', {mandatory:"true"});
}

if (Value('id').of(signataire.nombreMBE).eq('quatre') or Value('id').of(signataire.nombreMBE).eq('cinq')) {
attachment('formulaireBE4', 'formulaireBE4', {mandatory:"true"});
}

if (Value('id').of(signataire.nombreMBE).eq('cinq')) {
attachment('formulaireBE5', 'formulaireBE5', {mandatory:"true"});
}

attachment('pjIDActiviteReglementee', 'pjIDActiviteReglementee', { mandatory:"false"});