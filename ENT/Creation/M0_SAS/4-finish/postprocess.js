// prepare info to send

var adresseSiege = $m0SAS.cadre2AdresseSiegeGroup.cadre2AdresseSiege.cadreAdresseProfessionnelle;
var adresseEtablissement = $m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.cadreAdresseEtablissement;

var algo = "trouver destinataire";
var secteur2 = '';
if ((($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA != null
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	or ($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA != null
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	or ($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA != null
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.jqpaDomaineTransport).eq('jqpaTransportAutre')))
	and not $m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.optionCMACCI) 	{
var secteur1 =  "ARTISANAL";
} else if (Value('id').of($m0SAS.cadre1SocieteGroup.cadre1Identite.entrepriseFormeJuridique).eq('5785')
	or Value('id').of($m0SAS.cadre1SocieteGroup.cadre1Identite.entrepriseFormeJuridique).eq('5585')
	or Value('id').of($m0SAS.cadre1SocieteGroup.cadre1Identite.entrepriseFormeJuridique).eq('5685')
	or Value('id').of($m0SAS.cadre1SocieteGroup.cadre1Identite.entrepriseFormeJuridique).eq('5385')) {
var secteur1 =  "LIBERAL";
} else { var secteur1 =  "COMMERCIAL"; }

if (Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
	or Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
	or Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
	or Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre')
	or Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.jqpaDomaineTransport).eq('jqpaTransportAutre')
	or Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA).eq('nafaAutre')
	or Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineAlimentation).eq('alimentationAutre')
	or Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineAutresServices).eq('servicesAutre')
	or Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineProduction).eq('productionAutre')
	or Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.jqpaDomaineTransport).eq('jqpaTransportAutre')
	or Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA).eq('nafaAutre')
	or Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineAlimentation).eq('alimentationAutre')
	or Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineAutresServices).eq('servicesAutre')
	or Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineProduction).eq('productionAutre') 
	or Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.jqpaDomaineTransport).eq('jqpaTransportAutre')) {
	secteur2 =  "COMMERCIAL";
}

var typePersonne = "PM";
var formJuridique = (Value('id').of($m0SAS.cadre1SocieteGroup.cadre1Identite.entrepriseFormeJuridique).eq('5710') 
					or Value('id').of($m0SAS.cadre1SocieteGroup.cadre1Identite.entrepriseFormeJuridique).eq('5720')) ? "SAS" : 
                    ((Value('id').of($m0SAS.cadre1SocieteGroup.cadre1Identite.entrepriseFormeJuridique).eq('5308') 
					or Value('id').of($m0SAS.cadre1SocieteGroup.cadre1Identite.entrepriseFormeJuridique).eq('5309')) ? "SCA" :
					((Value('id').of($m0SAS.cadre1SocieteGroup.cadre1Identite.entrepriseFormeJuridique).eq('5202') 
					or Value('id').of($m0SAS.cadre1SocieteGroup.cadre1Identite.entrepriseFormeJuridique).eq('5203')) ? "SNC" :
					((Value('id').of($m0SAS.cadre1SocieteGroup.cadre1Identite.entrepriseFormeJuridique).eq('5306') 
					or Value('id').of($m0SAS.cadre1SocieteGroup.cadre1Identite.entrepriseFormeJuridique).eq('5307')) ? "SCS" :
					((Value('id').of($m0SAS.cadre1SocieteGroup.cadre1Identite.entrepriseFormeJuridique).eq('5785') 
					or Value('id').of($m0SAS.cadre1SocieteGroup.cadre1Identite.entrepriseFormeJuridique).eq('5585')
					or Value('id').of($m0SAS.cadre1SocieteGroup.cadre1Identite.entrepriseFormeJuridique).eq('5685') 
					or Value('id').of($m0SAS.cadre1SocieteGroup.cadre1Identite.entrepriseFormeJuridique).eq('5385')) ? "SEL" : "SA"))));
var optionCMACCI = "NON";

var codeCommune = '';

if (Value('id').of($m0SAS.cadre1SocieteGroup.cadre1Identite.entrepriseFormeJuridique).eq('3110') or Value('id').of($m0SAS.cadre1SocieteGroup.cadre1Identite.entrepriseFormeJuridique).eq('3120')
	or (Value('id').of($m0SAS.cadre1SocieteGroup.cadre1Identite.entrepriseFormeJuridique).eq('5800') 
	and not Value('id').of($m0SAS.cadre2AdresseSiegeGroup.cadre2AdresseSiege.cadreAdresseProfessionnelle.paysAdresseSiege).eq('FRXXXXX')
	and Value('id').of($m0SAS.cadre1SocieteGroup.cadre1Identite.entrepriseEur).eq('ouvertureEtablissement'))) {
codeCommune = adresseEtablissement.communeAdresseEtablissement.getId();
} else if (Value('id').of($m0SAS.cadre1SocieteGroup.cadre1Identite.entrepriseFormeJuridique).eq('5800') 
	and Value('id').of($m0SAS.cadre1SocieteGroup.cadre1Identite.entrepriseEur).eq('ouvertureAmbulant')) {
codeCommune = $m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.communeAdresseMarche.getId();
} else { codeCommune = adresseSiege.communeAdresseSiege.getId(); }


var attachement = "/3-review/generated/generated.record-1-M0_SAS_Creation.pdf";

return spec.create({
	id : 'prepareSend',
	label : "Préparation de la recherche du destinataire",
	groups : [ spec.createGroup({
		id : 'view',
		label : "Informations",
		data : [ spec.createData({
			id : 'algo',
			label : "Algo",
			type : 'String',
			mandatory : true,
			value : algo
		}), spec.createData({
			id : 'secteur1',
			label : "Secteur",
			type : 'String',
			value : secteur1
		}), spec.createData({
			id : 'typePersonne',
			label : "Type personne",
			type : 'String',
			value : typePersonne
		}), spec.createData({
			id : 'formJuridique',
			label : "Forme juridique",
			type : 'String',
			value : formJuridique
		}), spec.createData({
			id : 'optionCMACCI',
			label : "Option CMACCI",
			type : 'String',
			value : optionCMACCI
		}), spec.createData({
			id : 'codeCommune',
			label : "Code commune",
			type : 'String',
			value : codeCommune
		}), spec.createData({
			id : 'attachement',
			label : "Pièce jointe",
			type : 'String',
			value : attachement
		}) ]
	}) ]
});