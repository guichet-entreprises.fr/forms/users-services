//Fichier de mapping des champs Regent pour :
//Version : V2008.11 
//Evenements : 03M,  
//Fichier généré le : 2018-11-02_114826 


function pad(s) { return (s < 10) ? '0' + s : s; }
function sanitize(s){return s.replaceAll("’","'").replaceAll("–","-").replaceAll("­","");}
var societe = $m0SAS.cadre1SocieteGroup.cadre1Identite;
var siege = $m0SAS.cadre2AdresseSiegeGroup.cadre2AdresseSiege; 

var regentVersion = "V2008.11";  // (V2008.11, V2016.02) 
var eventRegent = (siege.formaliteCreationAvecActivite or Value('id').of(societe.entrepriseFormeJuridique).eq('5785') or Value('id').of(societe.entrepriseFormeJuridique).eq('5585') or Value('id').of(societe.entrepriseFormeJuridique).eq('5685') or Value('id').of(societe.entrepriseFormeJuridique).eq('5385')) ? (siege.etablissementPrincipalDifferentSiege ? "03M" : "01M") : ((Value('id').of(societe.entrepriseFormeJuridique).eq('3110') or Value('id').of(societe.entrepriseFormeJuridique).eq('3120') or Value('id').of(societe.entrepriseFormeJuridique).eq('5800')) ? "04M" : "02M"); //[] 
var authorityType = "CFE"; // (CFE, TDR)
var authorityId = _INPUT_.dataGroup.codeEDI; // (code EDI)

//Ajout du type de la deuxième autorité
var authorityType2 = "TDR"; // (CFE, TDR) 
//Ajout du code de la deuxième autorité
var authorityId2 = _INPUT_.dataGroup.codeEDI2; // (code EDI) 

var regEx = new RegExp('[0-9]{5}'); 
var etablissement = $m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite ;
var origine = $m0SAS.cadre4OrigineFondsGroup.cadre4OrigineFonds ;
var dirigeant1 = $m0SAS.dirigeant1.cadreIdentiteDirigeant;
var dirigeant2 = $m0SAS.dirigeant2.cadreIdentiteDirigeant;
var dirigeant3 = $m0SAS.dirigeant3.cadreIdentiteDirigeant;
var dirigeant4 = $m0SAS.dirigeant4.cadreIdentiteDirigeant;
var dirigeant5 = $m0SAS.dirigeant5.cadreIdentiteDirigeant;
var dirigeant6 = $m0SAS.dirigeant6.cadreIdentiteDirigeant;
var dirigeant7 = $m0SAS.dirigeant7.cadreIdentiteDirigeant;
var dirigeant8 = $m0SAS.dirigeant8.cadreIdentiteDirigeant;
var dirigeant9 = $m0SAS.dirigeant9.cadreIdentiteDirigeant;
var dirigeant10 = $m0SAS.dirigeant10.cadreIdentiteDirigeant;
var dirigeant11 = $m0SAS.dirigeant11.cadreIdentiteDirigeant;
var dirigeant12 = $m0SAS.dirigeant12.cadreIdentiteDirigeant;
var dirigeant13 = $m0SAS.dirigeant13.cadreIdentiteDirigeant;
var dirigeant14 = $m0SAS.dirigeant14.cadreIdentiteDirigeant;
var fiscal = $m0SAS.cadre20optFiscGroup.cadre20optFisc ;
var correspondance = $m0SAS.cadre10RensCompGroup.cadre10RensComp ;
var signataire = $m0SAS.cadre11SignatureGroup.cadre11Signature ;
var jqpa1 = $m0SAS.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification1;
var jqpa2 = $m0SAS.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification2;
var jqpa3 = $m0SAS.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification3;
var fusion1 = $m0SAS.cadre1SocieteGroup.cadre1Identite.fusionGroup1 ;
var fusion2 = $m0SAS.cadre1SocieteGroup.cadre1Identite.fusionGroup2 ;
var fusion3 = $m0SAS.cadre1SocieteGroup.cadre1Identite.fusionGroup3 ;
var autreEtablissement1 = $m0SAS.cadre1SocieteGroup.cadre1Identite.cadreAutreEtablissementUE1 ;
var autreEtablissement2 = $m0SAS.cadre1SocieteGroup.cadre1Identite.cadreAutreEtablissementUE2 ;
var socialDirigeant1 = $m0SAS.dirigeant1.cadreIdentiteDirigeant.cadre7DeclarationSociale;
var socialDirigeant2 = $m0SAS.dirigeant2.cadreIdentiteDirigeant.cadre7DeclarationSociale;
var socialDirigeant3 = $m0SAS.dirigeant3.cadreIdentiteDirigeant.cadre7DeclarationSociale;
var socialDirigeant4 = $m0SAS.dirigeant4.cadreIdentiteDirigeant.cadre7DeclarationSociale;
var socialDirigeant5 = $m0SAS.dirigeant5.cadreIdentiteDirigeant.cadre7DeclarationSociale;
var socialDirigeant6 = $m0SAS.dirigeant6.cadreIdentiteDirigeant.cadre7DeclarationSociale;
var socialDirigeant7 = $m0SAS.dirigeant7.cadreIdentiteDirigeant.cadre7DeclarationSociale;
var socialDirigeant8 = $m0SAS.dirigeant8.cadreIdentiteDirigeant.cadre7DeclarationSociale;
var socialDirigeant9 = $m0SAS.dirigeant9.cadreIdentiteDirigeant.cadre7DeclarationSociale;
var socialDirigeant10 = $m0SAS.dirigeant10.cadreIdentiteDirigeant.cadre7DeclarationSociale;
var socialDirigeant11 = $m0SAS.dirigeant11.cadreIdentiteDirigeant.cadre7DeclarationSociale;
var socialDirigeant12 = $m0SAS.dirigeant12.cadreIdentiteDirigeant.cadre7DeclarationSociale;
var socialDirigeant13 = $m0SAS.dirigeant13.cadreIdentiteDirigeant.cadre7DeclarationSociale;
var socialDirigeant14 = $m0SAS.dirigeant14.cadreIdentiteDirigeant.cadre7DeclarationSociale;

var regentFields = {}; 

// A mettre toujours sous "Z1611"
regentFields['/REGENT-XML/Emetteur']= "Z1611";

regentFields['/REGENT-XML/Destinataire']= authorityId;

// Complété par destiny
regentFields['/REGENT-XML/DateHeureEmission']= null;
regentFields['/REGENT-XML/VersionMessage']= null;
regentFields['/REGENT-XML/VersionNorme']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Specification/NomService']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Specification/VersionService']= null; 

                                  // Groupe GDF : Groupe données de service

// Sous groupe IDF : Identification de la formalité

// Généré par destiny
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C01']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C03']='0';
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C04']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05']= ((($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA != null
																	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
																	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
																	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
																	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre')
																	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.jqpaDomaineTransport).eq('jqpaTransportAutre'))
																	or ($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA != null
																	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA).eq('nafaAutre')
																	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineAlimentation).eq('alimentationAutre')
																	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineAutresServices).eq('servicesAutre')
																	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineProduction).eq('productionAutre')
																	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.jqpaDomaineTransport).eq('jqpaTransportAutre'))
																	or ($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA != null
																	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA).eq('nafaAutre')
																	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineAlimentation).eq('alimentationAutre')
																	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineAutresServices).eq('servicesAutre')
																	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineProduction).eq('productionAutre')
																	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.jqpaDomaineTransport).eq('jqpaTransportAutre')))
																	and not $m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.optionCMACCI) ? "M" : "C";

if ((($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA != null
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	or ($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA != null
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	or ($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA != null
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.jqpaDomaineTransport).eq('jqpaTransportAutre')))
	and not $m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.optionCMACCI) 	{
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C06']= (($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA != null
																	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
																	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
																	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
																	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre')
																	and not Value('id').of($m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.jqpaDomaineTransport).eq('jqpaTransportAutre'))
																	and not $m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.optionCMACCI) ? "O" : "N";
}																	

// Sous groupe EDF : Evènement déclaré
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.1']= (siege.formaliteCreationAvecActivite or Value('id').of(societe.entrepriseFormeJuridique).eq('5785') or Value('id').of(societe.entrepriseFormeJuridique).eq('5585') or Value('id').of(societe.entrepriseFormeJuridique).eq('5685') or Value('id').of(societe.entrepriseFormeJuridique).eq('5385')) ? (siege.etablissementPrincipalDifferentSiege ? "03M" : "01M") : ((Value('id').of(societe.entrepriseFormeJuridique).eq('3110') or Value('id').of(societe.entrepriseFormeJuridique).eq('3120') or Value('id').of(societe.entrepriseFormeJuridique).eq('5800')) ? "04M" : "02M"); 
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2']= date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2']= date;
}

// Sous groupe DMF : Destinataire de la formalité

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[1]']= authorityId;
if (authorityId != "" and authorityId2 != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[2]']= authorityId2;
}


// Sous groupe ADF : Adresse de correspondance

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C36']= (Value('id').of(correspondance.adresseCorrespond).eq('siege') or Value('id').of(correspondance.adresseCorrespond).eq('etablissement')) ?
																		societe.entrepriseDenomination : correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance;

if (Value('id').of(correspondance.adresseCorrespond).eq('siege') and siege.cadreAdresseProfessionnelle.communeAdresseSiege != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']= siege.cadreAdresseProfessionnelle.communeAdresseSiege.getId();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('siege') and siege.cadreAdresseProfessionnelle.villeAdresseSiege != null) {
	var idPays = siege.cadreAdresseProfessionnelle.paysAdresseSiege.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']= result;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('etablissement')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']=etablissement.cadreAdresseEtablissement.communeAdresseEtablissement.getId();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']=correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune.getId();
}	

if ((Value('id').of(correspondance.adresseCorrespond).eq('siege') and siege.cadreAdresseProfessionnelle.numeroAdresseSiege != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('etablissement') and etablissement.cadreAdresseEtablissement.numeroAdresseEtablissement != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.numeroRueAdresseCorrespondance != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.5']= Value('id').of(correspondance.adresseCorrespond).eq('siege') ? siege.cadreAdresseProfessionnelle.numeroAdresseSiege :
																			(Value('id').of(correspondance.adresseCorrespond).eq('etablissement') ? etablissement.cadreAdresseEtablissement.numeroAdresseEtablissement : 
																			correspondance.adresseCorrespondance.numeroRueAdresseCorrespondance);
}
	
if (Value('id').of(correspondance.adresseCorrespond).eq('siege') and siege.cadreAdresseProfessionnelle.indiceAdresseSiege != null) {
   var monId1 = Value('id').of(siege.cadreAdresseProfessionnelle.indiceAdresseSiege)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId1;  
} else if (Value('id').of(correspondance.adresseCorrespond).eq('etablissement') and etablissement.cadreAdresseEtablissement.indiceAdresseEtablissement != null) {
   var monId2 = Value('id').of(etablissement.cadreAdresseEtablissement.indiceAdresseEtablissement)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId2;
} else if (correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance !== null) {
   var monId3 = Value('id').of(correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId3;
}

if ((Value('id').of(correspondance.adresseCorrespond).eq('siege') and siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('etablissement') and etablissement.cadreAdresseEtablissement.distributionSpecialeAdresseEtablissement != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.distriutionSpecialeVoieAdresseCorrespondance != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.7']= Value('id').of(correspondance.adresseCorrespond).eq('siege') ? siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege : 
																				(Value('id').of(correspondance.adresseCorrespond).eq('etablissement') ? etablissement.cadreAdresseEtablissement.distributionSpecialeAdresseEtablissement : 
																				correspondance.adresseCorrespondance.distriutionSpecialeVoieAdresseCorrespondance);
}

if (Value('id').of(correspondance.adresseCorrespond).eq('siege')
	or (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('etablissement') and etablissement.cadreAdresseEtablissement.codePostalAdresseEtablissement != null)) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8']= Value('id').of(correspondance.adresseCorrespond).eq('siege') ? (siege.cadreAdresseProfessionnelle.codePostalAdresseSiege != null ? siege.cadreAdresseProfessionnelle.codePostalAdresseSiege : ".") : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('etablissement') ? etablissement.cadreAdresseEtablissement.codePostalAdresseEtablissement : 
																			correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal);
}
	
if ((Value('id').of(correspondance.adresseCorrespond).eq('siege') and siege.cadreAdresseProfessionnelle.complementAdresseSiege != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('etablissement') and etablissement.cadreAdresseEtablissement.complementAdresseEtablissement != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.rueComplementAdresseCorrespondance != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.10']= Value('id').of(correspondance.adresseCorrespond).eq('siege') ? siege.cadreAdresseProfessionnelle.complementAdresseSiege : 
																				(Value('id').of(correspondance.adresseCorrespond).eq('etablissement') ? etablissement.cadreAdresseEtablissement.complementAdresseEtablissement : 
																				correspondance.adresseCorrespondance.rueComplementAdresseCorrespondance);
}
	
if (Value('id').of(correspondance.adresseCorrespond).eq('siege') and siege.cadreAdresseProfessionnelle.typeAdresseSiege !== null) {
   var monId1 = Value('id').of(siege.cadreAdresseProfessionnelle.typeAdresseSiege)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId1;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('etablissement') and etablissement.cadreAdresseEtablissement.typeAdresseEtablissement !== null) {
   var monId2 = Value('id').of(etablissement.cadreAdresseEtablissement.typeAdresseEtablissement)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId2;
} else if (correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance !== null) {
   var monId3 = Value('id').of(correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId3;
}	

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12']= Value('id').of(correspondance.adresseCorrespond).eq('siege') ? siege.cadreAdresseProfessionnelle.voieAdresseSiege : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('etablissement') ? etablissement.cadreAdresseEtablissement.voieAdresseEtablissement : 
																			correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance);
	
if 	(Value('id').of(correspondance.adresseCorrespond).eq('siege') and siege.cadreAdresseProfessionnelle.communeAdresseSiege != null) {																		
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= siege.cadreAdresseProfessionnelle.communeAdresseSiege.getLabel(); 
} else if (Value('id').of(correspondance.adresseCorrespond).eq('siege') and siege.cadreAdresseProfessionnelle.villeAdresseSiege != null) {																		
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= siege.cadreAdresseProfessionnelle.villeAdresseSiege; 
} else if (Value('id').of(correspondance.adresseCorrespond).eq('etablissement')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= etablissement.cadreAdresseEtablissement.communeAdresseEtablissement.getLabel(); 
} else if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune.getLabel();
}

if (Value('id').of(correspondance.adresseCorrespond).eq('siege') and siege.cadreAdresseProfessionnelle.villeAdresseSiege != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.14']= siege.cadreAdresseProfessionnelle.paysAdresseSiege.getLabel();
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.1[1]']= correspondance.infosSup.formaliteTelephone2.e164.replace('+', '00');

if (correspondance.infosSup.formaliteTelephone1 != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.1[2]']= correspondance.infosSup.formaliteTelephone1.e164.replace('+', '00');
}

if (correspondance.infosSup.telecopie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.2']= correspondance.infosSup.telecopie.e164.replace('+', '00');
}

if (correspondance.infosSup.formaliteFaxCourriel != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.3']= correspondance.infosSup.formaliteFaxCourriel;
}


// Sous groupe SIF : Signature de la formalité

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.1']= Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? 
																			signataire.adresseMandataire.nomPrenomDenominationMandataire : 
																			((Value('id').of(signataire.representantLegalNumero).eq('gerant1') or not dirigeant1.autreDirigeant) ?
																			(dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? 
																			(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + ' ' + dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) : 
																			(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + ' ' + dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) :
																			dirigeant1.civilitePersonneMorale.personneLieePMDenomination)))) :
																			(Value('id').of(signataire.representantLegalNumero).eq('gerant2') ?
																			(dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? 
																			(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + ' ' + dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) : 
																			(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + ' ' + dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) :
																			dirigeant2.civilitePersonneMorale.personneLieePMDenomination)))) :
																			(Value('id').of(signataire.representantLegalNumero).eq('gerant3') ?
																			(dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? 
																			(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + ' ' + dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) : 
																			(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + ' ' + dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) :
																			dirigeant3.civilitePersonneMorale.personneLieePMDenomination)))) :
																			(Value('id').of(signataire.representantLegalNumero).eq('gerant4') ?
																			(dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? 
																			(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + ' ' + dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) : 
																			(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + ' ' + dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) :
																			dirigeant4.civilitePersonneMorale.personneLieePMDenomination)))) :
																			(Value('id').of(signataire.representantLegalNumero).eq('gerant5') ?
																			(dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? 
																			(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + ' ' + dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) : 
																			(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + ' ' + dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) :
																			dirigeant5.civilitePersonneMorale.personneLieePMDenomination)))) :
																			(Value('id').of(signataire.representantLegalNumero).eq('gerant6') ?
																			(dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? 
																			(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + ' ' + dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) : 
																			(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + ' ' + dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) :
																			dirigeant6.civilitePersonneMorale.personneLieePMDenomination)))) :
																			(Value('id').of(signataire.representantLegalNumero).eq('gerant7') ?
																			(dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? 
																			(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + ' ' + dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) : 
																			(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + ' ' + dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) :
																			dirigeant7.civilitePersonneMorale.personneLieePMDenomination)))) :
																			(Value('id').of(signataire.representantLegalNumero).eq('gerant8') ?
																			(dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? 
																			(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + ' ' + dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) : 
																			(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + ' ' + dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) :
																			dirigeant8.civilitePersonneMorale.personneLieePMDenomination)))) :
																			(Value('id').of(signataire.representantLegalNumero).eq('gerant9') ?
																			(dirigeant9.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeant9.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeant9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? 
																			(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + ' ' + dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) : 
																			(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + ' ' + dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) :
																			dirigeant9.civilitePersonneMorale.personneLieePMDenomination)))) :
																			(Value('id').of(signataire.representantLegalNumero).eq('gerant10') ?
																			(dirigeant10.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeant10.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? 
																			(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + ' ' + dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) : 
																			(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + ' ' + dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) :
																			dirigeant10.civilitePersonneMorale.personneLieePMDenomination)))) :
																			(Value('id').of(signataire.representantLegalNumero).eq('gerant11') ?
																			(dirigeant11.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeant11.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeant11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? 
																			(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + ' ' + dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) : 
																			(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + ' ' + dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) :
																			dirigeant11.civilitePersonneMorale.personneLieePMDenomination)))) :
																			(Value('id').of(signataire.representantLegalNumero).eq('gerant12') ?
																			(dirigeant12.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeant12.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeant12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? 
																			(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + ' ' + dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) : 
																			(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + ' ' + dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) :
																			dirigeant12.civilitePersonneMorale.personneLieePMDenomination)))) :
																			(Value('id').of(signataire.representantLegalNumero).eq('gerant13') ?
																			(dirigeant13.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeant13.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeant13.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeant13.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant13.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeant13.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			(dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? 
																			(dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + ' ' + dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) : 
																			(dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + ' ' + dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) :
																			dirigeant13.civilitePersonneMorale.personneLieePMDenomination)))) :
																			(Value('id').of(signataire.representantLegalNumero).eq('gerant14') ?
																			(dirigeant14.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeant14.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeant14.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeant14.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant14.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeant14.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			(dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? 
																			(dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + ' ' + dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) : 
																			(dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + ' ' + dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) :
																			dirigeant14.civilitePersonneMorale.personneLieePMDenomination)))) : ''))))))))))))));

																			
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.2']= "Mandataire";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.3']= signataire.adresseMandataire.villeAdresseMandataire.getId();
if (signataire.adresseMandataire.numeroVoieMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.5']= signataire.adresseMandataire.numeroVoieMandataire;
}
if (signataire.adresseMandataire.indiceVoieMandataire !== null) {
   var monId = Value('id').of(signataire.adresseMandataire.indiceVoieMandataire)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.6']          = monId;
}
if (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.7']= signataire.adresseMandataire.voieDistributionSpecialeMandataire;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.8']= signataire.adresseMandataire.codePostalMandataire;
if (signataire.adresseMandataire.voieComplementMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.10']= signataire.adresseMandataire.voieComplementMandataire;
}
if (signataire.adresseMandataire.typeVoieMandataire !== null) {
   var monId = Value('id').of(signataire.adresseMandataire.typeVoieMandataire)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.11']          = monId;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.12']= signataire.adresseMandataire.nomVoieMandataire;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.13']= signataire.adresseMandataire.villeAdresseMandataire.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C41']= signataire.formaliteSignatureLieu;
if(signataire.formaliteSignatureDate !== null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C42']= date;
}	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C43']= (societe.entrepriseESS ? "M22=O!" : '') +''+(jqpa3.jqpaSituation != null ? "JQPA=3!" : (jqpa2.jqpaSituation != null ? "JQPA=2!" : 
(jqpa1.jqpaSituation != null ? "JQPA=1!" : ''))) +''+ ((siege.societeNomDomaine[0].length > 0) ? "M05=O!" : '') +''+ (etablissement.etablissementNomDomaine != null ? "E06=0!" : '') + '' + (correspondance.formaliteObservations != null ? sanitize(correspondance.formaliteObservations) : '') ;

// Groupe ICM : Identification complète de la personne morale

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/ICM/M01/M01.1']= societe.entrepriseDenomination;
if (societe.entrepriseSigle != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/ICM/M01/M01.2']= societe.entrepriseSigle;
}
if (Value('id').of(societe.entrepriseEur).eq('ouvertureAmbulant')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/ICM/M02']= "E";
}

// Groupe FJM : Forme juridique de la personne morale (ajout)

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FJM/M21/M21.1']= societe.entrepriseFormeJuridique.getId();
if (societe.entrepriseAssocieUnique or Value('id').of(societe.entrepriseFormeJuridique).eq('5720')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FJM/M23']= "O";
}

// Groupe CSM : Caractéristiques de la personne morale (ajout)

if (societe.entrepriseCapitalVariable) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M24/M24.1']= "2";
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M24/M24.2']= societe.entrepriseCapitalMontant != null ? (societe.entrepriseCapitalMontant +''+ "00") : (societe.entrepriseCapitalMontantEtr +''+ "00");
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M24/M24.3']= societe.entrepriseCapitalMontant != null ? "eur" : societe.entrepriseCapitalDevise.getId();
if (societe.entrepriseCapitalVariable) {regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M25']
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M24/M24.4']= societe.entrepriseCapitalVariableMinimum;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M25']= societe.entrepriseDuree;

if(societe.entrepriseDateClotureExerciceSocial != null) {
	var dateTemp = new Date(parseInt(societe.entrepriseDateClotureExerciceSocial.getTimeInMillis()));
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = "--" + month + "-" + day ; 
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M26']= date;
}

if(societe.entrepriseDateCloturePremierExerciceSocial != null) {
	var dateTemp = new Date(parseInt(societe.entrepriseDateCloturePremierExerciceSocial.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M28']          = date;
}	

// Groupe FSM : Fusion / Scission de la personne morale 

if (societe.entrepriseFusionScission) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M54']= Value('id').of(societe.fusionScission).eq('fusion') ? "1" : "2";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.1']= fusion1.entrepriseLieeEntreprisePMDenomination;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.2']= fusion1.entreperiseLieeEntreprisePMFormeJuridique.getId();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.3']= fusion1.adresseFusionScission.communeAdresseFusion.getId();
if(fusion1.adresseFusionScission.numeroAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.5']= fusion1.adresseFusionScission.numeroAdresseFusion;
}
if (fusion1.adresseFusionScission.indiceAdresseFusion !== null) {
   var monId = Value('id').of(fusion1.adresseFusionScission.indiceAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.6']          = monId;
}
if(fusion1.adresseFusionScission.distributionSpecialeAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.7']= fusion1.adresseFusionScission.distributionSpecialeAdresseFusion;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.8']= fusion1.adresseFusionScission.codePostalAdresseFusion;
if(fusion1.adresseFusionScission.complementAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.10']= fusion1.adresseFusionScission.complementAdresseFusion;
}
if (fusion1.adresseFusionScission.typeAdresseFusion !== null) {
   var monId = Value('id').of(fusion1.adresseFusionScission.typeAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.11']          = monId;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.12']= fusion1.adresseFusionScission.voieAdresseFusion;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.13']= fusion1.adresseFusionScission.communeAdresseFusion.getLabel();

//Récupérer le EntityId du greffe
var greffeId = fusion1.entrepriseLieeGreffe.id ;
//Appeler Directory pour récupérer l'autorité
_log.info('l entityId du greffe selectionne est {}', greffeId);
var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', greffeId) //
		   .connectionTimeout(10000) //
		   .receiveTimeout(10000) //
		   .accept('json') //
		   .get();
//result
var receiverInfo = response.asObject();
var contactInfo = !receiverInfo.details ? null : receiverInfo.details;
//Récupérer le code EDI
var codeEDIGreffe = !contactInfo.ediCode ? null : contactInfo.ediCode;
_log.info('le code edi du greffe selectionne est {}', codeEDIGreffe);
// À modifier quand Destiny fourni le réferentiel
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.4']=codeEDIGreffe;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.6']= fusion1.entrepriseLieeSiren.split(' ').join('');

if (fusion1.fusionAutreSociete) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.1']= fusion2.entrepriseLieeEntreprisePMDenomination;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.2']= fusion2.entreperiseLieeEntreprisePMFormeJuridique.getId();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.3']= fusion2.adresseFusionScission.communeAdresseFusion.getId();
if(fusion2.adresseFusionScission.numeroAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.5']= fusion2.adresseFusionScission.numeroAdresseFusion;
}
if (fusion2.adresseFusionScission.indiceAdresseFusion !== null) {
   var monId = Value('id').of(fusion2.adresseFusionScission.indiceAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.6']          = monId;
}
if(fusion2.adresseFusionScission.distributionSpecialeAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.7']= fusion2.adresseFusionScission.distributionSpecialeAdresseFusion;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.8']= fusion2.adresseFusionScission.codePostalAdresseFusion;
if(fusion2.adresseFusionScission.complementAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.10']= fusion2.adresseFusionScission.complementAdresseFusion;
}
if (fusion2.adresseFusionScission.typeAdresseFusion !== null) {
   var monId = Value('id').of(fusion2.adresseFusionScission.typeAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.11']          = monId;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.12']= fusion2.adresseFusionScission.voieAdresseFusion;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.13']= fusion2.adresseFusionScission.communeAdresseFusion.getLabel();

//Récupérer le EntityId du greffe
var greffeId = fusion2.entrepriseLieeGreffe.id ;
//Appeler Directory pour récupérer l'autorité
_log.info('l entityId du greffe selectionne est {}', greffeId);
var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', greffeId) //
		   .connectionTimeout(10000) //
		   .receiveTimeout(10000) //
		   .accept('json') //
		   .get();
//result
var receiverInfo = response.asObject();
var contactInfo = !receiverInfo.details ? null : receiverInfo.details;
//Récupérer le code EDI
var codeEDIGreffe = !contactInfo.ediCode ? null : contactInfo.ediCode;
_log.info('le code edi du greffe selectionne est {}', codeEDIGreffe);
// À modifier quand Destiny fourni le réferentiel
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.4']=codeEDIGreffe;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.6']= fusion2.entrepriseLieeSiren.split(' ').join('');
}

if (fusion2.fusionAutreSociete2) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.1']= fusion3.entrepriseLieeEntreprisePMDenomination;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.2']= fusion3.entreperiseLieeEntreprisePMFormeJuridique.getId();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.3']= fusion3.adresseFusionScission.communeAdresseFusion.getId();
if(fusion3.adresseFusionScission.numeroAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.5']= fusion3.adresseFusionScission.numeroAdresseFusion;
}
if (fusion3.adresseFusionScission.indiceAdresseFusion !== null) {
   var monId = Value('id').of(fusion3.adresseFusionScission.indiceAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.6']          = monId;
}
if(fusion3.adresseFusionScission.distributionSpecialeAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.7']= fusion3.adresseFusionScission.distributionSpecialeAdresseFusion;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.8']= fusion3.adresseFusionScission.codePostalAdresseFusion;
if(fusion3.adresseFusionScission.complementAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.10']= fusion3.adresseFusionScission.complementAdresseFusion;
}
if (fusion3.adresseFusionScission.typeAdresseFusion !== null) {
   var monId = Value('id').of(fusion3.adresseFusionScission.typeAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.11']          = monId;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.12']= fusion3.adresseFusionScission.voieAdresseFusion;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.13']= fusion3.adresseFusionScission.communeAdresseFusion.getLabel();

//Récupérer le EntityId du greffe
var greffeId = fusion3.entrepriseLieeGreffe.id ;
//Appeler Directory pour récupérer l'autorité
_log.info('l entityId du greffe selectionne est {}', greffeId);
var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', greffeId) //
		   .connectionTimeout(10000) //
		   .receiveTimeout(10000) //
		   .accept('json') //
		   .get();
//result
var receiverInfo = response.asObject();
var contactInfo = !receiverInfo.details ? null : receiverInfo.details;
//Récupérer le code EDI
var codeEDIGreffe = !contactInfo.ediCode ? null : contactInfo.ediCode;
_log.info('le code edi du greffe selectionne est {}', codeEDIGreffe);
// À modifier quand Destiny fourni le réferentiel
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.4']=codeEDIGreffe;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.6']= fusion3.entrepriseLieeSiren.split(' ').join('');
}
}

// Groupe SIU : Siège de l'entreprise

if (Value('id').of(siege.activiteLieu).eq('etablissementDomiciliationOui')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U10']= sanitize(siege.entrepriseLieeNom);
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U15']= siege.entrepriseLieeSiren.split(' ').join('');
}
if (Value('id').of(siege.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U16']= "O";
}
if (siege.cadreAdresseProfessionnelle.communeAdresseSiege != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.3']= siege.cadreAdresseProfessionnelle.communeAdresseSiege.getId();
} else {
		var idPays = siege.cadreAdresseProfessionnelle.paysAdresseSiege.getId();
		var result = idPays.match(regEx)[0]; 
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.3']= result;
}
if (siege.cadreAdresseProfessionnelle.numeroAdresseSiege != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.5']= siege.cadreAdresseProfessionnelle.numeroAdresseSiege;
}
if (siege.cadreAdresseProfessionnelle.indiceAdresseSiege !== null) {
   var monId = Value('id').of(siege.cadreAdresseProfessionnelle.indiceAdresseSiege)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.6']          = monId;
}
if (siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.7']= siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege;
}
if (siege.cadreAdresseProfessionnelle.codePostalAdresseSiege != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.8']= siege.cadreAdresseProfessionnelle.codePostalAdresseSiege;
} else if (siege.cadreAdresseProfessionnelle.codePostalAdresseSiegeEtr != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.8']= siege.cadreAdresseProfessionnelle.codePostalAdresseSiegeEtr;
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.8']= ".";
}	
if (siege.cadreAdresseProfessionnelle.complementAdresseSiege != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.10']= siege.cadreAdresseProfessionnelle.complementAdresseSiege;
}
if (siege.cadreAdresseProfessionnelle.typeAdresseSiege !== null) {
   var monId = Value('id').of(siege.cadreAdresseProfessionnelle.typeAdresseSiege)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.11']          = monId;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.12']= siege.cadreAdresseProfessionnelle.voieAdresseSiege;
if 	(siege.cadreAdresseProfessionnelle.communeAdresseSiege != null) {																		
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.13']= siege.cadreAdresseProfessionnelle.communeAdresseSiege.getLabel();
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.13']= siege.cadreAdresseProfessionnelle.villeAdresseSiege;
}
if (siege.cadreAdresseProfessionnelle.paysAdresseSiege != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.14']= siege.cadreAdresseProfessionnelle.paysAdresseSiege.getLabel();
}
if (societe.entrepriseNumeroImmatriculation != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U14/U14.2']= societe.entrepriseNumeroImmatriculation;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U14/U14.3']= societe.entrepriseLieuImmatriculation;
}

// Groupe CPU : Caractéristiques de l'entreprise

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U21']= sanitize(siege.entrepriseActivitesPrincipales);
if (etablissement.etablissementEffectifSalariePresenceOui) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U22']= etablissement.cadre3EffectifSalarie.etablissementEffectifSalarieNombre;
}

// Groupe ISU : Etablissements situés dans l'UE

if (siege.entrepriseAutreEtablissementUE) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1].U41.1']= siege.cadreAutreEtablissementUE1.lieuImmatriculationAutreEtablissementUE;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1].U41.2']= siege.cadreAutreEtablissementUE1.numeroImmatriculationAutreEtablissementUE;

if (siege.cadreAutreEtablissementUE1.adresseAutreEtablissementUE.paysAdresseAutreEtablissementUE != null) {
		var idPays = siege.cadreAutreEtablissementUE1.adresseAutreEtablissementUE.paysAdresseAutreEtablissementUE.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1].U41.3/U41.3.3'] = result;
}
if (siege.cadreAutreEtablissementUE1.adresseAutreEtablissementUE.rueNumeroAdresseAutreEtablissementUE != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1].U41.3/U41.3.5']= siege.cadreAutreEtablissementUE1.adresseAutreEtablissementUE.rueNumeroAdresseAutreEtablissementUE;
}
if (siege.cadreAutreEtablissementUE1.adresseAutreEtablissementUE.indiceVoieAdresseAutreEtablissementUE != null) {
   var monId1 = Value('id').of(siege.cadreAutreEtablissementUE1.adresseAutreEtablissementUE.indiceVoieAdresseAutreEtablissementUE)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1].U41.3/U41.3.6']          = monId1;  
} 
if (siege.cadreAutreEtablissementUE1.adresseAutreEtablissementUE.distributionSpecialeAdresseAutreEtablissementUE != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1].U41.3/U41.3.7']= siege.cadreAutreEtablissementUE1.adresseAutreEtablissementUE.distributionSpecialeAdresseAutreEtablissementUE;
}
if (siege.cadreAutreEtablissementUE1.adresseAutreEtablissementUE.codePostalAdresseAutreEtablissementUE != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1].U41.3/U41.3.8']= siege.cadreAutreEtablissementUE1.adresseAutreEtablissementUE.codePostalAdresseAutreEtablissementUE;
}
if (siege.cadreAutreEtablissementUE1.adresseAutreEtablissementUE.rueComplementAdresseAutreEtablissementUE != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1].U41.3/U41.3.10']= siege.cadreAutreEtablissementUE1.adresseAutreEtablissementUE.rueComplementAdresseAutreEtablissementUE;
}
if (siege.cadreAutreEtablissementUE1.adresseAutreEtablissementUE.typeVoieAdresseAutreEtablissementUE != null) {
   var monId1 = Value('id').of(siege.cadreAutreEtablissementUE1.adresseAutreEtablissementUE.typeVoieAdresseAutreEtablissementUE)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1].U41.3/U41.3.11']          = monId1;  
} 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1].U41.3/U41.3.12']= siege.cadreAutreEtablissementUE1.adresseAutreEtablissementUE.nomVoieAdresseAutreEtablissementUE;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1].U41.3/U41.3.13']= siege.cadreAutreEtablissementUE1.adresseAutreEtablissementUE.communeAdresseAutreEtablissementUECommune;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1].U41.3/U41.3.14']= siege.cadreAutreEtablissementUE1.adresseAutreEtablissementUE.paysAdresseAutreEtablissementUE.getLabel();

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1].U41.4']= siege.cadreAutreEtablissementUE1.activiteAutreEtablissementUE;

if (siege.cadreAutreEtablissementUE1.declarationAutreEtablissementUE) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2].U41.1']= siege.cadreAutreEtablissementUE2.lieuImmatriculationAutreEtablissementUE;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2].U41.2']= siege.cadreAutreEtablissementUE2.numeroImmatriculationAutreEtablissementUE;

if (siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.paysAdresseAutreEtablissementUE != null) {
		var idPays = siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.paysAdresseAutreEtablissementUE.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2].U41.3/U41.3.3'] = result;
}
if (siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.rueNumeroAdresseAutreEtablissementUE != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2].U41.3/U41.3.5']= siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.rueNumeroAdresseAutreEtablissementUE;
}
if (siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.indiceVoieAdresseAutreEtablissementUE != null) {
   var monId1 = Value('id').of(siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.indiceVoieAdresseAutreEtablissementUE)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2].U41.3/U41.3.6']          = monId1;  
} 
if (siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.distributionSpecialeAdresseAutreEtablissementUE != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2].U41.3/U41.3.7']= siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.distributionSpecialeAdresseAutreEtablissementUE;
}
if (siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.codePostalAdresseAutreEtablissementUE != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2].U41.3/U41.3.8']= siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.codePostalAdresseAutreEtablissementUE;
}
if (siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.rueComplementAdresseAutreEtablissementUE != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2].U41.3/U41.3.10']= siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.rueComplementAdresseAutreEtablissementUE;
}
if (siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.typeVoieAdresseAutreEtablissementUE != null) {
   var monId1 = Value('id').of(siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.typeVoieAdresseAutreEtablissementUE)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2].U41.3/U41.3.11']          = monId1;  
} 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2].U41.3/U41.3.12']= siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.nomVoieAdresseAutreEtablissementUE;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2].U41.3/U41.3.13']= siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.communeAdresseAutreEtablissementUECommune;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2].U41.3/U41.3.14']= siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.paysAdresseAutreEtablissementUE.getLabel();

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2].U41.4']= siege.cadreAutreEtablissementUE2.activiteAutreEtablissementUE;
}
}

// RFU : Régime fiscal de l'entreprise

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U31']= Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberale).eq('regimeFiscalRegimeImpositionBeneficesDcBNC') ? "111" :
																		((Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeCommerciale).eq('regimeFiscalRegimeImpositionBeneficesReelBIC') and Value('id').of(fiscal.impositionBenefices.regimeFiscalReel).eq('regimeFiscalRegimeImpositionBeneficesRs')) ? "112" : 
																		((Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeCommerciale).eq('regimeFiscalRegimeImpositionBeneficesReelBIC') and Value('id').of(fiscal.impositionBenefices.regimeFiscalReel).eq('regimeFiscalRegimeImpositionBeneficesRn')) ? "113" : 
																		(((Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeCommerciale).eq('regimeFiscalRegimeImpositionBeneficesOptionIS') or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberale).eq('regimeFiscalRegimeImpositionBeneficesOptionIS')) and Value('id').of(fiscal.impositionBenefices.regimeFiscalReel).eq('regimeFiscalRegimeImpositionBeneficesRs')) ? "114" : "115")));

if ((Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeCommerciale).eq('regimeFiscalRegimeImpositionBeneficesOptionIS') or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberale).eq('regimeFiscalRegimeImpositionBeneficesOptionIS')) 
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5306') or Value('id').of(societe.entrepriseFormeJuridique).eq('5307')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5202') or Value('id').of(societe.entrepriseFormeJuridique).eq('5203'))) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U32']= "211";
} else if ((Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberale).eq('regimeFiscalRegimeImpositionBeneficesDcBNC') or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeCommerciale).eq('regimeFiscalRegimeImpositionBeneficesReelBIC'))
		and not Value('id').of(societe.entrepriseFormeJuridique).eq('5306') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5307')
		and not Value('id').of(societe.entrepriseFormeJuridique).eq('5202') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5203')
		and not Value('id').of(societe.entrepriseFormeJuridique).eq('3110') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5800')
		and not Value('id').of(societe.entrepriseFormeJuridique).eq('3120')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U32']= "212";
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.1']= (Value('id').of(fiscal.optionsTVA.regimeTVA1).eq('regimeFiscalRegimeImpositionTVARfTVA') or Value('id').of(fiscal.optionsTVA.regimeTVA2).eq('regimeFiscalRegimeImpositionTVARfTVA') or Value('id').of(fiscal.optionsTVA.regimeTVA3).eq('regimeFiscalRegimeImpositionTVARfTVA')) ? "310" :
																			   ((Value('id').of(fiscal.optionsTVA.regimeTVA1).eq('regimeFiscalRegimeImpositionTVARsTVA') or Value('id').of(fiscal.optionsTVA.regimeTVA3).eq('regimeFiscalRegimeImpositionTVARsTVA')) ? "311" :
																			   ((Value('id').of(fiscal.optionsTVA.regimeTVA2).eq('regimeFiscalRegimeImpositionTVARnTVA') or Value('id').of(fiscal.optionsTVA.regimeTVA3).eq('regimeFiscalRegimeImpositionTVARnTVA')) ? "312" : "313"));

if (fiscal.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres1 or fiscal.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres2) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.2']= fiscal.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres1 ? "410" : "411";
}

if(societe.entrepriseDateClotureExerciceSocial != null) {
	var dateTemp = new Date(parseInt(societe.entrepriseDateClotureExerciceSocial.getTimeInMillis()));
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = "--" + month + "-" + day ; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U37']          = date;
}

// Groupe OIU : Options fiscales avec incidence sociale

if ((Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeCommerciale).eq('regimeFiscalRegimeImpositionBeneficesOptionIS') or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberale).eq('regimeFiscalRegimeImpositionBeneficesOptionIS')) 
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5306') or Value('id').of(societe.entrepriseFormeJuridique).eq('5307')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5202') or Value('id').of(societe.entrepriseFormeJuridique).eq('5203'))) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OIU/U75']= "211";
} else if ((Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberale).eq('regimeFiscalRegimeImpositionBeneficesDcBNC') or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeCommerciale).eq('regimeFiscalRegimeImpositionBeneficesReelBIC'))
		and not Value('id').of(societe.entrepriseFormeJuridique).eq('5306') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5307')
		and not Value('id').of(societe.entrepriseFormeJuridique).eq('5202') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5203')
		and not Value('id').of(societe.entrepriseFormeJuridique).eq('3110') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5800')
		and not Value('id').of(societe.entrepriseFormeJuridique).eq('3120')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OIU/U75']= "212";
}

// Groupe GRD : Dirigeant personne physique

var occurrencesGRD = [];
var occurrence = {};

// Dirigeant 1 

if (Value('id').of(dirigeant1.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant1.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant1.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant1.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant1.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant1.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant1.personneLieeQualite).eq('63')
	or ((Value('id').of(dirigeant1.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant1.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant1.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant1.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant1.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant1.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))) {

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant1.personneLieeQualite.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
} else if (dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
}
if (dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(dirigeant1.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null ) {
	var dateTemp = new Date(parseInt(dirigeant1.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
} else if(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeant1.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant1.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (Value('id').of(dirigeant1.personneLieeQualite).eq('28') or Value('id').of(dirigeant1.personneLieeQualite).eq('74')) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant1.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant1.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant1.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant1.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant1.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant1.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant1.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant1.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant1.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant1.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 2

if (Value('id').of(dirigeant2.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant2.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant2.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant2.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant2.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant2.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant2.personneLieeQualite).eq('63')
	or ((Value('id').of(dirigeant2.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant2.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant2.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant2.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant2.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant2.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))) {

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant2.personneLieeQualite.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
} else if (dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
}
if (dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(dirigeant2.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant2.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
} else if(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeant2.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant2.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (Value('id').of(dirigeant2.personneLieeQualite).eq('28') or Value('id').of(dirigeant2.personneLieeQualite).eq('74')) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant2.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant2.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant2.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant2.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant2.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant2.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant2.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant2.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant2.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant2.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 3

if (Value('id').of(dirigeant3.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant3.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant3.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant3.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant3.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant3.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant3.personneLieeQualite).eq('63')
	or ((Value('id').of(dirigeant3.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant3.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant3.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant3.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant3.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant3.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))) {

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant3.personneLieeQualite.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
} else if (dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
}
if (dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(dirigeant3.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant3.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
} else if(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeant3.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant3.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (Value('id').of(dirigeant3.personneLieeQualite).eq('28') or Value('id').of(dirigeant3.personneLieeQualite).eq('74')) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant3.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant3.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant3.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant3.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant3.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant3.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant3.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant3.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant3.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant3.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 4

if (Value('id').of(dirigeant4.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant4.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant4.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant4.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant4.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant4.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant4.personneLieeQualite).eq('63')
	or ((Value('id').of(dirigeant4.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant4.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant4.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant4.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant4.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant4.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))) {

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant4.personneLieeQualite.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
} else if (dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
}
if (dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(dirigeant4.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant4.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
} else if(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeant4.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant4.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (Value('id').of(dirigeant4.personneLieeQualite).eq('28') or Value('id').of(dirigeant4.personneLieeQualite).eq('74')) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant4.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant4.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant4.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant4.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant4.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant4.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant4.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant4.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant4.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant4.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 5

if (Value('id').of(dirigeant5.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant5.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant5.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant5.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant5.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant5.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant5.personneLieeQualite).eq('63')
	or ((Value('id').of(dirigeant5.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant5.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant5.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant5.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant5.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant5.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))) {

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant5.personneLieeQualite.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
} else if (dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
}
if (dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(dirigeant5.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant5.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
} else if(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeant5.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant5.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (Value('id').of(dirigeant5.personneLieeQualite).eq('28') or Value('id').of(dirigeant5.personneLieeQualite).eq('74')) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant5.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant5.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant5.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant5.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant5.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant5.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant5.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant5.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant5.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant5.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 6

if (Value('id').of(dirigeant6.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant6.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant6.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant6.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant6.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant6.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant6.personneLieeQualite).eq('63')
	or ((Value('id').of(dirigeant6.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant6.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant6.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant6.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant6.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant6.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))) {

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant6.personneLieeQualite.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
} else if (dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
}
if (dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(dirigeant6.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant6.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
} else if(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeant6.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant6.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (Value('id').of(dirigeant6.personneLieeQualite).eq('28') or Value('id').of(dirigeant6.personneLieeQualite).eq('74')) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant6.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant6.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant6.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant6.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant6.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant6.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant6.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant6.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant6.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant6.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 7

if (Value('id').of(dirigeant7.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant7.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant7.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant7.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant7.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant7.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant7.personneLieeQualite).eq('63')
	or ((Value('id').of(dirigeant7.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant7.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant7.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant7.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant7.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant7.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))) {

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant7.personneLieeQualite.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
} else if (dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
}
if (dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(dirigeant7.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant7.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
} else if(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeant7.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant7.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (Value('id').of(dirigeant7.personneLieeQualite).eq('28') or Value('id').of(dirigeant7.personneLieeQualite).eq('74')) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant7.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant7.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant7.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant7.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant7.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant7.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant7.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant7.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant7.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant7.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant7.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant7.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant7.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant7.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 8

if (Value('id').of(dirigeant8.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant8.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant8.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant8.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant8.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant8.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant8.personneLieeQualite).eq('63')
	or ((Value('id').of(dirigeant8.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant8.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant8.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant8.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant8.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant8.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))) {

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant8.personneLieeQualite.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
} else if (dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
}
if (dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(dirigeant8.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant8.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
} else if(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeant8.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant8.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (Value('id').of(dirigeant8.personneLieeQualite).eq('28') or Value('id').of(dirigeant8.personneLieeQualite).eq('74')) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant8.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant8.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant8.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant8.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant8.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant8.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant8.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant8.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant8.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant8.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant8.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant8.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant8.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant8.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 9

if (Value('id').of(dirigeant9.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant9.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant9.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant9.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant9.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant9.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant9.personneLieeQualite).eq('63')
	or ((Value('id').of(dirigeant9.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant9.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant9.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant9.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant9.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant9.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))) {

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant9.personneLieeQualite.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeant9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
} else if (dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
}
if (dirigeant9.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeant9.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant9.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(dirigeant9.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant9.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
} else if(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant9.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeant9.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant9.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (Value('id').of(dirigeant9.personneLieeQualite).eq('28') or Value('id').of(dirigeant9.personneLieeQualite).eq('74')) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant9.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant9.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant9.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant9.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant9.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant9.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant9.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant9.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant9.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant9.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant9.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant9.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant9.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant9.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 10

if (Value('id').of(dirigeant10.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant10.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant10.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant10.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant10.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant10.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant10.personneLieeQualite).eq('63')
	or ((Value('id').of(dirigeant10.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant10.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant10.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant10.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant10.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant10.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))) {

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant10.personneLieeQualite.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
} else if (dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
}
if (dirigeant10.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeant10.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant10.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(dirigeant10.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant10.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
} else if(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant10.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeant10.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant10.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (Value('id').of(dirigeant10.personneLieeQualite).eq('28') or Value('id').of(dirigeant10.personneLieeQualite).eq('74')) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant10.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant10.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant10.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant10.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant10.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant10.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant10.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant10.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant10.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant10.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant10.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant10.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant10.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant10.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 11

if (Value('id').of(dirigeant11.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant11.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant11.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant11.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant11.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant11.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant11.personneLieeQualite).eq('63')
	or ((Value('id').of(dirigeant11.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant11.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant11.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant11.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant11.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant11.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))) {

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant11.personneLieeQualite.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeant11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
} else if (dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
}
if (dirigeant11.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeant11.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant11.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(dirigeant11.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant11.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
} else if(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant11.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeant11.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant11.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (Value('id').of(dirigeant11.personneLieeQualite).eq('28') or Value('id').of(dirigeant11.personneLieeQualite).eq('74')) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant11.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant11.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant11.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant11.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant11.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant11.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant11.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant11.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant11.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant11.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant11.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant11.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant11.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant11.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 12

if (Value('id').of(dirigeant12.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant12.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant12.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant12.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant12.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant12.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant12.personneLieeQualite).eq('63')
	or ((Value('id').of(dirigeant12.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant12.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant12.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant12.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant12.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant12.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))) {

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant12.personneLieeQualite.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeant12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
} else if (dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
}
if (dirigeant12.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeant12.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant12.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(dirigeant12.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant12.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
} else if(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant12.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeant12.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant12.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (Value('id').of(dirigeant12.personneLieeQualite).eq('28') or Value('id').of(dirigeant12.personneLieeQualite).eq('74')) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant12.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant12.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant12.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant12.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant12.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant12.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant12.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant12.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant12.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant12.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant12.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant12.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant12.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant12.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 13

if (Value('id').of(dirigeant13.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant13.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant13.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant13.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant13.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant13.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant13.personneLieeQualite).eq('63')
	or ((Value('id').of(dirigeant13.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant13.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant13.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant13.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant13.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant13.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))) {

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant13.personneLieeQualite.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeant13.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant13.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeant13.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant13.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeant13.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant13.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
} else if (dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
}
if (dirigeant13.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeant13.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant13.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(dirigeant13.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant13.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
} else if(dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeant13.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant13.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeant13.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant13.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant13.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant13.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant13.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeant13.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant13.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant13.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeant13.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeant13.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant13.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant13.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeant13.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeant13.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant13.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant13.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeant13.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeant13.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeant13.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant13.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant13.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeant13.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant13.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant13.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeant13.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant13.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant13.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant13.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant13.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant13.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeant13.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeant13.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant13.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (Value('id').of(dirigeant13.personneLieeQualite).eq('28') or Value('id').of(dirigeant13.personneLieeQualite).eq('74')) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant13.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant13.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant13.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant13.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant13.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant13.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant13.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant13.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant13.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant13.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant13.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant13.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant13.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant13.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant13.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant13.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant13.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant13.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant13.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 14

if (Value('id').of(dirigeant14.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant14.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant14.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant14.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant14.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant14.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant14.personneLieeQualite).eq('63')
	or ((Value('id').of(dirigeant14.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant14.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant14.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant14.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant14.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant14.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))) {

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant14.personneLieeQualite.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeant14.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant14.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeant14.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant14.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeant14.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant14.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
} else if (dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
}
if (dirigeant14.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeant14.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant14.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(dirigeant14.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant14.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
} else if(dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeant14.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant14.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeant14.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant14.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant14.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant14.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant14.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeant14.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant14.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant14.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeant14.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeant14.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant14.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant14.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeant14.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeant14.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant14.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant14.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeant14.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeant14.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeant14.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant14.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant14.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeant14.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant14.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant14.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeant14.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant14.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant14.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant14.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant14.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant14.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeant14.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeant14.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant14.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (Value('id').of(dirigeant14.personneLieeQualite).eq('28') or Value('id').of(dirigeant14.personneLieeQualite).eq('74')) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant14.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant14.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant14.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant14.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant14.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant14.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant14.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant14.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant14.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant14.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant14.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant14.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant14.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant14.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant14.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant14.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant14.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant14.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant14.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

occurrencesGRD.forEach(function (value, i) {
	var idx = i+1;
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/DIU/U50/U50.1']= value['U50.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/DIU/U50/U50.2']= value['U50.2'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/DIU/U51/U51.1']= value['U51.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/DIU/U52']= value['U52'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D01/D01.2']= value['D01.2'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D01/D01.3']= value['D01.3'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D01/D01.4']= value['D01.4'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D03/D03.1']= value['D03.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D03/D03.2']= value['D03.2'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D03/D03.3']= value['D03.3'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D03/D03.4']= value['D03.4'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.3']= value['D04.3'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.5']= value['D04.5'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.6']= value['D04.6'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.7']= value['D04.7'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.8']= value['D04.8'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.10']= value['D04.10'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.11']= value['D04.11'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.12']= value['D04.12'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.13']= value['D04.13'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.14']= value['D04.14'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/NPD/D21']= value['D21'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/ISS/A10/A10.1']= value['A10.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/ISS/A10/A10.2']= value['A10.2'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A21/A21.2']= value['A21.2'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A21/A21.3']= value['A21.3'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A21/A21.4']= value['A21.4'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A21/A21.6']= value['A21.6'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A22/A22.3']= value['A22.3'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A22/A22.4']= value['A22.4'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A24/A24.2']= value['A24.2'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A24/A24.3']= value['A24.3'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/CAS/A42/A42.1']= value['A42.1'];
});


// Groupe GDR : Dirigeant personne morale

var occurrencesGDR = [];
var occurrence = {};

// Dirigeant 1 

if (Value('id').of(dirigeant1.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant1.personneLieeQualite.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeant1.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeant1.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
if (dirigeant1.civiliteRepresentant.personneLieePPNomNaissance != null) {
occurrence['R08'] = "1";
occurrence['R04.2'] = dirigeant1.civiliteRepresentant.personneLieePPNomNaissance;
var prenoms=[];
for (i = 0; i < dirigeant1.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(dirigeant1.civiliteRepresentant.personneLieePPPrenom[i]);
} occurrence['R04.3']= prenoms;
if (dirigeant1.civiliteRepresentant.personneLieePPNomUsage != null) {
occurrence['R04.4'] = dirigeant1.civiliteRepresentant.personneLieePPNomUsage;
}
occurrence['R05.3']= dirigeant1.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getId();
if (dirigeant1.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null) {
occurrence['R05.5']= dirigeant1.civiliteRepresentant.adresseRepresentant.representantNumeroVoie;
}
if (dirigeant1.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant1.civiliteRepresentant.adresseRepresentant.representantIndiceVoie)._eval();
   occurrence['R05.6']          = monId1;  
} 
if (dirigeant1.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null) {
occurrence['R05.7']= dirigeant1.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale;
}
occurrence['R05.8']= dirigeant1.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
if (dirigeant1.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null) {
occurrence['R05.10']= dirigeant1.civiliteRepresentant.adresseRepresentant.representantComplementVoie;
}
if (dirigeant1.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant1.civiliteRepresentant.adresseRepresentant.representantTypeVoie)._eval();
   occurrence['R05.11']          = monId1;  
} 
occurrence['R05.12']= dirigeant1.civiliteRepresentant.adresseRepresentant.representantNomVoie;
occurrence['R05.13']= dirigeant1.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getLabel();
if(dirigeant1.civiliteRepresentant.personneLieePPDateNaissance !== null) {
	var dateTemp = new Date(parseInt(dirigeant1.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['R06.1']= date;
}
 if (not Value('id').of(dirigeant1.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = dirigeant1.civiliteRepresentant.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['R06.2']= result;
} else if (Value('id').of(dirigeant1.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	occurrence['R06.2'] = dirigeant1.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getId();
} 		
if (not Value('id').of(dirigeant1.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
	occurrence['R06.3'] = dirigeant1.civiliteRepresentant.personneLieePPLieuNaissancePays.getLabel();
}
if (Value('id').of(dirigeant1.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
	occurrence['R06.4'] = dirigeant1.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getLabel();
}
occurrence['R07']= dirigeant1.civiliteRepresentant.personneLieePPNationalite;
}
occurrence['R22.2']= dirigeant1.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant1.civilitePersonneMorale.personneLieePMNumeroIdentification : dirigeant1.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee;
occurrence['R22.3']= dirigeant1.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 2 

if (Value('id').of(dirigeant2.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant2.personneLieeQualite.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeant2.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeant2.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
if (dirigeant2.civiliteRepresentant.personneLieePPNomNaissance != null) {
occurrence['R08'] = "1";
occurrence['R04.2'] = dirigeant2.civiliteRepresentant.personneLieePPNomNaissance;
var prenoms=[];
for (i = 0; i < dirigeant2.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(dirigeant2.civiliteRepresentant.personneLieePPPrenom[i]);
} occurrence['R04.3']= prenoms;
if (dirigeant2.civiliteRepresentant.personneLieePPNomUsage != null) {
occurrence['R04.4'] = dirigeant2.civiliteRepresentant.personneLieePPNomUsage;
}
occurrence['R05.3']= dirigeant2.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getId();
if (dirigeant2.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null) {
occurrence['R05.5']= dirigeant2.civiliteRepresentant.adresseRepresentant.representantNumeroVoie;
}
if (dirigeant2.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant2.civiliteRepresentant.adresseRepresentant.representantIndiceVoie)._eval();
   occurrence['R05.6']          = monId1;  
} 
if (dirigeant2.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null) {
occurrence['R05.7']= dirigeant2.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale;
}
occurrence['R05.8']= dirigeant2.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
if (dirigeant2.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null) {
occurrence['R05.10']= dirigeant2.civiliteRepresentant.adresseRepresentant.representantComplementVoie;
}
if (dirigeant2.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant2.civiliteRepresentant.adresseRepresentant.representantTypeVoie)._eval();
   occurrence['R05.11']          = monId1;  
} 
occurrence['R05.12']= dirigeant2.civiliteRepresentant.adresseRepresentant.representantNomVoie;
occurrence['R05.13']= dirigeant2.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getLabel();
if(dirigeant2.civiliteRepresentant.personneLieePPDateNaissance !== null) {
	var dateTemp = new Date(parseInt(dirigeant2.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['R06.1']= date;
}
 if (not Value('id').of(dirigeant2.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = dirigeant2.civiliteRepresentant.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['R06.2']= result;
} else if (Value('id').of(dirigeant2.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	occurrence['R06.2'] = dirigeant2.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getId();
} 		
if (not Value('id').of(dirigeant2.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
	occurrence['R06.3'] = dirigeant2.civiliteRepresentant.personneLieePPLieuNaissancePays.getLabel();
}
if (Value('id').of(dirigeant2.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
	occurrence['R06.4'] = dirigeant2.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getLabel();
}
occurrence['R07']= dirigeant2.civiliteRepresentant.personneLieePPNationalite;
}
occurrence['R22.2']= dirigeant2.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant2.civilitePersonneMorale.personneLieePMNumeroIdentification : dirigeant2.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee;
occurrence['R22.3']= dirigeant2.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 3 

if (Value('id').of(dirigeant3.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant3.personneLieeQualite.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeant3.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeant3.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
if (dirigeant3.civiliteRepresentant.personneLieePPNomNaissance != null) {
occurrence['R08'] = "1";
occurrence['R04.2'] = dirigeant3.civiliteRepresentant.personneLieePPNomNaissance;
var prenoms=[];
for (i = 0; i < dirigeant3.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(dirigeant3.civiliteRepresentant.personneLieePPPrenom[i]);
} occurrence['R04.3']= prenoms;
if (dirigeant3.civiliteRepresentant.personneLieePPNomUsage != null) {
occurrence['R04.4'] = dirigeant3.civiliteRepresentant.personneLieePPNomUsage;
}
occurrence['R05.3']= dirigeant3.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getId();
if (dirigeant3.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null) {
occurrence['R05.5']= dirigeant3.civiliteRepresentant.adresseRepresentant.representantNumeroVoie;
}
if (dirigeant3.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant3.civiliteRepresentant.adresseRepresentant.representantIndiceVoie)._eval();
   occurrence['R05.6']          = monId1;  
} 
if (dirigeant3.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null) {
occurrence['R05.7']= dirigeant3.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale;
}
occurrence['R05.8']= dirigeant3.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
if (dirigeant3.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null) {
occurrence['R05.10']= dirigeant3.civiliteRepresentant.adresseRepresentant.representantComplementVoie;
}
if (dirigeant3.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant3.civiliteRepresentant.adresseRepresentant.representantTypeVoie)._eval();
   occurrence['R05.11']          = monId1;  
} 
occurrence['R05.12']= dirigeant3.civiliteRepresentant.adresseRepresentant.representantNomVoie;
occurrence['R05.13']= dirigeant3.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getLabel();
if(dirigeant3.civiliteRepresentant.personneLieePPDateNaissance !== null) {
	var dateTemp = new Date(parseInt(dirigeant3.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['R06.1']= date;
}
 if (not Value('id').of(dirigeant3.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = dirigeant3.civiliteRepresentant.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['R06.2']= result;
} else if (Value('id').of(dirigeant3.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	occurrence['R06.2'] = dirigeant3.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getId();
} 		
if (not Value('id').of(dirigeant3.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
	occurrence['R06.3'] = dirigeant3.civiliteRepresentant.personneLieePPLieuNaissancePays.getLabel();
}
if (Value('id').of(dirigeant3.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
	occurrence['R06.4'] = dirigeant3.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getLabel();
}
occurrence['R07']= dirigeant3.civiliteRepresentant.personneLieePPNationalite;
}
occurrence['R22.2']= dirigeant3.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant3.civilitePersonneMorale.personneLieePMNumeroIdentification : dirigeant3.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee;
occurrence['R22.3']= dirigeant3.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 4

if (Value('id').of(dirigeant4.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant4.personneLieeQualite.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeant4.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeant4.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
if (dirigeant4.civiliteRepresentant.personneLieePPNomNaissance != null) {
occurrence['R08'] = "1";
occurrence['R04.2'] = dirigeant4.civiliteRepresentant.personneLieePPNomNaissance;
var prenoms=[];
for (i = 0; i < dirigeant4.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(dirigeant4.civiliteRepresentant.personneLieePPPrenom[i]);
} occurrence['R04.3']= prenoms;
if (dirigeant4.civiliteRepresentant.personneLieePPNomUsage != null) {
occurrence['R04.4'] = dirigeant4.civiliteRepresentant.personneLieePPNomUsage;
}
occurrence['R05.3']= dirigeant4.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getId();
if (dirigeant4.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null) {
occurrence['R05.5']= dirigeant4.civiliteRepresentant.adresseRepresentant.representantNumeroVoie;
}
if (dirigeant4.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant4.civiliteRepresentant.adresseRepresentant.representantIndiceVoie)._eval();
   occurrence['R05.6']          = monId1;  
} 
if (dirigeant4.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null) {
occurrence['R05.7']= dirigeant4.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale;
}
occurrence['R05.8']= dirigeant4.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
if (dirigeant4.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null) {
occurrence['R05.10']= dirigeant4.civiliteRepresentant.adresseRepresentant.representantComplementVoie;
}
if (dirigeant4.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant4.civiliteRepresentant.adresseRepresentant.representantTypeVoie)._eval();
   occurrence['R05.11']          = monId1;  
} 
occurrence['R05.12']= dirigeant4.civiliteRepresentant.adresseRepresentant.representantNomVoie;
occurrence['R05.13']= dirigeant4.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getLabel();
if(dirigeant4.civiliteRepresentant.personneLieePPDateNaissance !== null) {
	var dateTemp = new Date(parseInt(dirigeant4.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['R06.1']= date;
}
 if (not Value('id').of(dirigeant4.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = dirigeant4.civiliteRepresentant.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['R06.2']= result;
} else if (Value('id').of(dirigeant4.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	occurrence['R06.2'] = dirigeant4.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getId();
} 		
if (not Value('id').of(dirigeant4.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
	occurrence['R06.3'] = dirigeant4.civiliteRepresentant.personneLieePPLieuNaissancePays.getLabel();
}
if (Value('id').of(dirigeant4.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
	occurrence['R06.4'] = dirigeant4.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getLabel();
}
occurrence['R07']= dirigeant4.civiliteRepresentant.personneLieePPNationalite;
}
occurrence['R22.2']= dirigeant4.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant4.civilitePersonneMorale.personneLieePMNumeroIdentification : dirigeant4.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee;
occurrence['R22.3']= dirigeant4.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 5 

if (Value('id').of(dirigeant5.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant5.personneLieeQualite.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeant5.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeant5.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
if (dirigeant5.civiliteRepresentant.personneLieePPNomNaissance != null) {
occurrence['R08'] = "1";
occurrence['R04.2'] = dirigeant5.civiliteRepresentant.personneLieePPNomNaissance;
var prenoms=[];
for (i = 0; i < dirigeant5.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(dirigeant5.civiliteRepresentant.personneLieePPPrenom[i]);
} occurrence['R04.3']= prenoms;
if (dirigeant5.civiliteRepresentant.personneLieePPNomUsage != null) {
occurrence['R04.4'] = dirigeant5.civiliteRepresentant.personneLieePPNomUsage;
}
occurrence['R05.3']= dirigeant5.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getId();
if (dirigeant5.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null) {
occurrence['R05.5']= dirigeant5.civiliteRepresentant.adresseRepresentant.representantNumeroVoie;
}
if (dirigeant5.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant5.civiliteRepresentant.adresseRepresentant.representantIndiceVoie)._eval();
   occurrence['R05.6']          = monId1;  
} 
if (dirigeant5.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null) {
occurrence['R05.7']= dirigeant5.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale;
}
occurrence['R05.8']= dirigeant5.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
if (dirigeant5.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null) {
occurrence['R05.10']= dirigeant5.civiliteRepresentant.adresseRepresentant.representantComplementVoie;
}
if (dirigeant5.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant5.civiliteRepresentant.adresseRepresentant.representantTypeVoie)._eval();
   occurrence['R05.11']          = monId1;  
} 
occurrence['R05.12']= dirigeant5.civiliteRepresentant.adresseRepresentant.representantNomVoie;
occurrence['R05.13']= dirigeant5.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getLabel();
if(dirigeant5.civiliteRepresentant.personneLieePPDateNaissance !== null) {
	var dateTemp = new Date(parseInt(dirigeant5.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['R06.1']= date;
}
 if (not Value('id').of(dirigeant5.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = dirigeant5.civiliteRepresentant.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['R06.2']= result;
} else if (Value('id').of(dirigeant5.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	occurrence['R06.2'] = dirigeant5.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getId();
} 		
if (not Value('id').of(dirigeant5.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
	occurrence['R06.3'] = dirigeant5.civiliteRepresentant.personneLieePPLieuNaissancePays.getLabel();
}
if (Value('id').of(dirigeant5.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
	occurrence['R06.4'] = dirigeant5.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getLabel();
}
occurrence['R07']= dirigeant5.civiliteRepresentant.personneLieePPNationalite;
}
occurrence['R22.2']= dirigeant5.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant5.civilitePersonneMorale.personneLieePMNumeroIdentification : dirigeant5.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee;
occurrence['R22.3']= dirigeant5.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 6 

if (Value('id').of(dirigeant6.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant6.personneLieeQualite.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeant6.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeant6.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
if (dirigeant6.civiliteRepresentant.personneLieePPNomNaissance != null) {
occurrence['R08'] = "1";
occurrence['R04.2'] = dirigeant6.civiliteRepresentant.personneLieePPNomNaissance;
var prenoms=[];
for (i = 0; i < dirigeant6.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(dirigeant6.civiliteRepresentant.personneLieePPPrenom[i]);
} occurrence['R04.3']= prenoms;
if (dirigeant6.civiliteRepresentant.personneLieePPNomUsage != null) {
occurrence['R04.4'] = dirigeant6.civiliteRepresentant.personneLieePPNomUsage;
}
occurrence['R05.3']= dirigeant6.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getId();
if (dirigeant6.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null) {
occurrence['R05.5']= dirigeant6.civiliteRepresentant.adresseRepresentant.representantNumeroVoie;
}
if (dirigeant6.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant6.civiliteRepresentant.adresseRepresentant.representantIndiceVoie)._eval();
   occurrence['R05.6']          = monId1;  
} 
if (dirigeant6.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null) {
occurrence['R05.7']= dirigeant6.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale;
}
occurrence['R05.8']= dirigeant6.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
if (dirigeant6.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null) {
occurrence['R05.10']= dirigeant6.civiliteRepresentant.adresseRepresentant.representantComplementVoie;
}
if (dirigeant6.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant6.civiliteRepresentant.adresseRepresentant.representantTypeVoie)._eval();
   occurrence['R05.11']          = monId1;  
} 
occurrence['R05.12']= dirigeant6.civiliteRepresentant.adresseRepresentant.representantNomVoie;
occurrence['R05.13']= dirigeant6.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getLabel();
if(dirigeant6.civiliteRepresentant.personneLieePPDateNaissance !== null) {
	var dateTemp = new Date(parseInt(dirigeant6.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['R06.1']= date;
}
 if (not Value('id').of(dirigeant6.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = dirigeant6.civiliteRepresentant.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['R06.2']= result;
} else if (Value('id').of(dirigeant6.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	occurrence['R06.2'] = dirigeant6.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getId();
} 		
if (not Value('id').of(dirigeant6.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
	occurrence['R06.3'] = dirigeant6.civiliteRepresentant.personneLieePPLieuNaissancePays.getLabel();
}
if (Value('id').of(dirigeant6.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
	occurrence['R06.4'] = dirigeant6.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getLabel();
}
occurrence['R07']= dirigeant6.civiliteRepresentant.personneLieePPNationalite;
}
occurrence['R22.2']= dirigeant6.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant6.civilitePersonneMorale.personneLieePMNumeroIdentification : dirigeant6.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee;
occurrence['R22.3']= dirigeant6.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 7 

if (Value('id').of(dirigeant7.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant7.personneLieeQualite.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeant7.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeant7.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
if (dirigeant7.civiliteRepresentant.personneLieePPNomNaissance != null) {
occurrence['R08'] = "1";
occurrence['R04.2'] = dirigeant7.civiliteRepresentant.personneLieePPNomNaissance;
var prenoms=[];
for (i = 0; i < dirigeant7.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(dirigeant7.civiliteRepresentant.personneLieePPPrenom[i]);
} occurrence['R04.3']= prenoms;
if (dirigeant7.civiliteRepresentant.personneLieePPNomUsage != null) {
occurrence['R04.4'] = dirigeant7.civiliteRepresentant.personneLieePPNomUsage;
}
occurrence['R05.3']= dirigeant7.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getId();
if (dirigeant7.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null) {
occurrence['R05.5']= dirigeant7.civiliteRepresentant.adresseRepresentant.representantNumeroVoie;
}
if (dirigeant7.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant7.civiliteRepresentant.adresseRepresentant.representantIndiceVoie)._eval();
   occurrence['R05.6']          = monId1;  
} 
if (dirigeant7.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null) {
occurrence['R05.7']= dirigeant7.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale;
}
occurrence['R05.8']= dirigeant7.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
if (dirigeant7.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null) {
occurrence['R05.10']= dirigeant7.civiliteRepresentant.adresseRepresentant.representantComplementVoie;
}
if (dirigeant7.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant7.civiliteRepresentant.adresseRepresentant.representantTypeVoie)._eval();
   occurrence['R05.11']          = monId1;  
} 
occurrence['R05.12']= dirigeant7.civiliteRepresentant.adresseRepresentant.representantNomVoie;
occurrence['R05.13']= dirigeant7.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getLabel();
if(dirigeant7.civiliteRepresentant.personneLieePPDateNaissance !== null) {
	var dateTemp = new Date(parseInt(dirigeant7.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['R06.1']= date;
}
 if (not Value('id').of(dirigeant7.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = dirigeant7.civiliteRepresentant.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['R06.2']= result;
} else if (Value('id').of(dirigeant7.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	occurrence['R06.2'] = dirigeant7.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getId();
} 		
if (not Value('id').of(dirigeant7.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
	occurrence['R06.3'] = dirigeant7.civiliteRepresentant.personneLieePPLieuNaissancePays.getLabel();
}
if (Value('id').of(dirigeant7.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
	occurrence['R06.4'] = dirigeant7.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getLabel();
}
occurrence['R07']= dirigeant7.civiliteRepresentant.personneLieePPNationalite;
}
occurrence['R22.2']= dirigeant7.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant7.civilitePersonneMorale.personneLieePMNumeroIdentification : dirigeant7.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee;
occurrence['R22.3']= dirigeant7.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 8

if (Value('id').of(dirigeant8.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant8.personneLieeQualite.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeant8.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeant8.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
if (dirigeant8.civiliteRepresentant.personneLieePPNomNaissance != null) {
occurrence['R08'] = "1";
occurrence['R04.2'] = dirigeant8.civiliteRepresentant.personneLieePPNomNaissance;
var prenoms=[];
for (i = 0; i < dirigeant8.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(dirigeant8.civiliteRepresentant.personneLieePPPrenom[i]);
} occurrence['R04.3']= prenoms;
if (dirigeant8.civiliteRepresentant.personneLieePPNomUsage != null) {
occurrence['R04.4'] = dirigeant8.civiliteRepresentant.personneLieePPNomUsage;
}
occurrence['R05.3']= dirigeant8.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getId();
if (dirigeant8.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null) {
occurrence['R05.5']= dirigeant8.civiliteRepresentant.adresseRepresentant.representantNumeroVoie;
}
if (dirigeant8.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant8.civiliteRepresentant.adresseRepresentant.representantIndiceVoie)._eval();
   occurrence['R05.6']          = monId1;  
} 
if (dirigeant8.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null) {
occurrence['R05.7']= dirigeant8.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale;
}
occurrence['R05.8']= dirigeant8.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
if (dirigeant8.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null) {
occurrence['R05.10']= dirigeant8.civiliteRepresentant.adresseRepresentant.representantComplementVoie;
}
if (dirigeant8.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant8.civiliteRepresentant.adresseRepresentant.representantTypeVoie)._eval();
   occurrence['R05.11']          = monId1;  
} 
occurrence['R05.12']= dirigeant8.civiliteRepresentant.adresseRepresentant.representantNomVoie;
occurrence['R05.13']= dirigeant8.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getLabel();
if(dirigeant8.civiliteRepresentant.personneLieePPDateNaissance !== null) {
	var dateTemp = new Date(parseInt(dirigeant8.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['R06.1']= date;
}
 if (not Value('id').of(dirigeant8.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = dirigeant8.civiliteRepresentant.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['R06.2']= result;
} else if (Value('id').of(dirigeant8.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	occurrence['R06.2'] = dirigeant8.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getId();
} 		
if (not Value('id').of(dirigeant8.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
	occurrence['R06.3'] = dirigeant8.civiliteRepresentant.personneLieePPLieuNaissancePays.getLabel();
}
if (Value('id').of(dirigeant8.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
	occurrence['R06.4'] = dirigeant8.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getLabel();
}
occurrence['R07']= dirigeant8.civiliteRepresentant.personneLieePPNationalite;
}
occurrence['R22.2']= dirigeant8.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant8.civilitePersonneMorale.personneLieePMNumeroIdentification : dirigeant8.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee;
occurrence['R22.3']= dirigeant8.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 9 

if (Value('id').of(dirigeant9.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant9.personneLieeQualite.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeant9.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant9.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeant9.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
if (dirigeant9.civiliteRepresentant.personneLieePPNomNaissance != null) {
occurrence['R08'] = "1";
occurrence['R04.2'] = dirigeant9.civiliteRepresentant.personneLieePPNomNaissance;
var prenoms=[];
for (i = 0; i < dirigeant9.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(dirigeant9.civiliteRepresentant.personneLieePPPrenom[i]);
} occurrence['R04.3']= prenoms;
if (dirigeant9.civiliteRepresentant.personneLieePPNomUsage != null) {
occurrence['R04.4'] = dirigeant9.civiliteRepresentant.personneLieePPNomUsage;
}
occurrence['R05.3']= dirigeant9.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getId();
if (dirigeant9.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null) {
occurrence['R05.5']= dirigeant9.civiliteRepresentant.adresseRepresentant.representantNumeroVoie;
}
if (dirigeant9.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant9.civiliteRepresentant.adresseRepresentant.representantIndiceVoie)._eval();
   occurrence['R05.6']          = monId1;  
} 
if (dirigeant9.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null) {
occurrence['R05.7']= dirigeant9.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale;
}
occurrence['R05.8']= dirigeant9.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
if (dirigeant9.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null) {
occurrence['R05.10']= dirigeant9.civiliteRepresentant.adresseRepresentant.representantComplementVoie;
}
if (dirigeant9.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant9.civiliteRepresentant.adresseRepresentant.representantTypeVoie)._eval();
   occurrence['R05.11']          = monId1;  
} 
occurrence['R05.12']= dirigeant9.civiliteRepresentant.adresseRepresentant.representantNomVoie;
occurrence['R05.13']= dirigeant9.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getLabel();
if(dirigeant9.civiliteRepresentant.personneLieePPDateNaissance !== null) {
	var dateTemp = new Date(parseInt(dirigeant9.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['R06.1']= date;
}
 if (not Value('id').of(dirigeant9.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = dirigeant9.civiliteRepresentant.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['R06.2']= result;
} else if (Value('id').of(dirigeant9.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	occurrence['R06.2'] = dirigeant9.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getId();
} 		
if (not Value('id').of(dirigeant9.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
	occurrence['R06.3'] = dirigeant9.civiliteRepresentant.personneLieePPLieuNaissancePays.getLabel();
}
if (Value('id').of(dirigeant9.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
	occurrence['R06.4'] = dirigeant9.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getLabel();
}
occurrence['R07']= dirigeant9.civiliteRepresentant.personneLieePPNationalite;
}
occurrence['R22.2']= dirigeant9.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant9.civilitePersonneMorale.personneLieePMNumeroIdentification : dirigeant9.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee;
occurrence['R22.3']= dirigeant9.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 10

if (Value('id').of(dirigeant10.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant10.personneLieeQualite.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeant10.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant10.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeant10.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
if (dirigeant10.civiliteRepresentant.personneLieePPNomNaissance != null) {
occurrence['R08'] = "1";
occurrence['R04.2'] = dirigeant10.civiliteRepresentant.personneLieePPNomNaissance;
var prenoms=[];
for (i = 0; i < dirigeant10.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(dirigeant10.civiliteRepresentant.personneLieePPPrenom[i]);
} occurrence['R04.3']= prenoms;
if (dirigeant10.civiliteRepresentant.personneLieePPNomUsage != null) {
occurrence['R04.4'] = dirigeant10.civiliteRepresentant.personneLieePPNomUsage;
}
occurrence['R05.3']= dirigeant10.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getId();
if (dirigeant10.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null) {
occurrence['R05.5']= dirigeant10.civiliteRepresentant.adresseRepresentant.representantNumeroVoie;
}
if (dirigeant10.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant10.civiliteRepresentant.adresseRepresentant.representantIndiceVoie)._eval();
   occurrence['R05.6']          = monId1;  
} 
if (dirigeant10.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null) {
occurrence['R05.7']= dirigeant10.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale;
}
occurrence['R05.8']= dirigeant10.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
if (dirigeant10.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null) {
occurrence['R05.10']= dirigeant10.civiliteRepresentant.adresseRepresentant.representantComplementVoie;
}
if (dirigeant10.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant10.civiliteRepresentant.adresseRepresentant.representantTypeVoie)._eval();
   occurrence['R05.11']          = monId1;  
} 
occurrence['R05.12']= dirigeant10.civiliteRepresentant.adresseRepresentant.representantNomVoie;
occurrence['R05.13']= dirigeant10.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getLabel();
if(dirigeant10.civiliteRepresentant.personneLieePPDateNaissance !== null) {
	var dateTemp = new Date(parseInt(dirigeant10.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['R06.1']= date;
}
 if (not Value('id').of(dirigeant10.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = dirigeant10.civiliteRepresentant.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['R06.2']= result;
} else if (Value('id').of(dirigeant10.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	occurrence['R06.2'] = dirigeant10.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getId();
} 		
if (not Value('id').of(dirigeant10.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
	occurrence['R06.3'] = dirigeant10.civiliteRepresentant.personneLieePPLieuNaissancePays.getLabel();
}
if (Value('id').of(dirigeant10.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
	occurrence['R06.4'] = dirigeant10.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getLabel();
}
occurrence['R07']= dirigeant10.civiliteRepresentant.personneLieePPNationalite;
}
occurrence['R22.2']= dirigeant10.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant10.civilitePersonneMorale.personneLieePMNumeroIdentification : dirigeant10.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee;
occurrence['R22.3']= dirigeant10.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 11 

if (Value('id').of(dirigeant11.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant11.personneLieeQualite.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeant11.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant11.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeant11.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
if (dirigeant11.civiliteRepresentant.personneLieePPNomNaissance != null) {
occurrence['R08'] = "1";
occurrence['R04.2'] = dirigeant11.civiliteRepresentant.personneLieePPNomNaissance;
var prenoms=[];
for (i = 0; i < dirigeant11.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(dirigeant11.civiliteRepresentant.personneLieePPPrenom[i]);
} occurrence['R04.3']= prenoms;
if (dirigeant11.civiliteRepresentant.personneLieePPNomUsage != null) {
occurrence['R04.4'] = dirigeant11.civiliteRepresentant.personneLieePPNomUsage;
}
occurrence['R05.3']= dirigeant11.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getId();
if (dirigeant11.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null) {
occurrence['R05.5']= dirigeant11.civiliteRepresentant.adresseRepresentant.representantNumeroVoie;
}
if (dirigeant11.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant11.civiliteRepresentant.adresseRepresentant.representantIndiceVoie)._eval();
   occurrence['R05.6']          = monId1;  
} 
if (dirigeant11.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null) {
occurrence['R05.7']= dirigeant11.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale;
}
occurrence['R05.8']= dirigeant11.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
if (dirigeant11.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null) {
occurrence['R05.10']= dirigeant11.civiliteRepresentant.adresseRepresentant.representantComplementVoie;
}
if (dirigeant11.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant11.civiliteRepresentant.adresseRepresentant.representantTypeVoie)._eval();
   occurrence['R05.11']          = monId1;  
} 
occurrence['R05.12']= dirigeant11.civiliteRepresentant.adresseRepresentant.representantNomVoie;
occurrence['R05.13']= dirigeant11.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getLabel();
if(dirigeant11.civiliteRepresentant.personneLieePPDateNaissance !== null) {
	var dateTemp = new Date(parseInt(dirigeant11.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['R06.1']= date;
}
 if (not Value('id').of(dirigeant11.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = dirigeant11.civiliteRepresentant.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['R06.2']= result;
} else if (Value('id').of(dirigeant11.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	occurrence['R06.2'] = dirigeant11.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getId();
} 		
if (not Value('id').of(dirigeant11.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
	occurrence['R06.3'] = dirigeant11.civiliteRepresentant.personneLieePPLieuNaissancePays.getLabel();
}
if (Value('id').of(dirigeant11.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
	occurrence['R06.4'] = dirigeant11.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getLabel();
}
occurrence['R07']= dirigeant11.civiliteRepresentant.personneLieePPNationalite;
}
occurrence['R22.2']= dirigeant11.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant11.civilitePersonneMorale.personneLieePMNumeroIdentification : dirigeant11.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee;
occurrence['R22.3']= dirigeant11.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 12 

if (Value('id').of(dirigeant12.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant12.personneLieeQualite.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeant12.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant12.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeant12.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
if (dirigeant12.civiliteRepresentant.personneLieePPNomNaissance != null) {
occurrence['R08'] = "1";
occurrence['R04.2'] = dirigeant12.civiliteRepresentant.personneLieePPNomNaissance;
var prenoms=[];
for (i = 0; i < dirigeant12.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(dirigeant12.civiliteRepresentant.personneLieePPPrenom[i]);
} occurrence['R04.3']= prenoms;
if (dirigeant12.civiliteRepresentant.personneLieePPNomUsage != null) {
occurrence['R04.4'] = dirigeant12.civiliteRepresentant.personneLieePPNomUsage;
}
occurrence['R05.3']= dirigeant12.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getId();
if (dirigeant12.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null) {
occurrence['R05.5']= dirigeant12.civiliteRepresentant.adresseRepresentant.representantNumeroVoie;
}
if (dirigeant12.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant12.civiliteRepresentant.adresseRepresentant.representantIndiceVoie)._eval();
   occurrence['R05.6']          = monId1;  
} 
if (dirigeant12.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null) {
occurrence['R05.7']= dirigeant12.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale;
}
occurrence['R05.8']= dirigeant12.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
if (dirigeant12.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null) {
occurrence['R05.10']= dirigeant12.civiliteRepresentant.adresseRepresentant.representantComplementVoie;
}
if (dirigeant12.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant12.civiliteRepresentant.adresseRepresentant.representantTypeVoie)._eval();
   occurrence['R05.11']          = monId1;  
} 
occurrence['R05.12']= dirigeant12.civiliteRepresentant.adresseRepresentant.representantNomVoie;
occurrence['R05.13']= dirigeant12.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getLabel();
if(dirigeant12.civiliteRepresentant.personneLieePPDateNaissance !== null) {
	var dateTemp = new Date(parseInt(dirigeant12.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['R06.1']= date;
}
 if (not Value('id').of(dirigeant12.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = dirigeant12.civiliteRepresentant.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['R06.2']= result;
} else if (Value('id').of(dirigeant12.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	occurrence['R06.2'] = dirigeant12.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getId();
} 		
if (not Value('id').of(dirigeant12.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
	occurrence['R06.3'] = dirigeant12.civiliteRepresentant.personneLieePPLieuNaissancePays.getLabel();
}
if (Value('id').of(dirigeant12.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
	occurrence['R06.4'] = dirigeant12.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getLabel();
}
occurrence['R07']= dirigeant12.civiliteRepresentant.personneLieePPNationalite;
}
occurrence['R22.2']= dirigeant12.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant12.civilitePersonneMorale.personneLieePMNumeroIdentification : dirigeant12.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee;
occurrence['R22.3']= dirigeant12.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 13 

if (Value('id').of(dirigeant13.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant13.personneLieeQualite.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeant13.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeant13.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeant13.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant13.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant13.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeant13.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeant13.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant13.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant13.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeant13.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeant13.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeant13.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant13.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant13.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeant13.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant13.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant13.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeant13.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant13.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant13.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant13.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant13.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant13.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeant13.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeant13.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
if (dirigeant13.civiliteRepresentant.personneLieePPNomNaissance != null) {
occurrence['R08'] = "1";
occurrence['R04.2'] = dirigeant13.civiliteRepresentant.personneLieePPNomNaissance;
var prenoms=[];
for (i = 0; i < dirigeant13.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(dirigeant13.civiliteRepresentant.personneLieePPPrenom[i]);
} occurrence['R04.3']= prenoms;
if (dirigeant13.civiliteRepresentant.personneLieePPNomUsage != null) {
occurrence['R04.4'] = dirigeant13.civiliteRepresentant.personneLieePPNomUsage;
}
occurrence['R05.3']= dirigeant13.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getId();
if (dirigeant13.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null) {
occurrence['R05.5']= dirigeant13.civiliteRepresentant.adresseRepresentant.representantNumeroVoie;
}
if (dirigeant13.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant13.civiliteRepresentant.adresseRepresentant.representantIndiceVoie)._eval();
   occurrence['R05.6']          = monId1;  
} 
if (dirigeant13.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null) {
occurrence['R05.7']= dirigeant13.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale;
}
occurrence['R05.8']= dirigeant13.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
if (dirigeant13.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null) {
occurrence['R05.10']= dirigeant13.civiliteRepresentant.adresseRepresentant.representantComplementVoie;
}
if (dirigeant13.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant13.civiliteRepresentant.adresseRepresentant.representantTypeVoie)._eval();
   occurrence['R05.11']          = monId1;  
} 
occurrence['R05.12']= dirigeant13.civiliteRepresentant.adresseRepresentant.representantNomVoie;
occurrence['R05.13']= dirigeant13.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getLabel();
if(dirigeant13.civiliteRepresentant.personneLieePPDateNaissance !== null) {
	var dateTemp = new Date(parseInt(dirigeant13.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['R06.1']= date;
}
 if (not Value('id').of(dirigeant13.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = dirigeant13.civiliteRepresentant.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['R06.2']= result;
} else if (Value('id').of(dirigeant13.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	occurrence['R06.2'] = dirigeant13.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getId();
} 		
if (not Value('id').of(dirigeant13.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
	occurrence['R06.3'] = dirigeant13.civiliteRepresentant.personneLieePPLieuNaissancePays.getLabel();
}
if (Value('id').of(dirigeant13.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
	occurrence['R06.4'] = dirigeant13.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getLabel();
}
occurrence['R07']= dirigeant13.civiliteRepresentant.personneLieePPNationalite;
}
occurrence['R22.2']= dirigeant13.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant13.civilitePersonneMorale.personneLieePMNumeroIdentification : dirigeant13.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee;
occurrence['R22.3']= dirigeant13.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 14 

if (Value('id').of(dirigeant14.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant14.personneLieeQualite.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeant14.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeant14.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeant14.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant14.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant14.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeant14.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeant14.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant14.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant14.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeant14.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeant14.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeant14.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant14.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant14.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeant14.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant14.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant14.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeant14.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant14.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant14.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant14.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant14.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant14.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeant14.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeant14.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
if (dirigeant14.civiliteRepresentant.personneLieePPNomNaissance != null) {
occurrence['R08'] = "1";
occurrence['R04.2'] = dirigeant14.civiliteRepresentant.personneLieePPNomNaissance;
var prenoms=[];
for (i = 0; i < dirigeant14.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(dirigeant14.civiliteRepresentant.personneLieePPPrenom[i]);
} occurrence['R04.3']= prenoms;
if (dirigeant14.civiliteRepresentant.personneLieePPNomUsage != null) {
occurrence['R04.4'] = dirigeant14.civiliteRepresentant.personneLieePPNomUsage;
}
occurrence['R05.3']= dirigeant14.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getId();
if (dirigeant14.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null) {
occurrence['R05.5']= dirigeant14.civiliteRepresentant.adresseRepresentant.representantNumeroVoie;
}
if (dirigeant14.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant14.civiliteRepresentant.adresseRepresentant.representantIndiceVoie)._eval();
   occurrence['R05.6']          = monId1;  
} 
if (dirigeant14.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null) {
occurrence['R05.7']= dirigeant14.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale;
}
occurrence['R05.8']= dirigeant14.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
if (dirigeant14.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null) {
occurrence['R05.10']= dirigeant14.civiliteRepresentant.adresseRepresentant.representantComplementVoie;
}
if (dirigeant14.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant14.civiliteRepresentant.adresseRepresentant.representantTypeVoie)._eval();
   occurrence['R05.11']          = monId1;  
} 
occurrence['R05.12']= dirigeant14.civiliteRepresentant.adresseRepresentant.representantNomVoie;
occurrence['R05.13']= dirigeant14.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getLabel();
if(dirigeant14.civiliteRepresentant.personneLieePPDateNaissance !== null) {
	var dateTemp = new Date(parseInt(dirigeant14.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['R06.1']= date;
}
 if (not Value('id').of(dirigeant14.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = dirigeant14.civiliteRepresentant.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['R06.2']= result;
} else if (Value('id').of(dirigeant14.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	occurrence['R06.2'] = dirigeant14.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getId();
} 		
if (not Value('id').of(dirigeant14.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
	occurrence['R06.3'] = dirigeant14.civiliteRepresentant.personneLieePPLieuNaissancePays.getLabel();
}
if (Value('id').of(dirigeant14.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
	occurrence['R06.4'] = dirigeant14.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getLabel();
}
occurrence['R07']= dirigeant14.civiliteRepresentant.personneLieePPNationalite;
}
occurrence['R22.2']= dirigeant14.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant14.civilitePersonneMorale.personneLieePMNumeroIdentification : dirigeant14.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee;
occurrence['R22.3']= dirigeant14.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}


occurrencesGDR.forEach(function (value, i) {
	var idx = i+1;
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/DIU/U50/U50.1']= value['U50.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/DIU/U50/U50.2']= value['U50.2'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/DIU/U51/U51.1']= value['U51.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/DIU/U52']= value['U52'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R01/R01.1']= value['R01.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.3']= value['R02.3'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.5']= value['R02.5'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.6']= value['R02.6'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.7']= value['R02.7'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.8']= value['R02.8'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.10']= value['R02.10'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.11']= value['R02.11'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.12']= value['R02.12'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.13']= value['R02.13'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.14']= value['R02.14'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R03']= value['R03'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R08']= value['R08'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R04/R04.2']= value['R04.2'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R04/R04.3']= value['R04.3'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R04/R04.4']= value['R04.4'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R05/R05.3']= value['R05.3'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R05/R05.5']= value['R05.5'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R05/R05.6']= value['R05.6'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R05/R05.7']= value['R05.7'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R05/R05.8']= value['R05.8'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R05/R05.10']= value['R05.10'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R05/R05.11']= value['R05.11'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R05/R05.12']= value['R05.12'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R05/R05.13']= value['R05.13'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R06/R06.1']= value['R06.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R06/R06.2']= value['R06.2'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R06/R06.3']= value['R06.3'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R06/R06.4']= value['R06.4'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R07']= value['R07'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R22/R22.2']= value['R22.2'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R22/R22.3']= value['R22.3'];
});


// Fin Regent 02M

if (siege.formaliteCreationAvecActivite or
	Value('id').of(societe.entrepriseFormeJuridique).eq('5785') or
	Value('id').of(societe.entrepriseFormeJuridique).eq('5585') or
	Value('id').of(societe.entrepriseFormeJuridique).eq('5685') or
	Value('id').of(societe.entrepriseFormeJuridique).eq('5385') or
	Value('id').of(societe.entrepriseFormeJuridique).eq('3120') or
	Value('id').of(societe.entrepriseFormeJuridique).eq('3110') or
	Value('id').of(societe.entrepriseFormeJuridique).eq('5800')) {
	
// Groupe ICE : Identification complète de l'établissement

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E01']= "1";
if (siege.etablissementPrincipalDifferentSiege or Value('id').of(societe.entrepriseFormeJuridique).eq('3110') or Value('id').of(societe.entrepriseFormeJuridique).eq('3120') or (Value('id').of(societe.entrepriseFormeJuridique).eq('5800') and Value('id').of(societe.entrepriseEur).eq('ouvertureEtablissement'))) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.3']= etablissement.cadreAdresseEtablissement.communeAdresseEtablissement.getId();
if (etablissement.cadreAdresseEtablissement.numeroAdresseEtablissement != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5']= etablissement.cadreAdresseEtablissement.numeroAdresseEtablissement;
}
if (etablissement.cadreAdresseEtablissement.indiceAdresseEtablissement != null) {
   var monId1 = Value('id').of(etablissement.cadreAdresseEtablissement.indiceAdresseEtablissement)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.6']          = monId1;  
}
if (etablissement.cadreAdresseEtablissement.distributionSpecialeAdresseEtablissement != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.7']= etablissement.cadreAdresseEtablissement.distributionSpecialeAdresseEtablissement;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.8']= etablissement.cadreAdresseEtablissement.codePostalAdresseEtablissement;
if (etablissement.cadreAdresseEtablissement.complementAdresseEtablissement != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.10']= etablissement.cadreAdresseEtablissement.complementAdresseEtablissement;
}
if (etablissement.cadreAdresseEtablissement.typeAdresseEtablissement != null) {
   var monId1 = Value('id').of(etablissement.cadreAdresseEtablissement.typeAdresseEtablissement)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.11']          = monId1;  
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.12']= etablissement.cadreAdresseEtablissement.voieAdresseEtablissement;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.13']= etablissement.cadreAdresseEtablissement.communeAdresseEtablissement.getLabel();
}  else if (Value('id').of(societe.entrepriseFormeJuridique).eq('5800') and Value('id').of(societe.entrepriseEur).eq('ouvertureAmbulant')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.3']= etablissement.communeAdresseMarche.getId();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.8']= etablissement.codePostalAdresseMarche;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.13']= etablissement.communeAdresseMarche.getLabel();
} else if (not siege.etablissementPrincipalDifferentSiege) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.3']= siege.cadreAdresseProfessionnelle.communeAdresseSiege.getId();	
if (siege.cadreAdresseProfessionnelle.numeroAdresseSiege != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5']= siege.cadreAdresseProfessionnelle.numeroAdresseSiege;
}
if (siege.cadreAdresseProfessionnelle.indiceAdresseSiege != null) {
   var monId1 = Value('id').of(siege.cadreAdresseProfessionnelle.indiceAdresseSiege)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.6']          = monId1;  
}
if (siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.7']= siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.8']= siege.cadreAdresseProfessionnelle.codePostalAdresseSiege;
if (siege.cadreAdresseProfessionnelle.complementAdresseSiege != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.10']= siege.cadreAdresseProfessionnelle.complementAdresseSiege;
}
if (siege.cadreAdresseProfessionnelle.typeAdresseSiege != null) {
   var monId1 = Value('id').of(siege.cadreAdresseProfessionnelle.typeAdresseSiege)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.11']          = monId1;  
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.12']= siege.cadreAdresseProfessionnelle.voieAdresseSiege;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.13']= siege.cadreAdresseProfessionnelle.communeAdresseSiege.getLabel();	
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E04']= siege.etablissementPrincipalDifferentSiege ? "3" : ((Value('id').of(societe.entrepriseFormeJuridique).eq('3110') or Value('id').of(societe.entrepriseFormeJuridique).eq('3120') or Value('id').of(societe.entrepriseFormeJuridique).eq('5800')) ? "5" : "2");
if (etablissement.etablissementEnseigne != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E05']= etablissement.etablissementEnseigne;
}
if (etablissement.etablissementNomCommercialProfessionnel != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E09']= etablissement.etablissementNomCommercialProfessionnel;
}


// Groupe ORE : Origine de l'établissement

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.1']= (Value('id').of(origine.etablissementOrigineFondsLiberal).eq('EtablissementOrigineFondsCreation') or Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsCreation') or ((Value('id').of(siege.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') or Value('id').of(siege.activiteLieu).eq('etablissementDomiciliationOui')) and not siege.etablissementPrincipalDifferentSiege)
or (Value('id').of(societe.entrepriseFormeJuridique).eq('5800') and Value('id').of(societe.entrepriseEur).eq('ouvertureAmbulant'))) ? "1" : 
																							(Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsAchat') ? "3" : 
																							(Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsApport') ? "4" :
																							(Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsLocationGerance') ? "6" :
																							(Value('id').of(origine.etablissementOrigineFondsLiberal).eq('EtablissementOrigineFondsRepriseActiviteLiberale') ? "8" :
																							(Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsGeranceMandat') ? "F" : "9")))));
if (Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsCocheAutre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.2']= origine.etablissementOrigineFondsAutre;	
}
if ((Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsAchat') or Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsApport')) and (origine.precedentExploitant.etablissementJournalAnnoncesLegalesNom != null or origine.precedentExploitant.etablissementJournalAnnoncesLegalesNomBis != null)) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E22/E22.1']= origine.precedentExploitant.etablissementJournalAnnoncesLegalesNom != null ? origine.precedentExploitant.etablissementJournalAnnoncesLegalesNom : origine.precedentExploitant.etablissementJournalAnnoncesLegalesNomBis;	
if(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParution !== null) {
	var dateTemp = new Date(parseInt(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParution.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E22/E22.2']= date;
} else if(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParutionBis !== null) {
	var dateTemp = new Date(parseInt(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParutionBis.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E22/E22.2']= date;
}	
}

// Groupe PEE : Précédent exploitant de l'établissement

if (Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsAchat') or Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsApport') or Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsLocationGerance')) {
if (origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.1']= origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant.split(' ').join('');
}
if (Value('id').of(origine.precedentExploitant.typePersonnePrecedentExploitant).eq('typePersonnePrecedentExploitantPP')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.2']= origine.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant;
if (origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.3']=  origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.4']= origine.precedentExploitant.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant;
}	
if (Value('id').of(origine.precedentExploitant.typePersonnePrecedentExploitant).eq('typePersonnePrecedentExploitantPM')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.7']= origine.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant;
}
}

// Groupe LGE

if(Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsLocationGerance') or Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsGeranceMandat')){
if(origine.geranceMandat.etablissementLoueurMandantDuFondsDebutContrat !== null) {
	var dateTemp = new Date(parseInt(origine.geranceMandat.etablissementLoueurMandantDuFondsDebutContrat.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ;
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E61/E61.1'] = date;
}
if(origine.geranceMandat.etablissementLoueurMandantDuFondsFinContrat !== null) {
	var dateTemp = new Date(parseInt(origine.geranceMandat.etablissementLoueurMandantDuFondsFinContrat.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ;
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E61/E61.2'] = date;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E61/E61.3'] = origine.geranceMandat.etablissementLoueurMandantDuFondsRenouvellementTaciteReconductionOui ? "O" : "N";
if (Value('id').of(origine.geranceMandat.typePersonneLoueurMandant).eq('typePersonneLoueurMandantPP')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E62'] = origine.geranceMandat.entrepriseLieeEntreprisePMNomNaissanceLoueurMandantDuFonds;
if (origine.geranceMandat.entrepriseLieeEntreprisePPNomUsageLoueurMandantDuFonds != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E64'] = origine.geranceMandat.entrepriseLieeEntreprisePPNomUsageLoueurMandantDuFonds;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E65']= origine.geranceMandat.entrepriseLieeEntreprisePPPrenom1LoueurMandantDuFonds;
}
if (Value('id').of(origine.geranceMandat.typePersonneLoueurMandant).eq('typePersonneLoueurMandantPM')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E66']=origine.geranceMandat.entrepriseLieeEntreprisePMDenominationLoueurMandantDuFonds;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.3'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.communeLoueurMandantDuFonds.getId();
if (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.numRueAdresseLoueurMandantDufonds != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.5'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.numRueAdresseLoueurMandantDufonds; 
}
if (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.indiceVoieLoueurMandantDuFonds != null) {																							
	var monId1 = Value('id').of(origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.indiceVoieLoueurMandantDuFonds)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.6']=monId1;
}
if (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.distributionSpecialeLoueurMandantDuFonds != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.7'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.distributionSpecialeLoueurMandantDuFonds;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.8'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.codePostalLoueurMandantDuFonds;
if (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.rueComplementAdresseLoueurMandantDuFonds != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.10'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.rueComplementAdresseLoueurMandantDuFonds;
}
if (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.typeVoieLoueurMandantDuFonds != null) {			
	var typeVoie = Value('id').of(origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.typeVoieLoueurMandantDuFonds)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.11']=typeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.12'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.nomVoieLoueurMandantDuFonds;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.13'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.communeLoueurMandantDuFonds.getLabel();
if (Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsGeranceMandat')){
if (origine.geranceMandat.entrepriseLieeSirenGeranceMandat != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E67']=origine.geranceMandat.entrepriseLieeSirenGeranceMandat.split(' ').join('');
}
	//Récupérer le EntityId du greffe
	var greffeId = origine.geranceMandat.entrepriseLieeGreffeImmatriculation.id ;
	//Appeler Directory pour récupérer l'autorité
	_log.info('l entityId du greffe selectionne est {}', greffeId);
	var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', greffeId) //
	           .connectionTimeout(10000) //
	           .receiveTimeout(10000) //
	           .accept('json') //
	           .get();
	//result
	var receiverInfo = response.asObject();
	var contactInfo = !receiverInfo.details ? null : receiverInfo.details;
	//Récupérer le code EDI
	var codeEDIGreffe = !contactInfo.ediCode ? null : contactInfo.ediCode;
	_log.info('le code edi du greffe selectionne est {}', codeEDIGreffe);
	// À modifier quand Destiny fourni le réferentiel
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E68']=codeEDIGreffe;
}
}

// Groupe ACE : Activité de l'établissement

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E70']= sanitize(etablissement.etablissementActivites);
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E71']= etablissement.etablissementActivitesLaPlusImportante != null ? sanitize(etablissement.etablissementActivitesLaPlusImportante) : sanitize(etablissement.etablissementActivites.substring(0, 140));

// Groupe CAE : Complément sur l'activité de l'établissement

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E73/E73.1']= Value('id').of(etablissement.activiteTemps).eq('etablissementActivitePermanenteSaisonnierePermanente') ? "P" : "S";
if (etablissement.etablissementNonSedentariteQualiteAmbulant or Value('id').of (societe.entrepriseEur).eq('ouvertureAmbulant')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E73/E73.2']= Value('id').of (societe.entrepriseEur).eq('ouvertureAmbulant') ? "E" : "A";
}
if (Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMagasin') or Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMarche') or Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceDetail')) { 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.1']= "10";
} else if (Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceGros')){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.1']= "09";
} else if (Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureFabricationProduction')){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.1']= "04";
} else if (Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureBatTravauxPublics')){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.1']= "14";
} else if (Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCocheAutre')){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.1']= "99";
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.2']= sanitize(etablissement.etablissementActiviteNatureAutre.substring(0, 50));
} else {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.1']= "11";
}	
if (Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMagasin')) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E77/E77.1']= "05";
function lpad(str, padString, length) {
  var ch = str;
  while (ch.length < length) {
      ch = padString + ch;
  }
  log.info("lpad : "+ch);
  return ch;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E77/E77.3']= lpad(etablissement.etablissementActiviteLieuExerciceMagasinSurface.toString(), "0", 5);
} else if (Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMarche')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E77/E77.1']= "92";
} else if (Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceDetail')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E77/E77.1']= "99";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E77/E77.2']= "sur internet";
}

// Groupe SAE : Salariés de l'établissement

if (etablissement.etablissementEffectifSalariePresenceOui and etablissement.cadre3EffectifSalarie.etablissementEffectifSalarieEmbauchePremierSalarieOui){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E83/E83.1']="1";
}	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E84']=etablissement.etablissementEffectifSalariePresenceOui ? etablissement.cadre3EffectifSalarie.etablissementEffectifSalarieNombre : "0";
if (etablissement.cadre3EffectifSalarie.etablissementEffectifSalarieApprentis != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E85/E85.8']= etablissement.cadre3EffectifSalarie.etablissementEffectifSalarieApprentis;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E86']= etablissement.etablissementEffectifSalariePresenceOui ? "O" : "N";

// Groupe IDE : Identification de la personne liée à l'établissement

var occurrencesIDE = [];
var occurrence = {};

// Fondé de pouvoir 1	

if (Value('id').of(dirigeant1.personneLieeQualite).eq('66')) {

occurrence['E91.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
}
occurrence['E92.1'] = dirigeant1.civilitePersonnePhysique.personneLieePersonnePouvoirLimiteEtablissementOui ? "P" : "S";
occurrence['E93.2'] = dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['E93.4'] = dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for (i = 0; i < dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['E93.3']= prenoms;
if (Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.3']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['E94.3']= result;
}
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['E94.5']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['E94.6']          = monId1;  
} 
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['E94.7']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['E94.8']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['E94.10']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['E94.11']          = monId1;  
} 
occurrence['E94.12']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.14']= dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
if(dirigeant1.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant1.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['E95.1']= date;
}
if (not Value('id').of(dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['E95.2']= result;
} else if (Value('id').of(dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['E95.2'] = dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['E95.3'] = dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['E95.4'] = dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
occurrence['E96']= dirigeant1.civilitePersonnePhysique.personneLieePPNationaliteGerant;
occurrencesIDE.push(occurrence);
occurrence = {};
}

// Fondé de pouvoir 2	

if (Value('id').of(dirigeant2.personneLieeQualite).eq('66')) {

occurrence['E91.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
}
occurrence['E92.1'] = dirigeant2.civilitePersonnePhysique.personneLieePersonnePouvoirLimiteEtablissementOui ? "P" : "S";
occurrence['E93.2'] = dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['E93.4'] = dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for (i = 0; i < dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['E93.3']= prenoms;
if (Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.3']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['E94.3']= result;
}
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['E94.5']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['E94.6']          = monId1;  
} 
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['E94.7']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['E94.8']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['E94.10']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['E94.11']          = monId1;  
} 
occurrence['E94.12']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.14']= dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
if(dirigeant2.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant2.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['E95.1']= date;
}
if (not Value('id').of(dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['E95.2']= result;
} else if (Value('id').of(dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['E95.2'] = dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['E95.3'] = dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['E95.4'] = dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
occurrence['E96']= dirigeant2.civilitePersonnePhysique.personneLieePPNationaliteGerant;
occurrencesIDE.push(occurrence);
occurrence = {};
}

// Fondé de pouvoir 3	

if (Value('id').of(dirigeant3.personneLieeQualite).eq('66')) {

occurrence['E91.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
}
occurrence['E92.1'] = dirigeant3.civilitePersonnePhysique.personneLieePersonnePouvoirLimiteEtablissementOui ? "P" : "S";
occurrence['E93.2'] = dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['E93.4'] = dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for (i = 0; i < dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['E93.3']= prenoms;
if (Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.3']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['E94.3']= result;
}
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['E94.5']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['E94.6']          = monId1;  
} 
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['E94.7']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['E94.8']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['E94.10']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['E94.11']          = monId1;  
} 
occurrence['E94.12']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.14']= dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
if(dirigeant3.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant3.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['E95.1']= date;
}
if (not Value('id').of(dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['E95.2']= result;
} else if (Value('id').of(dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['E95.2'] = dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['E95.3'] = dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['E95.4'] = dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
occurrence['E96']= dirigeant3.civilitePersonnePhysique.personneLieePPNationaliteGerant;
occurrencesIDE.push(occurrence);
occurrence = {};
}

// Fondé de pouvoir 4	

if (Value('id').of(dirigeant4.personneLieeQualite).eq('66')) {

occurrence['E91.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
}
occurrence['E92.1'] = dirigeant4.civilitePersonnePhysique.personneLieePersonnePouvoirLimiteEtablissementOui ? "P" : "S";
occurrence['E93.2'] = dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['E93.4'] = dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for (i = 0; i < dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['E93.3']= prenoms;
if (Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.3']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['E94.3']= result;
}
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['E94.5']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['E94.6']          = monId1;  
} 
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['E94.7']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['E94.8']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['E94.10']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['E94.11']          = monId1;  
} 
occurrence['E94.12']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.14']= dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
if(dirigeant4.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant4.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['E95.1']= date;
}
if (not Value('id').of(dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['E95.2']= result;
} else if (Value('id').of(dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['E95.2'] = dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['E95.3'] = dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['E95.4'] = dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
occurrence['E96']= dirigeant4.civilitePersonnePhysique.personneLieePPNationaliteGerant;
occurrencesIDE.push(occurrence);
occurrence = {};
}

// Fondé de pouvoir 5	

if (Value('id').of(dirigeant5.personneLieeQualite).eq('66')) {

occurrence['E91.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
}
occurrence['E92.1'] = dirigeant5.civilitePersonnePhysique.personneLieePersonnePouvoirLimiteEtablissementOui ? "P" : "S";
occurrence['E93.2'] = dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['E93.4'] = dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for (i = 0; i < dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['E93.3']= prenoms;
if (Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.3']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['E94.3']= result;
}
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['E94.5']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['E94.6']          = monId1;  
} 
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['E94.7']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['E94.8']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['E94.10']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['E94.11']          = monId1;  
} 
occurrence['E94.12']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.14']= dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
if(dirigeant5.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant5.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['E95.1']= date;
}
if (not Value('id').of(dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['E95.2']= result;
} else if (Value('id').of(dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['E95.2'] = dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['E95.3'] = dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['E95.4'] = dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
occurrence['E96']= dirigeant5.civilitePersonnePhysique.personneLieePPNationaliteGerant;
occurrencesIDE.push(occurrence);
occurrence = {};
}

// Fondé de pouvoir 6	

if (Value('id').of(dirigeant6.personneLieeQualite).eq('66')) {

occurrence['E91.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
}
occurrence['E92.1'] = dirigeant6.civilitePersonnePhysique.personneLieePersonnePouvoirLimiteEtablissementOui ? "P" : "S";
occurrence['E93.2'] = dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['E93.4'] = dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for (i = 0; i < dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['E93.3']= prenoms;
if (Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.3']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['E94.3']= result;
}
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['E94.5']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['E94.6']          = monId1;  
} 
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['E94.7']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['E94.8']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['E94.10']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['E94.11']          = monId1;  
} 
occurrence['E94.12']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.14']= dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
if(dirigeant6.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant6.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['E95.1']= date;
}
if (not Value('id').of(dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['E95.2']= result;
} else if (Value('id').of(dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['E95.2'] = dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['E95.3'] = dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['E95.4'] = dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
occurrence['E96']= dirigeant6.civilitePersonnePhysique.personneLieePPNationaliteGerant;
occurrencesIDE.push(occurrence);
occurrence = {};
}

// Fondé de pouvoir 7	

if (Value('id').of(dirigeant7.personneLieeQualite).eq('66')) {

occurrence['E91.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
}
occurrence['E92.1'] = dirigeant7.civilitePersonnePhysique.personneLieePersonnePouvoirLimiteEtablissementOui ? "P" : "S";
occurrence['E93.2'] = dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['E93.4'] = dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for (i = 0; i < dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['E93.3']= prenoms;
if (Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.3']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['E94.3']= result;
}
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['E94.5']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['E94.6']          = monId1;  
} 
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['E94.7']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['E94.8']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['E94.10']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['E94.11']          = monId1;  
} 
occurrence['E94.12']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.14']= dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
if(dirigeant7.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant7.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['E95.1']= date;
}
if (not Value('id').of(dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['E95.2']= result;
} else if (Value('id').of(dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['E95.2'] = dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['E95.3'] = dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['E95.4'] = dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
occurrence['E96']= dirigeant7.civilitePersonnePhysique.personneLieePPNationaliteGerant;
occurrencesIDE.push(occurrence);
occurrence = {};
}

// Fondé de pouvoir 8	

if (Value('id').of(dirigeant8.personneLieeQualite).eq('66')) {

occurrence['E91.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
}
occurrence['E92.1'] = dirigeant8.civilitePersonnePhysique.personneLieePersonnePouvoirLimiteEtablissementOui ? "P" : "S";
occurrence['E93.2'] = dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['E93.4'] = dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for (i = 0; i < dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['E93.3']= prenoms;
if (Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.3']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['E94.3']= result;
}
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['E94.5']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['E94.6']          = monId1;  
} 
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['E94.7']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['E94.8']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['E94.10']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['E94.11']          = monId1;  
} 
occurrence['E94.12']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.14']= dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
if(dirigeant8.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant8.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['E95.1']= date;
}
if (not Value('id').of(dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['E95.2']= result;
} else if (Value('id').of(dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['E95.2'] = dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['E95.3'] = dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['E95.4'] = dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
occurrence['E96']= dirigeant8.civilitePersonnePhysique.personneLieePPNationaliteGerant;
occurrencesIDE.push(occurrence);
occurrence = {};
}

// Fondé de pouvoir 9	

if (Value('id').of(dirigeant9.personneLieeQualite).eq('66')) {

occurrence['E91.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
}
occurrence['E92.1'] = dirigeant9.civilitePersonnePhysique.personneLieePersonnePouvoirLimiteEtablissementOui ? "P" : "S";
occurrence['E93.2'] = dirigeant9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (dirigeant9.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['E93.4'] = dirigeant9.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for (i = 0; i < dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['E93.3']= prenoms;
if (Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.3']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['E94.3']= result;
}
if (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['E94.5']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['E94.6']          = monId1;  
} 
if (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['E94.7']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['E94.8']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant9.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['E94.10']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['E94.11']          = monId1;  
} 
occurrence['E94.12']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.14']= dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
if(dirigeant9.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant9.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['E95.1']= date;
}
if (not Value('id').of(dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['E95.2']= result;
} else if (Value('id').of(dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['E95.2'] = dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['E95.3'] = dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['E95.4'] = dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
occurrence['E96']= dirigeant9.civilitePersonnePhysique.personneLieePPNationaliteGerant;
occurrencesIDE.push(occurrence);
occurrence = {};
}

// Fondé de pouvoir 10	

if (Value('id').of(dirigeant10.personneLieeQualite).eq('66')) {

occurrence['E91.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
}
occurrence['E92.1'] = dirigeant10.civilitePersonnePhysique.personneLieePersonnePouvoirLimiteEtablissementOui ? "P" : "S";
occurrence['E93.2'] = dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (dirigeant10.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['E93.4'] = dirigeant10.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for (i = 0; i < dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['E93.3']= prenoms;
if (Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.3']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['E94.3']= result;
}
if (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['E94.5']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['E94.6']          = monId1;  
} 
if (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['E94.7']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['E94.8']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant10.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['E94.10']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['E94.11']          = monId1;  
} 
occurrence['E94.12']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.14']= dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
if(dirigeant10.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant10.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['E95.1']= date;
}
if (not Value('id').of(dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['E95.2']= result;
} else if (Value('id').of(dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['E95.2'] = dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['E95.3'] = dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['E95.4'] = dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
occurrence['E96']= dirigeant10.civilitePersonnePhysique.personneLieePPNationaliteGerant;
occurrencesIDE.push(occurrence);
occurrence = {};
}

// Fondé de pouvoir 11	

if (Value('id').of(dirigeant11.personneLieeQualite).eq('66')) {

occurrence['E91.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
}
occurrence['E92.1'] = dirigeant11.civilitePersonnePhysique.personneLieePersonnePouvoirLimiteEtablissementOui ? "P" : "S";
occurrence['E93.2'] = dirigeant11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (dirigeant11.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['E93.4'] = dirigeant11.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for (i = 0; i < dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['E93.3']= prenoms;
if (Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.3']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['E94.3']= result;
}
if (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['E94.5']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['E94.6']          = monId1;  
} 
if (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['E94.7']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['E94.8']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant11.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['E94.10']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['E94.11']          = monId1;  
} 
occurrence['E94.12']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.14']= dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
if(dirigeant11.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant11.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['E95.1']= date;
}
if (not Value('id').of(dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['E95.2']= result;
} else if (Value('id').of(dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['E95.2'] = dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['E95.3'] = dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['E95.4'] = dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
occurrence['E96']= dirigeant11.civilitePersonnePhysique.personneLieePPNationaliteGerant;
occurrencesIDE.push(occurrence);
occurrence = {};
}

// Fondé de pouvoir 12	

if (Value('id').of(dirigeant12.personneLieeQualite).eq('66')) {

occurrence['E91.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
}
occurrence['E92.1'] = dirigeant12.civilitePersonnePhysique.personneLieePersonnePouvoirLimiteEtablissementOui ? "P" : "S";
occurrence['E93.2'] = dirigeant12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (dirigeant12.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['E93.4'] = dirigeant12.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for (i = 0; i < dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['E93.3']= prenoms;
if (Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.3']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['E94.3']= result;
}
if (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['E94.5']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['E94.6']          = monId1;  
} 
if (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['E94.7']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['E94.8']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant12.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['E94.10']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['E94.11']          = monId1;  
} 
occurrence['E94.12']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.14']= dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
if(dirigeant12.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant12.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['E95.1']= date;
}
if (not Value('id').of(dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['E95.2']= result;
} else if (Value('id').of(dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['E95.2'] = dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['E95.3'] = dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['E95.4'] = dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
occurrence['E96']= dirigeant12.civilitePersonnePhysique.personneLieePPNationaliteGerant;
occurrencesIDE.push(occurrence);
occurrence = {};
}

// Fondé de pouvoir 13	

if (Value('id').of(dirigeant13.personneLieeQualite).eq('66')) {

occurrence['E91.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
}
occurrence['E92.1'] = dirigeant13.civilitePersonnePhysique.personneLieePersonnePouvoirLimiteEtablissementOui ? "P" : "S";
occurrence['E93.2'] = dirigeant13.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (dirigeant13.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['E93.4'] = dirigeant13.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for (i = 0; i < dirigeant13.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant13.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['E93.3']= prenoms;
if (Value('id').of(dirigeant13.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.3']= dirigeant13.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant13.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant13.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['E94.3']= result;
}
if (dirigeant13.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['E94.5']= dirigeant13.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant13.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant13.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['E94.6']          = monId1;  
} 
if (dirigeant13.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['E94.7']= dirigeant13.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['E94.8']= dirigeant13.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant13.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant13.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['E94.10']= dirigeant13.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant13.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant13.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['E94.11']          = monId1;  
} 
occurrence['E94.12']= dirigeant13.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant13.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeant13.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant13.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeant13.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant13.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.14']= dirigeant13.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
if(dirigeant13.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant13.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['E95.1']= date;
}
if (not Value('id').of(dirigeant13.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant13.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['E95.2']= result;
} else if (Value('id').of(dirigeant13.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['E95.2'] = dirigeant13.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant13.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['E95.3'] = dirigeant13.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant13.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['E95.4'] = dirigeant13.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
occurrence['E96']= dirigeant13.civilitePersonnePhysique.personneLieePPNationaliteGerant;
occurrencesIDE.push(occurrence);
occurrence = {};
}

// Fondé de pouvoir 14	

if (Value('id').of(dirigeant14.personneLieeQualite).eq('66')) {

occurrence['E91.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
}
occurrence['E92.1'] = dirigeant14.civilitePersonnePhysique.personneLieePersonnePouvoirLimiteEtablissementOui ? "P" : "S";
occurrence['E93.2'] = dirigeant14.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (dirigeant14.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['E93.4'] = dirigeant14.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for (i = 0; i < dirigeant14.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant14.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['E93.3']= prenoms;
if (Value('id').of(dirigeant14.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.3']= dirigeant14.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant14.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant14.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['E94.3']= result;
}
if (dirigeant14.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['E94.5']= dirigeant14.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant14.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant14.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['E94.6']          = monId1;  
} 
if (dirigeant14.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['E94.7']= dirigeant14.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['E94.8']= dirigeant14.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant14.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant14.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['E94.10']= dirigeant14.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant14.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant14.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['E94.11']          = monId1;  
} 
occurrence['E94.12']= dirigeant14.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant14.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeant14.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant14.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeant14.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant14.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.14']= dirigeant14.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
if(dirigeant14.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant14.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['E95.1']= date;
}
if (not Value('id').of(dirigeant14.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant14.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['E95.2']= result;
} else if (Value('id').of(dirigeant14.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['E95.2'] = dirigeant14.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant14.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['E95.3'] = dirigeant14.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant14.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['E95.4'] = dirigeant14.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
occurrence['E96']= dirigeant14.civilitePersonnePhysique.personneLieePPNationaliteGerant;
occurrencesIDE.push(occurrence);
occurrence = {};
}

occurrencesIDE.forEach(function (value, i) {
	var idx = i+1;
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E91/E91.1']= value['E91.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E91/E91.2']= value['E91.2'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E92/E92.1']= value['E92.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E93/E93.2']= value['E93.2'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E93/E93.4']= value['E93.4'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E93/E93.3']= value['E93.3'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E94/E94.3']= value['E94.3'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E94/E94.5']= value['E94.5'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E94/E94.6']= value['E94.6'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E94/E94.7']= value['E94.7'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E94/E94.8']= value['E94.8'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E94/E94.10']= value['E94.10'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E94/E94.11']= value['E94.11'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E94/E94.12']= value['E94.12'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E94/E94.13']= value['E94.13'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E94/E94.14']= value['E94.14'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E95/E95.1']= value['E95.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E95/E95.2']= value['E95.2'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E95/E95.3']= value['E95.3'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E95/E95.4']= value['E95.4'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E96']= value['E96'];
});

}
 
 
// Call to the XML-REGENT generation WS 
var response = nash.service.request('${regent.baseUrl}/private/v1/xml-regent/generate/{regentVersion}/{authorityType}/{authorityId}', regentVersion, authorityType, authorityId) 
.dataType('application/json') // 
.accept('json') // 
.param('listTypeEvenement',eventRegent) 
//-->MINE-263 : Permettre à l'équipe FF de voir les problèmes de validation XSD et le regent sur studio
.continueOnError(true) //
.post(JSON.stringify(regentFields)); 

// Record the generated XML-REGENT 
//MOD déplacement du test de la réponse du premier ws
if (response != null && response.status == 200) { 
    var xmlRegentStr = response.asBytes(); 
    nash.record.saveFile("XML_REGENT.xml",xmlRegentStr); 

//debut de l'ajout
 if (authorityId2 != null) {
//MOD extraction du numéro de liasse du premeir regent généré
var numeroLiasse = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02');
numeroLiasse = JSON.parse(numeroLiasse);
_log.info('extracted numero de liasse is {}', numeroLiasse);
 regentFields['/REGENT-XML/Destinataire']= authorityId2;
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05']= "C";
 
 // Filtre regent greffe
var undefined;
// Groupe RFU
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U31']= undefined; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U32']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.1']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.2']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U37']= undefined;
// Groupe OIU
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OIU/U75']= undefined;
// Groupe GCS/ISS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/ISS/A10/A10.1']  = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/ISS/A10/A10.2'] = undefined;
// Groupe GCS/SNS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A21/A21.2']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A21/A21.3']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A21/A21.4']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A21/A21.6']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A22/A22.3']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A22/A22.4']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A24/A24.2']= undefined; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A24/A24.3']= undefined;
// Groupe GCS/CAS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/CAS/A42/A42.1']= undefined;
// Groupe SAE
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E83/E83.1']=undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E84']=undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E85/E85.8']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E86']= undefined;

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS']  = undefined;


 //MOD modification de l'appel pour la génération du deuxième régent afi de prendre e compte le numéro de liasse du premier
 var response2 = nash.service.request('${regent.baseUrl}/private/v1/xml-regent/generate/{regentVersion}/{authorityType}/{authorityId}', regentVersion, authorityType2, authorityId2) 
.dataType('application/json') // 
.accept('json') // 
.param('listTypeEvenement',eventRegent) 
.param('liasseNumber', numeroLiasse.C02) 
.continueOnError(true)
.post(JSON.stringify(regentFields)); 

	//Début de l'ajout
	//MOD modifier reponse en response2
	if (response2 != null && response2.status == 200) { 
		var xmlRegentStr = response2.asBytes(); 
		nash.record.saveFile("XML_REGENT2.xml",xmlRegentStr);
	}else{
		
		var returnedResponse = response2.asObject();
		_log.info("Call ws regent returned errors  {}", returnedResponse);
		
		
		var regent2 = returnedResponse['regent'];
		var errors2 = returnedResponse['errors']; 
		nash.record.saveFile("XML_REGENT2.xml",regent2.getBytes()); 
		nash.record.saveFile("XML_VALIDATION_ERRORS_2.xml",errors2.getBytes()); 
		
		//-->MINE-263 : Permettre à l'équipe FF de voir les problèmes de validation XSD et le regent sur studio
		
		return spec.create({
		id : 'xmlGenerationConfirmation',
		label : "Xml Regent confirmation message",
		groups : [ spec.createGroup({
				id : 'Regent ',
				description : "Une erreur s'est produite lors de la génération du XML Regent.",
				data : [
				spec.createData({
					id: 'regent',
					label: "XML regent généré",
					type: 'Text',
					mandatory: true,
					value: regent2
				}),spec.createData({
					id: 'errors',
					label: "Erreurs de validations xsd",
					type: 'Text',
					mandatory: false,
					value: errors2
				}),spec.createData({
					id: 'blockingField',
					label: "xsd erroné",
					help:"Ce champs sert à arrêter le process du dossier pour que le support puisse le consulter",
					type: 'StringReadOnly',
					mandatory: true
				})]
			})]
		});
	}	
}
	return spec.create({
	id : 'xmlGenerationConfirmation',
	label : "Xml Regent confirmation message",
	groups : [ spec.createGroup({
		id : 'confirmationMessageOk',
		description : "Le fichier XML Regent a été généré et ajouté au dossier.",
		data : []
		}) ]
	});
}else{
	
	var returnedResponse = response.asObject();
	_log.info("Call ws regent returned errors  {}", returnedResponse);
	
	
	var regent = returnedResponse['regent'];
	var errors = returnedResponse['errors']; 
	nash.record.saveFile("XML_REGENT.xml",regent.getBytes()); 
	nash.record.saveFile("XML_VALIDATION_ERRORS.xml",errors.getBytes()); 
	
	
	return spec.create({
	id : 'xmlGenerationConfirmation',
	label : "Xml Regent confirmation message",
	groups : [ spec.createGroup({
		id : 'Regent ',
		description : "Une erreur s'est produite lors de la génération du XML Regent de la première autorité.",
		data : [
				spec.createData({
					id: 'regent',
					label: "XML regent généré",
					type: 'Text',
					mandatory: true,
					value: regent
				}),spec.createData({
					id: 'errors',
					label: "Erreurs de validations xsd",
					type: 'Text',
					mandatory: false,
					value: errors
				}),spec.createData({
					id: 'blockingField',
					label: "xsd erroné",
					help:"Ce champs sert à arrêter le process du dossier pour que le support puisse le consulter",
					type: 'StringReadOnly',
					mandatory: true
			})]
		}) ]
	});
}	