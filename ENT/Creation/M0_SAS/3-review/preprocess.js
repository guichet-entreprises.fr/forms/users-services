function pad(s) { return (s < 10) ? '0' + s : s; }
var formFields = {};

// Cadre 1 

var societe = $m0SAS.cadre1SocieteGroup.cadre1Identite;
var siege = $m0SAS.cadre2AdresseSiegeGroup.cadre2AdresseSiege; 

if (not Value('id').of(societe.entrepriseFormeJuridique).eq('5785') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5585') 
	and not Value('id').of(societe.entrepriseFormeJuridique).eq('5685') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5385') 
	and not Value('id').of(societe.entrepriseFormeJuridique).eq('3110') and not Value('id').of(societe.entrepriseFormeJuridique).eq('3120') 
	and not Value('id').of(societe.entrepriseFormeJuridique).eq('5800')) {
formFields['formalité_societeCommerciale']                                                  = true;
formFields['formalite_evenement']                                                           = siege.formaliteCreationAvecActivite ? false : true;
}
formFields['formalite_societeCommercialeEtrangere']                                         = (Value('id').of(societe.entrepriseFormeJuridique).eq('3110') 
																							or Value('id').of(societe.entrepriseFormeJuridique).eq('3120') 
																							or Value('id').of(societe.entrepriseFormeJuridique).eq('5800')) ? true : false;
formFields['formalite_constitutionPM']                                                      = (Value('id').of(societe.entrepriseFormeJuridique).eq('5785') 
																							or Value('id').of(societe.entrepriseFormeJuridique).eq('5585') 
																							or Value('id').of(societe.entrepriseFormeJuridique).eq('5685') 
																							or Value('id').of(societe.entrepriseFormeJuridique).eq('5385')) ? true : false;
formFields['formalite_ouvertureEtablissementFrance']                                        = (Value('id').of(societe.entrepriseFormeJuridique).eq('3110') 
																							or Value('id').of(societe.entrepriseFormeJuridique).eq('3120')
																							or (Value('id').of(societe.entrepriseFormeJuridique).eq('5800')
																							and Value('id').of(societe.entrepriseEur).eq('ouvertureEtablissement'))) ? true : false;
formFields['formalite_activiteAmbulanteEEE']                                                = (Value('id').of(societe.entrepriseFormeJuridique).eq('5800')
																							and Value('id').of(societe.entrepriseEur).eq('ouvertureAmbulant')) ? true : false;


// Cadre 2 - Déclaration relative à la société

formFields['entreprise_denomination']                                                       = societe.entrepriseDenomination;
formFields['entreprise_sigle']                                                              = societe.entrepriseSigle;
formFields['entreprise_formeJuridique']                                                     = societe.entrepriseFormeJuridique;
formFields['entreprise_associeUnique']                                                      = (Value('id').of(societe.entrepriseFormeJuridique).eq('5720') 
																							or (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
																							and societe.entrepriseAssocieUnique)) ? true : false;
formFields['entreprise_associeUniqueEstPresident']                                          = (societe.entrepriseAssocieUniquePresident1 or societe.entrepriseAssocieUniquePresident2) ? true : false;
formFields['entreprise_duree']                                                              = societe.entrepriseDuree + ' ' + "ans";
formFields['entreprise_capitalMontant']                                                     = societe.entrepriseCapitalMontant != null ? (societe.entrepriseCapitalMontant + ' ' + "euros") : (societe.entrepriseCapitalMontantEtr + ' ' + societe.entrepriseCapitalDevise.getId());
formFields['entreprise_capitalVariableMinimum']                                             = societe.entrepriseCapitalVariableMinimum != null ? (societe.entrepriseCapitalVariableMinimum + ' ' + "euros") : '';
if (societe.entrepriseDateClotureExerciceSocial !== null) {
    var dateTmp = new Date(parseInt(societe.entrepriseDateClotureExerciceSocial.getTimeInMillis()));
    var dateClotureEc = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateClotureEc = dateClotureEc.concat(pad(month.toString()));
    formFields['entreprise_dateClotureExercice']          = dateClotureEc;
}
if(societe.entrepriseDateCloturePremierExerciceSocial != null) {
	var dateTmp = new Date(parseInt(societe.entrepriseDateCloturePremierExerciceSocial.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['entreprise_dateClotureExerciceCasEcheant'] = date;
}
formFields['entreprise_adhesionSociete']                                                    = societe.entrepriseESS ? true : false;
formFields['entreprise_societeMission']                                                     = societe.entrepriseMission ? true : false;

// Cadre 3

formFields['entreprise_activitesPrincipales']                                               = siege.entrepriseActivitesPrincipales;

// Cadre 4

formFields['entreprise_fusionScission']                                                     = societe.entrepriseFusionScission ? true : false;

// Cadre 5

formFields['entreprise_autreEtablissementUE']                                               = societe.entrepriseAutreEtablissementUE ? true : false;

// Cadre 6

formFields['entreprise_adresseEntreprisePM_voie']                                           = (siege.cadreAdresseProfessionnelle.numeroAdresseSiege != null ? siege.cadreAdresseProfessionnelle.numeroAdresseSiege : '')
																							+ ' ' + (siege.cadreAdresseProfessionnelle.indiceAdresseSiege != null ? siege.cadreAdresseProfessionnelle.indiceAdresseSiege : '')
																							+ ' ' + (siege.cadreAdresseProfessionnelle.typeAdresseSiege != null ? siege.cadreAdresseProfessionnelle.typeAdresseSiege : '')
																							+ ' ' + siege.cadreAdresseProfessionnelle.voieAdresseSiege
																							+ ' ' + (siege.cadreAdresseProfessionnelle.complementAdresseSiege != null ? siege.cadreAdresseProfessionnelle.complementAdresseSiege : '')
																							+ ' ' + (siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege != null ? siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege : '');
formFields['entreprise_adresseEntreprisePM_codePostal']                                     = siege.cadreAdresseProfessionnelle.codePostalAdresseSiege != null ? siege.cadreAdresseProfessionnelle.codePostalAdresseSiege : (siege.cadreAdresseProfessionnelle.codePostalAdresseSiegeEtr != null ? siege.cadreAdresseProfessionnelle.codePostalAdresseSiegeEtr : '');
formFields['entreprise_adresseEntreprisePM_commune']                                        = siege.cadreAdresseProfessionnelle.communeAdresseSiege != null ? siege.cadreAdresseProfessionnelle.communeAdresseSiege : (siege.cadreAdresseProfessionnelle.villeAdresseSiege + ' / ' + siege.cadreAdresseProfessionnelle.paysAdresseSiege);
formFields['entreprise_adresseEntreprisePM_communeAncienne']                                = siege.cadreAdresseProfessionnelle.communeAncienneAdresseSiege != null ? siege.cadreAdresseProfessionnelle.communeAncienneAdresseSiege : '';
formFields['entreprise_adresseEntreprisePMSituation_domicile']                              = Value('id').of(siege.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? true : false;
if (Value('id').of(siege.activiteLieu).eq('etablissementDomiciliationOui')) {
formFields['entreprise_adresseEntreprisePMSituation_domiciliation']                         = true;
formFields['entrepriseLiee_siren_entrepriseDomiciliation']                                  = siege.entrepriseLieeSiren.split(' ').join('');
formFields['entrepriseLiee_nom']                                                            = siege.entrepriseLieeNom;
}

// Cadre 7

var etablissement = $m0SAS.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite ;

if (Value('id').of(societe.entrepriseFormeJuridique).eq('5800') or Value('id').of(societe.entrepriseFormeJuridique).eq('3110') or Value('id').of(societe.entrepriseFormeJuridique).eq('3120')) {
formFields['entrepriseLiee_societeEtrangere_lieuPays']                                            = societe.entrepriseLieuImmatriculation + ' ' + societe.entreprisePaysImmatriculation;
formFields['entrepriseLiee_societeEtrangere_immatriculation']                                     = societe.entrepriseNumeroImmatriculation;
}
if ((Value('id').of(societe.entrepriseFormeJuridique).eq('5800') and Value('id').of(societe.entrepriseEur).eq('ouvertureEtablissement')) or Value('id').of(societe.entrepriseFormeJuridique).eq('3110') or Value('id').of(societe.entrepriseFormeJuridique).eq('3120')) {
formFields['entrepriseLiee_societeEtrangere_adresseEtablissementVoie']							  = (etablissement.cadreAdresseEtablissement.numeroAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.numeroAdresseEtablissement : '')
																								  + ' ' + (etablissement.cadreAdresseEtablissement.indiceAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.indiceAdresseEtablissement : '')
																								  + ' ' + (etablissement.cadreAdresseEtablissement.typeAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.typeAdresseEtablissement : '')
																								  + ' ' + etablissement.cadreAdresseEtablissement.voieAdresseEtablissement
																								  + ' ' + (etablissement.cadreAdresseEtablissement.complementAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.complementAdresseEtablissement : '')
																								  + ' ' + (etablissement.cadreAdresseEtablissement.distributionSpecialeAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.distributionSpecialeAdresseEtablissement : '');
formFields['entrepriseLiee_societeEtrangere_codePostalEnFrance']                                  = etablissement.cadreAdresseEtablissement.codePostalAdresseEtablissement;
formFields['entrepriseLiee_societeEtrangere_communeEnFrance']                                     = etablissement.cadreAdresseEtablissement.communeAdresseEtablissement;
}
if (Value('id').of(societe.entrepriseFormeJuridique).eq('5800') and Value('id').of(societe.entrepriseEur).eq('ouvertureAmbulant')) {
formFields['entrepriseLiee_societeEtrangere_EEE_codePostal']                                      = etablissement.codePostalAdresseMarche;
formFields['entrepriseLiee_societeEtrangere_EEE_commune']                                         = etablissement.communeAdresseMarche;
}

// Cadre 8

if (siege.etablissementPrincipalDifferentSiege or ((Value('id').of(societe.entrepriseFormeJuridique).eq('5800') and Value('id').of(societe.entrepriseEur).eq('ouvertureEtablissement')) or Value('id').of(societe.entrepriseFormeJuridique).eq('3110') or Value('id').of(societe.entrepriseFormeJuridique).eq('3120')))  {
formFields['etablissement_adresse_voie']				   										  = (etablissement.cadreAdresseEtablissement.numeroAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.numeroAdresseEtablissement : '')
																								  + ' ' + (etablissement.cadreAdresseEtablissement.indiceAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.indiceAdresseEtablissement : '')
																								  + ' ' + (etablissement.cadreAdresseEtablissement.typeAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.typeAdresseEtablissement : '')
																								  + ' ' + etablissement.cadreAdresseEtablissement.voieAdresseEtablissement;
formFields['etablissement_adresse_complementVoie']                                                = (etablissement.cadreAdresseEtablissement.complementAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.complementAdresseEtablissement : '')
																								  + ' ' + (etablissement.cadreAdresseEtablissement.distributionSpecialeAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.distributionSpecialeAdresseEtablissement : '');
formFields['etablissement_adresse_codePostal']                  				                  = etablissement.cadreAdresseEtablissement.codePostalAdresseEtablissement;
formFields['etablissement_adresse_commune']          				                              = etablissement.cadreAdresseEtablissement.communeAdresseEtablissement;
formFields['etablissement_adresse_communeAncienne']                                               = etablissement.cadreAdresseEtablissement.communeAncienneAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.communeAncienneAdresseEtablissement : '';
} 

// Cadre 9

if (siege.formaliteCreationAvecActivite or Value('id').of(societe.entrepriseFormeJuridique).eq('5785') 
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585') or Value('id').of(societe.entrepriseFormeJuridique).eq('5685') 
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5385') or Value('id').of(societe.entrepriseFormeJuridique).eq('3120') 
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5800') or Value('id').of(societe.entrepriseFormeJuridique).eq('3110')) {
		
formFields['etablissement_nomCommercialProfessionnel']                                            = etablissement.etablissementNomCommercialProfessionnel != null ? etablissement.etablissementNomCommercialProfessionnel : '';
formFields['etablissement_enseigne']                                                              = etablissement.etablissementEnseigne != null ? etablissement.etablissementEnseigne : '';

// Cadre 10 

if(etablissement.etablissementDateDebutActivite != null) {
	var dateTmp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_dateDebutActivite'] = date;
}
formFields['etablissement_activitePermanenteSaisonniere_permanente']                              = Value('id').of(etablissement.activiteTemps).eq('etablissementActivitePermanenteSaisonnierePermanente') ? true : false;
formFields['etablissement_activitePermanenteSaisonniere_saisonniere']                             = Value('id').of(etablissement.activiteTemps).eq('etablissementActivitePermanenteSaisonniereSaisonniere') ? true : false;
formFields['etablissement_nonSedentariteQualite_ambulant']                                        = (etablissement.etablissementNonSedentariteQualiteAmbulant or (Value('id').of(societe.entrepriseFormeJuridique).eq('5800') and Value('id').of(societe.entrepriseEur).eq('ouvertureAmbulant'))) ? true : false;
formFields['etablissement_activites']                                                             = (etablissement.etablissementActivites.length > 165) ? (etablissement.etablissementActivites.substring(0, 150) + ' ' + "(suite en obs)") : etablissement.etablissementActivites;
formFields['etablissement_activitesPrincipale']                                             	  = etablissement.etablissementActivitesLaPlusImportante != null ? etablissement.etablissementActivitesLaPlusImportante : '';
formFields['etablissement_lieuExercice_magasin']                                                  = Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMagasin') ? true : false;
formFields['etablissement_surfaceMagasin']                                        			      = Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMagasin') ? etablissement.etablissementActiviteLieuExerciceMagasinSurface : '';
formFields['etablissement_lieuExercice_marche']                                                   = Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMarche') ? true : false;
formFields['etablissement_lieuExercice_internet']                                                 = Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceDetail') ? true : false;
formFields['etablissement_lieuExercice_gros']                                                     = Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceGros') ? true : false;
formFields['etablissement_activiteNature_fabricationProduction']                                  = Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureFabricationProduction') ? true : false;
formFields['etablissement_activiteNature_batTravauxPublics']                                      = Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureBatTravauxPublics') ? true : false;
formFields['etablissement_activiteLieuExercice_cocheAutre']                                       = (Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCocheAutre')
																									or Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
																									or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
																									or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')
																									or Value('id').of(societe.entrepriseFormeJuridique).eq('5385')
																									or siege.activiteLiberaleOui) ? true : false;
formFields['etablissement_activiteLieuExerciceAutre']                                             = Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCocheAutre') ? etablissement.etablissementActiviteNatureAutre : 
																									((Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
																									or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
																									or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')
																									or Value('id').of(societe.entrepriseFormeJuridique).eq('5385')
																									or siege.activiteLiberaleOui) ? "Profession libérale" : '');

// Cadre 11

var origine = $m0SAS.cadre4OrigineFondsGroup.cadre4OrigineFonds ;

if (siege.activiteLiberaleOui or Value('id').of(societe.entrepriseFormeJuridique).eq('5785') or Value('id').of(societe.entrepriseFormeJuridique).eq('5685') or Value('id').of(societe.entrepriseFormeJuridique).eq('5585') or Value('id').of(societe.entrepriseFormeJuridique).eq('5385')) {
formFields['etablissement_origineFonds_creation_activiteLiberale']                                = siege.etablissementPrincipalDifferentSiege ? (Value('id').of(origine.etablissementOrigineFondsLiberal).eq('EtablissementOrigineFondsCreation') ? true : false) : (not Value('id').of(siege.activiteLieu).eq('etablissementDomiciliationNon') ? true : (Value('id').of(origine.etablissementOrigineFondsLiberal).eq('EtablissementOrigineFondsCreation') ? true : false));
formFields['etablissement_origineFonds_reprise_activiteLiberale']                                 = Value('id').of(origine.etablissementOrigineFondsLiberal).eq('EtablissementOrigineFondsRepriseActiviteLiberale') ? true : false;
formFields['entrepriseLiee_siren_precedentExploitant_activiteLiberale']                           = origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant.split(' ').join('') : '';
formFields['entrepriseLiee_entreprisePP_nomNaissance_precedentExploitant_activiteLiberale']       = origine.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant : (origine.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant : '');
formFields['entrepriseLiee_entreprisePP_nomUsage_precedentExploitant_activiteLiberale']           = origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant : '';
formFields['entrepriseLiee_entreprisePP_prenom_precedentExploitant_activiteLiberale']             = origine.precedentExploitant.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant : '';
}
if (not siege.activiteLiberaleOui and not Value('id').of(societe.entrepriseFormeJuridique).eq('5785') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5685') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5585') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5385')) {
formFields['etablissement_origineFonds_creation_fondsCommerceArtisanal']                          = siege.etablissementPrincipalDifferentSiege ? 
																								(Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsCreation') ? true : false) : 
																								(not Value('id').of(siege.activiteLieu).eq('etablissementDomiciliationNon') ? true : 
																								(Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsCreation') ? true : 
																								((Value('id').of(societe.entrepriseFormeJuridique).eq('5800') and Value('id').of(societe.entrepriseEur).eq('ouvertureAmbulant')) ? true : false)));
formFields['etablissement_origineFonds_achat_fondsCommerceArtisanal']                             = Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsAchat') ? true : false;
formFields['etablissement_origineFonds_apport_fondsCommerceArtisanal']                            = Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsApport') ? true : false;
if(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParution != null) {
	var dateTmp = new Date(parseInt(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParution.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_journalAnnoncesLegalesDateParution'] = date;
} else if(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParutionBis != null) {
	var dateTmp = new Date(parseInt(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParutionBis.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_journalAnnoncesLegalesDateParution'] = date;
}
formFields['etablissement_journalAnnoncesLegalesNom']                                             = origine.precedentExploitant.etablissementJournalAnnoncesLegalesNom != null ? origine.precedentExploitant.etablissementJournalAnnoncesLegalesNom : (origine.precedentExploitant.etablissementJournalAnnoncesLegalesNomBis != null ? origine.precedentExploitant.etablissementJournalAnnoncesLegalesNomBis : '');
formFields['entrepriseLiee_siren_precedentExploitant_fondsCommerceArtisanal']                     = origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant.split(' ').join('') : '';
formFields['entrepriseLiee_entreprisePP_nomNaissance_precedentExploitant_fondsCommerceArtisanal'] = origine.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant : (origine.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant : '');
formFields['entrepriseLiee_entreprisePP_nomUsage_precedentExploitant_fondsCommerceArtisanal']     = origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant : '';
formFields['entrepriseLiee_entreprisePP_prenom_precedentExploitant_fondsCommerceArtisanal']       = origine.precedentExploitant.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant : '';
formFields['etablissement_origineFonds_locationGerance_fondsCommerceArtisanal']                   = Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsLocationGerance') ? true : false;
formFields['etablissement_origineFonds_geranceMandat_fondsCommerceArtisanal']                     = Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsGeranceMandat') ? true : false;
formFields['etablissement_origineFonds_cocheAutre_fondsCommerceArtisanal']                        = Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsCocheAutre') ? true : false;
formFields['etablissement_origineFonds_autre_fondsCommerceArtisanal']                             = Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsCocheAutre') ? origine.etablissementOrigineFondsAutre : '';
if(origine.geranceMandat.etablissementLoueurMandantDuFondsDebutContrat != null) {
	var dateTmp = new Date(parseInt(origine.geranceMandat.etablissementLoueurMandantDuFondsDebutContrat.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_loueurMandantDuFondsDebutContrat'] = date;
}
if(origine.geranceMandat.etablissementLoueurMandantDuFondsFinContrat != null) {
	var dateTmp = new Date(parseInt(origine.geranceMandat.etablissementLoueurMandantDuFondsFinContrat.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_loueurMandantDuFondsFinContrat'] = date;
}
if (Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsLocationGerance') or Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsGeranceMandat')) {
formFields['etablissement_loueurMandantDuFondsRenouvellementTaciteReconduction_oui']              = origine.geranceMandat.etablissementLoueurMandantDuFondsRenouvellementTaciteReconductionOui ? true : false;
formFields['etablissement_loueurMandantDuFondsRenouvellementTaciteReconduction_non']              = origine.geranceMandat.etablissementLoueurMandantDuFondsRenouvellementTaciteReconductionOui ? false : true ;
formFields['entrepriseLiee_entreprisePP_nomNaissance_loueurMandantDuFonds']                       = origine.geranceMandat.entrepriseLieeEntreprisePMDenominationLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeEntreprisePMDenominationLoueurMandantDuFonds : (origine.geranceMandat.entrepriseLieeEntreprisePMNomNaissanceLoueurMandantDuFonds !=null ? origine.geranceMandat.entrepriseLieeEntreprisePMNomNaissanceLoueurMandantDuFonds : '');
formFields['entrepriseLiee_entreprisePP_nomUsage_loueurMandantDuFonds']                           = origine.geranceMandat.entrepriseLieeEntreprisePPNomUsageLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeEntreprisePPNomUsageLoueurMandantDuFonds : '';
formFields['entrepriseLiee_entreprisePP_prenom_loueurMandantDuFonds']                             = origine.geranceMandat.entrepriseLieeEntreprisePPPrenom1LoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeEntreprisePPPrenom1LoueurMandantDuFonds : '';
formFields['entrepriseLiee_adresse_voie__loueurMandantDuFonds']                                   = (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.numRueAdresseLoueurMandantDufonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.numRueAdresseLoueurMandantDufonds : '') 
																									+ ' ' + (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.indiceVoieLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.indiceVoieLoueurMandantDuFonds : '')
																									+ ' ' + (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.typeVoieLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.typeVoieLoueurMandantDuFonds : '')
																									+ ' ' + origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.nomVoieLoueurMandantDuFonds
																									+ ' ' + (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.rueComplementAdresseLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.rueComplementAdresseLoueurMandantDuFonds : '')
																									+ ' ' + (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.distributionSpecialeLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.distributionSpecialeLoueurMandantDuFonds : '');                                                                                 
formFields['entrepriseLiee_adresse_codePostal_loueurMandantDuFonds']                              = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.codePostalLoueurMandantDuFonds;
formFields['entrepriseLiee_adresse_commune_loueurMandantDeFonds']                                 = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.communeLoueurMandantDuFonds;
formFields['entrepriseLiee_siren_geranceMandat']                                                  = origine.geranceMandat.entrepriseLieeSirenGeranceMandat != null ? origine.geranceMandat.entrepriseLieeSirenGeranceMandat.split(' ').join('') : '';
formFields['entrepriseLiee_greffeImmatriculation']                                                = origine.geranceMandat.entrepriseLieeGreffeImmatriculation != null ? origine.geranceMandat.entrepriseLieeGreffeImmatriculation : '';
}
}

// Cadre 12

formFields['etablissement_effectifSalariePresence_non']                                           = etablissement.etablissementEffectifSalariePresenceOui ? false : true;
formFields['etablissement_effectifSalariePresence_oui']                                           = etablissement.etablissementEffectifSalariePresenceOui ? true : false;
formFields['etablissement_effectifSalarieNombre']                                                 = etablissement.etablissementEffectifSalariePresenceOui ? etablissement.cadre3EffectifSalarie.etablissementEffectifSalarieNombre : '';
formFields['etablissement_effectifSalarieApprentis']                                              = etablissement.cadre3EffectifSalarie.etablissementEffectifSalarieApprentis != null ? etablissement.cadre3EffectifSalarie.etablissementEffectifSalarieApprentis : '';
formFields['etablissement_effectifSalarieEmbauchePremierSalarie_oui']                             = etablissement.cadre3EffectifSalarie.etablissementEffectifSalarieEmbauchePremierSalarieOui ? true : false;
formFields['etablissement_effectifSalarieEmbauchePremierSalarie_non']                             = etablissement.etablissementEffectifSalariePresenceOui ? (etablissement.cadre3EffectifSalarie.etablissementEffectifSalarieEmbauchePremierSalarieOui ? false : true) : false;
}

// Cadre 13 - Dirigeant 1

var dirigeant1 = $m0SAS.dirigeant1.cadreIdentiteDirigeant;

formFields['dirigeantQualite[0]']                                                           = dirigeant1.personneLieeQualite;
formFields['dirigeantNomNaissance[0]']                                                      = dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeant1.civilitePersonneMorale.personneLieePMDenomination) ;
formFields['dirigeantNomUsage[0]']                                                          = dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFields['dirigeantPrenoms[0]']                                                      = prenoms.toString();
} else if (dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFields['dirigeantPrenoms[0]']                                                      = prenoms.toString();
}
if (dirigeant1.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant1.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFields['dirigeantDateNaissance[0]']          = date1;
}  else if (dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFields['dirigeantDateNaissance[0]']          = date2;
}
formFields['dirigeantLieuNaissance[0]']                                              = dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																					(dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																					(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																					(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																					(dirigeant1.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? 
																					(dirigeant1.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : 
																					(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
																					(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFields['dirigeantNationalite[0]']                                               = dirigeant1.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant1.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
formFields['dirigeant_siegeVoie[0]']                                                = (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '') 
																					+ ' ' + (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
																					+ ' ' + (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
																					+ ' ' + dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNomVoie
																					+ ' ' + (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
																					+ ' ' + (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFields['dirigeant_siegeCP[0]']                                                  = dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFields['dirigeant_siegeCommune[0]']                                             = dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays);
if (Value('id').of(dirigeant1.personnaliteJuridique).eq('personneMorale')) {
formFields['dirigeant_formeJuridique[0]']                                           = dirigeant1.civilitePersonneMorale.personneLieePMFormeJuridique;
formFields['dirigeant_numeroImmatriculation[0]']                                    = dirigeant1.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant1.civilitePersonneMorale.personneLieePMNumeroIdentification : (dirigeant1.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee != null ? (dirigeant1.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee + ' ' + dirigeant1.civilitePersonneMorale.personneLieePMNomNonImmatriculee) : '');
formFields['dirigeant_lieuImmatriculation[0]']                                      = dirigeant1.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant1.civilitePersonneMorale.personneLieePMLieuImmatriculation : (dirigeant1.civilitePersonneMorale.personneLieePMLieuImmatriculation + ' / ' + dirigeant1.civilitePersonneMorale.personneLieePMPaysNonImmatriculee);
}

// Cadre 14 - Dirigeant 2

var dirigeant2 = $m0SAS.dirigeant2.cadreIdentiteDirigeant;

if (dirigeant1.autreDirigeant) {
formFields['dirigeantQualite[1]']                                                           = dirigeant2.personneLieeQualite;
formFields['dirigeantNomNaissance[1]']                                                      = dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeant2.civilitePersonneMorale.personneLieePMDenomination) ;
formFields['dirigeantNomUsage[1]']                                                          = dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFields['dirigeantPrenoms[1]']                                                      = prenoms.toString();
} else if (dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFields['dirigeantPrenoms[1]']                                                      = prenoms.toString();
}
if (dirigeant2.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant2.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFields['dirigeantDateNaissance[1]']          = date1;
}  else if (dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFields['dirigeantDateNaissance[1]']          = date2;
}
formFields['dirigeantLieuNaissance[1]']                                              = dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																					(dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																					(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																					(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																					(dirigeant2.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? 
																					(dirigeant2.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : 
																					(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
																					(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFields['dirigeantNationalite[1]']                                               = dirigeant2.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant2.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
formFields['dirigeant_siegeVoie[1]']                                                = (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '') 
																					+ ' ' + (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
																					+ ' ' + (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
																					+ ' ' + dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNomVoie
																					+ ' ' + (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
																					+ ' ' + (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFields['dirigeant_siegeCP[1]']                                                  = dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFields['dirigeant_siegeCommune[1]']                                             = dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays);
if (Value('id').of(dirigeant2.personnaliteJuridique).eq('personneMorale')) {
formFields['dirigeant_formeJuridique[1]']                                           = dirigeant2.civilitePersonneMorale.personneLieePMFormeJuridique;
formFields['dirigeant_numeroImmatriculation[1]']                                    = dirigeant2.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant2.civilitePersonneMorale.personneLieePMNumeroIdentification : (dirigeant2.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee != null ? (dirigeant2.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee + ' ' + dirigeant2.civilitePersonneMorale.personneLieePMNomNonImmatriculee) : '');
formFields['dirigeant_lieuImmatriculation[1]']                                      = dirigeant2.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant2.civilitePersonneMorale.personneLieePMLieuImmatriculation : (dirigeant2.civilitePersonneMorale.personneLieePMLieuImmatriculation + ' / ' + dirigeant2.civilitePersonneMorale.personneLieePMPaysNonImmatriculee);
}
}

// Cadre 15 - Dirigeant 3

var dirigeant3 = $m0SAS.dirigeant3.cadreIdentiteDirigeant;

if (dirigeant2.autreDirigeant) {
formFields['dirigeantQualite[2]']                                                           = dirigeant3.personneLieeQualite;
formFields['dirigeantNomNaissance[2]']                                                      = dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeant3.civilitePersonneMorale.personneLieePMDenomination) ;
formFields['dirigeantNomUsage[2]']                                                          = dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFields['dirigeantPrenoms[2]']                                                      = prenoms.toString();
} else if (dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFields['dirigeantPrenoms[2]']                                                      = prenoms.toString();
}
if (dirigeant3.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant3.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFields['dirigeantDateNaissance[2]']          = date1;
}  else if (dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFields['dirigeantDateNaissance[2]']          = date2;
}
formFields['dirigeantLieuNaissance[2]']                                              = dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																					(dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																					(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																					(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																					(dirigeant3.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? 
																					(dirigeant3.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : 
																					(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
																					(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFields['dirigeantNationalite[2]']                                               = dirigeant3.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant3.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
formFields['dirigeant_siegeVoie[2]']                                                = (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '') 
																					+ ' ' + (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
																					+ ' ' + (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
																					+ ' ' + dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNomVoie
																					+ ' ' + (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
																					+ ' ' + (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFields['dirigeant_siegeCP[2]']                                                  = dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFields['dirigeant_siegeCommune[2]']                                             = dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays);
if (Value('id').of(dirigeant3.personnaliteJuridique).eq('personneMorale')) {
formFields['dirigeant_formeJuridique[2]']                                           = dirigeant3.civilitePersonneMorale.personneLieePMFormeJuridique;
formFields['dirigeant_numeroImmatriculation[2]']                                    = dirigeant3.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant3.civilitePersonneMorale.personneLieePMNumeroIdentification : (dirigeant3.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee != null ? (dirigeant3.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee + ' ' + dirigeant3.civilitePersonneMorale.personneLieePMNomNonImmatriculee) : '');
formFields['dirigeant_lieuImmatriculation[2]']                                      = dirigeant3.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant3.civilitePersonneMorale.personneLieePMLieuImmatriculation : (dirigeant3.civilitePersonneMorale.personneLieePMLieuImmatriculation + ' / ' + dirigeant3.civilitePersonneMorale.personneLieePMPaysNonImmatriculee);
}
}

// Cadre 16 - Dirigeant 4

var dirigeant4 = $m0SAS.dirigeant4.cadreIdentiteDirigeant;

if (dirigeant3.autreDirigeant) {
formFields['dirigeantQualite[3]']                                                           = dirigeant4.personneLieeQualite;
formFields['dirigeantNomNaissance[3]']                                                      = dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeant4.civilitePersonneMorale.personneLieePMDenomination) ;
formFields['dirigeantNomUsage[3]']                                                          = dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFields['dirigeantPrenoms[3]']                                                      = prenoms.toString();
} else if (dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFields['dirigeantPrenoms[3]']                                                      = prenoms.toString();
}
if (dirigeant4.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant4.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFields['dirigeantDateNaissance[3]']          = date1;
}  else if (dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFields['dirigeantDateNaissance[3]']          = date2;
}
formFields['dirigeantLieuNaissance[3]']                                              = dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																					(dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																					(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																					(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																					(dirigeant4.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? 
																					(dirigeant4.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : 
																					(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
																					(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFields['dirigeantNationalite[3]']                                               = dirigeant4.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant4.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
formFields['dirigeant_siegeVoie[3]']                                                = (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '') 
																					+ ' ' + (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
																					+ ' ' + (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
																					+ ' ' + dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNomVoie
																					+ ' ' + (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
																					+ ' ' + (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFields['dirigeant_siegeCP[3]']                                                  = dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFields['dirigeant_siegeCommune[3]']                                             = dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays);
if (Value('id').of(dirigeant4.personnaliteJuridique).eq('personneMorale')) {
formFields['dirigeant_formeJuridique[3]']                                           = dirigeant4.civilitePersonneMorale.personneLieePMFormeJuridique;
formFields['dirigeant_numeroImmatriculation[3]']                                    = dirigeant4.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant4.civilitePersonneMorale.personneLieePMNumeroIdentification : (dirigeant4.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee != null ? (dirigeant4.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee + ' ' + dirigeant4.civilitePersonneMorale.personneLieePMNomNonImmatriculee) : '');
formFields['dirigeant_lieuImmatriculation[3]']                                      = dirigeant4.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant4.civilitePersonneMorale.personneLieePMLieuImmatriculation : (dirigeant4.civilitePersonneMorale.personneLieePMLieuImmatriculation + ' / ' + dirigeant4.civilitePersonneMorale.personneLieePMPaysNonImmatriculee);
}
}

// Cadre 17 - Dirigeant 5

var dirigeant5 = $m0SAS.dirigeant5.cadreIdentiteDirigeant;

if (dirigeant4.autreDirigeant) {
formFields['dirigeantQualite[4]']                                                           = dirigeant5.personneLieeQualite;
formFields['dirigeantNomNaissance[4]']                                                      = dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeant5.civilitePersonneMorale.personneLieePMDenomination) ;
formFields['dirigeantNomUsage[4]']                                                          = dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFields['dirigeantPrenoms[4]']                                                      = prenoms.toString();
} else if (dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFields['dirigeantPrenoms[4]']                                                      = prenoms.toString();
}
if (dirigeant5.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant5.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFields['dirigeantDateNaissance[4]']          = date1;
}  else if (dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFields['dirigeantDateNaissance[4]']          = date2;
}
formFields['dirigeantLieuNaissance[4]']                                              = dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																					(dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																					(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																					(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																					(dirigeant5.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? 
																					(dirigeant5.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : 
																					(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
																					(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFields['dirigeantNationalite[4]']                                               = dirigeant5.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant5.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
formFields['dirigeant_siegeVoie[4]']                                                = (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '') 
																					+ ' ' + (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
																					+ ' ' + (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
																					+ ' ' + dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNomVoie
																					+ ' ' + (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
																					+ ' ' + (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFields['dirigeant_siegeCP[4]']                                                  = dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFields['dirigeant_siegeCommune[4]']                                             = dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays);
if (Value('id').of(dirigeant5.personnaliteJuridique).eq('personneMorale')) {
formFields['dirigeant_formeJuridique[4]']                                           = dirigeant5.civilitePersonneMorale.personneLieePMFormeJuridique;
formFields['dirigeant_numeroImmatriculation[4]']                                    = dirigeant5.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant5.civilitePersonneMorale.personneLieePMNumeroIdentification : (dirigeant5.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee != null ? (dirigeant5.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee + ' ' + dirigeant5.civilitePersonneMorale.personneLieePMNomNonImmatriculee) : '');
formFields['dirigeant_lieuImmatriculation[4]']                                      = dirigeant5.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant5.civilitePersonneMorale.personneLieePMLieuImmatriculation : (dirigeant5.civilitePersonneMorale.personneLieePMLieuImmatriculation + ' / ' + dirigeant5.civilitePersonneMorale.personneLieePMPaysNonImmatriculee);
}
}

// Cadre 18 - Dirigeant 6

var dirigeant6 = $m0SAS.dirigeant6.cadreIdentiteDirigeant;

if (dirigeant5.autreDirigeant) {
formFields['dirigeantQualite[5]']                                                           = dirigeant6.personneLieeQualite;
formFields['dirigeantNomNaissance[5]']                                                      = dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeant6.civilitePersonneMorale.personneLieePMDenomination) ;
formFields['dirigeantNomUsage[5]']                                                          = dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFields['dirigeantPrenoms[5]']                                                      = prenoms.toString();
} else if (dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFields['dirigeantPrenoms[5]']                                                      = prenoms.toString();
}
if (dirigeant6.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant6.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFields['dirigeantDateNaissance[5]']          = date1;
}  else if (dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFields['dirigeantDateNaissance[5]']          = date2;
}
formFields['dirigeantLieuNaissance[5]']                                              = dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																					(dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																					(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																					(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																					(dirigeant6.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? 
																					(dirigeant6.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : 
																					(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
																					(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFields['dirigeantNationalite[5]']                                               = dirigeant6.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant6.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
formFields['dirigeant_siegeVoie[5]']                                                = (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '') 
																					+ ' ' + (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant6.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
																					+ ' ' + (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant6.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
																					+ ' ' + dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNomVoie
																					+ ' ' + (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant6.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
																					+ ' ' + (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant6.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFields['dirigeant_siegeCP[5]']                                                  = dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFields['dirigeant_siegeCommune[5]']                                             = dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays);
if (Value('id').of(dirigeant6.personnaliteJuridique).eq('personneMorale')) {
formFields['dirigeant_formeJuridique[5]']                                           = dirigeant6.civilitePersonneMorale.personneLieePMFormeJuridique;
formFields['dirigeant_numeroImmatriculation[5]']                                    = dirigeant6.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant6.civilitePersonneMorale.personneLieePMNumeroIdentification : (dirigeant6.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee != null ? (dirigeant6.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee + ' ' + dirigeant6.civilitePersonneMorale.personneLieePMNomNonImmatriculee) : '');
formFields['dirigeant_lieuImmatriculation[5]']                                      = dirigeant6.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant6.civilitePersonneMorale.personneLieePMLieuImmatriculation : (dirigeant6.civilitePersonneMorale.personneLieePMLieuImmatriculation + ' / ' + dirigeant6.civilitePersonneMorale.personneLieePMPaysNonImmatriculee);
}
}

// Cadre 19 Options fiscales

var fiscal = $m0SAS.cadre20optFiscGroup.cadre20optFisc ;

formFields['regimeFiscal_regimeImpositionBenefices_rsBIC']                                        = (Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeCommerciale).eq('regimeFiscalRegimeImpositionBeneficesReelBIC') and Value('id').of(fiscal.impositionBenefices.regimeFiscalReel).eq('regimeFiscalRegimeImpositionBeneficesRs')) ? true : false;
formFields['regimeFiscal_regimeImpositionBenefices_rnBIC']                                        = (Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeCommerciale).eq('regimeFiscalRegimeImpositionBeneficesReelBIC') and Value('id').of(fiscal.impositionBenefices.regimeFiscalReel).eq('regimeFiscalRegimeImpositionBeneficesRn')) ? true : false;
formFields['regimeFiscal_regimeImpositionBenefices_dcBNC']                                        = Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberale).eq('regimeFiscalRegimeImpositionBeneficesDcBNC') ? true : false;
formFields['regimeFiscal_regimeImpositionBenefices_rsIS']                                         = ((Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberale).eq('regimeFiscalRegimeImpositionBeneficesOptionIS') or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeCommerciale).eq('regimeFiscalRegimeImpositionBeneficesOptionIS')) and Value('id').of(fiscal.impositionBenefices.regimeFiscalReel).eq('regimeFiscalRegimeImpositionBeneficesRs')) ? true : false;
formFields['regimeFiscal_regimeImpositionBenefices_rnIS']                                         = ((Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberale).eq('regimeFiscalRegimeImpositionBeneficesOptionIS') or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeCommerciale).eq('regimeFiscalRegimeImpositionBeneficesOptionIS')) and Value('id').of(fiscal.impositionBenefices.regimeFiscalReel).eq('regimeFiscalRegimeImpositionBeneficesRn')) ? true : false;
formFields['regimeFiscal_regimeImpositionBeneficesOptionsParticulieres_optionIS']                 = ((Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberale).eq('regimeFiscalRegimeImpositionBeneficesOptionIS') or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeCommerciale).eq('regimeFiscalRegimeImpositionBeneficesOptionIS')) and (Value('id').of(societe.entrepriseFormeJuridique).eq('5306') or Value('id').of(societe.entrepriseFormeJuridique).eq('5307') or Value('id').of(societe.entrepriseFormeJuridique).eq('5202') or Value('id').of(societe.entrepriseFormeJuridique).eq('5203'))) ? true : false;
formFields['regimeFiscal_regimeImpositionBeneficesOptionsParticulieres_regimeSocietePersonnes']   = ((Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberale).eq('regimeFiscalRegimeImpositionBeneficesDcBNC') or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeCommerciale).eq('regimeFiscalRegimeImpositionBeneficesReelBIC')) and (not Value('id').of(societe.entrepriseFormeJuridique).eq('5306') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5307') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5202') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5203') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5308') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5309') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5385') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5800') and not Value('id').of(societe.entrepriseFormeJuridique).eq('3110') and not Value('id').of(societe.entrepriseFormeJuridique).eq('3120'))) ? true : false;
formFields['regimeFiscal_regimeImpositionTVA_rfTVA']                                              = (Value('id').of(fiscal.optionsTVA.regimeTVA1).eq('regimeFiscalRegimeImpositionTVARfTVA') or Value('id').of(fiscal.optionsTVA.regimeTVA2).eq('regimeFiscalRegimeImpositionTVARfTVA') or Value('id').of(fiscal.optionsTVA.regimeTVA3).eq('regimeFiscalRegimeImpositionTVARfTVA')) ? true : false;
formFields['regimeFiscal_regimeImpositionTVA_rsTVA']                                              = (Value('id').of(fiscal.optionsTVA.regimeTVA1).eq('regimeFiscalRegimeImpositionTVARsTVA') or Value('id').of(fiscal.optionsTVA.regimeTVA3).eq('regimeFiscalRegimeImpositionTVARsTVA')) ? true : false;
formFields['regimeFiscal_regimeImpositionTVA_mrTVA']                                              = Value('id').of(fiscal.optionsTVA.regimeTVA1).eq('regimeFiscalRegimeImpositionTVAMrTVA') ? true : false;
formFields['regimeFiscal_regimeImpositionTVA_rnTVA']                                              = (Value('id').of(fiscal.optionsTVA.regimeTVA2).eq('regimeFiscalRegimeImpositionTVARnTVA') or Value('id').of(fiscal.optionsTVA.regimeTVA3).eq('regimeFiscalRegimeImpositionTVARnTVA')) ? true : false;
formFields['regimeFiscal_regimeImpositionTVAOptionsParticulieres2']                               = fiscal.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres2 ? true : false;
formFields['regimeFiscal_regimeImpositionTVAOptionsParticulieres1']                               = fiscal.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres1 ? true : false;

// Cadre 20 Observations

var correspondance = $m0SAS.cadre10RensCompGroup.cadre10RensComp ;

formFields['formalite_observations']                                                    = ((etablissement.etablissementActivites.length > 165) ? ("Suite activité :" + ' ' + etablissement.etablissementActivites.substring(150, 420)) : '') + ' ' + (correspondance.formaliteObservations != null ? ('/ ' + correspondance.formaliteObservations) : '');

// Cadres 19 Correspondance

formFields['formalite_cocheAdresseCorrespondance']                                      = (Value('id').of(correspondance.adresseCorrespond).eq('siege') or Value('id').of(correspondance.adresseCorrespond).eq('etablissement')) ? true : false;
formFields['formalite_declareeCadreNumero']                                             = Value('id').of(correspondance.adresseCorrespond).eq('siege') ? "6" : (Value('id').of(correspondance.adresseCorrespond).eq('etablissement') ? "8" : ' ');
formFields['formalite_correspondanceAdresse']                                           = Value('id').of(correspondance.adresseCorrespond).eq('autre') ? true : false;
if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
formFields['formalite_correspondanceAdresse_voie']                                      = correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance
																						  + ' ' + (correspondance.adresseCorrespondance.numeroRueAdresseCorrespondance != null ? correspondance.adresseCorrespondance.numeroRueAdresseCorrespondance : '')
																						  + ' ' + (correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance : '')
																						  + ' ' + (correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance : '')
																						  + ' ' + correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance;
formFields['formalite_correspondanceAdresse_complementVoie']                            = (correspondance.adresseCorrespondance.rueComplementAdresseCorrespondance != null ? correspondance.adresseCorrespondance.rueComplementAdresseCorrespondance : '')
																						  + ' ' + (correspondance.adresseCorrespondance.distriutionSpecialeVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.distriutionSpecialeVoieAdresseCorrespondance : '');
formFields['formalite_correspondanceAdresse_codePostal']                                = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal;
formFields['formalite_correspondanceAdresse_commune']									= correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune;
}
formFields['formalite_telephone1']                                                      = correspondance.infosSup.formaliteTelephone1;
formFields['formalite_telephone2']                                                      = correspondance.infosSup.formaliteTelephone2;
formFields['formalite_fax_courriel']                                                    = correspondance.infosSup.formaliteFaxCourriel != null ? correspondance.infosSup.formaliteFaxCourriel : correspondance.infosSup.telecopie;

// Cadres 20 - Signature

var signataire = $m0SAS.cadre11SignatureGroup.cadre11Signature ;
var jqpa1 = $m0SAS.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification1;
var jqpa2 = $m0SAS.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification2;
var jqpa3 = $m0SAS.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification3;

formFields['formalite_representantLegal']                                               = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentantLegal') ? true : false;
formFields['formalite_representantLegal_declareeCadreNumero']                           = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentantLegal') ? (not dirigeant1.autreDirigeant or Value('id').of(signataire.representantLegalNumero).eq('gerant1') ? "13" : (Value('id').of(signataire.representantLegalNumero).eq('gerant2') ? "14" : (Value('id').of(signataire.representantLegalNumero).eq('gerant3') ? "15" : (Value('id').of(signataire.representantLegalNumero).eq('gerant4') ? "16" : (Value('id').of(signataire.representantLegalNumero).eq('gerant5') ? "17" : (Value('id').of(signataire.representantLegalNumero).eq('gerant6') ? "18" : (Value('id').of(signataire.representantLegalNumero).eq('gerant7') ? "2 M0'" : (Value('id').of(signataire.representantLegalNumero).eq('gerant8') ? "3 M0'" : (Value('id').of(signataire.representantLegalNumero).eq('gerant9') ? "4 M0'" : (Value('id').of(signataire.representantLegalNumero).eq('gerant10') ? "5 M0'" : (Value('id').of(signataire.representantLegalNumero).eq('gerant11') ? "6 M0'" : (Value('id').of(signataire.representantLegalNumero).eq('gerant12') ? "7 M0'" : (Value('id').of(signataire.representantLegalNumero).eq('gerant13') ? "8 M0'" : (Value('id').of(signataire.representantLegalNumero).eq('gerant14') ? "9 M0'" : '')))))))))))))) : '';
formFields['formalite_mandataire']                                                      = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFields['formalite_signataireNom']						           					= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFields['formalite_signataireAdresse_voie']                                          = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																						  + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																						  + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																						  + ' ' + signataire.adresseMandataire.nomVoieMandataire
																						  + ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																						  + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');
formFields['formalite_signataireAdresse_CP']                                            = signataire.adresseMandataire.codePostalMandataire;
formFields['formalite_signataireAdresse_commune']                                       = signataire.adresseMandataire.villeAdresseMandataire;
}
formFields['formalite_signatureLieu']                                                   = signataire.formaliteSignatureLieu;
if (signataire.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFields['formalite_signatureDate']          = date1;
}
// A conditionner selon l'activité
formFields['formalite_nombreIntercalairesTNS']                                          = '';
formFields['formalite_nombreIntercalairesJQPA']                                         = (jqpa3.jqpaSituation != null ? "3" : (jqpa2.jqpaSituation != null ? "2" : (jqpa1.jqpaSituation != null ? "1" : "0")));
formFields['nombreIntercalaireMBE']                                                     = signataire.nombreMBE.getLabel();
formFields['signature']                                                                 = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce";


// M0 prime 1

var formFieldsPers1 = {}

// Cadre 1

formFieldsPers1['numeroIntercalaireM0Prime']                                            = "01" 
formFieldsPers1['entreprise_denomination']                                              = societe.entrepriseDenomination;
formFieldsPers1['entreprise_formeJuridique']                                            = societe.entrepriseFormeJuridique;

// Cadre 2 - Dirigeant 1M0

var dirigeant7 = $m0SAS.dirigeant7.cadreIdentiteDirigeant;

if (dirigeant6.autreDirigeant) {
formFieldsPers1['dirigeantQualite[6]']                                                           = dirigeant7.personneLieeQualite;
formFieldsPers1['dirigeantNomNaissance[6]']                                                      = dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeant7.civilitePersonneMorale.personneLieePMDenomination) ;
formFieldsPers1['dirigeantNomUsage[6]']                                                          = dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['dirigeantPrenoms[6]']                                                      = prenoms.toString();
} else if (dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['dirigeantPrenoms[6]']                                                      = prenoms.toString();
}
if (dirigeant7.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant7.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['dirigeantDateNaissance[6]']          = date1;
}  else if (dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['dirigeantDateNaissance[6]']          = date2;
}
formFieldsPers1['dirigeantLieuNaissance[6]']                                              = dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																					(dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																					(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																					(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																					(dirigeant7.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? 
																					(dirigeant7.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : 
																					(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
																					(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers1['dirigeantNationalite[6]']                                               = dirigeant7.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant7.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
formFieldsPers1['dirigeant_siegeVoie[6]']                                                = (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '') 
																					+ ' ' + (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant7.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
																					+ ' ' + (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant7.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
																					+ ' ' + dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNomVoie
																					+ ' ' + (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant7.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
																					+ ' ' + (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant7.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers1['dirigeant_siegeCP[6]']                                                  = dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers1['dirigeant_siegeCommune[6]']                                             = dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays);
if (Value('id').of(dirigeant7.personnaliteJuridique).eq('personneMorale')) {
formFieldsPers1['dirigeant_formeJuridique[6]']                                           = dirigeant7.civilitePersonneMorale.personneLieePMFormeJuridique;
formFieldsPers1['dirigeant_numeroImmatriculation[6]']                                    = dirigeant7.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant7.civilitePersonneMorale.personneLieePMNumeroIdentification : (dirigeant7.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee != null ? (dirigeant7.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee + ' ' + dirigeant7.civilitePersonneMorale.personneLieePMNomNonImmatriculee) : '');
formFieldsPers1['dirigeant_lieuImmatriculation[6]']                                      = dirigeant7.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant7.civilitePersonneMorale.personneLieePMLieuImmatriculation : (dirigeant7.civilitePersonneMorale.personneLieePMLieuImmatriculation + ' / ' + dirigeant7.civilitePersonneMorale.personneLieePMPaysNonImmatriculee);
}
}

// Cadre 3 - Dirigeant 2M0

var dirigeant8 = $m0SAS.dirigeant8.cadreIdentiteDirigeant;

if (dirigeant7.autreDirigeant) {
formFieldsPers1['dirigeantQualite[7]']                                                           = dirigeant8.personneLieeQualite;
formFieldsPers1['dirigeantNomNaissance[7]']                                                      = dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeant8.civilitePersonneMorale.personneLieePMDenomination) ;
formFieldsPers1['dirigeantNomUsage[7]']                                                          = dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['dirigeantPrenoms[7]']                                                      = prenoms.toString();
} else if (dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['dirigeantPrenoms[7]']                                                      = prenoms.toString();
}
if (dirigeant8.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant8.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['dirigeantDateNaissance[7]']          = date1;
}  else if (dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['dirigeantDateNaissance[7]']          = date2;
}
formFieldsPers1['dirigeantLieuNaissance[7]']                                              = dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																					(dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																					(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																					(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																					(dirigeant8.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? 
																					(dirigeant8.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : 
																					(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
																					(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers1['dirigeantNationalite[7]']                                               = dirigeant8.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant8.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
formFieldsPers1['dirigeant_siegeVoie[7]']                                                = (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '') 
																					+ ' ' + (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant8.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
																					+ ' ' + (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant8.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
																					+ ' ' + dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNomVoie
																					+ ' ' + (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant8.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
																					+ ' ' + (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant8.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers1['dirigeant_siegeCP[7]']                                                  = dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers1['dirigeant_siegeCommune[7]']                                             = dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays);
if (Value('id').of(dirigeant8.personnaliteJuridique).eq('personneMorale')) {
formFieldsPers1['dirigeant_formeJuridique[7]']                                           = dirigeant8.civilitePersonneMorale.personneLieePMFormeJuridique;
formFieldsPers1['dirigeant_numeroImmatriculation[7]']                                    = dirigeant8.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant8.civilitePersonneMorale.personneLieePMNumeroIdentification : (dirigeant8.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee != null ? (dirigeant8.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee + ' ' + dirigeant8.civilitePersonneMorale.personneLieePMNomNonImmatriculee) : '');
formFieldsPers1['dirigeant_lieuImmatriculation[7]']                                      = dirigeant8.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant8.civilitePersonneMorale.personneLieePMLieuImmatriculation : (dirigeant8.civilitePersonneMorale.personneLieePMLieuImmatriculation + ' / ' + dirigeant8.civilitePersonneMorale.personneLieePMPaysNonImmatriculee);
}
}

// Cadre 4 - Dirigeant 3M0

var dirigeant9 = $m0SAS.dirigeant9.cadreIdentiteDirigeant;

if (dirigeant8.autreDirigeant) {
formFieldsPers1['dirigeantQualite[8]']                                                           = dirigeant9.personneLieeQualite;
formFieldsPers1['dirigeantNomNaissance[8]']                                                      = dirigeant9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeant9.civilitePersonneMorale.personneLieePMDenomination) ;
formFieldsPers1['dirigeantNomUsage[8]']                                                          = dirigeant9.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant9.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['dirigeantPrenoms[8]']                                                      = prenoms.toString();
} else if (dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['dirigeantPrenoms[8]']                                                      = prenoms.toString();
}
if (dirigeant9.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant9.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['dirigeantDateNaissance[8]']          = date1;
}  else if (dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['dirigeantDateNaissance[8]']          = date2;
}
formFieldsPers1['dirigeantLieuNaissance[8]']                                              = dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																					(dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																					(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																					(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																					(dirigeant9.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? 
																					(dirigeant9.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : 
																					(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
																					(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers1['dirigeantNationalite[8]']                                               = dirigeant9.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant9.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
formFieldsPers1['dirigeant_siegeVoie[8]']                                                = (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant9.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '') 
																					+ ' ' + (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant9.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
																					+ ' ' + (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant9.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
																					+ ' ' + dirigeant9.cadreAdresseDirigeant.dirigeantAdresseNomVoie
																					+ ' ' + (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant9.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
																					+ ' ' + (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant9.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers1['dirigeant_siegeCP[8]']                                                  = dirigeant9.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant9.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers1['dirigeant_siegeCommune[8]']                                             = dirigeant9.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant9.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays);
if (Value('id').of(dirigeant9.personnaliteJuridique).eq('personneMorale')) {
formFieldsPers1['dirigeant_formeJuridique[8]']                                           = dirigeant9.civilitePersonneMorale.personneLieePMFormeJuridique;
formFieldsPers1['dirigeant_numeroImmatriculation[8]']                                    = dirigeant9.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant9.civilitePersonneMorale.personneLieePMNumeroIdentification : (dirigeant9.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee != null ? (dirigeant9.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee + ' ' + dirigeant9.civilitePersonneMorale.personneLieePMNomNonImmatriculee) : '');
formFieldsPers1['dirigeant_lieuImmatriculation[8]']                                      = dirigeant9.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant9.civilitePersonneMorale.personneLieePMLieuImmatriculation : (dirigeant9.civilitePersonneMorale.personneLieePMLieuImmatriculation + ' / ' + dirigeant9.civilitePersonneMorale.personneLieePMPaysNonImmatriculee);
}
}

// Cadre 5 - Dirigeant 4M0

var dirigeant10 = $m0SAS.dirigeant10.cadreIdentiteDirigeant;

if (dirigeant9.autreDirigeant) {
formFieldsPers1['dirigeantQualite[9]']                                                           = dirigeant10.personneLieeQualite;
formFieldsPers1['dirigeantNomNaissance[9]']                                                      = dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeant10.civilitePersonneMorale.personneLieePMDenomination) ;
formFieldsPers1['dirigeantNomUsage[9]']                                                          = dirigeant10.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant10.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['dirigeantPrenoms[9]']                                                      = prenoms.toString();
} else if (dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['dirigeantPrenoms[9]']                                                      = prenoms.toString();
}
if (dirigeant10.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant10.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['dirigeantDateNaissance[9]']          = date1;
}  else if (dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['dirigeantDateNaissance[9]']          = date2;
}
formFieldsPers1['dirigeantLieuNaissance[9]']                                              = dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																					(dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																					(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																					(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																					(dirigeant10.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? 
																					(dirigeant10.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : 
																					(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
																					(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers1['dirigeantNationalite[9]']                                               = dirigeant10.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant10.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
formFieldsPers1['dirigeant_siegeVoie[9]']                                                = (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant10.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '') 
																					+ ' ' + (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant10.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
																					+ ' ' + (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant10.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
																					+ ' ' + dirigeant10.cadreAdresseDirigeant.dirigeantAdresseNomVoie
																					+ ' ' + (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant10.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
																					+ ' ' + (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant10.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers1['dirigeant_siegeCP[9]']                                                  = dirigeant10.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant10.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers1['dirigeant_siegeCommune[9]']                                             = dirigeant10.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant10.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays);
if (Value('id').of(dirigeant10.personnaliteJuridique).eq('personneMorale')) {
formFieldsPers1['dirigeant_formeJuridique[9]']                                           = dirigeant10.civilitePersonneMorale.personneLieePMFormeJuridique;
formFieldsPers1['dirigeant_numeroImmatriculation[9]']                                    = dirigeant10.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant10.civilitePersonneMorale.personneLieePMNumeroIdentification : (dirigeant10.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee != null ? (dirigeant10.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee + ' ' + dirigeant10.civilitePersonneMorale.personneLieePMNomNonImmatriculee) : '');
formFieldsPers1['dirigeant_lieuImmatriculation[9]']                                      = dirigeant10.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant10.civilitePersonneMorale.personneLieePMLieuImmatriculation : (dirigeant10.civilitePersonneMorale.personneLieePMLieuImmatriculation + ' / ' + dirigeant10.civilitePersonneMorale.personneLieePMPaysNonImmatriculee);
}
}

// Cadre 6 - Dirigeant 5M0

var dirigeant11 = $m0SAS.dirigeant11.cadreIdentiteDirigeant;

if (dirigeant10.autreDirigeant) {
formFieldsPers1['dirigeantQualite[10]']                                                           = dirigeant11.personneLieeQualite;
formFieldsPers1['dirigeantNomNaissance[10]']                                                      = dirigeant11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeant11.civilitePersonneMorale.personneLieePMDenomination) ;
formFieldsPers1['dirigeantNomUsage[10]']                                                          = dirigeant11.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant11.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['dirigeantPrenoms[10]']                                                      = prenoms.toString();
} else if (dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['dirigeantPrenoms[10]']                                                      = prenoms.toString();
}
if (dirigeant11.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant11.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['dirigeantDateNaissance[10]']          = date1;
}  else if (dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['dirigeantDateNaissance[10]']          = date2;
}
formFieldsPers1['dirigeantLieuNaissance[10]']                                              = dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																					(dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																					(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																					(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																					(dirigeant11.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? 
																					(dirigeant11.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : 
																					(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
																					(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers1['dirigeantNationalite[10]']                                               = dirigeant11.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant11.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
formFieldsPers1['dirigeant_siegeVoie[10]']                                                = (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant11.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '') 
																					+ ' ' + (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant11.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
																					+ ' ' + (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant11.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
																					+ ' ' + dirigeant11.cadreAdresseDirigeant.dirigeantAdresseNomVoie
																					+ ' ' + (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant11.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
																					+ ' ' + (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant11.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers1['dirigeant_siegeCP[10]']                                                  = dirigeant11.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant11.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers1['dirigeant_siegeCommune[10]']                                             = dirigeant11.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant11.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays);
if (Value('id').of(dirigeant11.personnaliteJuridique).eq('personneMorale')) {
formFieldsPers1['dirigeant_formeJuridique[10]']                                           = dirigeant11.civilitePersonneMorale.personneLieePMFormeJuridique;
formFieldsPers1['dirigeant_numeroImmatriculation[10]']                                    = dirigeant11.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant11.civilitePersonneMorale.personneLieePMNumeroIdentification : (dirigeant11.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee != null ? (dirigeant11.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee + ' ' + dirigeant11.civilitePersonneMorale.personneLieePMNomNonImmatriculee) : '');
formFieldsPers1['dirigeant_lieuImmatriculation[10]']                                      = dirigeant11.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant11.civilitePersonneMorale.personneLieePMLieuImmatriculation : (dirigeant11.civilitePersonneMorale.personneLieePMLieuImmatriculation + ' / ' + dirigeant11.civilitePersonneMorale.personneLieePMPaysNonImmatriculee);
}
}

// Cadre 7 - Dirigeant 6M0

var dirigeant12 = $m0SAS.dirigeant12.cadreIdentiteDirigeant;

if (dirigeant11.autreDirigeant) {
formFieldsPers1['dirigeantQualite[11]']                                                           = dirigeant12.personneLieeQualite;
formFieldsPers1['dirigeantNomNaissance[11]']                                                      = dirigeant12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeant12.civilitePersonneMorale.personneLieePMDenomination) ;
formFieldsPers1['dirigeantNomUsage[11]']                                                          = dirigeant12.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant12.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['dirigeantPrenoms[11]']                                                      = prenoms.toString();
} else if (dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['dirigeantPrenoms[11]']                                                      = prenoms.toString();
}
if (dirigeant12.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant12.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['dirigeantDateNaissance[11]']          = date1;
}  else if (dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['dirigeantDateNaissance[11]']          = date2;
}
formFieldsPers1['dirigeantLieuNaissance[11]']                                              = dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																					(dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																					(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																					(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																					(dirigeant12.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? 
																					(dirigeant12.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : 
																					(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
																					(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers1['dirigeantNationalite[11]']                                               = dirigeant12.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant12.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
formFieldsPers1['dirigeant_siegeVoie[11]']                                                = (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant12.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '') 
																					+ ' ' + (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant12.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
																					+ ' ' + (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant12.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
																					+ ' ' + dirigeant12.cadreAdresseDirigeant.dirigeantAdresseNomVoie
																					+ ' ' + (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant12.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
																					+ ' ' + (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant12.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers1['dirigeant_siegeCP[11]']                                                  = dirigeant12.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant12.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers1['dirigeant_siegeCommune[11]']                                             = dirigeant12.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant12.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays);
if (Value('id').of(dirigeant12.personnaliteJuridique).eq('personneMorale')) {
formFieldsPers1['dirigeant_formeJuridique[11]']                                           = dirigeant12.civilitePersonneMorale.personneLieePMFormeJuridique;
formFieldsPers1['dirigeant_numeroImmatriculation[11]']                                    = dirigeant12.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant12.civilitePersonneMorale.personneLieePMNumeroIdentification : (dirigeant12.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee != null ? (dirigeant12.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee + ' ' + dirigeant12.civilitePersonneMorale.personneLieePMNomNonImmatriculee) : '');
formFieldsPers1['dirigeant_lieuImmatriculation[11]']                                      = dirigeant12.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant12.civilitePersonneMorale.personneLieePMLieuImmatriculation : (dirigeant12.civilitePersonneMorale.personneLieePMLieuImmatriculation + ' / ' + dirigeant12.civilitePersonneMorale.personneLieePMPaysNonImmatriculee);
}
}

// Cadre 8 - Dirigeant 7M0

var dirigeant13 = $m0SAS.dirigeant13.cadreIdentiteDirigeant;

if (dirigeant12.autreDirigeant) {
formFieldsPers1['dirigeantQualite[12]']                                                           = dirigeant13.personneLieeQualite;
formFieldsPers1['dirigeantNomNaissance[12]']                                                      = dirigeant13.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant13.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeant13.civilitePersonneMorale.personneLieePMDenomination) ;
formFieldsPers1['dirigeantNomUsage[12]']                                                          = dirigeant13.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant13.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant13.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant13.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant13.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant13.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['dirigeantPrenoms[12]']                                                      = prenoms.toString();
} else if (dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['dirigeantPrenoms[12]']                                                      = prenoms.toString();
}
if (dirigeant13.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant13.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['dirigeantDateNaissance[12]']          = date1;
}  else if (dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['dirigeantDateNaissance[12]']          = date2;
}
formFieldsPers1['dirigeantLieuNaissance[12]']                                              = dirigeant13.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																					(dirigeant13.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant13.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																					(dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																					(dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																					(dirigeant13.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? 
																					(dirigeant13.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant13.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : 
																					(dirigeant13.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
																					(dirigeant13.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers1['dirigeantNationalite[12]']                                               = dirigeant13.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant13.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
formFieldsPers1['dirigeant_siegeVoie[12]']                                                = (dirigeant13.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant13.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '') 
																					+ ' ' + (dirigeant13.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant13.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
																					+ ' ' + (dirigeant13.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant13.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
																					+ ' ' + dirigeant13.cadreAdresseDirigeant.dirigeantAdresseNomVoie
																					+ ' ' + (dirigeant13.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant13.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
																					+ ' ' + (dirigeant13.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant13.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers1['dirigeant_siegeCP[12]']                                                  = dirigeant13.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant13.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers1['dirigeant_siegeCommune[12]']                                             = dirigeant13.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant13.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant13.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant13.cadreAdresseDirigeant.dirigeantAdressePays);
if (Value('id').of(dirigeant13.personnaliteJuridique).eq('personneMorale')) {
formFieldsPers1['dirigeant_formeJuridique[12]']                                           = dirigeant13.civilitePersonneMorale.personneLieePMFormeJuridique;
formFieldsPers1['dirigeant_numeroImmatriculation[12]']                                    = dirigeant13.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant13.civilitePersonneMorale.personneLieePMNumeroIdentification : (dirigeant13.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee != null ? (dirigeant13.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee + ' ' + dirigeant13.civilitePersonneMorale.personneLieePMNomNonImmatriculee) : '');
formFieldsPers1['dirigeant_lieuImmatriculation[12]']                                      = dirigeant13.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant13.civilitePersonneMorale.personneLieePMLieuImmatriculation : (dirigeant13.civilitePersonneMorale.personneLieePMLieuImmatriculation + ' / ' + dirigeant13.civilitePersonneMorale.personneLieePMPaysNonImmatriculee);
}
}

// Cadre 9 - Dirigeant 8M0

var dirigeant14 = $m0SAS.dirigeant14.cadreIdentiteDirigeant;

if (dirigeant13.autreDirigeant) {
formFieldsPers1['dirigeantQualite[13]']                                                           = dirigeant14.personneLieeQualite;
formFieldsPers1['dirigeantNomNaissance[13]']                                                      = dirigeant14.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant14.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeant14.civilitePersonneMorale.personneLieePMDenomination) ;
formFieldsPers1['dirigeantNomUsage[13]']                                                          = dirigeant14.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant14.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant14.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant14.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant14.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant14.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['dirigeantPrenoms[13]']                                                      = prenoms.toString();
} else if (dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['dirigeantPrenoms[13]']                                                      = prenoms.toString();
}
if (dirigeant14.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant14.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['dirigeantDateNaissance[13]']          = date1;
}  else if (dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['dirigeantDateNaissance[13]']          = date2;
}
formFieldsPers1['dirigeantLieuNaissance[13]']                                              = dirigeant14.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																					(dirigeant14.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant14.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																					(dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																					(dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																					(dirigeant14.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? 
																					(dirigeant14.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant14.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : 
																					(dirigeant14.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
																					(dirigeant14.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers1['dirigeantNationalite[13]']                                               = dirigeant14.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant14.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
formFieldsPers1['dirigeant_siegeVoie[13]']                                                = (dirigeant14.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant14.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '') 
																					+ ' ' + (dirigeant14.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant14.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
																					+ ' ' + (dirigeant14.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant14.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
																					+ ' ' + dirigeant14.cadreAdresseDirigeant.dirigeantAdresseNomVoie
																					+ ' ' + (dirigeant14.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant14.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
																					+ ' ' + (dirigeant14.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant14.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers1['dirigeant_siegeCP[13]']                                                  = dirigeant14.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant14.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers1['dirigeant_siegeCommune[13]']                                             = dirigeant14.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant14.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant14.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant14.cadreAdresseDirigeant.dirigeantAdressePays);
if (Value('id').of(dirigeant14.personnaliteJuridique).eq('personneMorale')) {
formFieldsPers1['dirigeant_formeJuridique[13]']                                           = dirigeant14.civilitePersonneMorale.personneLieePMFormeJuridique;
formFieldsPers1['dirigeant_numeroImmatriculation[13]']                                    = dirigeant14.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant14.civilitePersonneMorale.personneLieePMNumeroIdentification : (dirigeant14.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee != null ? (dirigeant14.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee + ' ' + dirigeant14.civilitePersonneMorale.personneLieePMNomNonImmatriculee) : '');
formFieldsPers1['dirigeant_lieuImmatriculation[13]']                                      = dirigeant14.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant14.civilitePersonneMorale.personneLieePMLieuImmatriculation : (dirigeant14.civilitePersonneMorale.personneLieePMLieuImmatriculation + ' / ' + dirigeant14.civilitePersonneMorale.personneLieePMPaysNonImmatriculee);
}
}

var representants = [];

var cadreDirigeants = [
{
	'cadre': '13',
	'dirigeant' : null == $m0SAS.dirigeant1 ? null : $m0SAS.dirigeant1
},{
	'cadre': '14',
	'dirigeant' : null == $m0SAS.dirigeant2 ? null : $m0SAS.dirigeant2
},{
	'cadre': '15',
	'dirigeant' : null == $m0SAS.dirigeant3 ? null : $m0SAS.dirigeant3
},{
	'cadre': '16',
	'dirigeant' : null == $m0SAS.dirigeant4 ? null : $m0SAS.dirigeant4
},{
	'cadre': '17',
	'dirigeant' : null == $m0SAS.dirigeant5 ? null : $m0SAS.dirigeant5
},{
	'cadre': '18',
	'dirigeant' : null == $m0SAS.dirigeant6 ? null : $m0SAS.dirigeant6
},{
	'cadre': '2',
	'dirigeant' : null == $m0SAS.dirigeant7 ? null : $m0SAS.dirigeant7
},{
	'cadre': '3',
	'dirigeant' : null == $m0SAS.dirigeant8 ? null : $m0SAS.dirigeant8
},{
	'cadre': '4',
	'dirigeant' : null == $m0SAS.dirigeant9 ? null : $m0SAS.dirigeant9
},{
	'cadre': '5',
	'dirigeant' : null == $m0SAS.dirigeant10 ? null : $m0SAS.dirigeant10
},{
	'cadre': '6',
	'dirigeant' : null == $m0SAS.dirigeant11 ? null : $m0SAS.dirigeant11
},{
	'cadre': '7',
	'dirigeant' : null == $m0SAS.dirigeant12 ? null : $m0SAS.dirigeant12
},{
	'cadre': '8',
	'dirigeant' : null == $m0SAS.dirigeant13 ? null : $m0SAS.dirigeant13
},{
	'cadre': '9',
	'dirigeant' : null == $m0SAS.dirigeant14 ? null : $m0SAS.dirigeant14
}
];

for (var idx in cadreDirigeants) {
	var cadreDirigeant = cadreDirigeants[idx];
	var dirigeant = cadreDirigeant['dirigeant'];
	if (null != dirigeant.cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomNaissance) {
		representants.push(cadreDirigeant);	
	}
}

log.info("Number of representants : {}", representants.length);
log.info("Representants : {}", representants);


formFields['formalite_nombreIntercalairesM0Prime']                                      =  (representants.length > 12) ? "4" : ((societe.fusionGroup2.fusionAutreSociete2 or (representants.length > 8)) ? "3" : ((societe.fusionGroup1.fusionAutreSociete or societe.cadreAutreEtablissementUE1.declarationAutreEtablissementUE or (representants.length > 4)) ? "2" : ((dirigeant6.autreDirigeant or societe.entrepriseFusionScission or societe.entrepriseAutreEtablissementUE or (representants.length > 0)) ? "1" : "0")));


// Cadre 10 Représentant permanent 1

if (representants.length > 0) {
formFieldsPers1['representantPermanent_numeroCadre[0]']          = representants[0]['cadre'];
formFieldsPers1['representantPermanent_estPermanent[0]']         = true;
formFieldsPers1['representantPermanent_estAutre[0]']             = false;
formFieldsPers1['representantPermanent_nomNaissance[0]']         = representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomNaissance;
formFieldsPers1['representantPermanent_nomUsage[0]']             = representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage != null ? representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage : '';
if (representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom != null) {
var prenoms=[];
for ( i = 0; i < representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom[i]);}                            
formFieldsPers1['representantPermanent_prenom[0]']                                                      = prenoms.toString();
}
formFieldsPers1['representantPermanent_nationalite[0]']          = representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNationalite;
if (representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['representantPermanent_dateNaissance[0]']          = date1;
}
formFieldsPers1['representantPermanent_lieuNaissanceCommune[0]'] = representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune != null ? 
																	(representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune + ' ' +  "(" + '' + representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceDepartement.getId() +''+ ")") :
																	(representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissancePays);
formFieldsPers1['representantPermanent_adresseVoie[0]']          = (representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null ? representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie : '')
																	+ ' ' + (representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null ? representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie : '')
																	+ ' ' + (representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null ? representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie : '')
																	+ ' ' + representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNomVoie
																	+ ' ' + (representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null ? representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie : '')
																	+ ' ' + (representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null ? representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale : '');
formFieldsPers1['representantPermanent_adresseCP[0]']            = representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal != null ? representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal : '';
formFieldsPers1['representantPermanent_adresseCommune[0]']       = representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune != null ? representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune  : (representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseVille  + " / " + representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantPays );
}

// Cadre 11 Représentant permanent 2

if (representants.length > 1) {
formFieldsPers1['representantPermanent_numeroCadre[1]']          = representants[1]['cadre'];
formFieldsPers1['representantPermanent_estPermanent[1]']         = true;
formFieldsPers1['representantPermanent_estAutre[1]']             = false;
formFieldsPers1['representantPermanent_nomNaissance[1]']         = representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomNaissance;
formFieldsPers1['representantPermanent_nomUsage[1]']             = representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage != null ? representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage : '';
if (representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom != null) {
var prenoms=[];
for ( i = 0; i < representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom[i]);}                            
formFieldsPers1['representantPermanent_prenom[1]']                                                      = prenoms.toString();
}
formFieldsPers1['representantPermanent_nationalite[1]']          = representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNationalite;
if (representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['representantPermanent_dateNaissance[1]']          = date1;
}
formFieldsPers1['representantPermanent_lieuNaissanceCommune[1]'] = representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune != null ? 
																	(representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune + ' ' +  "(" + '' + representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceDepartement.getId() +''+ ")") :
																	(representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissancePays);
formFieldsPers1['representantPermanent_adresseVoie[1]']          = (representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null ? representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie : '')
																	+ ' ' + (representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null ? representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie : '')
																	+ ' ' + (representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null ? representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie : '')
																	+ ' ' + representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNomVoie
																	+ ' ' + (representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null ? representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie : '')
																	+ ' ' + (representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null ? representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale : '');
formFieldsPers1['representantPermanent_adresseCP[1]']            = representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal != null ? representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal : '';
formFieldsPers1['representantPermanent_adresseCommune[1]']       = representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune != null ? representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune  : (representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseVille  + " / " + representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantPays );
}

// Cadre 12 Représentant permanent 3

if (representants.length > 2) {
formFieldsPers1['representantPermanent_numeroCadre[2]']          = representants[2]['cadre'];
formFieldsPers1['representantPermanent_estPermanent[2]']         = true;
formFieldsPers1['representantPermanent_estAutre[2]']             = false;
formFieldsPers1['representantPermanent_nomNaissance[2]']         = representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomNaissance;
formFieldsPers1['representantPermanent_nomUsage[2]']             = representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage != null ? representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage : '';
if (representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom != null) {
var prenoms=[];
for ( i = 0; i < representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom[i]);}                            
formFieldsPers1['representantPermanent_prenom[2]']                                                      = prenoms.toString();
}
formFieldsPers1['representantPermanent_nationalite[2]']          = representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNationalite;
if (representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['representantPermanent_dateNaissance[2]']          = date1;
}
formFieldsPers1['representantPermanent_lieuNaissanceCommune[2]'] = representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune != null ? 
																	(representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune + ' ' +  "(" + '' + representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceDepartement.getId() +''+ ")") :
																	(representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissancePays);
formFieldsPers1['representantPermanent_adresseVoie[2]']          = (representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null ? representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie : '')
																	+ ' ' + (representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null ? representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie : '')
																	+ ' ' + (representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null ? representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie : '')
																	+ ' ' + representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNomVoie
																	+ ' ' + (representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null ? representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie : '')
																	+ ' ' + (representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null ? representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale : '');
formFieldsPers1['representantPermanent_adresseCP[2]']            = representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal != null ? representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal : '';
formFieldsPers1['representantPermanent_adresseCommune[2]']       = representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune != null ? representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune  : (representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseVille  + " / " + representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantPays );
}

// Cadre 13 Représentant permanent 4

if (representants.length > 3) {
formFieldsPers1['representantPermanent_numeroCadre[3]']          = representants[3]['cadre'];
formFieldsPers1['representantPermanent_estPermanent[3]']         = true;
formFieldsPers1['representantPermanent_estAutre[3]']             = false;
formFieldsPers1['representantPermanent_nomNaissance[3]']         = representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomNaissance;
formFieldsPers1['representantPermanent_nomUsage[3]']             = representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage != null ? representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage : '';
if (representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom != null) {
var prenoms=[];
for ( i = 0; i < representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom[i]);}                            
formFieldsPers1['representantPermanent_prenom[3]']                                                      = prenoms.toString();
}
formFieldsPers1['representantPermanent_nationalite[3]']          = representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNationalite;
if (representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['representantPermanent_dateNaissance[3]']          = date1;
}
formFieldsPers1['representantPermanent_lieuNaissanceCommune[3]'] = representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune != null ? 
																	(representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune + ' ' +  "(" + '' + representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceDepartement.getId() +''+ ")") :
																	(representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissancePays);
formFieldsPers1['representantPermanent_adresseVoie[3]']          = (representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null ? representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie : '')
																	+ ' ' + (representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null ? representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie : '')
																	+ ' ' + (representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null ? representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie : '')
																	+ ' ' + representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNomVoie
																	+ ' ' + (representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null ? representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie : '')
																	+ ' ' + (representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null ? representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale : '');
formFieldsPers1['representantPermanent_adresseCP[3]']            = representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal != null ? representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal : '';
formFieldsPers1['representantPermanent_adresseCommune[3]']       = representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune != null ? representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune  : (representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseVille  + " / " + representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantPays );
}

// Cadre 14 Fusion

var fusion1 = $m0SAS.cadre1SocieteGroup.cadre1Identite.fusionGroup1 ;

if (societe.entrepriseFusionScission) {
formFieldsPers1['entrepriseLiee_siren']                                                 = fusion1.entrepriseLieeSiren.split(' ').join('');
formFieldsPers1['entrepriseLiee_greffeImmatriculation']                                 = fusion1.entrepriseLieeGreffe;
formFieldsPers1['entrepriseLiee_entreprisePM_formeJuridique']                           = fusion1.entreperiseLieeEntreprisePMFormeJuridique;
formFieldsPers1['entrepriseLiee_entreprisePM_denomination']                             = fusion1.entrepriseLieeEntreprisePMDenomination;
formFieldsPers1['entrepriseLiee_adresse_voie_Prime']                                         = (fusion1.adresseFusionScission.numeroAdresseFusion != null ? fusion1.adresseFusionScission.numeroAdresseFusion : '')
																							 + ' ' + (fusion1.adresseFusionScission.indiceAdresseFusion != null ? fusion1.adresseFusionScission.indiceAdresseFusion : '')
																							 + ' ' + (fusion1.adresseFusionScission.typeAdresseFusion != null ? fusion1.adresseFusionScission.typeAdresseFusion : '')
																							 + ' ' + fusion1.adresseFusionScission.voieAdresseFusion
																							 + ' ' + (fusion1.adresseFusionScission.complementAdresseFusion != null ? fusion1.adresseFusionScission.complementAdresseFusion : '')
																							 + ' ' + (fusion1.adresseFusionScission.distributionSpecialeAdresseFusion != null ? fusion1.adresseFusionScission.distributionSpecialeAdresseFusion : '');
formFieldsPers1['entrepriseLiee_adresse_commune_Prime']                                      = fusion1.adresseFusionScission.codePostalAdresseFusion + ' ' + fusion1.adresseFusionScission.communeAdresseFusion;
}

// Cadre 15 Autre établissement UE

var autreEtablissement1 = $m0SAS.cadre1SocieteGroup.cadre1Identite.cadreAutreEtablissementUE1 ;

if (societe.entrepriseAutreEtablissementUE) {
formFieldsPers1['autreEtablissementUE_numeroImmatriculation']                  = autreEtablissement1.numeroImmatriculationAutreEtablissementUE;
formFieldsPers1['autreEtablissementUE_lieuImmatriculation']                    = autreEtablissement1.lieuImmatriculationAutreEtablissementUE + ' / ' + autreEtablissement1.paysImmatriculationAutreEtablissementUE;
formFieldsPers1['autreEtablissementUE_activites']                              = autreEtablissement1.activiteAutreEtablissementUE;
formFieldsPers1['autreEtablissementUE_adresse_voie_Prime']                     = (autreEtablissement1.adresseAutreEtablissementUE.rueNumeroAdresseAutreEtablissementUE != null ? autreEtablissement1.adresseAutreEtablissementUE.rueNumeroAdresseAutreEtablissementUE : '') 
																				+ ' ' + (autreEtablissement1.adresseAutreEtablissementUE.indiceVoieAdresseAutreEtablissementUE != null ? autreEtablissement1.adresseAutreEtablissementUE.indiceVoieAdresseAutreEtablissementUE : '')
																				+ ' ' + (autreEtablissement1.adresseAutreEtablissementUE.typeVoieAdresseAutreEtablissementUE != null ? autreEtablissement1.adresseAutreEtablissementUE.typeVoieAdresseAutreEtablissementUE : '')
																				+ ' ' + autreEtablissement1.adresseAutreEtablissementUE.nomVoieAdresseAutreEtablissementUE
																				+ ' ' + (autreEtablissement1.adresseAutreEtablissementUE.rueComplementAdresseAutreEtablissementUE != null ? autreEtablissement1.adresseAutreEtablissementUE.rueComplementAdresseAutreEtablissementUE : '')
																				+ ' ' + (autreEtablissement1.adresseAutreEtablissementUE.distributionSpecialeAdresseAutreEtablissementUE != null ? autreEtablissement1.adresseAutreEtablissementUE.distributionSpecialeAdresseAutreEtablissementUE : '');
formFieldsPers1['autreEtablissementUE_adresse_commune_Prime'] 				   = (autreEtablissement1.adresseAutreEtablissementUE.codePostalAdresseAutreEtablissementUE != null ? autreEtablissement1.adresseAutreEtablissementUE.codePostalAdresseAutreEtablissementUE : '')
																				+ ' ' + autreEtablissement1.adresseAutreEtablissementUE.communeAdresseAutreEtablissementUECommune + ' / ' + autreEtablissement1.adresseAutreEtablissementUE.paysAdresseAutreEtablissementUE;	
}

// M0 prime 2

var formFieldsPers2 = {}

// Cadre 1

formFieldsPers2['numeroIntercalaireM0Prime']                                            = "02" 
formFieldsPers2['entreprise_denomination']                                              = societe.entrepriseDenomination;
formFieldsPers2['entreprise_formeJuridique']                                            = societe.entrepriseFormeJuridique;

// Cadre 10 Représentant permanent 5

if (representants.length > 4) {
formFieldsPers2['representantPermanent_numeroCadre[0]']          = representants[4]['cadre'];
formFieldsPers2['representantPermanent_estPermanent[0]']         = true;
formFieldsPers2['representantPermanent_estAutre[0]']             = false;
formFieldsPers2['representantPermanent_nomNaissance[0]']         = representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomNaissance;
formFieldsPers2['representantPermanent_nomUsage[0]']             = representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage != null ? representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage : '';
if (representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom != null) {
var prenoms=[];
for ( i = 0; i < representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom[i]);}                            
formFieldsPers2['representantPermanent_prenom[0]']                                                      = prenoms.toString();
}
formFieldsPers2['representantPermanent_nationalite[0]']          = representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNationalite;
if (representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['representantPermanent_dateNaissance[0]']          = date1;
}
formFieldsPers2['representantPermanent_lieuNaissanceCommune[0]'] = representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune != null ? 
																	(representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune + ' ' +  "(" + '' + representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceDepartement.getId() +''+ ")") :
																	(representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissancePays);
formFieldsPers2['representantPermanent_adresseVoie[0]']          = (representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null ? representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie : '')
																	+ ' ' + (representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null ? representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie : '')
																	+ ' ' + (representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null ? representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie : '')
																	+ ' ' + representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNomVoie
																	+ ' ' + (representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null ? representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie : '')
																	+ ' ' + (representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null ? representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale : '');
formFieldsPers2['representantPermanent_adresseCP[0]']            = representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal != null ? representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal : '';
formFieldsPers2['representantPermanent_adresseCommune[0]']       = representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune != null ? representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune  : (representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseVille  + " / " + representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantPays );
}

// Cadre 11 Représentant permanent 6

if (representants.length > 5) {
formFieldsPers2['representantPermanent_numeroCadre[1]']          = representants[5]['cadre'];
formFieldsPers2['representantPermanent_estPermanent[1]']         = true;
formFieldsPers2['representantPermanent_estAutre[1]']             = false;
formFieldsPers2['representantPermanent_nomNaissance[1]']         = representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomNaissance;
formFieldsPers2['representantPermanent_nomUsage[1]']             = representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage != null ? representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage : '';
if (representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom != null) {
var prenoms=[];
for ( i = 0; i < representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom[i]);}                            
formFieldsPers2['representantPermanent_prenom[1]']                                                      = prenoms.toString();
}
formFieldsPers2['representantPermanent_nationalite[1]']          = representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNationalite;
if (representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['representantPermanent_dateNaissance[1]']          = date1;
}
formFieldsPers2['representantPermanent_lieuNaissanceCommune[1]'] = representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune != null ? 
																	(representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune + ' ' +  "(" + '' + representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceDepartement.getId() +''+ ")") :
																	(representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissancePays);
formFieldsPers2['representantPermanent_adresseVoie[1]']          = (representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null ? representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie : '')
																	+ ' ' + (representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null ? representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie : '')
																	+ ' ' + (representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null ? representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie : '')
																	+ ' ' + representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNomVoie
																	+ ' ' + (representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null ? representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie : '')
																	+ ' ' + (representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null ? representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale : '');
formFieldsPers2['representantPermanent_adresseCP[1]']            = representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal != null ? representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal : '';
formFieldsPers2['representantPermanent_adresseCommune[1]']       = representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune != null ? representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune  : (representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseVille  + " / " + representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantPays );
}

// Cadre 12 Représentant permanent 7

if (representants.length > 6) {
formFieldsPers2['representantPermanent_numeroCadre[2]']          = representants[6]['cadre'];
formFieldsPers2['representantPermanent_estPermanent[2]']         = true;
formFieldsPers2['representantPermanent_estAutre[2]']             = false;
formFieldsPers2['representantPermanent_nomNaissance[2]']         = representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomNaissance;
formFieldsPers2['representantPermanent_nomUsage[2]']             = representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage != null ? representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage : '';
if (representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom != null) {
var prenoms=[];
for ( i = 0; i < representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom[i]);}                            
formFieldsPers2['representantPermanent_prenom[2]']                                                      = prenoms.toString();
}
formFieldsPers2['representantPermanent_nationalite[2]']          = representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNationalite;
if (representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['representantPermanent_dateNaissance[2]']          = date1;
}
formFieldsPers2['representantPermanent_lieuNaissanceCommune[2]'] = representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune != null ? 
																	(representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune + ' ' +  "(" + '' + representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceDepartement.getId() +''+ ")") :
																	(representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissancePays);
formFieldsPers2['representantPermanent_adresseVoie[2]']          = (representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null ? representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie : '')
																	+ ' ' + (representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null ? representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie : '')
																	+ ' ' + (representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null ? representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie : '')
																	+ ' ' + representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNomVoie
																	+ ' ' + (representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null ? representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie : '')
																	+ ' ' + (representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null ? representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale : '');
formFieldsPers2['representantPermanent_adresseCP[2]']            = representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal != null ? representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal : '';
formFieldsPers2['representantPermanent_adresseCommune[2]']       = representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune != null ? representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune  : (representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseVille  + " / " + representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantPays );
}

// Cadre 13 Représentant permanent 8

if (representants.length > 7) {
formFieldsPers2['representantPermanent_numeroCadre[3]']          = representants[7]['cadre'];
formFieldsPers2['representantPermanent_estPermanent[3]']         = true;
formFieldsPers2['representantPermanent_estAutre[3]']             = false;
formFieldsPers2['representantPermanent_nomNaissance[3]']         = representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomNaissance;
formFieldsPers2['representantPermanent_nomUsage[3]']             = representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage != null ? representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage : '';
if (representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom != null) {
var prenoms=[];
for ( i = 0; i < representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom[i]);}                            
formFieldsPers2['representantPermanent_prenom[3]']                                                      = prenoms.toString();
}
formFieldsPers2['representantPermanent_nationalite[3]']          = representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNationalite;
if (representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['representantPermanent_dateNaissance[3]']          = date1;
}
formFieldsPers2['representantPermanent_lieuNaissanceCommune[3]'] = representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune != null ? 
																	(representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune + ' ' +  "(" + '' + representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceDepartement.getId() +''+ ")") :
																	(representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissancePays);
formFieldsPers2['representantPermanent_adresseVoie[3]']          = (representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null ? representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie : '')
																	+ ' ' + (representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null ? representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie : '')
																	+ ' ' + (representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null ? representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie : '')
																	+ ' ' + representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNomVoie
																	+ ' ' + (representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null ? representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie : '')
																	+ ' ' + (representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null ? representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale : '');
formFieldsPers2['representantPermanent_adresseCP[3]']            = representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal != null ? representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal : '';
formFieldsPers2['representantPermanent_adresseCommune[3]']       = representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune != null ? representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune  : (representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseVille  + " / " + representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantPays );
}

// Cadre 14 Fusion

var fusion2 = $m0SAS.cadre1SocieteGroup.cadre1Identite.fusionGroup2 ;

if (fusion1.fusionAutreSociete) {
formFieldsPers2['entrepriseLiee_siren']                                                 = fusion2.entrepriseLieeSiren.split(' ').join('');
formFieldsPers2['entrepriseLiee_greffeImmatriculation']                                 = fusion2.entrepriseLieeGreffe;
formFieldsPers2['entrepriseLiee_entreprisePM_formeJuridique']                           = fusion2.entreperiseLieeEntreprisePMFormeJuridique;
formFieldsPers2['entrepriseLiee_entreprisePM_denomination']                             = fusion2.entrepriseLieeEntreprisePMDenomination;
formFieldsPers2['entrepriseLiee_adresse_voie_Prime']                                         = (fusion2.adresseFusionScission.numeroAdresseFusion != null ? fusion2.adresseFusionScission.numeroAdresseFusion : '')
																							 + ' ' + (fusion2.adresseFusionScission.indiceAdresseFusion != null ? fusion2.adresseFusionScission.indiceAdresseFusion : '')
																							 + ' ' + (fusion2.adresseFusionScission.typeAdresseFusion != null ? fusion2.adresseFusionScission.typeAdresseFusion : '')
																							 + ' ' + fusion2.adresseFusionScission.voieAdresseFusion
																							 + ' ' + (fusion2.adresseFusionScission.complementAdresseFusion != null ? fusion2.adresseFusionScission.complementAdresseFusion : '')
																							 + ' ' + (fusion2.adresseFusionScission.distributionSpecialeAdresseFusion != null ? fusion2.adresseFusionScission.distributionSpecialeAdresseFusion : '');
formFieldsPers2['entrepriseLiee_adresse_commune_Prime']                                      = fusion2.adresseFusionScission.codePostalAdresseFusion + ' ' + fusion2.adresseFusionScission.communeAdresseFusion;
}

// Cadre 15 Autre établissement UE

var autreEtablissement2 = $m0SAS.cadre1SocieteGroup.cadre1Identite.cadreAutreEtablissementUE2 ;

if (autreEtablissement1.declarationAutreEtablissementUE) {
formFieldsPers2['autreEtablissementUE_numeroImmatriculation']                  = autreEtablissement2.numeroImmatriculationAutreEtablissementUE;
formFieldsPers2['autreEtablissementUE_lieuImmatriculation']                    = autreEtablissement2.lieuImmatriculationAutreEtablissementUE + ' / ' + autreEtablissement2.paysImmatriculationAutreEtablissementUE;
formFieldsPers2['autreEtablissementUE_activites']                              = autreEtablissement2.activiteAutreEtablissementUE;
formFieldsPers2['autreEtablissementUE_adresse_voie_Prime']                     = (autreEtablissement2.adresseAutreEtablissementUE.rueNumeroAdresseAutreEtablissementUE != null ? autreEtablissement2.adresseAutreEtablissementUE.rueNumeroAdresseAutreEtablissementUE : '') 
																				+ ' ' + (autreEtablissement2.adresseAutreEtablissementUE.indiceVoieAdresseAutreEtablissementUE != null ? autreEtablissement2.adresseAutreEtablissementUE.indiceVoieAdresseAutreEtablissementUE : '')
																				+ ' ' + (autreEtablissement2.adresseAutreEtablissementUE.typeVoieAdresseAutreEtablissementUE != null ? autreEtablissement2.adresseAutreEtablissementUE.typeVoieAdresseAutreEtablissementUE : '')
																				+ ' ' + autreEtablissement2.adresseAutreEtablissementUE.nomVoieAdresseAutreEtablissementUE
																				+ ' ' + (autreEtablissement2.adresseAutreEtablissementUE.rueComplementAdresseAutreEtablissementUE != null ? autreEtablissement2.adresseAutreEtablissementUE.rueComplementAdresseAutreEtablissementUE : '')
																				+ ' ' + (autreEtablissement2.adresseAutreEtablissementUE.distributionSpecialeAdresseAutreEtablissementUE != null ? autreEtablissement2.adresseAutreEtablissementUE.distributionSpecialeAdresseAutreEtablissementUE : '');
formFieldsPers2['autreEtablissementUE_adresse_commune_Prime'] 				   = (autreEtablissement2.adresseAutreEtablissementUE.codePostalAdresseAutreEtablissementUE != null ? autreEtablissement2.adresseAutreEtablissementUE.codePostalAdresseAutreEtablissementUE : '')
																				+ ' ' + autreEtablissement2.adresseAutreEtablissementUE.communeAdresseAutreEtablissementUECommune + ' / ' + autreEtablissement2.adresseAutreEtablissementUE.paysAdresseAutreEtablissementUE;	
}

// M0 prime 3

var formFieldsPers3 = {}

// Cadre 1

formFieldsPers3['numeroIntercalaireM0Prime']                                            = "03" 
formFieldsPers3['entreprise_denomination']                                              = societe.entrepriseDenomination;
formFieldsPers3['entreprise_formeJuridique']                                            = societe.entrepriseFormeJuridique;

// Cadre 10 Représentant permanent 9

if (representants.length > 8) {
formFieldsPers3['representantPermanent_numeroCadre[0]']          = representants[8]['cadre'];
formFieldsPers3['representantPermanent_estPermanent[0]']         = true;
formFieldsPers3['representantPermanent_estAutre[0]']             = false;
formFieldsPers3['representantPermanent_nomNaissance[0]']         = representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomNaissance;
formFieldsPers3['representantPermanent_nomUsage[0]']             = representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage != null ? representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage : '';
if (representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom != null) {
var prenoms=[];
for ( i = 0; i < representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom[i]);}                            
formFieldsPers3['representantPermanent_prenom[0]']                                                      = prenoms.toString();
}
formFieldsPers3['representantPermanent_nationalite[0]']          = representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNationalite;
if (representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers3['representantPermanent_dateNaissance[0]']          = date1;
}
formFieldsPers3['representantPermanent_lieuNaissanceCommune[0]'] = representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune != null ? 
																	(representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune + ' ' +  "(" + '' + representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceDepartement.getId() +''+ ")") :
																	(representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissancePays);
formFieldsPers3['representantPermanent_adresseVoie[0]']          = (representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null ? representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie : '')
																	+ ' ' + (representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null ? representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie : '')
																	+ ' ' + (representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null ? representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie : '')
																	+ ' ' + representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNomVoie
																	+ ' ' + (representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null ? representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie : '')
																	+ ' ' + (representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null ? representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale : '');
formFieldsPers3['representantPermanent_adresseCP[0]']            = representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal != null ? representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal : '';
formFieldsPers3['representantPermanent_adresseCommune[0]']       = representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune != null ? representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune  : (representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseVille  + " / " + representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantPays );
}

// Cadre 11 Représentant permanent 10

if (representants.length > 9) {
formFieldsPers3['representantPermanent_numeroCadre[1]']          = representants[9]['cadre'];
formFieldsPers3['representantPermanent_estPermanent[1]']         = true;
formFieldsPers3['representantPermanent_estAutre[1]']             = false;
formFieldsPers3['representantPermanent_nomNaissance[1]']         = representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomNaissance;
formFieldsPers3['representantPermanent_nomUsage[1]']             = representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage != null ? representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage : '';
if (representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom != null) {
var prenoms=[];
for ( i = 0; i < representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom[i]);}                            
formFieldsPers3['representantPermanent_prenom[1]']                                                      = prenoms.toString();
}
formFieldsPers3['representantPermanent_nationalite[1]']          = representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNationalite;
if (representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers3['representantPermanent_dateNaissance[1]']          = date1;
}
formFieldsPers3['representantPermanent_lieuNaissanceCommune[1]'] = representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune != null ? 
																	(representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune + ' ' +  "(" + '' + representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceDepartement.getId() +''+ ")") :
																	(representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissancePays);
formFieldsPers3['representantPermanent_adresseVoie[1]']          = (representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null ? representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie : '')
																	+ ' ' + (representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null ? representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie : '')
																	+ ' ' + (representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null ? representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie : '')
																	+ ' ' + representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNomVoie
																	+ ' ' + (representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null ? representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie : '')
																	+ ' ' + (representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null ? representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale : '');
formFieldsPers3['representantPermanent_adresseCP[1]']            = representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal != null ? representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal : '';
formFieldsPers3['representantPermanent_adresseCommune[1]']       = representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune != null ? representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune  : (representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseVille  + " / " + representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantPays );
}

// Cadre 12 Représentant permanent 11

if (representants.length > 10) {
formFieldsPers3['representantPermanent_numeroCadre[2]']          = representants[10]['cadre'];
formFieldsPers3['representantPermanent_estPermanent[2]']         = true;
formFieldsPers3['representantPermanent_estAutre[2]']             = false;
formFieldsPers3['representantPermanent_nomNaissance[2]']         = representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomNaissance;
formFieldsPers3['representantPermanent_nomUsage[2]']             = representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage != null ? representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage : '';
if (representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom != null) {
var prenoms=[];
for ( i = 0; i < representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom[i]);}                            
formFieldsPers3['representantPermanent_prenom[2]']                                                      = prenoms.toString();
}
formFieldsPers3['representantPermanent_nationalite[2]']          = representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNationalite;
if (representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers3['representantPermanent_dateNaissance[2]']          = date1;
}
formFieldsPers3['representantPermanent_lieuNaissanceCommune[2]'] = representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune != null ? 
																	(representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune + ' ' +  "(" + '' + representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceDepartement.getId() +''+ ")") :
																	(representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissancePays);
formFieldsPers3['representantPermanent_adresseVoie[2]']          = (representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null ? representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie : '')
																	+ ' ' + (representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null ? representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie : '')
																	+ ' ' + (representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null ? representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie : '')
																	+ ' ' + representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNomVoie
																	+ ' ' + (representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null ? representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie : '')
																	+ ' ' + (representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null ? representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale : '');
formFieldsPers3['representantPermanent_adresseCP[2]']            = representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal != null ? representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal : '';
formFieldsPers3['representantPermanent_adresseCommune[2]']       = representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune != null ? representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune  : (representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseVille  + " / " + representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantPays );
}

// Cadre 13 Représentant permanent 12

if (representants.length > 11) {
formFieldsPers3['representantPermanent_numeroCadre[3]']          = representants[11]['cadre'];
formFieldsPers3['representantPermanent_estPermanent[3]']         = true;
formFieldsPers3['representantPermanent_estAutre[3]']             = false;
formFieldsPers3['representantPermanent_nomNaissance[3]']         = representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomNaissance;
formFieldsPers3['representantPermanent_nomUsage[3]']             = representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage != null ? representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage : '';
if (representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom != null) {
var prenoms=[];
for ( i = 0; i < representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom[i]);}                            
formFieldsPers3['representantPermanent_prenom[3]']                                                      = prenoms.toString();
}
formFieldsPers3['representantPermanent_nationalite[3]']          = representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNationalite;
if (representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers3['representantPermanent_dateNaissance[3]']          = date1;
}
formFieldsPers3['representantPermanent_lieuNaissanceCommune[3]'] = representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune != null ? 
																	(representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune + ' ' +  "(" + '' + representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceDepartement.getId() +''+ ")") :
																	(representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissancePays);
formFieldsPers3['representantPermanent_adresseVoie[3]']          = (representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null ? representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie : '')
																	+ ' ' + (representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null ? representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie : '')
																	+ ' ' + (representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null ? representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie : '')
																	+ ' ' + representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNomVoie
																	+ ' ' + (representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null ? representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie : '')
																	+ ' ' + (representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null ? representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale : '');
formFieldsPers3['representantPermanent_adresseCP[3]']            = representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal != null ? representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal : '';
formFieldsPers3['representantPermanent_adresseCommune[3]']       = representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune != null ? representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune  : (representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseVille  + " / " + representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantPays );
}

// Cadre 14 Fusion

var fusion3 = $m0SAS.cadre1SocieteGroup.cadre1Identite.fusionGroup3 ;

if (fusion2.fusionAutreSociete2) {
formFieldsPers3['entrepriseLiee_siren']                                                 = fusion3.entrepriseLieeSiren.split(' ').join('');
formFieldsPers3['entrepriseLiee_greffeImmatriculation']                                 = fusion3.entrepriseLieeGreffe;
formFieldsPers3['entrepriseLiee_entreprisePM_formeJuridique']                           = fusion3.entreperiseLieeEntreprisePMFormeJuridique;
formFieldsPers3['entrepriseLiee_entreprisePM_denomination']                             = fusion3.entrepriseLieeEntreprisePMDenomination;
formFieldsPers3['entrepriseLiee_adresse_voie_Prime']                                         = (fusion3.adresseFusionScission.numeroAdresseFusion != null ? fusion3.adresseFusionScission.numeroAdresseFusion : '')
																							 + ' ' + (fusion3.adresseFusionScission.indiceAdresseFusion != null ? fusion3.adresseFusionScission.indiceAdresseFusion : '')
																							 + ' ' + (fusion3.adresseFusionScission.typeAdresseFusion != null ? fusion3.adresseFusionScission.typeAdresseFusion : '')
																							 + ' ' + fusion3.adresseFusionScission.voieAdresseFusion
																							 + ' ' + (fusion3.adresseFusionScission.complementAdresseFusion != null ? fusion3.adresseFusionScission.complementAdresseFusion : '')
																							 + ' ' + (fusion3.adresseFusionScission.distributionSpecialeAdresseFusion != null ? fusion3.adresseFusionScission.distributionSpecialeAdresseFusion : '');
formFieldsPers3['entrepriseLiee_adresse_commune_Prime']                                      = fusion3.adresseFusionScission.codePostalAdresseFusion + ' ' + fusion3.adresseFusionScission.communeAdresseFusion;
}

// M0 prime 4

var formFieldsPers4 = {}

// Cadre 1

formFieldsPers4['numeroIntercalaireM0Prime']                                            = "04" 
formFieldsPers4['entreprise_denomination']                                              = societe.entrepriseDenomination;
formFieldsPers4['entreprise_formeJuridique']                                            = societe.entrepriseFormeJuridique;

// Cadre 10 Représentant permanent 13

if (representants.length > 12) {
formFieldsPers4['representantPermanent_numeroCadre[0]']          = representants[12]['cadre'];
formFieldsPers4['representantPermanent_estPermanent[0]']         = true;
formFieldsPers4['representantPermanent_estAutre[0]']             = false;
formFieldsPers4['representantPermanent_nomNaissance[0]']         = representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomNaissance;
formFieldsPers4['representantPermanent_nomUsage[0]']             = representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage != null ? representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage : '';
if (representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom != null) {
var prenoms=[];
for ( i = 0; i < representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom[i]);}                            
formFieldsPers4['representantPermanent_prenom[0]']                                                      = prenoms.toString();
}
formFieldsPers4['representantPermanent_nationalite[0]']          = representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNationalite;
if (representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers4['representantPermanent_dateNaissance[0]']          = date1;
}
formFieldsPers4['representantPermanent_lieuNaissanceCommune[0]'] = representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune != null ? 
																	(representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune + ' ' +  "(" + '' + representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceDepartement.getId() +''+ ")") :
																	(representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissancePays);
formFieldsPers4['representantPermanent_adresseVoie[0]']          = (representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null ? representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie : '')
																	+ ' ' + (representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null ? representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie : '')
																	+ ' ' + (representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null ? representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie : '')
																	+ ' ' + representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNomVoie
																	+ ' ' + (representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null ? representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie : '')
																	+ ' ' + (representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null ? representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale : '');
formFieldsPers4['representantPermanent_adresseCP[0]']            = representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal != null ? representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal : '';
formFieldsPers4['representantPermanent_adresseCommune[0]']       = representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune != null ? representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune  : (representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseVille  + " / " + representants[12]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantPays );
}

// Cadre 11 Représentant permanent 2

if (representants.length > 13) {
formFieldsPers4['representantPermanent_numeroCadre[1]']          = representants[13]['cadre'];
formFieldsPers4['representantPermanent_estPermanent[1]']         = true;
formFieldsPers4['representantPermanent_estAutre[1]']             = false;
formFieldsPers4['representantPermanent_nomNaissance[1]']         = representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomNaissance;
formFieldsPers4['representantPermanent_nomUsage[1]']             = representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage != null ? representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage : '';
if (representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom != null) {
var prenoms=[];
for ( i = 0; i < representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom[i]);}                            
formFieldsPers4['representantPermanent_prenom[1]']                                                      = prenoms.toString();
}
formFieldsPers4['representantPermanent_nationalite[1]']          = representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNationalite;
if (representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers4['representantPermanent_dateNaissance[1]']          = date1;
}
formFieldsPers4['representantPermanent_lieuNaissanceCommune[1]'] = representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune != null ? 
																	(representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune + ' ' +  "(" + '' + representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceDepartement.getId() +''+ ")") :
																	(representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissancePays);
formFieldsPers4['representantPermanent_adresseVoie[1]']          = (representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null ? representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie : '')
																	+ ' ' + (representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null ? representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie : '')
																	+ ' ' + (representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null ? representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie : '')
																	+ ' ' + representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNomVoie
																	+ ' ' + (representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null ? representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie : '')
																	+ ' ' + (representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null ? representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale : '');
formFieldsPers4['representantPermanent_adresseCP[1]']            = representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal != null ? representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal : '';
formFieldsPers4['representantPermanent_adresseCommune[1]']       = representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune != null ? representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune  : (representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseVille  + " / " + representants[13]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantPays );
}

// TNS Dirigeant 1 

//cadre 1
var formFieldsPers5 = {}

formFieldsPers5['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers5['formulaireDependanceM0SNC_TNS']                                    = true;
formFieldsPers5['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers5['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers5['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers5['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers5['formulaireDependanceTNS_TNS']                                      = false;
   
//cadre 2  
                                                                                                   
formFieldsPers5['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 
var socialDirigeant1 = $m0SAS.dirigeant1.cadreIdentiteDirigeant.cadre7DeclarationSociale;

formFieldsPers5['personneLieePPNomNaissance_TNS']                                                 = dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeant1.civilitePersonneMorale.personneLieePMDenomination) ;
formFieldsPers5['personneLieePPNomUsage_TNS']                                                     = dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers5['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers5['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers5['personneLieeQualite_TNS']                                          = dirigeant1.personneLieeQualite;

var nirDeclarant = socialDirigeant1.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers5['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers5['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant1.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS != null ? socialDirigeant1.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
formFieldsPers5['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant1.voletSocialActiviteExerceeAnterieurementLibelleTNS !=null ? socialDirigeant1.voletSocialActiviteExerceeAnterieurementLibelleTNS : '';
formFieldsPers5['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant1.voletSocialActiviteExerceeAnterieurementDepartementTNS != null ? socialDirigeant1.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId() : '';
formFieldsPers5['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant1.voletSocialActiviteExerceeAnterieurementCommuneTNS != null ? socialDirigeant1.voletSocialActiviteExerceeAnterieurementCommuneTNS : '';
if(socialDirigeant1.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant1.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers5['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
formFieldsPers5['voletSocialActiviteSimultaneeOUI_TNS']                             = socialDirigeant1.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers5['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant1.voletSocialActiviteSimultaneeOUINON ? false : true;
formFieldsPers5['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers5['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers5['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers5['voletSocialActiviteSimultaneeAutre_TNS']                           = Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers5['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant1.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
formFieldsPers5['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant1.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers5['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant1.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers5['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant1.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers5['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant1.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

//cadre 5

formFieldsPers5['renseignementsComplementairesObservations_TNS']                    = '';
                                                                                 
//Cadre 6  

formFieldsPers5['renseignementsComplementairesLeDeclarant_TNS']                     = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentantLegal') ? true : false;
formFieldsPers5['renseignementsComplementairesLeMandataire_TNS']                    = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers5['formaliteSignataireNomPrenomDenomination_TNS']						= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers5['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + signataire.adresseMandataire.nomVoieMandataire
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');
formFieldsPers5['formaliteSignataireAdresseComplement_TNS']                         = signataire.adresseMandataire.codePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
formFieldsPers5['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
formFieldsPers5['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers5['formaliteSignatureDate_TNS'] = date;
}

// TNS Dirigeant 2 

//cadre 1
var formFieldsPers6 = {}

formFieldsPers6['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers6['formulaireDependanceM0SNC_TNS']                                    = true;
formFieldsPers6['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers6['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers6['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers6['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers6['formulaireDependanceTNS_TNS']                                      = false;
   
//cadre 2  
                                                                                                   
formFieldsPers6['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 
var socialDirigeant2 = $m0SAS.dirigeant2.cadreIdentiteDirigeant.cadre7DeclarationSociale;

formFieldsPers6['personneLieePPNomNaissance_TNS']                                                 = dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeant2.civilitePersonneMorale.personneLieePMDenomination) ;
formFieldsPers6['personneLieePPNomUsage_TNS']                                                     = dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers6['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers6['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers6['personneLieeQualite_TNS']                                          = dirigeant2.personneLieeQualite;

var nirDeclarant = socialDirigeant2.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers6['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers6['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers6['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers6['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers6['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers6['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers6['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers6['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant2.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS != null ? socialDirigeant2.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
formFieldsPers6['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant2.voletSocialActiviteExerceeAnterieurementLibelleTNS !=null ? socialDirigeant2.voletSocialActiviteExerceeAnterieurementLibelleTNS : '';
formFieldsPers6['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant2.voletSocialActiviteExerceeAnterieurementDepartementTNS != null ? socialDirigeant2.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId() : '';
formFieldsPers6['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant2.voletSocialActiviteExerceeAnterieurementCommuneTNS != null ? socialDirigeant2.voletSocialActiviteExerceeAnterieurementCommuneTNS : '';
if(socialDirigeant2.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant2.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers6['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
formFieldsPers6['voletSocialActiviteSimultaneeOUI_TNS']                             = socialDirigeant2.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers6['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant2.voletSocialActiviteSimultaneeOUINON ? false : true;
formFieldsPers6['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers6['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers6['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers6['voletSocialActiviteSimultaneeAutre_TNS']                           = Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers6['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant2.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
formFieldsPers6['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant2.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers6['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant2.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers6['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant2.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers6['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant2.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

//cadre 5

formFieldsPers6['renseignementsComplementairesObservations_TNS']                    = '';
                                                                                 
//Cadre 6  

formFieldsPers6['renseignementsComplementairesLeDeclarant_TNS']                     = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentantLegal') ? true : false;
formFieldsPers6['renseignementsComplementairesLeMandataire_TNS']                    = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers6['formaliteSignataireNomPrenomDenomination_TNS']						= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers6['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + signataire.adresseMandataire.nomVoieMandataire
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');
formFieldsPers6['formaliteSignataireAdresseComplement_TNS']                         = signataire.adresseMandataire.codePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
formFieldsPers6['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
formFieldsPers6['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers6['formaliteSignatureDate_TNS'] = date;
}

// TNS Dirigeant 3 

//cadre 1
var formFieldsPers7 = {}

formFieldsPers7['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers7['formulaireDependanceM0SNC_TNS']                                    = true;
formFieldsPers7['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers7['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers7['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers7['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers7['formulaireDependanceTNS_TNS']                                      = false;
   
//cadre 2  
                                                                                                   
formFieldsPers7['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 
var socialDirigeant3 = $m0SAS.dirigeant3.cadreIdentiteDirigeant.cadre7DeclarationSociale;

formFieldsPers7['personneLieePPNomNaissance_TNS']                                                 = dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeant3.civilitePersonneMorale.personneLieePMDenomination) ;
formFieldsPers7['personneLieePPNomUsage_TNS']                                                     = dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers7['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers7['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers7['personneLieeQualite_TNS']                                          = dirigeant3.personneLieeQualite;

var nirDeclarant = socialDirigeant3.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers7['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers7['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers7['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers7['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers7['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers7['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers7['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers7['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant3.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS != null ? socialDirigeant3.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
formFieldsPers7['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant3.voletSocialActiviteExerceeAnterieurementLibelleTNS !=null ? socialDirigeant3.voletSocialActiviteExerceeAnterieurementLibelleTNS : '';
formFieldsPers7['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant3.voletSocialActiviteExerceeAnterieurementDepartementTNS != null ? socialDirigeant3.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId() : '';
formFieldsPers7['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant3.voletSocialActiviteExerceeAnterieurementCommuneTNS != null ? socialDirigeant3.voletSocialActiviteExerceeAnterieurementCommuneTNS : '';
if(socialDirigeant3.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant3.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers7['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
formFieldsPers7['voletSocialActiviteSimultaneeOUI_TNS']                             = socialDirigeant3.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers7['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant3.voletSocialActiviteSimultaneeOUINON ? false : true;
formFieldsPers7['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers7['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers7['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers7['voletSocialActiviteSimultaneeAutre_TNS']                           = Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers7['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant3.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
formFieldsPers7['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant3.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers7['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant3.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers7['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant3.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers7['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant3.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

//cadre 5

formFieldsPers7['renseignementsComplementairesObservations_TNS']                    = '';
                                                                                 
//Cadre 6  

formFieldsPers7['renseignementsComplementairesLeDeclarant_TNS']                     = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentantLegal') ? true : false;
formFieldsPers7['renseignementsComplementairesLeMandataire_TNS']                    = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers7['formaliteSignataireNomPrenomDenomination_TNS']						= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers7['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + signataire.adresseMandataire.nomVoieMandataire
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');
formFieldsPers7['formaliteSignataireAdresseComplement_TNS']                         = signataire.adresseMandataire.codePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
formFieldsPers7['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
formFieldsPers7['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers7['formaliteSignatureDate_TNS'] = date;
}

// TNS Dirigeant 4 

//cadre 1
var formFieldsPers8 = {}

formFieldsPers8['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers8['formulaireDependanceM0SNC_TNS']                                    = true;
formFieldsPers8['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers8['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers8['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers8['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers8['formulaireDependanceTNS_TNS']                                      = false;
   
//cadre 2  
                                                                                                   
formFieldsPers8['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 
var socialDirigeant4 = $m0SAS.dirigeant4.cadreIdentiteDirigeant.cadre7DeclarationSociale;

formFieldsPers8['personneLieePPNomNaissance_TNS']                                                 = dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeant4.civilitePersonneMorale.personneLieePMDenomination) ;
formFieldsPers8['personneLieePPNomUsage_TNS']                                                     = dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers8['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers8['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers8['personneLieeQualite_TNS']                                          = dirigeant4.personneLieeQualite;

var nirDeclarant = socialDirigeant4.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers8['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers8['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers8['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers8['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers8['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers8['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers8['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers8['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant4.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS != null ? socialDirigeant4.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
formFieldsPers8['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant4.voletSocialActiviteExerceeAnterieurementLibelleTNS !=null ? socialDirigeant4.voletSocialActiviteExerceeAnterieurementLibelleTNS : '';
formFieldsPers8['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant4.voletSocialActiviteExerceeAnterieurementDepartementTNS != null ? socialDirigeant4.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId() : '';
formFieldsPers8['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant4.voletSocialActiviteExerceeAnterieurementCommuneTNS != null ? socialDirigeant4.voletSocialActiviteExerceeAnterieurementCommuneTNS : '';
if(socialDirigeant4.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant4.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers8['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
formFieldsPers8['voletSocialActiviteSimultaneeOUI_TNS']                             = socialDirigeant4.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers8['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant4.voletSocialActiviteSimultaneeOUINON ? false : true;
formFieldsPers8['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers8['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers8['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers8['voletSocialActiviteSimultaneeAutre_TNS']                           = Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers8['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant4.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
formFieldsPers8['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant4.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers8['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant4.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers8['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant4.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers8['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant4.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

//cadre 5

formFieldsPers8['renseignementsComplementairesObservations_TNS']                    = '';
                                                                                 
//Cadre 6  

formFieldsPers8['renseignementsComplementairesLeDeclarant_TNS']                     = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentantLegal') ? true : false;
formFieldsPers8['renseignementsComplementairesLeMandataire_TNS']                    = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers8['formaliteSignataireNomPrenomDenomination_TNS']						= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers8['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + signataire.adresseMandataire.nomVoieMandataire
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');
formFieldsPers8['formaliteSignataireAdresseComplement_TNS']                         = signataire.adresseMandataire.codePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
formFieldsPers8['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
formFieldsPers8['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers8['formaliteSignatureDate_TNS'] = date;
}

// TNS Dirigeant 5 

//cadre 1
var formFieldsPers9 = {}

formFieldsPers9['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers9['formulaireDependanceM0SNC_TNS']                                    = true;
formFieldsPers9['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers9['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers9['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers9['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers9['formulaireDependanceTNS_TNS']                                      = false;
   
//cadre 2  
                                                                                                   
formFieldsPers9['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 
var socialDirigeant5 = $m0SAS.dirigeant5.cadreIdentiteDirigeant.cadre7DeclarationSociale;

formFieldsPers9['personneLieePPNomNaissance_TNS']                                                 = dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeant5.civilitePersonneMorale.personneLieePMDenomination) ;
formFieldsPers9['personneLieePPNomUsage_TNS']                                                     = dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers9['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers9['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers9['personneLieeQualite_TNS']                                          = dirigeant5.personneLieeQualite;

var nirDeclarant = socialDirigeant5.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers9['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers9['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers9['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers9['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers9['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers9['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers9['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers9['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant5.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS != null ? socialDirigeant5.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
formFieldsPers9['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant5.voletSocialActiviteExerceeAnterieurementLibelleTNS !=null ? socialDirigeant5.voletSocialActiviteExerceeAnterieurementLibelleTNS : '';
formFieldsPers9['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant5.voletSocialActiviteExerceeAnterieurementDepartementTNS != null ? socialDirigeant5.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId() : '';
formFieldsPers9['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant5.voletSocialActiviteExerceeAnterieurementCommuneTNS != null ? socialDirigeant5.voletSocialActiviteExerceeAnterieurementCommuneTNS : '';
if(socialDirigeant5.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant5.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers9['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
formFieldsPers9['voletSocialActiviteSimultaneeOUI_TNS']                             = socialDirigeant5.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers9['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant5.voletSocialActiviteSimultaneeOUINON ? false : true;
formFieldsPers9['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers9['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers9['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers9['voletSocialActiviteSimultaneeAutre_TNS']                           = Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers9['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant5.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
formFieldsPers9['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant5.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers9['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant5.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers9['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant5.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers9['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant5.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

//cadre 5

formFieldsPers9['renseignementsComplementairesObservations_TNS']                    = '';
                                                                                 
//Cadre 6  

formFieldsPers9['renseignementsComplementairesLeDeclarant_TNS']                     = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentantLegal') ? true : false;
formFieldsPers9['renseignementsComplementairesLeMandataire_TNS']                    = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers9['formaliteSignataireNomPrenomDenomination_TNS']						= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers9['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + signataire.adresseMandataire.nomVoieMandataire
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');
formFieldsPers9['formaliteSignataireAdresseComplement_TNS']                         = signataire.adresseMandataire.codePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
formFieldsPers9['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
formFieldsPers9['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers9['formaliteSignatureDate_TNS'] = date;
}

// TNS Dirigeant 6 

//cadre 1
var formFieldsPers10 = {}

formFieldsPers10['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers10['formulaireDependanceM0SNC_TNS']                                    = true;
formFieldsPers10['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers10['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers10['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers10['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers10['formulaireDependanceTNS_TNS']                                      = false;
   
//cadre 2  
                                                                                                   
formFieldsPers10['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 
var socialDirigeant6 = $m0SAS.dirigeant6.cadreIdentiteDirigeant.cadre7DeclarationSociale;

formFieldsPers10['personneLieePPNomNaissance_TNS']                                                 = dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeant6.civilitePersonneMorale.personneLieePMDenomination) ;
formFieldsPers10['personneLieePPNomUsage_TNS']                                                     = dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers10['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers10['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers10['personneLieeQualite_TNS']                                          = dirigeant6.personneLieeQualite;

var nirDeclarant = socialDirigeant6.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers10['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers10['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers10['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers10['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers10['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers10['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers10['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers10['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant6.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS != null ? socialDirigeant6.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
formFieldsPers10['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant6.voletSocialActiviteExerceeAnterieurementLibelleTNS !=null ? socialDirigeant6.voletSocialActiviteExerceeAnterieurementLibelleTNS : '';
formFieldsPers10['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant6.voletSocialActiviteExerceeAnterieurementDepartementTNS != null ? socialDirigeant6.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId() : '';
formFieldsPers10['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant6.voletSocialActiviteExerceeAnterieurementCommuneTNS != null ? socialDirigeant6.voletSocialActiviteExerceeAnterieurementCommuneTNS : '';
if(socialDirigeant6.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant6.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers10['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
formFieldsPers10['voletSocialActiviteSimultaneeOUI_TNS']                             = socialDirigeant6.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers10['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant6.voletSocialActiviteSimultaneeOUINON ? false : true;
formFieldsPers10['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers10['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers10['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers10['voletSocialActiviteSimultaneeAutre_TNS']                           = Value('id').of(socialDirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers10['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = Value('id').of(socialDirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant6.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
formFieldsPers10['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant6.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers10['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant6.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers10['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant6.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers10['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant6.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

//cadre 5

formFieldsPers10['renseignementsComplementairesObservations_TNS']                    = '';
                                                                                 
//Cadre 6  

formFieldsPers10['renseignementsComplementairesLeDeclarant_TNS']                     = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentantLegal') ? true : false;
formFieldsPers10['renseignementsComplementairesLeMandataire_TNS']                    = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers10['formaliteSignataireNomPrenomDenomination_TNS']						= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers10['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + signataire.adresseMandataire.nomVoieMandataire
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');
formFieldsPers10['formaliteSignataireAdresseComplement_TNS']                         = signataire.adresseMandataire.codePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
formFieldsPers10['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
formFieldsPers10['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers10['formaliteSignatureDate_TNS'] = date;
}

// TNS Dirigeant 7 

//cadre 1
var formFieldsPers11 = {}

formFieldsPers11['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers11['formulaireDependanceM0SNC_TNS']                                    = true;
formFieldsPers11['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers11['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers11['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers11['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers11['formulaireDependanceTNS_TNS']                                      = false;
   
//cadre 2  
                                                                                                   
formFieldsPers11['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 
var socialDirigeant7 = $m0SAS.dirigeant7.cadreIdentiteDirigeant.cadre7DeclarationSociale;

formFieldsPers11['personneLieePPNomNaissance_TNS']                                                 = dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeant7.civilitePersonneMorale.personneLieePMDenomination) ;
formFieldsPers11['personneLieePPNomUsage_TNS']                                                     = dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers11['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers11['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers11['personneLieeQualite_TNS']                                          = dirigeant7.personneLieeQualite;

var nirDeclarant = socialDirigeant7.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers11['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers11['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers11['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers11['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers11['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers11['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers11['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers11['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant7.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS != null ? socialDirigeant7.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
formFieldsPers11['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant7.voletSocialActiviteExerceeAnterieurementLibelleTNS !=null ? socialDirigeant7.voletSocialActiviteExerceeAnterieurementLibelleTNS : '';
formFieldsPers11['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant7.voletSocialActiviteExerceeAnterieurementDepartementTNS != null ? socialDirigeant7.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId() : '';
formFieldsPers11['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant7.voletSocialActiviteExerceeAnterieurementCommuneTNS != null ? socialDirigeant7.voletSocialActiviteExerceeAnterieurementCommuneTNS : '';
if(socialDirigeant7.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant7.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers11['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
formFieldsPers11['voletSocialActiviteSimultaneeOUI_TNS']                             = socialDirigeant7.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers11['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant7.voletSocialActiviteSimultaneeOUINON ? false : true;
formFieldsPers11['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant7.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers11['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant7.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers11['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant7.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers11['voletSocialActiviteSimultaneeAutre_TNS']                           = Value('id').of(socialDirigeant7.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers11['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = Value('id').of(socialDirigeant7.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant7.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
formFieldsPers11['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant7.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers11['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant7.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers11['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant7.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers11['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant7.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

//cadre 5

formFieldsPers11['renseignementsComplementairesObservations_TNS']                    = '';
                                                                                 
//Cadre 6  

formFieldsPers11['renseignementsComplementairesLeDeclarant_TNS']                     = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentantLegal') ? true : false;
formFieldsPers11['renseignementsComplementairesLeMandataire_TNS']                    = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers11['formaliteSignataireNomPrenomDenomination_TNS']						= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers11['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + signataire.adresseMandataire.nomVoieMandataire
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');
formFieldsPers11['formaliteSignataireAdresseComplement_TNS']                         = signataire.adresseMandataire.codePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
formFieldsPers11['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
formFieldsPers11['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers11['formaliteSignatureDate_TNS'] = date;
}

// TNS Dirigeant 8 

//cadre 1
var formFieldsPers12 = {}

formFieldsPers12['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers12['formulaireDependanceM0SNC_TNS']                                    = true;
formFieldsPers12['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers12['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers12['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers12['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers12['formulaireDependanceTNS_TNS']                                      = false;
   
//cadre 2  
                                                                                                   
formFieldsPers12['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 
var socialDirigeant8 = $m0SAS.dirigeant8.cadreIdentiteDirigeant.cadre7DeclarationSociale;

formFieldsPers12['personneLieePPNomNaissance_TNS']                                                 = dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeant8.civilitePersonneMorale.personneLieePMDenomination) ;
formFieldsPers12['personneLieePPNomUsage_TNS']                                                     = dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers12['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers12['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers12['personneLieeQualite_TNS']                                          = dirigeant8.personneLieeQualite;

var nirDeclarant = socialDirigeant8.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers12['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers12['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers12['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers12['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers12['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers12['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers12['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers12['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant8.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS != null ? socialDirigeant8.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
formFieldsPers12['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant8.voletSocialActiviteExerceeAnterieurementLibelleTNS !=null ? socialDirigeant8.voletSocialActiviteExerceeAnterieurementLibelleTNS : '';
formFieldsPers12['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant8.voletSocialActiviteExerceeAnterieurementDepartementTNS != null ? socialDirigeant8.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId() : '';
formFieldsPers12['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant8.voletSocialActiviteExerceeAnterieurementCommuneTNS != null ? socialDirigeant8.voletSocialActiviteExerceeAnterieurementCommuneTNS : '';
if(socialDirigeant8.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant8.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers12['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
formFieldsPers12['voletSocialActiviteSimultaneeOUI_TNS']                             = socialDirigeant8.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers12['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant8.voletSocialActiviteSimultaneeOUINON ? false : true;
formFieldsPers12['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant8.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers12['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant8.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers12['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant8.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers12['voletSocialActiviteSimultaneeAutre_TNS']                           = Value('id').of(socialDirigeant8.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers12['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = Value('id').of(socialDirigeant8.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant8.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
formFieldsPers12['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant8.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers12['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant8.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers12['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant8.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers12['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant8.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

//cadre 5

formFieldsPers12['renseignementsComplementairesObservations_TNS']                    = '';
                                                                                 
//Cadre 6  

formFieldsPers12['renseignementsComplementairesLeDeclarant_TNS']                     = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentantLegal') ? true : false;
formFieldsPers12['renseignementsComplementairesLeMandataire_TNS']                    = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers12['formaliteSignataireNomPrenomDenomination_TNS']						= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers12['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + signataire.adresseMandataire.nomVoieMandataire
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');
formFieldsPers12['formaliteSignataireAdresseComplement_TNS']                         = signataire.adresseMandataire.codePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
formFieldsPers12['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
formFieldsPers12['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers12['formaliteSignatureDate_TNS'] = date;
}

// TNS Dirigeant 9 

//cadre 1
var formFieldsPers13 = {}

formFieldsPers13['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers13['formulaireDependanceM0SNC_TNS']                                    = true;
formFieldsPers13['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers13['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers13['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers13['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers13['formulaireDependanceTNS_TNS']                                      = false;
   
//cadre 2  
                                                                                                   
formFieldsPers13['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 
var socialDirigeant9 = $m0SAS.dirigeant9.cadreIdentiteDirigeant.cadre7DeclarationSociale;

formFieldsPers13['personneLieePPNomNaissance_TNS']                                                 = dirigeant9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeant9.civilitePersonneMorale.personneLieePMDenomination) ;
formFieldsPers13['personneLieePPNomUsage_TNS']                                                     = dirigeant9.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant9.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers13['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers13['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers13['personneLieeQualite_TNS']                                          = dirigeant9.personneLieeQualite;

var nirDeclarant = socialDirigeant9.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers13['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers13['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers13['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers13['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers13['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers13['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers13['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers13['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant9.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS != null ? socialDirigeant9.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
formFieldsPers13['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant9.voletSocialActiviteExerceeAnterieurementLibelleTNS !=null ? socialDirigeant9.voletSocialActiviteExerceeAnterieurementLibelleTNS : '';
formFieldsPers13['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant9.voletSocialActiviteExerceeAnterieurementDepartementTNS != null ? socialDirigeant9.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId() : '';
formFieldsPers13['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant9.voletSocialActiviteExerceeAnterieurementCommuneTNS != null ? socialDirigeant9.voletSocialActiviteExerceeAnterieurementCommuneTNS : '';
if(socialDirigeant9.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant9.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers13['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
formFieldsPers13['voletSocialActiviteSimultaneeOUI_TNS']                             = socialDirigeant9.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers13['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant9.voletSocialActiviteSimultaneeOUINON ? false : true;
formFieldsPers13['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant9.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers13['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant9.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers13['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant9.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers13['voletSocialActiviteSimultaneeAutre_TNS']                           = Value('id').of(socialDirigeant9.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers13['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = Value('id').of(socialDirigeant9.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant9.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
formFieldsPers13['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant9.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers13['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant9.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers13['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant9.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers13['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant9.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

//cadre 5

formFieldsPers13['renseignementsComplementairesObservations_TNS']                    = '';
                                                                                 
//Cadre 6  

formFieldsPers13['renseignementsComplementairesLeDeclarant_TNS']                     = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentantLegal') ? true : false;
formFieldsPers13['renseignementsComplementairesLeMandataire_TNS']                    = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers13['formaliteSignataireNomPrenomDenomination_TNS']						= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers13['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + signataire.adresseMandataire.nomVoieMandataire
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');
formFieldsPers13['formaliteSignataireAdresseComplement_TNS']                         = signataire.adresseMandataire.codePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
formFieldsPers13['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
formFieldsPers13['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers13['formaliteSignatureDate_TNS'] = date;
}

// TNS Dirigeant 10 

//cadre 1
var formFieldsPers14 = {}

formFieldsPers14['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers14['formulaireDependanceM0SNC_TNS']                                    = true;
formFieldsPers14['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers14['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers14['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers14['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers14['formulaireDependanceTNS_TNS']                                      = false;
   
//cadre 2  
                                                                                                   
formFieldsPers14['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 
var socialDirigeant10 = $m0SAS.dirigeant10.cadreIdentiteDirigeant.cadre7DeclarationSociale;

formFieldsPers14['personneLieePPNomNaissance_TNS']                                                 = dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeant10.civilitePersonneMorale.personneLieePMDenomination) ;
formFieldsPers14['personneLieePPNomUsage_TNS']                                                     = dirigeant10.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant10.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers14['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers14['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers14['personneLieeQualite_TNS']                                          = dirigeant10.personneLieeQualite;

var nirDeclarant = socialDirigeant10.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers14['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers14['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers14['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers14['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers14['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers14['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers14['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers14['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant10.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS != null ? socialDirigeant10.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
formFieldsPers14['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant10.voletSocialActiviteExerceeAnterieurementLibelleTNS !=null ? socialDirigeant10.voletSocialActiviteExerceeAnterieurementLibelleTNS : '';
formFieldsPers14['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant10.voletSocialActiviteExerceeAnterieurementDepartementTNS != null ? socialDirigeant10.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId() : '';
formFieldsPers14['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant10.voletSocialActiviteExerceeAnterieurementCommuneTNS != null ? socialDirigeant10.voletSocialActiviteExerceeAnterieurementCommuneTNS : '';
if(socialDirigeant10.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant10.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers14['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
formFieldsPers14['voletSocialActiviteSimultaneeOUI_TNS']                             = socialDirigeant10.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers14['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant10.voletSocialActiviteSimultaneeOUINON ? false : true;
formFieldsPers14['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant10.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers14['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant10.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers14['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant10.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers14['voletSocialActiviteSimultaneeAutre_TNS']                           = Value('id').of(socialDirigeant10.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers14['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = Value('id').of(socialDirigeant10.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant10.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
formFieldsPers14['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant10.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers14['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant10.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers14['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant10.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers14['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant10.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

//cadre 5

formFieldsPers14['renseignementsComplementairesObservations_TNS']                    = '';
                                                                                 
//Cadre 6  

formFieldsPers14['renseignementsComplementairesLeDeclarant_TNS']                     = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentantLegal') ? true : false;
formFieldsPers14['renseignementsComplementairesLeMandataire_TNS']                    = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers14['formaliteSignataireNomPrenomDenomination_TNS']						= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers14['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + signataire.adresseMandataire.nomVoieMandataire
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');
formFieldsPers14['formaliteSignataireAdresseComplement_TNS']                         = signataire.adresseMandataire.codePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
formFieldsPers14['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
formFieldsPers14['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers14['formaliteSignatureDate_TNS'] = date;
}

// TNS Dirigeant 11 

//cadre 1
var formFieldsPers15 = {}

formFieldsPers15['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers15['formulaireDependanceM0SNC_TNS']                                    = true;
formFieldsPers15['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers15['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers15['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers15['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers15['formulaireDependanceTNS_TNS']                                      = false;
   
//cadre 2  
                                                                                                   
formFieldsPers15['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 
var socialDirigeant11 = $m0SAS.dirigeant11.cadreIdentiteDirigeant.cadre7DeclarationSociale;

formFieldsPers15['personneLieePPNomNaissance_TNS']                                                 = dirigeant11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeant11.civilitePersonneMorale.personneLieePMDenomination) ;
formFieldsPers15['personneLieePPNomUsage_TNS']                                                     = dirigeant11.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant11.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers15['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers15['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers15['personneLieeQualite_TNS']                                          = dirigeant11.personneLieeQualite;

var nirDeclarant = socialDirigeant11.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers15['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers15['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers15['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers15['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers15['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers15['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers15['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers15['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant11.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS != null ? socialDirigeant11.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
formFieldsPers15['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant11.voletSocialActiviteExerceeAnterieurementLibelleTNS !=null ? socialDirigeant11.voletSocialActiviteExerceeAnterieurementLibelleTNS : '';
formFieldsPers15['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant11.voletSocialActiviteExerceeAnterieurementDepartementTNS != null ? socialDirigeant11.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId() : '';
formFieldsPers15['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant11.voletSocialActiviteExerceeAnterieurementCommuneTNS != null ? socialDirigeant11.voletSocialActiviteExerceeAnterieurementCommuneTNS : '';
if(socialDirigeant11.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant11.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers15['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
formFieldsPers15['voletSocialActiviteSimultaneeOUI_TNS']                             = socialDirigeant11.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers15['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant11.voletSocialActiviteSimultaneeOUINON ? false : true;
formFieldsPers15['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant11.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers15['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant11.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers15['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant11.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers15['voletSocialActiviteSimultaneeAutre_TNS']                           = Value('id').of(socialDirigeant11.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers15['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = Value('id').of(socialDirigeant11.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant11.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
formFieldsPers15['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant11.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers15['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant11.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers15['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant11.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers15['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant11.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

//cadre 5

formFieldsPers15['renseignementsComplementairesObservations_TNS']                    = '';
                                                                                 
//Cadre 6  

formFieldsPers15['renseignementsComplementairesLeDeclarant_TNS']                     = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentantLegal') ? true : false;
formFieldsPers15['renseignementsComplementairesLeMandataire_TNS']                    = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers15['formaliteSignataireNomPrenomDenomination_TNS']						= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers15['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + signataire.adresseMandataire.nomVoieMandataire
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');
formFieldsPers15['formaliteSignataireAdresseComplement_TNS']                         = signataire.adresseMandataire.codePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
formFieldsPers15['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
formFieldsPers15['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers15['formaliteSignatureDate_TNS'] = date;
}

// TNS Dirigeant 12 

//cadre 1
var formFieldsPers16 = {}

formFieldsPers16['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers16['formulaireDependanceM0SNC_TNS']                                    = true;
formFieldsPers16['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers16['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers16['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers16['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers16['formulaireDependanceTNS_TNS']                                      = false;
   
//cadre 2  
                                                                                                   
formFieldsPers16['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 
var socialDirigeant12 = $m0SAS.dirigeant12.cadreIdentiteDirigeant.cadre7DeclarationSociale;

formFieldsPers16['personneLieePPNomNaissance_TNS']                                                 = dirigeant12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeant12.civilitePersonneMorale.personneLieePMDenomination) ;
formFieldsPers16['personneLieePPNomUsage_TNS']                                                     = dirigeant12.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant12.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers16['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers16['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers16['personneLieeQualite_TNS']                                          = dirigeant12.personneLieeQualite;

var nirDeclarant = socialDirigeant12.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers16['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers16['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers16['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers16['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers16['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers16['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers16['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers16['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant12.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS != null ? socialDirigeant12.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
formFieldsPers16['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant12.voletSocialActiviteExerceeAnterieurementLibelleTNS !=null ? socialDirigeant12.voletSocialActiviteExerceeAnterieurementLibelleTNS : '';
formFieldsPers16['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant12.voletSocialActiviteExerceeAnterieurementDepartementTNS != null ? socialDirigeant12.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId() : '';
formFieldsPers16['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant12.voletSocialActiviteExerceeAnterieurementCommuneTNS != null ? socialDirigeant12.voletSocialActiviteExerceeAnterieurementCommuneTNS : '';
if(socialDirigeant12.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant12.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers16['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
formFieldsPers16['voletSocialActiviteSimultaneeOUI_TNS']                             = socialDirigeant12.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers16['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant12.voletSocialActiviteSimultaneeOUINON ? false : true;
formFieldsPers16['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant12.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers16['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant12.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers16['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant12.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers16['voletSocialActiviteSimultaneeAutre_TNS']                           = Value('id').of(socialDirigeant12.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers16['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = Value('id').of(socialDirigeant12.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant12.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
formFieldsPers16['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant12.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers16['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant12.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers16['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant12.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers16['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant12.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

//cadre 5

formFieldsPers16['renseignementsComplementairesObservations_TNS']                    = '';
                                                                                 
//Cadre 6  

formFieldsPers16['renseignementsComplementairesLeDeclarant_TNS']                     = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentantLegal') ? true : false;
formFieldsPers16['renseignementsComplementairesLeMandataire_TNS']                    = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers16['formaliteSignataireNomPrenomDenomination_TNS']						= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers16['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + signataire.adresseMandataire.nomVoieMandataire
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');
formFieldsPers16['formaliteSignataireAdresseComplement_TNS']                         = signataire.adresseMandataire.codePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
formFieldsPers16['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
formFieldsPers16['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers16['formaliteSignatureDate_TNS'] = date;
}

// TNS Dirigeant 13 

//cadre 1
var formFieldsPers17 = {}

formFieldsPers17['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers17['formulaireDependanceM0SNC_TNS']                                    = true;
formFieldsPers17['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers17['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers17['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers17['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers17['formulaireDependanceTNS_TNS']                                      = false;
   
//cadre 2  
                                                                                                   
formFieldsPers17['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 
var socialDirigeant13 = $m0SAS.dirigeant13.cadreIdentiteDirigeant.cadre7DeclarationSociale;

formFieldsPers17['personneLieePPNomNaissance_TNS']                                                 = dirigeant13.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant13.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeant13.civilitePersonneMorale.personneLieePMDenomination) ;
formFieldsPers17['personneLieePPNomUsage_TNS']                                                     = dirigeant13.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant13.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant13.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant13.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant13.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant13.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers17['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant13.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers17['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers17['personneLieeQualite_TNS']                                          = dirigeant13.personneLieeQualite;

var nirDeclarant = socialDirigeant13.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers17['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers17['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers17['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant13.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers17['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant13.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers17['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant13.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers17['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant13.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers17['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant13.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers17['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant13.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS != null ? socialDirigeant13.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
formFieldsPers17['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant13.voletSocialActiviteExerceeAnterieurementLibelleTNS !=null ? socialDirigeant13.voletSocialActiviteExerceeAnterieurementLibelleTNS : '';
formFieldsPers17['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant13.voletSocialActiviteExerceeAnterieurementDepartementTNS != null ? socialDirigeant13.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId() : '';
formFieldsPers17['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant13.voletSocialActiviteExerceeAnterieurementCommuneTNS != null ? socialDirigeant13.voletSocialActiviteExerceeAnterieurementCommuneTNS : '';
if(socialDirigeant13.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant13.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers17['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
formFieldsPers17['voletSocialActiviteSimultaneeOUI_TNS']                             = socialDirigeant13.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers17['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant13.voletSocialActiviteSimultaneeOUINON ? false : true;
formFieldsPers17['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant13.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers17['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant13.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers17['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant13.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers17['voletSocialActiviteSimultaneeAutre_TNS']                           = Value('id').of(socialDirigeant13.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers17['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = Value('id').of(socialDirigeant13.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant13.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
formFieldsPers17['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant13.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers17['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant13.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers17['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant13.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers17['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant13.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

//cadre 5

formFieldsPers17['renseignementsComplementairesObservations_TNS']                    = '';
                                                                                 
//Cadre 6  

formFieldsPers17['renseignementsComplementairesLeDeclarant_TNS']                     = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentantLegal') ? true : false;
formFieldsPers17['renseignementsComplementairesLeMandataire_TNS']                    = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers17['formaliteSignataireNomPrenomDenomination_TNS']						= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers17['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + signataire.adresseMandataire.nomVoieMandataire
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');
formFieldsPers17['formaliteSignataireAdresseComplement_TNS']                         = signataire.adresseMandataire.codePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
formFieldsPers17['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
formFieldsPers17['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers17['formaliteSignatureDate_TNS'] = date;
}

// TNS Dirigeant 14 

//cadre 1
var formFieldsPers18 = {}

formFieldsPers18['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers18['formulaireDependanceM0SNC_TNS']                                    = true;
formFieldsPers18['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers18['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers18['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers18['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers18['formulaireDependanceTNS_TNS']                                      = false;
   
//cadre 2  
                                                                                                   
formFieldsPers18['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 
var socialDirigeant14 = $m0SAS.dirigeant14.cadreIdentiteDirigeant.cadre7DeclarationSociale;

formFieldsPers18['personneLieePPNomNaissance_TNS']                                                 = dirigeant14.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant14.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeant14.civilitePersonneMorale.personneLieePMDenomination) ;
formFieldsPers18['personneLieePPNomUsage_TNS']                                                     = dirigeant14.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant14.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant14.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant14.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant14.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant14.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers18['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant14.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers18['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers18['personneLieeQualite_TNS']                                          = dirigeant14.personneLieeQualite;

var nirDeclarant = socialDirigeant14.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers18['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers18['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers18['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant14.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers18['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant14.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers18['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant14.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers18['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant14.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers18['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant14.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers18['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant14.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS != null ? socialDirigeant14.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
formFieldsPers18['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant14.voletSocialActiviteExerceeAnterieurementLibelleTNS !=null ? socialDirigeant14.voletSocialActiviteExerceeAnterieurementLibelleTNS : '';
formFieldsPers18['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant14.voletSocialActiviteExerceeAnterieurementDepartementTNS != null ? socialDirigeant14.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId() : '';
formFieldsPers18['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant14.voletSocialActiviteExerceeAnterieurementCommuneTNS != null ? socialDirigeant14.voletSocialActiviteExerceeAnterieurementCommuneTNS : '';
if(socialDirigeant14.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant14.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers18['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
formFieldsPers18['voletSocialActiviteSimultaneeOUI_TNS']                             = socialDirigeant14.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers18['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant14.voletSocialActiviteSimultaneeOUINON ? false : true;
formFieldsPers18['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant14.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers18['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant14.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers18['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant14.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers18['voletSocialActiviteSimultaneeAutre_TNS']                           = Value('id').of(socialDirigeant14.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers18['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = Value('id').of(socialDirigeant14.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant14.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
formFieldsPers18['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant14.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers18['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant14.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers18['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant14.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers18['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant14.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

//cadre 5

formFieldsPers18['renseignementsComplementairesObservations_TNS']                    = '';
                                                                                 
//Cadre 6  

formFieldsPers18['renseignementsComplementairesLeDeclarant_TNS']                     = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentantLegal') ? true : false;
formFieldsPers18['renseignementsComplementairesLeMandataire_TNS']                    = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers18['formaliteSignataireNomPrenomDenomination_TNS']						= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers18['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + signataire.adresseMandataire.nomVoieMandataire
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');
formFieldsPers18['formaliteSignataireAdresseComplement_TNS']                         = signataire.adresseMandataire.codePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
formFieldsPers18['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
formFieldsPers18['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers18['formaliteSignatureDate_TNS'] = date;
}

// JQPA 1

//cadre 1
var formFieldsPers19 = {}

// Cadre 1
formFieldsPers19['jqpa_intercalaire']                         = true;
formFieldsPers19['jqpa_formulaire']                           = false;

// Cadre 4
formFieldsPers19['jqpa_entreprise_denomination']              = societe.entrepriseDenomination;
formFieldsPers19['jqpa_entreprise_formeJuridique']            = societe.entrepriseFormeJuridique; 
formFieldsPers19['jqpa_entreprise_siren']                     = '';

 // Cadre 5A
formFieldsPers19['jqpa_pm_activite']                          = jqpa1.jqpaActiviteJqpaPP;

//Cadre 5B
formFieldsPers19['jqpa_aqpaSituationPM_diplome']              = Value('id').of(jqpa1.jqpaSituation).eq('jqpaSituationDiplome') or Value('id').of(jqpa1.jqpaSituation).eq('jqpaSituationExperience') ? true : false;
formFieldsPers19['jqpa_qualitePersonneQualifieePM_representant'] = Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPRepresentantLegal') ? true : false;
formFieldsPers19['jqpa_qualitePersonneQualifieePM_conjoint']  = Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPConjoint') ? true : false;
formFieldsPers19['jqpa_qualitePersonneQualifieePM_salarie']   = Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') ? true : false;
formFieldsPers19['jqpa_qualitePersonneQualifieePM_autre']     = Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre') ? true : false;
formFieldsPers19['jqpa_qualitePersonneQualifieePMAutre']      = jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifieePPChampAutre;

formFieldsPers19['jqpa_identitePMNomNaissance']               = jqpa1.identitePersonneQualifiee.jqpaIdentitePPNomNaissance != null ? jqpa1.identitePersonneQualifiee.jqpaIdentitePPNomNaissance : '';
formFieldsPers19['jqpa_identitePMNomUsage']                   = jqpa1.identitePersonneQualifiee.jqpaIdentitePPNomUsage != null ? jqpa1.identitePersonneQualifiee.jqpaIdentitePPNomUsage : '';
var prenoms=[];
for ( i = 0; i < jqpa1.identitePersonneQualifiee.jqpaIdentitePPPrenom.size() ; i++ ){prenoms.push(jqpa1.identitePersonneQualifiee.jqpaIdentitePPPrenom[i]);}                            
formFieldsPers19['jqpa_identitePMPrenom']            = prenoms.toString();

if (jqpa1.identitePersonneQualifiee.jqpaIdentitePPDateNaissance !== null) {
	var dateTmp = new Date(parseInt(jqpa1.identitePersonneQualifiee.jqpaIdentitePPDateNaissance.getTimeInMillis()));
	var dateNaissance = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	dateNaissance = dateNaissance.concat(pad(month.toString()));
	dateNaissance = dateNaissance.concat(dateTmp.getFullYear().toString());
	formFieldsPers19['jqpa_identitePMDateNaissance']          = dateNaissance;
}
formFieldsPers19['jqpa_identitePMDepartement']                = jqpa1.identitePersonneQualifiee.jqpaIdentitePPDepartement != null ? jqpa1.identitePersonneQualifiee.jqpaIdentitePPDepartement.getId() : '';
formFieldsPers19['jqpa_identitePMLieuNaissancePays']          = (Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') or Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre')) ? ((jqpa1.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune != null ? jqpa1.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune : jqpa1.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceVille) + ' / ' + jqpa1.identitePersonneQualifiee.jqpaIdentitePPLieuNaissancePays) : '';

// Cadre 5C
formFieldsPers19['jqpa_aqpaSituationPM_engagement']           = Value('id').of(jqpa1.jqpaSituation).eq('jqpaSituationRecrutement') ? true : false;

// JQPA 2

//cadre 1
var formFieldsPers20 = {}

// Cadre 1
formFieldsPers20['jqpa_intercalaire']                         = true;
formFieldsPers20['jqpa_formulaire']                           = false;

// Cadre 4
formFieldsPers20['jqpa_entreprise_denomination']              = societe.entrepriseDenomination;
formFieldsPers20['jqpa_entreprise_formeJuridique']            = societe.entrepriseFormeJuridique; 
formFieldsPers20['jqpa_entreprise_siren']                     = '';

 // Cadre 5A
formFieldsPers20['jqpa_pm_activite']                          = jqpa2.jqpaActiviteJqpaPP;

//Cadre 5B
formFieldsPers20['jqpa_aqpaSituationPM_diplome']              = Value('id').of(jqpa2.jqpaSituation).eq('jqpaSituationDiplome') or Value('id').of(jqpa2.jqpaSituation).eq('jqpaSituationExperience') ? true : false;
formFieldsPers20['jqpa_qualitePersonneQualifieePM_representant'] = Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPRepresentantLegal') ? true : false;
formFieldsPers20['jqpa_qualitePersonneQualifieePM_conjoint']  = Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPConjoint') ? true : false;
formFieldsPers20['jqpa_qualitePersonneQualifieePM_salarie']   = Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') ? true : false;
formFieldsPers20['jqpa_qualitePersonneQualifieePM_autre']     = Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre') ? true : false;
formFieldsPers20['jqpa_qualitePersonneQualifieePMAutre']      = jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifieePPChampAutre;

formFieldsPers20['jqpa_identitePMNomNaissance']               = jqpa2.identitePersonneQualifiee.jqpaIdentitePPNomNaissance != null ? jqpa2.identitePersonneQualifiee.jqpaIdentitePPNomNaissance : '';
formFieldsPers20['jqpa_identitePMNomUsage']                   = jqpa2.identitePersonneQualifiee.jqpaIdentitePPNomUsage != null ? jqpa2.identitePersonneQualifiee.jqpaIdentitePPNomUsage : '';
var prenoms=[];
for ( i = 0; i < jqpa2.identitePersonneQualifiee.jqpaIdentitePPPrenom.size() ; i++ ){prenoms.push(jqpa2.identitePersonneQualifiee.jqpaIdentitePPPrenom[i]);}                            
formFieldsPers20['jqpa_identitePMPrenom']            = prenoms.toString();

if (jqpa2.identitePersonneQualifiee.jqpaIdentitePPDateNaissance !== null) {
	var dateTmp = new Date(parseInt(jqpa2.identitePersonneQualifiee.jqpaIdentitePPDateNaissance.getTimeInMillis()));
	var dateNaissance = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	dateNaissance = dateNaissance.concat(pad(month.toString()));
	dateNaissance = dateNaissance.concat(dateTmp.getFullYear().toString());
	formFieldsPers20['jqpa_identitePMDateNaissance']          = dateNaissance;
}
formFieldsPers20['jqpa_identitePMDepartement']                = jqpa2.identitePersonneQualifiee.jqpaIdentitePPDepartement != null ? jqpa2.identitePersonneQualifiee.jqpaIdentitePPDepartement.getId() : '';
formFieldsPers20['jqpa_identitePMLieuNaissancePays']          = (Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') or Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre')) ? ((jqpa2.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune != null ? jqpa2.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune : jqpa2.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceVille) + ' / ' + jqpa2.identitePersonneQualifiee.jqpaIdentitePPLieuNaissancePays) : '';

// Cadre 5C
formFieldsPers20['jqpa_aqpaSituationPM_engagement']           = Value('id').of(jqpa2.jqpaSituation).eq('jqpaSituationRecrutement') ? true : false;

// JQPA 3

//cadre 1
var formFieldsPers21 = {}

// Cadre 1
formFieldsPers21['jqpa_intercalaire']                         = true;
formFieldsPers21['jqpa_formulaire']                           = false;

// Cadre 4
formFieldsPers21['jqpa_entreprise_denomination']              = societe.entrepriseDenomination;
formFieldsPers21['jqpa_entreprise_formeJuridique']            = societe.entrepriseFormeJuridique; 
formFieldsPers21['jqpa_entreprise_siren']                     = '';

 // Cadre 5A
formFieldsPers21['jqpa_pm_activite']                          = jqpa3.jqpaActiviteJqpaPP;

//Cadre 5B
formFieldsPers21['jqpa_aqpaSituationPM_diplome']              = Value('id').of(jqpa3.jqpaSituation).eq('jqpaSituationDiplome') or Value('id').of(jqpa3.jqpaSituation).eq('jqpaSituationExperience') ? true : false;
formFieldsPers21['jqpa_qualitePersonneQualifieePM_representant'] = Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPRepresentantLegal') ? true : false;
formFieldsPers21['jqpa_qualitePersonneQualifieePM_conjoint']  = Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPConjoint') ? true : false;
formFieldsPers21['jqpa_qualitePersonneQualifieePM_salarie']   = Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') ? true : false;
formFieldsPers21['jqpa_qualitePersonneQualifieePM_autre']     = Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre') ? true : false;
formFieldsPers21['jqpa_qualitePersonneQualifieePMAutre']      = jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifieePPChampAutre;

formFieldsPers21['jqpa_identitePMNomNaissance']               = jqpa3.identitePersonneQualifiee.jqpaIdentitePPNomNaissance != null ? jqpa3.identitePersonneQualifiee.jqpaIdentitePPNomNaissance : '';
formFieldsPers21['jqpa_identitePMNomUsage']                   = jqpa3.identitePersonneQualifiee.jqpaIdentitePPNomUsage != null ? jqpa3.identitePersonneQualifiee.jqpaIdentitePPNomUsage : '';
var prenoms=[];
for ( i = 0; i < jqpa3.identitePersonneQualifiee.jqpaIdentitePPPrenom.size() ; i++ ){prenoms.push(jqpa3.identitePersonneQualifiee.jqpaIdentitePPPrenom[i]);}                            
formFieldsPers21['jqpa_identitePMPrenom']            = prenoms.toString();

if (jqpa3.identitePersonneQualifiee.jqpaIdentitePPDateNaissance !== null) {
	var dateTmp = new Date(parseInt(jqpa3.identitePersonneQualifiee.jqpaIdentitePPDateNaissance.getTimeInMillis()));
	var dateNaissance = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	dateNaissance = dateNaissance.concat(pad(month.toString()));
	dateNaissance = dateNaissance.concat(dateTmp.getFullYear().toString());
	formFieldsPers21['jqpa_identitePMDateNaissance']          = dateNaissance;
}
formFieldsPers21['jqpa_identitePMDepartement']                = jqpa3.identitePersonneQualifiee.jqpaIdentitePPDepartement != null ? jqpa3.identitePersonneQualifiee.jqpaIdentitePPDepartement.getId() : '';
formFieldsPers21['jqpa_identitePMLieuNaissancePays']          = (Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') or Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre')) ? ((jqpa3.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune != null ? jqpa3.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune : jqpa3.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceVille) + ' / ' + jqpa3.identitePersonneQualifiee.jqpaIdentitePPLieuNaissancePays) : '';

// Cadre 5C
formFieldsPers21['jqpa_aqpaSituationPM_engagement']           = Value('id').of(jqpa3.jqpaSituation).eq('jqpaSituationRecrutement') ? true : false;

// Intercalaire NDI

// Cadre 1

formFields['complete_immatriculation_ndi']                   = true;
formFields['complete_modification_ndi']                      = false;

// Cadre 2

formFields['denomination_ndi']                               = societe.entrepriseDenomination;
formFields['adresseSiege_voie_ndi']                          = (siege.cadreAdresseProfessionnelle.numeroAdresseSiege != null ? siege.cadreAdresseProfessionnelle.numeroAdresseSiege : '')
															 + ' ' + (siege.cadreAdresseProfessionnelle.indiceAdresseSiege != null ? siege.cadreAdresseProfessionnelle.indiceAdresseSiege : '')
															 + ' ' + (siege.cadreAdresseProfessionnelle.typeAdresseSiege != null ? siege.cadreAdresseProfessionnelle.typeAdresseSiege : '')
															 +' ' + siege.cadreAdresseProfessionnelle.voieAdresseSiege
															 + ' ' + (siege.cadreAdresseProfessionnelle.complementAdresseSiege != null ? siege.cadreAdresseProfessionnelle.complementAdresseSiege : '')
															 + ' ' + (siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege != null ? siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege : '');
formFields['adresseSiege_codePostal_ndi']                    = siege.cadreAdresseProfessionnelle.codePostalAdresseSiege;
formFields['adresseSiege_commune_ndi']                       = siege.cadreAdresseProfessionnelle.communeAdresseSiege;

// Cadre 3
if(etablissement.etablissementNomDomaine != null) {
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTmp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['dateDebutActivite_ndi'] = date;
} else if (signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['dateDebutActivite_ndi'] = date;
}

formFields['adresseEtablissement_voie_ndi']                          = (siege.formaliteCreationAvecActivite or Value('id').of(societe.entrepriseFormeJuridique).eq('5785') or Value('id').of(societe.entrepriseFormeJuridique).eq('5585') or Value('id').of(societe.entrepriseFormeJuridique).eq('5685') or Value('id').of(societe.entrepriseFormeJuridique).eq('5385') or Value('id').of(societe.entrepriseFormeJuridique).eq('3110') or Value('id').of(societe.entrepriseFormeJuridique).eq('3120') or Value('id').of(societe.entrepriseFormeJuridique).eq('5800')) ?
																	(etablissement.cadreAdresseEtablissement.voieAdresseEtablissement != null ?
																	((etablissement.cadreAdresseEtablissement.numeroAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.numeroAdresseEtablissement : '')
																	+ ' ' + (etablissement.cadreAdresseEtablissement.indiceAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.indiceAdresseEtablissement : '')
																	+ ' ' + (etablissement.cadreAdresseEtablissement.typeAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.typeAdresseEtablissement : '')
																	+ ' ' + etablissement.cadreAdresseEtablissement.voieAdresseEtablissement
																	+ ' ' + (etablissement.cadreAdresseEtablissement.complementAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.complementAdresseEtablissement : '')
																	+ ' ' + (etablissement.cadreAdresseEtablissement.distributionSpecialeAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.distributionSpecialeAdresseEtablissement : '')) : 
																	((siege.cadreAdresseProfessionnelle.numeroAdresseSiege != null ? siege.cadreAdresseProfessionnelle.numeroAdresseSiege : '')
																	+ ' ' + (siege.cadreAdresseProfessionnelle.indiceAdresseSiege != null ? siege.cadreAdresseProfessionnelle.indiceAdresseSiege : '')
																	+ ' ' + (siege.cadreAdresseProfessionnelle.typeAdresseSiege != null ? siege.cadreAdresseProfessionnelle.typeAdresseSiege : '')
																	+' ' + siege.cadreAdresseProfessionnelle.voieAdresseSiege
																	+ ' ' + (siege.cadreAdresseProfessionnelle.complementAdresseSiege != null ? siege.cadreAdresseProfessionnelle.complementAdresseSiege : '')
																	+ ' ' + (siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege != null ? siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege : ''))) : '';
formFields['adresseEtablissement_codePostal_ndi']                   = (siege.formaliteCreationAvecActivite or Value('id').of(societe.entrepriseFormeJuridique).eq('5785') or Value('id').of(societe.entrepriseFormeJuridique).eq('5585') or Value('id').of(societe.entrepriseFormeJuridique).eq('5685') or Value('id').of(societe.entrepriseFormeJuridique).eq('5385') or Value('id').of(societe.entrepriseFormeJuridique).eq('3110') or Value('id').of(societe.entrepriseFormeJuridique).eq('3120') or Value('id').of(societe.entrepriseFormeJuridique).eq('5800')) ?
																	(etablissement.cadreAdresseEtablissement.codePostalAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.codePostalAdresseEtablissement : siege.cadreAdresseProfessionnelle.codePostalAdresseSiege) : '';
formFields['adresseEtablissement_commune_ndi']                      = (siege.formaliteCreationAvecActivite or Value('id').of(societe.entrepriseFormeJuridique).eq('5785') or Value('id').of(societe.entrepriseFormeJuridique).eq('5585') or Value('id').of(societe.entrepriseFormeJuridique).eq('5685') or Value('id').of(societe.entrepriseFormeJuridique).eq('5385') or Value('id').of(societe.entrepriseFormeJuridique).eq('3110') or Value('id').of(societe.entrepriseFormeJuridique).eq('3120') or Value('id').of(societe.entrepriseFormeJuridique).eq('5800')) ?
																	(etablissement.cadreAdresseEtablissement.communeAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.communeAdresseEtablissement : siege.cadreAdresseProfessionnelle.communeAdresseSiege) : '';
formFields['nomDomaineEtablissement_ndi']                    = etablissement.etablissementNomDomaine;
formFields['suppressionNomDomaineEtablissement_ndi']         = false;
formFields['remplacementNomDomaineEtablissement_ndi']        = false;
formFields['declarationInitialeNomDomaineEtablissement_ndi'] = true;
}

// Cadre 4

if (siege.societeNomDomaine.size() > 0) {
   if (etablissement.etablissementDateDebutActivite != null and "" !== siege.societeNomDomaine[0]) {
       var dateTmp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
       var date = pad(dateTmp.getDate().toString());
       var month = dateTmp.getMonth() + 1;
       date = date.concat(pad(month.toString()));
       date = date.concat(dateTmp.getFullYear().toString());
       formFields['dateEffetNomDomainePM_ndi'] = date;
   } else if (signataire.formaliteSignatureDate != null and "" !== siege.societeNomDomaine[0]) {
       var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
       var date = pad(dateTmp.getDate().toString());
       var month = dateTmp.getMonth() + 1;
       date = date.concat(pad(month.toString()));
       date = date.concat(dateTmp.getFullYear().toString());
       formFields['dateEffetNomDomainePM_ndi'] = date;
   }
   for (var i = 0; i < siege.societeNomDomaine.size(); i++) {
       formFields['nomDomainePM_ndi[' + i + ']'] = siege.societeNomDomaine[i];
       formFields['nouveauNomDomainePM_ndi[' + i + ']'] = siege.societeNomDomaine[i] !== "" ? true : false;
   }
}


/*
 * Création du dossier avec option fiscale : ajout du cerfa avec option fiscale
 */
var cerfaDoc1 = nash.doc //
	.load('models/cerfa_13959-07_M0_SAS_avec_option_fiscale.pdf') //
	.apply(formFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));

/*
 * Création du dossier sans option fiscale : ajout du cerfa sans option fiscale
 */
 
 var cerfaDoc2 = nash.doc //
	.load('models/cerfa_13959-07_M0_SAS_sans_option_fiscale.pdf') //
	.apply (formFields);
	cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));


/*
 * Ajout de l'intercalaire M0 Prime 1
 */
 
 if (societe.entrepriseFusionScission or societe.entrepriseAutreEtablissementUE or dirigeant6.autreDirigeant or (representants.length > 0))
{
	var m0PrimeDoc1 = nash.doc //
		.load('models/cerfa_M0prime_SAS_creation.pdf') //
		.apply (formFieldsPers1);
	cerfaDoc1.append(m0PrimeDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire M0 Prime 2
 */
 
 if (societe.fusionGroup1.fusionAutreSociete or societe.cadreAutreEtablissementUE1.declarationAutreEtablissementUE or (representants.length > 3))
{
	var m0PrimeDoc1 = nash.doc //
		.load('models/cerfa_M0prime_SAS_creation.pdf') //
		.apply (formFieldsPers2);
	cerfaDoc1.append(m0PrimeDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire M0 Prime 3
 */
 
 if (societe.fusionGroup2.fusionAutreSociete2 or (representants.length > 7))
{
	var m0PrimeDoc1 = nash.doc //
		.load('models/cerfa_M0prime_SAS_creation.pdf') //
		.apply (formFieldsPers3);
	cerfaDoc1.append(m0PrimeDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire M0 Prime 4
 */
 
 if (representants.length > 11)
{
	var m0PrimeDoc1 = nash.doc //
		.load('models/cerfa_M0prime_SAS_creation.pdf') //
		.apply (formFieldsPers4);
	cerfaDoc1.append(m0PrimeDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 1
 */
 
 if (socialDirigeant1.voletSocialNumeroSecuriteSocialTNS != null) 
{
	var tnsDoc1 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers5);
	cerfaDoc1.append(tnsDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 2
 */
 
 if (socialDirigeant2.voletSocialNumeroSecuriteSocialTNS != null) 
{
	var tnsDoc1 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers6);
	cerfaDoc1.append(tnsDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 3
 */
 
 if (socialDirigeant3.voletSocialNumeroSecuriteSocialTNS != null) 
{
	var tnsDoc1 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers7);
	cerfaDoc1.append(tnsDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 4
 */
 
 if (socialDirigeant4.voletSocialNumeroSecuriteSocialTNS != null) 
{
	var tnsDoc1 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers8);
	cerfaDoc1.append(tnsDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 5
 */
 
 if (socialDirigeant5.voletSocialNumeroSecuriteSocialTNS != null) 
{
	var tnsDoc1 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers9);
	cerfaDoc1.append(tnsDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 6
 */
 
 if (socialDirigeant6.voletSocialNumeroSecuriteSocialTNS != null) 
{
	var tnsDoc1 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers10);
	cerfaDoc1.append(tnsDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 7
 */
 
 if (socialDirigeant7.voletSocialNumeroSecuriteSocialTNS != null) 
{
	var tnsDoc1 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers11);
	cerfaDoc1.append(tnsDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 8
 */
 
 if (socialDirigeant8.voletSocialNumeroSecuriteSocialTNS != null) 
{
	var tnsDoc1 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers12);
	cerfaDoc1.append(tnsDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 9
 */
 
 if (socialDirigeant9.voletSocialNumeroSecuriteSocialTNS != null) 
{
	var tnsDoc1 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers13);
	cerfaDoc1.append(tnsDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 10
 */
 
 if (socialDirigeant10.voletSocialNumeroSecuriteSocialTNS != null) 
{
	var tnsDoc1 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers14);
	cerfaDoc1.append(tnsDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 11
 */
 
 if (socialDirigeant11.voletSocialNumeroSecuriteSocialTNS != null) 
{
	var tnsDoc1 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers15);
	cerfaDoc1.append(tnsDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 12
 */
 
 if (socialDirigeant12.voletSocialNumeroSecuriteSocialTNS != null) 
{
	var tnsDoc1 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers16);
	cerfaDoc1.append(tnsDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 13
 */
 
 if (socialDirigeant13.voletSocialNumeroSecuriteSocialTNS != null) 
{
	var tnsDoc1 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers17);
	cerfaDoc1.append(tnsDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 14
 */
 
 if (socialDirigeant14.voletSocialNumeroSecuriteSocialTNS != null) 
{
	var tnsDoc1 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers18);
	cerfaDoc1.append(tnsDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire JQPA 1
 */
 
 if (jqpa1.jqpaActiviteJqpaPP) 
{
	var jqpaDoc1 = nash.doc //
		.load('models/cerfa_14077-02_JQPA.pdf') //
		.apply (formFieldsPers19);
	cerfaDoc1.append(jqpaDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire JQPA 2
 */
 
 if (jqpa2.jqpaActiviteJqpaPP) 
{
	var jqpaDoc2 = nash.doc //
		.load('models/cerfa_14077-02_JQPA.pdf') //
		.apply (formFieldsPers20);
	cerfaDoc1.append(jqpaDoc2.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire JQPA 3
 */
 
 if (jqpa3.jqpaActiviteJqpaPP) 
{
	var jqpaDoc3 = nash.doc //
		.load('models/cerfa_14077-02_JQPA.pdf') //
		.apply (formFieldsPers21);
	cerfaDoc1.append(jqpaDoc3.save('cerfa.pdf'));
}
 
/*
 * Ajout de l'intercalaire NDI Volet 1
 */
 
 if (etablissement.etablissementNomDomaine != null or (siege.societeNomDomaine[0].length > 0)) {
	var ndiDoc1 = nash.doc //
		.load('models/cerfa_14943-01_NDI_volet1.pdf') //
		.apply (formFields);
	cerfaDoc1.append(ndiDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire NDI Volet 2
 */
 
if (etablissement.etablissementNomDomaine != null or (siege.societeNomDomaine[0].length > 0)) {
	var ndiDoc2 = nash.doc //
		.load('models/cerfa_14943-01_NDI_volet2.pdf') //
		.apply (formFields);
	cerfaDoc1.append(ndiDoc2.save('cerfa.pdf'));
}

var pjUser = [];
var metas = [];

function pushPjPreview(fld) {
	fld.forEach(function (elm) {
        pjUser.push(elm);
        metas.push({'name':'document', 'value': '/'+elm.getAbsolutePath()});
    });
}

/*
 * Ajout des PJs
 */
 
var pj=$m0SAS.cadre11SignatureGroup.cadre11Signature.soussigne;
 
// PJ Dirigeant personne physique 1

if((Value('id').of(dirigeant1.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant1.personneLieeQualite).eq('66')
	or Value('id').of(dirigeant1.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant1.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant1.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant1.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant1.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant1.personneLieeQualite).eq('63'))
	or ((Value('id').of(dirigeant1.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant1.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant1.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant1.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant1.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant1.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))
	and not Value('id').of(dirigeant1.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeant1.personneLieeQualite).eq('72')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant1);
	
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and (not dirigeant1.autreDirigeant 
	or Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant1'))) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant1Signataire);
	} else if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or (dirigeant1.autreDirigeant
	and not Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant1') 
	and Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal'))) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant1NonSignataire);
	}
}

// PJ Dirigeant personne physique 2

if((Value('id').of(dirigeant2.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant2.personneLieeQualite).eq('66')
	or Value('id').of(dirigeant2.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant2.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant2.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant2.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant2.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant2.personneLieeQualite).eq('63'))
	or ((Value('id').of(dirigeant2.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant2.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant2.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant2.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant2.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant2.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))
	and not Value('id').of(dirigeant2.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeant2.personneLieeQualite).eq('72')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant2);
		
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant2')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant2Signataire);
	} else if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or (not Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant2') 
	and Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal'))) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant2NonSignataire);
	}
}

// PJ Dirigeant personne physique 3

if((Value('id').of(dirigeant3.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant3.personneLieeQualite).eq('66')
	or Value('id').of(dirigeant3.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant3.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant3.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant3.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant3.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant3.personneLieeQualite).eq('63'))
	or ((Value('id').of(dirigeant3.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant3.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant3.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant3.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant3.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant3.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))
	and not Value('id').of(dirigeant3.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeant3.personneLieeQualite).eq('72')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant3);
		
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant3')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant3Signataire);
	} else if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or (not Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant3') 
	and Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal'))) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant3NonSignataire);
	}
}

// PJ Dirigeant personne physique 4

if((Value('id').of(dirigeant4.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant4.personneLieeQualite).eq('66')
	or Value('id').of(dirigeant4.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant4.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant4.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant4.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant4.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant4.personneLieeQualite).eq('63'))
	or ((Value('id').of(dirigeant4.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant4.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant4.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant4.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant4.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant4.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))
	and not Value('id').of(dirigeant4.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeant4.personneLieeQualite).eq('72')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant4);
		
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant4')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant4Signataire);
	} else if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or (not Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant4') 
	and Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal'))) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant4NonSignataire);
	}
}

// PJ Dirigeant personne physique 5

if((Value('id').of(dirigeant5.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant5.personneLieeQualite).eq('66')
	or Value('id').of(dirigeant5.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant5.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant5.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant5.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant5.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant5.personneLieeQualite).eq('63'))
	or ((Value('id').of(dirigeant5.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant5.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant5.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant5.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant5.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant5.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))
	and not Value('id').of(dirigeant5.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeant5.personneLieeQualite).eq('72')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant5);
		
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant5')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant5Signataire);
	} else if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or (not Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant5') 
	and Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal'))) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant5NonSignataire);
	}
}

// PJ Dirigeant personne physique 6

if((Value('id').of(dirigeant6.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant6.personneLieeQualite).eq('66')
	or Value('id').of(dirigeant6.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant6.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant6.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant6.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant6.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant6.personneLieeQualite).eq('63'))
	or ((Value('id').of(dirigeant6.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant6.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant6.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant6.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant6.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant6.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))
	and not Value('id').of(dirigeant6.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeant6.personneLieeQualite).eq('72')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant6);
		
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant6')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant6Signataire);
	} else if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or (not Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant6') 
	and Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal'))) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant6NonSignataire);
	}
}

// PJ Dirigeant personne physique 7

if((Value('id').of(dirigeant7.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant7.personneLieeQualite).eq('66')
	or Value('id').of(dirigeant7.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant7.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant7.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant7.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant7.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant7.personneLieeQualite).eq('63'))
	or ((Value('id').of(dirigeant7.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant7.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant7.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant7.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant7.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant7.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))
	and not Value('id').of(dirigeant7.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeant7.personneLieeQualite).eq('72')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant7);
		
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant7')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant7Signataire);
	} else if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or (not Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant7') 
	and Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal'))) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant7NonSignataire);
	}
}

// PJ Dirigeant personne physique 8

if((Value('id').of(dirigeant8.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant8.personneLieeQualite).eq('66')
	or Value('id').of(dirigeant8.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant8.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant8.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant8.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant8.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant8.personneLieeQualite).eq('63'))
	or ((Value('id').of(dirigeant8.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant8.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant8.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant8.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant8.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant8.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))
	and not Value('id').of(dirigeant8.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeant8.personneLieeQualite).eq('72')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant8);
		
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant8')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant8Signataire);
	} else if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or (not Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant8') 
	and Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal'))) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant8NonSignataire);
	}
}

// PJ Dirigeant personne physique 9

if((Value('id').of(dirigeant9.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant9.personneLieeQualite).eq('66')
	or Value('id').of(dirigeant9.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant9.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant9.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant9.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant9.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant9.personneLieeQualite).eq('63'))
	or ((Value('id').of(dirigeant9.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant9.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant9.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant9.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant9.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant9.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))
	and not Value('id').of(dirigeant9.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeant9.personneLieeQualite).eq('72')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant9);
		
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant9')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant9Signataire);
	} else if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or (not Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant9') 
	and Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal'))) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant9NonSignataire);
	}
}

// PJ Dirigeant personne physique 10

if((Value('id').of(dirigeant10.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant10.personneLieeQualite).eq('66')
	or Value('id').of(dirigeant10.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant10.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant10.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant10.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant10.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant10.personneLieeQualite).eq('63'))
	or ((Value('id').of(dirigeant10.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant10.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant10.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant10.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant10.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant10.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))
	and not Value('id').of(dirigeant10.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeant10.personneLieeQualite).eq('72')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant10);
		
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant10')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant10Signataire);
	} else if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or (not Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant10') 
	and Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal'))) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant10NonSignataire);
	}
}

// PJ Dirigeant personne physique 11

if((Value('id').of(dirigeant11.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant11.personneLieeQualite).eq('66')
	or Value('id').of(dirigeant11.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant11.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant11.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant11.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant11.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant11.personneLieeQualite).eq('63'))
	or ((Value('id').of(dirigeant11.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant11.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant11.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant11.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant11.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant11.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))
	and not Value('id').of(dirigeant11.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeant11.personneLieeQualite).eq('72')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant11);
		
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant11')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant11Signataire);
	} else if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or (not Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant11') 
	and Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal'))) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant11NonSignataire);
	}
}

// PJ Dirigeant personne physique 12

if((Value('id').of(dirigeant12.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant12.personneLieeQualite).eq('66')
	or Value('id').of(dirigeant12.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant12.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant12.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant12.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant12.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant12.personneLieeQualite).eq('63'))
	or ((Value('id').of(dirigeant12.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant12.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant12.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant12.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant12.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant12.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))
	and not Value('id').of(dirigeant12.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeant12.personneLieeQualite).eq('72')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant12);
	
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant12')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant12Signataire);
	} else if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or (not Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant12') 
	and Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal'))) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant12NonSignataire);
	}
}

// PJ Dirigeant personne physique 13

if((Value('id').of(dirigeant13.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant13.personneLieeQualite).eq('66')
	or Value('id').of(dirigeant13.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant13.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant13.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant13.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant13.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant13.personneLieeQualite).eq('63'))
	or ((Value('id').of(dirigeant13.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant13.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant13.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant13.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant13.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant13.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))
	and not Value('id').of(dirigeant13.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeant13.personneLieeQualite).eq('72')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant13);
		
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant13')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant13Signataire);
	} else if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or (not Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant13') 
	and Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal'))) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant13NonSignataire);
	}
}

// PJ Dirigeant personne physique 14

if((Value('id').of(dirigeant14.personnaliteJuridique).eq('personnePhysique')
	or (Value('id').of(dirigeant14.personneLieeQualite).eq('66')
	or Value('id').of(dirigeant14.personneLieeQualite).eq('51')
	or Value('id').of(dirigeant14.personneLieeQualite).eq('69')
	or Value('id').of(dirigeant14.personneLieeQualite).eq('60')
	or Value('id').of(dirigeant14.personneLieeQualite).eq('61')
	or Value('id').of(dirigeant14.personneLieeQualite).eq('52')
	or Value('id').of(dirigeant14.personneLieeQualite).eq('63'))
	or ((Value('id').of(dirigeant14.personneLieeQualite).eq('73')
	or Value('id').of(dirigeant14.personneLieeQualite).eq('53')
	or Value('id').of(dirigeant14.personneLieeQualite).eq('70')
	or Value('id').of(dirigeant14.personneLieeQualite).eq('30'))
	and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
	or ((Value('id').of(dirigeant14.personneLieeQualite).eq('28')
	or Value('id').of(dirigeant14.personneLieeQualite).eq('74'))
	and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))
	and not Value('id').of(dirigeant14.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeant14.personneLieeQualite).eq('72')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant14);
		
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant14')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant14Signataire);
	} else if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or (not Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant14') 
	and Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal'))) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant14NonSignataire);
	}
}

// PJ Dirigeant Personne Morale 1 

if(Value('id').of(dirigeant1.personnaliteJuridique).eq('personneMorale') 
	and not Value('id').of(dirigeant1.personneLieeQualite).eq('71') 
	and not Value('id').of(dirigeant1.personneLieeQualite).eq('72'))	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS1);
	
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and (not dirigeant1.autreDirigeant 
	or Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant1'))) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire1);
	}
}

// PJ Dirigeant Personne Morale 2 

if(Value('id').of(dirigeant2.personnaliteJuridique).eq('personneMorale') 
	and not Value('id').of(dirigeant2.personneLieeQualite).eq('71') 
	and not Value('id').of(dirigeant2.personneLieeQualite).eq('72'))	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS2);

	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant2')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire2);
	}
}

// PJ Dirigeant Personne Morale 3 

if(Value('id').of(dirigeant3.personnaliteJuridique).eq('personneMorale') 
	and not Value('id').of(dirigeant3.personneLieeQualite).eq('71') 
	and not Value('id').of(dirigeant3.personneLieeQualite).eq('72'))	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS3);
	
	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant3')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire3);
	}
}

// PJ Dirigeant Personne Morale 4 

if(Value('id').of(dirigeant4.personnaliteJuridique).eq('personneMorale') 
	and not Value('id').of(dirigeant4.personneLieeQualite).eq('71') 
	and not Value('id').of(dirigeant4.personneLieeQualite).eq('72'))	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS4);

	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant4')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire4);
	}
}

// PJ Dirigeant Personne Morale 5 

if(Value('id').of(dirigeant5.personnaliteJuridique).eq('personneMorale') 
	and not Value('id').of(dirigeant5.personneLieeQualite).eq('71') 
	and not Value('id').of(dirigeant5.personneLieeQualite).eq('72'))	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS5);

	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant5')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire5);
	}
}

// PJ Dirigeant Personne Morale 6 

if(Value('id').of(dirigeant6.personnaliteJuridique).eq('personneMorale') 
	and not Value('id').of(dirigeant6.personneLieeQualite).eq('71') 
	and not Value('id').of(dirigeant6.personneLieeQualite).eq('72'))	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS6);

	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant6')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire6);
	}
}

// PJ Dirigeant Personne Morale 7 

if(Value('id').of(dirigeant7.personnaliteJuridique).eq('personneMorale') 
	and not Value('id').of(dirigeant7.personneLieeQualite).eq('71') 
	and not Value('id').of(dirigeant7.personneLieeQualite).eq('72'))	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS7);

	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant7')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire7);
	}
}

// PJ Dirigeant Personne Morale 8 

if(Value('id').of(dirigeant8.personnaliteJuridique).eq('personneMorale') 
	and not Value('id').of(dirigeant8.personneLieeQualite).eq('71') 
	and not Value('id').of(dirigeant8.personneLieeQualite).eq('72'))	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS8);

	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant8')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire8);
	}
}

// PJ Dirigeant Personne Morale 9 

if(Value('id').of(dirigeant9.personnaliteJuridique).eq('personneMorale') 
	and not Value('id').of(dirigeant9.personneLieeQualite).eq('71') 
	and not Value('id').of(dirigeant9.personneLieeQualite).eq('72'))	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS9);

	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant9')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire9);
	}
}

// PJ Dirigeant Personne Morale 10 

if(Value('id').of(dirigeant10.personnaliteJuridique).eq('personneMorale') 
	and not Value('id').of(dirigeant10.personneLieeQualite).eq('71') 
	and not Value('id').of(dirigeant10.personneLieeQualite).eq('72'))	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS10);

	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant10')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire10);
	}
}

// PJ Dirigeant Personne Morale 11 

if(Value('id').of(dirigeant11.personnaliteJuridique).eq('personneMorale') 
	and not Value('id').of(dirigeant11.personneLieeQualite).eq('71') 
	and not Value('id').of(dirigeant11.personneLieeQualite).eq('72'))	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS11);

	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant11')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire11);
	}
}

// PJ Dirigeant Personne Morale 12 

if(Value('id').of(dirigeant12.personnaliteJuridique).eq('personneMorale') 
	and not Value('id').of(dirigeant12.personneLieeQualite).eq('71') 
	and not Value('id').of(dirigeant12.personneLieeQualite).eq('72'))	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS12);

	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant12')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire12);
	}
}

// PJ Dirigeant Personne Morale 13 

if(Value('id').of(dirigeant13.personnaliteJuridique).eq('personneMorale') 
	and not Value('id').of(dirigeant13.personneLieeQualite).eq('71') 
	and not Value('id').of(dirigeant13.personneLieeQualite).eq('72'))	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS13);

	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant13')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire13);
	}
}

// PJ Dirigeant Personne Morale 14 

if(Value('id').of(dirigeant14.personnaliteJuridique).eq('personneMorale') 
	and not Value('id').of(dirigeant14.personneLieeQualite).eq('71') 
	and not Value('id').of(dirigeant14.personneLieeQualite).eq('72'))	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS14);

	if(Value('id').of(pj).eq('FormaliteSignataireQualiteRepresentantLegal') 
	and Value('id').of($m0SAS.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant14')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire14);
	}
}

// Nomination dirigeants

if(not dirigeant1.statutPresenceDirigeant 
	or (dirigeant1.autreDirigeant and not dirigeant2.statutPresenceDirigeant) 
	or (dirigeant2.autreDirigeant and not dirigeant3.statutPresenceDirigeant) 
	or (dirigeant3.autreDirigeant and not dirigeant4.statutPresenceDirigeant) 
	or (dirigeant4.autreDirigeant and not dirigeant5.statutPresenceDirigeant) 
	or (dirigeant5.autreDirigeant and not dirigeant6.statutPresenceDirigeant) 
	or (dirigeant6.autreDirigeant and not dirigeant7.statutPresenceDirigeant) 
	or (dirigeant7.autreDirigeant and not dirigeant8.statutPresenceDirigeant) 
	or (dirigeant8.autreDirigeant and not dirigeant9.statutPresenceDirigeant) 
	or (dirigeant9.autreDirigeant and not dirigeant10.statutPresenceDirigeant) 
	or (dirigeant10.autreDirigeant and not dirigeant11.statutPresenceDirigeant)
	or (dirigeant11.autreDirigeant and not dirigeant12.statutPresenceDirigeant) 
	or (dirigeant12.autreDirigeant and not dirigeant13.statutPresenceDirigeant) 
	or (dirigeant13.autreDirigeant and not dirigeant14.statutPresenceDirigeant)) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjNominationDirigeant);
}

// PJ Représentant Permanent 1

if(dirigeant1.civiliteRepresentant.personneLieePPNomNaissance != null){
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant1);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDRepresentant1NonSignataire);
}

// PJ Représentant Permanent 2

if(dirigeant2.civiliteRepresentant.personneLieePPNomNaissance != null){
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant2);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDRepresentant2NonSignataire);
}

// PJ Représentant Permanent 3

if(dirigeant3.civiliteRepresentant.personneLieePPNomNaissance != null){
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant3);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDRepresentant3NonSignataire);
}

// PJ Représentant Permanent 4

if(dirigeant4.civiliteRepresentant.personneLieePPNomNaissance != null){
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant4);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDRepresentant4NonSignataire);
}

// PJ Représentant Permanent 5

if(dirigeant5.civiliteRepresentant.personneLieePPNomNaissance != null){
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant5);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDRepresentant5NonSignataire);
}

// PJ Représentant Permanent 6

if(dirigeant6.civiliteRepresentant.personneLieePPNomNaissance != null){
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant6);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDRepresentant6NonSignataire);
}

// PJ Représentant Permanent 7

if(dirigeant7.civiliteRepresentant.personneLieePPNomNaissance != null){
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant7);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDRepresentant7NonSignataire);
}

// PJ Représentant Permanent 8

if(dirigeant8.civiliteRepresentant.personneLieePPNomNaissance != null){
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant8);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDRepresentant8NonSignataire);
}

// PJ Représentant Permanent 9

if(dirigeant9.civiliteRepresentant.personneLieePPNomNaissance != null){
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant9);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDRepresentant9NonSignataire);
}

// PJ Représentant Permanent 10

if(dirigeant10.civiliteRepresentant.personneLieePPNomNaissance != null){
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant10);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDRepresentant10NonSignataire);
}

// PJ Représentant Permanent 11

if(dirigeant11.civiliteRepresentant.personneLieePPNomNaissance != null){
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant11);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDRepresentant11NonSignataire);
}

// PJ Représentant Permanent 12

if(dirigeant12.civiliteRepresentant.personneLieePPNomNaissance != null){
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant12);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDRepresentant12NonSignataire);
}

// PJ Représentant Permanent 13

if(dirigeant13.civiliteRepresentant.personneLieePPNomNaissance != null){
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant13);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDRepresentant13NonSignataire);
}

// PJ Représentant Permanent 14

if(dirigeant14.civiliteRepresentant.personneLieePPNomNaissance != null){
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant14);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDRepresentant14NonSignataire);
}


// Pj CAC Titulaire 

if(Value('id').of(dirigeant1.personneLieeQualite).eq('71')
or Value('id').of(dirigeant2.personneLieeQualite).eq('71')
or Value('id').of(dirigeant3.personneLieeQualite).eq('71')
or Value('id').of(dirigeant4.personneLieeQualite).eq('71')
or Value('id').of(dirigeant5.personneLieeQualite).eq('71')
or Value('id').of(dirigeant6.personneLieeQualite).eq('71')
or Value('id').of(dirigeant7.personneLieeQualite).eq('71')
or Value('id').of(dirigeant8.personneLieeQualite).eq('71')
or Value('id').of(dirigeant9.personneLieeQualite).eq('71')
or Value('id').of(dirigeant10.personneLieeQualite).eq('71')
or Value('id').of(dirigeant11.personneLieeQualite).eq('71')
or Value('id').of(dirigeant12.personneLieeQualite).eq('71')
or Value('id').of(dirigeant13.personneLieeQualite).eq('71')
or Value('id').of(dirigeant14.personneLieeQualite).eq('71')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjJustificatifInscriptionCACTitulaire);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjLettreAcceptationCACTitulaire);
}

// Pj CAC Suppléant

if(Value('id').of(dirigeant1.personneLieeQualite).eq('72')
or Value('id').of(dirigeant2.personneLieeQualite).eq('72')
or Value('id').of(dirigeant3.personneLieeQualite).eq('72')
or Value('id').of(dirigeant4.personneLieeQualite).eq('72')
or Value('id').of(dirigeant5.personneLieeQualite).eq('72')
or Value('id').of(dirigeant6.personneLieeQualite).eq('72')
or Value('id').of(dirigeant7.personneLieeQualite).eq('72')
or Value('id').of(dirigeant8.personneLieeQualite).eq('72')
or Value('id').of(dirigeant9.personneLieeQualite).eq('72')
or Value('id').of(dirigeant10.personneLieeQualite).eq('72')
or Value('id').of(dirigeant11.personneLieeQualite).eq('72')
or Value('id').of(dirigeant12.personneLieeQualite).eq('72')
or Value('id').of(dirigeant13.personneLieeQualite).eq('72')
or Value('id').of(dirigeant14.personneLieeQualite).eq('72')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjJustificatifInscriptionCACSuppleant);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjLettreAcceptationCACSuppleant);
}

// PJ Mandataire

if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPouvoir);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire);
}

//PJ Société

if(not Value('id').of(societe.entrepriseFormeJuridique).eq('3110') and not Value('id').of(societe.entrepriseFormeJuridique).eq('3120') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5800')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjStatuts);

	if(not Value('id').of(societe.entrepriseFormeJuridique).eq('5202') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5203') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5306') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5307')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCertificatDepositaireFonds);
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCertificatListeSouscripteurs);
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjAnnonceLegaleConstitution);
		if (societe.entrepriseCapitalApportNature and ((not Value('id').of(societe.entrepriseFormeJuridique).eq('5720')
			and not Value('id').of(societe.entrepriseFormeJuridique).eq('5710') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5785'))
			or (societe.entrepriseCapitalApportMoitierCapital or societe.entrepriseCapitalApport30000))) {
			pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjRapportCAA);
		}
	}
	if(Value('id').of(societe.entrepriseFormeJuridique).eq('5202') and not Value('id').of(societe.entrepriseFormeJuridique).eq('5203') or Value('id').of(societe.entrepriseFormeJuridique).eq('5306') or Value('id').of(societe.entrepriseFormeJuridique).eq('5307')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjJournalConstitution);
	}
}	

if(Value('id').of(societe.entrepriseFormeJuridique).eq('3110') or Value('id').of(societe.entrepriseFormeJuridique).eq('3120') or Value('id').of(societe.entrepriseFormeJuridique).eq('5800')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjStatutsEtr);
}

// PJ Etablissement

// Création ou constitution sans activité

var origine = $m0SAS.cadre4OrigineFondsGroup.cadre4OrigineFonds;


if(Value('id').of(siege.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDomicile);
}

if(Value('id').of(siege.activiteLieu).eq('etablissementDomiciliationOui')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjContratDomiciliation);
}

if (Value('id').of(siege.activiteLieu).eq('etablissementDomiciliationNon')
 and (siege.etablissementPrincipalDifferentSiege or not siege.formaliteCreationAvecActivite 
 or (not siege.etablissementPrincipalDifferentSiege 
 and (Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsCreation') 
 or Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsCocheAutre')
 or ((Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsAchat')
 or Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsApport')) 
 and origine.precedentExploitant.fondsArtisanal)
 or Value('id').of(origine.etablissementOrigineFondsLiberal).eq('EtablissementOrigineFondsCreation')
 or Value('id').of(origine.etablissementOrigineFondsLiberal).eq('EtablissementOrigineFondsRepriseActiviteLiberale'))))
 and not Value('id').of(societe.entrepriseFormeJuridique).eq('3110') 
 and not Value('id').of(societe.entrepriseFormeJuridique).eq('3120') 
 and not Value('id').of(societe.entrepriseFormeJuridique).eq('5800')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjSocieteCreation);
}

if((siege.etablissementPrincipalDifferentSiege or Value('id').of(societe.entrepriseFormeJuridique).eq('3110') 
 or Value('id').of(societe.entrepriseFormeJuridique).eq('3120') 
 or Value('id').of(societe.entrepriseFormeJuridique).eq('5800'))
and (Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsCreation') 
 or Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsCocheAutre')
 or ((Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsAchat')
 or Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsApport')) 
 and origine.precedentExploitant.fondsArtisanal)
 or Value('id').of(origine.etablissementOrigineFondsLiberal).eq('EtablissementOrigineFondsCreation')
 or Value('id').of(origine.etablissementOrigineFondsLiberal).eq('EtablissementOrigineFondsRepriseActiviteLiberale'))) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjEtablissementCreation);
}

// Location-gérance

if (Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsLocationGerance')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjLocationGerance);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjLocationGerancePublication);
}

// Gérance-mandat

if(Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsGeranceMandat')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjGeranceMandat);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjGeranceMandatPublication);
}

// Achat

if(Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsAchat')
	and origine.precedentExploitant.cessionFonds) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPlanCession);
}

if(Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsAchat') 
	and origine.precedentExploitant.etablissementJournalAnnoncesLegalesNom != null) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjAchat);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjAchatPublication);
}

// Apport

if(Value('id').of(origine.etablissementOrigineFondsCommercial).eq('EtablissementOrigineFondsApport')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjApport);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjApportPublication);
}

// PJ JQPA

var jqpa1 = $m0SAS.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification1;
if(Value('id').of(jqpa1.jqpaSituation).eq('jqpaSituationDiplome')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDiplomes1);
}
if(Value('id').of(jqpa1.jqpaSituation).eq('jqpaSituationExperience')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles1);
}

var jqpa2 = $m0SAS.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification2;
if(Value('id').of(jqpa2.jqpaSituation).eq('jqpaSituationDiplome')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDiplomes2);
}
if(Value('id').of(jqpa2.jqpaSituation).eq('jqpaSituationExperience')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles2);
}

var jqpa3 = $m0SAS.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification3;
if(Value('id').of(jqpa3.jqpaSituation).eq('jqpaSituationDiplome')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDiplomes3);
}
if(Value('id').of(jqpa3.jqpaSituation).eq('jqpaSituationExperience')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles3);
}

// PJ MBE

pushPjPreview($attachmentPreprocess.attachmentPreprocess.formulaireBE1);

if (not Value('id').of(signataire.nombreMBE).eq('un')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.formulaireBE2);
}

if (not Value('id').of(signataire.nombreMBE).eq('un') and not Value('id').of(signataire.nombreMBE).eq('deux')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.formulaireBE3);
}

if (Value('id').of(signataire.nombreMBE).eq('quatre') or Value('id').of(signataire.nombreMBE).eq('cinq')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.formulaireBE4);
}

if (Value('id').of(signataire.nombreMBE).eq('cinq')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.formulaireBE5);
}


pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDActiviteReglementee);


/*
 * Enregistrement du fichier (en mémoire)
 */

var finalDoc = cerfaDoc1.save('M0_SAS_Creation.pdf');

//Remove old metas before insert
var metasToDelete = [];
var recordMetas = nash.record.meta() != null ? nash.record.meta().metas : null;
var recordMetasSize = recordMetas != null ? recordMetas.size() : 0;

for (var i = 0; i < recordMetasSize; i++) {
   if (recordMetas.get(i).name == 'document' && !recordMetas.get(i).value.contains("proxy")) {
       metasToDelete.push({'name':'document', 'value': recordMetas.get(i).value});
   }
}

nash.record.removeMeta(metasToDelete);
metas.push({'name':'document', 'value': '/3-review/generated/generated.record-1-M0_SAS_Creation.pdf'});
nash.record.meta(metas);
var data = [ spec.createData({
    id : 'record',
    label : 'Déclaration de création d\'une société (SAS, SA, SNC, SELAFA, SELAS, Commandite et société commerciale étrangère)',
    description : 'Voici le formulaire obtenu à partir des données saisies.',
    type : 'FileReadOnly',
    value : [ finalDoc ]
}),  spec.createData({
    id : 'attachments',
    label : 'Pièces jointes',
    description : 'Pièces jointes ajoutées au formulaire.',
    type : 'FileReadOnly',
    value : pjUser
}) ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Déclaration de création d\'une société (SAS, SA, SNC, SELAFA, SELAS, Commandite et société commerciale étrangère)',
    groups : groups
});