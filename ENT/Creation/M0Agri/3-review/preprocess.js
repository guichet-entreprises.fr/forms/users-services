function pad(s) {
    return (s < 10) ? '0' + s : s;
}
var formFields = {}; //m0agri
var formFieldsPers1 = {}; //M0'
var formFieldsNSm1 = {}; //Nsm 1
var formFieldsNSm2 = {};
var formFieldsNSm3 = {};
var formFieldsNSm4 = {};
var formFieldsNSm5 = {};
var formFieldsNSm6 = {};
var formFieldsNSm7 = {};
var formFieldsNSm8 = {};
var formFieldsNSm9 = {};
var formFieldsNSm10 = {}; //Nsm 10
var formFieldsNSp1 = {}; //Nsp 1
var formFieldsNSp2 = {};
var formFieldsNSp3 = {};
var formFieldsNSp4 = {};
var formFieldsNSp5 = {};
var formFieldsNSp6 = {};
var formFieldsNSp7 = {};
var formFieldsNSp8 = {};
var formFieldsNSp9 = {};
var formFieldsNSp10 = {}; //Nsp 10
var formFieldsNDI = {}; //NDI
var societe = $m0Agricole.cadreDeclarationPMGroup.cadreDeclarationPM;
var societeCommerciale = $m0Agricole.cadreComplementSocieteCommercialeGroup;
var etrangere = societeCommerciale.cadreSocieteEtrangere.cadreSocieteEtrangereRegistre;
var etabEtranger = societeCommerciale.cadreSocieteEtrangere.cadreSocieteEtrangereAutreEtab;
var siege = $m0Agricole.cadreDeclarationPMGroup.siegeAdresseEntreprise;
var activite = $m0Agricole.cadreDeclarationExploitationActiviteGroup.cadreDeclarationExploitationActivite;
var dirigeant1 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant1.personalite1;
var dirigeant2 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant2.personalite2;
var dirigeant3 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant3.personalite3;
var dirigeant4 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant4.personalite4;
var dirigeant5 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant5.personalite5;
var dirigeant6 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant6.personalite6;
var dirigeant7 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant7.personalite7;
var dirigeant8 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant8.personalite8;
var dirigeant9 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant9.personalite9;
var dirigeant10 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant10.personalite10;
var nsp1 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant1.aideFamilialNsp1;
var nsp2 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant2.aideFamilialNsp2;
var nsp3 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant3.aideFamilialNsp3;
var nsp4 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant4.aideFamilialNsp4;
var nsp5 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant5.aideFamilialNsp5;
var nsp6 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant6.aideFamilialNsp6;
var nsp7 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant7.aideFamilialNsp7;
var nsp8 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant8.aideFamilialNsp8;
var nsp9 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant9.aideFamilialNsp9;
var nsp10 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant10.aideFamilialNsp10;
var fiscal = $m0Agricole.cadreOptionsFiscalesGroup.cadreOptionsFiscales;
var cpt = $m0Agricole.cadre20RensCompGroup.cadre20RensComp;
var tag = $m0Agricole.cadre21SignatureGroup.cadre21Signature;

//Déclaration de constitution d'une société ayant une activité principale agricole - "M0 Agricole"
//Cadre 1
formFields['societeCivileEARL'] = Value('id').of(societe.entrepriseFormeJuridiqueCivile).eq('6598') ? true : false;
formFields['societeCivileGAEC'] = Value('id').of(societe.entrepriseFormeJuridiqueCivile).eq('6533') ? true : false;
formFields['societeCivileGFA'] = Value('id').of(societe.entrepriseFormeJuridiqueCivile).eq('6534') ? true : false;
formFields['societeCivileSCEA'] = Value('id').of(societe.entrepriseFormeJuridiqueCivile).eq('6597') ? true : false;
formFields['societeCivileAutre'] = Value('id').of(societe.entrepriseFormeJuridiqueCivile).eq('6599') ? true : false;
if (Value('id').of(societe.entrepriseFormeJuridiqueCivile).eq('6599')) {
    formFields['societeCivileAutreLibelle'] = societe.entrepriseSocieteAutre;
}
if (Value('id').of(societe.entrepriseFormeJuridiqueCommerciale).eq('5531') or Value('id').of(societe.entrepriseFormeJuridiqueCommerciale).eq('5532') or Value('id').of(societe.entrepriseFormeJuridiqueCommerciale).eq('5599') or Value('id').of(societe.entrepriseFormeJuridiqueCommerciale).eq('5699'))
{
formFields['societeCommercialeSA'] = true;
}	
//formFields['societeCommercialeSA'] = Value('id').of(societe.entrepriseFormeJuridiqueCommerciale).eq('societeCommercialeSA') ? true : false;
formFields['societeCommercialeSARL'] = Value('id').of(societe.entrepriseFormeJuridiqueCommerciale).eq('5499') ? true : false;
formFields['societeCommercialeSAS'] = Value('id').of(societe.entrepriseFormeJuridiqueCommerciale).eq('5710') ? true : false;
formFields['societeCommercialeSNC'] = Value('id').of(societe.entrepriseFormeJuridiqueCommerciale).eq('5202') ? true : false; 
formFields['societeCommercialeAutre'] = Value('id').of(societe.entrepriseFormeJuridiqueCommerciale).eq('9900') ? true : false;
if (Value('id').of(societe.entrepriseFormeJuridiqueCommerciale).eq('9900')) {
    formFields['societeCommercialeAutreLibelle'] = societe.entrepriseSocieteAutre;
}
formFields['entrepriseSocieteConstitueSansActivite'] = societe.entrepriseSocieteConstitueSansActivite ? true : false;
formFields['entrepriseSocieteCommercialeEtrangereOuverturePremierEtab'] = societe.entrepriseSocieteCommercialeEtrangereOuverturePremierEtab ? true : false;
// Déclaration relative à la personne morale
// Cadre 2
formFields['entrepriseDenomination'] = societe.entrepriseDenomination;
formFields['entrepriseSigle'] = societe.entrepriseSigle;
if (societe.entrepriseSocieteAutre != null) {
    formFields['entrepriseFormeJuridiqueConstitution'] = societe.entrepriseSocieteAutre;
} else if (Value('id').of(societe.entrepriseFormeJuridiqueConstitution).eq('societeCivile')) {
    formFields['entrepriseFormeJuridiqueConstitution'] = societe.entrepriseFormeJuridiqueCivile;
} else {
    formFields['entrepriseFormeJuridiqueConstitution'] = societe.entrepriseFormeJuridiqueCommerciale;
}
formFields['entrepriseAssocieUnique'] = societe.entrepriseAssocieUnique != false ? true : false;
formFields['entrepriseDuree'] = societe.entrepriseDuree;
formFields['entrepriseCapitalMontant'] = societe.entrepriseCapitalMontant != null ? (societe.entrepriseCapitalMontant + ' ' + societe.entrepriseCapitalDevise) : '';
formFields['entrepriseCapitalVariableMinimum'] = societe.entrepriseCapitalVariableMinimum != null ? (societe.entrepriseCapitalVariableMinimum + ' ' + societe.entrepriseCapitalDevise) : '';
// Cadre 3
formFields['entrepriseFusionScission'] = societe.entrepriseFusionScission;
// Cadre 4 - Adresse du siège
formFields['siegeAdresseEntreprise'] = (siege.siegeAdresseNumeroVoie != null ? siege.siegeAdresseNumeroVoie : ' ') + ' ' +
(siege.siegeAdresseIndiceVoie != null ? siege.siegeAdresseIndiceVoie : ' ') + ' ' +
(siege.siegeAdresseTypeVoie != null ? siege.siegeAdresseTypeVoie : ' ') + ' ' +
siege.siegeAdresseNomVoie + ' ' +
(siege.siegeAdresseComplementVoie != null ? siege.siegeAdresseComplementVoie : '') + ' ' +
(siege.siegeADresseDistributionSpeciale != null ? siege.siegeADresseDistributionSpeciale : '');
formFields['siegeAdresseCodePostal'] = siege.siegeAdresseCodePostalEtranger != null ? siege.siegeAdresseCodePostalEtranger : siege.siegeAdresseCodePostal;
formFields['siegeAdresseCommunePays'] = siege.siegeAdresseVille != null ? (siege.siegeAdresseVille + ' / ' + siege.siegeAdressePays) : (siege.siegeAdresseCommune + ' / ' + siege.siegeAdressePays);
formFields['siegeAdresseCommuneAncienne'] = siege.siegeAdresseCommuneAncienne != null ? siege.siegeAdresseCommuneAncienne : '';
// Cadre 5 - Principales activités de l'objet social
formFields['activiteObjetSocial'] = $m0Agricole.cadreDeclarationPMGroup.principaleActiviteObjetSocial.activiteObjetSocial;
//SOCIETES CIVILES
// cadre 6
if (societe.entrepriseSocieteCivileGAECDateAgrement != null) {
    var dateTmp = new Date(parseInt(societe.entrepriseSocieteCivileGAECDateAgrement.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
    date = date.concat(dateTmp.getFullYear().toString());
    formFields['entrepriseSocieteCivileGAECDateAgrement'] = date;
}
//SOCIETES COMMERCIALES
// cadre 7
formFields['statutLegalParticulier'] = societeCommerciale.cadreInfosCommunes.statutLegalParticulier != null ? societeCommerciale.cadreInfosCommunes.statutLegalParticulier : '';
if (societeCommerciale.cadreInfosCommunes.dateClotureExerciceM0AC != null) {
    var dateTmp = new Date(parseInt(societeCommerciale.cadreInfosCommunes.dateClotureExerciceM0AC.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
    date = date.concat(dateTmp.getFullYear().toString());
    formFields['dateClotureExerciceM0AC'] = date;
}
if (societeCommerciale.cadreInfosCommunes.dateClotureExerciceSiDifferenteM0AC != null) {
    var dateTmp = new Date(parseInt(societeCommerciale.cadreInfosCommunes.dateClotureExerciceSiDifferenteM0AC.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
    date = date.concat(dateTmp.getFullYear().toString());
    formFields['dateClotureExerciceSiDifferenteM0AC'] = date;
}
formFields['adhesionESSM0AC'] = societeCommerciale.cadreInfosCommunes.adhesionESSM0AC ? true : false;
// cadre 8A
if (Value('id').of(societe.entrepriseFormeJuridiqueCommerciale).eq('5499') and societe.entrepriseAssocieUnique) {
    formFields['entrepriseAssocieUniqueGerant'] = societe.entrepriseAssocieUniqueGerant ? true : false;
    formFields['entrepriseStatutTypeSansModification'] = Value('id').of(societe.entrepriseStatutType).eq('entrepriseStatutTypeSansModification') ? true : false;
    formFields['entrepriseStatutTypeDifferents'] = Value('id').of(societe.entrepriseStatutType).eq('entrepriseStatutTypeDifferents') ? true : false;
}
formFields['entrepriseAssocieUnique'] = (Value('id').of(societe.entrepriseFormeJuridiqueCommerciale).eq('5710') and societe.entrepriseAssocieUnique) ? true : false;
formFields['entrepriseAssocieUniquePresident'] = societe.entrepriseAssocieUniquePresident ? true : false;

// cadre 8B
if (Value('id').of(societe.entrepriseFormeJuridiqueCommerciale).eq('5499')) {
    formFields['geranceMajoritaireSARLM0AC'] = Value('id').of(societeCommerciale.cadreUniquementSarl.geranceSARL).eq('geranceMajoritaireSARLM0AC') ? true : false;
    formFields['geranceMinoritaireSARLM0AC'] = Value('id').of(societeCommerciale.cadreUniquementSarl.geranceSARL).eq('geranceMinoritaireSARLM0AC') ? true : false;
    formFields['geranceTiersSARLM0AC'] = Value('id').of(societeCommerciale.cadreUniquementSarl.geranceSARL).eq('geranceTiersSARLM0AC') ? true : false;
    formFields['geranceMinoritaireAssocieOUISARLM0AC'] = societeCommerciale.cadreUniquementSarl.geranceMinoritaireAssocieOUINONSARLM0AC ? true : false;
    formFields['geranceMinoritaireAssocieNONSARLM0AC'] = societeCommerciale.cadreUniquementSarl.geranceMinoritaireAssocieOUINONSARLM0AC ? false : true;
}
// cadre 9 - Sociétés étrangères ayant une activité agricole
formFields['registrePublicSiegeLieuPaysM0AC'] = etrangere.registrePublicSiegeLieuM0AC != null ? etrangere.registrePublicSiegeLieuM0AC + ' / ' + etrangere.registrePublicSiegePaysM0AC : '';
formFields['registrePublicSiegeNumeroImmatriculationM0AC'] = etrangere.registrePublicSiegeNumeroImmatriculationM0AC != null ? etrangere.registrePublicSiegeNumeroImmatriculationM0AC : '';
/* formFields['adresseEtablissementAgricoleFranceM0AC'] =
    (etrangere.adresseEtablissementAgricoleFranceVoieM0AC != null ? etrangere.adresseEtablissementAgricoleFranceVoieM0AC : '') + ' ' +
(etrangere.adresseEtablissementAgricoleFranceIndiceVoieM0AC != null ? etrangere.adresseEtablissementAgricoleFranceIndiceVoieM0AC : '') + ' ' +
(etrangere.adresseEtablissementAgricoleFranceTypeVoieM0AC != null ? etrangere.adresseEtablissementAgricoleFranceTypeVoieM0AC : '') + ' ' +
etrangere.adresseEtablissementAgricoleFranceNomVoieM0AC + ' ' +
(etrangere.adresseEtablissementAgricoleFranceComplementVoieM0AC != null ? etrangere.adresseEtablissementAgricoleFranceComplementVoieM0AC : '') + ' ' +
(etrangere.adresseEtablissementAgricoleFranceDistributionSpecialeVoieM0AC != null ? etrangere.adresseEtablissementAgricoleFranceDistributionSpecialeVoieM0AC : '');
formFields['adresseEtablissementAgricoleFranceCodePostalM0AC'] = etrangere.adresseEtablissementAgricoleFranceCodePostalM0AC != null ? etrangere.adresseEtablissementAgricoleFranceCodePostalM0AC : '';
formFields['adresseEtablissementAgricoleFranceCommuneM0AC'] = etrangere.adresseEtablissementAgricoleFranceCommuneM0AC != null ? etrangere.adresseEtablissementAgricoleFranceCommuneM0AC : ''; */
// cadre 10 - Autres établissements situées dans un état membre de l'UE
formFields['adresseEtablissementLieu'] = etabEtranger.adresseEtablissementLieu;
formFields['adresseEtablissementNumeroImmatriculation'] = etabEtranger.adresseEtablissementNumeroImmatriculation;
formFields['adresseEtablissementfacultatifActivite'] = etabEtranger.adresseEtablissementfacultatifActivite != null ? etabEtranger.adresseEtablissementfacultatifActivite : '';
formFields['adresseEtablissementfacultatifAdresse'] =
    (etabEtranger.adresseEtablissementfacultatifAdresse1 != null ? etabEtranger.adresseEtablissementfacultatifAdresse1 : '') + ' ' +
(etabEtranger.adresseEtablissementfacultatifAdresse2 != null ? etabEtranger.adresseEtablissementfacultatifAdresse2 : '') + ' ' +
(etabEtranger.adresseEtablissementfacultatifAdresse3 != null ? etabEtranger.adresseEtablissementfacultatifAdresse3 : '') + ' ' +
(etabEtranger.adresseEtablissementfacultatifAdresse4 != null ? etabEtranger.adresseEtablissementfacultatifAdresse4 : '');

//DÉCLARATION RELATIVE A L'EXPLOITATION ET A L'ACTIVITÉ
// cadre 11 - Adresse de l'exploitation
formFields['adresseExploitation'] = activite.adresseExploitationDifferente ? (
    (activite.adresseExploitation.adresseExploitationNumeroVoie != null ? activite.adresseExploitation.adresseExploitationNumeroVoie : '') + ' ' +
(activite.adresseExploitation.adresseExploitationIndiceVoie != null ? activite.adresseExploitation.adresseExploitationIndiceVoie : '') + ' ' +
(activite.adresseExploitation.adresseExploitationTypeVoie != null ? activite.adresseExploitation.adresseExploitationTypeVoie : '') + ' ' +
(activite.adresseExploitation.adresseExploitationNomVoie != null ? activite.adresseExploitation.adresseExploitationNomVoie : '') + ' ' +
(activite.adresseExploitation.adresseExploitationComplementVoie != null ? activite.adresseExploitation.adresseExploitationComplementVoie : '') + ' ' +
(activite.adresseExploitation.adresseExploitationDistributionSpecialeVoie != null ? activite.adresseExploitation.adresseExploitationDistributionSpecialeVoie : '')) :
((siege.siegeAdresseNumeroVoie != null ? siege.siegeAdresseNumeroVoie : ' ') + ' ' +
(siege.siegeAdresseIndiceVoie != null ? siege.siegeAdresseIndiceVoie : ' ') + ' ' +
(siege.siegeAdresseTypeVoie != null ? siege.siegeAdresseTypeVoie : ' ') + ' ' + siege.siegeAdresseNomVoie + ' ' +
(siege.siegeAdresseComplementVoie != null ? siege.siegeAdresseComplementVoie : '') + ' ' +
(siege.siegeADresseDistributionSpeciale != null ? siege.siegeADresseDistributionSpeciale : ''));
formFields['adresseExploitationCodePostal'] = activite.adresseExploitationDifferente ? activite.adresseExploitation.adresseExploitationCodePostal  : (siege.siegeAdresseCodePostalEtranger != null ? siege.siegeAdresseCodePostalEtranger : siege.siegeAdresseCodePostal);
formFields['adresseExploitationCommune'] = activite.adresseExploitationDifferente ? activite.adresseExploitation.adresseExploitationCommune : (siege.siegeAdresseVille != null ? (siege.siegeAdresseVille + ' / ' + siege.siegeAdressePays) : (siege.siegeAdresseCommune + ' / ' + siege.siegeAdressePays));

// cadre 12 - Nom de l'exploitation
formFields['cadreNomExploitation'] = activite.cadreNomExploitation.etablissementNomExploitation;
// cadre 13 - Effectif salarié
formFields['etablissementEffectifSalariePresenceNon'] = activite.cadreEffectifSalarie.etablissementEffectifSalariePresenceOui != false ? false : true;
formFields['etablissementEffectifSalariePresenceOui'] = activite.cadreEffectifSalarie.etablissementEffectifSalariePresenceOui != false ? true : false;
formFields['etablissementEffectifSalarieAgricoleNombre'] = activite.cadreEffectifSalarie.etablissementEffectifSalarieAgricoleNombre;
formFields['etablissementEffectifSalarieEmbauchePremierSalarieOui'] = activite.cadreEffectifSalarie.etablissementEffectifSalarieEmbauchePremierSalarieOui != false ? true : false;
formFields['etablissementEffectifSalarieEmbauchePremierSalarieNon'] = activite.cadreEffectifSalarie.etablissementEffectifSalarieEmbauchePremierSalarieOui != false ? false : true;

// Cadre 14 Origine de l'activité
formFields['etablissementOrigineCreation'] = Value('id').of(activite.cadreOrigineActivite.etablisementOrigineActivite).eq('etablissementOrigineCreation') ? true : false;
formFields['etablissementOrigineRepriseTotalePartielle'] = Value('id').of(activite.cadreOrigineActivite.etablisementOrigineActivite).eq('etablissementOrigineRepriseTotalePartielle') ? true : false;
formFields['etablissementOrigineAutre'] = Value('id').of(activite.cadreOrigineActivite.etablisementOrigineActivite).eq('etablissementOrigineAutre') ? true : false;

// Cadre 14 Origine de l'activité - Précédent exploitant
formFields['entrepriseLieeSirenPrecedentExploitant'] = (activite.cadreOrigineActivite.precedentExploitant.entrepriseLieeSirenPrecedentExploitant != null ? activite.cadreOrigineActivite.precedentExploitant.entrepriseLieeSirenPrecedentExploitant.split(' ').join('') : '');
formFields['entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant'] = activite.cadreOrigineActivite.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant;
formFields['entrepriseLieeEntreprisePPNomUsagePrecedentExploitant'] = activite.cadreOrigineActivite.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant;
formFields['entrepriseLiee_entreprisePP_prenom_precedentExploitant'] = activite.cadreOrigineActivite.precedentExploitant.entrepriseLieeEntreprisePPPrenomPrecedentExploitant;
formFields['entrepriseLieeEntreprisePPDenominationPrecedentExploitant'] = activite.cadreOrigineActivite.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant;
formFields['entrepriseLieeNumeroDetenteurPrecedentExploitant'] = activite.cadreOrigineActivite.precedentExploitant.entrepriseLieeNumeroDetenteurPrecedentExploitant;
formFields['entrepriseLieeNumeroExploitationPrecedentExploitant'] = activite.cadreOrigineActivite.precedentExploitant.entrepriseLieeNumeroExploitationPrecedentExploitant;

// Cadre 15 Informations sur les Activités
if (activite.informationActivite.etablissementDateDebutActivite != null) {
    var dateTmp = new Date(parseInt(activite.informationActivite.etablissementDateDebutActivite.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
    date = date.concat(dateTmp.getFullYear().toString());
    formFields['etablissementDateDebutActivite'] = date;
}
// Cadre 15 Activités Culture : questionActiviteCulture
formFields['etablissementActivitePlusImportanteAgricoleCereales'] = Value('id').of(activite.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleCereales') ? true : false;
formFields['etablissementActivitePlusImportanteAgricoleRiz'] = Value('id').of(activite.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleRiz') ? true : false; 
formFields['etablissementActivitePlusImportanteAgricoleLegumes'] = Value('id').of(activite.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleLegumes')  ? true : false;
formFields['etablissementActivitePlusImportanteAgricoleCanneSucre'] = Value('id').of(activite.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleCanneSucre') ? true : false;
formFields['etablissementActivitePlusImportanteAgricoleTabac'] = Value('id').of(activite.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleTabac') ? true : false;
formFields['etablissementActivitePlusImportanteAgricolePlantesFibres'] = Value('id').of(activite.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricolePlantesFibres') ? true : false;
formFields['etablissementActivitePlusImportanteAgricoleVigne'] = Value('id').of(activite.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleVigne')  ? true : false;
formFields['etablissement_activitePluetablissementActivitePlusImportanteAgricoleFruitsTropicauxsImportanteAgricole_fruitsTropicaux'] = Value('id').of(activite.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleFruitsTropicaux')  ? true : false;
formFields['etablissementActivitePlusImportanteAgricoleAgrumes'] = Value('id').of(activite.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleAgrumes') ? true : false;
formFields['etablissementActivitePlusImportanteAgricoleFruitsPepins'] = Value('id').of(activite.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleFruitsPepins')  ? true : false;
formFields['etablissementActivitePlusImportanteAgricoleFruitsOleagineux'] = Value('id').of(activite.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleFruitsOleagineux') ? true : false;
formFields['etablissementActivitePlusImportanteAgricoleAutresFruitsArbres'] = Value('id').of(activite.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleAutresFruitsArbres') ? true : false;
formFields['etablissementActivitePlusImportanteAgricolePlantesBoisson'] = Value('id').of(activite.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricolePlantesBoisson')  ? true : false;
formFields['etablissementActivitePlusImportanteAgricolePlantesEpicesAromatiques'] = Value('id').of(activite.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricolePlantesEpicesAromatiques')  ? true : false;
formFields['etablissementActivitePlusImportanteAgricoleAutresCulturesPermanentes'] = Value('id').of(activite.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleAutresCulturesPermanentes') ? true : false;
formFields['etablissementActivitePlusImportanteAgricoleAutresPreciser1'] = activite.informationActivite.etablissementActivitePlusImportanteAgricoleAutresPreciser;
// Cadre 15 Activités Elevage : questionActiviteElevage
formFields['etablissementActivitePlusImportanteAgricoleVachesLaitieres'] = Value('id').of(activite.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricoleVachesLaitieres') ? true : false;
formFields['etablissementActivitePlusImportanteAgricoleChevaux'] = Value('id').of(activite.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricoleChevaux')  ? true : false;
formFields['etablissementActivitePlusImportanteAgricoleChameaux'] = Value('id').of(activite.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricoleChameaux')  ? true : false;
formFields['etablissementActivitePlusImportanteAgricolePorcins'] = Value('id').of(activite.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricolePorcins') ? true : false;
formFields['etablissementActivitePlusImportanteAgricoleAquacultureMer'] = Value('id').of(activite.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricoleAquacultureMer') ? true : false;
formFields['etablissementActivitePlusImportanteAgricoleAquacultureEauDouce'] = Value('id').of(activite.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricoleAquacultureEauDouce')  ? true : false;
formFields['etablissementActivitePlusImportanteAgricoleAutresAnimaux'] = Value('id').of(activite.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricoleAutresAnimaux') ? true : false;
//oubli
formFields['etablissementActivitePlusImportanteAgricoleOvinsCaprins'] = Value('id').of(activite.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricoleOvinsCaprins') ? true : false;
formFields['etablissementActivitePlusImportanteAgricoleVolailles'] = Value('id').of(activite.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricoleVolailles') ? true : false;
formFields['etablissementActivitePlusImportanteAgricoleAutresBovins'] = Value('id').of(activite.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricoleAutresBovins') ? true : false;
//fin oubli
formFields['etablissementActivitePlusImportanteAgricoleAutresPreciser2'] = activite.informationActivite.etablissementActivitePlusImportanteAgricoleAutresPreciser;

// Cadre 15 Activités Autres : questionActiviteAutres
formFields['etablissementActivitePlusImportanteAgricoleCultureElevageAssocies'] = Value('id').of(activite.informationActivite.questionActiviteAutres).eq('etablissementActivitePlusImportanteAgricoleCultureElevageAssocies')  ? true : false;
formFields['etablissementActivitePlusImportanteAgricoleActivitesPepinieres'] = Value('id').of(activite.informationActivite.questionActiviteAutres).eq('etablissementActivitePlusImportanteAgricoleActivitesPepinieres')  ? true : false;
formFields['etablissementActivitePlusImportantgeAgricoleSylviculture'] = Value('id').of(activite.informationActivite.questionActiviteAutres).eq('etablissementActivitePlusImportantgeAgricoleSylviculture')  ? true : false;
formFields['etablissementActivitePlusImportanteAricoleAutre'] = Value('id').of(activite.informationActivite.questionActiviteAutres).eq('etablissementActivitePlusImportanteAricoleAutre') ? true : false;
formFields['etablissementActivitePlusImportanteAgricoleAutresPreciser3'] = activite.informationActivite.etablissementActivitePlusImportanteAgricoleAutresPreciser;

// Cadre 15 Activités complémantaires 
formFields['activiteSecondaire'] = activite.informationActivite.activiteSecondaire != null ? activite.informationActivite.activiteSecondaire : '';

// Cadre 16 Dirigeant 1
formFields['personneLieeQualite[0]'] = dirigeant1.personneLieeQualite;
formFields['personneLieePPNomNaissance[0]'] = dirigeant1.civilitePersonnePhysique1.personneLieePPNomNaissance != null ? dirigeant1.civilitePersonnePhysique1.personneLieePPNomNaissance : (dirigeant1.civiliteRepresentant1.personneLieePPNomNaissance != null ? dirigeant1.civiliteRepresentant1.personneLieePPNomNaissance : '');
formFields['personneLieePPNomUsage[0]'] = dirigeant1.civilitePersonnePhysique1.personneLieePPNomUsage != null ? dirigeant1.civilitePersonnePhysique1.personneLieePPNomUsage : (dirigeant1.civiliteRepresentant1.personneLieePPNomUsage != null ? dirigeant1.civiliteRepresentant1.personneLieePPNomUsage : '');
if (dirigeant1.civilitePersonnePhysique1.personneLieePPPrenom != null and dirigeant1.civilitePersonnePhysique1.personneLieePPPrenom[0].length > 0) {
    var prenoms = [];
    for (i = 0; i < dirigeant1.civilitePersonnePhysique1.personneLieePPPrenom.size(); i++) {
        prenoms.push(dirigeant1.civilitePersonnePhysique1.personneLieePPPrenom[i]);
    }
    formFields['personneLieePPPrenom[0]'] = prenoms.toString();
} else if (dirigeant1.civiliteRepresentant1.personneLieePPPrenom != null ) {
    var prenoms = [];
    for (i = 0; i < dirigeant1.civiliteRepresentant1.personneLieePPPrenom.size(); i++) {
        prenoms.push(dirigeant1.civiliteRepresentant1.personneLieePPPrenom[i]);
    }
    formFields['personneLieePPPrenom[0]'] = prenoms.toString();
}

if (dirigeant1.civilitePersonnePhysique1.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant1.civilitePersonnePhysique1.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFields['personneLieePPDateNaissance[0]'] = date1;
} else if (dirigeant1.civiliteRepresentant1.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant1.civiliteRepresentant1.personneLieePPDateNaissance.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
    date2 = date2.concat(dateTmp.getFullYear().toString());
    formFields['personneLieePPDateNaissance[0]'] = date2;
}
formFields['personneLieePPLieuNaissance[0]'] = dirigeant1.civilitePersonnePhysique1.personneLieePPLieuNaissanceCommune != null ? (dirigeant1.civilitePersonnePhysique1.personneLieePPLieuNaissanceCommune + ' ' + "(" + '' + dirigeant1.civilitePersonnePhysique1.personneLieePPDepartementNaissance.getId() + '' + ")") : (dirigeant1.civiliteRepresentant1.personneLieePPLieuNaissanceCommune != null ? (dirigeant1.civiliteRepresentant1.personneLieePPLieuNaissanceCommune + ' ' + "(" + '' + dirigeant1.civiliteRepresentant1.personneLieePPDepartementNaissance.getId() + '' + ")") : (dirigeant1.civilitePersonnePhysique1.personneLieePPLieuNaissanceVille != null ? (dirigeant1.civilitePersonnePhysique1.personneLieePPLieuNaissanceVille + ' / ' + dirigeant1.civilitePersonnePhysique1.personneLieePPLieuNaissancePays) : (dirigeant1.civiliteRepresentant1.personneLieePPLieuNaissanceVille + ' / ' + dirigeant1.civiliteRepresentant1.personneLieePPLieuNaissancePays)));
formFields['personneLieePPNationalite[0]'] = dirigeant1.civilitePersonnePhysique1.personneLieePPNationalite != null ? dirigeant1.civilitePersonnePhysique1.personneLieePPNationalite : dirigeant1.civiliteRepresentant1.personneLieePPNationalite;
formFields['genreMasculin[0]'] = Value('id').of(dirigeant1.civilitePersonnePhysique1.personnaliteGenre).eq('genreMasculin') ? true : false;
formFields['genreFeminin[0]']  = Value('id').of(dirigeant1.civilitePersonnePhysique1.personnaliteGenre).eq('genreFeminin')  ? true : false;
formFields['dirigeantMajoritaireCapitalSocial[0]'] = dirigeant1.civilitePersonnePhysique1.dirigeantMajoritaireCapitalSocial != false ? true : false;
if (Value('id').of(dirigeant1.personnaliteJuridique).eq('personneMorale')) {
    formFields['personneLieePMDenominationFormeJuridique[0]'] = dirigeant1.civilitePersonneMorale1.personneLieePMDenomination + '/' + dirigeant1.civilitePersonneMorale1.personneLieePMFormeJuridique;
    formFields['personneLieePMLieuImmatriculation[0]'] = dirigeant1.civilitePersonneMorale1.personneLieePMNumeroIdentification + ' ' + dirigeant1.civilitePersonneMorale1.personneLieePMLieuImmatriculation;
}
formFields['adresseDirigeant[0]'] = (dirigeant1.adresseDirigeant1.siegeAdresseNumVoie != null ? dirigeant1.adresseDirigeant1.siegeAdresseNumVoie : '') + ' ' + (dirigeant1.adresseDirigeant1.siegeAdresseIndiceVoie != null ? dirigeant1.adresseDirigeant1.siegeAdresseIndiceVoie : '') + ' ' + (dirigeant1.adresseDirigeant1.siegeAdresseTypeVoie != null ? dirigeant1.adresseDirigeant1.siegeAdresseTypeVoie : '') + ' ' + dirigeant1.adresseDirigeant1.siegeAdresseNomVoie
 + ' ' + (dirigeant1.adresseDirigeant1.siegeAdresseComplementVoie != null ? dirigeant1.adresseDirigeant1.siegeAdresseComplementVoie : '')
 + ' ' + (dirigeant1.adresseDirigeant1.siegeAdresseDistributionSpecialeVoie != null ? dirigeant1.adresseDirigeant1.siegeAdresseDistributionSpecialeVoie : '');
formFields['siegeAdresseCodePostal[0]'] = dirigeant1.adresseDirigeant1.siegeAdresseCodePostal != null ? dirigeant1.adresseDirigeant1.siegeAdresseCodePostal : dirigeant1.adresseDirigeant1.siegeAdresseCodePostaEtranger;
formFields['siegeAdresseCommune[0]'] = dirigeant1.adresseDirigeant1.siegeAdresseCommune != null ? dirigeant1.adresseDirigeant1.siegeAdresseCommune : (dirigeant1.adresseDirigeant1.siegeAdresseVille + ' / ' + dirigeant1.adresseDirigeant1.siegeAdressePays);

// Cadre 17 Dirigeant 2
formFields['personneLieeQualite[1]'] = dirigeant2.personneLieeQualite;
formFields['personneLieePPNomNaissance[1]'] = dirigeant2.civilitePersonnePhysique2.personneLieePPNomNaissance != null ? dirigeant2.civilitePersonnePhysique2.personneLieePPNomNaissance : dirigeant2.civiliteRepresentant2.personneLieePPNomNaissance;
formFields['personneLieePPNomUsage[1]'] = dirigeant2.civilitePersonnePhysique2.personneLieePPNomUsage != null ? dirigeant2.civilitePersonnePhysique2.personneLieePPNomUsage : dirigeant2.civiliteRepresentant2.personneLieePPNomUsage;
if (dirigeant2.civilitePersonnePhysique2.personneLieePPPrenom != null and dirigeant2.civilitePersonnePhysique2.personneLieePPPrenom[0].length > 0) {
    var prenoms = [];
    for (i = 0; i < dirigeant2.civilitePersonnePhysique2.personneLieePPPrenom.size(); i++) {
        prenoms.push(dirigeant2.civilitePersonnePhysique2.personneLieePPPrenom[i]);
    }
    formFields['personneLieePPPrenom[1]'] = prenoms.toString();
} else if (dirigeant2.civiliteRepresentant2.personneLieePPPrenom != null) {
    var prenoms = [];
    for (i = 0; i < dirigeant2.civiliteRepresentant2.personneLieePPPrenom.size(); i++) {
        prenoms.push(dirigeant2.civiliteRepresentant2.personneLieePPPrenom[i]);
    }
    formFields['personneLieePPPrenom[1]'] = prenoms.toString();
}

if (dirigeant2.civilitePersonnePhysique2.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant2.civilitePersonnePhysique2.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFields['personneLieePPDateNaissance[1]'] = date1;
} else if (dirigeant2.civiliteRepresentant2.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant2.civiliteRepresentant2.personneLieePPDateNaissance.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
    date2 = date2.concat(dateTmp.getFullYear().toString());
    formFields['personneLieePPDateNaissance[1]'] = date2;
}
formFields['personneLieePPLieuNaissance[1]'] = 
dirigeant2.civilitePersonnePhysique2.personneLieePPLieuNaissanceCommune != null ? (dirigeant2.civilitePersonnePhysique2.personneLieePPLieuNaissanceCommune + ' ' + "(" + '' + dirigeant2.civilitePersonnePhysique2.personneLieePPDepartementNaissance.getId() + '' + ")") : 
(dirigeant2.civiliteRepresentant2.personneLieePPLieuNaissanceCommune != null ? (dirigeant2.civiliteRepresentant2.personneLieePPLieuNaissanceCommune + ' ' + "(" + '' + dirigeant2.civiliteRepresentant2.personneLieePPDepartementNaissance.getId() + '' + ")") : 
(dirigeant2.civilitePersonnePhysique2.personneLieePPLieuNaissanceVille != null ? (dirigeant2.civilitePersonnePhysique2.personneLieePPLieuNaissanceVille + ' / ' + dirigeant2.civilitePersonnePhysique2.personneLieePPLieuNaissancePays) : 
(dirigeant2.civiliteRepresentant2.personneLieePPLieuNaissanceVille != null ? (dirigeant2.civiliteRepresentant2.personneLieePPLieuNaissanceVille + ' / ' + dirigeant2.civiliteRepresentant2.personneLieePPLieuNaissancePays) : '')));
formFields['personneLieePPNationalite[1]'] = dirigeant2.civilitePersonnePhysique2.personneLieePPNationalite != null ? dirigeant2.civilitePersonnePhysique2.personneLieePPNationalite : dirigeant2.civiliteRepresentant2.personneLieePPNationalite;
formFields['genreMasculin[1]'] = Value('id').of(dirigeant2.civilitePersonnePhysique2.personnaliteGenre).eq('genreMasculin') ? true : false;
formFields['genreFeminin[1]']  = Value('id').of(dirigeant2.civilitePersonnePhysique2.personnaliteGenre).eq('genreFeminin')  ? true : false;
formFields['dirigeantMajoritaireCapitalSocial[1]'] = dirigeant2.civilitePersonnePhysique2.dirigeantMajoritaireCapitalSocial != false ? true : false;
if (Value('id').of(dirigeant1.personnaliteJuridique).eq('personneMorale')) {
    formFields['personneLieePMDenominationFormeJuridique[1]'] = dirigeant2.civilitePersonneMorale2.personneLieePMDenomination + '/' + dirigeant2.civilitePersonneMorale2.personneLieePMFormeJuridique;
    formFields['personneLieePMLieuImmatriculation[1]'] = dirigeant2.civilitePersonneMorale2.personneLieePMNumeroIdentification + ' ' + dirigeant2.civilitePersonneMorale2.personneLieePMLieuImmatriculation;
}
formFields['adresseDirigeant[1]'] = (dirigeant2.adresseDirigeant2.siegeAdresseNumVoie != null ? dirigeant2.adresseDirigeant2.siegeAdresseNumVoie : '') + ' ' + (dirigeant2.adresseDirigeant2.siegeAdresseIndiceVoie != null ? dirigeant2.adresseDirigeant2.siegeAdresseIndiceVoie : '') + ' ' + (dirigeant2.adresseDirigeant2.siegeAdresseTypeVoie != null ? dirigeant2.adresseDirigeant2.siegeAdresseTypeVoie : '') 
 + ' ' + (dirigeant2.adresseDirigeant2.siegeAdresseNomVoie !=null ? dirigeant2.adresseDirigeant2.siegeAdresseNomVoie :'')
 + ' ' + (dirigeant2.adresseDirigeant2.siegeAdresseComplementVoie != null ? dirigeant2.adresseDirigeant2.siegeAdresseComplementVoie : '')
 + ' ' + (dirigeant2.adresseDirigeant2.siegeAdresseDistributionSpecialeVoie != null ? dirigeant2.adresseDirigeant2.siegeAdresseDistributionSpecialeVoie : '');
formFields['siegeAdresseCodePostal[1]'] = dirigeant2.adresseDirigeant2.siegeAdresseCodePostal != null ? dirigeant2.adresseDirigeant2.siegeAdresseCodePostal : dirigeant2.adresseDirigeant2.siegeAdresseCodePostaEtranger;
formFields['siegeAdresseCommune[1]'] = dirigeant2.adresseDirigeant2.siegeAdresseCommune != null ? dirigeant2.adresseDirigeant2.siegeAdresseCommune : (dirigeant2.adresseDirigeant2.siegeAdresseVille != null ? dirigeant2.adresseDirigeant2.siegeAdresseVille + ' / ' + dirigeant2.adresseDirigeant2.siegeAdressePays : '');

//******************************************************************//
//******************** Cadre 18 Options fiscales *******************//
//******************************************************************//
//impositionBenefices
formFields['regimeFiscalRegimeImpositionBeneficesMBA'] = Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('regimeFiscalRegimeImpositionBeneficesMBA') != false ? true : false;
formFields['regimeFiscalRegimeImpositionBeneficesRsBA'] = Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('regimeFiscalRegimeImpositionBeneficesRsBA') != false ? true : false;
formFields['regimeFiscalRegimeImpositionBeneficesRnBA'] = Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('regimeFiscalRegimeImpositionBeneficesRnBA') != false ? true : false;
formFields['regimeFiscalRegimeImpositionBeneficesFfBA'] = Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('regimeFiscalRegimeImpositionBeneficesFfBA') != false ? true : false;
formFields['regimeFiscalRegimeImpositionBeneficesOrspBA'] = Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('regimeFiscalRegimeImpositionBeneficesOrspBA') != false ? true : false;
//impotSociete
formFields['regimeFiscalRegimeImpositionSocieteIsoIS'] = Value('id').of(fiscal.impotSociete.regimeFiscalImpositionSociete).eq('regimeFiscalRegimeImpositionSocieteIsoIS') != false ? true : false;
formFields['regimeFiscalRegimeImpositionSocieteRsIS'] = Value('id').of(fiscal.impotSociete.regimeFiscalImpositionSociete).eq('regimeFiscalRegimeImpositionSocieteRsIS') != false ? true : false;
formFields['regimeFiscalRegimeImpositionSocieteRnIS'] = Value('id').of(fiscal.impotSociete.regimeFiscalImpositionSociete).eq('regimeFiscalRegimeImpositionSocieteRnIS') != false ? true : false;
//OptionsTVA
formFields['impositionTVARemboursementForfaitaire'] = Value('id').of(fiscal.optionsTVA.regimeTVA).eq('impositionTVARemboursementForfaitaire') != false ? true : false;
formFields['impositionTVAImpositionObligatoire'] = Value('id').of(fiscal.optionsTVA.regimeTVA).eq('impositionTVAImpositionObligatoire') != false ? true : false;
/* formFields['regimeFiscalRegimeImpositionTVAOptionsParticulieres4'] = Value('id').of(fiscal.optionsTVA.optionTVAvolontaire).eq('regimeFiscalRegimeImpositionTVAOptionsParticulieres4') != false ? true : false; */
formFields['regimeFiscalRegimeImpositionTVAOptionsParticulieres4'] = fiscal.optionsTVA.optionTVAvolontaire != false ? true : false;
formFields['optionTVADeclaration'] = fiscal.optionsTVA.optionTVADeclaration != false ? true : false;
if (fiscal.optionsTVA.regimeFiscalDateClotureExerciceComptable != null) {
    var dateTmp = new Date(parseInt(fiscal.optionsTVA.regimeFiscalDateClotureExerciceComptable.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
    date = date.concat(dateTmp.getFullYear().toString());
    formFields['regimeFiscalDateClotureExerciceComptable'] = date;
}
formFields['regimeFiscalRegimeImpositionTVADeclarationTrimestrielle'] = Value('id').of(fiscal.optionsTVA.optionTVADepot).eq('regimeFiscalRegimeImpositionTVADeclarationTrimestrielle') != false ? true : false;
formFields['regimeFiscalRegimeImpositionTVADeclarationMensuelle'] = Value('id').of(fiscal.optionsTVA.optionTVADepot).eq('regimeFiscalRegimeImpositionTVADeclarationMensuelle') != false ? true : false;
//Bénéfices commerciaux BIC
formFields['regimeFiscalRegimeImpositionBeneficesMBIC'] = Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC).eq('regimeFiscalRegimeImpositionBeneficesMBIC') != false ? true : false;
formFields['regimeFiscalRegimeImpositionBeneficesRsBIC'] = Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC).eq('regimeFiscalRegimeImpositionBeneficesRsBIC') != false ? true : false;
formFields['regimeFiscalRegimeImpositionBeneficesRnBIC'] = Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC).eq('regimeFiscalRegimeImpositionBeneficesRnBIC') != false ? true : false;
//Bénéfices non commerciaux BNC
formFields['RegimeFiscalRegimeImpositionBeneficesRsBNC'] = Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBNC).eq('RegimeFiscalRegimeImpositionBeneficesRsBNC') != false ? true : false;
formFields['RegimeFiscalRegimeImpositionBeneficesDcBNC'] = Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBNC).eq('RegimeFiscalRegimeImpositionBeneficesDcBNC') != false ? true : false;
formFields['regimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionCreanceDette'] = Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBNC).eq('regimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionCreanceDette') != false ? true : false;
// TVA régime général
formFields['regimeFiscalRegimeImpositionTVARfTVA'] = Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC.regimeTVA1).eq('regimeFiscalRegimeImpositionTVARfTVA') != false ? true : Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC.regimeTVA2).eq('regimeFiscalRegimeImpositionTVARfTVA') != false ? true :
    Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBNC.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARfTVA') != false ? true : false;
formFields['regimeFiscalRegimeImpositionTVARsTVA'] = Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC.regimeTVA1).eq('RegimeFiscalRegimeImpositionTVARsTVA') != false ? true : Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBNC.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARsTVA') != false ? true : false;
formFields['regimeFiscalRegimeImpositionTVAMrTVA'] = Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC.regimeTVA1).eq('regimeFiscalRegimeImpositionTVAMrTVA') != false ? true : false;
formFields['regimeFiscalRegimeImpositionTVARnTVA'] = Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC.regimeTVA2).eq('regimeFiscalRegimeImpositionTVARnTVA') != false ? true :
    Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBNC.regimeTVA).eq('regimeFiscalRegimeImpositionTVARnTVA') != false ? true : false;
formFields['regimeFiscalRegimeImpositionTVAOptionsParticulieres1'] = fiscal.impositionAccessoire.impositionBeneficesBIC.regimeFiscalRegimeImpositionTVAOptionsParticulieres1 != false ? true :
    fiscal.impositionAccessoire.impositionBeneficesBNC.regimeFiscalRegimeImpositionTVAOptionsParticulieres1 != false ? true : false;
formFields['regimeFiscalRegimeImpositionTVAOptionsParticulieres2'] = fiscal.impositionAccessoire.impositionBeneficesBIC.regimeFiscalRegimeImpositionTVAOptionsParticulieres2 != false ? true : false;
//RENSEIGNEMENTS COMPLÉMENTAIRES
//Cadre 19 - Observations
formFields['formaliteObservations'] = cpt.formaliteObservations;
//Cadre 20 - Adresse de correspondance
formFields['cadreNumAdresse'] = Value('id').of(cpt.adresseCorrespond).eq('siege') ? "4" : (Value('id').of(cpt.adresseCorrespond).eq('exploitation') ? "11" : ' ');
if (Value('id').of(cpt.adresseCorrespond).eq('autre')) {
    formFields['adresseCorrespondance1'] = cpt.adresseCorrespondance.nomPrenomDenominationCorrespondance + ' ' + (cpt.adresseCorrespondance.numeroVoieAdresseCorrespondance != null ? cpt.adresseCorrespondance.numeroVoieAdresseCorrespondance : '')
         + ' ' + (cpt.adresseCorrespondance.indiceVoieAdresseCorrespondance != null ? cpt.adresseCorrespondance.indiceVoieAdresseCorrespondance : '')
         + ' ' + (cpt.adresseCorrespondance.typeVoieAdresseCorrespondance != null ? cpt.adresseCorrespondance.typeVoieAdresseCorrespondance : '')
         + ' ' + cpt.adresseCorrespondance.nomVoieAdresseCorrespondance;
    formFields['adresseCorrespondance2'] = (cpt.adresseCorrespondance.voieComplementAdresseCorrespondance != null ? cpt.adresseCorrespondance.voieComplementAdresseCorrespondance : '') + ' ' + (cpt.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance != null ? cpt.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance : '');
    formFields['formaliteCorrespondanceAdresseCodePostal'] = cpt.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal;
    formFields['formaliteCorrespondanceAdresseCommune'] = cpt.adresseCorrespondance.formaliteCorrespondanceAdresseCommune;
}
formFields['formaliteTelephone1'] = cpt.infosSup.formaliteTelephone1;
formFields['formaliteTelephone2'] = cpt.infosSup.formaliteTelephone2;
formFields['formaliteFaxCourriel'] = cpt.infosSup.formaliteFaxCourriel != null ? cpt.infosSup.formaliteFaxCourriel : cpt.infosSup.telecopie;

// Cadre 21 - Signature
formFields['formaliteSignataireQualiteRepresentantLegal'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal') != false ? true : false;
formFields['representantLegalNumero'] = Value('id').of(tag.soussigne).eq('FormaliteSignataireQualiteRepresentantLegal') ? (Value('id').of(tag.representantLegalNumero).eq('gerant1') ? "16" : (Value('id').of(tag.representantLegalNumero).eq('gerant2') ? "17" : (Value('id').of(tag.representantLegalNumero).eq('gerant3') ? "4 M0'" : (Value('id').of(tag.representantLegalNumero).eq('gerant4') ? "5 M0'" : (Value('id').of(tag.representantLegalNumero).eq('gerant5') ? "6 M0'" : (Value('id').of(tag.representantLegalNumero).eq('gerant6') ? "7 M0'" : (Value('id').of(tag.representantLegalNumero).eq('gerant7') ? "8 M0'" : (Value('id').of(tag.representantLegalNumero).eq('gerant8') ? "9 M0'" : (Value('id').of(tag.representantLegalNumero).eq('gerant9') ? "10 M0'" : (Value('id').of(tag.representantLegalNumero).eq('gerant10') ? "11 M0'" : '')))))))))) : '';

formFields['formaliteSignataireQualiteMandataire'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire') != false ? true : false;
if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire')) {
    formFields['nomPrenomDenominationMandataire'] = tag.adresseMandataire.nomPrenomDenominationMandataire;
    formFields['adresseMandataire'] = (tag.adresseMandataire.numeroVoieMandataire != null ? tag.adresseMandataire.numeroVoieMandataire : '')
     + ' ' + (tag.adresseMandataire.indiceVoieMandataire != null ? tag.adresseMandataire.indiceVoieMandataire : '')
     + ' ' + (tag.adresseMandataire.typeVoieMandataire != null ? tag.adresseMandataire.typeVoieMandataire : '')
     + ' ' + tag.adresseMandataire.nomVoieMandataire
     + ' ' + (tag.adresseMandataire.complementVoieMandataire != null ? tag.adresseMandataire.complementVoieMandataire : '')
     + ' ' + (tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
     + ' ' + tag.adresseMandataire.codePostalMandataire + ' ' + tag.adresseMandataire.villeAdresseMandataire; 
}
formFields['formaliteSignatureLieu'] = tag.formaliteSignatureLieu;
if (tag.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFields['formaliteSignatureDate'] = date1;
}
formFields['nbM0Prime'] = societe.entrepriseFusionScission or activite.cadreOrigineActivite.precedentExploitant.reprisePrecedentExploitant2 or dirigeant2.declarationDirigeant3 ? "1" : "0";
formFields['nbNsm'] = dirigeant10.cadreDirigeantNonSalarie10.voletSocialNumeroSecuriteSocialNSMA !=null ? "10" : 
(dirigeant9.cadreDirigeantNonSalarie9.voletSocialNumeroSecuriteSocialNSMA !=null ? "9" : 
(dirigeant8.cadreDirigeantNonSalarie8.voletSocialNumeroSecuriteSocialNSMA !=null ? "8" : 
(dirigeant7.cadreDirigeantNonSalarie7.voletSocialNumeroSecuriteSocialNSMA !=null ? "7" : 
(dirigeant6.cadreDirigeantNonSalarie6.voletSocialNumeroSecuriteSocialNSMA !=null ? "6" : 
(dirigeant5.cadreDirigeantNonSalarie5.voletSocialNumeroSecuriteSocialNSMA !=null ? "5" : 
(dirigeant4.cadreDirigeantNonSalarie4.voletSocialNumeroSecuriteSocialNSMA !=null ? "4" : 
(dirigeant3.cadreDirigeantNonSalarie3.voletSocialNumeroSecuriteSocialNSMA !=null ? "3" : 
(dirigeant2.cadreDirigeantNonSalarie2.voletSocialNumeroSecuriteSocialNSMA !=null ? "2" : 
(dirigeant1.cadreDirigeantNonSalarie1.voletSocialNumeroSecuriteSocialNSMA !=null ? "1" : "0"))))))))); 
formFields['signature'] = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce";

// M0 prime Agricole 1
// Cadre 1
formFieldsPers1['numeroIntercalaireM0Prime'] = "01";
formFieldsPers1['entrepriseDenomination'] = societe.entrepriseDenomination;
if (societe.entrepriseSocieteAutre != null) {
    formFieldsPers1['entrepriseFormeJuridique'] = societe.entrepriseSocieteAutre;
} else if (Value('id').of(societe.entrepriseFormeJuridiqueConstitution).eq('societeCivile')) {
    formFieldsPers1['entrepriseFormeJuridique'] = societe.entrepriseFormeJuridiqueCivile;
} else {
    formFieldsPers1['entrepriseFormeJuridique'] = societe.entrepriseFormeJuridiqueCommerciale;
}
//Cadre 4 Dirigeant 3
formFieldsPers1['personneLieeQualite[2]'] = dirigeant3.personneLieeQualite;
formFieldsPers1['personneLieePPNomNaissance[2]'] = dirigeant3.civilitePersonnePhysique3.personneLieePPNomNaissance != null ? dirigeant3.civilitePersonnePhysique3.personneLieePPNomNaissance : dirigeant3.civiliteRepresentant3.personneLieePPNomNaissance;
formFieldsPers1['personneLieePPNomUsage[2]'] = (dirigeant3.civilitePersonnePhysique3.personneLieePPNomUsage != null ? dirigeant3.civilitePersonnePhysique3.personneLieePPNomUsage : dirigeant3.civiliteRepresentant3.personneLieePPNomUsage);
if (dirigeant3.civilitePersonnePhysique3.personneLieePPPrenom != null and dirigeant3.civilitePersonnePhysique3.personneLieePPPrenom[0].length > 0) {
    var prenoms = [];3
    for (i = 0; i < dirigeant3.civilitePersonnePhysique3.personneLieePPPrenom.size(); i++) {
        prenoms.push(dirigeant3.civilitePersonnePhysique3.personneLieePPPrenom[i]);
    }
    formFieldsPers1['personneLieePPPrenom[2]'] = prenoms.toString();
} else if (dirigeant3.civiliteRepresentant3.personneLieePPPrenom != null) {
    var prenoms = [];
    for (i = 0; i < dirigeant3.civiliteRepresentant3.personneLieePPPrenom.size(); i++) {
        prenoms.push(dirigeant3.civiliteRepresentant3.personneLieePPPrenom[i]);
    }
    formFieldsPers1['personneLieePPPrenom[2]'] = prenoms.toString();
}

if (dirigeant3.civilitePersonnePhysique3.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant3.civilitePersonnePhysique3.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['personneLieePPDateNaissance[2]'] = date1;
} else if (dirigeant3.civiliteRepresentant3.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant3.civiliteRepresentant3.personneLieePPDateNaissance.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
    date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['personneLieePPDateNaissance[2]'] = date2;
}
formFieldsPers1['personneLieePPLieuNaissance[2]'] = dirigeant3.civilitePersonnePhysique3.personneLieePPLieuNaissanceCommune != null ? (dirigeant3.civilitePersonnePhysique3.personneLieePPLieuNaissanceCommune + ' ' + "(" + '' + dirigeant3.civilitePersonnePhysique3.personneLieePPDepartementNaissance.getId() + '' + ")") : (dirigeant3.civiliteRepresentant3.personneLieePPLieuNaissanceCommune != null ? (dirigeant3.civiliteRepresentant3.personneLieePPLieuNaissanceCommune + ' ' + "(" + '' + dirigeant3.civiliteRepresentant3.personneLieePPDepartementNaissance.getId() + '' + ")") : (dirigeant3.civilitePersonnePhysique3.personneLieePPLieuNaissanceVille != null ? (dirigeant3.civilitePersonnePhysique3.personneLieePPLieuNaissanceVille + ' / ' + dirigeant3.civilitePersonnePhysique3.personneLieePPLieuNaissancePays) : (dirigeant3.civiliteRepresentant3.personneLieePPLieuNaissanceVille + ' / ' + dirigeant3.civiliteRepresentant3.personneLieePPLieuNaissancePays)));
formFieldsPers1['personneLieePPNationalite[2]'] = dirigeant3.civilitePersonnePhysique3.personneLieePPNationalite != null ? dirigeant3.civilitePersonnePhysique3.personneLieePPNationalite : dirigeant3.civiliteRepresentant3.personneLieePPNationalite;
formFieldsPers1['genreMasculin[2]'] = Value('id').of(dirigeant3.civilitePersonnePhysique3.personnaliteGenre).eq('genreMasculin') ? true : false;
formFieldsPers1['genreFeminin[2]']  = Value('id').of(dirigeant3.civilitePersonnePhysique3.personnaliteGenre).eq('genreFeminin')  ? true : false;
formFieldsPers1['dirigeantMajoritaireCapitalSocial[2]'] = dirigeant3.civilitePersonnePhysique3.dirigeantMajoritaireCapitalSocial != false ? true : false;
if (Value('id').of(dirigeant3.personnaliteJuridique).eq('personneMorale')) {
    formFieldsPers1['personneLieePMDenominationFormeJuridique[2]'] = dirigeant3.civilitePersonneMorale3.personneLieePMDenomination + '/' + dirigeant3.civilitePersonneMorale3.personneLieePMFormeJuridique;
    formFieldsPers1['personneLieePMLieuImmatriculation[2]'] = dirigeant3.civilitePersonneMorale3.personneLieePMNumeroIdentification + ' ' + dirigeant3.civilitePersonneMorale3.personneLieePMLieuImmatriculation;
}
formFieldsPers1['adresseDirigeant[2]'] = (dirigeant3.adresseDirigeant3.siegeAdresseNumVoie != null ? dirigeant3.adresseDirigeant3.siegeAdresseNumVoie : '') + ' ' + (dirigeant3.adresseDirigeant3.siegeAdresseIndiceVoie != null ? dirigeant3.adresseDirigeant3.siegeAdresseIndiceVoie : '') + ' ' + (dirigeant3.adresseDirigeant3.siegeAdresseTypeVoie != null ? dirigeant3.adresseDirigeant3.siegeAdresseTypeVoie : '') + ' ' + dirigeant3.adresseDirigeant3.siegeAdresseNomVoie
 + ' ' + (dirigeant3.adresseDirigeant3.siegeAdresseComplementVoie != null ? dirigeant3.adresseDirigeant3.siegeAdresseComplementVoie : '')
 + ' ' + (dirigeant3.adresseDirigeant3.siegeAdresseDistributionSpecialeVoie != null ? dirigeant3.adresseDirigeant3.siegeAdresseDistributionSpecialeVoie : '');
formFieldsPers1['siegeAdresseCodePostal[2]'] = dirigeant3.adresseDirigeant3.siegeAdresseCodePostal != null ? dirigeant3.adresseDirigeant3.siegeAdresseCodePostal : dirigeant3.adresseDirigeant3.siegeAdresseCodePostaEtranger;
formFieldsPers1['siegeAdresseCommune[2]'] = dirigeant3.adresseDirigeant3.siegeAdresseCommune != null ? dirigeant3.adresseDirigeant3.siegeAdresseCommune : (dirigeant3.adresseDirigeant3.siegeAdresseVille + ' / ' + dirigeant3.adresseDirigeant3.siegeAdressePays);

//Cadre 5 Dirigeant 4
formFieldsPers1['personneLieeQualite[3]'] = dirigeant4.personneLieeQualite;
formFieldsPers1['personneLieePPNomNaissance[3]'] = dirigeant4.civilitePersonnePhysique4.personneLieePPNomNaissance != null ? dirigeant4.civilitePersonnePhysique4.personneLieePPNomNaissance : dirigeant4.civiliteRepresentant4.personneLieePPNomNaissance;
formFieldsPers1['personneLieePPNomUsage[3]'] = dirigeant4.civilitePersonnePhysique4.personneLieePPNomUsage != null ? dirigeant4.civilitePersonnePhysique4.personneLieePPNomUsage : dirigeant4.civiliteRepresentant4.personneLieePPNomUsage;
if (dirigeant4.civilitePersonnePhysique4.personneLieePPPrenom != null and dirigeant4.civilitePersonnePhysique4.personneLieePPPrenom[0].length > 0) {
    var prenoms = [];
    for (i = 0; i < dirigeant4.civilitePersonnePhysique4.personneLieePPPrenom.size(); i++) {
        prenoms.push(dirigeant4.civilitePersonnePhysique4.personneLieePPPrenom[i]);
    }
    formFieldsPers1['personneLieePPPrenom[3]'] = prenoms.toString();
} else if (dirigeant4.civiliteRepresentant4.personneLieePPPrenom != null) {
    var prenoms = [];
    for (i = 0; i < dirigeant4.civiliteRepresentant4.personneLieePPPrenom.size(); i++) {
        prenoms.push(dirigeant4.civiliteRepresentant4.personneLieePPPrenom[i]);
    }
    formFieldsPers1['personneLieePPPrenom[3]'] = prenoms.toString();
}

if (dirigeant4.civilitePersonnePhysique4.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant4.civilitePersonnePhysique4.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['personneLieePPDateNaissance[3]'] = date1;
} else if (dirigeant4.civiliteRepresentant4.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant4.civiliteRepresentant4.personneLieePPDateNaissance.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
    date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['personneLieePPDateNaissance[3]'] = date2;
}
formFieldsPers1['personneLieePPLieuNaissance[3]'] = dirigeant4.civilitePersonnePhysique4.personneLieePPLieuNaissanceCommune != null ? (dirigeant4.civilitePersonnePhysique4.personneLieePPLieuNaissanceCommune + ' ' + "(" + '' + dirigeant4.civilitePersonnePhysique4.personneLieePPDepartementNaissance.getId() + '' + ")") : (dirigeant4.civiliteRepresentant4.personneLieePPLieuNaissanceCommune != null ? (dirigeant4.civiliteRepresentant4.personneLieePPLieuNaissanceCommune + ' ' + "(" + '' + dirigeant4.civiliteRepresentant4.personneLieePPDepartementNaissance.getId() + '' + ")") : (dirigeant4.civilitePersonnePhysique4.personneLieePPLieuNaissanceVille != null ? (dirigeant4.civilitePersonnePhysique4.personneLieePPLieuNaissanceVille + ' / ' + dirigeant4.civilitePersonnePhysique4.personneLieePPLieuNaissancePays) : (dirigeant4.civiliteRepresentant4.personneLieePPLieuNaissanceVille + ' / ' + dirigeant4.civiliteRepresentant4.personneLieePPLieuNaissancePays)));
formFieldsPers1['personneLieePPNationalite[3]'] = dirigeant4.civilitePersonnePhysique4.personneLieePPNationalite != null ? dirigeant4.civilitePersonnePhysique4.personneLieePPNationalite : dirigeant4.civiliteRepresentant4.personneLieePPNationalite;
formFieldsPers1['genreMasculin[3]'] = Value('id').of(dirigeant4.civilitePersonnePhysique4.personnaliteGenre).eq('genreMasculin') ? true : false;
formFieldsPers1['genreFeminin[3]']  = Value('id').of(dirigeant4.civilitePersonnePhysique4.personnaliteGenre).eq('genreFeminin')  ? true : false;
formFieldsPers1['dirigeantMajoritaireCapitalSocial[3]'] = dirigeant4.civilitePersonnePhysique4.dirigeantMajoritaireCapitalSocial != false ? true : false;
if (Value('id').of(dirigeant4.personnaliteJuridique).eq('personneMorale')) {
    formFieldsPers1['personneLieePMDenominationFormeJuridique[3]'] = dirigeant4.civilitePersonneMorale4.personneLieePMDenomination + '/' + dirigeant4.civilitePersonneMorale4.personneLieePMFormeJuridique;
    formFieldsPers1['personneLieePMLieuImmatriculation[3]'] = dirigeant4.civilitePersonneMorale4.personneLieePMNumeroIdentification + ' ' + dirigeant4.civilitePersonneMorale4.personneLieePMLieuImmatriculation;
}
formFieldsPers1['adresseDirigeant[3]'] = (dirigeant4.adresseDirigeant4.siegeAdresseNumVoie != null ? dirigeant4.adresseDirigeant4.siegeAdresseNumVoie : '') + ' ' + (dirigeant4.adresseDirigeant4.siegeAdresseIndiceVoie != null ? dirigeant4.adresseDirigeant4.siegeAdresseIndiceVoie : '') + ' ' + (dirigeant4.adresseDirigeant4.siegeAdresseTypeVoie != null ? dirigeant4.adresseDirigeant4.siegeAdresseTypeVoie : '') + ' ' + dirigeant4.adresseDirigeant4.siegeAdresseNomVoie
 + ' ' + (dirigeant4.adresseDirigeant4.siegeAdresseComplementVoie != null ? dirigeant4.adresseDirigeant4.siegeAdresseComplementVoie : '')
 + ' ' + (dirigeant4.adresseDirigeant4.siegeAdresseDistributionSpecialeVoie != null ? dirigeant4.adresseDirigeant4.siegeAdresseDistributionSpecialeVoie : '');
formFieldsPers1['siegeAdresseCodePostal[3]'] = dirigeant4.adresseDirigeant4.siegeAdresseCodePostal != null ? dirigeant4.adresseDirigeant4.siegeAdresseCodePostal : dirigeant4.adresseDirigeant4.siegeAdresseCodePostaEtranger;
formFieldsPers1['siegeAdresseCommune[3]'] = dirigeant4.adresseDirigeant4.siegeAdresseCommune != null ? dirigeant4.adresseDirigeant4.siegeAdresseCommune : (dirigeant4.adresseDirigeant4.siegeAdresseVille + ' / ' + dirigeant4.adresseDirigeant4.siegeAdressePays);

//Cadre 6 Dirigeant 5
formFieldsPers1['personneLieeQualite[4]'] = dirigeant5.personneLieeQualite;
formFieldsPers1['personneLieePPNomNaissance[4]'] = dirigeant5.civilitePersonnePhysique5.personneLieePPNomNaissance != null ? dirigeant5.civilitePersonnePhysique5.personneLieePPNomNaissance : dirigeant5.civiliteRepresentant5.personneLieePPNomNaissance;
formFieldsPers1['personneLieePPNomUsage[4]'] = dirigeant5.civilitePersonnePhysique5.personneLieePPNomUsage != null ? dirigeant5.civilitePersonnePhysique5.personneLieePPNomUsage : dirigeant5.civiliteRepresentant5.personneLieePPNomUsage;
if (dirigeant5.civilitePersonnePhysique5.personneLieePPPrenom != null and dirigeant5.civilitePersonnePhysique5.personneLieePPPrenom[0].length > 0) {
    var prenoms = [];
    for (i = 0; i < dirigeant5.civilitePersonnePhysique5.personneLieePPPrenom.size(); i++) {
        prenoms.push(dirigeant5.civilitePersonnePhysique5.personneLieePPPrenom[i]);
    }
    formFieldsPers1['personneLieePPPrenom[4]'] = prenoms.toString();
} else if (dirigeant5.civiliteRepresentant5.personneLieePPPrenom != null ) {
    var prenoms = [];
    for (i = 0; i < dirigeant5.civiliteRepresentant5.personneLieePPPrenom.size(); i++) {
        prenoms.push(dirigeant5.civiliteRepresentant5.personneLieePPPrenom[i]);
    }
    formFieldsPers1['personneLieePPPrenom[4]'] = prenoms.toString();
}

if (dirigeant5.civilitePersonnePhysique5.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant5.civilitePersonnePhysique5.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['personneLieePPDateNaissance[4]'] = date1;
} else if (dirigeant5.civiliteRepresentant5.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant5.civiliteRepresentant5.personneLieePPDateNaissance.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
    date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['personneLieePPDateNaissance[4]'] = date2;
}
formFieldsPers1['personneLieePPLieuNaissance[4]'] = dirigeant5.civilitePersonnePhysique5.personneLieePPLieuNaissanceCommune != null ? (dirigeant5.civilitePersonnePhysique5.personneLieePPLieuNaissanceCommune + ' ' + "(" + '' + dirigeant5.civilitePersonnePhysique5.personneLieePPDepartementNaissance.getId() + '' + ")") : (dirigeant5.civiliteRepresentant5.personneLieePPLieuNaissanceCommune != null ? (dirigeant5.civiliteRepresentant5.personneLieePPLieuNaissanceCommune + ' ' + "(" + '' + dirigeant5.civiliteRepresentant5.personneLieePPDepartementNaissance.getId() + '' + ")") : (dirigeant5.civilitePersonnePhysique5.personneLieePPLieuNaissanceVille != null ? (dirigeant5.civilitePersonnePhysique5.personneLieePPLieuNaissanceVille + ' / ' + dirigeant5.civilitePersonnePhysique5.personneLieePPLieuNaissancePays) : (dirigeant5.civiliteRepresentant5.personneLieePPLieuNaissanceVille + ' / ' + dirigeant5.civiliteRepresentant5.personneLieePPLieuNaissancePays)));
formFieldsPers1['personneLieePPNationalite[4]'] = dirigeant5.civilitePersonnePhysique5.personneLieePPNationalite != null ? dirigeant5.civilitePersonnePhysique5.personneLieePPNationalite : dirigeant5.civiliteRepresentant5.personneLieePPNationalite;
formFieldsPers1['genreMasculin[4]'] = Value('id').of(dirigeant5.civilitePersonnePhysique5.personnaliteGenre).eq('genreMasculin') ? true : false;
formFieldsPers1['genreFeminin[4]']  = Value('id').of(dirigeant5.civilitePersonnePhysique5.personnaliteGenre).eq('genreFeminin')  ? true : false;
formFieldsPers1['dirigeantMajoritaireCapitalSocial[4]'] = dirigeant5.civilitePersonnePhysique5.dirigeantMajoritaireCapitalSocial != false ? true : false;
if (Value('id').of(dirigeant5.personnaliteJuridique).eq('personneMorale')) {
    formFieldsPers1['personneLieePMDenominationFormeJuridique[4]'] = dirigeant5.civilitePersonneMorale5.personneLieePMDenomination + '/' + dirigeant5.civilitePersonneMorale5.personneLieePMFormeJuridique;
    formFieldsPers1['personneLieePMLieuImmatriculation[4]'] = dirigeant5.civilitePersonneMorale5.personneLieePMNumeroIdentification + ' ' + dirigeant5.civilitePersonneMorale5.personneLieePMLieuImmatriculation;
}
formFieldsPers1['adresseDirigeant[4]'] = (dirigeant5.adresseDirigeant5.siegeAdresseNumVoie != null ? dirigeant5.adresseDirigeant5.siegeAdresseNumVoie : '') + ' ' + (dirigeant5.adresseDirigeant5.siegeAdresseIndiceVoie != null ? dirigeant5.adresseDirigeant5.siegeAdresseIndiceVoie : '') + ' ' + (dirigeant5.adresseDirigeant5.siegeAdresseTypeVoie != null ? dirigeant5.adresseDirigeant5.siegeAdresseTypeVoie : '') + ' ' + dirigeant5.adresseDirigeant5.siegeAdresseNomVoie
 + ' ' + (dirigeant5.adresseDirigeant5.siegeAdresseComplementVoie != null ? dirigeant5.adresseDirigeant5.siegeAdresseComplementVoie : '')
 + ' ' + (dirigeant5.adresseDirigeant5.siegeAdresseDistributionSpecialeVoie != null ? dirigeant5.adresseDirigeant5.siegeAdresseDistributionSpecialeVoie : '');
formFieldsPers1['siegeAdresseCodePostal[4]'] = dirigeant5.adresseDirigeant5.siegeAdresseCodePostal != null ? dirigeant5.adresseDirigeant5.siegeAdresseCodePostal : dirigeant5.adresseDirigeant5.siegeAdresseCodePostaEtranger;
formFieldsPers1['siegeAdresseCommune[4]'] = dirigeant5.adresseDirigeant5.siegeAdresseCommune != null ? dirigeant5.adresseDirigeant5.siegeAdresseCommune : (dirigeant5.adresseDirigeant5.siegeAdresseVille + ' / ' + dirigeant5.adresseDirigeant5.siegeAdressePays);

//Cadre 7 Dirigeant 6
formFieldsPers1['personneLieeQualite[5]'] = dirigeant6.personneLieeQualite;
formFieldsPers1['personneLieePPNomNaissance[5]'] = dirigeant6.civilitePersonnePhysique6.personneLieePPNomNaissance != null ? dirigeant6.civilitePersonnePhysique6.personneLieePPNomNaissance : dirigeant6.civiliteRepresentant6.personneLieePPNomNaissance;
formFieldsPers1['personneLieePPNomUsage[5]'] = dirigeant6.civilitePersonnePhysique6.personneLieePPNomUsage != null ? dirigeant6.civilitePersonnePhysique6.personneLieePPNomUsage : dirigeant6.civiliteRepresentant6.personneLieePPNomUsage;
if (dirigeant6.civilitePersonnePhysique6.personneLieePPPrenom != null and dirigeant6.civilitePersonnePhysique6.personneLieePPPrenom[0].length > 0) {
    var prenoms = [];
    for (i = 0; i < dirigeant6.civilitePersonnePhysique6.personneLieePPPrenom.size(); i++) {
        prenoms.push(dirigeant6.civilitePersonnePhysique6.personneLieePPPrenom[i]);
    }
    formFieldsPers1['personneLieePPPrenom[5]'] = prenoms.toString();
} else if (dirigeant6.civiliteRepresentant6.personneLieePPPrenom != null ) {
    var prenoms = [];
    for (i = 0; i < dirigeant6.civiliteRepresentant6.personneLieePPPrenom.size(); i++) {
        prenoms.push(dirigeant6.civiliteRepresentant6.personneLieePPPrenom[i]);
    }
    formFieldsPers1['personneLieePPPrenom[5]'] = prenoms.toString();
}

if (dirigeant6.civilitePersonnePhysique6.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant6.civilitePersonnePhysique6.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['personneLieePPDateNaissance[5]'] = date1;
} else if (dirigeant6.civiliteRepresentant6.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant6.civiliteRepresentant6.personneLieePPDateNaissance.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
    date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['personneLieePPDateNaissance[5]'] = date2;
}
formFieldsPers1['personneLieePPLieuNaissance[5]'] = dirigeant6.civilitePersonnePhysique6.personneLieePPLieuNaissanceCommune != null ? (dirigeant6.civilitePersonnePhysique6.personneLieePPLieuNaissanceCommune + ' ' + "(" + '' + dirigeant6.civilitePersonnePhysique6.personneLieePPDepartementNaissance.getId() + '' + ")") : (dirigeant6.civiliteRepresentant6.personneLieePPLieuNaissanceCommune != null ? (dirigeant6.civiliteRepresentant6.personneLieePPLieuNaissanceCommune + ' ' + "(" + '' + dirigeant6.civiliteRepresentant6.personneLieePPDepartementNaissance.getId() + '' + ")") : (dirigeant6.civilitePersonnePhysique6.personneLieePPLieuNaissanceVille != null ? (dirigeant6.civilitePersonnePhysique6.personneLieePPLieuNaissanceVille + ' / ' + dirigeant6.civilitePersonnePhysique6.personneLieePPLieuNaissancePays) : (dirigeant6.civiliteRepresentant6.personneLieePPLieuNaissanceVille + ' / ' + dirigeant6.civiliteRepresentant6.personneLieePPLieuNaissancePays)));
formFieldsPers1['personneLieePPNationalite[5]'] = dirigeant6.civilitePersonnePhysique6.personneLieePPNationalite != null ? dirigeant6.civilitePersonnePhysique6.personneLieePPNationalite : dirigeant6.civiliteRepresentant6.personneLieePPNationalite;
formFieldsPers1['genreMasculin[5]'] = Value('id').of(dirigeant6.civilitePersonnePhysique6.personnaliteGenre).eq('genreMasculin') ? true : false;
formFieldsPers1['genreFeminin[5]']  = Value('id').of(dirigeant6.civilitePersonnePhysique6.personnaliteGenre).eq('genreFeminin')  ? true : false;
formFieldsPers1['dirigeantMajoritaireCapitalSocial[5]'] = dirigeant6.civilitePersonnePhysique6.dirigeantMajoritaireCapitalSocial != false ? true : false;
if (Value('id').of(dirigeant6.personnaliteJuridique).eq('personneMorale')) {
    formFieldsPers1['personneLieePMDenominationFormeJuridique[5]'] = dirigeant6.civilitePersonneMorale6.personneLieePMDenomination + '/' + dirigeant6.civilitePersonneMorale6.personneLieePMFormeJuridique;
    formFieldsPers1['personneLieePMLieuImmatriculation[5]'] = dirigeant6.civilitePersonneMorale6.personneLieePMNumeroIdentification + ' ' + dirigeant6.civilitePersonneMorale6.personneLieePMLieuImmatriculation;
}
formFieldsPers1['adresseDirigeant[5]'] = (dirigeant6.adresseDirigeant6.siegeAdresseNumVoie != null ? dirigeant6.adresseDirigeant6.siegeAdresseNumVoie : '') + ' ' + (dirigeant6.adresseDirigeant6.siegeAdresseIndiceVoie != null ? dirigeant6.adresseDirigeant6.siegeAdresseIndiceVoie : '') + ' ' + (dirigeant6.adresseDirigeant6.siegeAdresseTypeVoie != null ? dirigeant6.adresseDirigeant6.siegeAdresseTypeVoie : '') + ' ' + dirigeant6.adresseDirigeant6.siegeAdresseNomVoie
 + ' ' + (dirigeant6.adresseDirigeant6.siegeAdresseComplementVoie != null ? dirigeant6.adresseDirigeant6.siegeAdresseComplementVoie : '')
 + ' ' + (dirigeant6.adresseDirigeant6.siegeAdresseDistributionSpecialeVoie != null ? dirigeant6.adresseDirigeant6.siegeAdresseDistributionSpecialeVoie : '');
formFieldsPers1['siegeAdresseCodePostal[5]'] = dirigeant6.adresseDirigeant6.siegeAdresseCodePostal != null ? dirigeant6.adresseDirigeant6.siegeAdresseCodePostal : dirigeant6.adresseDirigeant6.siegeAdresseCodePostaEtranger;
formFieldsPers1['siegeAdresseCommune[5]'] = dirigeant6.adresseDirigeant6.siegeAdresseCommune != null ? dirigeant6.adresseDirigeant6.siegeAdresseCommune : (dirigeant6.adresseDirigeant6.siegeAdresseVille + ' / ' + dirigeant6.adresseDirigeant6.siegeAdressePays);

//Cadre 8 Dirigeant 7
formFieldsPers1['personneLieeQualite[6]'] = dirigeant7.personneLieeQualite;
formFieldsPers1['personneLieePPNomNaissance[6]'] = dirigeant7.civilitePersonnePhysique7.personneLieePPNomNaissance != null ? dirigeant7.civilitePersonnePhysique7.personneLieePPNomNaissance : dirigeant7.civiliteRepresentant7.personneLieePPNomNaissance;
formFieldsPers1['personneLieePPNomUsage[6]'] = dirigeant7.civilitePersonnePhysique7.personneLieePPNomUsage != null ? dirigeant7.civilitePersonnePhysique7.personneLieePPNomUsage : dirigeant7.civiliteRepresentant7.personneLieePPNomUsage;
if (dirigeant7.civilitePersonnePhysique7.personneLieePPPrenom != null and dirigeant7.civilitePersonnePhysique7.personneLieePPPrenom[0].length > 0 ) {
    var prenoms = [];
    for (i = 0; i < dirigeant7.civilitePersonnePhysique7.personneLieePPPrenom.size(); i++) {
        prenoms.push(dirigeant7.civilitePersonnePhysique7.personneLieePPPrenom[i]);
    }
    formFieldsPers1['personneLieePPPrenom[6]'] = prenoms.toString();
} else if (dirigeant7.civiliteRepresentant7.personneLieePPPrenom != null ) {
    var prenoms = [];
    for (i = 0; i < dirigeant7.civiliteRepresentant7.personneLieePPPrenom.size(); i++) {
        prenoms.push(dirigeant7.civiliteRepresentant7.personneLieePPPrenom[i]);
    }
    formFieldsPers1['personneLieePPPrenom[6]'] = prenoms.toString();
}

if (dirigeant7.civilitePersonnePhysique7.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant7.civilitePersonnePhysique7.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['personneLieePPDateNaissance[6]'] = date1;
} else if (dirigeant7.civiliteRepresentant7.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant7.civiliteRepresentant7.personneLieePPDateNaissance.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
    date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['personneLieePPDateNaissance[6]'] = date2;
}
formFieldsPers1['personneLieePPLieuNaissance[6]'] = dirigeant7.civilitePersonnePhysique7.personneLieePPLieuNaissanceCommune != null ? (dirigeant7.civilitePersonnePhysique7.personneLieePPLieuNaissanceCommune + ' ' + "(" + '' + dirigeant7.civilitePersonnePhysique7.personneLieePPDepartementNaissance.getId() + '' + ")") : (dirigeant7.civiliteRepresentant7.personneLieePPLieuNaissanceCommune != null ? (dirigeant7.civiliteRepresentant7.personneLieePPLieuNaissanceCommune + ' ' + "(" + '' + dirigeant7.civiliteRepresentant7.personneLieePPDepartementNaissance.getId() + '' + ")") : (dirigeant7.civilitePersonnePhysique7.personneLieePPLieuNaissanceVille != null ? (dirigeant7.civilitePersonnePhysique7.personneLieePPLieuNaissanceVille + ' / ' + dirigeant7.civilitePersonnePhysique7.personneLieePPLieuNaissancePays) : (dirigeant7.civiliteRepresentant7.personneLieePPLieuNaissanceVille + ' / ' + dirigeant7.civiliteRepresentant7.personneLieePPLieuNaissancePays)));
formFieldsPers1['personneLieePPNationalite[6]'] = dirigeant7.civilitePersonnePhysique7.personneLieePPNationalite != null ? dirigeant7.civilitePersonnePhysique7.personneLieePPNationalite : dirigeant7.civiliteRepresentant7.personneLieePPNationalite;
formFieldsPers1['genreMasculin[6]'] = Value('id').of(dirigeant7.civilitePersonnePhysique7.personnaliteGenre).eq('genreMasculin') ? true : false;
formFieldsPers1['genreFeminin[6]']  = Value('id').of(dirigeant7.civilitePersonnePhysique7.personnaliteGenre).eq('genreFeminin')  ? true : false;
formFieldsPers1['dirigeantMajoritaireCapitalSocial[6]'] = dirigeant7.civilitePersonnePhysique7.dirigeantMajoritaireCapitalSocial != false ? true : false;
if (Value('id').of(dirigeant7.personnaliteJuridique).eq('personneMorale')) {
    formFieldsPers1['personneLieePMDenominationFormeJuridique[6]'] = dirigeant7.civilitePersonneMorale7.personneLieePMDenomination + '/' + dirigeant7.civilitePersonneMorale7.personneLieePMFormeJuridique;
    formFieldsPers1['personneLieePMLieuImmatriculation[6]'] = dirigeant7.civilitePersonneMorale7.personneLieePMNumeroIdentification + ' ' + dirigeant7.civilitePersonneMorale7.personneLieePMLieuImmatriculation;
}
formFieldsPers1['adresseDirigeant[6]'] = (dirigeant7.adresseDirigeant7.siegeAdresseNumVoie != null ? dirigeant7.adresseDirigeant7.siegeAdresseNumVoie : '') + ' ' + (dirigeant7.adresseDirigeant7.siegeAdresseIndiceVoie != null ? dirigeant7.adresseDirigeant7.siegeAdresseIndiceVoie : '') + ' ' + (dirigeant7.adresseDirigeant7.siegeAdresseTypeVoie != null ? dirigeant7.adresseDirigeant7.siegeAdresseTypeVoie : '') + ' ' + dirigeant7.adresseDirigeant7.siegeAdresseNomVoie
 + ' ' + (dirigeant7.adresseDirigeant7.siegeAdresseComplementVoie != null ? dirigeant7.adresseDirigeant7.siegeAdresseComplementVoie : '')
 + ' ' + (dirigeant7.adresseDirigeant7.siegeAdresseDistributionSpecialeVoie != null ? dirigeant7.adresseDirigeant7.siegeAdresseDistributionSpecialeVoie : '');
formFieldsPers1['siegeAdresseCodePostal[6]'] = dirigeant7.adresseDirigeant7.siegeAdresseCodePostal != null ? dirigeant7.adresseDirigeant7.siegeAdresseCodePostal : dirigeant7.adresseDirigeant7.siegeAdresseCodePostaEtranger;
formFieldsPers1['siegeAdresseCommune[6]'] = dirigeant7.adresseDirigeant7.siegeAdresseCommune != null ? dirigeant7.adresseDirigeant7.siegeAdresseCommune : (dirigeant7.adresseDirigeant7.siegeAdresseVille + ' / ' + dirigeant7.adresseDirigeant7.siegeAdressePays);

//Cadre 9 Dirigeant 8
formFieldsPers1['personneLieeQualite[7]'] = dirigeant8.personneLieeQualite;
formFieldsPers1['personneLieePPNomNaissance[7]'] = dirigeant8.civilitePersonnePhysique8.personneLieePPNomNaissance != null ? dirigeant8.civilitePersonnePhysique8.personneLieePPNomNaissance : dirigeant8.civiliteRepresentant8.personneLieePPNomNaissance;
formFieldsPers1['personneLieePPNomUsage[7]'] = dirigeant8.civilitePersonnePhysique8.personneLieePPNomUsage != null ? dirigeant8.civilitePersonnePhysique8.personneLieePPNomUsage : dirigeant8.civiliteRepresentant8.personneLieePPNomUsage;
if (dirigeant8.civilitePersonnePhysique8.personneLieePPPrenom != null and dirigeant8.civilitePersonnePhysique8.personneLieePPPrenom[0].length > 0) {
    var prenoms = [];
    for (i = 0; i < dirigeant8.civilitePersonnePhysique8.personneLieePPPrenom.size(); i++) {
        prenoms.push(dirigeant8.civilitePersonnePhysique8.personneLieePPPrenom[i]);
    }
    formFieldsPers1['personneLieePPPrenom[7]'] = prenoms.toString();
} else if (dirigeant8.civiliteRepresentant8.personneLieePPPrenom != null ) {
    var prenoms = [];
    for (i = 0; i < dirigeant8.civiliteRepresentant8.personneLieePPPrenom.size(); i++) {
        prenoms.push(dirigeant8.civiliteRepresentant8.personneLieePPPrenom[i]);
    }
    formFieldsPers1['personneLieePPPrenom[7]'] = prenoms.toString();
}

if (dirigeant8.civilitePersonnePhysique8.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant8.civilitePersonnePhysique8.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['personneLieePPDateNaissance[7]'] = date1;
} else if (dirigeant8.civiliteRepresentant8.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant8.civiliteRepresentant8.personneLieePPDateNaissance.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
    date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['personneLieePPDateNaissance[7]'] = date2;
}
formFieldsPers1['personneLieePPLieuNaissance[7]'] = dirigeant8.civilitePersonnePhysique8.personneLieePPLieuNaissanceCommune != null ? (dirigeant8.civilitePersonnePhysique8.personneLieePPLieuNaissanceCommune + ' ' + "(" + '' + dirigeant8.civilitePersonnePhysique8.personneLieePPDepartementNaissance.getId() + '' + ")") : (dirigeant8.civiliteRepresentant8.personneLieePPLieuNaissanceCommune != null ? (dirigeant8.civiliteRepresentant8.personneLieePPLieuNaissanceCommune + ' ' + "(" + '' + dirigeant8.civiliteRepresentant8.personneLieePPDepartementNaissance.getId() + '' + ")") : (dirigeant8.civilitePersonnePhysique8.personneLieePPLieuNaissanceVille != null ? (dirigeant8.civilitePersonnePhysique8.personneLieePPLieuNaissanceVille + ' / ' + dirigeant8.civilitePersonnePhysique8.personneLieePPLieuNaissancePays) : (dirigeant8.civiliteRepresentant8.personneLieePPLieuNaissanceVille + ' / ' + dirigeant8.civiliteRepresentant8.personneLieePPLieuNaissancePays)));
formFieldsPers1['personneLieePPNationalite[7]'] = dirigeant8.civilitePersonnePhysique8.personneLieePPNationalite != null ? dirigeant8.civilitePersonnePhysique8.personneLieePPNationalite : dirigeant8.civiliteRepresentant8.personneLieePPNationalite;
formFieldsPers1['genreMasculin[7]'] = Value('id').of(dirigeant8.civilitePersonnePhysique8.personnaliteGenre).eq('genreMasculin') ? true : false;
formFieldsPers1['genreFeminin[7]']  = Value('id').of(dirigeant8.civilitePersonnePhysique8.personnaliteGenre).eq('genreFeminin')  ? true : false;
formFieldsPers1['dirigeantMajoritaireCapitalSocial[7]'] = dirigeant8.civilitePersonnePhysique8.dirigeantMajoritaireCapitalSocial != false ? true : false;
if (Value('id').of(dirigeant8.personnaliteJuridique).eq('personneMorale')) {
    formFieldsPers1['personneLieePMDenominationFormeJuridique[7]'] = dirigeant8.civilitePersonneMorale8.personneLieePMDenomination + '/' + dirigeant8.civilitePersonneMorale8.personneLieePMFormeJuridique;
    formFieldsPers1['personneLieePMLieuImmatriculation[7]'] = dirigeant8.civilitePersonneMorale8.personneLieePMNumeroIdentification + ' ' + dirigeant8.civilitePersonneMorale8.personneLieePMLieuImmatriculation;
}
formFieldsPers1['adresseDirigeant[7]'] = (dirigeant8.adresseDirigeant8.siegeAdresseNumVoie != null ? dirigeant8.adresseDirigeant8.siegeAdresseNumVoie : '') + ' ' + (dirigeant8.adresseDirigeant8.siegeAdresseIndiceVoie != null ? dirigeant8.adresseDirigeant8.siegeAdresseIndiceVoie : '') + ' ' + (dirigeant8.adresseDirigeant8.siegeAdresseTypeVoie != null ? dirigeant8.adresseDirigeant8.siegeAdresseTypeVoie : '') + ' ' + dirigeant8.adresseDirigeant8.siegeAdresseNomVoie
 + ' ' + (dirigeant8.adresseDirigeant8.siegeAdresseComplementVoie != null ? dirigeant8.adresseDirigeant8.siegeAdresseComplementVoie : '')
 + ' ' + (dirigeant8.adresseDirigeant8.siegeAdresseDistributionSpecialeVoie != null ? dirigeant8.adresseDirigeant8.siegeAdresseDistributionSpecialeVoie : '');
formFieldsPers1['siegeAdresseCodePostal[7]'] = dirigeant8.adresseDirigeant8.siegeAdresseCodePostal != null ? dirigeant8.adresseDirigeant8.siegeAdresseCodePostal : dirigeant8.adresseDirigeant8.siegeAdresseCodePostaEtranger;
formFieldsPers1['siegeAdresseCommune[7]'] = dirigeant8.adresseDirigeant8.siegeAdresseCommune != null ? dirigeant8.adresseDirigeant8.siegeAdresseCommune : (dirigeant8.adresseDirigeant8.siegeAdresseVille + ' / ' + dirigeant8.adresseDirigeant8.siegeAdressePays);

//Cadre 10 Dirigeant 9
formFieldsPers1['personneLieeQualite[8]'] = dirigeant9.personneLieeQualite;
formFieldsPers1['personneLieePPNomNaissance[8]'] = dirigeant9.civilitePersonnePhysique9.personneLieePPNomNaissance != null ? dirigeant9.civilitePersonnePhysique9.personneLieePPNomNaissance : dirigeant9.civiliteRepresentant9.personneLieePPNomNaissance;
formFieldsPers1['personneLieePPNomUsage[8]'] = dirigeant9.civilitePersonnePhysique9.personneLieePPNomUsage != null ? dirigeant9.civilitePersonnePhysique9.personneLieePPNomUsage : dirigeant9.civiliteRepresentant9.personneLieePPNomUsage;
if (dirigeant9.civilitePersonnePhysique9.personneLieePPPrenom != null and dirigeant9.civilitePersonnePhysique9.personneLieePPPrenom[0].length > 0) {
    var prenoms = [];
    for (i = 0; i < dirigeant9.civilitePersonnePhysique9.personneLieePPPrenom.size(); i++) {
        prenoms.push(dirigeant9.civilitePersonnePhysique9.personneLieePPPrenom[i]);
    }
    formFieldsPers1['personneLieePPPrenom[8]'] = prenoms.toString();
} else if (dirigeant9.civiliteRepresentant9.personneLieePPPrenom != null ) {
    var prenoms = [];
    for (i = 0; i < dirigeant9.civiliteRepresentant9.personneLieePPPrenom.size(); i++) {
        prenoms.push(dirigeant9.civiliteRepresentant9.personneLieePPPrenom[i]);
    }
    formFieldsPers1['personneLieePPPrenom[8]'] = prenoms.toString();
}

if (dirigeant9.civilitePersonnePhysique9.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant9.civilitePersonnePhysique9.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['personneLieePPDateNaissance[8]'] = date1;
} else if (dirigeant9.civiliteRepresentant9.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant9.civiliteRepresentant9.personneLieePPDateNaissance.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
    date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['personneLieePPDateNaissance[8]'] = date2;
}
formFieldsPers1['personneLieePPLieuNaissance[8]'] = dirigeant9.civilitePersonnePhysique9.personneLieePPLieuNaissanceCommune != null ? (dirigeant9.civilitePersonnePhysique9.personneLieePPLieuNaissanceCommune + ' ' + "(" + '' + dirigeant9.civilitePersonnePhysique9.personneLieePPDepartementNaissance.getId() + '' + ")") : (dirigeant9.civiliteRepresentant9.personneLieePPLieuNaissanceCommune != null ? (dirigeant9.civiliteRepresentant9.personneLieePPLieuNaissanceCommune + ' ' + "(" + '' + dirigeant9.civiliteRepresentant9.personneLieePPDepartementNaissance.getId() + '' + ")") : (dirigeant9.civilitePersonnePhysique9.personneLieePPLieuNaissanceVille != null ? (dirigeant9.civilitePersonnePhysique9.personneLieePPLieuNaissanceVille + ' / ' + dirigeant9.civilitePersonnePhysique9.personneLieePPLieuNaissancePays) : (dirigeant9.civiliteRepresentant9.personneLieePPLieuNaissanceVille + ' / ' + dirigeant9.civiliteRepresentant9.personneLieePPLieuNaissancePays)));
formFieldsPers1['personneLieePPNationalite[8]'] = dirigeant9.civilitePersonnePhysique9.personneLieePPNationalite != null ? dirigeant9.civilitePersonnePhysique9.personneLieePPNationalite : dirigeant9.civiliteRepresentant9.personneLieePPNationalite;
formFieldsPers1['genreMasculin[8]'] = Value('id').of(dirigeant9.civilitePersonnePhysique9.personnaliteGenre).eq('genreMasculin') ? true : false;
formFieldsPers1['genreFeminin[8]']  = Value('id').of(dirigeant9.civilitePersonnePhysique9.personnaliteGenre).eq('genreFeminin')  ? true : false;
formFieldsPers1['dirigeantMajoritaireCapitalSocial[8]'] = dirigeant9.civilitePersonnePhysique9.dirigeantMajoritaireCapitalSocial != false ? true : false;
if (Value('id').of(dirigeant9.personnaliteJuridique).eq('personneMorale')) {
    formFieldsPers1['personneLieePMDenominationFormeJuridique[8]'] = dirigeant9.civilitePersonneMorale9.personneLieePMDenomination + '/' + dirigeant9.civilitePersonneMorale9.personneLieePMFormeJuridique;
    formFieldsPers1['personneLieePMLieuImmatriculation[8]'] = dirigeant9.civilitePersonneMorale9.personneLieePMNumeroIdentification + ' ' + dirigeant9.civilitePersonneMorale9.personneLieePMLieuImmatriculation;
}
formFieldsPers1['adresseDirigeant[8]'] = (dirigeant9.adresseDirigeant9.siegeAdresseNumVoie != null ? dirigeant9.adresseDirigeant9.siegeAdresseNumVoie : '') + ' ' + (dirigeant9.adresseDirigeant9.siegeAdresseIndiceVoie != null ? dirigeant9.adresseDirigeant9.siegeAdresseIndiceVoie : '') + ' ' + (dirigeant9.adresseDirigeant9.siegeAdresseTypeVoie != null ? dirigeant9.adresseDirigeant9.siegeAdresseTypeVoie : '') + ' ' + dirigeant9.adresseDirigeant9.siegeAdresseNomVoie
 + ' ' + (dirigeant9.adresseDirigeant9.siegeAdresseComplementVoie != null ? dirigeant9.adresseDirigeant9.siegeAdresseComplementVoie : '')
 + ' ' + (dirigeant9.adresseDirigeant9.siegeAdresseDistributionSpecialeVoie != null ? dirigeant9.adresseDirigeant9.siegeAdresseDistributionSpecialeVoie : '');
formFieldsPers1['siegeAdresseCodePostal[8]'] = dirigeant9.adresseDirigeant9.siegeAdresseCodePostal != null ? dirigeant9.adresseDirigeant9.siegeAdresseCodePostal : dirigeant9.adresseDirigeant9.siegeAdresseCodePostaEtranger;
formFieldsPers1['siegeAdresseCommune[8]'] = dirigeant9.adresseDirigeant9.siegeAdresseCommune != null ? dirigeant9.adresseDirigeant9.siegeAdresseCommune : (dirigeant9.adresseDirigeant9.siegeAdresseVille + ' / ' + dirigeant9.adresseDirigeant9.siegeAdressePays);

//Cadre 11 Dirigeant 10
formFieldsPers1['personneLieeQualite[9]'] = dirigeant10.personneLieeQualite;
formFieldsPers1['personneLieePPNomNaissance[9]'] = dirigeant10.civilitePersonnePhysique10.personneLieePPNomNaissance != null ? dirigeant10.civilitePersonnePhysique10.personneLieePPNomNaissance : dirigeant10.civiliteRepresentant10.personneLieePPNomNaissance;
formFieldsPers1['personneLieePPNomUsage[9]'] = dirigeant10.civilitePersonnePhysique10.personneLieePPNomUsage != null ? dirigeant10.civilitePersonnePhysique10.personneLieePPNomUsage : dirigeant10.civiliteRepresentant10.personneLieePPNomUsage;
if (dirigeant10.civilitePersonnePhysique10.personneLieePPPrenom != null and dirigeant10.civilitePersonnePhysique10.personneLieePPPrenom[0].length > 0) {
    var prenoms = [];
    for (i = 0; i < dirigeant10.civilitePersonnePhysique10.personneLieePPPrenom.size(); i++) {
        prenoms.push(dirigeant10.civilitePersonnePhysique10.personneLieePPPrenom[i]);
    }
    formFieldsPers1['personneLieePPPrenom[9]'] = prenoms.toString();
} else if (dirigeant10.civiliteRepresentant10.personneLieePPPrenom != null ) {
    var prenoms = [];
    for (i = 0; i < dirigeant10.civiliteRepresentant10.personneLieePPPrenom.size(); i++) {
        prenoms.push(dirigeant10.civiliteRepresentant10.personneLieePPPrenom[i]);
    }
    formFieldsPers1['personneLieePPPrenom[9]'] = prenoms.toString();
}

if (dirigeant10.civilitePersonnePhysique10.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant10.civilitePersonnePhysique10.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['personneLieePPDateNaissance[9]'] = date1;
} else if (dirigeant10.civiliteRepresentant10.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant10.civiliteRepresentant10.personneLieePPDateNaissance.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
    date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['personneLieePPDateNaissance[9]'] = date2;
}
formFieldsPers1['personneLieePPLieuNaissance[9]'] = dirigeant10.civilitePersonnePhysique10.personneLieePPLieuNaissanceCommune != null ? (dirigeant10.civilitePersonnePhysique10.personneLieePPLieuNaissanceCommune + ' ' + "(" + '' + dirigeant10.civilitePersonnePhysique10.personneLieePPDepartementNaissance.getId() + '' + ")") : (dirigeant10.civiliteRepresentant10.personneLieePPLieuNaissanceCommune != null ? (dirigeant10.civiliteRepresentant10.personneLieePPLieuNaissanceCommune + ' ' + "(" + '' + dirigeant10.civiliteRepresentant10.personneLieePPDepartementNaissance.getId() + '' + ")") : (dirigeant10.civilitePersonnePhysique10.personneLieePPLieuNaissanceVille != null ? (dirigeant10.civilitePersonnePhysique10.personneLieePPLieuNaissanceVille + ' / ' + dirigeant10.civilitePersonnePhysique10.personneLieePPLieuNaissancePays) : (dirigeant10.civiliteRepresentant10.personneLieePPLieuNaissanceVille + ' / ' + dirigeant10.civiliteRepresentant10.personneLieePPLieuNaissancePays)));
formFieldsPers1['personneLieePPNationalite[9]'] = dirigeant10.civilitePersonnePhysique10.personneLieePPNationalite != null ? dirigeant10.civilitePersonnePhysique10.personneLieePPNationalite : dirigeant10.civiliteRepresentant10.personneLieePPNationalite;
formFieldsPers1['genreMasculin[9]'] = Value('id').of(dirigeant10.civilitePersonnePhysique10.personnaliteGenre).eq('genreMasculin') ? true : false;
formFieldsPers1['genreFeminin[9]']  = Value('id').of(dirigeant10.civilitePersonnePhysique10.personnaliteGenre).eq('genreFeminin')  ? true : false;
formFieldsPers1['dirigeantMajoritaireCapitalSocial[9]'] = dirigeant10.civilitePersonnePhysique10.dirigeantMajoritaireCapitalSocial != false ? true : false;
if (Value('id').of(dirigeant10.personnaliteJuridique).eq('personneMorale')) {
    formFieldsPers1['personneLieePMDenominationFormeJuridique[9]'] = dirigeant10.civilitePersonneMorale10.personneLieePMDenomination + '/' + dirigeant10.civilitePersonneMorale10.personneLieePMFormeJuridique;
    formFieldsPers1['personneLieePMLieuImmatriculation[9]'] = dirigeant10.civilitePersonneMorale10.personneLieePMNumeroIdentification + ' ' + dirigeant10.civilitePersonneMorale10.personneLieePMLieuImmatriculation;
}
formFieldsPers1['adresseDirigeant[9]'] = (dirigeant10.adresseDirigeant10.siegeAdresseNumVoie != null ? dirigeant10.adresseDirigeant10.siegeAdresseNumVoie : '') + ' ' + (dirigeant10.adresseDirigeant10.siegeAdresseIndiceVoie != null ? dirigeant10.adresseDirigeant10.siegeAdresseIndiceVoie : '') + ' ' + (dirigeant10.adresseDirigeant10.siegeAdresseTypeVoie != null ? dirigeant10.adresseDirigeant10.siegeAdresseTypeVoie : '') + ' ' + dirigeant10.adresseDirigeant10.siegeAdresseNomVoie
 + ' ' + (dirigeant10.adresseDirigeant10.siegeAdresseComplementVoie != null ? dirigeant10.adresseDirigeant10.siegeAdresseComplementVoie : '')
 + ' ' + (dirigeant10.adresseDirigeant10.siegeAdresseDistributionSpecialeVoie != null ? dirigeant10.adresseDirigeant10.siegeAdresseDistributionSpecialeVoie : '');
formFieldsPers1['siegeAdresseCodePostal[9]'] = dirigeant10.adresseDirigeant10.siegeAdresseCodePostal != null ? dirigeant10.adresseDirigeant10.siegeAdresseCodePostal : dirigeant10.adresseDirigeant10.siegeAdresseCodePostaEtranger;
formFieldsPers1['siegeAdresseCommune[9]'] = dirigeant10.adresseDirigeant10.siegeAdresseCommune != null ? dirigeant10.adresseDirigeant10.siegeAdresseCommune : (dirigeant10.adresseDirigeant10.siegeAdresseVille + ' / ' + dirigeant10.adresseDirigeant10.siegeAdressePays);

//Cadre 13 Origine de l'exploitation
//Précédent exploitant 2
formFieldsPers1['entrepriseLieeSirenPrecedentExploitant2'] = (activite.cadreOrigineActivite.precedentExploitant2.entrepriseLieeSirenPrecedentExploitant2 != null ? activite.cadreOrigineActivite.precedentExploitant2.entrepriseLieeSirenPrecedentExploitant2.split(' ').join('') : '');
formFieldsPers1['entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant2'] = activite.cadreOrigineActivite.precedentExploitant2.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant2;
formFieldsPers1['entrepriseLieeEntreprisePPNomUsagePrecedentExploitant2'] = activite.cadreOrigineActivite.precedentExploitant2.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant2;
formFieldsPers1['entrepriseLieeEntreprisePPPrenomPrecedentExploitant2'] = activite.cadreOrigineActivite.precedentExploitant2.entrepriseLieeEntreprisePPPrenomPrecedentExploitant2;
formFieldsPers1['entrepriseLieeEntreprisePPDenominationPrecedentExploitant2'] = activite.cadreOrigineActivite.precedentExploitant2.entrepriseLieeEntreprisePPDenominationPrecedentExploitant2;
formFieldsPers1['entrepriseLieeNumeroDetenteurPrecedentExploitant2'] = (activite.cadreOrigineActivite.precedentExploitant2.entrepriseLieeNumeroDetenteurPrecedentExploitant2 != null ? activite.cadreOrigineActivite.precedentExploitant2.entrepriseLieeNumeroDetenteurPrecedentExploitant2.split(' ') : '');
formFieldsPers1['entrepriseLieeNumeroExploitationPrecedentExploitant2'] = (activite.cadreOrigineActivite.precedentExploitant2.entrepriseLieeNumeroExploitationPrecedentExploitant2 != null ? activite.cadreOrigineActivite.precedentExploitant2.entrepriseLieeNumeroExploitationPrecedentExploitant2.split(' ').join('') : '');
//Précédent exploitant 3
formFieldsPers1['entrepriseLieeSirenPrecedentExploitant3'] = (activite.cadreOrigineActivite.precedentExploitant3.entrepriseLieeSirenPrecedentExploitant3 != null ? activite.cadreOrigineActivite.precedentExploitant3.entrepriseLieeSirenPrecedentExploitant3.split(' ').join('') : '');
formFieldsPers1['entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant3'] = activite.cadreOrigineActivite.precedentExploitant3.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant3;
formFieldsPers1['entrepriseLieeEntreprisePPNomUsagePrecedentExploitant3'] = activite.cadreOrigineActivite.precedentExploitant3.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant3;
formFieldsPers1['entrepriseLieeEntreprisePPPrenomPrecedentExploitant3'] = activite.cadreOrigineActivite.precedentExploitant3.entrepriseLieeEntreprisePPPrenomPrecedentExploitant3;
formFieldsPers1['entrepriseLieeEntreprisePPDenominationPrecedentExploitant3'] = activite.cadreOrigineActivite.precedentExploitant3.entrepriseLieeEntreprisePPDenominationPrecedentExploitant3;
formFieldsPers1['entrepriseLieeNumeroDetenteurPrecedentExploitant3'] = (activite.cadreOrigineActivite.precedentExploitant3.entrepriseLieeNumeroDetenteurPrecedentExploitant3 != null ? activite.cadreOrigineActivite.precedentExploitant3.entrepriseLieeNumeroDetenteurPrecedentExploitant3.split(' ') : '');
formFieldsPers1['entrepriseLieeNumeroExploitationPrecedentExploitant3'] = (activite.cadreOrigineActivite.precedentExploitant3.entrepriseLieeNumeroExploitationPrecedentExploitant3 != null ? activite.cadreOrigineActivite.precedentExploitant3.entrepriseLieeNumeroExploitationPrecedentExploitant3.split(' ').join('') : '');

//Cadre 14 Fusion-Scission

formFieldsPers1['fusionScissionDenomination'] = societe.cadreFusionScission.fusionScission.fusionScissionDenomination != null ? societe.cadreFusionScission.fusionScission.fusionScissionDenomination : '';
formFieldsPers1['fusionScissionFormeJuridique'] = societe.cadreFusionScission.fusionScission.fusionScissionFormeJuridique != null ? societe.cadreFusionScission.fusionScission.fusionScissionFormeJuridique : '';
formFieldsPers1['fusionScissionSiren'] = societe.cadreFusionScission.fusionScission.fusionScissionSiren != null ? societe.cadreFusionScission.fusionScission.fusionScissionSiren : '';
formFieldsPers1['fusionScissionGreffeImmatriculation'] = societe.cadreFusionScission.fusionScission.fusionScissionGreffeImmatriculation != null ? societe.cadreFusionScission.fusionScission.fusionScissionGreffeImmatriculation : '';
formFieldsPers1['fusionScissionAdresse'] = (societe.cadreFusionScission.adresseFusionScission.fusionScissionNumeroVoie != null ? societe.cadreFusionScission.adresseFusionScission.fusionScissionNumeroVoie : '') + ' ' + (societe.cadreFusionScission.adresseFusionScission.indiceVofusionScissionAdresseIndiceVoieieMandataire != null ? societe.cadreFusionScission.adresseFusionScission.adresseMandataire.fusionScissionAdresseIndiceVoie : '') + ' ' + (societe.cadreFusionScission.adresseFusionScission.fusionScissionAdresseTypeVoie != null ? societe.cadreFusionScission.adresseFusionScission.fusionScissionAdresseTypeVoie : '') + ' ' + societe.cadreFusionScission.adresseFusionScission.fusionScissionAdresseNomVoie + ' ' + (societe.cadreFusionScission.adresseFusionScission.fusionScissionAdresseComplementVoie != null ? societe.cadreFusionScission.adresseFusionScission.fusionScissionAdresseComplementVoie : '') + ' ' + (societe.cadreFusionScission.adresseFusionScission.fusionScissionAdresseDistributionSpecialeVoie != null ? societe.cadreFusionScission.adresseFusionScission.fusionScissionAdresseDistributionSpecialeVoie : '')
 + ' ' + societe.cadreFusionScission.adresseFusionScission.fusionScissionAdresseCodePostal + ' ' + societe.cadreFusionScission.adresseFusionScission.fusionScissionAdresseCommune;

//NSm Agricole
//Dirigeant 1
//NSm 1c

//cadre 1
formFieldsNSm1['formulaireDependanceAffiliation_NSMA'] = false;
formFieldsNSm1['formulaireDependanceModificationAffiliation_NSMA'] = false;
formFieldsNSm1['formulaireDependancePourGerant_NSMA'] = false;
formFieldsNSm1['formulaireDependancePourAssocie_NSMA'] = false;
formFieldsNSm1['formulaireDependanceM0_NSMA'] = true;
formFieldsNSm1['formulaireDependanceM2agricole_NSMA'] = false;
formFieldsNSm1['formulaireDependanceM3_NSMA'] = false;
formFieldsNSm1['formulaireDependanceIntercalaiaireNSM_NSMA'] = false;

//cadre 2
formFieldsNSm1['entrepriseDenomination_NSMA'] = societe.entrepriseDenomination;

//cadre 3

formFieldsNSm1['personneLieePPNomNaissance_NSMA'] = dirigeant1.civilitePersonnePhysique1.personneLieePPNomNaissance
formFieldsNSm1['personneLieePPNomUsage_NSMA'] = dirigeant1.civilitePersonnePhysique1.personneLieePPNomUsage ;

var prenoms = [];
for (i = 0; i < dirigeant1.civilitePersonnePhysique1.personneLieePPPrenom.size(); i++) {
    prenoms.push(dirigeant1.civilitePersonnePhysique1.personneLieePPPrenom[i]);
}
formFieldsNSm1['personneLieePPPrenom_NSMA'] = prenoms.toString();

//cadre 3B
if (dirigeant1.civilitePersonnePhysique1.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant1.civilitePersonnePhysique1.personneLieePPDateNaissance.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
    date = date.concat(dateTmp.getFullYear().toString());
    formFieldsNSm1['personneLieePPDateNaissance_NSMA'] = date;
} else
    formFieldsNSm1['personneLieePPDateNaissance_NSMA'] = '';

formFieldsNSm1['personneLieePPDepartementNaissance_NSMA'] = dirigeant1.civilitePersonnePhysique1.personneLieePPDepartementNaissance != null ? dirigeant1.civilitePersonnePhysique1.personneLieePPDepartementNaissance : '';
formFieldsNSm1['personneLieePPCommuneNaissance_NSMA'] = dirigeant1.civilitePersonnePhysique1.personneLieePPLieuNaissanceCommune != null ? dirigeant1.civilitePersonnePhysique1.personneLieePPLieuNaissanceCommune : (dirigeant1.civilitePersonnePhysique1.personneLieePPLieuNaissanceVille + ' / ' + dirigeant1.civilitePersonnePhysique1.personneLieePPLieuNaissancePays);

formFieldsNSm1['personneLieePPDomicilePerso_NSMA'] =
    (dirigeant1.adresseDirigeant1.siegeAdresseNumVoie != null ? dirigeant1.adresseDirigeant1.siegeAdresseNumVoie : '')
 + '  ' + (dirigeant1.adresseDirigeant1.siegeAdresseIndiceVoie != null ? dirigeant1.adresseDirigeant1.siegeAdresseIndiceVoie : '')
 + '  ' + (dirigeant1.adresseDirigeant1.siegeAdresseTypeVoie != null ? dirigeant1.adresseDirigeant1.siegeAdresseTypeVoie : '')
 + '  ' + dirigeant1.adresseDirigeant1.siegeAdresseNomVoie
 + '  ' + (dirigeant1.adresseDirigeant1.siegeAdresseComplementVoie != null ? dirigeant1.adresseDirigeant1.siegeAdresseComplementVoie : '')
 + '  ' + (dirigeant1.adresseDirigeant1.siegeAdresseDistributionSpecialeVoie != null ? dirigeant1.adresseDirigeant1.siegeAdresseDistributionSpecialeVoie : '');
formFieldsNSm1['personneLieePPCodePostalPerso_NSMA'] = dirigeant1.adresseDirigeant1.siegeAdresseCodePostal;
formFieldsNSm1['personneLieePPCommunePerso_NSMA'] = dirigeant1.adresseDirigeant1.siegeAdresseCommune;

//cadre 4
var nirDeclarantNSMA = dirigeant1.cadreDirigeantNonSalarie1.voletSocialNumeroSecuriteSocialNSMA;
if (nirDeclarantNSMA != null) {
    nirDeclarantNSMA = nirDeclarantNSMA.replace(/ /g, "");
    formFieldsNSm1['voletSocialNumeroSecuriteSocial_NSMA'] = nirDeclarantNSMA.substring(0, 13);
    formFieldsNSm1['voletSocialNumeroSecuriteSocialCle_NSMA'] = nirDeclarantNSMA.substring(13, 15);
}

formFieldsNSm1['voletSocialMSAOUI_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.voletSocialMSAOUINONNSMA ? true : false;
formFieldsNSm1['voletSocialMSANON_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.voletSocialMSAOUINONNSMA ? false : true;
formFieldsNSm1['voletSocialRegimeAssuranceMaladieRegimeGeneral_NSMA'] = Value('id').of(dirigeant1.cadreDirigeantNonSalarie1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralNSMA') ? true : false;
formFieldsNSm1['voletSocialRegimeAssuranceMaladieRegimeAgricole_NSMA'] = Value('id').of(dirigeant1.cadreDirigeantNonSalarie1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleNSMA') ? true : false;
formFieldsNSm1['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_NSMA'] = Value('id').of(dirigeant1.cadreDirigeantNonSalarie1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleNSMA') ? true : false;
formFieldsNSm1['voletSocialRegimeAssuranceMaladieRegimeAutre_NSMA'] = Value('id').of(dirigeant1.cadreDirigeantNonSalarie1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreNSMA') ? true : false;
formFieldsNSm1['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionNSMA;

formFieldsNSm1['voletSocialActiviteSimultaneeOUI_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.voletSocialActiviteSimultaneeOUINONNSMA ? true : false;
formFieldsNSm1['voletSocialActiviteSimultaneeNON_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.voletSocialActiviteSimultaneeOUINONNSMA ? false : true;

formFieldsNSm1['voletSocialActiviteSimultaneeSalarieAgricole_NSMA'] = Value('id').of(dirigeant1.cadreDirigeantNonSalarie1.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeSalarieAgricoleNSMA') ? true : false;
formFieldsNSm1['voletSocialActiviteSimultaneeSalarie_NSMA'] = Value('id').of(dirigeant1.cadreDirigeantNonSalarie1.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeSalarieNSMA') ? true : false;
formFieldsNSm1['voletSocialActiviteSimultaneeNonSalarieNonAgricole_NSMA'] = Value('id').of(dirigeant1.cadreDirigeantNonSalarie1.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeNonSalarieNonAgricoleNSMA') ? true : false;
formFieldsNSm1['voletSocialActiviteSimultaneeRetraite_NSMA'] = Value('id').of(dirigeant1.cadreDirigeantNonSalarie1.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeRetraiteNSMA') ? true : false;
formFieldsNSm1['voletSocialActiviteSimultaneePensionne_NSMA'] = Value('id').of(dirigeant1.cadreDirigeantNonSalarie1.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneePensionneNSMA') ? true : false;
formFieldsNSm1['voletSocialActiviteSimultaneeAutre_NSMA'] = Value('id').of(dirigeant1.cadreDirigeantNonSalarie1.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeAutreNSMA') ? true : false;
formFieldsNSm1['voletSocialActiviteSimultaneeAutrePrecision_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.voletSocialActiviteSimultaneeAutrePrecisionNSMA;
formFieldsNSm1['voletSocialActiviteSimultaneePensionOrganisme_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.voletSocialActiviteSimultaneePensionOrganismeNSMA;

formFieldsNSm1['voletSocialJeuneAgriculteurOUI_NSMA'] = Value('id').of(dirigeant1.cadreDirigeantNonSalarie1.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurOUINSMA') ? true : false;
formFieldsNSm1['voletSocialJeuneAgriculteurNON_NSMA'] = Value('id').of(dirigeant1.cadreDirigeantNonSalarie1.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurNONNSMA') ? true : false;
formFieldsNSm1['voletSocialJeuneAgriculteurENCOURS_NSMA'] = Value('id').of(dirigeant1.cadreDirigeantNonSalarie1.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurENCOURSNSMA') ? true : false;

formFieldsNSm1['voletSocialPacsRSAOUI_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.voletSocialPacsRSAOUINON ? true : false;
formFieldsNSm1['voletSocialPacsRSANON_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.voletSocialPacsRSAOUINON ? false : true;
formFieldsNSm1['voletSocialStatutConjointCollaborateur_NSMA'] = Value('id').of(dirigeant1.cadreDirigeantNonSalarie1.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointCollaborateurNSMA') ? true : false;
formFieldsNSm1['voletSocialStatutConjointSalarie_NSMA'] = Value('id').of(dirigeant1.cadreDirigeantNonSalarie1.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointSalarieNSMA') ? true : false;
formFieldsNSm1['voletSocialStatutConjointAssocie_NSMA'] = Value('id').of(dirigeant1.cadreDirigeantNonSalarie1.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointAssocieNSMA') ? true : false;

formFieldsNSm1['voletSocialConjointRegimeAssuranceOUI_NSMA'] = Value('id').of(dirigeant1.cadreDirigeantNonSalarie1.voletSocialConjointRegimeAssuranceOUINONNSMA).eq('voletSocialConjointRegimeAssuranceOUINSMA') ? true : false;
formFieldsNSm1['voletSocialConjointRegimeAssuranceNON_NSMA'] = Value('id').of(dirigeant1.cadreDirigeantNonSalarie1.voletSocialConjointRegimeAssuranceOUINONNSMA).eq('voletSocialConjointRegimeAssuranceNONNSMA') ? true : false;

var nirConjointNSMA = dirigeant1.cadreDirigeantNonSalarie1.voletSocialConjointNumeroSecuriteSocialNSMA;
if (nirConjointNSMA != null) {
    nirConjointNSMA = nirConjointNSMA.replace(/ /g, "");
    formFieldsNSm1['voletSocialConjointNumeroSecuriteSocial_NSMA'] = nirConjointNSMA.substring(0, 13);
    formFieldsNSm1['voletSocialConjointNumeroSecuriteSocialCle_NSMA'] = nirConjointNSMA.substring(13, 15);
} else {
    formFieldsNSm1['voletSocialConjointNumeroSecuriteSocial_NSMA'] = '';
    formFieldsNSm1['voletSocialConjointNumeroSecuriteSocialCle_NSMA'] = '';
}

//-----------------------AYANT-DROIT

//ayant droit numero 1
formFieldsNSm1['ayantDroitIdentiteNomNaissancePrenom1_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration1 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits11.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits11.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm1['ayantDroitNumeroSecuriteSociale1_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration1 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits11.presenceNumSSAM ? dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits11.ayantDroitNumeroSecuriteSociale1NSMA : '') : '';
formFieldsNSm1['ayantDroitDateNaissance1_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration1 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits11.presenceNumSSAM ? '' : dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits11.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm1['ayantDroitLieuNaissanceCommune1_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration1 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits11.presenceNumSSAM ? '' : (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits11.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits11.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits11.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm1['ayantDroitLienParente1_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration1 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits11.ayantDroitLienParenteNSMA) : '';
formFieldsNSm1['ayantDroitEnfantScolarise1OUI_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration1 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits11.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm1['ayantDroitEnfantScolarise1NON_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration1 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits11.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm1['ayantDroitNationalite1_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration1 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits11.ayantDroitNationaliteNSMA) : '';

// numero 2
formFieldsNSm1['ayantDroitIdentiteNomNaissancePrenom2_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration2 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits12.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits12.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm1['ayantDroitNumeroSecuriteSociale2_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration2 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits12.presenceNumSSAM ? dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits12.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm1['ayantDroitDateNaissance2_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration2 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits12.presenceNumSSAM ? '' : dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits12.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm1['ayantDroitLieuNaissanceCommune2_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration2 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits12.presenceNumSSAM ? '' : (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits12.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits12.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits12.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm1['ayantDroitLienParente2_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration2 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits12.ayantDroitLienParenteNSMA) : '';
formFieldsNSm1['ayantDroitEnfantScolarise2OUI_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration2 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits12.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm1['ayantDroitEnfantScolarise2NON_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration2 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits12.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm1['ayantDroitNationalite2_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration2 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits12.ayantDroitNationaliteNSMA) : '';

//numero 3
formFieldsNSm1['ayantDroitIdentiteNomNaissancePrenom3_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration3 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits13.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits13.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm1['ayantDroitNumeroSecuriteSociale3_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration3 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits13.presenceNumSSAM ? dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits13.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm1['ayantDroitDateNaissance3_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration3 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits13.presenceNumSSAM ? '' : dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits13.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm1['ayantDroitLieuNaissanceCommune3_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration3 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits13.presenceNumSSAM ? '' : (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits13.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits13.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits13.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm1['ayantDroitLienParente3_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration3 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits13.ayantDroitLienParenteNSMA) : '';
formFieldsNSm1['ayantDroitEnfantScolarise3OUI_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration3 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits13.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm1['ayantDroitEnfantScolarise3NON_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration3 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits13.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm1['ayantDroitNationalite3_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration3 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits13.ayantDroitNationaliteNSMA) : '';

//numero 4
formFieldsNSm1['ayantDroitIdentiteNomNaissancePrenom4_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration4 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits14.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits14.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm1['ayantDroitNumeroSecuriteSociale4_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration4 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits14.presenceNumSSAM ? dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits14.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm1['ayantDroitDateNaissance4_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration4 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits14.presenceNumSSAM ? '' : dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits14.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm1['ayantDroitLieuNaissanceCommune4_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration4 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits14.presenceNumSSAM ? '' : (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits14.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits14.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits14.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm1['ayantDroitLienParente4_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration4 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits14.ayantDroitLienParenteNSMA) : '';
formFieldsNSm1['ayantDroitEnfantScolarise4OUI_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration4 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits14.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm1['ayantDroitEnfantScolarise4NON_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration4 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits14.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm1['ayantDroitNationalite4_NSMA'] = dirigeant1.cadreDirigeantNonSalarie1.ayantDroitsDeclaration4 ? (dirigeant1.cadreDirigeantNonSalarie1.ayantsDroits14.ayantDroitNationaliteNSMA) : '';

//-----------------------
//Cadre 4B
formFieldsNSm1['voletSocialMembreGAECOUI_NSMA'] = Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant1.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECOUINSMA') ? true : false;
formFieldsNSm1['voletSocialMembreGAECNON_NSMA'] = Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant1.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECNONNSMA') ? true : false;

//Cadre 5
formFieldsNSm1['renseignementsComplementairesObservations_NSMA'] = $m0Agricole.cadreDirigeantGroup.cadreDirigeant1.cadreObservationsNSM1.renseignementsComplementairesObservationsNSMA;

//Cadre 6
formFieldsNSm1['renseignementsComplementairesLeDeclarant_NSMA'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal') != false ? true : false;
formFieldsNSm1['renseignementsComplementairesLeMandataire_NSMA'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire') != false ? true : false;
formFieldsNSm1['formaliteSignataire_NSMA'] = (tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire :'')
     + ' ' + (tag.adresseMandataire.numeroVoieMandataire != null ? tag.adresseMandataire.numeroVoieMandataire : '')
     + ' ' + (tag.adresseMandataire.indiceVoieMandataire != null ? tag.adresseMandataire.indiceVoieMandataire : '')
     + ' ' + (tag.adresseMandataire.typeVoieMandataire != null ? tag.adresseMandataire.typeVoieMandataire : '')
     + ' ' + (tag.adresseMandataire.nomVoieMandataire != null ? tag.adresseMandataire.nomVoieMandataire : '')
     + ' ' + (tag.adresseMandataire.complementVoieMandataire != null ? tag.adresseMandataire.complementVoieMandataire : '')
     + ' ' + (tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
     + ' ' + (tag.adresseMandataire.codePostalMandataire != null ? tag.adresseMandataire.codePostalMandataire : '')
	 + ' ' + (tag.adresseMandataire.villeAdresseMandataire != null ? tag.adresseMandataire.villeAdresseMandataire : ''); 
formFieldsNSm1['formaliteSignatureLieu_NSMA']= tag.formaliteSignatureLieu;
if (tag.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSm1['formaliteSignatureDate_NSMA'] = tag.formaliteSignatureDate;
}

formFieldsNSm1['signature_NSMA'] = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";

// A remplir avec le formulaire principal
formFieldsNSm1['formaliteSignatureNBIntercalaire_NSMA'] = '';

//Dirigeant 2
//NSm 2
//cadre 1
formFieldsNSm2['formulaireDependanceAffiliation_NSMA'] = false;
formFieldsNSm2['formulaireDependanceModificationAffiliation_NSMA'] = false;
formFieldsNSm2['formulaireDependancePourGerant_NSMA'] = false;
formFieldsNSm2['formulaireDependancePourAssocie_NSMA'] = false;
formFieldsNSm2['formulaireDependanceM0_NSMA'] = true;
formFieldsNSm2['formulaireDependanceM2agricole_NSMA'] = false;
formFieldsNSm2['formulaireDependanceM3_NSMA'] = false;
formFieldsNSm2['formulaireDependanceIntercalaiaireNSM_NSMA'] = false;

//cadre 2
formFieldsNSm2['entrepriseDenomination_NSMA'] = societe.entrepriseDenomination;

//cadre 3

formFieldsNSm2['personneLieePPNomNaissance_NSMA'] = dirigeant2.civilitePersonnePhysique2.personneLieePPNomNaissance;
formFieldsNSm2['personneLieePPNomUsage_NSMA'] = dirigeant2.civilitePersonnePhysique2.personneLieePPNomUsage;
var prenoms = [];
for (i = 0; i < dirigeant2.civilitePersonnePhysique2.personneLieePPPrenom.size(); i++) {
    prenoms.push(dirigeant2.civilitePersonnePhysique2.personneLieePPPrenom[i]);
}
formFieldsNSm2['personneLieePPPrenom_NSMA'] = prenoms.toString();

//cadre 3B
if (dirigeant2.civilitePersonnePhysique2.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant2.civilitePersonnePhysique2.personneLieePPDateNaissance.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
    date = date.concat(dateTmp.getFullYear().toString());
    formFieldsNSm2['personneLieePPDateNaissance_NSMA'] = date;
} else
    formFieldsNSm2['personneLieePPDateNaissance_NSMA'] = '';

formFieldsNSm2['personneLieePPDepartementNaissance_NSMA'] = dirigeant2.civilitePersonnePhysique2.personneLieePPDepartementNaissance != null ? dirigeant2.civilitePersonnePhysique2.personneLieePPDepartementNaissance : '';
formFieldsNSm2['personneLieePPCommuneNaissance_NSMA'] = dirigeant2.civilitePersonnePhysique2.personneLieePPLieuNaissanceCommune != null ? dirigeant2.civilitePersonnePhysique2.personneLieePPLieuNaissanceCommune : (dirigeant2.civilitePersonnePhysique2.personneLieePPLieuNaissanceVille + ' / ' + dirigeant2.civilitePersonnePhysique2.personneLieePPLieuNaissancePays);

formFieldsNSm2['personneLieePPDomicilePerso_NSMA'] =
    (dirigeant2.adresseDirigeant2.siegeAdresseNumVoie != null ? dirigeant2.adresseDirigeant2.siegeAdresseNumVoie : '')
 + '  ' + (dirigeant2.adresseDirigeant2.siegeAdresseIndiceVoie != null ? dirigeant2.adresseDirigeant2.siegeAdresseIndiceVoie : '')
 + '  ' + (dirigeant2.adresseDirigeant2.siegeAdresseTypeVoie != null ? dirigeant2.adresseDirigeant2.siegeAdresseTypeVoie : '')
 + '  ' + dirigeant2.adresseDirigeant2.siegeAdresseNomVoie
 + '  ' + (dirigeant2.adresseDirigeant2.siegeAdresseComplementVoie != null ? dirigeant2.adresseDirigeant2.siegeAdresseComplementVoie : '')
 + '  ' + (dirigeant2.adresseDirigeant2.siegeAdresseDistributionSpecialeVoie != null ? dirigeant2.adresseDirigeant2.siegeAdresseDistributionSpecialeVoie : '');
formFieldsNSm2['personneLieePPCodePostalPerso_NSMA'] = dirigeant2.adresseDirigeant2.siegeAdresseCodePostal;
formFieldsNSm2['personneLieePPCommunePerso_NSMA'] = dirigeant2.adresseDirigeant2.siegeAdresseCommune;

//cadre 4
var nirDeclarantNSMA = dirigeant2.cadreDirigeantNonSalarie2.voletSocialNumeroSecuriteSocialNSMA;
if (nirDeclarantNSMA != null) {
    nirDeclarantNSMA = nirDeclarantNSMA.replace(/ /g, "");
    formFieldsNSm2['voletSocialNumeroSecuriteSocial_NSMA'] = nirDeclarantNSMA.substring(0, 13);
    formFieldsNSm2['voletSocialNumeroSecuriteSocialCle_NSMA'] = nirDeclarantNSMA.substring(13, 15);
}

formFieldsNSm2['voletSocialMSAOUI_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.voletSocialMSAOUINONNSMA ? true : false;
formFieldsNSm2['voletSocialMSANON_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.voletSocialMSAOUINONNSMA ? false : true;
formFieldsNSm2['voletSocialRegimeAssuranceMaladieRegimeGeneral_NSMA'] = Value('id').of(dirigeant2.cadreDirigeantNonSalarie2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralNSMA') ? true : false;
formFieldsNSm2['voletSocialRegimeAssuranceMaladieRegimeAgricole_NSMA'] = Value('id').of(dirigeant2.cadreDirigeantNonSalarie2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleNSMA') ? true : false;
formFieldsNSm2['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_NSMA'] = Value('id').of(dirigeant2.cadreDirigeantNonSalarie2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleNSMA') ? true : false;
formFieldsNSm2['voletSocialRegimeAssuranceMaladieRegimeAutre_NSMA'] = Value('id').of(dirigeant2.cadreDirigeantNonSalarie2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreNSMA') ? true : false;
formFieldsNSm2['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionNSMA;

formFieldsNSm2['voletSocialActiviteSimultaneeOUI_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.voletSocialActiviteSimultaneeOUINONNSMA ? true : false;
formFieldsNSm2['voletSocialActiviteSimultaneeNON_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.voletSocialActiviteSimultaneeOUINONNSMA ? false : true;

formFieldsNSm2['voletSocialActiviteSimultaneeSalarieAgricole_NSMA'] = Value('id').of(dirigeant2.cadreDirigeantNonSalarie2.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeSalarieAgricoleNSMA') ? true : false;
formFieldsNSm2['voletSocialActiviteSimultaneeSalarie_NSMA'] = Value('id').of(dirigeant2.cadreDirigeantNonSalarie2.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeSalarieNSMA') ? true : false;
formFieldsNSm2['voletSocialActiviteSimultaneeNonSalarieNonAgricole_NSMA'] = Value('id').of(dirigeant2.cadreDirigeantNonSalarie2.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeNonSalarieNonAgricoleNSMA') ? true : false;
formFieldsNSm2['voletSocialActiviteSimultaneeRetraite_NSMA'] = Value('id').of(dirigeant2.cadreDirigeantNonSalarie2.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeRetraiteNSMA') ? true : false;
formFieldsNSm2['voletSocialActiviteSimultaneePensionne_NSMA'] = Value('id').of(dirigeant2.cadreDirigeantNonSalarie2.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneePensionneNSMA') ? true : false;
formFieldsNSm2['voletSocialActiviteSimultaneeAutre_NSMA'] = Value('id').of(dirigeant2.cadreDirigeantNonSalarie2.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeAutreNSMA') ? true : false;
formFieldsNSm2['voletSocialActiviteSimultaneeAutrePrecision_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.voletSocialActiviteSimultaneeAutrePrecisionNSMA;
formFieldsNSm2['voletSocialActiviteSimultaneePensionOrganisme_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.voletSocialActiviteSimultaneePensionOrganismeNSMA;

formFieldsNSm2['voletSocialJeuneAgriculteurOUI_NSMA'] = Value('id').of(dirigeant2.cadreDirigeantNonSalarie2.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurOUINSMA') ? true : false;
formFieldsNSm2['voletSocialJeuneAgriculteurNON_NSMA'] = Value('id').of(dirigeant2.cadreDirigeantNonSalarie2.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurNONNSMA') ? true : false;
formFieldsNSm2['voletSocialJeuneAgriculteurENCOURS_NSMA'] = Value('id').of(dirigeant2.cadreDirigeantNonSalarie2.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurENCOURSNSMA') ? true : false;

formFieldsNSm2['voletSocialPacsRSAOUI_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.voletSocialPacsRSAOUINON ? true : false;
formFieldsNSm2['voletSocialPacsRSANON_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.voletSocialPacsRSAOUINON ? false : true;
formFieldsNSm2['voletSocialStatutConjointCollaborateur_NSMA'] = Value('id').of(dirigeant2.cadreDirigeantNonSalarie2.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointCollaborateurNSMA') ? true : false;
formFieldsNSm2['voletSocialStatutConjointSalarie_NSMA'] = Value('id').of(dirigeant2.cadreDirigeantNonSalarie2.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointSalarieNSMA') ? true : false;
formFieldsNSm2['voletSocialStatutConjointAssocie_NSMA'] = Value('id').of(dirigeant2.cadreDirigeantNonSalarie2.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointAssocieNSMA') ? true : false;

formFieldsNSm2['voletSocialConjointRegimeAssuranceOUI_NSMA'] = Value('id').of(dirigeant2.cadreDirigeantNonSalarie2.voletSocialConjointRegimeAssuranceOUINONNSMA).eq('voletSocialConjointRegimeAssuranceOUINSMA') ? true : false;
formFieldsNSm2['voletSocialConjointRegimeAssuranceNON_NSMA'] = Value('id').of(dirigeant2.cadreDirigeantNonSalarie2.voletSocialConjointRegimeAssuranceOUINONNSMA).eq('voletSocialConjointRegimeAssuranceNONNSMA') ? true : false;

var nirConjointNSMA = dirigeant2.cadreDirigeantNonSalarie2.voletSocialConjointNumeroSecuriteSocialNSMA;
if (nirConjointNSMA != null) {
    nirConjointNSMA = nirConjointNSMA.replace(/ /g, "");
    formFieldsNSm2['voletSocialConjointNumeroSecuriteSocial_NSMA'] = nirConjointNSMA.substring(0, 13);
    formFieldsNSm2['voletSocialConjointNumeroSecuriteSocialCle_NSMA'] = nirConjointNSMA.substring(13, 15);
} else {
    formFieldsNSm2['voletSocialConjointNumeroSecuriteSocial_NSMA'] = '';
    formFieldsNSm2['voletSocialConjointNumeroSecuriteSocialCle_NSMA'] = '';
}

//-----------------------AYANT-DROIT

//ayant droit numero 1
formFieldsNSm2['ayantDroitIdentiteNomNaissancePrenom1_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration1 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits21.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits21.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm2['ayantDroitNumeroSecuriteSociale1_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration1 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits21.presenceNumSSAM ? dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits21.ayantDroitNumeroSecuriteSociale1NSMA : '') : '';
formFieldsNSm2['ayantDroitDateNaissance1_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration1 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits21.presenceNumSSAM ? '' : dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits21.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm2['ayantDroitLieuNaissanceCommune1_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration1 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits21.presenceNumSSAM ? '' : (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits21.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits21.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits21.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm2['ayantDroitLienParente1_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration1 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits21.ayantDroitLienParenteNSMA) : '';
formFieldsNSm2['ayantDroitEnfantScolarise1OUI_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration1 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits21.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm2['ayantDroitEnfantScolarise1NON_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration1 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits21.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm2['ayantDroitNationalite1_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration1 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits21.ayantDroitNationaliteNSMA) : '';

// numero 2
formFieldsNSm2['ayantDroitIdentiteNomNaissancePrenom2_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration2 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits22.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits22.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm2['ayantDroitNumeroSecuriteSociale2_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration2 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits22.presenceNumSSAM ? dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits22.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm2['ayantDroitDateNaissance2_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration2 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits22.presenceNumSSAM ? '' : dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits22.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm2['ayantDroitLieuNaissanceCommune2_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration2 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits22.presenceNumSSAM ? '' : (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits22.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits22.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits22.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm2['ayantDroitLienParente2_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration2 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits22.ayantDroitLienParenteNSMA) : '';
formFieldsNSm2['ayantDroitEnfantScolarise2OUI_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration2 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits22.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm2['ayantDroitEnfantScolarise2NON_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration2 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits22.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm2['ayantDroitNationalite2_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration2 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits22.ayantDroitNationaliteNSMA) : '';

//numero 3
formFieldsNSm2['ayantDroitIdentiteNomNaissancePrenom3_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration3 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits23.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits23.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm2['ayantDroitNumeroSecuriteSociale3_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration3 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits23.presenceNumSSAM ? dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits23.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm2['ayantDroitDateNaissance3_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration3 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits23.presenceNumSSAM ? '' : dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits23.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm2['ayantDroitLieuNaissanceCommune3_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration3 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits23.presenceNumSSAM ? '' : (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits23.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits23.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits23.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm2['ayantDroitLienParente3_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration3 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits23.ayantDroitLienParenteNSMA) : '';
formFieldsNSm2['ayantDroitEnfantScolarise3OUI_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration3 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits23.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm2['ayantDroitEnfantScolarise3NON_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration3 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits23.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm2['ayantDroitNationalite3_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration3 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits23.ayantDroitNationaliteNSMA) : '';

//numero 4
formFieldsNSm2['ayantDroitIdentiteNomNaissancePrenom4_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration4 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits24.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits24.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm2['ayantDroitNumeroSecuriteSociale4_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration4 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits24.presenceNumSSAM ? dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits24.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm2['ayantDroitDateNaissance4_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration4 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits24.presenceNumSSAM ? '' : dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits24.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm2['ayantDroitLieuNaissanceCommune4_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration4 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits24.presenceNumSSAM ? '' : (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits24.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits24.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits24.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm2['ayantDroitLienParente4_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration4 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits24.ayantDroitLienParenteNSMA) : '';
formFieldsNSm2['ayantDroitEnfantScolarise4OUI_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration4 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits24.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm2['ayantDroitEnfantScolarise4NON_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration4 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits24.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm2['ayantDroitNationalite4_NSMA'] = dirigeant2.cadreDirigeantNonSalarie2.ayantDroitsDeclaration4 ? (dirigeant2.cadreDirigeantNonSalarie2.ayantsDroits24.ayantDroitNationaliteNSMA) : '';

//-----------------------
//Cadre 4B
formFieldsNSm2['voletSocialMembreGAECOUI_NSMA'] = Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant2.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECOUINSMA') ? true : false;
formFieldsNSm2['voletSocialMembreGAECNON_NSMA'] = Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant2.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECNONNSMA') ? true : false;

//Cadre 5
formFieldsNSm2['renseignementsComplementairesObservations_NSMA'] = $m0Agricole.cadreDirigeantGroup.cadreDirigeant2.cadreObservationsNSM2.renseignementsComplementairesObservationsNSMA;

//Cadre 6
formFieldsNSm2['renseignementsComplementairesLeDeclarant_NSMA'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal') != false ? true : false;
formFieldsNSm2['renseignementsComplementairesLeMandataire_NSMA'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire') != false ? true : false;
formFieldsNSm2['formaliteSignataire_NSMA'] = (tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire :'')
+ ' ' + (tag.adresseMandataire.numeroVoieMandataire != null ? tag.adresseMandataire.numeroVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.indiceVoieMandataire != null ? tag.adresseMandataire.indiceVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.typeVoieMandataire != null ? tag.adresseMandataire.typeVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.nomVoieMandataire != null ? tag.adresseMandataire.nomVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.complementVoieMandataire != null ? tag.adresseMandataire.complementVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
+ ' ' + (tag.adresseMandataire.codePostalMandataire != null ? tag.adresseMandataire.codePostalMandataire : '')
+ ' ' + (tag.adresseMandataire.villeAdresseMandataire != null ? tag.adresseMandataire.villeAdresseMandataire : ''); 
formFieldsNSm2['formaliteSignatureLieu_NSMA'] = tag.formaliteSignatureLieu;
if (tag.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSm2['formaliteSignatureDate_NSMA'] = tag.formaliteSignatureDate;
}

formFieldsNSm2['signature_NSMA'] = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";

// A remplir avec le formulaire principal
formFieldsNSm2['formaliteSignatureNBIntercalaire_NSMA'] = '';

//NSm 3

//cadre 1
formFieldsNSm3['formulaireDependanceAffiliation_NSMA'] = false;
formFieldsNSm3['formulaireDependanceModificationAffiliation_NSMA'] = false;
formFieldsNSm3['formulaireDependancePourGerant_NSMA'] = false;
formFieldsNSm3['formulaireDependancePourAssocie_NSMA'] = false;
formFieldsNSm3['formulaireDependanceM0_NSMA'] = true;
formFieldsNSm3['formulaireDependanceM2agricole_NSMA'] = false;
formFieldsNSm3['formulaireDependanceM3_NSMA'] = false;
formFieldsNSm3['formulaireDependanceIntercalaiaireNSM_NSMA'] = false;

//cadre 2
formFieldsNSm3['entrepriseDenomination_NSMA'] = societe.entrepriseDenomination;

//cadre 3

formFieldsNSm3['personneLieePPNomNaissance_NSMA'] = dirigeant3.civilitePersonnePhysique3.personneLieePPNomNaissance;
formFieldsNSm3['personneLieePPNomUsage_NSMA'] = dirigeant3.civilitePersonnePhysique3.personneLieePPNomUsage;
var prenoms = [];
for (i = 0; i < dirigeant3.civilitePersonnePhysique3.personneLieePPPrenom.size(); i++) {
    prenoms.push(dirigeant3.civilitePersonnePhysique3.personneLieePPPrenom[i]);
}
formFieldsNSm3['personneLieePPPrenom_NSMA'] = prenoms.toString();

//cadre 3B
if (dirigeant3.civilitePersonnePhysique3.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant3.civilitePersonnePhysique3.personneLieePPDateNaissance.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
    date = date.concat(dateTmp.getFullYear().toString());
    formFieldsNSm3['personneLieePPDateNaissance_NSMA'] = date;
} else
    formFieldsNSm3['personneLieePPDateNaissance_NSMA'] = '';

formFieldsNSm3['personneLieePPDepartementNaissance_NSMA'] = dirigeant3.civilitePersonnePhysique3.personneLieePPDepartementNaissance != null ? dirigeant3.civilitePersonnePhysique3.personneLieePPDepartementNaissance : '';
formFieldsNSm3['personneLieePPCommuneNaissance_NSMA'] = dirigeant3.civilitePersonnePhysique3.personneLieePPLieuNaissanceCommune != null ? dirigeant3.civilitePersonnePhysique3.personneLieePPLieuNaissanceCommune : (dirigeant3.civilitePersonnePhysique3.personneLieePPLieuNaissanceVille + ' / ' + dirigeant3.civilitePersonnePhysique3.personneLieePPLieuNaissancePays);

formFieldsNSm3['personneLieePPDomicilePerso_NSMA'] =
    (dirigeant3.adresseDirigeant3.siegeAdresseNumVoie != null ? dirigeant3.adresseDirigeant3.siegeAdresseNumVoie : '')
 + '  ' + (dirigeant3.adresseDirigeant3.siegeAdresseIndiceVoie != null ? dirigeant3.adresseDirigeant3.siegeAdresseIndiceVoie : '')
 + '  ' + (dirigeant3.adresseDirigeant3.siegeAdresseTypeVoie != null ? dirigeant3.adresseDirigeant3.siegeAdresseTypeVoie : '')
 + '  ' + dirigeant3.adresseDirigeant3.siegeAdresseNomVoie
 + '  ' + (dirigeant3.adresseDirigeant3.siegeAdresseComplementVoie != null ? dirigeant3.adresseDirigeant3.siegeAdresseComplementVoie : '')
 + '  ' + (dirigeant3.adresseDirigeant3.siegeAdresseDistributionSpecialeVoie != null ? dirigeant3.adresseDirigeant3.siegeAdresseDistributionSpecialeVoie : '');
formFieldsNSm3['personneLieePPCodePostalPerso_NSMA'] = dirigeant3.adresseDirigeant3.siegeAdresseCodePostal;
formFieldsNSm3['personneLieePPCommunePerso_NSMA'] = dirigeant3.adresseDirigeant3.siegeAdresseCommune;

//cadre 4
var nirDeclarantNSMA = dirigeant3.cadreDirigeantNonSalarie3.voletSocialNumeroSecuriteSocialNSMA;
if (nirDeclarantNSMA != null) {
    nirDeclarantNSMA = nirDeclarantNSMA.replace(/ /g, "");
    formFieldsNSm3['voletSocialNumeroSecuriteSocial_NSMA'] = nirDeclarantNSMA.substring(0, 13);
    formFieldsNSm3['voletSocialNumeroSecuriteSocialCle_NSMA'] = nirDeclarantNSMA.substring(13, 15);
}

formFieldsNSm3['voletSocialMSAOUI_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.voletSocialMSAOUINONNSMA ? true : false;
formFieldsNSm3['voletSocialMSANON_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.voletSocialMSAOUINONNSMA ? false : true;
formFieldsNSm3['voletSocialRegimeAssuranceMaladieRegimeGeneral_NSMA'] = Value('id').of(dirigeant3.cadreDirigeantNonSalarie3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralNSMA') ? true : false;
formFieldsNSm3['voletSocialRegimeAssuranceMaladieRegimeAgricole_NSMA'] = Value('id').of(dirigeant3.cadreDirigeantNonSalarie3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleNSMA') ? true : false;
formFieldsNSm3['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_NSMA'] = Value('id').of(dirigeant3.cadreDirigeantNonSalarie3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleNSMA') ? true : false;
formFieldsNSm3['voletSocialRegimeAssuranceMaladieRegimeAutre_NSMA'] = Value('id').of(dirigeant3.cadreDirigeantNonSalarie3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreNSMA') ? true : false;
formFieldsNSm3['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionNSMA;

formFieldsNSm3['voletSocialActiviteSimultaneeOUI_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.voletSocialActiviteSimultaneeOUINONNSMA ? true : false;
formFieldsNSm3['voletSocialActiviteSimultaneeNON_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.voletSocialActiviteSimultaneeOUINONNSMA ? false : true;

formFieldsNSm3['voletSocialActiviteSimultaneeSalarieAgricole_NSMA'] = Value('id').of(dirigeant3.cadreDirigeantNonSalarie3.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeSalarieAgricoleNSMA') ? true : false;
formFieldsNSm3['voletSocialActiviteSimultaneeSalarie_NSMA'] = Value('id').of(dirigeant3.cadreDirigeantNonSalarie3.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeSalarieNSMA') ? true : false;
formFieldsNSm3['voletSocialActiviteSimultaneeNonSalarieNonAgricole_NSMA'] = Value('id').of(dirigeant3.cadreDirigeantNonSalarie3.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeNonSalarieNonAgricoleNSMA') ? true : false;
formFieldsNSm3['voletSocialActiviteSimultaneeRetraite_NSMA'] = Value('id').of(dirigeant3.cadreDirigeantNonSalarie3.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeRetraiteNSMA') ? true : false;
formFieldsNSm3['voletSocialActiviteSimultaneePensionne_NSMA'] = Value('id').of(dirigeant3.cadreDirigeantNonSalarie3.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneePensionneNSMA') ? true : false;
formFieldsNSm3['voletSocialActiviteSimultaneeAutre_NSMA'] = Value('id').of(dirigeant3.cadreDirigeantNonSalarie3.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeAutreNSMA') ? true : false;
formFieldsNSm3['voletSocialActiviteSimultaneeAutrePrecision_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.voletSocialActiviteSimultaneeAutrePrecisionNSMA;
formFieldsNSm3['voletSocialActiviteSimultaneePensionOrganisme_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.voletSocialActiviteSimultaneePensionOrganismeNSMA;

formFieldsNSm3['voletSocialJeuneAgriculteurOUI_NSMA'] = Value('id').of(dirigeant3.cadreDirigeantNonSalarie3.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurOUINSMA') ? true : false;
formFieldsNSm3['voletSocialJeuneAgriculteurNON_NSMA'] = Value('id').of(dirigeant3.cadreDirigeantNonSalarie3.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurNONNSMA') ? true : false;
formFieldsNSm3['voletSocialJeuneAgriculteurENCOURS_NSMA'] = Value('id').of(dirigeant3.cadreDirigeantNonSalarie3.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurENCOURSNSMA') ? true : false;

formFieldsNSm3['voletSocialPacsRSAOUI_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.voletSocialPacsRSAOUINON ? true : false;
formFieldsNSm3['voletSocialPacsRSANON_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.voletSocialPacsRSAOUINON ? false : true;
formFieldsNSm3['voletSocialStatutConjointCollaborateur_NSMA'] = Value('id').of(dirigeant3.cadreDirigeantNonSalarie3.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointCollaborateurNSMA') ? true : false;
formFieldsNSm3['voletSocialStatutConjointSalarie_NSMA'] = Value('id').of(dirigeant3.cadreDirigeantNonSalarie3.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointSalarieNSMA') ? true : false;
formFieldsNSm3['voletSocialStatutConjointAssocie_NSMA'] = Value('id').of(dirigeant3.cadreDirigeantNonSalarie3.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointAssocieNSMA') ? true : false;

formFieldsNSm3['voletSocialConjointRegimeAssuranceOUI_NSMA'] = Value('id').of(dirigeant3.cadreDirigeantNonSalarie3.voletSocialConjointRegimeAssuranceOUINONNSMA).eq('voletSocialConjointRegimeAssuranceOUINSMA') ? true : false;
formFieldsNSm3['voletSocialConjointRegimeAssuranceNON_NSMA'] = Value('id').of(dirigeant3.cadreDirigeantNonSalarie3.voletSocialConjointRegimeAssuranceOUINONNSMA).eq('voletSocialConjointRegimeAssuranceNONNSMA') ? true : false;

var nirConjointNSMA = dirigeant3.cadreDirigeantNonSalarie3.voletSocialConjointNumeroSecuriteSocialNSMA;
if (nirConjointNSMA != null) {
    nirConjointNSMA = nirConjointNSMA.replace(/ /g, "");
    formFieldsNSm3['voletSocialConjointNumeroSecuriteSocial_NSMA'] = nirConjointNSMA.substring(0, 13);
    formFieldsNSm3['voletSocialConjointNumeroSecuriteSocialCle_NSMA'] = nirConjointNSMA.substring(13, 15);
} else {
    formFieldsNSm3['voletSocialConjointNumeroSecuriteSocial_NSMA'] = '';
    formFieldsNSm3['voletSocialConjointNumeroSecuriteSocialCle_NSMA'] = '';
}

//-----------------------AYANT-DROIT

//ayant droit numero 1
formFieldsNSm3['ayantDroitIdentiteNomNaissancePrenom1_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration1 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits31.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits31.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm3['ayantDroitNumeroSecuriteSociale1_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration1 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits31.presenceNumSSAM ? dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits31.ayantDroitNumeroSecuriteSociale1NSMA : '') : '';
formFieldsNSm3['ayantDroitDateNaissance1_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration1 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits31.presenceNumSSAM ? '' : dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits31.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm3['ayantDroitLieuNaissanceCommune1_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration1 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits31.presenceNumSSAM ? '' : (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits31.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits31.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits31.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm3['ayantDroitLienParente1_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration1 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits31.ayantDroitLienParenteNSMA) : '';
formFieldsNSm3['ayantDroitEnfantScolarise1OUI_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration1 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits31.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm3['ayantDroitEnfantScolarise1NON_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration1 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits31.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm3['ayantDroitNationalite1_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration1 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits31.ayantDroitNationaliteNSMA) : '';

// numero 2
formFieldsNSm3['ayantDroitIdentiteNomNaissancePrenom2_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration2 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits32.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits32.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm3['ayantDroitNumeroSecuriteSociale2_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration2 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits32.presenceNumSSAM ? dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits32.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm3['ayantDroitDateNaissance2_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration2 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits32.presenceNumSSAM ? '' : dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits32.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm3['ayantDroitLieuNaissanceCommune2_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration2 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits32.presenceNumSSAM ? '' : (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits32.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits32.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits32.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm3['ayantDroitLienParente2_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration2 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits32.ayantDroitLienParenteNSMA) : '';
formFieldsNSm3['ayantDroitEnfantScolarise2OUI_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration2 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits32.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm3['ayantDroitEnfantScolarise2NON_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration2 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits32.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm3['ayantDroitNationalite2_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration2 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits32.ayantDroitNationaliteNSMA) : '';

//numero 3
formFieldsNSm3['ayantDroitIdentiteNomNaissancePrenom3_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration3 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits33.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits33.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm3['ayantDroitNumeroSecuriteSociale3_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration3 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits33.presenceNumSSAM ? dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits33.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm3['ayantDroitDateNaissance3_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration3 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits33.presenceNumSSAM ? '' : dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits33.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm3['ayantDroitLieuNaissanceCommune3_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration3 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits33.presenceNumSSAM ? '' : (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits33.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits33.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits33.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm3['ayantDroitLienParente3_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration3 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits33.ayantDroitLienParenteNSMA) : '';
formFieldsNSm3['ayantDroitEnfantScolarise3OUI_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration3 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits33.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm3['ayantDroitEnfantScolarise3NON_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration3 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits33.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm3['ayantDroitNationalite3_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration3 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits33.ayantDroitNationaliteNSMA) : '';

//numero 4
formFieldsNSm3['ayantDroitIdentiteNomNaissancePrenom4_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration4 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits34.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits34.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm3['ayantDroitNumeroSecuriteSociale4_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration4 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits34.presenceNumSSAM ? dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits34.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm3['ayantDroitDateNaissance4_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration4 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits34.presenceNumSSAM ? '' : dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits34.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm3['ayantDroitLieuNaissanceCommune4_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration4 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits34.presenceNumSSAM ? '' : (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits34.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits34.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits34.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm3['ayantDroitLienParente4_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration4 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits34.ayantDroitLienParenteNSMA) : '';
formFieldsNSm3['ayantDroitEnfantScolarise4OUI_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration4 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits34.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm3['ayantDroitEnfantScolarise4NON_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration4 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits34.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm3['ayantDroitNationalite4_NSMA'] = dirigeant3.cadreDirigeantNonSalarie3.ayantDroitsDeclaration4 ? (dirigeant3.cadreDirigeantNonSalarie3.ayantsDroits34.ayantDroitNationaliteNSMA) : '';

//-----------------------
//Cadre 4B
formFieldsNSm3['voletSocialMembreGAECOUI_NSMA'] = Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant3.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECOUINSMA') ? true : false;
formFieldsNSm3['voletSocialMembreGAECNON_NSMA'] = Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant3.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECNONNSMA') ? true : false;

//Cadre 5
formFieldsNSm3['renseignementsComplementairesObservations_NSMA'] = $m0Agricole.cadreDirigeantGroup.cadreDirigeant3.renseignementsComplementairesObservationsNSMA;

//Cadre 6
formFieldsNSm3['renseignementsComplementairesLeDeclarant_NSMA'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal') != false ? true : false;
formFieldsNSm3['renseignementsComplementairesLeMandataire_NSMA'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire') != false ? true : false;
formFieldsNSm3['formaliteSignataire_NSMA'] = (tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire :'')
+ ' ' + (tag.adresseMandataire.numeroVoieMandataire != null ? tag.adresseMandataire.numeroVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.indiceVoieMandataire != null ? tag.adresseMandataire.indiceVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.typeVoieMandataire != null ? tag.adresseMandataire.typeVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.nomVoieMandataire != null ? tag.adresseMandataire.nomVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.complementVoieMandataire != null ? tag.adresseMandataire.complementVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
+ ' ' + (tag.adresseMandataire.codePostalMandataire != null ? tag.adresseMandataire.codePostalMandataire : '')
+ ' ' + (tag.adresseMandataire.villeAdresseMandataire != null ? tag.adresseMandataire.villeAdresseMandataire : ''); 
formFieldsNSm3['formaliteSignatureLieu_NSMA'] = tag.formaliteSignatureLieu;
if (tag.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSm3['formaliteSignatureDate_NSMA'] = tag.formaliteSignatureDate;
}

formFieldsNSm3['signature_NSMA'] = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";

// A remplir avec le formulaire principal
formFieldsNSm3['formaliteSignatureNBIntercalaire_NSMA'] = '';

//NSm 4


//cadre 1
formFieldsNSm4['formulaireDependanceAffiliation_NSMA'] = false;
formFieldsNSm4['formulaireDependanceModificationAffiliation_NSMA'] = false;
formFieldsNSm4['formulaireDependancePourGerant_NSMA'] = false;
formFieldsNSm4['formulaireDependancePourAssocie_NSMA'] = false;
formFieldsNSm4['formulaireDependanceM0_NSMA'] = true;
formFieldsNSm4['formulaireDependanceM2agricole_NSMA'] = false;
formFieldsNSm4['formulaireDependanceM3_NSMA'] = false;
formFieldsNSm4['formulaireDependanceIntercalaiaireNSM_NSMA'] = false;

//cadre 2
formFieldsNSm4['entrepriseDenomination_NSMA'] = societe.entrepriseDenomination;

//cadre 3

formFieldsNSm4['personneLieePPNomNaissance_NSMA'] = dirigeant4.civilitePersonnePhysique4.personneLieePPNomNaissance;
formFieldsNSm4['personneLieePPNomUsage_NSMA'] = dirigeant4.civilitePersonnePhysique4.personneLieePPNomUsage;
var prenoms = [];
for (i = 0; i < dirigeant4.civilitePersonnePhysique4.personneLieePPPrenom.size(); i++) {
    prenoms.push(dirigeant4.civilitePersonnePhysique4.personneLieePPPrenom[i]);
}
formFieldsNSm4['personneLieePPPrenom_NSMA'] = prenoms.toString();

//cadre 3B
if (dirigeant4.civilitePersonnePhysique4.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant4.civilitePersonnePhysique4.personneLieePPDateNaissance.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
    date = date.concat(dateTmp.getFullYear().toString());
    formFieldsNSm4['personneLieePPDateNaissance_NSMA'] = date;
} else
    formFieldsNSm4['personneLieePPDateNaissance_NSMA'] = '';

formFieldsNSm4['personneLieePPDepartementNaissance_NSMA'] = dirigeant4.civilitePersonnePhysique4.personneLieePPDepartementNaissance != null ? dirigeant4.civilitePersonnePhysique4.personneLieePPDepartementNaissance : '';
formFieldsNSm4['personneLieePPCommuneNaissance_NSMA'] = dirigeant4.civilitePersonnePhysique4.personneLieePPLieuNaissanceCommune != null ? dirigeant4.civilitePersonnePhysique4.personneLieePPLieuNaissanceCommune : (dirigeant4.civilitePersonnePhysique4.personneLieePPLieuNaissanceVille + ' / ' + dirigeant4.civilitePersonnePhysique4.personneLieePPLieuNaissancePays);

formFieldsNSm4['personneLieePPDomicilePerso_NSMA'] =
    (dirigeant4.adresseDirigeant4.siegeAdresseNumVoie != null ? dirigeant4.adresseDirigeant4.siegeAdresseNumVoie : '')
 + '  ' + (dirigeant4.adresseDirigeant4.siegeAdresseIndiceVoie != null ? dirigeant4.adresseDirigeant4.siegeAdresseIndiceVoie : '')
 + '  ' + (dirigeant4.adresseDirigeant4.siegeAdresseTypeVoie != null ? dirigeant4.adresseDirigeant4.siegeAdresseTypeVoie : '')
 + '  ' + dirigeant4.adresseDirigeant4.siegeAdresseNomVoie
 + '  ' + (dirigeant4.adresseDirigeant4.siegeAdresseComplementVoie != null ? dirigeant4.adresseDirigeant4.siegeAdresseComplementVoie : '')
 + '  ' + (dirigeant4.adresseDirigeant4.siegeAdresseDistributionSpecialeVoie != null ? dirigeant4.adresseDirigeant4.siegeAdresseDistributionSpecialeVoie : '');
formFieldsNSm4['personneLieePPCodePostalPerso_NSMA'] = dirigeant4.adresseDirigeant4.siegeAdresseCodePostal;
formFieldsNSm4['personneLieePPCommunePerso_NSMA'] = dirigeant4.adresseDirigeant4.siegeAdresseCommune;

//cadre 4
var nirDeclarantNSMA = dirigeant4.cadreDirigeantNonSalarie4.voletSocialNumeroSecuriteSocialNSMA;
if (nirDeclarantNSMA != null) {
    nirDeclarantNSMA = nirDeclarantNSMA.replace(/ /g, "");
    formFieldsNSm4['voletSocialNumeroSecuriteSocial_NSMA'] = nirDeclarantNSMA.substring(0, 13);
    formFieldsNSm4['voletSocialNumeroSecuriteSocialCle_NSMA'] = nirDeclarantNSMA.substring(13, 15);
}

formFieldsNSm4['voletSocialMSAOUI_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.voletSocialMSAOUINONNSMA ? true : false;
formFieldsNSm4['voletSocialMSANON_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.voletSocialMSAOUINONNSMA ? false : true;
formFieldsNSm4['voletSocialRegimeAssuranceMaladieRegimeGeneral_NSMA'] = Value('id').of(dirigeant4.cadreDirigeantNonSalarie4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralNSMA') ? true : false;
formFieldsNSm4['voletSocialRegimeAssuranceMaladieRegimeAgricole_NSMA'] = Value('id').of(dirigeant4.cadreDirigeantNonSalarie4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleNSMA') ? true : false;
formFieldsNSm4['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_NSMA'] = Value('id').of(dirigeant4.cadreDirigeantNonSalarie4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleNSMA') ? true : false;
formFieldsNSm4['voletSocialRegimeAssuranceMaladieRegimeAutre_NSMA'] = Value('id').of(dirigeant4.cadreDirigeantNonSalarie4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreNSMA') ? true : false;
formFieldsNSm4['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionNSMA;

formFieldsNSm4['voletSocialActiviteSimultaneeOUI_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.voletSocialActiviteSimultaneeOUINONNSMA ? true : false;
formFieldsNSm4['voletSocialActiviteSimultaneeNON_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.voletSocialActiviteSimultaneeOUINONNSMA ? false : true;

formFieldsNSm4['voletSocialActiviteSimultaneeSalarieAgricole_NSMA'] = Value('id').of(dirigeant4.cadreDirigeantNonSalarie4.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeSalarieAgricoleNSMA') ? true : false;
formFieldsNSm4['voletSocialActiviteSimultaneeSalarie_NSMA'] = Value('id').of(dirigeant4.cadreDirigeantNonSalarie4.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeSalarieNSMA') ? true : false;
formFieldsNSm4['voletSocialActiviteSimultaneeNonSalarieNonAgricole_NSMA'] = Value('id').of(dirigeant4.cadreDirigeantNonSalarie4.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeNonSalarieNonAgricoleNSMA') ? true : false;
formFieldsNSm4['voletSocialActiviteSimultaneeRetraite_NSMA'] = Value('id').of(dirigeant4.cadreDirigeantNonSalarie4.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeRetraiteNSMA') ? true : false;
formFieldsNSm4['voletSocialActiviteSimultaneePensionne_NSMA'] = Value('id').of(dirigeant4.cadreDirigeantNonSalarie4.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneePensionneNSMA') ? true : false;
formFieldsNSm4['voletSocialActiviteSimultaneeAutre_NSMA'] = Value('id').of(dirigeant4.cadreDirigeantNonSalarie4.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeAutreNSMA') ? true : false;
formFieldsNSm4['voletSocialActiviteSimultaneeAutrePrecision_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.voletSocialActiviteSimultaneeAutrePrecisionNSMA;
formFieldsNSm4['voletSocialActiviteSimultaneePensionOrganisme_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.voletSocialActiviteSimultaneePensionOrganismeNSMA;

formFieldsNSm4['voletSocialJeuneAgriculteurOUI_NSMA'] = Value('id').of(dirigeant4.cadreDirigeantNonSalarie4.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurOUINSMA') ? true : false;
formFieldsNSm4['voletSocialJeuneAgriculteurNON_NSMA'] = Value('id').of(dirigeant4.cadreDirigeantNonSalarie4.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurNONNSMA') ? true : false;
formFieldsNSm4['voletSocialJeuneAgriculteurENCOURS_NSMA'] = Value('id').of(dirigeant4.cadreDirigeantNonSalarie4.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurENCOURSNSMA') ? true : false;

formFieldsNSm4['voletSocialPacsRSAOUI_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.voletSocialPacsRSAOUINON ? true : false;
formFieldsNSm4['voletSocialPacsRSANON_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.voletSocialPacsRSAOUINON ? false : true;
formFieldsNSm4['voletSocialStatutConjointCollaborateur_NSMA'] = Value('id').of(dirigeant4.cadreDirigeantNonSalarie4.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointCollaborateurNSMA') ? true : false;
formFieldsNSm4['voletSocialStatutConjointSalarie_NSMA'] = Value('id').of(dirigeant4.cadreDirigeantNonSalarie4.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointSalarieNSMA') ? true : false;
formFieldsNSm4['voletSocialStatutConjointAssocie_NSMA'] = Value('id').of(dirigeant4.cadreDirigeantNonSalarie4.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointAssocieNSMA') ? true : false;

formFieldsNSm4['voletSocialConjointRegimeAssuranceOUI_NSMA'] = Value('id').of(dirigeant4.cadreDirigeantNonSalarie4.voletSocialConjointRegimeAssuranceOUINONNSMA).eq('voletSocialConjointRegimeAssuranceOUINSMA') ? true : false;
formFieldsNSm4['voletSocialConjointRegimeAssuranceNON_NSMA'] = Value('id').of(dirigeant4.cadreDirigeantNonSalarie4.voletSocialConjointRegimeAssuranceOUINONNSMA).eq('voletSocialConjointRegimeAssuranceNONNSMA') ? true : false;

var nirConjointNSMA = dirigeant4.cadreDirigeantNonSalarie4.voletSocialConjointNumeroSecuriteSocialNSMA;
if (nirConjointNSMA != null) {
    nirConjointNSMA = nirConjointNSMA.replace(/ /g, "");
    formFieldsNSm4['voletSocialConjointNumeroSecuriteSocial_NSMA'] = nirConjointNSMA.substring(0, 13);
    formFieldsNSm4['voletSocialConjointNumeroSecuriteSocialCle_NSMA'] = nirConjointNSMA.substring(13, 15);
} else {
    formFieldsNSm4['voletSocialConjointNumeroSecuriteSocial_NSMA'] = '';
    formFieldsNSm4['voletSocialConjointNumeroSecuriteSocialCle_NSMA'] = '';
}

//-----------------------AYANT-DROIT

//ayant droit numero 1
formFieldsNSm4['ayantDroitIdentiteNomNaissancePrenom1_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration1 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits41.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits41.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm4['ayantDroitNumeroSecuriteSociale1_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration1 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits41.presenceNumSSAM ? dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits41.ayantDroitNumeroSecuriteSociale1NSMA : '') : '';
formFieldsNSm4['ayantDroitDateNaissance1_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration1 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits41.presenceNumSSAM ? '' : dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits41.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm4['ayantDroitLieuNaissanceCommune1_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration1 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits41.presenceNumSSAM ? '' : (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits41.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits41.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits41.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm4['ayantDroitLienParente1_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration1 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits41.ayantDroitLienParenteNSMA) : '';
formFieldsNSm4['ayantDroitEnfantScolarise1OUI_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration1 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits41.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm4['ayantDroitEnfantScolarise1NON_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration1 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits41.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm4['ayantDroitNationalite1_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration1 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits41.ayantDroitNationaliteNSMA) : '';

// numero 2
formFieldsNSm4['ayantDroitIdentiteNomNaissancePrenom2_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration2 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits42.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits42.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm4['ayantDroitNumeroSecuriteSociale2_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration2 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits42.presenceNumSSAM ? dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits42.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm4['ayantDroitDateNaissance2_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration2 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits42.presenceNumSSAM ? '' : dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits42.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm4['ayantDroitLieuNaissanceCommune2_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration2 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits42.presenceNumSSAM ? '' : (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits42.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits42.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits42.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm4['ayantDroitLienParente2_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration2 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits42.ayantDroitLienParenteNSMA) : '';
formFieldsNSm4['ayantDroitEnfantScolarise2OUI_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration2 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits42.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm4['ayantDroitEnfantScolarise2NON_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration2 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits42.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm4['ayantDroitNationalite2_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration2 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits42.ayantDroitNationaliteNSMA) : '';

//numero 3
formFieldsNSm4['ayantDroitIdentiteNomNaissancePrenom3_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration3 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits43.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits43.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm4['ayantDroitNumeroSecuriteSociale3_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration3 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits43.presenceNumSSAM ? dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits43.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm4['ayantDroitDateNaissance3_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration3 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits43.presenceNumSSAM ? '' : dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits43.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm4['ayantDroitLieuNaissanceCommune3_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration3 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits43.presenceNumSSAM ? '' : (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits43.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits43.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits43.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm4['ayantDroitLienParente3_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration3 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits43.ayantDroitLienParenteNSMA) : '';
formFieldsNSm4['ayantDroitEnfantScolarise3OUI_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration3 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits43.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm4['ayantDroitEnfantScolarise3NON_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration3 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits43.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm4['ayantDroitNationalite3_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration3 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits43.ayantDroitNationaliteNSMA) : '';

//numero 4
formFieldsNSm4['ayantDroitIdentiteNomNaissancePrenom4_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration4 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits44.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits44.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm4['ayantDroitNumeroSecuriteSociale4_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration4 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits44.presenceNumSSAM ? dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits44.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm4['ayantDroitDateNaissance4_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration4 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits44.presenceNumSSAM ? '' : dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits44.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm4['ayantDroitLieuNaissanceCommune4_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration4 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits44.presenceNumSSAM ? '' : (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits44.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits44.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits44.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm4['ayantDroitLienParente4_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration4 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits44.ayantDroitLienParenteNSMA) : '';
formFieldsNSm4['ayantDroitEnfantScolarise4OUI_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration4 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits44.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm4['ayantDroitEnfantScolarise4NON_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration4 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits44.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm4['ayantDroitNationalite4_NSMA'] = dirigeant4.cadreDirigeantNonSalarie4.ayantDroitsDeclaration4 ? (dirigeant4.cadreDirigeantNonSalarie4.ayantsDroits44.ayantDroitNationaliteNSMA) : '';

//-----------------------
//Cadre 4B
formFieldsNSm4['voletSocialMembreGAECOUI_NSMA'] = Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant4.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECOUINSMA') ? true : false;
formFieldsNSm4['voletSocialMembreGAECNON_NSMA'] = Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant4.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECNONNSMA') ? true : false;

//Cadre 5
formFieldsNSm4['renseignementsComplementairesObservations_NSMA'] = $m0Agricole.cadreDirigeantGroup.cadreDirigeant4.cadreObservationsNSM4.renseignementsComplementairesObservationsNSMA;

//Cadre 6
formFieldsNSm4['renseignementsComplementairesLeDeclarant_NSMA'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal') != false ? true : false;
formFieldsNSm4['renseignementsComplementairesLeMandataire_NSMA'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire') != false ? true : false;
formFieldsNSm4['formaliteSignataire_NSMA'] = (tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire :'')
+ ' ' + (tag.adresseMandataire.numeroVoieMandataire != null ? tag.adresseMandataire.numeroVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.indiceVoieMandataire != null ? tag.adresseMandataire.indiceVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.typeVoieMandataire != null ? tag.adresseMandataire.typeVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.nomVoieMandataire != null ? tag.adresseMandataire.nomVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.complementVoieMandataire != null ? tag.adresseMandataire.complementVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
+ ' ' + (tag.adresseMandataire.codePostalMandataire != null ? tag.adresseMandataire.codePostalMandataire : '')
+ ' ' + (tag.adresseMandataire.villeAdresseMandataire != null ? tag.adresseMandataire.villeAdresseMandataire : ''); 
formFieldsNSm4['formaliteSignatureLieu_NSMA'] = tag.formaliteSignatureLieu;
if (tag.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSm4['formaliteSignatureDate_NSMA'] = tag.formaliteSignatureDate;
}

formFieldsNSm4['signature_NSMA'] = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";

// A remplir avec le formulaire principal
formFieldsNSm4['formaliteSignatureNBIntercalaire_NSMA'] = '';

//NSm 5


//cadre 1
formFieldsNSm5['formulaireDependanceAffiliation_NSMA'] = false;
formFieldsNSm5['formulaireDependanceModificationAffiliation_NSMA'] = false;
formFieldsNSm5['formulaireDependancePourGerant_NSMA'] = false;
formFieldsNSm5['formulaireDependancePourAssocie_NSMA'] = false;
formFieldsNSm5['formulaireDependanceM0_NSMA'] = true;
formFieldsNSm5['formulaireDependanceM2agricole_NSMA'] = false;
formFieldsNSm5['formulaireDependanceM3_NSMA'] = false;
formFieldsNSm5['formulaireDependanceIntercalaiaireNSM_NSMA'] = false;

//cadre 2
formFieldsNSm5['entrepriseDenomination_NSMA'] = societe.entrepriseDenomination;

//cadre 3

formFieldsNSm5['personneLieePPNomNaissance_NSMA'] = dirigeant5.civilitePersonnePhysique5.personneLieePPNomNaissance;
formFieldsNSm5['personneLieePPNomUsage_NSMA'] = dirigeant5.civilitePersonnePhysique5.personneLieePPNomUsage;
var prenoms = [];
for (i = 0; i < dirigeant5.civilitePersonnePhysique5.personneLieePPPrenom.size(); i++) {
    prenoms.push(dirigeant5.civilitePersonnePhysique5.personneLieePPPrenom[i]);
}
formFieldsNSm5['personneLieePPPrenom_NSMA'] = prenoms.toString();

//cadre 3B
if (dirigeant5.civilitePersonnePhysique5.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant5.civilitePersonnePhysique5.personneLieePPDateNaissance.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
    date = date.concat(dateTmp.getFullYear().toString());
    formFieldsNSm5['personneLieePPDateNaissance_NSMA'] = date;
} else
    formFieldsNSm5['personneLieePPDateNaissance_NSMA'] = '';

formFieldsNSm5['personneLieePPDepartementNaissance_NSMA'] = dirigeant5.civilitePersonnePhysique5.personneLieePPDepartementNaissance != null ? dirigeant5.civilitePersonnePhysique5.personneLieePPDepartementNaissance : '';
formFieldsNSm5['personneLieePPCommuneNaissance_NSMA'] = dirigeant5.civilitePersonnePhysique5.personneLieePPLieuNaissanceCommune != null ? dirigeant5.civilitePersonnePhysique5.personneLieePPLieuNaissanceCommune : (dirigeant5.civilitePersonnePhysique5.personneLieePPLieuNaissanceVille + ' / ' + dirigeant5.civilitePersonnePhysique5.personneLieePPLieuNaissancePays);

formFieldsNSm5['personneLieePPDomicilePerso_NSMA'] =
    (dirigeant5.adresseDirigeant5.siegeAdresseNumVoie != null ? dirigeant5.adresseDirigeant5.siegeAdresseNumVoie : '')
 + '  ' + (dirigeant5.adresseDirigeant5.siegeAdresseIndiceVoie != null ? dirigeant5.adresseDirigeant5.siegeAdresseIndiceVoie : '')
 + '  ' + (dirigeant5.adresseDirigeant5.siegeAdresseTypeVoie != null ? dirigeant5.adresseDirigeant5.siegeAdresseTypeVoie : '')
 + '  ' + dirigeant5.adresseDirigeant5.siegeAdresseNomVoie
 + '  ' + (dirigeant5.adresseDirigeant5.siegeAdresseComplementVoie != null ? dirigeant5.adresseDirigeant5.siegeAdresseComplementVoie : '')
 + '  ' + (dirigeant5.adresseDirigeant5.siegeAdresseDistributionSpecialeVoie != null ? dirigeant5.adresseDirigeant5.siegeAdresseDistributionSpecialeVoie : '');
formFieldsNSm5['personneLieePPCodePostalPerso_NSMA'] = dirigeant5.adresseDirigeant5.siegeAdresseCodePostal;
formFieldsNSm5['personneLieePPCommunePerso_NSMA'] = dirigeant5.adresseDirigeant5.siegeAdresseCommune;

//cadre 4
var nirDeclarantNSMA = dirigeant5.cadreDirigeantNonSalarie5.voletSocialNumeroSecuriteSocialNSMA;
if (nirDeclarantNSMA != null) {
    nirDeclarantNSMA = nirDeclarantNSMA.replace(/ /g, "");
    formFieldsNSm5['voletSocialNumeroSecuriteSocial_NSMA'] = nirDeclarantNSMA.substring(0, 13);
    formFieldsNSm5['voletSocialNumeroSecuriteSocialCle_NSMA'] = nirDeclarantNSMA.substring(13, 15);
}

formFieldsNSm5['voletSocialMSAOUI_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.voletSocialMSAOUINONNSMA ? true : false;
formFieldsNSm5['voletSocialMSANON_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.voletSocialMSAOUINONNSMA ? false : true;
formFieldsNSm5['voletSocialRegimeAssuranceMaladieRegimeGeneral_NSMA'] = Value('id').of(dirigeant5.cadreDirigeantNonSalarie5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralNSMA') ? true : false;
formFieldsNSm5['voletSocialRegimeAssuranceMaladieRegimeAgricole_NSMA'] = Value('id').of(dirigeant5.cadreDirigeantNonSalarie5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleNSMA') ? true : false;
formFieldsNSm5['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_NSMA'] = Value('id').of(dirigeant5.cadreDirigeantNonSalarie5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleNSMA') ? true : false;
formFieldsNSm5['voletSocialRegimeAssuranceMaladieRegimeAutre_NSMA'] = Value('id').of(dirigeant5.cadreDirigeantNonSalarie5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreNSMA') ? true : false;
formFieldsNSm5['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionNSMA;

formFieldsNSm5['voletSocialActiviteSimultaneeOUI_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.voletSocialActiviteSimultaneeOUINONNSMA ? true : false;
formFieldsNSm5['voletSocialActiviteSimultaneeNON_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.voletSocialActiviteSimultaneeOUINONNSMA ? false : true;

formFieldsNSm5['voletSocialActiviteSimultaneeSalarieAgricole_NSMA'] = Value('id').of(dirigeant5.cadreDirigeantNonSalarie5.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeSalarieAgricoleNSMA') ? true : false;
formFieldsNSm5['voletSocialActiviteSimultaneeSalarie_NSMA'] = Value('id').of(dirigeant5.cadreDirigeantNonSalarie5.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeSalarieNSMA') ? true : false;
formFieldsNSm5['voletSocialActiviteSimultaneeNonSalarieNonAgricole_NSMA'] = Value('id').of(dirigeant5.cadreDirigeantNonSalarie5.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeNonSalarieNonAgricoleNSMA') ? true : false;
formFieldsNSm5['voletSocialActiviteSimultaneeRetraite_NSMA'] = Value('id').of(dirigeant5.cadreDirigeantNonSalarie5.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeRetraiteNSMA') ? true : false;
formFieldsNSm5['voletSocialActiviteSimultaneePensionne_NSMA'] = Value('id').of(dirigeant5.cadreDirigeantNonSalarie5.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneePensionneNSMA') ? true : false;
formFieldsNSm5['voletSocialActiviteSimultaneeAutre_NSMA'] = Value('id').of(dirigeant5.cadreDirigeantNonSalarie5.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeAutreNSMA') ? true : false;
formFieldsNSm5['voletSocialActiviteSimultaneeAutrePrecision_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.voletSocialActiviteSimultaneeAutrePrecisionNSMA;
formFieldsNSm5['voletSocialActiviteSimultaneePensionOrganisme_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.voletSocialActiviteSimultaneePensionOrganismeNSMA;

formFieldsNSm5['voletSocialJeuneAgriculteurOUI_NSMA'] = Value('id').of(dirigeant5.cadreDirigeantNonSalarie5.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurOUINSMA') ? true : false;
formFieldsNSm5['voletSocialJeuneAgriculteurNON_NSMA'] = Value('id').of(dirigeant5.cadreDirigeantNonSalarie5.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurNONNSMA') ? true : false;
formFieldsNSm5['voletSocialJeuneAgriculteurENCOURS_NSMA'] = Value('id').of(dirigeant5.cadreDirigeantNonSalarie5.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurENCOURSNSMA') ? true : false;

formFieldsNSm5['voletSocialPacsRSAOUI_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.voletSocialPacsRSAOUINON ? true : false;
formFieldsNSm5['voletSocialPacsRSANON_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.voletSocialPacsRSAOUINON ? false : true;
formFieldsNSm5['voletSocialStatutConjointCollaborateur_NSMA'] = Value('id').of(dirigeant5.cadreDirigeantNonSalarie5.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointCollaborateurNSMA') ? true : false;
formFieldsNSm5['voletSocialStatutConjointSalarie_NSMA'] = Value('id').of(dirigeant5.cadreDirigeantNonSalarie5.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointSalarieNSMA') ? true : false;
formFieldsNSm5['voletSocialStatutConjointAssocie_NSMA'] = Value('id').of(dirigeant5.cadreDirigeantNonSalarie5.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointAssocieNSMA') ? true : false;

formFieldsNSm5['voletSocialConjointRegimeAssuranceOUI_NSMA'] = Value('id').of(dirigeant5.cadreDirigeantNonSalarie5.voletSocialConjointRegimeAssuranceOUINONNSMA).eq('voletSocialConjointRegimeAssuranceOUINSMA') ? true : false;
formFieldsNSm5['voletSocialConjointRegimeAssuranceNON_NSMA'] = Value('id').of(dirigeant5.cadreDirigeantNonSalarie5.voletSocialConjointRegimeAssuranceOUINONNSMA).eq('voletSocialConjointRegimeAssuranceNONNSMA') ? true : false;

var nirConjointNSMA = dirigeant5.cadreDirigeantNonSalarie5.voletSocialConjointNumeroSecuriteSocialNSMA;
if (nirConjointNSMA != null) {
    nirConjointNSMA = nirConjointNSMA.replace(/ /g, "");
    formFieldsNSm5['voletSocialConjointNumeroSecuriteSocial_NSMA'] = nirConjointNSMA.substring(0, 13);
    formFieldsNSm5['voletSocialConjointNumeroSecuriteSocialCle_NSMA'] = nirConjointNSMA.substring(13, 15);
} else {
    formFieldsNSm5['voletSocialConjointNumeroSecuriteSocial_NSMA'] = '';
    formFieldsNSm5['voletSocialConjointNumeroSecuriteSocialCle_NSMA'] = '';
}

//-----------------------AYANT-DROIT

//ayant droit numero 1
formFieldsNSm5['ayantDroitIdentiteNomNaissancePrenom1_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration1 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits51.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits51.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm5['ayantDroitNumeroSecuriteSociale1_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration1 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits51.presenceNumSSAM ? dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits51.ayantDroitNumeroSecuriteSociale1NSMA : '') : '';
formFieldsNSm5['ayantDroitDateNaissance1_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration1 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits51.presenceNumSSAM ? '' : dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits51.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm5['ayantDroitLieuNaissanceCommune1_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration1 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits51.presenceNumSSAM ? '' : (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits51.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits51.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits51.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm5['ayantDroitLienParente1_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration1 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits51.ayantDroitLienParenteNSMA) : '';
formFieldsNSm5['ayantDroitEnfantScolarise1OUI_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration1 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits51.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm5['ayantDroitEnfantScolarise1NON_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration1 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits51.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm5['ayantDroitNationalite1_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration1 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits51.ayantDroitNationaliteNSMA) : '';

// numero 2
formFieldsNSm5['ayantDroitIdentiteNomNaissancePrenom2_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration2 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits52.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits52.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm5['ayantDroitNumeroSecuriteSociale2_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration2 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits52.presenceNumSSAM ? dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits52.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm5['ayantDroitDateNaissance2_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration2 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits52.presenceNumSSAM ? '' : dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits52.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm5['ayantDroitLieuNaissanceCommune2_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration2 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits52.presenceNumSSAM ? '' : (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits52.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits52.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits52.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm5['ayantDroitLienParente2_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration2 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits52.ayantDroitLienParenteNSMA) : '';
formFieldsNSm5['ayantDroitEnfantScolarise2OUI_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration2 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits52.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm5['ayantDroitEnfantScolarise2NON_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration2 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits52.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm5['ayantDroitNationalite2_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration2 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits52.ayantDroitNationaliteNSMA) : '';

//numero 3
formFieldsNSm5['ayantDroitIdentiteNomNaissancePrenom3_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration3 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits53.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits53.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm5['ayantDroitNumeroSecuriteSociale3_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration3 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits53.presenceNumSSAM ? dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits53.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm5['ayantDroitDateNaissance3_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration3 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits53.presenceNumSSAM ? '' : dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits53.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm5['ayantDroitLieuNaissanceCommune3_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration3 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits53.presenceNumSSAM ? '' : (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits53.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits53.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits53.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm5['ayantDroitLienParente3_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration3 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits53.ayantDroitLienParenteNSMA) : '';
formFieldsNSm5['ayantDroitEnfantScolarise3OUI_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration3 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits53.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm5['ayantDroitEnfantScolarise3NON_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration3 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits53.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm5['ayantDroitNationalite3_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration3 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits53.ayantDroitNationaliteNSMA) : '';

//numero 4
formFieldsNSm5['ayantDroitIdentiteNomNaissancePrenom4_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration4 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits54.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits54.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm5['ayantDroitNumeroSecuriteSociale4_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration4 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits54.presenceNumSSAM ? dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits54.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm5['ayantDroitDateNaissance4_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration4 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits54.presenceNumSSAM ? '' : dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits54.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm5['ayantDroitLieuNaissanceCommune4_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration4 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits54.presenceNumSSAM ? '' : (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits54.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits54.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits54.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm5['ayantDroitLienParente4_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration4 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits54.ayantDroitLienParenteNSMA) : '';
formFieldsNSm5['ayantDroitEnfantScolarise4OUI_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration4 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits54.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm5['ayantDroitEnfantScolarise4NON_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration4 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits54.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm5['ayantDroitNationalite4_NSMA'] = dirigeant5.cadreDirigeantNonSalarie5.ayantDroitsDeclaration4 ? (dirigeant5.cadreDirigeantNonSalarie5.ayantsDroits54.ayantDroitNationaliteNSMA) : '';

//-----------------------
//Cadre 4B
formFieldsNSm5['voletSocialMembreGAECOUI_NSMA'] = Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant5.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECOUINSMA') ? true : false;
formFieldsNSm5['voletSocialMembreGAECNON_NSMA'] = Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant5.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECNONNSMA') ? true : false;

//Cadre 5
formFieldsNSm5['renseignementsComplementairesObservations_NSMA'] = $m0Agricole.cadreDirigeantGroup.cadreDirigeant5.cadreObservationsNSM5.renseignementsComplementairesObservationsNSMA;

//Cadre 6
formFieldsNSm5['renseignementsComplementairesLeDeclarant_NSMA'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal') != false ? true : false;
formFieldsNSm5['renseignementsComplementairesLeMandataire_NSMA'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire') != false ? true : false;
formFieldsNSm5['formaliteSignataire_NSMA'] = (tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire :'')
+ ' ' + (tag.adresseMandataire.numeroVoieMandataire != null ? tag.adresseMandataire.numeroVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.indiceVoieMandataire != null ? tag.adresseMandataire.indiceVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.typeVoieMandataire != null ? tag.adresseMandataire.typeVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.nomVoieMandataire != null ? tag.adresseMandataire.nomVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.complementVoieMandataire != null ? tag.adresseMandataire.complementVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
+ ' ' + (tag.adresseMandataire.codePostalMandataire != null ? tag.adresseMandataire.codePostalMandataire : '')
+ ' ' + (tag.adresseMandataire.villeAdresseMandataire != null ? tag.adresseMandataire.villeAdresseMandataire : ''); 
formFieldsNSm5['formaliteSignatureLieu_NSMA'] = tag.formaliteSignatureLieu;
if (tag.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSm5['formaliteSignatureDate_NSMA'] = tag.formaliteSignatureDate;
}

formFieldsNSm5['signature_NSMA'] = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";

// A remplir avec le formulaire principal
formFieldsNSm5['formaliteSignatureNBIntercalaire_NSMA'] = '';

//NSm 6


//cadre 1
formFieldsNSm6['formulaireDependanceAffiliation_NSMA'] = false;
formFieldsNSm6['formulaireDependanceModificationAffiliation_NSMA'] = false;
formFieldsNSm6['formulaireDependancePourGerant_NSMA'] = false;
formFieldsNSm6['formulaireDependancePourAssocie_NSMA'] = false;
formFieldsNSm6['formulaireDependanceM0_NSMA'] = true;
formFieldsNSm6['formulaireDependanceM2agricole_NSMA'] = false;
formFieldsNSm6['formulaireDependanceM3_NSMA'] = false;
formFieldsNSm6['formulaireDependanceIntercalaiaireNSM_NSMA'] = false;

//cadre 2
formFieldsNSm6['entrepriseDenomination_NSMA'] = societe.entrepriseDenomination;

//cadre 3

formFieldsNSm6['personneLieePPNomNaissance_NSMA'] = dirigeant6.civilitePersonnePhysique6.personneLieePPNomNaissance;
formFieldsNSm6['personneLieePPNomUsage_NSMA'] = dirigeant6.civilitePersonnePhysique6.personneLieePPNomUsage;
var prenoms = [];
for (i = 0; i < dirigeant6.civilitePersonnePhysique6.personneLieePPPrenom.size(); i++) {
    prenoms.push(dirigeant6.civilitePersonnePhysique6.personneLieePPPrenom[i]);
}
formFieldsNSm6['personneLieePPPrenom_NSMA'] = prenoms.toString();

//cadre 3B
if (dirigeant6.civilitePersonnePhysique6.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant6.civilitePersonnePhysique6.personneLieePPDateNaissance.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
    date = date.concat(dateTmp.getFullYear().toString());
    formFieldsNSm6['personneLieePPDateNaissance_NSMA'] = date;
} else
    formFieldsNSm6['personneLieePPDateNaissance_NSMA'] = '';

formFieldsNSm6['personneLieePPDepartementNaissance_NSMA'] = dirigeant6.civilitePersonnePhysique6.personneLieePPDepartementNaissance != null ? dirigeant6.civilitePersonnePhysique6.personneLieePPDepartementNaissance : '';
formFieldsNSm6['personneLieePPCommuneNaissance_NSMA'] = dirigeant6.civilitePersonnePhysique6.personneLieePPLieuNaissanceCommune != null ? dirigeant6.civilitePersonnePhysique6.personneLieePPLieuNaissanceCommune : (dirigeant6.civilitePersonnePhysique6.personneLieePPLieuNaissanceVille + ' / ' + dirigeant6.civilitePersonnePhysique6.personneLieePPLieuNaissancePays);

formFieldsNSm6['personneLieePPDomicilePerso_NSMA'] =
    (dirigeant6.adresseDirigeant6.siegeAdresseNumVoie != null ? dirigeant6.adresseDirigeant6.siegeAdresseNumVoie : '')
 + '  ' + (dirigeant6.adresseDirigeant6.siegeAdresseIndiceVoie != null ? dirigeant6.adresseDirigeant6.siegeAdresseIndiceVoie : '')
 + '  ' + (dirigeant6.adresseDirigeant6.siegeAdresseTypeVoie != null ? dirigeant6.adresseDirigeant6.siegeAdresseTypeVoie : '')
 + '  ' + dirigeant6.adresseDirigeant6.siegeAdresseNomVoie
 + '  ' + (dirigeant6.adresseDirigeant6.siegeAdresseComplementVoie != null ? dirigeant6.adresseDirigeant6.siegeAdresseComplementVoie : '')
 + '  ' + (dirigeant6.adresseDirigeant6.siegeAdresseDistributionSpecialeVoie != null ? dirigeant6.adresseDirigeant6.siegeAdresseDistributionSpecialeVoie : '');
formFieldsNSm6['personneLieePPCodePostalPerso_NSMA'] = dirigeant6.adresseDirigeant6.siegeAdresseCodePostal;
formFieldsNSm6['personneLieePPCommunePerso_NSMA'] = dirigeant6.adresseDirigeant6.siegeAdresseCommune;

//cadre 4
var nirDeclarantNSMA = dirigeant6.cadreDirigeantNonSalarie6.voletSocialNumeroSecuriteSocialNSMA;
if (nirDeclarantNSMA != null) {
    nirDeclarantNSMA = nirDeclarantNSMA.replace(/ /g, "");
    formFieldsNSm6['voletSocialNumeroSecuriteSocial_NSMA'] = nirDeclarantNSMA.substring(0, 13);
    formFieldsNSm6['voletSocialNumeroSecuriteSocialCle_NSMA'] = nirDeclarantNSMA.substring(13, 15);
}

formFieldsNSm6['voletSocialMSAOUI_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.voletSocialMSAOUINONNSMA ? true : false;
formFieldsNSm6['voletSocialMSANON_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.voletSocialMSAOUINONNSMA ? false : true;
formFieldsNSm6['voletSocialRegimeAssuranceMaladieRegimeGeneral_NSMA'] = Value('id').of(dirigeant6.cadreDirigeantNonSalarie6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralNSMA') ? true : false;
formFieldsNSm6['voletSocialRegimeAssuranceMaladieRegimeAgricole_NSMA'] = Value('id').of(dirigeant6.cadreDirigeantNonSalarie6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleNSMA') ? true : false;
formFieldsNSm6['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_NSMA'] = Value('id').of(dirigeant6.cadreDirigeantNonSalarie6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleNSMA') ? true : false;
formFieldsNSm6['voletSocialRegimeAssuranceMaladieRegimeAutre_NSMA'] = Value('id').of(dirigeant6.cadreDirigeantNonSalarie6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreNSMA') ? true : false;
formFieldsNSm6['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionNSMA;

formFieldsNSm6['voletSocialActiviteSimultaneeOUI_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.voletSocialActiviteSimultaneeOUINONNSMA ? true : false;
formFieldsNSm6['voletSocialActiviteSimultaneeNON_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.voletSocialActiviteSimultaneeOUINONNSMA ? false : true;

formFieldsNSm6['voletSocialActiviteSimultaneeSalarieAgricole_NSMA'] = Value('id').of(dirigeant6.cadreDirigeantNonSalarie6.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeSalarieAgricoleNSMA') ? true : false;
formFieldsNSm6['voletSocialActiviteSimultaneeSalarie_NSMA'] = Value('id').of(dirigeant6.cadreDirigeantNonSalarie6.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeSalarieNSMA') ? true : false;
formFieldsNSm6['voletSocialActiviteSimultaneeNonSalarieNonAgricole_NSMA'] = Value('id').of(dirigeant6.cadreDirigeantNonSalarie6.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeNonSalarieNonAgricoleNSMA') ? true : false;
formFieldsNSm6['voletSocialActiviteSimultaneeRetraite_NSMA'] = Value('id').of(dirigeant6.cadreDirigeantNonSalarie6.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeRetraiteNSMA') ? true : false;
formFieldsNSm6['voletSocialActiviteSimultaneePensionne_NSMA'] = Value('id').of(dirigeant6.cadreDirigeantNonSalarie6.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneePensionneNSMA') ? true : false;
formFieldsNSm6['voletSocialActiviteSimultaneeAutre_NSMA'] = Value('id').of(dirigeant6.cadreDirigeantNonSalarie6.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeAutreNSMA') ? true : false;
formFieldsNSm6['voletSocialActiviteSimultaneeAutrePrecision_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.voletSocialActiviteSimultaneeAutrePrecisionNSMA;
formFieldsNSm6['voletSocialActiviteSimultaneePensionOrganisme_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.voletSocialActiviteSimultaneePensionOrganismeNSMA;

formFieldsNSm6['voletSocialJeuneAgriculteurOUI_NSMA'] = Value('id').of(dirigeant6.cadreDirigeantNonSalarie6.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurOUINSMA') ? true : false;
formFieldsNSm6['voletSocialJeuneAgriculteurNON_NSMA'] = Value('id').of(dirigeant6.cadreDirigeantNonSalarie6.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurNONNSMA') ? true : false;
formFieldsNSm6['voletSocialJeuneAgriculteurENCOURS_NSMA'] = Value('id').of(dirigeant6.cadreDirigeantNonSalarie6.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurENCOURSNSMA') ? true : false;

formFieldsNSm6['voletSocialPacsRSAOUI_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.voletSocialPacsRSAOUINON ? true : false;
formFieldsNSm6['voletSocialPacsRSANON_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.voletSocialPacsRSAOUINON ? false : true;
formFieldsNSm6['voletSocialStatutConjointCollaborateur_NSMA'] = Value('id').of(dirigeant6.cadreDirigeantNonSalarie6.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointCollaborateurNSMA') ? true : false;
formFieldsNSm6['voletSocialStatutConjointSalarie_NSMA'] = Value('id').of(dirigeant6.cadreDirigeantNonSalarie6.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointSalarieNSMA') ? true : false;
formFieldsNSm6['voletSocialStatutConjointAssocie_NSMA'] = Value('id').of(dirigeant6.cadreDirigeantNonSalarie6.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointAssocieNSMA') ? true : false;

formFieldsNSm6['voletSocialConjointRegimeAssuranceOUI_NSMA'] = Value('id').of(dirigeant6.cadreDirigeantNonSalarie6.voletSocialConjointRegimeAssuranceOUINONNSMA).eq('voletSocialConjointRegimeAssuranceOUINSMA') ? true : false;
formFieldsNSm6['voletSocialConjointRegimeAssuranceNON_NSMA'] = Value('id').of(dirigeant6.cadreDirigeantNonSalarie6.voletSocialConjointRegimeAssuranceOUINONNSMA).eq('voletSocialConjointRegimeAssuranceNONNSMA') ? true : false;

var nirConjointNSMA = dirigeant6.cadreDirigeantNonSalarie6.voletSocialConjointNumeroSecuriteSocialNSMA;
if (nirConjointNSMA != null) {
    nirConjointNSMA = nirConjointNSMA.replace(/ /g, "");
    formFieldsNSm6['voletSocialConjointNumeroSecuriteSocial_NSMA'] = nirConjointNSMA.substring(0, 13);
    formFieldsNSm6['voletSocialConjointNumeroSecuriteSocialCle_NSMA'] = nirConjointNSMA.substring(13, 15);
} else {
    formFieldsNSm6['voletSocialConjointNumeroSecuriteSocial_NSMA'] = '';
    formFieldsNSm6['voletSocialConjointNumeroSecuriteSocialCle_NSMA'] = '';
}

//-----------------------AYANT-DROIT

//ayant droit numero 1
formFieldsNSm6['ayantDroitIdentiteNomNaissancePrenom1_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration1 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits61.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits61.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm6['ayantDroitNumeroSecuriteSociale1_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration1 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits61.presenceNumSSAM ? dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits61.ayantDroitNumeroSecuriteSociale1NSMA : '') : '';
formFieldsNSm6['ayantDroitDateNaissance1_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration1 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits61.presenceNumSSAM ? '' : dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits61.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm6['ayantDroitLieuNaissanceCommune1_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration1 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits61.presenceNumSSAM ? '' : (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits61.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits61.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits61.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm6['ayantDroitLienParente1_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration1 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits61.ayantDroitLienParenteNSMA) : '';
formFieldsNSm6['ayantDroitEnfantScolarise1OUI_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration1 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits61.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm6['ayantDroitEnfantScolarise1NON_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration1 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits61.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm6['ayantDroitNationalite1_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration1 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits61.ayantDroitNationaliteNSMA) : '';

// numero 2
formFieldsNSm6['ayantDroitIdentiteNomNaissancePrenom2_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration2 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits62.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits62.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm6['ayantDroitNumeroSecuriteSociale2_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration2 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits62.presenceNumSSAM ? dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits62.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm6['ayantDroitDateNaissance2_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration2 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits62.presenceNumSSAM ? '' : dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits62.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm6['ayantDroitLieuNaissanceCommune2_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration2 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits62.presenceNumSSAM ? '' : (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits62.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits62.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits62.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm6['ayantDroitLienParente2_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration2 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits62.ayantDroitLienParenteNSMA) : '';
formFieldsNSm6['ayantDroitEnfantScolarise2OUI_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration2 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits62.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm6['ayantDroitEnfantScolarise2NON_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration2 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits62.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm6['ayantDroitNationalite2_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration2 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits62.ayantDroitNationaliteNSMA) : '';

//numero 3
formFieldsNSm6['ayantDroitIdentiteNomNaissancePrenom3_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration3 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits63.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits63.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm6['ayantDroitNumeroSecuriteSociale3_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration3 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits63.presenceNumSSAM ? dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits63.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm6['ayantDroitDateNaissance3_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration3 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits63.presenceNumSSAM ? '' : dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits63.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm6['ayantDroitLieuNaissanceCommune3_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration3 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits63.presenceNumSSAM ? '' : (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits63.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits63.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits63.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm6['ayantDroitLienParente3_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration3 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits63.ayantDroitLienParenteNSMA) : '';
formFieldsNSm6['ayantDroitEnfantScolarise3OUI_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration3 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits63.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm6['ayantDroitEnfantScolarise3NON_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration3 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits63.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm6['ayantDroitNationalite3_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration3 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits63.ayantDroitNationaliteNSMA) : '';

//numero 4
formFieldsNSm6['ayantDroitIdentiteNomNaissancePrenom4_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration4 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits64.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits64.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm6['ayantDroitNumeroSecuriteSociale4_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration4 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits64.presenceNumSSAM ? dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits64.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm6['ayantDroitDateNaissance4_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration4 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits64.presenceNumSSAM ? '' : dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits64.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm6['ayantDroitLieuNaissanceCommune4_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration4 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits64.presenceNumSSAM ? '' : (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits64.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits64.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits64.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm6['ayantDroitLienParente4_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration4 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits64.ayantDroitLienParenteNSMA) : '';
formFieldsNSm6['ayantDroitEnfantScolarise4OUI_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration4 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits64.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm6['ayantDroitEnfantScolarise4NON_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration4 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits64.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm6['ayantDroitNationalite4_NSMA'] = dirigeant6.cadreDirigeantNonSalarie6.ayantDroitsDeclaration4 ? (dirigeant6.cadreDirigeantNonSalarie6.ayantsDroits64.ayantDroitNationaliteNSMA) : '';

//-----------------------
//Cadre 4B
formFieldsNSm6['voletSocialMembreGAECOUI_NSMA'] = Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant6.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECOUINSMA') ? true : false;
formFieldsNSm6['voletSocialMembreGAECNON_NSMA'] = Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant6.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECNONNSMA') ? true : false;

//Cadre 5
formFieldsNSm6['renseignementsComplementairesObservations_NSMA'] = $m0Agricole.cadreDirigeantGroup.cadreDirigeant6.cadreObservationsNSM6.renseignementsComplementairesObservationsNSMA;

//Cadre 6
formFieldsNSm6['renseignementsComplementairesLeDeclarant_NSMA'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal') != false ? true : false;
formFieldsNSm6['renseignementsComplementairesLeMandataire_NSMA'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire') != false ? true : false;
formFieldsNSm6['formaliteSignataire_NSMA'] = (tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire :'')
+ ' ' + (tag.adresseMandataire.numeroVoieMandataire != null ? tag.adresseMandataire.numeroVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.indiceVoieMandataire != null ? tag.adresseMandataire.indiceVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.typeVoieMandataire != null ? tag.adresseMandataire.typeVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.nomVoieMandataire != null ? tag.adresseMandataire.nomVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.complementVoieMandataire != null ? tag.adresseMandataire.complementVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
+ ' ' + (tag.adresseMandataire.codePostalMandataire != null ? tag.adresseMandataire.codePostalMandataire : '')
+ ' ' + (tag.adresseMandataire.villeAdresseMandataire != null ? tag.adresseMandataire.villeAdresseMandataire : ''); 
formFieldsNSm6['formaliteSignatureLieu_NSMA'] = tag.formaliteSignatureLieu;
if (tag.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSm6['formaliteSignatureDate_NSMA'] = tag.formaliteSignatureDate;
}
formFieldsNSm6['signature_NSMA'] = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";

// A remplir avec le formulaire principal
formFieldsNSm6['formaliteSignatureNBIntercalaire_NSMA'] = '';

//NSm 7


//cadre 1
formFieldsNSm7['formulaireDependanceAffiliation_NSMA'] = false;
formFieldsNSm7['formulaireDependanceModificationAffiliation_NSMA'] = false;
formFieldsNSm7['formulaireDependancePourGerant_NSMA'] = false;
formFieldsNSm7['formulaireDependancePourAssocie_NSMA'] = false;
formFieldsNSm7['formulaireDependanceM0_NSMA'] = true;
formFieldsNSm7['formulaireDependanceM2agricole_NSMA'] = false;
formFieldsNSm7['formulaireDependanceM3_NSMA'] = false;
formFieldsNSm7['formulaireDependanceIntercalaiaireNSM_NSMA'] = false;

//cadre 2
formFieldsNSm7['entrepriseDenomination_NSMA'] = societe.entrepriseDenomination;

//cadre 3

formFieldsNSm7['personneLieePPNomNaissance_NSMA'] = dirigeant7.civilitePersonnePhysique7.personneLieePPNomNaissance;
formFieldsNSm7['personneLieePPNomUsage_NSMA'] = dirigeant7.civilitePersonnePhysique7.personneLieePPNomUsage;
var prenoms = [];
for (i = 0; i < dirigeant7.civilitePersonnePhysique7.personneLieePPPrenom.size(); i++) {
    prenoms.push(dirigeant7.civilitePersonnePhysique7.personneLieePPPrenom[i]);
}
formFieldsNSm7['personneLieePPPrenom_NSMA'] = prenoms.toString();

//cadre 3B
if (dirigeant7.civilitePersonnePhysique7.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant7.civilitePersonnePhysique7.personneLieePPDateNaissance.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
    date = date.concat(dateTmp.getFullYear().toString());
    formFieldsNSm7['personneLieePPDateNaissance_NSMA'] = date;
} else
    formFieldsNSm7['personneLieePPDateNaissance_NSMA'] = '';

formFieldsNSm7['personneLieePPDepartementNaissance_NSMA'] = dirigeant7.civilitePersonnePhysique7.personneLieePPDepartementNaissance != null ? dirigeant7.civilitePersonnePhysique7.personneLieePPDepartementNaissance : '';
formFieldsNSm7['personneLieePPCommuneNaissance_NSMA'] = dirigeant7.civilitePersonnePhysique7.personneLieePPLieuNaissanceCommune != null ? dirigeant7.civilitePersonnePhysique7.personneLieePPLieuNaissanceCommune : (dirigeant7.civilitePersonnePhysique7.personneLieePPLieuNaissanceVille + ' / ' + dirigeant7.civilitePersonnePhysique7.personneLieePPLieuNaissancePays);

formFieldsNSm7['personneLieePPDomicilePerso_NSMA'] =
    (dirigeant7.adresseDirigeant7.siegeAdresseNumVoie != null ? dirigeant7.adresseDirigeant7.siegeAdresseNumVoie : '')
 + '  ' + (dirigeant7.adresseDirigeant7.siegeAdresseIndiceVoie != null ? dirigeant7.adresseDirigeant7.siegeAdresseIndiceVoie : '')
 + '  ' + (dirigeant7.adresseDirigeant7.siegeAdresseTypeVoie != null ? dirigeant7.adresseDirigeant7.siegeAdresseTypeVoie : '')
 + '  ' + dirigeant7.adresseDirigeant7.siegeAdresseNomVoie
 + '  ' + (dirigeant7.adresseDirigeant7.siegeAdresseComplementVoie != null ? dirigeant7.adresseDirigeant7.siegeAdresseComplementVoie : '')
 + '  ' + (dirigeant7.adresseDirigeant7.siegeAdresseDistributionSpecialeVoie != null ? dirigeant7.adresseDirigeant7.siegeAdresseDistributionSpecialeVoie : '');
formFieldsNSm7['personneLieePPCodePostalPerso_NSMA'] = dirigeant7.adresseDirigeant7.siegeAdresseCodePostal;
formFieldsNSm7['personneLieePPCommunePerso_NSMA'] = dirigeant7.adresseDirigeant7.siegeAdresseCommune;

//cadre 4
var nirDeclarantNSMA = dirigeant7.cadreDirigeantNonSalarie7.voletSocialNumeroSecuriteSocialNSMA;
if (nirDeclarantNSMA != null) {
    nirDeclarantNSMA = nirDeclarantNSMA.replace(/ /g, "");
    formFieldsNSm7['voletSocialNumeroSecuriteSocial_NSMA'] = nirDeclarantNSMA.substring(0, 13);
    formFieldsNSm7['voletSocialNumeroSecuriteSocialCle_NSMA'] = nirDeclarantNSMA.substring(13, 15);
}

formFieldsNSm7['voletSocialMSAOUI_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.voletSocialMSAOUINONNSMA ? true : false;
formFieldsNSm7['voletSocialMSANON_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.voletSocialMSAOUINONNSMA ? false : true;
formFieldsNSm7['voletSocialRegimeAssuranceMaladieRegimeGeneral_NSMA'] = Value('id').of(dirigeant7.cadreDirigeantNonSalarie7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralNSMA') ? true : false;
formFieldsNSm7['voletSocialRegimeAssuranceMaladieRegimeAgricole_NSMA'] = Value('id').of(dirigeant7.cadreDirigeantNonSalarie7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleNSMA') ? true : false;
formFieldsNSm7['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_NSMA'] = Value('id').of(dirigeant7.cadreDirigeantNonSalarie7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleNSMA') ? true : false;
formFieldsNSm7['voletSocialRegimeAssuranceMaladieRegimeAutre_NSMA'] = Value('id').of(dirigeant7.cadreDirigeantNonSalarie7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreNSMA') ? true : false;
formFieldsNSm7['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionNSMA;

formFieldsNSm7['voletSocialActiviteSimultaneeOUI_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.voletSocialActiviteSimultaneeOUINONNSMA ? true : false;
formFieldsNSm7['voletSocialActiviteSimultaneeNON_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.voletSocialActiviteSimultaneeOUINONNSMA ? false : true;

formFieldsNSm7['voletSocialActiviteSimultaneeSalarieAgricole_NSMA'] = Value('id').of(dirigeant7.cadreDirigeantNonSalarie7.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeSalarieAgricoleNSMA') ? true : false;
formFieldsNSm7['voletSocialActiviteSimultaneeSalarie_NSMA'] = Value('id').of(dirigeant7.cadreDirigeantNonSalarie7.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeSalarieNSMA') ? true : false;
formFieldsNSm7['voletSocialActiviteSimultaneeNonSalarieNonAgricole_NSMA'] = Value('id').of(dirigeant7.cadreDirigeantNonSalarie7.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeNonSalarieNonAgricoleNSMA') ? true : false;
formFieldsNSm7['voletSocialActiviteSimultaneeRetraite_NSMA'] = Value('id').of(dirigeant7.cadreDirigeantNonSalarie7.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeRetraiteNSMA') ? true : false;
formFieldsNSm7['voletSocialActiviteSimultaneePensionne_NSMA'] = Value('id').of(dirigeant7.cadreDirigeantNonSalarie7.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneePensionneNSMA') ? true : false;
formFieldsNSm7['voletSocialActiviteSimultaneeAutre_NSMA'] = Value('id').of(dirigeant7.cadreDirigeantNonSalarie7.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeAutreNSMA') ? true : false;
formFieldsNSm7['voletSocialActiviteSimultaneeAutrePrecision_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.voletSocialActiviteSimultaneeAutrePrecisionNSMA;
formFieldsNSm7['voletSocialActiviteSimultaneePensionOrganisme_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.voletSocialActiviteSimultaneePensionOrganismeNSMA;

formFieldsNSm7['voletSocialJeuneAgriculteurOUI_NSMA'] = Value('id').of(dirigeant7.cadreDirigeantNonSalarie7.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurOUINSMA') ? true : false;
formFieldsNSm7['voletSocialJeuneAgriculteurNON_NSMA'] = Value('id').of(dirigeant7.cadreDirigeantNonSalarie7.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurNONNSMA') ? true : false;
formFieldsNSm7['voletSocialJeuneAgriculteurENCOURS_NSMA'] = Value('id').of(dirigeant7.cadreDirigeantNonSalarie7.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurENCOURSNSMA') ? true : false;

formFieldsNSm7['voletSocialPacsRSAOUI_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.voletSocialPacsRSAOUINON ? true : false;
formFieldsNSm7['voletSocialPacsRSANON_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.voletSocialPacsRSAOUINON ? false : true;
formFieldsNSm7['voletSocialStatutConjointCollaborateur_NSMA'] = Value('id').of(dirigeant7.cadreDirigeantNonSalarie7.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointCollaborateurNSMA') ? true : false;
formFieldsNSm7['voletSocialStatutConjointSalarie_NSMA'] = Value('id').of(dirigeant7.cadreDirigeantNonSalarie7.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointSalarieNSMA') ? true : false;
formFieldsNSm7['voletSocialStatutConjointAssocie_NSMA'] = Value('id').of(dirigeant7.cadreDirigeantNonSalarie7.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointAssocieNSMA') ? true : false;

formFieldsNSm7['voletSocialConjointRegimeAssuranceOUI_NSMA'] = Value('id').of(dirigeant7.cadreDirigeantNonSalarie7.voletSocialConjointRegimeAssuranceOUINONNSMA).eq('voletSocialConjointRegimeAssuranceOUINSMA') ? true : false;
formFieldsNSm7['voletSocialConjointRegimeAssuranceNON_NSMA'] = Value('id').of(dirigeant7.cadreDirigeantNonSalarie7.voletSocialConjointRegimeAssuranceOUINONNSMA).eq('voletSocialConjointRegimeAssuranceNONNSMA') ? true : false;

var nirConjointNSMA = dirigeant7.cadreDirigeantNonSalarie7.voletSocialConjointNumeroSecuriteSocialNSMA;
if (nirConjointNSMA != null) {
    nirConjointNSMA = nirConjointNSMA.replace(/ /g, "");
    formFieldsNSm7['voletSocialConjointNumeroSecuriteSocial_NSMA'] = nirConjointNSMA.substring(0, 13);
    formFieldsNSm7['voletSocialConjointNumeroSecuriteSocialCle_NSMA'] = nirConjointNSMA.substring(13, 15);
} else {
    formFieldsNSm7['voletSocialConjointNumeroSecuriteSocial_NSMA'] = '';
    formFieldsNSm7['voletSocialConjointNumeroSecuriteSocialCle_NSMA'] = '';
}

//-----------------------AYANT-DROIT

//ayant droit numero 1
formFieldsNSm7['ayantDroitIdentiteNomNaissancePrenom1_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration1 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits71.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits71.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm7['ayantDroitNumeroSecuriteSociale1_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration1 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits71.presenceNumSSAM ? dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits71.ayantDroitNumeroSecuriteSociale1NSMA : '') : '';
formFieldsNSm7['ayantDroitDateNaissance1_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration1 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits71.presenceNumSSAM ? '' : dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits71.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm7['ayantDroitLieuNaissanceCommune1_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration1 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits71.presenceNumSSAM ? '' : (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits71.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits71.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits71.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm7['ayantDroitLienParente1_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration1 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits71.ayantDroitLienParenteNSMA) : '';
formFieldsNSm7['ayantDroitEnfantScolarise1OUI_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration1 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits71.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm7['ayantDroitEnfantScolarise1NON_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration1 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits71.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm7['ayantDroitNationalite1_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration1 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits71.ayantDroitNationaliteNSMA) : '';

// numero 2
formFieldsNSm7['ayantDroitIdentiteNomNaissancePrenom2_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration2 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits72.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits72.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm7['ayantDroitNumeroSecuriteSociale2_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration2 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits72.presenceNumSSAM ? dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits72.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm7['ayantDroitDateNaissance2_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration2 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits72.presenceNumSSAM ? '' : dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits72.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm7['ayantDroitLieuNaissanceCommune2_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration2 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits72.presenceNumSSAM ? '' : (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits72.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits72.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits72.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm7['ayantDroitLienParente2_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration2 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits72.ayantDroitLienParenteNSMA) : '';
formFieldsNSm7['ayantDroitEnfantScolarise2OUI_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration2 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits72.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm7['ayantDroitEnfantScolarise2NON_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration2 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits72.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm7['ayantDroitNationalite2_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration2 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits72.ayantDroitNationaliteNSMA) : '';

//numero 3
formFieldsNSm7['ayantDroitIdentiteNomNaissancePrenom3_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration3 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits73.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits73.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm7['ayantDroitNumeroSecuriteSociale3_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration3 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits73.presenceNumSSAM ? dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits73.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm7['ayantDroitDateNaissance3_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration3 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits73.presenceNumSSAM ? '' : dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits73.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm7['ayantDroitLieuNaissanceCommune3_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration3 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits73.presenceNumSSAM ? '' : (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits73.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits73.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits73.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm7['ayantDroitLienParente3_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration3 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits73.ayantDroitLienParenteNSMA) : '';
formFieldsNSm7['ayantDroitEnfantScolarise3OUI_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration3 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits73.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm7['ayantDroitEnfantScolarise3NON_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration3 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits73.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm7['ayantDroitNationalite3_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration3 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits73.ayantDroitNationaliteNSMA) : '';

//numero 4
formFieldsNSm7['ayantDroitIdentiteNomNaissancePrenom4_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration4 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits74.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits74.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm7['ayantDroitNumeroSecuriteSociale4_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration4 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits74.presenceNumSSAM ? dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits74.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm7['ayantDroitDateNaissance4_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration4 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits74.presenceNumSSAM ? '' : dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits74.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm7['ayantDroitLieuNaissanceCommune4_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration4 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits74.presenceNumSSAM ? '' : (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits74.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits74.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits74.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm7['ayantDroitLienParente4_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration4 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits74.ayantDroitLienParenteNSMA) : '';
formFieldsNSm7['ayantDroitEnfantScolarise4OUI_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration4 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits74.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm7['ayantDroitEnfantScolarise4NON_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration4 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits74.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm7['ayantDroitNationalite4_NSMA'] = dirigeant7.cadreDirigeantNonSalarie7.ayantDroitsDeclaration4 ? (dirigeant7.cadreDirigeantNonSalarie7.ayantsDroits74.ayantDroitNationaliteNSMA) : '';

//-----------------------
//Cadre 4B
formFieldsNSm7['voletSocialMembreGAECOUI_NSMA'] = Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant7.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECOUINSMA') ? true : false;
formFieldsNSm7['voletSocialMembreGAECNON_NSMA'] = Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant7.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECNONNSMA') ? true : false;

//Cadre 5
formFieldsNSm7['renseignementsComplementairesObservations_NSMA'] = $m0Agricole.cadreDirigeantGroup.cadreDirigeant7.cadreObservationsNSM7.renseignementsComplementairesObservationsNSMA;

//Cadre 6
formFieldsNSm7['renseignementsComplementairesLeDeclarant_NSMA'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal') != false ? true : false;
formFieldsNSm7['renseignementsComplementairesLeMandataire_NSMA'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire') != false ? true : false;
formFieldsNSm7['formaliteSignataire_NSMA'] = (tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire :'')
+ ' ' + (tag.adresseMandataire.numeroVoieMandataire != null ? tag.adresseMandataire.numeroVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.indiceVoieMandataire != null ? tag.adresseMandataire.indiceVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.typeVoieMandataire != null ? tag.adresseMandataire.typeVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.nomVoieMandataire != null ? tag.adresseMandataire.nomVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.complementVoieMandataire != null ? tag.adresseMandataire.complementVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
+ ' ' + (tag.adresseMandataire.codePostalMandataire != null ? tag.adresseMandataire.codePostalMandataire : '')
+ ' ' + (tag.adresseMandataire.villeAdresseMandataire != null ? tag.adresseMandataire.villeAdresseMandataire : ''); 
formFieldsNSm7['formaliteSignatureLieu_NSMA'] = tag.formaliteSignatureLieu;
if (tag.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSm7['formaliteSignatureDate_NSMA'] = tag.formaliteSignatureDate;
}

formFieldsNSm7['signature_NSMA'] = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";

// A remplir avec le formulaire principal
formFieldsNSm7['formaliteSignatureNBIntercalaire_NSMA'] = '';

//NSm 8


//cadre 1
formFieldsNSm8['formulaireDependanceAffiliation_NSMA'] = false;
formFieldsNSm8['formulaireDependanceModificationAffiliation_NSMA'] = false;
formFieldsNSm8['formulaireDependancePourGerant_NSMA'] = false;
formFieldsNSm8['formulaireDependancePourAssocie_NSMA'] = false;
formFieldsNSm8['formulaireDependanceM0_NSMA'] = true;
formFieldsNSm8['formulaireDependanceM2agricole_NSMA'] = false;
formFieldsNSm8['formulaireDependanceM3_NSMA'] = false;
formFieldsNSm8['formulaireDependanceIntercalaiaireNSM_NSMA'] = false;

//cadre 2
formFieldsNSm8['entrepriseDenomination_NSMA'] = societe.entrepriseDenomination;

//cadre 3

formFieldsNSm8['personneLieePPNomNaissance_NSMA'] = dirigeant8.civilitePersonnePhysique8.personneLieePPNomNaissance;
formFieldsNSm8['personneLieePPNomUsage_NSMA'] = dirigeant8.civilitePersonnePhysique8.personneLieePPNomUsage;
var prenoms = [];
for (i = 0; i < dirigeant8.civilitePersonnePhysique8.personneLieePPPrenom.size(); i++) {
    prenoms.push(dirigeant8.civilitePersonnePhysique8.personneLieePPPrenom[i]);
}
formFieldsNSm8['personneLieePPPrenom_NSMA'] = prenoms.toString();

//cadre 3B
if (dirigeant8.civilitePersonnePhysique8.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant8.civilitePersonnePhysique8.personneLieePPDateNaissance.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
    date = date.concat(dateTmp.getFullYear().toString());
    formFieldsNSm8['personneLieePPDateNaissance_NSMA'] = date;
} else
    formFieldsNSm8['personneLieePPDateNaissance_NSMA'] = '';

formFieldsNSm8['personneLieePPDepartementNaissance_NSMA'] = dirigeant8.civilitePersonnePhysique8.personneLieePPDepartementNaissance != null ? dirigeant8.civilitePersonnePhysique8.personneLieePPDepartementNaissance : '';
formFieldsNSm8['personneLieePPCommuneNaissance_NSMA'] = dirigeant8.civilitePersonnePhysique8.personneLieePPLieuNaissanceCommune != null ? dirigeant8.civilitePersonnePhysique8.personneLieePPLieuNaissanceCommune : (dirigeant8.civilitePersonnePhysique8.personneLieePPLieuNaissanceVille + ' / ' + dirigeant8.civilitePersonnePhysique8.personneLieePPLieuNaissancePays);

formFieldsNSm8['personneLieePPDomicilePerso_NSMA'] =
    (dirigeant8.adresseDirigeant8.siegeAdresseNumVoie != null ? dirigeant8.adresseDirigeant8.siegeAdresseNumVoie : '')
 + '  ' + (dirigeant8.adresseDirigeant8.siegeAdresseIndiceVoie != null ? dirigeant8.adresseDirigeant8.siegeAdresseIndiceVoie : '')
 + '  ' + (dirigeant8.adresseDirigeant8.siegeAdresseTypeVoie != null ? dirigeant8.adresseDirigeant8.siegeAdresseTypeVoie : '')
 + '  ' + dirigeant8.adresseDirigeant8.siegeAdresseNomVoie
 + '  ' + (dirigeant8.adresseDirigeant8.siegeAdresseComplementVoie != null ? dirigeant8.adresseDirigeant8.siegeAdresseComplementVoie : '')
 + '  ' + (dirigeant8.adresseDirigeant8.siegeAdresseDistributionSpecialeVoie != null ? dirigeant8.adresseDirigeant8.siegeAdresseDistributionSpecialeVoie : '');
formFieldsNSm8['personneLieePPCodePostalPerso_NSMA'] = dirigeant8.adresseDirigeant8.siegeAdresseCodePostal;
formFieldsNSm8['personneLieePPCommunePerso_NSMA'] = dirigeant8.adresseDirigeant8.siegeAdresseCommune;

//cadre 4
var nirDeclarantNSMA = dirigeant8.cadreDirigeantNonSalarie8.voletSocialNumeroSecuriteSocialNSMA;
if (nirDeclarantNSMA != null) {
    nirDeclarantNSMA = nirDeclarantNSMA.replace(/ /g, "");
    formFieldsNSm8['voletSocialNumeroSecuriteSocial_NSMA'] = nirDeclarantNSMA.substring(0, 13);
    formFieldsNSm8['voletSocialNumeroSecuriteSocialCle_NSMA'] = nirDeclarantNSMA.substring(13, 15);
}

formFieldsNSm8['voletSocialMSAOUI_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.voletSocialMSAOUINONNSMA ? true : false;
formFieldsNSm8['voletSocialMSANON_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.voletSocialMSAOUINONNSMA ? false : true;
formFieldsNSm8['voletSocialRegimeAssuranceMaladieRegimeGeneral_NSMA'] = Value('id').of(dirigeant8.cadreDirigeantNonSalarie8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralNSMA') ? true : false;
formFieldsNSm8['voletSocialRegimeAssuranceMaladieRegimeAgricole_NSMA'] = Value('id').of(dirigeant8.cadreDirigeantNonSalarie8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleNSMA') ? true : false;
formFieldsNSm8['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_NSMA'] = Value('id').of(dirigeant8.cadreDirigeantNonSalarie8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleNSMA') ? true : false;
formFieldsNSm8['voletSocialRegimeAssuranceMaladieRegimeAutre_NSMA'] = Value('id').of(dirigeant8.cadreDirigeantNonSalarie8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreNSMA') ? true : false;
formFieldsNSm8['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionNSMA;

formFieldsNSm8['voletSocialActiviteSimultaneeOUI_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.voletSocialActiviteSimultaneeOUINONNSMA ? true : false;
formFieldsNSm8['voletSocialActiviteSimultaneeNON_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.voletSocialActiviteSimultaneeOUINONNSMA ? false : true;

formFieldsNSm8['voletSocialActiviteSimultaneeSalarieAgricole_NSMA'] = Value('id').of(dirigeant8.cadreDirigeantNonSalarie8.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeSalarieAgricoleNSMA') ? true : false;
formFieldsNSm8['voletSocialActiviteSimultaneeSalarie_NSMA'] = Value('id').of(dirigeant8.cadreDirigeantNonSalarie8.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeSalarieNSMA') ? true : false;
formFieldsNSm8['voletSocialActiviteSimultaneeNonSalarieNonAgricole_NSMA'] = Value('id').of(dirigeant8.cadreDirigeantNonSalarie8.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeNonSalarieNonAgricoleNSMA') ? true : false;
formFieldsNSm8['voletSocialActiviteSimultaneeRetraite_NSMA'] = Value('id').of(dirigeant8.cadreDirigeantNonSalarie8.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeRetraiteNSMA') ? true : false;
formFieldsNSm8['voletSocialActiviteSimultaneePensionne_NSMA'] = Value('id').of(dirigeant8.cadreDirigeantNonSalarie8.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneePensionneNSMA') ? true : false;
formFieldsNSm8['voletSocialActiviteSimultaneeAutre_NSMA'] = Value('id').of(dirigeant8.cadreDirigeantNonSalarie8.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeAutreNSMA') ? true : false;
formFieldsNSm8['voletSocialActiviteSimultaneeAutrePrecision_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.voletSocialActiviteSimultaneeAutrePrecisionNSMA;
formFieldsNSm8['voletSocialActiviteSimultaneePensionOrganisme_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.voletSocialActiviteSimultaneePensionOrganismeNSMA;

formFieldsNSm8['voletSocialJeuneAgriculteurOUI_NSMA'] = Value('id').of(dirigeant8.cadreDirigeantNonSalarie8.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurOUINSMA') ? true : false;
formFieldsNSm8['voletSocialJeuneAgriculteurNON_NSMA'] = Value('id').of(dirigeant8.cadreDirigeantNonSalarie8.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurNONNSMA') ? true : false;
formFieldsNSm8['voletSocialJeuneAgriculteurENCOURS_NSMA'] = Value('id').of(dirigeant8.cadreDirigeantNonSalarie8.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurENCOURSNSMA') ? true : false;

formFieldsNSm8['voletSocialPacsRSAOUI_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.voletSocialPacsRSAOUINON ? true : false;
formFieldsNSm8['voletSocialPacsRSANON_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.voletSocialPacsRSAOUINON ? false : true;
formFieldsNSm8['voletSocialStatutConjointCollaborateur_NSMA'] = Value('id').of(dirigeant8.cadreDirigeantNonSalarie8.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointCollaborateurNSMA') ? true : false;
formFieldsNSm8['voletSocialStatutConjointSalarie_NSMA'] = Value('id').of(dirigeant8.cadreDirigeantNonSalarie8.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointSalarieNSMA') ? true : false;
formFieldsNSm8['voletSocialStatutConjointAssocie_NSMA'] = Value('id').of(dirigeant8.cadreDirigeantNonSalarie8.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointAssocieNSMA') ? true : false;

formFieldsNSm8['voletSocialConjointRegimeAssuranceOUI_NSMA'] = Value('id').of(dirigeant8.cadreDirigeantNonSalarie8.voletSocialConjointRegimeAssuranceOUINONNSMA).eq('voletSocialConjointRegimeAssuranceOUINSMA') ? true : false;
formFieldsNSm8['voletSocialConjointRegimeAssuranceNON_NSMA'] = Value('id').of(dirigeant8.cadreDirigeantNonSalarie8.voletSocialConjointRegimeAssuranceOUINONNSMA).eq('voletSocialConjointRegimeAssuranceNONNSMA') ? true : false;

var nirConjointNSMA = dirigeant8.cadreDirigeantNonSalarie8.voletSocialConjointNumeroSecuriteSocialNSMA;
if (nirConjointNSMA != null) {
    nirConjointNSMA = nirConjointNSMA.replace(/ /g, "");
    formFieldsNSm8['voletSocialConjointNumeroSecuriteSocial_NSMA'] = nirConjointNSMA.substring(0, 13);
    formFieldsNSm8['voletSocialConjointNumeroSecuriteSocialCle_NSMA'] = nirConjointNSMA.substring(13, 15);
} else {
    formFieldsNSm8['voletSocialConjointNumeroSecuriteSocial_NSMA'] = '';
    formFieldsNSm8['voletSocialConjointNumeroSecuriteSocialCle_NSMA'] = '';
}

//-----------------------AYANT-DROIT

//ayant droit numero 1
formFieldsNSm8['ayantDroitIdentiteNomNaissancePrenom1_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration1 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits81.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits81.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm8['ayantDroitNumeroSecuriteSociale1_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration1 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits81.presenceNumSSAM ? dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits81.ayantDroitNumeroSecuriteSociale1NSMA : '') : '';
formFieldsNSm8['ayantDroitDateNaissance1_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration1 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits81.presenceNumSSAM ? '' : dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits81.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm8['ayantDroitLieuNaissanceCommune1_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration1 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits81.presenceNumSSAM ? '' : (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits81.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits81.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits81.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm8['ayantDroitLienParente1_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration1 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits81.ayantDroitLienParenteNSMA) : '';
formFieldsNSm8['ayantDroitEnfantScolarise1OUI_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration1 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits81.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm8['ayantDroitEnfantScolarise1NON_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration1 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits81.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm8['ayantDroitNationalite1_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration1 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits81.ayantDroitNationaliteNSMA) : '';

// numero 2
formFieldsNSm8['ayantDroitIdentiteNomNaissancePrenom2_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration2 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits82.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits82.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm8['ayantDroitNumeroSecuriteSociale2_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration2 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits82.presenceNumSSAM ? dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits82.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm8['ayantDroitDateNaissance2_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration2 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits82.presenceNumSSAM ? '' : dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits82.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm8['ayantDroitLieuNaissanceCommune2_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration2 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits82.presenceNumSSAM ? '' : (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits82.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits82.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits82.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm8['ayantDroitLienParente2_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration2 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits82.ayantDroitLienParenteNSMA) : '';
formFieldsNSm8['ayantDroitEnfantScolarise2OUI_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration2 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits82.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm8['ayantDroitEnfantScolarise2NON_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration2 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits82.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm8['ayantDroitNationalite2_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration2 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits82.ayantDroitNationaliteNSMA) : '';

//numero 3
formFieldsNSm8['ayantDroitIdentiteNomNaissancePrenom3_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration3 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits83.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits83.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm8['ayantDroitNumeroSecuriteSociale3_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration3 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits83.presenceNumSSAM ? dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits83.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm8['ayantDroitDateNaissance3_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration3 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits83.presenceNumSSAM ? '' : dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits83.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm8['ayantDroitLieuNaissanceCommune3_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration3 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits83.presenceNumSSAM ? '' : (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits83.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits83.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits83.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm8['ayantDroitLienParente3_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration3 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits83.ayantDroitLienParenteNSMA) : '';
formFieldsNSm8['ayantDroitEnfantScolarise3OUI_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration3 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits83.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm8['ayantDroitEnfantScolarise3NON_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration3 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits83.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm8['ayantDroitNationalite3_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration3 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits83.ayantDroitNationaliteNSMA) : '';

//numero 4
formFieldsNSm8['ayantDroitIdentiteNomNaissancePrenom4_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration4 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits84.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits84.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm8['ayantDroitNumeroSecuriteSociale4_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration4 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits84.presenceNumSSAM ? dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits84.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm8['ayantDroitDateNaissance4_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration4 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits84.presenceNumSSAM ? '' : dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits84.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm8['ayantDroitLieuNaissanceCommune4_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration4 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits84.presenceNumSSAM ? '' : (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits84.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits84.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits84.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm8['ayantDroitLienParente4_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration4 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits84.ayantDroitLienParenteNSMA) : '';
formFieldsNSm8['ayantDroitEnfantScolarise4OUI_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration4 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits84.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm8['ayantDroitEnfantScolarise4NON_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration4 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits84.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm8['ayantDroitNationalite4_NSMA'] = dirigeant8.cadreDirigeantNonSalarie8.ayantDroitsDeclaration4 ? (dirigeant8.cadreDirigeantNonSalarie8.ayantsDroits84.ayantDroitNationaliteNSMA) : '';

//-----------------------
//Cadre 4B
formFieldsNSm8['voletSocialMembreGAECOUI_NSMA'] = Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant8.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECOUINSMA') ? true : false;
formFieldsNSm8['voletSocialMembreGAECNON_NSMA'] = Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant8.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECNONNSMA') ? true : false;

//Cadre 5
formFieldsNSm8['renseignementsComplementairesObservations_NSMA'] = $m0Agricole.cadreDirigeantGroup.cadreDirigeant8.cadreObservationsNSM8.renseignementsComplementairesObservationsNSMA;

//Cadre 6
formFieldsNSm8['renseignementsComplementairesLeDeclarant_NSMA'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal') != false ? true : false;
formFieldsNSm8['renseignementsComplementairesLeMandataire_NSMA'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire') != false ? true : false;
formFieldsNSm8['formaliteSignataire_NSMA'] = (tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire :'')
+ ' ' + (tag.adresseMandataire.numeroVoieMandataire != null ? tag.adresseMandataire.numeroVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.indiceVoieMandataire != null ? tag.adresseMandataire.indiceVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.typeVoieMandataire != null ? tag.adresseMandataire.typeVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.nomVoieMandataire != null ? tag.adresseMandataire.nomVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.complementVoieMandataire != null ? tag.adresseMandataire.complementVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
+ ' ' + (tag.adresseMandataire.codePostalMandataire != null ? tag.adresseMandataire.codePostalMandataire : '')
+ ' ' + (tag.adresseMandataire.villeAdresseMandataire != null ? tag.adresseMandataire.villeAdresseMandataire : ''); 
formFieldsNSm8['formaliteSignatureLieu_NSMA'] = tag.formaliteSignatureLieu;
if (tag.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSm8['formaliteSignatureDate_NSMA'] = tag.formaliteSignatureDate;
}
formFieldsNSm8['signature_NSMA'] = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";

// A remplir avec le formulaire principal
formFieldsNSm8['formaliteSignatureNBIntercalaire_NSMA'] = '';

//NSm 9


//cadre 1
formFieldsNSm9['formulaireDependanceAffiliation_NSMA'] = false;
formFieldsNSm9['formulaireDependanceModificationAffiliation_NSMA'] = false;
formFieldsNSm9['formulaireDependancePourGerant_NSMA'] = false;
formFieldsNSm9['formulaireDependancePourAssocie_NSMA'] = false;
formFieldsNSm9['formulaireDependanceM0_NSMA'] = true;
formFieldsNSm9['formulaireDependanceM2agricole_NSMA'] = false;
formFieldsNSm9['formulaireDependanceM3_NSMA'] = false;
formFieldsNSm9['formulaireDependanceIntercalaiaireNSM_NSMA'] = false;

//cadre 2
formFieldsNSm9['entrepriseDenomination_NSMA'] = societe.entrepriseDenomination;

//cadre 3

formFieldsNSm9['personneLieePPNomNaissance_NSMA'] = dirigeant9.civilitePersonnePhysique9.personneLieePPNomNaissance;
formFieldsNSm9['personneLieePPNomUsage_NSMA'] = dirigeant9.civilitePersonnePhysique9.personneLieePPNomUsage;
var prenoms = [];
for (i = 0; i < dirigeant9.civilitePersonnePhysique9.personneLieePPPrenom.size(); i++) {
    prenoms.push(dirigeant9.civilitePersonnePhysique9.personneLieePPPrenom[i]);
}
formFieldsNSm9['personneLieePPPrenom_NSMA'] = prenoms.toString();

//cadre 3B
if (dirigeant9.civilitePersonnePhysique9.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant9.civilitePersonnePhysique9.personneLieePPDateNaissance.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
    date = date.concat(dateTmp.getFullYear().toString());
    formFieldsNSm9['personneLieePPDateNaissance_NSMA'] = date;
} else
    formFieldsNSm9['personneLieePPDateNaissance_NSMA'] = '';

formFieldsNSm9['personneLieePPDepartementNaissance_NSMA'] = dirigeant9.civilitePersonnePhysique9.personneLieePPDepartementNaissance != null ? dirigeant9.civilitePersonnePhysique9.personneLieePPDepartementNaissance : '';
formFieldsNSm9['personneLieePPCommuneNaissance_NSMA'] = dirigeant9.civilitePersonnePhysique9.personneLieePPLieuNaissanceCommune != null ? dirigeant9.civilitePersonnePhysique9.personneLieePPLieuNaissanceCommune : (dirigeant9.civilitePersonnePhysique9.personneLieePPLieuNaissanceVille + ' / ' + dirigeant9.civilitePersonnePhysique9.personneLieePPLieuNaissancePays);

formFieldsNSm9['personneLieePPDomicilePerso_NSMA'] =
    (dirigeant9.adresseDirigeant9.siegeAdresseNumVoie != null ? dirigeant9.adresseDirigeant9.siegeAdresseNumVoie : '')
 + '  ' + (dirigeant9.adresseDirigeant9.siegeAdresseIndiceVoie != null ? dirigeant9.adresseDirigeant9.siegeAdresseIndiceVoie : '')
 + '  ' + (dirigeant9.adresseDirigeant9.siegeAdresseTypeVoie != null ? dirigeant9.adresseDirigeant9.siegeAdresseTypeVoie : '')
 + '  ' + dirigeant9.adresseDirigeant9.siegeAdresseNomVoie
 + '  ' + (dirigeant9.adresseDirigeant9.siegeAdresseComplementVoie != null ? dirigeant9.adresseDirigeant9.siegeAdresseComplementVoie : '')
 + '  ' + (dirigeant9.adresseDirigeant9.siegeAdresseDistributionSpecialeVoie != null ? dirigeant9.adresseDirigeant9.siegeAdresseDistributionSpecialeVoie : '');
formFieldsNSm9['personneLieePPCodePostalPerso_NSMA'] = dirigeant9.adresseDirigeant9.siegeAdresseCodePostal;
formFieldsNSm9['personneLieePPCommunePerso_NSMA'] = dirigeant9.adresseDirigeant9.siegeAdresseCommune;

//cadre 4
var nirDeclarantNSMA = dirigeant9.cadreDirigeantNonSalarie9.voletSocialNumeroSecuriteSocialNSMA;
if (nirDeclarantNSMA != null) {
    nirDeclarantNSMA = nirDeclarantNSMA.replace(/ /g, "");
    formFieldsNSm9['voletSocialNumeroSecuriteSocial_NSMA'] = nirDeclarantNSMA.substring(0, 13);
    formFieldsNSm9['voletSocialNumeroSecuriteSocialCle_NSMA'] = nirDeclarantNSMA.substring(13, 15);
}

formFieldsNSm9['voletSocialMSAOUI_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.voletSocialMSAOUINONNSMA ? true : false;
formFieldsNSm9['voletSocialMSANON_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.voletSocialMSAOUINONNSMA ? false : true;
formFieldsNSm9['voletSocialRegimeAssuranceMaladieRegimeGeneral_NSMA'] = Value('id').of(dirigeant9.cadreDirigeantNonSalarie9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralNSMA') ? true : false;
formFieldsNSm9['voletSocialRegimeAssuranceMaladieRegimeAgricole_NSMA'] = Value('id').of(dirigeant9.cadreDirigeantNonSalarie9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleNSMA') ? true : false;
formFieldsNSm9['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_NSMA'] = Value('id').of(dirigeant9.cadreDirigeantNonSalarie9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleNSMA') ? true : false;
formFieldsNSm9['voletSocialRegimeAssuranceMaladieRegimeAutre_NSMA'] = Value('id').of(dirigeant9.cadreDirigeantNonSalarie9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreNSMA') ? true : false;
formFieldsNSm9['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionNSMA;

formFieldsNSm9['voletSocialActiviteSimultaneeOUI_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.voletSocialActiviteSimultaneeOUINONNSMA ? true : false;
formFieldsNSm9['voletSocialActiviteSimultaneeNON_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.voletSocialActiviteSimultaneeOUINONNSMA ? false : true;

formFieldsNSm9['voletSocialActiviteSimultaneeSalarieAgricole_NSMA'] = Value('id').of(dirigeant9.cadreDirigeantNonSalarie9.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeSalarieAgricoleNSMA') ? true : false;
formFieldsNSm9['voletSocialActiviteSimultaneeSalarie_NSMA'] = Value('id').of(dirigeant9.cadreDirigeantNonSalarie9.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeSalarieNSMA') ? true : false;
formFieldsNSm9['voletSocialActiviteSimultaneeNonSalarieNonAgricole_NSMA'] = Value('id').of(dirigeant9.cadreDirigeantNonSalarie9.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeNonSalarieNonAgricoleNSMA') ? true : false;
formFieldsNSm9['voletSocialActiviteSimultaneeRetraite_NSMA'] = Value('id').of(dirigeant9.cadreDirigeantNonSalarie9.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeRetraiteNSMA') ? true : false;
formFieldsNSm9['voletSocialActiviteSimultaneePensionne_NSMA'] = Value('id').of(dirigeant9.cadreDirigeantNonSalarie9.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneePensionneNSMA') ? true : false;
formFieldsNSm9['voletSocialActiviteSimultaneeAutre_NSMA'] = Value('id').of(dirigeant9.cadreDirigeantNonSalarie9.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeAutreNSMA') ? true : false;
formFieldsNSm9['voletSocialActiviteSimultaneeAutrePrecision_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.voletSocialActiviteSimultaneeAutrePrecisionNSMA;
formFieldsNSm9['voletSocialActiviteSimultaneePensionOrganisme_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.voletSocialActiviteSimultaneePensionOrganismeNSMA;

formFieldsNSm9['voletSocialJeuneAgriculteurOUI_NSMA'] = Value('id').of(dirigeant9.cadreDirigeantNonSalarie9.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurOUINSMA') ? true : false;
formFieldsNSm9['voletSocialJeuneAgriculteurNON_NSMA'] = Value('id').of(dirigeant9.cadreDirigeantNonSalarie9.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurNONNSMA') ? true : false;
formFieldsNSm9['voletSocialJeuneAgriculteurENCOURS_NSMA'] = Value('id').of(dirigeant9.cadreDirigeantNonSalarie9.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurENCOURSNSMA') ? true : false;

formFieldsNSm9['voletSocialPacsRSAOUI_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.voletSocialPacsRSAOUINON ? true : false;
formFieldsNSm9['voletSocialPacsRSANON_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.voletSocialPacsRSAOUINON ? false : true;
formFieldsNSm9['voletSocialStatutConjointCollaborateur_NSMA'] = Value('id').of(dirigeant9.cadreDirigeantNonSalarie9.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointCollaborateurNSMA') ? true : false;
formFieldsNSm9['voletSocialStatutConjointSalarie_NSMA'] = Value('id').of(dirigeant9.cadreDirigeantNonSalarie9.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointSalarieNSMA') ? true : false;
formFieldsNSm9['voletSocialStatutConjointAssocie_NSMA'] = Value('id').of(dirigeant9.cadreDirigeantNonSalarie9.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointAssocieNSMA') ? true : false;

formFieldsNSm9['voletSocialConjointRegimeAssuranceOUI_NSMA'] = Value('id').of(dirigeant9.cadreDirigeantNonSalarie9.voletSocialConjointRegimeAssuranceOUINONNSMA).eq('voletSocialConjointRegimeAssuranceOUINSMA') ? true : false;
formFieldsNSm9['voletSocialConjointRegimeAssuranceNON_NSMA'] = Value('id').of(dirigeant9.cadreDirigeantNonSalarie9.voletSocialConjointRegimeAssuranceOUINONNSMA).eq('voletSocialConjointRegimeAssuranceNONNSMA') ? true : false;

var nirConjointNSMA = dirigeant9.cadreDirigeantNonSalarie9.voletSocialConjointNumeroSecuriteSocialNSMA;
if (nirConjointNSMA != null) {
    nirConjointNSMA = nirConjointNSMA.replace(/ /g, "");
    formFieldsNSm9['voletSocialConjointNumeroSecuriteSocial_NSMA'] = nirConjointNSMA.substring(0, 13);
    formFieldsNSm9['voletSocialConjointNumeroSecuriteSocialCle_NSMA'] = nirConjointNSMA.substring(13, 15);
} else {
    formFieldsNSm9['voletSocialConjointNumeroSecuriteSocial_NSMA'] = '';
    formFieldsNSm9['voletSocialConjointNumeroSecuriteSocialCle_NSMA'] = '';
}

//-----------------------AYANT-DROIT

//ayant droit numero 1
formFieldsNSm9['ayantDroitIdentiteNomNaissancePrenom1_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration1 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits91.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits91.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm9['ayantDroitNumeroSecuriteSociale1_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration1 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits91.presenceNumSSAM ? dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits91.ayantDroitNumeroSecuriteSociale1NSMA : '') : '';
formFieldsNSm9['ayantDroitDateNaissance1_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration1 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits91.presenceNumSSAM ? '' : dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits91.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm9['ayantDroitLieuNaissanceCommune1_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration1 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits91.presenceNumSSAM ? '' : (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits91.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits91.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits91.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm9['ayantDroitLienParente1_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration1 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits91.ayantDroitLienParenteNSMA) : '';
formFieldsNSm9['ayantDroitEnfantScolarise1OUI_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration1 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits91.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm9['ayantDroitEnfantScolarise1NON_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration1 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits91.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm9['ayantDroitNationalite1_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration1 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits91.ayantDroitNationaliteNSMA) : '';

// numero 2
formFieldsNSm9['ayantDroitIdentiteNomNaissancePrenom2_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration2 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits92.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits92.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm9['ayantDroitNumeroSecuriteSociale2_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration2 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits92.presenceNumSSAM ? dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits92.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm9['ayantDroitDateNaissance2_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration2 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits92.presenceNumSSAM ? '' : dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits92.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm9['ayantDroitLieuNaissanceCommune2_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration2 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits92.presenceNumSSAM ? '' : (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits92.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits92.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits92.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm9['ayantDroitLienParente2_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration2 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits92.ayantDroitLienParenteNSMA) : '';
formFieldsNSm9['ayantDroitEnfantScolarise2OUI_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration2 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits92.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm9['ayantDroitEnfantScolarise2NON_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration2 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits92.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm9['ayantDroitNationalite2_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration2 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits92.ayantDroitNationaliteNSMA) : '';

//numero 3
formFieldsNSm9['ayantDroitIdentiteNomNaissancePrenom3_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration3 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits93.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits93.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm9['ayantDroitNumeroSecuriteSociale3_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration3 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits93.presenceNumSSAM ? dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits93.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm9['ayantDroitDateNaissance3_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration3 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits93.presenceNumSSAM ? '' : dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits93.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm9['ayantDroitLieuNaissanceCommune3_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration3 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits93.presenceNumSSAM ? '' : (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits93.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits93.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits93.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm9['ayantDroitLienParente3_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration3 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits93.ayantDroitLienParenteNSMA) : '';
formFieldsNSm9['ayantDroitEnfantScolarise3OUI_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration3 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits93.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm9['ayantDroitEnfantScolarise3NON_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration3 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits93.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm9['ayantDroitNationalite3_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration3 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits93.ayantDroitNationaliteNSMA) : '';

//numero 4
formFieldsNSm9['ayantDroitIdentiteNomNaissancePrenom4_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration4 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits94.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits94.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm9['ayantDroitNumeroSecuriteSociale4_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration4 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits94.presenceNumSSAM ? dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits94.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm9['ayantDroitDateNaissance4_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration4 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits94.presenceNumSSAM ? '' : dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits94.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm9['ayantDroitLieuNaissanceCommune4_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration4 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits94.presenceNumSSAM ? '' : (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits94.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits94.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits94.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm9['ayantDroitLienParente4_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration4 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits94.ayantDroitLienParenteNSMA) : '';
formFieldsNSm9['ayantDroitEnfantScolarise4OUI_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration4 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits94.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm9['ayantDroitEnfantScolarise4NON_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration4 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits94.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm9['ayantDroitNationalite4_NSMA'] = dirigeant9.cadreDirigeantNonSalarie9.ayantDroitsDeclaration4 ? (dirigeant9.cadreDirigeantNonSalarie9.ayantsDroits94.ayantDroitNationaliteNSMA) : '';

//-----------------------
//Cadre 4B
formFieldsNSm9['voletSocialMembreGAECOUI_NSMA'] = Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant9.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECOUINSMA') ? true : false;
formFieldsNSm9['voletSocialMembreGAECNON_NSMA'] = Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant9.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECNONNSMA') ? true : false;

//Cadre 5
formFieldsNSm9['renseignementsComplementairesObservations_NSMA'] = $m0Agricole.cadreDirigeantGroup.cadreDirigeant9.cadreObservationsNSM9.renseignementsComplementairesObservationsNSMA;

//Cadre 6
formFieldsNSm9['renseignementsComplementairesLeDeclarant_NSMA'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal') != false ? true : false;
formFieldsNSm9['renseignementsComplementairesLeMandataire_NSMA'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire') != false ? true : false;
formFieldsNSm9['formaliteSignataire_NSMA'] = (tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire :'')
+ ' ' + (tag.adresseMandataire.numeroVoieMandataire != null ? tag.adresseMandataire.numeroVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.indiceVoieMandataire != null ? tag.adresseMandataire.indiceVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.typeVoieMandataire != null ? tag.adresseMandataire.typeVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.nomVoieMandataire != null ? tag.adresseMandataire.nomVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.complementVoieMandataire != null ? tag.adresseMandataire.complementVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
+ ' ' + (tag.adresseMandataire.codePostalMandataire != null ? tag.adresseMandataire.codePostalMandataire : '')
+ ' ' + (tag.adresseMandataire.villeAdresseMandataire != null ? tag.adresseMandataire.villeAdresseMandataire : ''); 
formFieldsNSm9['formaliteSignatureLieu_NSMA'] = tag.formaliteSignatureLieu;
if (tag.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSm9['formaliteSignatureDate_NSMA'] = tag.formaliteSignatureDate;
}

formFieldsNSm9['signature_NSMA'] = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";

// A remplir avec le formulaire principal
formFieldsNSm9['formaliteSignatureNBIntercalaire_NSMA'] = '';

//NSm 10


//cadre 1
formFieldsNSm10['formulaireDependanceAffiliation_NSMA'] = false;
formFieldsNSm10['formulaireDependanceModificationAffiliation_NSMA'] = false;
formFieldsNSm10['formulaireDependancePourGerant_NSMA'] = false;
formFieldsNSm10['formulaireDependancePourAssocie_NSMA'] = false;
formFieldsNSm10['formulaireDependanceM0_NSMA'] = true;
formFieldsNSm10['formulaireDependanceM2agricole_NSMA'] = false;
formFieldsNSm10['formulaireDependanceM3_NSMA'] = false;
formFieldsNSm10['formulaireDependanceIntercalaiaireNSM_NSMA'] = false;

//cadre 2
formFieldsNSm10['entrepriseDenomination_NSMA'] = societe.entrepriseDenomination;

//cadre 3

formFieldsNSm10['personneLieePPNomNaissance_NSMA'] = dirigeant10.civilitePersonnePhysique10.personneLieePPNomNaissance;
formFieldsNSm10['personneLieePPNomUsage_NSMA'] = dirigeant10.civilitePersonnePhysique10.personneLieePPNomUsage;
var prenoms = [];
for (i = 0; i < dirigeant10.civilitePersonnePhysique10.personneLieePPPrenom.size(); i++) {
    prenoms.push(dirigeant10.civilitePersonnePhysique10.personneLieePPPrenom[i]);
}
formFieldsNSm10['personneLieePPPrenom_NSMA'] = prenoms.toString();

//cadre 3B
if (dirigeant10.civilitePersonnePhysique10.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(dirigeant10.civilitePersonnePhysique10.personneLieePPDateNaissance.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
    date = date.concat(dateTmp.getFullYear().toString());
    formFieldsNSm10['personneLieePPDateNaissance_NSMA'] = date;
} else
    formFieldsNSm10['personneLieePPDateNaissance_NSMA'] = '';

formFieldsNSm10['personneLieePPDepartementNaissance_NSMA'] = dirigeant10.civilitePersonnePhysique10.personneLieePPDepartementNaissance != null ? dirigeant10.civilitePersonnePhysique10.personneLieePPDepartementNaissance : '';
formFieldsNSm10['personneLieePPCommuneNaissance_NSMA'] = dirigeant10.civilitePersonnePhysique10.personneLieePPLieuNaissanceCommune != null ? dirigeant10.civilitePersonnePhysique10.personneLieePPLieuNaissanceCommune : (dirigeant10.civilitePersonnePhysique10.personneLieePPLieuNaissanceVille + ' / ' + dirigeant10.civilitePersonnePhysique10.personneLieePPLieuNaissancePays);

formFieldsNSm10['personneLieePPDomicilePerso_NSMA'] =
    (dirigeant10.adresseDirigeant10.siegeAdresseNumVoie != null ? dirigeant10.adresseDirigeant10.siegeAdresseNumVoie : '')
 + '  ' + (dirigeant10.adresseDirigeant10.siegeAdresseIndiceVoie != null ? dirigeant10.adresseDirigeant10.siegeAdresseIndiceVoie : '')
 + '  ' + (dirigeant10.adresseDirigeant10.siegeAdresseTypeVoie != null ? dirigeant10.adresseDirigeant10.siegeAdresseTypeVoie : '')
 + '  ' + dirigeant10.adresseDirigeant10.siegeAdresseNomVoie
 + '  ' + (dirigeant10.adresseDirigeant10.siegeAdresseComplementVoie != null ? dirigeant10.adresseDirigeant10.siegeAdresseComplementVoie : '')
 + '  ' + (dirigeant10.adresseDirigeant10.siegeAdresseDistributionSpecialeVoie != null ? dirigeant10.adresseDirigeant10.siegeAdresseDistributionSpecialeVoie : '');
formFieldsNSm10['personneLieePPCodePostalPerso_NSMA'] = dirigeant10.adresseDirigeant10.siegeAdresseCodePostal;
formFieldsNSm10['personneLieePPCommunePerso_NSMA'] = dirigeant10.adresseDirigeant10.siegeAdresseCommune;

//cadre 4
var nirDeclarantNSMA = dirigeant10.cadreDirigeantNonSalarie10.voletSocialNumeroSecuriteSocialNSMA;
if (nirDeclarantNSMA != null) {
    nirDeclarantNSMA = nirDeclarantNSMA.replace(/ /g, "");
    formFieldsNSm10['voletSocialNumeroSecuriteSocial_NSMA'] = nirDeclarantNSMA.substring(0, 13);
    formFieldsNSm10['voletSocialNumeroSecuriteSocialCle_NSMA'] = nirDeclarantNSMA.substring(13, 15);
}

formFieldsNSm10['voletSocialMSAOUI_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.voletSocialMSAOUINONNSMA ? true : false;
formFieldsNSm10['voletSocialMSANON_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.voletSocialMSAOUINONNSMA ? false : true;
formFieldsNSm10['voletSocialRegimeAssuranceMaladieRegimeGeneral_NSMA'] = Value('id').of(dirigeant10.cadreDirigeantNonSalarie10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralNSMA') ? true : false;
formFieldsNSm10['voletSocialRegimeAssuranceMaladieRegimeAgricole_NSMA'] = Value('id').of(dirigeant10.cadreDirigeantNonSalarie10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleNSMA') ? true : false;
formFieldsNSm10['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_NSMA'] = Value('id').of(dirigeant10.cadreDirigeantNonSalarie10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleNSMA') ? true : false;
formFieldsNSm10['voletSocialRegimeAssuranceMaladieRegimeAutre_NSMA'] = Value('id').of(dirigeant10.cadreDirigeantNonSalarie10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreNSMA') ? true : false;
formFieldsNSm10['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionNSMA;

formFieldsNSm10['voletSocialActiviteSimultaneeOUI_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.voletSocialActiviteSimultaneeOUINONNSMA ? true : false;
formFieldsNSm10['voletSocialActiviteSimultaneeNON_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.voletSocialActiviteSimultaneeOUINONNSMA ? false : true;

formFieldsNSm10['voletSocialActiviteSimultaneeSalarieAgricole_NSMA'] = Value('id').of(dirigeant10.cadreDirigeantNonSalarie10.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeSalarieAgricoleNSMA') ? true : false;
formFieldsNSm10['voletSocialActiviteSimultaneeSalarie_NSMA'] = Value('id').of(dirigeant10.cadreDirigeantNonSalarie10.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeSalarieNSMA') ? true : false;
formFieldsNSm10['voletSocialActiviteSimultaneeNonSalarieNonAgricole_NSMA'] = Value('id').of(dirigeant10.cadreDirigeantNonSalarie10.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeNonSalarieNonAgricoleNSMA') ? true : false;
formFieldsNSm10['voletSocialActiviteSimultaneeRetraite_NSMA'] = Value('id').of(dirigeant10.cadreDirigeantNonSalarie10.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeRetraiteNSMA') ? true : false;
formFieldsNSm10['voletSocialActiviteSimultaneePensionne_NSMA'] = Value('id').of(dirigeant10.cadreDirigeantNonSalarie10.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneePensionneNSMA') ? true : false;
formFieldsNSm10['voletSocialActiviteSimultaneeAutre_NSMA'] = Value('id').of(dirigeant10.cadreDirigeantNonSalarie10.voletSocialActiviteSimultaneeNSMA).eq('voletSocialActiviteSimultaneeAutreNSMA') ? true : false;
formFieldsNSm10['voletSocialActiviteSimultaneeAutrePrecision_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.voletSocialActiviteSimultaneeAutrePrecisionNSMA;
formFieldsNSm10['voletSocialActiviteSimultaneePensionOrganisme_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.voletSocialActiviteSimultaneePensionOrganismeNSMA;

formFieldsNSm10['voletSocialJeuneAgriculteurOUI_NSMA'] = Value('id').of(dirigeant10.cadreDirigeantNonSalarie10.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurOUINSMA') ? true : false;
formFieldsNSm10['voletSocialJeuneAgriculteurNON_NSMA'] = Value('id').of(dirigeant10.cadreDirigeantNonSalarie10.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurNONNSMA') ? true : false;
formFieldsNSm10['voletSocialJeuneAgriculteurENCOURS_NSMA'] = Value('id').of(dirigeant10.cadreDirigeantNonSalarie10.voletSocialJeuneAgriculteurNSMA).eq('voletSocialJeuneAgriculteurENCOURSNSMA') ? true : false;

formFieldsNSm10['voletSocialPacsRSAOUI_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.voletSocialPacsRSAOUINON ? true : false;
formFieldsNSm10['voletSocialPacsRSANON_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.voletSocialPacsRSAOUINON ? false : true;
formFieldsNSm10['voletSocialStatutConjointCollaborateur_NSMA'] = Value('id').of(dirigeant10.cadreDirigeantNonSalarie10.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointCollaborateurNSMA') ? true : false;
formFieldsNSm10['voletSocialStatutConjointSalarie_NSMA'] = Value('id').of(dirigeant10.cadreDirigeantNonSalarie10.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointSalarieNSMA') ? true : false;
formFieldsNSm10['voletSocialStatutConjointAssocie_NSMA'] = Value('id').of(dirigeant10.cadreDirigeantNonSalarie10.personneLieeConjointStatutCollaborateurNSMA).eq('voletSocialStatutConjointAssocieNSMA') ? true : false;

formFieldsNSm10['voletSocialConjointRegimeAssuranceOUI_NSMA'] = Value('id').of(dirigeant10.cadreDirigeantNonSalarie10.voletSocialConjointRegimeAssuranceOUINONNSMA).eq('voletSocialConjointRegimeAssuranceOUINSMA') ? true : false;
formFieldsNSm10['voletSocialConjointRegimeAssuranceNON_NSMA'] = Value('id').of(dirigeant10.cadreDirigeantNonSalarie10.voletSocialConjointRegimeAssuranceOUINONNSMA).eq('voletSocialConjointRegimeAssuranceNONNSMA') ? true : false;

var nirConjointNSMA = dirigeant10.cadreDirigeantNonSalarie10.voletSocialConjointNumeroSecuriteSocialNSMA;
if (nirConjointNSMA != null) {
    nirConjointNSMA = nirConjointNSMA.replace(/ /g, "");
    formFieldsNSm10['voletSocialConjointNumeroSecuriteSocial_NSMA'] = nirConjointNSMA.substring(0, 13);
    formFieldsNSm10['voletSocialConjointNumeroSecuriteSocialCle_NSMA'] = nirConjointNSMA.substring(13, 15);
} else {
    formFieldsNSm10['voletSocialConjointNumeroSecuriteSocial_NSMA'] = '';
    formFieldsNSm10['voletSocialConjointNumeroSecuriteSocialCle_NSMA'] = '';
}

//-----------------------AYANT-DROIT

//ayant droit numero 1
formFieldsNSm10['ayantDroitIdentiteNomNaissancePrenom1_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration1 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits101.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits101.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm10['ayantDroitNumeroSecuriteSociale1_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration1 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits101.presenceNumSSAM ? dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits101.ayantDroitNumeroSecuriteSociale1NSMA : '') : '';
formFieldsNSm10['ayantDroitDateNaissance1_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration1 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits101.presenceNumSSAM ? '' : dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits101.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm10['ayantDroitLieuNaissanceCommune1_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration1 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits101.presenceNumSSAM ? '' : (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits101.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits101.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits101.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm10['ayantDroitLienParente1_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration1 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits101.ayantDroitLienParenteNSMA) : '';
formFieldsNSm10['ayantDroitEnfantScolarise1OUI_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration1 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits101.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm10['ayantDroitEnfantScolarise1NON_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration1 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits101.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm10['ayantDroitNationalite1_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration1 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits101.ayantDroitNationaliteNSMA) : '';

// numero 2
formFieldsNSm10['ayantDroitIdentiteNomNaissancePrenom2_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration2 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits102.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits102.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm10['ayantDroitNumeroSecuriteSociale2_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration2 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits102.presenceNumSSAM ? dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits102.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm10['ayantDroitDateNaissance2_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration2 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits102.presenceNumSSAM ? '' : dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits102.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm10['ayantDroitLieuNaissanceCommune2_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration2 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits102.presenceNumSSAM ? '' : (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits102.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits102.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits102.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm10['ayantDroitLienParente2_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration2 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits102.ayantDroitLienParenteNSMA) : '';
formFieldsNSm10['ayantDroitEnfantScolarise2OUI_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration2 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits102.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm10['ayantDroitEnfantScolarise2NON_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration2 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits102.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm10['ayantDroitNationalite2_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration2 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits102.ayantDroitNationaliteNSMA) : '';

//numero 3
formFieldsNSm10['ayantDroitIdentiteNomNaissancePrenom3_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration3 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits103.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits103.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm10['ayantDroitNumeroSecuriteSociale3_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration3 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits103.presenceNumSSAM ? dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits103.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm10['ayantDroitDateNaissance3_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration3 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits103.presenceNumSSAM ? '' : dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits103.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm10['ayantDroitLieuNaissanceCommune3_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration3 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits103.presenceNumSSAM ? '' : (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits103.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits103.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits103.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm10['ayantDroitLienParente3_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration3 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits103.ayantDroitLienParenteNSMA) : '';
formFieldsNSm10['ayantDroitEnfantScolarise3OUI_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration3 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits103.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm10['ayantDroitEnfantScolarise3NON_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration3 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits103.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm10['ayantDroitNationalite3_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration3 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits103.ayantDroitNationaliteNSMA) : '';

//numero 4
formFieldsNSm10['ayantDroitIdentiteNomNaissancePrenom4_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration4 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits104.ayantDroitIdentiteNomNaissanceNSMA + ' ' + dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits104.ayantDroitIdentitePrenomNSMA) : '';
formFieldsNSm10['ayantDroitNumeroSecuriteSociale4_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration4 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits104.presenceNumSSAM ? dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits104.ayantDroitNumeroSecuriteSocialeNSMA : '') : '';
formFieldsNSm10['ayantDroitDateNaissance4_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration4 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits104.presenceNumSSAM ? '' : dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits104.ayantDroitDateNaissanceNSMA) : '';
formFieldsNSm10['ayantDroitLieuNaissanceCommune4_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration4 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits104.presenceNumSSAM ? '' : (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits104.ayantDroitLieuNaissanceCommuneNSMA + ', ' + dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits104.ayantDroitLieuNaissancePaysNSMA + ' / ' + (Value('id').of(dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits104.ayantDroitComplementCiviliteNSMA).eq('feminin') ? 'F' : 'M'))) : '';
formFieldsNSm10['ayantDroitLienParente4_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration4 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits104.ayantDroitLienParenteNSMA) : '';
formFieldsNSm10['ayantDroitEnfantScolarise4OUI_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration4 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits104.ayantDroitEnfantScolariseOUINON ? true : false) : '';
formFieldsNSm10['ayantDroitEnfantScolarise4NON_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration4 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits104.ayantDroitEnfantScolariseOUINON ? false : true) : '';
formFieldsNSm10['ayantDroitNationalite4_NSMA'] = dirigeant10.cadreDirigeantNonSalarie10.ayantDroitsDeclaration4 ? (dirigeant10.cadreDirigeantNonSalarie10.ayantsDroits104.ayantDroitNationaliteNSMA) : '';

//-----------------------
//Cadre 4B
formFieldsNSm10['voletSocialMembreGAECOUI_NSMA'] = Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant10.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECOUINSMA') ? true : false;
formFieldsNSm10['voletSocialMembreGAECNON_NSMA'] = Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant10.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECNONNSMA') ? true : false;

//Cadre 5
formFieldsNSm10['renseignementsComplementairesObservations_NSMA'] = $m0Agricole.cadreDirigeantGroup.cadreDirigeant10.cadreObservationsNSM10.renseignementsComplementairesObservationsNSMA;

//Cadre 6
formFieldsNSm10['renseignementsComplementairesLeDeclarant_NSMA'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal') != false ? true : false;
formFieldsNSm10['renseignementsComplementairesLeMandataire_NSMA'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire') != false ? true : false;
formFieldsNSm10['formaliteSignataire_NSMA'] = (tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire :'')
+ ' ' + (tag.adresseMandataire.numeroVoieMandataire != null ? tag.adresseMandataire.numeroVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.indiceVoieMandataire != null ? tag.adresseMandataire.indiceVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.typeVoieMandataire != null ? tag.adresseMandataire.typeVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.nomVoieMandataire != null ? tag.adresseMandataire.nomVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.complementVoieMandataire != null ? tag.adresseMandataire.complementVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
+ ' ' + (tag.adresseMandataire.codePostalMandataire != null ? tag.adresseMandataire.codePostalMandataire : '')
+ ' ' + (tag.adresseMandataire.villeAdresseMandataire != null ? tag.adresseMandataire.villeAdresseMandataire : ''); 
formFieldsNSm10['formaliteSignatureLieu_NSMA'] = tag.formaliteSignatureLieu;
if (tag.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSm10['formaliteSignatureDate_NSMA'] = tag.formaliteSignatureDate;
}
formFieldsNSm10['signature_NSMA'] = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";

// A remplir avec le formulaire principal
formFieldsNSm10['formaliteSignatureNBIntercalaire_NSMA'] = '';

//NSp Agricole

// NSP Dirigeant 1
//Cadre 1
formFieldsNSp1['ayantDroit_lienParente_membreExploitation_NSP'] = false;
formFieldsNSp1['ayantDroit_lienParente_aideFamilial_NSP'] = true;
formFieldsNSp1['ayantDroit_lienParente_associeExploitation_NSP'] = false;
formFieldsNSp1['ayantDroit_affiliationMSA_non_NSP'] = false;
formFieldsNSp1['ayantDroit_affiliationMSA_oui_NSP'] = false;
formFieldsNSp1['formulaire_dependance_P0Agricole_NSP'] = false;
formFieldsNSp1['formulaire_dependance_FAgricole_NSP'] = false;
formFieldsNSp1['formulaire_dependance_M0Agricole_NSP'] = true;
formFieldsNSp1['formulaire_dependance_M2Agricole_NSP'] = false;
formFieldsNSp1['formulaire_dependance_M3_NSP'] = false;

//Cadre 2B

formFieldsNSp1['personneLiee_PP_nomNaissance_NSP'] = dirigeant1.civilitePersonnePhysique1.personneLieePPNomNaissance + ' ' + dirigeant1.civilitePersonnePhysique1.personneLieePPPrenom;

//cadre 3

var nirDeclarant1 = nsp1.aideFamilialNumeroSecuriteSocialeNsp;
if (nirDeclarant1 != null) {
    nirDeclarant1 = nirDeclarant1.replace(/ /g, "");
    formFieldsNSp1['ayantDroit_numeroSecuriteSociale_NSP'] = nirDeclarant1.substring(0, 13);
    formFieldsNSp1['ayantDroit_numeroSecuriteSocialeCle_NSP'] = nirDeclarant1.substring(13, 15);
}
formFieldsNSp1['ayantDroit_identite_nomNaissance_NSP'] = nsp1.aideFamilialNomNaissanceNsp;
formFieldsNSp1['ayantDroit_identite_nomUsage_NSP'] = nsp1.aideFamilialNomUsageNsp
    var prenoms = [];
for (i = 0; i < nsp1.aideFamilialPrenomNsp.size(); i++) {
    prenoms.push(nsp1.aideFamilialPrenomNsp[i]);
}
formFieldsNSp1['ayantDroit_identite_prenom_NSP'] = prenoms.toString();
formFieldsNSp1['ayantDroit_connuMSA_oui_NSP'] = nsp1.aideFamilialConnuMSAOuiNsp ? true : false;
formFieldsNSp1['ayantDroit_connuMSA_non_NSP'] = nsp1.aideFamilialConnuMSAOuiNsp ? false : true;
formFieldsNSp1['ayantDroit_regimeAssuranceMaladie_regimeGeneral_NSP'] = Value('id').of(nsp1.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieRegimeGeneralNSP') ? true : false;
formFieldsNSp1['ayantDroit_regimeAssuranceMaladie_agricole_NSP'] = Value('id').of(nsp1.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieAgricoleNSP') ? true : false;
formFieldsNSp1['ayantDroit_regimeAssuranceMaladie_nonSalarieNonAgricole_NSP'] = Value('id').of(nsp1.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieNonSalarieNonAgricoleNSP') ? true : false;
formFieldsNSp1['ayantDroit_regimeAssuranceMaladie_autre_NSP'] = Value('id').of(nsp1.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieAutreNSP') ? true : false;
formFieldsNSp1['ayantDroit_regimeAssuranceMaladieAutre_NSP'] = nsp1.aideFamilialRegimeAssuranceMaladieAutreTexteNsp;
formFieldsNSp1['ayantDroit_activiteAutreQueDeclareeStatut_oui'] = nsp1.aideFamilialActiviteAutreQueDeclareeStatutOui ? true : false;
formFieldsNSp1['ayantDroit_activiteAutreQueDeclareeStatut_non'] = nsp1.aideFamilialActiviteAutreQueDeclareeStatutOui ? false : true;
formFieldsNSp1['ayantDroit_regimeAssuranceMaladie_agricole_simultane_NSP'] = Value('id').of(nsp1.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieAgricoleSimultaneNSP') ? true : false;
formFieldsNSp1['ayantDroit_activiteAutreQueDeclareeStatut_salarie_NSP'] = Value('id').of(nsp1.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutSalarieNSP') ? true : false;
formFieldsNSp1['ayantDroit_regimeAssuranceMaladie_nonSalarieNonAgricole_simultane_NSP'] = Value('id').of(nsp1.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieNonSalarieNonAgricoleSimultaneNSP') ? true : false;
formFieldsNSp1['ayantDroit_activiteAutreQueDeclareeStatut_retraite_NSP'] = Value('id').of(nsp1.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutRetraiteNSP') ? true : false;
formFieldsNSp1['ayantDroit_activiteAutreQueDeclareeStatut_pensionneInvalidite_NSP'] = Value('id').of(nsp1.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutPensionneInvaliditeNSP') ? true : false;
formFieldsNSp1['ayantDroit_regimeAssuranceMaladie_autre_simultane_NSP'] = Value('id').of(nsp1.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieAutreSimultaneNSP') ? true : false;
formFieldsNSp1['ayantDroit_regimeAssuranceMaladieAutre_simultane_NSP'] = nsp1.aideFamilialRegimeAssuranceMaladieAutreSimultaneTexteNsp;
formFieldsNSp1['ayantDroit_organismeServantPension_NSP'] = nsp1.aideFamilialOrganismeServantPensionNsp;
formFieldsNSp1['ayantDroit_conjointCouvertAssuranceMaladie_oui_NSP'] = Value('id').of(nsp1.aideFamilialConjointCouvertAssuranceMaladieOuiNsp).eq('oui') ? true : false;
formFieldsNSp1['ayantDroit_conjointCouvertAssuranceMaladie_non_NSP'] = Value('id').of(nsp1.aideFamilialConjointCouvertAssuranceMaladieOuiNsp).eq('non') ? true : false;

var nirDeclarant = nsp1.aideFamilialCouvertAssuranceMaladieNumeroSSNsp;
if (nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsNSp1['ayantDroit_conjointCouvertAssuranceMaladie_numeroSS_NSP'] = nirDeclarant.substring(0, 13);
    formFieldsNSp1['ayantDroit_conjointCouvertAssuranceMaladie_numeroSSCle_NSP'] = nirDeclarant.substring(13, 15);
}
//cadre 3B

formFieldsNSp1['ayantDroit_adresse_nomVoie_NSP'] = (nsp1.aideFamilialAdresse.aideFamilialAdresseNumeroVoie != null ? nsp1.aideFamilialAdresse.aideFamilialAdresseNumeroVoie : '')
 + ' ' + (nsp1.aideFamilialAdresse.aideFamilialAdresseIndiceVoie != null ? nsp1.aideFamilialAdresse.aideFamilialAdresseIndiceVoie : '')
 + ' ' + (nsp1.aideFamilialAdresse.aideFamilialAdresseTypeVoie != null ? nsp1.aideFamilialAdresse.aideFamilialAdresseTypeVoie : '')
 + ' ' + (nsp1.aideFamilialAdresse.aideFamilialAdresseNomVoie  != null ? nsp1.aideFamilialAdresse.aideFamilialAdresseNomVoie : '')
 + ' ' + (nsp1.aideFamilialAdresse.aideFamilialAdresseComplement != null ? nsp1.aideFamilialAdresse.aideFamilialAdresseComplement : '')
 + ' ' + (nsp1.aideFamilialAdresse.aideFamilialAdresseDistributionSpeciale != null ? nsp1.aideFamilialAdresse.aideFamilialAdresseDistributionSpeciale : '');
formFieldsNSp1['ayantDroit_adresse_codePostal_NSP'] = nsp1.aideFamilialAdresse.aideFamilialAdresseCodePostal;
formFieldsNSp1['ayantDroit_adresse_commune_NSP'] = nsp1.aideFamilialAdresse.aideFamilialAdresseCommune;

//-------------------------Premier ayant-droit de l'aide familial

formFieldsNSp1['ayantDroit_identite_nomNaissance1_NSP'] = nsp1.ayantsDroitsAideFamilialNsp11.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp1.ayantsDroitsAideFamilialNsp11.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp1.ayantsDroitsAideFamilialNsp11.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp1['ayantDroit_numeroSecuriteSociale1_NSP'] = nsp1.ayantsDroitsAideFamilialNsp11.ayantDroitNumeroSecuriteSociale != null ? nsp1.ayantsDroitsAideFamilialNsp11.ayantDroitNumeroSecuriteSociale : '';
if (nsp1.ayantsDroitsAideFamilialNsp11.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp1.ayantsDroitsAideFamilialNsp11.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSp1['ayantDroit_identiteComplement_dateNaissance1_NSP'] = date1;
}
formFieldsNSp1['ayantDroit_identiteComplement_lieuNaissanceCommune1_NSP'] = nsp1.ayantsDroitsAideFamilialNsp11.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp1.ayantsDroitsAideFamilialNsp11.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp1.ayantsDroitsAideFamilialNsp11.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp1['ayantDroit_lienParente1_NSP'] = nsp1.ayantsDroitsAideFamilialNsp11.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp1['ayantDroit_enfantScolarise_oui1_NSP'] = nsp1.ayantsDroitsAideFamilialNsp11.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp1.ayantsDroitsAideFamilialNsp11.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp1['ayantDroit_enfantScolarise_non1_NSP'] = nsp1.ayantsDroitsAideFamilialNsp11.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp1.ayantsDroitsAideFamilialNsp11.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp1['ayantDroit_nationalite1_NSP'] = nsp1.ayantsDroitsAideFamilialNsp11.ayantDroitNationaliteAideFamilialNsp;

//---------------------Deuxième ayant-droit de l'aide familial

formFieldsNSp1['ayantDroit_identite_nomNaissance2_NSP'] = nsp1.ayantsDroitsAideFamilialNsp12.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp1.ayantsDroitsAideFamilialNsp12.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp1.ayantsDroitsAideFamilialNsp12.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp1['ayantDroit_numeroSecuriteSociale2_NSP'] = nsp1.ayantsDroitsAideFamilialNsp12.ayantDroitNumeroSecuriteSociale != null ? nsp1.ayantsDroitsAideFamilialNsp12.ayantDroitNumeroSecuriteSociale : '';
if (nsp1.ayantsDroitsAideFamilialNsp12.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp1.ayantsDroitsAideFamilialNsp12.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
    date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsNSp1['ayantDroit_identiteComplement_dateNaissance2_NSP'] = date2;
}
formFieldsNSp1['ayantDroit_identiteComplement_lieuNaissanceCommune2_NSP'] = nsp1.ayantsDroitsAideFamilialNsp12.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp1.ayantsDroitsAideFamilialNsp12.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp1.ayantsDroitsAideFamilialNsp12.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp1['ayantDroit_lienParente2_NSP'] = nsp1.ayantsDroitsAideFamilialNsp12.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp1['ayantDroit_enfantScolarise_oui2_NSP'] = nsp1.ayantsDroitsAideFamilialNsp12.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp1.ayantsDroitsAideFamilialNsp12.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp1['ayantDroit_enfantScolarise_non2_NSP'] = nsp1.ayantsDroitsAideFamilialNsp12.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp1.ayantsDroitsAideFamilialNsp12.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp1['ayantDroit_nationalite2_NSP'] = nsp1.ayantsDroitsAideFamilialNsp12.ayantDroitNationaliteAideFamilialNsp;

//---------------------Troisième ayant-droit de l'aide familial
formFieldsNSp1['ayantDroit_identite_nomNaissance3_NSP'] = nsp1.ayantsDroitsAideFamilialNsp13.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp1.ayantsDroitsAideFamilialNsp13.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp1.ayantsDroitsAideFamilialNsp13.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp1['ayantDroit_numeroSecuriteSociale3_NSP'] = nsp1.ayantsDroitsAideFamilialNsp13.ayantDroitNumeroSecuriteSociale != null ? nsp1.ayantsDroitsAideFamilialNsp13.ayantDroitNumeroSecuriteSociale : '';
if (nsp1.ayantsDroitsAideFamilialNsp13.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp1.ayantsDroitsAideFamilialNsp13.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date3 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date3 = date3.concat(pad(month.toString()));
    date3 = date3.concat(dateTmp.getFullYear().toString());
    formFieldsNSp1['ayantDroit_identiteComplement_dateNaissance3_NSP'] = date3;
}
formFieldsNSp1['ayantDroit_identiteComplement_lieuNaissanceCommune3_NSP'] = nsp1.ayantsDroitsAideFamilialNsp13.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp1.ayantsDroitsAideFamilialNsp13.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp1.ayantsDroitsAideFamilialNsp13.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp1['ayantDroit_lienParente3_NSP'] = nsp1.ayantsDroitsAideFamilialNsp13.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp1['ayantDroit_enfantScolarise_oui3_NSP'] = nsp1.ayantsDroitsAideFamilialNsp13.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp1.ayantsDroitsAideFamilialNsp13.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp1['ayantDroit_enfantScolarise_non3_NSP'] = nsp1.ayantsDroitsAideFamilialNsp13.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp1.ayantsDroitsAideFamilialNsp13.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp1['ayantDroit_nationalite3_NSP'] = nsp1.ayantsDroitsAideFamilialNsp13.ayantDroitNationaliteAideFamilialNsp;

// Signature

formFieldsNSp1['formalite_signataireQualite_declarantNSP'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal') != false ? true : false;
formFieldsNSp1['formalite_signataireQualite_mandataireNSP'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire') != false ? true : false;
formFieldsNSp1['formalite_signataireNom_NSP'] = tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire : '';
formFieldsNSp1['formalite_signataireAdresse_NSP'] = (tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire :'')
+ ' ' + (tag.adresseMandataire.numeroVoieMandataire != null ? tag.adresseMandataire.numeroVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.indiceVoieMandataire != null ? tag.adresseMandataire.indiceVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.typeVoieMandataire != null ? tag.adresseMandataire.typeVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.nomVoieMandataire != null ? tag.adresseMandataire.nomVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.complementVoieMandataire != null ? tag.adresseMandataire.complementVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
+ ' ' + (tag.adresseMandataire.codePostalMandataire != null ? tag.adresseMandataire.codePostalMandataire : '')
+ ' ' + (tag.adresseMandataire.villeAdresseMandataire != null ? tag.adresseMandataire.villeAdresseMandataire : ''); 

formFieldsNSp1['formalite_signatureLieu_NSP'] = tag.formaliteSignatureLieu;
if (tag.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSp1['formalite_signatureDate_NSP'] = tag.formaliteSignatureDate;
}

formFieldsNSp1['signature_NSP'] = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";

// A remplir avec le formulaire principal
formFieldsNSp1['nombreP0prime_NSP'] = '';

// NSP Dirigeant 2
//Cadre 1
formFieldsNSp2['ayantDroit_lienParente_membreExploitation_NSP'] = false;
formFieldsNSp2['ayantDroit_lienParente_aideFamilial_NSP'] = true;
formFieldsNSp2['ayantDroit_lienParente_associeExploitation_NSP'] = false;
formFieldsNSp2['ayantDroit_affiliationMSA_non_NSP'] = false;
formFieldsNSp2['ayantDroit_affiliationMSA_oui_NSP'] = false;
formFieldsNSp2['formulaire_dependance_P0Agricole_NSP'] = false;
formFieldsNSp2['formulaire_dependance_FAgricole_NSP'] = false;
formFieldsNSp2['formulaire_dependance_M0Agricole_NSP'] = true;
formFieldsNSp2['formulaire_dependance_M2Agricole_NSP'] = false;
formFieldsNSp2['formulaire_dependance_M3_NSP'] = false;

//Cadre 2B

formFieldsNSp2['personneLiee_PP_nomNaissance_NSP'] = dirigeant2.civilitePersonnePhysique2.personneLieePPNomNaissance + ' ' + dirigeant2.civilitePersonnePhysique2.personneLieePPPrenom;

//cadre 3

var nirDeclarant2 = nsp2.aideFamilialNumeroSecuriteSocialeNsp;
if (nirDeclarant2 != null) {
    nirDeclarant2 = nirDeclarant2.replace(/ /g, "");
    formFieldsNSp2['ayantDroit_numeroSecuriteSociale_NSP'] = nirDeclarant2.substring(0, 13);
    formFieldsNSp2['ayantDroit_numeroSecuriteSocialeCle_NSP'] = nirDeclarant2.substring(13, 15);
}
formFieldsNSp2['ayantDroit_identite_nomNaissance_NSP'] = nsp2.aideFamilialNomNaissanceNsp;
formFieldsNSp2['ayantDroit_identite_nomUsage_NSP'] = nsp2.aideFamilialNomUsageNsp
    var prenoms = [];
for (i = 0; i < nsp2.aideFamilialPrenomNsp.size(); i++) {
    prenoms.push(nsp2.aideFamilialPrenomNsp[i]);
}
formFieldsNSp2['ayantDroit_identite_prenom_NSP'] = prenoms.toString();
formFieldsNSp2['ayantDroit_connuMSA_oui_NSP'] = nsp2.aideFamilialConnuMSAOuiNsp ? true : false;
formFieldsNSp2['ayantDroit_connuMSA_non_NSP'] = nsp2.aideFamilialConnuMSAOuiNsp ? false : true;
formFieldsNSp2['ayantDroit_regimeAssuranceMaladie_regimeGeneral_NSP'] = Value('id').of(nsp2.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieRegimeGeneralNSP') ? true : false;
formFieldsNSp2['ayantDroit_regimeAssuranceMaladie_agricole_NSP'] = Value('id').of(nsp2.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieAgricoleNSP') ? true : false;
formFieldsNSp2['ayantDroit_regimeAssuranceMaladie_nonSalarieNonAgricole_NSP'] = Value('id').of(nsp2.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieNonSalarieNonAgricoleNSP') ? true : false;
formFieldsNSp2['ayantDroit_regimeAssuranceMaladie_autre_NSP'] = Value('id').of(nsp2.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieAutreNSP') ? true : false;
formFieldsNSp2['ayantDroit_regimeAssuranceMaladieAutre_NSP'] = nsp2.aideFamilialRegimeAssuranceMaladieAutreTexteNsp;
formFieldsNSp2['ayantDroit_activiteAutreQueDeclareeStatut_oui'] = nsp2.aideFamilialActiviteAutreQueDeclareeStatutOui ? true : false;
formFieldsNSp2['ayantDroit_activiteAutreQueDeclareeStatut_non'] = nsp2.aideFamilialActiviteAutreQueDeclareeStatutOui ? false : true;
formFieldsNSp2['ayantDroit_regimeAssuranceMaladie_agricole_simultane_NSP'] = Value('id').of(nsp2.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieAgricoleSimultaneNSP') ? true : false;
formFieldsNSp2['ayantDroit_activiteAutreQueDeclareeStatut_salarie_NSP'] = Value('id').of(nsp2.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutSalarieNSP') ? true : false;
formFieldsNSp2['ayantDroit_regimeAssuranceMaladie_nonSalarieNonAgricole_simultane_NSP'] = Value('id').of(nsp2.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieNonSalarieNonAgricoleSimultaneNSP') ? true : false;
formFieldsNSp2['ayantDroit_activiteAutreQueDeclareeStatut_retraite_NSP'] = Value('id').of(nsp2.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutRetraiteNSP') ? true : false;
formFieldsNSp2['ayantDroit_activiteAutreQueDeclareeStatut_pensionneInvalidite_NSP'] = Value('id').of(nsp2.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutPensionneInvaliditeNSP') ? true : false;
formFieldsNSp2['ayantDroit_regimeAssuranceMaladie_autre_simultane_NSP'] = Value('id').of(nsp2.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieAutreSimultaneNSP') ? true : false;
formFieldsNSp2['ayantDroit_regimeAssuranceMaladieAutre_simultane_NSP'] = nsp2.aideFamilialRegimeAssuranceMaladieAutreSimultaneTexteNsp;
formFieldsNSp2['ayantDroit_organismeServantPension_NSP'] = nsp2.aideFamilialOrganismeServantPensionNsp;
formFieldsNSp2['ayantDroit_conjointCouvertAssuranceMaladie_oui_NSP'] = Value('id').of(nsp2.aideFamilialConjointCouvertAssuranceMaladieOuiNsp).eq('oui') ? true : false;
formFieldsNSp2['ayantDroit_conjointCouvertAssuranceMaladie_non_NSP'] = Value('id').of(nsp2.aideFamilialConjointCouvertAssuranceMaladieOuiNsp).eq('non') ? true : false;

var nirDeclarant = nsp2.aideFamilialCouvertAssuranceMaladieNumeroSSNsp;
if (nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsNSp2['ayantDroit_conjointCouvertAssuranceMaladie_numeroSS_NSP'] = nirDeclarant.substring(0, 13);
    formFieldsNSp2['ayantDroit_conjointCouvertAssuranceMaladie_numeroSSCle_NSP'] = nirDeclarant.substring(13, 15);
}
//cadre 3B

formFieldsNSp2['ayantDroit_adresse_nomVoie_NSP'] = (nsp2.aideFamilialAdresse.aideFamilialAdresseNumeroVoie != null ? nsp2.aideFamilialAdresse.aideFamilialAdresseNumeroVoie : '')
 + ' ' + (nsp2.aideFamilialAdresse.aideFamilialAdresseIndiceVoie != null ? nsp2.aideFamilialAdresse.aideFamilialAdresseIndiceVoie : '')
 + ' ' + (nsp2.aideFamilialAdresse.aideFamilialAdresseTypeVoie != null ? nsp2.aideFamilialAdresse.aideFamilialAdresseTypeVoie : '')
 + ' ' + nsp2.aideFamilialAdresse.aideFamilialAdresseNomVoie
 + ' ' + (nsp2.aideFamilialAdresse.aideFamilialAdresseComplement != null ? nsp2.aideFamilialAdresse.aideFamilialAdresseComplement : '')
 + ' ' + (nsp2.aideFamilialAdresse.aideFamilialAdresseDistributionSpeciale != null ? nsp2.aideFamilialAdresse.aideFamilialAdresseDistributionSpeciale : '');
formFieldsNSp2['ayantDroit_adresse_codePostal_NSP'] = nsp2.aideFamilialAdresse.aideFamilialAdresseCodePostal;
formFieldsNSp2['ayantDroit_adresse_commune_NSP'] = nsp2.aideFamilialAdresse.aideFamilialAdresseCommune;

//-------------------------Premier ayant-droit de l'aide familial

formFieldsNSp2['ayantDroit_identite_nomNaissance1_NSP'] = nsp2.ayantsDroitsaideFamilialNsp21.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp2.ayantsDroitsaideFamilialNsp21.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp2.ayantsDroitsaideFamilialNsp21.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp2['ayantDroit_numeroSecuriteSociale1_NSP'] = nsp2.ayantsDroitsaideFamilialNsp21.ayantDroitNumeroSecuriteSociale != null ? nsp2.ayantsDroitsaideFamilialNsp21.ayantDroitNumeroSecuriteSociale : '';
if (nsp2.ayantsDroitsaideFamilialNsp21.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp2.ayantsDroitsaideFamilialNsp21.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSp2['ayantDroit_identiteComplement_dateNaissance1_NSP'] = date1;
}
formFieldsNSp2['ayantDroit_identiteComplement_lieuNaissanceCommune1_NSP'] = nsp2.ayantsDroitsaideFamilialNsp21.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp2.ayantsDroitsaideFamilialNsp21.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp2.ayantsDroitsaideFamilialNsp21.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp2['ayantDroit_lienParente1_NSP'] = nsp2.ayantsDroitsaideFamilialNsp21.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp2['ayantDroit_enfantScolarise_oui1_NSP'] = nsp2.ayantsDroitsaideFamilialNsp21.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp2.ayantsDroitsaideFamilialNsp21.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp2['ayantDroit_enfantScolarise_non1_NSP'] = nsp2.ayantsDroitsaideFamilialNsp21.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp2.ayantsDroitsaideFamilialNsp21.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp2['ayantDroit_nationalite1_NSP'] = nsp2.ayantsDroitsaideFamilialNsp21.ayantDroitNationaliteAideFamilialNsp;

//---------------------Deuxième ayant-droit de l'aide familial

formFieldsNSp2['ayantDroit_identite_nomNaissance2_NSP'] = nsp2.ayantsDroitsaideFamilialNsp22.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp2.ayantsDroitsaideFamilialNsp22.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp2.ayantsDroitsaideFamilialNsp22.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp2['ayantDroit_numeroSecuriteSociale2_NSP'] = nsp2.ayantsDroitsaideFamilialNsp22.ayantDroitNumeroSecuriteSociale != null ? nsp2.ayantsDroitsaideFamilialNsp22.ayantDroitNumeroSecuriteSociale : '';
if (nsp2.ayantsDroitsaideFamilialNsp22.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp2.ayantsDroitsaideFamilialNsp22.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
    date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsNSp2['ayantDroit_identiteComplement_dateNaissance2_NSP'] = date2;
}
formFieldsNSp2['ayantDroit_identiteComplement_lieuNaissanceCommune2_NSP'] = nsp2.ayantsDroitsaideFamilialNsp22.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp2.ayantsDroitsaideFamilialNsp22.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp2.ayantsDroitsaideFamilialNsp22.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp2['ayantDroit_lienParente2_NSP'] = nsp2.ayantsDroitsaideFamilialNsp22.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp2['ayantDroit_enfantScolarise_oui2_NSP'] = nsp2.ayantsDroitsaideFamilialNsp22.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp2.ayantsDroitsaideFamilialNsp22.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp2['ayantDroit_enfantScolarise_non2_NSP'] = nsp2.ayantsDroitsaideFamilialNsp22.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp2.ayantsDroitsaideFamilialNsp22.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp2['ayantDroit_nationalite2_NSP'] = nsp2.ayantsDroitsaideFamilialNsp22.ayantDroitNationaliteAideFamilialNsp;

//---------------------Troisième ayant-droit de l'aide familial
formFieldsNSp2['ayantDroit_identite_nomNaissance3_NSP'] = nsp2.ayantsDroitsaideFamilialNsp23.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp2.ayantsDroitsaideFamilialNsp23.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp2.ayantsDroitsaideFamilialNsp23.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp2['ayantDroit_numeroSecuriteSociale3_NSP'] = nsp2.ayantsDroitsaideFamilialNsp23.ayantDroitNumeroSecuriteSociale != null ? nsp2.ayantsDroitsaideFamilialNsp23.ayantDroitNumeroSecuriteSociale : '';
if (nsp2.ayantsDroitsaideFamilialNsp23.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp2.ayantsDroitsaideFamilialNsp23.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date3 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date3 = date3.concat(pad(month.toString()));
    date3 = date3.concat(dateTmp.getFullYear().toString());
    formFieldsNSp2['ayantDroit_identiteComplement_dateNaissance3_NSP'] = date3;
}
formFieldsNSp2['ayantDroit_identiteComplement_lieuNaissanceCommune3_NSP'] = nsp2.ayantsDroitsaideFamilialNsp23.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp2.ayantsDroitsaideFamilialNsp23.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp2.ayantsDroitsaideFamilialNsp23.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp2['ayantDroit_lienParente3_NSP'] = nsp2.ayantsDroitsaideFamilialNsp23.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp2['ayantDroit_enfantScolarise_oui3_NSP'] = nsp2.ayantsDroitsaideFamilialNsp23.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp2.ayantsDroitsaideFamilialNsp23.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp2['ayantDroit_enfantScolarise_non3_NSP'] = nsp2.ayantsDroitsaideFamilialNsp23.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp2.ayantsDroitsaideFamilialNsp23.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp2['ayantDroit_nationalite3_NSP'] = nsp2.ayantsDroitsaideFamilialNsp23.ayantDroitNationaliteAideFamilialNsp;

// Signature

formFieldsNSp2['formalite_signataireQualite_declarantNSP'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal') != false ? true : false;
formFieldsNSp2['formalite_signataireQualite_mandataireNSP'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire') != false ? true : false;
formFieldsNSp2['formalite_signataireNom_NSP'] = tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire : '';
formFieldsNSp2['formalite_signataireAdresse_NSP'] = (tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire :'')
+ ' ' + (tag.adresseMandataire.numeroVoieMandataire != null ? tag.adresseMandataire.numeroVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.indiceVoieMandataire != null ? tag.adresseMandataire.indiceVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.typeVoieMandataire != null ? tag.adresseMandataire.typeVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.nomVoieMandataire != null ? tag.adresseMandataire.nomVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.complementVoieMandataire != null ? tag.adresseMandataire.complementVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
+ ' ' + (tag.adresseMandataire.codePostalMandataire != null ? tag.adresseMandataire.codePostalMandataire : '')
+ ' ' + (tag.adresseMandataire.villeAdresseMandataire != null ? tag.adresseMandataire.villeAdresseMandataire : ''); 

formFieldsNSp2['formalite_signatureLieu_NSP'] = tag.formaliteSignatureLieu;
if (tag.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSp2['formalite_signatureDate_NSP'] = tag.formaliteSignatureDate;
}

formFieldsNSp2['signature_NSP'] = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";

// A remplir avec le formulaire principal
formFieldsNSp2['nombreP0prime_NSP'] = '';

// NSP Dirigeant 3

//Cadre 1
formFieldsNSp3['ayantDroit_lienParente_membreExploitation_NSP'] = false;
formFieldsNSp3['ayantDroit_lienParente_aideFamilial_NSP'] = true;
formFieldsNSp3['ayantDroit_lienParente_associeExploitation_NSP'] = false;
formFieldsNSp3['ayantDroit_affiliationMSA_non_NSP'] = false;
formFieldsNSp3['ayantDroit_affiliationMSA_oui_NSP'] = false;
formFieldsNSp3['formulaire_dependance_P0Agricole_NSP'] = false;
formFieldsNSp3['formulaire_dependance_FAgricole_NSP'] = false;
formFieldsNSp3['formulaire_dependance_M0Agricole_NSP'] = true;
formFieldsNSp3['formulaire_dependance_M2Agricole_NSP'] = false;
formFieldsNSp3['formulaire_dependance_M3_NSP'] = false;

//Cadre 2B

formFieldsNSp3['personneLiee_PP_nomNaissance_NSP'] = dirigeant3.civilitePersonnePhysique3.personneLieePPNomNaissance + ' ' + dirigeant3.civilitePersonnePhysique3.personneLieePPPrenom;

//cadre 3

var nirDeclarant3 = nsp3.aideFamilialNumeroSecuriteSocialeNsp;
if (nirDeclarant3 != null) {
    nirDeclarant3 = nirDeclarant3.replace(/ /g, "");
    formFieldsNSp3['ayantDroit_numeroSecuriteSociale_NSP'] = nirDeclarant3.substring(0, 13);
    formFieldsNSp3['ayantDroit_numeroSecuriteSocialeCle_NSP'] = nirDeclarant3.substring(13, 15);
}
formFieldsNSp3['ayantDroit_identite_nomNaissance_NSP'] = nsp3.aideFamilialNomNaissanceNsp;
formFieldsNSp3['ayantDroit_identite_nomUsage_NSP'] = nsp3.aideFamilialNomUsageNsp
    var prenoms = [];
for (i = 0; i < nsp3.aideFamilialPrenomNsp.size(); i++) {
    prenoms.push(nsp3.aideFamilialPrenomNsp[i]);
}
formFieldsNSp3['ayantDroit_identite_prenom_NSP'] = prenoms.toString();
formFieldsNSp3['ayantDroit_connuMSA_oui_NSP'] = nsp3.aideFamilialConnuMSAOuiNsp ? true : false;
formFieldsNSp3['ayantDroit_connuMSA_non_NSP'] = nsp3.aideFamilialConnuMSAOuiNsp ? false : true;
formFieldsNSp3['ayantDroit_regimeAssuranceMaladie_regimeGeneral_NSP'] = Value('id').of(nsp3.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieRegimeGeneralNSP') ? true : false;
formFieldsNSp3['ayantDroit_regimeAssuranceMaladie_agricole_NSP'] = Value('id').of(nsp3.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieAgricoleNSP') ? true : false;
formFieldsNSp3['ayantDroit_regimeAssuranceMaladie_nonSalarieNonAgricole_NSP'] = Value('id').of(nsp3.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieNonSalarieNonAgricoleNSP') ? true : false;
formFieldsNSp3['ayantDroit_regimeAssuranceMaladie_autre_NSP'] = Value('id').of(nsp3.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieAutreNSP') ? true : false;
formFieldsNSp3['ayantDroit_regimeAssuranceMaladieAutre_NSP'] = nsp3.aideFamilialRegimeAssuranceMaladieAutreTexteNsp;
formFieldsNSp3['ayantDroit_activiteAutreQueDeclareeStatut_oui'] = nsp3.aideFamilialActiviteAutreQueDeclareeStatutOui ? true : false;
formFieldsNSp3['ayantDroit_activiteAutreQueDeclareeStatut_non'] = nsp3.aideFamilialActiviteAutreQueDeclareeStatutOui ? false : true;
formFieldsNSp3['ayantDroit_regimeAssuranceMaladie_agricole_simultane_NSP'] = Value('id').of(nsp3.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieAgricoleSimultaneNSP') ? true : false;
formFieldsNSp3['ayantDroit_activiteAutreQueDeclareeStatut_salarie_NSP'] = Value('id').of(nsp3.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutSalarieNSP') ? true : false;
formFieldsNSp3['ayantDroit_regimeAssuranceMaladie_nonSalarieNonAgricole_simultane_NSP'] = Value('id').of(nsp3.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieNonSalarieNonAgricoleSimultaneNSP') ? true : false;
formFieldsNSp3['ayantDroit_activiteAutreQueDeclareeStatut_retraite_NSP'] = Value('id').of(nsp3.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutRetraiteNSP') ? true : false;
formFieldsNSp3['ayantDroit_activiteAutreQueDeclareeStatut_pensionneInvalidite_NSP'] = Value('id').of(nsp3.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutPensionneInvaliditeNSP') ? true : false;
formFieldsNSp3['ayantDroit_regimeAssuranceMaladie_autre_simultane_NSP'] = Value('id').of(nsp3.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieAutreSimultaneNSP') ? true : false;
formFieldsNSp3['ayantDroit_regimeAssuranceMaladieAutre_simultane_NSP'] = nsp3.aideFamilialRegimeAssuranceMaladieAutreSimultaneTexteNsp;
formFieldsNSp3['ayantDroit_organismeServantPension_NSP'] = nsp3.aideFamilialOrganismeServantPensionNsp;
formFieldsNSp3['ayantDroit_conjointCouvertAssuranceMaladie_oui_NSP'] = Value('id').of(nsp3.aideFamilialConjointCouvertAssuranceMaladieOuiNsp).eq('oui') ? true : false;
formFieldsNSp3['ayantDroit_conjointCouvertAssuranceMaladie_non_NSP'] = Value('id').of(nsp3.aideFamilialConjointCouvertAssuranceMaladieOuiNsp).eq('non') ? true : false;

var nirDeclarant = nsp3.aideFamilialCouvertAssuranceMaladieNumeroSSNsp;
if (nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsNSp3['ayantDroit_conjointCouvertAssuranceMaladie_numeroSS_NSP'] = nirDeclarant.substring(0, 13);
    formFieldsNSp3['ayantDroit_conjointCouvertAssuranceMaladie_numeroSSCle_NSP'] = nirDeclarant.substring(13, 15);
}
//cadre 3B

formFieldsNSp3['ayantDroit_adresse_nomVoie_NSP'] = (nsp3.aideFamilialAdresse.aideFamilialAdresseNumeroVoie != null ? nsp3.aideFamilialAdresse.aideFamilialAdresseNumeroVoie : '')
 + ' ' + (nsp3.aideFamilialAdresse.aideFamilialAdresseIndiceVoie != null ? nsp3.aideFamilialAdresse.aideFamilialAdresseIndiceVoie : '')
 + ' ' + (nsp3.aideFamilialAdresse.aideFamilialAdresseTypeVoie != null ? nsp3.aideFamilialAdresse.aideFamilialAdresseTypeVoie : '')
 + ' ' + nsp3.aideFamilialAdresse.aideFamilialAdresseNomVoie
 + ' ' + (nsp3.aideFamilialAdresse.aideFamilialAdresseComplement != null ? nsp3.aideFamilialAdresse.aideFamilialAdresseComplement : '')
 + ' ' + (nsp3.aideFamilialAdresse.aideFamilialAdresseDistributionSpeciale != null ? nsp3.aideFamilialAdresse.aideFamilialAdresseDistributionSpeciale : '');
formFieldsNSp3['ayantDroit_adresse_codePostal_NSP'] = nsp3.aideFamilialAdresse.aideFamilialAdresseCodePostal;
formFieldsNSp3['ayantDroit_adresse_commune_NSP'] = nsp3.aideFamilialAdresse.aideFamilialAdresseCommune;

//-------------------------Premier ayant-droit de l'aide familial

formFieldsNSp3['ayantDroit_identite_nomNaissance1_NSP'] = nsp3.ayantsDroitsaideFamilialNsp31.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp3.ayantsDroitsaideFamilialNsp31.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp3.ayantsDroitsaideFamilialNsp31.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp3['ayantDroit_numeroSecuriteSociale1_NSP'] = nsp3.ayantsDroitsaideFamilialNsp31.ayantDroitNumeroSecuriteSociale != null ? nsp3.ayantsDroitsaideFamilialNsp31.ayantDroitNumeroSecuriteSociale : '';
if (nsp3.ayantsDroitsaideFamilialNsp31.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp3.ayantsDroitsaideFamilialNsp31.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSp3['ayantDroit_identiteComplement_dateNaissance1_NSP'] = date1;
}
formFieldsNSp3['ayantDroit_identiteComplement_lieuNaissanceCommune1_NSP'] = nsp3.ayantsDroitsaideFamilialNsp31.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp3.ayantsDroitsaideFamilialNsp31.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp3.ayantsDroitsaideFamilialNsp31.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp3['ayantDroit_lienParente1_NSP'] = nsp3.ayantsDroitsaideFamilialNsp31.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp3['ayantDroit_enfantScolarise_oui1_NSP'] = nsp3.ayantsDroitsaideFamilialNsp31.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp3.ayantsDroitsaideFamilialNsp31.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp3['ayantDroit_enfantScolarise_non1_NSP'] = nsp3.ayantsDroitsaideFamilialNsp31.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp3.ayantsDroitsaideFamilialNsp31.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp3['ayantDroit_nationalite1_NSP'] = nsp3.ayantsDroitsaideFamilialNsp31.ayantDroitNationaliteAideFamilialNsp;

//---------------------Deuxième ayant-droit de l'aide familial

formFieldsNSp3['ayantDroit_identite_nomNaissance2_NSP'] = nsp3.ayantsDroitsaideFamilialNsp32.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp3.ayantsDroitsaideFamilialNsp32.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp3.ayantsDroitsaideFamilialNsp32.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp3['ayantDroit_numeroSecuriteSociale2_NSP'] = nsp3.ayantsDroitsaideFamilialNsp32.ayantDroitNumeroSecuriteSociale != null ? nsp3.ayantsDroitsaideFamilialNsp32.ayantDroitNumeroSecuriteSociale : '';
if (nsp3.ayantsDroitsaideFamilialNsp32.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp3.ayantsDroitsaideFamilialNsp32.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
    date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsNSp3['ayantDroit_identiteComplement_dateNaissance2_NSP'] = date2;
}
formFieldsNSp3['ayantDroit_identiteComplement_lieuNaissanceCommune2_NSP'] = nsp3.ayantsDroitsaideFamilialNsp32.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp3.ayantsDroitsaideFamilialNsp32.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp3.ayantsDroitsaideFamilialNsp32.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp3['ayantDroit_lienParente2_NSP'] = nsp3.ayantsDroitsaideFamilialNsp32.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp3['ayantDroit_enfantScolarise_oui2_NSP'] = nsp3.ayantsDroitsaideFamilialNsp32.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp3.ayantsDroitsaideFamilialNsp32.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp3['ayantDroit_enfantScolarise_non2_NSP'] = nsp3.ayantsDroitsaideFamilialNsp32.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp3.ayantsDroitsaideFamilialNsp32.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp3['ayantDroit_nationalite2_NSP'] = nsp3.ayantsDroitsaideFamilialNsp32.ayantDroitNationaliteAideFamilialNsp;

//---------------------Troisième ayant-droit de l'aide familial
formFieldsNSp3['ayantDroit_identite_nomNaissance3_NSP'] = nsp3.ayantsDroitsaideFamilialNsp33.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp3.ayantsDroitsaideFamilialNsp33.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp3.ayantsDroitsaideFamilialNsp33.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp3['ayantDroit_numeroSecuriteSociale3_NSP'] = nsp3.ayantsDroitsaideFamilialNsp33.ayantDroitNumeroSecuriteSociale != null ? nsp3.ayantsDroitsaideFamilialNsp33.ayantDroitNumeroSecuriteSociale : '';
if (nsp3.ayantsDroitsaideFamilialNsp33.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp3.ayantsDroitsaideFamilialNsp33.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date3 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date3 = date3.concat(pad(month.toString()));
    date3 = date3.concat(dateTmp.getFullYear().toString());
    formFieldsNSp3['ayantDroit_identiteComplement_dateNaissance3_NSP'] = date3;
}
formFieldsNSp3['ayantDroit_identiteComplement_lieuNaissanceCommune3_NSP'] = nsp3.ayantsDroitsaideFamilialNsp33.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp3.ayantsDroitsaideFamilialNsp33.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp3.ayantsDroitsaideFamilialNsp33.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp3['ayantDroit_lienParente3_NSP'] = nsp3.ayantsDroitsaideFamilialNsp33.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp3['ayantDroit_enfantScolarise_oui3_NSP'] = nsp3.ayantsDroitsaideFamilialNsp33.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp3.ayantsDroitsaideFamilialNsp33.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp3['ayantDroit_enfantScolarise_non3_NSP'] = nsp3.ayantsDroitsaideFamilialNsp33.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp3.ayantsDroitsaideFamilialNsp33.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp3['ayantDroit_nationalite3_NSP'] = nsp3.ayantsDroitsaideFamilialNsp33.ayantDroitNationaliteAideFamilialNsp;

// Signature

formFieldsNSp3['formalite_signataireQualite_declarantNSP'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal') != false ? true : false;
formFieldsNSp3['formalite_signataireQualite_mandataireNSP'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire') != false ? true : false;
formFieldsNSp3['formalite_signataireNom_NSP'] = tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire : '';
formFieldsNSp3['formalite_signataireAdresse_NSP'] = (tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire :'')
+ ' ' + (tag.adresseMandataire.numeroVoieMandataire != null ? tag.adresseMandataire.numeroVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.indiceVoieMandataire != null ? tag.adresseMandataire.indiceVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.typeVoieMandataire != null ? tag.adresseMandataire.typeVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.nomVoieMandataire != null ? tag.adresseMandataire.nomVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.complementVoieMandataire != null ? tag.adresseMandataire.complementVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
+ ' ' + (tag.adresseMandataire.codePostalMandataire != null ? tag.adresseMandataire.codePostalMandataire : '')
+ ' ' + (tag.adresseMandataire.villeAdresseMandataire != null ? tag.adresseMandataire.villeAdresseMandataire : ''); 

formFieldsNSp3['formalite_signatureLieu_NSP'] = tag.formaliteSignatureLieu;
if (tag.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSp3['formalite_signatureDate_NSP'] = tag.formaliteSignatureDate;
}

formFieldsNSp3['signature_NSP'] = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";

// A remplir avec le formulaire principal
formFieldsNSp3['nombreP0prime_NSP'] = '';

// NSP Dirigeant 4
// NSP Dirigeant 4
//Cadre 1
formFieldsNSp4['ayantDroit_lienParente_membreExploitation_NSP'] = false;
formFieldsNSp4['ayantDroit_lienParente_aideFamilial_NSP'] = true;
formFieldsNSp4['ayantDroit_lienParente_associeExploitation_NSP'] = false;
formFieldsNSp4['ayantDroit_affiliationMSA_non_NSP'] = false;
formFieldsNSp4['ayantDroit_affiliationMSA_oui_NSP'] = false;
formFieldsNSp4['formulaire_dependance_P0Agricole_NSP'] = false;
formFieldsNSp4['formulaire_dependance_FAgricole_NSP'] = false;
formFieldsNSp4['formulaire_dependance_M0Agricole_NSP'] = true;
formFieldsNSp4['formulaire_dependance_M2Agricole_NSP'] = false;
formFieldsNSp4['formulaire_dependance_M3_NSP'] = false;

//Cadre 2B

formFieldsNSp4['personneLiee_PP_nomNaissance_NSP'] = dirigeant4.civilitePersonnePhysique4.personneLieePPNomNaissance + ' ' + dirigeant4.civilitePersonnePhysique4.personneLieePPPrenom;

//cadre 3

var nirDeclarant4 = nsp4.aideFamilialNumeroSecuriteSocialeNsp;
if (nirDeclarant4 != null) {
    nirDeclarant4 = nirDeclarant4.replace(/ /g, "");
    formFieldsNSp4['ayantDroit_numeroSecuriteSociale_NSP'] = nirDeclarant4.substring(0, 13);
    formFieldsNSp4['ayantDroit_numeroSecuriteSocialeCle_NSP'] = nirDeclarant4.substring(13, 15);
}
formFieldsNSp4['ayantDroit_identite_nomNaissance_NSP'] = nsp4.aideFamilialNomNaissanceNsp;
formFieldsNSp4['ayantDroit_identite_nomUsage_NSP'] = nsp4.aideFamilialNomUsageNsp
    var prenoms = [];
for (i = 0; i < nsp4.aideFamilialPrenomNsp.size(); i++) {
    prenoms.push(nsp4.aideFamilialPrenomNsp[i]);
}
formFieldsNSp4['ayantDroit_identite_prenom_NSP'] = prenoms.toString();
formFieldsNSp4['ayantDroit_connuMSA_oui_NSP'] = nsp4.aideFamilialConnuMSAOuiNsp ? true : false;
formFieldsNSp4['ayantDroit_connuMSA_non_NSP'] = nsp4.aideFamilialConnuMSAOuiNsp ? false : true;
formFieldsNSp4['ayantDroit_regimeAssuranceMaladie_regimeGeneral_NSP'] = Value('id').of(nsp4.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieRegimeGeneralNSP') ? true : false;
formFieldsNSp4['ayantDroit_regimeAssuranceMaladie_agricole_NSP'] = Value('id').of(nsp4.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieAgricoleNSP') ? true : false;
formFieldsNSp4['ayantDroit_regimeAssuranceMaladie_nonSalarieNonAgricole_NSP'] = Value('id').of(nsp4.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieNonSalarieNonAgricoleNSP') ? true : false;
formFieldsNSp4['ayantDroit_regimeAssuranceMaladie_autre_NSP'] = Value('id').of(nsp4.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieAutreNSP') ? true : false;
formFieldsNSp4['ayantDroit_regimeAssuranceMaladieAutre_NSP'] = nsp4.aideFamilialRegimeAssuranceMaladieAutreTexteNsp;
formFieldsNSp4['ayantDroit_activiteAutreQueDeclareeStatut_oui'] = nsp4.aideFamilialActiviteAutreQueDeclareeStatutOui ? true : false;
formFieldsNSp4['ayantDroit_activiteAutreQueDeclareeStatut_non'] = nsp4.aideFamilialActiviteAutreQueDeclareeStatutOui ? false : true;
formFieldsNSp4['ayantDroit_regimeAssuranceMaladie_agricole_simultane_NSP'] = Value('id').of(nsp4.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieAgricoleSimultaneNSP') ? true : false;
formFieldsNSp4['ayantDroit_activiteAutreQueDeclareeStatut_salarie_NSP'] = Value('id').of(nsp4.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutSalarieNSP') ? true : false;
formFieldsNSp4['ayantDroit_regimeAssuranceMaladie_nonSalarieNonAgricole_simultane_NSP'] = Value('id').of(nsp4.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieNonSalarieNonAgricoleSimultaneNSP') ? true : false;
formFieldsNSp4['ayantDroit_activiteAutreQueDeclareeStatut_retraite_NSP'] = Value('id').of(nsp4.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutRetraiteNSP') ? true : false;
formFieldsNSp4['ayantDroit_activiteAutreQueDeclareeStatut_pensionneInvalidite_NSP'] = Value('id').of(nsp4.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutPensionneInvaliditeNSP') ? true : false;
formFieldsNSp4['ayantDroit_regimeAssuranceMaladie_autre_simultane_NSP'] = Value('id').of(nsp4.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieAutreSimultaneNSP') ? true : false;
formFieldsNSp4['ayantDroit_regimeAssuranceMaladieAutre_simultane_NSP'] = nsp4.aideFamilialRegimeAssuranceMaladieAutreSimultaneTexteNsp;
formFieldsNSp4['ayantDroit_organismeServantPension_NSP'] = nsp4.aideFamilialOrganismeServantPensionNsp;
formFieldsNSp4['ayantDroit_conjointCouvertAssuranceMaladie_oui_NSP'] = Value('id').of(nsp4.aideFamilialConjointCouvertAssuranceMaladieOuiNsp).eq('oui') ? true : false;
formFieldsNSp4['ayantDroit_conjointCouvertAssuranceMaladie_non_NSP'] = Value('id').of(nsp4.aideFamilialConjointCouvertAssuranceMaladieOuiNsp).eq('non') ? true : false;

var nirDeclarant = nsp4.aideFamilialCouvertAssuranceMaladieNumeroSSNsp;
if (nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsNSp4['ayantDroit_conjointCouvertAssuranceMaladie_numeroSS_NSP'] = nirDeclarant.substring(0, 13);
    formFieldsNSp4['ayantDroit_conjointCouvertAssuranceMaladie_numeroSSCle_NSP'] = nirDeclarant.substring(13, 15);
}
//cadre 3B

formFieldsNSp4['ayantDroit_adresse_nomVoie_NSP'] = (nsp4.aideFamilialAdresse.aideFamilialAdresseNumeroVoie != null ? nsp4.aideFamilialAdresse.aideFamilialAdresseNumeroVoie : '')
 + ' ' + (nsp4.aideFamilialAdresse.aideFamilialAdresseIndiceVoie != null ? nsp4.aideFamilialAdresse.aideFamilialAdresseIndiceVoie : '')
 + ' ' + (nsp4.aideFamilialAdresse.aideFamilialAdresseTypeVoie != null ? nsp4.aideFamilialAdresse.aideFamilialAdresseTypeVoie : '')
 + ' ' + nsp4.aideFamilialAdresse.aideFamilialAdresseNomVoie
 + ' ' + (nsp4.aideFamilialAdresse.aideFamilialAdresseComplement != null ? nsp4.aideFamilialAdresse.aideFamilialAdresseComplement : '')
 + ' ' + (nsp4.aideFamilialAdresse.aideFamilialAdresseDistributionSpeciale != null ? nsp4.aideFamilialAdresse.aideFamilialAdresseDistributionSpeciale : '');
formFieldsNSp4['ayantDroit_adresse_codePostal_NSP'] = nsp4.aideFamilialAdresse.aideFamilialAdresseCodePostal;
formFieldsNSp4['ayantDroit_adresse_commune_NSP'] = nsp4.aideFamilialAdresse.aideFamilialAdresseCommune;

//-------------------------Premier ayant-droit de l'aide familial

formFieldsNSp4['ayantDroit_identite_nomNaissance1_NSP'] = nsp4.ayantsDroitsaideFamilialNsp41.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp4.ayantsDroitsaideFamilialNsp41.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp4.ayantsDroitsaideFamilialNsp41.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp4['ayantDroit_numeroSecuriteSociale1_NSP'] = nsp4.ayantsDroitsaideFamilialNsp41.ayantDroitNumeroSecuriteSociale != null ? nsp4.ayantsDroitsaideFamilialNsp41.ayantDroitNumeroSecuriteSociale : '';
if (nsp4.ayantsDroitsaideFamilialNsp41.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp4.ayantsDroitsaideFamilialNsp41.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSp4['ayantDroit_identiteComplement_dateNaissance1_NSP'] = date1;
}
formFieldsNSp4['ayantDroit_identiteComplement_lieuNaissanceCommune1_NSP'] = nsp4.ayantsDroitsaideFamilialNsp41.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp4.ayantsDroitsaideFamilialNsp41.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp4.ayantsDroitsaideFamilialNsp41.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp4['ayantDroit_lienParente1_NSP'] = nsp4.ayantsDroitsaideFamilialNsp41.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp4['ayantDroit_enfantScolarise_oui1_NSP'] = nsp4.ayantsDroitsaideFamilialNsp41.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp4.ayantsDroitsaideFamilialNsp41.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp4['ayantDroit_enfantScolarise_non1_NSP'] = nsp4.ayantsDroitsaideFamilialNsp41.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp4.ayantsDroitsaideFamilialNsp41.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp4['ayantDroit_nationalite1_NSP'] = nsp4.ayantsDroitsaideFamilialNsp41.ayantDroitNationaliteAideFamilialNsp;

//---------------------Deuxième ayant-droit de l'aide familial

formFieldsNSp4['ayantDroit_identite_nomNaissance2_NSP'] = nsp4.ayantsDroitsaideFamilialNsp42.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp4.ayantsDroitsaideFamilialNsp42.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp4.ayantsDroitsaideFamilialNsp42.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp4['ayantDroit_numeroSecuriteSociale2_NSP'] = nsp4.ayantsDroitsaideFamilialNsp42.ayantDroitNumeroSecuriteSociale != null ? nsp4.ayantsDroitsaideFamilialNsp42.ayantDroitNumeroSecuriteSociale : '';
if (nsp4.ayantsDroitsaideFamilialNsp42.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp4.ayantsDroitsaideFamilialNsp42.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
    date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsNSp4['ayantDroit_identiteComplement_dateNaissance2_NSP'] = date2;
}
formFieldsNSp4['ayantDroit_identiteComplement_lieuNaissanceCommune2_NSP'] = nsp4.ayantsDroitsaideFamilialNsp42.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp4.ayantsDroitsaideFamilialNsp42.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp4.ayantsDroitsaideFamilialNsp42.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp4['ayantDroit_lienParente2_NSP'] = nsp4.ayantsDroitsaideFamilialNsp42.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp4['ayantDroit_enfantScolarise_oui2_NSP'] =  nsp4.ayantsDroitsaideFamilialNsp42.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp4.ayantsDroitsaideFamilialNsp42.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp4['ayantDroit_enfantScolarise_non2_NSP'] =  nsp4.ayantsDroitsaideFamilialNsp42.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp4.ayantsDroitsaideFamilialNsp42.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp4['ayantDroit_nationalite2_NSP'] = nsp4.ayantsDroitsaideFamilialNsp42.ayantDroitNationaliteAideFamilialNsp;

//---------------------Troisième ayant-droit de l'aide familial
formFieldsNSp4['ayantDroit_identite_nomNaissance3_NSP'] = nsp4.ayantsDroitsaideFamilialNsp43.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp4.ayantsDroitsaideFamilialNsp43.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp4.ayantsDroitsaideFamilialNsp43.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp4['ayantDroit_numeroSecuriteSociale3_NSP'] = nsp4.ayantsDroitsaideFamilialNsp43.ayantDroitNumeroSecuriteSociale != null ? nsp4.ayantsDroitsaideFamilialNsp43.ayantDroitNumeroSecuriteSociale : '';
if (nsp4.ayantsDroitsaideFamilialNsp43.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp4.ayantsDroitsaideFamilialNsp43.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date3 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date3 = date3.concat(pad(month.toString()));
    date3 = date3.concat(dateTmp.getFullYear().toString());
    formFieldsNSp4['ayantDroit_identiteComplement_dateNaissance3_NSP'] = date3;
}
formFieldsNSp4['ayantDroit_identiteComplement_lieuNaissanceCommune3_NSP'] = nsp4.ayantsDroitsaideFamilialNsp43.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp4.ayantsDroitsaideFamilialNsp43.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp4.ayantsDroitsaideFamilialNsp43.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp4['ayantDroit_lienParente3_NSP'] = nsp4.ayantsDroitsaideFamilialNsp43.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp4['ayantDroit_enfantScolarise_oui3_NSP'] = nsp4.ayantsDroitsaideFamilialNsp43.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp4.ayantsDroitsaideFamilialNsp43.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp4['ayantDroit_enfantScolarise_non3_NSP'] = nsp4.ayantsDroitsaideFamilialNsp43.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp4.ayantsDroitsaideFamilialNsp43.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp4['ayantDroit_nationalite3_NSP'] = nsp4.ayantsDroitsaideFamilialNsp43.ayantDroitNationaliteAideFamilialNsp;

// Signature

formFieldsNSp4['formalite_signataireQualite_declarantNSP'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal') != false ? true : false;
formFieldsNSp4['formalite_signataireQualite_mandataireNSP'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire') != false ? true : false;
formFieldsNSp4['formalite_signataireNom_NSP'] = tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire : '';
formFieldsNSp4['formalite_signataireAdresse_NSP'] = (tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire :'')
+ ' ' + (tag.adresseMandataire.numeroVoieMandataire != null ? tag.adresseMandataire.numeroVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.indiceVoieMandataire != null ? tag.adresseMandataire.indiceVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.typeVoieMandataire != null ? tag.adresseMandataire.typeVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.nomVoieMandataire != null ? tag.adresseMandataire.nomVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.complementVoieMandataire != null ? tag.adresseMandataire.complementVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
+ ' ' + (tag.adresseMandataire.codePostalMandataire != null ? tag.adresseMandataire.codePostalMandataire : '')
+ ' ' + (tag.adresseMandataire.villeAdresseMandataire != null ? tag.adresseMandataire.villeAdresseMandataire : ''); 

formFieldsNSp4['formalite_signatureLieu_NSP'] = tag.formaliteSignatureLieu;
if (tag.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSp4['formalite_signatureDate_NSP'] = tag.formaliteSignatureDate;
}

formFieldsNSp4['signature_NSP'] = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";

// A remplir avec le formulaire principal
formFieldsNSp4['nombreP0prime_NSP'] = '';

// NSP Dirigeant 5
//Cadre 1
formFieldsNSp5['ayantDroit_lienParente_membreExploitation_NSP'] = false;
formFieldsNSp5['ayantDroit_lienParente_aideFamilial_NSP'] = true;
formFieldsNSp5['ayantDroit_lienParente_associeExploitation_NSP'] = false;
formFieldsNSp5['ayantDroit_affiliationMSA_non_NSP'] = false;
formFieldsNSp5['ayantDroit_affiliationMSA_oui_NSP'] = false;
formFieldsNSp5['formulaire_dependance_P0Agricole_NSP'] = false;
formFieldsNSp5['formulaire_dependance_FAgricole_NSP'] = false;
formFieldsNSp5['formulaire_dependance_M0Agricole_NSP'] = true;
formFieldsNSp5['formulaire_dependance_M2Agricole_NSP'] = false;
formFieldsNSp5['formulaire_dependance_M3_NSP'] = false;

//Cadre 2B

formFieldsNSp5['personneLiee_PP_nomNaissance_NSP'] = dirigeant5.civilitePersonnePhysique5.personneLieePPNomNaissance + ' ' + dirigeant5.civilitePersonnePhysique5.personneLieePPPrenom;

//cadre 3

var nirDeclarant5 = nsp5.aideFamilialNumeroSecuriteSocialeNsp;
if (nirDeclarant5 != null) {
    nirDeclarant5 = nirDeclarant5.replace(/ /g, "");
    formFieldsNSp5['ayantDroit_numeroSecuriteSociale_NSP'] = nirDeclarant5.substring(0, 13);
    formFieldsNSp5['ayantDroit_numeroSecuriteSocialeCle_NSP'] = nirDeclarant5.substring(13, 15);
}
formFieldsNSp5['ayantDroit_identite_nomNaissance_NSP'] = nsp5.aideFamilialNomNaissanceNsp;
formFieldsNSp5['ayantDroit_identite_nomUsage_NSP'] = nsp5.aideFamilialNomUsageNsp
    var prenoms = [];
for (i = 0; i < nsp5.aideFamilialPrenomNsp.size(); i++) {
    prenoms.push(nsp5.aideFamilialPrenomNsp[i]);
}
formFieldsNSp5['ayantDroit_identite_prenom_NSP'] = prenoms.toString();
formFieldsNSp5['ayantDroit_connuMSA_oui_NSP'] = nsp5.aideFamilialConnuMSAOuiNsp ? true : false;
formFieldsNSp5['ayantDroit_connuMSA_non_NSP'] = nsp5.aideFamilialConnuMSAOuiNsp ? false : true;
formFieldsNSp5['ayantDroit_regimeAssuranceMaladie_regimeGeneral_NSP'] = Value('id').of(nsp5.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieRegimeGeneralNSP') ? true : false;
formFieldsNSp5['ayantDroit_regimeAssuranceMaladie_agricole_NSP'] = Value('id').of(nsp5.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieAgricoleNSP') ? true : false;
formFieldsNSp5['ayantDroit_regimeAssuranceMaladie_nonSalarieNonAgricole_NSP'] = Value('id').of(nsp5.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieNonSalarieNonAgricoleNSP') ? true : false;
formFieldsNSp5['ayantDroit_regimeAssuranceMaladie_autre_NSP'] = Value('id').of(nsp5.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieAutreNSP') ? true : false;
formFieldsNSp5['ayantDroit_regimeAssuranceMaladieAutre_NSP'] = nsp5.aideFamilialRegimeAssuranceMaladieAutreTexteNsp;
formFieldsNSp5['ayantDroit_activiteAutreQueDeclareeStatut_oui'] = nsp5.aideFamilialActiviteAutreQueDeclareeStatutOui ? true : false;
formFieldsNSp5['ayantDroit_activiteAutreQueDeclareeStatut_non'] = nsp5.aideFamilialActiviteAutreQueDeclareeStatutOui ? false : true;
formFieldsNSp5['ayantDroit_regimeAssuranceMaladie_agricole_simultane_NSP'] = Value('id').of(nsp5.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieAgricoleSimultaneNSP') ? true : false;
formFieldsNSp5['ayantDroit_activiteAutreQueDeclareeStatut_salarie_NSP'] = Value('id').of(nsp5.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutSalarieNSP') ? true : false;
formFieldsNSp5['ayantDroit_regimeAssuranceMaladie_nonSalarieNonAgricole_simultane_NSP'] = Value('id').of(nsp5.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieNonSalarieNonAgricoleSimultaneNSP') ? true : false;
formFieldsNSp5['ayantDroit_activiteAutreQueDeclareeStatut_retraite_NSP'] = Value('id').of(nsp5.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutRetraiteNSP') ? true : false;
formFieldsNSp5['ayantDroit_activiteAutreQueDeclareeStatut_pensionneInvalidite_NSP'] = Value('id').of(nsp5.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutPensionneInvaliditeNSP') ? true : false;
formFieldsNSp5['ayantDroit_regimeAssuranceMaladie_autre_simultane_NSP'] = Value('id').of(nsp5.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieAutreSimultaneNSP') ? true : false;
formFieldsNSp5['ayantDroit_regimeAssuranceMaladieAutre_simultane_NSP'] = nsp5.aideFamilialRegimeAssuranceMaladieAutreSimultaneTexteNsp;
formFieldsNSp5['ayantDroit_organismeServantPension_NSP'] = nsp5.aideFamilialOrganismeServantPensionNsp;
formFieldsNSp5['ayantDroit_conjointCouvertAssuranceMaladie_oui_NSP'] = Value('id').of(nsp5.aideFamilialConjointCouvertAssuranceMaladieOuiNsp).eq('oui') ? true : false;
formFieldsNSp5['ayantDroit_conjointCouvertAssuranceMaladie_non_NSP'] = Value('id').of(nsp5.aideFamilialConjointCouvertAssuranceMaladieOuiNsp).eq('non') ? true : false;

var nirDeclarant = nsp5.aideFamilialCouvertAssuranceMaladieNumeroSSNsp;
if (nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsNSp5['ayantDroit_conjointCouvertAssuranceMaladie_numeroSS_NSP'] = nirDeclarant.substring(0, 13);
    formFieldsNSp5['ayantDroit_conjointCouvertAssuranceMaladie_numeroSSCle_NSP'] = nirDeclarant.substring(13, 15);
}
//cadre 3B

formFieldsNSp5['ayantDroit_adresse_nomVoie_NSP'] = (nsp5.aideFamilialAdresse.aideFamilialAdresseNumeroVoie != null ? nsp5.aideFamilialAdresse.aideFamilialAdresseNumeroVoie : '')
 + ' ' + (nsp5.aideFamilialAdresse.aideFamilialAdresseIndiceVoie != null ? nsp5.aideFamilialAdresse.aideFamilialAdresseIndiceVoie : '')
 + ' ' + (nsp5.aideFamilialAdresse.aideFamilialAdresseTypeVoie != null ? nsp5.aideFamilialAdresse.aideFamilialAdresseTypeVoie : '')
 + ' ' + nsp5.aideFamilialAdresse.aideFamilialAdresseNomVoie
 + ' ' + (nsp5.aideFamilialAdresse.aideFamilialAdresseComplement != null ? nsp5.aideFamilialAdresse.aideFamilialAdresseComplement : '')
 + ' ' + (nsp5.aideFamilialAdresse.aideFamilialAdresseDistributionSpeciale != null ? nsp5.aideFamilialAdresse.aideFamilialAdresseDistributionSpeciale : '');
formFieldsNSp5['ayantDroit_adresse_codePostal_NSP'] = nsp5.aideFamilialAdresse.aideFamilialAdresseCodePostal;
formFieldsNSp5['ayantDroit_adresse_commune_NSP'] = nsp5.aideFamilialAdresse.aideFamilialAdresseCommune;

//-------------------------Premier ayant-droit de l'aide familial

formFieldsNSp5['ayantDroit_identite_nomNaissance1_NSP'] = nsp5.ayantsDroitsaideFamilialNsp51.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp5.ayantsDroitsaideFamilialNsp51.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp5.ayantsDroitsaideFamilialNsp51.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp5['ayantDroit_numeroSecuriteSociale1_NSP'] = nsp5.ayantsDroitsaideFamilialNsp51.ayantDroitNumeroSecuriteSociale != null ? nsp5.ayantsDroitsaideFamilialNsp51.ayantDroitNumeroSecuriteSociale : '';
if (nsp5.ayantsDroitsaideFamilialNsp51.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp5.ayantsDroitsaideFamilialNsp51.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSp5['ayantDroit_identiteComplement_dateNaissance1_NSP'] = date1;
}
formFieldsNSp5['ayantDroit_identiteComplement_lieuNaissanceCommune1_NSP'] = nsp5.ayantsDroitsaideFamilialNsp51.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp5.ayantsDroitsaideFamilialNsp51.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp5.ayantsDroitsaideFamilialNsp51.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp5['ayantDroit_lienParente1_NSP'] = nsp5.ayantsDroitsaideFamilialNsp51.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp5['ayantDroit_enfantScolarise_oui1_NSP'] = nsp5.ayantsDroitsaideFamilialNsp51.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp5.ayantsDroitsaideFamilialNsp51.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp5['ayantDroit_enfantScolarise_non1_NSP'] = nsp5.ayantsDroitsaideFamilialNsp51.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp5.ayantsDroitsaideFamilialNsp51.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp5['ayantDroit_nationalite1_NSP'] = nsp5.ayantsDroitsaideFamilialNsp51.ayantDroitNationaliteAideFamilialNsp;

//---------------------Deuxième ayant-droit de l'aide familial

formFieldsNSp5['ayantDroit_identite_nomNaissance2_NSP'] = nsp5.ayantsDroitsaideFamilialNsp52.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp5.ayantsDroitsaideFamilialNsp52.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp5.ayantsDroitsaideFamilialNsp52.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp5['ayantDroit_numeroSecuriteSociale2_NSP'] = nsp5.ayantsDroitsaideFamilialNsp52.ayantDroitNumeroSecuriteSociale != null ? nsp5.ayantsDroitsaideFamilialNsp52.ayantDroitNumeroSecuriteSociale : '';
if (nsp5.ayantsDroitsaideFamilialNsp52.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp5.ayantsDroitsaideFamilialNsp52.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
    date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsNSp5['ayantDroit_identiteComplement_dateNaissance2_NSP'] = date2;
}
formFieldsNSp5['ayantDroit_identiteComplement_lieuNaissanceCommune2_NSP'] = nsp5.ayantsDroitsaideFamilialNsp52.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp5.ayantsDroitsaideFamilialNsp52.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp5.ayantsDroitsaideFamilialNsp52.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp5['ayantDroit_lienParente2_NSP'] = nsp5.ayantsDroitsaideFamilialNsp52.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp5['ayantDroit_enfantScolarise_oui2_NSP'] = nsp5.ayantsDroitsaideFamilialNsp52.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp5.ayantsDroitsaideFamilialNsp52.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp5['ayantDroit_enfantScolarise_non2_NSP'] = nsp5.ayantsDroitsaideFamilialNsp52.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp5.ayantsDroitsaideFamilialNsp52.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp5['ayantDroit_nationalite2_NSP'] = nsp5.ayantsDroitsaideFamilialNsp52.ayantDroitNationaliteAideFamilialNsp;

//---------------------Troisième ayant-droit de l'aide familial
formFieldsNSp5['ayantDroit_identite_nomNaissance3_NSP'] = nsp5.ayantsDroitsaideFamilialNsp53.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp5.ayantsDroitsaideFamilialNsp53.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp5.ayantsDroitsaideFamilialNsp53.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp5['ayantDroit_numeroSecuriteSociale3_NSP'] = nsp5.ayantsDroitsaideFamilialNsp53.ayantDroitNumeroSecuriteSociale != null ? nsp5.ayantsDroitsaideFamilialNsp53.ayantDroitNumeroSecuriteSociale : '';
if (nsp5.ayantsDroitsaideFamilialNsp53.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp5.ayantsDroitsaideFamilialNsp53.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date3 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date3 = date3.concat(pad(month.toString()));
    date3 = date3.concat(dateTmp.getFullYear().toString());
    formFieldsNSp5['ayantDroit_identiteComplement_dateNaissance3_NSP'] = date3;
}
formFieldsNSp5['ayantDroit_identiteComplement_lieuNaissanceCommune3_NSP'] = nsp5.ayantsDroitsaideFamilialNsp53.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp5.ayantsDroitsaideFamilialNsp53.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp5.ayantsDroitsaideFamilialNsp53.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp5['ayantDroit_lienParente3_NSP'] = nsp5.ayantsDroitsaideFamilialNsp53.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp5['ayantDroit_enfantScolarise_oui3_NSP'] = nsp5.ayantsDroitsaideFamilialNsp53.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp5.ayantsDroitsaideFamilialNsp53.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp5['ayantDroit_enfantScolarise_non3_NSP'] = nsp5.ayantsDroitsaideFamilialNsp53.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp5.ayantsDroitsaideFamilialNsp53.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp5['ayantDroit_nationalite3_NSP'] = nsp5.ayantsDroitsaideFamilialNsp53.ayantDroitNationaliteAideFamilialNsp;

// Signature

formFieldsNSp5['formalite_signataireQualite_declarantNSP'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal') != false ? true : false;
formFieldsNSp5['formalite_signataireQualite_mandataireNSP'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire') != false ? true : false;
formFieldsNSp5['formalite_signataireNom_NSP'] = tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire : '';
formFieldsNSp5['formalite_signataireAdresse_NSP'] = (tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire :'')
+ ' ' + (tag.adresseMandataire.numeroVoieMandataire != null ? tag.adresseMandataire.numeroVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.indiceVoieMandataire != null ? tag.adresseMandataire.indiceVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.typeVoieMandataire != null ? tag.adresseMandataire.typeVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.nomVoieMandataire != null ? tag.adresseMandataire.nomVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.complementVoieMandataire != null ? tag.adresseMandataire.complementVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
+ ' ' + (tag.adresseMandataire.codePostalMandataire != null ? tag.adresseMandataire.codePostalMandataire : '')
+ ' ' + (tag.adresseMandataire.villeAdresseMandataire != null ? tag.adresseMandataire.villeAdresseMandataire : ''); 

formFieldsNSp5['formalite_signatureLieu_NSP'] = tag.formaliteSignatureLieu;
if (tag.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSp5['formalite_signatureDate_NSP'] = tag.formaliteSignatureDate;
}
formFieldsNSp5['signature_NSP'] = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";

// A remplir avec le formulaire principal
formFieldsNSp5['nombreP0prime_NSP'] = '';

// NSP Dirigeant 6
//Cadre 1
formFieldsNSp6['ayantDroit_lienParente_membreExploitation_NSP'] = false;
formFieldsNSp6['ayantDroit_lienParente_aideFamilial_NSP'] = true;
formFieldsNSp6['ayantDroit_lienParente_associeExploitation_NSP'] = false;
formFieldsNSp6['ayantDroit_affiliationMSA_non_NSP'] = false;
formFieldsNSp6['ayantDroit_affiliationMSA_oui_NSP'] = false;
formFieldsNSp6['formulaire_dependance_P0Agricole_NSP'] = false;
formFieldsNSp6['formulaire_dependance_FAgricole_NSP'] = false;
formFieldsNSp6['formulaire_dependance_M0Agricole_NSP'] = true;
formFieldsNSp6['formulaire_dependance_M2Agricole_NSP'] = false;
formFieldsNSp6['formulaire_dependance_M3_NSP'] = false;

//Cadre 2B

formFieldsNSp6['personneLiee_PP_nomNaissance_NSP'] = dirigeant6.civilitePersonnePhysique6.personneLieePPNomNaissance + ' ' + dirigeant6.civilitePersonnePhysique6.personneLieePPPrenom;

//cadre 3

var nirDeclarant6 = nsp6.aideFamilialNumeroSecuriteSocialeNsp;
if (nirDeclarant6 != null) {
    nirDeclarant6 = nirDeclarant6.replace(/ /g, "");
    formFieldsNSp6['ayantDroit_numeroSecuriteSociale_NSP'] = nirDeclarant6.substring(0, 13);
    formFieldsNSp6['ayantDroit_numeroSecuriteSocialeCle_NSP'] = nirDeclarant6.substring(13, 15);
}
formFieldsNSp6['ayantDroit_identite_nomNaissance_NSP'] = nsp6.aideFamilialNomNaissanceNsp;
formFieldsNSp6['ayantDroit_identite_nomUsage_NSP'] = nsp6.aideFamilialNomUsageNsp
    var prenoms = [];
for (i = 0; i < nsp6.aideFamilialPrenomNsp.size(); i++) {
    prenoms.push(nsp6.aideFamilialPrenomNsp[i]);
}
formFieldsNSp6['ayantDroit_identite_prenom_NSP'] = prenoms.toString();
formFieldsNSp6['ayantDroit_connuMSA_oui_NSP'] = nsp6.aideFamilialConnuMSAOuiNsp ? true : false;
formFieldsNSp6['ayantDroit_connuMSA_non_NSP'] = nsp6.aideFamilialConnuMSAOuiNsp ? false : true;
formFieldsNSp6['ayantDroit_regimeAssuranceMaladie_regimeGeneral_NSP'] = Value('id').of(nsp6.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieRegimeGeneralNSP') ? true : false;
formFieldsNSp6['ayantDroit_regimeAssuranceMaladie_agricole_NSP'] = Value('id').of(nsp6.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieAgricoleNSP') ? true : false;
formFieldsNSp6['ayantDroit_regimeAssuranceMaladie_nonSalarieNonAgricole_NSP'] = Value('id').of(nsp6.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieNonSalarieNonAgricoleNSP') ? true : false;
formFieldsNSp6['ayantDroit_regimeAssuranceMaladie_autre_NSP'] = Value('id').of(nsp6.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieAutreNSP') ? true : false;
formFieldsNSp6['ayantDroit_regimeAssuranceMaladieAutre_NSP'] = nsp6.aideFamilialRegimeAssuranceMaladieAutreTexteNsp;
formFieldsNSp6['ayantDroit_activiteAutreQueDeclareeStatut_oui'] = nsp6.aideFamilialActiviteAutreQueDeclareeStatutOui ? true : false;
formFieldsNSp6['ayantDroit_activiteAutreQueDeclareeStatut_non'] = nsp6.aideFamilialActiviteAutreQueDeclareeStatutOui ? false : true;
formFieldsNSp6['ayantDroit_regimeAssuranceMaladie_agricole_simultane_NSP'] = Value('id').of(nsp6.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieAgricoleSimultaneNSP') ? true : false;
formFieldsNSp6['ayantDroit_activiteAutreQueDeclareeStatut_salarie_NSP'] = Value('id').of(nsp6.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutSalarieNSP') ? true : false;
formFieldsNSp6['ayantDroit_regimeAssuranceMaladie_nonSalarieNonAgricole_simultane_NSP'] = Value('id').of(nsp6.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieNonSalarieNonAgricoleSimultaneNSP') ? true : false;
formFieldsNSp6['ayantDroit_activiteAutreQueDeclareeStatut_retraite_NSP'] = Value('id').of(nsp6.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutRetraiteNSP') ? true : false;
formFieldsNSp6['ayantDroit_activiteAutreQueDeclareeStatut_pensionneInvalidite_NSP'] = Value('id').of(nsp6.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutPensionneInvaliditeNSP') ? true : false;
formFieldsNSp6['ayantDroit_regimeAssuranceMaladie_autre_simultane_NSP'] = Value('id').of(nsp6.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieAutreSimultaneNSP') ? true : false;
formFieldsNSp6['ayantDroit_regimeAssuranceMaladieAutre_simultane_NSP'] = nsp6.aideFamilialRegimeAssuranceMaladieAutreSimultaneTexteNsp;
formFieldsNSp6['ayantDroit_organismeServantPension_NSP'] = nsp6.aideFamilialOrganismeServantPensionNsp;
formFieldsNSp6['ayantDroit_conjointCouvertAssuranceMaladie_oui_NSP'] = Value('id').of(nsp6.aideFamilialConjointCouvertAssuranceMaladieOuiNsp).eq('oui') ? true : false;
formFieldsNSp6['ayantDroit_conjointCouvertAssuranceMaladie_non_NSP'] = Value('id').of(nsp6.aideFamilialConjointCouvertAssuranceMaladieOuiNsp).eq('non') ? true : false;

var nirDeclarant = nsp6.aideFamilialCouvertAssuranceMaladieNumeroSSNsp;
if (nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsNSp6['ayantDroit_conjointCouvertAssuranceMaladie_numeroSS_NSP'] = nirDeclarant.substring(0, 13);
    formFieldsNSp6['ayantDroit_conjointCouvertAssuranceMaladie_numeroSSCle_NSP'] = nirDeclarant.substring(13, 15);
}
//cadre 3B

formFieldsNSp6['ayantDroit_adresse_nomVoie_NSP'] = (nsp6.aideFamilialAdresse.aideFamilialAdresseNumeroVoie != null ? nsp6.aideFamilialAdresse.aideFamilialAdresseNumeroVoie : '')
 + ' ' + (nsp6.aideFamilialAdresse.aideFamilialAdresseIndiceVoie != null ? nsp6.aideFamilialAdresse.aideFamilialAdresseIndiceVoie : '')
 + ' ' + (nsp6.aideFamilialAdresse.aideFamilialAdresseTypeVoie != null ? nsp6.aideFamilialAdresse.aideFamilialAdresseTypeVoie : '')
 + ' ' + nsp6.aideFamilialAdresse.aideFamilialAdresseNomVoie
 + ' ' + (nsp6.aideFamilialAdresse.aideFamilialAdresseComplement != null ? nsp6.aideFamilialAdresse.aideFamilialAdresseComplement : '')
 + ' ' + (nsp6.aideFamilialAdresse.aideFamilialAdresseDistributionSpeciale != null ? nsp6.aideFamilialAdresse.aideFamilialAdresseDistributionSpeciale : '');
formFieldsNSp6['ayantDroit_adresse_codePostal_NSP'] = nsp6.aideFamilialAdresse.aideFamilialAdresseCodePostal;
formFieldsNSp6['ayantDroit_adresse_commune_NSP'] = nsp6.aideFamilialAdresse.aideFamilialAdresseCommune;

//-------------------------Premier ayant-droit de l'aide familial

formFieldsNSp6['ayantDroit_identite_nomNaissance1_NSP'] = nsp6.ayantsDroitsaideFamilialNsp61.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp6.ayantsDroitsaideFamilialNsp61.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp6.ayantsDroitsaideFamilialNsp61.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp6['ayantDroit_numeroSecuriteSociale1_NSP'] = nsp6.ayantsDroitsaideFamilialNsp61.ayantDroitNumeroSecuriteSociale != null ? nsp6.ayantsDroitsaideFamilialNsp61.ayantDroitNumeroSecuriteSociale : '';
if (nsp6.ayantsDroitsaideFamilialNsp61.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp6.ayantsDroitsaideFamilialNsp61.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSp6['ayantDroit_identiteComplement_dateNaissance1_NSP'] = date1;
}
formFieldsNSp6['ayantDroit_identiteComplement_lieuNaissanceCommune1_NSP'] = nsp6.ayantsDroitsaideFamilialNsp61.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp6.ayantsDroitsaideFamilialNsp61.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp6.ayantsDroitsaideFamilialNsp61.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp6['ayantDroit_lienParente1_NSP'] = nsp6.ayantsDroitsaideFamilialNsp61.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp6['ayantDroit_enfantScolarise_oui1_NSP'] = nsp6.ayantsDroitsaideFamilialNsp61.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp6.ayantsDroitsaideFamilialNsp61.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp6['ayantDroit_enfantScolarise_non1_NSP'] = nsp6.ayantsDroitsaideFamilialNsp61.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp6.ayantsDroitsaideFamilialNsp61.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp6['ayantDroit_nationalite1_NSP'] = nsp6.ayantsDroitsaideFamilialNsp61.ayantDroitNationaliteAideFamilialNsp;

//---------------------Deuxième ayant-droit de l'aide familial

formFieldsNSp6['ayantDroit_identite_nomNaissance2_NSP'] = nsp6.ayantsDroitsaideFamilialNsp62.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp6.ayantsDroitsaideFamilialNsp62.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp6.ayantsDroitsaideFamilialNsp62.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp6['ayantDroit_numeroSecuriteSociale2_NSP'] = nsp6.ayantsDroitsaideFamilialNsp62.ayantDroitNumeroSecuriteSociale != null ? nsp6.ayantsDroitsaideFamilialNsp62.ayantDroitNumeroSecuriteSociale : '';
if (nsp6.ayantsDroitsaideFamilialNsp62.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp6.ayantsDroitsaideFamilialNsp62.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
    date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsNSp6['ayantDroit_identiteComplement_dateNaissance2_NSP'] = date2;
}
formFieldsNSp6['ayantDroit_identiteComplement_lieuNaissanceCommune2_NSP'] = nsp6.ayantsDroitsaideFamilialNsp62.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp6.ayantsDroitsaideFamilialNsp62.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp6.ayantsDroitsaideFamilialNsp62.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp6['ayantDroit_lienParente2_NSP'] = nsp6.ayantsDroitsaideFamilialNsp62.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp6['ayantDroit_enfantScolarise_oui2_NSP'] = nsp6.ayantsDroitsaideFamilialNsp62.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp6.ayantsDroitsaideFamilialNsp62.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp6['ayantDroit_enfantScolarise_non2_NSP'] = nsp6.ayantsDroitsaideFamilialNsp62.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp6.ayantsDroitsaideFamilialNsp62.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp6['ayantDroit_nationalite2_NSP'] = nsp6.ayantsDroitsaideFamilialNsp62.ayantDroitNationaliteAideFamilialNsp;

//---------------------Troisième ayant-droit de l'aide familial
formFieldsNSp6['ayantDroit_identite_nomNaissance3_NSP'] = nsp6.ayantsDroitsaideFamilialNsp63.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp6.ayantsDroitsaideFamilialNsp63.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp6.ayantsDroitsaideFamilialNsp63.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp6['ayantDroit_numeroSecuriteSociale3_NSP'] = nsp6.ayantsDroitsaideFamilialNsp63.ayantDroitNumeroSecuriteSociale != null ? nsp6.ayantsDroitsaideFamilialNsp63.ayantDroitNumeroSecuriteSociale : '';
if (nsp6.ayantsDroitsaideFamilialNsp63.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp6.ayantsDroitsaideFamilialNsp63.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date3 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date3 = date3.concat(pad(month.toString()));
    date3 = date3.concat(dateTmp.getFullYear().toString());
    formFieldsNSp6['ayantDroit_identiteComplement_dateNaissance3_NSP'] = date3;
}
formFieldsNSp6['ayantDroit_identiteComplement_lieuNaissanceCommune3_NSP'] = nsp6.ayantsDroitsaideFamilialNsp63.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp6.ayantsDroitsaideFamilialNsp63.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp6.ayantsDroitsaideFamilialNsp63.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp6['ayantDroit_lienParente3_NSP'] = nsp6.ayantsDroitsaideFamilialNsp63.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp6['ayantDroit_enfantScolarise_oui3_NSP'] = nsp6.ayantsDroitsaideFamilialNsp63.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp6.ayantsDroitsaideFamilialNsp63.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp6['ayantDroit_enfantScolarise_non3_NSP'] = nsp6.ayantsDroitsaideFamilialNsp63.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp6.ayantsDroitsaideFamilialNsp63.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp6['ayantDroit_nationalite3_NSP'] = nsp6.ayantsDroitsaideFamilialNsp63.ayantDroitNationaliteAideFamilialNsp;

// Signature

formFieldsNSp6['formalite_signataireQualite_declarantNSP'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal') != false ? true : false;
formFieldsNSp6['formalite_signataireQualite_mandataireNSP'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire') != false ? true : false;
formFieldsNSp6['formalite_signataireNom_NSP'] = tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire : '';
formFieldsNSp6['formalite_signataireAdresse_NSP'] = (tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire :'')
+ ' ' + (tag.adresseMandataire.numeroVoieMandataire != null ? tag.adresseMandataire.numeroVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.indiceVoieMandataire != null ? tag.adresseMandataire.indiceVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.typeVoieMandataire != null ? tag.adresseMandataire.typeVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.nomVoieMandataire != null ? tag.adresseMandataire.nomVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.complementVoieMandataire != null ? tag.adresseMandataire.complementVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
+ ' ' + (tag.adresseMandataire.codePostalMandataire != null ? tag.adresseMandataire.codePostalMandataire : '')
+ ' ' + (tag.adresseMandataire.villeAdresseMandataire != null ? tag.adresseMandataire.villeAdresseMandataire : ''); 

formFieldsNSp6['formalite_signatureLieu_NSP'] = tag.formaliteSignatureLieu;
if (tag.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSp6['formalite_signatureDate_NSP'] = tag.formaliteSignatureDate;
}

formFieldsNSp6['signature_NSP'] = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";

// A remplir avec le formulaire principal
formFieldsNSp6['nombreP0prime_NSP'] = '';

// NSP Dirigeant 7
//Cadre 1
formFieldsNSp7['ayantDroit_lienParente_membreExploitation_NSP'] = false;
formFieldsNSp7['ayantDroit_lienParente_aideFamilial_NSP'] = true;
formFieldsNSp7['ayantDroit_lienParente_associeExploitation_NSP'] = false;
formFieldsNSp7['ayantDroit_affiliationMSA_non_NSP'] = false;
formFieldsNSp7['ayantDroit_affiliationMSA_oui_NSP'] = false;
formFieldsNSp7['formulaire_dependance_P0Agricole_NSP'] = false;
formFieldsNSp7['formulaire_dependance_FAgricole_NSP'] = false;
formFieldsNSp7['formulaire_dependance_M0Agricole_NSP'] = true;
formFieldsNSp7['formulaire_dependance_M2Agricole_NSP'] = false;
formFieldsNSp7['formulaire_dependance_M3_NSP'] = false;

//Cadre 2B

formFieldsNSp7['personneLiee_PP_nomNaissance_NSP'] = dirigeant7.civilitePersonnePhysique7.personneLieePPNomNaissance + ' ' + dirigeant7.civilitePersonnePhysique7.personneLieePPPrenom;

//cadre 3

var nirDeclarant7 = nsp7.aideFamilialNumeroSecuriteSocialeNsp;
if (nirDeclarant7 != null) {
    nirDeclarant7 = nirDeclarant7.replace(/ /g, "");
    formFieldsNSp7['ayantDroit_numeroSecuriteSociale_NSP'] = nirDeclarant7.substring(0, 13);
    formFieldsNSp7['ayantDroit_numeroSecuriteSocialeCle_NSP'] = nirDeclarant7.substring(13, 15);
}
formFieldsNSp7['ayantDroit_identite_nomNaissance_NSP'] = nsp7.aideFamilialNomNaissanceNsp;
formFieldsNSp7['ayantDroit_identite_nomUsage_NSP'] = nsp7.aideFamilialNomUsageNsp
    var prenoms = [];
for (i = 0; i < nsp7.aideFamilialPrenomNsp.size(); i++) {
    prenoms.push(nsp7.aideFamilialPrenomNsp[i]);
}
formFieldsNSp7['ayantDroit_identite_prenom_NSP'] = prenoms.toString();
formFieldsNSp7['ayantDroit_connuMSA_oui_NSP'] = nsp7.aideFamilialConnuMSAOuiNsp ? true : false;
formFieldsNSp7['ayantDroit_connuMSA_non_NSP'] = nsp7.aideFamilialConnuMSAOuiNsp ? false : true;
formFieldsNSp7['ayantDroit_regimeAssuranceMaladie_regimeGeneral_NSP'] = Value('id').of(nsp7.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieRegimeGeneralNSP') ? true : false;
formFieldsNSp7['ayantDroit_regimeAssuranceMaladie_agricole_NSP'] = Value('id').of(nsp7.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieAgricoleNSP') ? true : false;
formFieldsNSp7['ayantDroit_regimeAssuranceMaladie_nonSalarieNonAgricole_NSP'] = Value('id').of(nsp7.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieNonSalarieNonAgricoleNSP') ? true : false;
formFieldsNSp7['ayantDroit_regimeAssuranceMaladie_autre_NSP'] = Value('id').of(nsp7.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieAutreNSP') ? true : false;
formFieldsNSp7['ayantDroit_regimeAssuranceMaladieAutre_NSP'] = nsp7.aideFamilialRegimeAssuranceMaladieAutreTexteNsp;
formFieldsNSp7['ayantDroit_activiteAutreQueDeclareeStatut_oui'] = nsp7.aideFamilialActiviteAutreQueDeclareeStatutOui ? true : false;
formFieldsNSp7['ayantDroit_activiteAutreQueDeclareeStatut_non'] = nsp7.aideFamilialActiviteAutreQueDeclareeStatutOui ? false : true;
formFieldsNSp7['ayantDroit_regimeAssuranceMaladie_agricole_simultane_NSP'] = Value('id').of(nsp7.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieAgricoleSimultaneNSP') ? true : false;
formFieldsNSp7['ayantDroit_activiteAutreQueDeclareeStatut_salarie_NSP'] = Value('id').of(nsp7.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutSalarieNSP') ? true : false;
formFieldsNSp7['ayantDroit_regimeAssuranceMaladie_nonSalarieNonAgricole_simultane_NSP'] = Value('id').of(nsp7.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieNonSalarieNonAgricoleSimultaneNSP') ? true : false;
formFieldsNSp7['ayantDroit_activiteAutreQueDeclareeStatut_retraite_NSP'] = Value('id').of(nsp7.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutRetraiteNSP') ? true : false;
formFieldsNSp7['ayantDroit_activiteAutreQueDeclareeStatut_pensionneInvalidite_NSP'] = Value('id').of(nsp7.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutPensionneInvaliditeNSP') ? true : false;
formFieldsNSp7['ayantDroit_regimeAssuranceMaladie_autre_simultane_NSP'] = Value('id').of(nsp7.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieAutreSimultaneNSP') ? true : false;
formFieldsNSp7['ayantDroit_regimeAssuranceMaladieAutre_simultane_NSP'] = nsp7.aideFamilialRegimeAssuranceMaladieAutreSimultaneTexteNsp;
formFieldsNSp7['ayantDroit_organismeServantPension_NSP'] = nsp7.aideFamilialOrganismeServantPensionNsp;
formFieldsNSp7['ayantDroit_conjointCouvertAssuranceMaladie_oui_NSP'] = Value('id').of(nsp7.aideFamilialConjointCouvertAssuranceMaladieOuiNsp).eq('oui') ? true : false;
formFieldsNSp7['ayantDroit_conjointCouvertAssuranceMaladie_non_NSP'] = Value('id').of(nsp7.aideFamilialConjointCouvertAssuranceMaladieOuiNsp).eq('non') ? true : false;

var nirDeclarant = nsp7.aideFamilialCouvertAssuranceMaladieNumeroSSNsp;
if (nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsNSp7['ayantDroit_conjointCouvertAssuranceMaladie_numeroSS_NSP'] = nirDeclarant.substring(0, 13);
    formFieldsNSp7['ayantDroit_conjointCouvertAssuranceMaladie_numeroSSCle_NSP'] = nirDeclarant.substring(13, 15);
}
//cadre 3B

formFieldsNSp7['ayantDroit_adresse_nomVoie_NSP'] = (nsp7.aideFamilialAdresse.aideFamilialAdresseNumeroVoie != null ? nsp7.aideFamilialAdresse.aideFamilialAdresseNumeroVoie : '')
 + ' ' + (nsp7.aideFamilialAdresse.aideFamilialAdresseIndiceVoie != null ? nsp7.aideFamilialAdresse.aideFamilialAdresseIndiceVoie : '')
 + ' ' + (nsp7.aideFamilialAdresse.aideFamilialAdresseTypeVoie != null ? nsp7.aideFamilialAdresse.aideFamilialAdresseTypeVoie : '')
 + ' ' + nsp7.aideFamilialAdresse.aideFamilialAdresseNomVoie
 + ' ' + (nsp7.aideFamilialAdresse.aideFamilialAdresseComplement != null ? nsp7.aideFamilialAdresse.aideFamilialAdresseComplement : '')
 + ' ' + (nsp7.aideFamilialAdresse.aideFamilialAdresseDistributionSpeciale != null ? nsp7.aideFamilialAdresse.aideFamilialAdresseDistributionSpeciale : '');
formFieldsNSp7['ayantDroit_adresse_codePostal_NSP'] = nsp7.aideFamilialAdresse.aideFamilialAdresseCodePostal;
formFieldsNSp7['ayantDroit_adresse_commune_NSP'] = nsp7.aideFamilialAdresse.aideFamilialAdresseCommune;

//-------------------------Premier ayant-droit de l'aide familial

formFieldsNSp7['ayantDroit_identite_nomNaissance1_NSP'] = nsp7.ayantsDroitsaideFamilialNsp71.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp7.ayantsDroitsaideFamilialNsp71.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp7.ayantsDroitsaideFamilialNsp71.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp7['ayantDroit_numeroSecuriteSociale1_NSP'] = nsp7.ayantsDroitsaideFamilialNsp71.ayantDroitNumeroSecuriteSociale != null ? nsp7.ayantsDroitsaideFamilialNsp71.ayantDroitNumeroSecuriteSociale : '';
if (nsp7.ayantsDroitsaideFamilialNsp71.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp7.ayantsDroitsaideFamilialNsp71.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSp7['ayantDroit_identiteComplement_dateNaissance1_NSP'] = date1;
}
formFieldsNSp7['ayantDroit_identiteComplement_lieuNaissanceCommune1_NSP'] = nsp7.ayantsDroitsaideFamilialNsp71.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp7.ayantsDroitsaideFamilialNsp71.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp7.ayantsDroitsaideFamilialNsp71.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp7['ayantDroit_lienParente1_NSP'] = nsp7.ayantsDroitsaideFamilialNsp71.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp7['ayantDroit_enfantScolarise_oui1_NSP'] = nsp7.ayantsDroitsaideFamilialNsp71.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp7.ayantsDroitsaideFamilialNsp71.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp7['ayantDroit_enfantScolarise_non1_NSP'] = nsp7.ayantsDroitsaideFamilialNsp71.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp7.ayantsDroitsaideFamilialNsp71.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp7['ayantDroit_nationalite1_NSP'] = nsp7.ayantsDroitsaideFamilialNsp71.ayantDroitNationaliteAideFamilialNsp;

//---------------------Deuxième ayant-droit de l'aide familial

formFieldsNSp7['ayantDroit_identite_nomNaissance2_NSP'] = nsp7.ayantsDroitsaideFamilialNsp72.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp7.ayantsDroitsaideFamilialNsp72.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp7.ayantsDroitsaideFamilialNsp72.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp7['ayantDroit_numeroSecuriteSociale2_NSP'] = nsp7.ayantsDroitsaideFamilialNsp72.ayantDroitNumeroSecuriteSociale != null ? nsp7.ayantsDroitsaideFamilialNsp72.ayantDroitNumeroSecuriteSociale : '';
if (nsp7.ayantsDroitsaideFamilialNsp72.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp7.ayantsDroitsaideFamilialNsp72.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
    date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsNSp7['ayantDroit_identiteComplement_dateNaissance2_NSP'] = date2;
}
formFieldsNSp7['ayantDroit_identiteComplement_lieuNaissanceCommune2_NSP'] = nsp7.ayantsDroitsaideFamilialNsp72.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp7.ayantsDroitsaideFamilialNsp72.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp7.ayantsDroitsaideFamilialNsp72.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp7['ayantDroit_lienParente2_NSP'] = nsp7.ayantsDroitsaideFamilialNsp72.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp7['ayantDroit_enfantScolarise_oui2_NSP'] = nsp7.ayantsDroitsaideFamilialNsp72.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp7.ayantsDroitsaideFamilialNsp72.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp7['ayantDroit_enfantScolarise_non2_NSP'] = nsp7.ayantsDroitsaideFamilialNsp72.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp7.ayantsDroitsaideFamilialNsp72.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp7['ayantDroit_nationalite2_NSP'] = nsp7.ayantsDroitsaideFamilialNsp72.ayantDroitNationaliteAideFamilialNsp;

//---------------------Troisième ayant-droit de l'aide familial
formFieldsNSp7['ayantDroit_identite_nomNaissance3_NSP'] = nsp7.ayantsDroitsaideFamilialNsp73.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp7.ayantsDroitsaideFamilialNsp73.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp7.ayantsDroitsaideFamilialNsp73.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp7['ayantDroit_numeroSecuriteSociale3_NSP'] = nsp7.ayantsDroitsaideFamilialNsp73.ayantDroitNumeroSecuriteSociale != null ? nsp7.ayantsDroitsaideFamilialNsp73.ayantDroitNumeroSecuriteSociale : '';
if (nsp7.ayantsDroitsaideFamilialNsp73.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp7.ayantsDroitsaideFamilialNsp73.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date3 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date3 = date3.concat(pad(month.toString()));
    date3 = date3.concat(dateTmp.getFullYear().toString());
    formFieldsNSp7['ayantDroit_identiteComplement_dateNaissance3_NSP'] = date3;
}
formFieldsNSp7['ayantDroit_identiteComplement_lieuNaissanceCommune3_NSP'] = nsp7.ayantsDroitsaideFamilialNsp73.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp7.ayantsDroitsaideFamilialNsp73.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp7.ayantsDroitsaideFamilialNsp73.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp7['ayantDroit_lienParente3_NSP'] = nsp7.ayantsDroitsaideFamilialNsp73.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp7['ayantDroit_enfantScolarise_oui3_NSP'] = nsp7.ayantsDroitsaideFamilialNsp73.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp7.ayantsDroitsaideFamilialNsp73.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp7['ayantDroit_enfantScolarise_non3_NSP'] = nsp7.ayantsDroitsaideFamilialNsp73.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp7.ayantsDroitsaideFamilialNsp73.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp7['ayantDroit_nationalite3_NSP'] = nsp7.ayantsDroitsaideFamilialNsp73.ayantDroitNationaliteAideFamilialNsp;

// Signature

formFieldsNSp7['formalite_signataireQualite_declarantNSP'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal') != false ? true : false;
formFieldsNSp7['formalite_signataireQualite_mandataireNSP'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire') != false ? true : false;
formFieldsNSp7['formalite_signataireNom_NSP'] = tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire : '';
formFieldsNSp7['formalite_signataireAdresse_NSP'] = (tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire :'')
+ ' ' + (tag.adresseMandataire.numeroVoieMandataire != null ? tag.adresseMandataire.numeroVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.indiceVoieMandataire != null ? tag.adresseMandataire.indiceVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.typeVoieMandataire != null ? tag.adresseMandataire.typeVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.nomVoieMandataire != null ? tag.adresseMandataire.nomVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.complementVoieMandataire != null ? tag.adresseMandataire.complementVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
+ ' ' + (tag.adresseMandataire.codePostalMandataire != null ? tag.adresseMandataire.codePostalMandataire : '')
+ ' ' + (tag.adresseMandataire.villeAdresseMandataire != null ? tag.adresseMandataire.villeAdresseMandataire : ''); 

formFieldsNSp7['formalite_signatureLieu_NSP'] = tag.formaliteSignatureLieu;
if (tag.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSp7['formalite_signatureDate_NSP'] = tag.formaliteSignatureDate;
}

formFieldsNSp7['signature_NSP'] = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";

// A remplir avec le formulaire principal
formFieldsNSp7['nombreP0prime_NSP'] = '';

// NSP Dirigeant 8
//Cadre 1
formFieldsNSp8['ayantDroit_lienParente_membreExploitation_NSP'] = false;
formFieldsNSp8['ayantDroit_lienParente_aideFamilial_NSP'] = true;
formFieldsNSp8['ayantDroit_lienParente_associeExploitation_NSP'] = false;
formFieldsNSp8['ayantDroit_affiliationMSA_non_NSP'] = false;
formFieldsNSp8['ayantDroit_affiliationMSA_oui_NSP'] = false;
formFieldsNSp8['formulaire_dependance_P0Agricole_NSP'] = false;
formFieldsNSp8['formulaire_dependance_FAgricole_NSP'] = false;
formFieldsNSp8['formulaire_dependance_M0Agricole_NSP'] = true;
formFieldsNSp8['formulaire_dependance_M2Agricole_NSP'] = false;
formFieldsNSp8['formulaire_dependance_M3_NSP'] = false;

//Cadre 2B

formFieldsNSp8['personneLiee_PP_nomNaissance_NSP'] = dirigeant8.civilitePersonnePhysique8.personneLieePPNomNaissance + ' ' + dirigeant8.civilitePersonnePhysique8.personneLieePPPrenom;

//cadre 3

var nirDeclarant8 = nsp8.aideFamilialNumeroSecuriteSocialeNsp;
if (nirDeclarant8 != null) {
    nirDeclarant8 = nirDeclarant8.replace(/ /g, "");
    formFieldsNSp8['ayantDroit_numeroSecuriteSociale_NSP'] = nirDeclarant8.substring(0, 13);
    formFieldsNSp8['ayantDroit_numeroSecuriteSocialeCle_NSP'] = nirDeclarant8.substring(13, 15);
}
formFieldsNSp8['ayantDroit_identite_nomNaissance_NSP'] = nsp8.aideFamilialNomNaissanceNsp;
formFieldsNSp8['ayantDroit_identite_nomUsage_NSP'] = nsp8.aideFamilialNomUsageNsp
    var prenoms = [];
for (i = 0; i < nsp8.aideFamilialPrenomNsp.size(); i++) {
    prenoms.push(nsp8.aideFamilialPrenomNsp[i]);
}
formFieldsNSp8['ayantDroit_identite_prenom_NSP'] = prenoms.toString();
formFieldsNSp8['ayantDroit_connuMSA_oui_NSP'] = nsp8.aideFamilialConnuMSAOuiNsp ? true : false;
formFieldsNSp8['ayantDroit_connuMSA_non_NSP'] = nsp8.aideFamilialConnuMSAOuiNsp ? false : true;
formFieldsNSp8['ayantDroit_regimeAssuranceMaladie_regimeGeneral_NSP'] = Value('id').of(nsp8.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieRegimeGeneralNSP') ? true : false;
formFieldsNSp8['ayantDroit_regimeAssuranceMaladie_agricole_NSP'] = Value('id').of(nsp8.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieAgricoleNSP') ? true : false;
formFieldsNSp8['ayantDroit_regimeAssuranceMaladie_nonSalarieNonAgricole_NSP'] = Value('id').of(nsp8.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieNonSalarieNonAgricoleNSP') ? true : false;
formFieldsNSp8['ayantDroit_regimeAssuranceMaladie_autre_NSP'] = Value('id').of(nsp8.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieAutreNSP') ? true : false;
formFieldsNSp8['ayantDroit_regimeAssuranceMaladieAutre_NSP'] = nsp8.aideFamilialRegimeAssuranceMaladieAutreTexteNsp;
formFieldsNSp8['ayantDroit_activiteAutreQueDeclareeStatut_oui'] = nsp8.aideFamilialActiviteAutreQueDeclareeStatutOui ? true : false;
formFieldsNSp8['ayantDroit_activiteAutreQueDeclareeStatut_non'] = nsp8.aideFamilialActiviteAutreQueDeclareeStatutOui ? false : true;
formFieldsNSp8['ayantDroit_regimeAssuranceMaladie_agricole_simultane_NSP'] = Value('id').of(nsp8.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieAgricoleSimultaneNSP') ? true : false;
formFieldsNSp8['ayantDroit_activiteAutreQueDeclareeStatut_salarie_NSP'] = Value('id').of(nsp8.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutSalarieNSP') ? true : false;
formFieldsNSp8['ayantDroit_regimeAssuranceMaladie_nonSalarieNonAgricole_simultane_NSP'] = Value('id').of(nsp8.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieNonSalarieNonAgricoleSimultaneNSP') ? true : false;
formFieldsNSp8['ayantDroit_activiteAutreQueDeclareeStatut_retraite_NSP'] = Value('id').of(nsp8.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutRetraiteNSP') ? true : false;
formFieldsNSp8['ayantDroit_activiteAutreQueDeclareeStatut_pensionneInvalidite_NSP'] = Value('id').of(nsp8.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutPensionneInvaliditeNSP') ? true : false;
formFieldsNSp8['ayantDroit_regimeAssuranceMaladie_autre_simultane_NSP'] = Value('id').of(nsp8.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieAutreSimultaneNSP') ? true : false;
formFieldsNSp8['ayantDroit_regimeAssuranceMaladieAutre_simultane_NSP'] = nsp8.aideFamilialRegimeAssuranceMaladieAutreSimultaneTexteNsp;
formFieldsNSp8['ayantDroit_organismeServantPension_NSP'] = nsp8.aideFamilialOrganismeServantPensionNsp;
formFieldsNSp8['ayantDroit_conjointCouvertAssuranceMaladie_oui_NSP'] = Value('id').of(nsp8.aideFamilialConjointCouvertAssuranceMaladieOuiNsp).eq('oui') ? true : false;
formFieldsNSp8['ayantDroit_conjointCouvertAssuranceMaladie_non_NSP'] = Value('id').of(nsp8.aideFamilialConjointCouvertAssuranceMaladieOuiNsp).eq('non') ? true : false;

var nirDeclarant = nsp8.aideFamilialCouvertAssuranceMaladieNumeroSSNsp;
if (nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsNSp8['ayantDroit_conjointCouvertAssuranceMaladie_numeroSS_NSP'] = nirDeclarant.substring(0, 13);
    formFieldsNSp8['ayantDroit_conjointCouvertAssuranceMaladie_numeroSSCle_NSP'] = nirDeclarant.substring(13, 15);
}
//cadre 3B

formFieldsNSp8['ayantDroit_adresse_nomVoie_NSP'] = (nsp8.aideFamilialAdresse.aideFamilialAdresseNumeroVoie != null ? nsp8.aideFamilialAdresse.aideFamilialAdresseNumeroVoie : '')
 + ' ' + (nsp8.aideFamilialAdresse.aideFamilialAdresseIndiceVoie != null ? nsp8.aideFamilialAdresse.aideFamilialAdresseIndiceVoie : '')
 + ' ' + (nsp8.aideFamilialAdresse.aideFamilialAdresseTypeVoie != null ? nsp8.aideFamilialAdresse.aideFamilialAdresseTypeVoie : '')
 + ' ' + nsp8.aideFamilialAdresse.aideFamilialAdresseNomVoie
 + ' ' + (nsp8.aideFamilialAdresse.aideFamilialAdresseComplement != null ? nsp8.aideFamilialAdresse.aideFamilialAdresseComplement : '')
 + ' ' + (nsp8.aideFamilialAdresse.aideFamilialAdresseDistributionSpeciale != null ? nsp8.aideFamilialAdresse.aideFamilialAdresseDistributionSpeciale : '');
formFieldsNSp8['ayantDroit_adresse_codePostal_NSP'] = nsp8.aideFamilialAdresse.aideFamilialAdresseCodePostal;
formFieldsNSp8['ayantDroit_adresse_commune_NSP'] = nsp8.aideFamilialAdresse.aideFamilialAdresseCommune;

//-------------------------Premier ayant-droit de l'aide familial

formFieldsNSp8['ayantDroit_identite_nomNaissance1_NSP'] = nsp8.ayantsDroitsaideFamilialNsp81.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp8.ayantsDroitsaideFamilialNsp81.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp8.ayantsDroitsaideFamilialNsp81.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp8['ayantDroit_numeroSecuriteSociale1_NSP'] = nsp8.ayantsDroitsaideFamilialNsp81.ayantDroitNumeroSecuriteSociale != null ? nsp8.ayantsDroitsaideFamilialNsp81.ayantDroitNumeroSecuriteSociale : '';
if (nsp8.ayantsDroitsaideFamilialNsp81.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp8.ayantsDroitsaideFamilialNsp81.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSp8['ayantDroit_identiteComplement_dateNaissance1_NSP'] = date1;
}
formFieldsNSp8['ayantDroit_identiteComplement_lieuNaissanceCommune1_NSP'] = nsp8.ayantsDroitsaideFamilialNsp81.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp8.ayantsDroitsaideFamilialNsp81.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp8.ayantsDroitsaideFamilialNsp81.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp8['ayantDroit_lienParente1_NSP'] = nsp8.ayantsDroitsaideFamilialNsp81.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp8['ayantDroit_enfantScolarise_oui1_NSP'] = nsp8.ayantsDroitsaideFamilialNsp81.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp8.ayantsDroitsaideFamilialNsp81.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp8['ayantDroit_enfantScolarise_non1_NSP'] = nsp8.ayantsDroitsaideFamilialNsp81.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp8.ayantsDroitsaideFamilialNsp81.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp8['ayantDroit_nationalite1_NSP'] = nsp8.ayantsDroitsaideFamilialNsp81.ayantDroitNationaliteAideFamilialNsp;

//---------------------Deuxième ayant-droit de l'aide familial

formFieldsNSp8['ayantDroit_identite_nomNaissance2_NSP'] = nsp8.ayantsDroitsaideFamilialNsp82.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp8.ayantsDroitsaideFamilialNsp82.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp8.ayantsDroitsaideFamilialNsp82.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp8['ayantDroit_numeroSecuriteSociale2_NSP'] = nsp8.ayantsDroitsaideFamilialNsp82.ayantDroitNumeroSecuriteSociale != null ? nsp8.ayantsDroitsaideFamilialNsp82.ayantDroitNumeroSecuriteSociale : '';
if (nsp8.ayantsDroitsaideFamilialNsp82.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp8.ayantsDroitsaideFamilialNsp82.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
    date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsNSp8['ayantDroit_identiteComplement_dateNaissance2_NSP'] = date2;
}
formFieldsNSp8['ayantDroit_identiteComplement_lieuNaissanceCommune2_NSP'] = nsp8.ayantsDroitsaideFamilialNsp82.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp8.ayantsDroitsaideFamilialNsp82.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp8.ayantsDroitsaideFamilialNsp82.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp8['ayantDroit_lienParente2_NSP'] = nsp8.ayantsDroitsaideFamilialNsp82.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp8['ayantDroit_enfantScolarise_oui2_NSP'] = nsp8.ayantsDroitsaideFamilialNsp82.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp8.ayantsDroitsaideFamilialNsp82.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp8['ayantDroit_enfantScolarise_non2_NSP'] = nsp8.ayantsDroitsaideFamilialNsp82.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp8.ayantsDroitsaideFamilialNsp82.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp8['ayantDroit_nationalite2_NSP'] = nsp8.ayantsDroitsaideFamilialNsp82.ayantDroitNationaliteAideFamilialNsp;

//---------------------Troisième ayant-droit de l'aide familial
formFieldsNSp8['ayantDroit_identite_nomNaissance3_NSP'] = nsp8.ayantsDroitsaideFamilialNsp83.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp8.ayantsDroitsaideFamilialNsp83.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp8.ayantsDroitsaideFamilialNsp83.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp8['ayantDroit_numeroSecuriteSociale3_NSP'] = nsp8.ayantsDroitsaideFamilialNsp83.ayantDroitNumeroSecuriteSociale != null ? nsp8.ayantsDroitsaideFamilialNsp83.ayantDroitNumeroSecuriteSociale : '';
if (nsp8.ayantsDroitsaideFamilialNsp83.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp8.ayantsDroitsaideFamilialNsp83.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date3 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date3 = date3.concat(pad(month.toString()));
    date3 = date3.concat(dateTmp.getFullYear().toString());
    formFieldsNSp8['ayantDroit_identiteComplement_dateNaissance3_NSP'] = date3;
}
formFieldsNSp8['ayantDroit_identiteComplement_lieuNaissanceCommune3_NSP'] = nsp8.ayantsDroitsaideFamilialNsp83.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp8.ayantsDroitsaideFamilialNsp83.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp8.ayantsDroitsaideFamilialNsp83.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp8['ayantDroit_lienParente3_NSP'] = nsp8.ayantsDroitsaideFamilialNsp83.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp8['ayantDroit_enfantScolarise_oui3_NSP'] = nsp8.ayantsDroitsaideFamilialNsp83.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp8.ayantsDroitsaideFamilialNsp83.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp8['ayantDroit_enfantScolarise_non3_NSP'] = nsp8.ayantsDroitsaideFamilialNsp83.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp8.ayantsDroitsaideFamilialNsp83.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp8['ayantDroit_nationalite3_NSP'] = nsp8.ayantsDroitsaideFamilialNsp83.ayantDroitNationaliteAideFamilialNsp;

// Signature

formFieldsNSp8['formalite_signataireQualite_declarantNSP'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal') != false ? true : false;
formFieldsNSp8['formalite_signataireQualite_mandataireNSP'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire') != false ? true : false;
formFieldsNSp8['formalite_signataireNom_NSP'] = tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire : '';
formFieldsNSp8['formalite_signataireAdresse_NSP'] = (tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire :'')
+ ' ' + (tag.adresseMandataire.numeroVoieMandataire != null ? tag.adresseMandataire.numeroVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.indiceVoieMandataire != null ? tag.adresseMandataire.indiceVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.typeVoieMandataire != null ? tag.adresseMandataire.typeVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.nomVoieMandataire != null ? tag.adresseMandataire.nomVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.complementVoieMandataire != null ? tag.adresseMandataire.complementVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
+ ' ' + (tag.adresseMandataire.codePostalMandataire != null ? tag.adresseMandataire.codePostalMandataire : '')
+ ' ' + (tag.adresseMandataire.villeAdresseMandataire != null ? tag.adresseMandataire.villeAdresseMandataire : ''); 

formFieldsNSp8['formalite_signatureLieu_NSP'] = tag.formaliteSignatureLieu;
if (tag.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSp8['formalite_signatureDate_NSP'] = tag.formaliteSignatureDate;
}

formFieldsNSp8['signature_NSP'] = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";

// A remplir avec le formulaire principal
formFieldsNSp8['nombreP0prime_NSP'] = '';

// NSP Dirigeant 9
//Cadre 1
formFieldsNSp9['ayantDroit_lienParente_membreExploitation_NSP'] = false;
formFieldsNSp9['ayantDroit_lienParente_aideFamilial_NSP'] = true;
formFieldsNSp9['ayantDroit_lienParente_associeExploitation_NSP'] = false;
formFieldsNSp9['ayantDroit_affiliationMSA_non_NSP'] = false;
formFieldsNSp9['ayantDroit_affiliationMSA_oui_NSP'] = false;
formFieldsNSp9['formulaire_dependance_P0Agricole_NSP'] = false;
formFieldsNSp9['formulaire_dependance_FAgricole_NSP'] = false;
formFieldsNSp9['formulaire_dependance_M0Agricole_NSP'] = true;
formFieldsNSp9['formulaire_dependance_M2Agricole_NSP'] = false;
formFieldsNSp9['formulaire_dependance_M3_NSP'] = false;

//Cadre 2B

formFieldsNSp9['personneLiee_PP_nomNaissance_NSP'] = dirigeant9.civilitePersonnePhysique9.personneLieePPNomNaissance + ' ' + dirigeant9.civilitePersonnePhysique9.personneLieePPPrenom;

//cadre 3

var nirDeclarant9 = nsp9.aideFamilialNumeroSecuriteSocialeNsp;
if (nirDeclarant9 != null) {
    nirDeclarant9 = nirDeclarant9.replace(/ /g, "");
    formFieldsNSp9['ayantDroit_numeroSecuriteSociale_NSP'] = nirDeclarant9.substring(0, 13);
    formFieldsNSp9['ayantDroit_numeroSecuriteSocialeCle_NSP'] = nirDeclarant9.substring(13, 15);
}
formFieldsNSp9['ayantDroit_identite_nomNaissance_NSP'] = nsp9.aideFamilialNomNaissanceNsp;
formFieldsNSp9['ayantDroit_identite_nomUsage_NSP'] = nsp9.aideFamilialNomUsageNsp
    var prenoms = [];
for (i = 0; i < nsp9.aideFamilialPrenomNsp.size(); i++) {
    prenoms.push(nsp9.aideFamilialPrenomNsp[i]);
}
formFieldsNSp9['ayantDroit_identite_prenom_NSP'] = prenoms.toString();
formFieldsNSp9['ayantDroit_connuMSA_oui_NSP'] = nsp9.aideFamilialConnuMSAOuiNsp ? true : false;
formFieldsNSp9['ayantDroit_connuMSA_non_NSP'] = nsp9.aideFamilialConnuMSAOuiNsp ? false : true;
formFieldsNSp9['ayantDroit_regimeAssuranceMaladie_regimeGeneral_NSP'] = Value('id').of(nsp9.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieRegimeGeneralNSP') ? true : false;
formFieldsNSp9['ayantDroit_regimeAssuranceMaladie_agricole_NSP'] = Value('id').of(nsp9.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieAgricoleNSP') ? true : false;
formFieldsNSp9['ayantDroit_regimeAssuranceMaladie_nonSalarieNonAgricole_NSP'] = Value('id').of(nsp9.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieNonSalarieNonAgricoleNSP') ? true : false;
formFieldsNSp9['ayantDroit_regimeAssuranceMaladie_autre_NSP'] = Value('id').of(nsp9.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieAutreNSP') ? true : false;
formFieldsNSp9['ayantDroit_regimeAssuranceMaladieAutre_NSP'] = nsp9.aideFamilialRegimeAssuranceMaladieAutreTexteNsp;
formFieldsNSp9['ayantDroit_activiteAutreQueDeclareeStatut_oui'] = nsp9.aideFamilialActiviteAutreQueDeclareeStatutOui ? true : false;
formFieldsNSp9['ayantDroit_activiteAutreQueDeclareeStatut_non'] = nsp9.aideFamilialActiviteAutreQueDeclareeStatutOui ? false : true;
formFieldsNSp9['ayantDroit_regimeAssuranceMaladie_agricole_simultane_NSP'] = Value('id').of(nsp9.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieAgricoleSimultaneNSP') ? true : false;
formFieldsNSp9['ayantDroit_activiteAutreQueDeclareeStatut_salarie_NSP'] = Value('id').of(nsp9.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutSalarieNSP') ? true : false;
formFieldsNSp9['ayantDroit_regimeAssuranceMaladie_nonSalarieNonAgricole_simultane_NSP'] = Value('id').of(nsp9.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieNonSalarieNonAgricoleSimultaneNSP') ? true : false;
formFieldsNSp9['ayantDroit_activiteAutreQueDeclareeStatut_retraite_NSP'] = Value('id').of(nsp9.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutRetraiteNSP') ? true : false;
formFieldsNSp9['ayantDroit_activiteAutreQueDeclareeStatut_pensionneInvalidite_NSP'] = Value('id').of(nsp9.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutPensionneInvaliditeNSP') ? true : false;
formFieldsNSp9['ayantDroit_regimeAssuranceMaladie_autre_simultane_NSP'] = Value('id').of(nsp9.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieAutreSimultaneNSP') ? true : false;
formFieldsNSp9['ayantDroit_regimeAssuranceMaladieAutre_simultane_NSP'] = nsp9.aideFamilialRegimeAssuranceMaladieAutreSimultaneTexteNsp;
formFieldsNSp9['ayantDroit_organismeServantPension_NSP'] = nsp9.aideFamilialOrganismeServantPensionNsp;
formFieldsNSp9['ayantDroit_conjointCouvertAssuranceMaladie_oui_NSP'] = Value('id').of(nsp9.aideFamilialConjointCouvertAssuranceMaladieOuiNsp).eq('oui') ? true : false;
formFieldsNSp9['ayantDroit_conjointCouvertAssuranceMaladie_non_NSP'] = Value('id').of(nsp9.aideFamilialConjointCouvertAssuranceMaladieOuiNsp).eq('non') ? true : false;

var nirDeclarant = nsp9.aideFamilialCouvertAssuranceMaladieNumeroSSNsp;
if (nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsNSp9['ayantDroit_conjointCouvertAssuranceMaladie_numeroSS_NSP'] = nirDeclarant.substring(0, 13);
    formFieldsNSp9['ayantDroit_conjointCouvertAssuranceMaladie_numeroSSCle_NSP'] = nirDeclarant.substring(13, 15);
}
//cadre 3B

formFieldsNSp9['ayantDroit_adresse_nomVoie_NSP'] = (nsp9.aideFamilialAdresse.aideFamilialAdresseNumeroVoie != null ? nsp9.aideFamilialAdresse.aideFamilialAdresseNumeroVoie : '')
 + ' ' + (nsp9.aideFamilialAdresse.aideFamilialAdresseIndiceVoie != null ? nsp9.aideFamilialAdresse.aideFamilialAdresseIndiceVoie : '')
 + ' ' + (nsp9.aideFamilialAdresse.aideFamilialAdresseTypeVoie != null ? nsp9.aideFamilialAdresse.aideFamilialAdresseTypeVoie : '')
 + ' ' + nsp9.aideFamilialAdresse.aideFamilialAdresseNomVoie
 + ' ' + (nsp9.aideFamilialAdresse.aideFamilialAdresseComplement != null ? nsp9.aideFamilialAdresse.aideFamilialAdresseComplement : '')
 + ' ' + (nsp9.aideFamilialAdresse.aideFamilialAdresseDistributionSpeciale != null ? nsp9.aideFamilialAdresse.aideFamilialAdresseDistributionSpeciale : '');
formFieldsNSp9['ayantDroit_adresse_codePostal_NSP'] = nsp9.aideFamilialAdresse.aideFamilialAdresseCodePostal;
formFieldsNSp9['ayantDroit_adresse_commune_NSP'] = nsp9.aideFamilialAdresse.aideFamilialAdresseCommune;

//-------------------------Premier ayant-droit de l'aide familial

formFieldsNSp9['ayantDroit_identite_nomNaissance1_NSP'] = nsp9.ayantsDroitsaideFamilialNsp91.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp9.ayantsDroitsaideFamilialNsp91.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp9.ayantsDroitsaideFamilialNsp91.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp9['ayantDroit_numeroSecuriteSociale1_NSP'] = nsp9.ayantsDroitsaideFamilialNsp91.ayantDroitNumeroSecuriteSociale != null ? nsp9.ayantsDroitsaideFamilialNsp91.ayantDroitNumeroSecuriteSociale : '';
if (nsp9.ayantsDroitsaideFamilialNsp91.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp9.ayantsDroitsaideFamilialNsp91.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSp9['ayantDroit_identiteComplement_dateNaissance1_NSP'] = date1;
}
formFieldsNSp9['ayantDroit_identiteComplement_lieuNaissanceCommune1_NSP'] = nsp9.ayantsDroitsaideFamilialNsp91.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp9.ayantsDroitsaideFamilialNsp91.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp9.ayantsDroitsaideFamilialNsp91.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp9['ayantDroit_lienParente1_NSP'] = nsp9.ayantsDroitsaideFamilialNsp91.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp9['ayantDroit_enfantScolarise_oui1_NSP'] = nsp9.ayantsDroitsaideFamilialNsp91.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp9.ayantsDroitsaideFamilialNsp91.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp9['ayantDroit_enfantScolarise_non1_NSP'] = nsp9.ayantsDroitsaideFamilialNsp91.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp9.ayantsDroitsaideFamilialNsp91.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp9['ayantDroit_nationalite1_NSP'] = nsp9.ayantsDroitsaideFamilialNsp91.ayantDroitNationaliteAideFamilialNsp;

//---------------------Deuxième ayant-droit de l'aide familial

formFieldsNSp9['ayantDroit_identite_nomNaissance2_NSP'] = nsp9.ayantsDroitsaideFamilialNsp92.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp9.ayantsDroitsaideFamilialNsp92.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp9.ayantsDroitsaideFamilialNsp92.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp9['ayantDroit_numeroSecuriteSociale2_NSP'] = nsp9.ayantsDroitsaideFamilialNsp92.ayantDroitNumeroSecuriteSociale != null ? nsp9.ayantsDroitsaideFamilialNsp92.ayantDroitNumeroSecuriteSociale : '';
if (nsp9.ayantsDroitsaideFamilialNsp92.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp9.ayantsDroitsaideFamilialNsp92.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
    date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsNSp9['ayantDroit_identiteComplement_dateNaissance2_NSP'] = date2;
}
formFieldsNSp9['ayantDroit_identiteComplement_lieuNaissanceCommune2_NSP'] = nsp9.ayantsDroitsaideFamilialNsp92.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp9.ayantsDroitsaideFamilialNsp92.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp9.ayantsDroitsaideFamilialNsp92.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp9['ayantDroit_lienParente2_NSP'] = nsp9.ayantsDroitsaideFamilialNsp92.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp9['ayantDroit_enfantScolarise_oui2_NSP'] = nsp9.ayantsDroitsaideFamilialNsp92.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp9.ayantsDroitsaideFamilialNsp92.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp9['ayantDroit_enfantScolarise_non2_NSP'] = nsp9.ayantsDroitsaideFamilialNsp92.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp9.ayantsDroitsaideFamilialNsp92.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp9['ayantDroit_nationalite2_NSP'] = nsp9.ayantsDroitsaideFamilialNsp92.ayantDroitNationaliteAideFamilialNsp;

//---------------------Troisième ayant-droit de l'aide familial
formFieldsNSp9['ayantDroit_identite_nomNaissance3_NSP'] = nsp9.ayantsDroitsaideFamilialNsp93.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp9.ayantsDroitsaideFamilialNsp93.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp9.ayantsDroitsaideFamilialNsp93.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp9['ayantDroit_numeroSecuriteSociale3_NSP'] = nsp9.ayantsDroitsaideFamilialNsp93.ayantDroitNumeroSecuriteSociale != null ? nsp9.ayantsDroitsaideFamilialNsp93.ayantDroitNumeroSecuriteSociale : '';
if (nsp9.ayantsDroitsaideFamilialNsp93.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp9.ayantsDroitsaideFamilialNsp93.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date3 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date3 = date3.concat(pad(month.toString()));
    date3 = date3.concat(dateTmp.getFullYear().toString());
    formFieldsNSp9['ayantDroit_identiteComplement_dateNaissance3_NSP'] = date3;
}
formFieldsNSp9['ayantDroit_identiteComplement_lieuNaissanceCommune3_NSP'] = nsp9.ayantsDroitsaideFamilialNsp93.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp9.ayantsDroitsaideFamilialNsp93.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp9.ayantsDroitsaideFamilialNsp93.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp9['ayantDroit_lienParente3_NSP'] = nsp9.ayantsDroitsaideFamilialNsp93.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp9['ayantDroit_enfantScolarise_oui3_NSP'] = nsp9.ayantsDroitsaideFamilialNsp93.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp9.ayantsDroitsaideFamilialNsp93.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp9['ayantDroit_enfantScolarise_non3_NSP'] = nsp9.ayantsDroitsaideFamilialNsp93.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp9.ayantsDroitsaideFamilialNsp93.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp9['ayantDroit_nationalite3_NSP'] = nsp9.ayantsDroitsaideFamilialNsp93.ayantDroitNationaliteAideFamilialNsp;

// Signature

formFieldsNSp9['formalite_signataireQualite_declarantNSP'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal') != false ? true : false;
formFieldsNSp9['formalite_signataireQualite_mandataireNSP'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire') != false ? true : false;
formFieldsNSp9['formalite_signataireNom_NSP'] = tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire : '';
formFieldsNSp9['formalite_signataireAdresse_NSP'] = (tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire :'')
+ ' ' + (tag.adresseMandataire.numeroVoieMandataire != null ? tag.adresseMandataire.numeroVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.indiceVoieMandataire != null ? tag.adresseMandataire.indiceVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.typeVoieMandataire != null ? tag.adresseMandataire.typeVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.nomVoieMandataire != null ? tag.adresseMandataire.nomVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.complementVoieMandataire != null ? tag.adresseMandataire.complementVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
+ ' ' + (tag.adresseMandataire.codePostalMandataire != null ? tag.adresseMandataire.codePostalMandataire : '')
+ ' ' + (tag.adresseMandataire.villeAdresseMandataire != null ? tag.adresseMandataire.villeAdresseMandataire : ''); 

formFieldsNSp9['formalite_signatureLieu_NSP'] = tag.formaliteSignatureLieu;
if (tag.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSp9['formalite_signatureDate_NSP'] = tag.formaliteSignatureDate;
}

formFieldsNSp9['signature_NSP'] = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";

// A remplir avec le formulaire principal
formFieldsNSp9['nombreP0prime_NSP'] = '';

// NSP Dirigeant 10
//Cadre 1
formFieldsNSp10['ayantDroit_lienParente_membreExploitation_NSP'] = false;
formFieldsNSp10['ayantDroit_lienParente_aideFamilial_NSP'] = true;
formFieldsNSp10['ayantDroit_lienParente_associeExploitation_NSP'] = false;
formFieldsNSp10['ayantDroit_affiliationMSA_non_NSP'] = false;
formFieldsNSp10['ayantDroit_affiliationMSA_oui_NSP'] = false;
formFieldsNSp10['formulaire_dependance_P0Agricole_NSP'] = false;
formFieldsNSp10['formulaire_dependance_FAgricole_NSP'] = false;
formFieldsNSp10['formulaire_dependance_M0Agricole_NSP'] = true;
formFieldsNSp10['formulaire_dependance_M2Agricole_NSP'] = false;
formFieldsNSp10['formulaire_dependance_M3_NSP'] = false;

//Cadre 2B

formFieldsNSp10['personneLiee_PP_nomNaissance_NSP'] = dirigeant10.civilitePersonnePhysique10.personneLieePPNomNaissance + ' ' + dirigeant10.civilitePersonnePhysique10.personneLieePPPrenom;

//cadre 3

var nirDeclarant10 = nsp10.aideFamilialNumeroSecuriteSocialeNsp;
if (nirDeclarant10 != null) {
    nirDeclarant10 = nirDeclarant10.replace(/ /g, "");
    formFieldsNSp10['ayantDroit_numeroSecuriteSociale_NSP'] = nirDeclarant10.substring(0, 13);
    formFieldsNSp10['ayantDroit_numeroSecuriteSocialeCle_NSP'] = nirDeclarant10.substring(13, 15);
}
formFieldsNSp10['ayantDroit_identite_nomNaissance_NSP'] = nsp10.aideFamilialNomNaissanceNsp;
formFieldsNSp10['ayantDroit_identite_nomUsage_NSP'] = nsp10.aideFamilialNomUsageNsp
    var prenoms = [];
for (i = 0; i < nsp10.aideFamilialPrenomNsp.size(); i++) {
    prenoms.push(nsp10.aideFamilialPrenomNsp[i]);
}
formFieldsNSp10['ayantDroit_identite_prenom_NSP'] = prenoms.toString();
formFieldsNSp10['ayantDroit_connuMSA_oui_NSP'] = nsp10.aideFamilialConnuMSAOuiNsp ? true : false;
formFieldsNSp10['ayantDroit_connuMSA_non_NSP'] = nsp10.aideFamilialConnuMSAOuiNsp ? false : true;
formFieldsNSp10['ayantDroit_regimeAssuranceMaladie_regimeGeneral_NSP'] = Value('id').of(nsp10.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieRegimeGeneralNSP') ? true : false;
formFieldsNSp10['ayantDroit_regimeAssuranceMaladie_agricole_NSP'] = Value('id').of(nsp10.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieAgricoleNSP') ? true : false;
formFieldsNSp10['ayantDroit_regimeAssuranceMaladie_nonSalarieNonAgricole_NSP'] = Value('id').of(nsp10.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieNonSalarieNonAgricoleNSP') ? true : false;
formFieldsNSp10['ayantDroit_regimeAssuranceMaladie_autre_NSP'] = Value('id').of(nsp10.aideFamilialRegimeMaladie).eq('ayantDroitRegimeAssuranceMaladieAutreNSP') ? true : false;
formFieldsNSp10['ayantDroit_regimeAssuranceMaladieAutre_NSP'] = nsp10.aideFamilialRegimeAssuranceMaladieAutreTexteNsp;
formFieldsNSp10['ayantDroit_activiteAutreQueDeclareeStatut_oui'] = nsp10.aideFamilialActiviteAutreQueDeclareeStatutOui ? true : false;
formFieldsNSp10['ayantDroit_activiteAutreQueDeclareeStatut_non'] = nsp10.aideFamilialActiviteAutreQueDeclareeStatutOui ? false : true;
formFieldsNSp10['ayantDroit_regimeAssuranceMaladie_agricole_simultane_NSP'] = Value('id').of(nsp10.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieAgricoleSimultaneNSP') ? true : false;
formFieldsNSp10['ayantDroit_activiteAutreQueDeclareeStatut_salarie_NSP'] = Value('id').of(nsp10.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutSalarieNSP') ? true : false;
formFieldsNSp10['ayantDroit_regimeAssuranceMaladie_nonSalarieNonAgricole_simultane_NSP'] = Value('id').of(nsp10.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieNonSalarieNonAgricoleSimultaneNSP') ? true : false;
formFieldsNSp10['ayantDroit_activiteAutreQueDeclareeStatut_retraite_NSP'] = Value('id').of(nsp10.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutRetraiteNSP') ? true : false;
formFieldsNSp10['ayantDroit_activiteAutreQueDeclareeStatut_pensionneInvalidite_NSP'] = Value('id').of(nsp10.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitActiviteAutreQueDeclareeStatutPensionneInvaliditeNSP') ? true : false;
formFieldsNSp10['ayantDroit_regimeAssuranceMaladie_autre_simultane_NSP'] = Value('id').of(nsp10.aideFamilialActiviteAutreQueDeclaree).eq('ayantDroitRegimeAssuranceMaladieAutreSimultaneNSP') ? true : false;
formFieldsNSp10['ayantDroit_regimeAssuranceMaladieAutre_simultane_NSP'] = nsp10.aideFamilialRegimeAssuranceMaladieAutreSimultaneTexteNsp;
formFieldsNSp10['ayantDroit_organismeServantPension_NSP'] = nsp10.aideFamilialOrganismeServantPensionNsp;
formFieldsNSp10['ayantDroit_conjointCouvertAssuranceMaladie_oui_NSP'] = Value('id').of(nsp10.aideFamilialConjointCouvertAssuranceMaladieOuiNsp).eq('oui') ? true : false;
formFieldsNSp10['ayantDroit_conjointCouvertAssuranceMaladie_non_NSP'] = Value('id').of(nsp10.aideFamilialConjointCouvertAssuranceMaladieOuiNsp).eq('non') ? true : false;

var nirDeclarant = nsp10.aideFamilialCouvertAssuranceMaladieNumeroSSNsp;
if (nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsNSp10['ayantDroit_conjointCouvertAssuranceMaladie_numeroSS_NSP'] = nirDeclarant.substring(0, 13);
    formFieldsNSp10['ayantDroit_conjointCouvertAssuranceMaladie_numeroSSCle_NSP'] = nirDeclarant.substring(13, 15);
}
//cadre 3B

formFieldsNSp10['ayantDroit_adresse_nomVoie_NSP'] = (nsp10.aideFamilialAdresse.aideFamilialAdresseNumeroVoie != null ? nsp10.aideFamilialAdresse.aideFamilialAdresseNumeroVoie : '')
 + ' ' + (nsp10.aideFamilialAdresse.aideFamilialAdresseIndiceVoie != null ? nsp10.aideFamilialAdresse.aideFamilialAdresseIndiceVoie : '')
 + ' ' + (nsp10.aideFamilialAdresse.aideFamilialAdresseTypeVoie != null ? nsp10.aideFamilialAdresse.aideFamilialAdresseTypeVoie : '')
 + ' ' + nsp10.aideFamilialAdresse.aideFamilialAdresseNomVoie
 + ' ' + (nsp10.aideFamilialAdresse.aideFamilialAdresseComplement != null ? nsp10.aideFamilialAdresse.aideFamilialAdresseComplement : '')
 + ' ' + (nsp10.aideFamilialAdresse.aideFamilialAdresseDistributionSpeciale != null ? nsp10.aideFamilialAdresse.aideFamilialAdresseDistributionSpeciale : '');
formFieldsNSp10['ayantDroit_adresse_codePostal_NSP'] = nsp10.aideFamilialAdresse.aideFamilialAdresseCodePostal;
formFieldsNSp10['ayantDroit_adresse_commune_NSP'] = nsp10.aideFamilialAdresse.aideFamilialAdresseCommune;

//-------------------------Premier ayant-droit de l'aide familial

formFieldsNSp10['ayantDroit_identite_nomNaissance1_NSP'] = nsp10.ayantsDroitsaideFamilialNsp101.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp10.ayantsDroitsaideFamilialNsp101.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp10.ayantsDroitsaideFamilialNsp101.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp10['ayantDroit_numeroSecuriteSociale1_NSP'] = nsp10.ayantsDroitsaideFamilialNsp101.ayantDroitNumeroSecuriteSociale != null ? nsp10.ayantsDroitsaideFamilialNsp101.ayantDroitNumeroSecuriteSociale : '';
if (nsp10.ayantsDroitsaideFamilialNsp101.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp10.ayantsDroitsaideFamilialNsp101.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSp10['ayantDroit_identiteComplement_dateNaissance1_NSP'] = date1;
}
formFieldsNSp10['ayantDroit_identiteComplement_lieuNaissanceCommune1_NSP'] = nsp10.ayantsDroitsaideFamilialNsp101.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp10.ayantsDroitsaideFamilialNsp101.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp10.ayantsDroitsaideFamilialNsp101.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp10['ayantDroit_lienParente1_NSP'] = nsp10.ayantsDroitsaideFamilialNsp101.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp10['ayantDroit_enfantScolarise_oui1_NSP'] = nsp10.ayantsDroitsaideFamilialNsp101.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp10.ayantsDroitsaideFamilialNsp101.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp10['ayantDroit_enfantScolarise_non1_NSP'] = nsp10.ayantsDroitsaideFamilialNsp101.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp10.ayantsDroitsaideFamilialNsp101.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp10['ayantDroit_nationalite1_NSP'] = nsp10.ayantsDroitsaideFamilialNsp101.ayantDroitNationaliteAideFamilialNsp;

//---------------------Deuxième ayant-droit de l'aide familial

formFieldsNSp10['ayantDroit_identite_nomNaissance2_NSP'] = nsp10.ayantsDroitsaideFamilialNsp102.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp10.ayantsDroitsaideFamilialNsp102.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp10.ayantsDroitsaideFamilialNsp102.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp10['ayantDroit_numeroSecuriteSociale2_NSP'] = nsp10.ayantsDroitsaideFamilialNsp102.ayantDroitNumeroSecuriteSociale != null ? nsp10.ayantsDroitsaideFamilialNsp102.ayantDroitNumeroSecuriteSociale : '';
if (nsp10.ayantsDroitsaideFamilialNsp102.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp10.ayantsDroitsaideFamilialNsp102.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
    date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsNSp10['ayantDroit_identiteComplement_dateNaissance2_NSP'] = date2;
}
formFieldsNSp10['ayantDroit_identiteComplement_lieuNaissanceCommune2_NSP'] = nsp10.ayantsDroitsaideFamilialNsp102.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp10.ayantsDroitsaideFamilialNsp102.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp10.ayantsDroitsaideFamilialNsp102.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp10['ayantDroit_lienParente2_NSP'] = nsp10.ayantsDroitsaideFamilialNsp102.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp10['ayantDroit_enfantScolarise_oui2_NSP'] = nsp10.ayantsDroitsaideFamilialNsp102.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp10.ayantsDroitsaideFamilialNsp102.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp10['ayantDroit_enfantScolarise_non2_NSP'] = nsp10.ayantsDroitsaideFamilialNsp102.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp10.ayantsDroitsaideFamilialNsp102.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp10['ayantDroit_nationalite2_NSP'] = nsp10.ayantsDroitsaideFamilialNsp102.ayantDroitNationaliteAideFamilialNsp;

//---------------------Troisième ayant-droit de l'aide familial
formFieldsNSp10['ayantDroit_identite_nomNaissance3_NSP'] = nsp10.ayantsDroitsaideFamilialNsp103.ayantDroitNomNaissanceAideFamilialNsp != null ? (nsp10.ayantsDroitsaideFamilialNsp103.ayantDroitNomNaissanceAideFamilialNsp + ' ' + nsp10.ayantsDroitsaideFamilialNsp103.ayantDroitPrenomNaissanceAideFamilialNsp) : '';
formFieldsNSp10['ayantDroit_numeroSecuriteSociale3_NSP'] = nsp10.ayantsDroitsaideFamilialNsp103.ayantDroitNumeroSecuriteSociale != null ? nsp10.ayantsDroitsaideFamilialNsp103.ayantDroitNumeroSecuriteSociale : '';
if (nsp10.ayantsDroitsaideFamilialNsp103.ayantDroitDateNaissanceAideFamilialNsp != null) {
    var dateTmp = new Date(parseInt(nsp10.ayantsDroitsaideFamilialNsp103.ayantDroitDateNaissanceAideFamilialNsp.getTimeInMillis()));
    var date3 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date3 = date3.concat(pad(month.toString()));
    date3 = date3.concat(dateTmp.getFullYear().toString());
    formFieldsNSp10['ayantDroit_identiteComplement_dateNaissance3_NSP'] = date3;
}
formFieldsNSp10['ayantDroit_identiteComplement_lieuNaissanceCommune3_NSP'] = nsp10.ayantsDroitsaideFamilialNsp103.ayantDroitLieuNaissanceCommuneAideFamilialNsp != null ? (nsp10.ayantsDroitsaideFamilialNsp103.ayantDroitLieuNaissanceCommuneAideFamilialNsp + ' ' + nsp10.ayantsDroitsaideFamilialNsp103.ayantDroitSexeAideFamilialNsp) : '';
formFieldsNSp10['ayantDroit_lienParente3_NSP'] = nsp10.ayantsDroitsaideFamilialNsp103.ayantDroitLienParenteAideFamilialNsp;
formFieldsNSp10['ayantDroit_enfantScolarise_oui3_NSP'] = nsp10.ayantsDroitsaideFamilialNsp103.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp10.ayantsDroitsaideFamilialNsp103.ayantDroitEnfantScolariseAideFamilialNsp ? true : false) : '' ;
formFieldsNSp10['ayantDroit_enfantScolarise_non3_NSP'] = nsp10.ayantsDroitsaideFamilialNsp103.ayantDroitEnfantScolariseAideFamilialNsp  ? (nsp10.ayantsDroitsaideFamilialNsp103.ayantDroitEnfantScolariseAideFamilialNsp ? false : true) : '' ;
formFieldsNSp10['ayantDroit_nationalite3_NSP'] = nsp10.ayantsDroitsaideFamilialNsp103.ayantDroitNationaliteAideFamilialNsp;

// Signature

formFieldsNSp10['formalite_signataireQualite_declarantNSP'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal') != false ? true : false;
formFieldsNSp10['formalite_signataireQualite_mandataireNSP'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire') != false ? true : false;
formFieldsNSp10['formalite_signataireNom_NSP'] = tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire : '';
formFieldsNSp10['formalite_signataireAdresse_NSP'] = (tag.adresseMandataire.nomPrenomDenominationMandataire != null ? tag.adresseMandataire.nomPrenomDenominationMandataire :'')
+ ' ' + (tag.adresseMandataire.numeroVoieMandataire != null ? tag.adresseMandataire.numeroVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.indiceVoieMandataire != null ? tag.adresseMandataire.indiceVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.typeVoieMandataire != null ? tag.adresseMandataire.typeVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.nomVoieMandataire != null ? tag.adresseMandataire.nomVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.complementVoieMandataire != null ? tag.adresseMandataire.complementVoieMandataire : '')
+ ' ' + (tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
+ ' ' + (tag.adresseMandataire.codePostalMandataire != null ? tag.adresseMandataire.codePostalMandataire : '')
+ ' ' + (tag.adresseMandataire.villeAdresseMandataire != null ? tag.adresseMandataire.villeAdresseMandataire : ''); 

formFieldsNSp10['formalite_signatureLieu_NSP'] = tag.formaliteSignatureLieu;
if (tag.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsNSp10['formalite_signatureDate_NSP'] = tag.formaliteSignatureDate;
}
formFieldsNSp10['signature_NSP'] = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";

// A remplir avec le formulaire principal
formFieldsNSp10['nombreP0prime_NSP'] = '';

// Intercalaire NDI

// Cadre 1

formFieldsNDI['complete_immatriculation_ndi']                   = true;
formFieldsNDI['complete_modification_ndi']                      = false;

// Cadre 2

formFieldsNDI['denomination_ndi']      = societe.entrepriseDenomination;
formFieldsNDI['adresseSiege_voie_ndi'] = (siege.siegeAdresseNumeroVoie != null ? siege.siegeAdresseNumeroVoie : ' ') + ' ' +
(siege.siegeAdresseIndiceVoie != null ? siege.siegeAdresseIndiceVoie : ' ') + ' ' +
(siege.siegeAdresseTypeVoie != null ? siege.siegeAdresseTypeVoie : ' ') + ' ' +
siege.siegeAdresseNomVoie + ' ' +
(siege.siegeAdresseComplementVoie != null ? siege.siegeAdresseComplementVoie : '') + ' ' +
(siege.siegeADresseDistributionSpeciale != null ? siege.siegeADresseDistributionSpeciale : '');
formFieldsNDI['adresseSiege_codePostal_ndi'] = siege.siegeAdresseCodePostalEtranger != null ? siege.siegeAdresseCodePostalEtranger : siege.siegeAdresseCodePostal;
formFieldsNDI['adresseSiege_commune_ndi'] = siege.siegeAdresseVille != null ? (siege.siegeAdresseVille + ' / ' + siege.siegeAdressePays) : (siege.siegeAdresseCommune + ' / ' + siege.siegeAdressePays);

// Cadre 3
if(activite.cadreNomExploitation.etablissementNomDomaine != null) {
	if (activite.informationActivite.etablissementDateDebutActivite != null) {
		var dateTmp = new Date(parseInt(activite.informationActivite.etablissementDateDebutActivite.getTimeInMillis()));
		var date = pad(dateTmp.getDate().toString());
		var month = dateTmp.getMonth() + 1;
		date = date.concat(pad(month.toString()));
		date = date.concat(dateTmp.getFullYear().toString());
		formFieldsNDI['dateDebutActivite_ndi'] = date;
	} else if (tag.formaliteSignatureDate != null) {
		var dateTmp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
		var date1 = pad(dateTmp.getDate().toString());
		var month = dateTmp.getMonth() + 1;
		date1 = date1.concat(pad(month.toString()));
		date1 = date1.concat(dateTmp.getFullYear().toString());
		formFieldsNDI['dateDebutActivite_ndi'] = date1;
	}

	if (siege.entrepriseSocieteConstitueSansActivite == null) {
	formFieldsNDI['adresseEtablissement_voie_ndi']    = (activite.adresseExploitationDifferente ?
													((activite.adresseExploitation.adresseExploitationNumeroVoie != null ? activite.adresseExploitation.adresseExploitationNumeroVoie : '') + ' ' +
													(activite.adresseExploitation.adresseExploitationIndiceVoie != null ? activite.adresseExploitation.adresseExploitationIndiceVoie : '') + ' ' +
													(activite.adresseExploitation.adresseExploitationTypeVoie != null ? activite.adresseExploitation.adresseExploitationTypeVoie : '') + ' ' +
													(activite.adresseExploitation.adresseExploitationNomVoie != null ? activite.adresseExploitation.adresseExploitationNomVoie : '') + ' ' +
													(activite.adresseExploitation.adresseExploitationComplementVoie != null ? activite.adresseExploitation.adresseExploitationComplementVoie : '') + ' ' +
													(activite.adresseExploitation.adresseExploitationDistributionSpecialeVoie != null ? activite.adresseExploitation.adresseExploitationDistributionSpecialeVoie : '')) : 
													((siege.siegeAdresseNumeroVoie != null ? siege.siegeAdresseNumeroVoie : ' ') + ' ' +
													(siege.siegeAdresseIndiceVoie != null ? siege.siegeAdresseIndiceVoie : ' ') + ' ' +
													(siege.siegeAdresseTypeVoie != null ? siege.siegeAdresseTypeVoie : ' ') + ' ' +
													siege.siegeAdresseNomVoie + ' ' +
													(siege.siegeAdresseComplementVoie != null ? siege.siegeAdresseComplementVoie : '') + ' ' +
													(siege.siegeADresseDistributionSpeciale != null ? siege.siegeADresseDistributionSpeciale : '')));}
	else{
	formFieldsNDI['adresseEtablissement_voie_ndi']    = "";}
	
	if (siege.entrepriseSocieteConstitueSansActivite == null) {													
formFieldsNDI['adresseEtablissement_codePostal_ndi'] = (activite.adresseExploitationDifferente ? activite.adresseExploitation.adresseExploitationCodePostal  : (siege.siegeAdresseCodePostalEtranger != null ? siege.siegeAdresseCodePostalEtranger : siege.siegeAdresseCodePostal));
	}else {formFieldsNDI['adresseEtablissement_codePostal_ndi'] = '';}
	
	if (siege.entrepriseSocieteConstitueSansActivite == null) {	
formFieldsNDI['adresseEtablissement_commune_ndi'] = (activite.adresseExploitationDifferente ? activite.adresseExploitation.adresseExploitationCommune : (siege.siegeAdresseVille != null ? (siege.siegeAdresseVille + ' / ' + siege.siegeAdressePays) : (siege.siegeAdresseCommune + ' / ' + siege.siegeAdressePays)));
	} else {formFieldsNDI['adresseEtablissement_commune_ndi'] = '';}
formFieldsNDI['nomDomaineEtablissement_ndi']                    = activite.cadreNomExploitation.etablissementNomDomaine;
formFieldsNDI['suppressionNomDomaineEtablissement_ndi']         = false;
formFieldsNDI['remplacementNomDomaineEtablissement_ndi']        = false;
formFieldsNDI['declarationInitialeNomDomaineEtablissement_ndi'] = true;
}

// Cadre 4

if($m0Agricole.cadreDeclarationPMGroup.principaleActiviteObjetSocial.societeNomDomaine.size() > 0) {
   if(activite.informationActivite.etablissementDateDebutActivite != null and "" !== $m0Agricole.cadreDeclarationPMGroup.principaleActiviteObjetSocial.societeNomDomaine[0]) {
		var dateTmp = new Date(parseInt(activite.informationActivite.etablissementDateDebutActivite.getTimeInMillis()));
		var date = pad(dateTmp.getDate().toString());
		var month = dateTmp.getMonth() + 1;
		date = date.concat(pad(month.toString()));
		date = date.concat(dateTmp.getFullYear().toString());
       formFieldsNDI['dateEffetNomDomainePM_ndi'] = date;
   } else if (tag.formaliteSignatureDate != null and "" !== $m0Agricole.cadreDeclarationPMGroup.principaleActiviteObjetSocial.societeNomDomaine[0]) {
       var dateTmp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
       var date = pad(dateTmp.getDate().toString());
       var month = dateTmp.getMonth() + 1;
       date = date.concat(pad(month.toString()));
       date = date.concat(dateTmp.getFullYear().toString());
       formFieldsNDI['dateEffetNomDomainePM_ndi'] = date;
   }
   for (var i = 0; i < $m0Agricole.cadreDeclarationPMGroup.principaleActiviteObjetSocial.societeNomDomaine.size(); i++) {
       formFieldsNDI['nomDomainePM_ndi[' + i + ']'] = $m0Agricole.cadreDeclarationPMGroup.principaleActiviteObjetSocial.societeNomDomaine[i];
       formFieldsNDI['nouveauNomDomainePM_ndi[' + i + ']'] = $m0Agricole.cadreDeclarationPMGroup.principaleActiviteObjetSocial.societeNomDomaine[i] !== "" ? true : false
   }
}

/*
 * Création du dossier avec option fiscale : ajout du cerfa avec option fiscale
 */
var cerfaDoc1 = nash.doc //
    .load('models/cerfa_11927-05_M0_agricole.pdf') //
    .apply(formFields);

var cerfaDoc2 = nash.doc //
    .load('models/cerfa_11927-05_M0_agricole_sans_option_fiscale.pdf') //
    .apply(formFields);
cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));

//M0' Agricole
if ($m0Agricole.cadreDirigeantGroup.cadreDirigeant2.declarationDirigeant3 or $m0Agricole.cadreDeclarationPMGroup.cadreDeclarationPM.entrepriseFusionScission) {
    var cerfaDoc3 = nash.doc //
        .load('models/cerfa_14117-02_M0Prime_agricole_creation.pdf') //
        .apply(formFieldsPers1);
    cerfaDoc1.append(cerfaDoc3.save('cerfa.pdf'));
}

/* if ($m0Agricole.cadreDeclarationPMGroup.cadreDeclarationPM.entrepriseFusionScission) {
    var cerfaDoc2 = nash.doc //
        .load('models/cerfa_14117-02_M0Prime_agricole_creation.pdf') //
        .apply(formFields);
    cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));
} */
//NSm Agricole
//dirigeant 1
if (dirigeant1.cadreDirigeantNonSalarie1.voletSocialNumeroSecuriteSocialNSMA != null) {
    var cerfaDoc5 = nash.doc //
        .load('models/cerfa_11925-04_NSM.pdf') //
        .apply(formFieldsNSm1);
    cerfaDoc1.append(cerfaDoc5.save('cerfa.pdf'));
}

//dirigeant 2
if (dirigeant2.cadreDirigeantNonSalarie2.voletSocialNumeroSecuriteSocialNSMA != null) {
    var cerfaDoc5 = nash.doc //
        .load('models/cerfa_11925-04_NSM.pdf') //
        .apply(formFieldsNSm2);
    cerfaDoc1.append(cerfaDoc5.save('cerfa.pdf'));
}

//dirigeant 3
if (dirigeant3.cadreDirigeantNonSalarie3.voletSocialNumeroSecuriteSocialNSMA != null) {
    var cerfaDoc5 = nash.doc //
        .load('models/cerfa_11925-04_NSM.pdf') //
        .apply(formFieldsNSm3);
    cerfaDoc1.append(cerfaDoc5.save('cerfa.pdf'));
}

//dirigeant 4
if (dirigeant4.cadreDirigeantNonSalarie4.voletSocialNumeroSecuriteSocialNSMA != null) {
    var cerfaDoc5 = nash.doc //
        .load('models/cerfa_11925-04_NSM.pdf') //
        .apply(formFieldsNSm4);
    cerfaDoc1.append(cerfaDoc5.save('cerfa.pdf'));
}

//dirigeant 5
if (dirigeant5.cadreDirigeantNonSalarie5.voletSocialNumeroSecuriteSocialNSMA != null) {
    var cerfaDoc5 = nash.doc //
        .load('models/cerfa_11925-04_NSM.pdf') //
        .apply(formFieldsNSm5);
    cerfaDoc1.append(cerfaDoc5.save('cerfa.pdf'));
}

//dirigeant 6
if (dirigeant6.cadreDirigeantNonSalarie6.voletSocialNumeroSecuriteSocialNSMA != null) {
    var cerfaDoc5 = nash.doc //
        .load('models/cerfa_11925-04_NSM.pdf') //
        .apply(formFieldsNSm6);
    cerfaDoc1.append(cerfaDoc5.save('cerfa.pdf'));
}

//dirigeant 7
if (dirigeant7.cadreDirigeantNonSalarie7.voletSocialNumeroSecuriteSocialNSMA != null) {
    var cerfaDoc5 = nash.doc //
        .load('models/cerfa_11925-04_NSM.pdf') //
        .apply(formFieldsNSm7);
    cerfaDoc1.append(cerfaDoc5.save('cerfa.pdf'));
}

//dirigeant 8
if (dirigeant8.cadreDirigeantNonSalarie8.voletSocialNumeroSecuriteSocialNSMA != null) {
    var cerfaDoc5 = nash.doc //
        .load('models/cerfa_11925-04_NSM.pdf') //
        .apply(formFieldsNSm8);
    cerfaDoc1.append(cerfaDoc5.save('cerfa.pdf'));
}

//dirigeant 9
if (dirigeant9.cadreDirigeantNonSalarie9.voletSocialNumeroSecuriteSocialNSMA != null) {
    var cerfaDoc5 = nash.doc //
        .load('models/cerfa_11925-04_NSM.pdf') //
        .apply(formFieldsNSm9);
    cerfaDoc1.append(cerfaDoc5.save('cerfa.pdf'));
}

//dirigeant 10
if (dirigeant10.cadreDirigeantNonSalarie10.voletSocialNumeroSecuriteSocialNSMA != null) {
    var cerfaDoc5 = nash.doc //
        .load('models/cerfa_11925-04_NSM.pdf') //
        .apply(formFieldsNSm10);
    cerfaDoc1.append(cerfaDoc5.save('cerfa.pdf'));
}

//NSp Agricole
//Dirigeant 1
if (Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant1.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECOUINSMA')) {
    var cerfaDoc6 = nash.doc //
        .load('models/cerfa_11926-04_NSP.pdf') //
        .apply(formFieldsNSp1);
    cerfaDoc1.append(cerfaDoc6.save('cerfa.pdf'));
}

//Dirigeant 2
if (Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant2.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECOUINSMA')) {
    var cerfaDoc6 = nash.doc //
        .load('models/cerfa_11926-04_NSP.pdf') //
        .apply(formFieldsNSp2);
    cerfaDoc1.append(cerfaDoc6.save('cerfa.pdf'));
}

//Dirigeant 3
if (Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant3.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECOUINSMA')) {
    var cerfaDoc6 = nash.doc //
        .load('models/cerfa_11926-04_NSP.pdf') //
        .apply(formFieldsNSp3);
    cerfaDoc1.append(cerfaDoc6.save('cerfa.pdf'));
}

//Dirigeant 4
if (Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant4.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECOUINSMA')) {
    var cerfaDoc6 = nash.doc //
        .load('models/cerfa_11926-04_NSP.pdf') //
        .apply(formFieldsNSp4);
    cerfaDoc1.append(cerfaDoc6.save('cerfa.pdf'));
}

//Dirigeant 5
if (Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant5.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECOUINSMA')) {
    var cerfaDoc6 = nash.doc //
        .load('models/cerfa_11926-04_NSP.pdf') //
        .apply(formFieldsNSp5);
    cerfaDoc1.append(cerfaDoc6.save('cerfa.pdf'));
}

//Dirigeant 6
if (Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant6.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECOUINSMA')) {
    var cerfaDoc6 = nash.doc //
        .load('models/cerfa_11926-04_NSP.pdf') //
        .apply(formFieldsNSp6);
    cerfaDoc1.append(cerfaDoc6.save('cerfa.pdf'));
}

//Dirigeant 7
if (Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant7.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECOUINSMA')) {
    var cerfaDoc6 = nash.doc //
        .load('models/cerfa_11926-04_NSP.pdf') //
        .apply(formFieldsNSp7);
    cerfaDoc1.append(cerfaDoc6.save('cerfa.pdf'));
}

//Dirigeant 8
if (Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant8.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECOUINSMA')) {
    var cerfaDoc6 = nash.doc //
        .load('models/cerfa_11926-04_NSP.pdf') //
        .apply(formFieldsNSp8);
    cerfaDoc1.append(cerfaDoc6.save('cerfa.pdf'));
}

//Dirigeant 9
if (Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant9.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECOUINSMA')) {
    var cerfaDoc6 = nash.doc //
        .load('models/cerfa_11926-04_NSP.pdf') //
        .apply(formFieldsNSp9);
    cerfaDoc1.append(cerfaDoc6.save('cerfa.pdf'));
}

//Dirigeant 10
if (Value('id').of($m0Agricole.cadreDirigeantGroup.cadreDirigeant10.voletSocialMembreGAECOUINONNSMA).eq('voletSocialMembreGAECOUINSMA')) {
    var cerfaDoc6 = nash.doc //
        .load('models/cerfa_11926-04_NSP.pdf') //
        .apply(formFieldsNSp10);
    cerfaDoc1.append(cerfaDoc6.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire NDI Volet 1
 */
 
 if (activite.cadreNomExploitation.etablissementNomDomaine != null or ($m0Agricole.cadreDeclarationPMGroup.principaleActiviteObjetSocial.societeNomDomaine[0].length > 0)) {
	var ndiDoc1 = nash.doc //
		.load('models/cerfa_14943-01_NDI_volet1.pdf') //
		.apply (formFieldsNDI);
	cerfaDoc1.append(ndiDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire NDI Volet 2
 */
 
if (activite.cadreNomExploitation.etablissementNomDomaine != null or ($m0Agricole.cadreDeclarationPMGroup.principaleActiviteObjetSocial.societeNomDomaine[0].length > 0)) {
	var ndiDoc2 = nash.doc //
		.load('models/cerfa_14943-01_NDI_volet2.pdf') //
		.apply (formFieldsNDI);
	cerfaDoc1.append(ndiDoc2.save('cerfa.pdf'));
}
/*
 * Ajout des PJs
 */
var pjUser = [];

function pushPjPreview(fld) {
	fld.forEach(function (elm) {
        pjUser.push(elm);
    });
}

//Dirigeant Personne physique 1
if (Value('id').of(dirigeant1.personnaliteJuridique).eq('personnePhysique')) {
    if (not Value('id').of(dirigeant1.personneLieeQualite).eq('associeIndResp')) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant1);
    }
    if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and Value('id').of(tag.representantLegalNumero).eq('gerant1')) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant1Signataire);
    } else if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire')
        or(not Value('id').of(tag.representantLegalNumero).eq('gerant1')
            and Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal'))) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant1NonSignataire);
    }
}
//Dirigeant Personne physique 2

if (Value('id').of(dirigeant2.personnaliteJuridique).eq('personnePhysique')) {
    if (not Value('id').of(dirigeant2.personneLieeQualite).eq('associeIndResp')) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant2);
    }
    if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and Value('id').of(tag.representantLegalNumero).eq('gerant2')) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant2Signataire);
    } else if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire')
        or(not Value('id').of(tag.representantLegalNumero).eq('gerant2')
            and Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal'))) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant2NonSignataire);
    }
}

//Dirigeant Personne physique 3

if (Value('id').of(dirigeant3.personnaliteJuridique).eq('personnePhysique')) {
    if (not Value('id').of(dirigeant3.personneLieeQualite).eq('associeIndResp')) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant3);
    }
    if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and Value('id').of(tag.representantLegalNumero).eq('gerant3')) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant3Signataire);
    } else if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire')
        or(not Value('id').of(tag.representantLegalNumero).eq('gerant3')
            and Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal'))) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant3NonSignataire);
    }
}

//Dirigeant Personne physique 4

if (Value('id').of(dirigeant4.personnaliteJuridique).eq('personnePhysique')) {
    if (not Value('id').of(dirigeant4.personneLieeQualite).eq('associeIndResp')) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant4);
    }
    if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and Value('id').of(tag.representantLegalNumero).eq('gerant4')) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant4Signataire);
    } else if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire')
        or(not Value('id').of(tag.representantLegalNumero).eq('gerant4')
            and Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal'))) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant4NonSignataire);
    }
}

//Dirigeant Personne physique 5

if (Value('id').of(dirigeant5.personnaliteJuridique).eq('personnePhysique')) {
    if (not Value('id').of(dirigeant5.personneLieeQualite).eq('associeIndResp')) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant5);
    }
    if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and Value('id').of(tag.representantLegalNumero).eq('gerant5')) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant5Signataire);
    } else if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire')
        or(not Value('id').of(tag.representantLegalNumero).eq('gerant5')
            and Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal'))) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant5NonSignataire);
    }
}

//Dirigeant Personne physique 6

if (Value('id').of(dirigeant6.personnaliteJuridique).eq('personnePhysique')) {
    if (not Value('id').of(dirigeant6.personneLieeQualite).eq('associeIndResp')) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant6);
    }
    if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and Value('id').of(tag.representantLegalNumero).eq('gerant6')) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant6Signataire);
    } else if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire')
        or(not Value('id').of(tag.representantLegalNumero).eq('gerant6')
            and Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal'))) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant6NonSignataire);
    }
}

//Dirigeant Personne physique 7

if (Value('id').of(dirigeant7.personnaliteJuridique).eq('personnePhysique')) {
    if (not Value('id').of(dirigeant7.personneLieeQualite).eq('associeIndResp')) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant7);
    }
    if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and Value('id').of(tag.representantLegalNumero).eq('gerant7')) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant7Signataire);
    } else if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire')
        or(not Value('id').of(tag.representantLegalNumero).eq('gerant7')
            and Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal'))) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant7NonSignataire);
    }
}

//Dirigeant Personne physique 8

if (Value('id').of(dirigeant8.personnaliteJuridique).eq('personnePhysique')) {
    if (not Value('id').of(dirigeant8.personneLieeQualite).eq('associeIndResp')) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant8);
    }
    if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and Value('id').of(tag.representantLegalNumero).eq('gerant8')) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant8Signataire);
    } else if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire')
        or(not Value('id').of(tag.representantLegalNumero).eq('gerant8')
            and Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal'))) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant8NonSignataire);
    }
}

//Dirigeant Personne physique 9

if (Value('id').of(dirigeant9.personnaliteJuridique).eq('personnePhysique')) {
    if (not Value('id').of(dirigeant9.personneLieeQualite).eq('associeIndResp')) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant9);
    }
    if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and Value('id').of(tag.representantLegalNumero).eq('gerant9')) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant9Signataire);
    } else if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire')
        or(not Value('id').of(tag.representantLegalNumero).eq('gerant9')
            and Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal'))) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant9NonSignataire);
    }
}

//Dirigeant Personne physique 10

if (Value('id').of(dirigeant10.personnaliteJuridique).eq('personnePhysique')) {
    if (not Value('id').of(dirigeant10.personneLieeQualite).eq('associeIndResp')) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant10);
    }
    if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and Value('id').of(tag.representantLegalNumero).eq('gerant10')) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant10Signataire);
    } else if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire')
        or(not Value('id').of(tag.representantLegalNumero).eq('gerant10')
            and Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal'))) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant10NonSignataire);
    }
}

// PJ Dirigeant Personne Morale 1

if (Value('id').of(dirigeant1.personnaliteJuridique).eq('personneMorale')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS1);

    if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and(not dirigeant1.autreDirigeant
            or Value('id').of(tag.representantLegalNumero).eq('gerant1'))) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire1);
    }
}

// PJ Dirigeant Personne Morale 2

if (Value('id').of(dirigeant2.personnaliteJuridique).eq('personneMorale')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS2);

    if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and(not dirigeant2.autreDirigeant
            or Value('id').of(tag.representantLegalNumero).eq('gerant2'))) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire2);
    }
}

// PJ Dirigeant Personne Morale 3

if (Value('id').of(dirigeant3.personnaliteJuridique).eq('personneMorale')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS3);

    if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and(not dirigeant3.autreDirigeant
            or Value('id').of(tag.representantLegalNumero).eq('gerant3'))) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire3);
    }
}

// PJ Dirigeant Personne Morale 4

if (Value('id').of(dirigeant4.personnaliteJuridique).eq('personneMorale')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS4);

    if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and(not dirigeant4.autreDirigeant
            or Value('id').of(tag.representantLegalNumero).eq('gerant4'))) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire4);
    }
}

// PJ Dirigeant Personne Morale 5

if (Value('id').of(dirigeant5.personnaliteJuridique).eq('personneMorale')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS5);

    if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and(not dirigeant5.autreDirigeant
            or Value('id').of(tag.representantLegalNumero).eq('gerant5'))) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire5);
    }
}

// PJ Dirigeant Personne Morale 6

if (Value('id').of(dirigeant6.personnaliteJuridique).eq('personneMorale')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS6);

    if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and(not dirigeant6.autreDirigeant
            or Value('id').of(tag.representantLegalNumero).eq('gerant6'))) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire6);
    }
}

// PJ Dirigeant Personne Morale 7

if (Value('id').of(dirigeant7.personnaliteJuridique).eq('personneMorale')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS7);

    if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and(not dirigeant7.autreDirigeant
            or Value('id').of(tag.representantLegalNumero).eq('gerant7'))) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire7);
    }
}

// PJ Dirigeant Personne Morale 8

if (Value('id').of(dirigeant8.personnaliteJuridique).eq('personneMorale')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS8);

    if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and(not dirigeant8.autreDirigeant
            or Value('id').of(tag.representantLegalNumero).eq('gerant8'))) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire8);
    }
}

// PJ Dirigeant Personne Morale 9

if (Value('id').of(dirigeant9.personnaliteJuridique).eq('personneMorale')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS9);

    if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and(not dirigeant9.autreDirigeant
            or Value('id').of(tag.representantLegalNumero).eq('gerant9'))) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire9);
    }
}

// PJ Dirigeant Personne Morale 10

if (Value('id').of(dirigeant10.personnaliteJuridique).eq('personneMorale')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS10);

    if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and(not dirigeant10.autreDirigeant
            or Value('id').of(tag.representantLegalNumero).eq('gerant10'))) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire10);
    }
}

// Nomination dirigeants

if (not dirigeant1.statutPresenceDirigeant
    or($m0Agricole.cadreDirigeantGroup.cadreDirigeant1.declarationDirigeant2 and not dirigeant2.statutPresenceDirigeant)
    or($m0Agricole.cadreDirigeantGroup.cadreDirigeant2.declarationDirigeant3 and not dirigeant3.statutPresenceDirigeant)
    or($m0Agricole.cadreDirigeantGroup.cadreDirigeant3.declarationDirigeant4 and not dirigeant4.statutPresenceDirigeant)
    or($m0Agricole.cadreDirigeantGroup.cadreDirigeant4.declarationDirigeant5 and not dirigeant5.statutPresenceDirigeant)
    or($m0Agricole.cadreDirigeantGroup.cadreDirigeant5.declarationDirigeant6 and not dirigeant6.statutPresenceDirigeant)
    or($m0Agricole.cadreDirigeantGroup.cadreDirigeant6.declarationDirigeant7 and not dirigeant7.statutPresenceDirigeant)
    or($m0Agricole.cadreDirigeantGroup.cadreDirigeant7.declarationDirigeant8 and not dirigeant8.statutPresenceDirigeant)
    or($m0Agricole.cadreDirigeantGroup.cadreDirigeant8.declarationDirigeant9 and not dirigeant9.statutPresenceDirigeant)
    or($m0Agricole.cadreDirigeantGroup.cadreDirigeant9.declarationDirigeant10 and not dirigeant10.statutPresenceDirigeant)) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjNominationDirigeant);
}

// PJ Représentant Permanent 1

if (dirigeant1.civiliteRepresentant1.personneLieePPNomNaissance != null) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant1);
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDRepresentant1NonSignataire);
}

// PJ Représentant Permanent 2

if (dirigeant2.civiliteRepresentant2.personneLieePPNomNaissance != null) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant2);
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDRepresentant2NonSignataire);
}

// PJ Représentant Permanent 3

if (dirigeant3.civiliteRepresentant3.personneLieePPNomNaissance != null) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant3);
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDRepresentant3NonSignataire);
}

// PJ Représentant Permanent 4

if (dirigeant4.civiliteRepresentant4.personneLieePPNomNaissance != null) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant4);
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDRepresentant4NonSignataire);
}

// PJ Représentant Permanent 5

if (dirigeant5.civiliteRepresentant5.personneLieePPNomNaissance != null) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant5);
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDRepresentant5NonSignataire);
}

// PJ Représentant Permanent 6

if (dirigeant6.civiliteRepresentant6.personneLieePPNomNaissance != null) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant6);
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDRepresentant6NonSignataire);
}

// PJ Représentant Permanent 7

if (dirigeant7.civiliteRepresentant7.personneLieePPNomNaissance != null) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant7);
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDRepresentant7NonSignataire);
}

// PJ Représentant Permanent 8

if (dirigeant8.civiliteRepresentant8.personneLieePPNomNaissance != null) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant8);
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDRepresentant8NonSignataire);
}

// PJ Représentant Permanent 9

if (dirigeant9.civiliteRepresentant9.personneLieePPNomNaissance != null) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant9);
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDRepresentant9NonSignataire);
}

// PJ Représentant Permanent 10

if (dirigeant10.civiliteRepresentant10.personneLieePPNomNaissance != null) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant10);
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDRepresentant10NonSignataire);
}

// PJ Commissaire aux comptes
if (
    Value('id').of(dirigeant1.personneLieeQualite).eq('commissaireComptesTitulaire')
    or Value('id').of(dirigeant2.personneLieeQualite).eq('commissaireComptesTitulaire')
    or Value('id').of(dirigeant3.personneLieeQualite).eq('commissaireComptesTitulaire')
    or Value('id').of(dirigeant4.personneLieeQualite).eq('commissaireComptesTitulaire')
    or Value('id').of(dirigeant5.personneLieeQualite).eq('commissaireComptesTitulaire')
    or Value('id').of(dirigeant6.personneLieeQualite).eq('commissaireComptesTitulaire')
    or Value('id').of(dirigeant7.personneLieeQualite).eq('commissaireComptesTitulaire')
    or Value('id').of(dirigeant8.personneLieeQualite).eq('commissaireComptesTitulaire')
    or Value('id').of(dirigeant9.personneLieeQualite).eq('commissaireComptesTitulaire')
    or Value('id').of(dirigeant10.personneLieeQualite).eq('commissaireComptesTitulaire')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjJustificatifInscriptionCACTitulaire);
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjLettreAcceptationCACTitulaire);
}

// PJ Commissaire aux comptes suppléant
if (Value('id').of(dirigeant1.personneLieeQualite).eq('commissaireComptesSuppleant')
    or Value('id').of(dirigeant2.personneLieeQualite).eq('commissaireComptesSuppleant')
    or Value('id').of(dirigeant3.personneLieeQualite).eq('commissaireComptesSuppleant')
    or Value('id').of(dirigeant4.personneLieeQualite).eq('commissaireComptesSuppleant')
    or Value('id').of(dirigeant5.personneLieeQualite).eq('commissaireComptesSuppleant')
    or Value('id').of(dirigeant6.personneLieeQualite).eq('commissaireComptesSuppleant')
    or Value('id').of(dirigeant7.personneLieeQualite).eq('commissaireComptesSuppleant')
    or Value('id').of(dirigeant8.personneLieeQualite).eq('commissaireComptesSuppleant')
    or Value('id').of(dirigeant9.personneLieeQualite).eq('commissaireComptesSuppleant')
    or Value('id').of(dirigeant10.personneLieeQualite).eq('commissaireComptesSuppleant')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjJustificatifInscriptionCACSuppleant);
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjLettreAcceptationCACSuppleant);
}

// PJ Mandataire

if (Value('id').of(tag).eq('FormaliteSignataireQualiteMandataire')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPouvoir);
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire);
}
/*
 * Ajout des PJs
 */
 
 // PJ MBE

pushPjPreview($attachmentPreprocess.attachmentPreprocess.formulaireBE1);
if (not Value('id').of(tag.nombreMBE).eq('un')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.formulaireBE2);
}
if (not Value('id').of(tag.nombreMBE).eq('un') and not Value('id').of(tag.nombreMBE).eq('deux')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.formulaireBE3);
}
if (Value('id').of(tag.nombreMBE).eq('quatre') or Value('id').of(tag.nombreMBE).eq('cinq')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.formulaireBE4);
}
if (Value('id').of(tag.nombreMBE).eq('cinq')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.formulaireBE5);
}

// Identification de la société

if (Value('id').of(siege.siegeAdressePays).eq('FRXXXXX')) {
    if (not societe.entrepriseAssocieUniqueGerant and not Value('id').of(societe.entrepriseStatutType).eq('entrepriseStatutTypeSansModification')) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjStatutSigne);
        //sauf SARL dont l'associé est gérant unique adoptant les statuts types
    }
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjAttestationParutionLegales);
    if (Value('id').of(societe.entrepriseFormeJuridiqueCivile).eq('6598')
        and(not societe.entrepriseCapitalApportNumeraire or societe.entrepriseCapitalApport30000)) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjRapportCommissaire);
    }
    if (Value('id').of(societe.entrepriseFormeJuridiqueCivile).eq('6533')) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjAgrementPrefectoral);
    }
} else {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjStatutsEtr);
}

// Renseignements relatifs au siège de l'entreprise

if (Value('id').of(siege.questionDomiciliation).eq('locauxSociete')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjJustificatifLocaux);
}
if (Value('id').of(siege.questionDomiciliation).eq('locauxHabitation')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjJustificatifHabitation);
}
if (Value('id').of(siege.questionDomiciliation).eq('locauxEntrepriseDomiciliation')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjJustificatifDomiciliation);
}

/*
 * Enregistrement du fichier (en mémoire)
 */

var finalDoc = cerfaDoc1.save('M0_Agricole_Creation.pdf');

var data = [spec.createData({
        id: 'record',
        label: 'Déclaration de création d\'une société ayant une activité principale agricole',
        description: 'Voici le formulaire obtenu à partir des données saisies.',
        type: 'FileReadOnly',
        value: [finalDoc]
    }), spec.createData({
        id: 'attachments',
        label: 'Pièces jointes',
        description: 'Pièces jointes ajoutées au formulaire.',
        type: 'FileReadOnly',
        value: pjUser
    })];
var groups = [spec.createGroup({
        id: 'generated',
        label: 'Génération du dossier',
        data: data
    })];

return spec.create({
    id: 'review',
    label: 'Déclaration de création d\'une société ayant une activité principale agricole',
    groups: groups
});
