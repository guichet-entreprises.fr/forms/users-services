// prepare info to send

var societe = $m0Agricole.cadreDeclarationPMGroup.cadreDeclarationPM;
var adresse = $m0Agricole.cadreDeclarationPMGroup.siegeAdresseEntreprise;
var adresseEtab = $m0Agricole.cadreDeclarationExploitationActiviteGroup.cadreDeclarationExploitationActivite.adresseExploitation;
var algo = "trouver destinataire";
var secteur1 =  "AGRICOLE";
var secteur2 = '';
var typePersonne = "PM";
var formJuridique = Value('id').of(societe.entrepriseFormeJuridiqueCivile).eq('6598') 	? "EARL" : 
					Value('id').of(societe.entrepriseFormeJuridiqueCivile).eq('6533')	? "GAEC" : 
                    Value('id').of(societe.entrepriseFormeJuridiqueCivile).eq('6534') 	? "GFA"  :
					Value('id').of(societe.entrepriseFormeJuridiqueCivile).eq('6597')  ? "SCEA"  :
					Value('id').of(societe.entrepriseFormeJuridiqueCivile).eq('6597')  ? "Autre société civile "  :
				(	Value('id').of(societe.entrepriseFormeJuridiqueCommerciale).eq('5531') or
					Value('id').of(societe.entrepriseFormeJuridiqueCommerciale).eq('5532') or
					Value('id').of(societe.entrepriseFormeJuridiqueCommerciale).eq('5599') or
					Value('id').of(societe.entrepriseFormeJuridiqueCommerciale).eq('5699') ) ? "SA" :
					Value('id').of(societe.entrepriseFormeJuridiqueCommerciale).eq('5499') ? "SARL" : 
					Value('id').of(societe.entrepriseFormeJuridiqueCommerciale).eq('5710') ? "SAS": "SNC";

var optionCMACCI = "NON";
var formalityWithPayment = false;
var listFraisDestinataires=[];
var listDistinctAuthorities =[];
var destFuncId2 = '';		 
var codeEDI2 = '';
var partnerLabel2 = '';
var codeCommune = '';
if (not Value('id').of(adresse.siegeAdressePays).eq('FRXXXXX')) {
codeCommune = adresseEtab.adresseExploitationCommune.getId();
} else { codeCommune = adresse.siegeAdresseCommune.getId(); }
_log.info("codeCommune is {}",codeCommune);


var attachement = "/3-review/generated/generated.record-1-M0_Agricole_Creation.pdf";

return spec.create({
	id : 'prepareSend',
	label : "Préparation de la recherche du destinataire",
	groups : [ spec.createGroup({
		id : 'view',
		label : "Informations",
		data : [ spec.createData({
			id : 'algo',
			label : "Algo",
			type : 'String',
			mandatory : true,
			value : algo
		}), spec.createData({
			id : 'secteur1',
			label : "Secteur",
			type : 'String',
			value : secteur1
		}), spec.createData({
			id : 'typePersonne',
			label : "Type personne",
			type : 'String',
			value : typePersonne
		}), spec.createData({
			id : 'formJuridique',
			label : "Forme juridique",
			type : 'String',
			value : formJuridique
		}), spec.createData({
			id : 'optionCMACCI',
			label : "Option CMACCI",
			type : 'String',
			value : optionCMACCI
		}), spec.createData({
			id : 'codeCommune',
			label : "Code commune",
			type : 'String',
			value : codeCommune
		}), spec.createData({
			id : 'attachement',
			label : "Pièce jointe",
			type : 'String',
			value : attachement
		}) ]
	}) ]
});