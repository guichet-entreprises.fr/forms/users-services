//Fichier de mapping des champs Regent pour :
//Version : V2016.2
//Evenements : 01M, 02M, 04M
//Fichier généré le : 2019-12-10


function pad(s)
{
    return (s < 10) ? '0' + s : s;
}
var societe = $m0Agricole.cadreDeclarationPMGroup.cadreDeclarationPM;
var siege = $m0Agricole.cadreDeclarationPMGroup.siegeAdresseEntreprise;
var regentVersion = "V2016.02"; // (V2008.11, V2016.02)
var eventRegent = societe.entrepriseSocieteConstitueSansActivite != null ? "02M" : (societe.entrepriseSocieteCommercialeEtrangereOuverturePremierEtab != null ? "04M" : "01M");
var authorityType = "CFE"; // (CFE, TDR)
var authorityId = _INPUT_.dataGroup.codeEDI; // (code EDI)
//Ajout du type de la deuxième autorité
var authorityType2 = "TDR"; // (CFE, TDR)
var authorityId2 = _INPUT_.dataGroup.codeEDI2; // (code EDI)
var regEx = new RegExp('[0-9]{5}');
var exploitation = $m0Agricole.cadreDeclarationExploitationActiviteGroup.cadreDeclarationExploitationActivite;
var tag = $m0Agricole.cadre21SignatureGroup.cadre21Signature;
var correspondance = $m0Agricole.cadre20RensCompGroup.cadre20RensComp;
var etablissement = $m0Agricole.cadreDeclarationExploitationActiviteGroup.cadreDeclarationExploitationActivite;
var dirigeant1 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant1.personalite1;
var dirigeant2 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant2.personalite2;
var dirigeant3 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant3.personalite3;
var dirigeant4 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant4.personalite4;
var dirigeant5 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant5.personalite5;
var dirigeant6 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant6.personalite6;
var dirigeant7 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant7.personalite7;
var dirigeant8 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant8.personalite8;
var dirigeant9 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant9.personalite9;
var dirigeant10 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant10.personalite10;
var infoCpt = $m0Agricole.cadreComplementSocieteCommercialeGroup.cadreInfosCommunes;
var infoSte = $m0Agricole.cadreComplementSocieteCommercialeGroup.cadreUniquementSarl;
var infoSteEtrangere = $m0Agricole.cadreComplementSocieteCommercialeGroup.cadreSocieteEtrangere.cadreSocieteEtrangereRegistre;
var infoEtabEU = $m0Agricole.cadreComplementSocieteCommercialeGroup.cadreSocieteEtrangere.cadreSocieteEtrangereAutreEtab;
/* var infoEtabEU2= $m0Agricole.cadreComplementSocieteCommercialeGroup.cadreSocieteEtrangere.cadreSocieteEtrangereAutreEtab; */
var fusion1 = $m0Agricole.cadreDeclarationPMGroup.cadreDeclarationPM.cadreFusionScission;
var fusion2 = $m0Agricole.cadreDeclarationPMGroup.cadreDeclarationPM.cadreFusionScission2;
var fusion3 = $m0Agricole.cadreDeclarationPMGroup.cadreDeclarationPM.cadreFusionScission3;
var activite = $m0Agricole.cadreDeclarationExploitationActiviteGroup.cadreDeclarationExploitationActivite.informationActivite;
var exploitation = $m0Agricole.cadreDeclarationExploitationActiviteGroup.cadreDeclarationExploitationActivite;
var salarie = $m0Agricole.cadreDeclarationExploitationActiviteGroup.cadreDeclarationExploitationActivite.cadreEffectifSalarie;
var fiscal = $m0Agricole.cadreOptionsFiscalesGroup.cadreOptionsFiscales;
//

/* var origine = $m0SAS.cadre4OrigineFondsGroup.cadre4OrigineFonds;
var socialDirigeant1 = $m0SAS.dirigeant1.cadreIdentiteDirigeant.cadre7DeclarationSociale;
var socialDirigeant2 = $m0SAS.dirigeant2.cadreIdentiteDirigeant.cadre7DeclarationSociale;
var socialDirigeant3 = $m0SAS.dirigeant3.cadreIdentiteDirigeant.cadre7DeclarationSociale;
var socialDirigeant4 = $m0SAS.dirigeant4.cadreIdentiteDirigeant.cadre7DeclarationSociale;
var socialDirigeant5 = $m0SAS.dirigeant5.cadreIdentiteDirigeant.cadre7DeclarationSociale;
var socialDirigeant6 = $m0SAS.dirigeant6.cadreIdentiteDirigeant.cadre7DeclarationSociale;
var socialDirigeant7 = $m0SAS.dirigeant7.cadreIdentiteDirigeant.cadre7DeclarationSociale;
var socialDirigeant8 = $m0SAS.dirigeant8.cadreIdentiteDirigeant.cadre7DeclarationSociale;
var socialDirigeant9 = $m0SAS.dirigeant9.cadreIdentiteDirigeant.cadre7DeclarationSociale;
var socialDirigeant10 = $m0SAS.dirigeant10.cadreIdentiteDirigeant.cadre7DeclarationSociale;
var socialDirigeant11 = $m0SAS.dirigeant11.cadreIdentiteDirigeant.cadre7DeclarationSociale;
var socialDirigeant12 = $m0SAS.dirigeant12.cadreIdentiteDirigeant.cadre7DeclarationSociale;
var socialDirigeant13 = $m0SAS.dirigeant13.cadreIdentiteDirigeant.cadre7DeclarationSociale;
var socialDirigeant14 = $m0SAS.dirigeant14.cadreIdentiteDirigeant.cadre7DeclarationSociale;
 */
var regentFields = {};

// A mettre toujours sous "Z1611"
regentFields['/REGENT-XML/Emetteur'] = "Z1611";

regentFields['/REGENT-XML/Destinataire'] = authorityId;

// Complété par destiny
regentFields['/REGENT-XML/DateHeureEmission'] = null;
regentFields['/REGENT-XML/VersionMessage'] = null;
regentFields['/REGENT-XML/VersionNorme'] = null;
regentFields['/REGENT-XML/ServiceApplicatif/Specification/NomService'] = null;
regentFields['/REGENT-XML/ServiceApplicatif/Specification/VersionService'] = null;

// Groupe GDF : Groupe données de service

// Sous groupe IDF : Identification de la formalité
// Généré par destiny
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C01'] = "M0AGR";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02'] = null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C03'] = "0";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C04'] = null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05'] = "A";
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C06'] = null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C07'] = "O"; //Si déclaration de l’entreprise saisie par voie électronique (R123-20) 

// Sous groupe EDF : Evènement déclaré
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.1'] = societe.entrepriseSocieteConstitueSansActivite != false ? "02M" : (societe.entrepriseSocieteCommercialeEtrangereOuverturePremierEtab != false ? "04M" : "01M");
if (exploitation.informationActivite.etablissementDateDebutActivite != null)
{
    var dateTemp = new Date(parseInt(exploitation.informationActivite.etablissementDateDebutActivite.getTimeInMillis()));
    var year = dateTemp.getFullYear();
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
    var date = year + "-" + month + "-" + day;
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2'] = date;
}
else if (tag.formaliteSignatureDate != null)
{
    var dateTemp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
    var year = dateTemp.getFullYear();
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
    var date = year + "-" + month + "-" + day;
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2'] = date;
}

// Sous groupe DMF : Destinataire de la formalité

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[1]'] = authorityId;
if (authorityId != "" and authorityId2 != null)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[2]'] = authorityId2;
}

// Sous groupe ADF : Adresse de correspondance

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C36'] = (Value('id').of(correspondance.adresseCorrespond).eq('siege') or Value('id').of(correspondance.adresseCorrespond).eq('exploitation')) ? societe.entrepriseDenomination : correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance;

if (Value('id').of(correspondance.adresseCorrespond).eq('siege') and siege.siegeAdresseCommune != null)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3'] = siege.siegeAdresseCommune.getId();
} 
else if (Value('id').of(correspondance.adresseCorrespond).eq('siege') and siege.siegeAdresseVille != null)
{
    var idPays = siege.siegeAdressePays.getId();
    var result = idPays.match(regEx)[0];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3'] = result;
}
else if (Value('id').of(correspondance.adresseCorrespond).eq('exploitation'))
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3'] = exploitation.adresseExploitation.adresseExploitationCommune.getId();
}
else if (Value('id').of(correspondance.adresseCorrespond).eq('autre'))
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3'] = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune.getId();
}

if ((Value('id').of(correspondance.adresseCorrespond).eq('siege') and siege.siegeAdresseNumeroVoie != null)
    or(Value('id').of(correspondance.adresseCorrespond).eq('exploitation')
        and exploitation.adresseExploitation.adresseExploitationNumeroVoie != null)
    or(Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance != null))
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.5'] = Value('id').of(correspondance.adresseCorrespond).eq('siege') ? siege.siegeAdresseNumeroVoie : (Value('id').of(correspondance.adresseCorrespond).eq('exploitation') ? exploitation.adresseExploitation.adresseExploitationNumeroVoie : correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance);
}

if (Value('id').of(correspondance.adresseCorrespond).eq('siege') and siege.siegeAdresseIndiceVoie != null)
{
    var monId1 = Value('id').of(siege.siegeAdresseIndiceVoie)._eval();
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6'] = monId1;
}
else if (Value('id').of(correspondance.adresseCorrespond).eq('exploitation') and exploitation.adresseExploitation.adresseExploitationIndiceVoie != null)
{
    var monId2 = Value('id').of(exploitation.adresseExploitation.adresseExploitationIndiceVoie)._eval();
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6'] = monId2;
}
else if (correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance != null)
{
    var monId3 = Value('id').of(correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance)._eval();
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6'] = monId3;
}

if ((Value('id').of(correspondance.adresseCorrespond).eq('siege')and siege.siegeADresseDistributionSpeciale != null)
    or(Value('id').of(correspondance.adresseCorrespond).eq('exploitation')and exploitation.adresseExploitation.adresseExploitationDistributionSpecialeVoie != null)or(Value('id').of(correspondance.adresseCorrespond).eq('autre')and correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance != null))
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.7'] = Value('id').of(correspondance.adresseCorrespond).eq('siege') ? siege.siegeADresseDistributionSpeciale : (Value('id').of(correspondance.adresseCorrespond).eq('exploitation') ? exploitation.adresseExploitation.adresseExploitationDistributionSpecialeVoie :
            correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance);
}

if (Value('id').of(correspondance.adresseCorrespond).eq('siege')or(Value('id').of(correspondance.adresseCorrespond).eq('autre')and correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal != null)or(Value('id').of(correspondance.adresseCorrespond).eq('exploitation')and exploitation.adresseExploitation.adresseExploitationCodePostal != null))
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8'] = Value('id').of(correspondance.adresseCorrespond).eq('siege') ? (siege.codePostalAdresseSiege != null ? siege.siegeAdresseCodePostal : ".") :
        (Value('id').of(correspondance.adresseCorrespond).eq('exploitation') ? exploitation.adresseExploitation.adresseExploitationCodePostal :
            correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal);
}

if ((Value('id').of(correspondance.adresseCorrespond).eq('siege')and siege.siegeAdresseComplementVoie != null)or(Value('id').of(correspondance.adresseCorrespond).eq('exploitation')and exploitation.adresseExploitation.adresseExploitationComplementVoie != null)or(Value('id').of(correspondance.adresseCorrespond).eq('autre')and correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance != null))
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.10'] = Value('id').of(correspondance.adresseCorrespond).eq('siege') ? siege.siegeAdresseComplementVoie : (Value('id').of(correspondance.adresseCorrespond).eq('exploitation') ? exploitation.adresseExploitation.adresseExploitationComplementVoie : correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance);
}

if (Value('id').of(correspondance.adresseCorrespond).eq('siege')and siege.siegeAdresseTypeVoie != null)
{
    var monId1 = Value('id').of(siege.siegeAdresseTypeVoie)._eval();
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11'] = monId1;
}
else if (Value('id').of(correspondance.adresseCorrespond).eq('exploitation')and exploitation.adresseExploitation.adresseExploitationTypeVoie != null)
{
    var monId2 = Value('id').of(exploitation.adresseExploitation.adresseExploitationTypeVoie)._eval();
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11'] = monId2;
}
else if (correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance != null)
{
    var monId3 = Value('id').of(correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance)._eval();
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11'] = monId3;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12'] = Value('id').of(correspondance.adresseCorrespond).eq('siege') ? siege.siegeAdresseNomVoie : (Value('id').of(correspondance.adresseCorrespond).eq('exploitation') ? exploitation.adresseExploitation.adresseExploitationNomVoie : correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance);

if (Value('id').of(correspondance.adresseCorrespond).eq('siege')and siege.siegeAdresseCommune != null)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13'] = siege.siegeAdresseCommune.getLabel();
}
else if (Value('id').of(correspondance.adresseCorrespond).eq('siege') and siege.siegeAdresseVille != null)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13'] = siege.siegeAdresseVille;
}
else if (Value('id').of(correspondance.adresseCorrespond).eq('exploitation'))
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13'] = exploitation.adresseExploitation.adresseExploitationCommune.getLabel();
}
else if (Value('id').of(correspondance.adresseCorrespond).eq('autre'))
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13'] = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune.getLabel();
}

if (Value('id').of(correspondance.adresseCorrespond).eq('siege')and siege.siegeAdresseVille != null)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.14'] = siege.siegeAdressePays.getLabel();
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.1[1]'] = correspondance.infosSup.formaliteTelephone2.e164.replace('+', '00');

if (correspondance.infosSup.formaliteTelephone1 != null)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.1[2]'] = correspondance.infosSup.formaliteTelephone1.e164.replace('+', '00');
}

if (correspondance.infosSup.telecopie != null)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.2'] = correspondance.infosSup.telecopie.e164.replace('+', '00');
}

if (correspondance.infosSup.formaliteFaxCourriel != null)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.3'] = correspondance.infosSup.formaliteFaxCourriel;
}

// Sous groupe SIF : Signature de la formalité

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.1'] = Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire') ?
    tag.adresseMandataire.nomPrenomDenominationMandataire :
    ((Value('id').of(tag.representantLegalNumero).eq('gerant1')or not dirigeant1.declarationDirigeant2) ?
        (dirigeant1.civilitePersonnePhysique1.personneLieePPNomUsage != null ?
            (dirigeant1.civilitePersonnePhysique1.personneLieePPNomUsage + ' ' + dirigeant1.civilitePersonnePhysique1.personneLieePPPrenom[0]) :
            (dirigeant1.civilitePersonnePhysique1.personneLieePPNomNaissance != null ?
                (dirigeant1.civilitePersonnePhysique1.personneLieePPNomNaissance + ' ' + dirigeant1.civilitePersonnePhysique1.personneLieePPPrenom[0]) :
                (dirigeant1.civiliteRepresentant1.personneLieePPNomUsage != null ?
                    (dirigeant1.civiliteRepresentant1.personneLieePPNomUsage + ' ' + dirigeant1.civiliteRepresentant1.personneLieePPPrenom[0]) :
                    (dirigeant1.civiliteRepresentant1.personneLieePPNomNaissance != null ?
                        (dirigeant1.civiliteRepresentant1.personneLieePPNomNaissance + ' ' + dirigeant1.civiliteRepresentant1.personneLieePPPrenom[0]) :
                        dirigeant1.civilitePersonneMorale1.personneLieePMDenomination)))) :
        (Value('id').of(tag.representantLegalNumero).eq('gerant2') ?
            (dirigeant2.civilitePersonnePhysique2.personneLieePPNomUsage != null ?
                (dirigeant2.civilitePersonnePhysique2.personneLieePPNomUsage + ' ' + dirigeant2.civilitePersonnePhysique2.personneLieePPPrenom[0]) :
                (dirigeant2.civilitePersonnePhysique2.personneLieePPNomNaissance != null ?
                    (dirigeant2.civilitePersonnePhysique2.personneLieePPNomNaissance + ' ' + dirigeant2.civilitePersonnePhysique2.personneLieePPPrenom[0]) :
                    (dirigeant2.civiliteRepresentant2.personneLieePPNomUsage != null ?
                        (dirigeant2.civiliteRepresentant2.personneLieePPNomUsage + ' ' + dirigeant2.civiliteRepresentant2.personneLieePPPrenom[0]) :
                        (dirigeant2.civiliteRepresentant2.personneLieePPNomNaissance != null ?
                            (dirigeant2.civiliteRepresentant2.personneLieePPNomNaissance + ' ' + dirigeant2.civiliteRepresentant2.personneLieePPPrenom[0]) :
                            dirigeant2.civilitePersonneMorale2.personneLieePMDenomination)))) :
            (Value('id').of(tag.representantLegalNumero).eq('gerant3') ?
                (dirigeant3.civilitePersonnePhysique3.personneLieePPNomUsage != null ?
                    (dirigeant3.civilitePersonnePhysique3.personneLieePPNomUsage + ' ' + dirigeant3.civilitePersonnePhysique3.personneLieePPPrenom[0]) :
                    (dirigeant3.civilitePersonnePhysique3.personneLieePPNomNaissance != null ?
                        (dirigeant3.civilitePersonnePhysique3.personneLieePPNomNaissance + ' ' + dirigeant3.civilitePersonnePhysique3.personneLieePPPrenom[0]) :
                        (dirigeant3.civiliteRepresentant3.personneLieePPNomUsage != null ?
                            (dirigeant3.civiliteRepresentant3.personneLieePPNomUsage + ' ' + dirigeant3.civiliteRepresentant3.personneLieePPPrenom[0]) :
                            (dirigeant3.civiliteRepresentant3.personneLieePPNomNaissance != null ?
                                (dirigeant3.civiliteRepresentant3.personneLieePPNomNaissance + ' ' + dirigeant3.civiliteRepresentant3.personneLieePPPrenom[0]) :
                                dirigeant3.civilitePersonneMorale3.personneLieePMDenomination)))) :
                (Value('id').of(tag.representantLegalNumero).eq('gerant4') ?
                    (dirigeant4.civilitePersonnePhysique4.personneLieePPNomUsage != null ?
                        (dirigeant4.civilitePersonnePhysique4.personneLieePPNomUsage + ' ' + dirigeant4.civilitePersonnePhysique4.personneLieePPPrenom[0]) :
                        (dirigeant4.civilitePersonnePhysique4.personneLieePPNomNaissance != null ?
                            (dirigeant4.civilitePersonnePhysique4.personneLieePPNomNaissance + ' ' + dirigeant4.civilitePersonnePhysique4.personneLieePPPrenom[0]) :
                            (dirigeant4.civiliteRepresentant4.personneLieePPNomUsage != null ?
                                (dirigeant4.civiliteRepresentant4.personneLieePPNomUsage + ' ' + dirigeant4.civiliteRepresentant4.personneLieePPPrenom[0]) :
                                (dirigeant4.civiliteRepresentant4.personneLieePPNomNaissance != null ?
                                    (dirigeant4.civiliteRepresentant4.personneLieePPNomNaissance + ' ' + dirigeant4.civiliteRepresentant4.personneLieePPPrenom[0]) :
                                    dirigeant4.civilitePersonneMorale4.personneLieePMDenomination)))) :
                    (Value('id').of(tag.representantLegalNumero).eq('gerant5') ?
                        (dirigeant5.civilitePersonnePhysique5.personneLieePPNomUsage != null ?
                            (dirigeant5.civilitePersonnePhysique5.personneLieePPNomUsage + ' ' + dirigeant5.civilitePersonnePhysique5.personneLieePPPrenom[0]) :
                            (dirigeant5.civilitePersonnePhysique5.personneLieePPNomNaissance != null ?
                                (dirigeant5.civilitePersonnePhysique5.personneLieePPNomNaissance + ' ' + dirigeant5.civilitePersonnePhysique5.personneLieePPPrenom[0]) :
                                (dirigeant5.civiliteRepresentant5.personneLieePPNomUsage != null ?
                                    (dirigeant5.civiliteRepresentant5.personneLieePPNomUsage + ' ' + dirigeant5.civiliteRepresentant5.personneLieePPPrenom[0]) :
                                    (dirigeant5.civiliteRepresentant5.personneLieePPNomNaissance != null ?
                                        (dirigeant5.civiliteRepresentant5.personneLieePPNomNaissance + ' ' + dirigeant5.civiliteRepresentant5.personneLieePPPrenom[0]) :
                                        dirigeant5.civilitePersonneMorale5.personneLieePMDenomination)))) :
                        (Value('id').of(tag.representantLegalNumero).eq('gerant6') ?
                            (dirigeant6.civilitePersonnePhysique6.personneLieePPNomUsage != null ?
                                (dirigeant6.civilitePersonnePhysique6.personneLieePPNomUsage + ' ' + dirigeant6.civilitePersonnePhysique6.personneLieePPPrenom[0]) :
                                (dirigeant6.civilitePersonnePhysique6.personneLieePPNomNaissance != null ?
                                    (dirigeant6.civilitePersonnePhysique6.personneLieePPNomNaissance + ' ' + dirigeant6.civilitePersonnePhysique6.personneLieePPPrenom[0]) :
                                    (dirigeant6.civiliteRepresentant6.personneLieePPNomUsage != null ?
                                        (dirigeant6.civiliteRepresentant6.personneLieePPNomUsage + ' ' + dirigeant6.civiliteRepresentant6.personneLieePPPrenom[0]) :
                                        (dirigeant6.civiliteRepresentant6.personneLieePPNomNaissance != null ?
                                            (dirigeant6.civiliteRepresentant6.personneLieePPNomNaissance + ' ' + dirigeant6.civiliteRepresentant6.personneLieePPPrenom[0]) :
                                            dirigeant6.civilitePersonneMorale6.personneLieePMDenomination)))) :
                            (Value('id').of(tag.representantLegalNumero).eq('gerant7') ?
                                (dirigeant7.civilitePersonnePhysique7.personneLieePPNomUsage != null ?
                                    (dirigeant7.civilitePersonnePhysique7.personneLieePPNomUsage + ' ' + dirigeant7.civilitePersonnePhysique7.personneLieePPPrenom[0]) :
                                    (dirigeant7.civilitePersonnePhysique7.personneLieePPNomNaissance != null ?
                                        (dirigeant7.civilitePersonnePhysique7.personneLieePPNomNaissance + ' ' + dirigeant7.civilitePersonnePhysique7.personneLieePPPrenom[0]) :
                                        (dirigeant7.civiliteRepresentant7.personneLieePPNomUsage != null ?
                                            (dirigeant7.civiliteRepresentant7.personneLieePPNomUsage + ' ' + dirigeant7.civiliteRepresentant7.personneLieePPPrenom[0]) :
                                            (dirigeant7.civiliteRepresentant7.personneLieePPNomNaissance != null ?
                                                (dirigeant7.civiliteRepresentant7.personneLieePPNomNaissance + ' ' + dirigeant7.civiliteRepresentant7.personneLieePPPrenom[0]) :
                                                dirigeant7.civilitePersonneMorale7.personneLieePMDenomination)))) :
                                (Value('id').of(tag.representantLegalNumero).eq('gerant8') ?
                                    (dirigeant8.civilitePersonnePhysique8.personneLieePPNomUsage != null ?
                                        (dirigeant8.civilitePersonnePhysique8.personneLieePPNomUsage + ' ' + dirigeant8.civilitePersonnePhysique8.personneLieePPPrenom[0]) :
                                        (dirigeant8.civilitePersonnePhysique8.personneLieePPNomNaissance != null ?
                                            (dirigeant8.civilitePersonnePhysique8.personneLieePPNomNaissance + ' ' + dirigeant8.civilitePersonnePhysique8.personneLieePPPrenom[0]) :
                                            (dirigeant8.civiliteRepresentant8.personneLieePPNomUsage != null ?
                                                (dirigeant8.civiliteRepresentant8.personneLieePPNomUsage + ' ' + dirigeant8.civiliteRepresentant8.personneLieePPPrenom[0]) :
                                                (dirigeant8.civiliteRepresentant8.personneLieePPNomNaissance != null ?
                                                    (dirigeant8.civiliteRepresentant8.personneLieePPNomNaissance + ' ' + dirigeant8.civiliteRepresentant8.personneLieePPPrenom[0]) :
                                                    dirigeant8.civilitePersonneMorale8.personneLieePMDenomination)))) :
                                    (Value('id').of(tag.representantLegalNumero).eq('gerant9') ?
                                        (dirigeant9.civilitePersonnePhysique9.personneLieePPNomUsage != null ?
                                            (dirigeant9.civilitePersonnePhysique9.personneLieePPNomUsage + ' ' + dirigeant9.civilitePersonnePhysique9.personneLieePPPrenom[0]) :
                                            (dirigeant9.civilitePersonnePhysique9.personneLieePPNomNaissance != null ?
                                                (dirigeant9.civilitePersonnePhysique9.personneLieePPNomNaissance + ' ' + dirigeant9.civilitePersonnePhysique9.personneLieePPPrenom[0]) :
                                                (dirigeant9.civiliteRepresentant9.personneLieePPNomUsage != null ?
                                                    (dirigeant9.civiliteRepresentant9.personneLieePPNomUsage + ' ' + dirigeant9.civiliteRepresentant9.personneLieePPPrenom[0]) :
                                                    (dirigeant9.civiliteRepresentant9.personneLieePPNomNaissance != null ?
                                                        (dirigeant9.civiliteRepresentant9.personneLieePPNomNaissance + ' ' + dirigeant9.civiliteRepresentant9.personneLieePPPrenom[0]) :
                                                        dirigeant9.civilitePersonneMorale9.personneLieePMDenomination)))) :
                                        (Value('id').of(tag.representantLegalNumero).eq('gerant10') ?
                                            (dirigeant10.civilitePersonnePhysique10.personneLieePPNomUsage != null ?
                                                (dirigeant10.civilitePersonnePhysique10.personneLieePPNomUsage + ' ' + dirigeant10.civilitePersonnePhysique10.personneLieePPPrenom[0]) :
                                                (dirigeant10.civilitePersonnePhysique10.personneLieePPNomNaissance != null ?
                                                    (dirigeant10.civilitePersonnePhysique10.personneLieePPNomNaissance + ' ' + dirigeant10.civilitePersonnePhysique10.personneLieePPPrenom[0]) :
                                                    (dirigeant10.civiliteRepresentant10.personneLieePPNomUsage != null ?
                                                        (dirigeant10.civiliteRepresentant10.personneLieePPNomUsage + ' ' + dirigeant10.civiliteRepresentant10.personneLieePPPrenom[0]) :
                                                        (dirigeant10.civiliteRepresentant10.personneLieePPNomNaissance != null ?
                                                            (dirigeant10.civiliteRepresentant10.personneLieePPNomNaissance + ' ' + dirigeant10.civiliteRepresentant10.personneLieePPPrenom[0]) :
                                                            dirigeant10.civilitePersonneMorale10.personneLieePMDenomination)))) : ''))))))))));

if (Value('id').of(tag.soussigne).eq('formaliteSignataireQualiteMandataire'))
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.2'] = "Mandataire";
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.3'] = tag.adresseMandataire.villeAdresseMandataire.getId();
    if (tag.adresseMandataire.numeroVoieMandataire != null)
    {
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.5'] = tag.adresseMandataire.numeroVoieMandataire;
    }
    if (tag.adresseMandataire.indiceVoieMandataire != null)
    {
        var monId = Value('id').of(tag.adresseMandataire.indiceVoieMandataire)._eval();
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.6'] = monId;
    }
    if (tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null)
    {
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.7'] = tag.adresseMandataire.voieDistributionSpecialeAdresseMandataire;
    }
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.8'] = tag.adresseMandataire.codePostalMandataire;
    if (tag.adresseMandataire.complementVoieMandataire != null)
    {
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.10'] = tag.adresseMandataire.complementVoieMandataire;
    }
    if (tag.adresseMandataire.typeVoieMandataire != null)
    {
        var monId = Value('id').of(tag.adresseMandataire.typeVoieMandataire)._eval();
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.11'] = monId;
    }
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.12'] = tag.adresseMandataire.nomVoieMandataire;
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.13'] = tag.adresseMandataire.villeAdresseMandataire.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C41'] = tag.formaliteSignatureLieu;
if (tag.formaliteSignatureDate != null)
{
    var dateTemp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
    var year = dateTemp.getFullYear();
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
    var date = year + "-" + month + "-" + day;
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C42'] = date;
}
if (Value('id').of(societe.entrepriseFormeJuridiqueCivile).eq('5699')or Value('id').of(societe.entrepriseFormeJuridiqueCommerciale).eq('9900'))
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C43'] = societe.entrepriseSocieteAutre + ' ' + (correspondance.formaliteObservations != null ? correspondance.formaliteObservations : '');
}
else
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C43'] = correspondance.formaliteObservations != null ? correspondance.formaliteObservations : '';
}

// Groupe ICM : Identification complète de la personne morale

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/ICM/M01/M01.1'] = societe.entrepriseDenomination;
if (societe.entrepriseSigle != null)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/ICM/M01/M01.2'] = societe.entrepriseSigle;
}
// Groupe NDM  : Nom de domaine des sites Web PM

var societeNomDomaine = $m0Agricole.cadreDeclarationPMGroup.principaleActiviteObjetSocial.societeNomDomaine;
if (societeNomDomaine[0].length > 0) { 
	for (var i = 0; i < societeNomDomaine.size(); i++) {	
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/NDM/M05[' + (i+1) + ']/M05.1'] = societeNomDomaine[i];
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/NDM/M05[' + (i+1) + ']/M05.2'] = "1";
	
	}}

/* if ($m0Agricole.cadreDeclarationPMGroup.principaleActiviteObjetSocial.societeNomDomaine.size() > 0) {
var nomDomaine=[];
var idDomaine=[];
   for (var i = 0; i < $m0Agricole.cadreDeclarationPMGroup.principaleActiviteObjetSocial.societeNomDomaine.size(); i++) {	
   nomDomaine.push($m0Agricole.cadreDeclarationPMGroup.principaleActiviteObjetSocial.societeNomDomaine[i]);  
   idDomaine.push("1");  
   }regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/NDM[i]/M05/M05.1'] = nomDomaine;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/NDM/M05/M05.2'] = idDomaine;
} */
// Groupe FJM : Forme juridique de la personne morale (ajout)
if (Value('id').of(societe.entrepriseFormeJuridiqueConstitution).eq('societeCivile')) //EARL, GAEC, SCEA, GFA
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FJM/M21/M21.1'] = societe.entrepriseFormeJuridiqueCivile.getId();
}
else if (Value('id').of(societe.entrepriseFormeJuridiqueCommerciale).eq('5710')) //SAS
{
	if (societe.entrepriseAssocieUnique){
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FJM/M21/M21.1'] = "5720"; //SAS à associé unique
	} else {
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FJM/M21/M21.1'] =  "5710"; 
	}
}
else if (Value('id').of(societe.entrepriseFormeJuridiqueCommerciale).eq('5499')) //SARL
{
	if (societe.entrepriseAssocieUnique){
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FJM/M21/M21.1'] = "5498"; //SARL à associé unique
	} else {
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FJM/M21/M21.1'] =  "5499"; 
	}
}
else { //SA & SNC
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FJM/M21/M21.1'] = societe.entrepriseFormeJuridiqueCommerciale.getId();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FJM/M21/M21.2'] = infoCpt.statutLegalParticulier;
if (Value('id').of(societe.entrepriseFormeJuridiqueCivile).eq('6533'))
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FJM/M21/M21.4'] = societe.entrepriseSocieteCivileGAECDateAgrement;
}
if (infoCpt.adhesionESSM0AC)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FJM/M22'] = "O";
}
// Groupe SUM : Mention d'un associé unique (déclaration)

if (societe.entrepriseAssocieUnique)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/SUM/M23'] = "O";
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/SUM/M29'] = (societe.entrepriseAssocieUniqueGerant or societe.entrepriseAssocieUniquePresident) != false ? "O" : "N";
}
if (Value('id').of(societe.entrepriseStatutType).eq('entrepriseStatutTypeSansModification'))
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/SUM/M30'] = "1";
}
else if (Value('id').of(societe.entrepriseStatutType).eq('entrepriseStatutTypeDifferents'))
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/SUM/M30'] = "2";
}

// Groupe CSM : Caractéristiques de la personne morale

if (societe.questionCapital)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M24/M24.1'] = "2";
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M24/M24.2'] = societe.entrepriseCapitalMontant;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M24/M24.3'] = societe.entrepriseCapitalDevise;
if (societe.questionCapital)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M24/M24.4'] = societe.entrepriseCapitalVariableMinimum;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M25'] = societe.entrepriseDuree;

if (infoCpt.dateClotureExerciceM0AC != null)
{
    var dateTemp = new Date(parseInt(infoCpt.dateClotureExerciceM0AC.getTimeInMillis()));
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
    var date = "--" + month + "-" + day;
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M26'] = date;
}

if (infoCpt.dateClotureExerciceSiDifferenteM0AC != null)
{
    var dateTemp = new Date(parseInt(infoCpt.dateClotureExerciceSiDifferenteM0AC.getTimeInMillis()));
    var year = dateTemp.getFullYear();
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
    var date = year + "-" + month + "-" + day;
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M28'] = date;
}

// Groupe ACM : Aide aux créateurs de la personne morale

if (not societe.entrepriseSocieteConstitueSansActivite)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/ACM/M62'] = "N";
}

// Groupe NGM : Nature de la gérance de la personne morale

if (not societe.entrepriseSocieteConstitueSansActivite)
{
    if (Value('id').of(infoSte.geranceSARL).eq('geranceMajoritaireSARLM0AC'))
    {
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/NGM/M27'] = "1";
    }
    else if (Value('id').of(infoSte.geranceSARL).eq('geranceMinoritaireSARLM0AC')and infoSte.geranceMinoritaireAssocieOUINONSARLM0AC)
    {
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/NGM/M27'] = "3";
    }
    else if (Value('id').of(infoSte.geranceSARL).eq('geranceMinoritaireSARLM0AC')and not infoSte.geranceMinoritaireAssocieOUINONSARLM0AC)
    {
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/NGM/M27'] = "4";
    }
    else if (Value('id').of(infoSte.geranceSARL).eq('geranceTiersSARLM0AC')and infoSte.geranceMinoritaireAssocieOUINONSARLM0AC)
    {
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/NGM/M27'] = "5";
    }
    else if (Value('id').of(infoSte.geranceSARL).eq('geranceTiersSARLM0AC')and not infoSte.geranceMinoritaireAssocieOUINONSARLM0AC)
    {
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/NGM/M27'] = "6";
    }
}

// Groupe FSM : Fusion / Scission de la personne morale

if (societe.entrepriseFusionScission)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M54'] = Value('id').of(fusion1.fusionOuScission).eq('fusion') ? "1" : "2";
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.1'] = fusion1.fusionScission.fusionScissionDenomination;
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.2'] = fusion1.fusionScission.fusionScissionFormeJuridique.getId();
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.3'] = fusion1.fusionScission.adresseFusionScission.fusionScissionAdresseCommune.getId();
    if (fusion1.adresseFusionScission.fusionScissionNumeroVoie != null)
    {
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.5'] = fusion1.adresseFusionScission.fusionScissionNumeroVoie;
    }
    if (fusion1.adresseFusionScission.fusionScissionAdresseIndiceVoie != null)
    {
        var monId = Value('id').of(fusion1.adresseFusionScission.fusionScissionAdresseIndiceVoie)._eval();
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.6'] = monId;
    }
    if (fusion1.adresseFusionScission.fusionScissionAdresseDistributionSpecialeVoie != null)
    {
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.7'] = fusion1.adresseFusionScission.fusionScissionAdresseDistributionSpecialeVoie;
    }
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.8'] = fusion1.adresseFusionScission.fusionScissionAdresseCodePostal;
    if (fusion1.adresseFusionScission.fusionScissionAdresseComplementVoie != null)
    {
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.10'] = fusion1.adresseFusionScission.fusionScissionAdresseComplementVoie;
    }
    if (fusion1.adresseFusionScission.fusionScissionAdresseTypeVoie != null)
    {
        var monId = Value('id').of(fusion1.adresseFusionScission.fusionScissionAdresseTypeVoie)._eval();
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.11'] = monId;
    }
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.12'] = fusion1.adresseFusionScission.fusionScissionAdresseNomVoie;
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.13'] = fusion1.adresseFusionScission.fusionScissionAdresseCommune.getLabel();

    //Récupérer le EntityId du greffe
    var greffeId = fusion1.fusionScission.fusionScissionGreffeImmatriculation.id; //
    //Appeler Directory pour récupérer l'autorité
    var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', greffeId) //
        .connectionTimeout(10000) //
        .receiveTimeout(10000) //
        .accept('json') //
        .get();
    //result
    var receiverInfo = response.asObject();
    var contactInfo = !receiverInfo.details ? null : receiverInfo.details;
    //Récupérer le code EDI
    var codeEDIGreffe = !contactInfo.ediCode ? null : contactInfo.ediCode;
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.4'] = codeEDIGreffe;
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.6'] = fusion1.fusionScission.fusionScissionSiren.split(' ').join('');

    if (fusion1.entrepriseFusionScission2)
    {
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.1'] = fusion2.fusionScission2.fusionScissionDenomination2;
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.2'] = fusion2.fusionScission2.fusionScissionFormeJuridique2.getId();
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.3'] = fusion2.fusionScission2.adresseFusionScission2.fusionScissionAdresseCommune2.getId();
        if (fusion2.adresseFusionScission2.fusionScissionNumeroVoie2 != null)
        {
            regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.5'] = fusion2.adresseFusionScission2.fusionScissionNumeroVoie2;
        }
        if (fusion2.adresseFusionScission2.fusionScissionAdresseIndiceVoie2 != null)
        {
            var monId = Value('id').of(fusion2.adresseFusionScission2.fusionScissionAdresseIndiceVoie2)._eval();
            regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.6'] = monId;
        }
        if (fusion2.adresseFusionScission2.fusionScissionAdresseDistributionSpecialeVoie2 != null)
        {
            regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.7'] = fusion2.adresseFusionScission2.fusionScissionAdresseDistributionSpecialeVoie2;
        }
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.8'] = fusion2.adresseFusionScission2.fusionScissionAdresseCodePostal2;
        if (fusion2.adresseFusionScission2.fusionScissionAdresseComplementVoie2 != null)
        {
            regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.10'] = fusion2.adresseFusionScission2.fusionScissionAdresseComplementVoie2;
        }
        if (fusion2.adresseFusionScission2.fusionScissionAdresseTypeVoie2 != null)
        {
            var monId = Value('id').of(fusion2.adresseFusionScission2.fusionScissionAdresseTypeVoie2)._eval();
            regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.11'] = monId;
        }
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.12'] = fusion2.adresseFusionScission2.fusionScissionAdresseNomVoie2;
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.13'] = fusion2.adresseFusionScission2.fusionScissionAdresseCommune2.getLabel();

        //Récupérer le EntityId du greffe
        var greffeId = fusion2.fusionScission2.fusionScissionGreffeImmatriculation2.id; //
        var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', greffeId) //
            .connectionTimeout(10000) //
            .receiveTimeout(10000) //
            .accept('json') //
            .get();
        //result
        var receiverInfo = response.asObject();
        var contactInfo = !receiverInfo.details ? null : receiverInfo.details;
        //Récupérer le code EDI
        var codeEDIGreffe = !contactInfo.ediCode ? null : contactInfo.ediCode;
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.4'] = codeEDIGreffe;
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.6'] = fusion2.fusionScission2.fusionScissionSiren2.split(' ').join('');
    }

    if (fusion2.entrepriseFusionScission3)
    {

        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.1'] = fusion3.fusionScission3.fusionScissionDenomination3;
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.2'] = fusion3.fusionScission3.fusionScissionFormeJuridique3.getId();
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.3'] = fusion3.fusionScission3.adresseFusionScission3.fusionScissionAdresseCommune3.getId();
        if (fusion3.adresseFusionScission.fusionScissionNumeroVoie3 != null)
        {
            regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.5'] = fusion3.adresseFusionScission3.fusionScissionNumeroVoie3;
        }
        if (fusion3.adresseFusionScission2.fusionScissionAdresseIndiceVoie3 != null)
        {
            var monId = Value('id').of(fusion3.adresseFusionScission3.fusionScissionAdresseIndiceVoie3)._eval();
            regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.6'] = monId;
        }
        if (fusion3.adresseFusionScission3.fusionScissionAdresseDistributionSpecialeVoie3 != null)
        {
            regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.7'] = fusion3.adresseFusionScission3.fusionScissionAdresseDistributionSpecialeVoie3;
        }
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.8'] = fusion3.adresseFusionScission3.fusionScissionAdresseCodePostal3;
        if (fusion3.adresseFusionScission3.fusionScissionAdresseComplementVoie3 != null)
        {
            regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.10'] = fusion3.adresseFusionScission3.fusionScissionAdresseComplementVoie3;
        }
        if (fusion3.adresseFusionScission3.fusionScissionAdresseTypeVoie3 != null)
        {
            var monId = Value('id').of(fusion3.adresseFusionScission3.fusionScissionAdresseTypeVoie3)._eval();
            regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.11'] = monId;
        }
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.12'] = fusion3.adresseFusionScission3.fusionScissionAdresseNomVoie3;
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.13'] = fusion3.adresseFusionScission3.fusionScissionAdresseCommune3.getLabel();

        //Récupérer le EntityId du greffe
        var greffeId = fusion3.fusionScission3.fusionScissionGreffeImmatriculation3.id; //
        //Appeler Directory pour récupérer l'autorité
        var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', greffeId) //
            .connectionTimeout(10000) //
            .receiveTimeout(10000) //
            .accept('json') //
            .get();
        //result
        var receiverInfo = response.asObject();
        var contactInfo = !receiverInfo.details ? null : receiverInfo.details;
        //Récupérer le code EDI
        var codeEDIGreffe = !contactInfo.ediCode ? null : contactInfo.ediCode;
        // À modifier quand Destiny fourni le réferentiel
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.4'] = codeEDIGreffe;
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.6'] = fusion3.fusionScission3.fusionScissionSiren3.split(' ').join('');
    }
}

// Groupe SIU : Siège de l'entreprise

if (Value('id').of(siege.questionDomiciliation).eq('locauxEntrepriseDomiciliation'))
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U10'] = siege.domiciliataireNom;
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U15'] = siege.domiciliataireSiren.split(' ').join('');
}
if (Value('id').of(siege.questionDomiciliation).eq('locauxHabitation'))
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U16'] = "O";
}
if (siege.siegeAdresseCommune != null)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.3'] = siege.siegeAdresseCommune.getId();
}
else
{
    var idPays = siege.siegeAdressePays.getId();
    var result = idPays.match(regEx)[0];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.3'] = result;
}
if (siege.siegeAdresseNumeroVoie != null)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.5'] = siege.siegeAdresseNumeroVoie;
}
if (siege.siegeAdresseIndiceVoie != null)
{
    var monId = Value('id').of(siege.siegeAdresseIndiceVoie)._eval();
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.6'] = monId;
}
if (siege.siegeADresseDistributionSpeciale != null)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.7'] = siege.siegeADresseDistributionSpeciale;
}
if (siege.siegeAdresseCodePostal != null)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.8'] = siege.siegeAdresseCodePostal;
}
else if (siege.siegeAdresseCodePostalEtranger != null)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.8'] = siege.siegeAdresseCodePostalEtranger;
}
if (siege.siegeAdresseComplementVoie != null)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.10'] = siege.siegeAdresseComplementVoie;
}
if (siege.siegeAdresseTypeVoie != null)
{
    var monId = Value('id').of(siege.siegeAdresseTypeVoie)._eval();
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.11'] = monId;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.12'] = siege.siegeAdresseNomVoie;
if (siege.siegeAdresseCommune != null)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.13'] = siege.siegeAdresseCommune.getLabel();
}
else
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.13'] = siege.siegeAdresseVille;
}
if (siege.siegeAdressePays != null)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.14'] = siege.siegeAdressePays.getLabel();
}
if (infoSteEtrangere.registrePublicSiegeNumeroImmatriculationM0AC != null)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U14/U14.2'] = infoSteEtrangere.registrePublicSiegeNumeroImmatriculationM0AC;
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U14/U14.3'] = infoSteEtrangere.registrePublicSiegeLieuM0AC + "/" + infoSteEtrangere.registrePublicSiegePaysM0AC;
}

// Groupe CPU : Caractéristiques de l'entreprise

if (exploitation.informationActivite.activiteEtablissement != null)
{
    if (Value('id').of(exploitation.informationActivite.activiteEtablissement).eq('etablissementActivitesExerceesAgricoleCulture'))
    {
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U21'] = exploitation.informationActivite.questionActiviteCulture != null ? exploitation.informationActivite.questionActiviteCulture.toString() : exploitation.informationActivite.etablissementActivitePlusImportanteAgricoleAutresPreciser;
    }
    else if (Value('id').of(exploitation.informationActivite.activiteEtablissement).eq('etablissementActivitesExerceesAgricoleElevage'))
    {
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U21'] = exploitation.informationActivite.questionActiviteElevage != null ? exploitation.informationActivite.questionActiviteElevage.toString() : exploitation.informationActivite.etablissementActivitePlusImportanteAgricoleAutresPreciser;
    }
    else
    {
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U21'] = exploitation.informationActivite.questionActiviteAutres != null ? exploitation.informationActivite.questionActiviteAutres.toString() : exploitation.informationActivite.etablissementActivitePlusImportanteAgricoleAutresPreciser;
    }
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U21'] =
    Value('id').of(activite.activiteEtablissement).eq('etablissementActivitesExerceesAgricoleCulture') ? (activite.questionActiviteCulture != null ? activite.questionActiviteCulture.toString() : activite.etablissementActivitePlusImportanteAgricoleAutresPreciser) :
    (Value('id').of(activite.activiteEtablissement).eq('etablissementActivitesExerceesAgricoleElevage') ? (activite.questionActiviteElevage != null ? activite.questionActiviteElevage.toString() : activite.etablissementActivitePlusImportanteAgricoleAutresPreciser) :
        (Value('id').of(activite.activiteEtablissement).eq('etablissementActivitesExerceesAgricoleAutresActivites') ? (activite.questionActiviteAutres != null ? activite.questionActiviteAutres.toString() : activite.etablissementActivitePlusImportanteAgricoleAutresPreciser) : "."));
//
if (salarie.etablissementEffectifSalariePresenceOui)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U22'] = salarie.etablissementEffectifSalarieAgricoleNombre;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U24'] = $m0Agricole.cadreDeclarationPMGroup.principaleActiviteObjetSocial.activiteObjetSocial;
//par defaut à N sur le M0
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U28'] = "N";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U29'] = "N";

// Groupe ISU : Etablissements situés dans l'UE

if (siege.adresseEtablissementNumeroImmatriculation)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1].U41.1'] = infoEtabEU.adresseEtablissementLieu;
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1].U41.2'] = infoEtabEU.adresseEtablissementNumeroImmatriculation;

    if (infoEtabEU.adresseEtablissementfacultatifAdresse4 != null)
    {
        var idPays = infoEtabEU.adresseEtablissementfacultatifAdresse4.getId();
        var result = idPays.match(regEx)[0];
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1].U41.3/U41.3.3'] = result;
    }
    /* if (infoEtabEU.adresseEtablissementfacultatifAdresse1 != null) {
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1].U41.3/U41.3.5']= infoEtabEU.adresseEtablissementfacultatifAdresse1;
    } */
    /* if (infoEtabEUindiceVoieAdresseAutreEtablissementUE != null) {
    var monId1 = Value('id').of(infoEtabEU.indiceVoieAdresseAutreEtablissementUE)._eval();
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1].U41.3/U41.3.6']          = monId1;
    }  */
    /* if (infoEtabEU.distributionSpecialeAdresseAutreEtablissementUE != null) {
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1].U41.3/U41.3.7']= infoEtabEU.distributionSpecialeAdresseAutreEtablissementUE;
    } */
    if (infoEtabEU.adresseEtablissementfacultatifAdresse3 != null)
    {
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1].U41.3/U41.3.8'] = infoEtabEU.adresseEtablissementfacultatifAdresse3;
    }
    /* if (infoEtabEU.rueComplementAdresseAutreEtablissementUE != null) {
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1].U41.3/U41.3.10']= infoEtabEU.rueComplementAdresseAutreEtablissementUE;
    } */
    /* if (infoEtabEUtypeVoieAdresseAutreEtablissementUE != null) {
    var monId1 = Value('id').of(infoEtabEUtypeVoieAdresseAutreEtablissementUE)._eval();
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1].U41.3/U41.3.11']          = monId1;
    }  */
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1].U41.3/U41.3.12'] = infoEtabEU.adresseEtablissementfacultatifAdresse1;
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1].U41.3/U41.3.13'] = infoEtabEU.adresseEtablissementfacultatifAdresse2;
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1].U41.3/U41.3.14'] = infoEtabEU.adresseEtablissementfacultatifAdresse4.getLabel();

    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[1].U41.4'] = infoEtabEU.adresseEtablissementfacultatifActivite;

    /* if (siege.cadreAutreEtablissementUE1.declarationAutreEtablissementUE) {
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2].U41.1']= siege.cadreAutreEtablissementUE2.lieuImmatriculationAutreEtablissementUE;
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2].U41.2']= siege.cadreAutreEtablissementUE2.numeroImmatriculationAutreEtablissementUE;

    if (siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.paysAdresseAutreEtablissementUE != null) {
    var idPays = siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.paysAdresseAutreEtablissementUE.getId();
    var result = idPays.match(regEx)[0];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2].U41.3/U41.3.3'] = result;
    }
    if (siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.rueNumeroAdresseAutreEtablissementUE != null) {
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2].U41.3/U41.3.5']= siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.rueNumeroAdresseAutreEtablissementUE;
    }
    if (siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.indiceVoieAdresseAutreEtablissementUE != null) {
    var monId1 = Value('id').of(siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.indiceVoieAdresseAutreEtablissementUE)._eval();
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2].U41.3/U41.3.6']          = monId1;
    }
    if (siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.distributionSpecialeAdresseAutreEtablissementUE != null) {
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2].U41.3/U41.3.7']= siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.distributionSpecialeAdresseAutreEtablissementUE;
    }
    if (siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.codePostalAdresseAutreEtablissementUE != null) {
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2].U41.3/U41.3.8']= siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.codePostalAdresseAutreEtablissementUE;
    }
    if (siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.rueComplementAdresseAutreEtablissementUE != null) {
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2].U41.3/U41.3.10']= siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.rueComplementAdresseAutreEtablissementUE;
    }
    if (siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.typeVoieAdresseAutreEtablissementUE != null) {
    var monId1 = Value('id').of(siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.typeVoieAdresseAutreEtablissementUE)._eval();
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2].U41.3/U41.3.11']          = monId1;
    }
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2].U41.3/U41.3.12']= siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.nomVoieAdresseAutreEtablissementUE;
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2].U41.3/U41.3.13']= siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.communeAdresseAutreEtablissementUECommune;
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2].U41.3/U41.3.14']= siege.cadreAutreEtablissementUE2.adresseAutreEtablissementUE.paysAdresseAutreEtablissementUE.getLabel();

    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[2].U41.4']= siege.cadreAutreEtablissementUE2.activiteAutreEtablissementUE;
    } */
}

// RFU : Régime fiscal de l'entreprise

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U31'] =
    Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('regimeFiscalRegimeImpositionBeneficesMBA') ? "117" :
    (Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('regimeFiscalRegimeImpositionBeneficesRsBA') ? "118" :
        (Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('regimeFiscalRegimeImpositionBeneficesRnBA') ? "119" :
            (Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('regimeFiscalRegimeImpositionBeneficesFfBA') ? "120" : ".")));
//
if (Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('regimeFiscalRegimeImpositionBeneficesOrspBA'))
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U32'] = "212";
}
else if (Value('id').of(fiscal.impotSociete.regimeFiscalImpositionSociete).eq('regimeFiscalRegimeImpositionSocieteIsoIS'))
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U32'] = "211";
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.1'] = (Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC.regimeTVA1).eq('regimeFiscalRegimeImpositionTVARfTVA')or Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC.regimeTVA2).eq('regimeFiscalRegimeImpositionTVARfTVA')or Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBNC.regimeTVA).eq('regimeFiscalRegimeImpositionTVARfTVA')) ? "310" :
((Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC.regimeTVA1).eq('regimeFiscalRegimeImpositionTVARsTVA')or Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBNC.regimeTVA).eq('regimeFiscalRegimeImpositionTVARsTVA')) ? "311" :
    ((Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC.regimeTVA2).eq('regimeFiscalRegimeImpositionTVARnTVA')or Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBNC.regimeTVA).eq('regimeFiscalRegimeImpositionTVARnTVA')) ? "312" : (Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC.regimeTVA1).eq('regimeFiscalRegimeImpositionTVAMrTVA') ? "313" : (Value('id').of(fiscal.optionsTVA.regimeTVA).eq('impositionTVARemboursementForfaitaire') ? "314" : "315"))));

if (fiscal.impositionAccessoire.impositionBeneficesBIC.regimeFiscalRegimeImpositionTVAOptionsParticulieres1 or fiscal.impositionAccessoire.impositionBeneficesBNC.regimeFiscalRegimeImpositionTVAOptionsParticulieres1)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.2'] = "410";
}
else if (fiscal.impositionAccessoire.impositionBeneficesBIC.regimeFiscalRegimeImpositionTVAOptionsParticulieres2)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.2'] = "411";
}
else if (Value('id').of(fiscal.optionsTVA.optionTVAvolontaire).eq('regimeFiscalRegimeImpositionTVAOptionsParticulieres4'))
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.2'] = "414";
}
else if (fiscal.impositionAccessoire.impositionBeneficesBIC.regimeFiscalRegimeImpositionTVAOptionsParticulieres1 and fiscal.impositionAccessoire.impositionBeneficesBIC.regimeFiscalRegimeImpositionTVAOptionsParticulieres2)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.2'] = "420";
}
if (fiscal.optionsTVA.optionTVADeclaration)
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.3'] = "512";
}
else if (Value('id').of(fiscal.optionsTVA.optionTVADepot).eq('regimeFiscalRegimeImpositionTVADeclarationTrimestrielle'))
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.3'] = "511";
}
else if (Value('id').of(fiscal.optionsTVA.optionTVADepot).eq('regimeFiscalRegimeImpositionTVADeclarationMensuelle'))
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.3'] = "513";
}
if (fiscal.optionsTVA.regimeFiscalDateClotureExerciceComptable != null)
{
    var dateTemp = new Date(parseInt(societe.optionsTVA.regimeFiscalDateClotureExerciceComptable.getTimeInMillis()));
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
    var date = "--" + month + "-" + day;
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U37'] = date;
}

// Groupe OIU : Options fiscales avec incidence sociale

if (Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('regimeFiscalRegimeImpositionBeneficesOrspBA'))
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OIU/U75'] = "212";
}
else if (Value('id').of(fiscal.impotSociete.regimeFiscalImpositionSociete).eq('regimeFiscalRegimeImpositionSocieteIsoIS'))
{
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OIU/U75'] = "211";
}

// Groupe GRD : Dirigeant personne physique

var occurrencesGRD = [];
var occurrence = {};

// Dirigeant 1

if (Value('id').of(dirigeant1.personnaliteJuridique).eq('personnePhysique')or(Value('id').of(dirigeant1.personnaliteJuridique).eq('personneMorale')and not Value('id').of(dirigeant1.personneLieeQualite).eq('71')or not Value('id').of(dirigeant1.personneLieeQualite).eq('72')))
    /* 	or (Value('id').of(dirigeant1.personneLieeQualite).eq('51')
    or Value('id').of(dirigeant1.personneLieeQualite).eq('69')
    or Value('id').of(dirigeant1.personneLieeQualite).eq('60')
    or Value('id').of(dirigeant1.personneLieeQualite).eq('61')
    or Value('id').of(dirigeant1.personneLieeQualite).eq('52')
    or Value('id').of(dirigeant1.personneLieeQualite).eq('63')
    or ((Value('id').of(dirigeant1.personneLieeQualite).eq('73')
    or Value('id').of(dirigeant1.personneLieeQualite).eq('53')
    or Value('id').of(dirigeant1.personneLieeQualite).eq('70')
    or Value('id').of(dirigeant1.personneLieeQualite).eq('30'))
    and (Value('id').of(societe.entrepriseFormeJuridique).eq('5785')
    or Value('id').of(societe.entrepriseFormeJuridique).eq('5585')
    or Value('id').of(societe.entrepriseFormeJuridique).eq('5685')))
    or ((Value('id').of(dirigeant1.personneLieeQualite).eq('28')
    or Value('id').of(dirigeant1.personneLieeQualite).eq('74'))
    and Value('id').of(societe.entrepriseFormeJuridique).eq('5385')))) */
{

    //Groupe DIU : Dirigeant de l'entreprise

    occurrence['U50.1'] = "1";
    if (exploitation.informationActivite.etablissementDateDebutActivite != null)
    {
        var dateTemp = new Date(parseInt(exploitation.informationActivite.etablissementDateDebutActivite.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['U50.2'] = date;
    }
    else if (tag.formaliteSignatureDate != null)
    {
        var dateTemp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['U50.2'] = date;
    }
    occurrence['U51.1'] = dirigeant1.personneLieeQualite.getId();
    occurrence['U52'] = "P";

    // Groupe IPD : Identification du dirigeant de l'entreprise

    occurrence['D01.1'] = (Value('id').of(dirigeant1.civilitePersonnePhysique1.personnaliteGenre).eq('genreMasculin') or Value('id').of(dirigeant1.civiliteRepresentant1.personnaliteGenre).eq('genreMasculin')) ? "1" : "2";
    occurrence['D01.2'] = dirigeant1.civilitePersonnePhysique1.personneLieePPNomNaissance != null ? dirigeant1.civilitePersonnePhysique1.personneLieePPNomNaissance : dirigeant1.civiliteRepresentant1.personneLieePPNomNaissance;
    if (dirigeant1.civilitePersonnePhysique1.personneLieePPPrenom != null and dirigeant1.civilitePersonnePhysique1.personneLieePPPrenom[0].length > 0)
    {
        var prenoms = [];
        for (i = 0; i < dirigeant1.civilitePersonnePhysique1.personneLieePPPrenom.size(); i++)
        {
            prenoms.push(dirigeant1.civilitePersonnePhysique1.personneLieePPPrenom[i]);
        }
        occurrence['D01.3'] = prenoms;
    }
    else if (dirigeant1.civiliteRepresentant1.personneLieePPPrenom != null)
    {
        var prenoms = [];
        for (i = 0; i < dirigeant1.civiliteRepresentant1.personneLieePPPrenom.size(); i++)
        {
            prenoms.push(dirigeant1.civiliteRepresentant1.personneLieePPPrenom[i]);
        }
        occurrence['D01.3'] = prenoms;
    }
    if (dirigeant1.civilitePersonnePhysique1.personneLieePPNomUsage != null or dirigeant1.civiliteRepresentant1.personneLieePPNomUsage != null)
    {
        occurrence['D01.4'] = dirigeant1.civilitePersonnePhysique1.personneLieePPNomUsage != null ? dirigeant1.civilitePersonnePhysique1.personneLieePPNomUsage : dirigeant1.civiliteRepresentant1.personneLieePPNomUsage;
    }
    if (dirigeant1.civilitePersonnePhysique1.personneLieePPDateNaissance != null)
    {
        var dateTemp = new Date(parseInt(dirigeant1.civilitePersonnePhysique1.personneLieePPDateNaissance.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['D03.1'] = date;
    }
    else if (dirigeant1.civiliteRepresentant1.personneLieePPDateNaissance != null)
    {
        var dateTemp = new Date(parseInt(dirigeant1.civiliteRepresentant1.personneLieePPDateNaissance.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['D03.1'] = date;
    }
    if (not Value('id').of(dirigeant1.civilitePersonnePhysique1.personneLieePPLieuNaissancePays).eq('FRXXXXX')and dirigeant1.civilitePersonnePhysique1.personneLieePPNomNaissance != null)
    {
        var idPays = dirigeant1.civilitePersonnePhysique1.personneLieePPLieuNaissancePays.getId();
        var result = idPays.match(regEx)[0];
        occurrence['D03.2'] = result;
    }
    else if (Value('id').of(dirigeant1.civilitePersonnePhysique1.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
    {
        occurrence['D03.2'] = dirigeant1.civilitePersonnePhysique1.personneLieePPLieuNaissanceCommune.getId();
    }
    else if (not Value('id').of(dirigeant1.civiliteRepresentant1.personneLieePPLieuNaissancePays).eq('FRXXXXX') and dirigeant1.adresseDirigeant1.siegeAdresseVille)
    {
        var idPays = dirigeant1.civiliteRepresentant.personneLieePPLieuNaissancePays.getId();
        var result = idPays.match(regEx)[0];
        occurrence['D03.2'] = result;
    }
    else if (Value('id').of(dirigeant1.civiliteRepresentant1.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
    {
        occurrence['D03.2'] = dirigeant1.civiliteRepresentant1.personneLieePPLieuNaissanceCommune.getId();
    }
    if (not Value('id').of(dirigeant1.civilitePersonnePhysique1.personneLieePPLieuNaissancePays).eq('FRXXXXX')and dirigeant1.civilitePersonnePhysique1.personneLieePPNomNaissance != null)
    {
        occurrence['D03.3'] = dirigeant1.civilitePersonnePhysique1.personneLieePPLieuNaissancePays.getLabel();
    }
    else if (not Value('id').of(dirigeant1.civiliteRepresentant1.personneLieePPLieuNaissancePays).eq('FRXXXXX')and dirigeant1.civiliteRepresentant1.personneLieePPNomNaissance != null)
    {
        occurrence['D03.3'] = dirigeant1.civiliteRepresentant1.personneLieePPLieuNaissancePays.getLabel();
    }
    if (Value('id').of(dirigeant1.civilitePersonnePhysique1.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
    {
        occurrence['D03.4'] = dirigeant1.civilitePersonnePhysique1.personneLieePPLieuNaissanceCommune.getLabel();
    }
    else if (Value('id').of(dirigeant1.civiliteRepresentant1.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
    {
        occurrence['D03.4'] = dirigeant1.civiliteRepresentant1.personneLieePPLieuNaissanceCommune.getLabel();
    }
// adresse
    if (Value('id').of(dirigeant1.adresseDirigeant1.siegeAdressePays).eq('FRXXXXX'))
    {
        occurrence['D04.3'] = dirigeant1.adresseDirigeant1.siegeAdresseCommune.getId();
    }
    else if (not Value('id').of(dirigeant1.adresseDirigeant1.siegeAdressePays).eq('FRXXXXX'))
    {
        var idPays = dirigeant1.adresseDirigeant1.siegeAdressePays.getId();
        var result = idPays.match(regEx)[0];
        occurrence['D04.3'] = result;
    }
    if (dirigeant1.adresseDirigeant1.siegeAdresseNumVoie != null)
    {
        occurrence['D04.5'] = dirigeant1.adresseDirigeant1.siegeAdresseNumVoie;
    }
    if (dirigeant1.adresseDirigeant1.siegeAdresseIndiceVoie != null)
    {
        var monId1 = Value('id').of(dirigeant1.adresseDirigeant1.siegeAdresseIndiceVoie)._eval();
        occurrence['D04.6'] = monId1;
    }
    if (dirigeant1.adresseDirigeant1.siegeAdresseDistributionSpecialeVoie != null)
    {
        occurrence['D04.7'] = dirigeant1.adresseDirigeant1.siegeAdresseDistributionSpecialeVoie;
    }
	if (dirigeant1.adresseDirigeant1.siegeAdresseCodePostal != null)
    {
    occurrence['D04.8'] = dirigeant1.adresseDirigeant1.siegeAdresseCodePostal;
	}
	else if (dirigeant1.adresseDirigeant1.siegeAdresseCodePostaEtranger != null)
	{
	occurrence['D04.8'] = dirigeant1.adresseDirigeant1.siegeAdresseCodePostaEtranger;
	}
    if (dirigeant1.adresseDirigeant1.siegeAdresseComplementVoie != null)
    {
        occurrence['D04.10'] = dirigeant1.adresseDirigeant1.siegeAdresseComplementVoie;
    }
    if (dirigeant1.adresseDirigeant1.siegeAdresseTypeVoie != null)
    {
        var monId1 = Value('id').of(dirigeant1.adresseDirigeant1.siegeAdresseTypeVoie)._eval();
        occurrence['D04.11'] = monId1;
    }
	if (dirigeant1.adresseDirigeant1.siegeAdresseNomVoie != null)
    {
		occurrence['D04.12'] = dirigeant1.adresseDirigeant1.siegeAdresseNomVoie;
	}
    if (Value('id').of(dirigeant1.adresseDirigeant1.siegeAdressePays).eq('FRXXXXX'))
    {
        occurrence['D04.13'] = dirigeant1.adresseDirigeant1.siegeAdresseCommune.getLabel();
    }
  else if (not Value('id').of(dirigeant1.adresseDirigeant1.siegeAdressePays).eq('FRXXXXX') and dirigeant1.adresseDirigeant1.siegeAdresseVille)
    {
        occurrence['D04.13'] = dirigeant1.adresseDirigeant1.siegeAdresseVille;
		occurrence['D04.14'] = dirigeant1.adresseDirigeant1.siegeAdressePays.getLabel();
    }

    // Groupe NPD

    occurrence['D21'] = dirigeant1.civilitePersonnePhysique1.personneLieePPNationalite != null ? dirigeant1.civilitePersonnePhysique1.personneLieePPNationalite : dirigeant1.civiliteRepresentant1.personneLieePPNationalite;

    // Groupe GCS

    /* if (Value('id').of(dirigeant1.personneLieeQualite).eq('28') or Value('id').of(dirigeant1.personneLieeQualite).eq('74')) { */

    // Groupe ISS

    /* var nirDeclarant = socialDirigeant1.voletSocialNumeroSecuriteSocialTNS;
    if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    occurrence['A10.1']  = nirDeclarant.substring(0, 13)
    occurrence['A10.2'] = nirDeclarant.substring(13, 15);
    } */

    // Groupe SNS

    /* if (socialDirigeant1.voletSocialActiviteExerceeAnterieurementTNS) {
    if(socialDirigeant1.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
    var dateTemp = new Date(parseInt(socialDirigeant1.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
    var year = dateTemp.getFullYear();
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
    var date = year + "-" + month + "-" + day ;
    occurrence['A21.2']= date;
    }
    occurrence['A21.3']= socialDirigeant1.voletSocialActiviteExerceeAnterieurementLibelleTNS;
    occurrence['A21.4']= socialDirigeant1.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
    occurrence['A21.6']= socialDirigeant1.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
    }
    occurrence['A22.3']= Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" :
    (Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
    if (Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
    occurrence['A22.4']= socialDirigeant1.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
    } else if (Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
    occurrence['A22.4']= "ENIM";
    }
    if (socialDirigeant1.voletSocialActiviteSimultaneeOUINON) {
    occurrence['A24.2'] = Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" :
    (Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
    }
    if (Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
    occurrence['A24.3'] = socialDirigeant1.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
    }
     */
    // Groupe CAS

    /* occurrence['A42.1']= ".";
    }*/

    occurrencesGRD.push(occurrence);
    occurrence = {};
}

// Dirigeant 2

if (Value('id').of(dirigeant2.personnaliteJuridique).eq('personnePhysique')or(Value('id').of(dirigeant2.personnaliteJuridique).eq('personneMorale')and not Value('id').of(dirigeant2.personneLieeQualite).eq('71')or not Value('id').of(dirigeant2.personneLieeQualite).eq('72')))
{

    //Groupe DIU : Dirigeant de l'entreprise

    occurrence['U50.1'] = "1";
    if (exploitation.informationActivite.etablissementDateDebutActivite != null)
    {
        var dateTemp = new Date(parseInt(exploitation.informationActivite.etablissementDateDebutActivite.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['U50.2'] = date;
    }
    else if (tag.formaliteSignatureDate != null)
    {
        var dateTemp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['U50.2'] = date;
    }
    occurrence['U51.1'] = dirigeant2.personneLieeQualite != null ? dirigeant2.personneLieeQualite.getId(): '';
    occurrence['U52'] = "P";

    // Groupe IPD : Identification du dirigeant de l'entreprise

    occurrence['D01.1'] = (Value('id').of(dirigeant2.civilitePersonnePhysique2.personnaliteGenre).eq('genreMasculin') or Value('id').of(dirigeant2.civiliteRepresentant2.personnaliteGenre).eq('genreMasculin')) ? "1" : "2";
    occurrence['D01.2'] = dirigeant2.civilitePersonnePhysique2.personneLieePPNomNaissance != null ? dirigeant2.civilitePersonnePhysique2.personneLieePPNomNaissance : dirigeant2.civiliteRepresentant2.personneLieePPNomNaissance;
    if (dirigeant2.civilitePersonnePhysique2.personneLieePPPrenom != null and dirigeant2.civilitePersonnePhysique2.personneLieePPPrenom[0].length > 0)
    {
        var prenoms = [];
        for (i = 0; i < dirigeant2.civilitePersonnePhysique2.personneLieePPPrenom.size(); i++)
        {
            prenoms.push(dirigeant2.civilitePersonnePhysique2.personneLieePPPrenom[i]);
        }
        occurrence['D01.3'] = prenoms;
    }
    else if (dirigeant2.civiliteRepresentant2.personneLieePPPrenom != null)
    {
        var prenoms = [];
        for (i = 0; i < dirigeant2.civiliteRepresentant2.personneLieePPPrenom.size(); i++)
        {
            prenoms.push(dirigeant2.civiliteRepresentant2.personneLieePPPrenom[i]);
        }
        occurrence['D01.3'] = prenoms;
    }
    if (dirigeant2.civilitePersonnePhysique2.personneLieePPNomUsage != null or dirigeant2.civiliteRepresentant2.personneLieePPNomUsage != null)
    {
        occurrence['D01.4'] = dirigeant2.civilitePersonnePhysique2.personneLieePPNomUsage != null ? dirigeant2.civilitePersonnePhysique2.personneLieePPNomUsage : dirigeant2.civiliteRepresentant2.personneLieePPNomUsage;
    }
    if (dirigeant2.civilitePersonnePhysique2.personneLieePPDateNaissance != null)
    {
        var dateTemp = new Date(parseInt(dirigeant2.civilitePersonnePhysique2.personneLieePPDateNaissance.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['D03.1'] = date;
    }
    else if (dirigeant2.civiliteRepresentant2.personneLieePPDateNaissance != null)
    {
        var dateTemp = new Date(parseInt(dirigeant2.civiliteRepresentant2.personneLieePPDateNaissance.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['D03.1'] = date;
    }
    if (not Value('id').of(dirigeant2.civilitePersonnePhysique2.personneLieePPLieuNaissancePays).eq('FRXXXXX')and dirigeant2.civilitePersonnePhysique2.personneLieePPNomNaissance != null)
    {
        var idPays = dirigeant2.civilitePersonnePhysique2.personneLieePPLieuNaissancePays.getId();
        var result = idPays.match(regEx)[0];
        occurrence['D03.2'] = result;
    }
    else if (Value('id').of(dirigeant2.civilitePersonnePhysique2.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
    {
        occurrence['D03.2'] = dirigeant2.civilitePersonnePhysique2.personneLieePPLieuNaissanceCommune.getId();
    }
    else if (not Value('id').of(dirigeant2.civiliteRepresentant2.personneLieePPLieuNaissancePays).eq('FRXXXXX') and dirigeant2.civiliteRepresentant2.personneLieePPNomNaissance != null)
    {
        var idPays = dirigeant2.civiliteRepresentant2.personneLieePPLieuNaissancePays.getId();
        var result = idPays.match(regEx)[0];
        occurrence['D03.2'] = result;
    }
    else if (Value('id').of(dirigeant2.civiliteRepresentant2.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
    {
        occurrence['D03.2'] = dirigeant2.civiliteRepresentant2.personneLieePPLieuNaissanceCommune.getId();
    }
    if (not Value('id').of(dirigeant2.civilitePersonnePhysique2.personneLieePPLieuNaissancePays).eq('FRXXXXX')and dirigeant2.civilitePersonnePhysique2.personneLieePPNomNaissance != null)
    {
        occurrence['D03.3'] = dirigeant2.civilitePersonnePhysique2.personneLieePPLieuNaissancePays.getLabel();
    }
    else if (not Value('id').of(dirigeant2.civiliteRepresentant2.personneLieePPLieuNaissancePays).eq('FRXXXXX')and dirigeant2.civiliteRepresentant2.personneLieePPNomNaissance != null)
    {
        occurrence['D03.3'] = dirigeant2.civiliteRepresentant2.personneLieePPLieuNaissancePays.getLabel();
    }
    if (Value('id').of(dirigeant2.civilitePersonnePhysique2.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
    {
        occurrence['D03.4'] = dirigeant2.civilitePersonnePhysique2.personneLieePPLieuNaissanceCommune.getLabel();
    }
    else if (Value('id').of(dirigeant2.civiliteRepresentant2.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
    {
        occurrence['D03.4'] = dirigeant2.civiliteRepresentant2.personneLieePPLieuNaissanceCommune.getLabel();
    }
    if (Value('id').of(dirigeant2.adresseDirigeant2.siegeAdressePays).eq('FRXXXXX'))
    {
        occurrence['D04.3'] = dirigeant2.adresseDirigeant2.siegeAdresseCommune.getId();
    }
    else if (not Value('id').of(dirigeant2.adresseDirigeant2.siegeAdressePays).eq('FRXXXXX') and dirigeant2.adresseDirigeant2.siegeAdresseVille)
    {
        var idPays = dirigeant2.adresseDirigeant2.siegeAdressePays.getId();
        var result = idPays.match(regEx)[0];
        occurrence['D04.3'] = result;
    }
    if (dirigeant2.adresseDirigeant2.siegeAdresseNumVoie != null)
    {
        occurrence['D04.5'] = dirigeant2.adresseDirigeant2.siegeAdresseNumVoie;
    }
    if (dirigeant2.adresseDirigeant2.siegeAdresseIndiceVoie != null)
    {
        var monId1 = Value('id').of(dirigeant2.adresseDirigeant2.siegeAdresseIndiceVoie)._eval();
        occurrence['D04.6'] = monId1;
    }
    if (dirigeant2.adresseDirigeant2.siegeAdresseDistributionSpecialeVoie != null)
    {
        occurrence['D04.7'] = dirigeant2.adresseDirigeant2.siegeAdresseDistributionSpecialeVoie;
    }
	if (dirigeant2.adresseDirigeant2.siegeAdresseCodePostal != null)
    {
    occurrence['D04.8'] = dirigeant2.adresseDirigeant2.siegeAdresseCodePostal;
	}
	else if (dirigeant2.adresseDirigeant2.siegeAdresseCodePostaEtranger != null)
	{
	occurrence['D04.8'] = dirigeant2.adresseDirigeant2.siegeAdresseCodePostaEtranger;
	}
    if (dirigeant2.adresseDirigeant2.siegeAdresseComplementVoie != null)
    {
        occurrence['D04.10'] = dirigeant2.adresseDirigeant2.siegeAdresseComplementVoie;
    }
    if (dirigeant2.adresseDirigeant2.siegeAdresseTypeVoie != null)
    {
        var monId1 = Value('id').of(dirigeant2.adresseDirigeant2.siegeAdresseTypeVoie)._eval();
        occurrence['D04.11'] = monId1;
    }
	if (dirigeant2.adresseDirigeant2.siegeAdresseNomVoie != null)
	{
		occurrence['D04.12'] = dirigeant2.adresseDirigeant2.siegeAdresseNomVoie;
	}
    if (Value('id').of(dirigeant2.adresseDirigeant2.siegeAdressePays).eq('FRXXXXX'))
    {
        occurrence['D04.13'] = dirigeant2.adresseDirigeant2.siegeAdresseCommune.getLabel();
    }
    else if (not Value('id').of(dirigeant2.adresseDirigeant2.siegeAdressePays).eq('FRXXXXX') and dirigeant2.adresseDirigeant2.siegeAdresseVille)
    {
        occurrence['D04.13'] = dirigeant2.adresseDirigeant2.siegeAdresseVille;
		occurrence['D04.14'] = dirigeant2.adresseDirigeant2.siegeAdressePays.getLabel();
    }


    // Groupe NPD

    occurrence['D21'] = dirigeant2.civilitePersonnePhysique2.personneLieePPNationalite != null ? dirigeant2.civilitePersonnePhysique2.personneLieePPNationalite : dirigeant2.civiliteRepresentant2.personneLieePPNationalite;

    // Groupe GCS

    /* if (Value('id').of(dirigeant2.personneLieeQualite).eq('28') or Value('id').of(dirigeant2.personneLieeQualite).eq('74')) { */

    // Groupe ISS

    /* var nirDeclarant = socialdirigeant2.voletSocialNumeroSecuriteSocialTNS;
    if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    occurrence['A10.1']  = nirDeclarant.substring(0, 13)
    occurrence['A10.2'] = nirDeclarant.substring(13, 15);
    } */

    // Groupe SNS

    /* if (socialdirigeant2.voletSocialActiviteExerceeAnterieurementTNS) {
    if(socialdirigeant2.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
    var dateTemp = new Date(parseInt(socialdirigeant2.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
    var year = dateTemp.getFullYear();
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
    var date = year + "-" + month + "-" + day ;
    occurrence['A21.2']= date;
    }
    occurrence['A21.3']= socialdirigeant2.voletSocialActiviteExerceeAnterieurementLibelleTNS;
    occurrence['A21.4']= socialdirigeant2.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
    occurrence['A21.6']= socialdirigeant2.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
    }
    occurrence['A22.3']= Value('id').of(socialdirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialdirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" :
    (Value('id').of(socialdirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
    if (Value('id').of(socialdirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
    occurrence['A22.4']= socialdirigeant2.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
    } else if (Value('id').of(socialdirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
    occurrence['A22.4']= "ENIM";
    }
    if (socialdirigeant2.voletSocialActiviteSimultaneeOUINON) {
    occurrence['A24.2'] = Value('id').of(socialdirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialdirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" :
    (Value('id').of(socialdirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
    }
    if (Value('id').of(socialdirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
    occurrence['A24.3'] = socialdirigeant2.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
    }
     */
    // Groupe CAS

    /* occurrence['A42.1']= ".";
    }*/

    occurrencesGRD.push(occurrence);
    occurrence = {};
}

// Dirigeant 3

if (Value('id').of(dirigeant3.personnaliteJuridique).eq('personnePhysique')or(Value('id').of(dirigeant3.personnaliteJuridique).eq('personneMorale')and not Value('id').of(dirigeant3.personneLieeQualite).eq('71')or not Value('id').of(dirigeant3.personneLieeQualite).eq('72')))
{

    //Groupe DIU : Dirigeant de l'entreprise

    occurrence['U50.1'] = "1";
    if (exploitation.informationActivite.etablissementDateDebutActivite != null)
    {
        var dateTemp = new Date(parseInt(exploitation.informationActivite.etablissementDateDebutActivite.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['U50.2'] = date;
    }
    else if (tag.formaliteSignatureDate != null)
    {
        var dateTemp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['U50.2'] = date;
    }
    occurrence['U51.1'] = dirigeant3.personneLieeQualite != null ? dirigeant3.personneLieeQualite.getId(): '';
    occurrence['U52'] = "P";

    // Groupe IPD : Identification du dirigeant de l'entreprise

   occurrence['D01.1'] = (Value('id').of(dirigeant3.civilitePersonnePhysique3.personnaliteGenre).eq('genreMasculin') or Value('id').of(dirigeant3.civiliteRepresentant3.personnaliteGenre).eq('genreMasculin')) ? "1" : "2";
    occurrence['D01.2'] = dirigeant3.civilitePersonnePhysique3.personneLieePPNomNaissance != null ? dirigeant3.civilitePersonnePhysique3.personneLieePPNomNaissance : dirigeant3.civiliteRepresentant3.personneLieePPNomNaissance;
    if (dirigeant3.civilitePersonnePhysique3.personneLieePPPrenom != null and dirigeant3.civilitePersonnePhysique3.personneLieePPPrenom[0].length > 0)
    {
        var prenoms = [];
        for (i = 0; i < dirigeant3.civilitePersonnePhysique3.personneLieePPPrenom.size(); i++)
        {
            prenoms.push(dirigeant3.civilitePersonnePhysique3.personneLieePPPrenom[i]);
        }
        occurrence['D01.3'] = prenoms;
    }
    else if (dirigeant3.civiliteRepresentant3.personneLieePPPrenom != null)
    {
        var prenoms = [];
        for (i = 0; i < dirigeant3.civiliteRepresentant3.personneLieePPPrenom.size(); i++)
        {
            prenoms.push(dirigeant3.civiliteRepresentant3.personneLieePPPrenom[i]);
        }
        occurrence['D01.3'] = prenoms;
    }
    if (dirigeant3.civilitePersonnePhysique3.personneLieePPNomUsage != null or dirigeant3.civiliteRepresentant3.personneLieePPNomUsage != null)
    {
        occurrence['D01.4'] = dirigeant3.civilitePersonnePhysique3.personneLieePPNomUsage != null ? dirigeant3.civilitePersonnePhysique3.personneLieePPNomUsage : dirigeant3.civiliteRepresentant3.personneLieePPNomUsage;
    }
    if (dirigeant3.civilitePersonnePhysique3.personneLieePPDateNaissance != null)
    {
        var dateTemp = new Date(parseInt(dirigeant3.civilitePersonnePhysique3.personneLieePPDateNaissance.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['D03.1'] = date;
    }
    else if (dirigeant3.civiliteRepresentant3.personneLieePPDateNaissance != null)
    {
        var dateTemp = new Date(parseInt(dirigeant3.civiliteRepresentant3.personneLieePPDateNaissance.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['D03.1'] = date;
    }
    if (not Value('id').of(dirigeant3.civilitePersonnePhysique3.personneLieePPLieuNaissancePays).eq('FRXXXXX')and dirigeant3.civilitePersonnePhysique3.personneLieePPNomNaissance != null)
    {
        var idPays = dirigeant3.civilitePersonnePhysique3.personneLieePPLieuNaissancePays.getId();
        var result = idPays.match(regEx)[0];
        occurrence['D03.2'] = result;
    }
    else if (Value('id').of(dirigeant3.civilitePersonnePhysique3.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
    {
        occurrence['D03.2'] = dirigeant3.civilitePersonnePhysique3.personneLieePPLieuNaissanceCommune.getId();
    }
    else if (not Value('id').of(dirigeant3.civiliteRepresentant3.personneLieePPLieuNaissancePays).eq('FRXXXXX') and dirigeant3.adresseDirigeant3.siegeAdresseVille)
    {
        var idPays = dirigeant3.civiliteRepresentant3.personneLieePPLieuNaissancePays.getId();
        var result = idPays.match(regEx)[0];
        occurrence['D03.2'] = result;
    }
    else if (Value('id').of(dirigeant3.civiliteRepresentant3.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
    {
        occurrence['D03.2'] = dirigeant3.civiliteRepresentant3.personneLieePPLieuNaissanceCommune.getId();
    }
    if (not Value('id').of(dirigeant3.civilitePersonnePhysique3.personneLieePPLieuNaissancePays).eq('FRXXXXX')and dirigeant3.civilitePersonnePhysique3.personneLieePPNomNaissance != null)
    {
        occurrence['D03.3'] = dirigeant3.civilitePersonnePhysique3.personneLieePPLieuNaissancePays.getLabel();
    }
    else if (not Value('id').of(dirigeant3.civiliteRepresentant3.personneLieePPLieuNaissancePays).eq('FRXXXXX')and dirigeant3.civiliteRepresentant3.personneLieePPNomNaissance != null)
    {
        occurrence['D03.3'] = dirigeant3.civiliteRepresentant3.personneLieePPLieuNaissancePays.getLabel();
    }
    if (Value('id').of(dirigeant3.civilitePersonnePhysique3.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
    {
        occurrence['D03.4'] = dirigeant3.civilitePersonnePhysique3.personneLieePPLieuNaissanceCommune.getLabel();
    }
    else if (Value('id').of(dirigeant3.civiliteRepresentant3.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
    {
        occurrence['D03.4'] = dirigeant3.civiliteRepresentant3.personneLieePPLieuNaissanceCommune.getLabel();
    }
    if (Value('id').of(dirigeant3.adresseDirigeant3.siegeAdressePays).eq('FRXXXXX'))
    {
        occurrence['D04.3'] = dirigeant3.adresseDirigeant3.siegeAdresseCommune.getId();
    }
    else if (not Value('id').of(dirigeant3.adresseDirigeant3.siegeAdressePays).eq('FRXXXXX') and dirigeant3.adresseDirigeant3.siegeAdresseVille)
    {
        var idPays = dirigeant3.adresseDirigeant3.siegeAdressePays.getId();
        var result = idPays.match(regEx)[0];
        occurrence['D04.3'] = result;
    }
    if (dirigeant3.adresseDirigeant3.siegeAdresseNumVoie != null)
    {
        occurrence['D04.5'] = dirigeant3.adresseDirigeant3.siegeAdresseNumVoie;
    }
    if (dirigeant3.adresseDirigeant3.siegeAdresseIndiceVoie != null)
    {
        var monId1 = Value('id').of(dirigeant3.adresseDirigeant3.siegeAdresseIndiceVoie)._eval();
        occurrence['D04.6'] = monId1;
    }
    if (dirigeant3.adresseDirigeant3.siegeAdresseDistributionSpecialeVoie != null)
    {
        occurrence['D04.7'] = dirigeant3.adresseDirigeant3.siegeAdresseDistributionSpecialeVoie;
    }
   	if (dirigeant3.adresseDirigeant3.siegeAdresseCodePostal != null)
    {
		occurrence['D04.8'] = dirigeant3.adresseDirigeant3.siegeAdresseCodePostal;
	}
	else if (dirigeant3.adresseDirigeant3.siegeAdresseCodePostaEtranger != null)
	{
		occurrence['D04.8'] = dirigeant3.adresseDirigeant3.siegeAdresseCodePostaEtranger;
	}
    if (dirigeant3.adresseDirigeant3.siegeAdresseComplementVoie != null)
    {
        occurrence['D04.10'] = dirigeant3.adresseDirigeant3.siegeAdresseComplementVoie;
    }
    if (dirigeant3.adresseDirigeant3.siegeAdresseTypeVoie != null)
    {
        var monId1 = Value('id').of(dirigeant3.adresseDirigeant3.siegeAdresseTypeVoie)._eval();
        occurrence['D04.11'] = monId1;
    }
    if (dirigeant3.adresseDirigeant3.siegeAdresseNomVoie != null)
	{
		occurrence['D04.12'] = dirigeant3.adresseDirigeant3.siegeAdresseNomVoie;
	}
    if (Value('id').of(dirigeant3.adresseDirigeant3.siegeAdressePays).eq('FRXXXXX'))
    {
        occurrence['D04.13'] = dirigeant3.adresseDirigeant3.siegeAdresseCommune.getLabel();
    }
    else if (not Value('id').of(dirigeant3.adresseDirigeant3.siegeAdressePays).eq('FRXXXXX') and dirigeant3.adresseDirigeant3.siegeAdresseVille)
    {
        occurrence['D04.13'] = dirigeant3.adresseDirigeant3.siegeAdresseVille;
		occurrence['D04.14'] = dirigeant3.adresseDirigeant3.siegeAdressePays.getLabel();
    }


    // Groupe NPD

    occurrence['D21'] = dirigeant3.civilitePersonnePhysique3.personneLieePPNationalite != null ? dirigeant3.civilitePersonnePhysique3.personneLieePPNationalite : dirigeant3.civiliteRepresentant3.personneLieePPNationalite;

    // Groupe GCS

    /* if (Value('id').of(dirigeant3.personneLieeQualite).eq('28') or Value('id').of(dirigeant3.personneLieeQualite).eq('74')) { */

    // Groupe ISS

    /* var nirDeclarant = socialdirigeant3.voletSocialNumeroSecuriteSocialTNS;
    if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    occurrence['A10.1']  = nirDeclarant.substring(0, 13)
    occurrence['A10.2'] = nirDeclarant.substring(13, 15);
    } */

    // Groupe SNS

    /* if (socialdirigeant3.voletSocialActiviteExerceeAnterieurementTNS) {
    if(socialdirigeant3.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
    var dateTemp = new Date(parseInt(socialdirigeant3.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
    var year = dateTemp.getFullYear();
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
    var date = year + "-" + month + "-" + day ;
    occurrence['A21.2']= date;
    }
    occurrence['A21.3']= socialdirigeant3.voletSocialActiviteExerceeAnterieurementLibelleTNS;
    occurrence['A21.4']= socialdirigeant3.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
    occurrence['A21.6']= socialdirigeant3.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
    }
    occurrence['A22.3']= Value('id').of(socialdirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialdirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" :
    (Value('id').of(socialdirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
    if (Value('id').of(socialdirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
    occurrence['A22.4']= socialdirigeant3.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
    } else if (Value('id').of(socialdirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
    occurrence['A22.4']= "ENIM";
    }
    if (socialdirigeant3.voletSocialActiviteSimultaneeOUINON) {
    occurrence['A24.2'] = Value('id').of(socialdirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialdirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" :
    (Value('id').of(socialdirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
    }
    if (Value('id').of(socialdirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
    occurrence['A24.3'] = socialdirigeant3.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
    }
     */
    // Groupe CAS

    /* occurrence['A42.1']= ".";
    }*/

    occurrencesGRD.push(occurrence);
    occurrence = {};
}
// Dirigeant 4

// Dirigeant 4

if (Value('id').of(dirigeant4.personnaliteJuridique).eq('personnePhysique')or(Value('id').of(dirigeant4.personnaliteJuridique).eq('personneMorale')and not Value('id').of(dirigeant4.personneLieeQualite).eq('71')or not Value('id').of(dirigeant4.personneLieeQualite).eq('72')))
{

    //Groupe DIU : Dirigeant de l'entreprise

    occurrence['U50.1'] = "1";
    if (exploitation.informationActivite.etablissementDateDebutActivite != null)
    {
        var dateTemp = new Date(parseInt(exploitation.informationActivite.etablissementDateDebutActivite.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['U50.2'] = date;
    }
    else if (tag.formaliteSignatureDate != null)
    {
        var dateTemp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['U50.2'] = date;
    }
     occurrence['U51.1'] = dirigeant4.personneLieeQualite != null ? dirigeant4.personneLieeQualite.getId(): '';
    occurrence['U52'] = "P";

    // Groupe IPD : Identification du dirigeant de l'entreprise

    occurrence['D01.1'] = (Value('id').of(dirigeant4.civilitePersonnePhysique4.personnaliteGenre).eq('genreMasculin') or Value('id').of(dirigeant4.civiliteRepresentant4.personnaliteGenre).eq('genreMasculin')) ? "1" : "2";
    occurrence['D01.2'] = dirigeant4.civilitePersonnePhysique4.personneLieePPNomNaissance != null ? dirigeant4.civilitePersonnePhysique4.personneLieePPNomNaissance : dirigeant4.civiliteRepresentant4.personneLieePPNomNaissance;
    if (dirigeant4.civilitePersonnePhysique4.personneLieePPPrenom != null and dirigeant4.civilitePersonnePhysique4.personneLieePPPrenom[0].length > 0)
    {
        var prenoms = [];
        for (i = 0; i < dirigeant4.civilitePersonnePhysique4.personneLieePPPrenom.size(); i++)
        {
            prenoms.push(dirigeant4.civilitePersonnePhysique4.personneLieePPPrenom[i]);
        }
        occurrence['D01.3'] = prenoms;
    }
    else if (dirigeant4.civiliteRepresentant4.personneLieePPPrenom != null)
    {
        var prenoms = [];
        for (i = 0; i < dirigeant4.civiliteRepresentant4.personneLieePPPrenom.size(); i++)
        {
            prenoms.push(dirigeant4.civiliteRepresentant4.personneLieePPPrenom[i]);
        }
        occurrence['D01.3'] = prenoms;
    }
    if (dirigeant4.civilitePersonnePhysique4.personneLieePPNomUsage != null or dirigeant4.civiliteRepresentant4.personneLieePPNomUsage != null)
    {
        occurrence['D01.4'] = dirigeant4.civilitePersonnePhysique4.personneLieePPNomUsage != null ? dirigeant4.civilitePersonnePhysique4.personneLieePPNomUsage : dirigeant4.civiliteRepresentant4.personneLieePPNomUsage;
    }
    if (dirigeant4.civilitePersonnePhysique4.personneLieePPDateNaissance != null)
    {
        var dateTemp = new Date(parseInt(dirigeant4.civilitePersonnePhysique4.personneLieePPDateNaissance.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['D03.1'] = date;
    }
    else if (dirigeant4.civiliteRepresentant4.personneLieePPDateNaissance != null)
    {
        var dateTemp = new Date(parseInt(dirigeant4.civiliteRepresentant4.personneLieePPDateNaissance.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['D03.1'] = date;
    }
    if (not Value('id').of(dirigeant4.civilitePersonnePhysique4.personneLieePPLieuNaissancePays).eq('FRXXXXX')and dirigeant4.civilitePersonnePhysique4.personneLieePPNomNaissance != null)
    {
        var idPays = dirigeant4.civilitePersonnePhysique4.personneLieePPLieuNaissancePays.getId();
        var result = idPays.match(regEx)[0];
        occurrence['D03.2'] = result;
    }
    else if (Value('id').of(dirigeant4.civilitePersonnePhysique4.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
    {
        occurrence['D03.2'] = dirigeant4.civilitePersonnePhysique4.personneLieePPLieuNaissanceCommune.getId();
    }
    else if (not Value('id').of(dirigeant4.civiliteRepresentant4.personneLieePPLieuNaissancePays).eq('FRXXXXX') and dirigeant4.adresseDirigeant4.siegeAdresseVille )
    {
        var idPays = dirigeant4.civiliteRepresentant4.personneLieePPLieuNaissancePays.getId();
        var result = idPays.match(regEx)[0];
        occurrence['D03.2'] = result;
    }
    else if (Value('id').of(dirigeant4.civiliteRepresentant4.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
    {
        occurrence['D03.2'] = dirigeant4.civiliteRepresentant4.personneLieePPLieuNaissanceCommune.getId();
    }
    if (not Value('id').of(dirigeant4.civilitePersonnePhysique4.personneLieePPLieuNaissancePays).eq('FRXXXXX')and dirigeant4.civilitePersonnePhysique4.personneLieePPNomNaissance != null)
    {
        occurrence['D03.3'] = dirigeant4.civilitePersonnePhysique4.personneLieePPLieuNaissancePays.getLabel();
    }
    else if (not Value('id').of(dirigeant4.civiliteRepresentant4.personneLieePPLieuNaissancePays).eq('FRXXXXX')and dirigeant4.civiliteRepresentant4.personneLieePPNomNaissance != null)
    {
        occurrence['D03.3'] = dirigeant4.civiliteRepresentant4.personneLieePPLieuNaissancePays.getLabel();
    }
    if (Value('id').of(dirigeant4.civilitePersonnePhysique4.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
    {
        occurrence['D03.4'] = dirigeant4.civilitePersonnePhysique4.personneLieePPLieuNaissanceCommune.getLabel();
    }
    else if (Value('id').of(dirigeant4.civiliteRepresentant4.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
    {
        occurrence['D03.4'] = dirigeant4.civiliteRepresentant4.personneLieePPLieuNaissanceCommune.getLabel();
    }
    if (Value('id').of(dirigeant4.adresseDirigeant4.siegeAdressePays).eq('FRXXXXX'))
    {
        occurrence['D04.3'] = dirigeant4.adresseDirigeant4.siegeAdresseCommune.getId();
    }
    else if (not Value('id').of(dirigeant4.adresseDirigeant4.siegeAdressePays).eq('FRXXXXX') and dirigeant4.adresseDirigeant4.siegeAdresseVille)
    {
        var idPays = dirigeant4.adresseDirigeant4.siegeAdressePays.getId();
        var result = idPays.match(regEx)[0];
        occurrence['D04.3'] = result;
    }
    if (dirigeant4.adresseDirigeant4.siegeAdresseNumVoie != null)
    {
        occurrence['D04.5'] = dirigeant4.adresseDirigeant4.siegeAdresseNumVoie;
    }
    if (dirigeant4.adresseDirigeant4.siegeAdresseIndiceVoie != null)
    {
        var monId1 = Value('id').of(dirigeant4.adresseDirigeant4.siegeAdresseIndiceVoie)._eval();
        occurrence['D04.6'] = monId1;
    }
    if (dirigeant4.adresseDirigeant4.siegeAdresseDistributionSpecialeVoie != null)
    {
        occurrence['D04.7'] = dirigeant4.adresseDirigeant4.siegeAdresseDistributionSpecialeVoie;
    }

  	if (dirigeant4.adresseDirigeant4.siegeAdresseCodePostal != null)
    {
		occurrence['D04.8'] = dirigeant4.adresseDirigeant4.siegeAdresseCodePostal;
	}
	else if (dirigeant4.adresseDirigeant4.siegeAdresseCodePostaEtranger != null)
	{
		occurrence['D04.8'] = dirigeant4.adresseDirigeant4.siegeAdresseCodePostaEtranger;
	}
    if (dirigeant4.adresseDirigeant4.siegeAdresseComplementVoie != null)
    {
        occurrence['D04.10'] = dirigeant4.adresseDirigeant4.siegeAdresseComplementVoie;
    }
    if (dirigeant4.adresseDirigeant4.siegeAdresseTypeVoie != null)
    {
        var monId1 = Value('id').of(dirigeant4.adresseDirigeant4.siegeAdresseTypeVoie)._eval();
        occurrence['D04.11'] = monId1;
    }
    if (dirigeant4.adresseDirigeant4.siegeAdresseNomVoie != null)
	{
		occurrence['D04.12'] = dirigeant4.adresseDirigeant4.siegeAdresseNomVoie;
	}
    if (Value('id').of(dirigeant4.adresseDirigeant4.siegeAdressePays).eq('FRXXXXX'))
    {
        occurrence['D04.13'] = dirigeant4.adresseDirigeant4.siegeAdresseCommune.getLabel();
    }
    else if (not Value('id').of(dirigeant4.adresseDirigeant4.siegeAdressePays).eq('FRXXXXX') and dirigeant4.adresseDirigeant4.siegeAdresseVille)
    {
        occurrence['D04.13'] = dirigeant4.adresseDirigeant4.siegeAdresseVille;
		occurrence['D04.14'] = dirigeant4.adresseDirigeant4.siegeAdressePays.getLabel();
    }


    // Groupe NPD

    occurrence['D21'] = dirigeant4.civilitePersonnePhysique4.personneLieePPNationalite != null ? dirigeant4.civilitePersonnePhysique4.personneLieePPNationalite : dirigeant4.civiliteRepresentant4.personneLieePPNationalite;

    // Groupe GCS

    /* if (Value('id').of(dirigeant4.personneLieeQualite).eq('28') or Value('id').of(dirigeant4.personneLieeQualite).eq('74')) { */

    // Groupe ISS

    /* var nirDeclarant = socialdirigeant4.voletSocialNumeroSecuriteSocialTNS;
    if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    occurrence['A10.1']  = nirDeclarant.substring(0, 13)
    occurrence['A10.2'] = nirDeclarant.substring(13, 15);
    } */

    // Groupe SNS

    /* if (socialdirigeant4.voletSocialActiviteExerceeAnterieurementTNS) {
    if(socialdirigeant4.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
    var dateTemp = new Date(parseInt(socialdirigeant4.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
    var year = dateTemp.getFullYear();
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
    var date = year + "-" + month + "-" + day ;
    occurrence['A21.2']= date;
    }
    occurrence['A21.3']= socialdirigeant4.voletSocialActiviteExerceeAnterieurementLibelleTNS;
    occurrence['A21.4']= socialdirigeant4.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
    occurrence['A21.6']= socialdirigeant4.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
    }
    occurrence['A22.3']= Value('id').of(socialdirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialdirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" :
    (Value('id').of(socialdirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
    if (Value('id').of(socialdirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
    occurrence['A22.4']= socialdirigeant4.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
    } else if (Value('id').of(socialdirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
    occurrence['A22.4']= "ENIM";
    }
    if (socialdirigeant4.voletSocialActiviteSimultaneeOUINON) {
    occurrence['A24.2'] = Value('id').of(socialdirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialdirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" :
    (Value('id').of(socialdirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
    }
    if (Value('id').of(socialdirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
    occurrence['A24.3'] = socialdirigeant4.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
    }
     */
    // Groupe CAS

    /* occurrence['A42.1']= ".";
    }*/

    occurrencesGRD.push(occurrence);
    occurrence = {};
}

// Dirigeant 5

if (Value('id').of(dirigeant5.personnaliteJuridique).eq('personnePhysique')or(Value('id').of(dirigeant5.personnaliteJuridique).eq('personneMorale')and not Value('id').of(dirigeant5.personneLieeQualite).eq('71')or not Value('id').of(dirigeant5.personneLieeQualite).eq('72')))
{

    //Groupe DIU : Dirigeant de l'entreprise

    occurrence['U50.1'] = "1";
    if (exploitation.informationActivite.etablissementDateDebutActivite != null)
    {
        var dateTemp = new Date(parseInt(exploitation.informationActivite.etablissementDateDebutActivite.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['U50.2'] = date;
    }
    else if (tag.formaliteSignatureDate != null)
    {
        var dateTemp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['U50.2'] = date;
    }
     occurrence['U51.1'] = dirigeant5.personneLieeQualite != null ? dirigeant5.personneLieeQualite.getId(): '';
    occurrence['U52'] = "P";

    // Groupe IPD : Identification du dirigeant de l'entreprise

    occurrence['D01.1'] = (Value('id').of(dirigeant5.civilitePersonnePhysique5.personnaliteGenre).eq('genreMasculin') or Value('id').of(dirigeant5.civiliteRepresentant5.personnaliteGenre).eq('genreMasculin')) ? "1" : "2";
    occurrence['D01.2'] = dirigeant5.civilitePersonnePhysique5.personneLieePPNomNaissance != null ? dirigeant5.civilitePersonnePhysique5.personneLieePPNomNaissance : dirigeant5.civiliteRepresentant5.personneLieePPNomNaissance;
    if (dirigeant5.civilitePersonnePhysique5.personneLieePPPrenom != null and dirigeant5.civilitePersonnePhysique5.personneLieePPPrenom[0].length > 0)
    {
        var prenoms = [];
        for (i = 0; i < dirigeant5.civilitePersonnePhysique5.personneLieePPPrenom.size(); i++)
        {
            prenoms.push(dirigeant5.civilitePersonnePhysique5.personneLieePPPrenom[i]);
        }
        occurrence['D01.3'] = prenoms;
    }
    else if (dirigeant5.civiliteRepresentant5.personneLieePPPrenom != null)
    {
        var prenoms = [];
        for (i = 0; i < dirigeant5.civiliteRepresentant5.personneLieePPPrenom.size(); i++)
        {
            prenoms.push(dirigeant5.civiliteRepresentant5.personneLieePPPrenom[i]);
        }
        occurrence['D01.3'] = prenoms;
    }
    if (dirigeant5.civilitePersonnePhysique5.personneLieePPNomUsage != null or dirigeant5.civiliteRepresentant5.personneLieePPNomUsage != null)
    {
        occurrence['D01.4'] = dirigeant5.civilitePersonnePhysique5.personneLieePPNomUsage != null ? dirigeant5.civilitePersonnePhysique5.personneLieePPNomUsage : dirigeant5.civiliteRepresentant5.personneLieePPNomUsage;
    }
    if (dirigeant5.civilitePersonnePhysique5.personneLieePPDateNaissance != null)
    {
        var dateTemp = new Date(parseInt(dirigeant5.civilitePersonnePhysique5.personneLieePPDateNaissance.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['D03.1'] = date;
    }
    else if (dirigeant5.civiliteRepresentant5.personneLieePPDateNaissance != null)
    {
        var dateTemp = new Date(parseInt(dirigeant5.civiliteRepresentant5.personneLieePPDateNaissance.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['D03.1'] = date;
    }
    if (not Value('id').of(dirigeant5.civilitePersonnePhysique5.personneLieePPLieuNaissancePays).eq('FRXXXXX')and dirigeant5.civilitePersonnePhysique5.personneLieePPNomNaissance != null)
    {
        var idPays = dirigeant5.civilitePersonnePhysique5.personneLieePPLieuNaissancePays.getId();
        var result = idPays.match(regEx)[0];
        occurrence['D03.2'] = result;
    }
    else if (Value('id').of(dirigeant5.civilitePersonnePhysique5.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
    {
        occurrence['D03.2'] = dirigeant5.civilitePersonnePhysique5.personneLieePPLieuNaissanceCommune.getId();
    }
    else if (not Value('id').of(dirigeant5.civiliteRepresentant5.personneLieePPLieuNaissancePays).eq('FRXXXXX') and dirigeant5.adresseDirigeant5.siegeAdresseVille)
    {
        var idPays = dirigeant5.civiliteRepresentant5.personneLieePPLieuNaissancePays.getId();
        var result = idPays.match(regEx)[0];
        occurrence['D03.2'] = result;
    }
    else if (Value('id').of(dirigeant5.civiliteRepresentant5.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
    {
        occurrence['D03.2'] = dirigeant5.civiliteRepresentant5.personneLieePPLieuNaissanceCommune.getId();
    }
    if (not Value('id').of(dirigeant5.civilitePersonnePhysique5.personneLieePPLieuNaissancePays).eq('FRXXXXX')and dirigeant5.civilitePersonnePhysique5.personneLieePPNomNaissance != null)
    {
        occurrence['D03.3'] = dirigeant5.civilitePersonnePhysique5.personneLieePPLieuNaissancePays.getLabel();
    }
    else if (not Value('id').of(dirigeant5.civiliteRepresentant5.personneLieePPLieuNaissancePays).eq('FRXXXXX')and dirigeant5.civiliteRepresentant5.personneLieePPNomNaissance != null)
    {
        occurrence['D03.3'] = dirigeant5.civiliteRepresentant5.personneLieePPLieuNaissancePays.getLabel();
    }
    if (Value('id').of(dirigeant5.civilitePersonnePhysique5.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
    {
        occurrence['D03.4'] = dirigeant5.civilitePersonnePhysique5.personneLieePPLieuNaissanceCommune.getLabel();
    }
    else if (Value('id').of(dirigeant5.civiliteRepresentant5.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
    {
        occurrence['D03.4'] = dirigeant5.civiliteRepresentant5.personneLieePPLieuNaissanceCommune.getLabel();
    }
    if (Value('id').of(dirigeant5.adresseDirigeant5.siegeAdressePays).eq('FRXXXXX'))
    {
        occurrence['D04.3'] = dirigeant5.adresseDirigeant5.siegeAdresseCommune.getId();
    }
    else if (not Value('id').of(dirigeant5.adresseDirigeant5.siegeAdressePays).eq('FRXXXXX') and dirigeant5.adresseDirigeant5.siegeAdresseVille)
    {
        var idPays = dirigeant5.adresseDirigeant5.siegeAdressePays.getId();
        var result = idPays.match(regEx)[0];
        occurrence['D04.3'] = result;
    }
    if (dirigeant5.adresseDirigeant5.siegeAdresseNumVoie != null)
    {
        occurrence['D04.5'] = dirigeant5.adresseDirigeant5.siegeAdresseNumVoie;
    }
    if (dirigeant5.adresseDirigeant5.siegeAdresseIndiceVoie != null)
    {
        var monId1 = Value('id').of(dirigeant5.adresseDirigeant5.siegeAdresseIndiceVoie)._eval();
        occurrence['D04.6'] = monId1;
    }
    if (dirigeant5.adresseDirigeant5.siegeAdresseDistributionSpecialeVoie != null)
    {
        occurrence['D04.7'] = dirigeant5.adresseDirigeant5.siegeAdresseDistributionSpecialeVoie;
    }

  if (dirigeant5.adresseDirigeant5.siegeAdresseCodePostal != null)
    {
    occurrence['D04.8'] = dirigeant5.adresseDirigeant5.siegeAdresseCodePostal;
	}
	else if (dirigeant5.adresseDirigeant5.siegeAdresseCodePostaEtranger != null)
	{
	occurrence['D04.8'] = dirigeant5.adresseDirigeant5.siegeAdresseCodePostaEtranger;
	}

    if (dirigeant5.adresseDirigeant5.siegeAdresseComplementVoie != null)
    {
        occurrence['D04.10'] = dirigeant5.adresseDirigeant5.siegeAdresseComplementVoie;
    }
    if (dirigeant5.adresseDirigeant5.siegeAdresseTypeVoie != null)
    {
        var monId1 = Value('id').of(dirigeant5.adresseDirigeant5.siegeAdresseTypeVoie)._eval();
        occurrence['D04.11'] = monId1;
    }
	if (dirigeant5.adresseDirigeant5.siegeAdresseNomVoie != null)
	{
		occurrence['D04.12'] = dirigeant5.adresseDirigeant5.siegeAdresseNomVoie;
	}
    if (Value('id').of(dirigeant5.adresseDirigeant5.siegeAdressePays).eq('FRXXXXX'))
    {
        occurrence['D04.13'] = dirigeant5.adresseDirigeant5.siegeAdresseCommune.getLabel();
    }
    else if (not Value('id').of(dirigeant5.adresseDirigeant5.siegeAdressePays).eq('FRXXXXX') and dirigeant5.adresseDirigeant5.siegeAdresseVille)
    {
        occurrence['D04.13'] = dirigeant5.adresseDirigeant5.siegeAdresseVille;
		occurrence['D04.14'] = dirigeant5.adresseDirigeant5.siegeAdressePays.getLabel();
    }


    // Groupe NPD

    occurrence['D21'] = dirigeant5.civilitePersonnePhysique5.personneLieePPNationalite != null ? dirigeant5.civilitePersonnePhysique5.personneLieePPNationalite : dirigeant5.civiliteRepresentant5.personneLieePPNationalite;

    // Groupe GCS

    /* if (Value('id').of(dirigeant5.personneLieeQualite).eq('28') or Value('id').of(dirigeant5.personneLieeQualite).eq('74')) { */

    // Groupe ISS

    /* var nirDeclarant = socialdirigeant5.voletSocialNumeroSecuriteSocialTNS;
    if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    occurrence['A10.1']  = nirDeclarant.substring(0, 13)
    occurrence['A10.2'] = nirDeclarant.substring(13, 15);
    } */

    // Groupe SNS

    /* if (socialdirigeant5.voletSocialActiviteExerceeAnterieurementTNS) {
    if(socialdirigeant5.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
    var dateTemp = new Date(parseInt(socialdirigeant5.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
    var year = dateTemp.getFullYear();
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
    var date = year + "-" + month + "-" + day ;
    occurrence['A21.2']= date;
    }
    occurrence['A21.3']= socialdirigeant5.voletSocialActiviteExerceeAnterieurementLibelleTNS;
    occurrence['A21.4']= socialdirigeant5.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
    occurrence['A21.6']= socialdirigeant5.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
    }
    occurrence['A22.3']= Value('id').of(socialdirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialdirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" :
    (Value('id').of(socialdirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
    if (Value('id').of(socialdirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
    occurrence['A22.4']= socialdirigeant5.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
    } else if (Value('id').of(socialdirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
    occurrence['A22.4']= "ENIM";
    }
    if (socialdirigeant5.voletSocialActiviteSimultaneeOUINON) {
    occurrence['A24.2'] = Value('id').of(socialdirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialdirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" :
    (Value('id').of(socialdirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
    }
    if (Value('id').of(socialdirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
    occurrence['A24.3'] = socialdirigeant5.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
    }
     */
    // Groupe CAS

    /* occurrence['A42.1']= ".";
    }*/

    occurrencesGRD.push(occurrence);
    occurrence = {};
}

// Dirigeant 6

if (Value('id').of(dirigeant6.personnaliteJuridique).eq('personnePhysique')or(Value('id').of(dirigeant6.personnaliteJuridique).eq('personneMorale')and not Value('id').of(dirigeant6.personneLieeQualite).eq('71')or not Value('id').of(dirigeant6.personneLieeQualite).eq('72')))
{

    //Groupe DIU : Dirigeant de l'entreprise

    occurrence['U50.1'] = "1";
    if (exploitation.informationActivite.etablissementDateDebutActivite != null)
    {
        var dateTemp = new Date(parseInt(exploitation.informationActivite.etablissementDateDebutActivite.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['U50.2'] = date;
    }
    else if (tag.formaliteSignatureDate != null)
    {
        var dateTemp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['U50.2'] = date;
    }
     occurrence['U51.1'] = dirigeant6.personneLieeQualite != null ? dirigeant6.personneLieeQualite.getId(): '';
    occurrence['U52'] = "P";

    // Groupe IPD : Identification du dirigeant de l'entreprise

    occurrence['D01.1'] = (Value('id').of(dirigeant6.civilitePersonnePhysique6.personnaliteGenre).eq('genreMasculin') or Value('id').of(dirigeant6.civiliteRepresentant6.personnaliteGenre).eq('genreMasculin')) ? "1" : "2";
    occurrence['D01.2'] = dirigeant6.civilitePersonnePhysique6.personneLieePPNomNaissance != null ? dirigeant6.civilitePersonnePhysique6.personneLieePPNomNaissance : dirigeant6.civiliteRepresentant6.personneLieePPNomNaissance;
    if (dirigeant6.civilitePersonnePhysique6.personneLieePPPrenom != null and dirigeant6.civilitePersonnePhysique6.personneLieePPPrenom[0].length > 0)
    {
        var prenoms = [];
        for (i = 0; i < dirigeant6.civilitePersonnePhysique6.personneLieePPPrenom.size(); i++)
        {
            prenoms.push(dirigeant6.civilitePersonnePhysique6.personneLieePPPrenom[i]);
        }
        occurrence['D01.3'] = prenoms;
    }
    else if (dirigeant6.civiliteRepresentant6.personneLieePPPrenom != null)
    {
        var prenoms = [];
        for (i = 0; i < dirigeant6.civiliteRepresentant6.personneLieePPPrenom.size(); i++)
        {
            prenoms.push(dirigeant6.civiliteRepresentant6.personneLieePPPrenom[i]);
        }
        occurrence['D01.3'] = prenoms;
    }
    if (dirigeant6.civilitePersonnePhysique6.personneLieePPNomUsage != null or dirigeant6.civiliteRepresentant6.personneLieePPNomUsage != null)
    {
        occurrence['D01.4'] = dirigeant6.civilitePersonnePhysique6.personneLieePPNomUsage != null ? dirigeant6.civilitePersonnePhysique6.personneLieePPNomUsage : dirigeant6.civiliteRepresentant6.personneLieePPNomUsage;
    }
    if (dirigeant6.civilitePersonnePhysique6.personneLieePPDateNaissance != null)
    {
        var dateTemp = new Date(parseInt(dirigeant6.civilitePersonnePhysique6.personneLieePPDateNaissance.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['D03.1'] = date;
    }
    else if (dirigeant6.civiliteRepresentant6.personneLieePPDateNaissance != null)
    {
        var dateTemp = new Date(parseInt(dirigeant6.civiliteRepresentant6.personneLieePPDateNaissance.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['D03.1'] = date;
    }
    if (not Value('id').of(dirigeant6.civilitePersonnePhysique6.personneLieePPLieuNaissancePays).eq('FRXXXXX')and dirigeant6.civilitePersonnePhysique6.personneLieePPNomNaissance != null)
    {
        var idPays = dirigeant6.civilitePersonnePhysique6.personneLieePPLieuNaissancePays.getId();
        var result = idPays.match(regEx)[0];
        occurrence['D03.2'] = result;
    }
    else if (Value('id').of(dirigeant6.civilitePersonnePhysique6.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
    {
        occurrence['D03.2'] = dirigeant6.civilitePersonnePhysique6.personneLieePPLieuNaissanceCommune.getId();
    }
    else if (not Value('id').of(dirigeant6.civiliteRepresentant6.personneLieePPLieuNaissancePays).eq('FRXXXXX') and dirigeant6.adresseDirigeant6.siegeAdresseVille)
    {
        var idPays = dirigeant6.civiliteRepresentant6.personneLieePPLieuNaissancePays.getId();
        var result = idPays.match(regEx)[0];
        occurrence['D03.2'] = result;
    }
    else if (Value('id').of(dirigeant6.civiliteRepresentant6.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
    {
        occurrence['D03.2'] = dirigeant6.civiliteRepresentant6.personneLieePPLieuNaissanceCommune.getId();
    }
    if (not Value('id').of(dirigeant6.civilitePersonnePhysique6.personneLieePPLieuNaissancePays).eq('FRXXXXX')and dirigeant6.civilitePersonnePhysique6.personneLieePPNomNaissance != null)
    {
        occurrence['D03.3'] = dirigeant6.civilitePersonnePhysique6.personneLieePPLieuNaissancePays.getLabel();
    }
    else if (not Value('id').of(dirigeant6.civiliteRepresentant6.personneLieePPLieuNaissancePays).eq('FRXXXXX')and dirigeant6.civiliteRepresentant6.personneLieePPNomNaissance != null)
    {
        occurrence['D03.3'] = dirigeant6.civiliteRepresentant6.personneLieePPLieuNaissancePays.getLabel();
    }
    if (Value('id').of(dirigeant6.civilitePersonnePhysique6.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
    {
        occurrence['D03.4'] = dirigeant6.civilitePersonnePhysique6.personneLieePPLieuNaissanceCommune.getLabel();
    }
    else if (Value('id').of(dirigeant6.civiliteRepresentant6.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
    {
        occurrence['D03.4'] = dirigeant6.civiliteRepresentant6.personneLieePPLieuNaissanceCommune.getLabel();
    }
    if (Value('id').of(dirigeant6.adresseDirigeant6.siegeAdressePays).eq('FRXXXXX'))
    {
        occurrence['D04.3'] = dirigeant6.adresseDirigeant6.siegeAdresseCommune.getId();
    }
    else if (not Value('id').of(dirigeant6.adresseDirigeant6.siegeAdressePays).eq('FRXXXXX') and dirigeant6.adresseDirigeant6.siegeAdresseVille)
    {
        var idPays = dirigeant6.adresseDirigeant6.siegeAdressePays.getId();
        var result = idPays.match(regEx)[0];
        occurrence['D04.3'] = result;
    }
    if (dirigeant6.adresseDirigeant6.siegeAdresseNumVoie != null)
    {
        occurrence['D04.5'] = dirigeant6.adresseDirigeant6.siegeAdresseNumVoie;
    }
    if (dirigeant6.adresseDirigeant6.siegeAdresseIndiceVoie != null)
    {
        var monId1 = Value('id').of(dirigeant6.adresseDirigeant6.siegeAdresseIndiceVoie)._eval();
        occurrence['D04.6'] = monId1;
    }
    if (dirigeant6.adresseDirigeant6.siegeAdresseDistributionSpecialeVoie != null)
    {
        occurrence['D04.7'] = dirigeant6.adresseDirigeant6.siegeAdresseDistributionSpecialeVoie;
    }

   if (dirigeant6.adresseDirigeant6.siegeAdresseCodePostal != null)
    {
		occurrence['D04.8'] = dirigeant6.adresseDirigeant6.siegeAdresseCodePostal;
	}
	else if (dirigeant6.adresseDirigeant6.siegeAdresseCodePostaEtranger != null)
	{
		occurrence['D04.8'] = dirigeant6.adresseDirigeant6.siegeAdresseCodePostaEtranger;
	}

    if (dirigeant6.adresseDirigeant6.siegeAdresseComplementVoie != null)
    {
        occurrence['D04.10'] = dirigeant6.adresseDirigeant6.siegeAdresseComplementVoie;
    }
    if (dirigeant6.adresseDirigeant6.siegeAdresseTypeVoie != null)
    {
        var monId1 = Value('id').of(dirigeant6.adresseDirigeant6.siegeAdresseTypeVoie)._eval();
        occurrence['D04.11'] = monId1;
    }
	if (dirigeant6.adresseDirigeant6.siegeAdresseNomVoie != null)
    {
		occurrence['D04.12'] = dirigeant6.adresseDirigeant6.siegeAdresseNomVoie;
    }
	if (Value('id').of(dirigeant6.adresseDirigeant6.siegeAdressePays).eq('FRXXXXX'))
    {
        occurrence['D04.13'] = dirigeant6.adresseDirigeant6.siegeAdresseCommune.getLabel();
    }
   else if (not Value('id').of(dirigeant6.adresseDirigeant6.siegeAdressePays).eq('FRXXXXX') and dirigeant6.adresseDirigeant6.siegeAdresseVille)
    {
        occurrence['D04.13'] = dirigeant6.adresseDirigeant6.siegeAdresseVille;
		occurrence['D04.14'] = dirigeant6.adresseDirigeant6.siegeAdressePays.getLabel();
    }
 

    // Groupe NPD

    occurrence['D21'] = dirigeant6.civilitePersonnePhysique6.personneLieePPNationalite != null ? dirigeant6.civilitePersonnePhysique6.personneLieePPNationalite : dirigeant6.civiliteRepresentant6.personneLieePPNationalite;

    // Groupe GCS

    /* if (Value('id').of(dirigeant6.personneLieeQualite).eq('28') or Value('id').of(dirigeant6.personneLieeQualite).eq('74')) { */

    // Groupe ISS

    /* var nirDeclarant = socialdirigeant6.voletSocialNumeroSecuriteSocialTNS;
    if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    occurrence['A10.1']  = nirDeclarant.substring(0, 13)
    occurrence['A10.2'] = nirDeclarant.substring(13, 15);
    } */

    // Groupe SNS

    /* if (socialdirigeant6.voletSocialActiviteExerceeAnterieurementTNS) {
    if(socialdirigeant6.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
    var dateTemp = new Date(parseInt(socialdirigeant6.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
    var year = dateTemp.getFullYear();
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
    var date = year + "-" + month + "-" + day ;
    occurrence['A21.2']= date;
    }
    occurrence['A21.3']= socialdirigeant6.voletSocialActiviteExerceeAnterieurementLibelleTNS;
    occurrence['A21.4']= socialdirigeant6.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
    occurrence['A21.6']= socialdirigeant6.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
    }
    occurrence['A22.3']= Value('id').of(socialdirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialdirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" :
    (Value('id').of(socialdirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
    if (Value('id').of(socialdirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
    occurrence['A22.4']= socialdirigeant6.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
    } else if (Value('id').of(socialdirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
    occurrence['A22.4']= "ENIM";
    }
    if (socialdirigeant6.voletSocialActiviteSimultaneeOUINON) {
    occurrence['A24.2'] = Value('id').of(socialdirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialdirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" :
    (Value('id').of(socialdirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
    }
    if (Value('id').of(socialdirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
    occurrence['A24.3'] = socialdirigeant6.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
    }
     */
    // Groupe CAS

    /* occurrence['A42.1']= ".";
    }*/

    occurrencesGRD.push(occurrence);
    occurrence = {};
}

// Dirigeant 7
// Dirigeant 8
// Dirigeant 9
// Dirigeant 10

occurrencesGRD.forEach(function (value, i)
{
    var idx = i + 1;
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/DIU/U50/U50.1'] = value['U50.1'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/DIU/U50/U50.2'] = value['U50.2'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/DIU/U51/U51.1'] = value['U51.1'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/DIU/U52'] = value['U52'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D01/D01.1'] = value['D01.1'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D01/D01.2'] = value['D01.2'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D01/D01.3'] = value['D01.3'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D01/D01.4'] = value['D01.4'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D03/D03.1'] = value['D03.1'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D03/D03.2'] = value['D03.2'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D03/D03.3'] = value['D03.3'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D03/D03.4'] = value['D03.4'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.3'] = value['D04.3'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.5'] = value['D04.5'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.6'] = value['D04.6'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.7'] = value['D04.7'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.8'] = value['D04.8'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.10'] = value['D04.10'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.11'] = value['D04.11'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.12'] = value['D04.12'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.13'] = value['D04.13'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.14'] = value['D04.14'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/NPD/D21'] = value['D21'];
    /* 	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/ISS/A10/A10.1']= value['A10.1'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/ISS/A10/A10.2']= value['A10.2'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A21/A21.2']= value['A21.2'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A21/A21.3']= value['A21.3'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A21/A21.4']= value['A21.4'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A21/A21.6']= value['A21.6'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A22/A22.3']= value['A22.3'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A22/A22.4']= value['A22.4'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A24/A24.2']= value['A24.2'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A24/A24.3']= value['A24.3'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/CAS/A42/A42.1']= value['A42.1']; */
}
);

// Groupe GDR : Dirigeant personne morale

var occurrencesGDR = [];
var occurrence = {};

// Dirigeant 1

if (Value('id').of(dirigeant1.personnaliteJuridique).eq('personneMorale'))
{

    //Groupe DIU : Dirigeant de l'entreprise

    occurrence['U50.1'] = "1";
    if (exploitation.informationActivite.etablissementDateDebutActivite != null)
    {
        var dateTemp = new Date(parseInt(exploitation.informationActivite.etablissementDateDebutActivite.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['U50.2'] = date;
    }
    else if (tag.formaliteSignatureDate != null)
    {
        var dateTemp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['U50.2'] = date;
    }
    occurrence['U51.1'] = dirigeant1.personneLieeQualite.getId();
    occurrence['U52'] = "M";

    //Groupe ICR : Identification complète de la personne morale dirigeante

    occurrence['R01.1'] = dirigeant1.civilitePersonneMorale1.personneLieePMDenomination;
    if (Value('id').of(dirigeant1.adresseDirigeant1.siegeAdressePays).eq('FRXXXXX'))
    {
        occurrence['R02.3'] = dirigeant1.adresseDirigeant1.siegeAdresseCommune.getId();
    }
    else if (not Value('id').of(dirigeant1.adresseDirigeant1.siegeAdressePays).eq('FRXXXXX'))
    {
        var idPays = dirigeant1.adresseDirigeant1.siegeAdressePays.getId();
        var result = idPays.match(regEx)[0];
        occurrence['R02.3'] = result;
    }
    if (dirigeant1.adresseDirigeant1.siegeAdresseNumVoie != null)
    {
        occurrence['R02.5'] = dirigeant1.adresseDirigeant1.siegeAdresseNumVoie;
    }
    if (dirigeant1.adresseDirigeant1.siegeAdresseIndiceVoie != null)
    {
        var monId1 = Value('id').of(dirigeant1.adresseDirigeant1.siegeAdresseIndiceVoie)._eval();
        occurrence['R02.6'] = monId1;
    }
    if (dirigeant1.adresseDirigeant1.siegeAdresseDistributionSpecialeVoie != null)
    {
        occurrence['R02.7'] = dirigeant1.adresseDirigeant1.siegeAdresseDistributionSpecialeVoie;
    }
    occurrence['R02.8'] = dirigeant1.adresseDirigeant1.siegeAdresseCodePostal != null ? dirigeant1.adresseDirigeant1.siegeAdresseCodePostal : dirigeant1.adresseDirigeant1.siegeAdresseCodePostaEtranger;
    if (dirigeant1.adresseDirigeant1.siegeAdresseComplementVoie != null)
    {
        occurrence['R02.10'] = dirigeant1.adresseDirigeant1.siegeAdresseComplementVoie;
    }
    if (dirigeant1.adresseDirigeant1.siegeAdresseTypeVoie != null)
    {
        var monId1 = Value('id').of(dirigeant1.adresseDirigeant1.siegeAdresseTypeVoie)._eval();
        occurrence['R02.11'] = monId1;
    }
    occurrence['R02.12'] = dirigeant1.adresseDirigeant1.siegeAdresseNomVoie;
    if (Value('id').of(dirigeant1.adresseDirigeant1.dirigeantAdressePays).eq('FRXXXXX'))
    {
        occurrence['R02.13'] = dirigeant1.adresseDirigeant1.dirigeantAdresseCommune.getLabel();
    }
    else if (not Value('id').of(dirigeant1.adresseDirigeant1.dirigeantAdressePays).eq('FRXXXXX'))
    {
        occurrence['R02.13'] = dirigeant1.adresseDirigeant1.dirigeantAdresseVille;
    }
    if (not Value('id').of(dirigeant1.adresseDirigeant1.dirigeantAdressePays).eq('FRXXXXX'))
    {
        occurrence['R02.14'] = dirigeant1.adresseDirigeant1.dirigeantAdressePays.getLabel();
    }
    occurrence['R03'] = dirigeant1.civilitePersonneMorale1.personneLieePMFormeJuridique.getId();
    if (dirigeant1.civiliteRepresentant1.personneLieePPNomNaissance != null)
    {
        occurrence['R08'] = "1";
        occurrence['R04.2'] = dirigeant1.civiliteRepresentant1.personneLieePPNomNaissance;
        var prenoms = [];
        for (i = 0; i < dirigeant1.civiliteRepresentant1.personneLieePPPrenom.size(); i++)
        {
            prenoms.push(dirigeant1.civiliteRepresentant1.personneLieePPPrenom[i]);
        }
        occurrence['R04.3'] = prenoms;
        if (dirigeant1.civiliteRepresentant1.personneLieePPNomUsage != null)
        {
            occurrence['R04.4'] = dirigeant1.civiliteRepresentant1.personneLieePPNomUsage;
        }
        occurrence['R05.3'] = dirigeant1.civiliteRepresentant1.adresseRepresentant.representantAdresseCommune.getId();
        if (dirigeant1.civiliteRepresentant1.adresseRepresentant.representantNumeroVoie != null)
        {
            occurrence['R05.5'] = dirigeant1.civiliteRepresentant1.adresseRepresentant.representantNumeroVoie;
        }
        if (dirigeant1.civiliteRepresentant1.adresseRepresentant.representantIndiceVoie != null)
        {
            var monId1 = Value('id').of(dirigeant1.civiliteRepresentant1.adresseRepresentant.representantIndiceVoie)._eval();
            occurrence['R05.6'] = monId1;
        }
        if (dirigeant1.civiliteRepresentant1.adresseRepresentant.representantDistributionSpeciale != null)
        {
            occurrence['R05.7'] = dirigeant1.civiliteRepresentant1.adresseRepresentant.representantDistributionSpeciale;
        }
        occurrence['R05.8'] = dirigeant1.civiliteRepresentant1.adresseRepresentant.representantAdresseCodePostal;
        if (dirigeant1.civiliteRepresentant1.adresseRepresentant.representantComplementVoie != null)
        {
            occurrence['R05.10'] = dirigeant1.civiliteRepresentant1.adresseRepresentant.representantComplementVoie;
        }
        if (dirigeant1.civiliteRepresentant1.adresseRepresentant.representantTypeVoie != null)
        {
            var monId1 = Value('id').of(dirigeant1.civiliteRepresentant1.adresseRepresentant.representantTypeVoie)._eval();
            occurrence['R05.11'] = monId1;
        }
        occurrence['R05.12'] = dirigeant1.civiliteRepresentant1.adresseRepresentant.representantNomVoie;
        occurrence['R05.13'] = dirigeant1.civiliteRepresentant1.adresseRepresentant.representantAdresseCommune.getLabel();
        if (dirigeant1.civiliteRepresentant1.personneLieePPDateNaissance != null)
        {
            var dateTemp = new Date(parseInt(dirigeant1.civiliteRepresentant1.personneLieePPDateNaissance.getTimeInMillis()));
            var year = dateTemp.getFullYear();
            var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
            var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
            var date = year + "-" + month + "-" + day;
            occurrence['R06.1'] = date;
        }
        if (not Value('id').of(dirigeant1.civiliteRepresentant1.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
        {
            var idPays = dirigeant1.civiliteRepresentant1.personneLieePPLieuNaissancePays.getId();
            var result = idPays.match(regEx)[0];
            occurrence['R06.2'] = result;
        }
        else if (Value('id').of(dirigeant1.civiliteRepresentant1.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
        {
            occurrence['R06.2'] = dirigeant1.civiliteRepresentant1.personneLieePPLieuNaissanceCommnune.getId();
        }
        if (not Value('id').of(dirigeant1.civiliteRepresentant1.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
        {
            occurrence['R06.3'] = dirigeant1.civiliteRepresentant1.personneLieePPLieuNaissancePays.getLabel();
        }
        if (Value('id').of(dirigeant1.civiliteRepresentant1.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
        {
            occurrence['R06.4'] = dirigeant1.civiliteRepresentant1.personneLieePPLieuNaissanceCommnune.getLabel();
        }
        occurrence['R07'] = dirigeant1.civiliteRepresentant1.personneLieePPNationalite;
    }
    occurrence['R22.2'] = dirigeant1.civilitePersonneMorale1.personneLieePMNumeroIdentification;
    occurrence['R22.3'] = dirigeant1.civilitePersonneMorale1.personneLieePMLieuImmatriculation;
    occurrencesGDR.push(occurrence);
    occurrence = {};
}

// Dirigeant 2

if (Value('id').of(dirigeant2.personnaliteJuridique).eq('personneMorale'))
{

    //Groupe DIU : Dirigeant de l'entreprise

    occurrence['U50.1'] = "1";
    if (exploitation.informationActivite.etablissementDateDebutActivite != null)
    {
        var dateTemp = new Date(parseInt(exploitation.informationActivite.etablissementDateDebutActivite.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['U50.2'] = date;
    }
    else if (tag.formaliteSignatureDate != null)
    {
        var dateTemp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['U50.2'] = date;
    }
    occurrence['U51.1'] = dirigeant2.personneLieeQualite.getId();
    occurrence['U52'] = "M";

    //Groupe ICR : Identification complète de la personne morale dirigeante

    occurrence['R01.1'] = dirigeant2.civilitePersonneMorale2.personneLieePMDenomination;
    if (Value('id').of(dirigeant2.adresseDirigeant2.siegeAdressePays).eq('FRXXXXX'))
    {
        occurrence['R02.3'] = dirigeant2.adresseDirigeant2.siegeAdresseCommune.getId();
    }
    else if (not Value('id').of(dirigeant2.adresseDirigeant2.siegeAdressePays).eq('FRXXXXX'))
    {
        var idPays = dirigeant2.adresseDirigeant2.siegeAdressePays.getId();
        var result = idPays.match(regEx)[0];
        occurrence['R02.3'] = result;
    }
    if (dirigeant2.adresseDirigeant2.siegeAdresseNumVoie != null)
    {
        occurrence['R02.5'] = dirigeant2.adresseDirigeant2.siegeAdresseNumVoie;
    }
    if (dirigeant2.adresseDirigeant2.siegeAdresseIndiceVoie != null)
    {
        var monId1 = Value('id').of(dirigeant2.adresseDirigeant2.siegeAdresseIndiceVoie)._eval();
        occurrence['R02.6'] = monId1;
    }
    if (dirigeant2.adresseDirigeant2.siegeAdresseDistributionSpecialeVoie != null)
    {
        occurrence['R02.7'] = dirigeant2.adresseDirigeant2.siegeAdresseDistributionSpecialeVoie;
    }
    occurrence['R02.8'] = dirigeant2.adresseDirigeant2.siegeAdresseCodePostal != null ? dirigeant2.adresseDirigeant2.siegeAdresseCodePostal : dirigeant2.adresseDirigeant2.siegeAdresseCodePostaEtranger;
    if (dirigeant2.adresseDirigeant2.siegeAdresseComplementVoie != null)
    {
        occurrence['R02.10'] = dirigeant2.adresseDirigeant2.siegeAdresseComplementVoie;
    }
    if (dirigeant2.adresseDirigeant2.siegeAdresseTypeVoie != null)
    {
        var monId1 = Value('id').of(dirigeant2.adresseDirigeant2.siegeAdresseTypeVoie)._eval();
        occurrence['R02.11'] = monId1;
    }
    occurrence['R02.12'] = dirigeant2.adresseDirigeant2.siegeAdresseNomVoie;
    if (Value('id').of(dirigeant2.adresseDirigeant2.dirigeantAdressePays).eq('FRXXXXX'))
    {
        occurrence['R02.13'] = dirigeant2.adresseDirigeant2.dirigeantAdresseCommune.getLabel();
    }
    else if (not Value('id').of(dirigeant2.adresseDirigeant2.dirigeantAdressePays).eq('FRXXXXX'))
    {
        occurrence['R02.13'] = dirigeant2.adresseDirigeant2.dirigeantAdresseVille;
    }
    if (not Value('id').of(dirigeant2.adresseDirigeant2.dirigeantAdressePays).eq('FRXXXXX'))
    {
        occurrence['R02.14'] = dirigeant2.adresseDirigeant2.dirigeantAdressePays.getLabel();
    }
    occurrence['R03'] = dirigeant2.civilitePersonneMorale2.personneLieePMFormeJuridique.getId();
    if (dirigeant2.civiliteRepresentant2.personneLieePPNomNaissance != null)
    {
        occurrence['R08'] = "1";
        occurrence['R04.2'] = dirigeant2.civiliteRepresentant2.personneLieePPNomNaissance;
        var prenoms = [];
        for (i = 0; i < dirigeant2.civiliteRepresentant2.personneLieePPPrenom.size(); i++)
        {
            prenoms.push(dirigeant2.civiliteRepresentant2.personneLieePPPrenom[i]);
        }
        occurrence['R04.3'] = prenoms;
        if (dirigeant2.civiliteRepresentant2.personneLieePPNomUsage != null)
        {
            occurrence['R04.4'] = dirigeant2.civiliteRepresentant2.personneLieePPNomUsage;
        }
        occurrence['R05.3'] = dirigeant2.civiliteRepresentant2.adresseRepresentant.representantAdresseCommune.getId();
        if (dirigeant2.civiliteRepresentant2.adresseRepresentant.representantNumeroVoie != null)
        {
            occurrence['R05.5'] = dirigeant2.civiliteRepresentant2.adresseRepresentant.representantNumeroVoie;
        }
        if (dirigeant2.civiliteRepresentant2.adresseRepresentant.representantIndiceVoie != null)
        {
            var monId1 = Value('id').of(dirigeant2.civiliteRepresentant2.adresseRepresentant.representantIndiceVoie)._eval();
            occurrence['R05.6'] = monId1;
        }
        if (dirigeant2.civiliteRepresentant2.adresseRepresentant.representantDistributionSpeciale != null)
        {
            occurrence['R05.7'] = dirigeant2.civiliteRepresentant2.adresseRepresentant.representantDistributionSpeciale;
        }
        occurrence['R05.8'] = dirigeant2.civiliteRepresentant2.adresseRepresentant.representantAdresseCodePostal;
        if (dirigeant2.civiliteRepresentant2.adresseRepresentant.representantComplementVoie != null)
        {
            occurrence['R05.10'] = dirigeant2.civiliteRepresentant2.adresseRepresentant.representantComplementVoie;
        }
        if (dirigeant2.civiliteRepresentant2.adresseRepresentant.representantTypeVoie != null)
        {
            var monId1 = Value('id').of(dirigeant2.civiliteRepresentant2.adresseRepresentant.representantTypeVoie)._eval();
            occurrence['R05.11'] = monId1;
        }
        occurrence['R05.12'] = dirigeant2.civiliteRepresentant2.adresseRepresentant.representantNomVoie;
        occurrence['R05.13'] = dirigeant2.civiliteRepresentant2.adresseRepresentant.representantAdresseCommune.getLabel();
        if (dirigeant2.civiliteRepresentant2.personneLieePPDateNaissance != null)
        {
            var dateTemp = new Date(parseInt(dirigeant2.civiliteRepresentant2.personneLieePPDateNaissance.getTimeInMillis()));
            var year = dateTemp.getFullYear();
            var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
            var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
            var date = year + "-" + month + "-" + day;
            occurrence['R06.1'] = date;
        }
        if (not Value('id').of(dirigeant2.civiliteRepresentant2.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
        {
            var idPays = dirigeant2.civiliteRepresentant2.personneLieePPLieuNaissancePays.getId();
            var result = idPays.match(regEx)[0];
            occurrence['R06.2'] = result;
        }
        else if (Value('id').of(dirigeant2.civiliteRepresentant2.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
        {
            occurrence['R06.2'] = dirigeant2.civiliteRepresentant2.personneLieePPLieuNaissanceCommnune.getId();
        }
        if (not Value('id').of(dirigeant2.civiliteRepresentant2.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
        {
            occurrence['R06.3'] = dirigeant2.civiliteRepresentant2.personneLieePPLieuNaissancePays.getLabel();
        }
        if (Value('id').of(dirigeant2.civiliteRepresentant2.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
        {
            occurrence['R06.4'] = dirigeant2.civiliteRepresentant2.personneLieePPLieuNaissanceCommnune.getLabel();
        }
        occurrence['R07'] = dirigeant2.civiliteRepresentant2.personneLieePPNationalite;
    }
    occurrence['R22.2'] = dirigeant2.civilitePersonneMorale2.personneLieePMNumeroIdentification;
    occurrence['R22.3'] = dirigeant2.civilitePersonneMorale2.personneLieePMLieuImmatriculation;
    occurrencesGDR.push(occurrence);
    occurrence = {};
}

// Dirigeant 3

if (Value('id').of(dirigeant3.personnaliteJuridique).eq('personneMorale'))
{

    //Groupe DIU : Dirigeant de l'entreprise

    occurrence['U50.1'] = "1";
    if (exploitation.informationActivite.etablissementDateDebutActivite != null)
    {
        var dateTemp = new Date(parseInt(exploitation.informationActivite.etablissementDateDebutActivite.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['U50.2'] = date;
    }
    else if (tag.formaliteSignatureDate != null)
    {
        var dateTemp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['U50.2'] = date;
    }
    occurrence['U51.1'] = dirigeant3.personneLieeQualite.getId();
    occurrence['U52'] = "M";

    //Groupe ICR : Identification complète de la personne morale dirigeante

    occurrence['R01.1'] = dirigeant3.civilitePersonneMorale3.personneLieePMDenomination;
    if (Value('id').of(dirigeant3.adresseDirigeant3.siegeAdressePays).eq('FRXXXXX'))
    {
        occurrence['R02.3'] = dirigeant3.adresseDirigeant3.siegeAdresseCommune.getId();
    }
    else if (not Value('id').of(dirigeant3.adresseDirigeant3.siegeAdressePays).eq('FRXXXXX'))
    {
        var idPays = dirigeant3.adresseDirigeant3.siegeAdressePays.getId();
        var result = idPays.match(regEx)[0];
        occurrence['R02.3'] = result;
    }
    if (dirigeant3.adresseDirigeant3.siegeAdresseNumVoie != null)
    {
        occurrence['R02.5'] = dirigeant3.adresseDirigeant3.siegeAdresseNumVoie;
    }
    if (dirigeant3.adresseDirigeant3.siegeAdresseIndiceVoie != null)
    {
        var monId1 = Value('id').of(dirigeant3.adresseDirigeant3.siegeAdresseIndiceVoie)._eval();
        occurrence['R02.6'] = monId1;
    }
    if (dirigeant3.adresseDirigeant3.siegeAdresseDistributionSpecialeVoie != null)
    {
        occurrence['R02.7'] = dirigeant3.adresseDirigeant3.siegeAdresseDistributionSpecialeVoie;
    }
    occurrence['R02.8'] = dirigeant3.adresseDirigeant3.siegeAdresseCodePostal != null ? dirigeant3.adresseDirigeant3.siegeAdresseCodePostal : dirigeant3.adresseDirigeant3.siegeAdresseCodePostaEtranger;
    if (dirigeant3.adresseDirigeant3.siegeAdresseComplementVoie != null)
    {
        occurrence['R02.10'] = dirigeant3.adresseDirigeant3.siegeAdresseComplementVoie;
    }
    if (dirigeant3.adresseDirigeant3.siegeAdresseTypeVoie != null)
    {
        var monId1 = Value('id').of(dirigeant3.adresseDirigeant3.siegeAdresseTypeVoie)._eval();
        occurrence['R02.11'] = monId1;
    }
    occurrence['R02.12'] = dirigeant3.adresseDirigeant3.siegeAdresseNomVoie;
    if (Value('id').of(dirigeant3.adresseDirigeant3.dirigeantAdressePays).eq('FRXXXXX'))
    {
        occurrence['R02.13'] = dirigeant3.adresseDirigeant3.dirigeantAdresseCommune.getLabel();
    }
    else if (not Value('id').of(dirigeant3.adresseDirigeant3.dirigeantAdressePays).eq('FRXXXXX'))
    {
        occurrence['R02.13'] = dirigeant3.adresseDirigeant3.dirigeantAdresseVille;
    }
    if (not Value('id').of(dirigeant3.adresseDirigeant3.dirigeantAdressePays).eq('FRXXXXX'))
    {
        occurrence['R02.14'] = dirigeant3.adresseDirigeant3.dirigeantAdressePays.getLabel();
    }
    occurrence['R03'] = dirigeant3.civilitePersonneMorale3.personneLieePMFormeJuridique.getId();
    if (dirigeant3.civiliteRepresentant3.personneLieePPNomNaissance != null)
    {
        occurrence['R08'] = "1";
        occurrence['R04.2'] = dirigeant3.civiliteRepresentant3.personneLieePPNomNaissance;
        var prenoms = [];
        for (i = 0; i < dirigeant3.civiliteRepresentant3.personneLieePPPrenom.size(); i++)
        {
            prenoms.push(dirigeant3.civiliteRepresentant3.personneLieePPPrenom[i]);
        }
        occurrence['R04.3'] = prenoms;
        if (dirigeant3.civiliteRepresentant3.personneLieePPNomUsage != null)
        {
            occurrence['R04.4'] = dirigeant3.civiliteRepresentant3.personneLieePPNomUsage;
        }
        occurrence['R05.3'] = dirigeant3.civiliteRepresentant3.adresseRepresentant.representantAdresseCommune.getId();
        if (dirigeant3.civiliteRepresentant3.adresseRepresentant.representantNumeroVoie != null)
        {
            occurrence['R05.5'] = dirigeant3.civiliteRepresentant3.adresseRepresentant.representantNumeroVoie;
        }
        if (dirigeant3.civiliteRepresentant3.adresseRepresentant.representantIndiceVoie != null)
        {
            var monId1 = Value('id').of(dirigeant3.civiliteRepresentant3.adresseRepresentant.representantIndiceVoie)._eval();
            occurrence['R05.6'] = monId1;
        }
        if (dirigeant3.civiliteRepresentant3.adresseRepresentant.representantDistributionSpeciale != null)
        {
            occurrence['R05.7'] = dirigeant3.civiliteRepresentant3.adresseRepresentant.representantDistributionSpeciale;
        }
        occurrence['R05.8'] = dirigeant3.civiliteRepresentant3.adresseRepresentant.representantAdresseCodePostal;
        if (dirigeant3.civiliteRepresentant3.adresseRepresentant.representantComplementVoie != null)
        {
            occurrence['R05.10'] = dirigeant3.civiliteRepresentant3.adresseRepresentant.representantComplementVoie;
        }
        if (dirigeant3.civiliteRepresentant3.adresseRepresentant.representantTypeVoie != null)
        {
            var monId1 = Value('id').of(dirigeant3.civiliteRepresentant3.adresseRepresentant.representantTypeVoie)._eval();
            occurrence['R05.11'] = monId1;
        }
        occurrence['R05.12'] = dirigeant3.civiliteRepresentant3.adresseRepresentant.representantNomVoie;
        occurrence['R05.13'] = dirigeant3.civiliteRepresentant3.adresseRepresentant.representantAdresseCommune.getLabel();
        if (dirigeant3.civiliteRepresentant3.personneLieePPDateNaissance != null)
        {
            var dateTemp = new Date(parseInt(dirigeant3.civiliteRepresentant3.personneLieePPDateNaissance.getTimeInMillis()));
            var year = dateTemp.getFullYear();
            var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
            var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
            var date = year + "-" + month + "-" + day;
            occurrence['R06.1'] = date;
        }
        if (not Value('id').of(dirigeant3.civiliteRepresentant3.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
        {
            var idPays = dirigeant3.civiliteRepresentant3.personneLieePPLieuNaissancePays.getId();
            var result = idPays.match(regEx)[0];
            occurrence['R06.2'] = result;
        }
        else if (Value('id').of(dirigeant3.civiliteRepresentant3.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
        {
            occurrence['R06.2'] = dirigeant3.civiliteRepresentant3.personneLieePPLieuNaissanceCommnune.getId();
        }
        if (not Value('id').of(dirigeant3.civiliteRepresentant3.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
        {
            occurrence['R06.3'] = dirigeant3.civiliteRepresentant3.personneLieePPLieuNaissancePays.getLabel();
        }
        if (Value('id').of(dirigeant3.civiliteRepresentant3.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
        {
            occurrence['R06.4'] = dirigeant3.civiliteRepresentant3.personneLieePPLieuNaissanceCommnune.getLabel();
        }
        occurrence['R07'] = dirigeant3.civiliteRepresentant3.personneLieePPNationalite;
    }
    occurrence['R22.2'] = dirigeant3.civilitePersonneMorale3.personneLieePMNumeroIdentification;
    occurrence['R22.3'] = dirigeant3.civilitePersonneMorale3.personneLieePMLieuImmatriculation;
    occurrencesGDR.push(occurrence);
    occurrence = {};
}

// Dirigeant 4

if (Value('id').of(dirigeant4.personnaliteJuridique).eq('personneMorale'))
{

    //Groupe DIU : Dirigeant de l'entreprise

    occurrence['U50.1'] = "1";
    if (exploitation.informationActivite.etablissementDateDebutActivite != null)
    {
        var dateTemp = new Date(parseInt(exploitation.informationActivite.etablissementDateDebutActivite.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['U50.2'] = date;
    }
    else if (tag.formaliteSignatureDate != null)
    {
        var dateTemp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['U50.2'] = date;
    }
    occurrence['U51.1'] = dirigeant4.personneLieeQualite.getId();
    occurrence['U52'] = "M";

    //Groupe ICR : Identification complète de la personne morale dirigeante

    occurrence['R01.1'] = dirigeant4.civilitePersonneMorale4.personneLieePMDenomination;
    if (Value('id').of(dirigeant4.adresseDirigeant4.siegeAdressePays).eq('FRXXXXX'))
    {
        occurrence['R02.3'] = dirigeant4.adresseDirigeant4.siegeAdresseCommune.getId();
    }
    else if (not Value('id').of(dirigeant4.adresseDirigeant4.siegeAdressePays).eq('FRXXXXX'))
    {
        var idPays = dirigeant4.adresseDirigeant4.siegeAdressePays.getId();
        var result = idPays.match(regEx)[0];
        occurrence['R02.3'] = result;
    }
    if (dirigeant4.adresseDirigeant4.siegeAdresseNumVoie != null)
    {
        occurrence['R02.5'] = dirigeant4.adresseDirigeant4.siegeAdresseNumVoie;
    }
    if (dirigeant4.adresseDirigeant4.siegeAdresseIndiceVoie != null)
    {
        var monId1 = Value('id').of(dirigeant4.adresseDirigeant4.siegeAdresseIndiceVoie)._eval();
        occurrence['R02.6'] = monId1;
    }
    if (dirigeant4.adresseDirigeant4.siegeAdresseDistributionSpecialeVoie != null)
    {
        occurrence['R02.7'] = dirigeant4.adresseDirigeant4.siegeAdresseDistributionSpecialeVoie;
    }
    occurrence['R02.8'] = dirigeant4.adresseDirigeant4.siegeAdresseCodePostal != null ? dirigeant4.adresseDirigeant4.siegeAdresseCodePostal : dirigeant4.adresseDirigeant4.siegeAdresseCodePostaEtranger;
    if (dirigeant4.adresseDirigeant4.siegeAdresseComplementVoie != null)
    {
        occurrence['R02.10'] = dirigeant4.adresseDirigeant4.siegeAdresseComplementVoie;
    }
    if (dirigeant4.adresseDirigeant4.siegeAdresseTypeVoie != null)
    {
        var monId1 = Value('id').of(dirigeant4.adresseDirigeant4.siegeAdresseTypeVoie)._eval();
        occurrence['R02.11'] = monId1;
    }
    occurrence['R02.12'] = dirigeant4.adresseDirigeant4.siegeAdresseNomVoie;
    if (Value('id').of(dirigeant4.adresseDirigeant4.dirigeantAdressePays).eq('FRXXXXX'))
    {
        occurrence['R02.13'] = dirigeant4.adresseDirigeant4.dirigeantAdresseCommune.getLabel();
    }
    else if (not Value('id').of(dirigeant4.adresseDirigeant4.dirigeantAdressePays).eq('FRXXXXX'))
    {
        occurrence['R02.13'] = dirigeant4.adresseDirigeant4.dirigeantAdresseVille;
    }
    if (not Value('id').of(dirigeant4.adresseDirigeant4.dirigeantAdressePays).eq('FRXXXXX'))
    {
        occurrence['R02.14'] = dirigeant4.adresseDirigeant4.dirigeantAdressePays.getLabel();
    }
    occurrence['R03'] = dirigeant4.civilitePersonneMorale4.personneLieePMFormeJuridique.getId();
    if (dirigeant4.civiliteRepresentant4.personneLieePPNomNaissance != null)
    {
        occurrence['R08'] = "1";
        occurrence['R04.2'] = dirigeant4.civiliteRepresentant4.personneLieePPNomNaissance;
        var prenoms = [];
        for (i = 0; i < dirigeant4.civiliteRepresentant4.personneLieePPPrenom.size(); i++)
        {
            prenoms.push(dirigeant4.civiliteRepresentant4.personneLieePPPrenom[i]);
        }
        occurrence['R04.3'] = prenoms;
        if (dirigeant4.civiliteRepresentant4.personneLieePPNomUsage != null)
        {
            occurrence['R04.4'] = dirigeant4.civiliteRepresentant4.personneLieePPNomUsage;
        }
        occurrence['R05.3'] = dirigeant4.civiliteRepresentant4.adresseRepresentant.representantAdresseCommune.getId();
        if (dirigeant4.civiliteRepresentant4.adresseRepresentant.representantNumeroVoie != null)
        {
            occurrence['R05.5'] = dirigeant4.civiliteRepresentant4.adresseRepresentant.representantNumeroVoie;
        }
        if (dirigeant4.civiliteRepresentant4.adresseRepresentant.representantIndiceVoie != null)
        {
            var monId1 = Value('id').of(dirigeant4.civiliteRepresentant4.adresseRepresentant.representantIndiceVoie)._eval();
            occurrence['R05.6'] = monId1;
        }
        if (dirigeant4.civiliteRepresentant4.adresseRepresentant.representantDistributionSpeciale != null)
        {
            occurrence['R05.7'] = dirigeant4.civiliteRepresentant4.adresseRepresentant.representantDistributionSpeciale;
        }
        occurrence['R05.8'] = dirigeant4.civiliteRepresentant4.adresseRepresentant.representantAdresseCodePostal;
        if (dirigeant4.civiliteRepresentant4.adresseRepresentant.representantComplementVoie != null)
        {
            occurrence['R05.10'] = dirigeant4.civiliteRepresentant4.adresseRepresentant.representantComplementVoie;
        }
        if (dirigeant4.civiliteRepresentant4.adresseRepresentant.representantTypeVoie != null)
        {
            var monId1 = Value('id').of(dirigeant4.civiliteRepresentant4.adresseRepresentant.representantTypeVoie)._eval();
            occurrence['R05.11'] = monId1;
        }
        occurrence['R05.12'] = dirigeant4.civiliteRepresentant4.adresseRepresentant.representantNomVoie;
        occurrence['R05.13'] = dirigeant4.civiliteRepresentant4.adresseRepresentant.representantAdresseCommune.getLabel();
        if (dirigeant4.civiliteRepresentant4.personneLieePPDateNaissance != null)
        {
            var dateTemp = new Date(parseInt(dirigeant4.civiliteRepresentant4.personneLieePPDateNaissance.getTimeInMillis()));
            var year = dateTemp.getFullYear();
            var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
            var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
            var date = year + "-" + month + "-" + day;
            occurrence['R06.1'] = date;
        }
        if (not Value('id').of(dirigeant4.civiliteRepresentant4.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
        {
            var idPays = dirigeant4.civiliteRepresentant4.personneLieePPLieuNaissancePays.getId();
            var result = idPays.match(regEx)[0];
            occurrence['R06.2'] = result;
        }
        else if (Value('id').of(dirigeant4.civiliteRepresentant4.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
        {
            occurrence['R06.2'] = dirigeant4.civiliteRepresentant4.personneLieePPLieuNaissanceCommnune.getId();
        }
        if (not Value('id').of(dirigeant4.civiliteRepresentant4.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
        {
            occurrence['R06.3'] = dirigeant4.civiliteRepresentant4.personneLieePPLieuNaissancePays.getLabel();
        }
        if (Value('id').of(dirigeant4.civiliteRepresentant4.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
        {
            occurrence['R06.4'] = dirigeant4.civiliteRepresentant4.personneLieePPLieuNaissanceCommnune.getLabel();
        }
        occurrence['R07'] = dirigeant4.civiliteRepresentant4.personneLieePPNationalite;
    }
    occurrence['R22.2'] = dirigeant4.civilitePersonneMorale4.personneLieePMNumeroIdentification;
    occurrence['R22.3'] = dirigeant4.civilitePersonneMorale4.personneLieePMLieuImmatriculation;
    occurrencesGDR.push(occurrence);
    occurrence = {};
}

// Dirigeant 5

if (Value('id').of(dirigeant5.personnaliteJuridique).eq('personneMorale'))
{

    //Groupe DIU : Dirigeant de l'entreprise

    occurrence['U50.1'] = "1";
    if (exploitation.informationActivite.etablissementDateDebutActivite != null)
    {
        var dateTemp = new Date(parseInt(exploitation.informationActivite.etablissementDateDebutActivite.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['U50.2'] = date;
    }
    else if (tag.formaliteSignatureDate != null)
    {
        var dateTemp = new Date(parseInt(tag.formaliteSignatureDate.getTimeInMillis()));
        var year = dateTemp.getFullYear();
        var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
        var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
        var date = year + "-" + month + "-" + day;
        occurrence['U50.2'] = date;
    }
    occurrence['U51.1'] = dirigeant5.personneLieeQualite.getId();
    occurrence['U52'] = "M";

    //Groupe ICR : Identification complète de la personne morale dirigeante

    occurrence['R01.1'] = dirigeant5.civilitePersonneMorale5.personneLieePMDenomination;
    if (Value('id').of(dirigeant5.adresseDirigeant5.siegeAdressePays).eq('FRXXXXX'))
    {
        occurrence['R02.3'] = dirigeant5.adresseDirigeant5.siegeAdresseCommune.getId();
    }
    else if (not Value('id').of(dirigeant5.adresseDirigeant5.siegeAdressePays).eq('FRXXXXX'))
    {
        var idPays = dirigeant5.adresseDirigeant5.siegeAdressePays.getId();
        var result = idPays.match(regEx)[0];
        occurrence['R02.3'] = result;
    }
    if (dirigeant5.adresseDirigeant5.siegeAdresseNumVoie != null)
    {
        occurrence['R02.5'] = dirigeant5.adresseDirigeant5.siegeAdresseNumVoie;
    }
    if (dirigeant5.adresseDirigeant5.siegeAdresseIndiceVoie != null)
    {
        var monId1 = Value('id').of(dirigeant5.adresseDirigeant5.siegeAdresseIndiceVoie)._eval();
        occurrence['R02.6'] = monId1;
    }
    if (dirigeant5.adresseDirigeant5.siegeAdresseDistributionSpecialeVoie != null)
    {
        occurrence['R02.7'] = dirigeant5.adresseDirigeant5.siegeAdresseDistributionSpecialeVoie;
    }
    occurrence['R02.8'] = dirigeant5.adresseDirigeant5.siegeAdresseCodePostal != null ? dirigeant5.adresseDirigeant5.siegeAdresseCodePostal : dirigeant5.adresseDirigeant5.siegeAdresseCodePostaEtranger;
    if (dirigeant5.adresseDirigeant5.siegeAdresseComplementVoie != null)
    {
        occurrence['R02.10'] = dirigeant5.adresseDirigeant5.siegeAdresseComplementVoie;
    }
    if (dirigeant5.adresseDirigeant5.siegeAdresseTypeVoie != null)
    {
        var monId1 = Value('id').of(dirigeant5.adresseDirigeant5.siegeAdresseTypeVoie)._eval();
        occurrence['R02.11'] = monId1;
    }
    occurrence['R02.12'] = dirigeant5.adresseDirigeant5.siegeAdresseNomVoie;
    if (Value('id').of(dirigeant5.adresseDirigeant5.dirigeantAdressePays).eq('FRXXXXX'))
    {
        occurrence['R02.13'] = dirigeant5.adresseDirigeant5.dirigeantAdresseCommune.getLabel();
    }
    else if (not Value('id').of(dirigeant5.adresseDirigeant5.dirigeantAdressePays).eq('FRXXXXX'))
    {
        occurrence['R02.13'] = dirigeant5.adresseDirigeant5.dirigeantAdresseVille;
    }
    if (not Value('id').of(dirigeant5.adresseDirigeant5.dirigeantAdressePays).eq('FRXXXXX'))
    {
        occurrence['R02.14'] = dirigeant5.adresseDirigeant5.dirigeantAdressePays.getLabel();
    }
    occurrence['R03'] = dirigeant5.civilitePersonneMorale5.personneLieePMFormeJuridique.getId();
    if (dirigeant5.civiliteRepresentant5.personneLieePPNomNaissance != null)
    {
        occurrence['R08'] = "1";
        occurrence['R04.2'] = dirigeant5.civiliteRepresentant5.personneLieePPNomNaissance;
        var prenoms = [];
        for (i = 0; i < dirigeant5.civiliteRepresentant5.personneLieePPPrenom.size(); i++)
        {
            prenoms.push(dirigeant5.civiliteRepresentant5.personneLieePPPrenom[i]);
        }
        occurrence['R04.3'] = prenoms;
        if (dirigeant5.civiliteRepresentant5.personneLieePPNomUsage != null)
        {
            occurrence['R04.4'] = dirigeant5.civiliteRepresentant5.personneLieePPNomUsage;
        }
        occurrence['R05.3'] = dirigeant5.civiliteRepresentant5.adresseRepresentant.representantAdresseCommune.getId();
        if (dirigeant5.civiliteRepresentant5.adresseRepresentant.representantNumeroVoie != null)
        {
            occurrence['R05.5'] = dirigeant5.civiliteRepresentant5.adresseRepresentant.representantNumeroVoie;
        }
        if (dirigeant5.civiliteRepresentant5.adresseRepresentant.representantIndiceVoie != null)
        {
            var monId1 = Value('id').of(dirigeant5.civiliteRepresentant5.adresseRepresentant.representantIndiceVoie)._eval();
            occurrence['R05.6'] = monId1;
        }
        if (dirigeant5.civiliteRepresentant5.adresseRepresentant.representantDistributionSpeciale != null)
        {
            occurrence['R05.7'] = dirigeant5.civiliteRepresentant5.adresseRepresentant.representantDistributionSpeciale;
        }
        occurrence['R05.8'] = dirigeant5.civiliteRepresentant5.adresseRepresentant.representantAdresseCodePostal;
        if (dirigeant5.civiliteRepresentant5.adresseRepresentant.representantComplementVoie != null)
        {
            occurrence['R05.10'] = dirigeant5.civiliteRepresentant5.adresseRepresentant.representantComplementVoie;
        }
        if (dirigeant5.civiliteRepresentant5.adresseRepresentant.representantTypeVoie != null)
        {
            var monId1 = Value('id').of(dirigeant5.civiliteRepresentant5.adresseRepresentant.representantTypeVoie)._eval();
            occurrence['R05.11'] = monId1;
        }
        occurrence['R05.12'] = dirigeant5.civiliteRepresentant5.adresseRepresentant.representantNomVoie;
        occurrence['R05.13'] = dirigeant5.civiliteRepresentant5.adresseRepresentant.representantAdresseCommune.getLabel();
        if (dirigeant5.civiliteRepresentant5.personneLieePPDateNaissance != null)
        {
            var dateTemp = new Date(parseInt(dirigeant5.civiliteRepresentant5.personneLieePPDateNaissance.getTimeInMillis()));
            var year = dateTemp.getFullYear();
            var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
            var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
            var date = year + "-" + month + "-" + day;
            occurrence['R06.1'] = date;
        }
        if (not Value('id').of(dirigeant5.civiliteRepresentant5.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
        {
            var idPays = dirigeant5.civiliteRepresentant5.personneLieePPLieuNaissancePays.getId();
            var result = idPays.match(regEx)[0];
            occurrence['R06.2'] = result;
        }
        else if (Value('id').of(dirigeant5.civiliteRepresentant5.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
        {
            occurrence['R06.2'] = dirigeant5.civiliteRepresentant5.personneLieePPLieuNaissanceCommnune.getId();
        }
        if (not Value('id').of(dirigeant5.civiliteRepresentant5.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
        {
            occurrence['R06.3'] = dirigeant5.civiliteRepresentant5.personneLieePPLieuNaissancePays.getLabel();
        }
        if (Value('id').of(dirigeant5.civiliteRepresentant5.personneLieePPLieuNaissancePays).eq('FRXXXXX'))
        {
            occurrence['R06.4'] = dirigeant5.civiliteRepresentant5.personneLieePPLieuNaissanceCommnune.getLabel();
        }
        occurrence['R07'] = dirigeant5.civiliteRepresentant5.personneLieePPNationalite;
    }
    occurrence['R22.2'] = dirigeant5.civilitePersonneMorale5.personneLieePMNumeroIdentification;
    occurrence['R22.3'] = dirigeant5.civilitePersonneMorale5.personneLieePMLieuImmatriculation;
    occurrencesGDR.push(occurrence);
    occurrence = {};
}

// Dirigeant 6
// Dirigeant 7
// Dirigeant 8
// Dirigeant 9
// Dirigeant 10

occurrencesGDR.forEach(function (value, i)
{
    var idx = i + 1;
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/DIU/U50/U50.1'] = value['U50.1'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/DIU/U50/U50.2'] = value['U50.2'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/DIU/U51/U51.1'] = value['U51.1'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/DIU/U52'] = value['U52'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R01/R01.1'] = value['R01.1'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.3'] = value['R02.3'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.5'] = value['R02.5'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.6'] = value['R02.6'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.7'] = value['R02.7'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.8'] = value['R02.8'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.10'] = value['R02.10'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.11'] = value['R02.11'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.12'] = value['R02.12'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.13'] = value['R02.13'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.14'] = value['R02.14'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R03'] = value['R03'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R08'] = value['R08'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R04/R04.2'] = value['R04.2'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R04/R04.3'] = value['R04.3'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R04/R04.4'] = value['R04.4'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R05/R05.3'] = value['R05.3'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R05/R05.5'] = value['R05.5'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R05/R05.6'] = value['R05.6'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R05/R05.7'] = value['R05.7'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R05/R05.8'] = value['R05.8'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R05/R05.10'] = value['R05.10'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R05/R05.11'] = value['R05.11'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R05/R05.12'] = value['R05.12'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R05/R05.13'] = value['R05.13'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R06/R06.1'] = value['R06.1'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R06/R06.2'] = value['R06.2'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R06/R06.3'] = value['R06.3'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R06/R06.4'] = value['R06.4'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R07'] = value['R07'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R22/R22.2'] = value['R22.2'];
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R22/R22.3'] = value['R22.3'];
}
);

// Fin Regent 02M

if (not societe.entrepriseSocieteConstitueSansActivite)
{

    // Groupe ICE : Identification complète de l'établissement

    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E01'] = "1";
    if (exploitation.adresseExploitationDifferente)
    {
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.3'] = exploitation.adresseExploitation.adresseExploitationCommune.getId();
        if (exploitation.adresseExploitation.adresseExploitationNumeroVoie != null)
        {
            regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5'] = exploitation.adresseExploitation.numeroAdresseEtablissement;
        }
        if (exploitation.adresseExploitation.adresseExploitationIndiceVoie != null)
        {
            var monId1 = Value('id').of(exploitation.adresseExploitation.adresseExploitationIndiceVoie)._eval();
            regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.6'] = monId1;
        }
        if (exploitation.adresseExploitation.adresseExploitationDistributionSpecialeVoie != null)
        {
            regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.7'] = exploitation.adresseExploitation.adresseExploitationDistributionSpecialeVoie;
        }
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.8'] = exploitation.adresseExploitation.adresseExploitationCodePostal;
        if (exploitation.adresseExploitation.adresseExploitationComplementVoie != null)
        {
            regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.10'] = exploitation.adresseExploitation.adresseExploitationComplementVoie;
        }
        if (exploitation.adresseExploitation.adresseExploitationTypeVoie != null)
        {
            var monId1 = Value('id').of(exploitation.adresseExploitation.adresseExploitationTypeVoie)._eval();
            regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.11'] = monId1;
        }
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.12'] = exploitation.adresseExploitation.adresseExploitationNomVoie;
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.13'] = exploitation.adresseExploitation.adresseExploitationCommune.getLabel();
    }
    else
    {
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.3'] = siege.siegeAdresseCommune != null ? siege.siegeAdresseCommune.getId() : siege.siegeAdressePays.getId();
        if (siege.siegeAdresseNumeroVoie != null)
        {
            regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5'] = siege.siegeAdresseNumeroVoie;
        }
        if (siege.siegeAdresseIndiceVoie != null)
        {
            var monId1 = Value('id').of(siege.siegeAdresseIndiceVoie)._eval();
            regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.6'] = monId1;
        }
        if (siege.siegeADresseDistributionSpeciale != null)
        {
            regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.7'] = siege.siegeADresseDistributionSpeciale;
        }
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.8'] = siege.siegeAdresseCodePostal != null ? siege.siegeAdresseCodePostal : siegeAdresseCodePostalEtranger;
        if (siege.siegeAdresseComplementVoie != null)
        {
            regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.10'] = siege.siegeAdresseComplementVoie;
        }
        if (siege.siegeAdresseTypeVoie != null)
        {
            var monId1 = Value('id').of(siege.siegeAdresseTypeVoie)._eval();
            regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.11'] = monId1;
        }
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.12'] = siege.siegeAdresseNomVoie;
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.13'] = siege.siegeAdresseVille != null ? siegeAdresseVille : (siege.siegeAdresseCommuneAncienne != null ? siege.siegeAdresseCommune.getLabel() + '/' + siege.siegeAdresseCommuneAncienne : siege.siegeAdresseCommune.getLabel());
        if (not Value('id').of(siege.siegeAdressePays).eq('FRXXXXX'))
        {
            regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.14'] = siege.siegeAdressePays.getLabel();
        }
    }
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E04'] = exploitation.adresseExploitationDifferente ? "3" : ((societe.entrepriseSocieteCommercialeEtrangereOuverturePremierEtab) ? "5" : "2");
    if (exploitation.cadreNomExploitation.etablissementNomExploitation != null)
    {
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E05'] = exploitation.cadreNomExploitation.etablissementNomExploitation;
    }
	
	//Groupe NDE : Nom de domaine du site internet 
	
 if (exploitation.cadreNomExploitation.etablissementNomDomaine != null)
    {	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/NDE/E06/E06.1'] = exploitation.cadreNomExploitation.etablissementNomDomaine;
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/NDE/E06/E06.2'] = "1";
	}
	
    // Groupe ORE : Origine de l'établissement

    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.1'] = Value('id').of(exploitation.cadreOrigineActivite.etablisementOrigineActivite).eq('etablissementOrigineCreation') ? "1" : (Value('id').of(exploitation.cadreOrigineActivite.etablisementOrigineActivite).eq('etablissementOrigineRepriseTotalePartielle') ? "E" : "9");
    if (Value('id').of(exploitation.cadreOrigineActivite.etablisementOrigineActivite).eq('etablissementOrigineAutre'))
    {
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.2'] = "."
    }

    // Groupe PEE : Précédent exploitant de l'établissement

    if (Value('id').of(exploitation.cadreOrigineActivite.etablisementOrigineActivite).eq('etablissementOrigineRepriseTotalePartielle'))
    {
        if (exploitation.cadreOrigineActivite.precedentExploitant.entrepriseLieeSirenPrecedentExploitant != null)
        {
            regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.1'] = exploitation.cadreOrigineActivite.precedentExploitant.entrepriseLieeSirenPrecedentExploitant.split(' ').join('');
        }
        if (Value('id').of(exploitation.cadreOrigineActivite.precedentExploitant.typePersonnePrecedentExploitant).eq('typePersonnePrecedentExploitantPP'))
        {
            regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.2'] = exploitation.cadreOrigineActivite.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant;
            if (exploitation.cadreOrigineActivite.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant != null)
            {
                regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.3'] = exploitation.cadreOrigineActivite.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant;
            }
            if (exploitation.cadreOrigineActivite.precedentExploitant.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant != null and exploitation.cadreOrigineActivite.precedentExploitant.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant[0].length > 0)
            {
                var prenoms = [];
                for (i = 0; i < dirigeant1.civilitePersonnePhysique1.personneLieePPPrenom.size(); i++)
                {
                    prenoms.push(dirigeant1.civilitePersonnePhysique1.personneLieePPPrenom[i]);
                }
                regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.4'] = prenoms;
            }
        }
        if (exploitation.cadreOrigineActivite.precedentExploitant.entrepriseLieeNumeroDetenteurPrecedentExploitant != null)
        {
            regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.5'] = exploitation.cadreOrigineActivite.precedentExploitant.entrepriseLieeNumeroDetenteurPrecedentExploitant;
        }
        if (exploitation.cadreOrigineActivite.precedentExploitant.entrepriseLieeNumeroExploitationPrecedentExploitant != null)
        {
            regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.6'] = exploitation.cadreOrigineActivite.precedentExploitant.entrepriseLieeNumeroExploitationPrecedentExploitant;
        }
        if (Value('id').of(exploitation.cadreOrigineActivite.precedentExploitant.typePersonnePrecedentExploitant).eq('typePersonnePrecedentExploitantPM'))
        {
            regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.7'] = exploitation.cadreOrigineActivite.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant;
        }
    }

    // Groupe LGE --> ne concerne pas les Agricoles qui n'ont pas de location gérance

    // Groupe ACE : Activité de l'établissement
    if (exploitation.informationActivite.activiteSecondaire != null)
    {
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E70'] = exploitation.informationActivite.activiteSecondaire;
    }
/*    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E71'] = exploitation.informationActivite.etablissementActivitesLaPlusImportante != null ? etablissement.etablissementActivitesLaPlusImportante : etablissement.etablissementActivites; */
    
    if (exploitation.informationActivite.activiteEtablissement != null)
    {
        if (Value('id').of(exploitation.informationActivite.activiteEtablissement).eq('etablissementActivitesExerceesAgricoleCulture'))
        {
            regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E71'] = exploitation.informationActivite.questionActiviteCulture != null ? exploitation.informationActivite.questionActiviteCulture.toString() : exploitation.informationActivite.etablissementActivitePlusImportanteAgricoleAutresPreciser;
        }
        else if (Value('id').of(exploitation.informationActivite.activiteEtablissement).eq('etablissementActivitesExerceesAgricoleElevage'))
        {
            regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E71'] = exploitation.informationActivite.questionActiviteElevage != null ? exploitation.informationActivite.questionActiviteElevage.toString() : exploitation.informationActivite.etablissementActivitePlusImportanteAgricoleAutresPreciser;
        }
        else
        {
            regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E71'] = exploitation.informationActivite.questionActiviteAutres != null ? exploitation.informationActivite.questionActiviteAutres.toString() : exploitation.informationActivite.etablissementActivitePlusImportanteAgricoleAutresPreciser;
        }
    }
    // Groupe CAE : Complément sur l'activité de l'établissement --> ne concerne pas les Agricoles Si C05#A

    // Groupe SAE : Salariés de l'établissement

    if (exploitation.cadreEffectifSalarie.etablissementEffectifSalarieEmbauchePremierSalarieOui)
    {
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E83/E83.1'] = "1";
    }
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E84'] = exploitation.cadreEffectifSalarie.etablissementEffectifSalariePresenceOui ? exploitation.cadreEffectifSalarie.etablissementEffectifSalarieAgricoleNombre : "0";
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E86'] = exploitation.cadreEffectifSalarie.etablissementEffectifSalariePresenceOui ? "O" : "N";

    // Groupe IDE : Identification de la personne liée à l'établissement --> pas de Fondé de pouvoir pour les Agricoles
} //fin if (not societe.entrepriseSocieteConstitueSansActivite)


// Call to the XML-REGENT generation WS
var response = nash.service.request('${regent.baseUrl}/private/v1/xml-regent/generate/{regentVersion}/{authorityType}/{authorityId}', regentVersion, authorityType, authorityId)
    .dataType('application/json') //
    .accept('json') //
    .param('listTypeEvenement', eventRegent)
    .continueOnError(true) //
    .post(JSON.stringify(regentFields));

// Record the generated XML-REGENT
if (response != null && response.status == 200)
{
    var xmlRegentStr = response.asBytes();
    nash.record.saveFile("XML_REGENT.xml", xmlRegentStr);
    //debut de l'ajout
    if (authorityId2 != null)
    {
        //MOD extraction du numéro de liasse du premier regent généré
        var numeroLiasse = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02');
        numeroLiasse = JSON.parse(numeroLiasse);

        regentFields['/REGENT-XML/Destinataire'] = authorityId2;
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05'] = "G";
        // Filtre regent greffe
        var undefined;
        // Groupe RFU
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U31'] = undefined;
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U32'] = undefined;
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.1'] = undefined;
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.2'] = undefined;
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U37'] = undefined;
        // Groupe OIU
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OIU/U75'] = undefined;
        // Groupe GCS/ISS
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/ISS/A10/A10.1'] = undefined;
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/ISS/A10/A10.2'] = undefined;
        // Groupe GCS/SNS
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A21/A21.2'] = undefined;
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A21/A21.3'] = undefined;
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A21/A21.4'] = undefined;
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A21/A21.6'] = undefined;
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A22/A22.3'] = undefined;
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A22/A22.4'] = undefined;
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A24/A24.2'] = undefined;
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A24/A24.3'] = undefined;
        // Groupe GCS/CAS
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/CAS/A42/A42.1'] = undefined;
        // Groupe SAE
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E83/E83.1'] = undefined;
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E84'] = undefined;
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E85/E85.8'] = undefined;
        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E86'] = undefined;

        regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS'] = undefined;

        //Génération du deuxième régent
        var response2 = nash.service.request('${regent.baseUrl}/private/v1/xml-regent/generate/{regentVersion}/{authorityType}/{authorityId}', regentVersion, authorityType2, authorityId2)
            .dataType('application/json') //
            .accept('json') //
            .param('listTypeEvenement', eventRegent)
            .param('liasseNumber', numeroLiasse.C02)
            .continueOnError(true)
            .post(JSON.stringify(regentFields));

        //Début de l'ajout
        if (response2 != null && response2.status == 200)
        {
            var xmlRegentStr = response2.asBytes();
            nash.record.saveFile("XML_REGENT2.xml", xmlRegentStr);
        }
        else
        {
            var returnedResponse = response2.asObject();
            var regent2 = returnedResponse['regent'];
            var errors2 = returnedResponse['errors'];
            nash.record.saveFile("XML_REGENT2.xml", regent2.getBytes());
            nash.record.saveFile("XML_VALIDATION_ERRORS_2.xml", errors2.getBytes());

            return spec.create(
            {
                id: 'xmlGenerationConfirmation',
                label: "Xml Regent confirmation message",
                groups: [spec.createGroup(
                    {
                        id: 'Regent ',
                        description: "Une erreur s'est produite lors de la génération du XML Regent.",
                        data: [
                            spec.createData(
                            {
                                id: 'regent',
                                label: "XML regent généré",
                                type: 'Text',
                                mandatory: true,
                                value: regent2
                            }
                            ), spec.createData(
                            {
                                id: 'errors',
                                label: "Erreurs de validations xsd",
                                type: 'Text',
                                mandatory: false,
                                value: errors2
                            }
                            ), spec.createData(
                            {
                                id: 'blockingField',
                                label: "xsd erroné",
                                help: "Ce champs sert à arrêter le process du dossier pour que le support puisse le consulter",
                                type: 'StringReadOnly',
                                mandatory: true
                            }
                            )
                        ]
                    }
                    )]
            }
            );
        }
    }
    return spec.create(
    {
        id: 'xmlGenerationConfirmation',
        label: "Xml Regent confirmation message",
        groups: [spec.createGroup(
            {
                id: 'confirmationMessageOk',
                description: "Le fichier XML Regent a été généré et ajouté au dossier.",
                data: []
            }
            )]
    }
    );
}
else
{
    var returnedResponse = response.asObject();
    var regent = returnedResponse['regent'];
    var errors = returnedResponse['errors'];
    nash.record.saveFile("XML_REGENT.xml", regent.getBytes());
    nash.record.saveFile("XML_VALIDATION_ERRORS.xml", errors.getBytes());

    return spec.create(
    {
        id: 'xmlGenerationConfirmation',
        label: "Xml Regent confirmation message",
        groups: [spec.createGroup(
            {
                id: 'Regent ',
                description: "Une erreur s'est produite lors de la génération du XML Regent de la première autorité.",
                data: [
                    spec.createData(
                    {
                        id: 'regent',
                        label: "XML regent généré",
                        type: 'Text',
                        mandatory: true,
                        value: regent
                    }
                    ), spec.createData(
                    {
                        id: 'errors',
                        label: "Erreurs de validations xsd",
                        type: 'Text',
                        mandatory: false,
                        value: errors
                    }
                    ), spec.createData(
                    {
                        id: 'blockingField',
                        label: "xsd erroné",
                        help: "Ce champs sert à arrêter le process du dossier pour que le support puisse le consulter",
                        type: 'StringReadOnly',
                        mandatory: true
                    }
                    )
                ]
            }
            )]
    }
    );
}
