var attachment= "/3-review/generated/generated.record-1-M0_Agricole_Creation.pdf";
var societe = $m0Agricole.cadreDeclarationPMGroup.cadreDeclarationPM;
var adresse = $m0Agricole.cadreDeclarationPMGroup.siegeAdresseEntreprise;
var adresseEtab = $m0Agricole.cadreDeclarationExploitationActiviteGroup.cadreDeclarationExploitationActivite.adresseExploitation;
var algo = "trouver destinataire";
var secteur1 =  "AGRICOLE";
var secteur2 = '';
var typePersonne = "PM";
var formJuridique = Value('id').of(societe.entrepriseFormeJuridiqueCivile).eq('6598') 	? "EARL" : 
					Value('id').of(societe.entrepriseFormeJuridiqueCivile).eq('6533')	? "GAEC" : 
                    Value('id').of(societe.entrepriseFormeJuridiqueCivile).eq('6534') 	? "GFA"  :
					Value('id').of(societe.entrepriseFormeJuridiqueCivile).eq('6597')  ? "SCEA"  :
					Value('id').of(societe.entrepriseFormeJuridiqueCivile).eq('6597')  ? "Autre société civile "  :
				(	Value('id').of(societe.entrepriseFormeJuridiqueCommerciale).eq('5531') or
					Value('id').of(societe.entrepriseFormeJuridiqueCommerciale).eq('5532') or
					Value('id').of(societe.entrepriseFormeJuridiqueCommerciale).eq('5599') or
					Value('id').of(societe.entrepriseFormeJuridiqueCommerciale).eq('5699') ) ? "SA" :
					Value('id').of(societe.entrepriseFormeJuridiqueCommerciale).eq('5499') ? "SARL" : 
					Value('id').of(societe.entrepriseFormeJuridiqueCommerciale).eq('5710') ? "SAS": "SNC";

var optionCMACCI = "NON";
var formalityWithPayment = false;
var listFraisDestinataires=[];
var listDistinctAuthorities =[];
var destFuncId2 = '';		 
var codeEDI2 = '';
var partnerLabel2 = '';
var codeCommune = '';
if (not Value('id').of(adresse.siegeAdressePays).eq('FRXXXXX')) {
codeCommune = adresseEtab.adresseExploitationCommune.getId();
} else { codeCommune = adresse.siegeAdresseCommune.getId(); }
_log.info("codeCommune is {}",codeCommune);
//Destinataires 
var args = {
    "secteur1" : secteur1,
	"secteur2" : secteur2,
	"typePersonne" : typePersonne,
	"formJuridique" : formJuridique,
	"optionCMACCI":optionCMACCI,
	"codeCommune": codeCommune
};
// Appel directory 
var response = nash.service.request('${directory.baseUrl}/v1/algo/{code}/execute',algo) //
        .connectionTimeout(10000) //
        .receiveTimeout(10000)
        .accept('application/json') //
        .post(args);

var receiverInfo = JSON.parse(response.asString());

// Récupérer la liste des autorités distinctes 
	for(var i=0; i<receiverInfo.listAuthorities.length; i++){	
		if(listDistinctAuthorities.indexOf(receiverInfo.listAuthorities[i]['authority'])== -1){	
			listDistinctAuthorities.push(receiverInfo.listAuthorities[i]['authority']);
		}
	}	
//Récupérer les informations des destinataires :
		var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', listDistinctAuthorities[0]) //
				   .connectionTimeout(10000) //
				   .receiveTimeout(10000) //
				   .accept('json') //
				   .get();
		//result
		var receiverInfo2 = response.asObject();
		
		var funcId = !receiverInfo2.entityId ? null : receiverInfo2.entityId;
		var funcLabel = !receiverInfo2.label ? null :receiverInfo2.label;
		var contactInfo = !receiverInfo2.details ? null : receiverInfo2.details;
		var email = !contactInfo.profile.email ? null : contactInfo.profile.email;
		var tel = !contactInfo.profile.tel ? null : contactInfo.profile.tel;
		
			if(receiverInfo.listAuthorities[0]['role'] == "CFE"){
				var codeEDI = !contactInfo.ediCode ? null : contactInfo.ediCode;
			} else {
				var codeEDI2 = !contactInfo.ediCode ? null : contactInfo.ediCode;	
			}	
		var partnerLabel = !contactInfo.profile.address.recipientName ? null : contactInfo.profile.address.recipientName;
			if(listDistinctAuthorities.length == 2){
				var response2 = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', listDistinctAuthorities[1]) //
					   .connectionTimeout(10000) //
					   .receiveTimeout(10000) //
					   .accept('json') //
					   .get();
				var receiverInfo3 = response2.asObject();
				destFuncId2 = listDistinctAuthorities[1];
				contactInfo = !receiverInfo3.details ? null : receiverInfo3.details;
					if(receiverInfo.listAuthorities[1]['role'] == "CFE"){
						var codeEDI = !contactInfo.ediCode ? null : contactInfo.ediCode;
					} else{
						var codeEDI2 = !contactInfo.ediCode ? null : contactInfo.ediCode;	
					}
				 partnerLabel2 = !contactInfo.profile.address.recipientName ? null : contactInfo.profile.address.recipientName;			
			}	
		var regentXmlTcBloc = spec.createGroup({
									id: 'dataGroup',
									label: "Contenu d'une autorité compétente",
									data: [spec.createData({
											id: 'destFuncId',
											label: "Le code de l'autorité compétente",
											description: 'Code interne',
											type: 'String',
											mandatory: true,
											value: funcId
										}), spec.createData({
											id: 'labelFunc',
											label: "Le libellé fonctionnelle de l'autorité compétente",
											type: 'String',
											mandatory : true,
											value: funcLabel
										}), spec.createData({
											id: 'email',
											label: "L'adresse email de l'autorité compétente",
											type: 'email',
											mandatory:true,
											value: email
										}),	spec.createData({
											id: 'emailPj',
											label: "Le chemin de la pièce jointe",
											type: 'String',
											value: attachment
									}),   spec.createData({
											id: 'tel',
											label: "telephone",
											type: 'String',
											value: tel
									}), spec.createData({
											id: 'partnerLabel',
											label: "Nom autorité",
											type: 'String',
											value: partnerLabel
									}),spec.createData({
											id: 'codeEDI',
											label: "code EDI",
											type: 'String',
											value: codeEDI
									}),
									//début de l'ajout
									spec.createData({
											id: 'destFuncId2',
											label: "code de la deuxième autorité",
											type: 'String',
											value: destFuncId2
									}),spec.createData({
											id: 'codeEDI2',
											label: "code EDI de la deuxième autorité",
											type: 'String',
											value: codeEDI2
									}),spec.createData({
											id: 'partnerLabel2',
											label: "Nom de la deuxième autorité",
											type: 'String',
											value: partnerLabel2
									})
									]
								});

//Condition pour dire qu'une formalité contient un paiement
	//Mettre le Flag de paiement à "vrai"													  
var listCoupleAutoriteFrais=[];
for(var i=0; i<receiverInfo.listAuthorities.length; i++){	
// Dans ce cas nous avons un seul produit destiné aux greffes qui sont des TDR, on parcourt la liste des authorités calculés puis on test chaque role retourné, quand on trouve un TDR on va lui affecter le code du frais de dépot qui nous interesse : 
// Récupérer le réseau de l'autorité en cours de traitement		
	var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', receiverInfo.listAuthorities[i]['authority']) //
			   .connectionTimeout(10000) //
			   .receiveTimeout(10000) //
			   .accept('json') //
			   .get();
	//result
	var currentAuthority = response.asObject();
	var details = !currentAuthority.details ? null : currentAuthority.details;
	var ediCode = !details.ediCode ? null : details.ediCode;
	var authorityReseau = ediCode.charAt(0);		
	
	// Frais pour le RCS et l'autorité est un GREFFE 	
	if(receiverInfo.listAuthorities[i]['role'] == "TDR" && authorityReseau == 'G') {		
	//Récupérer les informations de chaque autorité calculée pour récupérer son libellé
		var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', receiverInfo.listAuthorities[i]['authority']) //
					   .connectionTimeout(10000) //
					   .receiveTimeout(10000) //
					   .accept('json') //
					   .get();   						   
		var authority = response.asObject();			
		var funcLabel = !authority.label ? null :authority.label;	
		if (Value('id').of(societe.entrepriseFormeJuridiqueConstitution).eq('societeCivile') or societe.entrepriseSocieteConstitueSansActivite){
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRCS004"
										};
		} else {
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRCS003"
										};
		}			
		listCoupleAutoriteFrais.push(coupleAutoriteFrais);
	}//fin if Frais pour le RCS et l'autorité 
}	

//Partie générique pour générer le specCreate à passer à paiement Début								
for(var listCoupleAutoriteFraisIdx=0; listCoupleAutoriteFraisIdx<listCoupleAutoriteFrais.length; listCoupleAutoriteFraisIdx++){
	//Récupérer les informations des frais
	var filters = [];
	var prix = 0;
	filters.push("details.entityId:"+listCoupleAutoriteFrais[listCoupleAutoriteFraisIdx]['frais']);
	var currentFraisAuthority = listCoupleAutoriteFrais[listCoupleAutoriteFraisIdx];
	var response = nash.service.request('${directory.baseUrl}/private/v1/repository/{repositoryId}', "frais") //
	   .connectionTimeout(10000) //
	   .receiveTimeout(10000) //
	   .accept('json') //
	   .param("filters",filters)
	   .get();
			   
	var receiverInfo = response.asObject();
	var details = !receiverInfo.content[0].details ? null : receiverInfo.content[0].details;		
	if(details != null){
		var entityId = !details.entityId ? null : details.entityId;
		var prix = !details.prix ? null : details.prix;
		var reseau = !details.reseau ? null : details.reseau;
		var label = !details.label ? null : details.label;
				
		//Construction de la page à afficher au user
		var authorityCode = currentFraisAuthority.authorityCode;
		var authorityLabel = currentFraisAuthority.authorityLabel;
		var fraisCode = currentFraisAuthority.frais;
		var fraisDestination = spec.createGroup({
			'id':'fraisDestinations[' + listCoupleAutoriteFraisIdx +']',
			'label':"Frais à payer",
			'maxOccurs':listCoupleAutoriteFrais.length,
			'data':[
					spec.createData({
						'id': 'authorityCode',
						'label': 'Authorité code :',
						'type': 'StringReadOnly',
						'value': authorityCode
						}),
					spec.createData({
						'id': 'authorityLabel',
						'label': 'Authorité label :',
						'type': 'StringReadOnly',
						'value':authorityLabel
						}),
					spec.createData({
						'id': 'fraisCode',
						'label': 'frais code :',
						'type': 'StringReadOnly',
						'value':fraisCode
						}),
					spec.createData({
						'id': 'fraisLabel',
						'label': 'frais label :',
						'type': 'StringReadOnly',
						'value':label
						}),
					spec.createData({
						'id': 'fraisPrix',
						'label': 'frais prix :',
						'type': 'StringReadOnly',
						'value':""+prix
						}),										
			]
		});
		listFraisDestinataires.push(fraisDestination);
	} else {
		return spec.create({
			id : 'codeFraisNotFound',
			label : 'Erreur lors de la recherche des codes de frais',
			groups : [ spec.createGroup({
			id : 'error',
			label : 'Erreur lors de la recherche des codes de frais',
			description : "Une erreur lors de la recherche des codes de frais dans le référentiel, veuillez contacter le support à l'adresse: support@guichet-entreprises.fr.",
			data : []
			}) ]
			});					
	}//else
} //Partie générique

var indiceFormalitePayante = spec.createGroup({
	'id':'flagFormalitePayante',
	'label':"la formalité est-elle payante?",
	'data':[
		spec.createData({
		'id': 'flagPayment',
		'label': 'flag du paiement :',
		'type': 'Boolean',
		'value':true
		})
	]
});
listFraisDestinataires.push(indiceFormalitePayante);
		
//Partie générique pour générer le specCreate à passer à paiement Fin

//Partie contenant les informations utilisateur à passer au paiement !
var civility = " ";
var lastName = societe.entrepriseDenomination;
var firstName = " ";			

var infoUser = spec.createGroup({
	id: 'infoUser',
	label: "Information de l'utilisateur",
	data: [spec.createData({
				id: 'userLastName',
				label: "Nom de l'utilisateur",
				description: "Nom de l'utilisateur",
				type: 'String',
				mandatory: true,
				value: lastName
						}),
			spec.createData({
				id: 'userFirstName',
				label: "Prénom de l'utilisateur",
				type: 'String',
				mandatory : true,
				value: firstName
						}),
			spec.createData({
				id: 'civility',
				label: "Civilité",
				type: 'String',
				mandatory:true,
				value: civility
						})	
		]
});

return spec.create({
    id : 'computeDestinataire',
    label : "Génération de la liste des frais pour le paiement",
	groups : [ spec.createGroup({
					id : 'information',
					label:"Liste des frais à payer",
					data : listFraisDestinataires
				}),
				regentXmlTcBloc,
				infoUser
	]
});