var societe = $m0Agricole.cadreDeclarationPMGroup.cadreDeclarationPM;
var siege = $m0Agricole.cadreDeclarationPMGroup.siegeAdresseEntreprise;
var pj = $m0Agricole.cadre21SignatureGroup.cadre21Signature;
var userMandataire = $m0Agricole.cadre21SignatureGroup.cadre21Signature.adresseMandataire;
var dirigeant1 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant1.personalite1;
var dirigeant2 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant2.personalite2;
var dirigeant3 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant3.personalite3;
var dirigeant4 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant4.personalite4;
var dirigeant5 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant5.personalite5;
var dirigeant6 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant6.personalite6;
var dirigeant7 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant7.personalite7;
var dirigeant8 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant8.personalite8;
var dirigeant9 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant9.personalite9;
var dirigeant10 = $m0Agricole.cadreDirigeantGroup.cadreDirigeant10.personalite10;
var userDeclarantCivilite1;
var userDeclarantCivilite2;
var userDeclarantCivilite3;
var userDeclarantCivilite4;
var userDeclarantCivilite5;
var userDeclarantCivilite6;
var userDeclarantCivilite7;
var userDeclarantCivilite8;
var userDeclarantCivilite9;
var userDeclarantCivilite10;
var userDeclarant1;
var userDeclarant2;
var userDeclarant3;
var userDeclarant4;
var userDeclarant5;
var userDeclarant6;
var userDeclarant7;
var userDeclarant8;
var userDeclarant9;
var userDeclarant10;
var userMorale1 = dirigeant1.civilitePersonneMorale1.personneLieePMDenomination;
var userMorale2 = dirigeant2.civilitePersonneMorale2.personneLieePMDenomination;
var userMorale3 = dirigeant3.civilitePersonneMorale3.personneLieePMDenomination;
var userMorale4 = dirigeant4.civilitePersonneMorale4.personneLieePMDenomination;
var userMorale5 = dirigeant5.civilitePersonneMorale5.personneLieePMDenomination;
var userMorale6 = dirigeant6.civilitePersonneMorale6.personneLieePMDenomination;
var userMorale7 = dirigeant7.civilitePersonneMorale7.personneLieePMDenomination;
var userMorale8 = dirigeant8.civilitePersonneMorale8.personneLieePMDenomination;
var userMorale9 = dirigeant9.civilitePersonneMorale9.personneLieePMDenomination;
var userMorale10 = dirigeant10.civilitePersonneMorale10.personneLieePMDenomination;
var userRepresentant1;
var userRepresentant2;
var userRepresentantCivilite1;
var userRepresentantCivilite2;

// PJ DIRIGEANT PERSONNE PHYSIQUE
//DIR1
if (Value('id').of(dirigeant1.civilitePersonnePhysique1.personnaliteGenre).eq('genreMasculin')) {
    userDeclarantCivilite1 = 'M.'
} else {
    userDeclarantCivilite1 = 'MME'
}

if (dirigeant1.civilitePersonnePhysique1.personneLieePPNomUsage != null) {
    var userDeclarant1 = userDeclarantCivilite1 + '  ' + dirigeant1.civilitePersonnePhysique1.personneLieePPNomUsage + '  ' + dirigeant1.civilitePersonnePhysique1.personneLieePPPrenom[0];
} else if (dirigeant1.civilitePersonnePhysique1.personneLieePPNomNaissance != null) {
    var userDeclarant1 = userDeclarantCivilite1 + '  ' + dirigeant1.civilitePersonnePhysique1.personneLieePPNomNaissance + '  ' + dirigeant1.civilitePersonnePhysique1.personneLieePPPrenom[0];
}
if (Value('id').of(dirigeant1.personnaliteJuridique).eq('personnePhysique')) { //pas pour les associés indéfiniment responsable
    if (not Value('id').of(dirigeant1.personneLieeQualite).eq('associeIndResp')) {
        attachment('pjDNCDirigeant1', 'pjDNCDirigeant1', {
            label: userDeclarant1,
            mandatory: "true"
        });
    }
    if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and Value('id').of(pj.representantLegalNumero).eq('gerant1')) {
        attachment('pjIDDirigeant1Signataire', 'pjIDDirigeant1Signataire', {
            label: userDeclarant1,
            mandatory: "true"
        });
    } else if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteMandataire')
        or(not Value('id').of(pj.representantLegalNumero).eq('gerant1')
            and Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal'))) {
        attachment('pjIDDirigeant1NonSignataire', 'pjIDDirigeant1NonSignataire', {
            label: userDeclarant1,
            mandatory: "true"
        });
    }
}

//DIR 2
if (Value('id').of(dirigeant2.civilitePersonnePhysique2.personnaliteGenre).eq('genreMasculin')) {
    userDeclarantCivilite2 = 'M.'
} else {
    userDeclarantCivilite2 = 'MME'
}

if (dirigeant2.civilitePersonnePhysique2.personneLieePPNomUsage != null) {
    var userDeclarant2 = userDeclarantCivilite2 + '  ' + dirigeant2.civilitePersonnePhysique2.personneLieePPNomUsage + '  ' + dirigeant2.civilitePersonnePhysique2.personneLieePPPrenom[0];
} else if (dirigeant2.civilitePersonnePhysique2.personneLieePPNomNaissance != null) {
    var userDeclarant2 = userDeclarantCivilite2 + '  ' + dirigeant2.civilitePersonnePhysique2.personneLieePPNomNaissance + '  ' + dirigeant2.civilitePersonnePhysique2.personneLieePPPrenom[0];
    if (Value('id').of(dirigeant2.personnaliteJuridique).eq('personnePhysique')) { //pas pour les associés indéfiniment responsable
        if (not Value('id').of(dirigeant2.personneLieeQualite).eq('associeIndResp')) {
            attachment('pjDNCDirigeant2', 'pjDNCDirigeant2', {
                label: userDeclarant2,
                mandatory: "true"
            });
        }
        if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
            and Value('id').of(pj.representantLegalNumero).eq('gerant2')) {
            attachment('pjIDDirigeant2Signataire', 'pjIDDirigeant2Signataire', {
                label: userDeclarant2,
                mandatory: "true"
            });
        } else if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteMandataire')
            or(not Value('id').of(pj.representantLegalNumero).eq('gerant2')
                and Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal'))) {
            attachment('pjIDDirigeant2NonSignataire', 'pjIDDirigeant2NonSignataire', {
                label: userDeclarant2,
                mandatory: "true"
            });
        }
    }
}

//DIR 3

if (Value('id').of(dirigeant3.civilitePersonnePhysique3.personnaliteGenre).eq('genreMasculin')) {
    userDeclarantCivilite3 = 'M.'
} else {
    userDeclarantCivilite3 = 'MME'
}

if (dirigeant3.civilitePersonnePhysique3.personneLieePPNomUsage != null) {
    var userDeclarant3 = userDeclarantCivilite3 + '  ' + dirigeant3.civilitePersonnePhysique3.personneLieePPNomUsage + '  ' + dirigeant3.civilitePersonnePhysique3.personneLieePPPrenom[0];
} else if (dirigeant3.civilitePersonnePhysique3.personneLieePPNomNaissance != null) {
    var userDeclarant3 = userDeclarantCivilite3 + '  ' + dirigeant3.civilitePersonnePhysique3.personneLieePPNomNaissance + '  ' + dirigeant3.civilitePersonnePhysique3.personneLieePPPrenom[0];
    if (Value('id').of(dirigeant3.personnaliteJuridique).eq('personnePhysique')) { //pas pour les associés indéfiniment responsable
        if (not Value('id').of(dirigeant3.personneLieeQualite).eq('associeIndResp')) {
            attachment('pjDNCDirigeant3', 'pjDNCDirigeant3', {
                label: userDeclarant3,
                mandatory: "true"
            });
        }
        if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
            and Value('id').of(pj.representantLegalNumero).eq('gerant3')) {
            attachment('pjIDDirigeant3Signataire', 'pjIDDirigeant3Signataire', {
                label: userDeclarant3,
                mandatory: "true"
            });
        } else if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteMandataire')
            or(not Value('id').of(pj.representantLegalNumero).eq('gerant3')
                and Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal'))) {
            attachment('pjIDDirigeant3NonSignataire', 'pjIDDirigeant3NonSignataire', {
                label: userDeclarant3,
                mandatory: "true"
            });
        }
    }
}

//DIR 4

if (Value('id').of(dirigeant4.civilitePersonnePhysique4.personnaliteGenre).eq('genreMasculin')) {
    userDeclarantCivilite4 = 'M.'
} else {
    userDeclarantCivilite4 = 'MME'
}

if (dirigeant4.civilitePersonnePhysique4.personneLieePPNomUsage != null) {
    var userDeclarant4 = userDeclarantCivilite4 + '  ' + dirigeant4.civilitePersonnePhysique4.personneLieePPNomUsage + '  ' + dirigeant4.civilitePersonnePhysique4.personneLieePPPrenom[0];
} else if (dirigeant4.civilitePersonnePhysique4.personneLieePPNomNaissance != null) {
    var userDeclarant4 = userDeclarantCivilite4 + '  ' + dirigeant4.civilitePersonnePhysique4.personneLieePPNomNaissance + '  ' + dirigeant4.civilitePersonnePhysique4.personneLieePPPrenom[0];
    if (Value('id').of(dirigeant4.personnaliteJuridique).eq('personnePhysique')) { //pas pour les associés indéfiniment responsable
        if (not Value('id').of(dirigeant4.personneLieeQualite).eq('associeIndResp')) {
            attachment('pjDNCDirigeant4', 'pjDNCDirigeant4', {
                label: userDeclarant4,
                mandatory: "true"
            });
        }
        if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
            and Value('id').of(pj.representantLegalNumero).eq('gerant4')) {
            attachment('pjIDDirigeant4Signataire', 'pjIDDirigeant4Signataire', {
                label: userDeclarant4,
                mandatory: "true"
            });
        } else if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteMandataire')
            or(not Value('id').of(pj.representantLegalNumero).eq('gerant4')
                and Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal'))) {
            attachment('pjIDDirigeant4NonSignataire', 'pjIDDirigeant4NonSignataire', {
                label: userDeclarant4,
                mandatory: "true"
            });
        }
    }
}

//DIR 5

if (Value('id').of(dirigeant5.civilitePersonnePhysique5.personnaliteGenre).eq('genreMasculin')) {
    userDeclarantCivilite5 = 'M.'
} else {
    userDeclarantCivilite5 = 'MME'
}

if (dirigeant5.civilitePersonnePhysique5.personneLieePPNomUsage != null) {
    var userDeclarant5 = userDeclarantCivilite5 + '  ' + dirigeant5.civilitePersonnePhysique5.personneLieePPNomUsage + '  ' + dirigeant5.civilitePersonnePhysique5.personneLieePPPrenom[0];
} else if (dirigeant5.civilitePersonnePhysique5.personneLieePPNomNaissance != null) {
    var userDeclarant5 = userDeclarantCivilite5 + '  ' + dirigeant5.civilitePersonnePhysique5.personneLieePPNomNaissance + '  ' + dirigeant5.civilitePersonnePhysique5.personneLieePPPrenom[0];
    if (Value('id').of(dirigeant5.personnaliteJuridique).eq('personnePhysique')) { //pas pour les associés indéfiniment responsable
        if (not Value('id').of(dirigeant5.personneLieeQualite).eq('associeIndResp')) {
            attachment('pjDNCDirigeant5', 'pjDNCDirigeant5', {
                label: userDeclarant5,
                mandatory: "true"
            });
        }
        if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
            and Value('id').of(pj.representantLegalNumero).eq('gerant5')) {
            attachment('pjIDDirigeant5Signataire', 'pjIDDirigeant5Signataire', {
                label: userDeclarant5,
                mandatory: "true"
            });
        } else if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteMandataire')
            or(not Value('id').of(pj.representantLegalNumero).eq('gerant5')
                and Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal'))) {
            attachment('pjIDDirigeant5NonSignataire', 'pjIDDirigeant5NonSignataire', {
                label: userDeclarant5,
                mandatory: "true"
            });
        }
    }
}

//DIR 6

if (Value('id').of(dirigeant6.civilitePersonnePhysique6.personnaliteGenre).eq('genreMasculin')) {
    userDeclarantCivilite6 = 'M.'
} else {
    userDeclarantCivilite6 = 'MME'
}

if (dirigeant6.civilitePersonnePhysique6.personneLieePPNomUsage != null) {
    var userDeclarant6 = userDeclarantCivilite6 + '  ' + dirigeant6.civilitePersonnePhysique6.personneLieePPNomUsage + '  ' + dirigeant6.civilitePersonnePhysique6.personneLieePPPrenom[0];
} else if (dirigeant6.civilitePersonnePhysique6.personneLieePPNomNaissance != null) {
    var userDeclarant6 = userDeclarantCivilite6 + '  ' + dirigeant6.civilitePersonnePhysique6.personneLieePPNomNaissance + '  ' + dirigeant6.civilitePersonnePhysique6.personneLieePPPrenom[0];
    if (Value('id').of(dirigeant6.personnaliteJuridique).eq('personnePhysique')) { //pas pour les associés indéfiniment responsable
        if (not Value('id').of(dirigeant6.personneLieeQualite).eq('associeIndResp')) {
            attachment('pjDNCDirigeant6', 'pjDNCDirigeant6', {
                label: userDeclarant6,
                mandatory: "true"
            });
        }
        if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
            and Value('id').of(pj.representantLegalNumero).eq('gerant6')) {
            attachment('pjIDDirigeant6Signataire', 'pjIDDirigeant6Signataire', {
                label: userDeclarant6,
                mandatory: "true"
            });
        } else if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteMandataire')
            or(not Value('id').of(pj.representantLegalNumero).eq('gerant6')
                and Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal'))) {
            attachment('pjIDDirigeant6NonSignataire', 'pjIDDirigeant6NonSignataire', {
                label: userDeclarant6,
                mandatory: "true"
            });
        }
    }
}

//DIR 7

if (Value('id').of(dirigeant7.civilitePersonnePhysique7.personnaliteGenre).eq('genreMasculin')) {
    userDeclarantCivilite7 = 'M.'
} else {
    userDeclarantCivilite7 = 'MME'
}

if (dirigeant7.civilitePersonnePhysique7.personneLieePPNomUsage != null) {
    var userDeclarant7 = userDeclarantCivilite7 + '  ' + dirigeant7.civilitePersonnePhysique7.personneLieePPNomUsage + '  ' + dirigeant7.civilitePersonnePhysique7.personneLieePPPrenom[0];
} else if (dirigeant7.civilitePersonnePhysique7.personneLieePPNomNaissance != null) {
    var userDeclarant7 = userDeclarantCivilite7 + '  ' + dirigeant7.civilitePersonnePhysique7.personneLieePPNomNaissance + '  ' + dirigeant7.civilitePersonnePhysique7.personneLieePPPrenom[0];
    if (Value('id').of(dirigeant7.personnaliteJuridique).eq('personnePhysique')) { //pas pour les associés indéfiniment responsable
        if (not Value('id').of(dirigeant7.personneLieeQualite).eq('associeIndResp')) {
            attachment('pjDNCDirigeant7', 'pjDNCDirigeant7', {
                label: userDeclarant7,
                mandatory: "true"
            });
        }
        if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
            and Value('id').of(pj.representantLegalNumero).eq('gerant7')) {
            attachment('pjIDDirigeant7Signataire', 'pjIDDirigeant7Signataire', {
                label: userDeclarant7,
                mandatory: "true"
            });
        } else if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteMandataire')
            or(not Value('id').of(pj.representantLegalNumero).eq('gerant7')
                and Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal'))) {
            attachment('pjIDDirigeant7NonSignataire', 'pjIDDirigeant7NonSignataire', {
                label: userDeclarant7,
                mandatory: "true"
            });
        }
    }
}

//DIR 8

if (Value('id').of(dirigeant8.civilitePersonnePhysique8.personnaliteGenre).eq('genreMasculin')) {
    userDeclarantCivilite8 = 'M.'
} else {
    userDeclarantCivilite8 = 'MME'
}

if (dirigeant8.civilitePersonnePhysique8.personneLieePPNomUsage != null) {
    var userDeclarant8 = userDeclarantCivilite8 + '  ' + dirigeant8.civilitePersonnePhysique8.personneLieePPNomUsage + '  ' + dirigeant8.civilitePersonnePhysique8.personneLieePPPrenom[0];
} else if (dirigeant8.civilitePersonnePhysique8.personneLieePPNomNaissance != null) {
    var userDeclarant8 = userDeclarantCivilite8 + '  ' + dirigeant8.civilitePersonnePhysique8.personneLieePPNomNaissance + '  ' + dirigeant8.civilitePersonnePhysique8.personneLieePPPrenom[0];
    if (Value('id').of(dirigeant8.personnaliteJuridique).eq('personnePhysique')) { //pas pour les associés indéfiniment responsable
        if (not Value('id').of(dirigeant8.personneLieeQualite).eq('associeIndResp')) {
            attachment('pjDNCDirigeant8', 'pjDNCDirigeant8', {
                label: userDeclarant8,
                mandatory: "true"
            });
        }
        if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
            and Value('id').of(pj.representantLegalNumero).eq('gerant8')) {
            attachment('pjIDDirigeant8Signataire', 'pjIDDirigeant8Signataire', {
                label: userDeclarant8,
                mandatory: "true"
            });
        } else if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteMandataire')
            or(not Value('id').of(pj.representantLegalNumero).eq('gerant8')
                and Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal'))) {
            attachment('pjIDDirigeant8NonSignataire', 'pjIDDirigeant8NonSignataire', {
                label: userDeclarant8,
                mandatory: "true"
            });
        }
    }
}

//DIR 9

if (Value('id').of(dirigeant9.civilitePersonnePhysique9.personnaliteGenre).eq('genreMasculin')) {
    userDeclarantCivilite9 = 'M.'
} else {
    userDeclarantCivilite9 = 'MME'
}

if (dirigeant9.civilitePersonnePhysique9.personneLieePPNomUsage != null) {
    var userDeclarant9 = userDeclarantCivilite9 + '  ' + dirigeant9.civilitePersonnePhysique9.personneLieePPNomUsage + '  ' + dirigeant9.civilitePersonnePhysique9.personneLieePPPrenom[0];
} else if (dirigeant9.civilitePersonnePhysique9.personneLieePPNomNaissance != null) {
    var userDeclarant9 = userDeclarantCivilite9 + '  ' + dirigeant9.civilitePersonnePhysique9.personneLieePPNomNaissance + '  ' + dirigeant9.civilitePersonnePhysique9.personneLieePPPrenom[0];
    if (Value('id').of(dirigeant9.personnaliteJuridique).eq('personnePhysique')) { //pas pour les associés indéfiniment responsable
        if (not Value('id').of(dirigeant9.personneLieeQualite).eq('associeIndResp')) {
            attachment('pjDNCDirigeant9', 'pjDNCDirigeant9', {
                label: userDeclarant9,
                mandatory: "true"
            });
        }
        if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
            and Value('id').of(pj.representantLegalNumero).eq('gerant9')) {
            attachment('pjIDDirigeant9Signataire', 'pjIDDirigeant9Signataire', {
                label: userDeclarant9,
                mandatory: "true"
            });
        } else if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteMandataire')
            or(not Value('id').of(pj.representantLegalNumero).eq('gerant9')
                and Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal'))) {
            attachment('pjIDDirigeant9NonSignataire', 'pjIDDirigeant9NonSignataire', {
                label: userDeclarant9,
                mandatory: "true"
            });
        }
    }
}

//DIR 10

if (Value('id').of(dirigeant10.civilitePersonnePhysique10.personnaliteGenre).eq('genreMasculin')) {
    userDeclarantCivilite10 = 'M.'
} else {
    userDeclarantCivilite10 = 'MME'
}

if (dirigeant10.civilitePersonnePhysique10.personneLieePPNomUsage != null) {
    var userDeclarant10 = userDeclarantCivilite10 + '  ' + dirigeant10.civilitePersonnePhysique10.personneLieePPNomUsage + '  ' + dirigeant10.civilitePersonnePhysique10.personneLieePPPrenom[0];
} else if (dirigeant10.civilitePersonnePhysique10.personneLieePPNomNaissance != null) {
    var userDeclarant10 = userDeclarantCivilite10 + '  ' + dirigeant10.civilitePersonnePhysique10.personneLieePPNomNaissance + '  ' + dirigeant10.civilitePersonnePhysique10.personneLieePPPrenom[0];
    if (Value('id').of(dirigeant10.personnaliteJuridique).eq('personnePhysique')) { //pas pour les associés indéfiniment responsable
        if (not Value('id').of(dirigeant10.personneLieeQualite).eq('associeIndResp')) {
            attachment('pjDNCDirigeant10', 'pjDNCDirigeant10', {
                label: userDeclarant10,
                mandatory: "true"
            });
        }
        if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
            and Value('id').of(pj.representantLegalNumero).eq('gerant10')) {
            attachment('pjIDDirigeant10Signataire', 'pjIDDirigeant10Signataire', {
                label: userDeclarant10,
                mandatory: "true"
            });
        } else if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteMandataire')
            or(not Value('id').of(pj.representantLegalNumero).eq('gerant10')
                and Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal'))) {
            attachment('pjIDDirigeant10NonSignataire', 'pjIDDirigeant10NonSignataire', {
                label: userDeclarant10,
                mandatory: "true"
            });
        }
    }
}

// PJ DIRIGEANT PERSONNE MORALE
//DIR1
if (Value('id').of(dirigeant1.personnaliteJuridique).eq('personneMorale')) {
    attachment('pjExtraitImmatriculationRCS1', 'pjExtraitImmatriculationRCS1', {
        label: userMorale1,
        mandatory: "true"
    });

    if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and(not dirigeant1.autreDirigeant
            or Value('id').of(pj.representantLegalNumero).eq('gerant1'))) {
        attachment('pjPersonneMoraleSignataire1', 'pjPersonneMoraleSignataire1', {
            label: userMorale1,
            mandatory: "true"
        });
    }
}
//DIR 2
if (Value('id').of(dirigeant2.personnaliteJuridique).eq('personneMorale')) {
    attachment('pjExtraitImmatriculationRCS2', 'pjExtraitImmatriculationRCS2', {
        label: userMorale2,
        mandatory: "true"
    });

    if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and(not dirigeant2.autreDirigeant
            or Value('id').of(pj.representantLegalNumero).eq('gerant2'))) {
        attachment('pjPersonneMoraleSignataire2', 'pjPersonneMoraleSignataire2', {
            label: userMorale2,
            mandatory: "true"
        });
    }
}

//DIR 3

if (Value('id').of(dirigeant3.personnaliteJuridique).eq('personneMorale')) {
    attachment('pjExtraitImmatriculationRCS3', 'pjExtraitImmatriculationRCS3', {
        label: userMorale3,
        mandatory: "true"
    });

    if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and(not dirigeant3.autreDirigeant
            or Value('id').of(pj.representantLegalNumero).eq('gerant3'))) {
        attachment('pjPersonneMoraleSignataire3', 'pjPersonneMoraleSignataire3', {
            label: userMorale3,
            mandatory: "true"
        });
    }
}

//DIR 4

if (Value('id').of(dirigeant4.personnaliteJuridique).eq('personneMorale')) {
    attachment('pjExtraitImmatriculationRCS4', 'pjExtraitImmatriculationRCS4', {
        label: userMorale4,
        mandatory: "true"
    });

    if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and(not dirigeant4.autreDirigeant
            or Value('id').of(pj.representantLegalNumero).eq('gerant4'))) {
        attachment('pjPersonneMoraleSignataire4', 'pjPersonneMoraleSignataire4', {
            label: userMorale4,
            mandatory: "true"
        });
    }
}

//DIR 5

if (Value('id').of(dirigeant5.personnaliteJuridique).eq('personneMorale')) {
    attachment('pjExtraitImmatriculationRCS5', 'pjExtraitImmatriculationRCS5', {
        label: userMorale5,
        mandatory: "true"
    });

    if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and(not dirigeant5.autreDirigeant
            or Value('id').of(pj.representantLegalNumero).eq('gerant5'))) {
        attachment('pjPersonneMoraleSignataire5', 'pjPersonneMoraleSignataire5', {
            label: userMorale5,
            mandatory: "true"
        });
    }
}

//DIR 6

if (Value('id').of(dirigeant6.personnaliteJuridique).eq('personneMorale')) {
    attachment('pjExtraitImmatriculationRCS6', 'pjExtraitImmatriculationRCS6', {
        label: userMorale6,
        mandatory: "true"
    });

    if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and(not dirigeant6.autreDirigeant
            or Value('id').of(pj.representantLegalNumero).eq('gerant6'))) {
        attachment('pjPersonneMoraleSignataire6', 'pjPersonneMoraleSignataire6', {
            label: userMorale6,
            mandatory: "true"
        });
    }
}

//DIR 7

if (Value('id').of(dirigeant7.personnaliteJuridique).eq('personneMorale')) {
    attachment('pjExtraitImmatriculationRCS7', 'pjExtraitImmatriculationRCS7', {
        label: userMorale7,
        mandatory: "true"
    });

    if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and(not dirigeant7.autreDirigeant
            or Value('id').of(pj.representantLegalNumero).eq('gerant7'))) {
        attachment('pjPersonneMoraleSignataire7', 'pjPersonneMoraleSignataire7', {
            label: userMorale7,
            mandatory: "true"
        });
    }
}

//DIR 8

if (Value('id').of(dirigeant8.personnaliteJuridique).eq('personneMorale')) {
    attachment('pjExtraitImmatriculationRCS8', 'pjExtraitImmatriculationRCS8', {
        label: userMorale8,
        mandatory: "true"
    });

    if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and(not dirigeant8.autreDirigeant
            or Value('id').of(pj.representantLegalNumero).eq('gerant8'))) {
        attachment('pjPersonneMoraleSignataire8', 'pjPersonneMoraleSignataire8', {
            label: userMorale8,
            mandatory: "true"
        });
    }
}

//DIR 9

if (Value('id').of(dirigeant9.personnaliteJuridique).eq('personneMorale')) {
    attachment('pjExtraitImmatriculationRCS9', 'pjExtraitImmatriculationRCS9', {
        label: userMorale9,
        mandatory: "true"
    });

    if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and(not dirigeant9.autreDirigeant
            or Value('id').of(pj.representantLegalNumero).eq('gerant9'))) {
        attachment('pjPersonneMoraleSignataire9', 'pjPersonneMoraleSignataire9', {
            label: userMorale9,
            mandatory: "true"
        });
    }
}

//DIR 10

if (Value('id').of(dirigeant10.personnaliteJuridique).eq('personneMorale')) {
    attachment('pjExtraitImmatriculationRCS10', 'pjExtraitImmatriculationRCS10', {
        label: userMorale10,
        mandatory: "true"
    });

    if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteRepresentantLegal')
        and(not dirigeant10.autreDirigeant
            or Value('id').of(pj.representantLegalNumero).eq('gerant10'))) {
        attachment('pjPersonneMoraleSignataire10', 'pjPersonneMoraleSignataire10', {
            label: userMorale10,
            mandatory: "true"
        });
    }
}

// NOMINATION DIRIGEANT
if (not dirigeant1.statutPresenceDirigeant
    or($m0Agricole.cadreDirigeantGroup.cadreDirigeant1.declarationDirigeant2 and not dirigeant2.statutPresenceDirigeant)
    or($m0Agricole.cadreDirigeantGroup.cadreDirigeant2.declarationDirigeant3 and not dirigeant3.statutPresenceDirigeant)
    or($m0Agricole.cadreDirigeantGroup.cadreDirigeant3.declarationDirigeant4 and not dirigeant4.statutPresenceDirigeant)
    or($m0Agricole.cadreDirigeantGroup.cadreDirigeant4.declarationDirigeant5 and not dirigeant5.statutPresenceDirigeant)
    or($m0Agricole.cadreDirigeantGroup.cadreDirigeant5.declarationDirigeant6 and not dirigeant6.statutPresenceDirigeant)
    or($m0Agricole.cadreDirigeantGroup.cadreDirigeant6.declarationDirigeant7 and not dirigeant7.statutPresenceDirigeant)
    or($m0Agricole.cadreDirigeantGroup.cadreDirigeant7.declarationDirigeant8 and not dirigeant8.statutPresenceDirigeant)
    or($m0Agricole.cadreDirigeantGroup.cadreDirigeant8.declarationDirigeant9 and not dirigeant9.statutPresenceDirigeant)
    or($m0Agricole.cadreDirigeantGroup.cadreDirigeant9.declarationDirigeant10 and not dirigeant10.statutPresenceDirigeant)) {
    attachment('pjNominationDirigeant', 'pjNominationDirigeant', {
        mandatory: "true"
    });
}

// PJ DIRIGEANT REPRÉSENTANT PERMANENT
//REP 1
if (Value('id').of(dirigeant1.civiliteRepresentant1.personnaliteGenre).eq('genreMasculin')) {
    userRepresentantCivilite1 = 'M.'
} else {
    userRepresentantCivilite1 = 'MME'
}

if (dirigeant1.civiliteRepresentant1.personneLieePPNomUsage != null) {
    var userRepresentant1 = userRepresentantCivilite1 + '  ' + dirigeant1.civiliteRepresentant1.personneLieePPNomUsage + '  ' + dirigeant1.civiliteRepresentant1.personneLieePPPrenom[0];
} else {
    var userRepresentant1 = userRepresentantCivilite1 + '  ' + dirigeant1.civiliteRepresentant1.personneLieePPNomNaissance + '  ' + dirigeant1.civiliteRepresentant1.personneLieePPPrenom[0];
}

if (dirigeant1.civiliteRepresentant1.personneLieePPNomNaissance != null) {
    attachment('pjDNCRepresentant1', 'pjDNCRepresentant1', {
        label: userRepresentant1,
        mandatory: "true"
    });
    attachment('pjIDRepresentant1NonSignataire', 'pjIDRepresentant1NonSignataire', {
        label: userRepresentant1,
        mandatory: "true"
    });
}

//REP 2
if (Value('id').of(dirigeant2.civiliteRepresentant2.personnaliteGenre).eq('genreMasculin')) {
    userRepresentantCivilite2 = 'M.'
} else {
    userRepresentantCivilite2 = 'MME'
}

if (dirigeant2.civiliteRepresentant2.personneLieePPNomUsage != null) {
    var userRepresentant2 = userRepresentantCivilite2 + '  ' + dirigeant2.civiliteRepresentant2.personneLieePPNomUsage + '  ' + dirigeant2.civiliteRepresentant2.personneLieePPPrenom[0];
} else {
    var userRepresentant2 = userRepresentantCivilite2 + '  ' + dirigeant2.civiliteRepresentant2.personneLieePPNomNaissance + '  ' + dirigeant2.civiliteRepresentant2.personneLieePPPrenom[0];
}

if (dirigeant2.civiliteRepresentant2.personneLieePPNomNaissance != null) {
    attachment('pjDNCRepresentant2', 'pjDNCRepresentant2', {
        label: userRepresentant2,
        mandatory: "true"
    });
    attachment('pjIDRepresentant2NonSignataire', 'pjIDRepresentant2NonSignataire', {
        label: userRepresentant2,
        mandatory: "true"
    });
}
//REP 3
if (Value('id').of(dirigeant3.civiliteRepresentant3.personnaliteGenre).eq('genreMasculin')) {
    userRepresentantCivilite3 = 'M.'
} else {
    userRepresentantCivilite3 = 'MME'
}

if (dirigeant3.civiliteRepresentant3.personneLieePPNomUsage != null) {
    var userRepresentant3 = userRepresentantCivilite3 + '  ' + dirigeant3.civiliteRepresentant3.personneLieePPNomUsage + '  ' + dirigeant3.civiliteRepresentant3.personneLieePPPrenom[0];
} else {
    var userRepresentant3 = userRepresentantCivilite3 + '  ' + dirigeant3.civiliteRepresentant3.personneLieePPNomNaissance + '  ' + dirigeant3.civiliteRepresentant3.personneLieePPPrenom[0];
}

if (dirigeant3.civiliteRepresentant3.personneLieePPNomNaissance != null) {
    attachment('pjDNCRepresentant3', 'pjDNCRepresentant3', {
        label: userRepresentant3,
        mandatory: "true"
    });
    attachment('pjIDRepresentant3NonSignataire', 'pjIDRepresentant3NonSignataire', {
        label: userRepresentant3,
        mandatory: "true"
    });
}
//REP 4
if (Value('id').of(dirigeant4.civiliteRepresentant4.personnaliteGenre).eq('genreMasculin')) {
    userRepresentantCivilite4 = 'M.'
} else {
    userRepresentantCivilite4 = 'MME'
}

if (dirigeant4.civiliteRepresentant4.personneLieePPNomUsage != null) {
    var userRepresentant4 = userRepresentantCivilite4 + '  ' + dirigeant4.civiliteRepresentant4.personneLieePPNomUsage + '  ' + dirigeant4.civiliteRepresentant4.personneLieePPPrenom[0];
} else {
    var userRepresentant4 = userRepresentantCivilite4 + '  ' + dirigeant4.civiliteRepresentant4.personneLieePPNomNaissance + '  ' + dirigeant4.civiliteRepresentant4.personneLieePPPrenom[0];
}

if (dirigeant4.civiliteRepresentant4.personneLieePPNomNaissance != null) {
    attachment('pjDNCRepresentant4', 'pjDNCRepresentant4', {
        label: userRepresentant4,
        mandatory: "true"
    });
    attachment('pjIDRepresentant4NonSignataire', 'pjIDRepresentant4NonSignataire', {
        label: userRepresentant4,
        mandatory: "true"
    });
}
//REP 5
if (Value('id').of(dirigeant5.civiliteRepresentant5.personnaliteGenre).eq('genreMasculin')) {
    userRepresentantCivilite5 = 'M.'
} else {
    userRepresentantCivilite5 = 'MME'
}

if (dirigeant5.civiliteRepresentant5.personneLieePPNomUsage != null) {
    var userRepresentant5 = userRepresentantCivilite5 + '  ' + dirigeant5.civiliteRepresentant5.personneLieePPNomUsage + '  ' + dirigeant5.civiliteRepresentant5.personneLieePPPrenom[0];
} else {
    var userRepresentant5 = userRepresentantCivilite5 + '  ' + dirigeant5.civiliteRepresentant5.personneLieePPNomNaissance + '  ' + dirigeant5.civiliteRepresentant5.personneLieePPPrenom[0];
}

if (dirigeant5.civiliteRepresentant5.personneLieePPNomNaissance != null) {
    attachment('pjDNCRepresentant5', 'pjDNCRepresentant5', {
        label: userRepresentant5,
        mandatory: "true"
    });
    attachment('pjIDRepresentant5NonSignataire', 'pjIDRepresentant5NonSignataire', {
        label: userRepresentant5,
        mandatory: "true"
    });
}
//REP 6
if (Value('id').of(dirigeant6.civiliteRepresentant6.personnaliteGenre).eq('genreMasculin')) {
    userRepresentantCivilite6 = 'M.'
} else {
    userRepresentantCivilite6 = 'MME'
}

if (dirigeant6.civiliteRepresentant6.personneLieePPNomUsage != null) {
    var userRepresentant6 = userRepresentantCivilite6 + '  ' + dirigeant6.civiliteRepresentant6.personneLieePPNomUsage + '  ' + dirigeant6.civiliteRepresentant6.personneLieePPPrenom[0];
} else {
    var userRepresentant6 = userRepresentantCivilite6 + '  ' + dirigeant6.civiliteRepresentant6.personneLieePPNomNaissance + '  ' + dirigeant6.civiliteRepresentant6.personneLieePPPrenom[0];
}

if (dirigeant6.civiliteRepresentant6.personneLieePPNomNaissance != null) {
    attachment('pjDNCRepresentant6', 'pjDNCRepresentant6', {
        label: userRepresentant6,
        mandatory: "true"
    });
    attachment('pjIDRepresentant6NonSignataire', 'pjIDRepresentant6NonSignataire', {
        label: userRepresentant6,
        mandatory: "true"
    });
}
//REP 7
if (Value('id').of(dirigeant7.civiliteRepresentant7.personnaliteGenre).eq('genreMasculin')) {
    userRepresentantCivilite7 = 'M.'
} else {
    userRepresentantCivilite7 = 'MME'
}

if (dirigeant7.civiliteRepresentant7.personneLieePPNomUsage != null) {
    var userRepresentant7 = userRepresentantCivilite7 + '  ' + dirigeant7.civiliteRepresentant7.personneLieePPNomUsage + '  ' + dirigeant7.civiliteRepresentant7.personneLieePPPrenom[0];
} else {
    var userRepresentant7 = userRepresentantCivilite7 + '  ' + dirigeant7.civiliteRepresentant7.personneLieePPNomNaissance + '  ' + dirigeant7.civiliteRepresentant7.personneLieePPPrenom[0];
}

if (dirigeant7.civiliteRepresentant7.personneLieePPNomNaissance != null) {
    attachment('pjDNCRepresentant7', 'pjDNCRepresentant7', {
        label: userRepresentant7,
        mandatory: "true"
    });
    attachment('pjIDRepresentant7NonSignataire', 'pjIDRepresentant7NonSignataire', {
        label: userRepresentant7,
        mandatory: "true"
    });
}
//REP 8
if (Value('id').of(dirigeant8.civiliteRepresentant8.personnaliteGenre).eq('genreMasculin')) {
    userRepresentantCivilite8 = 'M.'
} else {
    userRepresentantCivilite8 = 'MME'
}

if (dirigeant8.civiliteRepresentant8.personneLieePPNomUsage != null) {
    var userRepresentant8 = userRepresentantCivilite8 + '  ' + dirigeant8.civiliteRepresentant8.personneLieePPNomUsage + '  ' + dirigeant8.civiliteRepresentant8.personneLieePPPrenom[0];
} else {
    var userRepresentant8 = userRepresentantCivilite8 + '  ' + dirigeant8.civiliteRepresentant8.personneLieePPNomNaissance + '  ' + dirigeant8.civiliteRepresentant8.personneLieePPPrenom[0];
}

if (dirigeant8.civiliteRepresentant8.personneLieePPNomNaissance != null) {
    attachment('pjDNCRepresentant8', 'pjDNCRepresentant8', {
        label: userRepresentant8,
        mandatory: "true"
    });
    attachment('pjIDRepresentant8NonSignataire', 'pjIDRepresentant8NonSignataire', {
        label: userRepresentant8,
        mandatory: "true"
    });
}
//REP 9
if (Value('id').of(dirigeant9.civiliteRepresentant9.personnaliteGenre).eq('genreMasculin')) {
    userRepresentantCivilite9 = 'M.'
} else {
    userRepresentantCivilite9 = 'MME'
}

if (dirigeant9.civiliteRepresentant9.personneLieePPNomUsage != null) {
    var userRepresentant9 = userRepresentantCivilite9 + '  ' + dirigeant9.civiliteRepresentant9.personneLieePPNomUsage + '  ' + dirigeant9.civiliteRepresentant9.personneLieePPPrenom[0];
} else {
    var userRepresentant9 = userRepresentantCivilite9 + '  ' + dirigeant9.civiliteRepresentant9.personneLieePPNomNaissance + '  ' + dirigeant9.civiliteRepresentant9.personneLieePPPrenom[0];
}

if (dirigeant9.civiliteRepresentant9.personneLieePPNomNaissance != null) {
    attachment('pjDNCRepresentant9', 'pjDNCRepresentant9', {
        label: userRepresentant9,
        mandatory: "true"
    });
    attachment('pjIDRepresentant9NonSignataire', 'pjIDRepresentant9NonSignataire', {
        label: userRepresentant9,
        mandatory: "true"
    });
}
//REP 10
if (Value('id').of(dirigeant10.civiliteRepresentant10.personnaliteGenre).eq('genreMasculin')) {
    userRepresentantCivilite10 = 'M.'
} else {
    userRepresentantCivilite10 = 'MME'
}

if (dirigeant10.civiliteRepresentant10.personneLieePPNomUsage != null) {
    var userRepresentant10 = userRepresentantCivilite10 + '  ' + dirigeant10.civiliteRepresentant10.personneLieePPNomUsage + '  ' + dirigeant10.civiliteRepresentant10.personneLieePPPrenom[0];
} else {
    var userRepresentant10 = userRepresentantCivilite10 + '  ' + dirigeant10.civiliteRepresentant10.personneLieePPNomNaissance + '  ' + dirigeant10.civiliteRepresentant10.personneLieePPPrenom[0];
}

if (dirigeant10.civiliteRepresentant10.personneLieePPNomNaissance != null) {
    attachment('pjDNCRepresentant10', 'pjDNCRepresentant10', {
        label: userRepresentant10,
        mandatory: "true"
    });
    attachment('pjIDRepresentant10NonSignataire', 'pjIDRepresentant10NonSignataire', {
        label: userRepresentant10,
        mandatory: "true"
    });
}

//RENSEIGNEMENTS RELATIFS AUX COMMISSAIRES AUX COMPTES
// PJ COMMISSAIRE AUX COMPTES TITULAIRE
if (
    Value('id').of(dirigeant1.personneLieeQualite).eq('commissaireComptesTitulaire')
    or Value('id').of(dirigeant2.personneLieeQualite).eq('commissaireComptesTitulaire')
    or Value('id').of(dirigeant3.personneLieeQualite).eq('commissaireComptesTitulaire')
    or Value('id').of(dirigeant4.personneLieeQualite).eq('commissaireComptesTitulaire')
    or Value('id').of(dirigeant5.personneLieeQualite).eq('commissaireComptesTitulaire')
    or Value('id').of(dirigeant6.personneLieeQualite).eq('commissaireComptesTitulaire')
    or Value('id').of(dirigeant7.personneLieeQualite).eq('commissaireComptesTitulaire')
    or Value('id').of(dirigeant8.personneLieeQualite).eq('commissaireComptesTitulaire')
    or Value('id').of(dirigeant9.personneLieeQualite).eq('commissaireComptesTitulaire')
    or Value('id').of(dirigeant10.personneLieeQualite).eq('commissaireComptesTitulaire')) {
    attachment('pjJustificatifInscriptionCACTitulaire', 'pjJustificatifInscriptionCACTitulaire', {
        mandatory: "false"
    });
    attachment('pjLettreAcceptationCACTitulaire', 'pjLettreAcceptationCACTitulaire', {
        mandatory: "true"
    });
}

// PJ COMMISSAIRE AUX COMPTES SUPPLÉANT
if (Value('id').of(dirigeant1.personneLieeQualite).eq('commissaireComptesSuppleant')
    or Value('id').of(dirigeant2.personneLieeQualite).eq('commissaireComptesSuppleant')
    or Value('id').of(dirigeant3.personneLieeQualite).eq('commissaireComptesSuppleant')
    or Value('id').of(dirigeant4.personneLieeQualite).eq('commissaireComptesSuppleant')
    or Value('id').of(dirigeant5.personneLieeQualite).eq('commissaireComptesSuppleant')
    or Value('id').of(dirigeant6.personneLieeQualite).eq('commissaireComptesSuppleant')
    or Value('id').of(dirigeant7.personneLieeQualite).eq('commissaireComptesSuppleant')
    or Value('id').of(dirigeant8.personneLieeQualite).eq('commissaireComptesSuppleant')
    or Value('id').of(dirigeant9.personneLieeQualite).eq('commissaireComptesSuppleant')
    or Value('id').of(dirigeant10.personneLieeQualite).eq('commissaireComptesSuppleant')) {
    attachment('pjJustificatifInscriptionCACSuppleant', 'pjJustificatifInscriptionCACSuppleant', {
        mandatory: "false"
    });
    attachment('pjLettreAcceptationCACSuppleant', 'pjLettreAcceptationCACSuppleant', {
        mandatory: "true"
    });
}
// PJ MANDATAIRE
if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteMandataire')) {
    attachment('pjPouvoir', 'pjPouvoir', {
        mandatory: "true"
    });
    attachment('pjIDMandataireSignataire', 'pjIDMandataireSignataire', {
        label: userMandataire.nomPrenomDenominationMandataire,
        mandatory: "true"
    });
}

// PJ IDENTIFICATION SOCIÉTÉ
if (Value('id').of(siege.siegeAdressePays).eq('FRXXXXX')) {
    if (not societe.entrepriseAssocieUniqueGerant and not Value('id').of(societe.entrepriseStatutType).eq('entrepriseStatutTypeSansModification')) {
        attachment('pjStatutSigne', 'pjStatutSigne', {
            mandatory: "true"
        });
        //sauf SARL dont l'associé est gérant unique adoptant les statuts types
    }
    attachment('pjAttestationParutionLegales', 'pjAttestationParutionLegales', {
        mandatory: "true"
    });
    if (Value('id').of(societe.entrepriseFormeJuridiqueCivile).eq('6598')
        and(not societe.entrepriseCapitalApportNumeraire or societe.entrepriseCapitalApport30000)) {
        attachment('pjRapportCommissaire', 'pjRapportCommissaire', {
            mandatory: "true"
        });
    }
    if (Value('id').of(societe.entrepriseFormeJuridiqueCivile).eq('6533')) {
        attachment('pjAgrementPrefectoral', 'pjAgrementPrefectoral', {
            mandatory: "true"
        });
    }
} else {
    attachment('pjStatutsEtr', 'pjStatutsEtr', {
        mandatory: "true"
    });
}

// RENSEIGNEMENTS RELATIFS AU SIEGE DE L'ENTREPRISE :
if (Value('id').of(siege.questionDomiciliation).eq('locauxSociete')) {
    attachment('pjJustificatifLocaux', 'pjJustificatifLocaux', {
        mandatory: "true"
    });
}
if (Value('id').of(siege.questionDomiciliation).eq('locauxHabitation')) {
    attachment('pjJustificatifHabitation', 'pjJustificatifHabitation', {
        mandatory: "true"
    });

}
if (Value('id').of(siege.questionDomiciliation).eq('locauxEntrepriseDomiciliation')) {
    attachment('pjJustificatifDomiciliation', 'pjJustificatifDomiciliation', {
        mandatory: "true"
    });
}

// PJ MBE	

attachment('formulaireBE1', 'formulaireBE1', {mandatory:"true"});
if (not Value('id').of(pj.nombreMBE).eq('un')) {
attachment('formulaireBE2', 'formulaireBE2', {mandatory:"true"});
}
if (not Value('id').of(pj.nombreMBE).eq('un') and not Value('id').of(pj.nombreMBE).eq('deux')) {
attachment('formulaireBE3', 'formulaireBE3', {mandatory:"true"});
}
if (Value('id').of(pj.nombreMBE).eq('quatre') or Value('id').of(pj.nombreMBE).eq('cinq')) {
attachment('formulaireBE4', 'formulaireBE4', {mandatory:"true"});
}
if (Value('id').of(pj.nombreMBE).eq('cinq')) {
attachment('formulaireBE5', 'formulaireBE5', {mandatory:"true"});
}
