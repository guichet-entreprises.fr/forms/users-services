function pad(s) { return (s < 10) ? '0' + s : s; }
var formFields = {};

// Cadre  Identification de l'entreprise employeur
var entreprise = $e0.cadre1IdentiteGroup.cadre1Identite; 
// Personne morale
formFields['nomEntreprise']                   = Value('id').of(entreprise.entrepriseType).eq('entreprisePM') ? entreprise.civilitePersonneMorale.nomEntreprise : ' ' ;
formFields['formeJuridique']                  = Value('id').of(entreprise.entrepriseType).eq('entreprisePM') ? entreprise.civilitePersonneMorale.formeJuridique : ' ';
formFields['nomCommercial']                   = Value('id').of(entreprise.entrepriseType).eq('entreprisePM') ? entreprise.civilitePersonneMorale.nomCommercial : ' ';

// Personne physique
formFields['civiliteMasculinPP']              = Value('id').of(entreprise.civilitePP.civilite).eq('civiliteMasculinPP') ? true : false;
formFields['civiliteFemininPP']               = Value('id').of(entreprise.civilitePP.civilite).eq('civiliteFemininPP') ? true : false;
formFields['nomPP']                           = Value('id').of(entreprise.entrepriseType).eq('entreprisePP') ? (entreprise.civilitePP.usagePP != null ? (entreprise.civilitePP.usagePP + ' ' + (Value('id').of(entreprise.civilitePP.civilite).eq('civiliteMasculinPP') ? "né" : "née") + ' ' + entreprise.civilitePP.nomPP) : entreprise.civilitePP.nomPP ) : ' ';
formFields['prenomPP']                        = Value('id').of(entreprise.entrepriseType).eq('entreprisePP') ? entreprise.civilitePP.prenomPP : ' ';
formFields['dateNaissancePP']				  = entreprise.civilitePP.dateNaissancePP != null ? entreprise.civilitePP.dateNaissancePP : '';
formFields['communeNaissancePP']              = Value('id').of(entreprise.entrepriseType).eq('entreprisePP') ? entreprise.civilitePP.communeNaissancePP : ' ' ;
formFields['paysNaissanxePP']                 = Value('id').of(entreprise.entrepriseType).eq('entreprisePP') ? entreprise.civilitePP.paysNaissanxePP : ' ';
formFields['nationalitePP']                   = Value('id').of(entreprise.entrepriseType).eq('entreprisePP') ? entreprise.civilitePP.nationalitePP : ' ';


// Adresse
formFields['adresseVoie']                     = entreprise.adresseEntreprise.adresseVoie;
formFields['adresseComplement']               = entreprise.adresseEntreprise.adresseComplement != null ? entreprise.adresseEntreprise.adresseComplement : ' ';
formFields['adresseCommune']                  = entreprise.adresseEntreprise.adresseCommune;
formFields['adresseRegion']                   = entreprise.adresseEntreprise.adresseRegion != null ? entreprise.adresseEntreprise.adresseRegion : '';
formFields['adresseCodePostal']               = entreprise.adresseEntreprise.adresseCodePostal != null ? entreprise.adresseEntreprise.adresseCodePostal : '';
formFields['adressePays']                     = entreprise.adresseEntreprise.adressePays;

// Registre
formFields['lieuRegistrePublic']              = entreprise.registreEntreprise.lieuRegistrePublic;
for (var i = 0;i < entreprise.registreEntreprise.numeroEnregistrementRegistre.size(); i++) {
formFields['numeroEnregistrementRegistre' + i]         = entreprise.registreEntreprise.numeroEnregistrementRegistre[i];
}
/*
for (var i = entreprise.registreEntreprise.size(); i < 4; i++) {
formFields['numeroEnregistrementRegistre' + i]         = '';
}*/

// Cadre  Activite
var activite = $e0.cadreActiviteGroup.cadreActivite;
if(activite.dateCreationEntreprise != null) {
	var dateTmp = new Date(parseInt(activite.dateCreationEntreprise.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['dateCreationEntreprise'] = date;
}
formFields['activiteEntreprise']              = activite.activiteEntreprise;
formFields['activitePrincipale']              = activite.activitePrincipale;
formFields['nature_montage']                  = Value('id').of(activite.activiteNature).eq('natureMontage') ? true : false;
formFields['nature_commerceDetail']           = Value('id').of(activite.activiteNature).eq('natureCommerceDetail') ? true : false;
formFields['nature_services']                 = Value('id').of(activite.activiteNature).eq('natureServices') ? true : false;
formFields['nature_commerceGros']             = Value('id').of(activite.activiteNature).eq('natureCommerceGros') ? true : false;
formFields['nature_reparation']               = Value('id').of(activite.activiteNature).eq('natureReparation') ? true : false;
formFields['nature_batiment']                 = Value('id').of(activite.activiteNature).eq('natureBatiment') ? true : false;
formFields['nature_importExport']             = Value('id').of(activite.activiteNature).eq('natureImportExport') ? true : false;
formFields['nature_servicesEntreprises']      = Value('id').of(activite.activiteNature).eq('natureServicesEntreprises') ? true : false;
formFields['nature_autreLibelle']             = Value('id').of(activite.activiteNature).eq('natureAutreCoche') ? activite.natureAutreLibelle : '';
formFields['nature_autreCoche']               = Value('id').of(activite.activiteNature).eq('natureAutreCoche') ? true : false;

// Cadre Effectif salarié
var effectif = $e0.cadreActiviteGroup.cadreEffectifSalarie
formFields['effectifSalarieNombre']           = effectif.effectifSalarieNombre;
if(effectif.dateEmbauchePremierSalarie != null) {
	var dateTmp = new Date(parseInt(effectif.dateEmbauchePremierSalarie.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['dateEmbauchePremierSalarie'] = date;
}

// Cadre représentant 
var representant = $e0.obligationsSocialesFiscales.cadreRepresentant;
// Personne morale
formFields['denominationRepresentant']        = representant.civilitePMRepresentant.denominationRepresentant != null ? representant.civilitePMRepresentant.denominationRepresentant : '';
formFields['formeJuridiqueRepresentant']      = representant.civilitePMRepresentant.formeJuridiqueRepresentant != null ? representant.civilitePMRepresentant.formeJuridiqueRepresentant : '';
formFields['nomCommercialiRepresentant']      = representant.civilitePMRepresentant.nomCommercialRepresentant != null ? representant.civilitePMRepresentant.nomCommercialRepresentant : '';
formFields['sirenRepresentantPM']             = representant.civilitePMRepresentant.sirenRepresentantPM != null ? representant.civilitePMRepresentant.sirenRepresentantPM.split(' ').join('') : '';

// Personne physique
formFields['civiliteMasculinRepresentant']    = Value('id').of(representant.civilitePPRepresentant.civilite).eq('civiliteMasculinRepresentant') ? true : false;
formFields['civiliteFemininRepresentant']     = Value('id').of(representant.civilitePPRepresentant.civilite).eq('civiliteFemininRepresentant') ? true : false;
formFields['nomRepresentant']                 = Value('id').of(representant.representantType).eq('representantPP') ? (representant.civilitePPRepresentant.usageRepresentant != null ? (representant.civilitePPRepresentant.usageRepresentant + ' ' + (Value('id').of(representant.civilitePPRepresentant.civilite).eq('civiliteMasculinRepresentant') ? "né" : "née") + ' ' + representant.civilitePPRepresentant.nomRepresentant) : representant.civilitePPRepresentant.nomRepresentant ) : ' ';
formFields['prenomRepresentant']              = representant.civilitePPRepresentant.prenomRepresentant != null ? representant.civilitePPRepresentant.prenomRepresentant : '';
formFields['dateNaissanceRepresentant']       = representant.civilitePPRepresentant.dateNaissanceRepresentant != null ? representant.civilitePPRepresentant.dateNaissanceRepresentant : '';
formFields['communeNaissanceRepresentant']    = representant.civilitePPRepresentant.communeNaissanceRepresentant != null ? representant.civilitePPRepresentant.communeNaissanceRepresentant : '';
formFields['paysNaissanceRepresentant']       = representant.civilitePPRepresentant.paysNaissanceRepresentant != null ? representant.civilitePPRepresentant.paysNaissanceRepresentant : '';
formFields['nationaliteRepresentant']         = representant.civilitePPRepresentant.nationaliteRepresentant != null ? representant.civilitePPRepresentant.nationaliteRepresentant : '';
formFields['sirenRepresentantPP']             = representant.civilitePPRepresentant.sirenRepresentantPP != null ? representant.civilitePPRepresentant.sirenRepresentantPP.split(' ').join('') : '';


// Adresse
formFields['adresseVoieRepresentant']         = representant.adresseRepresentant.adresseVoie;
formFields['adresseComplementRepresentant']   = representant.adresseRepresentant.adresseComplement != null ? representant.adresseRepresentant.adresseComplement : '';
formFields['adresseCodePostalRepresentant']   = representant.adresseRepresentant.adresseCodePostal;
formFields['adresseCommuneRepresentant']      = representant.adresseRepresentant.adresseCommune;


// Cadre Fiscal
formFields['imposableFranceOui']              = $e0.obligationsSocialesFiscales.cadreFiscal.imposableFranceOui ? true : false;
formFields['imposableFranceNon']              = $e0.obligationsSocialesFiscales.cadreFiscal.imposableFranceOui ? false : true;

// Cadre correspondance
var correspondance = $e0.cadreCorrespondanceGroup.cadreCorrespondances;
formFields['nomCorrespondance']               = correspondance.nomCorrespondance;
formFields['adresseVoieCorrespondance']       = correspondance.adresseVoie;
formFields['adresseComplementCorrespondance'] = correspondance.adresseComplement != null ? correspondance.adresseComplement : '';
formFields['adresseCommuneCorrespondance']    = correspondance.adresseCommune;
formFields['adresseRegionCorrespondance']     = correspondance.adresseRegion != null ? correspondance.adresseRegion : '';
formFields['adresseCodePostalCorrespondance'] = correspondance.adresseCodePostal != null ? correspondance.adresseCodePostal : '';
formFields['adressePaysCorrespondance']       = correspondance.adressePays;
formFields['telephone']                       = $e0.cadreCorrespondanceGroup.infosSup.telephone;
formFields['fax']                             = $e0.cadreCorrespondanceGroup.infosSup.courriel;
formFields['courriel']                        = $e0.cadreCorrespondanceGroup.infosSup.fax;

// Cadre signature
var signature = $e0.cadreSignatureGroup.cadreSignature;
formFields['nomSignataire']                   = signature.nomSignataire;
formFields['lieuSignature']                   = signature.lieuSignature;
formFields['dateSignature'] 				  = signature.dateSignature;
formFields['qualiteSignataire']               = signature.qualiteSignataire;
formFields['signature']                       = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce";

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qpxxxPEx.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
var civNomPrenom = signature.nomSignataire
var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		date: signature.dateSignature,
		autoriteHabilitee :"Centre National Firmes Etrangères",
		demandeContexte : "Déclaration d'inscription d'une entreprise employant du personnel salarié et ne comportant pas d'établissement en France.",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));


/*
 * Création du dossier 
 */
 
var cerfaDoc1 = nash.doc //
	.load('models/formulaire_E0.pdf') //
	.apply(formFields); 

	function appendPj(fld) {
	fld.forEach(function (elm) {
        cerfaDoc1.append(elm);
    });
}

/*
 * Ajout des PJs
 */
 
 appendPj($attachmentPreprocess.attachmentPreprocess.pjIDSignataire);
 
 if($e0.obligationsSocialesFiscales.cadreRepresentant.representantPresence) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjConvention);
}
	
/*
 * Enregistrement du fichier (en mémoire)
 */

var finalDoc = cerfaDoc1.save('formulaire_E0.pdf');

var data = [ spec.createData({
    id : 'record',
    label : 'Déclaration d\'inscription d\'une entreprise employant du personnel salarié et ne comportant pas d\'établissement en France.',
    description : 'Voici le formulaire obtenu à partir des données saisies.',
    type : 'FileReadOnly',
    value : [ finalDoc ]
}) ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Déclaration d\'inscription d\'une entreprise employant du personnel salarié et ne comportant pas d\'établissement en France.',
    groups : groups
});