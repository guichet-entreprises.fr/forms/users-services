//--------------- Functions --------------//
function getAuthority(authorityId) {			
	var response = null;
	try {
		response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', authorityId) //
			   .connectionTimeout(10000) //
			   .receiveTimeout(10000) //
			   .accept('json') //
			   .get()
		;
	} catch (e) {	
		log.error(e);			
		throw 'An technical error occured when calling directory with authority uid : ' + authorityId;
	}
	if (response != null && response.getStatus() == 200) {			
		return response.asObject();
	}
	throw 'Cannot find any authority uid : ' + authorityId;
}

function executeAlgo(codeCommune) {
	log.debug('Input commune : {}', codeCommune);
	var authorityInfo = getAuthority(codeCommune);

	var greffeAuthorityId = (!authorityInfo.details || !authorityInfo.details || !authorityInfo.details.GREFFE) ? null : authorityInfo.details.GREFFE;
	
	if (null == greffeAuthorityId) {
		throw "Cannot find any GREFFE authority from " + codeCommune;
	}
	
	log.debug('Target authority : {}', greffeAuthorityId);
	return greffeAuthorityId;
}

var outputUserAttachment = [];

var codeCommune = $dbe.pageSociete.infoSocieteDeclarante.adresseSiege.commune.getId();

var targetAuthority = getAuthority(executeAlgo(codeCommune));
//--------------- Execute algo to determine target GREFFE authority --------------//


//--------------- Bind data --------------//
//-->By default, the postmail channel is selected for GREFFE authority
var dataGreffe = nash.instance.load('display.xml');
dataGreffe.bind('result', {
	'funcId' : targetAuthority.entityId,
	'funcLabel' : targetAuthority.label,
	'channels' : {
		'id' : 'addressChannel',
		'label' : 'Courrier papier'
	}
});

//-->Bind postmail information
if ( null != targetAuthority.details && null != targetAuthority.details.transferChannels && null != targetAuthority.details.transferChannels.address && null != targetAuthority.details.transferChannels.address.addressDetail ) {
	var addressDetail = targetAuthority.details.transferChannels.address.addressDetail;
	log.debug('Address details : {}', addressDetail);
	dataGreffe.bind('result', {
		'address' : {
			'referenceId' : nash.record.description().recordUid,
	        'recipientName' : addressDetail.recipientName,
	        'recipientNameComp' : addressDetail.recipientNameCompl,
	        'recipientAddressName' : addressDetail.addressName,
	        'recipientAddressNameCompl' : addressDetail.addressNameCompl,
	        'recipientPostalCode' : addressDetail.postalCode,
	        'recipientCity' : addressDetail.cityName
		}
	});
}

for (var idx = 0; idx < $review.generated.uploaded.length; idx++) {
	outputUserAttachment.push({'id' : 'attachement[' + idx + ']',  'label' : '/' + $review.generated.uploaded[idx].getAbsolutePath()});
}

log.debug('User attachment : {}', outputUserAttachment);

dataGreffe.bind('parameters', {
	"attachment" : {
		"cerfa" : '/' + $review.generated.cerfa[0].getAbsolutePath(),
		"others" : outputUserAttachment
	}
});
//--------------- Bind data --------------//
