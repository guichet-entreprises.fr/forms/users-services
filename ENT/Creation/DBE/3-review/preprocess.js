function pad(s) { return (s < 10) ? '0' + s : s; }
function getFieldValue(field) { return null == field ? '' : field; }
function getDateFormat(my_date){
	if(my_date == null) {
		return null;
	}
	var dateTmp = new Date(parseInt(my_date.getTimeInMillis()));
	var date_result = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date_result = date_result.concat(pad(month.toString()));
	date_result = date_result.concat(dateTmp.getFullYear().toString());
	return date_result;
}
function getPrenomsAutres(target){
	//construciton de la liste des prenoms
	if(target == null){
		return null;
	}
	var prenoms=[];
	for(var i=1; i<target.size() ; i++){
		prenoms.push(target[i]);
	}
	return prenoms.toString();
}


var pdfFields = {};
var pdfFieldsBis = {};
var listeBeneficiaire = [];                      

var infoSocieteDeclarante = $dbe.pageSociete.infoSocieteDeclarante;
pdfFields['Denomination']           = infoSocieteDeclarante.denomination;
pdfFields['Forme juridique']        = infoSocieteDeclarante.formeJuridique;
pdfFields['Numero_SIREN']           = infoSocieteDeclarante.numeroSIREN;
pdfFields['immatriculation_greffe'] = infoSocieteDeclarante.immatriculationGreffe;


var adresseSiege = infoSocieteDeclarante.adresseSiege
pdfFields['adresse_siege'] = getFieldValue(adresseSiege.numeroVoie) 
							+ ' ' + getFieldValue(adresseSiege.indiceVoie) 
							+ ' ' + getFieldValue(adresseSiege.typeVoie) 
							+ ' ' + adresseSiege.nomVoie;

pdfFields['code_postal_siege'] = getFieldValue(adresseSiege.codePostal);
pdfFields['commune_siege']     = getFieldValue(adresseSiege.commune);
pdfFields['pays_siege']        = "FRANCE";


var infoIdentiteBeneficiaire = $dbe.pageBeneficiaire.infoIdentiteBeneficiaire;
pdfFields['civilite_monsieur'] = Value('id').of(infoIdentiteBeneficiaire.civilite).eq('civiliteMasculin') ? true : false;
pdfFields['civilite_madame'] = Value('id').of(infoIdentiteBeneficiaire.civilite).eq('civiliteFeminin') ? true : false;

pdfFields['nom_naissance']          = infoIdentiteBeneficiaire.nomNaissance;
pdfFields['nom_usage']              = infoIdentiteBeneficiaire.nomUsage;
pdfFields['pseudonyme']             = infoIdentiteBeneficiaire.pseudonyme;
pdfFields['prenom_principal']       = infoIdentiteBeneficiaire.prenom[0];
pdfFields['autres_prenom'] 			= getPrenomsAutres(infoIdentiteBeneficiaire.prenom);

pdfFields['date_naissance'] = infoIdentiteBeneficiaire.dateNaissance;
pdfFields['commune_naissance']= (infoIdentiteBeneficiaire.communeNaissance != null ?  infoIdentiteBeneficiaire.communeNaissance : infoIdentiteBeneficiaire.villeNaissance);
pdfFields['pays_naissance']         = infoIdentiteBeneficiaire.paysNaissance;
pdfFields['nationalite']            = infoIdentiteBeneficiaire.nationalite;

var adresseDomicile = $dbe.pageBeneficiaire.infoIdentiteBeneficiaire.adresse;

pdfFields['adresse_domicile'] = getFieldValue(adresseDomicile.numeroVoie) 
							    + ' ' + getFieldValue(adresseDomicile.indiceVoie) 
								+ ' ' + getFieldValue(adresseDomicile.typeVoie) 
								+ ' ' + getFieldValue(adresseDomicile.nomVoie);

pdfFields['code_postal_domicile']   = adresseDomicile.codePostal != null ? adresseDomicile.codePostal : adresseDomicile.codePostalEtranger;
pdfFields['commune_domicile']       = adresseDomicile.commune != null ? adresseDomicile.commune : adresseDomicile.villeEtrangere;
pdfFields['pays_domicile']          = adresseDomicile.pays;

var infoModaliteControle = $dbe.pageModalite.infoModaliteControle;
var detentionGroup = infoModaliteControle.detentionGroup
pdfFields['detention']              = infoModaliteControle.detention;
pdfFields['directe_capital']        = detentionGroup.directeCapital;
pdfFields['indirecte_capital']      = detentionGroup.indirecteCapital;
pdfFields['detention_capital']      = detentionGroup.detentionCapital;
pdfFields['directe_droit_vote']     = detentionGroup.directeDroitVote;
pdfFields['indirecte_droit_vote']   = detentionGroup.indirecteDroitVote;
pdfFields['detention_droit_vote']   = detentionGroup.detentionDroitVote;
pdfFields['exercice']               = infoModaliteControle.exercice;
pdfFields['representant_legal']     = infoModaliteControle.representantLegal;
pdfFields['date_beneficiaire']      = infoModaliteControle.dateBeneficiaire;

var autresInformation = $dbe.pageModalite.autresInformation;
var pageAutreBeneficiaire = $dbe.pageAutreBeneficiaire;

pdfFields['autre_beneficiaire_non'] = !pageAutreBeneficiaire.autreBeneficiaire;
pdfFields['autre_beneficiaire_oui'] = pageAutreBeneficiaire.autreBeneficiaire;
if(pageAutreBeneficiaire.autreBeneficiaire){
	pdfFields['nb_intercalaire']        = $dbe.pageAutreBeneficiaire.autreBeneficiaireGroup.intercalaire2.size();
}

var signature = $dbe.pageSignature.signature;
pdfFields['lieu_signature']         = signature.lieuSignature;
pdfFields['date_signature']         = signature.dateSignature;
pdfFields['nom_representant_legal'] = signature.nomRepresentantLegal;
pdfFields['signature'] = signature.nomRepresentantLegal;

if(pageAutreBeneficiaire.autreBeneficiaire){
		var mapBeneficiaire = {};
	for(var i = 0; i < $dbe.pageAutreBeneficiaire.autreBeneficiaireGroup.intercalaire2.size(); i++){
		
		_log.info("passage {}", i);
		mapBeneficiaire = {};
		var beneficiaire = $dbe.pageAutreBeneficiaire.autreBeneficiaireGroup.intercalaire2[i];
		
		_log.info("beneficiaire is  {}", beneficiaire);
		mapBeneficiaire['Denomination']           = infoSocieteDeclarante.denomination;
		mapBeneficiaire['Numero_SIREN']           = infoSocieteDeclarante.numeroSIREN;
		mapBeneficiaire['immatriculation_greffe'] = infoSocieteDeclarante.immatriculationGreffe;
		mapBeneficiaire['Forme juridique']        = infoSocieteDeclarante.formeJuridique;
		
		mapBeneficiaire['adresse_siege'] = getFieldValue(adresseSiege.numeroVoie) 
										 	+ ' ' + getFieldValue(adresseSiege.indiceVoie) 
									  	 	+ ' ' + getFieldValue(adresseSiege.typeVoie) 
											+ ' ' + adresseSiege.nomVoie;
		mapBeneficiaire['code_postal_siege'] = getFieldValue(adresseSiege.codePostal);
		mapBeneficiaire['commune_siege']     = getFieldValue(adresseSiege.commune);
		mapBeneficiaire['pays_siege']        = "FRANCE";
		mapBeneficiaire['civilite_monsieur'] = Value('id').of(beneficiaire.civilite).eq('civiliteMasculin') ? true : false;
		mapBeneficiaire['civilite_madame'] = Value('id').of(beneficiaire.civilite).eq('civiliteFeminin') ? true : false;
		mapBeneficiaire['nom_naissance']          = beneficiaire.nomNaissance;
		mapBeneficiaire['nom_usage']              = beneficiaire.nomUsage;
		mapBeneficiaire['pseudonyme']             = beneficiaire.pseudonyme;
		mapBeneficiaire['prenom_principal']       = beneficiaire.prenom[0];
		mapBeneficiaire['autres_prenom'] 		  = getPrenomsAutres(beneficiaire.prenom);
		mapBeneficiaire['date_naissance'] 		  = beneficiaire.dateNaissance;
		mapBeneficiaire['commune_naissance']	  = beneficiaire.communeNaissance != null ?  beneficiaire.communeNaissance : beneficiaire.villeNaissance;
		mapBeneficiaire['pays_naissance']         = beneficiaire.paysNaissance;
		mapBeneficiaire['nationalite']            = beneficiaire.nationalite;
		
		var adresseBeneficiaire = beneficiaire.adresse;
		mapBeneficiaire['adresse_domicile'] = getFieldValue(adresseBeneficiaire.numeroVoie) 
										+ ' ' + getFieldValue(adresseBeneficiaire.indiceVoie) 
										+ ' ' + getFieldValue(adresseBeneficiaire.typeVoie) 
										+ ' ' + adresseBeneficiaire.nomVoie;

		mapBeneficiaire['code_postal_domicile']   = adresseBeneficiaire.codePostal != null ? adresseBeneficiaire.codePostal : adresseBeneficiaire.codePostalEtranger;
		mapBeneficiaire['commune_domicile']       = adresseBeneficiaire.commune != null ? adresseBeneficiaire.commune : adresseBeneficiaire.communeEtrangere;
		mapBeneficiaire['pays_domicile']          = adresseBeneficiaire.pays;


		var infoModaliteControleBeneficiaire = beneficiaire.infoModaliteControle;
		var detentionGroupBeneficiaire = infoModaliteControleBeneficiaire.detentionGroup;
		mapBeneficiaire['detention']              = infoModaliteControleBeneficiaire.detention;
		mapBeneficiaire['directe_capital']        = detentionGroupBeneficiaire.directeCapital;
		mapBeneficiaire['indirecte_capital']      = detentionGroupBeneficiaire.indirecteCapital;
		mapBeneficiaire['detention_capital']      = detentionGroupBeneficiaire.detentionCapital;
		mapBeneficiaire['directe_droit_vote']     = detentionGroupBeneficiaire.directeDroitVote;
		mapBeneficiaire['indirecte_droit_vote']   = detentionGroupBeneficiaire.indirecteDroitVote;
		mapBeneficiaire['detention_droit_vote']   = detentionGroupBeneficiaire.detentionDroitVote;
		mapBeneficiaire['exercice']               = infoModaliteControleBeneficiaire.exercice;
		mapBeneficiaire['representant_legal']     = infoModaliteControleBeneficiaire.representantLegal;
		mapBeneficiaire['date_beneficiaire']      = infoModaliteControleBeneficiaire.dateBeneficiaire;

		listeBeneficiaire.push(mapBeneficiaire);
	}
}

_log.info("listeBeneficiaire length is  {}", listeBeneficiaire.length);
_log.info("listeBeneficiaire is  {}", listeBeneficiaire);

var pageBis = $dbe.pageControleIndirect;
pdfFieldsBis['Greffe']                  = infoSocieteDeclarante.immatriculationGreffe;
pdfFieldsBis['numero_SIREN']            = infoSocieteDeclarante.numeroSIREN;
pdfFieldsBis['denomination']            = infoSocieteDeclarante.denomination;
pdfFieldsBis['nom_naissance']           = infoIdentiteBeneficiaire.nomNaissance;
pdfFieldsBis['nom_usage']               = infoIdentiteBeneficiaire.nomUsage;
pdfFieldsBis['pseudonyme']              = infoIdentiteBeneficiaire.pseudonyme;
pdfFieldsBis['prenom_principal']        = infoIdentiteBeneficiaire.prenom[0];                           
pdfFieldsBis['autres_prenoms'] 			= getPrenomsAutres(infoIdentiteBeneficiaire.prenom);

pdfFieldsBis['controle_indirect_choix'] = infoModaliteControle.exercice || detentionGroup.demembrementPropriete;
pdfFieldsBis['controle_indirect']       = $dbe.pageControleIndirect.controleIndirect;
pdfFieldsBis['pouvoir_controle_choix']  = detentionGroup.indirecteCapital;
pdfFieldsBis['pouvoir_controle']        = $dbe.pagePouvoirControle.pouvoirControle;


var pdfPrincipalModel = nash.doc.load('models/76-2017-4_Document_BE_societe_principal_DBE-S-1.pdf').apply(pdfFields);

for(var i=0; i< listeBeneficiaire.length; i++){
	_log.info("listeBeneficiaire.length is  {}", listeBeneficiaire.length);
	
	var pdfAnnexeModel = nash.doc.load('models/76-2017-5_Document_BE_societe_annexe_DBE-S-2.pdf').apply(listeBeneficiaire[i]);
	pdfPrincipalModel.append(pdfAnnexeModel.save('cerfa_annexe'+ i + '.pdf'));
}

if(infoModaliteControle.exercice || detentionGroup.demembrementPropriete || detentionGroup.indirecteCapital){
	var pdfBisModel = nash.doc.load('models/76-2017-6_Document_BE_societe_Feuillet_DBE-S-bis.pdf').apply(pdfFieldsBis);
	pdfPrincipalModel.append(pdfBisModel.save('cerfa_bis.pdf'));
}

//PJ
var pjUser = [];

$attachmentPreprocess.attachmentPreprocess.pjIDBeneficiaires.forEach(function (elm) {
	pjUser.push(elm);
});

var data = [ spec.createData({
    id : 'cerfa',
    label : 'Bénéficiaire éffectif d’une société',
    description : 'Bénéficiaire éffectif d’une société',
    type : 'FileReadOnly',
    value : [ pdfPrincipalModel.save('DBE.pdf') ]
}) ,  spec.createData({
    id : 'uploaded',
    label : 'Pièces jointes',
    description : 'Pièces jointes ajoutées au formulaire.',
    type : 'FileReadOnly',
    value : pjUser
})];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Generated records',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Review',
    groups : groups
});
