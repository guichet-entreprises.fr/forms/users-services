// PJ Déclarant

var userDeclarant;
if ($p0agricole.cadre1IdentiteGroup.cadre1Identite.personneLieePPNomUsage != null) {
    var userDeclarant = $p0agricole.cadre1IdentiteGroup.cadre1Identite.personneLieePPNomUsage + '  ' + $p0agricole.cadre1IdentiteGroup.cadre1Identite.personneLieePPPrenom[0] ;
} else {
    var userDeclarant = $p0agricole.cadre1IdentiteGroup.cadre1Identite.personneLieePPNomNaissance + '  '+ $p0agricole.cadre1IdentiteGroup.cadre1Identite.personneLieePPPrenom[0] ;
}

var pj=$p0agricole.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteDeclarant')) {
    attachment('pjIDDeclarantSignataire', 'pjIDDeclarantSignataire', { label: userDeclarant, mandatory:"true"});
}

var pj=$p0agricole.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
    attachment('pjIDDeclarantNonSignataire', 'pjIDDeclarantNonSignataire', { label: userDeclarant, mandatory:"true"});
}

// PJ conjoint

var pj=$p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.statutConjointCollaborateur ;
if(Value('id').of(pj).contains('VoletSocialStatutConjointCollaborateur') or Value('id').of(pj).contains('VoletSocialStatutConjointSalarie') or Value('id').of(pj).contains('VoletSocialStatutConjointCoExploitant')) {
    attachment('pjChoixStatutConjoint', 'pjChoixStatutConjoint', { mandatory:"true"});
}

// PJ Mandataire

var userMandataire=$p0agricole.cadre11SignatureGroup.cadre11Signature.adresseMandataire

var pj=$p0agricole.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
    attachment('pjIDMandataireSignataire', 'pjIDMandataireSignataire', {label: userMandataire.nomPrenomDenominationMandataire, mandatory:"true"});
}

// PJ Etablissement

var pj=$p0agricole.cadre4AdresseActiviteGroup.cadre4AdresseActivite.activiteLieu ;
if(Value('id').of(pj).contains('entrepriseAdresseEntrepriseDomicile')) {
    attachment('pjDomicile', 'pjDomicile', { mandatory:"true"});
}

var pj=$p0agricole.cadre4AdresseActiviteGroup.cadre4AdresseActivite.activiteLieu ;
if(Value('id').of(pj).contains('entrepriseAdresseEntrepriseLieuExploitation')) {
    attachment('pjEtablissementCreation', 'pjEtablissementCreation', { mandatory:"true"});
}

// PJ EIRL

var pj=$p0agricole.cadreEIRLGroup.cadreEIRL;
if(pj.estEIRL) {
    attachment('pjDNCDeclarant', 'pjDNCDeclarant',{ label: userDeclarant, mandatory:"true"}) ;
}

var pj=$p0agricole.cadreEIRLGroup.cadreEIRL;
if(pj.estEIRL and (Value('id').of(pj.cadre1DeclarationAffectationPatrimoine.eirlStatut).eq('EirlStatutEIRLReprise') or Value('id').of(pj.cadre1DeclarationAffectationPatrimoine.eirlDepot).eq('eirlAvecDepot'))) {
    attachment('pjDAP', 'pjDAP', { mandatory:"true"});
}

var pj=$p0agricole.cadreEIRLGroup.cadreEIRL;
if(pj.estEIRL) {
    attachment('pjLivretFamille', 'pjLivretFamille', { mandatory:"true"});
}

var pj=$p0agricole.cadreEIRLGroup.cadreEIRL;
if(pj.estEIRL) {
    attachment('pjAttestationAssurance', 'pjAttestationAssurance', { mandatory:"true"});
}

var pj=$p0agricole.cadreEIRLGroup.cadreEIRL;
if(pj.estEIRL) {
    attachment('pjNotificationDroitPaiement', 'pjNotificationDroitPaiement', { mandatory:"false"});
}

var pj=$p0agricole.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine;
if(Value('id').of(pj.cadreEIRLInformationsComplementaires.contenuDAP).contains('bienImmo') or Value('id').of(pj.cadreEIRLInformationsComplementairesBis.contenuDAPBis).contains('bienImmo')) {
    attachment('pjDAPActeNotarie', 'pjDAPActeNotarie', { mandatory:"true"});
}

if(Value('id').of(pj.cadreEIRLInformationsComplementaires.contenuDAP).contains('bienCommunIndivis') or Value('id').of(pj.cadreEIRLInformationsComplementairesBis.contenuDAPBis).contains('bienCommunIndivis')) {
    attachment('pjDAPAccordTiers', 'pjDAPAccordTiers', { mandatory:"true"});
}
var pj=$p0agricole.cadre1IdentiteGroup.cadre1Identite ;
if(not (pj.personneLieePersonnePhysiqueMineurEmancipe) and $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL) {
    attachment('pjAutorisationMineur', 'pjAutorisationMineur', { mandatory:"false"});
}