//prepare info to send 

var adresse = $p0agricole.cadre1IdentiteGroup.cadre1AdresseDeclarant;
var adressePro = $p0agricole.cadre4AdresseActiviteGroup.cadre4AdresseActivite;

var algo = "trouver Cfe";

var secteur1 = "Agricole";

var typePersonne = "PP";

var formJuridique = $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL ? "EIRL" : "EI";

var optionCMACCI = "NON";

var codeCommune = Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseLieuExploitation') ? adressePro.cadre4AdresseProfessionnelle.etablissementAdresseCommune.getId() : adresse.personneLieeAdresseCommune.getId();

var data = nash.instance.load("output.xml")
var identite = $p0agricole.cadre1IdentiteGroup.cadre1Identite;
var denomination = identite.personneLieePPNomNaissance + ' ' + identite.personneLieePPPrenom[0];
	

	
	
/** Other files */

function findUserAttachments() {
	var values = [];
	var metas = nash.record.meta();

	log.debug('meta list => {}', null == metas ? null : metas.metas);

	if (null != metas && null != metas.metas) {
		var valueMap = {};
		metas.metas.forEach(function (meta) {
			log.debug('  ---> meta {} : {}', meta.name, meta.value);
			if ('userAttachments' == meta.name) {
				valueMap[meta.value] = true;
			}
		})
		Object.keys(valueMap).forEach(function (elm) {
			values.push({
				id: elm.replaceAll('^(.*/)([^/]+)$', '$2'), //
				label: elm //
			});
		});
	}

	return values;
}



data.bind("parameters",{
	"algo" : {
		"algo" : algo,
		"formJuridique" : formJuridique,
		"optionCMACCI" : "NON",
		"codeCommune" : codeCommune,
		"typePersonne" : typePersonne,
		"secteur1" : secteur1
	},
	"attachment" : {
		"cerfa" : '/3-review/generated/attachments.cerfa-1-P0_Agricole.pdf',
		"others" : findUserAttachments()
	}
});

data.bind("result",{
	"funcId" : ""
})
data.bind("result", {
	"denomination" : denomination
});