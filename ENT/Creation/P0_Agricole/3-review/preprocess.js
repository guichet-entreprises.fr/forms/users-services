function pad(s) { return (s < 10) ? '0' + s : s; }
var formFields = {};

// Cadres 1 - Activité non salariée

var identite = $p0agricole.cadre1IdentiteGroup.cadre1Identite;

formFields['formalite_evenement_oui']                                                   = identite.formaliteEvenementOui ? true : false;
formFields['formalite_evenement_non']                                                   = identite.formaliteEvenementOui ? false : true;
formFields['voletSocial_activiteExerceeAnterieurementSIREN']                            = identite.voletSocialActiviteExerceeAnterieurementSIREN != null ? identite.voletSocialActiviteExerceeAnterieurementSIREN.split(' ').join('') : '';

// Cadres 2

formFields['nonEIRL']                                                      = $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL ? false : true;
formFields['estEIRL']                                                      = $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL ? true : false;

// Cadres 3 - Identité

formFields['personneLiee_PP_nomNaissance']              				                = identite.personneLieePPNomNaissance;
formFields['personneLiee_PP_nomUsage']     			                                    = identite.personneLieePPNomUsage != null ? identite.personneLieePPNomUsage :'';

var prenoms=[];
for ( i = 0; i < identite.personneLieePPPrenom.size() ; i++ ){prenoms.push(identite.personneLieePPPrenom[i]);}                            
formFields['personneLiee_PP_prenom1']                                                   = prenoms.toString();

formFields['personneLiee_PP_pseudonyme']                                                = identite.personneLieePPPseudonyme;
formFields['personneLiee_PP_nationalite']                                               = identite.personneLieePPNationalite;
formFields['personneLiee_PP_civiliteMasculin']                                          = Value('id').of(identite.civilite).eq('personneLieePPCiviliteMasculin') ? true : false;
formFields['personneLiee_PP_civiliteFeminin']                                           = Value('id').of(identite.civilite).eq('personneLieePPCiviliteFeminin') ? true : false;
if(identite.personneLieePPDateNaissance != null) {
	var dateTmp = new Date(parseInt(identite.personneLieePPDateNaissance.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['personneLiee_PP_dateNaissance'] = date;
}
formFields['personneLiee_PP_lieuNaissanceDepartement']                                  = identite.personneLieePPLieuNaissanceDepartement != null ? identite.personneLieePPLieuNaissanceDepartement.getId() : '';
formFields['personneLiee_PP_lieuNaissanceCommune']                                      = (identite.personneLieePPLieuNaissanceCommune != null ?  identite.personneLieePPLieuNaissanceCommune : identite.personneLieePPLieuNaissanceVille);
formFields['personneLiee_PP_lieuNaissancePays']                                         = identite.personneLieePPLieuNaissancePays;

var adresse = $p0agricole.cadre1IdentiteGroup.cadre1AdresseDeclarant;

formFields['personneLiee_adresse_nomVoie']                                              = (adresse.personneLieeAdresseNumeroVoie != null ? adresse.personneLieeAdresseNumeroVoie : '') 
																						+ ' ' + (adresse.personneLieeAdresseIndiceVoie != null ? adresse.personneLieeAdresseIndiceVoie : '') 
																						+ ' ' + (adresse.personneLieeAdresseTypeVoie != null ? adresse.personneLieeAdresseTypeVoie : '') 
																						+ ' ' + adresse.personneLieeAdresseNomVoie;
formFields['personneLiee_adresse_complementVoie']                                       = (adresse.personneLieeAdresseComplementVoie != null ? adresse.personneLieeAdresseComplementVoie : '') 
																						+ ' ' + (adresse.personneLieeAdresseDistriutionSpecialeVoie != null ? adresse.personneLieeAdresseDistriutionSpecialeVoie : '');
formFields['personneLiee_adresse_codePostal']                                           = adresse.personneLieeAdresseCodePostal;
formFields['personneLiee_adresse_commune']                                              = adresse.personneLieeAdresseCommune;
formFields['personneLiee_adresse_ancienne_commune']                                     = adresse.personneLieeAdresseCommuneAncienne;

// Cadre 4A - Lieu de l'exploitation

var adressePro = $p0agricole.cadre4AdresseActiviteGroup.cadre4AdresseActivite;

formFields['etablissement_adresse_nomVoie']                                             = (adressePro.cadre4AdresseProfessionnelle.etablissementAdresseNumeroVoie != null ? adressePro.cadre4AdresseProfessionnelle.etablissementAdresseNumeroVoie : '')
																				            + ' ' + (adressePro.cadre4AdresseProfessionnelle.etablissementAdresseIndiceVoie != null ? adressePro.cadre4AdresseProfessionnelle.etablissementAdresseIndiceVoie : '')
																							+ ' ' + (adressePro.cadre4AdresseProfessionnelle.etablissementAdresseTypeVoie != null ? adressePro.cadre4AdresseProfessionnelle.etablissementAdresseTypeVoie : '')
																				            + ' ' + (adressePro.cadre4AdresseProfessionnelle.etablissementAdresseNomVoie != null ? adressePro.cadre4AdresseProfessionnelle.etablissementAdresseNomVoie : '')
																							+ ' ' + (adressePro.cadre4AdresseProfessionnelle.etablissementAdresseComplementVoie != null ? adressePro.cadre4AdresseProfessionnelle.etablissementAdresseComplementVoie : '')
																				            + ' ' + (adressePro.cadre4AdresseProfessionnelle.etablissementAdresseDistriutionSpecialeVoie != null ? adressePro.cadre4AdresseProfessionnelle.etablissementAdresseDistriutionSpecialeVoie : '');
formFields['etablissement_adresse_codePostal']                                          = adressePro.cadre4AdresseProfessionnelle.etablissementAdresseCodePostal;
formFields['etablissement_adresse_commune']                                             = adressePro.cadre4AdresseProfessionnelle.etablissementAdresseCommune;
formFields['etablissement_adresse_ancienne_commune']                                    = adressePro.cadre4AdresseProfessionnelle.etablissementAdresseCommuneAncienne;

// Cadre 4B - Nom de l'exploitation

formFields['etablissement_enseigne']                                                        = $p0agricole.cadre4AdresseActiviteGroup.cadre4AdresseActivite.etablissementEnseigne;

// Cadre 5 - Activité

var activite = $p0agricole.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite;

if(activite.etablissementDateDebutActivite != null) {
	var dateTmp = new Date(parseInt(activite.etablissementDateDebutActivite.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_dateDebutActivite'] = date;
}	

formFields['etablissement_activitePlusImportanteAgricole_cereales']                         = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricoleCereales') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_riz']                              = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricoleRiz') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_legumes']                          = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricoleLegumes') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_canneSucre']                       = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricoleCanneSucre') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_tabac']                            = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricoleTabac') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_plantesFibres']                    = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricolePlantesFibres') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_autresCulturesNonPermanentes']     = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricoleAutresCulturesNonPermanentes') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_vigne']                            = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricoleVigne') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_fruitsTropicaux']                  = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricoleFruitsTropicaux') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_agrumes']                          = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricoleAgrumes') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_fruitsPepins']                     = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricoleFruitsPepins') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_fruitsOleagineux']                 = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricoleFruitsOleagineux') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_autresFruitsArbres']               = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricoleAutresFruitsArbres') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_plantesBoisson']                   = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricolePlantesBoisson') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_plantesEpicesAromatiques']         = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricolePlantesEpicesAromatiques') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_autresCulturesPermanentes']        = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricoleAutresCulturesPermanentes') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_autresCulturesPermanentes_Autres'] = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricoleAutresCulturesPermanentes') ? activite.etablissementActivitePlusImportanteAgricoleAutresCulturesPermanentesAutres : '';

formFields['etablissement_activitePlusImportanteAgricole_vachesLaitieres']                  = Value('id').of(activite.etablissementActivitesPrincipales3).eq('etablissementActivitePlusImportanteAgricoleVachesLaitieres') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_autresBovins']                     = Value('id').of(activite.etablissementActivitesPrincipales3).eq('etablissementActivitePlusImportanteAgricoleAutresBovins') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_chevaux']                          = Value('id').of(activite.etablissementActivitesPrincipales3).eq('etablissementActivitePlusImportanteAgricoleChevaux') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_chameaux']                         = Value('id').of(activite.etablissementActivitesPrincipales3).eq('etablissementActivitePlusImportanteAgricoleChameaux') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_ovinsCaprins']                     = Value('id').of(activite.etablissementActivitesPrincipales3).eq('etablissementActivitePlusImportanteAgricoleOvinsCaprins') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_porcins']                          = Value('id').of(activite.etablissementActivitesPrincipales3).eq('etablissementActivitePlusImportanteAgricolePorcins') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_volailles']                        = Value('id').of(activite.etablissementActivitesPrincipales3).eq('etablissementActivitePlusImportanteAgricoleVolailles') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_aquacultureMer']                   = Value('id').of(activite.etablissementActivitesPrincipales3).eq('etablissementActivitePlusImportanteAgricoleAquacultureMer') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_aquacultureEauDouce']              = Value('id').of(activite.etablissementActivitesPrincipales3).eq('etablissementActivitePlusImportanteAgricoleAquacultureEauDouce') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_autresAnimaux']                    = Value('id').of(activite.etablissementActivitesPrincipales3).eq('etablissementActivitePlusImportanteAgricoleAutresAnimaux') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_autresAnimaux_Autres']             = Value('id').of(activite.etablissementActivitesPrincipales3).eq('etablissementActivitePlusImportanteAgricoleAutresAnimaux') ? activite.etablissementActivitePlusImportanteAgricoleAutresAnimauxAutres : '';

formFields['etablissement_activitePlusImportanteAgricole_cultureElevageAssocies']           = Value('id').of(activite.etablissementActivitesPrincipales4).eq('etablissementActivitePlusImportanteAgricoleCultureElevageAssocies') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_activitesPepinieres']              = Value('id').of(activite.etablissementActivitesPrincipales4).eq('etablissementActivitePlusImportanteAgricoleActivitesPepinieres') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_sylviculture']                     = Value('id').of(activite.etablissementActivitesPrincipales4).eq('etablissementActivitePlusImportanteAgricoleSylviculture') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_bailleurBiensRuraux']              = Value('id').of(activite.etablissementActivitesPrincipales4).eq('etablissementActivitePlusImportanteAgricoleBailleurBiensRuraux') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_loueurCheptel']                    = Value('id').of(activite.etablissementActivitesPrincipales4).eq('etablissementActivitePlusImportanteAgricoleLoueurCheptel') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_autre']                            = Value('id').of(activite.etablissementActivitesPrincipales4).eq('etablissementActivitePlusImportanteAgricoleAutre') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_AutresPreciser']                   = Value('id').of(activite.etablissementActivitesPrincipales4).eq('etablissementActivitePlusImportanteAgricoleAutre') ? activite.etablissementActivitePlusImportanteAgricoleAutresPreciser : '';
formFields['activiteSecondaire']                                                            = activite.activiteSecondaire != null ? activite.activiteSecondaire : '';

// Cadre 6 - Origine Fonds

var origine = $p0agricole.cadre5EtablissementActiviteGroup.cadre4OrigineFonds;

formFields['etablissement_origineFonds_creation']                                       = Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation') ? true : false;
formFields['etablissement_origineFonds_repriseTotalePartielle']                         = Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsRepriseTotalePartielle') ? true : false;
formFields['etablissement_origineFonds_poursuiteConjoint']                              = Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsPoursuiteConjoint') ? true : false;
formFields['etablissement_origineFonds_autre']                                          = Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsAutre') ? true : false;
formFields['etablissement_origineFondsLibelle']                                         = origine.etablissementOrigineFondsLibelle;
formFields['entrepriseLiee_siren_precedentExploitant1']                                 = origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant.split(' ').join('') : '';
formFields['entrepriseLiee_numeroDetenteur_precedentExploitant1']                       = origine.precedentExploitant.entrepriseLieeNumeroDetenteurPrecedentExploitant;
formFields['entrepriseLiee_numeroExploitation_precedentExploitant1']                    = origine.precedentExploitant.entrepriseLieeNumeroExploitationPrecedentExploitant;
formFields['entrepriseLiee_entreprisePP_nomNaissance_precedentExploitant1']             = origine.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant;
formFields['entrepriseLiee_entreprisePP_nomUsage_precedentExploitant1']                 = origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant;
formFields['entrepriseLiee_entreprisePP_prenom1_precedentExploitant1']                  = origine.precedentExploitant.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant;
formFields['entrepriseLiee_entreprisePM_denomination_precedentExploitant1']             = origine.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant;

formFields['entrepriseLiee_siren_precedentExploitant2']                                 = origine.precedentExploitant2.entrepriseLieeSirenPrecedentExploitant2 != null ? origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant.split(' ').join('') : '';
formFields['entrepriseLiee_numeroDetenteur_precedentExploitant2']                       = origine.precedentExploitant2.entrepriseLieeNumeroDetenteurPrecedentExploitant2;
formFields['entrepriseLiee_numeroExploitation_precedentExploitant2']                    = origine.precedentExploitant2.entrepriseLieeNumeroExploitationPrecedentExploitant2;
formFields['entrepriseLiee_entreprisePP_nomNaissance_precedentExploitant2']             = origine.precedentExploitant2.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant2;
formFields['entrepriseLiee_entreprisePP_nomUsage_precedentExploitant2']                 = origine.precedentExploitant2.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant2;
formFields['entrepriseLiee_entreprisePP_prenom1_precedentExploitant2']                  = origine.precedentExploitant2.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant2;
formFields['entrepriseLiee_entreprisePM_denomination_precedentExploitant2']             = origine.precedentExploitant2.entrepriseLieeEntreprisePMDenominationPrecedentExploitant2;

// Cadre 7 - Effectif salarié

formFields['etablissement_effectifSalariePresence_non']                                 = activite.etablissementEffectifSalariePresenceOui ? false : true;
formFields['etablissement_effectifSalariePresence_oui']                                 = activite.etablissementEffectifSalariePresenceOui ? true : false;
formFields['etablissement_effectifSalarieNombre']                                       = activite.etablissementEffectifSalarieNombre;
formFields['etablissement_effectifSalarieEmbauchePremierSalarie_oui']                   = activite.etablissementEffectifSalarieEmbauchePremierSalarieOui ? true : false;
formFields['etablissement_effectifSalarieEmbauchePremierSalarie_non']                   = activite.etablissementEffectifSalarieEmbauchePremierSalarieOui ? false : (activite.etablissementEffectifSalariePresenceOui ? false : true);

// Cadre 8 - Mise en location de bien ruraux

var locationBien = $p0agricole.miseLocationBienRurauxGroup.miseLocationBienRurauxInformations;

formFields['miseEnLocation_totale']                                                     = Value('id').of(locationBien.objetMiseLocation).eq('miseEnLocationTotale');
formFields['miseEnLocation_partiel']                                                    = Value('id').of(locationBien.objetMiseLocation).eq('miseEnLocationPartiel');
formFields['miseEnLocation_locationDroits']                                             = Value('id').of(locationBien.objetMiseLocation).eq('miseEnLocationLocationDroits');
formFields['miseEnLocation_adresseVoie']                                                = (locationBien.cadreAdresseBienLoue.miseEnLocationAdresseNumeroVoie != null ? locationBien.cadreAdresseBienLoue.miseEnLocationAdresseNumeroVoie : '')  
																						+ ' ' + (locationBien.cadreAdresseBienLoue.miseEnLocationAdresseIndiceVoie != null ? locationBien.cadreAdresseBienLoue.miseEnLocationAdresseIndiceVoie : '')
																						+ ' ' + (locationBien.cadreAdresseBienLoue.miseEnLocationAdresseTypeVoie != null ? locationBien.cadreAdresseBienLoue.miseEnLocationAdresseTypeVoie : '')
																						+ ' ' + (locationBien.cadreAdresseBienLoue.miseEnLocationAdresseNomVoie != null ? locationBien.cadreAdresseBienLoue.miseEnLocationAdresseNomVoie : '')
																						+ ' ' + (locationBien.cadreAdresseBienLoue.miseEnLocationAdresseComplementVoie != null ? locationBien.cadreAdresseBienLoue.miseEnLocationAdresseComplementVoie : '')
																						+ ' ' + (locationBien.cadreAdresseBienLoue.miseEnLocationAdresseDistriutionSpeciale != null ? locationBien.cadreAdresseBienLoue.miseEnLocationAdresseDistriutionSpeciale : '');
formFields['miseEnLocation_adresseCodePostal']                                          = locationBien.cadreAdresseBienLoue.miseEnLocationAdresseCodePostal;
formFields['miseEnLocation_adresseCommune']                                             = locationBien.cadreAdresseBienLoue.miseEnLocationAdresseCommune;
formFields['miseEnLocation_nomPreneur']                                                 = locationBien.miseEnLocationDenominationPreneur != null ? locationBien.miseEnLocationDenominationPreneur : (locationBien.miseEnLocationNomUsagePreneur != null ? (locationBien.miseEnLocationNomUsagePreneur + ' ' + locationBien.miseEnLocationPrenomPreneur) : (locationBien.miseEnLocationNomNaissancePreneur != null ? locationBien.miseEnLocationNomNaissancePreneur + ' ' + locationBien.miseEnLocationPrenomPreneur : ''));
formFields['miseEnLocation_sirenPreneur']                                               = locationBien.miseEnLocationSirenPreneur != null ? locationBien.miseEnLocationSirenPreneur.split(' ').join('') : '';

// Cadre 9 - Volet Social

var social = $p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale;

var nirDeclarant = social.voletSocialNumeroSecuriteSociale;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFields['voletSocial_numeroSecuriteSociale']                = nirDeclarant.substring(0, 13);
    formFields['voletSocial_numeroSecuriteSocialeCle']             = nirDeclarant.substring(13, 15);
}
formFields['voletSocial_affiliationMSA_oui']                                            = social.voletSocialAffiliationMSAOui ? true : false;
formFields['voletSocial_affiliationMSA_non']                                            = social.voletSocialAffiliationMSAOui ? false : true;
formFields['voletSocial_titreSejour_numero']                                            = social.voletSocialTitreSejourNumero;
formFields['voletSocial_titreSejour_lieuDelivranceCommune']                             = social.voletSocialTitreSejourLieuDelivranceCommune;
if(social.voletSocialTitreSejourDateExpiration != null) {
	var dateTmp = new Date(parseInt(social.voletSocialTitreSejourDateExpiration.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['voletSocial_titreSejour_dateExpiration'] = date;
}
formFields['voletSocial_activiteAutreQueDeclareeStatut_oui']                            = social.voletSocialActiviteAutreQueDeclareeStatutOui ? true : false;
formFields['voletSocial_activiteAutreQueDeclareeStatut_non']                            = social.voletSocialActiviteAutreQueDeclareeStatutOui ? false : true;
formFields['voletSocial_activiteAutreQueDeclareeStatut_salarie']                        = Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutSalarie') ? true : false;
formFields['voletSocial_activiteAutreQueDeclareeStatut_agricole']                       = Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutAgricole') ? true : false;
formFields['voletSocial_activiteAutreQueDeclareeStatut_nonSalarieNonAgricole']          = Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutNonSalarieNonAgricole') ? true : false;
formFields['voletSocial_activiteAutreQueDeclareeStatut_retraite']                       = Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutRetraite') ? true : false;
formFields['voletSocial_activiteAutreQueDeclareeStatut_pensionneInvalidite']            = Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutPensionneInvalidite') ? true : false;
formFields['voletSocial_activiteAutreQueDeclareeStatut_autre'] 				        	= Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutAutre') ? true : false;
formFields['voletSocial_activiteAutreQueDeclareeStatut_autreTexte']                     = social.voletSocialActiviteAutreQueDeclareeStatutAutreTexte;
formFields['voletSocial_organismeServentPension']                                       = social.voletSocialOrganismeServentPension;
formFields['voletSocial_estJeuneAgriculteur_oui']                                       = Value('id').of(social.voletSocialEstJeuneAgriculteur).eq('VoletSocialEstJeuneAgriculteurOui') ? true : false;
formFields['voletSocial_estJeuneAgriculteur_non']                                       = Value('id').of(social.voletSocialEstJeuneAgriculteur).eq('VoletSocialEstJeuneAgriculteurNon') ? true : false;
formFields['voletSocial_demandeDotationJeuneAgriculteur']                               = Value('id').of(social.voletSocialEstJeuneAgriculteur).eq('VoletSocialDemandeDotationJeuneAgriculteur') ? true : false;
formFields['personneLiee_conjointActiviteEntreprise_oui']                               = social.conjointRole ? true : false;
formFields['personneLiee_conjointActiviteEntreprise_non']                               = social.conjointRole ? false : true;

if (social.conjointRole) {
formFields['voletSocial_statutConjoint_salarie']                                        = Value('id').of(social.statutConjointCollaborateur).eq('VoletSocialStatutConjointSalarie') ? true : false;
formFields['voletSocial_statutConjoint_collaborateur']                                  = Value('id').of(social.statutConjointCollaborateur).eq('VoletSocialStatutConjointCollaborateur') ? true : false;
formFields['voletSocial_statutConjoint_coExploitant']                                   = Value('id').of(social.statutConjointCollaborateur).eq('VoletSocialStatutConjointCoExploitant') ? true : false;
formFields['personneLiee_personnePhysique_nomNaissance_conjointPacse']                  = social.personneLieePersonnePhysiqueNomNaissanceConjointPacse;
var prenomsConjoint=[];
for ( i = 0; i < social.personneLieePersonnePhysiquePrenom1ConjointPacse.size() ; i++ ){prenomsConjoint.push(social.personneLieePersonnePhysiquePrenom1ConjointPacse[i]);}                            
formFields['personneLiee_personnePhysique_prenom1_conjointPacse']                       = prenomsConjoint.toString();
if (social.adresseConjointDifferente)  {
formFields['adresseConjointVoie']                                                       = (social.adresseDomicileConjoint.adresseConjointNumeroVoie != null ? social.adresseDomicileConjoint.adresseConjointNumeroVoie : '')
																						+ ' ' + (social.adresseDomicileConjoint.adresseConjointIndiceVoie != null ? social.adresseDomicileConjoint.adresseConjointIndiceVoie : '')
																						+ ' ' + (social.adresseDomicileConjoint.adresseConjointTypeVoie != null ? social.adresseDomicileConjoint.adresseConjointTypeVoie : '')
																						+ ' ' + social.adresseDomicileConjoint.adresseConjointNomVoie
																						+ ' ' + (social.adresseDomicileConjoint.adresseConjointComplementVoie != null ? social.adresseDomicileConjoint.adresseConjointComplementVoie : '')
																						+ ' ' + (social.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null ? social.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie : '');
formFields['adresseConjointCodePostal']                                                 = social.adresseDomicileConjoint.adresseConjointCodePostal;
formFields['adresseConjointCommune']                                                    = social.adresseDomicileConjoint.adresseConjointCommune;
}
formFields['voletSocial_conjointCouvertAssuranceMaladie_oui']                           = social.voletSocialConjointCouvertAssuranceMaladieOui ? true : false;
formFields['voletSocial_conjointCouvertAssuranceMaladie_non']                           = social.conjointRole ? (social.voletSocialConjointCouvertAssuranceMaladieOui ? false : true) : false;
var nirDeclarant = social.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFields['voletSocial_conjointCollaborateurNumeroSecuriteSociale']                = nirDeclarant.substring(0, 13);
    formFields['voletSocial_conjointCollaborateurNumeroSecuriteSocialeCle']             = nirDeclarant.substring(13, 15);
}
}

// Ayant droit 1

var socialAyantDroit1 = $p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.ayantsdroits1;

if (social.ayantDroitsDeclaration) {
formFields['ayantDroit_identite_nomNaissance1']                                         = socialAyantDroit1.ayantDroitIdentiteNomNaissance + ' ' + socialAyantDroit1.ayantDroitIdentitePrenomNaissance;
formFields['ayantDroit_numeroSecuriteSociale1']                                         = socialAyantDroit1.presenceNumSSAM ? socialAyantDroit1.ayantDroitNumeroSecuriteSociale : '';
formFields['ayantDroit_identiteComplement_dateNaissance1']                              = socialAyantDroit1.presenceNumSSAM ? '' : socialAyantDroit1.ayantDroitDateNaissance;
formFields['ayantDroit_identiteComplement_lieuNaissanceCommune1']                       = socialAyantDroit1.presenceNumSSAM ? '' : ((socialAyantDroit1.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? socialAyantDroit1.ayantDroitIdentiteComplementLieuNaissanceCommune : socialAyantDroit1.ayantDroitIdentiteComplementLieuNaissanceVille) + ' / ' + socialAyantDroit1.ayantDroitIdentiteComplementLieuNaissancePays + ' / ' + (Value('id').of(socialAyantDroit1.ayantDroitSexe).eq('feminin') ? "F" : "M"));
formFields['ayantDroit_lienParente1']                                                   = socialAyantDroit1.ayantDroitLienParente;
formFields['ayantDroit_enfantScolarise_oui1']                                           = socialAyantDroit1.dataEnfantScolariseBeneficiaireAM ? true : false;
formFields['ayantDroit_enfantScolarise_non1']                                           = socialAyantDroit1.dataEnfantScolariseBeneficiaireAM ? false : true;
formFields['ayantDroit_nationalite1']                                                   = socialAyantDroit1.ayantDroitNationalite;
}

// Ayant droit 2

var socialAyantDroit2 = $p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.ayantsdroits2;

if (socialAyantDroit1.autreBeneficiaireAM ) {
formFields['ayantDroit_identite_nomNaissance2']                                         = socialAyantDroit2.ayantDroitIdentiteNomNaissance + ' ' + socialAyantDroit2.ayantDroitIdentitePrenomNaissance;
formFields['ayantDroit_numeroSecuriteSociale2']                                         = socialAyantDroit2.presenceNumSSAM ? socialAyantDroit2.ayantDroitNumeroSecuriteSociale : '';
formFields['ayantDroit_identiteComplement_dateNaissance2']                              = socialAyantDroit2.presenceNumSSAM ? '' : socialAyantDroit2.ayantDroitDateNaissance;
formFields['ayantDroit_identiteComplement_lieuNaissanceCommune2']                       = socialAyantDroit2.presenceNumSSAM ? '' : ((socialAyantDroit2.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? socialAyantDroit2.ayantDroitIdentiteComplementLieuNaissanceCommune : socialAyantDroit2.ayantDroitIdentiteComplementLieuNaissanceVille) + ' / ' + socialAyantDroit2.ayantDroitIdentiteComplementLieuNaissancePays + ' / ' + (Value('id').of(socialAyantDroit2.ayantDroitSexe).eq('feminin') ? "F" : "M"));
formFields['ayantDroit_lienParente2']                                                   = socialAyantDroit2.ayantDroitLienParente;
formFields['ayantDroit_enfantScolarise_oui2']                                           = socialAyantDroit2.dataEnfantScolariseBeneficiaireAM ? true : false;
formFields['ayantDroit_enfantScolarise_non2']                                           = socialAyantDroit2.dataEnfantScolariseBeneficiaireAM ? false : true;
formFields['ayantDroit_nationalite2']                                                   = socialAyantDroit2.ayantDroitNationalite;
}

// Ayant droit 3
var socialAyantDroit3 = $p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.ayantsdroits3;

if (socialAyantDroit2.autreBeneficiaireAM) {
formFields['ayantDroit_identite_nomNaissance3']                                         = socialAyantDroit3.ayantDroitIdentiteNomNaissance + ' ' + socialAyantDroit3.ayantDroitIdentitePrenomNaissance;
formFields['ayantDroit_numeroSecuriteSociale3']                                         = socialAyantDroit3.presenceNumSSAM ? socialAyantDroit3.ayantDroitNumeroSecuriteSociale : '';
formFields['ayantDroit_identiteComplement_dateNaissance3']                              = socialAyantDroit3.presenceNumSSAM ? '' : socialAyantDroit3.ayantDroitDateNaissance;
formFields['ayantDroit_identiteComplement_lieuNaissanceCommune3']                       = socialAyantDroit3.presenceNumSSAM ? '' : ((socialAyantDroit3.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? socialAyantDroit3.ayantDroitIdentiteComplementLieuNaissanceCommune : socialAyantDroit3.ayantDroitIdentiteComplementLieuNaissanceVille) + ' / ' + socialAyantDroit3.ayantDroitIdentiteComplementLieuNaissancePays + ' / ' + (Value('id').of(socialAyantDroit3.ayantDroitSexe).eq('feminin') ? "F" : "M"));
formFields['ayantDroit_lienParente3']                                                   = socialAyantDroit3.ayantDroitLienParente;
formFields['ayantDroit_enfantScolarise_oui3']                                           = socialAyantDroit3.dataEnfantScolariseBeneficiaireAM ? true : false;
formFields['ayantDroit_enfantScolarise_non3']                                           = socialAyantDroit3.dataEnfantScolariseBeneficiaireAM ? false : true;
formFields['ayantDroit_nationalite3']                                                   = socialAyantDroit3.ayantDroitNationalite;
}

// Ayant droit 4
var socialAyantDroit4 = $p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.ayantsdroits4;

if (socialAyantDroit3.autreBeneficiaireAM) {
formFields['ayantDroit_identite_nomNaissance4']                                         = socialAyantDroit4.ayantDroitIdentiteNomNaissance + ' ' + socialAyantDroit4.ayantDroitIdentitePrenomNaissance;
formFields['ayantDroit_numeroSecuriteSociale4']                                         = socialAyantDroit4.presenceNumSSAM ? socialAyantDroit4.ayantDroitNumeroSecuriteSociale : '';
formFields['ayantDroit_identiteComplement_dateNaissance4']                              = socialAyantDroit4.presenceNumSSAM ? '' : socialAyantDroit4.ayantDroitDateNaissance;
formFields['ayantDroit_identiteComplement_lieuNaissanceCommune4']                       = socialAyantDroit4.presenceNumSSAM ? '' : ((socialAyantDroit4.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? socialAyantDroit4.ayantDroitIdentiteComplementLieuNaissanceCommune : socialAyantDroit4.ayantDroitIdentiteComplementLieuNaissanceVille) + ' / ' + socialAyantDroit4.ayantDroitIdentiteComplementLieuNaissancePays + ' / ' + (Value('id').of(socialAyantDroit4.ayantDroitSexe).eq('feminin') ? "F" : "M"));
formFields['ayantDroit_lienParente4']                                                   = socialAyantDroit4.ayantDroitLienParente;
formFields['ayantDroit_enfantScolarise_oui4']                                           = socialAyantDroit4.dataEnfantScolariseBeneficiaireAM ? true : false;
formFields['ayantDroit_enfantScolarise_non4']                                           = socialAyantDroit4.dataEnfantScolariseBeneficiaireAM ? false : true;
formFields['ayantDroit_nationalite4']                                                   = socialAyantDroit4.ayantDroitNationalite;
}

//Premier ayant droit sur intercalaire P0 Prime

var socialAyantDroit5 = $p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.ayantsdroits5;

if (socialAyantDroit4.autreBeneficiaireAM) {
formFields['ayantDroit_identite_nomPrenomNaissance1P0']                     = socialAyantDroit5.ayantDroitIdentiteNomNaissance + ' ' + socialAyantDroit5.ayantDroitIdentitePrenomNaissance;
formFields['ayantDroit_numeroSS_dateNaiss1P0']                              = socialAyantDroit5.presenceNumSSAM ? socialAyantDroit5.ayantDroitNumeroSecuriteSociale : socialAyantDroit5.ayantDroitDateNaissance;
formFields['ayantDroit_lieuNaissanceCommune_sexe1P0']                       = socialAyantDroit5.presenceNumSSAM ? '' : ((socialAyantDroit5.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? socialAyantDroit5.ayantDroitIdentiteComplementLieuNaissanceCommune : socialAyantDroit5.ayantDroitIdentiteComplementLieuNaissanceVille) + ' / ' + socialAyantDroit5.ayantDroitIdentiteComplementLieuNaissancePays + ' / ' + (Value('id').of(socialAyantDroit5.ayantDroitSexe).eq('feminin') ? "F" : "M"));
formFields['ayantDroit_lienParente1P0']                                     = socialAyantDroit5.ayantDroitLienParente;
formFields['ayantDroit_enfantScolarise_oui1P0']                             = socialAyantDroit5.dataEnfantScolariseBeneficiaireAM ? true : false;
formFields['ayantDroit_enfantScolarise_non1P0']                             = socialAyantDroit5.dataEnfantScolariseBeneficiaireAM ? false : true;
formFields['ayantDroit_nationalite1P0']                                     = socialAyantDroit5.ayantDroitNationalite;
}

//Deuxième ayant droit sur intercalaire P0 Prime

var socialAyantDroit6 = $p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.ayantsdroits6;

if (socialAyantDroit5.autreBeneficiaireAM) {
formFields['ayantDroit_identite_nomPrenomNaissance2P0']                     = socialAyantDroit6.ayantDroitIdentiteNomNaissance + ' ' + socialAyantDroit6.ayantDroitIdentitePrenomNaissance;
formFields['ayantDroit_numeroSS_dateNaiss2P0']                              = socialAyantDroit6.presenceNumSSAM ? socialAyantDroit6.ayantDroitNumeroSecuriteSociale : socialAyantDroit6.ayantDroitDateNaissance;
formFields['ayantDroit_lieuNaissanceCommune_sexe2P0']                       = socialAyantDroit6.presenceNumSSAM ? '' : ((socialAyantDroit6.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? socialAyantDroit6.ayantDroitIdentiteComplementLieuNaissanceCommune : socialAyantDroit6.ayantDroitIdentiteComplementLieuNaissanceVille) + ' / ' + socialAyantDroit6.ayantDroitIdentiteComplementLieuNaissancePays + ' / ' + (Value('id').of(socialAyantDroit6.ayantDroitSexe).eq('feminin') ? "F" : "M"));
formFields['ayantDroit_lienParente2P0']                                     = socialAyantDroit6.ayantDroitLienParente;
formFields['ayantDroit_enfantScolarise_oui2P0']                             = socialAyantDroit6.dataEnfantScolariseBeneficiaireAM ? true : false;
formFields['ayantDroit_enfantScolarise_non2P0']                             = socialAyantDroit6.dataEnfantScolariseBeneficiaireAM ? false : true;
formFields['ayantDroit_nationalite2P0']                                     = socialAyantDroit6.ayantDroitNationalite;
}

//Troisième ayant droit sur intercalaire P0 Prime

var socialAyantDroit7 = $p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.ayantsdroits7;

if (socialAyantDroit6.autreBeneficiaireAM) {
formFields['ayantDroit_identite_nomPrenomNaissance3P0']                     = socialAyantDroit7.ayantDroitIdentiteNomNaissance + ' ' + socialAyantDroit7.ayantDroitIdentitePrenomNaissance;
formFields['ayantDroit_numeroSS_dateNaiss3P0']                              = socialAyantDroit7.presenceNumSSAM ? socialAyantDroit7.ayantDroitNumeroSecuriteSociale : socialAyantDroit7.ayantDroitDateNaissance;
formFields['ayantDroit_lieuNaissanceCommune_sexe3P0']                       = socialAyantDroit7.presenceNumSSAM ? '' : ((socialAyantDroit7.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? socialAyantDroit7.ayantDroitIdentiteComplementLieuNaissanceCommune : socialAyantDroit7.ayantDroitIdentiteComplementLieuNaissanceVille) + ' / ' + socialAyantDroit7.ayantDroitIdentiteComplementLieuNaissancePays + ' / ' + (Value('id').of(socialAyantDroit7.ayantDroitSexe).eq('feminin') ? "F" : "M"));
formFields['ayantDroit_lienParente3P0']                                     = socialAyantDroit7.ayantDroitLienParente;
formFields['ayantDroit_enfantScolarise_oui3P0']                             = socialAyantDroit7.dataEnfantScolariseBeneficiaireAM ? true : false;
formFields['ayantDroit_enfantScolarise_non3P0']                             = socialAyantDroit7.dataEnfantScolariseBeneficiaireAM ? false : true;
formFields['ayantDroit_nationalite3P0']                                     = socialAyantDroit7.ayantDroitNationalite;
}

//Quatrième ayant droit sur intercalaire P0 Prime

var socialAyantDroit8 = $p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.ayantsdroits8;

if (socialAyantDroit7.autreBeneficiaireAM) {
formFields['ayantDroit_identite_nomPrenomNaissance4P0']                     = socialAyantDroit8.ayantDroitIdentiteNomNaissance + ' ' + socialAyantDroit8.ayantDroitIdentitePrenomNaissance;
formFields['ayantDroit_numeroSS_dateNaiss4P0']                              = socialAyantDroit8.presenceNumSSAM ? socialAyantDroit8.ayantDroitNumeroSecuriteSociale : socialAyantDroit8.ayantDroitDateNaissance;
formFields['ayantDroit_lieuNaissanceCommune_sexe4P0']                       = socialAyantDroit8.presenceNumSSAM ? '' : ((socialAyantDroit8.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? socialAyantDroit8.ayantDroitIdentiteComplementLieuNaissanceCommune : socialAyantDroit8.ayantDroitIdentiteComplementLieuNaissanceVille) + ' / ' + socialAyantDroit8.ayantDroitIdentiteComplementLieuNaissancePays + ' / ' + (Value('id').of(socialAyantDroit8.ayantDroitSexe).eq('feminin') ? "F" : "M"));
formFields['ayantDroit_lienParente4P0']                                     = socialAyantDroit8.ayantDroitLienParente;
formFields['ayantDroit_enfantScolarise_oui4P0']                             = socialAyantDroit8.dataEnfantScolariseBeneficiaireAM ? true : false;
formFields['ayantDroit_enfantScolarise_non4P0']                             = socialAyantDroit8.dataEnfantScolariseBeneficiaireAM ? false : true;
formFields['ayantDroit_nationalite4P0']                                     = socialAyantDroit8.ayantDroitNationalite;
}

//Cinquième ayant droit sur intercalaire P0 Prime

var socialAyantDroit9 = $p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.ayantsdroits9;

if (socialAyantDroit8.autreBeneficiaireAM)  {
formFields['ayantDroit_identite_nomPrenomNaissance5P0']                     = socialAyantDroit9.ayantDroitIdentiteNomNaissance + ' ' + socialAyantDroit9.ayantDroitIdentitePrenomNaissance;
formFields['ayantDroit_numeroSS_dateNaiss5P0']                              = socialAyantDroit9.presenceNumSSAM ? socialAyantDroit9.ayantDroitNumeroSecuriteSociale : socialAyantDroit9.ayantDroitDateNaissance;
formFields['ayantDroit_lieuNaissanceCommune_sexe5P0']                       = socialAyantDroit9.presenceNumSSAM ? '' : ((socialAyantDroit9.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? socialAyantDroit9.ayantDroitIdentiteComplementLieuNaissanceCommune : socialAyantDroit9.ayantDroitIdentiteComplementLieuNaissanceVille) + ' / ' + socialAyantDroit9.ayantDroitIdentiteComplementLieuNaissancePays + ' / ' + (Value('id').of(socialAyantDroit9.ayantDroitSexe).eq('feminin') ? "F" : "M"));
formFields['ayantDroit_lienParente5P0']                                     = socialAyantDroit9.ayantDroitLienParente;
formFields['ayantDroit_enfantScolarise_oui5P0']                             = socialAyantDroit9.dataEnfantScolariseBeneficiaireAM ? true : false;
formFields['ayantDroit_enfantScolarise_non5P0']                             = socialAyantDroit9.dataEnfantScolariseBeneficiaireAM ? false : true;
formFields['ayantDroit_nationalite5P0']                                     = socialAyantDroit9.ayantDroitNationalite;
}

//Sixième ayant droit sur intercalaire P0 Prime

var socialAyantDroit10 = $p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.ayantsdroits10;

if (socialAyantDroit9.autreBeneficiaireAM) {
formFields['ayantDroit_identite_nomPrenomNaissance6P0']                     = socialAyantDroit10.ayantDroitIdentiteNomNaissance + ' ' + socialAyantDroit10.ayantDroitIdentitePrenomNaissance;
formFields['ayantDroit_numeroSS_dateNaiss6P0']                              = socialAyantDroit10.presenceNumSSAM ? socialAyantDroit10.ayantDroitNumeroSecuriteSociale : socialAyantDroit10.ayantDroitDateNaissance;
formFields['ayantDroit_lieuNaissanceCommune_sexe6P0']                       = socialAyantDroit10.presenceNumSSAM ? '' : ((socialAyantDroit10.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? socialAyantDroit10.ayantDroitIdentiteComplementLieuNaissanceCommune : socialAyantDroit10.ayantDroitIdentiteComplementLieuNaissanceVille) + ' / ' + socialAyantDroit10.ayantDroitIdentiteComplementLieuNaissancePays + ' / ' + (Value('id').of(socialAyantDroit10.ayantDroitSexe).eq('feminin') ? "F" : "M"));
formFields['ayantDroit_lienParente6P0']                                     = socialAyantDroit10.ayantDroitLienParente;
formFields['ayantDroit_enfantScolarise_oui6P0']                             = socialAyantDroit10.dataEnfantScolariseBeneficiaireAM ? true : false;
formFields['ayantDroit_enfantScolarise_non6P0']                             = socialAyantDroit10.dataEnfantScolariseBeneficiaireAM ? false : true;
formFields['ayantDroit_nationalite6P0']                                     = socialAyantDroit10.ayantDroitNationalite;
}

//Cadre 10 - Options fiscales

var fiscal = $p0agricole.cadre9optFiscHorsEIRLGroup.cadre9optFiscHorsEIRL;
var declarationAffectation = $p0agricole.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine;
var impotEIRL = $p0agricole.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine.cadre3OptionsFiscalesEIRL;

formFields['regimeFiscal_regimeImpositionBenefices_mBA']                                    = (declarationAffectation.declarationPatrimoine.objetPartiel or not $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL) ? (Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesMBA')? true : false) : (Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesMBA') ? true : false);
formFields['regimeFiscal_regimeImpositionBenefices_rsBA']                                   = (declarationAffectation.declarationPatrimoine.objetPartiel or not $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL) ? (Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesRsBA')? true : false) : (Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesRsBA') ? true : false);
formFields['regimeFiscal_regimeImpositionBenefices_rnBA']                                   = (declarationAffectation.declarationPatrimoine.objetPartiel or not $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL) ? (Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesRnBA')? true : false) : (Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesRnBA') ? true : false);
formFields['regimeFiscal_regimeImpositionBenefices_rfBA']                                   = (declarationAffectation.declarationPatrimoine.objetPartiel or not $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL) ? (Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesRfBA')? true : false) : (Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesRfBA') ? true : false);
formFields['regimeFiscal_regimeImpositionBenefices_ffBA']                                   = (declarationAffectation.declarationPatrimoine.objetPartiel or not $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL) ? (Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesffBA')? true : false) : (Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesffBA') ? true : false);
formFields['regimeFiscal_regimeImpositionTVA_remboursementForfaitaire']                     = (declarationAffectation.declarationPatrimoine.objetPartiel or not $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL) ? (Value('id').of(fiscal.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARemboursementForfaitaire')? true : false) : (Value('id').of(impotEIRL.optionsTVA.regimeTVA1).eq('eirlRegimeFiscalRegimeImpositionTVARemboursementForfaitaire') ? true : false);
formFields['regimeFiscal_regimeImpositionTVA_impositionObligatoire']                        = (declarationAffectation.declarationPatrimoine.objetPartiel or not $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL) ? ((Value('id').of(fiscal.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVAImpositionObligatoire') or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesRsBA') or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesRnBA')) ? true : false) : ((Value('id').of(impotEIRL.optionsTVA.regimeTVA1).eq('eirlRegimeFiscalRegimeImpositionTVAImpositionObligatoire') or Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesRsBA') or Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesRnBA')) ? true : false);
formFields['regimeFiscal_regimeImpositionTVAOptionsParticulieres4']                         = (declarationAffectation.declarationPatrimoine.objetPartiel or not $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL) ? (Value('id').of(fiscal.optionsTVA.optionTVAvolontaire).eq('RegimeFiscalRegimeImpositionTVAOptionsParticulieres4')? true : false) : (Value('id').of(impotEIRL.optionsTVA.optionTVAvolontaire).eq('eirlRegimeFiscalRegimeImpositionTVAOptionsParticulieres4') ? true : false);
formFields['regimeFiscal_regimeImpositionTVAOptionsBailleursRuraux']                        = (declarationAffectation.declarationPatrimoine.objetPartiel or not $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL) ? (Value('id').of(fiscal.optionsTVA.optionTVAvolontaire).eq('RegimeFiscalRegimeImpositionTVAOptionsBailleursRuraux')? true : false) : (Value('id').of(impotEIRL.optionsTVA.optionTVAvolontaire).eq('eirlRegimeFiscalRegimeImpositionTVAOptionsBailleurRuraux') ? true : false);
// Option dépôt de déclarations
formFields['regimeFiscal_regimeImpositionTVAOptionsParticulieres5']                         = (declarationAffectation.declarationPatrimoine.objetPartiel or not $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL) ? (Value('id').of(fiscal.optionsTVA.optionTVADepot).eq('RegimeFiscalRegimeImpositionTVADeclarationAnnuelle') ? true : false) :  (Value('id').of(impotEIRL.optionsTVA.optionTVADepot).eq('eirlRegimeFiscalRegimeImpositionTVAOptionsParticulieres3') ? true : false);
formFields['regimeFiscal_regimeImpositionTVAOptionsDepotDeclarationTrimestrielle']          = (declarationAffectation.declarationPatrimoine.objetPartiel or not $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL) ? (Value('id').of(fiscal.optionsTVA.optionTVADepot).eq('RegimeFiscalRegimeImpositionTVADeclarationTrimestrielle')? true : false) : (Value('id').of(impotEIRL.optionsTVA.optionTVADepot).eq('eirlRegimeFiscalRegimeImpositionTVADepotTrimestriel') ? true : false);
formFields['regimeFiscal_regimeImpositionTVAOptionsDepotDeclarationMensuelle']              = (declarationAffectation.declarationPatrimoine.objetPartiel or not $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL) ? (Value('id').of(fiscal.optionsTVA.optionTVADepot).eq('RegimeFiscalRegimeImpositionTVADeclarationMensuelle')? true : false) : (Value('id').of(impotEIRL.optionsTVA.optionTVADepot).eq('eirlRegimeFiscalRegimeImpositionTVADepotMensuel') ? true : false);
if((declarationAffectation.declarationPatrimoine.objetPartiel or not $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL) and fiscal.optionsTVA.regimeFiscalDateClotureExerciceComptable !== null) {
	var dateTmp = new Date(parseInt(fiscal.optionsTVA.regimeFiscalDateClotureExerciceComptable.getTimeInMillis()));
	var date1 = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
	formFields['regimeFiscal_dateClotureExerciceComptable'] = date1;
} else if (($p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL and not declarationAffectation.declarationPatrimoine.objetPartiel) and impotEIRL.optionsTVA.eirlRegimeFiscalDateClotureExerciceComptable !== null) {
	var dateTmp = new Date(parseInt(impotEIRL.optionsTVA.eirlRegimeFiscalDateClotureExerciceComptable.getTimeInMillis()));
	var date2 = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
	formFields['regimeFiscal_dateClotureExerciceComptable'] = date2;
}

formFields['regimeFiscal_regimeImpositionBenefices_rsBNC']                                  = (declarationAffectation.declarationPatrimoine.objetPartiel or not $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL) ? (Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBNC.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesRsBNC')? true : false) : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBNC.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesmBNC') ? true : false);
formFields['regimeFiscal_regimeImpositionBenefices_dcBNC']                                  = (declarationAffectation.declarationPatrimoine.objetPartiel or not $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL) ? (Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBNC.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesDcBNC')? true : false) : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBNC.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesDcBNC') ? true : false);
formFields['regimeFiscal_regimeImpositionBeneficesOptionsParticulieres_optionCreanceDette'] = (declarationAffectation.declarationPatrimoine.objetPartiel or not $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL) ? (fiscal.impositionAccessoire.impositionBeneficesBNC.regimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionCreanceDette ? true : false) : (impotEIRL.impositionAccessoire.impositionBeneficesBNC.eirlRegimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionCreanceDette ? true : false);
formFields['regimeFiscal_regimeImpositionBenefices_mBIC']                                   = (declarationAffectation.declarationPatrimoine.objetPartiel or not $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL) ? (Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC.regimeFiscalRegimeImpositionBenefice).eq('regimeFiscalRegimeImpositionBeneficesMBIC')? true : false) : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBIC.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesMBIC') ? true : false);
formFields['regimeFiscal_regimeImpositionBenefices_rsBIC']                                  = (declarationAffectation.declarationPatrimoine.objetPartiel or not $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL) ? (Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC.regimeFiscalRegimeImpositionBenefice).eq('regimeFiscalRegimeImpositionBeneficesRsBIC')? true : false) : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBIC.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesRsBIC') ? true : false);
formFields['regimeFiscal_regimeImpositionBenefices_rnBIC']                                  = (declarationAffectation.declarationPatrimoine.objetPartiel or not $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL) ? (Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC.regimeFiscalRegimeImpositionBenefice).eq('regimeFiscalRegimeImpositionBeneficesRnBIC')? true : false) : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBIC.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesRnBIC') ? true : false);
formFields['regimeFiscal_regimeImpositionTVA_rfTVA']                                        = (declarationAffectation.declarationPatrimoine.objetPartiel or not $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL) ? (Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC.regimeFiscalRegimeImpositionBenefice).eq('regimeFiscalRegimeImpositionBeneficesMBIC')? true : (Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC.regimeTVA1).eq('regimeFiscalRegimeImpositionTVARfTVA')? true : (Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC.regimeTVA2).eq('regimeFiscalRegimeImpositionTVARfTVA')? true : (Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBNC.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesRsBNC')? true : (Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBNC.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARfTVA') ? true : false))))) : (Value('id').of(impotEIRL.optionsTVA.regimeTVA2).eq('eirlRegimeFiscalRegimeImpositionTVARfTVA') ? true : (Value('id').of(impotEIRL.optionsTVA.regimeTVA3).eq('eirlRegimeFiscalRegimeImpositionTVARfTVA') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBIC.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesMBIC') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBIC.regimeTVA1).eq('eirlRegimeFiscalRegimeImpositionTVARfTVA') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBIC.regimeTVA2).eq('eirlRegimeFiscalRegimeImpositionTVARfTVA') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBNC.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesmBNC') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBNC.regimeTVA).eq('eirlRegimeFiscalRegimeImpositionTVARfTVA') ? true : false)))))));
formFields['regimeFiscal_regimeImpositionTVA_rsTVA']                                        = (declarationAffectation.declarationPatrimoine.objetPartiel or not $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL) ? (Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC.regimeTVA1).eq('regimeFiscalRegimeImpositionTVARsTVA')? true : (Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBNC.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARsTVA')? true : false)) : (Value('id').of(impotEIRL.optionsTVA.regimeTVA2).eq('eirlRegimeFiscalRegimeImpositionTVARsTVA') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBIC.regimeTVA1).eq('eirlRegimeFiscalRegimeImpositionTVARsTVA') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBNC.regimeTVA).eq('eirlRegimeFiscalRegimeImpositionTVARsTVA') ? true : false)));
formFields['regimeFiscal_regimeImpositionTVA_mrTVA']                                        = (declarationAffectation.declarationPatrimoine.objetPartiel or not $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL) ? (Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC.regimeTVA1).eq('regimeFiscalRegimeImpositionTVAMrTVA')? true : false) : (Value('id').of(impotEIRL.optionsTVA.regimeTVA2).eq('eirlRegimeFiscalRegimeImpositionTVAMrTVA') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBIC.regimeTVA1).eq('eirlRegimeFiscalRegimeImpositionTVAMrTVA') ? true : false));
formFields['regimeFiscal_regimeImpositionTVA_rnTVA']                                        = (declarationAffectation.declarationPatrimoine.objetPartiel or not $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL) ? (Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC.regimeTVA2).eq('regimeFiscalRegimeImpositionTVARnTVA')? true : (Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBNC.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARnTVA')? true : false)) : (Value('id').of(impotEIRL.optionsTVA.regimeTVA3).eq('eirlRegimeFiscalRegimeImpositionTVARnTVA') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBIC.regimeTVA2).eq('eirlRegimeFiscalRegimeImpositionTVARnTVA') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBNC.regimeTVA).eq('eirlRegimeFiscalRegimeImpositionTVARnTVA') ? true : false)));
formFields['regimeFiscal_regimeImpositionTVAOptionsParticulieres2']                         = (declarationAffectation.declarationPatrimoine.objetPartiel or not $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL) ? (fiscal.impositionAccessoire.impositionBeneficesBIC.regimeFiscalRegimeImpositionTVAOptionsParticulieres2 ? true : false) : (impotEIRL.impositionAccessoire.impositionBeneficesBIC.eirlRegimeFiscalRegimeImpositionTVAOptionsParticulieres2 ? true : false);
formFields['regimeFiscal_regimeImpositionTVAOptionsParticulieres1']                         = (declarationAffectation.declarationPatrimoine.objetPartiel or not $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL) ? (fiscal.impositionAccessoire.impositionBeneficesBIC.regimeFiscalRegimeImpositionTVAOptionsParticulieres1 ? true : (fiscal.impositionAccessoire.impositionBeneficesBNC.regimeFiscalRegimeImpositionTVAOptionsParticulieres1 ? true : false)) : (impotEIRL.optionsTVA.eirlregimeFiscalRegimeImpositionTVAOptionsParticulieres1 ? true : (impotEIRL.impositionAccessoire.impositionBeneficesBIC.eirlregimeFiscalRegimeImpositionTVAOptionsParticulieres1 ? true : (impotEIRL.impositionAccessoire.impositionBeneficesBNC.eirlRegimeFiscalRegimeImpositionTVAOptionsParticulieres1 ? true : false)));


//Cadre 11 - Observations

var correspondance = $p0agricole.cadre10RensCompGroup.cadre10RensComp ;

formFields['formalite_observations']                                                    = correspondance.formaliteObservations;

// Cadre 12 - Adresse de correspondance

formFields['formalite_correspondanceAdresseCocheCadre']                                 = (Value('id').of(correspondance.adresseCorrespond).eq('domi') or Value('id').of(correspondance.adresseCorrespond).eq('prof')) ? true : false;
formFields['formalite_correspondanceAdresseCadre']                                      = Value('id').of(correspondance.adresseCorrespond).eq('domi') ? "2" : (Value('id').of(correspondance.adresseCorrespond).eq('prof') ? "4" : ' ');
formFields['formalite_correspondanceAdresseCocheAutre']                                 = Value('id').of(correspondance.adresseCorrespond).eq('autre') ? true : false;
formFields['formalite_correspondanceAdresse_nomVoie']                                   = (correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance != null ? correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance : '')
																						+ ' ' + (correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance : '')
																						+ ' ' + (correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance : '')
																						+ ' ' + (correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance : '')
																						+ ' ' + (correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance : '');
formFields['formalite_correspondanceAdresse_complementVoie']                            = (correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance != null ? correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance : '')
																						+ ' ' + (correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance != null ? correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance : '');
formFields['formalite_correspondanceAdresse_codePostal']                                = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal != null ? correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal : '';
formFields['formalite_correspondanceAdresse_commune']									= correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune != null ? correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune : '';
formFields['formalite_telephone1']                                                      = correspondance.infosSup.formaliteTelephone1;
formFields['formalite_telephone2']                                                      = correspondance.infosSup.formaliteTelephone2;
formFields['formalite_fax_courriel']                                                    = correspondance.infosSup.formaliteFaxCourriel != null ? correspondance.infosSup.formaliteFaxCourriel : correspondance.infosSup.telecopie;

// Cadre 13 - Non diffusion

var signataire = $p0agricole.cadre11SignatureGroup.cadre11Signature ;

formFields['formalite_nonDiffusionInformation']                                         = signataire.formaliteNonDiffusionInformation ? true : false;

// Cadres 20 - Signature

formFields['formalite_signataireQualite_declarant']                                     = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteDeclarant') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFields['formalite_signataireQualite_mandataire']                                    = true;
formFields['formalite_signataireNom']						           					= signataire.adresseMandataire.nomPrenomDenominationMandataire;	
formFields['formalite_signataireAdresseVoie']                                           = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '') 
																						+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																						+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																						+ ' ' + signataire.adresseMandataire.nomVoieMandataire
																						+ ' ' + (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '')
																						+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '');
formFields['formalite_signataireAdresseCodePostal']                                     = signataire.adresseMandataire.codePostalMandataire; 
formFields['formalite_signataireAdresseCommune']                                        = signataire.adresseMandataire.villeAdresseMandataire;
}
formFields['formalite_signatureLieu']                                                   = signataire.formaliteSignatureLieu;
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['formalite_signatureDate'] = date;
}	
formFields['formalite_peirl_oui']                                                       = $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL ? true : false;
formFields['formalite_peirl_non']                                                       = $p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL ? false : true;
formFields['formalite_nb_intercalaires']                                                = socialAyantDroit4.autreBeneficiaireAM ? "1" : "0";
formFields['signature']                                                                 = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce";

//P0' Intercalaire

formFields['complete_p0pl']                                                              = false;
formFields['complete_aco']                                                               = false;
formFields['complete_p0agr']                                                             = true;
formFields['complete_p0agricole']                                                        = false;
var prenoms=[];
for ( i = 0; i < identite.personneLieePPPrenom.size() ; i++ ){prenoms.push(identite.personneLieePPPrenom[i]);}                            
formFields['personneLiee_personnePhysique_nomNaissance_prenom']                          = identite.personneLieePPNomNaissance + ' ' + prenoms.toString();
formFields['personneLiee_personnePhysique_dateNaissance_P0Prime']                        = identite.personneLieePPDateNaissance;
formFields['numero_intercalaire']                                                        = "1";
formFields['complete_cadre']                                                             = '';
formFields['entrepriseLieeContratAppui']                                                 = '';
formFields['complete_cerfa']                                                             = '';


// PEIRL agricole

//Cadre 1 PEIRL CMB - Déclaration ou modification d'affectation de patrimoine

formFields['formulaire_dependance_P0Agricole']                                                   = true;
formFields['formulaire_dependance_P2Agricole']                                                   = false;
formFields['formulaire_dependance_P4Agricole']                                                   = false;
formFields['declaration_initiale']                                                               = true;
formFields['declaration_modification']                                                           = false;

// Cadre 2 PEIRL CMB- Rappel d'identification

formFields['eirl_siren']                                                                    = '';
formFields['eirl_nomNaissance']                                                             = $p0agricole.cadre1IdentiteGroup.cadre1Identite.personneLieePPNomNaissance;
formFields['eirl_nomUsage']                                                                 = identite.personneLieePPNomUsage;
var prenoms=[];
for ( i = 0; i < identite.personneLieePPPrenom.size() ; i++ ){prenoms.push(identite.personneLieePPPrenom[i]);}                            
formFields['eirl_prenom']                          											= prenoms.toString();

// Cadre 3 - Déclaration d'affectation de patrimoine

formFields['eirl_statutEIRL_declarationInitialeSansDepot']                                  =  Value('id').of(declarationAffectation.eirlDepot).eq('eirlSansDepot') ? true : false;
formFields['eirl_statutEIRL_declarationInitialeAvecDepot']                                  =  Value('id').of(declarationAffectation.eirlDepot).eq('eirlAvecDepot') ? true : false;
formFields['eirl_denomination']                                                             = declarationAffectation.declarationPatrimoine.eirlDenomination;
formFields['eirl_objet']                                                                    = declarationAffectation.declarationPatrimoine.eirlObjet;
if (declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable !== null) {
    var dateTmp = new Date(parseInt(declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable.getTimeInMillis()));
    var dateClotureEc = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateClotureEc = dateClotureEc.concat(pad(month.toString()));
    formFields['eirl_dateClotureExerciceComptable']          = dateClotureEc;
}
if (Value('id').of(declarationAffectation.eirlStatut).eq('EirlStatutEIRLReprise')) {
formFields['eirl_statutEIRL_reprise']                                                       = true;
formFields['eirl_precedentEIRLDenomination']                                                = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLDenomination;
formFields['eirl_precedentEIRLLieuImmatriculation']                                         = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLLieuImmatriculation;
formFields['eirl_precedentEIRLSIREN']                                                       = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN != null ? (declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN.split(' ').join('')) : '';
}
// Cadre 7 - Options fiscales EIRL

var impotEIRL = $p0agricole.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine.cadre3OptionsFiscalesEIRL;

formFields['eirl_regimeFiscal_regimeImpositionBenefices_mBA']                                    = Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesMBA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rsBA']                                   = Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesRsBA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rnBA']                                   = Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesRnBA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_ffBA']                                   = Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesffBA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rfBA']                                   = Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesRfBA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_optionIS']                               = Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesOptionIS') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rsIS']                                   = Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeIS).eq('eirlRegimeFiscalRegimeImpositionBeneficesRsIS') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rnIS']                                   = Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeIS).eq('eirlRegimeFiscalRegimeImpositionBeneficesRnIS') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVA_remboursementForfaitaire']                     = Value('id').of(impotEIRL.optionsTVA.regimeTVA1).eq('eirlRegimeFiscalRegimeImpositionTVARemboursementForfaitaire') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVA_impositionObligatoire']                        = (Value('id').of(impotEIRL.optionsTVA.regimeTVA1).eq('eirlRegimeFiscalRegimeImpositionTVAImpositionObligatoire') or Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesRsBA') or Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesRnBA')) ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVAOptionsParticulieres4']                         = Value('id').of(impotEIRL.optionsTVA.optionTVAvolontaire).eq('eirlRegimeFiscalRegimeImpositionTVAOptionsParticulieres4') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVAOptionsParticulieres3']                         = Value('id').of(impotEIRL.optionsTVA.optionTVADepot).eq('eirlRegimeFiscalRegimeImpositionTVAOptionsParticulieres3') ? true : false;
if(impotEIRL.optionsTVA.eirlRegimeFiscalDateClotureExerciceComptable !== null) {
	var dateTmp = new Date(parseInt(impotEIRL.optionsTVA.eirlRegimeFiscalDateClotureExerciceComptable.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['eirl_regimeFiscal_dateClotureExerciceComptable'] = date;
}

formFields['eirl_regimeFiscal_regimeImpositionTVAOptionsDepotTrimestrielle']                     = Value('id').of(impotEIRL.optionsTVA.optionTVADepot).eq('eirlRegimeFiscalRegimeImpositionTVADepotTrimestriel') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVAOptionsDepotMensuelle']                         = Value('id').of(impotEIRL.optionsTVA.optionTVADepot).eq('eirlRegimeFiscalRegimeImpositionTVADepotMensuel') ? true : false;

formFields['eirl_regimeFiscal_regimeImpositionTVAOptionsBailleurRuraux']                         = Value('id').of(impotEIRL.optionsTVA.optionTVAvolontaire).eq('eirlRegimeFiscalRegimeImpositionTVAOptionsBailleurRuraux') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVA_rfTVA']                                        = Value('id').of(impotEIRL.optionsTVA.regimeTVA2).eq('eirlRegimeFiscalRegimeImpositionTVARfTVA') ? true : (Value('id').of(impotEIRL.optionsTVA.regimeTVA3).eq('eirlRegimeFiscalRegimeImpositionTVARfTVA') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBIC.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesMBIC') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBIC.regimeTVA1).eq('eirlRegimeFiscalRegimeImpositionTVARfTVA') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBIC.regimeTVA2).eq('eirlRegimeFiscalRegimeImpositionTVARfTVA') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBNC.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesmBNC') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBNC.regimeTVA).eq('eirlRegimeFiscalRegimeImpositionTVARfTVA') ? true : false))))));
formFields['eirl_regimeFiscal_regimeImpositionTVA_rsTVA']                                        = Value('id').of(impotEIRL.optionsTVA.regimeTVA2).eq('eirlRegimeFiscalRegimeImpositionTVARsTVA') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBIC.regimeTVA1).eq('eirlRegimeFiscalRegimeImpositionTVARsTVA') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBNC.regimeTVA).eq('eirlRegimeFiscalRegimeImpositionTVARsTVA') ? true : false));
formFields['eirl_regimeFiscal_regimeImpositionTVA_mrTVA']                                        = Value('id').of(impotEIRL.optionsTVA.regimeTVA2).eq('eirlRegimeFiscalRegimeImpositionTVAMrTVA') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBIC.regimeTVA1).eq('eirlRegimeFiscalRegimeImpositionTVAMrTVA') ? true : false);
formFields['eirl_regimeFiscal_regimeImpositionTVA_rnTVA']                                        = Value('id').of(impotEIRL.optionsTVA.regimeTVA3).eq('eirlRegimeFiscalRegimeImpositionTVARnTVA') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBIC.regimeTVA2).eq('eirlRegimeFiscalRegimeImpositionTVARnTVA') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBNC.regimeTVA).eq('eirlRegimeFiscalRegimeImpositionTVARnTVA') ? true : false));
formFields['eirl_regimeFiscal_regimeImpositionTVAOptionsParticulieres2']                         = impotEIRL.impositionAccessoire.impositionBeneficesBIC.eirlRegimeFiscalRegimeImpositionTVAOptionsParticulieres2 ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVAOptionsParticulieres1']                         = impotEIRL.optionsTVA.eirlregimeFiscalRegimeImpositionTVAOptionsParticulieres1 ? true : (impotEIRL.impositionAccessoire.impositionBeneficesBIC.eirlregimeFiscalRegimeImpositionTVAOptionsParticulieres1 ? true : (impotEIRL.impositionAccessoire.impositionBeneficesBNC.eirlRegimeFiscalRegimeImpositionTVAOptionsParticulieres1 ? true : false));
formFields['eirl_regimeFiscal_regimeImpositionBenefices_mBIC']                                   = Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBIC.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesMBIC') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rsBIC']                                  = Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBIC.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesRsBIC') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rnBIC']                                  = Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBIC.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesRnBIC') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_dcBNC']                                  = Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBNC.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesDcBNC') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBeneficesOptionsParticulieres_optionCreanceDette'] = impotEIRL.impositionAccessoire.impositionBeneficesBNC.eirlRegimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionCreanceDette ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_mBNC']                                   = Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBNC.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesmBNC') ? true : false;


// NSP 1

var socialAideFamilial1 = $p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.aideFamilial1.aideFamilialInfo;
var formFieldsPers1 = {}

if (social.aideFamilialDeclaration) {
// Cadre 1
formFieldsPers1['ayantDroit_lienParente_membreExploitation_NSP']                         = false;
formFieldsPers1['ayantDroit_lienParente_aideFamilial_NSP']                               = true;
formFieldsPers1['ayantDroit_lienParente_associeExploitation_NSP']                        = false;
formFieldsPers1['ayantDroit_affiliationMSA_non_NSP']                                     = false;
formFieldsPers1['ayantDroit_affiliationMSA_oui_NSP']                                     = true;
formFieldsPers1['formulaire_dependance_P0Agricole_NSP']                                  = true;
formFieldsPers1['formulaire_dependance_FAgricole_NSP']                                   = false;
formFieldsPers1['formulaire_dependance_M0Agricole_NSP']                                  = false;
formFieldsPers1['formulaire_dependance_M2Agricole_NSP']                                  = false;
formFieldsPers1['formulaire_dependance_M3_NSP']                                          = false;

// Cadre 2 Bis
var prenoms=[];
for ( i = 0; i < identite.personneLieePPPrenom.size() ; i++ ){prenoms.push(identite.personneLieePPPrenom[i]);}                            
formFieldsPers1['personneLiee_PP_nomNaissance_NSP']                                      = (identite.personneLieePPNomUsage != null ? identite.personneLieePPNomUsage : identite.personneLieePPNomNaissance) + ' ' + prenoms.toString();


// Cadre 3A
var nirDeclarant1 = socialAideFamilial1.aideFamilialNumeroSecuriteSociale;
if(nirDeclarant1 != null) {
    nirDeclarant1 = nirDeclarant1.replace(/ /g, "");
    formFieldsPers1['aideFamilial_numeroSecuriteSociale_NSP']                = nirDeclarant1.substring(0, 13);
    formFieldsPers1['aideFamilial_numeroSecuriteSocialeCle_NSP']             = nirDeclarant1.substring(13, 15);
} 
formFieldsPers1['aideFamilial_identite_nomNaissance_NSP']                                = socialAideFamilial1.aideFamilialIdentiteNomNaissance;
formFieldsPers1['aideFamilial_identite_nomUsage_NSP']                                    = socialAideFamilial1.aideFamilialIdentiteNomUsage != null ? socialAideFamilial1.aideFamilialIdentiteNomUsage : '';
formFieldsPers1['aideFamilial_identite_prenom_NSP']                                      = socialAideFamilial1.aideFamilialIdentitePrenomNaissance;
formFieldsPers1['aideFamilial_connuMSA_oui_NSP']                                         = socialAideFamilial1.aideFamilialConnuMSAOuiNSP ? true : false;
formFieldsPers1['aideFamilial_connuMSA_non_NSP']                                         = socialAideFamilial1.aideFamilialConnuMSAOuiNSP ? false : true;
formFieldsPers1['aideFamilial_regimeAssuranceMaladie_regimeGeneral_NSP']                 = Value('id').of(socialAideFamilial1.aideFamilialRegimeMaladie).eq('aideFamilialRegimeAssuranceMaladieRegimeGeneralNSP') ? true : false;
formFieldsPers1['aideFamilial_regimeAssuranceMaladie_agricole_NSP']                      = Value('id').of(socialAideFamilial1.aideFamilialRegimeMaladie).eq('aideFamilialRegimeAssuranceMaladieAgricoleNSP') ? true : false;
formFieldsPers1['aideFamilial_regimeAssuranceMaladie_nonSalarieNonAgricole_NSP']         = Value('id').of(socialAideFamilial1.aideFamilialRegimeMaladie).eq('aideFamilialRegimeAssuranceMaladieNonSalarieNonAgricoleNSP') ? true : false;
formFieldsPers1['aideFamilial_regimeAssuranceMaladie_autre_NSP']                         = Value('id').of(socialAideFamilial1.aideFamilialRegimeMaladie).eq('aideFamilialRegimeAssuranceMaladieAutreNSP') ? true : false;
formFieldsPers1['aideFamilial_regimeAssuranceMaladieAutre_NSP']                          = socialAideFamilial1.aideFamilialRegimeAssuranceMaladieAutreTexteNSP;
formFieldsPers1['aideFamilial_activiteAutreQueDeclareeStatut_non']                       = socialAideFamilial1.aideFamilialActiviteAutreQueDeclareeStatutOui ? false : true;
if (socialAideFamilial1.aideFamilialActiviteAutreQueDeclareeStatutOui) {
formFieldsPers1['aideFamilial_activiteAutreQueDeclareeStatut_oui']                       = true;
formFieldsPers1['aideFamilial_regimeAssuranceMaladie_agricole_simultane_NSP']            = Value('id').of(socialAideFamilial1.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialRegimeAssuranceMaladieAgricoleSimultaneNSP') ? true : false;
formFieldsPers1['aideFamilial_regimeAssuranceMaladie_regimeGeneral_simultane_NSP']       = Value('id').of(socialAideFamilial1.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialActiviteAutreQueDeclareeStatutSalarieNSP') ? true : false;
formFieldsPers1['aideFamilial_regimeAssuranceMaladie_nonSalarieNonAgricole_simultane_NSP'] = Value('id').of(socialAideFamilial1.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialRegimeAssuranceMaladieNonSalarieNonAgricoleSimultaneNSP') ? true : false;
formFieldsPers1['aideFamilial_regimeAssuranceMaladie_retraite_simultane_NSP']            = Value('id').of(socialAideFamilial1.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialActiviteAutreQueDeclareeStatutRetraiteNSP') ? true : false;
formFieldsPers1['aideFamilial_regimeAssuranceMaladie_invalidite_simultane_NSP']          = Value('id').of(socialAideFamilial1.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialActiviteAutreQueDeclareeStatutPensionneInvaliditeNSP') ? true : false;
formFieldsPers1['aideFamilial_regimeAssuranceMaladie_autre_simultane_NSP']               = Value('id').of(socialAideFamilial1.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialRegimeAssuranceMaladieAutreSimultaneNSP') ? true : false;
formFieldsPers1['aideFamilial_regimeAssuranceMaladie_autreLibelle_simultane_NSP']        = socialAideFamilial1.aideFamilialRegimeAssuranceMaladieAutreSimultaneTexteNSP != null ? socialAideFamilial1.aideFamilialRegimeAssuranceMaladieAutreSimultaneTexteNSP : '';
formFieldsPers1['aideFamilial_organismeServantPension_NSP']                              = socialAideFamilial1.aideFamilialOrganismeServantPensionNSP != null ? socialAideFamilial1.aideFamilialOrganismeServantPensionNSP : '';
}
formFieldsPers1['aideFamilial_conjointCouvertAssuranceMaladie_oui_NSP']                  = Value('id').of(socialAideFamilial1.aideFamilialConjointCouvertAssuranceMaladieOuiNSP).eq('oui') ? true : false;
formFieldsPers1['aideFamilial_conjointCouvertAssuranceMaladie_non_NSP']                  = Value('id').of(socialAideFamilial1.aideFamilialConjointCouvertAssuranceMaladieOuiNSP).eq('non') ? true : false;
var nirDeclarant = socialAideFamilial1.aideFamilialConjointCouvertAssuranceMaladieNumeroSSNSP;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers1['aideFamilial_conjointCouvertAssuranceMaladie_numeroSS_NSP']                = nirDeclarant.substring(0, 13);
    formFieldsPers1['aideFamilial_conjointCouvertAssuranceMaladie_numeroSSCle_NSP']             = nirDeclarant.substring(13, 15);
}

// Cadre 3B
formFieldsPers1['aideFamilial_adresse_nomVoie_NSP']                                      = (socialAideFamilial1.adresseAideFamilial.adresseNumeroVoie != null ? socialAideFamilial1.adresseAideFamilial.adresseNumeroVoie : '')
																						 + ' ' + (socialAideFamilial1.adresseAideFamilial.adresseIndiceVoie != null ? socialAideFamilial1.adresseAideFamilial.adresseIndiceVoie : '') 
																						 + ' ' + (socialAideFamilial1.adresseAideFamilial.adresseTypeVoie != null ? socialAideFamilial1.adresseAideFamilial.adresseTypeVoie : '')
																						 + ' ' + socialAideFamilial1.adresseAideFamilial.adresseNomVoie
																						 + ' ' + (socialAideFamilial1.adresseAideFamilial.adresseComplementVoie != null ? socialAideFamilial1.adresseAideFamilial.adresseComplementVoie : '')
																						 + ' ' + (socialAideFamilial1.adresseAideFamilial.adresseDistriutionSpecialeVoie != null ? socialAideFamilial1.adresseAideFamilial.adresseDistriutionSpecialeVoie : '');
formFieldsPers1['aideFamilial_adresse_codePostal_NSP']                                  = socialAideFamilial1.adresseAideFamilial.adresseCodePostal;
formFieldsPers1['aideFamilial_adresse_commune_NSP']                                     = socialAideFamilial1.adresseAideFamilial.adresseCommune;


// NSP 1 Ayant droit 1 

if (socialAideFamilial1.ayantDroitsDeclarationNSP) {
formFieldsPers1['ayantDroit_identite_nomNaissance1_NSP']                                 = socialAideFamilial1.ayantsdroitsNSP1.ayantDroitIdentiteNomNaissance + ' ' + socialAideFamilial1.ayantsdroitsNSP1.ayantDroitIdentitePrenomNaissance;
formFieldsPers1['ayantDroit_numeroSecuriteSociale1_NSP']                                 = socialAideFamilial1.ayantsdroitsNSP1.ayantDroitNumeroSecuriteSociale != null ? socialAideFamilial1.ayantsdroitsNSP1.ayantDroitNumeroSecuriteSociale : '';
formFieldsPers1['ayantDroit_identiteComplement_dateNaissance1_NSP']                      = socialAideFamilial1.ayantsdroitsNSP1.ayantDroitDateNaissance != null ? socialAideFamilial1.ayantsdroitsNSP1.ayantDroitDateNaissance : '';
formFieldsPers1['ayantDroit_identiteComplement_lieuNaissanceCommune1_NSP']               = socialAideFamilial1.ayantsdroitsNSP1.ayantDroitNumeroSecuriteSociale != null ? '' :
																						((socialAideFamilial1.ayantsdroitsNSP1.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? 
																						socialAideFamilial1.ayantsdroitsNSP1.ayantDroitIdentiteComplementLieuNaissanceCommune : socialAideFamilial1.ayantsdroitsNSP1.ayantDroitIdentiteComplementLieuNaissanceVille)			
																						 + ' / ' + socialAideFamilial1.ayantsdroitsNSP1.ayantDroitIdentiteComplementLieuNaissancePays 
																						 + ' ' + (Value('id').of(socialAideFamilial1.ayantsdroitsNSP1.ayantDroitSexe).eq('feminin') ? "F" : "M"));
formFieldsPers1['ayantDroit_lienParente1_NSP']                                           = socialAideFamilial1.ayantsdroitsNSP1.ayantDroitLienParente;
formFieldsPers1['ayantDroit_enfantScolarise_oui1_NSP']                                   = socialAideFamilial1.ayantsdroitsNSP1.dataEnfantScolariseBeneficiaireAM ? true : false;
formFieldsPers1['ayantDroit_enfantScolarise_non1_NSP']                                   = socialAideFamilial1.ayantsdroitsNSP1.dataEnfantScolariseBeneficiaireAM ? false : true;
formFieldsPers1['ayantDroit_nationalite1_NSP']                                           = socialAideFamilial1.ayantsdroitsNSP1.ayantDroitNationalite;
}

// NSP 1 Ayant droit 2 

if (socialAideFamilial1.ayantsdroitsNSP1.autreBeneficiaireAMnsp) {
formFieldsPers1['ayantDroit_identite_nomNaissance2_NSP']                                 = socialAideFamilial1.ayantsdroitsNSP2.ayantDroitIdentiteNomNaissance + ' ' + socialAideFamilial1.ayantsdroitsNSP2.ayantDroitIdentitePrenomNaissance;
formFieldsPers1['ayantDroit_numeroSecuriteSociale2_NSP']                                 = socialAideFamilial1.ayantsdroitsNSP2.ayantDroitNumeroSecuriteSociale != null ? socialAideFamilial1.ayantsdroitsNSP2.ayantDroitNumeroSecuriteSociale : '';
formFieldsPers1['ayantDroit_identiteComplement_dateNaissance2_NSP']                      = socialAideFamilial1.ayantsdroitsNSP2.ayantDroitDateNaissance != null ? socialAideFamilial1.ayantsdroitsNSP2.ayantDroitDateNaissance : '';
formFieldsPers1['ayantDroit_identiteComplement_lieuNaissanceCommune2_NSP']               = socialAideFamilial1.ayantsdroitsNSP2.ayantDroitNumeroSecuriteSociale != null ? '' :
																						((socialAideFamilial1.ayantsdroitsNSP2.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? 
																						socialAideFamilial1.ayantsdroitsNSP2.ayantDroitIdentiteComplementLieuNaissanceCommune : socialAideFamilial1.ayantsdroitsNSP2.ayantDroitIdentiteComplementLieuNaissanceVille)			
																						 + ' / ' + socialAideFamilial1.ayantsdroitsNSP2.ayantDroitIdentiteComplementLieuNaissancePays 
																						 + ' ' + (Value('id').of(socialAideFamilial1.ayantsdroitsNSP2.ayantDroitSexe).eq('feminin') ? "F" : "M"));
formFieldsPers1['ayantDroit_lienParente2_NSP']                                           = socialAideFamilial1.ayantsdroitsNSP2.ayantDroitLienParente;
formFieldsPers1['ayantDroit_enfantScolarise_oui2_NSP']                                   = socialAideFamilial1.ayantsdroitsNSP2.dataEnfantScolariseBeneficiaireAM ? true : false;
formFieldsPers1['ayantDroit_enfantScolarise_non2_NSP']                                   = socialAideFamilial1.ayantsdroitsNSP2.dataEnfantScolariseBeneficiaireAM ? false : true;
formFieldsPers1['ayantDroit_nationalite2_NSP']                                           = socialAideFamilial1.ayantsdroitsNSP2.ayantDroitNationalite;
}

// NSP 1 Ayant droit 3 

if (socialAideFamilial1.ayantsdroitsNSP2.autreBeneficiaireAMnsp) {
formFieldsPers1['ayantDroit_identite_nomNaissance3_NSP']                                 = socialAideFamilial1.ayantsdroitsNSP3.ayantDroitIdentiteNomNaissance + ' ' + socialAideFamilial1.ayantsdroitsNSP3.ayantDroitIdentitePrenomNaissance;
formFieldsPers1['ayantDroit_numeroSecuriteSociale3_NSP']                                 = socialAideFamilial1.ayantsdroitsNSP3.ayantDroitNumeroSecuriteSociale != null ? socialAideFamilial1.ayantsdroitsNSP3.ayantDroitNumeroSecuriteSociale : '';
formFieldsPers1['ayantDroit_identiteComplement_dateNaissance3_NSP']                      = socialAideFamilial1.ayantsdroitsNSP3.ayantDroitDateNaissance != null ? socialAideFamilial1.ayantsdroitsNSP3.ayantDroitDateNaissance : '';
formFieldsPers1['ayantDroit_identiteComplement_lieuNaissanceCommune3_NSP']               = socialAideFamilial1.ayantsdroitsNSP3.ayantDroitNumeroSecuriteSociale != null ? '' :
																						((socialAideFamilial1.ayantsdroitsNSP3.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? 
																						socialAideFamilial1.ayantsdroitsNSP3.ayantDroitIdentiteComplementLieuNaissanceCommune : socialAideFamilial1.ayantsdroitsNSP3.ayantDroitIdentiteComplementLieuNaissanceVille)			
																						 + ' / ' + socialAideFamilial1.ayantsdroitsNSP3.ayantDroitIdentiteComplementLieuNaissancePays 
																						 + ' ' + (Value('id').of(socialAideFamilial1.ayantsdroitsNSP3.ayantDroitSexe).eq('feminin') ? "F" : "M"));
formFieldsPers1['ayantDroit_lienParente3_NSP']                                           = socialAideFamilial1.ayantsdroitsNSP3.ayantDroitLienParente;
formFieldsPers1['ayantDroit_enfantScolarise_oui3_NSP']                                   = socialAideFamilial1.ayantsdroitsNSP3.dataEnfantScolariseBeneficiaireAM ? true : false;
formFieldsPers1['ayantDroit_enfantScolarise_non3_NSP']                                   = socialAideFamilial1.ayantsdroitsNSP3.dataEnfantScolariseBeneficiaireAM ? false : true;
formFieldsPers1['ayantDroit_nationalite3_NSP']                                           = socialAideFamilial1.ayantsdroitsNSP3.ayantDroitNationalite;
}

// Cadre 5

formFieldsPers1['formalite_observations_NSP']                                     = $p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.aideFamilial1.aideFamilialInfoComplementairesGroup.aideFamilialInfoComplementaires != null ? $p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.aideFamilial1.aideFamilialInfoComplementairesGroup.aideFamilialInfoComplementaires : '';

// Cadre 6
formFieldsPers1['formalite_signataireQualite_declarantNSP']                              = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteDeclarant') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers1['formalite_signataireQualite_mandataireNSP']                             = true;
formFieldsPers1['formalite_signataireAdresse_NSP']                                       = signataire.adresseMandataire.nomPrenomDenominationMandataire
																							+ ' ' + (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '') 
																							+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																							+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																							+ ' ' + signataire.adresseMandataire.nomVoieMandataire
																							+ ' ' + (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '')
																							+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '');
formFieldsPers1['formalite_signataireCodePostal_NSP']                                    = signataire.adresseMandataire.codePostalMandataire; 
formFieldsPers1['formalite_signataireCommune_NSP']                                       = signataire.adresseMandataire.villeAdresseMandataire;
}
formFieldsPers1['formalite_signatureLieu_NSP']                                           = signataire.formaliteSignatureLieu;
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers1['formalite_signatureDate_NSP'] = date;
}	
formFieldsPers1['nombreP0prime_NSP']                                                     = socialAyantDroit4.autreBeneficiaireAM ? "1" : "0";
formFieldsPers1['signature_NSP']                                                         = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce";
}

// NSP 2
var socialAideFamilial2 = $p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.aideFamilial2.aideFamilialInfo;
var formFieldsPers2 = {}

if ($p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.aideFamilial1.aideFamilialDeclarationAutre) {
// Cadre 1
formFieldsPers2['ayantDroit_lienParente_membreExploitation_NSP']                         = false;
formFieldsPers2['ayantDroit_lienParente_aideFamilial_NSP']                               = true;
formFieldsPers2['ayantDroit_lienParente_associeExploitation_NSP']                        = false;
formFieldsPers2['ayantDroit_affiliationMSA_non_NSP']                                     = false;
formFieldsPers2['ayantDroit_affiliationMSA_oui_NSP']                                     = true;
formFieldsPers2['formulaire_dependance_P0Agricole_NSP']                                  = true;
formFieldsPers2['formulaire_dependance_FAgricole_NSP']                                   = false;
formFieldsPers2['formulaire_dependance_M0Agricole_NSP']                                  = false;
formFieldsPers2['formulaire_dependance_M2Agricole_NSP']                                  = false;
formFieldsPers2['formulaire_dependance_M3_NSP']                                          = false;

// Cadre 2 Bis
var prenoms=[];
for ( i = 0; i < identite.personneLieePPPrenom.size() ; i++ ){prenoms.push(identite.personneLieePPPrenom[i]);}                            
formFieldsPers2['personneLiee_PP_nomNaissance_NSP']                                      = (identite.personneLieePPNomUsage != null ? identite.personneLieePPNomUsage : identite.personneLieePPNomNaissance) + ' ' + prenoms.toString();


// Cadre 3A
var nirDeclarant1 = socialAideFamilial2.aideFamilialNumeroSecuriteSociale;
if(nirDeclarant1 != null) {
    nirDeclarant1 = nirDeclarant1.replace(/ /g, "");
    formFieldsPers2['aideFamilial_numeroSecuriteSociale_NSP']                = nirDeclarant1.substring(0, 13);
    formFieldsPers2['aideFamilial_numeroSecuriteSocialeCle_NSP']             = nirDeclarant1.substring(13, 15);
} 
formFieldsPers2['aideFamilial_identite_nomNaissance_NSP']                                = socialAideFamilial2.aideFamilialIdentiteNomNaissance;
formFieldsPers2['aideFamilial_identite_nomUsage_NSP']                                    = socialAideFamilial2.aideFamilialIdentiteNomUsage != null ? socialAideFamilial2.aideFamilialIdentiteNomUsage : '';
formFieldsPers2['aideFamilial_identite_prenom_NSP']                                      = socialAideFamilial2.aideFamilialIdentitePrenomNaissance;
formFieldsPers2['aideFamilial_connuMSA_oui_NSP']                                         = socialAideFamilial2.aideFamilialConnuMSAOuiNSP ? true : false;
formFieldsPers2['aideFamilial_connuMSA_non_NSP']                                         = socialAideFamilial2.aideFamilialConnuMSAOuiNSP ? false : true;
formFieldsPers2['aideFamilial_regimeAssuranceMaladie_regimeGeneral_NSP']                 = Value('id').of(socialAideFamilial2.aideFamilialRegimeMaladie).eq('aideFamilialRegimeAssuranceMaladieRegimeGeneralNSP') ? true : false;
formFieldsPers2['aideFamilial_regimeAssuranceMaladie_agricole_NSP']                      = Value('id').of(socialAideFamilial2.aideFamilialRegimeMaladie).eq('aideFamilialRegimeAssuranceMaladieAgricoleNSP') ? true : false;
formFieldsPers2['aideFamilial_regimeAssuranceMaladie_nonSalarieNonAgricole_NSP']         = Value('id').of(socialAideFamilial2.aideFamilialRegimeMaladie).eq('aideFamilialRegimeAssuranceMaladieNonSalarieNonAgricoleNSP') ? true : false;
formFieldsPers2['aideFamilial_regimeAssuranceMaladie_autre_NSP']                         = Value('id').of(socialAideFamilial2.aideFamilialRegimeMaladie).eq('aideFamilialRegimeAssuranceMaladieAutreNSP') ? true : false;
formFieldsPers2['aideFamilial_regimeAssuranceMaladieAutre_NSP']                          = socialAideFamilial2.aideFamilialRegimeAssuranceMaladieAutreTexteNSP;
formFieldsPers2['aideFamilial_activiteAutreQueDeclareeStatut_non']                       = socialAideFamilial2.aideFamilialActiviteAutreQueDeclareeStatutOui ? false : true;
if (socialAideFamilial2.aideFamilialActiviteAutreQueDeclareeStatutOui) {
formFieldsPers2['aideFamilial_activiteAutreQueDeclareeStatut_oui']                       = true;
formFieldsPers2['aideFamilial_regimeAssuranceMaladie_agricole_simultane_NSP']            = Value('id').of(socialAideFamilial2.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialRegimeAssuranceMaladieAgricoleSimultaneNSP') ? true : false;
formFieldsPers2['aideFamilial_regimeAssuranceMaladie_regimeGeneral_simultane_NSP']       = Value('id').of(socialAideFamilial2.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialActiviteAutreQueDeclareeStatutSalarieNSP') ? true : false;
formFieldsPers2['aideFamilial_regimeAssuranceMaladie_nonSalarieNonAgricole_simultane_NSP'] = Value('id').of(socialAideFamilial2.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialRegimeAssuranceMaladieNonSalarieNonAgricoleSimultaneNSP') ? true : false;
formFieldsPers2['aideFamilial_regimeAssuranceMaladie_retraite_simultane_NSP']            = Value('id').of(socialAideFamilial2.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialActiviteAutreQueDeclareeStatutRetraiteNSP') ? true : false;
formFieldsPers2['aideFamilial_regimeAssuranceMaladie_invalidite_simultane_NSP']          = Value('id').of(socialAideFamilial2.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialActiviteAutreQueDeclareeStatutPensionneInvaliditeNSP') ? true : false;
formFieldsPers2['aideFamilial_regimeAssuranceMaladie_autre_simultane_NSP']               = Value('id').of(socialAideFamilial2.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialRegimeAssuranceMaladieAutreSimultaneNSP') ? true : false;
formFieldsPers2['aideFamilial_regimeAssuranceMaladie_autreLibelle_simultane_NSP']        = socialAideFamilial2.aideFamilialRegimeAssuranceMaladieAutreSimultaneTexteNSP != null ? socialAideFamilial2.aideFamilialRegimeAssuranceMaladieAutreSimultaneTexteNSP : '';
formFieldsPers2['aideFamilial_organismeServantPension_NSP']                              = socialAideFamilial2.aideFamilialOrganismeServantPensionNSP != null ? socialAideFamilial2.aideFamilialOrganismeServantPensionNSP : '';
}
formFieldsPers2['aideFamilial_conjointCouvertAssuranceMaladie_oui_NSP']                  = Value('id').of(socialAideFamilial2.aideFamilialConjointCouvertAssuranceMaladieOuiNSP).eq('oui') ? true : false;
formFieldsPers2['aideFamilial_conjointCouvertAssuranceMaladie_non_NSP']                  = Value('id').of(socialAideFamilial2.aideFamilialConjointCouvertAssuranceMaladieOuiNSP).eq('non') ? true : false;
var nirDeclarant = socialAideFamilial2.aideFamilialConjointCouvertAssuranceMaladieNumeroSSNSP;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers2['aideFamilial_conjointCouvertAssuranceMaladie_numeroSS_NSP']                = nirDeclarant.substring(0, 13);
    formFieldsPers2['aideFamilial_conjointCouvertAssuranceMaladie_numeroSSCle_NSP']             = nirDeclarant.substring(13, 15);
}

// Cadre 3B
formFieldsPers2['aideFamilial_adresse_nomVoie_NSP']                                      = (socialAideFamilial2.adresseAideFamilial.adresseNumeroVoie != null ? socialAideFamilial2.adresseAideFamilial.adresseNumeroVoie : '')
																						 + ' ' + (socialAideFamilial2.adresseAideFamilial.adresseIndiceVoie != null ? socialAideFamilial2.adresseAideFamilial.adresseIndiceVoie : '') 
																						 + ' ' + (socialAideFamilial2.adresseAideFamilial.adresseTypeVoie != null ? socialAideFamilial2.adresseAideFamilial.adresseTypeVoie : '')
																						 + ' ' + socialAideFamilial2.adresseAideFamilial.adresseNomVoie
																						 + ' ' + (socialAideFamilial2.adresseAideFamilial.adresseComplementVoie != null ? socialAideFamilial2.adresseAideFamilial.adresseComplementVoie : '')
																						 + ' ' + (socialAideFamilial2.adresseAideFamilial.adresseDistriutionSpecialeVoie != null ? socialAideFamilial2.adresseAideFamilial.adresseDistriutionSpecialeVoie : '');
formFieldsPers2['aideFamilial_adresse_codePostal_NSP']                                  = socialAideFamilial2.adresseAideFamilial.adresseCodePostal;
formFieldsPers2['aideFamilial_adresse_commune_NSP']                                     = socialAideFamilial2.adresseAideFamilial.adresseCommune;


// NSP 1 Ayant droit 1 

if (socialAideFamilial2.ayantDroitsDeclarationNSP) {
formFieldsPers2['ayantDroit_identite_nomNaissance1_NSP']                                 = socialAideFamilial2.ayantsdroitsNSP1.ayantDroitIdentiteNomNaissance + ' ' + socialAideFamilial2.ayantsdroitsNSP1.ayantDroitIdentitePrenomNaissance;
formFieldsPers2['ayantDroit_numeroSecuriteSociale1_NSP']                                 = socialAideFamilial2.ayantsdroitsNSP1.ayantDroitNumeroSecuriteSociale != null ? socialAideFamilial2.ayantsdroitsNSP1.ayantDroitNumeroSecuriteSociale : '';
formFieldsPers2['ayantDroit_identiteComplement_dateNaissance1_NSP']                      = socialAideFamilial2.ayantsdroitsNSP1.ayantDroitDateNaissance != null ? socialAideFamilial2.ayantsdroitsNSP1.ayantDroitDateNaissance : '';
formFieldsPers2['ayantDroit_identiteComplement_lieuNaissanceCommune1_NSP']               = socialAideFamilial2.ayantsdroitsNSP1.ayantDroitNumeroSecuriteSociale != null ? '' :
																						((socialAideFamilial2.ayantsdroitsNSP1.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? 
																						socialAideFamilial2.ayantsdroitsNSP1.ayantDroitIdentiteComplementLieuNaissanceCommune : socialAideFamilial2.ayantsdroitsNSP1.ayantDroitIdentiteComplementLieuNaissanceVille)			
																						 + ' / ' + socialAideFamilial2.ayantsdroitsNSP1.ayantDroitIdentiteComplementLieuNaissancePays 
																						 + ' ' + (Value('id').of(socialAideFamilial2.ayantsdroitsNSP1.ayantDroitSexe).eq('feminin') ? "F" : "M"));
formFieldsPers2['ayantDroit_lienParente1_NSP']                                           = socialAideFamilial2.ayantsdroitsNSP1.ayantDroitLienParente;
formFieldsPers2['ayantDroit_enfantScolarise_oui1_NSP']                                   = socialAideFamilial2.ayantsdroitsNSP1.dataEnfantScolariseBeneficiaireAM ? true : false;
formFieldsPers2['ayantDroit_enfantScolarise_non1_NSP']                                   = socialAideFamilial2.ayantsdroitsNSP1.dataEnfantScolariseBeneficiaireAM ? false : true;
formFieldsPers2['ayantDroit_nationalite1_NSP']                                           = socialAideFamilial2.ayantsdroitsNSP1.ayantDroitNationalite;
}

// NSP 1 Ayant droit 2 

if (socialAideFamilial2.ayantsdroitsNSP1.autreBeneficiaireAMnsp) {
formFieldsPers2['ayantDroit_identite_nomNaissance2_NSP']                                 = socialAideFamilial2.ayantsdroitsNSP2.ayantDroitIdentiteNomNaissance + ' ' + socialAideFamilial2.ayantsdroitsNSP2.ayantDroitIdentitePrenomNaissance;
formFieldsPers2['ayantDroit_numeroSecuriteSociale2_NSP']                                 = socialAideFamilial2.ayantsdroitsNSP2.ayantDroitNumeroSecuriteSociale != null ? socialAideFamilial2.ayantsdroitsNSP2.ayantDroitNumeroSecuriteSociale : '';
formFieldsPers2['ayantDroit_identiteComplement_dateNaissance2_NSP']                      = socialAideFamilial2.ayantsdroitsNSP2.ayantDroitDateNaissance != null ? socialAideFamilial2.ayantsdroitsNSP2.ayantDroitDateNaissance : '';
formFieldsPers2['ayantDroit_identiteComplement_lieuNaissanceCommune2_NSP']               = socialAideFamilial2.ayantsdroitsNSP2.ayantDroitNumeroSecuriteSociale != null ? '' :
																						((socialAideFamilial2.ayantsdroitsNSP2.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? 
																						socialAideFamilial2.ayantsdroitsNSP2.ayantDroitIdentiteComplementLieuNaissanceCommune : socialAideFamilial2.ayantsdroitsNSP2.ayantDroitIdentiteComplementLieuNaissanceVille)			
																						 + ' / ' + socialAideFamilial2.ayantsdroitsNSP2.ayantDroitIdentiteComplementLieuNaissancePays 
																						 + ' ' + (Value('id').of(socialAideFamilial2.ayantsdroitsNSP2.ayantDroitSexe).eq('feminin') ? "F" : "M"));
formFieldsPers2['ayantDroit_lienParente2_NSP']                                           = socialAideFamilial2.ayantsdroitsNSP2.ayantDroitLienParente;
formFieldsPers2['ayantDroit_enfantScolarise_oui2_NSP']                                   = socialAideFamilial2.ayantsdroitsNSP2.dataEnfantScolariseBeneficiaireAM ? true : false;
formFieldsPers2['ayantDroit_enfantScolarise_non2_NSP']                                   = socialAideFamilial2.ayantsdroitsNSP2.dataEnfantScolariseBeneficiaireAM ? false : true;
formFieldsPers2['ayantDroit_nationalite2_NSP']                                           = socialAideFamilial2.ayantsdroitsNSP2.ayantDroitNationalite;
}

// NSP 1 Ayant droit 3 

if (socialAideFamilial2.ayantsdroitsNSP2.autreBeneficiaireAMnsp) {
formFieldsPers2['ayantDroit_identite_nomNaissance3_NSP']                                 = socialAideFamilial2.ayantsdroitsNSP3.ayantDroitIdentiteNomNaissance + ' ' + socialAideFamilial2.ayantsdroitsNSP3.ayantDroitIdentitePrenomNaissance;
formFieldsPers2['ayantDroit_numeroSecuriteSociale3_NSP']                                 = socialAideFamilial2.ayantsdroitsNSP3.ayantDroitNumeroSecuriteSociale != null ? socialAideFamilial2.ayantsdroitsNSP3.ayantDroitNumeroSecuriteSociale : '';
formFieldsPers2['ayantDroit_identiteComplement_dateNaissance3_NSP']                      = socialAideFamilial2.ayantsdroitsNSP3.ayantDroitDateNaissance != null ? socialAideFamilial2.ayantsdroitsNSP3.ayantDroitDateNaissance : '';
formFieldsPers2['ayantDroit_identiteComplement_lieuNaissanceCommune3_NSP']               = socialAideFamilial2.ayantsdroitsNSP3.ayantDroitNumeroSecuriteSociale != null ? '' :
																						((socialAideFamilial2.ayantsdroitsNSP3.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? 
																						socialAideFamilial2.ayantsdroitsNSP3.ayantDroitIdentiteComplementLieuNaissanceCommune : socialAideFamilial2.ayantsdroitsNSP3.ayantDroitIdentiteComplementLieuNaissanceVille)			
																						 + ' / ' + socialAideFamilial2.ayantsdroitsNSP3.ayantDroitIdentiteComplementLieuNaissancePays 
																						 + ' ' + (Value('id').of(socialAideFamilial2.ayantsdroitsNSP3.ayantDroitSexe).eq('feminin') ? "F" : "M"));
formFieldsPers2['ayantDroit_lienParente3_NSP']                                           = socialAideFamilial2.ayantsdroitsNSP3.ayantDroitLienParente;
formFieldsPers2['ayantDroit_enfantScolarise_oui3_NSP']                                   = socialAideFamilial2.ayantsdroitsNSP3.dataEnfantScolariseBeneficiaireAM ? true : false;
formFieldsPers2['ayantDroit_enfantScolarise_non3_NSP']                                   = socialAideFamilial2.ayantsdroitsNSP3.dataEnfantScolariseBeneficiaireAM ? false : true;
formFieldsPers2['ayantDroit_nationalite3_NSP']                                           = socialAideFamilial2.ayantsdroitsNSP3.ayantDroitNationalite;
}

// Cadre 5

formFieldsPers2['formalite_observations_NSP']                                     = $p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.aideFamilial1.aideFamilialInfoComplementairesGroup.aideFamilialInfoComplementaires != null ? $p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.aideFamilial1.aideFamilialInfoComplementairesGroup.aideFamilialInfoComplementaires : '';

// Cadre 6
formFieldsPers2['formalite_signataireQualite_declarantNSP']                              = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteDeclarant') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers2['formalite_signataireQualite_mandataireNSP']                             = true;
formFieldsPers2['formalite_signataireAdresse_NSP']                                       = signataire.adresseMandataire.nomPrenomDenominationMandataire
																							+ ' ' + (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '') 
																							+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																							+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																							+ ' ' + signataire.adresseMandataire.nomVoieMandataire
																							+ ' ' + (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '')
																							+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '');
formFieldsPers2['formalite_signataireCodePostal_NSP']                                    = signataire.adresseMandataire.codePostalMandataire; 
formFieldsPers2['formalite_signataireCommune_NSP']                                       = signataire.adresseMandataire.villeAdresseMandataire;
}
formFieldsPers2['formalite_signatureLieu_NSP']                                           = signataire.formaliteSignatureLieu;
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers2['formalite_signatureDate_NSP'] = date;
}	
formFieldsPers2['nombreP0prime_NSP']                                                     = socialAyantDroit4.autreBeneficiaireAM ? "1" : "0";
formFieldsPers2['signature_NSP']                                                         = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce";
}

// NSP 3
var socialAideFamilial3 = $p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.aideFamilial3.aideFamilialInfo;
var formFieldsPers3 = {}

if ($p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.aideFamilial2.aideFamilialDeclarationAutre) {
// Cadre 1
formFieldsPers3['ayantDroit_lienParente_membreExploitation_NSP']                         = false;
formFieldsPers3['ayantDroit_lienParente_aideFamilial_NSP']                               = true;
formFieldsPers3['ayantDroit_lienParente_associeExploitation_NSP']                        = false;
formFieldsPers3['ayantDroit_affiliationMSA_non_NSP']                                     = false;
formFieldsPers3['ayantDroit_affiliationMSA_oui_NSP']                                     = true;
formFieldsPers3['formulaire_dependance_P0Agricole_NSP']                                  = true;
formFieldsPers3['formulaire_dependance_FAgricole_NSP']                                   = false;
formFieldsPers3['formulaire_dependance_M0Agricole_NSP']                                  = false;
formFieldsPers3['formulaire_dependance_M2Agricole_NSP']                                  = false;
formFieldsPers3['formulaire_dependance_M3_NSP']                                          = false;

// Cadre 2 Bis
var prenoms=[];
for ( i = 0; i < identite.personneLieePPPrenom.size() ; i++ ){prenoms.push(identite.personneLieePPPrenom[i]);}                            
formFieldsPers3['personneLiee_PP_nomNaissance_NSP']                                      = (identite.personneLieePPNomUsage != null ? identite.personneLieePPNomUsage : identite.personneLieePPNomNaissance) + ' ' + prenoms.toString();


// Cadre 3A
var nirDeclarant1 = socialAideFamilial3.aideFamilialNumeroSecuriteSociale;
if(nirDeclarant1 != null) {
    nirDeclarant1 = nirDeclarant1.replace(/ /g, "");
    formFieldsPers3['aideFamilial_numeroSecuriteSociale_NSP']                = nirDeclarant1.substring(0, 13);
    formFieldsPers3['aideFamilial_numeroSecuriteSocialeCle_NSP']             = nirDeclarant1.substring(13, 15);
} 
formFieldsPers3['aideFamilial_identite_nomNaissance_NSP']                                = socialAideFamilial3.aideFamilialIdentiteNomNaissance;
formFieldsPers3['aideFamilial_identite_nomUsage_NSP']                                    = socialAideFamilial3.aideFamilialIdentiteNomUsage != null ? socialAideFamilial3.aideFamilialIdentiteNomUsage : '';
formFieldsPers3['aideFamilial_identite_prenom_NSP']                                      = socialAideFamilial3.aideFamilialIdentitePrenomNaissance;
formFieldsPers3['aideFamilial_connuMSA_oui_NSP']                                         = socialAideFamilial3.aideFamilialConnuMSAOuiNSP ? true : false;
formFieldsPers3['aideFamilial_connuMSA_non_NSP']                                         = socialAideFamilial3.aideFamilialConnuMSAOuiNSP ? false : true;
formFieldsPers3['aideFamilial_regimeAssuranceMaladie_regimeGeneral_NSP']                 = Value('id').of(socialAideFamilial3.aideFamilialRegimeMaladie).eq('aideFamilialRegimeAssuranceMaladieRegimeGeneralNSP') ? true : false;
formFieldsPers3['aideFamilial_regimeAssuranceMaladie_agricole_NSP']                      = Value('id').of(socialAideFamilial3.aideFamilialRegimeMaladie).eq('aideFamilialRegimeAssuranceMaladieAgricoleNSP') ? true : false;
formFieldsPers3['aideFamilial_regimeAssuranceMaladie_nonSalarieNonAgricole_NSP']         = Value('id').of(socialAideFamilial3.aideFamilialRegimeMaladie).eq('aideFamilialRegimeAssuranceMaladieNonSalarieNonAgricoleNSP') ? true : false;
formFieldsPers3['aideFamilial_regimeAssuranceMaladie_autre_NSP']                         = Value('id').of(socialAideFamilial3.aideFamilialRegimeMaladie).eq('aideFamilialRegimeAssuranceMaladieAutreNSP') ? true : false;
formFieldsPers3['aideFamilial_regimeAssuranceMaladieAutre_NSP']                          = socialAideFamilial3.aideFamilialRegimeAssuranceMaladieAutreTexteNSP;
formFieldsPers3['aideFamilial_activiteAutreQueDeclareeStatut_non']                       = socialAideFamilial3.aideFamilialActiviteAutreQueDeclareeStatutOui ? false : true;
if (socialAideFamilial3.aideFamilialActiviteAutreQueDeclareeStatutOui) {
formFieldsPers3['aideFamilial_activiteAutreQueDeclareeStatut_oui']                       = true;
formFieldsPers3['aideFamilial_regimeAssuranceMaladie_agricole_simultane_NSP']            = Value('id').of(socialAideFamilial3.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialRegimeAssuranceMaladieAgricoleSimultaneNSP') ? true : false;
formFieldsPers3['aideFamilial_regimeAssuranceMaladie_regimeGeneral_simultane_NSP']       = Value('id').of(socialAideFamilial3.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialActiviteAutreQueDeclareeStatutSalarieNSP') ? true : false;
formFieldsPers3['aideFamilial_regimeAssuranceMaladie_nonSalarieNonAgricole_simultane_NSP'] = Value('id').of(socialAideFamilial3.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialRegimeAssuranceMaladieNonSalarieNonAgricoleSimultaneNSP') ? true : false;
formFieldsPers3['aideFamilial_regimeAssuranceMaladie_retraite_simultane_NSP']            = Value('id').of(socialAideFamilial3.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialActiviteAutreQueDeclareeStatutRetraiteNSP') ? true : false;
formFieldsPers3['aideFamilial_regimeAssuranceMaladie_invalidite_simultane_NSP']          = Value('id').of(socialAideFamilial3.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialActiviteAutreQueDeclareeStatutPensionneInvaliditeNSP') ? true : false;
formFieldsPers3['aideFamilial_regimeAssuranceMaladie_autre_simultane_NSP']               = Value('id').of(socialAideFamilial3.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialRegimeAssuranceMaladieAutreSimultaneNSP') ? true : false;
formFieldsPers3['aideFamilial_regimeAssuranceMaladie_autreLibelle_simultane_NSP']        = socialAideFamilial3.aideFamilialRegimeAssuranceMaladieAutreSimultaneTexteNSP != null ? socialAideFamilial3.aideFamilialRegimeAssuranceMaladieAutreSimultaneTexteNSP : '';
formFieldsPers3['aideFamilial_organismeServantPension_NSP']                              = socialAideFamilial3.aideFamilialOrganismeServantPensionNSP != null ? socialAideFamilial3.aideFamilialOrganismeServantPensionNSP : '';
}
formFieldsPers3['aideFamilial_conjointCouvertAssuranceMaladie_oui_NSP']                  = Value('id').of(socialAideFamilial3.aideFamilialConjointCouvertAssuranceMaladieOuiNSP).eq('oui') ? true : false;
formFieldsPers3['aideFamilial_conjointCouvertAssuranceMaladie_non_NSP']                  = Value('id').of(socialAideFamilial3.aideFamilialConjointCouvertAssuranceMaladieOuiNSP).eq('non') ? true : false;
var nirDeclarant = socialAideFamilial3.aideFamilialConjointCouvertAssuranceMaladieNumeroSSNSP;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers3['aideFamilial_conjointCouvertAssuranceMaladie_numeroSS_NSP']                = nirDeclarant.substring(0, 13);
    formFieldsPers3['aideFamilial_conjointCouvertAssuranceMaladie_numeroSSCle_NSP']             = nirDeclarant.substring(13, 15);
}

// Cadre 3B
formFieldsPers3['aideFamilial_adresse_nomVoie_NSP']                                      = (socialAideFamilial3.adresseAideFamilial.adresseNumeroVoie != null ? socialAideFamilial3.adresseAideFamilial.adresseNumeroVoie : '')
																						 + ' ' + (socialAideFamilial3.adresseAideFamilial.adresseIndiceVoie != null ? socialAideFamilial3.adresseAideFamilial.adresseIndiceVoie : '') 
																						 + ' ' + (socialAideFamilial3.adresseAideFamilial.adresseTypeVoie != null ? socialAideFamilial3.adresseAideFamilial.adresseTypeVoie : '')
																						 + ' ' + socialAideFamilial3.adresseAideFamilial.adresseNomVoie
																						 + ' ' + (socialAideFamilial3.adresseAideFamilial.adresseComplementVoie != null ? socialAideFamilial3.adresseAideFamilial.adresseComplementVoie : '')
																						 + ' ' + (socialAideFamilial3.adresseAideFamilial.adresseDistriutionSpecialeVoie != null ? socialAideFamilial3.adresseAideFamilial.adresseDistriutionSpecialeVoie : '');
formFieldsPers3['aideFamilial_adresse_codePostal_NSP']                                  = socialAideFamilial3.adresseAideFamilial.adresseCodePostal;
formFieldsPers3['aideFamilial_adresse_commune_NSP']                                     = socialAideFamilial3.adresseAideFamilial.adresseCommune;


// NSP 1 Ayant droit 1 

if (socialAideFamilial3.ayantDroitsDeclarationNSP) {
formFieldsPers3['ayantDroit_identite_nomNaissance1_NSP']                                 = socialAideFamilial3.ayantsdroitsNSP1.ayantDroitIdentiteNomNaissance + ' ' + socialAideFamilial3.ayantsdroitsNSP1.ayantDroitIdentitePrenomNaissance;
formFieldsPers3['ayantDroit_numeroSecuriteSociale1_NSP']                                 = socialAideFamilial3.ayantsdroitsNSP1.ayantDroitNumeroSecuriteSociale != null ? socialAideFamilial3.ayantsdroitsNSP1.ayantDroitNumeroSecuriteSociale : '';
formFieldsPers3['ayantDroit_identiteComplement_dateNaissance1_NSP']                      = socialAideFamilial3.ayantsdroitsNSP1.ayantDroitDateNaissance != null ? socialAideFamilial3.ayantsdroitsNSP1.ayantDroitDateNaissance : '';
formFieldsPers3['ayantDroit_identiteComplement_lieuNaissanceCommune1_NSP']               = socialAideFamilial3.ayantsdroitsNSP1.ayantDroitNumeroSecuriteSociale != null ? '' :
																						((socialAideFamilial3.ayantsdroitsNSP1.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? 
																						socialAideFamilial3.ayantsdroitsNSP1.ayantDroitIdentiteComplementLieuNaissanceCommune : socialAideFamilial3.ayantsdroitsNSP1.ayantDroitIdentiteComplementLieuNaissanceVille)			
																						 + ' / ' + socialAideFamilial3.ayantsdroitsNSP1.ayantDroitIdentiteComplementLieuNaissancePays 
																						 + ' ' + (Value('id').of(socialAideFamilial3.ayantsdroitsNSP1.ayantDroitSexe).eq('feminin') ? "F" : "M"));
formFieldsPers3['ayantDroit_lienParente1_NSP']                                           = socialAideFamilial3.ayantsdroitsNSP1.ayantDroitLienParente;
formFieldsPers3['ayantDroit_enfantScolarise_oui1_NSP']                                   = socialAideFamilial3.ayantsdroitsNSP1.dataEnfantScolariseBeneficiaireAM ? true : false;
formFieldsPers3['ayantDroit_enfantScolarise_non1_NSP']                                   = socialAideFamilial3.ayantsdroitsNSP1.dataEnfantScolariseBeneficiaireAM ? false : true;
formFieldsPers3['ayantDroit_nationalite1_NSP']                                           = socialAideFamilial3.ayantsdroitsNSP1.ayantDroitNationalite;
}

// NSP 1 Ayant droit 2 

if (socialAideFamilial3.ayantsdroitsNSP1.autreBeneficiaireAMnsp) {
formFieldsPers3['ayantDroit_identite_nomNaissance2_NSP']                                 = socialAideFamilial3.ayantsdroitsNSP2.ayantDroitIdentiteNomNaissance + ' ' + socialAideFamilial3.ayantsdroitsNSP2.ayantDroitIdentitePrenomNaissance;
formFieldsPers3['ayantDroit_numeroSecuriteSociale2_NSP']                                 = socialAideFamilial3.ayantsdroitsNSP2.ayantDroitNumeroSecuriteSociale != null ? socialAideFamilial3.ayantsdroitsNSP2.ayantDroitNumeroSecuriteSociale : '';
formFieldsPers3['ayantDroit_identiteComplement_dateNaissance2_NSP']                      = socialAideFamilial3.ayantsdroitsNSP2.ayantDroitDateNaissance != null ? socialAideFamilial3.ayantsdroitsNSP2.ayantDroitDateNaissance : '';
formFieldsPers3['ayantDroit_identiteComplement_lieuNaissanceCommune2_NSP']               = socialAideFamilial3.ayantsdroitsNSP2.ayantDroitNumeroSecuriteSociale != null ? '' :
																						((socialAideFamilial3.ayantsdroitsNSP2.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? 
																						socialAideFamilial3.ayantsdroitsNSP2.ayantDroitIdentiteComplementLieuNaissanceCommune : socialAideFamilial3.ayantsdroitsNSP2.ayantDroitIdentiteComplementLieuNaissanceVille)			
																						 + ' / ' + socialAideFamilial3.ayantsdroitsNSP2.ayantDroitIdentiteComplementLieuNaissancePays 
																						 + ' ' + (Value('id').of(socialAideFamilial3.ayantsdroitsNSP2.ayantDroitSexe).eq('feminin') ? "F" : "M"));
formFieldsPers3['ayantDroit_lienParente2_NSP']                                           = socialAideFamilial3.ayantsdroitsNSP2.ayantDroitLienParente;
formFieldsPers3['ayantDroit_enfantScolarise_oui2_NSP']                                   = socialAideFamilial3.ayantsdroitsNSP2.dataEnfantScolariseBeneficiaireAM ? true : false;
formFieldsPers3['ayantDroit_enfantScolarise_non2_NSP']                                   = socialAideFamilial3.ayantsdroitsNSP2.dataEnfantScolariseBeneficiaireAM ? false : true;
formFieldsPers3['ayantDroit_nationalite2_NSP']                                           = socialAideFamilial3.ayantsdroitsNSP2.ayantDroitNationalite;
}

// NSP 1 Ayant droit 3 

if (socialAideFamilial3.ayantsdroitsNSP2.autreBeneficiaireAMnsp) {
formFieldsPers3['ayantDroit_identite_nomNaissance3_NSP']                                 = socialAideFamilial3.ayantsdroitsNSP3.ayantDroitIdentiteNomNaissance + ' ' + socialAideFamilial3.ayantsdroitsNSP3.ayantDroitIdentitePrenomNaissance;
formFieldsPers3['ayantDroit_numeroSecuriteSociale3_NSP']                                 = socialAideFamilial3.ayantsdroitsNSP3.ayantDroitNumeroSecuriteSociale != null ? socialAideFamilial3.ayantsdroitsNSP3.ayantDroitNumeroSecuriteSociale : '';
formFieldsPers3['ayantDroit_identiteComplement_dateNaissance3_NSP']                      = socialAideFamilial3.ayantsdroitsNSP3.ayantDroitDateNaissance != null ? socialAideFamilial3.ayantsdroitsNSP3.ayantDroitDateNaissance : '';
formFieldsPers3['ayantDroit_identiteComplement_lieuNaissanceCommune3_NSP']               = socialAideFamilial3.ayantsdroitsNSP3.ayantDroitNumeroSecuriteSociale != null ? '' :
																						((socialAideFamilial3.ayantsdroitsNSP3.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? 
																						socialAideFamilial3.ayantsdroitsNSP3.ayantDroitIdentiteComplementLieuNaissanceCommune : socialAideFamilial3.ayantsdroitsNSP3.ayantDroitIdentiteComplementLieuNaissanceVille)			
																						 + ' / ' + socialAideFamilial3.ayantsdroitsNSP3.ayantDroitIdentiteComplementLieuNaissancePays 
																						 + ' ' + (Value('id').of(socialAideFamilial3.ayantsdroitsNSP3.ayantDroitSexe).eq('feminin') ? "F" : "M"));
formFieldsPers3['ayantDroit_lienParente3_NSP']                                           = socialAideFamilial3.ayantsdroitsNSP3.ayantDroitLienParente;
formFieldsPers3['ayantDroit_enfantScolarise_oui3_NSP']                                   = socialAideFamilial3.ayantsdroitsNSP3.dataEnfantScolariseBeneficiaireAM ? true : false;
formFieldsPers3['ayantDroit_enfantScolarise_non3_NSP']                                   = socialAideFamilial3.ayantsdroitsNSP3.dataEnfantScolariseBeneficiaireAM ? false : true;
formFieldsPers3['ayantDroit_nationalite3_NSP']                                           = socialAideFamilial3.ayantsdroitsNSP3.ayantDroitNationalite;
}

// Cadre 5

formFieldsPers3['formalite_observations_NSP']                                     = $p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.aideFamilial1.aideFamilialInfoComplementairesGroup.aideFamilialInfoComplementaires != null ? $p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.aideFamilial1.aideFamilialInfoComplementairesGroup.aideFamilialInfoComplementaires : '';

// Cadre 6
formFieldsPers3['formalite_signataireQualite_declarantNSP']                              = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteDeclarant') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers3['formalite_signataireQualite_mandataireNSP']                             = true;
formFieldsPers3['formalite_signataireAdresse_NSP']                                       = signataire.adresseMandataire.nomPrenomDenominationMandataire
																							+ ' ' + (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '') 
																							+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																							+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																							+ ' ' + signataire.adresseMandataire.nomVoieMandataire
																							+ ' ' + (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '')
																							+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '');
formFieldsPers3['formalite_signataireCodePostal_NSP']                                    = signataire.adresseMandataire.codePostalMandataire; 
formFieldsPers3['formalite_signataireCommune_NSP']                                       = signataire.adresseMandataire.villeAdresseMandataire;
}
formFieldsPers3['formalite_signatureLieu_NSP']                                           = signataire.formaliteSignatureLieu;
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers3['formalite_signatureDate_NSP'] = date;
}	
formFieldsPers3['nombreP0prime_NSP']                                                     = socialAyantDroit4.autreBeneficiaireAM ? "1" : "0";
formFieldsPers3['signature_NSP']                                                         = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce";
}

// NSP 4
var socialAideFamilial4 = $p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.aideFamilial4.aideFamilialInfo;
var formFieldsPers4 = {}

if ($p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.aideFamilial3.aideFamilialDeclarationAutre) {
// Cadre 1
formFieldsPers4['ayantDroit_lienParente_membreExploitation_NSP']                         = false;
formFieldsPers4['ayantDroit_lienParente_aideFamilial_NSP']                               = true;
formFieldsPers4['ayantDroit_lienParente_associeExploitation_NSP']                        = false;
formFieldsPers4['ayantDroit_affiliationMSA_non_NSP']                                     = false;
formFieldsPers4['ayantDroit_affiliationMSA_oui_NSP']                                     = true;
formFieldsPers4['formulaire_dependance_P0Agricole_NSP']                                  = true;
formFieldsPers4['formulaire_dependance_FAgricole_NSP']                                   = false;
formFieldsPers4['formulaire_dependance_M0Agricole_NSP']                                  = false;
formFieldsPers4['formulaire_dependance_M2Agricole_NSP']                                  = false;
formFieldsPers4['formulaire_dependance_M3_NSP']                                          = false;

// Cadre 2 Bis
var prenoms=[];
for ( i = 0; i < identite.personneLieePPPrenom.size() ; i++ ){prenoms.push(identite.personneLieePPPrenom[i]);}                            
formFieldsPers4['personneLiee_PP_nomNaissance_NSP']                                      = (identite.personneLieePPNomUsage != null ? identite.personneLieePPNomUsage : identite.personneLieePPNomNaissance) + ' ' + prenoms.toString();


// Cadre 3A
var nirDeclarant1 = socialAideFamilial4.aideFamilialNumeroSecuriteSociale;
if(nirDeclarant1 != null) {
    nirDeclarant1 = nirDeclarant1.replace(/ /g, "");
    formFieldsPers4['aideFamilial_numeroSecuriteSociale_NSP']                = nirDeclarant1.substring(0, 13);
    formFieldsPers4['aideFamilial_numeroSecuriteSocialeCle_NSP']             = nirDeclarant1.substring(13, 15);
} 
formFieldsPers4['aideFamilial_identite_nomNaissance_NSP']                                = socialAideFamilial4.aideFamilialIdentiteNomNaissance;
formFieldsPers4['aideFamilial_identite_nomUsage_NSP']                                    = socialAideFamilial4.aideFamilialIdentiteNomUsage != null ? socialAideFamilial4.aideFamilialIdentiteNomUsage : '';
formFieldsPers4['aideFamilial_identite_prenom_NSP']                                      = socialAideFamilial4.aideFamilialIdentitePrenomNaissance;
formFieldsPers4['aideFamilial_connuMSA_oui_NSP']                                         = socialAideFamilial4.aideFamilialConnuMSAOuiNSP ? true : false;
formFieldsPers4['aideFamilial_connuMSA_non_NSP']                                         = socialAideFamilial4.aideFamilialConnuMSAOuiNSP ? false : true;
formFieldsPers4['aideFamilial_regimeAssuranceMaladie_regimeGeneral_NSP']                 = Value('id').of(socialAideFamilial4.aideFamilialRegimeMaladie).eq('aideFamilialRegimeAssuranceMaladieRegimeGeneralNSP') ? true : false;
formFieldsPers4['aideFamilial_regimeAssuranceMaladie_agricole_NSP']                      = Value('id').of(socialAideFamilial4.aideFamilialRegimeMaladie).eq('aideFamilialRegimeAssuranceMaladieAgricoleNSP') ? true : false;
formFieldsPers4['aideFamilial_regimeAssuranceMaladie_nonSalarieNonAgricole_NSP']         = Value('id').of(socialAideFamilial4.aideFamilialRegimeMaladie).eq('aideFamilialRegimeAssuranceMaladieNonSalarieNonAgricoleNSP') ? true : false;
formFieldsPers4['aideFamilial_regimeAssuranceMaladie_autre_NSP']                         = Value('id').of(socialAideFamilial4.aideFamilialRegimeMaladie).eq('aideFamilialRegimeAssuranceMaladieAutreNSP') ? true : false;
formFieldsPers4['aideFamilial_regimeAssuranceMaladieAutre_NSP']                          = socialAideFamilial4.aideFamilialRegimeAssuranceMaladieAutreTexteNSP;
formFieldsPers4['aideFamilial_activiteAutreQueDeclareeStatut_non']                       = socialAideFamilial4.aideFamilialActiviteAutreQueDeclareeStatutOui ? false : true;
if (socialAideFamilial4.aideFamilialActiviteAutreQueDeclareeStatutOui) {
formFieldsPers4['aideFamilial_activiteAutreQueDeclareeStatut_oui']                       = true;
formFieldsPers4['aideFamilial_regimeAssuranceMaladie_agricole_simultane_NSP']            = Value('id').of(socialAideFamilial4.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialRegimeAssuranceMaladieAgricoleSimultaneNSP') ? true : false;
formFieldsPers4['aideFamilial_regimeAssuranceMaladie_regimeGeneral_simultane_NSP']       = Value('id').of(socialAideFamilial4.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialActiviteAutreQueDeclareeStatutSalarieNSP') ? true : false;
formFieldsPers4['aideFamilial_regimeAssuranceMaladie_nonSalarieNonAgricole_simultane_NSP'] = Value('id').of(socialAideFamilial4.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialRegimeAssuranceMaladieNonSalarieNonAgricoleSimultaneNSP') ? true : false;
formFieldsPers4['aideFamilial_regimeAssuranceMaladie_retraite_simultane_NSP']            = Value('id').of(socialAideFamilial4.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialActiviteAutreQueDeclareeStatutRetraiteNSP') ? true : false;
formFieldsPers4['aideFamilial_regimeAssuranceMaladie_invalidite_simultane_NSP']          = Value('id').of(socialAideFamilial4.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialActiviteAutreQueDeclareeStatutPensionneInvaliditeNSP') ? true : false;
formFieldsPers4['aideFamilial_regimeAssuranceMaladie_autre_simultane_NSP']               = Value('id').of(socialAideFamilial4.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialRegimeAssuranceMaladieAutreSimultaneNSP') ? true : false;
formFieldsPers4['aideFamilial_regimeAssuranceMaladie_autreLibelle_simultane_NSP']        = socialAideFamilial4.aideFamilialRegimeAssuranceMaladieAutreSimultaneTexteNSP != null ? socialAideFamilial4.aideFamilialRegimeAssuranceMaladieAutreSimultaneTexteNSP : '';
formFieldsPers4['aideFamilial_organismeServantPension_NSP']                              = socialAideFamilial4.aideFamilialOrganismeServantPensionNSP != null ? socialAideFamilial4.aideFamilialOrganismeServantPensionNSP : '';
}
formFieldsPers4['aideFamilial_conjointCouvertAssuranceMaladie_oui_NSP']                  = Value('id').of(socialAideFamilial4.aideFamilialConjointCouvertAssuranceMaladieOuiNSP).eq('oui') ? true : false;
formFieldsPers4['aideFamilial_conjointCouvertAssuranceMaladie_non_NSP']                  = Value('id').of(socialAideFamilial4.aideFamilialConjointCouvertAssuranceMaladieOuiNSP).eq('non') ? true : false;
var nirDeclarant = socialAideFamilial4.aideFamilialConjointCouvertAssuranceMaladieNumeroSSNSP;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers4['aideFamilial_conjointCouvertAssuranceMaladie_numeroSS_NSP']                = nirDeclarant.substring(0, 13);
    formFieldsPers4['aideFamilial_conjointCouvertAssuranceMaladie_numeroSSCle_NSP']             = nirDeclarant.substring(13, 15);
}

// Cadre 3B
formFieldsPers4['aideFamilial_adresse_nomVoie_NSP']                                      = (socialAideFamilial4.adresseAideFamilial.adresseNumeroVoie != null ? socialAideFamilial4.adresseAideFamilial.adresseNumeroVoie : '')
																						 + ' ' + (socialAideFamilial4.adresseAideFamilial.adresseIndiceVoie != null ? socialAideFamilial4.adresseAideFamilial.adresseIndiceVoie : '') 
																						 + ' ' + (socialAideFamilial4.adresseAideFamilial.adresseTypeVoie != null ? socialAideFamilial4.adresseAideFamilial.adresseTypeVoie : '')
																						 + ' ' + socialAideFamilial4.adresseAideFamilial.adresseNomVoie
																						 + ' ' + (socialAideFamilial4.adresseAideFamilial.adresseComplementVoie != null ? socialAideFamilial4.adresseAideFamilial.adresseComplementVoie : '')
																						 + ' ' + (socialAideFamilial4.adresseAideFamilial.adresseDistriutionSpecialeVoie != null ? socialAideFamilial4.adresseAideFamilial.adresseDistriutionSpecialeVoie : '');
formFieldsPers4['aideFamilial_adresse_codePostal_NSP']                                  = socialAideFamilial4.adresseAideFamilial.adresseCodePostal;
formFieldsPers4['aideFamilial_adresse_commune_NSP']                                     = socialAideFamilial4.adresseAideFamilial.adresseCommune;


// NSP 1 Ayant droit 1 

if (socialAideFamilial4.ayantDroitsDeclarationNSP) {
formFieldsPers4['ayantDroit_identite_nomNaissance1_NSP']                                 = socialAideFamilial4.ayantsdroitsNSP1.ayantDroitIdentiteNomNaissance + ' ' + socialAideFamilial4.ayantsdroitsNSP1.ayantDroitIdentitePrenomNaissance;
formFieldsPers4['ayantDroit_numeroSecuriteSociale1_NSP']                                 = socialAideFamilial4.ayantsdroitsNSP1.ayantDroitNumeroSecuriteSociale != null ? socialAideFamilial4.ayantsdroitsNSP1.ayantDroitNumeroSecuriteSociale : '';
formFieldsPers4['ayantDroit_identiteComplement_dateNaissance1_NSP']                      = socialAideFamilial4.ayantsdroitsNSP1.ayantDroitDateNaissance != null ? socialAideFamilial4.ayantsdroitsNSP1.ayantDroitDateNaissance : '';
formFieldsPers4['ayantDroit_identiteComplement_lieuNaissanceCommune1_NSP']               = socialAideFamilial4.ayantsdroitsNSP1.ayantDroitNumeroSecuriteSociale != null ? '' :
																						((socialAideFamilial4.ayantsdroitsNSP1.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? 
																						socialAideFamilial4.ayantsdroitsNSP1.ayantDroitIdentiteComplementLieuNaissanceCommune : socialAideFamilial4.ayantsdroitsNSP1.ayantDroitIdentiteComplementLieuNaissanceVille)			
																						 + ' / ' + socialAideFamilial4.ayantsdroitsNSP1.ayantDroitIdentiteComplementLieuNaissancePays 
																						 + ' ' + (Value('id').of(socialAideFamilial4.ayantsdroitsNSP1.ayantDroitSexe).eq('feminin') ? "F" : "M"));
formFieldsPers4['ayantDroit_lienParente1_NSP']                                           = socialAideFamilial4.ayantsdroitsNSP1.ayantDroitLienParente;
formFieldsPers4['ayantDroit_enfantScolarise_oui1_NSP']                                   = socialAideFamilial4.ayantsdroitsNSP1.dataEnfantScolariseBeneficiaireAM ? true : false;
formFieldsPers4['ayantDroit_enfantScolarise_non1_NSP']                                   = socialAideFamilial4.ayantsdroitsNSP1.dataEnfantScolariseBeneficiaireAM ? false : true;
formFieldsPers4['ayantDroit_nationalite1_NSP']                                           = socialAideFamilial4.ayantsdroitsNSP1.ayantDroitNationalite;
}

// NSP 1 Ayant droit 2 

if (socialAideFamilial4.ayantsdroitsNSP1.autreBeneficiaireAMnsp) {
formFieldsPers4['ayantDroit_identite_nomNaissance2_NSP']                                 = socialAideFamilial4.ayantsdroitsNSP2.ayantDroitIdentiteNomNaissance + ' ' + socialAideFamilial4.ayantsdroitsNSP2.ayantDroitIdentitePrenomNaissance;
formFieldsPers4['ayantDroit_numeroSecuriteSociale2_NSP']                                 = socialAideFamilial4.ayantsdroitsNSP2.ayantDroitNumeroSecuriteSociale != null ? socialAideFamilial4.ayantsdroitsNSP2.ayantDroitNumeroSecuriteSociale : '';
formFieldsPers4['ayantDroit_identiteComplement_dateNaissance2_NSP']                      = socialAideFamilial4.ayantsdroitsNSP2.ayantDroitDateNaissance != null ? socialAideFamilial4.ayantsdroitsNSP2.ayantDroitDateNaissance : '';
formFieldsPers4['ayantDroit_identiteComplement_lieuNaissanceCommune2_NSP']               = socialAideFamilial4.ayantsdroitsNSP2.ayantDroitNumeroSecuriteSociale != null ? '' :
																						((socialAideFamilial4.ayantsdroitsNSP2.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? 
																						socialAideFamilial4.ayantsdroitsNSP2.ayantDroitIdentiteComplementLieuNaissanceCommune : socialAideFamilial4.ayantsdroitsNSP2.ayantDroitIdentiteComplementLieuNaissanceVille)			
																						 + ' / ' + socialAideFamilial4.ayantsdroitsNSP2.ayantDroitIdentiteComplementLieuNaissancePays 
																						 + ' ' + (Value('id').of(socialAideFamilial4.ayantsdroitsNSP2.ayantDroitSexe).eq('feminin') ? "F" : "M"));
formFieldsPers4['ayantDroit_lienParente2_NSP']                                           = socialAideFamilial4.ayantsdroitsNSP2.ayantDroitLienParente;
formFieldsPers4['ayantDroit_enfantScolarise_oui2_NSP']                                   = socialAideFamilial4.ayantsdroitsNSP2.dataEnfantScolariseBeneficiaireAM ? true : false;
formFieldsPers4['ayantDroit_enfantScolarise_non2_NSP']                                   = socialAideFamilial4.ayantsdroitsNSP2.dataEnfantScolariseBeneficiaireAM ? false : true;
formFieldsPers4['ayantDroit_nationalite2_NSP']                                           = socialAideFamilial4.ayantsdroitsNSP2.ayantDroitNationalite;
}

// NSP 1 Ayant droit 3 

if (socialAideFamilial4.ayantsdroitsNSP2.autreBeneficiaireAMnsp) {
formFieldsPers4['ayantDroit_identite_nomNaissance3_NSP']                                 = socialAideFamilial4.ayantsdroitsNSP3.ayantDroitIdentiteNomNaissance + ' ' + socialAideFamilial4.ayantsdroitsNSP3.ayantDroitIdentitePrenomNaissance;
formFieldsPers4['ayantDroit_numeroSecuriteSociale3_NSP']                                 = socialAideFamilial4.ayantsdroitsNSP3.ayantDroitNumeroSecuriteSociale != null ? socialAideFamilial4.ayantsdroitsNSP3.ayantDroitNumeroSecuriteSociale : '';
formFieldsPers4['ayantDroit_identiteComplement_dateNaissance3_NSP']                      = socialAideFamilial4.ayantsdroitsNSP3.ayantDroitDateNaissance != null ? socialAideFamilial4.ayantsdroitsNSP3.ayantDroitDateNaissance : '';
formFieldsPers4['ayantDroit_identiteComplement_lieuNaissanceCommune3_NSP']               = socialAideFamilial4.ayantsdroitsNSP3.ayantDroitNumeroSecuriteSociale != null ? '' :
																						((socialAideFamilial4.ayantsdroitsNSP3.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? 
																						socialAideFamilial4.ayantsdroitsNSP3.ayantDroitIdentiteComplementLieuNaissanceCommune : socialAideFamilial4.ayantsdroitsNSP3.ayantDroitIdentiteComplementLieuNaissanceVille)			
																						 + ' / ' + socialAideFamilial4.ayantsdroitsNSP3.ayantDroitIdentiteComplementLieuNaissancePays 
																						 + ' ' + (Value('id').of(socialAideFamilial4.ayantsdroitsNSP3.ayantDroitSexe).eq('feminin') ? "F" : "M"));
formFieldsPers4['ayantDroit_lienParente3_NSP']                                           = socialAideFamilial4.ayantsdroitsNSP3.ayantDroitLienParente;
formFieldsPers4['ayantDroit_enfantScolarise_oui3_NSP']                                   = socialAideFamilial4.ayantsdroitsNSP3.dataEnfantScolariseBeneficiaireAM ? true : false;
formFieldsPers4['ayantDroit_enfantScolarise_non3_NSP']                                   = socialAideFamilial4.ayantsdroitsNSP3.dataEnfantScolariseBeneficiaireAM ? false : true;
formFieldsPers4['ayantDroit_nationalite3_NSP']                                           = socialAideFamilial4.ayantsdroitsNSP3.ayantDroitNationalite;
}

// Cadre 5

formFieldsPers4['formalite_observations_NSP']                                     = $p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.aideFamilial1.aideFamilialInfoComplementairesGroup.aideFamilialInfoComplementaires != null ? $p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.aideFamilial1.aideFamilialInfoComplementairesGroup.aideFamilialInfoComplementaires : '';

// Cadre 6
formFieldsPers4['formalite_signataireQualite_declarantNSP']                              = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteDeclarant') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers4['formalite_signataireQualite_mandataireNSP']                             = true;
formFieldsPers4['formalite_signataireAdresse_NSP']                                       = signataire.adresseMandataire.nomPrenomDenominationMandataire
																							+ ' ' + (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '') 
																							+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																							+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																							+ ' ' + signataire.adresseMandataire.nomVoieMandataire
																							+ ' ' + (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '')
																							+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '');
formFieldsPers4['formalite_signataireCodePostal_NSP']                                    = signataire.adresseMandataire.codePostalMandataire; 
formFieldsPers4['formalite_signataireCommune_NSP']                                       = signataire.adresseMandataire.villeAdresseMandataire;
}
formFieldsPers4['formalite_signatureLieu_NSP']                                           = signataire.formaliteSignatureLieu;
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers4['formalite_signatureDate_NSP'] = date;
}	
formFieldsPers4['nombreP0prime_NSP']                                                     = socialAyantDroit4.autreBeneficiaireAM ? "1" : "0";
formFieldsPers4['signature_NSP']                                                         = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce";
}

// NSP 5
var socialAideFamilial5 = $p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.aideFamilial5.aideFamilialInfo;
var formFieldsPers5 = {}

if ($p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.aideFamilial3.aideFamilialDeclarationAutre) {
// Cadre 1
formFieldsPers5['ayantDroit_lienParente_membreExploitation_NSP']                         = false;
formFieldsPers5['ayantDroit_lienParente_aideFamilial_NSP']                               = true;
formFieldsPers5['ayantDroit_lienParente_associeExploitation_NSP']                        = false;
formFieldsPers5['ayantDroit_affiliationMSA_non_NSP']                                     = false;
formFieldsPers5['ayantDroit_affiliationMSA_oui_NSP']                                     = true;
formFieldsPers5['formulaire_dependance_P0Agricole_NSP']                                  = true;
formFieldsPers5['formulaire_dependance_FAgricole_NSP']                                   = false;
formFieldsPers5['formulaire_dependance_M0Agricole_NSP']                                  = false;
formFieldsPers5['formulaire_dependance_M2Agricole_NSP']                                  = false;
formFieldsPers5['formulaire_dependance_M3_NSP']                                          = false;

// Cadre 2 Bis
var prenoms=[];
for ( i = 0; i < identite.personneLieePPPrenom.size() ; i++ ){prenoms.push(identite.personneLieePPPrenom[i]);}                            
formFieldsPers5['personneLiee_PP_nomNaissance_NSP']                                      = (identite.personneLieePPNomUsage != null ? identite.personneLieePPNomUsage : identite.personneLieePPNomNaissance) + ' ' + prenoms.toString();


// Cadre 3A
var nirDeclarant1 = socialAideFamilial5.aideFamilialNumeroSecuriteSociale;
if(nirDeclarant1 != null) {
    nirDeclarant1 = nirDeclarant1.replace(/ /g, "");
    formFieldsPers5['aideFamilial_numeroSecuriteSociale_NSP']                = nirDeclarant1.substring(0, 13);
    formFieldsPers5['aideFamilial_numeroSecuriteSocialeCle_NSP']             = nirDeclarant1.substring(13, 15);
} 
formFieldsPers5['aideFamilial_identite_nomNaissance_NSP']                                = socialAideFamilial5.aideFamilialIdentiteNomNaissance;
formFieldsPers5['aideFamilial_identite_nomUsage_NSP']                                    = socialAideFamilial5.aideFamilialIdentiteNomUsage != null ? socialAideFamilial5.aideFamilialIdentiteNomUsage : '';
formFieldsPers5['aideFamilial_identite_prenom_NSP']                                      = socialAideFamilial5.aideFamilialIdentitePrenomNaissance;
formFieldsPers5['aideFamilial_connuMSA_oui_NSP']                                         = socialAideFamilial5.aideFamilialConnuMSAOuiNSP ? true : false;
formFieldsPers5['aideFamilial_connuMSA_non_NSP']                                         = socialAideFamilial5.aideFamilialConnuMSAOuiNSP ? false : true;
formFieldsPers5['aideFamilial_regimeAssuranceMaladie_regimeGeneral_NSP']                 = Value('id').of(socialAideFamilial5.aideFamilialRegimeMaladie).eq('aideFamilialRegimeAssuranceMaladieRegimeGeneralNSP') ? true : false;
formFieldsPers5['aideFamilial_regimeAssuranceMaladie_agricole_NSP']                      = Value('id').of(socialAideFamilial5.aideFamilialRegimeMaladie).eq('aideFamilialRegimeAssuranceMaladieAgricoleNSP') ? true : false;
formFieldsPers5['aideFamilial_regimeAssuranceMaladie_nonSalarieNonAgricole_NSP']         = Value('id').of(socialAideFamilial5.aideFamilialRegimeMaladie).eq('aideFamilialRegimeAssuranceMaladieNonSalarieNonAgricoleNSP') ? true : false;
formFieldsPers5['aideFamilial_regimeAssuranceMaladie_autre_NSP']                         = Value('id').of(socialAideFamilial5.aideFamilialRegimeMaladie).eq('aideFamilialRegimeAssuranceMaladieAutreNSP') ? true : false;
formFieldsPers5['aideFamilial_regimeAssuranceMaladieAutre_NSP']                          = socialAideFamilial5.aideFamilialRegimeAssuranceMaladieAutreTexteNSP;
formFieldsPers5['aideFamilial_activiteAutreQueDeclareeStatut_non']                       = socialAideFamilial5.aideFamilialActiviteAutreQueDeclareeStatutOui ? false : true;
if (socialAideFamilial5.aideFamilialActiviteAutreQueDeclareeStatutOui) {
formFieldsPers5['aideFamilial_activiteAutreQueDeclareeStatut_oui']                       = true;
formFieldsPers5['aideFamilial_regimeAssuranceMaladie_agricole_simultane_NSP']            = Value('id').of(socialAideFamilial5.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialRegimeAssuranceMaladieAgricoleSimultaneNSP') ? true : false;
formFieldsPers5['aideFamilial_regimeAssuranceMaladie_regimeGeneral_simultane_NSP']       = Value('id').of(socialAideFamilial5.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialActiviteAutreQueDeclareeStatutSalarieNSP') ? true : false;
formFieldsPers5['aideFamilial_regimeAssuranceMaladie_nonSalarieNonAgricole_simultane_NSP'] = Value('id').of(socialAideFamilial5.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialRegimeAssuranceMaladieNonSalarieNonAgricoleSimultaneNSP') ? true : false;
formFieldsPers5['aideFamilial_regimeAssuranceMaladie_retraite_simultane_NSP']            = Value('id').of(socialAideFamilial5.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialActiviteAutreQueDeclareeStatutRetraiteNSP') ? true : false;
formFieldsPers5['aideFamilial_regimeAssuranceMaladie_invalidite_simultane_NSP']          = Value('id').of(socialAideFamilial5.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialActiviteAutreQueDeclareeStatutPensionneInvaliditeNSP') ? true : false;
formFieldsPers5['aideFamilial_regimeAssuranceMaladie_autre_simultane_NSP']               = Value('id').of(socialAideFamilial5.aideFamilialActiviteAutreQueDeclaree).eq('aideFamilialRegimeAssuranceMaladieAutreSimultaneNSP') ? true : false;
formFieldsPers5['aideFamilial_regimeAssuranceMaladie_autreLibelle_simultane_NSP']        = socialAideFamilial5.aideFamilialRegimeAssuranceMaladieAutreSimultaneTexteNSP != null ? socialAideFamilial5.aideFamilialRegimeAssuranceMaladieAutreSimultaneTexteNSP : '';
formFieldsPers5['aideFamilial_organismeServantPension_NSP']                              = socialAideFamilial5.aideFamilialOrganismeServantPensionNSP != null ? socialAideFamilial5.aideFamilialOrganismeServantPensionNSP : '';
}
formFieldsPers5['aideFamilial_conjointCouvertAssuranceMaladie_oui_NSP']                  = Value('id').of(socialAideFamilial5.aideFamilialConjointCouvertAssuranceMaladieOuiNSP).eq('oui') ? true : false;
formFieldsPers5['aideFamilial_conjointCouvertAssuranceMaladie_non_NSP']                  = Value('id').of(socialAideFamilial5.aideFamilialConjointCouvertAssuranceMaladieOuiNSP).eq('non') ? true : false;
var nirDeclarant = socialAideFamilial5.aideFamilialConjointCouvertAssuranceMaladieNumeroSSNSP;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers5['aideFamilial_conjointCouvertAssuranceMaladie_numeroSS_NSP']                = nirDeclarant.substring(0, 13);
    formFieldsPers5['aideFamilial_conjointCouvertAssuranceMaladie_numeroSSCle_NSP']             = nirDeclarant.substring(13, 15);
}

// Cadre 3B
formFieldsPers5['aideFamilial_adresse_nomVoie_NSP']                                      = (socialAideFamilial5.adresseAideFamilial.adresseNumeroVoie != null ? socialAideFamilial5.adresseAideFamilial.adresseNumeroVoie : '')
																						 + ' ' + (socialAideFamilial5.adresseAideFamilial.adresseIndiceVoie != null ? socialAideFamilial5.adresseAideFamilial.adresseIndiceVoie : '') 
																						 + ' ' + (socialAideFamilial5.adresseAideFamilial.adresseTypeVoie != null ? socialAideFamilial5.adresseAideFamilial.adresseTypeVoie : '')
																						 + ' ' + socialAideFamilial5.adresseAideFamilial.adresseNomVoie
																						 + ' ' + (socialAideFamilial5.adresseAideFamilial.adresseComplementVoie != null ? socialAideFamilial5.adresseAideFamilial.adresseComplementVoie : '')
																						 + ' ' + (socialAideFamilial5.adresseAideFamilial.adresseDistriutionSpecialeVoie != null ? socialAideFamilial5.adresseAideFamilial.adresseDistriutionSpecialeVoie : '');
formFieldsPers5['aideFamilial_adresse_codePostal_NSP']                                  = socialAideFamilial5.adresseAideFamilial.adresseCodePostal;
formFieldsPers5['aideFamilial_adresse_commune_NSP']                                     = socialAideFamilial5.adresseAideFamilial.adresseCommune;


// NSP 1 Ayant droit 1 

if (socialAideFamilial5.ayantDroitsDeclarationNSP) {
formFieldsPers5['ayantDroit_identite_nomNaissance1_NSP']                                 = socialAideFamilial5.ayantsdroitsNSP1.ayantDroitIdentiteNomNaissance + ' ' + socialAideFamilial5.ayantsdroitsNSP1.ayantDroitIdentitePrenomNaissance;
formFieldsPers5['ayantDroit_numeroSecuriteSociale1_NSP']                                 = socialAideFamilial5.ayantsdroitsNSP1.ayantDroitNumeroSecuriteSociale != null ? socialAideFamilial5.ayantsdroitsNSP1.ayantDroitNumeroSecuriteSociale : '';
formFieldsPers5['ayantDroit_identiteComplement_dateNaissance1_NSP']                      = socialAideFamilial5.ayantsdroitsNSP1.ayantDroitDateNaissance != null ? socialAideFamilial5.ayantsdroitsNSP1.ayantDroitDateNaissance : '';
formFieldsPers5['ayantDroit_identiteComplement_lieuNaissanceCommune1_NSP']               = socialAideFamilial5.ayantsdroitsNSP1.ayantDroitNumeroSecuriteSociale != null ? '' :
																						((socialAideFamilial5.ayantsdroitsNSP1.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? 
																						socialAideFamilial5.ayantsdroitsNSP1.ayantDroitIdentiteComplementLieuNaissanceCommune : socialAideFamilial5.ayantsdroitsNSP1.ayantDroitIdentiteComplementLieuNaissanceVille)			
																						 + ' / ' + socialAideFamilial5.ayantsdroitsNSP1.ayantDroitIdentiteComplementLieuNaissancePays 
																						 + ' ' + (Value('id').of(socialAideFamilial5.ayantsdroitsNSP1.ayantDroitSexe).eq('feminin') ? "F" : "M"));
formFieldsPers5['ayantDroit_lienParente1_NSP']                                           = socialAideFamilial5.ayantsdroitsNSP1.ayantDroitLienParente;
formFieldsPers5['ayantDroit_enfantScolarise_oui1_NSP']                                   = socialAideFamilial5.ayantsdroitsNSP1.dataEnfantScolariseBeneficiaireAM ? true : false;
formFieldsPers5['ayantDroit_enfantScolarise_non1_NSP']                                   = socialAideFamilial5.ayantsdroitsNSP1.dataEnfantScolariseBeneficiaireAM ? false : true;
formFieldsPers5['ayantDroit_nationalite1_NSP']                                           = socialAideFamilial5.ayantsdroitsNSP1.ayantDroitNationalite;
}

// NSP 1 Ayant droit 2 

if (socialAideFamilial5.ayantsdroitsNSP1.autreBeneficiaireAMnsp) {
formFieldsPers5['ayantDroit_identite_nomNaissance2_NSP']                                 = socialAideFamilial5.ayantsdroitsNSP2.ayantDroitIdentiteNomNaissance + ' ' + socialAideFamilial5.ayantsdroitsNSP2.ayantDroitIdentitePrenomNaissance;
formFieldsPers5['ayantDroit_numeroSecuriteSociale2_NSP']                                 = socialAideFamilial5.ayantsdroitsNSP2.ayantDroitNumeroSecuriteSociale != null ? socialAideFamilial5.ayantsdroitsNSP2.ayantDroitNumeroSecuriteSociale : '';
formFieldsPers5['ayantDroit_identiteComplement_dateNaissance2_NSP']                      = socialAideFamilial5.ayantsdroitsNSP2.ayantDroitDateNaissance != null ? socialAideFamilial5.ayantsdroitsNSP2.ayantDroitDateNaissance : '';
formFieldsPers5['ayantDroit_identiteComplement_lieuNaissanceCommune2_NSP']               = socialAideFamilial5.ayantsdroitsNSP2.ayantDroitNumeroSecuriteSociale != null ? '' :
																						((socialAideFamilial5.ayantsdroitsNSP2.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? 
																						socialAideFamilial5.ayantsdroitsNSP2.ayantDroitIdentiteComplementLieuNaissanceCommune : socialAideFamilial5.ayantsdroitsNSP2.ayantDroitIdentiteComplementLieuNaissanceVille)			
																						 + ' / ' + socialAideFamilial5.ayantsdroitsNSP2.ayantDroitIdentiteComplementLieuNaissancePays 
																						 + ' ' + (Value('id').of(socialAideFamilial5.ayantsdroitsNSP2.ayantDroitSexe).eq('feminin') ? "F" : "M"));
formFieldsPers5['ayantDroit_lienParente2_NSP']                                           = socialAideFamilial5.ayantsdroitsNSP2.ayantDroitLienParente;
formFieldsPers5['ayantDroit_enfantScolarise_oui2_NSP']                                   = socialAideFamilial5.ayantsdroitsNSP2.dataEnfantScolariseBeneficiaireAM ? true : false;
formFieldsPers5['ayantDroit_enfantScolarise_non2_NSP']                                   = socialAideFamilial5.ayantsdroitsNSP2.dataEnfantScolariseBeneficiaireAM ? false : true;
formFieldsPers5['ayantDroit_nationalite2_NSP']                                           = socialAideFamilial5.ayantsdroitsNSP2.ayantDroitNationalite;
}

// NSP 1 Ayant droit 3 

if (socialAideFamilial5.ayantsdroitsNSP2.autreBeneficiaireAMnsp) {
formFieldsPers5['ayantDroit_identite_nomNaissance3_NSP']                                 = socialAideFamilial5.ayantsdroitsNSP3.ayantDroitIdentiteNomNaissance + ' ' + socialAideFamilial5.ayantsdroitsNSP3.ayantDroitIdentitePrenomNaissance;
formFieldsPers5['ayantDroit_numeroSecuriteSociale3_NSP']                                 = socialAideFamilial5.ayantsdroitsNSP3.ayantDroitNumeroSecuriteSociale != null ? socialAideFamilial5.ayantsdroitsNSP3.ayantDroitNumeroSecuriteSociale : '';
formFieldsPers5['ayantDroit_identiteComplement_dateNaissance3_NSP']                      = socialAideFamilial5.ayantsdroitsNSP3.ayantDroitDateNaissance != null ? socialAideFamilial5.ayantsdroitsNSP3.ayantDroitDateNaissance : '';
formFieldsPers5['ayantDroit_identiteComplement_lieuNaissanceCommune3_NSP']               = socialAideFamilial5.ayantsdroitsNSP3.ayantDroitNumeroSecuriteSociale != null ? '' :
																						((socialAideFamilial5.ayantsdroitsNSP3.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? 
																						socialAideFamilial5.ayantsdroitsNSP3.ayantDroitIdentiteComplementLieuNaissanceCommune : socialAideFamilial5.ayantsdroitsNSP3.ayantDroitIdentiteComplementLieuNaissanceVille)			
																						 + ' / ' + socialAideFamilial5.ayantsdroitsNSP3.ayantDroitIdentiteComplementLieuNaissancePays 
																						 + ' ' + (Value('id').of(socialAideFamilial5.ayantsdroitsNSP3.ayantDroitSexe).eq('feminin') ? "F" : "M"));
formFieldsPers5['ayantDroit_lienParente3_NSP']                                           = socialAideFamilial5.ayantsdroitsNSP3.ayantDroitLienParente;
formFieldsPers5['ayantDroit_enfantScolarise_oui3_NSP']                                   = socialAideFamilial5.ayantsdroitsNSP3.dataEnfantScolariseBeneficiaireAM ? true : false;
formFieldsPers5['ayantDroit_enfantScolarise_non3_NSP']                                   = socialAideFamilial5.ayantsdroitsNSP3.dataEnfantScolariseBeneficiaireAM ? false : true;
formFieldsPers5['ayantDroit_nationalite3_NSP']                                           = socialAideFamilial5.ayantsdroitsNSP3.ayantDroitNationalite;
}

// Cadre 5

formFieldsPers5['formalite_observations_NSP']                                     = $p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.aideFamilial1.aideFamilialInfoComplementairesGroup.aideFamilialInfoComplementaires != null ? $p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.aideFamilial1.aideFamilialInfoComplementairesGroup.aideFamilialInfoComplementaires : '';

// Cadre 6
formFieldsPers5['formalite_signataireQualite_declarantNSP']                              = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteDeclarant') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers5['formalite_signataireQualite_mandataireNSP']                             = true;
formFieldsPers5['formalite_signataireAdresse_NSP']                                       = signataire.adresseMandataire.nomPrenomDenominationMandataire
																							+ ' ' + (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '') 
																							+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																							+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																							+ ' ' + signataire.adresseMandataire.nomVoieMandataire
																							+ ' ' + (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '')
																							+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '');
formFieldsPers5['formalite_signataireCodePostal_NSP']                                    = signataire.adresseMandataire.codePostalMandataire; 
formFieldsPers5['formalite_signataireCommune_NSP']                                       = signataire.adresseMandataire.villeAdresseMandataire;
}
formFieldsPers5['formalite_signatureLieu_NSP']                                           = signataire.formaliteSignatureLieu;
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers5['formalite_signatureDate_NSP'] = date;
}	
formFieldsPers5['nombreP0prime_NSP']                                                     = socialAyantDroit4.autreBeneficiaireAM ? "1" : "0";
formFieldsPers5['signature_NSP']                                                         = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce";
}


/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
var prenoms=[];
for ( i = 0; i < identite.personneLieePPPrenom.size() ; i++ ){prenoms.push(identite.personneLieePPPrenom[i]);}                            
var civNomPrenom  = identite.personneLieePPNomNaissance + ' ' + prenoms.toString();

 var cerfaDoc1 = nash.doc //
	.load('models/Courrier au premier dossier avant l\'ouverture de GP_postCoEd.pdf') //
	.apply({
		date: signataire.formaliteSignatureDate,
		autoriteHabilitee :"Chambre d'Agriculture",
		demandeContexte : "Déclaration de début d'activité",
		civiliteNomPrenom : civNomPrenom
	});


/*
 * Création du dossier avec option fiscale 
 */
 
var cerfaDoc1 = nash.doc //
	.load('models/cerfa_11922-08_P0_Agricole_avec_volet_social.pdf') //
	.apply(formFields); 

/*
 * Création du dossier sans option fiscale : ajout du cerfa sans option fiscale
 */

  var cerfaDoc2 = nash.doc //
	.load('models/cerfa_11922-08_P0_Agricole_sans_volet_social.pdf') //
	.apply (formFields);
	cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));
	
/*
 * Ajout de l'intercalaire P0 Prime avec volet social
 */
 
 if (socialAyantDroit4.autreBeneficiaireAM) 
{
	var p0PrimeDoc1 = nash.doc //
		.load('models/cerfa_11771-02_P0_Prime_avec_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(p0PrimeDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire P0 Prime sans volet social
 */
 
 if (socialAyantDroit4.autreBeneficiaireAM)
{
	var p0PrimeDoc2 = nash.doc //
		.load('models/cerfa_11771-02_P0_Prime_sans_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(p0PrimeDoc2.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire PEIRL Agricole avec option fiscale
 */
if ($p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL == true)
{
	var peirlMEDoc1 = nash.doc //
		.load('models/cerfa_14216-06_peirl_agricole_avec_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(peirlMEDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire PEIRL Agricole sans option fiscale
 */
if ($p0agricole.cadreEIRLGroup.cadreEIRL.estEIRL == true)
{
	var peirlMEDoc2 = nash.doc //
		.load('models/cerfa_14216-06_peirl_agricole_sans_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(peirlMEDoc2.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire NSP Agricole 1
 */
 
 if (social.aideFamilialDeclaration) {
	var nspDoc1 = nash.doc //
		.load('models/cerfa_11926-05_NSP.pdf') //
		.apply(formFieldsPers1);
	cerfaDoc1.append(nspDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire NSP Agricole 2
 */
 
 if ($p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.aideFamilial1.aideFamilialDeclarationAutre) {
	var nspDoc2 = nash.doc //
		.load('models/cerfa_11926-05_NSP.pdf') //
		.apply(formFieldsPers2);
	cerfaDoc1.append(nspDoc2.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire NSP Agricole 3
 */
 
if ($p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.aideFamilial2.aideFamilialDeclarationAutre) {
	var nspDoc3 = nash.doc //
		.load('models/cerfa_11926-05_NSP.pdf') //
		.apply(formFieldsPers3);
	cerfaDoc1.append(nspDoc3.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire NSP Agricole 4
 */
 
if ($p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.aideFamilial3.aideFamilialDeclarationAutre) {
	var nspDoc4 = nash.doc //
		.load('models/cerfa_11926-05_NSP.pdf') //
		.apply(formFieldsPers4);
	cerfaDoc1.append(nspDoc4.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire NSP Agricole 5 
 */
 
if ($p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.aideFamilial4.aideFamilialDeclarationAutre) {
	var nspDoc5 = nash.doc //
		.load('models/cerfa_11926-05_NSP.pdf') //
		.apply(formFieldsPers5);
	cerfaDoc1.append(nspDoc5.save('cerfa.pdf'));
}

 

var metas = [];

/*
 * Ajout des PJs
 */

// PJ Déclarant

var pj=$p0agricole.cadre11SignatureGroup.cadre11Signature.soussigne;

var pjUser = [];

function appendPj(fld) {
	fld.forEach(function (elm) {
        pjUser.push(elm);
		_log.info('elm.getAbsolutePath() = >'+elm.getAbsolutePath());
		metas.push({'name':'userAttachments', 'value': '/'+elm.getAbsolutePath()});
		metas.push({'name':'document', 'value': '/'+elm.getAbsolutePath()});
    });
}

if(Value('id').of(pj).contains('FormaliteSignataireQualiteDeclarant')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantSignataire);
}

if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantNonSignataire);
} 

// PJ conjoint

var pj=$p0agricole.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.statutConjointCollaborateur ;
if(Value('id').of(pj).contains('VoletSocialStatutConjointCollaborateur') or Value('id').of(pj).contains('VoletSocialStatutConjointSalarie') or Value('id').of(pj).contains('VoletSocialStatutConjointCoExploitant')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjChoixStatutConjoint);
}

// PJ Mandataire

if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire);
}

// PJ Etablissement

var pj=$p0agricole.cadre4AdresseActiviteGroup.cadre4AdresseActivite.activiteLieu ;
if(Value('id').of(pj).contains('entrepriseAdresseEntrepriseDomicile')) {
    appendPj($attachmentPreprocess.attachmentPreprocess.pjDomicile);
}

var pj=$p0agricole.cadre4AdresseActiviteGroup.cadre4AdresseActivite.activiteLieu ;
if(Value('id').of(pj).contains('entrepriseAdresseEntrepriseLieuExploitation')) {
    appendPj($attachmentPreprocess.attachmentPreprocess.pjEtablissementCreation);
}


// PJ Eirl

var pj=$p0agricole.cadreEIRLGroup.cadreEIRL;
if(pj.estEIRL) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjDNCDeclarant);
	appendPj($attachmentPreprocess.attachmentPreprocess.pjLivretFamille);
	appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationAssurance);
	appendPj($attachmentPreprocess.attachmentPreprocess.pjNotificationDroitPaiement);
	appendPj($attachmentPreprocess.attachmentPreprocess.pjAutorisationMineur);
}

if(pj.estEIRL and (Value('id').of(pj.cadre1DeclarationAffectationPatrimoine.eirlStatut).eq('EirlStatutEIRLReprise') or Value('id').of(pj.cadre1DeclarationAffectationPatrimoine.eirlDepot).eq('eirlAvecDepot'))) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjDAP);
}

var pj=$p0agricole.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine;
if(Value('id').of(pj.cadreEIRLInformationsComplementaires.contenuDAP).contains('bienImmo') or Value('id').of(pj.cadreEIRLInformationsComplementairesBis.contenuDAPBis).contains('bienImmo')) {
    appendPj($attachmentPreprocess.attachmentPreprocess.pjDAPActeNotarie);
}

if(Value('id').of(pj.cadreEIRLInformationsComplementaires.contenuDAP).contains('bienCommunIndivis') or Value('id').of(pj.cadreEIRLInformationsComplementairesBis.contenuDAPBis).contains('bienCommunIndivis')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjDAPAccordTiers);
}

 
var finalDoc = cerfaDoc1.save('P0_Agricole.pdf');

//Remove old metas before insert
var metasToDelete = [];
var recordMetas = nash.record.meta() != null ? nash.record.meta().metas : null;
var recordMetasSize = recordMetas != null ? recordMetas.size() : 0;

for (var i = 0; i < recordMetasSize; i++) {
   if (recordMetas.get(i).name == 'document' && !recordMetas.get(i).value.contains("proxy")) {
       metasToDelete.push({'name':'document', 'value': recordMetas.get(i).value});
   }
}

nash.record.removeMeta(metasToDelete);
nash.record.meta(metas);


var data = [ spec.createData({
    id : 'cerfa',
    label : 'Déclaration de début d\'activité agricole',
    description : 'Voici le formulaire obtenu à partir des données saisies.',
    type : 'FileReadOnly',
    value : [ finalDoc ]
}),  spec.createData({
    id : 'uploaded',
    label : 'Pièces jointes',
    description : 'Pièces jointes ajoutées au formulaire.',
    type : 'FileReadOnly',
    value : pjUser
}) ];

var groups = [ spec.createGroup({
    id : 'attachments',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Déclaration de début d\'activité agricole',
    groups : groups
});