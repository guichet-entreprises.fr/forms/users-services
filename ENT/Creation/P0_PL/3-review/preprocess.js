function pad(s) { return (s < 10) ? '0' + s : s; }
var formFields = {};

// Cadres 1 - Activité non salariée

var identite = $p0pl.cadre1IdentiteGroup.cadre1Identite;

formFields['professionLiberale']                                                        = Value('id').of(identite.objetP0).eq('professionLiberale') ? true : false;
formFields['artisteAuteur']                                                             = Value('id').of(identite.objetP0).eq('artisteAuteur') ? true : false;
formFields['formalite_evenement_oui']                                                   = identite.formaliteEvenementOui ? true : false;
formFields['formalite_evenement_non']                                                   = identite.formaliteEvenementOui ? false : true;
formFields['voletSocial_activiteExerceeAnterieurementSIREN']                            = identite.voletSocialActiviteExerceeAnterieurementSIREN != null ? identite.voletSocialActiviteExerceeAnterieurementSIREN.split(' ').join('') : '';

// Cadres 2A - EI

formFields['nonEIRL']                                                                   = $p0pl.cadreEIRLGroup.cadreEIRL.estEIRL ? false : true;

// Cadres 2B - EIRL

formFields['estEIRL']                                                                   = $p0pl.cadreEIRLGroup.cadreEIRL.estEIRL ? true : false;

// Cadres 3 - Identité

formFields['personneLiee_personnePhysique_nomNaissance']                                = identite.personneLieePersonnePhysiqueNomNaissance;
formFields['personneLiee_personnePhysique_nomUsage']                                    = identite.personneLieePersonnePhysiqueNomUsage;

var prenoms=[];
for ( i = 0; i < identite.personneLieePPPrenom.size() ; i++ ){prenoms.push(identite.personneLieePPPrenom[i]);}                            
formFields['personneLiee_personnePhysique_prenom1']                                      = prenoms.toString();



formFields['personneLiee_personnePhysique_pseudonyme']                                  = identite.personneLieePersonnePhysiquePseudonyme;
formFields['personneLiee_personnePhysique_nationalite']                                 = identite.personneLieePersonnePhysiqueNationalite;
formFields['personneLiee_personnePhysique_civiliteMasculin']                            = Value('id').of(identite.civilite).eq('PersonneLieePersonnePhysiqueCiviliteMasculin') ? true : false;
formFields['personneLiee_personnePhysique_civiliteFeminin']                             = Value('id').of(identite.civilite).eq('PersonneLieePersonnePhysiqueCiviliteFeminin') ? true : false;

if(identite.personneLieePersonnePhysiqueDateNaissance != null) {
	var dateTmp = new Date(parseInt(identite.personneLieePersonnePhysiqueDateNaissance.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['personneLiee_personnePhysique_dateNaissance'] = date;
}
formFields['personneLiee_personnePhysique_lieuNaissanceDepartement']                    = identite.personneLieePPLieuNaissanceDepartement != null ? identite.personneLieePPLieuNaissanceDepartement.getId() : '';
formFields['personneLiee_personnePhysique_lieuNaissanceCommune']                        = identite.personneLieePPLieuNaissanceCommune != null ? identite.personneLieePPLieuNaissanceCommune : identite.personneLieePPLieuNaissanceVille;
formFields['personneLiee_personnePhysique_lieuNaissancePays']                           = Value('id').of(identite.personneLieePPLieuNaissancePays).eq('FRXXXXX') ? '' : identite.personneLieePPLieuNaissancePays;

var adresse = $p0pl.cadre1IdentiteGroup.cadre1AdresseDeclarant;

formFields['personneLiee_adresse_voie1']                                              = (adresse.personneLieeAdresseNumeroVoie != null ? adresse.personneLieeAdresseNumeroVoie : '') 
																						+ ' ' + (adresse.personneLieeAdresseIndiceVoie != null ? adresse.personneLieeAdresseIndiceVoie : '') 
																						+ ' ' + (adresse.personneLieeAdresseTypeVoie != null ? adresse.personneLieeAdresseTypeVoie : '') 
																						+ ' ' + adresse.personneLieeAdresseNomVoie
																						+ ' ' + (adresse.personneLieeAdresseComplementVoie != null ? adresse.personneLieeAdresseComplementVoie : '') 
																						+ ' ' + (adresse.personneLieeAdresseDistriutionSpecialeVoie != null ? adresse.personneLieeAdresseDistriutionSpecialeVoie : '');
formFields['personneLiee_adresse_codePostal']                                           = adresse.personneLieeAdresseCodePostal;
formFields['personneLiee_adresse_commune']                                              = Value('id').of(adresse.paysAdresseDeclarant).eq('FRXXXXX') ? adresse.personneLieeAdresseCommune : adresse.personneLieeAdresseVille;
formFields['personneLiee_adresse_pays']                                                 = Value('id').of(adresse.paysAdresseDeclarant).eq('FRXXXXX') ? '' : adresse.paysAdresseDeclarant;
formFields['personneLiee_adresse_communeAncienne']                                      = adresse.personneLieeAdresseCommuneAncienne != null ? adresse.personneLieeAdresseCommuneAncienne : '';
																					

// Cadre 4 - Statut Conjoint

var conjoint = $p0pl.cadre2conjointGroup.cadre2Conjoint;

formFields['personneLiee_conjointActiviteEntreprise_oui']                   			= conjoint.conjointRole ? true : false;
formFields['personneLiee_conjointActiviteEntreprise_non']                  				= conjoint.conjointRole ? false : true;
if (conjoint.conjointRole) {
formFields['personneLiee_conjointCollaborateurSalarie_collaborateur']                   = Value('id').of(conjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieCollaborateur') ? true : false;
formFields['personneLiee_conjointCollaborateurSalarie_salarie']                         = Value('id').of(conjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieSalarie') ? true : false;
formFields['personneLiee_personnePhysique_nomNaissance_conjointPacse']                  = conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueNomNaissanceConjointPacse;
formFields['personneLiee_personnePhysique_nomUsage_conjointPacse']                      = conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueNomUsageConjointPacse;
var prenoms=[];
for ( i = 0; i < conjoint.cadre2InfosConjoint.personneLieePersonnePhysiquePrenom1ConjointPacse.size() ; i++ ){prenoms.push(conjoint.cadre2InfosConjoint.personneLieePersonnePhysiquePrenom1ConjointPacse[i]);}                            
formFields['personneLiee_personnePhysique_prenom1_conjointPacse']                                      = prenoms.toString();
formFields['personneLiee_personnePhysique_nationalite_conjointPacse']                   = conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueNationaliteConjointPacse;
if(conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueDateNaissanceConjointPacse != null) {
	var dateTmp = new Date(parseInt(conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueDateNaissanceConjointPacse.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['personneLiee_personnePhysique_dateNaissance_conjointPacse'] = date;
}
formFields['personneLiee_personnePhysique_lieuNaissanceDepartement_conjointPacse']  = (conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueLieuNaissanceDepartementConjointPacse != null ? conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueLieuNaissanceDepartementConjointPacse.getId() : '');
formFields['personneLiee_personnePhysique_lieuNaissanceCommune_conjointPacse']      = conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueLieuNaissanceCommuneConjointPacse != null ? conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueLieuNaissanceCommuneConjointPacse : conjoint.cadre2InfosConjoint.personneLieePPLieuNaissanceVilleConjointPacse;
formFields['personneLiee_personnePhysique_lieuNaissancePays_conjointPacse']         = conjoint.cadre2InfosConjoint.personneLieePersonnePhysiquePaysNaissanceConjointPacse;
formFields['adresseConjointVoie']                                       	            = conjoint.cadre2InfosConjoint.adresseConjointDifferente ? ((conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointNumeroVoie != null ? conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointNumeroVoie : '')
																						+ '  ' + (conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointIndiceVoie != null ? conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointIndiceVoie : '')
																						+ '  ' + (conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointTypeVoie  != null ? conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointTypeVoie : '')
																						+ '  ' + (conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointNomVoie  != null ? conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointNomVoie : '')
																						+ '  ' + (conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointComplementVoie != null ? conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointComplementVoie :'')
																						+ '  ' + (conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null ? conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie :'')
																						) : '';
formFields['adresseConjointCodePostal']                                                 = conjoint.cadre2InfosConjoint.adresseConjointDifferente ? conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointCodePostal : '';
formFields['adresseConjointCommune']                                                    = conjoint.cadre2InfosConjoint.adresseConjointDifferente ? conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointCommune : '';
}

//Cadre 5 - Adresse de l'établissement

var adressePro = $p0pl.cadre4AdresseActiviteGroup.cadre4AdresseActivite;

formFields['entreprise_adresseEntreprise_domicile']                                     = Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? true : false;
formFields['entreprise_adresseEntreprise_adressePro']                                   = Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? false : true;

if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')) {
formFields['entrepriseLiee_adresse_voie']                                              =(adressePro.cadre4AdresseProfessionnelle.etablissementAdresseNumeroVoie != null ? adressePro.cadre4AdresseProfessionnelle.etablissementAdresseNumeroVoie : '')
																				            + ' ' + (adressePro.cadre4AdresseProfessionnelle.etablissementAdresseIndiceVoie != null ? adressePro.cadre4AdresseProfessionnelle.etablissementAdresseIndiceVoie : '')
																							+ ' ' + (adressePro.cadre4AdresseProfessionnelle.etablissementAdresseTypeVoie != null ? adressePro.cadre4AdresseProfessionnelle.etablissementAdresseTypeVoie : '')
																				            + ' ' + (adressePro.cadre4AdresseProfessionnelle.etablissementAdresseNomVoie != null ? adressePro.cadre4AdresseProfessionnelle.etablissementAdresseNomVoie : '')
																							+ ' ' + (adressePro.cadre4AdresseProfessionnelle.etablissementAdresseComplementVoie != null ? adressePro.cadre4AdresseProfessionnelle.etablissementAdresseComplementVoie : '')
																				            + ' ' + (adressePro.cadre4AdresseProfessionnelle.etablissementAdresseDistriutionSpecialeVoie != null ? adressePro.cadre4AdresseProfessionnelle.etablissementAdresseDistriutionSpecialeVoie : '');
formFields['entrepriseLiee_adresse_codePostal']                                          = adressePro.cadre4AdresseProfessionnelle.etablissementAdresseCodePostal;
formFields['entrepriseLiee_adresse_commune']                                             = adressePro.cadre4AdresseProfessionnelle.etablissementAdresseCommune;
formFields['entrepriseLiee_adresse_communeAncienne']                                     = adressePro.cadre4AdresseProfessionnelle.etablissementAdresseCommuneAncienne != null ? adressePro.cadre4AdresseProfessionnelle.etablissementAdresseCommuneAncienne : '';
}

// Cadre 6 - Activité

var activite = $p0pl.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite

if(activite.etablissementDateDebutActivite != null) {
	var dateTmp = new Date(parseInt(activite.etablissementDateDebutActivite.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_dateDebutActivite'] = date;
}	
formFields['etablissement_activitesAutres']                                             = activite.etablissementActivitesAutres;
formFields['etablissement_activitesPrincipales2']                                       = activite.etablissementActivitesPrincipales2;
formFields['entreprise_activitePermanenteSaisonniere_saisonniere']                      = Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonniereSaisonniere') ? true : false;
formFields['activiteSaisonnierePeriodeDu']                                              = Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonniereSaisonniere') ? "Du" : '';
formFields['activiteSaisonnierePeriodeDebut']                                           = Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonniereSaisonniere') ? $p0pl.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.activiteSaisonnierePeriode.from : '';
formFields['activiteSaisonnierePeriodeAu']                                              = Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonniereSaisonniere') ? "au" : '';
formFields['activiteSaisonnierePeriodeFin']                                             = Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonniereSaisonniere') ? $p0pl.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.activiteSaisonnierePeriode.to : '';
formFields['activiteSaisonnierePeriode2Du']                                             = activite.autrePeriode ? "Du" : '';
formFields['activiteSaisonnierePeriode2Debut']                                          = activite.autrePeriode ? $p0pl.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.activiteSaisonnierePeriode2.from : '';
formFields['activiteSaisonnierePeriode2Au']                                             = activite.autrePeriode ? "au" : '';
formFields['activiteSaisonnierePeriode2Fin']                                            = activite.autrePeriode ? $p0pl.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.activiteSaisonnierePeriode2.to : '';
formFields['etablissment_nonSedentariteQualite_nonSedentaire']                          = activite.etablissmentNonSedentariteQualiteNonSedentaire ? true : false;

if (activite.activiteSante) {
formFields['santeTitulaire']                                               = Value('id').of(activite.activiteSanteStatut).eq('santeStatutTitulaire') ? true : false;
formFields['santeRemplacant']                                              = Value('id').of(activite.activiteSanteStatut).eq('santeStatutRemplacant') ? true : false;
formFields['santeRemplacantOffreSimplifiee']                               = Value('id').of(activite.activiteSanteStatut).eq('santeStatutRemplacantSimplifie') ? true : false;
formFields['chirurgienDentiste']                                           = Value('id').of(activite.activiteSanteSpecialite).eq('chirurgienDentiste') ? true : false;
formFields['infirmier']                                                    = Value('id').of(activite.activiteSanteSpecialite).eq('infirmier') ? true : false;
formFields['medecinGeneraliste']                                           = Value('id').of(activite.activiteSanteSpecialite).eq('medecinSect1Gen') ? true : false;
formFields['medecinSpecialiste']                                           = Value('id').of(activite.activiteSanteSpecialite).eq('medecinSect1Spe') ? true : false;
formFields['medecinTI']                                                    = Value('id').of(activite.activiteSanteSpecialite).eq('medecinSect2TI') ? true : false;
formFields['medecinPAMC']                                                  = Value('id').of(activite.activiteSanteSpecialite).eq('medecinSect2PAMC') ? true : false;
formFields['masseurKine']                                                  = Value('id').of(activite.activiteSanteSpecialite).eq('masseurKine') ? true : false;
formFields['orthophoniste']                                                = Value('id').of(activite.activiteSanteSpecialite).eq('orthophoniste') ? true : false;
formFields['orthoptiste']                                                  = Value('id').of(activite.activiteSanteSpecialite).eq('orthoptiste') ? true : false;
formFields['podologueTI']                                                  = Value('id').of(activite.activiteSanteSpecialite).eq('podologueTI') ? true : false;
formFields['podologuePAMC']                                                = Value('id').of(activite.activiteSanteSpecialite).eq('podologuePAMC') ? true : false;
formFields['sageFemme']                                                    = Value('id').of(activite.activiteSanteSpecialite).eq('sageFemme') ? true : false;
formFields['santeAutre']                                                   = Value('id').of(activite.activiteSanteSpecialite).eq('santeAutre') ? true : false;
if (Value('id').of(activite.activiteSanteSpecialite).eq('santeAutre')) {
formFields['santeAutreLibelle']                                            = activite.activiteSanteAutre;
}	
}	

// Cadre 7 - Origine du fonds

formFields['etablissement_origineFonds_creation']                                       = Value('id').of(adressePro.cadre4OrigineFonds.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation') ? true : (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? true : false);
formFields['etablissement_origineFonds_reprise']                                        = Value('id').of(adressePro.cadre4OrigineFonds.etablisementOrigineFonds).eq('EtablissementOrigineFondsReprise') ? true : false;
formFields['entrepriseLiee_siren_precedentExploitant']                                  = adressePro.cadre4OrigineFonds.precedentExploitant.entrepriseLieeSirenPrecedentExploitant != null ? adressePro.cadre4OrigineFonds.precedentExploitant.entrepriseLieeSirenPrecedentExploitant.split(' ').join('') : '';
formFields['entrepriseLiee_entreprisePP_nomNaissance_precedentExploitant']              = adressePro.cadre4OrigineFonds.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant;
formFields['entrepriseLiee_entreprisePP_nomUsage_loueurMandantDuFonds']                 = adressePro.cadre4OrigineFonds.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant;
formFields['entrepriseLiee_entreprisePP_prenom1_loueurMandantDuFonds']                  = adressePro.cadre4OrigineFonds.precedentExploitant.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant;
formFields['entrepriseLiee_entreprisePM_denomination_loueurMandantDuFonds']             = adressePro.cadre4OrigineFonds.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant;

//Cadres 8 - Effectif salarié

formFields['etablissement_effectifSalariePresence_non']                                 = activite.etablissementEffectifSalariePresenceOui ? false : true;
formFields['etablissement_effectifSalariePresence_oui']                                 = activite.etablissementEffectifSalariePresenceOui ? true : false;
formFields['etablissement_effectifSalarieNombre']                                       = activite.etablissementEffectifSalarieNombre;
formFields['etablissement_effectifSalarieEmbauchePremierSalarie_oui']                   = activite.etablissementEffectifSalarieEmbauchePremierSalarieOui ? true : false;
formFields['etablissement_effectifSalarieEmbauchePremierSalarie_non']                   = activite.etablissementEffectifSalariePresenceOui ? (activite.etablissementEffectifSalarieEmbauchePremierSalarieOui ? false : true) : false;

// Cadre 9 - Volet Social

var social = $p0pl.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale;

var nirDeclarant = social.voletSocialNumeroSecuriteSociale;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFields['voletSocial_numeroSecuriteSociale']                = nirDeclarant.substring(0, 13);
    formFields['voletSocial_numeroSecuriteSocialeCle']             = nirDeclarant.substring(13, 15);
}
if (social.ressortissantHorsUE) {
formFields['voletSocial_titreSejour_numero']                                            = social.voletSocialTitreSejourNumero;
formFields['voletSocial_titreSejour_lieuDelivranceCommune']                             = social.voletSocialTitreSejourLieuDelivranceCommune;
if(social.voletSocialTitreSejourDateExpiration != null) {
	var dateTmp = new Date(parseInt(social.voletSocialTitreSejourDateExpiration.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['voletSocial_titreSejour_dateExpiration'] = date;
}}
formFields['voletSocial_activiteAutreQueDeclareeStatut_oui']                            = social.voletSocialActiviteAutreQueDeclareeStatutOui ? true : false;
formFields['voletSocial_activiteAutreQueDeclareeStatut_non']                            = social.voletSocialActiviteAutreQueDeclareeStatutOui ? false : true;
formFields['voletSocial_activiteAutreQueDeclareeStatut_salarie']                        = Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutSalarie') ? true : false;
formFields['voletSocial_activiteAutreQueDeclareeStatut_salarieAgricole']                = Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutSalarieAgricole') ? true : false;
formFields['voletSocial_activiteAutreQueDeclareeStatut_retraitePensionne']              = Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutRetraitePensionne') ? true : false;
formFields['voletSocial_activiteAutreQueDeclareeStatut_cocheAutre'] 				    = Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutCocheAutre') ? true : false;
formFields['voletSocial_activiteAutreQueDeclareeStatutAutre']                           = social.voletSocialActiviteAutreQueDeclareeStatutAutre;
formFields['voletSocial_regimePraticien_oui']                                           = social.voletSocialRegimePraticienOuiNon != null ? (Value('id').of(social.voletSocialRegimePraticienOuiNon).eq('voletSocialRegimePraticienOui') ? true : false) : false;
formFields['voletSocial_regimePraticien_non']                                           = social.voletSocialRegimePraticienOuiNon != null ? (Value('id').of(social.voletSocialRegimePraticienOuiNon).eq('voletSocialRegimePraticienNon') ? true : false) : false;
formFields['marinProfessionnel']                                                        = social.voletSocialRegimeMarin != null ? (social.voletSocialRegimeMarin ? true : false) : false;
formFields['optionPrecompte']                                                           = social.voletSocialOptionPrecompte != null ? (social.voletSocialOptionPrecompte ? true : false) : false;
if (activite.activiteSante) {
formFields['numeroPraticienADELI']                                                      = social.numeroADELI;
}
formFields['voletSocial_conjointCouvertAssuranceMaladie_oui']                           = Value('id').of($p0pl.cadre2conjointGroup.cadre2Conjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieCollaborateur') ? (conjoint.cadre2InfosConjoint.voletSocialConjointCouvertAssuranceMaladieOui ? true : false) : false;
formFields['voletSocial_conjointCouvertAssuranceMaladie_non']                           = Value('id').of($p0pl.cadre2conjointGroup.cadre2Conjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieCollaborateur') ? (conjoint.cadre2InfosConjoint.voletSocialConjointCouvertAssuranceMaladieOui ? false : true) : false;

var nirConjoint = conjoint.cadre2InfosConjoint.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(nirConjoint != null) {
    nirConjoint = nirConjoint.replace(/ /g, "");
    formFields['voletSocial_conjointCollaborateurNumeroSecuriteSociale']                = nirConjoint.substring(0, 13);
    formFields['voletSocial_conjointCollaborateurNumeroSecuriteSocialeCle']             = nirConjoint.substring(13, 15);
}

// Cadre 10 - Régime fiscal

var optionsFiscales = $p0pl.cadre9optFiscHorsEIRLGroup.cadre9optFiscHorsEIRL;
var declarationAffectation = $p0pl.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine;

if (declarationAffectation.declarationPatrimoine.objetPartiel or not $p0pl.cadreEIRLGroup.cadreEIRL.estEIRL) {
if (Value('id').of(optionsFiscales.impositionBenefices.choixRegime).eq('regimeBNC')) {	
formFields['regimeFiscal_regimeImpositionBeneficesOptionsParticulieres_optionCreanceDette'] = optionsFiscales.impositionBenefices.regimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionCreanceDette ? true : false;
formFields['regimeFiscal_regimeImpositionBenefices_dcBNC']                                  = Value('id').of(optionsFiscales.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('regimeFiscalRegimeImpositionBeneficesDcBNC') ? true : false;
formFields['regimeFiscal_regimeImpositionBenefices_rsBNC']                                  = Value('id').of(optionsFiscales.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('regimeFiscalRegimeImpositionBeneficesRsBNC') ? true : false;
formFields['regimeFiscal_regimeImpositionTVA_rfTVA']                                        = Value('id').of(optionsFiscales.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('regimeFiscalRegimeImpositionBeneficesRsBNC') ? true : (Value('id').of(optionsFiscales.optionsTVA.regimeTVA1).eq('regimeFiscalRegimeImpositionTVARfTVA') ? true : false);
formFields['regimeFiscal_regimeImpositionTVA_rsTVA']                                        = Value('id').of(optionsFiscales.optionsTVA.regimeTVA1).eq('regimeFiscalRegimeImpositionTVARsTVA') ? true : false;
formFields['regimeFiscal_regimeImpositionTVA_rnTVA']                                        = Value('id').of(optionsFiscales.optionsTVA.regimeTVA1).eq('regimeFiscalRegimeImpositionTVARnTVA') ? true : false;
formFields['regimeFiscal_regimeImpositionTVAOptionsParticulieres1']                         = optionsFiscales.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres1 ? true : false;
}
if (Value('id').of(optionsFiscales.impositionBenefices.choixRegime).eq('regimeBIC')) {
formFields['regimeFiscal_regimeImpositionBenefices_mBIC'] 									= Value('id').of(optionsFiscales.impositionBenefices.regimeFiscalRegimeImpositionBeneficeBIC).eq('regimeFiscalRegimeImpositionBeneficesMBIC') ? true : false;
formFields['regimeFiscal_regimeImpositionBenefices_rsBIC']                                  = Value('id').of(optionsFiscales.impositionBenefices.regimeFiscalRegimeImpositionBeneficeBIC).eq('regimeFiscalRegimeImpositionBeneficesRsBIC') ? true : false;
formFields['regimeFiscal_regimeImpositionBenefices_rnBIC']                                  = Value('id').of(optionsFiscales.impositionBenefices.regimeFiscalRegimeImpositionBeneficeBIC).eq('regimeFiscalRegimeImpositionBeneficesRnBIC') ? true : false;
if (optionsFiscales.impositionBenefices.regimeFiscalDateClotureExerciceComptable !== null) {
    var dateTmp = new Date(parseInt(optionsFiscales.impositionBenefices.regimeFiscalDateClotureExerciceComptable.getTimeInMillis()));
    var dateClotureEc = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateClotureEc = dateClotureEc.concat(pad(month.toString()));
    formFields['regimeFiscal_dateClotureExerciceComptable']          = dateClotureEc;
}
formFields['regimeFiscal_regimeImpositionTVA_rfTVABIC']                                     = Value('id').of(optionsFiscales.impositionBenefices.regimeFiscalRegimeImpositionBeneficeBIC).eq('regimeFiscalRegimeImpositionBeneficesMBIC') ? true : ((Value('id').of(optionsFiscales.optionsTVA.regimeTVA2).eq('regimeFiscalRegimeImpositionTVARfTVA') or Value('id').of(optionsFiscales.optionsTVA.regimeTVA3).eq('regimeFiscalRegimeImpositionTVARfTVA')) ? true : false);
formFields['regimeFiscal_regimeImpositionTVA_rsTVABIC']                                     = Value('id').of(optionsFiscales.optionsTVA.regimeTVA2).eq('regimeFiscalRegimeImpositionTVARsTVA') ? true : false;
formFields['regimeFiscal_regimeImpositionTVA_rnTVABIC']                                     = Value('id').of(optionsFiscales.optionsTVA.regimeTVA3).eq('regimeFiscalRegimeImpositionTVARnTVA') ? true : false;
formFields['regimeFiscal_regimeImpositionTVA_mrTVABIC']                                     = Value('id').of(optionsFiscales.optionsTVA.regimeTVA2).eq('regimeFiscalRegimeImpositionTVAMrTVA') ? true : false;
formFields['regimeFiscal_regimeImpositionTVAOptionsParticulieres1BIC']                      = optionsFiscales.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres1 ? true : false;
formFields['regimeFiscal_regimeImpositionTVAOptionsParticulieres2']                         = optionsFiscales.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres2 ? true : false;
}
}

//Cadre 11 - Observations

var correspondance = $p0pl.cadre10RensCompGroup.cadre10RensComp ;

formFields['formalite_observations']                                                    = correspondance.formaliteObservations;

// Cadre 12 - Adresse de correspondance

formFields['formalite_DeclarerCadre']                                                   = (Value('id').of(correspondance.adresseCorrespond).eq('domi') or Value('id').of(correspondance.adresseCorrespond).eq('prof')) ? true : false;
formFields['formalite_DeclarerCadreNumero']                                             = Value('id').of(correspondance.adresseCorrespond).eq('domi') ? "3" : (Value('id').of(correspondance.adresseCorrespond).eq('prof') ? "5" : ' ');
formFields['formalite_correspondanceAdresse']                                           = Value('id').of(correspondance.adresseCorrespond).eq('autre') ? true : false;
formFields['formalite_correspondanceAdresse_nomVoie']                                   = Value('id').of(correspondance.adresseCorrespond).eq('autre') ?
																						((correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance != null ? correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance : '')
																						+ ' ' +	(correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance : '')
																				        + ' ' + (correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance : '')
																				        + ' ' + (correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance : '')) : '';
formFields['formalite_correspondanceAdresse_complementVoie']                            = (correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance  != null ? correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance :'')
                                                                                        + ' ' + (correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance != null ? correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance : '') 
																						+ ' ' + (correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance != null ? correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance : '');
formFields['formalite_correspondanceAdresse_codePostal']                                = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal;
formFields['formalite_correspondanceAdresse_commune']									= correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune;
formFields['telephone1']                                                                = correspondance.infosSup.formaliteTelephone1 != null ? correspondance.infosSup.formaliteTelephone1 : '';
formFields['telephone2']                                                                = correspondance.infosSup.formaliteTelephone2;
formFields['fax_mail']                                                    = correspondance.infosSup.formaliteFaxCourriel != null ? correspondance.infosSup.formaliteFaxCourriel : correspondance.infosSup.telecopie;

// Cadre 13 - Non diffusion

var signataire = $p0pl.cadre11SignatureGroup.cadre11Signature ;

formFields['formalite_nonDiffusionInformation']                                         = signataire.formaliteNonDiffusionInformation ? true : false;

// Cadres 20 - Signature

formFields['formalite_signataireQualite_declarant']                                     = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFields['formalite_signataireQualite_mandataire']                                    = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFields['formalite_signataireNom']										   = signataire.adresseMandataire.nomPrenomDenominationMandataire;	
formFields['formalite_signataireAdresse_voie']                                 = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire + ' ' : '') 
																				+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																				+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																				+ ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																				+ ' ' + (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '')
																				+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '');
formFields['formalite_signataire_adresseCP']                                    = signataire.adresseMandataire.dataCodePostalMandataire;
formFields['formalite_signataire_adresseCommune']                               = signataire.adresseMandataire.villeAdresseMandataire;
}
formFields['formalite_signatureLieu']                                                   = signataire.formaliteSignatureLieu;
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['formalite_signatureDate'] = date;
}
formFields['formalite_peirl_oui']                                                       = $p0pl.cadreEIRLGroup.cadreEIRL.estEIRL ? true : false;
formFields['formalite_peirl_non']                                                       = $p0pl.cadreEIRLGroup.cadreEIRL.estEIRL ? false : true;

// A conditionner selon l'activité
formFields['formalite_nb_intercalaires']                                                = "0";
formFields['signature']                                                                 = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce";

// Intercalaire PEIRL

//Cadre 1 - Déclaration ou modification d'affectation de patrimoine

formFields['formulaire_dependance_P0PL']                                                    = true;
formFields['formulaire_dependance_P0PLME']                                                  = false;
formFields['formulaire_dependance_P2PL']                                                    = false;
formFields['formulaire_dependance_P4PL']                                                    = false;
formFields['formulaire_dependance_AC0']                                                     = false;
formFields['formulaire_dependance_AC2']                                                     = false;
formFields['formulaire_dependance_AC4']     												= false;
formFields['declaration_initiale']                                                          = $p0pl.cadreEIRLGroup.cadreEIRL.estEIRL ? true : false;
formFields['declaration_modification']                                                      = false;

// Cadre 2 - Rappel d'identification

formFields['eirl_nomNaissance']                                                  = identite.personneLieePersonnePhysiqueNomNaissance;
formFields['eirl_nomUsage']                                                      = identite.personneLieePersonnePhysiqueNomUsage;
var prenoms=[];
for ( i = 0; i < identite.personneLieePPPrenom.size() ; i++ ){prenoms.push(identite.personneLieePPPrenom[i]);}                            
formFields['eirl_prenom']                                                   = prenoms.toString();

// Cadre 3 - Déclaration d'affectation de patrimoine

formFields['eirl_statutEIRL_declarationInitialeSansDepot']                                  = Value('id').of(declarationAffectation.eirlDepot).eq('eirlSansDepot') ? true : false;
formFields['eirl_statutEIRL_declarationInitialeAvecDepot']                                  = Value('id').of(declarationAffectation.eirlDepot).eq('eirlAvecDepot') ? true : false;
formFields['eirl_statutEIRL_reprise']                                                       = Value('id').of(declarationAffectation.eirlStatut).eq('EirlStatutEIRLReprise') ? true : false;
formFields['eirl_denomination']                                                             = declarationAffectation.declarationPatrimoine.eirlDenomination;
if (declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable !== null) {
    var dateTmp = new Date(parseInt(declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable.getTimeInMillis()));
    var dateClotureEc = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateClotureEc = dateClotureEc.concat(pad(month.toString()));
    formFields['eirl_dateClotureExerciceComptable']          = dateClotureEc;
}
formFields['eirl_objet']                                                                    = declarationAffectation.declarationPatrimoine.objetPartiel ? declarationAffectation.declarationPatrimoine.eirlObjet : ($p0pl.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite.etablissementActivitesAutres);
formFields['eirl_precedentEIRLDenomination']                                                = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLDenomination;
formFields['eirl_precedentEIRLRegistre_rseirl']                                             = Value('id').of(declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLRegistre).eq('EirlPrecedentEIRLRegistreRseirl') ? true : false;
formFields['eirl_precedentEIRLRegistre_rsac']                                               = Value('id').of(declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLRegistre).eq('EirlPrecedentEIRLRegistreRsac') ? true : false;
formFields['eirl_precedentEIRLLieuImmatriculation']                                         = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLLieuImmatriculation;
formFields['eirl_precedentEIRLSIREN']                                                       = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN != null ? (declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN.split(' ').join('')) : '';

// Cadre 7 - Options fiscales

formFields['eirl_regimeFiscal_regimeImpositionBenefices_rsBNC']                                  = Value('id').of(declarationAffectation.cadre3OptionsFiscalesEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesRsBNC') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_dcBNC']                                  = Value('id').of(declarationAffectation.cadre3OptionsFiscalesEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesDcBNC') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBeneficesOptionsParticulieres_optionCreanceDette'] = declarationAffectation.cadre3OptionsFiscalesEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionCreanceDette ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBeneficesOptionsParticulieres_optionIS']           = Value('id').of(declarationAffectation.cadre3OptionsFiscalesEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionIS') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rsIS']                                   = Value('id').of(declarationAffectation.cadre3OptionsFiscalesEIRL.impositionBeneficesEIRL.regimeFiscalIS).eq('RegimeFiscalRegimeImpositionBeneficesRsIS') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rnIS']                                   = Value('id').of(declarationAffectation.cadre3OptionsFiscalesEIRL.impositionBeneficesEIRL.regimeFiscalIS).eq('RegimeFiscalRegimeImpositionBeneficesRnIS') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVA_rfTVA']                                        = Value('id').of(declarationAffectation.cadre3OptionsFiscalesEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesRsBNC') ? true : (Value('id').of(declarationAffectation.cadre3OptionsFiscalesEIRL.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARfTVA') ? true : false);
formFields['eirl_regimeFiscal_regimeImpositionTVA_rsTVA']                                        = Value('id').of(declarationAffectation.cadre3OptionsFiscalesEIRL.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARsTVA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVA_rnTVA']                                        = Value('id').of(declarationAffectation.cadre3OptionsFiscalesEIRL.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARnTVA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVAOptionsParticulieres1']                         = declarationAffectation.cadre3OptionsFiscalesEIRL.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres1DC ? true : false;


/*
 * Création du dossier avec volet social : ajout du cerfa avec volet social
 */
var cerfaDoc1 = nash.doc //
	.load('models/cerfa_11768-08_p0pl_avec_volet_social.pdf') //
	.apply(formFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));

/*
 * Création du dossier sans volet social : ajout du cerfa sans volet social
 */
 
 var cerfaDoc2 = nash.doc //
	.load('models/cerfa_11768-08_p0pl_sans_volet_social.pdf') //
	.apply (formFields);
	cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));



/*
 * Ajout de l'intercalaire PEIRL PL avec option fiscale
 */
if ($p0pl.cadreEIRLGroup.cadreEIRL.estEIRL == true)
{
	var peirlMEDoc1 = nash.doc //
		.load('models/cerfa1_14218-03_peirl_PL_AC_avec_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(peirlMEDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire PEIRL PL sans option fiscale
 */
if ($p0pl.cadreEIRLGroup.cadreEIRL.estEIRL == true)
{
	var peirlMEDoc2 = nash.doc //
		.load('models/cerfa1_14218-03_peirl_PL_AC_sans_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(peirlMEDoc2.save('cerfa.pdf'));
}


/*
 * Ajout des PJs
 */

// PJ Déclarant

var pj=$p0pl.cadre11SignatureGroup.cadre11Signature.soussigne;
var pjUser = [];
var metas = [];

function pushPjPreview(fld) {
	fld.forEach(function (elm) {
        pjUser.push(elm);
		metas.push({'name':'document', 'value': '/'+elm.getAbsolutePath()});
    });
}


if(Value('id').of(pj).contains('FormaliteSignataireQualiteDeclarant')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantSignataire);
}

if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantNonSignataire);
} 

// PJ conjoint

var pj=$p0pl.cadre2conjointGroup.cadre2Conjoint.statutConjointCollaborateur ;
if(Value('id').of(pj).contains('PersonneLieeConjointCollaborateurSalarieCollaborateur') or Value('id').of(pj).contains('PersonneLieeConjointCollaborateurSalarieSalarie')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjChoixStatutConjoint);
}

// PJ Mandataire

if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire);
}

if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire') and $p0pl.cadreEIRLGroup.cadreEIRL.estEIRL) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPouvoir);
}


// PJ Eirl

var pj=$p0pl.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine;
if($p0pl.cadreEIRLGroup.cadreEIRL.estEIRL) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjAutorisationMineur);
}

if($p0pl.cadreEIRLGroup.cadreEIRL.estEIRL and not Value('id').of(pj.eirlDepot).eq('eirlSansDepot')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAP);
}

var pj=$p0pl.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine;
if(Value('id').of(pj.cadreEIRLInformationsComplementaires.contenuDAP).contains('bienImmo') or Value('id').of(pj.cadreEIRLInformationsComplementairesBis.contenuDAPBis).contains('bienImmo')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAPActeNotarie);
}

if(Value('id').of(pj.cadreEIRLInformationsComplementaires.contenuDAP).contains('bienCommunIndivis') or Value('id').of(pj.cadreEIRLInformationsComplementairesBis.contenuDAPBis).contains('bienCommunIndivis')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAPAccordTiers);
}
	

/*
 * Enregistrement du fichier (en mémoire)
 */

var finalDoc = cerfaDoc1.save('PE_p0pl_Creation.pdf');
// Remove old metas before insert
var metasToDelete = [];
var recordMetas = nash.record.meta() != null ? nash.record.meta().metas : null;
var recordMetasSize = recordMetas != null ? recordMetas.size() : 0;

for (var i = 0; i < recordMetasSize; i++) {
   if (recordMetas.get(i).name == 'document' && !recordMetas.get(i).value.contains("proxy") ) {
       metasToDelete.push({'name':'document', 'value': recordMetas.get(i).value});
   }
}
nash.record.removeMeta(metasToDelete);

nash.record.meta(metas);

var data = [ spec.createData({
    id : 'record',
    label : 'Déclaration de début d\'activite libérale hors micro-entrepreneur.',
    description : 'Voici le formulaire obtenu à partir des données saisies.',
    type : 'FileReadOnly',
    value : [ finalDoc ]
}),  spec.createData({
    id : 'attachments',
    label : 'Pièces jointes',
    description : 'Pièces jointes ajoutées au formulaire.',
    type : 'FileReadOnly',
    value : pjUser
}) ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Déclaration de début d\'activite libérale hors micro-entrepreneur',
    groups : groups
});