//Fichier de mapping des champs Regent pour :
//Version : V2008.11 
//Evenements : 05P,  
//Fichier généré le : 2018-07-16_103631 

function pad(s) { return (s < 10) ? '0' + s : s; }
function sanitize(s){return s.replaceAll("’","'").replaceAll("–","-").replaceAll("­","");}

var regentVersion = "V2008.11";  // (V2008.11, V2016.02) 
var eventRegent = $p0pl.cadre1IdentiteGroup.cadre1Identite.formaliteEvenementOui ? "05P" : "01P"; //[] 

// Valeurs à compléter par Directory  
var authorityType = "CFE"; // (CFE, TDR) 
var authorityId = _INPUT_.dataGroup.codeEDI; // (code EDI) 


//Ajout du type de la deuxième autorité
var authorityType2 = "TDR"; // (CFE, TDR) 
//Ajout du code de la deuxième autorité
var authorityId2 = _INPUT_.dataGroup.codeEDI2; // (code EDI) 

var regEx = new RegExp('[0-9]{5}'); 
var identite = $p0pl.cadre1IdentiteGroup.cadre1Identite;
var activite = $p0pl.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite;
var adresse = $p0pl.cadre1IdentiteGroup.cadre1AdresseDeclarant;
var adressePro = $p0pl.cadre4AdresseActiviteGroup.cadre4AdresseActivite;
var origine = $p0pl.cadre4AdresseActiviteGroup.cadre4AdresseActivite.cadre4OrigineFonds;
var social = $p0pl.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale;
var socialAyantDroit1 = $p0pl.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.ayantsdroits1;
var socialAyantDroit2 = $p0pl.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.ayantsdroits2;
var socialAyantDroit3 = $p0pl.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.ayantsdroits3;
var socialAyantDroit4 = $p0pl.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.ayantsdroits4;
var socialAyantDroit5 = $p0pl.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.ayantsdroits5;
var socialAyantDroit6 = $p0pl.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.ayantsdroits6;
var socialAyantDroit7 = $p0pl.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.ayantsdroits7;
var socialAyantDroit8 = $p0pl.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.ayantsdroits8;
var socialAyantDroit9 = $p0pl.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.ayantsdroits9;
var socialAyantDroit10 = $p0pl.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.ayantsdroits10;
var socialAyantDroit11 = $p0pl.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.ayantsdroits11;
var socialAyantDroit12 = $p0pl.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale.ayantsdroits12;
var declarationAffectation = $p0pl.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine;
var impotEIRL = $p0pl.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine.cadre3OptionsFiscalesEIRL;
var correspondance = $p0pl.cadre10RensCompGroup.cadre10RensComp;
var signataire = $p0pl.cadre11SignatureGroup.cadre11Signature;
var statutConjoint = $p0pl.cadre2conjointGroup.cadre2Conjoint;
var conjoint = $p0pl.cadre2conjointGroup.cadre2Conjoint.cadre2InfosConjoint;
var eirl = $p0pl.cadreEIRLGroup.cadreEIRL;
var fiscal = $p0pl.cadre9optFiscHorsEIRLGroup.cadre9optFiscHorsEIRL;

var regentFields = {}; 

// A mettre toujours sous "Z1611"
regentFields['/REGENT-XML/Emetteur']="Z1611";

// A récuperer depuis directory
regentFields['/REGENT-XML/Destinataire']= authorityId;

// Complété par destiny
regentFields['/REGENT-XML/DateHeureEmission']= null;
regentFields['/REGENT-XML/VersionMessage']= null;
regentFields['/REGENT-XML/VersionNorme']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Specification/NomService']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Specification/VersionService']= null;

                                  // Groupe GDF : Groupe données de service

// Sous groupe IDF : Identification de la formalité

// Généré par destiny
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C01']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C03']='0';
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C04']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05']= "L";

/*
// A ne générer que pour les artisans (valeur "O" ou "N")
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C06']= null;
*/

// Sous groupe EDF : Evènement déclaré
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.1']= identite.formaliteEvenementOui ? "05P" : "01P";
if(activite.etablissementDateDebutActivite !== null) {
	var dateTemp = new Date(parseInt(activite.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2']= date;
}

// Sous groupe DMF : Destinataire de la formalité

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[1]']= authorityId;
if (authorityId != "" and authorityId2 != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[2]']= authorityId2;
}

//adresse de correspondance
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C36']= correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance != null ? correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance : (identite.personneLieePersonnePhysiqueNomUsage != null ? (identite.personneLieePersonnePhysiqueNomUsage + ' ' + identite.personneLieePPPrenom[0]) : (identite.personneLieePersonnePhysiqueNomNaissance + ' ' + identite.personneLieePPPrenom[0]));;

if (Value('id').of(adresse.paysAdresseDeclarant).eq('FRXXXXX') and Value('id').of(correspondance.adresseCorrespond).eq('domi')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']=adresse.personneLieeAdresseCommune.getId();
}
else if (not Value('id').of(adresse.paysAdresseDeclarant).eq('FRXXXXX') and Value('id').of(correspondance.adresseCorrespond).eq('domi')) {
		var idPays = adresse.paysAdresseDeclarant.getId();
		var result = idPays.match(regEx)[0]; 
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']= result;
}
else if (Value('id').of(correspondance.adresseCorrespond).eq('prof')) {
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']=adressePro.cadre4AdresseProfessionnelle.etablissementAdresseCommune.getId();
}
else if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']=correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune.getId();
}
																	
if ((Value('id').of(correspondance.adresseCorrespond).eq('domi') and adresse.personneLieeAdresseNumeroVoie != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adressePro.cadre4AdresseProfessionnelle.etablissementAdresseNumeroVoie != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.5']= Value('id').of(correspondance.adresseCorrespond).eq('domi') ? adresse.personneLieeAdresseNumeroVoie :
																			(Value('id').of(correspondance.adresseCorrespond).eq('prof') ? adressePro.cadre4AdresseProfessionnelle.etablissementAdresseNumeroVoie : 
																			correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance);
	}
	
if (Value('id').of(correspondance.adresseCorrespond).eq('domi') and adresse.personneLieeAdresseIndiceVoie !== null) {
   var monId1 = Value('id').of(adresse.personneLieeAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId1;  
} 
else if (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adressePro.cadre4AdresseProfessionnelle.etablissementAdresseIndiceVoie !== null) {
   var monId2 = Value('id').of(adressePro.cadre4AdresseProfessionnelle.etablissementAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId2;
}  
else if (correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance !== null) {
   var monId3 = Value('id').of(correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId3;
}

if ((Value('id').of(correspondance.adresseCorrespond).eq('domi') and adresse.personneLieeAdresseDistriutionSpecialeVoie != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adressePro.cadre4AdresseProfessionnelle.etablissementAdresseDistriutionSpecialeVoie != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.7']= Value('id').of(correspondance.adresseCorrespond).eq('domi') ? adresse.personneLieeAdresseDistriutionSpecialeVoie : 
																				(Value('id').of(correspondance.adresseCorrespond).eq('prof') ? adressePro.cadre4AdresseProfessionnelle.etablissementAdresseDistriutionSpecialeVoie : 
																				correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance);
}

if ((Value('id').of(correspondance.adresseCorrespond).eq('domi') and adresse.personneLieeAdresseCodePostal != null and Value('id').of(adresse.paysAdresseDeclarant).eq('FRXXXXX'))
	or (Value('id').of(correspondance.adresseCorrespond).eq('domi') and not Value('id').of(adresse.paysAdresseDeclarant).eq('FRXXXXX'))
	or (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adressePro.cadre4AdresseProfessionnelle.etablissementAdresseCodePostal != null)) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8']= Value('id').of(correspondance.adresseCorrespond).eq('domi') ? (adresse.personneLieeAdresseCodePostal != null ? adresse.personneLieeAdresseCodePostal : ".") : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('prof') ? adressePro.cadre4AdresseProfessionnelle.etablissementAdresseCodePostal : 
																			correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal);
}

/*
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.9']= null;
*/

if ((Value('id').of(correspondance.adresseCorrespond).eq('domi') and adresse.personneLieeAdresseComplementVoie != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adressePro.cadre4AdresseProfessionnelle.etablissementAdresseComplementVoie != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.10']= Value('id').of(correspondance.adresseCorrespond).eq('domi') ? adresse.personneLieeAdresseComplementVoie : 
																				(Value('id').of(correspondance.adresseCorrespond).eq('prof') ? adressePro.cadre4AdresseProfessionnelle.etablissementAdresseComplementVoie : 
																				correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance);
}

if (Value('id').of(correspondance.adresseCorrespond).eq('domi') and adresse.personneLieeAdresseTypeVoie !== null) {
   var monId1 = Value('id').of(adresse.personneLieeAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId1;
} 
else if (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adressePro.cadre4AdresseProfessionnelle.etablissementAdresseTypeVoie !== null) {
   var monId2 = Value('id').of(adressePro.cadre4AdresseProfessionnelle.etablissementAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId2;
}  
else if (correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance !== null) {
   var monId3 = Value('id').of(correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId3;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12']= Value('id').of(correspondance.adresseCorrespond).eq('domi') ? adresse.personneLieeAdresseNomVoie : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('prof') ? adressePro.cadre4AdresseProfessionnelle.etablissementAdresseNomVoie : 
																			correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance);
																			
if 	(Value('id').of(correspondance.adresseCorrespond).eq('domi') and Value('id').of(adresse.paysAdresseDeclarant).eq('FRXXXXX')) {																		
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= adresse.personneLieeAdresseCommune.getLabel();
}
else if (Value('id').of(correspondance.adresseCorrespond).eq('domi') and not Value('id').of(adresse.paysAdresseDeclarant).eq('FRXXXXX')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= adresse.personneLieeAdresseVille;
}
else if (Value('id').of(correspondance.adresseCorrespond).eq('prof')) {
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= adressePro.cadre4AdresseProfessionnelle.etablissementAdresseCommune.getLabel();
}
else if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune.getLabel();
}

if (Value('id').of(correspondance.adresseCorrespond).eq('domi') and not Value('id').of(adresse.paysAdresseDeclarant).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.14']= adresse.paysAdresseDeclarant.getLabel();
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.1[1]']= correspondance.infosSup.formaliteTelephone2.e164.replace('+', '00');

if (correspondance.infosSup.formaliteTelephone1 != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.1[2]']= correspondance.infosSup.formaliteTelephone1.e164.replace('+', '00');
}

if (correspondance.infosSup.telecopie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.2']= correspondance.infosSup.telecopie.e164.replace('+', '00');
}

if (correspondance.infosSup.formaliteFaxCourriel != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.3']= correspondance.infosSup.formaliteFaxCourriel;
}

// Sous groupe SIF : Signature de la formalité
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.1']= Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? signataire.adresseMandataire.nomPrenomDenominationMandataire : (identite.personneLieePersonnePhysiqueNomUsage != null ? (identite.personneLieePersonnePhysiqueNomUsage + ' ' + identite.personneLieePPPrenom[0]) : (identite.personneLieePersonnePhysiqueNomNaissance + ' ' + identite.personneLieePPPrenom[0]));

if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.2']= "Mandataire";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.3']= signataire.adresseMandataire.villeAdresseMandataire.getId();

/*
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.4']= null;
*/

if (signataire.adresseMandataire.numeroVoieMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.5']= signataire.adresseMandataire.numeroVoieMandataire;
}

if (signataire.adresseMandataire.indiceVoieMandataire !== null) {
   var monId = Value('id').of(signataire.adresseMandataire.indiceVoieMandataire)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.6']          = monId;
}

if (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.7']= signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.8']= signataire.adresseMandataire.dataCodePostalMandataire;

/*
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.9']= null;
*/

if (signataire.adresseMandataire.complementVoieMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.10']= signataire.adresseMandataire.complementVoieMandataire;
}

if (signataire.adresseMandataire.typeVoieMandataire !== null) {
   var monId = Value('id').of(signataire.adresseMandataire.typeVoieMandataire)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.11']          = monId;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.12']= signataire.adresseMandataire.nomVoieMandataire;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.13']= signataire.adresseMandataire.villeAdresseMandataire.getLabel();

/*
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.14']= signataire.adresseMandataire.paysAdresseMandataire != null ? signataire.adresseMandataire.paysAdresseMandataire : null;
*/
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C41']= signataire.formaliteSignatureLieu;

if(signataire.formaliteSignatureDate !== null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C42']= date;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C43']= (signataire.formaliteNonDiffusionInformation ? "C45=N!" : "C45=0!") + ' ' + (correspondance.formaliteObservations != null ? correspondance.formaliteObservations : '') ;

/*
// A ne pas générer 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C44']= null;
*/

								// Groupe ICP : Identification complète de la personne physique

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.1']= Value('id').of(identite.civilite).eq('PersonneLieePersonnePhysiqueCiviliteMasculin') ? "1" : "2";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.2']= identite.personneLieePersonnePhysiqueNomNaissance;

var prenoms=[];
for ( i = 0; i < identite.personneLieePPPrenom.size() ; i++ ){prenoms.push(identite.personneLieePPPrenom[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.3']= prenoms;

if (identite.personneLieePersonnePhysiqueNomUsage != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P02/P02.1']= identite.personneLieePersonnePhysiqueNomUsage;
}

if (identite.personneLieePersonnePhysiquePseudonyme != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P02/P02.2']= identite.personneLieePersonnePhysiquePseudonyme;
}

if(identite.personneLieePersonnePhysiqueDateNaissance !== null) {
	var dateTemp = new Date(parseInt(identite.personneLieePersonnePhysiqueDateNaissance.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.1']= date;
}

if (not Value('id').of(identite.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = identite.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.2']= result;
}
else if (Value('id').of(identite.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.2'] = identite.personneLieePPLieuNaissanceCommune.getId();
}	

if (not Value('id').of(identite.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.3']= identite.personneLieePPLieuNaissancePays.getLabel();
}

if (Value('id').of(identite.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.4']= identite.personneLieePPLieuNaissanceCommune.getLabel();
}

if (Value('id').of(adresse.paysAdresseDeclarant).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.3']= adresse.personneLieeAdresseCommune.getId();
}
else if (not Value('id').of(adresse.paysAdresseDeclarant).eq('FRXXXXX')) {
	var idPays = adresse.paysAdresseDeclarant.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.3']= result;
}

if (adresse.personneLieeAdresseNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.5']= adresse.personneLieeAdresseNumeroVoie;
}

if (adresse.personneLieeAdresseIndiceVoie !== null) {
   var monId = Value('id').of(adresse.personneLieeAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.6']          = monId;
}

if (adresse.personneLieeAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.7']= adresse.personneLieeAdresseDistriutionSpecialeVoie;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.8']= adresse.personneLieeAdresseCodePostal != null ? adresse.personneLieeAdresseCodePostal : ".";


if (adresse.personneLieeAdresseComplementVoie != null)  {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.10']= adresse.personneLieeAdresseComplementVoie;
}

if (adresse.personneLieeAdresseTypeVoie !== null) {
   var monId = Value('id').of(adresse.personneLieeAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.11']          = monId;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.12']= adresse.personneLieeAdresseNomVoie;

if (Value('id').of(adresse.paysAdresseDeclarant).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.13']= adresse.personneLieeAdresseCommune.getLabel();
}
else if (not Value('id').of(adresse.paysAdresseDeclarant).eq('FRXXXXX')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.13']= adresse.personneLieeAdresseVille;
}

if (not Value('id').of(adresse.paysAdresseDeclarant).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.14']= adresse.paysAdresseDeclarant.getLabel();
}

/*
// N'a pas lieu d'être car relatif à un 16P à laisser à null
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P05']= null;
*/

if (activite.etablissmentNonSedentariteQualiteNonSedentaire) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P06']= "A";
}


								// Groupe AIP : Ancienne identification de la personne physique (à ne compléter que si 05P)

if (identite.formaliteEvenementOui) {								
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P12']= identite.voletSocialActiviteExerceeAnterieurementSIREN.split(' ').join('');
}
								// Groupe NAP : Nationalité de la personne physique
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/NAP/P21']= identite.personneLieePersonnePhysiqueNationalite;

								// Groupe MEP : Mineur émancipé (si déclaré)
/*
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/MEP']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/MEP/P22']= null;
*/

								// Groupe ISP : Insaisissabilité  (s'il ne s'agit pas d'un agricole et si déclaré)
/*
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ISP']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ISP/P90']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ISP/P91']= null;
*/

								// Groupe DAP : Déclaration d'affectation du patrimoine (si option pour l'EIRL)
if ($p0pl.cadreEIRLGroup.cadreEIRL.estEIRL == true) {
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P70']= Value('id').of(declarationAffectation.eirlStatut).eq('EirlStatutEIRLReprise') ? "R" : "O";
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P71']= declarationAffectation.declarationPatrimoine.eirlDenomination;
if (declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable !== null) {
    var dateTmp = new Date(parseInt(declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable.getTimeInMillis()));
    var dateClotureEc = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateClotureEc = dateClotureEc.concat(pad(month.toString()));
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P72']          = dateClotureEc;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P73']=  sanitize(declarationAffectation.declarationPatrimoine.objetPartiel ? declarationAffectation.declarationPatrimoine.eirlObjet : activite.etablissementActivitesAutres);



if (Value('id').of(eirl.cadre1DeclarationAffectationPatrimoine.eirlStatut).eq('EirlStatutEIRLReprise')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P76/P76.1']= declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLDenomination;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P76/P76.2']= Value('id').of(declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLRegistre).eq('EirlPrecedentEIRLRegistreRseirl') ? "3" : "4";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P76/P76.3']= declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLLieuImmatriculation;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P76/P76.4']= declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN.split(' ').join('');
}


}

// Groupe SMP : Situation matrimoniale de la personne physique (conditionnel)

if (Value('id').of(statutConjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieCollaborateur')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P25/P25.2']= conjoint.personneLieePersonnePhysiqueNomNaissanceConjointPacse != null ? conjoint.personneLieePersonnePhysiqueNomNaissanceConjointPacse : null;
var prenoms=[];
for ( i = 0; i < conjoint.personneLieePersonnePhysiquePrenom1ConjointPacse.size() ; i++ ){prenoms.push(conjoint.personneLieePersonnePhysiquePrenom1ConjointPacse[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P25/P25.3']= prenoms;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P25/P25.4']= conjoint.personneLieePersonnePhysiqueNomUsageConjointPacse != null ? conjoint.personneLieePersonnePhysiqueNomUsageConjointPacse : null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P40']= "O";

if(conjoint.personneLieePersonnePhysiqueDateNaissanceConjointPacse !== null) {
	var dateTemp = new Date(parseInt(conjoint.personneLieePersonnePhysiqueDateNaissanceConjointPacse.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.1']= date;
}

if	(Value('id').of(conjoint.personneLieePersonnePhysiquePaysNaissanceConjointPacse).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.2']= conjoint.personneLieePersonnePhysiqueLieuNaissanceCommuneConjointPacse.getId();
} else if (not Value('id').of(conjoint.personneLieePersonnePhysiquePaysNaissanceConjointPacse).eq('FRXXXXX')) {
		var idPays = conjoint.personneLieePersonnePhysiquePaysNaissanceConjointPacse.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.2']= result;
}

if (not Value('id').of(conjoint.personneLieePersonnePhysiquePaysNaissanceConjointPacse).eq('FRXXXXX')) {																					 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.3']= conjoint.personneLieePersonnePhysiquePaysNaissanceConjointPacse.getLabel();
}																					 

if (Value('id').of(conjoint.personneLieePersonnePhysiquePaysNaissanceConjointPacse).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.4']= conjoint.personneLieePersonnePhysiqueLieuNaissanceCommuneConjointPacse.getLabel();
}																				 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P42']= conjoint.personneLieePersonnePhysiqueNationaliteConjointPacse;

if (conjoint.adresseConjointDifferente) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.3']= conjoint.adresseDomicileConjoint.adresseConjointCommune.getId();

if (conjoint.adresseDomicileConjoint.adresseConjointNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.5']= conjoint.adresseDomicileConjoint.adresseConjointNumeroVoie;
}

if (conjoint.adresseDomicileConjoint.adresseConjointIndiceVoie !== null) {
   var monId = Value('id').of(conjoint.adresseDomicileConjoint.adresseConjointIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.6']          = monId;
}

if (conjoint.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.7']= conjoint.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.8']= conjoint.adresseDomicileConjoint.adresseConjointCodePostal;
																						
if(conjoint.adresseDomicileConjoint.adresseConjointComplementVoie != null) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.10']= conjoint.adresseDomicileConjoint.adresseConjointComplementVoie;
}
	
if (conjoint.adresseDomicileConjoint.adresseConjointTypeVoie !== null) {
   var monId = Value('id').of(conjoint.adresseDomicileConjoint.adresseConjointTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.11']          = monId;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.12']= conjoint.adresseDomicileConjoint.adresseConjointNomVoie;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.13']= conjoint.adresseDomicileConjoint.adresseConjointCommune.getLabel();
}
}

if (Value('id').of(statutConjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieSalarie')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P45']= "O";
}

								// Groupe JGP : Justification pour le RCS des déclaration relatives à la personne physique
/*
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/JGP/J00']= null;
*/

								// Groupe GCS : Groupe complément social

// Sous groupe ISS : Immatriculation sécurité sociale du travailleur non salarié

var nirDeclarant = social.voletSocialNumeroSecuriteSociale;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A10/A10.1']  = nirDeclarant.substring(0, 13);
}
var nirDeclarant = social.voletSocialNumeroSecuriteSociale;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A10/A10.2']            = nirDeclarant.substring(13, 15);
}


regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A23']= "O";

if (social.voletSocialActiviteAutreQueDeclareeStatutOui) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A24/A24.2']= Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutSalarie') ? "1" :
																						(Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutSalarieAgricole') ? "2" : 
																						(Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutRetraitePensionne') ? "8" : "9" ));

if (Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutCocheAutre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A24/A24.3']=social.voletSocialActiviteAutreQueDeclareeStatutAutre.substring(0,40);
}
/*
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A24/A24.5']= "France";
*/
/*
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A24/A24.6']= ".";
*/
}

/* regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A27']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A28']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A29']= null;
*/

// Sous groupe JES : Justification du droit d'exercice (conditionnel)

if (social.ressortissantHorsUE == true) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/JES/A30/A30.1']= social.voletSocialTitreSejourLieuDelivranceCommune.getId() ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/JES/A30/A30.2']= social.voletSocialTitreSejourLieuDelivranceCommune.getLabel();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/JES/A30/A30.3']= social.voletSocialTitreSejourNumero;
if(social.voletSocialTitreSejourDateExpiration !== null) {
	var dateTemp = new Date(parseInt(social.voletSocialTitreSejourDateExpiration.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/JES/A30/A30.4']= date;
}}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/CAS/A42/A42.1']= ".";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/CAS/A42/A42.2']= ".";

/*
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/CAS/A44']= null;
*/

// Sous groupe SCS : Situation du conjoint vis-à-vis de la sécurité sociale

if (Value('id').of(statutConjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieCollaborateur')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A53/A53.4']= conjoint.voletSocialConjointCouvertAssuranceMaladieOui ? "O" : "N";

var nirConjoit = conjoint.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(nirConjoit != null) {
    nirConjoit = nirConjoit.replace(/ /g, "");
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A55/A55.1']  = nirConjoit.substring(0, 13);
}
var nirConjoit = conjoint.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(nirConjoit != null) {
    nirConjoit = nirConjoit.replace(/ /g, "");
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A55/A55.2']            = nirConjoit.substring(13, 15);
}}


// Sous groupe MSS : Régime micro Social du TNS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/MSS/A31/A31.1']= "N";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/MSS/A31/A31.2']= ".";

// Groupe SIU : siège de l'entreprise (adresse de l'entreprise individuelle)
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U10']= null;
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U14/U14.2']= null;
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U14/U14.3']= null;
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U15']= null;

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U16']= "O";
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.3']= Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ?  adresse.personneLieeAdresseCommune.getId() : adressePro.cadre4AdresseProfessionnelle.etablissementAdresseCommune.getId();

if ((not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadre4AdresseProfessionnelle.etablissementAdresseNumeroVoie != null)
	or (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adresse.personneLieeAdresseNumeroVoie != null)) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.5']= Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? 
																			   adresse.personneLieeAdresseNumeroVoie : adressePro.cadre4AdresseProfessionnelle.etablissementAdresseNumeroVoie;
}

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adresse.personneLieeAdresseIndiceVoie !== null) {
   var monId1 = Value('id').of(adresse.personneLieeAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.6']          = monId1;
} else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadre4AdresseProfessionnelle.etablissementAdresseIndiceVoie !== null) {
   var monId2 = Value('id').of(adressePro.cadre4AdresseProfessionnelle.etablissementAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.6']          = monId2;
}
																			   
if ((not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadre4AdresseProfessionnelle.etablissementAdresseDistriutionSpecialeVoie != null)
	or (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adresse.personneLieeAdresseDistriutionSpecialeVoie != null)) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.7']= Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? 
																			adresse.personneLieeAdresseDistriutionSpecialeVoie : adressePro.cadre4AdresseProfessionnelle.etablissementAdresseDistriutionSpecialeVoie;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.8']= Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? 
																			adresse.personneLieeAdresseCodePostal : adressePro.cadre4AdresseProfessionnelle.etablissementAdresseCodePostal;

if ((not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadre4AdresseProfessionnelle.etablissementAdresseComplementVoie != null)
	or (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adresse.personneLieeAdresseComplementVoie != null)) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.10']= Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? 
																				adresse.personneLieeAdresseComplementVoie : adressePro.cadre4AdresseProfessionnelle.etablissementAdresseComplementVoie;
}
																				
if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adresse.personneLieeAdresseTypeVoie !== null) {
   var monId1 = Value('id').of(adresse.personneLieeAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.11']          = monId1;
} else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadre4AdresseProfessionnelle.etablissementAdresseTypeVoie !== null) {
   var monId2 = Value('id').of(adressePro.cadre4AdresseProfessionnelle.etablissementAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.11']          = monId2;
}
	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.12']= Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? adresse.personneLieeAdresseNomVoie : adressePro.cadre4AdresseProfessionnelle.etablissementAdresseNomVoie;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.13']= Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? adresse.personneLieeAdresseCommune.getLabel() : adressePro.cadre4AdresseProfessionnelle.etablissementAdresseCommune.getLabel();

/*
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.14']= "France";
*/

							// Groupe CPU : Caractéristiques de l'entreprise
							
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U21']= sanitize(activite.etablissementActivitesAutres);

if (activite.etablissementEffectifSalariePresenceOui) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U22']= activite.etablissementEffectifSalarieNombre != null ? activite.etablissementEffectifSalarieNombre : "."
}

/*
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U24']= null;
*/

							
							// Groupe RFU : Régime fiscal de l'entreprise


/*							
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U30']= null;
*/

if ($p0pl.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine.declarationPatrimoine.objetPartiel or not $p0pl.cadreEIRLGroup.cadreEIRL.estEIRL) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U31']= Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesRsBNC') ? "110" : "111";

if (fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionCreanceDette) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U32']= "210";
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.1']= (Value('id').of(fiscal.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARfTVA') or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesRsBNC')) ? '310' :
																			(Value('id').of(fiscal.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARsTVA') ? '311' : '312');

if (fiscal.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres1) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.2'] = "410";
}
/*
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U37']= null;
*/
}


							// Groupe OFU : Option Fiscale EIRL (si EIRL) 
							
if ($p0pl.cadreEIRLGroup.cadreEIRL.estEIRL == true) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U70']= Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesRsBNC') ? '110' :
																		(Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesDcBNC') ? '111' : 
																		(Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeFiscalIS).eq('RegimeFiscalRegimeImpositionBeneficesRsIS') ? "114" : "115"));

if (impotEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionCreanceDette) {																		
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U71']= "210";
}
else if (Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionIS')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U71']= "211";
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U72/U72.1'] = (Value('id').of(impotEIRL.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARfTVA') or Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesRsBNC')) ? "310" :
																				(Value('id').of(impotEIRL.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARsTVA') ? "311" : "312");

if (impotEIRL.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres1DC) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U72/U72.2'] = "410";
}

/*																			
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U74']= null;
*/
}


							// Groupe ICE : Identification complète de l'établissement (Mettre E04=3, E01=1 et E02=U11)

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E01']= "1";

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.3']= Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? adresse.personneLieeAdresseCommune.getId() : adressePro.cadre4AdresseProfessionnelle.etablissementAdresseCommune.getId();

if ((not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadre4AdresseProfessionnelle.etablissementAdresseNumeroVoie != null)
	or (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adresse.personneLieeAdresseNumeroVoie != null)) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5']= Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? 
																							adresse.personneLieeAdresseNumeroVoie : adressePro.cadre4AdresseProfessionnelle.etablissementAdresseNumeroVoie;
}

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adresse.personneLieeAdresseIndiceVoie !== null) {
   var monId1 = Value('id').of(adresse.personneLieeAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.6']          = monId1;
} 
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadre4AdresseProfessionnelle.etablissementAdresseIndiceVoie !== null) {
   var monId2 = Value('id').of(adressePro.cadre4AdresseProfessionnelle.etablissementAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.6']          = monId2;
}
																			   
if ((not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadre4AdresseProfessionnelle.etablissementAdresseDistriutionSpecialeVoie != null)
	or (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adresse.personneLieeAdresseDistriutionSpecialeVoie != null)) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.7']= Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? 
																							adresse.personneLieeAdresseDistriutionSpecialeVoie : adressePro.cadre4AdresseProfessionnelle.etablissementAdresseDistriutionSpecialeVoie;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.8']= Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? 
																							adresse.personneLieeAdresseCodePostal : adressePro.cadre4AdresseProfessionnelle.etablissementAdresseCodePostal;
																							
if ((not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadre4AdresseProfessionnelle.etablissementAdresseComplementVoie != null)
	or (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adresse.personneLieeAdresseComplementVoie != null)) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.10']= Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? 
																							adresse.personneLieeAdresseComplementVoie : adressePro.cadre4AdresseProfessionnelle.etablissementAdresseComplementVoie;
}

if (Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adresse.personneLieeAdresseTypeVoie !== null) {
   var monId1 = Value('id').of(adresse.personneLieeAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.11']          = monId1;
} 
else if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') and adressePro.cadre4AdresseProfessionnelle.etablissementAdresseTypeVoie !== null) {
   var monId2 = Value('id').of(adressePro.cadre4AdresseProfessionnelle.etablissementAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.11']          = monId2;
}	

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.12']= Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? adresse.personneLieeAdresseNomVoie : adressePro.cadre4AdresseProfessionnelle.etablissementAdresseNomVoie;

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.13']= Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? adresse.personneLieeAdresseCommune.getLabel() : adressePro.cadre4AdresseProfessionnelle.etablissementAdresseCommune.getLabel();

/*
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.14']= "France";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E03/E03.1']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E03/E03.2']= null;
*/

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E04']= "3";

/*
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E05']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E09']= null;
*/

							// Groupe ORE : Origine de l'établissement
							
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.1']=  (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation') or Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')) ? "1" : "8";


							// Groupe PEE : précédent exploitant de l'établissement
							
if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsReprise')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.1']= origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant.split(' ').join('');;

if (Value('id').of(origine.precedentExploitant.typePersonnePrecedentExploitant).eq('typePersonnePrecedentExploitantPP')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.2']= origine.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant;

if (origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.3']=  origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34[1]/E34.4']= origine.precedentExploitant.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant;
}

if (Value('id').of(origine.precedentExploitant.typePersonnePrecedentExploitant).eq('typePersonnePrecedentExploitantPM')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34[1]/E34.7']= origine.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant : null;
}
}


							// Groupe ACE : Activité de l'établissement
							
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E70']= sanitize(activite.etablissementActivitesAutres);

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E71']= sanitize(activite.etablissementActivitesPrincipales2 != null ? activite.etablissementActivitesPrincipales2 : activite.etablissementActivitesAutres.substring(0, 140)) ;

/* regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E79/E79.1']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E79/E79.2']= null; 
*/

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E73/E73.1']= Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonnierePermanente') ? 'P' : 'S';

if (Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonniereSaisonniere')) {
if(Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonniereSaisonniere')) {
	var dateTemp = new Date(parseInt((activite.activiteSaisonnierePeriode.from).getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E73/E73.11[1]/E73.111']= date;
}
if (Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonniereSaisonniere')) {
	var dateTemp = new Date(parseInt((activite.activiteSaisonnierePeriode.to).getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E73/E73.11[1]/E73.112']= date;
}

if (activite.autrePeriode) {
	var dateTemp = new Date(parseInt((activite.activiteSaisonnierePeriode2.from).getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E73/E73.11[2]/E73.111']= date;
}
if (activite.autrePeriode) {
	var dateTemp = new Date(parseInt((activite.activiteSaisonnierePeriode2.to).getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E73/E73.11[2]/E73.112']= date;
}	
}	


if (activite.etablissmentNonSedentariteQualiteNonSedentaire) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E73/E73.2']='A';
}


				// Groupe SAE : salariés de l'établissement
if (activite.etablissementEffectifSalariePresenceOui) {
if (activite.etablissementEffectifSalarieEmbauchePremierSalarieOui) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E83/E83.1']= "1";
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E84']= activite.etablissementEffectifSalarieNombre;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E86']= activite.etablissementEffectifSalariePresenceOui ? "O" : "N";



								// Groupe JGE : Justification pour le RCS des déclaration relatives à l'établissement
/*
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/JGE/J00']= null;
*/ 
//-->DEST-689 : Afficher les erreurs XSD dans la formalité quand il y'a une erreur de génération

// Call to the XML-REGENT generation WS 
var response = nash.service.request('${regent.baseUrl}/private/v1/xml-regent/generate/{regentVersion}/{authorityType}/{authorityId}', regentVersion, authorityType, authorityId) 
.dataType('application/json') // 
.accept('json') // 
.param('listTypeEvenement',eventRegent) 
//-->MINE-263 : Permettre à l'équipe FF de voir les problèmes de validation XSD et le regent sur studio
.continueOnError(true) //
.post(JSON.stringify(regentFields)); 

// Record the generated XML-REGENT 
//MOD déplacement du test de la réponse du premier ws
if (response != null && response.status == 200) { 
    var xmlRegentStr = response.asBytes(); 
    nash.record.saveFile("XML_REGENT.xml",xmlRegentStr); 

//debut de l'ajout
 if (authorityId2 != null) {
//MOD extraction du numéro de liasse du premeir regent généré
var numeroLiasse = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02');
numeroLiasse = JSON.parse(numeroLiasse);
_log.info('extracted numero de liasse is {}', numeroLiasse);
 regentFields['/REGENT-XML/Destinataire']= authorityId2;
 
 // Filtre regent greffe
var undefined;
// Groupe GCS/ISS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A10/A10.1']  = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A10/A10.2'] = undefined;
// Groupe GCS/SNS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A23'] = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A24/A24.2']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SNS/A24/A24.3']= undefined;
// Groupe GCS/JES
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/JES/A30/A30.1']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/JES/A30/A30.2']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/JES/A30/A30.3']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/JES/A30/A30.4'] = undefined;
// Groupe GCS/CAS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/CAS/A42/A42.1']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/CAS/A42/A42.2']= undefined;
// Groupe GCS/SCS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A53/A53.4']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A55/A55.1'] = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A55/A55.2'] = undefined;
// Groupe GCS/MSS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/MSS/A31/A31.1']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/MSS/A31/A31.2']= undefined;
// Groupe RFU
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U31']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.1']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.2']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U37']= undefined;
// Groupe OFU
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U70']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U71']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U72/U72.1']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U72/U72.2']= undefined;
// Groupe SAE
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E83/E83.1']=undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E84']=undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E85/E85.8']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E86']= undefined;


 //MOD modification de l'appel pour la génération du deuxième régent afin de prendre en compte le numéro de liasse du premier
 var response2 = nash.service.request('${regent.baseUrl}/private/v1/xml-regent/generate/{regentVersion}/{authorityType}/{authorityId}', regentVersion, authorityType2, authorityId2) 
.dataType('application/json') // 
.accept('json') // 
.param('listTypeEvenement',eventRegent) 
.param('liasseNumber', numeroLiasse.C02) 
.continueOnError(true)
.post(JSON.stringify(regentFields)); 


	//Début de l'ajout
	//MOD modifier reponse en response2
	if (response2 != null && response2.status == 200) { 
		var xmlRegentStr = response2.asBytes(); 
		nash.record.saveFile("XML_REGENT2.xml",xmlRegentStr);
	}else{
		
		var returnedResponse = response2.asObject();
		_log.info("Call ws regent returned errors  {}", returnedResponse);
		
		
		var regent2 = returnedResponse['regent'];
		var errors2 = returnedResponse['errors']; 
		nash.record.saveFile("XML_REGENT2.xml",regent2.getBytes()); 
		nash.record.saveFile("XML_VALIDATION_ERRORS_2.xml",errors2.getBytes()); 
		
		//-->MINE-263 : Permettre à l'équipe FF de voir les problèmes de validation XSD et le regent sur studio
		
		return spec.create({
		id : 'xmlGenerationConfirmation',
		label : "Xml Regent confirmation message",
		groups : [ spec.createGroup({
				id : 'Regent ',
				description : "Une erreur s'est produite lors de la génération du XML Regent.",
				data : [
				spec.createData({
					id: 'regent',
					label: "XML regent généré",
					type: 'Text',
					mandatory: true,
					value: regent2
				}),spec.createData({
					id: 'errors',
					label: "Erreurs de validations xsd",
					type: 'Text',
					mandatory: false,
					value: errors2
				}),spec.createData({
					id: 'blockingField',
					label: "xsd erroné",
					help:"Ce champs sert à arrêter le process du dossier pour que le support puisse le consulter",
					type: 'StringReadOnly',
					mandatory: true
				})]
			})]
		});
	}	
}	

	return spec.create({
	id : 'xmlGenerationConfirmation',
	label : "Xml Regent confirmation message",
	groups : [ spec.createGroup({
		id : 'confirmationMessageOk',
		description : "Le fichier XML Regent a été généré et ajouté au dossier.",
		data : []
		}) ]
	});
	
}else{
	
	var returnedResponse = response.asObject();
	_log.info("Call ws regent returned errors  {}", returnedResponse);
	
	
	var regent = returnedResponse['regent'];
	var errors = returnedResponse['errors']; 
	nash.record.saveFile("XML_REGENT.xml",regent.getBytes()); 
	nash.record.saveFile("XML_VALIDATION_ERRORS.xml",errors.getBytes()); 
	
	
	return spec.create({
	id : 'xmlGenerationConfirmation',
	label : "Xml Regent confirmation message",
	groups : [ spec.createGroup({
		id : 'Regent ',
		description : "Une erreur s'est produite lors de la génération du XML Regent de la première autorité.",
		data : [
				spec.createData({
					id: 'regent',
					label: "XML regent généré",
					type: 'Text',
					mandatory: true,
					value: regent
				}),spec.createData({
					id: 'errors',
					label: "Erreurs de validations xsd",
					type: 'Text',
					mandatory: false,
					value: errors
				}),spec.createData({
					id: 'blockingField',
					label: "xsd erroné",
					help:"Ce champs sert à arrêter le process du dossier pour que le support puisse le consulter",
					type: 'StringReadOnly',
					mandatory: true
			})]
		}) ]
	});
}	