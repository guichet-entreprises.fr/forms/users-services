//prepare info to send 

var adresse = $p0pl.cadre1IdentiteGroup.cadre1AdresseDeclarant;
var adressePro = $p0pl.cadre4AdresseActiviteGroup.cadre4AdresseActivite;

var algo = "trouver destinataire";

var secteur1 = "Liberal";

var typePersonne = "PP";

var formJuridique = $p0pl.cadreEIRLGroup.cadreEIRL.estEIRL ? "EIRL" : "EI";

var optionCMACCI = "NON";

var codeCommune = Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? adresse.personneLieeAdresseCommune.getId() : adressePro.cadre4AdresseProfessionnelle.etablissementAdresseCommune.getId();

var attachement= "/3-review/generated/generated.record-1-PE_p0pl_Creation.pdf";

return spec.create({
		id: 'prepareSend',
		label: "Préparation de la recherche du destinataire",
		groups: [spec.createGroup({
				id: 'view',
				label: "Informations",
				data: [spec.createData({
						id: 'algo',
						label: "Algo",
						type: 'String',
						mandatory: true,
						value: algo
					}), spec.createData({
						id: 'secteur1',
						label: "Secteur",
						type: 'String',
						value: secteur1
					}), spec.createData({
						id: 'typePersonne',
						label: "Type personne",
						type: 'String',
						value: typePersonne
					}), spec.createData({
						id: 'formJuridique',
						label: "Forme juridique",
						type: 'String',
						value: formJuridique
					}),spec.createData({
						id: 'optionCMACCI',
						label: "Option CMACCI",
						type: 'String',
						value: optionCMACCI
					}),spec.createData({
						id: 'codeCommune',
						label: "Code commune",
						type: 'String',
						value: codeCommune
					}) ,spec.createData({
					    id: 'attachement',
					    label: "Pièce jointe",
					    type: 'String',
					    value: attachement
					})
				]
			})]
});