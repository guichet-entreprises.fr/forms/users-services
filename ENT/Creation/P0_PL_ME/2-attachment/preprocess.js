// PJ Déclarant

var userDeclarant;
if ($p0PLMEP1.cadre1IdentiteGroup.cadre1Identite.personneLieePersonnePhysiqueNomUsage != null) {
    var userDeclarant = $p0PLMEP1.cadre1IdentiteGroup.cadre1Identite.personneLieePersonnePhysiqueNomUsage + '  ' + $p0PLMEP1.cadre1IdentiteGroup.cadre1Identite.personneLieePPPrenom[0] ;
} else {
    var userDeclarant = $p0PLMEP1.cadre1IdentiteGroup.cadre1Identite.personneLieePersonnePhysiqueNomNaissance + '  '+ $p0PLMEP1.cadre1IdentiteGroup.cadre1Identite.personneLieePPPrenom[0] ;
}

var pj=$p0PLMEP1.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteDeclarant')) {
    attachment('pjIDDeclarantSignataire', 'pjIDDeclarantSignataire', { label: userDeclarant, mandatory:"true"});
}

var pj=$p0PLMEP1.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
    attachment('pjIDDeclarantNonSignataire', 'pjIDDeclarantNonSignataire', { label: userDeclarant, mandatory:"true"});
}

// PJ conjoint

var pj=$p0PLMEP1.cadre2conjointGroup.cadre2Conjoint.statutConjointCollaborateur ;
if(Value('id').of(pj).contains('PersonneLieeConjointCollaborateurSalarieCollaborateur') or Value('id').of(pj).contains('PersonneLieeConjointCollaborateurSalarieSalarie')) {
    attachment('pjChoixStatutConjoint', 'pjChoixStatutConjoint', { mandatory:"true"});
}

// PJ Mandataire

var userMandataire=$p0PLMEP1.cadre11SignatureGroup.cadre11Signature.adresseMandataire

var pj=$p0PLMEP1.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
    attachment('pjIDMandataireSignataire', 'pjIDMandataireSignataire', {label: userMandataire.nomPrenomDenominationMandataire, mandatory:"true"});
}

if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire') and $p0PLMEP1.cadreEIRLGroup.cadreEIRL.estEIRL) {
    attachment('pjPouvoir', 'pjPouvoir', { mandatory:"true"});
}

// PJ EIRL

var pj=$p0PLMEP1.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine;
if($p0PLMEP1.cadreEIRLGroup.cadreEIRL.estEIRL) {
	attachment('pjAutorisationMineur', 'pjAutorisationMineur', { mandatory:"false"});
}

if($p0PLMEP1.cadreEIRLGroup.cadreEIRL.estEIRL and not Value('id').of(pj.eirlDepot).eq('eirlSansDepot')) {
    attachment('pjDAP', 'pjDAP', { mandatory:"true"});
}

var pj=$p0PLMEP1.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine;
if(Value('id').of(pj.cadreEIRLInformationsComplementaires.contenuDAP).contains('bienImmo') or Value('id').of(pj.cadreEIRLInformationsComplementairesBis.contenuDAPBis).contains('bienImmo')) {
    attachment('pjDAPActeNotarie', 'pjDAPActeNotarie', { mandatory:"true"});
}

if(Value('id').of(pj.cadreEIRLInformationsComplementaires.contenuDAP).contains('bienCommunIndivis') or Value('id').of(pj.cadreEIRLInformationsComplementairesBis.contenuDAPBis).contains('bienCommunIndivis')) {
    attachment('pjDAPAccordTiers', 'pjDAPAccordTiers', { mandatory:"true"});
}