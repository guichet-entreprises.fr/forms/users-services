var formFields = {};
var cerfaFields = {};
function pad(s) { return (s < 10) ? '0' + s : s; }


// Cadres 1 - Activité non salariée

var identite = $p0PLMEP1.cadre1IdentiteGroup.cadre1Identite;

formFields['formalite_evenement_oui']                                                   = identite.formaliteEvenementOui ? true : false;
formFields['formalite_evenement_non']                                                   = identite.formaliteEvenementOui ? false : true;
formFields['voletSocial_activiteExerceeAnterieurementSIREN']                            = identite.voletSocialActiviteExerceeAnterieurementSIREN != null ? identite.voletSocialActiviteExerceeAnterieurementSIREN.split(' ').join('') : '';

// Cadres 2A - EI

formFields['nonEIRL']                                                                   = $p0PLMEP1.cadreEIRLGroup.cadreEIRL.estEIRL ? false : true;

// Cadres 2B - EIRL

formFields['estEIRL']                                                                   = $p0PLMEP1.cadreEIRLGroup.cadreEIRL.estEIRL ? true : false;

// Cadres 3 - Identité

formFields['personneLiee_personnePhysique_nomNaissance']                                = identite.personneLieePersonnePhysiqueNomNaissance;
formFields['personneLiee_personnePhysique_nomUsage']                                    = identite.personneLieePersonnePhysiqueNomUsage;

var prenoms=[];
for ( i = 0; i < identite.personneLieePPPrenom.size() ; i++ ){prenoms.push(identite.personneLieePPPrenom[i]);}                            
formFields['personneLiee_personnePhysique_prenom1']                                      = prenoms.toString();

formFields['personneLiee_personnePhysique_civiliteMasculin']                            = Value('id').of(identite.civilite).eq('PersonneLieePersonnePhysiqueCiviliteMasculin') ? true : false;
formFields['personneLiee_personnePhysique_civiliteFeminin']                             = Value('id').of(identite.civilite).eq('PersonneLieePersonnePhysiqueCiviliteFeminin') ? true : false;
formFields['personneLiee_personnePhysique_nationalite']                                 = identite.personneLieePersonnePhysiqueNationalite;

if(identite.personneLieePersonnePhysiqueDateNaissance != null) {
	var dateTmp = new Date(parseInt(identite.personneLieePersonnePhysiqueDateNaissance.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['personneLiee_personnePhysique_dateNaissance'] = date;
}
formFields['personneLiee_personnePhysique_lieuNaissanceDepartement']                    = identite.personneLieePPLieuNaissanceDepartement != null ? identite.personneLieePPLieuNaissanceDepartement.getId() : '';
formFields['personneLiee_personnePhysique_lieuNaissanceCommune']                        = identite.personneLieePPLieuNaissanceCommune != null ? identite.personneLieePPLieuNaissanceCommune : identite.personneLieePPLieuNaissanceVille;
formFields['personneLiee_personnePhysique_lieuNaissancePays']                           = Value('id').of(identite.personneLieePPLieuNaissancePays).eq('FRXXXXX') ? '' : identite.personneLieePPLieuNaissancePays;

var adresse = $p0PLMEP1.cadre1IdentiteGroup.cadre1AdresseDeclarant;

formFields['personneLiee_adresse_voie']                                              = (adresse.personneLieeAdresseNumeroVoie != null ? adresse.personneLieeAdresseNumeroVoie : '') 
																						+ ' ' + (adresse.personneLieeAdresseIndiceVoie != null ? adresse.personneLieeAdresseIndiceVoie : '') 
																						+ ' ' + (adresse.personneLieeAdresseTypeVoie != null ? adresse.personneLieeAdresseTypeVoie : '') 
																						+ ' ' + adresse.personneLieeAdresseNomVoie
																						+ ' ' + (adresse.personneLieeAdresseComplementVoie != null ? adresse.personneLieeAdresseComplementVoie : '') 
																						+ ' ' + (adresse.personneLieeAdresseDistriutionSpecialeVoie != null ? adresse.personneLieeAdresseDistriutionSpecialeVoie : '');
formFields['personneLiee_adresse_codePostal']                                           = adresse.personneLieeAdresseCodePostal;
formFields['personneLiee_adresse_commune']                                              = Value('id').of(adresse.paysAdresseDeclarant).eq('FRXXXXX') ? adresse.personneLieeAdresseCommune : adresse.personneLieeAdresseVille;
formFields['personneLiee_adresse_pays']                                                 = Value('id').of(adresse.paysAdresseDeclarant).eq('FRXXXXX') ? '' : adresse.paysAdresseDeclarant;
formFields['personneLiee_adresse_communeAncienne']                                      = adresse.personneLieeAdresseCommuneAncienne != null ? adresse.personneLieeAdresseCommuneAncienne : '';


//Cadre 4 - Conjoint

var conjoint = $p0PLMEP1.cadre2conjointGroup.cadre2Conjoint;

formFields['personneLiee_conjointActiviteEntreprise_oui']                   			= conjoint.conjointRole ? true : false;
formFields['personneLiee_conjointActiviteEntreprise_non']                  				= conjoint.conjointRole ? false : true;
formFields['personneLiee_conjointCollaborateurSalarie_collaborateur']                   = Value('id').of(conjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieCollaborateur') ? true : false;
formFields['personneLiee_conjointCollaborateurSalarie_salarie']                         = Value('id').of(conjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieSalarie') ? true : false;
if (conjoint.conjointRole) {
formFields['personneLiee_personnePhysique_nomNaissance_conjointPacse']                  = conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueNomNaissanceConjointPacse;
formFields['personneLiee_personnePhysique_nomUsage_conjointPacse']                      = conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueNomUsageConjointPacse;
var prenoms=[];
for ( i = 0; i < conjoint.cadre2InfosConjoint.personneLieePersonnePhysiquePrenom1ConjointPacse.size() ; i++ ){prenoms.push(conjoint.cadre2InfosConjoint.personneLieePersonnePhysiquePrenom1ConjointPacse[i]);}                            
formFields['personneLiee_personnePhysique_prenom1_conjointPacse']                                      = prenoms.toString();
formFields['personneLiee_personnePhysique_nationalite_conjointPacse']                   = conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueNationaliteConjointPacse;
if(conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueDateNaissanceConjointPacse != null) {
	var dateTmp = new Date(parseInt(conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueDateNaissanceConjointPacse.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['personneLiee_personnePhysique_dateNaissance_conjointPacse'] = date;
}
formFields['personneLiee_personnePhysique_lieuNaissanceDepartement_conjointPacse']  = (conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueLieuNaissanceDepartementConjointPacse != null ? conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueLieuNaissanceDepartementConjointPacse.getId() : '');
formFields['personneLiee_personnePhysique_lieuNaissanceCommune_conjointPacse']      = conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueLieuNaissanceCommuneConjointPacse != null ? conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueLieuNaissanceCommuneConjointPacse : conjoint.cadre2InfosConjoint.personneLieePPLieuNaissanceVilleConjointPacse;
formFields['personneLiee_personnePhysique_lieuNaissancePays_conjointPacse']         = conjoint.cadre2InfosConjoint.personneLieePersonnePhysiquePaysNaissanceConjointPacse;
if (conjoint.cadre2InfosConjoint.adresseConjointDifferente) {
formFields['adresseConjointVoie']                                       	            = (conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointNumeroVoie != null ? conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointNumeroVoie : '')
																						+ '  ' + (conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointIndiceVoie != null ? conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointIndiceVoie : '')
																						+ '  ' + (conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointTypeVoie  != null ? conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointTypeVoie : '')
																						+ '  ' + (conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointNomVoie  != null ? conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointNomVoie : '')
																						+ '  ' + (conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointComplementVoie != null ? conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointComplementVoie :'')
																						+ '  ' + (conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null ? conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie :'');
formFields['adresseConjointCodePostal']                                                 = conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointCodePostal ;
formFields['adresseConjointCommune']                                                    = conjoint.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointCommune;
}                                                
}


//Cadres 5 - Adresse pro

var adressePro = $p0PLMEP1.cadre4AdresseActiviteGroup.cadre4AdresseActivite;

formFields['entreprise_adresseEntreprise_domicile']                                     = Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? true : false;
formFields['etablissement_domiciliation_non']                                           = Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? false : true;
if (not Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')) {
formFields['entrepriseLiee_adresse_voie']                                              =(adressePro.cadre4AdresseProfessionnelle.etablissementAdresseNumeroVoie != null ? adressePro.cadre4AdresseProfessionnelle.etablissementAdresseNumeroVoie : '')
																				            + ' ' + (adressePro.cadre4AdresseProfessionnelle.etablissementAdresseIndiceVoie != null ? adressePro.cadre4AdresseProfessionnelle.etablissementAdresseIndiceVoie : '')
																							+ ' ' + (adressePro.cadre4AdresseProfessionnelle.etablissementAdresseTypeVoie != null ? adressePro.cadre4AdresseProfessionnelle.etablissementAdresseTypeVoie : '')
																				            + ' ' + (adressePro.cadre4AdresseProfessionnelle.etablissementAdresseNomVoie != null ? adressePro.cadre4AdresseProfessionnelle.etablissementAdresseNomVoie : '')
																							+ ' ' + (adressePro.cadre4AdresseProfessionnelle.etablissementAdresseComplementVoie != null ? adressePro.cadre4AdresseProfessionnelle.etablissementAdresseComplementVoie : '')
																				            + ' ' + (adressePro.cadre4AdresseProfessionnelle.etablissementAdresseDistriutionSpecialeVoie != null ? adressePro.cadre4AdresseProfessionnelle.etablissementAdresseDistriutionSpecialeVoie : '');
formFields['entrepriseLiee_adresse_codePostal']                                          = adressePro.cadre4AdresseProfessionnelle.etablissementAdresseCodePostal;
formFields['entrepriseLiee_adresse_commune']                                             = adressePro.cadre4AdresseProfessionnelle.etablissementAdresseCommune;
formFields['entrepriseLiee_adresse_communeAncienne']                                     = adressePro.cadre4AdresseProfessionnelle.etablissementAdresseCommuneAncienne != null ? adressePro.cadre4AdresseProfessionnelle.etablissementAdresseCommuneAncienne : '';
}

// Cadre 6 - Activité

var activite = $p0PLMEP1.cadre4AdresseActiviteGroup.cadre5EtablissementActivite ;

if(activite.etablissementDateDebutActivite != null) {
	var dateTmp = new Date(parseInt(activite.etablissementDateDebutActivite.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_dateDebutActivite'] = date;
}	
formFields['etablissement_activites']                                                   = activite.etablissementActivites;
formFields['etablissement_activitePlusImportante']                                      = activite.etablissementActivitePrincipale;
formFields['entreprise_activitePermanenteSaisonniere_permanente']                       = Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonnierePermanente') ? true : false;
formFields['entreprise_activitePermanenteSaisonniere_saisonniere']                      = Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonniereSaisonniere') ? true : false;
formFields['etablissment_nonSedentariteQualite_nonSedentaire']                          = activite.etablissmentNonSedentariteQualiteNonSedentaire ? true : false;


//Cadre 7 - Déclaration sociale

var social = $p0PLMEP1.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale;

var nirDeclarant = social.voletSocialNumeroSecuriteSociale;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFields['voletSocial_numeroSecuriteSociale']                = nirDeclarant.substring(0, 13);
    formFields['voletSocial_numeroSecuriteSocialeCle']             = nirDeclarant.substring(13, 15);
}
formFields['voletSocial_titreSejour_numero']                                            = social.voletSocialTitreSejourNumero;
formFields['voletSocial_titreSejour_lieuDelivranceCommune']                             = social.voletSocialTitreSejourLieuDelivranceCommune;
if(social.voletSocialTitreSejourDateExpiration != null) {
	var dateTmp = new Date(parseInt(social.voletSocialTitreSejourDateExpiration.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['voletSocial_titreSejour_dateExpiration'] = date;
}
formFields['voletSocial_activiteAutreQueDeclaree_Oui']                                 = social.voletSocialActiviteAutreQueDeclareeStatutOui ? true : false;
formFields['voletSocial_activiteAutreQueDeclaree_Non']                                  = social.voletSocialActiviteAutreQueDeclareeStatutOui ? false : true;
formFields['voletSocial_activiteAutreQueDeclareeStatut_salarie']                        = Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutSalarie') ? true : false;
formFields['voletSocial_activiteAutreQueDeclareeStatut_salarieAgricole']                = Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutSalarieAgricole') ? true : false;
formFields['voletSocial_activiteAutreQueDeclareeStatut_retraitePensionne']              = Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutRetraitePensionne') ? true : false;
formFields['voletSocial_activiteAutreQueDeclareeStatut_autre'] 				            = Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutCocheAutre') ? true : false;
formFields['voletSocial_activiteAutreQueDeclareeStatutAutre']                           = social.voletSocialActiviteAutreQueDeclareeStatutAutre;
formFields['voletSocial_optionMicroSocialVersement_mensuel']           	             	= Value('id').of(social.optionMicroSocialSimplifie).eq('VoletSocialOptionMicroSocialVersementMensuel') ? true : false;
formFields['voletSocial_optionMicroSocialVersement_trimestriel']                		= Value('id').of(social.optionMicroSocialSimplifie).eq('VoletSocialOptionMicroSocialVersementTrimestriel') ? true : false;
formFields['voletSocial_conjointCouvertAssuranceMaladie_oui']                           = Value('id').of($p0PLMEP1.cadre2conjointGroup.cadre2Conjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieCollaborateur') ? (conjoint.cadre2InfosConjoint.voletSocialConjointCouvertAssuranceMaladieOui ? true : false) : false;
formFields['voletSocial_conjointCouvertAssuranceMaladie_non']                           = Value('id').of($p0PLMEP1.cadre2conjointGroup.cadre2Conjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieCollaborateur') ? (conjoint.cadre2InfosConjoint.voletSocialConjointCouvertAssuranceMaladieOui ? false : true) : false;

var nirConjoint = conjoint.cadre2InfosConjoint.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(nirConjoint != null) {
    nirConjoint = nirConjoint.replace(/ /g, "");
    formFields['voletSocial_conjointCollaborateurNumeroSecuriteSociale']                = nirConjoint.substring(0, 13);
    formFields['voletSocial_conjointCollaborateurNumeroSecuriteSocialeCle']             = nirConjoint.substring(13, 15);
}


//Cadre 8 - Otion fiscale hors EIRL

var optionsFiscales = $p0PLMEP1.cadre9optFiscHorsEIRLGroup.cadre9optFiscHorsEIRL;
var declarationAffectation = $p0PLMEP1.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine;

if($p0PLMEP1.cadreEIRLGroup.cadreEIRL.estEIRL and not declarationAffectation.declarationPatrimoine.objetPartiel) {
	formFields['regimeFiscal_regimeImpositionBeneficesVersementLiberatoire_oui']	= declarationAffectation.cadre3OptionsFiscalesEIRL.regimeFiscalRegimeImpositionBeneficesVersementLiberatoire ? true : false;
	formFields['regimeFiscal_regimeImpositionBeneficesVersementLiberatoire_non']	= declarationAffectation.cadre3OptionsFiscalesEIRL.regimeFiscalRegimeImpositionBeneficesVersementLiberatoire ? false : true;
} else {
	formFields['regimeFiscal_regimeImpositionBeneficesVersementLiberatoire_oui']	= $p0PLMEP1.cadre9optFiscHorsEIRLGroup.cadre9optFiscHorsEIRL.dataMicroHorsEIRLIRcalculeSurRecettes ? true : false;
	formFields['regimeFiscal_regimeImpositionBeneficesVersementLiberatoire_non']	= $p0PLMEP1.cadre9optFiscHorsEIRLGroup.cadre9optFiscHorsEIRL.dataMicroHorsEIRLIRcalculeSurRecettes ? false : true;
}

//Cadre 10 - Observations

var correspondance = $p0PLMEP1.cadre10RensCompGroup.cadre10RensComp ;

formFields['formalite_observations']                                                    = correspondance.formaliteObservations;


// Cadre 11 - Adresse de correspondance


formFields['formalite_correspondance_cadre2']                                           = Value('id').of(correspondance.adresseCorrespond).eq('domi') ? true : false;
formFields['formalite_correspondance_cadre5']                                           = Value('id').of(correspondance.adresseCorrespond).eq('prof') ? true : false;
formFields['formalite_correspondance_autre']                                           	= Value('id').of(correspondance.adresseCorrespond).eq('autre') ? true : false;
if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
formFields['formalite_correspondanceAdresse_nom']                                       = correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance != null ? correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance : '';
formFields['formalite_correspondanceAdresse_voie_complement']                           = (correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance : '')
																				        + ' ' + (correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance : '')
																				        + ' ' + (correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance : '')
																						+ ' ' + (correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance  != null ? correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance :'')
                                                                                        + ' ' + (correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance != null ? correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance : '') 
																						+ ' ' + (correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance != null ? correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance : '');
formFields['formalite_correspondanceAdresse_codePostal']                                = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal;
formFields['formalite_correspondanceAdresse_commune']									= correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune;
}
formFields['formalite_telephone1']                                                      = correspondance.infosSup.formaliteTelephone1 != null ? correspondance.infosSup.formaliteTelephone1 : '';
formFields['formalite_telephone2']                                                      = correspondance.infosSup.formaliteTelephone2;
formFields['formalite_fax_courriel']                                                    = correspondance.infosSup.formaliteFaxCourriel != null ? correspondance.infosSup.formaliteFaxCourriel : correspondance.infosSup.telecopie;


//Cadre 12 - Non diffusion

var signataire = $p0PLMEP1.cadre11SignatureGroup.cadre11Signature ;

formFields['formalite_nonDiffusionInformation']                                         = signataire.formaliteNonDiffusionInformation ? true : false;


//Cadre 13 - Signature

formFields['formalite_signataireQualite_declarant']                                  = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFields['formalite_signataireQualite_mandataire']                                 = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFields['nomPrenomDenominationMandataire']										 = signataire.adresseMandataire.nomPrenomDenominationMandataire;	
formFields['adresseMandataire_voie']                                                 = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire + ' ' : '') 
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '');
formFields['formalite_signataire_adresseCP']                                        = signataire.adresseMandataire.dataCodePostalMandataire;
formFields['formalite_signataire_adresseCommune']									= signataire.adresseMandataire.villeAdresseMandataire;
}
formFields['formalite_signatureLieu']                                               = signataire.formaliteSignatureLieu;
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['formalite_signatureDate'] = date;
}
formFields['estEIRL_oui']                                                               = $p0PLMEP1.cadreEIRLGroup.cadreEIRL.estEIRL ? true : false;
formFields['estEIRL_non']                                                               = $p0PLMEP1.cadreEIRLGroup.cadreEIRL.estEIRL ? false : true;
formFields['entreprise_intercalaireP0_nombre']                              = "0";
formFields['signature']                                                     = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce";


// Intercalaire PEIRL

//Cadre 1 - Déclaration ou modification d'affectation de patrimoine

formFields['formulaire_dependance_P0PL']                                                         = false;
formFields['formulaire_dependance_AC0']                                                          = false;
formFields['formulaire_dependance_P2PL']                                                         = false;
formFields['formulaire_dependance_AC2']                                                          = false;
formFields['formulaire_dependance_P0PLME']                                                       = true;
formFields['formulaire_dependance_P4PL']                                                         = false;
formFields['formulaire_dependance_AC4']                                                          = false;
formFields['declaration_initiale']                                                          = $p0PLMEP1.cadreEIRLGroup.cadreEIRL.estEIRL ? true : false;
formFields['declaration_modification']                                                      = false;


// Cadre 2 - Rappel d'identification

formFields['eirl_nomNaissance']                                                  = identite.personneLieePersonnePhysiqueNomNaissance;
formFields['eirl_nomUsage']                                                      = identite.personneLieePersonnePhysiqueNomUsage;
var prenoms=[];
for ( i = 0; i < identite.personneLieePPPrenom.size() ; i++ ){prenoms.push(identite.personneLieePPPrenom[i]);}                            
formFields['eirl_prenom']                                                   = prenoms.toString();

// Cadre 3 - Déclaration d'affectation de patrimoine

formFields['eirl_statutEIRL_declarationInitialeSansDepot']                                  = Value('id').of(declarationAffectation.eirlDepot).eq('eirlSansDepot') ? true : false;
formFields['eirl_statutEIRL_declarationInitialeAvecDepot']                                  = Value('id').of(declarationAffectation.eirlDepot).eq('eirlAvecDepot') ? true : false;
formFields['eirl_statutEIRL_reprise']                                                       = Value('id').of(declarationAffectation.eirlStatut).eq('EirlStatutEIRLReprise') ? true : false;
formFields['eirl_denomination']                                                             = declarationAffectation.declarationPatrimoine.eirlDenomination;
if (declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable !== null) {
    var dateTmp = new Date(parseInt(declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable.getTimeInMillis()));
    var dateClotureEc = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateClotureEc = dateClotureEc.concat(pad(month.toString()));
    formFields['eirl_dateClotureExerciceComptable']          = dateClotureEc;
}
formFields['eirl_objet']                                                                    = declarationAffectation.declarationPatrimoine.objetPartiel ? declarationAffectation.declarationPatrimoine.eirlObjet : $p0PLMEP1.cadre4AdresseActiviteGroup.cadre5EtablissementActivite.etablissementActivites;
if (Value('id').of(declarationAffectation.eirlStatut).eq('EirlStatutEIRLReprise')) {
formFields['eirl_precedentEIRLDenomination']                                                = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLDenomination;
formFields['eirl_precedentEIRLRegistre_rseirl']                                             = true;
formFields['eirl_precedentEIRLRegistre_rsac']                                               = false;
formFields['eirl_precedentEIRLLieuImmatriculation']                                         = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLLieuImmatriculation;
formFields['eirl_precedentEIRLSIREN']                                                       = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN != null ? (declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN.split(' ').join('')) : '';
}
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rsBNC']                             = true;
formFields['eirl_regimeFiscal_regimeImpositionTVA_rfTVA']                                   = true;
formFields['eirl_regimeFiscal_regimeImpositionBeneficesVersementLiberatoire']               = declarationAffectation.cadre3OptionsFiscalesEIRL.regimeFiscalRegimeImpositionBeneficesVersementLiberatoire ? true : false;

/*
 * Création du dossier avec volet social : ajout du cerfa avec volet social
 */
var cerfaDoc1 = nash.doc //
	.load('models/cerfa_13821-07_p0plME_avec_volet_soc.pdf') //
	.apply(formFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));

/*
 * Création du dossier sans volet social : ajout du cerfa sans volet social
 */
 
 var cerfaDoc2 = nash.doc //
	.load('models/cerfa_13821-07_p0plME_sans_volet_soc.pdf') //
	.apply (formFields);
	cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));


/*
 * Ajout de l'intercalaire PEIRL ME avec option fiscale
 */
if ($p0PLMEP1.cadreEIRLGroup.cadreEIRL.estEIRL == true)
{
	var peirlMEDoc = nash.doc //
		.load('models/cerfa1_14218-03_peirl_PL_AC_avec_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(peirlMEDoc.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire PEIRL ME sans option fiscale
 */
if ($p0PLMEP1.cadreEIRLGroup.cadreEIRL.estEIRL == true)
{
	var peirlMEDoc = nash.doc //
		.load('models/cerfa1_14218-03_peirl_PL_AC_sans_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(peirlMEDoc.save('cerfa.pdf'));
}


/*
 * Ajout des PJs
 */

// PJ Déclarant

var pj=$p0PLMEP1.cadre11SignatureGroup.cadre11Signature.soussigne;
var pjUser = [];
var metas = [];

function pushPjPreview(fld) {
	fld.forEach(function (elm) {
        pjUser.push(elm);
        metas.push({'name':'document', 'value': '/'+elm.getAbsolutePath()});
    });
}

if(Value('id').of(pj).contains('FormaliteSignataireQualiteDeclarant')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantSignataire);
}

if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantNonSignataire);
} 

// PJ conjoint

var pj=$p0PLMEP1.cadre2conjointGroup.cadre2Conjoint.statutConjointCollaborateur ;
if(Value('id').of(pj).contains('PersonneLieeConjointCollaborateurSalarieCollaborateur') or Value('id').of(pj).contains('PersonneLieeConjointCollaborateurSalarieSalarie')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjChoixStatutConjoint);
}

// PJ Mandataire

if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire);
}

if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire') and $p0PLMEP1.cadreEIRLGroup.cadreEIRL.estEIRL) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPouvoir);
}

// PJ Eirl

var pj=$p0PLMEP1.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine;
if($p0PLMEP1.cadreEIRLGroup.cadreEIRL.estEIRL) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjAutorisationMineur);
}

if($p0PLMEP1.cadreEIRLGroup.cadreEIRL.estEIRL and not Value('id').of(pj.eirlDepot).eq('eirlSansDepot')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAP);
}

var pj=$p0PLMEP1.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine;
if(Value('id').of(pj.cadreEIRLInformationsComplementaires.contenuDAP).contains('bienImmo') or Value('id').of(pj.cadreEIRLInformationsComplementairesBis.contenuDAPBis).contains('bienImmo')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAPActeNotarie);
}

if(Value('id').of(pj.cadreEIRLInformationsComplementaires.contenuDAP).contains('bienCommunIndivis') or Value('id').of(pj.cadreEIRLInformationsComplementairesBis.contenuDAPBis).contains('bienCommunIndivis')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAPAccordTiers);
}


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDoc = cerfaDoc1.save('PE_PO_PL_ME_Creation.pdf');

// Remove old metas before insert
var metasToDelete = [];
var recordMetas = nash.record.meta() != null ? nash.record.meta().metas : null;
var recordMetasSize = recordMetas != null ? recordMetas.size() : 0;

for (var i = 0; i < recordMetasSize; i++) {
   if (recordMetas.get(i).name == 'document' && !recordMetas.get(i).value.contains("proxy") ) {
       metasToDelete.push({'name':'document', 'value': recordMetas.get(i).value});
   }
}
nash.record.removeMeta(metasToDelete);

nash.record.meta(metas);

var data = [ spec.createData({
    id : 'record',
    label : 'Déclaration de début d\'activité libérale en tant que micro-entrepreneur',
    description : 'Voici le formulaire obtenu à partir des données saisies.',
    type : 'FileReadOnly',
    value : [ finalDoc ]
}),  spec.createData({
    id : 'attachments',
    label : 'Pièces jointes',
    description : 'Pièces jointes ajoutées au formulaire.',
    type : 'FileReadOnly',
    value : pjUser
}) ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Déclaration de début d\'activité libérale en tant que micro-entrepreneur',
    groups : groups
});