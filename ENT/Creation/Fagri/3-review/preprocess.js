function pad(s) {
    return (s < 10) ? '0' + s : s;
}

var pdfFields = {};
var pdfFields2 = {}; //fAgri 2
var pdfFields3 = {}; //fAgri 3
var pdfFields4 = {}; //fAgri 4
var formFieldsPers1 = {}; //Nsp 
var formFieldsPers2 = {}; //Nsp 2
var formFieldsPers3 = {}; //Nsp 3
var formFieldsPers4 = {}; //Nsp 4


var identification = $fAgri.cadreDeclarationECGroup.cadreDeclarationEC;
var membre1 = $fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre1;
var membre2 = $fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre2;
var membre3 = $fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre3;
var membre4 = $fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre4;
var effectif = $fAgri.cadreEffectifSalarieGroup.cadreEffectifSalarie;
var etablissement = $fAgri.cadreDeclarationEtablissementActiviteGroup.cadreDeclarationEtablissementActivite;
var etabSecondaire = $fAgri.cadreDeclarationEtablissementSecondaireActiviteGroup.cadreDeclarationEtablissementSecondaireActivite;
var bailleur = $fAgri.miseLocationBienRurauxGroup.miseLocationBienRurauxInformations;
var fiscal = $fAgri.cadreOptionsFiscalesGroup.cadreOptionsFiscales;
var infoCpt = $fAgri.cadre20RensCompGroup.cadre20RensComp;
var signature = $fAgri.cadre21SignatureGroup.cadre21Signature;
var nsp1 = $fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre1.membreDeclarationSociale; 
var nsp2 = $fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre2.membreDeclarationSociale; 
var nsp3 = $fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre3.membreDeclarationSociale; 
var nsp4 = $fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre4.membreDeclarationSociale; 
/* var membre3 = $fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre3; */

//Déclaration de constitution d'une exploitation en commun - "F Agricole"
//Cadre 1
pdfFields['creationExploitationCommun'] = true;
pdfFields['societeFait']  = Value('id').of(identification.entrepriseFormeJuridiqueConstitution).eq('societeFait') ? true : false ;
pdfFields['societeParticipation'] = Value('id').of(identification.entrepriseFormeJuridiqueConstitution).eq('societeParticipation') ? true : false ;
pdfFields['indivision']  = Value('id').of(identification.entrepriseFormeJuridiqueConstitution).eq('indivision') ? true : false ;
//Cadre 2 Identification de l'exploitation                                                                                          
pdfFields['entrepriseNom']  = identification.entrepriseNom;
pdfFields['siegeAdresse1']  = (identification.siegeAdresseNumeroVoie != null ? identification.siegeAdresseNumeroVoie : "")
+ ' ' + (identification.siegeAdresseIndiceVoie != null ? identification.siegeAdresseIndiceVoie : "")
+ ' ' + (identification.siegeAdresseTypeVoie != null ? identification.siegeAdresseTypeVoie : "")
+ ' ' + identification.siegeAdresseNomVoie ;
pdfFields['siegeAdresse2']   = (identification.siegeAdresseComplementVoie != null ? identification.siegeAdresseComplementVoie : '')
+ ' ' + (identification.siegeADresseDistributionSpeciale  != null ? identification.siegeADresseDistributionSpeciale : '');
pdfFields['siegeAdresseCodePostal']  = identification.siegeAdresseCodePostal;
pdfFields['siegeAdresseCommune'] = identification.siegeAdresseCommune;
pdfFields['siegeAdresseCommuneAncienne']   = identification.siegeAdresseCommuneAncienne != null ? identification.siegeAdresseCommuneAncienne : '';

//Cadre 2 Établissement supplémentaire 
if (identification.etabSupp) {
//Cadre 1
pdfFields2['creationExploitationCommun'] = true;
pdfFields2['societeFait']  = Value('id').of(identification.entrepriseFormeJuridiqueConstitution).eq('societeFait') ? true : false ;
pdfFields2['societeParticipation'] = Value('id').of(identification.entrepriseFormeJuridiqueConstitution).eq('societeParticipation') ? true : false ;
pdfFields2['indivision']  = Value('id').of(identification.entrepriseFormeJuridiqueConstitution).eq('indivision') ? true : false ;   
pdfFields2['intercalaireSuiteImprimeF'] = true ;   
//Cadre 2 Identification de l'exploitation 
pdfFields2['siegeAdresse1']  = (identification.etabSuppNumeroVoie != null ? identification.etabSuppNumeroVoie : "")
+ ' ' + (identification.etabSuppIndiceVoie != null ? identification.etabSuppIndiceVoie : "")
+ ' ' + (identification.etabSuppTypeVoie != null ? identification.etabSuppTypeVoie : "")
+ ' ' + identification.etabSuppNomVoie ;
pdfFields2['siegeAdresse2']   = (identification.etabSuppComplementVoie != null ? identification.etabSuppComplementVoie : '')
+ ' ' + (identification.etabSuppDistributionSpeciale  != null ? identification.etabSuppDistributionSpeciale : '');
pdfFields2['siegeAdresseCodePostal']  = identification.etabSuppCodePostal != null ? identification.etabSuppCodePostal : '';
pdfFields2['siegeAdresseCommune'] = identification.etabSuppCommune != null ? identification.etabSuppCommune : '';
pdfFields2['siegeAdresseCommuneAncienne']   = identification.etabSuppCommuneAncienne != null ? identification.etabSuppCommuneAncienne : '';
//Cadre 3 Activité principale exercée de l'établissement supplémentaire
pdfFields2['activitePrincipaleExercee']   = identification.etabSuppActivitePrincipaleExercee != null ? identification.etabSuppActivitePrincipaleExercee : '';
}
//Cadre 3 Activité principale exercée
pdfFields['activitePrincipaleExercee']   = identification.activitePrincipaleExercee;
//Cadre 4  - MEMBRES 
// 1er membre Personne Physique
if (membre1.membrePersonnePhysique.personneNomNaissance != null) {
    pdfFields['nouveauPP[0]']  = membre1.membrePersonnePhysique.personneNomNaissance != null ? true : false ;  
    pdfFields['activiteExerceeAnterieureSIREN[0]'] = membre1.membrePersonnePhysique.activiteExerceeAnterieureSIREN != null ? membre1.membrePersonnePhysique.activiteExerceeAnterieureSIREN.split(' ').join('') : '';  
    pdfFields['personneNomNaissance[0]'] = membre1.membrePersonnePhysique.personneNomNaissance != null ? membre1.membrePersonnePhysique.personneNomNaissance : '';
    pdfFields['personneNomUsage[0]'] = membre1.membrePersonnePhysique.personneNomUsage != null ? membre1.membrePersonnePhysique.personneNomUsage : '';
        if (membre1.membrePersonnePhysique.personnePrenom != null and membre1.membrePersonnePhysique.personnePrenom[0].length > 0) {
            var prenoms = [];
            for (i = 0; i < membre1.membrePersonnePhysique.personnePrenom.size(); i++) {
                prenoms.push(membre1.membrePersonnePhysique.personnePrenom[i]);
            }
        pdfFields['personnePrenoms[0]'] =  prenoms.toString();
        }
        if (membre1.membrePersonnePhysique.personneDateNaissance != null) {
            var dateTmp = new Date(parseInt(membre1.membrePersonnePhysique.personneDateNaissance.getTimeInMillis()));
            var date1 = pad(dateTmp.getDate().toString());
            var month = dateTmp.getMonth() + 1;
            date1 = date1.concat(pad(month.toString()));
            date1 = date1.concat(dateTmp.getFullYear().toString());
            pdfFields['personneDateNaissance[0]'] = date1;
        }
    pdfFields['personneLieuNaissanceDepartement[0]'] = membre1.membrePersonnePhysique.personneLieuNaissanceDepartement != null ? membre1.membrePersonnePhysique.personneLieuNaissanceDepartement.getId() : '';
    pdfFields['personneLieuNaissanceCommune[0]'] = membre1.membrePersonnePhysique.personneLieuNaissanceVille != null ? (membre1.membrePersonnePhysique.personneLieuNaissanceVille + '/' + membre1.membrePersonnePhysique.personneLieuNaissancePays) : (membre1.membrePersonnePhysique.personneLieuNaissanceCommune != null ? membre1.membrePersonnePhysique.personneLieuNaissanceCommune : '');
    pdfFields['personneLieeAdresse[0]'] = 
        (membre1.membreAdresse.personneLieeAdresseNumeroVoie != null ? membre1.membreAdresse.personneLieeAdresseNumeroVoie : '')
        + ' ' + (membre1.membreAdresse.personneLieeAdresseIndiceVoie != null ? membre1.membreAdresse.personneLieeAdresseIndiceVoie : '')
        + ' ' + (membre1.membreAdresse.personneLieeAdresseTypeVoie != null ? membre1.membreAdresse.personneLieeAdresseTypeVoie : '')
        + ' ' + (membre1.membreAdresse.personneLieeAdresseNomVoie != null ? membre1.membreAdresse.personneLieeAdresseNomVoie : '')
        + ' ' + (membre1.membreAdresse.personneLieeAdresseComplementVoie != null ? membre1.membreAdresse.personneLieeAdresseComplementVoie : '')
        + ' ' + (membre1.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie != null ? membre1.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie : '');
    pdfFields['personneLieeAdresseCodePostal[0]'] = membre1.membreAdresse.personneLieeAdresseCodePostal != null ? membre1.membreAdresse.personneLieeAdresseCodePostal : '';
    pdfFields['personneLieeAdresseCommune[0]'] = membre1.membreAdresse.personneLieeAdresseCommune != null ? membre1.membreAdresse.personneLieeAdresseCommune : '';
        
}
//2ème membre Personne physique
if (membre2.membrePersonnePhysique.personneNomNaissance != null) {
    if (membre1.membrePersonneMorale.personneLieePMDenomination != null) {
    pdfFields['nouveauPP[0]']  = membre2.membrePersonnePhysique.personneNomNaissance != null ? true : false ;  
    pdfFields['activiteExerceeAnterieureSIREN[0]'] = membre2.membrePersonnePhysique.activiteExerceeAnterieureSIREN != null ? membre2.membrePersonnePhysique.activiteExerceeAnterieureSIREN.split(' ').join('') : '' ; 
    pdfFields['personneNomNaissance[0]'] = membre2.membrePersonnePhysique.personneNomNaissance != null ? membre2.membrePersonnePhysique.personneNomNaissance : '';
    pdfFields['personneNomUsage[0]'] = membre2.membrePersonnePhysique.personneNomUsage != null ? membre2.membrePersonnePhysique.personneNomUsage : '';
            if (membre2.membrePersonnePhysique.personnePrenom != null and membre2.membrePersonnePhysique.personnePrenom[0].length > 0) {
                var prenoms = [];
                for (i = 0; i < membre2.membrePersonnePhysique.personnePrenom.size(); i++) {
                    prenoms.push(membre2.membrePersonnePhysique.personnePrenom[i]);
                }
    pdfFields['personnePrenoms[0]'] =  prenoms.toString();
            }
            if (membre2.membrePersonnePhysique.personneDateNaissance != null) {
                var dateTmp = new Date(parseInt(membre2.membrePersonnePhysique.personneDateNaissance.getTimeInMillis()));
                var date1 = pad(dateTmp.getDate().toString());
                var month = dateTmp.getMonth() + 1;
                date1 = date1.concat(pad(month.toString()));
                date1 = date1.concat(dateTmp.getFullYear().toString());
                pdfFields['personneDateNaissance[0]'] = date1;
            }
    pdfFields['personneLieuNaissanceDepartement[0]'] = membre2.membrePersonnePhysique.personneLieuNaissanceDepartement != null ? membre2.membrePersonnePhysique.personneLieuNaissanceDepartement.getId() : '';
    pdfFields['personneLieuNaissanceCommune[0]'] = membre2.membrePersonnePhysique.personneLieuNaissanceVille != null ? (membre2.membrePersonnePhysique.personneLieuNaissanceVille + '/' + membre2.membrePersonnePhysique.personneLieuNaissancePays) : (membre2.membrePersonnePhysique.personneLieuNaissanceCommune != null ? membre2.membrePersonnePhysique.personneLieuNaissanceCommune : '');
    pdfFields['personneLieeAdresse[0]'] = 
        (membre2.membreAdresse.personneLieeAdresseNumeroVoie != null ? membre2.membreAdresse.personneLieeAdresseNumeroVoie : '')
        + ' ' + (membre2.membreAdresse.personneLieeAdresseIndiceVoie != null ? membre2.membreAdresse.personneLieeAdresseIndiceVoie : '')
        + ' ' + (membre2.membreAdresse.personneLieeAdresseTypeVoie != null ? membre2.membreAdresse.personneLieeAdresseTypeVoie : '')
        + ' ' + (membre2.membreAdresse.personneLieeAdresseNomVoie != null ? membre2.membreAdresse.personneLieeAdresseNomVoie : '')
        + ' ' + (membre2.membreAdresse.personneLieeAdresseComplementVoie != null ? membre2.membreAdresse.personneLieeAdresseComplementVoie : '')
        + ' ' + (membre2.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie != null ? membre2.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie : '');
    pdfFields['personneLieeAdresseCodePostal[0]'] = membre2.membreAdresse.personneLieeAdresseCodePostal != null ? membre2.membreAdresse.personneLieeAdresseCodePostal : '';
    pdfFields['personneLieeAdresseCommune[0]'] = membre2.membreAdresse.personneLieeAdresseCommune != null ? membre2.membreAdresse.personneLieeAdresseCommune : '';
    } else { 
    pdfFields['nouveauPP[1]']  = membre2.membrePersonnePhysique.personneNomNaissance != null ? true : false ;  
    pdfFields['activiteExerceeAnterieureSIREN[1]'] = membre2.membrePersonnePhysique.activiteExerceeAnterieureSIREN != null ? membre2.membrePersonnePhysique.activiteExerceeAnterieureSIREN.split(' ').join('') : '' ; 
    pdfFields['personneNomNaissance[1]'] = membre2.membrePersonnePhysique.personneNomNaissance != null ? membre2.membrePersonnePhysique.personneNomNaissance : '';
    pdfFields['personneNomUsage[1]'] = membre2.membrePersonnePhysique.personneNomUsage != null ? membre2.membrePersonnePhysique.personneNomUsage : '';
        if (membre2.membrePersonnePhysique.personnePrenom != null and membre2.membrePersonnePhysique.personnePrenom[0].length > 0) {
            var prenoms = [];
            for (i = 0; i < membre2.membrePersonnePhysique.personnePrenom.size(); i++) {
                prenoms.push(membre2.membrePersonnePhysique.personnePrenom[i]);
            }
        pdfFields['personnePrenoms[1]'] =  prenoms.toString();
        }
        if (membre2.membrePersonnePhysique.personneDateNaissance != null) {
            var dateTmp = new Date(parseInt(membre2.membrePersonnePhysique.personneDateNaissance.getTimeInMillis()));
            var date1 = pad(dateTmp.getDate().toString());
            var month = dateTmp.getMonth() + 1;
            date1 = date1.concat(pad(month.toString()));
            date1 = date1.concat(dateTmp.getFullYear().toString());
            pdfFields['personneDateNaissance[1]'] = date1;
        }
    pdfFields['personneLieuNaissanceDepartement[1]'] = membre2.membrePersonnePhysique.personneLieuNaissanceDepartement != null ? membre2.membrePersonnePhysique.personneLieuNaissanceDepartement.getId() : '';
    pdfFields['personneLieuNaissanceCommune[1]'] = membre2.membrePersonnePhysique.personneLieuNaissanceVille != null ? (membre2.membrePersonnePhysique.personneLieuNaissanceVille + '/' + membre2.membrePersonnePhysique.personneLieuNaissancePays) : (membre2.membrePersonnePhysique.personneLieuNaissanceCommune != null ? membre2.membrePersonnePhysique.personneLieuNaissanceCommune : '');
    pdfFields['personneLieeAdresse[1]'] = 
    (membre2.membreAdresse.personneLieeAdresseNumeroVoie != null ? membre2.membreAdresse.personneLieeAdresseNumeroVoie : '')
    + ' ' + (membre2.membreAdresse.personneLieeAdresseIndiceVoie != null ? membre2.membreAdresse.personneLieeAdresseIndiceVoie : '')
    + ' ' + (membre2.membreAdresse.personneLieeAdresseTypeVoie != null ? membre2.membreAdresse.personneLieeAdresseTypeVoie : '')
    + ' ' + (membre2.membreAdresse.personneLieeAdresseNomVoie != null ? membre2.membreAdresse.personneLieeAdresseNomVoie : '')
    + ' ' + (membre2.membreAdresse.personneLieeAdresseComplementVoie != null ? membre2.membreAdresse.personneLieeAdresseComplementVoie : '')
    + ' ' + (membre2.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie != null ? membre2.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie : '');
    pdfFields['personneLieeAdresseCodePostal[1]'] = membre2.membreAdresse.personneLieeAdresseCodePostal != null ? membre2.membreAdresse.personneLieeAdresseCodePostal : '';
    pdfFields['personneLieeAdresseCommune[1]'] = membre2.membreAdresse.personneLieeAdresseCommune != null ? membre2.membreAdresse.personneLieeAdresseCommune : '';
    }
}
//Si 3ème membre Personne physique sur F n°2
//if (Value('id').of(membre3.personnaliteJuridique).eq('personnePhysique')) {
if (membre3.membrePersonnePhysique.personneNomNaissance != null) {
    if ((membre1.membrePersonneMorale.personneLieePMDenomination != null) and (membre2.membrePersonneMorale.personneLieePMDenomination != null) ) {
    pdfFields['nouveauPP[0]']  = membre3.membrePersonnePhysique.personneNomNaissance != null ? true : false ;  
    pdfFields['activiteExerceeAnterieureSIREN[0]'] = membre3.membrePersonnePhysique.activiteExerceeAnterieureSIREN != null ? membre3.membrePersonnePhysique.activiteExerceeAnterieureSIREN.split(' ').join('') : '' ; 
    pdfFields['personneNomNaissance[0]'] = membre3.membrePersonnePhysique.personneNomNaissance != null ? membre3.membrePersonnePhysique.personneNomNaissance : '';
    pdfFields['personneNomUsage[0]'] = membre3.membrePersonnePhysique.personneNomUsage != null ? membre3.membrePersonnePhysique.personneNomUsage : '';
        if ((membre3.membrePersonnePhysique.personnePrenom != null) and (membre3.membrePersonnePhysique.personnePrenom[0].length > 0)) {
            var prenoms = [];
            for (i = 0; i < membre3.membrePersonnePhysique.personnePrenom.size(); i++) {
                prenoms.push(membre3.membrePersonnePhysique.personnePrenom[i]);
            }
            pdfFields['personnePrenoms[0]'] =  prenoms.toString();
        }
        if (membre3.membrePersonnePhysique.personneDateNaissance != null) {
            var dateTmp = new Date(parseInt(membre3.membrePersonnePhysique.personneDateNaissance.getTimeInMillis()));
            var date1 = pad(dateTmp.getDate().toString());
            var month = dateTmp.getMonth() + 1;
            date1 = date1.concat(pad(month.toString()));
            date1 = date1.concat(dateTmp.getFullYear().toString());
            pdfFields['personneDateNaissance[0]'] = date1;
        }
        pdfFields['personneLieuNaissanceDepartement[0]'] = membre3.membrePersonnePhysique.personneLieuNaissanceDepartement != null ? membre3.membrePersonnePhysique.personneLieuNaissanceDepartement.getId() : '';
        pdfFields['personneLieuNaissanceCommune[0]'] = membre3.membrePersonnePhysique.personneLieuNaissanceVille != null ? (membre3.membrePersonnePhysique.personneLieuNaissanceVille + '/' + membre3.membrePersonnePhysique.personneLieuNaissancePays) : (membre3.membrePersonnePhysique.personneLieuNaissanceCommune != null ? membre3.membrePersonnePhysique.personneLieuNaissanceCommune : '');
        pdfFields['personneLieeAdresse[0]'] = 
    (membre3.membreAdresse.personneLieeAdresseNumeroVoie != null ? membre3.membreAdresse.personneLieeAdresseNumeroVoie : '')
    + ' ' + (membre3.membreAdresse.personneLieeAdresseIndiceVoie != null ? membre3.membreAdresse.personneLieeAdresseIndiceVoie : '')
    + ' ' + (membre3.membreAdresse.personneLieeAdresseTypeVoie != null ? membre3.membreAdresse.personneLieeAdresseTypeVoie : '')
    + ' ' + (membre3.membreAdresse.personneLieeAdresseNomVoie != null ? membre3.membreAdresse.personneLieeAdresseNomVoie : '')
    + ' ' + (membre3.membreAdresse.personneLieeAdresseComplementVoie != null ? membre3.membreAdresse.personneLieeAdresseComplementVoie : '')
    + ' ' + (membre3.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie != null ? membre3.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie : '');
    pdfFields['personneLieeAdresseCodePostal[0]'] = membre3.membreAdresse.personneLieeAdresseCodePostal != null ? membre3.membreAdresse.personneLieeAdresseCodePostal : '';
    pdfFields['personneLieeAdresseCommune[0]'] = membre3.membreAdresse.personneLieeAdresseCommune != null ? membre3.membreAdresse.personneLieeAdresseCommune : '';
} else if ((membre1.membrePersonneMorale.personneLieePMDenomination != null) and (membre2.membrePersonnePhysique.personneNomNaissance != null) ) {
    pdfFields['nouveauPP[1]']  = membre3.membrePersonnePhysique.personneNomNaissance != null ? true : false ;  
    pdfFields['activiteExerceeAnterieureSIREN[1]'] = membre3.membrePersonnePhysique.activiteExerceeAnterieureSIREN != null ? membre3.membrePersonnePhysique.activiteExerceeAnterieureSIREN.split(' ').join('') : '' ; 
    pdfFields['personneNomNaissance[1]'] = membre3.membrePersonnePhysique.personneNomNaissance != null ? membre3.membrePersonnePhysique.personneNomNaissance : '';
    pdfFields['personneNomUsage[1]'] = membre3.membrePersonnePhysique.personneNomUsage != null ? membre3.membrePersonnePhysique.personneNomUsage : '';
        if ((membre3.membrePersonnePhysique.personnePrenom != null) and (membre3.membrePersonnePhysique.personnePrenom[0].length > 0)) {
            var prenoms = [];
            for (i = 0; i < membre3.membrePersonnePhysique.personnePrenom.size(); i++) {
                prenoms.push(membre3.membrePersonnePhysique.personnePrenom[i]);
            }
            pdfFields['personnePrenoms[1]'] =  prenoms.toString();
        }
        if (membre3.membrePersonnePhysique.personneDateNaissance != null) {
            var dateTmp = new Date(parseInt(membre3.membrePersonnePhysique.personneDateNaissance.getTimeInMillis()));
            var date1 = pad(dateTmp.getDate().toString());
            var month = dateTmp.getMonth() + 1;
            date1 = date1.concat(pad(month.toString()));
            date1 = date1.concat(dateTmp.getFullYear().toString());
            pdfFields['personneDateNaissance[1]'] = date1;
        }
        pdfFields['personneLieuNaissanceDepartement[1]'] = membre3.membrePersonnePhysique.personneLieuNaissanceDepartement != null ? membre3.membrePersonnePhysique.personneLieuNaissanceDepartement.getId() : '';
        pdfFields['personneLieuNaissanceCommune[1]'] = membre3.membrePersonnePhysique.personneLieuNaissanceVille != null ? (membre3.membrePersonnePhysique.personneLieuNaissanceVille + '/' + membre3.membrePersonnePhysique.personneLieuNaissancePays) : (membre3.membrePersonnePhysique.personneLieuNaissanceCommune != null ? membre3.membrePersonnePhysique.personneLieuNaissanceCommune : '');
        pdfFields['personneLieeAdresse[1]'] = 
    (membre3.membreAdresse.personneLieeAdresseNumeroVoie != null ? membre3.membreAdresse.personneLieeAdresseNumeroVoie : '')
    + ' ' + (membre3.membreAdresse.personneLieeAdresseIndiceVoie != null ? membre3.membreAdresse.personneLieeAdresseIndiceVoie : '')
    + ' ' + (membre3.membreAdresse.personneLieeAdresseTypeVoie != null ? membre3.membreAdresse.personneLieeAdresseTypeVoie : '')
    + ' ' + (membre3.membreAdresse.personneLieeAdresseNomVoie != null ? membre3.membreAdresse.personneLieeAdresseNomVoie : '')
    + ' ' + (membre3.membreAdresse.personneLieeAdresseComplementVoie != null ? membre3.membreAdresse.personneLieeAdresseComplementVoie : '')
    + ' ' + (membre3.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie != null ? membre3.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie : '');
    pdfFields['personneLieeAdresseCodePostal[1]'] = membre3.membreAdresse.personneLieeAdresseCodePostal != null ? membre3.membreAdresse.personneLieeAdresseCodePostal : '';
    pdfFields['personneLieeAdresseCommune[1]'] = membre3.membreAdresse.personneLieeAdresseCommune != null ? membre3.membreAdresse.personneLieeAdresseCommune : '';
} else {
   if (not identification.etabSupp){
    pdfFields2['intercalaireSuiteImprimeF'] =  true;  
   }
    pdfFields2['nouveauPP[0]']  = membre3.membrePersonnePhysique.personneNomNaissance != null ? true : false ;  
    pdfFields2['activiteExerceeAnterieureSIREN[0]'] = membre3.membrePersonnePhysique.activiteExerceeAnterieureSIREN != null ? membre3.membrePersonnePhysique.activiteExerceeAnterieureSIREN.split(' ').join('') : '';  
    pdfFields2['personneNomNaissance[0]'] = membre3.membrePersonnePhysique.personneNomNaissance != null ? membre3.membrePersonnePhysique.personneNomNaissance : '';
    pdfFields2['personneNomUsage[0]'] = membre3.membrePersonnePhysique.personneNomUsage != null ? membre3.membrePersonnePhysique.personneNomUsage : '';
        if (membre3.membrePersonnePhysique.personnePrenom != null and membre3.membrePersonnePhysique.personnePrenom[0].length > 0) {
            var prenoms = [];
            for (i = 0; i < membre3.membrePersonnePhysique.personnePrenom.size(); i++) {
                prenoms.push(membre3.membrePersonnePhysique.personnePrenom[i]);
            }
        pdfFields2['personnePrenoms[0]'] =  prenoms.toString();
        }
        if (membre3.membrePersonnePhysique.personneDateNaissance != null) {
            var dateTmp = new Date(parseInt(membre3.membrePersonnePhysique.personneDateNaissance.getTimeInMillis()));
            var date1 = pad(dateTmp.getDate().toString());
            var month = dateTmp.getMonth() + 1;
            date1 = date1.concat(pad(month.toString()));
            date1 = date1.concat(dateTmp.getFullYear().toString());
            pdfFields2['personneDateNaissance[0]'] = date1;
        }
    pdfFields2['personneLieuNaissanceDepartement[0]'] = membre3.membrePersonnePhysique.personneLieuNaissanceDepartement != null ? membre3.membrePersonnePhysique.personneLieuNaissanceDepartement.getId() : '';
    pdfFields2['personneLieuNaissanceCommune[0]'] = membre3.membrePersonnePhysique.personneLieuNaissanceVille != null ? (membre3.membrePersonnePhysique.personneLieuNaissanceVille + '/' + membre3.membrePersonnePhysique.personneLieuNaissancePays) : (membre3.membrePersonnePhysique.personneLieuNaissanceCommune != null ? membre3.membrePersonnePhysique.personneLieuNaissanceCommune : '');
    pdfFields2['personneLieeAdresse[0]'] = 
        (membre3.membreAdresse.personneLieeAdresseNumeroVoie != null ? membre3.membreAdresse.personneLieeAdresseNumeroVoie : '')
        + ' ' + (membre3.membreAdresse.personneLieeAdresseIndiceVoie != null ? membre3.membreAdresse.personneLieeAdresseIndiceVoie : '')
        + ' ' + (membre3.membreAdresse.personneLieeAdresseTypeVoie != null ? membre3.membreAdresse.personneLieeAdresseTypeVoie : '')
        + ' ' + (membre3.membreAdresse.personneLieeAdresseNomVoie != null ? membre3.membreAdresse.personneLieeAdresseNomVoie : '')
        + ' ' + (membre3.membreAdresse.personneLieeAdresseComplementVoie != null ? membre3.membreAdresse.personneLieeAdresseComplementVoie : '')
        + ' ' + (membre3.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie != null ? membre3.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie : '');
    pdfFields2['personneLieeAdresseCodePostal[0]'] = membre3.membreAdresse.personneLieeAdresseCodePostal != null ? membre3.membreAdresse.personneLieeAdresseCodePostal : '';
    pdfFields2['personneLieeAdresseCommune[0]'] = membre3.membreAdresse.personneLieeAdresseCommune != null ? membre3.membreAdresse.personneLieeAdresseCommune : '';
    }        
}
//Si 4ème membre Personne physique sur F n°2
//if (Value('id').of(membre4.personnaliteJuridique).eq('personnePhysique')) {
if (membre4.membrePersonnePhysique.personneNomNaissance != null) {
    if ((membre1.membrePersonneMorale.personneLieePMDenomination != null)
    and (membre2.membrePersonneMorale.personneLieePMDenomination != null)
    and (membre3.membrePersonneMorale.personneLieePMDenomination  != null)) {
pdfFields['nouveauPP[0]']  = membre4.membrePersonnePhysique.personneNomNaissance != null ? true : false ;  
pdfFields['activiteExerceeAnterieureSIREN[0]'] = membre4.membrePersonnePhysique.activiteExerceeAnterieureSIREN != null ? membre4.membrePersonnePhysique.activiteExerceeAnterieureSIREN.split(' ').join('') : '' ; 
pdfFields['personneNomNaissance[0]'] = membre4.membrePersonnePhysique.personneNomNaissance != null ? membre4.membrePersonnePhysique.personneNomNaissance : '';
pdfFields['personneNomUsage[0]'] = membre4.membrePersonnePhysique.personneNomUsage != null ? membre4.membrePersonnePhysique.personneNomUsage : '';
    if (membre4.membrePersonnePhysique.personnePrenom != null and membre4.membrePersonnePhysique.personnePrenom[0].length > 0) {
        var prenoms = [];
        for (i = 0; i < membre4.membrePersonnePhysique.personnePrenom.size(); i++) {
            prenoms.push(membre4.membrePersonnePhysique.personnePrenom[i]);
        }
        pdfFields['personnePrenoms[0]'] =  prenoms.toString();
    }
    if (membre4.membrePersonnePhysique.personneDateNaissance != null) {
        var dateTmp = new Date(parseInt(membre4.membrePersonnePhysique.personneDateNaissance.getTimeInMillis()));
        var date1 = pad(dateTmp.getDate().toString());
        var month = dateTmp.getMonth() + 1;
        date1 = date1.concat(pad(month.toString()));
        date1 = date1.concat(dateTmp.getFullYear().toString());
        pdfFields['personneDateNaissance[0]'] = date1;
    }
    pdfFields['personneLieuNaissanceDepartement[0]'] = membre4.membrePersonnePhysique.personneLieuNaissanceDepartement != null ? membre4.membrePersonnePhysique.personneLieuNaissanceDepartement.getId() : '';
    pdfFields['personneLieuNaissanceCommune[0]'] = membre4.membrePersonnePhysique.personneLieuNaissanceVille != null ? (membre4.membrePersonnePhysique.personneLieuNaissanceVille + '/' + membre4.membrePersonnePhysique.personneLieuNaissancePays) : (membre4.membrePersonnePhysique.personneLieuNaissanceCommune != null ? membre4.membrePersonnePhysique.personneLieuNaissanceCommune : '');
    pdfFields['personneLieeAdresse[0]'] = 
(membre4.membreAdresse.personneLieeAdresseNumeroVoie != null ? membre4.membreAdresse.personneLieeAdresseNumeroVoie : '')
+ ' ' + (membre4.membreAdresse.personneLieeAdresseIndiceVoie != null ? membre4.membreAdresse.personneLieeAdresseIndiceVoie : '')
+ ' ' + (membre4.membreAdresse.personneLieeAdresseTypeVoie != null ? membre4.membreAdresse.personneLieeAdresseTypeVoie : '')
+ ' ' + (membre4.membreAdresse.personneLieeAdresseNomVoie != null ? membre4.membreAdresse.personneLieeAdresseNomVoie : '')
+ ' ' + (membre4.membreAdresse.personneLieeAdresseComplementVoie != null ? membre4.membreAdresse.personneLieeAdresseComplementVoie : '')
+ ' ' + (membre4.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie != null ? membre4.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie : '');
pdfFields['personneLieeAdresseCodePostal[0]'] = membre4.membreAdresse.personneLieeAdresseCodePostal != null ? membre4.membreAdresse.personneLieeAdresseCodePostal : '';
pdfFields['personneLieeAdresseCommune[0]'] = membre4.membreAdresse.personneLieeAdresseCommune != null ? membre4.membreAdresse.personneLieeAdresseCommune : '';
} else if ((membre1.membrePersonneMorale.personneLieePMDenomination != null)
        and (membre2.membrePersonneMorale.personneLieePMDenomination != null)
        and (membre3.membrePersonnePhysique.personneNomNaissance  != null)) {
    pdfFields['nouveauPP[1]']  = membre4.membrePersonnePhysique.personneNomNaissance != null ? true : false ;  
    pdfFields['activiteExerceeAnterieureSIREN[1]'] = membre4.membrePersonnePhysique.activiteExerceeAnterieureSIREN != null ? membre4.membrePersonnePhysique.activiteExerceeAnterieureSIREN.split(' ').join('') : '' ; 
    pdfFields['personneNomNaissance[1]'] = membre4.membrePersonnePhysique.personneNomNaissance != null ? membre4.membrePersonnePhysique.personneNomNaissance : '';
    pdfFields['personneNomUsage[1]'] = membre4.membrePersonnePhysique.personneNomUsage != null ? membre4.membrePersonnePhysique.personneNomUsage : '';
        if (membre4.membrePersonnePhysique.personnePrenom != null and membre4.membrePersonnePhysique.personnePrenom[0].length > 0) {
            var prenoms = [];
            for (i = 0; i < membre4.membrePersonnePhysique.personnePrenom.size(); i++) {
                prenoms.push(membre4.membrePersonnePhysique.personnePrenom[i]);
            }
            pdfFields['personnePrenoms[1]'] =  prenoms.toString();
        }
        if (membre4.membrePersonnePhysique.personneDateNaissance != null) {
            var dateTmp = new Date(parseInt(membre4.membrePersonnePhysique.personneDateNaissance.getTimeInMillis()));
            var date1 = pad(dateTmp.getDate().toString());
            var month = dateTmp.getMonth() + 1;
            date1 = date1.concat(pad(month.toString()));
            date1 = date1.concat(dateTmp.getFullYear().toString());
            pdfFields['personneDateNaissance[1]'] = date1;
        }
        pdfFields['personneLieuNaissanceDepartement[1]'] = membre4.membrePersonnePhysique.personneLieuNaissanceDepartement != null ? membre4.membrePersonnePhysique.personneLieuNaissanceDepartement.getId() : '';
        pdfFields['personneLieuNaissanceCommune[1]'] = membre4.membrePersonnePhysique.personneLieuNaissanceVille != null ? (membre4.membrePersonnePhysique.personneLieuNaissanceVille + '/' + membre4.membrePersonnePhysique.personneLieuNaissancePays) : (membre4.membrePersonnePhysique.personneLieuNaissanceCommune != null ? membre4.membrePersonnePhysique.personneLieuNaissanceCommune : '');
        pdfFields['personneLieeAdresse[1]'] = 
    (membre4.membreAdresse.personneLieeAdresseNumeroVoie != null ? membre4.membreAdresse.personneLieeAdresseNumeroVoie : '')
    + ' ' + (membre4.membreAdresse.personneLieeAdresseIndiceVoie != null ? membre4.membreAdresse.personneLieeAdresseIndiceVoie : '')
    + ' ' + (membre4.membreAdresse.personneLieeAdresseTypeVoie != null ? membre4.membreAdresse.personneLieeAdresseTypeVoie : '')
    + ' ' + (membre4.membreAdresse.personneLieeAdresseNomVoie != null ? membre4.membreAdresse.personneLieeAdresseNomVoie : '')
    + ' ' + (membre4.membreAdresse.personneLieeAdresseComplementVoie != null ? membre4.membreAdresse.personneLieeAdresseComplementVoie : '')
    + ' ' + (membre4.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie != null ? membre4.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie : '');
    pdfFields['personneLieeAdresseCodePostal[1]'] = membre4.membreAdresse.personneLieeAdresseCodePostal != null ? membre4.membreAdresse.personneLieeAdresseCodePostal : '';
    pdfFields['personneLieeAdresseCommune[1]'] = membre4.membreAdresse.personneLieeAdresseCommune != null ? membre4.membreAdresse.personneLieeAdresseCommune : '';
    } else {
    pdfFields2['intercalaireSuiteImprimeF'] =  true; 
    pdfFields2['nouveauPP[1]']  = membre4.membrePersonnePhysique.personneNomNaissance != null ? true : false ;  
    pdfFields2['activiteExerceeAnterieureSIREN[1]'] = membre4.membrePersonnePhysique.activiteExerceeAnterieureSIREN != null ? membre4.membrePersonnePhysique.activiteExerceeAnterieureSIREN.split(' ').join('') : '' ; 
    pdfFields2['personneNomNaissance[1]'] = membre4.membrePersonnePhysique.personneNomNaissance != null ? membre4.membrePersonnePhysique.personneNomNaissance : '';
    pdfFields2['personneNomUsage[1]'] = membre4.membrePersonnePhysique.personneNomUsage != null ? membre4.membrePersonnePhysique.personneNomUsage : '';
        if (membre4.membrePersonnePhysique.personnePrenom != null and membre4.membrePersonnePhysique.personnePrenom[0].length > 0) {
            var prenoms = [];
            for (i = 0; i < membre4.membrePersonnePhysique.personnePrenom.size(); i++) {
                prenoms.push(membre4.membrePersonnePhysique.personnePrenom[i]);
            }
        pdfFields2['personnePrenoms[1]'] =  prenoms.toString();
        }
        if (membre4.membrePersonnePhysique.personneDateNaissance != null) {
            var dateTmp = new Date(parseInt(membre4.membrePersonnePhysique.personneDateNaissance.getTimeInMillis()));
            var date1 = pad(dateTmp.getDate().toString());
            var month = dateTmp.getMonth() + 1;
            date1 = date1.concat(pad(month.toString()));
            date1 = date1.concat(dateTmp.getFullYear().toString());
            pdfFields2['personneDateNaissance[1]'] = date1;
        }
    pdfFields2['personneLieuNaissanceDepartement[1]'] = membre4.membrePersonnePhysique.personneLieuNaissanceDepartement != null ? membre4.membrePersonnePhysique.personneLieuNaissanceDepartement.getId() : '';
    pdfFields2['personneLieuNaissanceCommune[1]'] = membre4.membrePersonnePhysique.personneLieuNaissanceVille != null ? (membre4.membrePersonnePhysique.personneLieuNaissanceVille + '/' + membre4.membrePersonnePhysique.personneLieuNaissancePays) : (membre4.membrePersonnePhysique.personneLieuNaissanceCommune != null ? membre4.membrePersonnePhysique.personneLieuNaissanceCommune : '');
    pdfFields2['personneLieeAdresse[1]'] = 
    (membre4.membreAdresse.personneLieeAdresseNumeroVoie != null ? membre4.membreAdresse.personneLieeAdresseNumeroVoie : '')
    + ' ' + (membre4.membreAdresse.personneLieeAdresseIndiceVoie != null ? membre4.membreAdresse.personneLieeAdresseIndiceVoie : '')
    + ' ' + (membre4.membreAdresse.personneLieeAdresseTypeVoie != null ? membre4.membreAdresse.personneLieeAdresseTypeVoie : '')
    + ' ' + (membre4.membreAdresse.personneLieeAdresseNomVoie != null ? membre4.membreAdresse.personneLieeAdresseNomVoie : '')
    + ' ' + (membre4.membreAdresse.personneLieeAdresseComplementVoie != null ? membre4.membreAdresse.personneLieeAdresseComplementVoie : '')
    + ' ' + (membre4.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie != null ? membre4.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie : '');
    pdfFields2['personneLieeAdresseCodePostal[1]'] = membre4.membreAdresse.personneLieeAdresseCodePostal != null ? membre4.membreAdresse.personneLieeAdresseCodePostal : '';
    pdfFields2['personneLieeAdresseCommune[1]'] = membre4.membreAdresse.personneLieeAdresseCommune != null ? membre4.membreAdresse.personneLieeAdresseCommune : '';
    }
}
//Si 1er membre Personne Morale
//if (Value('id').of(membre1.personnaliteJuridique).eq('personneMorale')) {
if (membre1.membrePersonneMorale.personneLieePMDenomination != null) {
    pdfFields['nouveauPM[0]']  = membre1.membrePersonneMorale.personneLieePMDenomination != null ? true : false ;  
    pdfFields['personneLieePMDenomination'] = membre1.membrePersonneMorale.personneLieePMDenomination != null ? membre1.membrePersonneMorale.personneLieePMDenomination : '';
    pdfFields['personneLieePMNumeroIdentificationLieu'] = membre1.membrePersonneMorale.personneLieePMNumeroIdentification != null ?  (membre1.membrePersonneMorale.personneLieePMNumeroIdentification + ' ' + membre1.membrePersonneMorale.personneLieePMNumeroIdentificationLieu) : '';
    pdfFields['personneLieePMAdresse'] = 
            (membre1.membreAdresse.personneLieeAdresseNumeroVoie != null ? membre1.membreAdresse.personneLieeAdresseNumeroVoie : '')
            + ' ' + (membre1.membreAdresse.personneLieeAdresseIndiceVoie != null ? membre1.membreAdresse.personneLieeAdresseIndiceVoie : '')
            + ' ' + (membre1.membreAdresse.personneLieeAdresseTypeVoie != null ? membre1.membreAdresse.personneLieeAdresseTypeVoie : '')
            + ' ' + (membre1.membreAdresse.personneLieeAdresseNomVoie != null ? membre1.membreAdresse.personneLieeAdresseNomVoie : '')
            + ' ' + (membre1.membreAdresse.personneLieeAdresseComplementVoie != null ? membre1.membreAdresse.personneLieeAdresseComplementVoie : '')
            + ' ' + (membre1.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie != null ? membre1.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie : '');
    pdfFields['personneLieePMAdresseCodePostal'] = membre1.membreAdresse.personneLieeAdresseCodePostal != null ? membre1.membreAdresse.personneLieeAdresseCodePostal : '';
    pdfFields['personneLieePMAdresseCommune'] = membre1.membreAdresse.personneLieeAdresseCommune != null ? membre1.membreAdresse.personneLieeAdresseCommune : '';  
}
//Si 2ème membre Personne Morale sur F si membre 1 Personne Physique sinon sur F n°2                                                       
if (membre2.membrePersonneMorale.personneLieePMDenomination != null) {
    if (membre1.membrePersonnePhysique.personneNomNaissance  != null) { 
        pdfFields['nouveauPM[0]']  = membre2.membrePersonneMorale.personneLieePMDenomination != null ? true : false ;  
        pdfFields['personneLieePMDenomination'] = membre2.membrePersonneMorale.personneLieePMDenomination != null ? membre2.membrePersonneMorale.personneLieePMDenomination : '';
        pdfFields['personneLieePMNumeroIdentificationLieu'] = membre2.membrePersonneMorale.personneLieePMNumeroIdentification != null ?  (membre2.membrePersonneMorale.personneLieePMNumeroIdentification + ' ' + membre2.membrePersonneMorale.personneLieePMNumeroIdentificationLieu) : '';
        pdfFields['personneLieePMAdresse'] = 
                (membre2.membreAdresse.personneLieeAdresseNumeroVoie != null ? membre2.membreAdresse.personneLieeAdresseNumeroVoie : '')
                + ' ' + (membre2.membreAdresse.personneLieeAdresseIndiceVoie != null ? membre2.membreAdresse.personneLieeAdresseIndiceVoie : '')
                + ' ' + (membre2.membreAdresse.personneLieeAdresseTypeVoie != null ? membre2.membreAdresse.personneLieeAdresseTypeVoie : '')
                + ' ' + (membre2.membreAdresse.personneLieeAdresseNomVoie != null ? membre2.membreAdresse.personneLieeAdresseNomVoie : '')
                + ' ' + (membre2.membreAdresse.personneLieeAdresseComplementVoie != null ? membre2.membreAdresse.personneLieeAdresseComplementVoie : '')
                + ' ' + (membre2.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie != null ? membre2.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie : '');
        pdfFields['personneLieePMAdresseCodePostal'] = membre2.membreAdresse.personneLieeAdresseCodePostal != null ? membre2.membreAdresse.personneLieeAdresseCodePostal : '';
        pdfFields['personneLieePMAdresseCommune'] = membre2.membreAdresse.personneLieeAdresseCommune != null ? membre2.membreAdresse.personneLieeAdresseCommune : '';
    } else {
       if (not identification.etabSupp) {
        pdfFields2['intercalaireSuiteImprimeF'] =  true; 
        } 
        pdfFields2['nouveauPM[0]']  = membre2.membrePersonneMorale.personneLieePMDenomination != null ? true : false ;  
        pdfFields2['personneLieePMDenomination'] = membre2.membrePersonneMorale.personneLieePMDenomination != null ? membre2.membrePersonneMorale.personneLieePMDenomination : '';
        pdfFields2['personneLieePMNumeroIdentificationLieu'] = membre2.membrePersonneMorale.personneLieePMNumeroIdentification != null ?  (membre2.membrePersonneMorale.personneLieePMNumeroIdentification + ' ' + membre2.membrePersonneMorale.personneLieePMNumeroIdentificationLieu) : '';
        pdfFields2['personneLieePMAdresse'] = 
                (membre2.membreAdresse.personneLieeAdresseNumeroVoie != null ? membre2.membreAdresse.personneLieeAdresseNumeroVoie : '')
                + ' ' + (membre2.membreAdresse.personneLieeAdresseIndiceVoie != null ? membre2.membreAdresse.personneLieeAdresseIndiceVoie : '')
                + ' ' + (membre2.membreAdresse.personneLieeAdresseTypeVoie != null ? membre2.membreAdresse.personneLieeAdresseTypeVoie : '')
                + ' ' + (membre2.membreAdresse.personneLieeAdresseNomVoie != null ? membre2.membreAdresse.personneLieeAdresseNomVoie : '')
                + ' ' + (membre2.membreAdresse.personneLieeAdresseComplementVoie != null ? membre2.membreAdresse.personneLieeAdresseComplementVoie : '')
                + ' ' + (membre2.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie != null ? membre2.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie : '');
        pdfFields2['personneLieePMAdresseCodePostal'] = membre2.membreAdresse.personneLieeAdresseCodePostal != null ? membre2.membreAdresse.personneLieeAdresseCodePostal : '';
        pdfFields2['personneLieePMAdresseCommune'] = membre2.membreAdresse.personneLieeAdresseCommune != null ? membre2.membreAdresse.personneLieeAdresseCommune : '';
    }
}
//Si 3ème membre Personne Morale sur F si membre 1 et membre 2 Personne Physique sinon F n°3
if (membre3.membrePersonneMorale.personneLieePMDenomination != null) {
    if ((membre1.membrePersonnePhysique.personneNomNaissance  != null) and (membre2.membrePersonnePhysique.personneNomNaissance  != null)) { 
        pdfFields['nouveauPM[0]']  = membre3.membrePersonneMorale.personneLieePMDenomination != null ? true : false ;  
        pdfFields['personneLieePMDenomination'] = membre3.membrePersonneMorale.personneLieePMDenomination != null ? membre3.membrePersonneMorale.personneLieePMDenomination : '';
        pdfFields['personneLieePMNumeroIdentificationLieu'] = membre3.membrePersonneMorale.personneLieePMNumeroIdentification != null ?  (membre3.membrePersonneMorale.personneLieePMNumeroIdentification + ' ' + membre3.membrePersonneMorale.personneLieePMNumeroIdentificationLieu) : '';
        pdfFields['personneLieePMAdresse'] = 
                    (membre3.membreAdresse.personneLieeAdresseNumeroVoie != null ? membre3.membreAdresse.personneLieeAdresseNumeroVoie : '')
                    + ' ' + (membre3.membreAdresse.personneLieeAdresseIndiceVoie != null ? membre3.membreAdresse.personneLieeAdresseIndiceVoie : '')
                    + ' ' + (membre3.membreAdresse.personneLieeAdresseTypeVoie != null ? membre3.membreAdresse.personneLieeAdresseTypeVoie : '')
                    + ' ' + (membre3.membreAdresse.personneLieeAdresseNomVoie != null ? membre3.membreAdresse.personneLieeAdresseNomVoie : '')
                    + ' ' + (membre3.membreAdresse.personneLieeAdresseComplementVoie != null ? membre3.membreAdresse.personneLieeAdresseComplementVoie : '')
                    + ' ' + (membre3.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie != null ? membre3.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie : '');
        pdfFields['personneLieePMAdresseCodePostal'] = membre3.membreAdresse.personneLieeAdresseCodePostal != null ? membre3.membreAdresse.personneLieeAdresseCodePostal : '';
        pdfFields['personneLieePMAdresseCommune'] = membre3.membreAdresse.personneLieeAdresseCommune != null ? membre3.membreAdresse.personneLieeAdresseCommune : '';         
    } else if ((membre1.membrePersonnePhysique.personneNomNaissance  != null) and (membre2.membrePersonneMorale.personneLieePMDenomination != null)){
        pdfFields2['intercalaireSuiteImprimeF'] =  true;  
        pdfFields2['nouveauPM[0]']  = membre3.membrePersonneMorale.personneLieePMDenomination != null ? true : false ;  
        pdfFields2['personneLieePMDenomination'] = membre3.membrePersonneMorale.personneLieePMDenomination != null ? membre3.membrePersonneMorale.personneLieePMDenomination : '';
        pdfFields2['personneLieePMNumeroIdentificationLieu'] = membre3.membrePersonneMorale.personneLieePMNumeroIdentification != null ?  (membre3.membrePersonneMorale.personneLieePMNumeroIdentification + ' ' + membre3.membrePersonneMorale.personneLieePMNumeroIdentificationLieu) : '';
        pdfFields2['personneLieePMAdresse'] = 
                (membre3.membreAdresse.personneLieeAdresseNumeroVoie != null ? membre3.membreAdresse.personneLieeAdresseNumeroVoie : '')
                + ' ' + (membre3.membreAdresse.personneLieeAdresseIndiceVoie != null ? membre3.membreAdresse.personneLieeAdresseIndiceVoie : '')
                + ' ' + (membre3.membreAdresse.personneLieeAdresseTypeVoie != null ? membre3.membreAdresse.personneLieeAdresseTypeVoie : '')
                + ' ' + (membre3.membreAdresse.personneLieeAdresseNomVoie != null ? membre3.membreAdresse.personneLieeAdresseNomVoie : '')
                + ' ' + (membre3.membreAdresse.personneLieeAdresseComplementVoie != null ? membre3.membreAdresse.personneLieeAdresseComplementVoie : '')
                + ' ' + (membre3.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie != null ? membre3.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie : '');
        pdfFields2['personneLieePMAdresseCodePostal'] = membre3.membreAdresse.personneLieeAdresseCodePostal != null ? membre3.membreAdresse.personneLieeAdresseCodePostal : '';
        pdfFields2['personneLieePMAdresseCommune'] = membre3.membreAdresse.personneLieeAdresseCommune != null ? membre3.membreAdresse.personneLieeAdresseCommune : '';
    } else {
        pdfFields3['intercalaireSuiteImprimeF'] = true;  
        pdfFields3['nouveauPM[0]']  = membre3.membrePersonneMorale.personneLieePMDenomination != null ? true : false ;  
        pdfFields3['personneLieePMDenomination'] = membre3.membrePersonneMorale.personneLieePMDenomination != null ? membre3.membrePersonneMorale.personneLieePMDenomination : '';
        pdfFields3['personneLieePMNumeroIdentificationLieu'] = membre3.membrePersonneMorale.personneLieePMNumeroIdentification != null ?  (membre3.membrePersonneMorale.personneLieePMNumeroIdentification + ' ' + membre3.membrePersonneMorale.personneLieePMNumeroIdentificationLieu) : '';
        pdfFields3['personneLieePMAdresse'] = 
                (membre3.membreAdresse.personneLieeAdresseNumeroVoie != null ? membre3.membreAdresse.personneLieeAdresseNumeroVoie : '')
                + ' ' + (membre3.membreAdresse.personneLieeAdresseIndiceVoie != null ? membre3.membreAdresse.personneLieeAdresseIndiceVoie : '')
                + ' ' + (membre3.membreAdresse.personneLieeAdresseTypeVoie != null ? membre3.membreAdresse.personneLieeAdresseTypeVoie : '')
                + ' ' + (membre3.membreAdresse.personneLieeAdresseNomVoie != null ? membre3.membreAdresse.personneLieeAdresseNomVoie : '')
                + ' ' + (membre3.membreAdresse.personneLieeAdresseComplementVoie != null ? membre3.membreAdresse.personneLieeAdresseComplementVoie : '')
                + ' ' + (membre3.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie != null ? membre3.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie : '');
        pdfFields3['personneLieePMAdresseCodePostal'] = membre3.membreAdresse.personneLieeAdresseCodePostal != null ? membre3.membreAdresse.personneLieeAdresseCodePostal : '';
        pdfFields3['personneLieePMAdresseCommune'] = membre3.membreAdresse.personneLieeAdresseCommune != null ? membre3.membreAdresse.personneLieeAdresseCommune : '';
        }
}
//Si 4ème membre Personne Morale sur F n°4
 //if (Value('id').of(membre4.personnaliteJuridique).eq('personneMorale')) {
if (membre4.membrePersonneMorale.personneLieePMDenomination != null) {
    if ((membre1.membrePersonnePhysique.personneNomNaissance  != null) and (membre2.membrePersonnePhysique.personneNomNaissance  != null)
        and (membre3.membrePersonnePhysique.personneNomNaissance  != null) ) { 
        pdfFields['nouveauPM[0]']  = membre4.membrePersonneMorale.personneLieePMDenomination != null ? true : false ;  
        pdfFields['personneLieePMDenomination'] = membre4.membrePersonneMorale.personneLieePMDenomination != null ? membre4.membrePersonneMorale.personneLieePMDenomination : '';
        pdfFields['personneLieePMNumeroIdentificationLieu'] = membre4.membrePersonneMorale.personneLieePMNumeroIdentification != null ?  (membre4.membrePersonneMorale.personneLieePMNumeroIdentification + ' ' + membre4.membrePersonneMorale.personneLieePMNumeroIdentificationLieu) : '';
        pdfFields['personneLieePMAdresse'] = 
                    (membre4.membreAdresse.personneLieeAdresseNumeroVoie != null ? membre3.membreAdresse.personneLieeAdresseNumeroVoie : '')
                    + ' ' + (membre4.membreAdresse.personneLieeAdresseIndiceVoie != null ? membre3.membreAdresse.personneLieeAdresseIndiceVoie : '')
                    + ' ' + (membre4.membreAdresse.personneLieeAdresseTypeVoie != null ? membre3.membreAdresse.personneLieeAdresseTypeVoie : '')
                    + ' ' + (membre4.membreAdresse.personneLieeAdresseNomVoie != null ? membre3.membreAdresse.personneLieeAdresseNomVoie : '')
                    + ' ' + (membre4.membreAdresse.personneLieeAdresseComplementVoie != null ? membre3.membreAdresse.personneLieeAdresseComplementVoie : '')
                    + ' ' + (membre4.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie != null ? membre3.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie : '');
        pdfFields['personneLieePMAdresseCodePostal'] = membre4.membreAdresse.personneLieeAdresseCodePostal != null ? membre3.membreAdresse.personneLieeAdresseCodePostal : '';
        pdfFields['personneLieePMAdresseCommune'] = membre4.membreAdresse.personneLieeAdresseCommune != null ? membre3.membreAdresse.personneLieeAdresseCommune : '';         
    } else if ((membre1.membrePersonnePhysique.personneNomNaissance  != null) and (membre2.membrePersonnePhysique.personneNomNaissance  != null)
    and (membre3.membrePersonneMorale.personneLieePMDenomination != null)){
        pdfFields2['intercalaireSuiteImprimeF'] =  true;  
        pdfFields2['nouveauPM[0]']  = membre4.membrePersonneMorale.personneLieePMDenomination != null ? true : false ;  
        pdfFields2['personneLieePMDenomination'] = membre4.membrePersonneMorale.personneLieePMDenomination != null ? membre4.membrePersonneMorale.personneLieePMDenomination : '';
        pdfFields2['personneLieePMNumeroIdentificationLieu'] = membre4.membrePersonneMorale.personneLieePMNumeroIdentification != null ?  (membre4.membrePersonneMorale.personneLieePMNumeroIdentification + ' ' + membre4.membrePersonneMorale.personneLieePMNumeroIdentificationLieu) : '';
        pdfFields2['personneLieePMAdresse'] = 
                (membre4.membreAdresse.personneLieeAdresseNumeroVoie != null ? membre4.membreAdresse.personneLieeAdresseNumeroVoie : '')
                + ' ' + (membre4.membreAdresse.personneLieeAdresseIndiceVoie != null ? membre4.membreAdresse.personneLieeAdresseIndiceVoie : '')
                + ' ' + (membre4.membreAdresse.personneLieeAdresseTypeVoie != null ? membre4.membreAdresse.personneLieeAdresseTypeVoie : '')
                + ' ' + (membre4.membreAdresse.personneLieeAdresseNomVoie != null ? membre4.membreAdresse.personneLieeAdresseNomVoie : '')
                + ' ' + (membre4.membreAdresse.personneLieeAdresseComplementVoie != null ? membre4.membreAdresse.personneLieeAdresseComplementVoie : '')
                + ' ' + (membre4.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie != null ? membre4.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie : '');
        pdfFields2['personneLieePMAdresseCodePostal'] = membre4.membreAdresse.personneLieeAdresseCodePostal != null ? membre4.membreAdresse.personneLieeAdresseCodePostal : '';
        pdfFields2['personneLieePMAdresseCommune'] = membre4.membreAdresse.personneLieeAdresseCommune != null ? membre4.membreAdresse.personneLieeAdresseCommune : '';
    } else {
        pdfFields4['intercalaireSuiteImprimeF'] = true;  
        pdfFields4['nouveauPM[0]']  = membre4.membrePersonneMorale.personneLieePMDenomination != null ? true : false ;  
        pdfFields4['personneLieePMDenomination'] = membre4.membrePersonneMorale.personneLieePMDenomination != null ? membre4.membrePersonneMorale.personneLieePMDenomination : '';
        pdfFields4['personneLieePMNumeroIdentificationLieu'] = membre4.membrePersonneMorale.personneLieePMNumeroIdentification != null ?  (membre4.membrePersonneMorale.personneLieePMNumeroIdentification + ' ' + membre4.membrePersonneMorale.personneLieePMNumeroIdentificationLieu) : '';
        pdfFields4['personneLieePMAdresse'] = 
                    (membre4.membreAdresse.personneLieeAdresseNumeroVoie != null ? membre4.membreAdresse.personneLieeAdresseNumeroVoie : '')
                    + ' ' + (membre4.membreAdresse.personneLieeAdresseIndiceVoie != null ? membre4.membreAdresse.personneLieeAdresseIndiceVoie : '')
                    + ' ' + (membre4.membreAdresse.personneLieeAdresseTypeVoie != null ? membre4.membreAdresse.personneLieeAdresseTypeVoie : '')
                    + ' ' + (membre4.membreAdresse.personneLieeAdresseNomVoie != null ? membre4.membreAdresse.personneLieeAdresseNomVoie : '')
                    + ' ' + (membre4.membreAdresse.personneLieeAdresseComplementVoie != null ? membre4.membreAdresse.personneLieeAdresseComplementVoie : '')
                    + ' ' + (membre4.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie != null ? membre4.membreAdresse.personneLieeAdresseDistriutionSpecialeVoie : '');
        pdfFields4['personneLieePMAdresseCodePostal'] = membre4.membreAdresse.personneLieeAdresseCodePostal != null ? membre4.membreAdresse.personneLieeAdresseCodePostal : '';
        pdfFields4['personneLieePMAdresseCommune'] = membre4.membreAdresse.personneLieeAdresseCommune != null ? membre4.membreAdresse.personneLieeAdresseCommune : '';
    }   
}
//Cadre 5 Effectif salarié
pdfFields['exploitationEffectifSalariePresenceNon']  = effectif.exploitationEffectifSalariePresenceOui != false ? false : true;
pdfFields['exploitationEffectifSalariePresenceOui']  = effectif.exploitationEffectifSalariePresenceOui != false ? true : false;
pdfFields['exploitationEffectifSalarieAgricoleNombre']  = effectif.exploitationEffectifSalarieAgricoleNombre != false ? effectif.exploitationEffectifSalarieAgricoleNombre : '';
pdfFields['exploitationEffectifSalarieEmbauchePremierSalarieOui']  = effectif.exploitationEffectifSalarieEmbauchePremierSalarieOui != false ? true : false;
pdfFields['exploitationEffectifSalarieEmbauchePremierSalarieNon']   = effectif.exploitationEffectifSalarieEmbauchePremierSalarieOui!= false ? false : true;

//Cadre 7 
pdfFields['ouvertureExploitation']    = true;
/****************************/
/**ÉTABLISSEMENT PRINCIPAL**/
/****************************/
//Cadre 9 Etablissement PRINCIPAL créé
pdfFields['etabPrincipal'] = true;   
if (etablissement.adresseEtablissementDifferente) {                                                                        
pdfFields['adresseEtablissement']  = 
(etablissement.adresseEtablissement.adresseEtablissementNumeroVoie != null ? etablissement.adresseEtablissement.adresseEtablissementNumeroVoie : '')
+ ' ' + (etablissement.adresseEtablissement.adresseEtablissementIndiceVoie != null ? etablissement.adresseEtablissement.adresseEtablissementIndiceVoie : '')
+ ' ' + (etablissement.adresseEtablissement.adresseEtablissementNomVoie != null ? etablissement.adresseEtablissement.adresseEtablissementNomVoie : '')
+ ' ' + (etablissement.adresseEtablissement.adresseEtablissementComplementVoie != null ? etablissement.adresseEtablissement.adresseEtablissementComplementVoie : '')
+ ' ' + (etablissement.adresseEtablissement.adresseEtablissementDistributionSpecialeVoie != null ? etablissement.adresseEtablissement.adresseEtablissementDistributionSpecialeVoie :'');
pdfFields['adresseEtablissementCodePostal']  = etablissement.adresseEtablissement.adresseEtablissementCodePostal != null ? etablissement.adresseEtablissement.adresseEtablissementCodePostal : '';
pdfFields['adresseEtablissementCommune']  = etablissement.adresseEtablissement.adresseEtablissementCommune != null ? etablissement.adresseEtablissement.adresseEtablissementCommune : '';
} else {
pdfFields['adresseEtablissement']  = (identification.siegeAdresseNumeroVoie != null ? identification.siegeAdresseNumeroVoie : "")
+ ' ' + (identification.siegeAdresseIndiceVoie != null ? identification.siegeAdresseIndiceVoie : "")
+ ' ' + (identification.siegeAdresseTypeVoie != null ? identification.siegeAdresseTypeVoie : "")
+ ' ' + identification.siegeAdresseNomVoie ;
+ ' ' + (identification.siegeAdresseComplementVoie != null ? identification.siegeAdresseComplementVoie : '')
+ ' ' + (identification.siegeADresseDistributionSpeciale  != null ? identification.siegeADresseDistributionSpeciale : '');
pdfFields['adresseEtablissementCodePostal']  = identification.siegeAdresseCodePostal;
pdfFields['adresseEtablissementCommune'] = identification.siegeAdresseCommuneAncienne != null ?  (identification.siegeAdresseCommune + '/' + identification.siegeAdresseCommuneAncienne) : identification.siegeAdresseCommune;
}
// Cadre 10 Activité de l'établissement PRINCIPAL
if (etablissement.informationActivite.etablissementDateDebutActivite != null) {
    var dateTmp = new Date(parseInt(etablissement.informationActivite.etablissementDateDebutActivite.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
    date = date.concat(dateTmp.getFullYear().toString());
    pdfFields['etablissementDateDebutActivite'] = date;
}

pdfFields['etablissementActivitePlusImportanteAgricoleCereales'] = Value('id').of(etablissement.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleCereales') ? true : false;
pdfFields['etablissementActivitePlusImportanteAgricoleRiz'] = Value('id').of(etablissement.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleRiz') ? true : false; 
pdfFields['etablissementActivitePlusImportanteAgricoleLegumes'] = Value('id').of(etablissement.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleLegumes')  ? true : false;
pdfFields['etablissementActivitePlusImportanteAgricoleCanneSucre'] = Value('id').of(etablissement.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleCanneSucre') ? true : false;
pdfFields['etablissementActivitePlusImportanteAgricoleTabac'] = Value('id').of(etablissement.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleTabac') ? true : false;
pdfFields['etablissementActivitePlusImportanteAgricolePlantesFibres'] = Value('id').of(etablissement.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricolePlantesFibres') ? true : false;
pdfFields['etablissementActivitePlusImportanteAgricoleVigne'] = Value('id').of(etablissement.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleVigne')  ? true : false;
pdfFields['etablissement_activitePluetablissementActivitePlusImportanteAgricoleFruitsTropicauxsImportanteAgricole_fruitsTropicaux'] = Value('id').of(etablissement.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleFruitsTropicaux')  ? true : false;
pdfFields['etablissementActivitePlusImportanteAgricoleAgrumes'] = Value('id').of(etablissement.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleAgrumes') ? true : false;
pdfFields['etablissementActivitePlusImportanteAgricoleFruitsPepins'] = Value('id').of(etablissement.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleFruitsPepins')  ? true : false;
pdfFields['etablissementActivitePlusImportanteAgricoleFruitsOleagineux'] = Value('id').of(etablissement.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleFruitsOleagineux') ? true : false;
pdfFields['etablissementActivitePlusImportanteAgricoleAutresFruitsArbres'] = Value('id').of(etablissement.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleAutresFruitsArbres') ? true : false;
pdfFields['etablissementActivitePlusImportanteAgricolePlantesBoisson'] = Value('id').of(etablissement.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricolePlantesBoisson')  ? true : false;
pdfFields['etablissementActivitePlusImportanteAgricolePlantesEpicesAromatiques'] = Value('id').of(etablissement.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricolePlantesEpicesAromatiques')  ? true : false;
pdfFields['etablissementActivitePlusImportanteAgricoleAutresCulturesPermanentes'] = Value('id').of(etablissement.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleAutresCulturesPermanentes') ? true : false;
pdfFields['etablissementActivitePlusImportanteAgricoleAutresPreciser[0]'] = etablissement.informationActivite.etablissementActivitePlusImportanteAgricoleAutresPreciser;
// Cadre 10 Activités Elevage : questionActiviteElevage
pdfFields['etablissementActivitePlusImportanteAgricoleVachesLaitieres'] = Value('id').of(etablissement.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricoleVachesLaitieres') ? true : false;
pdfFields['etablissementActivitePlusImportanteAgricoleAutresBovins'] = Value('id').of(etablissement.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricoleAutresBovins') ? true : false;
pdfFields['etablissementActivitePlusImportanteAgricoleChevaux'] = Value('id').of(etablissement.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricoleChevaux')  ? true : false;
pdfFields['etablissementActivitePlusImportanteAgricoleChameaux'] = Value('id').of(etablissement.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricoleChameaux')  ? true : false;
pdfFields['etablissementActivitePlusImportanteAgricoleOvinsCaprins'] = Value('id').of(etablissement.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricoleOvinsCaprins')  ? true : false;
pdfFields['etablissementActivitePlusImportanteAgricolePorcins'] = Value('id').of(etablissement.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricolePorcins') ? true : false;
pdfFields['etablissementActivitePlusImportanteAgricoleVolailles'] = Value('id').of(etablissement.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricoleVolailles') ? true : false;
pdfFields['etablissementActivitePlusImportanteAgricoleAquacultureMer'] = Value('id').of(etablissement.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricoleAquacultureMer') ? true : false;
pdfFields['etablissementActivitePlusImportanteAgricoleAquacultureEauDouce'] = Value('id').of(etablissement.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricoleAquacultureEauDouce')  ? true : false;
pdfFields['etablissementActivitePlusImportanteAgricoleAutresAnimaux'] = Value('id').of(etablissement.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricoleAutresAnimaux') ? true : false;
pdfFields['etablissementActivitePlusImportanteAgricoleAutresPreciser[1]'] = etablissement.informationActivite.etablissementActivitePlusImportanteAgricoleAutresPreciser;
// Cadre 10 Activités Autres : questionActiviteAutres
pdfFields['etablissementActivitePlusImportanteAgricoleCultureElevageAssocies'] = Value('id').of(etablissement.informationActivite.questionActiviteAutres).eq('etablissementActivitePlusImportanteAgricoleCultureElevageAssocies')  ? true : false;
pdfFields['etablissementActivitePlusImportanteAgricoleActivitesPepinieres'] = Value('id').of(etablissement.informationActivite.questionActiviteAutres).eq('etablissementActivitePlusImportanteAgricoleActivitesPepinieres')  ? true : false;
pdfFields['etablissementActivitePlusImportantgeAgricoleSylviculture'] = Value('id').of(etablissement.informationActivite.questionActiviteAutres).eq('etablissementActivitePlusImportantgeAgricoleSylviculture')  ? true : false;
pdfFields['etablissementActivitePlusImportantgeAgricoleBailleurBiensRuraux'] = Value('id').of(etablissement.informationActivite.questionActiviteAutres).eq('etablissementActivitePlusImportantgeAgricoleBailleurBiensRuraux')  ? true : false;
pdfFields['etablissementActivitePlusImportantgeAgricoleLoueurCheptel'] = Value('id').of(etablissement.informationActivite.questionActiviteAutres).eq('etablissementActivitePlusImportantgeAgricoleLoueurCheptel')  ? true : false;
pdfFields['etablissementActivitePlusImportanteAricoleAutre'] = Value('id').of(etablissement.informationActivite.questionActiviteAutres).eq('etablissementActivitePlusImportanteAricoleAutre') ? true : false;
pdfFields['etablissementActivitePlusImportanteAgricoleAutresPreciser[2]'] = etablissement.informationActivite.etablissementActivitePlusImportanteAgricoleAutresPreciser;
//
pdfFields['activiteElevageOui']  = Value('id').of(etablissement.informationActivite.activiteAgricole).eq('activiteElevage')  ? true : false;
pdfFields['activiteElevageNon']  = Value('id').of(etablissement.informationActivite.activiteAgricole).eq('activiteElevage') ? false : true;
pdfFields['activiteViticoleOui']  = Value('id').of(etablissement.informationActivite.activiteAgricole).eq('activiteViticole')  ? true : false;
pdfFields['activiteViticoleNon']  = Value('id').of(etablissement.informationActivite.activiteAgricole).eq('activiteViticole')  ? false : true;

// Cadre 10B Nom exploitation de cet établissement PRINCIPAL
pdfFields['cadreNomExploitation'] = etablissement.cadreNomExploitation.etablissementNomExploitation;

//Cadre 11 Origine de l'établissement PRINCIPAL
pdfFields['etablissementOrigineCreation'] = Value('id').of(etablissement.cadreOrigineActivite.etablisementOrigineActivite).eq('etablissementOrigineCreation') ? true : false;
pdfFields['etablissementOrigineRepriseTotalePartielle'] = Value('id').of(etablissement.cadreOrigineActivite.etablisementOrigineActivite).eq('etablissementOrigineRepriseTotalePartielle') ? true : false;
pdfFields['etablissementOrigineAutre'] = Value('id').of(etablissement.cadreOrigineActivite.etablisementOrigineActivite).eq('etablissementOrigineAutre') ? true : false;
pdfFields['etablissementOrigineLibelle']  = etablissement.cadreOrigineActivite.etablissementOrigineLibelle != null ? etablissement.cadreOrigineActivite.etablissementOrigineLibelle : '';
// Cadre 11 Origine de l'activité de l'établissement PRINCIPAL- Précédent exploitant
pdfFields['entrepriseLieeSirenPrecedentExploitant'] = (etablissement.cadreOrigineActivite.precedentExploitant.entrepriseLieeSirenPrecedentExploitant != null ? etablissement.cadreOrigineActivite.precedentExploitant.entrepriseLieeSirenPrecedentExploitant.split(' ').join('') : '');
pdfFields['entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant'] = etablissement.cadreOrigineActivite.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant;
pdfFields['entrepriseLieeEntreprisePPNomUsagePrecedentExploitant'] = etablissement.cadreOrigineActivite.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant;
pdfFields['entrepriseLiee_entreprisePP_prenom_precedentExploitant'] = etablissement.cadreOrigineActivite.precedentExploitant.entrepriseLieeEntreprisePPPrenomPrecedentExploitant;
pdfFields['entrepriseLieeEntreprisePPDenominationPrecedentExploitant'] = etablissement.cadreOrigineActivite.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant;
pdfFields['entrepriseLieeNumeroDetenteurPrecedentExploitant'] = etablissement.cadreOrigineActivite.precedentExploitant.entrepriseLieeNumeroDetenteurPrecedentExploitant;
pdfFields['entrepriseLieeNumeroExploitationPrecedentExploitant'] = etablissement.cadreOrigineActivite.precedentExploitant.entrepriseLieeNumeroExploitationPrecedentExploitant;
/****************************/
/**ÉTABLISSEMENT SECONDAIRE**/
/****************************/
if (etablissement.adresseAutreEtablissement) {
    pdfFields2['intercalaireSuiteImprimeF'] = true;  
    pdfFields2['etabSecondaire'] = true;   
    if (etabSecondaire.adresseEtablissementDifferente) {                                                                        
    pdfFields2['adresseEtablissement']  = 
    (etabSecondaire.adresseEtablissement.adresseEtablissementNumeroVoie != null ? etabSecondaire.adresseEtablissement.adresseEtablissementNumeroVoie : '')
    + ' ' + (etabSecondaire.adresseEtablissement.adresseEtablissementIndiceVoie != null ? etabSecondaire.adresseEtablissement.adresseEtablissementIndiceVoie : '')
    + ' ' + (etabSecondaire.adresseEtablissement.adresseEtablissementNomVoie != null ? etabSecondaire.adresseEtablissement.adresseEtablissementNomVoie : '')
    + ' ' + (etabSecondaire.adresseEtablissement.adresseEtablissementComplementVoie != null ? etabSecondaire.adresseEtablissement.adresseEtablissementComplementVoie : '')
    + ' ' + (etabSecondaire.adresseEtablissement.adresseEtablissementDistributionSpecialeVoie != null ? etabSecondaire.adresseEtablissement.adresseEtablissementDistributionSpecialeVoie :'');
    pdfFields2['adresseEtablissementCodePostal']  = etabSecondaire.adresseEtablissement.adresseEtablissementCodePostal != null ? etabSecondaire.adresseEtablissement.adresseEtablissementCodePostal : '';
    pdfFields2['adresseEtablissementCommune']  = etabSecondaire.adresseEtablissement.adresseEtablissementCommune != null ? etabSecondaire.adresseEtablissement.adresseEtablissementCommune : '';
    } else {
    pdfFields2['adresseEtablissement']  = (identification.siegeAdresseNumeroVoie != null ? identification.siegeAdresseNumeroVoie : "")
    + ' ' + (identification.siegeAdresseIndiceVoie != null ? identification.siegeAdresseIndiceVoie : "")
    + ' ' + (identification.siegeAdresseTypeVoie != null ? identification.siegeAdresseTypeVoie : "")
    + ' ' + identification.siegeAdresseNomVoie ;
    + ' ' + (identification.siegeAdresseComplementVoie != null ? identification.siegeAdresseComplementVoie : '')
    + ' ' + (identification.siegeADresseDistributionSpeciale  != null ? identification.siegeADresseDistributionSpeciale : '');
    pdfFields2['adresseEtablissementCodePostal']  = identification.siegeAdresseCodePostal;
    pdfFields2['adresseEtablissementCommune'] = identification.siegeAdresseCommuneAncienne != null ?  (identification.siegeAdresseCommune + '/' + identification.siegeAdresseCommuneAncienne) : identification.siegeAdresseCommune;
    }
    // Cadre 10 Activité de l'établissement SECONDAIRE
    if (etabSecondaire.informationActivite.etablissementDateDebutActivite != null) {
        var dateTmp = new Date(parseInt(etabSecondaire.informationActivite.etablissementDateDebutActivite.getTimeInMillis()));
        var date = pad(dateTmp.getDate().toString());
        var month = dateTmp.getMonth() + 1;
        date = date.concat(pad(month.toString()));
        date = date.concat(dateTmp.getFullYear().toString());
        pdfFields2['etablissementDateDebutActivite'] = date;
    }
    pdfFields2['etablissementActivitePlusImportanteAgricoleCereales'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleCereales') ? true : false;
    pdfFields2['etablissementActivitePlusImportanteAgricoleRiz'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleRiz') ? true : false; 
    pdfFields2['etablissementActivitePlusImportanteAgricoleLegumes'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleLegumes')  ? true : false;
    pdfFields2['etablissementActivitePlusImportanteAgricoleCanneSucre'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleCanneSucre') ? true : false;
    pdfFields2['etablissementActivitePlusImportanteAgricoleTabac'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleTabac') ? true : false;
    pdfFields2['etablissementActivitePlusImportanteAgricolePlantesFibres'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricolePlantesFibres') ? true : false;
    pdfFields2['etablissementActivitePlusImportanteAgricoleVigne'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleVigne')  ? true : false;
    pdfFields2['etablissement_activitePluetablissementActivitePlusImportanteAgricoleFruitsTropicauxsImportanteAgricole_fruitsTropicaux'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleFruitsTropicaux')  ? true : false;
    pdfFields2['etablissementActivitePlusImportanteAgricoleAgrumes'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleAgrumes') ? true : false;
    pdfFields2['etablissementActivitePlusImportanteAgricoleFruitsPepins'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleFruitsPepins')  ? true : false;
    pdfFields2['etablissementActivitePlusImportanteAgricoleFruitsOleagineux'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleFruitsOleagineux') ? true : false;
    pdfFields2['etablissementActivitePlusImportanteAgricoleAutresFruitsArbres'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleAutresFruitsArbres') ? true : false;
    pdfFields2['etablissementActivitePlusImportanteAgricolePlantesBoisson'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricolePlantesBoisson')  ? true : false;
    pdfFields2['etablissementActivitePlusImportanteAgricolePlantesEpicesAromatiques'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricolePlantesEpicesAromatiques')  ? true : false;
    pdfFields2['etablissementActivitePlusImportanteAgricoleAutresCulturesPermanentes'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteCulture).eq('etablissementActivitePlusImportanteAgricoleAutresCulturesPermanentes') ? true : false;
    pdfFields2['etablissementActivitePlusImportanteAgricoleAutresPreciser[0]'] = etabSecondaire.informationActivite.etablissementActivitePlusImportanteAgricoleAutresPreciser;
    // Cadre 10 Activités Elevage : questionActiviteElevage
    pdfFields2['etablissementActivitePlusImportanteAgricoleVachesLaitieres'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricoleVachesLaitieres') ? true : false;
	pdfFields2['etablissementActivitePlusImportanteAgricoleAutresBovins'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricoleAutresBovins') ? true : false;
    pdfFields2['etablissementActivitePlusImportanteAgricoleChevaux'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricoleChevaux')  ? true : false;
    pdfFields2['etablissementActivitePlusImportanteAgricoleChameaux'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricoleChameaux')  ? true : false;
	pdfFields2['etablissementActivitePlusImportanteAgricoleOvinsCaprins'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricoleOvinsCaprins')  ? true : false;
    pdfFields2['etablissementActivitePlusImportanteAgricolePorcins'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricolePorcins') ? true : false;
	pdfFields2['etablissementActivitePlusImportanteAgricoleVolailles'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricoleVolailles') ? true : false;
    pdfFields2['etablissementActivitePlusImportanteAgricoleAquacultureMer'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricoleAquacultureMer') ? true : false;
    pdfFields2['etablissementActivitePlusImportanteAgricoleAquacultureEauDouce'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricoleAquacultureEauDouce')  ? true : false;
    pdfFields2['etablissementActivitePlusImportanteAgricoleAutresAnimaux'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteElevage).eq('etablissementActivitePlusImportanteAgricoleAutresAnimaux') ? true : false;
    pdfFields2['etablissementActivitePlusImportanteAgricoleAutresPreciser[1]'] = etabSecondaire.informationActivite.etablissementActivitePlusImportanteAgricoleAutresPreciser;
    // Cadre 10 Activités Autres : questionActiviteAutres
    pdfFields2['etablissementActivitePlusImportanteAgricoleCultureElevageAssocies'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteAutres).eq('etablissementActivitePlusImportanteAgricoleCultureElevageAssocies')  ? true : false;
    pdfFields2['etablissementActivitePlusImportanteAgricoleActivitesPepinieres'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteAutres).eq('etablissementActivitePlusImportanteAgricoleActivitesPepinieres')  ? true : false;
    pdfFields2['etablissementActivitePlusImportantgeAgricoleSylviculture'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteAutres).eq('etablissementActivitePlusImportantgeAgricoleSylviculture')  ? true : false;
    pdfFields2['etablissementActivitePlusImportantgeAgricoleBailleurBiensRuraux'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteAutres).eq('etablissementActivitePlusImportantgeAgricoleBailleurBiensRuraux')  ? true : false;
    pdfFields2['etablissementActivitePlusImportantgeAgricoleLoueurCheptel'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteAutres).eq('etablissementActivitePlusImportantgeAgricoleLoueurCheptel')  ? true : false;
    pdfFields2['etablissementActivitePlusImportanteAricoleAutre'] = Value('id').of(etabSecondaire.informationActivite.questionActiviteAutres).eq('etablissementActivitePlusImportanteAricoleAutre') ? true : false;
    pdfFields2['etablissementActivitePlusImportanteAgricoleAutresPreciser[2]'] = etabSecondaire.informationActivite.etablissementActivitePlusImportanteAgricoleAutresPreciser;
    //
    pdfFields2['activiteElevageOui']  = Value('id').of(etabSecondaire.informationActivite.activiteAgricole).eq('activiteElevage')  ? true : false;
    pdfFields2['activiteElevageNon']  = Value('id').of(etabSecondaire.informationActivite.activiteAgricole).eq('activiteElevage') ? false : true;
    pdfFields2['activiteViticoleOui']  = Value('id').of(etabSecondaire.informationActivite.activiteAgricole).eq('activiteViticole')  ? true : false;
    pdfFields2['activiteViticoleNon']  = Value('id').of(etabSecondaire.informationActivite.activiteAgricole).eq('activiteViticole')  ? false : true;
     
    // Cadre 10B Nom exploitation de cet établissement SECONDAIRE
    pdfFields2['cadreNomExploitation'] = etabSecondaire.cadreNomExploitation.etablissementNomExploitation;
    
    //Cadre 11 Origine de l'établissement SECONDAIRE
    pdfFields2['etablissementOrigineCreation'] = Value('id').of(etabSecondaire.cadreOrigineActivite.etablisementOrigineActivite).eq('etablissementOrigineCreation') ? true : false;
    pdfFields2['etablissementOrigineRepriseTotalePartielle'] = Value('id').of(etabSecondaire.cadreOrigineActivite.etablisementOrigineActivite).eq('etablissementOrigineRepriseTotalePartielle') ? true : false;
    pdfFields2['etablissementOrigineAutre'] = Value('id').of(etabSecondaire.cadreOrigineActivite.etablisementOrigineActivite).eq('etablissementOrigineAutre') ? true : false;
    pdfFields2['etablissementOrigineLibelle']  = etabSecondaire.cadreOrigineActivite.etablissementOrigineLibelle != null ? etabSecondaire.cadreOrigineActivite.etablissementOrigineLibelle : '';
    // Cadre 11 Origine de l'activité de l'établissement SECONDAIRE - Précédent exploitant
    pdfFields2['entrepriseLieeSirenPrecedentExploitant'] = (etabSecondaire.cadreOrigineActivite.precedentExploitant.entrepriseLieeSirenPrecedentExploitant != null ? etabSecondaire.cadreOrigineActivite.precedentExploitant.entrepriseLieeSirenPrecedentExploitant.split(' ').join('') : '');
    pdfFields2['entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant'] = etabSecondaire.cadreOrigineActivite.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant;
    pdfFields2['entrepriseLieeEntreprisePPNomUsagePrecedentExploitant'] = etabSecondaire.cadreOrigineActivite.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant;
    pdfFields2['entrepriseLiee_entreprisePP_prenom_precedentExploitant'] = etabSecondaire.cadreOrigineActivite.precedentExploitant.entrepriseLieeEntreprisePPPrenomPrecedentExploitant;
    pdfFields2['entrepriseLieeEntreprisePPDenominationPrecedentExploitant'] = etabSecondaire.cadreOrigineActivite.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant;
    pdfFields2['entrepriseLieeNumeroDetenteurPrecedentExploitant'] = etabSecondaire.cadreOrigineActivite.precedentExploitant.entrepriseLieeNumeroDetenteurPrecedentExploitant;
    pdfFields2['entrepriseLieeNumeroExploitationPrecedentExploitant'] = etabSecondaire.cadreOrigineActivite.precedentExploitant.entrepriseLieeNumeroExploitationPrecedentExploitant;
    }

// Cadre 13 Location de biens ruraux
if (bailleur.dateDebutMiseLocation != null) {
    var dateTmp = new Date(parseInt(bailleur.dateDebutMiseLocation.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
    date = date.concat(dateTmp.getFullYear().toString());
    pdfFields['dateDebutMiseLocation'] = date;
}
pdfFields['miseEnLocationTotale'] = Value('id').of(bailleur.objetMiseLocation).eq('miseEnLocationTotale')  ? true : false;
pdfFields['miseEnLocationPartiel'] = Value('id').of(bailleur.objetMiseLocation).eq('miseEnLocationPartiel')  ? true : false;
pdfFields['miseEnLocationLocationDroits'] = Value('id').of(bailleur.objetMiseLocation).eq('miseEnLocationLocationDroits') ? true : false;
pdfFields['miseEnLocationAdresse'] = 
(bailleur.cadreAdresseBienLoue.miseEnLocationAdresseNumeroVoie != null ? bailleur.cadreAdresseBienLoue.miseEnLocationAdresseNumeroVoie : '')
+ ' ' + (bailleur.cadreAdresseBienLoue.miseEnLocationAdresseIndiceVoie != null ? bailleur.cadreAdresseBienLoue.miseEnLocationAdresseIndiceVoie : '')
+ ' ' + (bailleur.cadreAdresseBienLoue.miseEnLocationAdresseTypeVoie != null ? bailleur.cadreAdresseBienLoue.miseEnLocationAdresseTypeVoie : '')
+ ' ' + (bailleur.cadreAdresseBienLoue.miseEnLocationAdresseNomVoie != null ? bailleur.cadreAdresseBienLoue.miseEnLocationAdresseNomVoie : '')
+ ' ' + (bailleur.cadreAdresseBienLoue.miseEnLocationAdresseComplementVoie != null ? bailleur.cadreAdresseBienLoue.miseEnLocationAdresseComplementVoie : '')
+ ' ' + (bailleur.cadreAdresseBienLoue.miseEnLocationAdresseDistributionSpeciale != null ? bailleur.cadreAdresseBienLoue.miseEnLocationAdresseDistributionSpeciale : '');
pdfFields['miseEnLocationAdresseCodePostal'] = bailleur.cadreAdresseBienLoue.miseEnLocationAdresseCodePostal != null ? bailleur.cadreAdresseBienLoue.miseEnLocationAdresseCodePostal : '';
pdfFields['miseEnLocationAdresseCommune']  = bailleur.cadreAdresseBienLoue.miseEnLocationAdresseCommune != null ? bailleur.cadreAdresseBienLoue.miseEnLocationAdresseCommune : '';
pdfFields['miseEnLocationIdentificationPreneur'] = bailleur.miseEnLocationDenominationPreneur != null ? bailleur.miseEnLocationDenominationPreneur :
((bailleur.miseEnLocationNomNaissancePreneur  != null ? bailleur.miseEnLocationNomNaissancePreneur :'')
+ ' ' + (bailleur.miseEnLocationNomUsagePreneur != null ? bailleur.miseEnLocationNomUsagePreneur : '')
+ ' ' + (bailleur.miseEnLocationPrenomPreneur != null ? bailleur.miseEnLocationPrenomPreneur : ''));
pdfFields['miseEnLocationSirenPreneur'] = bailleur.miseEnLocationSirenPreneur != null ? bailleur.miseEnLocationSirenPreneur : '';

//Cadre 14 Options fiscales 
//impositionBenefices
pdfFields['regimeFiscalRegimeImpositionBeneficesMBA'] = Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('regimeFiscalRegimeImpositionBeneficesMBA') != false ? true : false;
pdfFields['regimeFiscalRegimeImpositionBeneficesRsBA'] = Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('regimeFiscalRegimeImpositionBeneficesRsBA') != false ? true : false;
pdfFields['regimeFiscalRegimeImpositionBeneficesRnBA'] = Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('regimeFiscalRegimeImpositionBeneficesRnBA') != false ? true : false;
pdfFields['regimeFiscalRegimeImpositionBeneficesFfBA'] = Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('regimeFiscalRegimeImpositionBeneficesFfBA') != false ? true : false;
pdfFields['regimeFiscalRegimeImpositionBeneficesRevFonc'] = Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBenefice).eq('regimeFiscalRegimeImpositionBeneficesRevFonc') != false ? true : false; 
//impotSociete
pdfFields['regimeFiscalRegimeImpositionSocieteIsoIS'] = Value('id').of(fiscal.impotSociete.regimeFiscalImpositionSociete).eq('regimeFiscalRegimeImpositionSocieteIsoIS') != false ? true : false;
pdfFields['regimeFiscalRegimeImpositionSocieteRsIS'] = Value('id').of(fiscal.impotSociete.regimeFiscalImpositionSociete).eq('regimeFiscalRegimeImpositionSocieteRsIS') != false ? true : false;
pdfFields['regimeFiscalRegimeImpositionSocieteRnIS'] = Value('id').of(fiscal.impotSociete.regimeFiscalImpositionSociete).eq('regimeFiscalRegimeImpositionSocieteRnIS') != false ? true : false;
//OptionsTVA
pdfFields['impositionTVARemboursementForfaitaire'] = Value('id').of(fiscal.optionsTVA.regimeTVA).eq('impositionTVARemboursementForfaitaire') != false ? true : false;
pdfFields['impositionTVAImpositionObligatoire'] = Value('id').of(fiscal.optionsTVA.regimeTVA).eq('impositionTVAImpositionObligatoire') != false ? true : false;
pdfFields['regimeFiscalRegimeImpositionTVAOptionsParticulieres4'] = Value('id').of(fiscal.optionsTVA.optionTVAvolontaire).eq('regimeFiscalRegimeImpositionTVAOptionsParticulieres4') != false ? true : false;
pdfFields['regimeFiscalRegimeImpositionTVAOptionsParticulieresLBR']  = Value('id').of(fiscal.optionsTVA.optionTVAvolontaire).eq('regimeFiscalRegimeImpositionTVAOptionsParticulieresLBR') != false ? true : false;
pdfFields['optionTVADeclaration'] = fiscal.optionsTVA.optionTVADeclaration != false ? true : false;
if (fiscal.optionsTVA.regimeFiscalDateClotureExerciceComptable != null) {
    var dateTmp = new Date(parseInt(fiscal.optionsTVA.regimeFiscalDateClotureExerciceComptable.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
    date = date.concat(dateTmp.getFullYear().toString());
    pdfFields['regimeFiscalDateClotureExerciceComptable'] = date;
}
pdfFields['regimeFiscalRegimeImpositionTVADeclarationTrimestrielle'] = Value('id').of(fiscal.optionsTVA.optionTVADepot).eq('regimeFiscalRegimeImpositionTVADeclarationTrimestrielle') != false ? true : false;
pdfFields['regimeFiscalRegimeImpositionTVADeclarationMensuelle'] = Value('id').of(fiscal.optionsTVA.optionTVADepot).eq('regimeFiscalRegimeImpositionTVADeclarationMensuelle') != false ? true : false;
//Bénéfices commerciaux BIC
pdfFields['regimeFiscalRegimeImpositionBeneficesMBIC'] = Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC).eq('regimeFiscalRegimeImpositionBeneficesMBIC') != false ? true : false;
pdfFields['regimeFiscalRegimeImpositionBeneficesRsBIC'] = Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC).eq('regimeFiscalRegimeImpositionBeneficesRsBIC') != false ? true : false;
pdfFields['regimeFiscalRegimeImpositionBeneficesRnBIC'] = Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC).eq('regimeFiscalRegimeImpositionBeneficesRnBIC') != false ? true : false;
//Bénéfices non commerciaux BNC
pdfFields['RegimeFiscalRegimeImpositionBeneficesRsBNC'] = Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBNC).eq('RegimeFiscalRegimeImpositionBeneficesRsBNC') != false ? true : false;
pdfFields['RegimeFiscalRegimeImpositionBeneficesDcBNC'] = Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBNC).eq('RegimeFiscalRegimeImpositionBeneficesDcBNC') != false ? true : false;
pdfFields['regimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionCreanceDette'] = Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBNC).eq('regimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionCreanceDette') != false ? true : false;
// TVA régime général
pdfFields['regimeFiscalRegimeImpositionTVARfTVA'] = Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC.regimeTVA1).eq('regimeFiscalRegimeImpositionTVARfTVA') != false ? true : Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC.regimeTVA2).eq('regimeFiscalRegimeImpositionTVARfTVA') != false ? true :
    Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBNC.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARfTVA') != false ? true : false;
pdfFields['regimeFiscalRegimeImpositionTVARsTVA'] = Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC.regimeTVA1).eq('RegimeFiscalRegimeImpositionTVARsTVA') != false ? true : Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBNC.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARsTVA') != false ? true : false;
pdfFields['regimeFiscalRegimeImpositionTVAMrTVA'] = Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC.regimeTVA1).eq('regimeFiscalRegimeImpositionTVAMrTVA') != false ? true : false;
pdfFields['regimeFiscalRegimeImpositionTVARnTVA'] = Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBIC.regimeTVA2).eq('regimeFiscalRegimeImpositionTVARnTVA') != false ? true :
    Value('id').of(fiscal.impositionAccessoire.impositionBeneficesBNC.regimeTVA).eq('regimeFiscalRegimeImpositionTVARnTVA') != false ? true : false;
pdfFields['regimeFiscalRegimeImpositionTVAOptionsParticulieres1'] = fiscal.impositionAccessoire.impositionBeneficesBIC.regimeFiscalRegimeImpositionTVAOptionsParticulieres1 != false ? true :
    fiscal.impositionAccessoire.impositionBeneficesBNC.regimeFiscalRegimeImpositionTVAOptionsParticulieres1 != false ? true : false;
pdfFields['regimeFiscalRegimeImpositionTVAOptionsParticulieres2'] = fiscal.impositionAccessoire.impositionBeneficesBIC.regimeFiscalRegimeImpositionTVAOptionsParticulieres2 != false ? true : false;

//Cadre 15 Observations
pdfFields['formaliteObservations']  = infoCpt.formaliteObservations != null ? infoCpt.formaliteObservations : '';

//Cadre 16 Correspondance
pdfFields['exploitation']   = Value('id').of(infoCpt.adresseCorrespond).eq('exploitation') ? true : false;
pdfFields['autre']  = (Value('id').of(infoCpt.adresseCorrespond).eq('autre') or  Value('id').of(infoCpt.adresseCorrespond).eq('etabPrincipal') or Value('id').of(infoCpt.adresseCorrespond).eq('etabSecondaire')) ? true : false;
//conditionné les adresses en fonctions du choix du déclarant
pdfFields['adresseCorrespondance1']  = 
(infoCpt.adresseCorrespondance.nomPrenomDenominationCorrespondance != null ? infoCpt.adresseCorrespondance.nomPrenomDenominationCorrespondance : '')
+ ' ' + (infoCpt.adresseCorrespondance.numeroVoieAdresseCorrespondance != null ? infoCpt.adresseCorrespondance.numeroVoieAdresseCorrespondance : '')
+ ' ' + (infoCpt.adresseCorrespondance.indiceVoieAdresseCorrespondance != null ? infoCpt.adresseCorrespondance.indiceVoieAdresseCorrespondance : '');
pdfFields['adresseCorrespondance2']  = 
(infoCpt.adresseCorrespondance.nomVoieAdresseCorrespondance != null ? infoCpt.adresseCorrespondance.nomVoieAdresseCorrespondance : '')
+ ' ' + (infoCpt.adresseCorrespondance.voieComplementAdresseCorrespondance != null ? infoCpt.adresseCorrespondance.voieComplementAdresseCorrespondance : '')
+ ' ' + (infoCpt.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance != null ? infoCpt.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance : '');
pdfFields['formaliteCorrespondanceAdresseCodePostal'] = infoCpt.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal != null ? infoCpt.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal : '';
pdfFields['formaliteCorrespondanceAdresseCommune']  = infoCpt.adresseCorrespondance.formaliteCorrespondanceAdresseCommune != null ? infoCpt.adresseCorrespondance.formaliteCorrespondanceAdresseCommune : '';
pdfFields['formaliteTelephone2'] = infoCpt.infosSup.formaliteTelephone2 != null ? infoCpt.infosSup.formaliteTelephone2 : '';
pdfFields['formaliteTelephone1']  = infoCpt.infosSup.formaliteTelephone1 != null ? infoCpt.infosSup.formaliteTelephone1 : '';
pdfFields['formaliteFaxCourriel'] = infoCpt.infosSup.formaliteFaxCourriel != null ? infoCpt.infosSup.formaliteFaxCourriel : '';

//Cadre 17 Signataire
pdfFields['formaliteSignataireQualiteCoExploitant']  = Value('id').of(signature.soussigne).eq('formaliteSignataireQualiteCoExploitant') != false ? true : false;
pdfFields['formaliteSignataireQualiteMandataire']  = Value('id').of(signature.soussigne).eq('formaliteSignataireQualiteMandataire') != false ? true : false;
pdfFields['nomPrenomDenominationMandataire']  = signature.adresseMandataire.nomPrenomDenominationMandataire != null ? signature.adresseMandataire.nomPrenomDenominationMandataire : '';
pdfFields['adresseMandataire']  = Value('id').of(signature.soussigne).eq('formaliteSignataireQualiteMandataire') ?
                                ((signature.adresseMandataire.numeroVoieMandataire != null ? signature.adresseMandataire.numeroVoieMandataire : '')
                                + ' ' + (signature.adresseMandataire.indiceVoieMandataire != null ? signature.adresseMandataire.indiceVoieMandataire : '')
                                + ' ' + (signature.adresseMandataire.typeVoieMandataire != null ? signature.adresseMandataire.typeVoieMandataire : '')
                                + ' ' + (signature.adresseMandataire.nomVoieMandataire != null ? signature.adresseMandataire.nomVoieMandataire : '')
                                + ' ' + (signature.adresseMandataire.complementVoieMandataire != null ? signature.adresseMandataire.complementVoieMandataire : '')
                                + ' ' + (signature.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signature.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
                                + ' ' + (signature.adresseMandataire.codePostalMandataire != null ? signature.adresseMandataire.codePostalMandataire : '')
                                + ' ' + (signature.adresseMandataire.villeAdresseMandataire != null ? signature.adresseMandataire.villeAdresseMandataire : '')) : '';

pdfFields['formaliteSignatureLieu']  = signature.formaliteSignatureLieu;
if (signature.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(signature.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
    date1 = date1.concat(dateTmp.getFullYear().toString());
    pdfFields['formaliteSignatureDate'] = date1;
}
pdfFields['signature'] =  "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce";
pdfFields['nbF']   = (membre4.membrePersonneMorale.personneLieePMDenomination != null 
and membre3.membrePersonneMorale.personneLieePMDenomination != null 
and membre2.membrePersonneMorale.personneLieePMDenomination != null) ? "3" :
                    ((membre3.membrePersonneMorale.personneLieePMDenomination != null
                    and membre2.membrePersonneMorale.personneLieePMDenomination != null 
                    and membre1.membrePersonneMorale.personneLieePMDenomination != null ? "2" :
                    ((membre3.membrePersonnePhysique.personneNomNaissance != null) 
                    or (membre2.membrePersonneMorale.personneLieePMDenomination != null and membre1.membrePersonneMorale.personneLieePMDenomination != null)
                    or (identification.etabSupp) 
                    or (etablissement.adresseAutreEtablissement) ? "1 " : '')));
pdfFields['nbNSP']  = membre1.membrePersonnePhysique.personneNomNaissance and membre2.membrePersonnePhysique.personneNomNaissance and membre3.membrePersonnePhysique.personneNomNaissance and membre4.membrePersonnePhysique.personneNomNaissance!= null ? "4"
                    : ((membre1.membrePersonnePhysique.personneNomNaissance and membre2.membrePersonnePhysique.personneNomNaissance and membre3.membrePersonnePhysique.personneNomNaissance != null ? "3"
                    : (membre1.membrePersonnePhysique.personneNomNaissance and membre2.membrePersonnePhysique.personneNomNaissance != null ? "2" 
                    : (membre1.membrePersonnePhysique.personneNomNaissance != null ? "1" 
                    : ''))));
                    
// NSP 1

formFieldsPers1['ayantDroit_lienParente_membreExploitation_NSP'] = true;
formFieldsPers1['ayantDroit_lienParente_aideFamilial_NSP'] = false;
formFieldsPers1['ayantDroit_lienParente_associeExploitation_NSP'] = false;
formFieldsPers1['ayantDroit_affiliationMSA_non_NSP']= false;
formFieldsPers1['ayantDroit_affiliationMSA_oui_NSP']  = false;
formFieldsPers1['formulaire_dependance_P0Agricole_NSP'] = false;
formFieldsPers1['formulaire_dependance_FAgricole_NSP']  = true;
formFieldsPers1['formulaire_dependance_M0Agricole_NSP']  = false;
formFieldsPers1['formulaire_dependance_M2Agricole_NSP']= false;
formFieldsPers1['formulaire_dependance_M3_NSP'] = false;

//Cadre 2 Rappel identification
formFieldsPers1['exploitationCommunNom_NSP'] = identification.entrepriseNom;

//Cadre 3 Personne non salariée participant aux travaux
var nirDeclarant1 = nsp1.voletSocialNumeroSecuriteSociale;
if(nirDeclarant1 != null) {
    nirDeclarant1 = nirDeclarant1.replace(/ /g, "");
    formFieldsPers1['ayantDroit_numeroSecuriteSociale_NSP'] = nirDeclarant1.substring(0, 13);
    formFieldsPers1['ayantDroit_numeroSecuriteSocialeCle_NSP']  = nirDeclarant1.substring(13, 15);
} 

formFieldsPers1['ayantDroit_identite_nomNaissance_NSP']    = membre1.membrePersonnePhysique.personneNomNaissance != null ? membre1.membrePersonnePhysique.personneNomNaissance : '';
formFieldsPers1['ayantDroit_identite_nomUsage_NSP'] = membre1.membrePersonnePhysique.personneNomUsage != null ? membre1.membrePersonnePhysique.personneNomUsage : '';
if (membre1.membrePersonnePhysique.personnePrenom != null and membre1.membrePersonnePhysique.personnePrenom[0].length > 0) {
    var prenoms = [];
    for (i = 0; i < membre1.membrePersonnePhysique.personnePrenom.size(); i++) {
        prenoms.push(membre1.membrePersonnePhysique.personnePrenom[i]);
    }
formFieldsPers1['ayantDroit_identite_prenom_NSP'] =  prenoms.toString();
}
formFieldsPers1['ayantDroit_connuMSA_oui_NSP']  = nsp1.voletSocialAffiliationMSAOui ? true : false;
formFieldsPers1['ayantDroit_connuMSA_non_NSP']  = nsp1.voletSocialAffiliationMSAOui ? false : true;

formFieldsPers1['ayantDroit_regimeAssuranceMaladie_regimeGeneral_NSP'] = Value('id').of(nsp1.voletSocialRegimeAssuranceMaladie).eq('voletSocialRegimeAssuranceMaladieStatutSalarie') ? true : false;
formFieldsPers1['ayantDroit_regimeAssuranceMaladie_agricole_NSP']= Value('id').of(nsp1.voletSocialRegimeAssuranceMaladie).eq('voletSocialRegimeAssuranceMaladieStatutAgricole') ? true : false;
formFieldsPers1['ayantDroit_regimeAssuranceMaladie_nonSalarieNonAgricole_NSP'] = Value('id').of(nsp1.voletSocialRegimeAssuranceMaladie).eq('voletSocialRegimeAssuranceMaladieStatutNonSalarieNonAgricole') ? true : false;
formFieldsPers1['ayantDroit_regimeAssuranceMaladie_autre_NSP'] = Value('id').of(nsp1.voletSocialRegimeAssuranceMaladie).eq('voletSocialRegimeAssuranceMaladieStatutAutre') ? true : false;
formFieldsPers1['ayantDroit_regimeAssuranceMaladieAutre_NSP'] = nsp1.voletSocialRegimeAssuranceMaladieStatutAutreTexte;

formFieldsPers1['ayantDroit_activiteAutreQueDeclareeStatut_oui'] = nsp1.voletSocialActiviteAutreQueDeclareeStatutOui ? true : false;
formFieldsPers1['ayantDroit_activiteAutreQueDeclareeStatut_non'] = nsp1.voletSocialActiviteAutreQueDeclareeStatutOui ? false : true;
formFieldsPers1['ayantDroit_regimeAssuranceMaladie_agricole_simultane_NSP']  = Value('id').of(nsp1.ayantDroitActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutAgricole') ? true : false;
formFieldsPers1['ayantDroit_activiteAutreQueDeclareeStatut_salarie_NSP']                 = Value('id').of(nsp1.ayantDroitActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutSalarie') ? true : false;
formFieldsPers1['ayantDroit_regimeAssuranceMaladie_nonSalarieNonAgricole_simultane_NSP'] = Value('id').of(nsp1.ayantDroitActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutNonSalarieNonAgricole') ? true : false;
formFieldsPers1['ayantDroit_activiteAutreQueDeclareeStatut_retraite_NSP'] = Value('id').of(nsp1.ayantDroitActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutRetraite') ? true : false;
formFieldsPers1['ayantDroit_activiteAutreQueDeclareeStatut_pensionneInvalidite_NSP']  = Value('id').of(nsp1.ayantDroitActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutPensionneInvalidite') ? true : false;
formFieldsPers1['ayantDroit_regimeAssuranceMaladie_autre_simultane_NSP']  = Value('id').of(nsp1.ayantDroitActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutAutre') ? true : false;
formFieldsPers1['ayantDroit_regimeAssuranceMaladieAutre_simultane_NSP']   = nsp1.voletSocialActiviteAutreQueDeclareeStatutAutreTexte;
formFieldsPers1['ayantDroit_organismeServantPension_NSP'] = nsp1.voletSocialOrganismeServentPension;

formFieldsPers1['ayantDroit_conjointCouvertAssuranceMaladie_oui_NSP']  = Value('id').of(nsp1.voletSocialConjointCouvertAssuranceMaladieOui).eq('oui') ? true : false;
formFieldsPers1['ayantDroit_conjointCouvertAssuranceMaladie_non_NSP']   = Value('id').of(nsp1.voletSocialConjointCouvertAssuranceMaladieOui).eq('non') ? true : false;

var nirDeclarant = nsp1.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers1['ayantDroit_conjointCouvertAssuranceMaladie_numeroSS_NSP']                = nirDeclarant.substring(0, 13);
    formFieldsPers1['ayantDroit_conjointCouvertAssuranceMaladie_numeroSSCle_NSP']             = nirDeclarant.substring(13, 15);
}

// NSP 1 Ayant droit 1 

formFieldsPers1['ayantDroit_identite_nomNaissance1_NSP']     = nsp1.ayantDroitsDeclaration ? (nsp1.ayantsdroits1.ayantDroitIdentiteNomNaissance + ' ' + nsp1.ayantsdroits1.ayantDroitIdentitePrenomNaissance) : '';
formFieldsPers1['ayantDroit_numeroSecuriteSociale1_NSP']   = nsp1.ayantDroitsDeclaration ? (nsp1.ayantsdroits1.ayantDroitNumeroSecuriteSociale != null ? nsp1.ayantsdroits1.ayantDroitNumeroSecuriteSociale : '') : '';
formFieldsPers1['ayantDroit_identiteComplement_dateNaissance1_NSP']   = nsp1.ayantDroitsDeclaration ? (nsp1.ayantsdroits1.ayantDroitDateNaissance != null ? nsp1.ayantsdroits1.ayantDroitDateNaissance : '') : '';
formFieldsPers1['ayantDroit_identiteComplement_lieuNaissanceCommune1_NSP']  = nsp1.ayantDroitsDeclaration ? (nsp1.ayantsdroits1.ayantDroitNumeroSecuriteSociale != null ? "" :
																						((nsp1.ayantsdroits1.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? 
																						nsp1.ayantsdroits1.ayantDroitIdentiteComplementLieuNaissanceCommune : nsp1.ayantsdroits1.ayantDroitIdentiteComplementLieuNaissanceVille)			
																						 + ' / ' + nsp1.ayantsdroits1.ayantDroitIdentiteComplementLieuNaissancePays 
																						 + ' ' + (Value('id').of(nsp1.ayantsdroits1.ayantDroitSexe).eq('feminin') ? "F" : "M"))) : '';
formFieldsPers1['ayantDroit_lienParente1_NSP']   = nsp1.ayantDroitsDeclaration ? (nsp1.ayantsdroits1.ayantDroitLienParente != null ? nsp1.ayantsdroits1.ayantDroitLienParente : '') : '';
formFieldsPers1['ayantDroit_enfantScolarise_oui1_NSP'] = nsp1.ayantDroitsDeclaration ? (Value('id').of(nsp1.ayantsdroits1.ayantDroitLienParente).eq('enfant') ? (nsp1.ayantsdroits1.dataEnfantScolariseBeneficiaireAM ? true : false) : false) : false;
formFieldsPers1['ayantDroit_enfantScolarise_non1_NSP']   = nsp1.ayantDroitsDeclaration ? (Value('id').of(nsp1.ayantsdroits1.ayantDroitLienParente).eq('enfant') ? (nsp1.ayantsdroits1.dataEnfantScolariseBeneficiaireAM ? false : true) : false) : false;
formFieldsPers1['ayantDroit_nationalite1_NSP']  = nsp1.ayantDroitsDeclaration ? (nsp1.ayantsdroits1.ayantDroitNationalite != null ? nsp1.ayantsdroits1.ayantDroitNationalite : '') : '';


// NSP 1 Ayant droit 2 

formFieldsPers1['ayantDroit_identite_nomNaissance2_NSP']                                 = nsp1.ayantsdroits1.autreBeneficiaireAM ? (nsp1.ayantsdroits2.ayantDroitIdentiteNomNaissance + ' ' + nsp1.ayantsdroits2.ayantDroitIdentitePrenomNaissance) : '';
formFieldsPers1['ayantDroit_numeroSecuriteSociale2_NSP']                                 = nsp1.ayantsdroits1.autreBeneficiaireAM ? (nsp1.ayantsdroits2.ayantDroitNumeroSecuriteSociale != null ? nsp1.ayantsdroits2.ayantDroitNumeroSecuriteSociale : '') : '';
formFieldsPers1['ayantDroit_identiteComplement_dateNaissance2_NSP']                      = nsp1.ayantsdroits1.autreBeneficiaireAM ? (nsp1.ayantsdroits2.ayantDroitDateNaissance != null ? nsp1.ayantsdroits2.ayantDroitDateNaissance : '') : '';
formFieldsPers1['ayantDroit_identiteComplement_lieuNaissanceCommune2_NSP']               = nsp1.ayantsdroits1.autreBeneficiaireAM ? (nsp1.ayantsdroits2.ayantDroitNumeroSecuriteSociale != null ? "" :
																						((nsp1.ayantsdroits2.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? 
																						nsp1.ayantsdroits2.ayantDroitIdentiteComplementLieuNaissanceCommune : nsp1.ayantsdroits2.ayantDroitIdentiteComplementLieuNaissanceVille)			
																						 + ' / ' + nsp1.ayantsdroits2.ayantDroitIdentiteComplementLieuNaissancePays 
																						 + ' ' + (Value('id').of(nsp1.ayantsdroits2.ayantDroitSexe).eq('feminin') ? "F" : "M"))) : '';
formFieldsPers1['ayantDroit_lienParente2_NSP']                                           = nsp1.ayantsdroits1.autreBeneficiaireAM ? (nsp1.ayantsdroits2.ayantDroitLienParente != null ? nsp1.ayantsdroits2.ayantDroitLienParente : '') : '';
formFieldsPers1['ayantDroit_enfantScolarise_oui2_NSP']                                   = nsp1.ayantsdroits1.autreBeneficiaireAM ? (Value('id').of(nsp1.ayantsdroits2.ayantDroitLienParente).eq('enfant') ? (nsp1.ayantsdroits2.dataEnfantScolariseBeneficiaireAM ? true : false) : false) : false;
formFieldsPers1['ayantDroit_enfantScolarise_non2_NSP']                                   = nsp1.ayantsdroits1.autreBeneficiaireAM ? (Value('id').of(nsp1.ayantsdroits2.ayantDroitLienParente).eq('enfant') ? (nsp1.ayantsdroits2.dataEnfantScolariseBeneficiaireAM ? false : true) : false) : false;
formFieldsPers1['ayantDroit_nationalite2_NSP']                                           = nsp1.ayantsdroits1.autreBeneficiaireAM ? (nsp1.ayantsdroits2.ayantDroitNationalite != null ? nsp1.ayantsdroits2.ayantDroitNationalite : '') : '';


// NSP 1 Ayant droit 3 

formFieldsPers1['ayantDroit_identite_nomNaissance3_NSP']                                 = nsp1.ayantsdroits2.autreBeneficiaireAM ? (nsp1.ayantsdroits3.ayantDroitIdentiteNomNaissance + ' ' + nsp1.ayantsdroits3.ayantDroitIdentitePrenomNaissance) : '';
formFieldsPers1['ayantDroit_numeroSecuriteSociale3_NSP']                                 = nsp1.ayantsdroits2.autreBeneficiaireAM ? (nsp1.ayantsdroits3.ayantDroitNumeroSecuriteSociale != null ? nsp1.ayantsdroits3.ayantDroitNumeroSecuriteSociale : '') : '';
formFieldsPers1['ayantDroit_identiteComplement_dateNaissance3_NSP']                      = nsp1.ayantsdroits2.autreBeneficiaireAM ? (nsp1.ayantsdroits3.ayantDroitDateNaissance != null ? nsp1.ayantsdroits3.ayantDroitDateNaissance : '') : '';
formFieldsPers1['ayantDroit_identiteComplement_lieuNaissanceCommune3_NSP']               = nsp1.ayantsdroits2.autreBeneficiaireAM ? (nsp1.ayantsdroits3.ayantDroitNumeroSecuriteSociale != null ? "" :
																						((nsp1.ayantsdroits3.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? 
																						nsp1.ayantsdroits3.ayantDroitIdentiteComplementLieuNaissanceCommune : nsp1.ayantsdroits3.ayantDroitIdentiteComplementLieuNaissanceVille)			
																						 + ' / ' + nsp1.ayantsdroits3.ayantDroitIdentiteComplementLieuNaissancePays 
																						 + ' ' + (Value('id').of(nsp1.ayantsdroits3.ayantDroitSexe).eq('feminin') ? "F" : "M"))) : '';
formFieldsPers1['ayantDroit_lienParente3_NSP']                                           = nsp1.ayantsdroits2.autreBeneficiaireAM ? (nsp1.ayantsdroits3.ayantDroitLienParente != null ? nsp1.ayantsdroits3.ayantDroitLienParente : '') : '';
formFieldsPers1['ayantDroit_enfantScolarise_oui3_NSP']                                   = nsp1.ayantsdroits2.autreBeneficiaireAM ? (Value('id').of(nsp1.ayantsdroits3.ayantDroitLienParente).eq('enfant') ? (nsp1.ayantsdroits3.dataEnfantScolariseBeneficiaireAM ? true : false) : false) : false;
formFieldsPers1['ayantDroit_enfantScolarise_non3_NSP']                                   = nsp1.ayantsdroits2.autreBeneficiaireAM ? (Value('id').of(nsp1.ayantsdroits3.ayantDroitLienParente).eq('enfant') ? (nsp1.ayantsdroits3.dataEnfantScolariseBeneficiaireAM ? false : true) : false) : false;
formFieldsPers1['ayantDroit_nationalite3_NSP']                                           = nsp1.ayantsdroits2.autreBeneficiaireAM ? (nsp1.ayantsdroits3.ayantDroitNationalite != null ? nsp1.ayantsdroits3.ayantDroitNationalite : '') : '';

//Pour les membres d'exploitation en commun
formFieldsPers1['voletSocial_qualiteJeuneAgriculteur_Oui'] = Value('id').of(nsp1.voletSocialEstJeuneAgriculteur).eq('VoletSocialEstJeuneAgriculteurOui') ? true : false;
formFieldsPers1['voletSocial_qualiteJeuneAgriculteur_Non'] = Value('id').of(nsp1.voletSocialEstJeuneAgriculteur).eq('VoletSocialEstJeuneAgriculteurNon') ? true : false;
formFieldsPers1['voletSocial_qualiteJeuneAgriculteur_Encours'] = Value('id').of(nsp1.voletSocialEstJeuneAgriculteur).eq('VoletSocialDemandeDotationJeuneAgriculteur') ? true : false;
formFieldsPers1['voletSocial_exploitationCommun_statutConjoint_collaborateur']  = Value('id').of(nsp1.statutConjointCollaborateur).eq('VoletSocialStatutConjointCollaborateur') ? true : false ;
formFieldsPers1['voletSocial_exploitationCommun_statutConjoint_salarie']   = Value('id').of(nsp1.statutConjointCollaborateur).eq('VoletSocialStatutConjointSalarie') ? true : false ;
formFieldsPers1['voletSocial_exploitationCommun_statutConjoint_coexploitant']  =  Value('id').of(nsp1.statutConjointCollaborateur).eq('VoletSocialStatutConjointCoExploitant') ? true : false;
formFieldsPers1['voletSocial_estBeneficiaireRASRMI_oui_NSP']   = nsp1.beneficiairePrimeActivite  ? true : false ;
formFieldsPers1['voletSocial_estBeneficiaireRASRMI_non_NSP']   = nsp1.beneficiairePrimeActivite  ? false : true ;
 
//Signature NSP 1
formFieldsPers1['formalite_signataireQualite_declarantNSP']  = Value('id').of(signature.soussigne).eq('formaliteSignataireQualiteCoExploitant') != false ? true : false;
formFieldsPers1['formalite_signataireQualite_mandataireNSP'] = Value('id').of(signature.soussigne).eq('formaliteSignataireQualiteMandataire') != false ? true : false;
formFieldsPers1['formalite_signataireNom_NSP']   = Value('id').of(signature.soussigne). eq('formaliteSignataireQualiteMandataire') ? signature.adresseMandataire.nomPrenomDenominationMandataire : '';
formFieldsPers1['formalite_signatureLieu_NSP']   = signature.formaliteSignatureLieu != null ? signature.formaliteSignatureLieu : '';
formFieldsPers1['formalite_signataireAdresse_NSP'] = Value('id').of(signature.soussigne).eq('formaliteSignataireQualiteMandataire') ?
((signature.adresseMandataire.numeroVoieMandataire != null ? signature.adresseMandataire.numeroVoieMandataire : '')
+ ' ' + (signature.adresseMandataire.indiceVoieMandataire != null ? signature.adresseMandataire.indiceVoieMandataire : '')
+ ' ' + (signature.adresseMandataire.typeVoieMandataire != null ? signature.adresseMandataire.typeVoieMandataire : '')
+ ' ' + (signature.adresseMandataire.nomVoieMandataire != null ? signature.adresseMandataire.nomVoieMandataire : '')
+ ' ' + (signature.adresseMandataire.complementVoieMandataire != null ? signature.adresseMandataire.complementVoieMandataire : '')
+ ' ' + (signature.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signature.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
+ ' ' + (signature.adresseMandataire.codePostalMandataire != null ? signature.adresseMandataire.codePostalMandataire : '')
+ ' ' + (signature.adresseMandataire.villeAdresseMandataire != null ? signature.adresseMandataire.villeAdresseMandataire : '')) : '';
formFieldsPers1['formalite_signatureDate_NSP']                                           = signature.formaliteSignatureDate != null ? signature.formaliteSignatureDate : '';
formFieldsPers1['nombreP0prime_NSP']                                                     = "0";
formFieldsPers1['signature_NSP']                                                         = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce";


// NSP 2

formFieldsPers2['ayantDroit_lienParente_membreExploitation_NSP'] = true;
formFieldsPers2['ayantDroit_lienParente_aideFamilial_NSP'] = false;
formFieldsPers2['ayantDroit_lienParente_associeExploitation_NSP'] = false;
formFieldsPers2['ayantDroit_affiliationMSA_non_NSP']= false;
formFieldsPers2['ayantDroit_affiliationMSA_oui_NSP']  = false;
formFieldsPers2['formulaire_dependance_P0Agricole_NSP'] = false;
formFieldsPers2['formulaire_dependance_FAgricole_NSP']  = true;
formFieldsPers2['formulaire_dependance_M0Agricole_NSP']  = false;
formFieldsPers2['formulaire_dependance_M2Agricole_NSP']= false;
formFieldsPers2['formulaire_dependance_M3_NSP'] = false;

//Cadre 2 Rappel identification
formFieldsPers2['exploitationCommunNom_NSP'] = identification.entrepriseNom;

//Cadre 3 Personne non salariée participant aux travaux
var nirDeclarant1 = nsp2.voletSocialNumeroSecuriteSociale;
if(nirDeclarant1 != null) {
    nirDeclarant1 = nirDeclarant1.replace(/ /g, "");
    formFieldsPers2['ayantDroit_numeroSecuriteSociale_NSP'] = nirDeclarant1.substring(0, 13);
    formFieldsPers2['ayantDroit_numeroSecuriteSocialeCle_NSP']  = nirDeclarant1.substring(13, 15);
} 

formFieldsPers2['ayantDroit_identite_nomNaissance_NSP']   = membre2.membrePersonnePhysique.personneNomNaissance != null ? membre2.membrePersonnePhysique.personneNomNaissance : '';
formFieldsPers2['ayantDroit_identite_nomUsage_NSP'] = membre2.membrePersonnePhysique.personneNomUsage != null ? membre2.membrePersonnePhysique.personneNomUsage : '';
if (membre2.membrePersonnePhysique.personnePrenom != null and membre2.membrePersonnePhysique.personnePrenom[0].length > 0) {
    var prenoms = [];
    for (i = 0; i < membre2.membrePersonnePhysique.personnePrenom.size(); i++) {
        prenoms.push(membre2.membrePersonnePhysique.personnePrenom[i]);
    }
formFieldsPers2['ayantDroit_identite_prenom_NSP'] =  prenoms.toString();
}
formFieldsPers2['ayantDroit_connuMSA_oui_NSP']  = nsp2.voletSocialAffiliationMSAOui ? true : false;
formFieldsPers2['ayantDroit_connuMSA_non_NSP']  = nsp2.voletSocialAffiliationMSAOui ? false : true;

formFieldsPers2['ayantDroit_regimeAssuranceMaladie_regimeGeneral_NSP'] = Value('id').of(nsp2.voletSocialRegimeAssuranceMaladie).eq('voletSocialRegimeAssuranceMaladieStatutSalarie') ? true : false;
formFieldsPers2['ayantDroit_regimeAssuranceMaladie_agricole_NSP']= Value('id').of(nsp2.voletSocialRegimeAssuranceMaladie).eq('voletSocialRegimeAssuranceMaladieStatutAgricole') ? true : false;
formFieldsPers2['ayantDroit_regimeAssuranceMaladie_nonSalarieNonAgricole_NSP'] = Value('id').of(nsp2.voletSocialRegimeAssuranceMaladie).eq('voletSocialRegimeAssuranceMaladieStatutNonSalarieNonAgricole') ? true : false;
formFieldsPers2['ayantDroit_regimeAssuranceMaladie_autre_NSP'] = Value('id').of(nsp2.voletSocialRegimeAssuranceMaladie).eq('voletSocialRegimeAssuranceMaladieStatutAutre') ? true : false;
formFieldsPers2['ayantDroit_regimeAssuranceMaladieAutre_NSP'] = nsp2.voletSocialRegimeAssuranceMaladieStatutAutreTexte;

formFieldsPers2['ayantDroit_activiteAutreQueDeclareeStatut_oui'] = nsp2.voletSocialActiviteAutreQueDeclareeStatutOui ? true : false;
formFieldsPers2['ayantDroit_activiteAutreQueDeclareeStatut_non'] = nsp2.voletSocialActiviteAutreQueDeclareeStatutOui ? false : true;
formFieldsPers2['ayantDroit_regimeAssuranceMaladie_agricole_simultane_NSP']  = Value('id').of(nsp2.ayantDroitActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutAgricole') ? true : false;
formFieldsPers2['ayantDroit_activiteAutreQueDeclareeStatut_salarie_NSP']                 = Value('id').of(nsp2.ayantDroitActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutSalarie') ? true : false;
formFieldsPers2['ayantDroit_regimeAssuranceMaladie_nonSalarieNonAgricole_simultane_NSP'] = Value('id').of(nsp2.ayantDroitActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutNonSalarieNonAgricole') ? true : false;
formFieldsPers2['ayantDroit_activiteAutreQueDeclareeStatut_retraite_NSP'] = Value('id').of(nsp2.ayantDroitActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutRetraite') ? true : false;
formFieldsPers2['ayantDroit_activiteAutreQueDeclareeStatut_pensionneInvalidite_NSP']  = Value('id').of(nsp2.ayantDroitActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutPensionneInvalidite') ? true : false;
formFieldsPers2['ayantDroit_regimeAssuranceMaladie_autre_simultane_NSP']  = Value('id').of(nsp2.ayantDroitActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutAutre') ? true : false;
formFieldsPers2['ayantDroit_regimeAssuranceMaladieAutre_simultane_NSP']   = nsp2.voletSocialActiviteAutreQueDeclareeStatutAutreTexte;
formFieldsPers2['ayantDroit_organismeServantPension_NSP'] = nsp2.voletSocialOrganismeServentPension;

formFieldsPers2['ayantDroit_conjointCouvertAssuranceMaladie_oui_NSP']  = Value('id').of(nsp2.voletSocialConjointCouvertAssuranceMaladieOui).eq('oui') ? true : false;
formFieldsPers2['ayantDroit_conjointCouvertAssuranceMaladie_non_NSP']   = Value('id').of(nsp2.voletSocialConjointCouvertAssuranceMaladieOui).eq('non') ? true : false;

var nirDeclarant = nsp2.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers2['ayantDroit_conjointCouvertAssuranceMaladie_numeroSS_NSP']                = nirDeclarant.substring(0, 13);
    formFieldsPers2['ayantDroit_conjointCouvertAssuranceMaladie_numeroSSCle_NSP']             = nirDeclarant.substring(13, 15);
}

// NSP 2 Ayant droit 1 

formFieldsPers2['ayantDroit_identite_nomNaissance1_NSP']     = nsp2.ayantDroitsDeclaration ? (nsp2.ayantsdroits1.ayantDroitIdentiteNomNaissance + ' ' + nsp2.ayantsdroits1.ayantDroitIdentitePrenomNaissance) : '';
formFieldsPers2['ayantDroit_numeroSecuriteSociale1_NSP']   = nsp2.ayantDroitsDeclaration ? (nsp2.ayantsdroits1.ayantDroitNumeroSecuriteSociale != null ? nsp2.ayantsdroits1.ayantDroitNumeroSecuriteSociale : '') : '';
formFieldsPers2['ayantDroit_identiteComplement_dateNaissance1_NSP']   = nsp2.ayantDroitsDeclaration ? (nsp2.ayantsdroits1.ayantDroitDateNaissance != null ? nsp2.ayantsdroits1.ayantDroitDateNaissance : '') : '';
formFieldsPers2['ayantDroit_identiteComplement_lieuNaissanceCommune1_NSP']  = nsp2.ayantDroitsDeclaration ? (nsp2.ayantsdroits1.ayantDroitNumeroSecuriteSociale != null ? "" :
																						((nsp2.ayantsdroits1.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? 
																						nsp2.ayantsdroits1.ayantDroitIdentiteComplementLieuNaissanceCommune : nsp2.ayantsdroits1.ayantDroitIdentiteComplementLieuNaissanceVille)			
																						 + ' / ' + nsp2.ayantsdroits1.ayantDroitIdentiteComplementLieuNaissancePays 
																						 + ' ' + (Value('id').of(nsp2.ayantsdroits1.ayantDroitSexe).eq('feminin') ? "F" : "M"))) : '';
formFieldsPers2['ayantDroit_lienParente1_NSP']   = nsp2.ayantDroitsDeclaration ? (nsp2.ayantsdroits1.ayantDroitLienParente != null ? nsp2.ayantsdroits1.ayantDroitLienParente : '') : '';
formFieldsPers2['ayantDroit_enfantScolarise_oui1_NSP'] = nsp2.ayantDroitsDeclaration ? (Value('id').of(nsp2.ayantsdroits1.ayantDroitLienParente).eq('enfant') ? (nsp2.ayantsdroits1.dataEnfantScolariseBeneficiaireAM ? true : false) : false) : false;
formFieldsPers2['ayantDroit_enfantScolarise_non1_NSP']   = nsp2.ayantDroitsDeclaration ? (Value('id').of(nsp2.ayantsdroits1.ayantDroitLienParente).eq('enfant') ? (nsp2.ayantsdroits1.dataEnfantScolariseBeneficiaireAM ? false : true) : false) : false;
formFieldsPers2['ayantDroit_nationalite1_NSP']  = nsp2.ayantDroitsDeclaration ? (nsp2.ayantsdroits1.ayantDroitNationalite != null ? nsp2.ayantsdroits1.ayantDroitNationalite : '') : '';


// NSP 2 Ayant droit 2 

formFieldsPers2['ayantDroit_identite_nomNaissance2_NSP']                                 = nsp2.ayantsdroits1.autreBeneficiaireAM ? (nsp2.ayantsdroits2.ayantDroitIdentiteNomNaissance + ' ' + nsp2.ayantsdroits2.ayantDroitIdentitePrenomNaissance) : '';
formFieldsPers2['ayantDroit_numeroSecuriteSociale2_NSP']                                 = nsp2.ayantsdroits1.autreBeneficiaireAM ? (nsp2.ayantsdroits2.ayantDroitNumeroSecuriteSociale != null ? nsp2.ayantsdroits2.ayantDroitNumeroSecuriteSociale : '') : '';
formFieldsPers2['ayantDroit_identiteComplement_dateNaissance2_NSP']                      = nsp2.ayantsdroits1.autreBeneficiaireAM ? (nsp2.ayantsdroits2.ayantDroitDateNaissance != null ? nsp2.ayantsdroits2.ayantDroitDateNaissance : '') : '';
formFieldsPers2['ayantDroit_identiteComplement_lieuNaissanceCommune2_NSP']               = nsp2.ayantsdroits1.autreBeneficiaireAM ? (nsp2.ayantsdroits2.ayantDroitNumeroSecuriteSociale != null ? "" :
																						((nsp2.ayantsdroits2.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? 
																						nsp2.ayantsdroits2.ayantDroitIdentiteComplementLieuNaissanceCommune : nsp2.ayantsdroits2.ayantDroitIdentiteComplementLieuNaissanceVille)			
																						 + ' / ' + nsp2.ayantsdroits2.ayantDroitIdentiteComplementLieuNaissancePays 
																						 + ' ' + (Value('id').of(nsp2.ayantsdroits2.ayantDroitSexe).eq('feminin') ? "F" : "M"))) : '';
formFieldsPers2['ayantDroit_lienParente2_NSP']                                           = nsp2.ayantsdroits1.autreBeneficiaireAM ? (nsp2.ayantsdroits2.ayantDroitLienParente != null ? nsp2.ayantsdroits2.ayantDroitLienParente : '') : '';
formFieldsPers2['ayantDroit_enfantScolarise_oui2_NSP']                                   = nsp2.ayantsdroits1.autreBeneficiaireAM ? (Value('id').of(nsp2.ayantsdroits2.ayantDroitLienParente).eq('enfant') ? (nsp2.ayantsdroits2.dataEnfantScolariseBeneficiaireAM ? true : false) : false) : false;
formFieldsPers2['ayantDroit_enfantScolarise_non2_NSP']                                   = nsp2.ayantsdroits1.autreBeneficiaireAM ? (Value('id').of(nsp2.ayantsdroits2.ayantDroitLienParente).eq('enfant') ? (nsp2.ayantsdroits2.dataEnfantScolariseBeneficiaireAM ? false : true) : false) : false;
formFieldsPers2['ayantDroit_nationalite2_NSP']                                           = nsp2.ayantsdroits1.autreBeneficiaireAM ? (nsp2.ayantsdroits2.ayantDroitNationalite != null ? nsp2.ayantsdroits2.ayantDroitNationalite : '') : '';


// NSP 2 Ayant droit 3 

formFieldsPers2['ayantDroit_identite_nomNaissance3_NSP']                                 = nsp2.ayantsdroits2.autreBeneficiaireAM ? (nsp2.ayantsdroits3.ayantDroitIdentiteNomNaissance + ' ' + nsp2.ayantsdroits3.ayantDroitIdentitePrenomNaissance) : '';
formFieldsPers2['ayantDroit_numeroSecuriteSociale3_NSP']                                 = nsp2.ayantsdroits2.autreBeneficiaireAM ? (nsp2.ayantsdroits3.ayantDroitNumeroSecuriteSociale != null ? nsp2.ayantsdroits3.ayantDroitNumeroSecuriteSociale : '') : '';
formFieldsPers2['ayantDroit_identiteComplement_dateNaissance3_NSP']                      = nsp2.ayantsdroits2.autreBeneficiaireAM ? (nsp2.ayantsdroits3.ayantDroitDateNaissance != null ? nsp2.ayantsdroits3.ayantDroitDateNaissance : '') : '';
formFieldsPers2['ayantDroit_identiteComplement_lieuNaissanceCommune3_NSP']               = nsp2.ayantsdroits2.autreBeneficiaireAM ? (nsp2.ayantsdroits3.ayantDroitNumeroSecuriteSociale != null ? "" :
																						((nsp2.ayantsdroits3.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? 
																						nsp2.ayantsdroits3.ayantDroitIdentiteComplementLieuNaissanceCommune : nsp2.ayantsdroits3.ayantDroitIdentiteComplementLieuNaissanceVille)			
																						 + ' / ' + nsp2.ayantsdroits3.ayantDroitIdentiteComplementLieuNaissancePays 
																						 + ' ' + (Value('id').of(nsp2.ayantsdroits3.ayantDroitSexe).eq('feminin') ? "F" : "M"))) : '';
formFieldsPers2['ayantDroit_lienParente3_NSP']                                           = nsp2.ayantsdroits2.autreBeneficiaireAM ? (nsp2.ayantsdroits3.ayantDroitLienParente != null ? nsp2.ayantsdroits3.ayantDroitLienParente : '') : '';
formFieldsPers2['ayantDroit_enfantScolarise_oui3_NSP']                                   = nsp2.ayantsdroits2.autreBeneficiaireAM ? (Value('id').of(nsp2.ayantsdroits3.ayantDroitLienParente).eq('enfant') ? (nsp2.ayantsdroits3.dataEnfantScolariseBeneficiaireAM ? true : false) : false) : false;
formFieldsPers2['ayantDroit_enfantScolarise_non3_NSP']                                   = nsp2.ayantsdroits2.autreBeneficiaireAM ? (Value('id').of(nsp2.ayantsdroits3.ayantDroitLienParente).eq('enfant') ? (nsp2.ayantsdroits3.dataEnfantScolariseBeneficiaireAM ? false : true) : false) : false;
formFieldsPers2['ayantDroit_nationalite3_NSP']                                           = nsp2.ayantsdroits2.autreBeneficiaireAM ? (nsp2.ayantsdroits3.ayantDroitNationalite != null ? nsp2.ayantsdroits3.ayantDroitNationalite : '') : '';

//Pour les membres d'explotation en commun
formFieldsPers2['voletSocial_qualiteJeuneAgriculteur_Oui'] = Value('id').of(nsp2.voletSocialEstJeuneAgriculteur).eq('VoletSocialEstJeuneAgriculteurOui') ? true : false;
formFieldsPers2['voletSocial_qualiteJeuneAgriculteur_Non'] = Value('id').of(nsp2.voletSocialEstJeuneAgriculteur).eq('VoletSocialEstJeuneAgriculteurNon') ? true : false;
formFieldsPers2['voletSocial_qualiteJeuneAgriculteur_Encours'] = Value('id').of(nsp2.voletSocialEstJeuneAgriculteur).eq('VoletSocialDemandeDotationJeuneAgriculteur') ? true : false;
formFieldsPers2['voletSocial_exploitationCommun_statutConjoint_collaborateur']  = Value('id').of(nsp2.statutConjointCollaborateur).eq('VoletSocialStatutConjointCollaborateur') ? true : false ;
formFieldsPers2['voletSocial_exploitationCommun_statutConjoint_salarie']   = Value('id').of(nsp2.statutConjointCollaborateur).eq('VoletSocialStatutConjointSalarie') ? true : false ;
formFieldsPers2['voletSocial_exploitationCommun_statutConjoint_coexploitant']  =  Value('id').of(nsp2.statutConjointCollaborateur).eq('VoletSocialStatutConjointCoExploitant') ? true : false;
formFieldsPers2['voletSocial_estBeneficiaireRASRMI_oui_NSP']   = nsp2.beneficiairePrimeActivite  ? true : false ;
formFieldsPers2['voletSocial_estBeneficiaireRASRMI_non_NSP']   = nsp2.beneficiairePrimeActivite  ? false : true ;
 
//Signature Nsp 2
formFieldsPers2['formalite_signataireQualite_declarantNSP']    = Value('id').of(signature.soussigne).eq('formaliteSignataireQualiteCoExploitant') != false ? true : false;
formFieldsPers2['formalite_signataireQualite_mandataireNSP']   = Value('id').of(signature.soussigne). eq('formaliteSignataireQualiteMandataire') != false ? true : false;
formFieldsPers2['formalite_signataireNom_NSP']                                           = Value('id').of(signature.soussigne). eq('formaliteSignataireQualiteMandataire') ? signature.adresseMandataire.nomPrenomDenominationMandataire : '';
formFieldsPers2['formalite_signatureLieu_NSP']                                           = signature.formaliteSignatureLieu != null ? signature.formaliteSignatureLieu : '';
/*if (signature.adresseMandataire.nomPrenomDenominationMandataire != false){
    formFieldsPers2['formalite_signataireAdresse_NSP'] = 
    (signature.adresseMandataire.numeroVoieMandataire != false ? signature.adresseMandataire.numeroVoieMandataire : '')
    + ' ' + (signature.adresseMandataire.indiceVoieMandataire != false ? signature.adresseMandataire.indiceVoieMandataire : '')
    + ' ' + (signature.adresseMandataire.typeVoieMandataire != false ? signature.adresseMandataire.typeVoieMandataire : '')
    + ' ' + (signature.adresseMandataire.nomVoieMandataire != false ? signature.adresseMandataire.nomVoieMandataire : '')
    + ' ' + (signature.adresseMandataire.complementVoieMandataire != false ? signature.adresseMandataire.complementVoieMandataire : '')
    + ' ' + (signature.adresseMandataire.voieDistributionSpecialeAdresseMandataire != false ? signature.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
    + ' ' + (signature.adresseMandataire.codePostalMandataire != false ? signature.adresseMandataire.codePostalMandataire : '')
    + ' ' + (signature.adresseMandataire.villeAdresseMandataire != false ? signature.adresseMandataire.villeAdresseMandataire : '');
    }*/
formFieldsPers2['formalite_signataireAdresse_NSP'] = Value('id').of(signature.soussigne).eq('formaliteSignataireQualiteMandataire') ?
((signature.adresseMandataire.numeroVoieMandataire != null ? signature.adresseMandataire.numeroVoieMandataire : '')
+ ' ' + (signature.adresseMandataire.indiceVoieMandataire != null ? signature.adresseMandataire.indiceVoieMandataire : '')
+ ' ' + (signature.adresseMandataire.typeVoieMandataire != null ? signature.adresseMandataire.typeVoieMandataire : '')
+ ' ' + (signature.adresseMandataire.nomVoieMandataire != null ? signature.adresseMandataire.nomVoieMandataire : '')
+ ' ' + (signature.adresseMandataire.complementVoieMandataire != null ? signature.adresseMandataire.complementVoieMandataire : '')
+ ' ' + (signature.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signature.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
+ ' ' + (signature.adresseMandataire.codePostalMandataire != null ? signature.adresseMandataire.codePostalMandataire : '')
+ ' ' + (signature.adresseMandataire.villeAdresseMandataire != null ? signature.adresseMandataire.villeAdresseMandataire : '')) : '';
//
formFieldsPers2['formalite_signatureDate_NSP']                                           = signature.formaliteSignatureDate != null ? signature.formaliteSignatureDate : '';
formFieldsPers2['nombreP0prime_NSP']                                                     = "0";
formFieldsPers2['signature_NSP']                                                         = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce";

// NSP 3

formFieldsPers3['ayantDroit_lienParente_membreExploitation_NSP'] = true;
formFieldsPers3['ayantDroit_lienParente_aideFamilial_NSP'] = false;
formFieldsPers3['ayantDroit_lienParente_associeExploitation_NSP'] = false;
formFieldsPers3['ayantDroit_affiliationMSA_non_NSP']= false;
formFieldsPers3['ayantDroit_affiliationMSA_oui_NSP']  = false;
formFieldsPers3['formulaire_dependance_P0Agricole_NSP'] = false;
formFieldsPers3['formulaire_dependance_FAgricole_NSP']  = true;
formFieldsPers3['formulaire_dependance_M0Agricole_NSP']  = false;
formFieldsPers3['formulaire_dependance_M2Agricole_NSP']= false;
formFieldsPers3['formulaire_dependance_M3_NSP'] = false;

//Cadre 2 Rappel identification
formFieldsPers3['exploitationCommunNom_NSP'] = identification.entrepriseNom;

//Cadre 3 Personne non salariée participant aux travaux
var nirDeclarant1 = nsp3.voletSocialNumeroSecuriteSociale;
if(nirDeclarant1 != null) {
    nirDeclarant1 = nirDeclarant1.replace(/ /g, "");
    formFieldsPers3['ayantDroit_numeroSecuriteSociale_NSP'] = nirDeclarant1.substring(0, 13);
    formFieldsPers3['ayantDroit_numeroSecuriteSocialeCle_NSP']  = nirDeclarant1.substring(13, 15);
} 

formFieldsPers3['ayantDroit_identite_nomNaissance_NSP']    = membre3.membrePersonnePhysique.personneNomNaissance != null ? membre3.membrePersonnePhysique.personneNomNaissance : '';
formFieldsPers3['ayantDroit_identite_nomUsage_NSP'] = membre3.membrePersonnePhysique.personneNomUsage != null ? membre3.membrePersonnePhysique.personneNomUsage : '';
if (membre3.membrePersonnePhysique.personnePrenom != null and membre3.membrePersonnePhysique.personnePrenom[0].length > 0) {
    var prenoms = [];
    for (i = 0; i < membre3.membrePersonnePhysique.personnePrenom.size(); i++) {
        prenoms.push(membre3.membrePersonnePhysique.personnePrenom[i]);
    }
formFieldsPers3['ayantDroit_identite_prenom_NSP'] =  prenoms.toString();
}
formFieldsPers3['ayantDroit_connuMSA_oui_NSP']  = nsp3.voletSocialAffiliationMSAOui ? true : false;
formFieldsPers3['ayantDroit_connuMSA_non_NSP']  = nsp3.voletSocialAffiliationMSAOui ? false : true;

formFieldsPers3['ayantDroit_regimeAssuranceMaladie_regimeGeneral_NSP'] = Value('id').of(nsp3.voletSocialRegimeAssuranceMaladie).eq('voletSocialRegimeAssuranceMaladieStatutSalarie') ? true : false;
formFieldsPers3['ayantDroit_regimeAssuranceMaladie_agricole_NSP']= Value('id').of(nsp3.voletSocialRegimeAssuranceMaladie).eq('voletSocialRegimeAssuranceMaladieStatutAgricole') ? true : false;
formFieldsPers3['ayantDroit_regimeAssuranceMaladie_nonSalarieNonAgricole_NSP'] = Value('id').of(nsp3.voletSocialRegimeAssuranceMaladie).eq('voletSocialRegimeAssuranceMaladieStatutNonSalarieNonAgricole') ? true : false;
formFieldsPers3['ayantDroit_regimeAssuranceMaladie_autre_NSP'] = Value('id').of(nsp3.voletSocialRegimeAssuranceMaladie).eq('voletSocialRegimeAssuranceMaladieStatutAutre') ? true : false;
formFieldsPers3['ayantDroit_regimeAssuranceMaladieAutre_NSP'] = nsp3.voletSocialRegimeAssuranceMaladieStatutAutreTexte;

formFieldsPers3['ayantDroit_activiteAutreQueDeclareeStatut_oui'] = nsp3.voletSocialActiviteAutreQueDeclareeStatutOui ? true : false;
formFieldsPers3['ayantDroit_activiteAutreQueDeclareeStatut_non'] = nsp3.voletSocialActiviteAutreQueDeclareeStatutOui ? false : true;
formFieldsPers3['ayantDroit_regimeAssuranceMaladie_agricole_simultane_NSP']  = Value('id').of(nsp3.ayantDroitActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutAgricole') ? true : false;
formFieldsPers3['ayantDroit_activiteAutreQueDeclareeStatut_salarie_NSP']                 = Value('id').of(nsp3.ayantDroitActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutSalarie') ? true : false;
formFieldsPers3['ayantDroit_regimeAssuranceMaladie_nonSalarieNonAgricole_simultane_NSP'] = Value('id').of(nsp3.ayantDroitActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutNonSalarieNonAgricole') ? true : false;
formFieldsPers3['ayantDroit_activiteAutreQueDeclareeStatut_retraite_NSP'] = Value('id').of(nsp3.ayantDroitActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutRetraite') ? true : false;
formFieldsPers3['ayantDroit_activiteAutreQueDeclareeStatut_pensionneInvalidite_NSP']  = Value('id').of(nsp3.ayantDroitActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutPensionneInvalidite') ? true : false;
formFieldsPers3['ayantDroit_regimeAssuranceMaladie_autre_simultane_NSP']  = Value('id').of(nsp3.ayantDroitActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutAutre') ? true : false;
formFieldsPers3['ayantDroit_regimeAssuranceMaladieAutre_simultane_NSP']   = nsp3.voletSocialActiviteAutreQueDeclareeStatutAutreTexte;
formFieldsPers3['ayantDroit_organismeServantPension_NSP'] = nsp3.voletSocialOrganismeServentPension;

formFieldsPers3['ayantDroit_conjointCouvertAssuranceMaladie_oui_NSP']  = Value('id').of(nsp3.voletSocialConjointCouvertAssuranceMaladieOui).eq('oui') ? true : false;
formFieldsPers3['ayantDroit_conjointCouvertAssuranceMaladie_non_NSP']   = Value('id').of(nsp3.voletSocialConjointCouvertAssuranceMaladieOui).eq('non') ? true : false;

var nirDeclarant = nsp3.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers3['ayantDroit_conjointCouvertAssuranceMaladie_numeroSS_NSP']                = nirDeclarant.substring(0, 13);
    formFieldsPers3['ayantDroit_conjointCouvertAssuranceMaladie_numeroSSCle_NSP']             = nirDeclarant.substring(13, 15);
}

// NSP 3 Ayant droit 1 

formFieldsPers3['ayantDroit_identite_nomNaissance1_NSP']     = nsp3.ayantDroitsDeclaration ? (nsp3.ayantsdroits1.ayantDroitIdentiteNomNaissance + ' ' + nsp3.ayantsdroits1.ayantDroitIdentitePrenomNaissance) : '';
formFieldsPers3['ayantDroit_numeroSecuriteSociale1_NSP']   = nsp3.ayantDroitsDeclaration ? (nsp3.ayantsdroits1.ayantDroitNumeroSecuriteSociale != null ? nsp3.ayantsdroits1.ayantDroitNumeroSecuriteSociale : '') : '';
formFieldsPers3['ayantDroit_identiteComplement_dateNaissance1_NSP']   = nsp3.ayantDroitsDeclaration ? (nsp3.ayantsdroits1.ayantDroitDateNaissance != null ? nsp3.ayantsdroits1.ayantDroitDateNaissance : '') : '';
formFieldsPers3['ayantDroit_identiteComplement_lieuNaissanceCommune1_NSP']  = nsp3.ayantDroitsDeclaration ? (nsp3.ayantsdroits1.ayantDroitNumeroSecuriteSociale != null ? "" :
																						((nsp3.ayantsdroits1.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? 
																						nsp3.ayantsdroits1.ayantDroitIdentiteComplementLieuNaissanceCommune : nsp3.ayantsdroits1.ayantDroitIdentiteComplementLieuNaissanceVille)			
																						 + ' / ' + nsp3.ayantsdroits1.ayantDroitIdentiteComplementLieuNaissancePays 
																						 + ' ' + (Value('id').of(nsp3.ayantsdroits1.ayantDroitSexe).eq('feminin') ? "F" : "M"))) : '';
formFieldsPers3['ayantDroit_lienParente1_NSP']   = nsp3.ayantDroitsDeclaration ? (nsp3.ayantsdroits1.ayantDroitLienParente != null ? nsp3.ayantsdroits1.ayantDroitLienParente : '') : '';
formFieldsPers3['ayantDroit_enfantScolarise_oui1_NSP'] = nsp3.ayantDroitsDeclaration ? (Value('id').of(nsp3.ayantsdroits1.ayantDroitLienParente).eq('enfant') ? (nsp3.ayantsdroits1.dataEnfantScolariseBeneficiaireAM ? true : false) : false) : false;
formFieldsPers3['ayantDroit_enfantScolarise_non1_NSP']   = nsp3.ayantDroitsDeclaration ? (Value('id').of(nsp3.ayantsdroits1.ayantDroitLienParente).eq('enfant') ? (nsp3.ayantsdroits1.dataEnfantScolariseBeneficiaireAM ? false : true) : false) : false;
formFieldsPers3['ayantDroit_nationalite1_NSP']  = nsp3.ayantDroitsDeclaration ? (nsp3.ayantsdroits1.ayantDroitNationalite != null ? nsp3.ayantsdroits1.ayantDroitNationalite : '') : '';


// NSP 3 Ayant droit 2 

formFieldsPers3['ayantDroit_identite_nomNaissance2_NSP']                                 = nsp3.ayantsdroits1.autreBeneficiaireAM ? (nsp3.ayantsdroits2.ayantDroitIdentiteNomNaissance + ' ' + nsp3.ayantsdroits2.ayantDroitIdentitePrenomNaissance) : '';
formFieldsPers3['ayantDroit_numeroSecuriteSociale2_NSP']                                 = nsp3.ayantsdroits1.autreBeneficiaireAM ? (nsp3.ayantsdroits2.ayantDroitNumeroSecuriteSociale != null ? nsp3.ayantsdroits2.ayantDroitNumeroSecuriteSociale : '') : '';
formFieldsPers3['ayantDroit_identiteComplement_dateNaissance2_NSP']                      = nsp3.ayantsdroits1.autreBeneficiaireAM ? (nsp3.ayantsdroits2.ayantDroitDateNaissance != null ? nsp3.ayantsdroits2.ayantDroitDateNaissance : '') : '';
formFieldsPers3['ayantDroit_identiteComplement_lieuNaissanceCommune2_NSP']               = nsp3.ayantsdroits1.autreBeneficiaireAM ? (nsp3.ayantsdroits2.ayantDroitNumeroSecuriteSociale != null ? "" :
																						((nsp3.ayantsdroits2.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? 
																						nsp3.ayantsdroits2.ayantDroitIdentiteComplementLieuNaissanceCommune : nsp3.ayantsdroits2.ayantDroitIdentiteComplementLieuNaissanceVille)			
																						 + ' / ' + nsp3.ayantsdroits2.ayantDroitIdentiteComplementLieuNaissancePays 
																						 + ' ' + (Value('id').of(nsp3.ayantsdroits2.ayantDroitSexe).eq('feminin') ? "F" : "M"))) : '';
formFieldsPers3['ayantDroit_lienParente2_NSP']                                           = nsp3.ayantsdroits1.autreBeneficiaireAM ? (nsp3.ayantsdroits2.ayantDroitLienParente != null ? nsp3.ayantsdroits2.ayantDroitLienParente : '') : '';
formFieldsPers3['ayantDroit_enfantScolarise_oui2_NSP']                                   = nsp3.ayantsdroits1.autreBeneficiaireAM ? (Value('id').of(nsp3.ayantsdroits2.ayantDroitLienParente).eq('enfant') ? (nsp3.ayantsdroits2.dataEnfantScolariseBeneficiaireAM ? true : false) : false) : false;
formFieldsPers3['ayantDroit_enfantScolarise_non2_NSP']                                   = nsp3.ayantsdroits1.autreBeneficiaireAM ? (Value('id').of(nsp3.ayantsdroits2.ayantDroitLienParente).eq('enfant') ? (nsp3.ayantsdroits2.dataEnfantScolariseBeneficiaireAM ? false : true) : false) : false;
formFieldsPers3['ayantDroit_nationalite2_NSP']                                           = nsp3.ayantsdroits1.autreBeneficiaireAM ? (nsp3.ayantsdroits2.ayantDroitNationalite != null ? nsp3.ayantsdroits2.ayantDroitNationalite : '') : '';


// NSP 3 Ayant droit 3 

formFieldsPers3['ayantDroit_identite_nomNaissance3_NSP']                                 = nsp3.ayantsdroits2.autreBeneficiaireAM ? (nsp3.ayantsdroits3.ayantDroitIdentiteNomNaissance + ' ' + nsp3.ayantsdroits3.ayantDroitIdentitePrenomNaissance) : '';
formFieldsPers3['ayantDroit_numeroSecuriteSociale3_NSP']                                 = nsp3.ayantsdroits2.autreBeneficiaireAM ? (nsp3.ayantsdroits3.ayantDroitNumeroSecuriteSociale != null ? nsp3.ayantsdroits3.ayantDroitNumeroSecuriteSociale : '') : '';
formFieldsPers3['ayantDroit_identiteComplement_dateNaissance3_NSP']                      = nsp3.ayantsdroits2.autreBeneficiaireAM ? (nsp3.ayantsdroits3.ayantDroitDateNaissance != null ? nsp3.ayantsdroits3.ayantDroitDateNaissance : '') : '';
formFieldsPers3['ayantDroit_identiteComplement_lieuNaissanceCommune3_NSP']               = nsp3.ayantsdroits2.autreBeneficiaireAM ? (nsp3.ayantsdroits3.ayantDroitNumeroSecuriteSociale != null ? "" :
																						((nsp3.ayantsdroits3.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? 
																						nsp3.ayantsdroits3.ayantDroitIdentiteComplementLieuNaissanceCommune : nsp3.ayantsdroits3.ayantDroitIdentiteComplementLieuNaissanceVille)			
																						 + ' / ' + nsp3.ayantsdroits3.ayantDroitIdentiteComplementLieuNaissancePays 
																						 + ' ' + (Value('id').of(nsp3.ayantsdroits3.ayantDroitSexe).eq('feminin') ? "F" : "M"))) : '';
formFieldsPers3['ayantDroit_lienParente3_NSP']                                           = nsp3.ayantsdroits2.autreBeneficiaireAM ? (nsp3.ayantsdroits3.ayantDroitLienParente != null ? nsp3.ayantsdroits3.ayantDroitLienParente : '') : '';
formFieldsPers3['ayantDroit_enfantScolarise_oui3_NSP']                                   = nsp3.ayantsdroits2.autreBeneficiaireAM ? (Value('id').of(nsp3.ayantsdroits3.ayantDroitLienParente).eq('enfant') ? (nsp3.ayantsdroits3.dataEnfantScolariseBeneficiaireAM ? true : false) : false) : false;
formFieldsPers3['ayantDroit_enfantScolarise_non3_NSP']                                   = nsp3.ayantsdroits2.autreBeneficiaireAM ? (Value('id').of(nsp3.ayantsdroits3.ayantDroitLienParente).eq('enfant') ? (nsp3.ayantsdroits3.dataEnfantScolariseBeneficiaireAM ? false : true) : false) : false;
formFieldsPers3['ayantDroit_nationalite3_NSP']                                           = nsp3.ayantsdroits2.autreBeneficiaireAM ? (nsp3.ayantsdroits3.ayantDroitNationalite != null ? nsp3.ayantsdroits3.ayantDroitNationalite : '') : '';

//Pour les membres d'exploitation en commun
formFieldsPers3['voletSocial_qualiteJeuneAgriculteur_Oui'] = Value('id').of(nsp3.voletSocialEstJeuneAgriculteur).eq('VoletSocialEstJeuneAgriculteurOui') ? true : false;
formFieldsPers3['voletSocial_qualiteJeuneAgriculteur_Non'] = Value('id').of(nsp3.voletSocialEstJeuneAgriculteur).eq('VoletSocialEstJeuneAgriculteurNon') ? true : false;
formFieldsPers3['voletSocial_qualiteJeuneAgriculteur_Encours'] = Value('id').of(nsp3.voletSocialEstJeuneAgriculteur).eq('VoletSocialDemandeDotationJeuneAgriculteur') ? true : false;
formFieldsPers3['voletSocial_exploitationCommun_statutConjoint_collaborateur']  = Value('id').of(nsp3.statutConjointCollaborateur).eq('VoletSocialStatutConjointCollaborateur') ? true : false ;
formFieldsPers3['voletSocial_exploitationCommun_statutConjoint_salarie']   = Value('id').of(nsp3.statutConjointCollaborateur).eq('VoletSocialStatutConjointSalarie') ? true : false ;
formFieldsPers3['voletSocial_exploitationCommun_statutConjoint_coexploitant']  =  Value('id').of(nsp3.statutConjointCollaborateur).eq('VoletSocialStatutConjointCoExploitant') ? true : false;
formFieldsPers3['voletSocial_estBeneficiaireRASRMI_oui_NSP']   = nsp3.beneficiairePrimeActivite  ? true : false ;
formFieldsPers3['voletSocial_estBeneficiaireRASRMI_non_NSP']   = nsp3.beneficiairePrimeActivite  ? false : true ;
 
//Signature
formFieldsPers3['formalite_signataireQualite_declarantNSP']  = Value('id').of(signature.soussigne).eq('formaliteSignataireQualiteCoExploitant') != false ? true : false;
formFieldsPers3['formalite_signataireQualite_mandataireNSP'] = Value('id').of(signature.soussigne).eq('formaliteSignataireQualiteMandataire') != false ? true : false;
formFieldsPers3['formalite_signataireNom_NSP']                                           = Value('id').of(signature.soussigne). eq('formaliteSignataireQualiteMandataire') ? signature.adresseMandataire.nomPrenomDenominationMandataire : '';
formFieldsPers3['formalite_signatureLieu_NSP']                                           = signature.formaliteSignatureLieu != null ? signature.formaliteSignatureLieu : '';
/*if (signature.adresseMandataire.nomPrenomDenominationMandataire != false){
    formFieldsPers3['formalite_signataireAdresse_NSP'] = 
    (signature.adresseMandataire.numeroVoieMandataire != false ? signature.adresseMandataire.numeroVoieMandataire : '')
    + ' ' + (signature.adresseMandataire.indiceVoieMandataire != false ? signature.adresseMandataire.indiceVoieMandataire : '')
    + ' ' + (signature.adresseMandataire.typeVoieMandataire != false ? signature.adresseMandataire.typeVoieMandataire : '')
    + ' ' + (signature.adresseMandataire.nomVoieMandataire != false ? signature.adresseMandataire.nomVoieMandataire : '')
    + ' ' + (signature.adresseMandataire.complementVoieMandataire != false ? signature.adresseMandataire.complementVoieMandataire : '')
    + ' ' + (signature.adresseMandataire.voieDistributionSpecialeAdresseMandataire != false ? signature.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
    + ' ' + (signature.adresseMandataire.codePostalMandataire != false ? signature.adresseMandataire.codePostalMandataire : '')
    + ' ' + (signature.adresseMandataire.villeAdresseMandataire != false ? signature.adresseMandataire.villeAdresseMandataire : '');
    }*/
formFieldsPers3['formalite_signataireAdresse_NSP'] = Value('id').of(signature.soussigne).eq('formaliteSignataireQualiteMandataire') ?
((signature.adresseMandataire.numeroVoieMandataire != null ? signature.adresseMandataire.numeroVoieMandataire : '')
+ ' ' + (signature.adresseMandataire.indiceVoieMandataire != null ? signature.adresseMandataire.indiceVoieMandataire : '')
+ ' ' + (signature.adresseMandataire.typeVoieMandataire != null ? signature.adresseMandataire.typeVoieMandataire : '')
+ ' ' + (signature.adresseMandataire.nomVoieMandataire != null ? signature.adresseMandataire.nomVoieMandataire : '')
+ ' ' + (signature.adresseMandataire.complementVoieMandataire != null ? signature.adresseMandataire.complementVoieMandataire : '')
+ ' ' + (signature.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signature.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
+ ' ' + (signature.adresseMandataire.codePostalMandataire != null ? signature.adresseMandataire.codePostalMandataire : '')
+ ' ' + (signature.adresseMandataire.villeAdresseMandataire != null ? signature.adresseMandataire.villeAdresseMandataire : '')) : '';
formFieldsPers3['formalite_signatureDate_NSP']                                           = signature.formaliteSignatureDate != null ? signature.formaliteSignatureDate : '';
formFieldsPers3['nombreP0prime_NSP']                                                     = "0";
formFieldsPers3['signature_NSP']                                                         = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce";

// NSP 4

formFieldsPers4['ayantDroit_lienParente_membreExploitation_NSP'] = true;
formFieldsPers4['ayantDroit_lienParente_aideFamilial_NSP'] = false;
formFieldsPers4['ayantDroit_lienParente_associeExploitation_NSP'] = false;
formFieldsPers4['ayantDroit_affiliationMSA_non_NSP']= false;
formFieldsPers4['ayantDroit_affiliationMSA_oui_NSP']  = false;
formFieldsPers4['formulaire_dependance_P0Agricole_NSP'] = false;
formFieldsPers4['formulaire_dependance_FAgricole_NSP']  = true;
formFieldsPers4['formulaire_dependance_M0Agricole_NSP']  = false;
formFieldsPers4['formulaire_dependance_M2Agricole_NSP']= false;
formFieldsPers4['formulaire_dependance_M3_NSP'] = false;

//Cadre 2 Rappel identification
formFieldsPers4['exploitationCommunNom_NSP'] = identification.entrepriseNom;

//Cadre 3 Personne non salariée participant aux travaux
var nirDeclarant1 = nsp4.voletSocialNumeroSecuriteSociale;
if(nirDeclarant1 != null) {
    nirDeclarant1 = nirDeclarant1.replace(/ /g, "");
    formFieldsPers4['ayantDroit_numeroSecuriteSociale_NSP'] = nirDeclarant1.substring(0, 13);
    formFieldsPers4['ayantDroit_numeroSecuriteSocialeCle_NSP']  = nirDeclarant1.substring(13, 15);
} 

formFieldsPers4['ayantDroit_identite_nomNaissance_NSP']    = membre4.membrePersonnePhysique.personneNomNaissance != null ? membre4.membrePersonnePhysique.personneNomNaissance : '';
formFieldsPers4['ayantDroit_identite_nomUsage_NSP'] = membre4.membrePersonnePhysique.personneNomUsage != null ? membre4.membrePersonnePhysique.personneNomUsage : '';
if (membre4.membrePersonnePhysique.personnePrenom != null and membre4.membrePersonnePhysique.personnePrenom[0].length > 0) {
    var prenoms = [];
    for (i = 0; i < membre4.membrePersonnePhysique.personnePrenom.size(); i++) {
        prenoms.push(membre4.membrePersonnePhysique.personnePrenom[i]);
    }
formFieldsPers4['ayantDroit_identite_prenom_NSP'] =  prenoms.toString();
}
formFieldsPers4['ayantDroit_connuMSA_oui_NSP']  = nsp4.voletSocialAffiliationMSAOui ? true : false;
formFieldsPers4['ayantDroit_connuMSA_non_NSP']  = nsp4.voletSocialAffiliationMSAOui ? false : true;

formFieldsPers4['ayantDroit_regimeAssuranceMaladie_regimeGeneral_NSP'] = Value('id').of(nsp4.voletSocialRegimeAssuranceMaladie).eq('voletSocialRegimeAssuranceMaladieStatutSalarie') ? true : false;
formFieldsPers4['ayantDroit_regimeAssuranceMaladie_agricole_NSP']= Value('id').of(nsp4.voletSocialRegimeAssuranceMaladie).eq('voletSocialRegimeAssuranceMaladieStatutAgricole') ? true : false;
formFieldsPers4['ayantDroit_regimeAssuranceMaladie_nonSalarieNonAgricole_NSP'] = Value('id').of(nsp4.voletSocialRegimeAssuranceMaladie).eq('voletSocialRegimeAssuranceMaladieStatutNonSalarieNonAgricole') ? true : false;
formFieldsPers4['ayantDroit_regimeAssuranceMaladie_autre_NSP'] = Value('id').of(nsp4.voletSocialRegimeAssuranceMaladie).eq('voletSocialRegimeAssuranceMaladieStatutAutre') ? true : false;
formFieldsPers4['ayantDroit_regimeAssuranceMaladieAutre_NSP'] = nsp4.voletSocialRegimeAssuranceMaladieStatutAutreTexte;

formFieldsPers4['ayantDroit_activiteAutreQueDeclareeStatut_oui'] = nsp4.voletSocialActiviteAutreQueDeclareeStatutOui ? true : false;
formFieldsPers4['ayantDroit_activiteAutreQueDeclareeStatut_non'] = nsp4.voletSocialActiviteAutreQueDeclareeStatutOui ? false : true;
formFieldsPers4['ayantDroit_regimeAssuranceMaladie_agricole_simultane_NSP']  = Value('id').of(nsp4.ayantDroitActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutAgricole') ? true : false;
formFieldsPers4['ayantDroit_activiteAutreQueDeclareeStatut_salarie_NSP']                 = Value('id').of(nsp4.ayantDroitActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutSalarie') ? true : false;
formFieldsPers4['ayantDroit_regimeAssuranceMaladie_nonSalarieNonAgricole_simultane_NSP'] = Value('id').of(nsp4.ayantDroitActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutNonSalarieNonAgricole') ? true : false;
formFieldsPers4['ayantDroit_activiteAutreQueDeclareeStatut_retraite_NSP'] = Value('id').of(nsp4.ayantDroitActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutRetraite') ? true : false;
formFieldsPers4['ayantDroit_activiteAutreQueDeclareeStatut_pensionneInvalidite_NSP']  = Value('id').of(nsp4.ayantDroitActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutPensionneInvalidite') ? true : false;
formFieldsPers4['ayantDroit_regimeAssuranceMaladie_autre_simultane_NSP']  = Value('id').of(nsp4.ayantDroitActiviteAutreQueDeclaree).eq('voletSocialActiviteAutreQueDeclareeStatutAutre') ? true : false;
formFieldsPers4['ayantDroit_regimeAssuranceMaladieAutre_simultane_NSP']   = nsp4.voletSocialActiviteAutreQueDeclareeStatutAutreTexte;
formFieldsPers4['ayantDroit_organismeServantPension_NSP'] = nsp4.voletSocialOrganismeServentPension;

formFieldsPers4['ayantDroit_conjointCouvertAssuranceMaladie_oui_NSP']  = Value('id').of(nsp4.voletSocialConjointCouvertAssuranceMaladieOui).eq('oui') ? true : false;
formFieldsPers4['ayantDroit_conjointCouvertAssuranceMaladie_non_NSP']   = Value('id').of(nsp4.voletSocialConjointCouvertAssuranceMaladieOui).eq('non') ? true : false;

var nirDeclarant = nsp4.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers4['ayantDroit_conjointCouvertAssuranceMaladie_numeroSS_NSP']                = nirDeclarant.substring(0, 13);
    formFieldsPers4['ayantDroit_conjointCouvertAssuranceMaladie_numeroSSCle_NSP']             = nirDeclarant.substring(13, 15);
}

// NSP 4 Ayant droit 1 

formFieldsPers4['ayantDroit_identite_nomNaissance1_NSP']     = nsp4.ayantDroitsDeclaration ? (nsp4.ayantsdroits1.ayantDroitIdentiteNomNaissance + ' ' + nsp4.ayantsdroits1.ayantDroitIdentitePrenomNaissance) : '';
formFieldsPers4['ayantDroit_numeroSecuriteSociale1_NSP']   = nsp4.ayantDroitsDeclaration ? (nsp4.ayantsdroits1.ayantDroitNumeroSecuriteSociale != null ? nsp4.ayantsdroits1.ayantDroitNumeroSecuriteSociale : '') : '';
formFieldsPers4['ayantDroit_identiteComplement_dateNaissance1_NSP']   = nsp4.ayantDroitsDeclaration ? (nsp4.ayantsdroits1.ayantDroitDateNaissance != null ? nsp4.ayantsdroits1.ayantDroitDateNaissance : '') : '';
formFieldsPers4['ayantDroit_identiteComplement_lieuNaissanceCommune1_NSP']  = nsp4.ayantDroitsDeclaration ? (nsp4.ayantsdroits1.ayantDroitNumeroSecuriteSociale != null ? "" :
																						((nsp4.ayantsdroits1.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? 
																						nsp4.ayantsdroits1.ayantDroitIdentiteComplementLieuNaissanceCommune : nsp4.ayantsdroits1.ayantDroitIdentiteComplementLieuNaissanceVille)			
																						 + ' / ' + nsp4.ayantsdroits1.ayantDroitIdentiteComplementLieuNaissancePays 
																						 + ' ' + (Value('id').of(nsp4.ayantsdroits1.ayantDroitSexe).eq('feminin') ? "F" : "M"))) : '';
formFieldsPers4['ayantDroit_lienParente1_NSP']   = nsp4.ayantDroitsDeclaration ? (nsp4.ayantsdroits1.ayantDroitLienParente != null ? nsp4.ayantsdroits1.ayantDroitLienParente : '') : '';
formFieldsPers4['ayantDroit_enfantScolarise_oui1_NSP'] = nsp4.ayantDroitsDeclaration ? (Value('id').of(nsp4.ayantsdroits1.ayantDroitLienParente).eq('enfant') ? (nsp4.ayantsdroits1.dataEnfantScolariseBeneficiaireAM ? true : false) : false) : false;
formFieldsPers4['ayantDroit_enfantScolarise_non1_NSP']   = nsp4.ayantDroitsDeclaration ? (Value('id').of(nsp4.ayantsdroits1.ayantDroitLienParente).eq('enfant') ? (nsp4.ayantsdroits1.dataEnfantScolariseBeneficiaireAM ? false : true) : false) : false;
formFieldsPers4['ayantDroit_nationalite1_NSP']  = nsp4.ayantDroitsDeclaration ? (nsp4.ayantsdroits1.ayantDroitNationalite != null ? nsp4.ayantsdroits1.ayantDroitNationalite : '') : '';


// NSP 4 Ayant droit 2 

formFieldsPers4['ayantDroit_identite_nomNaissance2_NSP']                                 = nsp4.ayantsdroits1.autreBeneficiaireAM ? (nsp4.ayantsdroits2.ayantDroitIdentiteNomNaissance + ' ' + nsp4.ayantsdroits2.ayantDroitIdentitePrenomNaissance) : '';
formFieldsPers4['ayantDroit_numeroSecuriteSociale2_NSP']                                 = nsp4.ayantsdroits1.autreBeneficiaireAM ? (nsp4.ayantsdroits2.ayantDroitNumeroSecuriteSociale != null ? nsp4.ayantsdroits2.ayantDroitNumeroSecuriteSociale : '') : '';
formFieldsPers4['ayantDroit_identiteComplement_dateNaissance2_NSP']                      = nsp4.ayantsdroits1.autreBeneficiaireAM ? (nsp4.ayantsdroits2.ayantDroitDateNaissance != null ? nsp4.ayantsdroits2.ayantDroitDateNaissance : '') : '';
formFieldsPers4['ayantDroit_identiteComplement_lieuNaissanceCommune2_NSP']               = nsp4.ayantsdroits1.autreBeneficiaireAM ? (nsp4.ayantsdroits2.ayantDroitNumeroSecuriteSociale != null ? "" :
																						((nsp4.ayantsdroits2.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? 
																						nsp4.ayantsdroits2.ayantDroitIdentiteComplementLieuNaissanceCommune : nsp4.ayantsdroits2.ayantDroitIdentiteComplementLieuNaissanceVille)			
																						 + ' / ' + nsp4.ayantsdroits2.ayantDroitIdentiteComplementLieuNaissancePays 
																						 + ' ' + (Value('id').of(nsp4.ayantsdroits2.ayantDroitSexe).eq('feminin') ? "F" : "M"))) : '';
formFieldsPers4['ayantDroit_lienParente2_NSP']                                           = nsp4.ayantsdroits1.autreBeneficiaireAM ? (nsp4.ayantsdroits2.ayantDroitLienParente != null ? nsp4.ayantsdroits2.ayantDroitLienParente : '') : '';
formFieldsPers4['ayantDroit_enfantScolarise_oui2_NSP']                                   = nsp4.ayantsdroits1.autreBeneficiaireAM ? (Value('id').of(nsp4.ayantsdroits2.ayantDroitLienParente).eq('enfant') ? (nsp4.ayantsdroits2.dataEnfantScolariseBeneficiaireAM ? true : false) : false) : false;
formFieldsPers4['ayantDroit_enfantScolarise_non2_NSP']                                   = nsp4.ayantsdroits1.autreBeneficiaireAM ? (Value('id').of(nsp4.ayantsdroits2.ayantDroitLienParente).eq('enfant') ? (nsp4.ayantsdroits2.dataEnfantScolariseBeneficiaireAM ? false : true) : false) : false;
formFieldsPers4['ayantDroit_nationalite2_NSP']                                           = nsp4.ayantsdroits1.autreBeneficiaireAM ? (nsp4.ayantsdroits2.ayantDroitNationalite != null ? nsp4.ayantsdroits2.ayantDroitNationalite : '') : '';


// NSP 4 Ayant droit 3 

formFieldsPers4['ayantDroit_identite_nomNaissance3_NSP']                                 = nsp4.ayantsdroits2.autreBeneficiaireAM ? (nsp4.ayantsdroits3.ayantDroitIdentiteNomNaissance + ' ' + nsp4.ayantsdroits3.ayantDroitIdentitePrenomNaissance) : '';
formFieldsPers4['ayantDroit_numeroSecuriteSociale3_NSP']                                 = nsp4.ayantsdroits2.autreBeneficiaireAM ? (nsp4.ayantsdroits3.ayantDroitNumeroSecuriteSociale != null ? nsp4.ayantsdroits3.ayantDroitNumeroSecuriteSociale : '') : '';
formFieldsPers4['ayantDroit_identiteComplement_dateNaissance3_NSP']                      = nsp4.ayantsdroits2.autreBeneficiaireAM ? (nsp4.ayantsdroits3.ayantDroitDateNaissance != null ? nsp4.ayantsdroits3.ayantDroitDateNaissance : '') : '';
formFieldsPers4['ayantDroit_identiteComplement_lieuNaissanceCommune3_NSP']               = nsp4.ayantsdroits2.autreBeneficiaireAM ? (nsp4.ayantsdroits3.ayantDroitNumeroSecuriteSociale != null ? "" :
																						((nsp4.ayantsdroits3.ayantDroitIdentiteComplementLieuNaissanceCommune != null ? 
																						nsp4.ayantsdroits3.ayantDroitIdentiteComplementLieuNaissanceCommune : nsp4.ayantsdroits3.ayantDroitIdentiteComplementLieuNaissanceVille)			
																						 + ' / ' + nsp4.ayantsdroits3.ayantDroitIdentiteComplementLieuNaissancePays 
																						 + ' ' + (Value('id').of(nsp4.ayantsdroits3.ayantDroitSexe).eq('feminin') ? "F" : "M"))) : '';
formFieldsPers4['ayantDroit_lienParente3_NSP']                                           = nsp4.ayantsdroits2.autreBeneficiaireAM ? (nsp4.ayantsdroits3.ayantDroitLienParente != null ? nsp4.ayantsdroits3.ayantDroitLienParente : '') : '';
formFieldsPers4['ayantDroit_enfantScolarise_oui3_NSP']                                   = nsp4.ayantsdroits2.autreBeneficiaireAM ? (Value('id').of(nsp4.ayantsdroits3.ayantDroitLienParente).eq('enfant') ? (nsp4.ayantsdroits3.dataEnfantScolariseBeneficiaireAM ? true : false) : false) : false;
formFieldsPers4['ayantDroit_enfantScolarise_non3_NSP']                                   = nsp4.ayantsdroits2.autreBeneficiaireAM ? (Value('id').of(nsp4.ayantsdroits3.ayantDroitLienParente).eq('enfant') ? (nsp4.ayantsdroits3.dataEnfantScolariseBeneficiaireAM ? false : true) : false) : false;
formFieldsPers4['ayantDroit_nationalite3_NSP']                                           = nsp4.ayantsdroits2.autreBeneficiaireAM ? (nsp4.ayantsdroits3.ayantDroitNationalite != null ? nsp4.ayantsdroits3.ayantDroitNationalite : '') : '';

//Pour les membres d'exploitation en commun
formFieldsPers4['voletSocial_qualiteJeuneAgriculteur_Oui'] = Value('id').of(nsp4.voletSocialEstJeuneAgriculteur).eq('VoletSocialEstJeuneAgriculteurOui') ? true : false;
formFieldsPers4['voletSocial_qualiteJeuneAgriculteur_Non'] = Value('id').of(nsp4.voletSocialEstJeuneAgriculteur).eq('VoletSocialEstJeuneAgriculteurNon') ? true : false;
formFieldsPers4['voletSocial_qualiteJeuneAgriculteur_Encours'] = Value('id').of(nsp4.voletSocialEstJeuneAgriculteur).eq('VoletSocialDemandeDotationJeuneAgriculteur') ? true : false;
formFieldsPers4['voletSocial_exploitationCommun_statutConjoint_collaborateur']  = Value('id').of(nsp4.statutConjointCollaborateur).eq('VoletSocialStatutConjointCollaborateur') ? true : false ;
formFieldsPers4['voletSocial_exploitationCommun_statutConjoint_salarie']   = Value('id').of(nsp4.statutConjointCollaborateur).eq('VoletSocialStatutConjointSalarie') ? true : false ;
formFieldsPers4['voletSocial_exploitationCommun_statutConjoint_coexploitant']  =  Value('id').of(nsp4.statutConjointCollaborateur).eq('VoletSocialStatutConjointCoExploitant') ? true : false;
formFieldsPers4['voletSocial_estBeneficiaireRASRMI_oui_NSP']   = nsp4.beneficiairePrimeActivite  ? true : false ;
formFieldsPers4['voletSocial_estBeneficiaireRASRMI_non_NSP']   = nsp4.beneficiairePrimeActivite  ? false : true ;
 
//Signature
formFieldsPers4['formalite_signataireQualite_declarantNSP']                              = Value('id').of(signature.soussigne). eq('formaliteSignataireQualiteCoExploitant') != false ? true : false;
formFieldsPers4['formalite_signataireQualite_mandataireNSP']                             = Value('id').of(signature.soussigne). eq('formaliteSignataireQualiteMandataire') != false ? true : false;
formFieldsPers4['formalite_signataireNom_NSP']                                           = Value('id').of(signature.soussigne). eq('formaliteSignataireQualiteMandataire') ? signature.adresseMandataire.nomPrenomDenominationMandataire : '';
formFieldsPers4['formalite_signatureLieu_NSP']                                           = signature.formaliteSignatureLieu != null ? signature.formaliteSignatureLieu : '';
formFieldsPers4['formalite_signataireAdresse_NSP']  = Value('id').of(signature.soussigne).eq('formaliteSignataireQualiteMandataire') ?
((signature.adresseMandataire.numeroVoieMandataire != null ? signature.adresseMandataire.numeroVoieMandataire : '')
+ ' ' + (signature.adresseMandataire.indiceVoieMandataire != null ? signature.adresseMandataire.indiceVoieMandataire : '')
+ ' ' + (signature.adresseMandataire.typeVoieMandataire != null ? signature.adresseMandataire.typeVoieMandataire : '')
+ ' ' + (signature.adresseMandataire.nomVoieMandataire != null ? signature.adresseMandataire.nomVoieMandataire : '')
+ ' ' + (signature.adresseMandataire.complementVoieMandataire != null ? signature.adresseMandataire.complementVoieMandataire : '')
+ ' ' + (signature.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signature.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
+ ' ' + (signature.adresseMandataire.codePostalMandataire != null ? signature.adresseMandataire.codePostalMandataire : '')
+ ' ' + (signature.adresseMandataire.villeAdresseMandataire != null ? signature.adresseMandataire.villeAdresseMandataire : '')) : '';
formFieldsPers4['formalite_signatureDate_NSP']                                           = signature.formaliteSignatureDate != null ? signature.formaliteSignatureDate : '';
formFieldsPers4['nombreP0prime_NSP']                                                     = "0";
formFieldsPers4['signature_NSP']                                                         = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce";


var cerfaDoc1 = nash.doc //
    .load('models/cerfa_11923-05_F_agricole.pdf') //
    .apply(pdfFields);

var cerfaDoc2 = nash.doc //
    .load('models/cerfa_11923-05_F_agricole_sans_option_fiscale.pdf') //
    .apply(pdfFields);
    cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));

// si membre3 physique ou membre2 morale    
if ((membre3.membrePersonnePhysique.personneNomNaissance != null) 
    or ((membre2.membrePersonneMorale.personneLieePMDenomination != null) and (membre1.membrePersonneMorale.personneLieePMDenomination != null))
    or ((membre1.membrePersonneMorale.personneLieePMDenomination != null) and (membre2.membrePersonnePhysique.personneNomNaissance  != null) and (membre3.membrePersonnePhysique.personneNomNaissance  != null))
    or (identification.etabSupp) 
    or (etablissement.adresseAutreEtablissement))
    {
var cerfaDoc3 = nash.doc 
    .load('models/cerfa_11923-05_F_agricole.pdf') //
    .apply(pdfFields2);
    cerfaDoc1.append(cerfaDoc3.save('cerfa.pdf'));
}
//si membre3 morale 
if (membre3.membrePersonneMorale.personneLieePMDenomination != null
    and membre2.membrePersonneMorale.personneLieePMDenomination != null 
    and membre1.membrePersonneMorale.personneLieePMDenomination != null) {
var cerfaDoc3 = nash.doc //
    .load('models/cerfa_11923-05_F_agricole.pdf') //
    .apply(pdfFields3);
    cerfaDoc1.append(cerfaDoc3.save('cerfa.pdf'));
 }
 //si membre4 morale 
if (membre4.membrePersonneMorale.personneLieePMDenomination != null) {
var cerfaDoc3 = nash.doc //
    .load('models/cerfa_11923-05_F_agricole.pdf') //
    .apply(pdfFields4);
    cerfaDoc1.append(cerfaDoc3.save('cerfa.pdf'));
 }
 // Gestion des Nsp
if (membre1.membrePersonnePhysique.personneNomNaissance != null) {
    var cerfaDoc5 = nash.doc //
        .load('models/cerfa_11926-04_NSP.pdf') //
        .apply(formFieldsPers1);
    cerfaDoc1.append(cerfaDoc5.save('cerfa.pdf'));
}
if (membre2.membrePersonnePhysique.personneNomNaissance != null) {
    var cerfaDoc5 = nash.doc //
        .load('models/cerfa_11926-04_NSP.pdf') //
        .apply(formFieldsPers2);
    cerfaDoc1.append(cerfaDoc5.save('cerfa.pdf'));
}
if (membre3.membrePersonnePhysique.personneNomNaissance != null) {
    var cerfaDoc5 = nash.doc //
        .load('models/cerfa_11926-04_NSP.pdf') //
        .apply(formFieldsPers3);
    cerfaDoc1.append(cerfaDoc5.save('cerfa.pdf'));
}
if (membre4.membrePersonnePhysique.personneNomNaissance != null) {
    var cerfaDoc5 = nash.doc //
        .load('models/cerfa_11926-04_NSP.pdf') //
        .apply(formFieldsPers4);
    cerfaDoc1.append(cerfaDoc5.save('cerfa.pdf'));
}

/*
 * Ajout des PJs
 */

var pjUser = [];

function pushPjPreview(fld) {
	fld.forEach(function (elm) {
        pjUser.push(elm);
    });
}

if  (Value('id').of($fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre1.personnaliteJuridique).eq('personnePhysique')
and Value('id').of(signature.soussigne).eq('formaliteSignataireQualiteCoExploitant')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant1Signataire);        
}
if  (Value('id').of($fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre2.personnaliteJuridique).eq('personnePhysique')
and Value('id').of(signature.soussigne).eq('formaliteSignataireQualiteCoExploitant')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant2Signataire);        
}
if  (Value('id').of($fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre3.personnaliteJuridique).eq('personnePhysique')
and Value('id').of(signature.soussigne).eq('formaliteSignataireQualiteCoExploitant')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant3Signataire);        
}
if  (Value('id').of($fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre4.personnaliteJuridique).eq('personnePhysique')
and Value('id').of(signature.soussigne).eq('formaliteSignataireQualiteCoExploitant')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant4Signataire);        
}
if  (Value('id').of($fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre1.personnaliteJuridique).eq('personneMorale')
and Value('id').of(signature.soussigne).eq('formaliteSignataireQualiteCoExploitant')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjRepresentantSignataire1);
    }
if  (Value('id').of($fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre2.personnaliteJuridique).eq('personneMorale')
and Value('id').of(signature.soussigne).eq('formaliteSignataireQualiteCoExploitant')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjRepresentantSignataire2);
    }
if  (Value('id').of($fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre3.personnaliteJuridique).eq('personneMorale')
    and Value('id').of(signature.soussigne).eq('formaliteSignataireQualiteCoExploitant')) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjRepresentantSignataire3);
    } 
if  (Value('id').of($fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre4.personnaliteJuridique).eq('personneMorale')
    and Value('id').of(signature.soussigne).eq('formaliteSignataireQualiteCoExploitant')) {
        pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjRepresentantSignataire4);
    }   
if (Value('id').of(signature.soussigne).eq('formaliteSignataireQualiteMandataire')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPouvoir);
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire);
    }

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDoc = cerfaDoc1.save('F_Agricole_Creation.pdf');

var data = [ spec.createData({
        id: 'record',
        label: 'Déclaration de création d\'une exploitation en commun d\'une entreprise agricole ou d\'une activité de bailleur de biens ruraux',
        description: 'Voici le formulaire obtenu à partir des données saisies.',
        type: 'FileReadOnly',
        value: [finalDoc] 
    }), spec.createData({
        id: 'attachments',
        label: 'Pièces jointes',
        description: 'Pièces jointes ajoutées au formulaire.',
        type: 'FileReadOnly',
        value: pjUser
    })]; 
var groups = [spec.createGroup({
        id: 'generated',
        label: 'Génération du dossier',
        data: data
    })];

return spec.create({
    id: 'review',
    label: 'Déclaration relative à la création d\'une exploitation en commun d\'une entreprise agricole ou d\'une activité de bailleur de biens ruraux',
    groups: groups
});