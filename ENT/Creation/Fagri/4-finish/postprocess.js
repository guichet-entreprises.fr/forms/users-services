pushingbox();

//prepare info to send 

var identite = $fAgri.cadreDeclarationECGroup.cadreDeclarationEC;

var algo = "trouver Cfe";
var secteur1 = "Agricole";
var typePersonne = "PP";
var formJuridique = "EI";
var optionCMACCI = "NON";
var codeCommune = identite.siegeAdresseCommune.getId();
var denomination = identite.entrepriseNom;	
var data = nash.instance.load("output.xml")
	
/** Other files */

function findUserAttachments() {
	var values = [];
	var metas = nash.record.meta();

	log.debug('meta list => {}', null == metas ? null : metas.metas);

	if (null != metas && null != metas.metas) {
		var valueMap = {};
		metas.metas.forEach(function (meta) {
			log.debug('  ---> meta {} : {}', meta.name, meta.value);
			if ('userAttachments' == meta.name) {
				valueMap[meta.value] = true;
			}
		})
		Object.keys(valueMap).forEach(function (elm) {
			values.push({
				id: elm.replaceAll('^(.*/)([^/]+)$', '$2'), //
				label: elm //
			});
		});
	}

	return values;
}

data.bind("parameters",{
	"algo" : {
		"algo" : algo,
		"formJuridique" : formJuridique,
		"optionCMACCI" : "NON",
		"codeCommune" : codeCommune,
		"typePersonne" : typePersonne,
		"secteur1" : secteur1
	},
	"attachment" : {
		"cerfa" : '/3-review/generated/attachments.cerfa-1-F_Agricole_Creation.pdf',
		"others" : findUserAttachments()
	}
});

data.bind("result",{
	"funcId" : ""
})
data.bind("result", {
	"denomination" : denomination
});