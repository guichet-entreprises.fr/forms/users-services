
var pj = $fAgri.cadre21SignatureGroup.cadre21Signature;
var userMandataire = $fAgri.cadre21SignatureGroup.cadre21Signature.adresseMandataire;

var dirigeant1 = $fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre1.membrePersonnePhysique;
var dirigeant2 = $fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre2.membrePersonnePhysique;
var dirigeant3 = $fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre3.membrePersonnePhysique;
var dirigeant4 = $fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre4.membrePersonnePhysique;
var userDeclarant1;
var userDeclarant2;
var userDeclarant3;
var userDeclarant4;
var representant1 = $fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre1.membrePersonneMorale;
var representant2 = $fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre2.membrePersonneMorale;
var representant3 = $fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre3.membrePersonneMorale;
var representant4 = $fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre4.membrePersonneMorale;
var userSociete1;
var userSociete2;
var userSociete3;
var userSociete4;

// PJ DIRIGEANT PERSONNE PHYSIQUE
//DIR1
if  (Value('id').of($fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre1.personnaliteJuridique).eq('personnePhysique')
 and  Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteCoExploitant')) {
    if (dirigeant1.personneNomUsage != null) { var userDeclarant1 = dirigeant1.personneNomUsage + '  ' + dirigeant1.personnePrenom[0];
    } else { var userDeclarant1 = dirigeant1.personneNomNaissance + '  ' + dirigeant1.personnePrenom[0];
    }
    attachment('pjIDDirigeant1Signataire', 'pjIDDirigeant1Signataire', {
    label: userDeclarant1,
    mandatory: "true"
    });          
    }
 
//DIR2
if  (Value('id').of($fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre2.personnaliteJuridique).eq('personnePhysique')
and Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteCoExploitant')) {
    if (dirigeant2.personneNomUsage != null) { var userDeclarant2 = dirigeant2.personneNomUsage + '  ' + dirigeant2.personnePrenom[0];
    } else { var userDeclarant2 = dirigeant2.personneNomNaissance + '  ' + dirigeant2.personnePrenom[0];
    }
    attachment('pjIDDirigeant2Signataire', 'pjIDDirigeant2Signataire', {
    label: userDeclarant2,
    mandatory: "true"
     });
    }

 //DIR3
if  (Value('id').of($fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre3.personnaliteJuridique).eq('personnePhysique')
and Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteCoExploitant')) {
    if (dirigeant3.personneNomUsage != null) {var userDeclarant3 = dirigeant3.personneNomUsage + '  ' + dirigeant3.personnePrenom[0];
    } else {var userDeclarant3 = dirigeant1.personneNomNaissance + '  ' + dirigeant3.personnePrenom[0];
    }
    attachment('pjIDDirigeant3Signataire', 'pjIDDirigeant3Signataire', {
    label: userDeclarant3,
     mandatory: "true"
    });
    }

 //DIR4
 if  (Value('id').of($fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre4.personnaliteJuridique).eq('personnePhysique')
 and Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteCoExploitant')) {
    if (dirigeant4.personneNomUsage != null) {var userDeclarant4 = dirigeant4.personneNomUsage + '  ' + dirigeant4.personnePrenom[0];
    } else  {var userDeclarant4 = dirigeant4.personneNomNaissance + '  ' + dirigeant4.personnePrenom[0];
    }
    attachment('pjIDDirigeant4Signataire', 'pjIDDirigeant4Signataire', {
    label: userDeclarant4,
    mandatory: "true"
    });
    }

// PJ REPRÉSENTANT PERSONNE MORALE
//REP1
if  (Value('id').of($fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre1.personnaliteJuridique).eq('personneMorale')
and Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteCoExploitant')) {
    if (representant1.personneLieePMDenomination != null) {
        var userSociete1 = representant1.personneLieePMDenomination;
    } 
    attachment('pjRepresentantSignataire1', 'pjRepresentantSignataire1', {
    label: userSociete1,
    mandatory: "true"
    });
    }
 
//REP2
if  (Value('id').of($fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre2.personnaliteJuridique).eq('personneMorale')
and Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteCoExploitant')) {
    if (representant2.personneLieePMDenomination != null) {
        var userSociete2 = representant2.personneLieePMDenomination;
    } 
    attachment('pjRepresentantSignataire2', 'pjRepresentantSignataire2', {
    label: userSociete2,
    mandatory: "true"
    });
    }

//REP3
if  (Value('id').of($fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre3.personnaliteJuridique).eq('personneMorale')
and Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteCoExploitant')) {
    if (representant3.personneLieePMDenomination != null) {
        var userSociete3 = representant3.personneLieePMDenomination;
    } 
    attachment('pjRepresentantSignataire3', 'pjRepresentantSignataire3', {
    label: userSociete3,
    mandatory: "true"
    });
    }

//REP4
if  (Value('id').of($fAgri.cadreIdentiteMembreGroup.cadreIdentiteMembre4.personnaliteJuridique).eq('personneMorale')
and Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteCoExploitant')) {
    if (representant4.personneLieePMDenomination != null) {
        var userSociete4 = representant4.personneLieePMDenomination;
    } 
    attachment('pjRepresentantSignataire4', 'pjRepresentantSignataire4', {
    label: userSociete4,
    mandatory: "true"
    });
    }

// PJ MANDATAIRE
if (Value('id').of(pj.soussigne).eq('formaliteSignataireQualiteMandataire')) {
    attachment('pjPouvoir', 'pjPouvoir', {
        mandatory: "true"
    });
    attachment('pjIDMandataireSignataire', 'pjIDMandataireSignataire', {
        label: userMandataire.nomPrenomDenominationMandataire,
        mandatory: "true"
    });
}

