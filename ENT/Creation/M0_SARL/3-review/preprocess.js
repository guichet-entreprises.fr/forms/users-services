function pad(s) { return (s < 10) ? '0' + s : s; }
var formFields = {};


// Cadre 1 

var societe = $m0sarl.cadre1SocieteGroup.cadre1Identite;
var siege = $m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege; 

formFields['entreprise_formeJuridique_SARL']                                                      = Value('id').of(societe.entrepriseFormeJuridique).eq('sarl') ? true : false;
formFields['entreprise_formeJuridique_SELARL']                                                    = Value('id').of(societe.entrepriseFormeJuridique).eq('selarl') ? true : false;
formFields['entreprise_associeUnique']                                                            = societe.entrepriseAssocieUnique ? true : false;
formFields['formalite_creationSansActivite']                                                      = siege.formaliteCreationAvecActivite ? false : true;

// Cadre 2 - Déclaration relative à la société

formFields['entreprise_denomination']                                                             = societe.entrepriseDenomination;
formFields['entreprise_sigle']                                                                    = societe.entrepriseSigle;
formFields['entreprise_duree']                                                                    = societe.entrepriseDuree + ' ' + "ans";
formFields['entreprise_capitalMontant']                                                           = societe.entrepriseCapitalMontant + ' ' + "euros";
formFields['entreprise_capitalVariableMinimum']                                                   = societe.entrepriseCapitalVariableMinimum != null ? (societe.entrepriseCapitalVariableMinimum + ' ' + "euros") : '';
if (societe.entrepriseDateClotureExerciceSocial !== null) {
    var dateTmp = new Date(parseInt(societe.entrepriseDateClotureExerciceSocial.getTimeInMillis()));
    var dateClotureEc = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateClotureEc = dateClotureEc.concat(pad(month.toString()));
    formFields['entreprise_dateClotureExerciceSocial']          = dateClotureEc;
}
if(societe.entrepriseDateCloturePremierExerciceSocial != null) {
	var dateTmp = new Date(parseInt(societe.entrepriseDateCloturePremierExerciceSocial.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['entreprise_dateCloturePremierExerciceSocial'] = date;
}
formFields['entreprise_statutLegalParticulier']                                                   = societe.entrepriseCapitalVariable ? ("société à capital variable" + ' ' + (societe.entrepriseAssocieUnique ? "à associé unique" : '')) : (societe.entrepriseAssocieUnique ? "société à associé unique" : '');
formFields['entreprise_ESS']                                                                      = societe.entrepriseESS ? true : false;
formFields['entreprise_societeMission']                                                           = societe.entrepriseMission ? true : false;

// Cadre 3

formFields['entreprise_statutType_sansModification']                                              = Value('id').of(societe.entrepriseStatutType).eq('entrepriseStatutTypeSansModification');
formFields['entreprise_statutType_differents']                                                    = Value('id').of(societe.entrepriseStatutType).eq('entrepriseStatutTypeDifferents');
formFields['option_ME']                                                                           = societe.optionME ? true : false;

if (societe.entrepriseContratAppuiPresence) {
if(societe.contratAppuiDateFin != null) {
	var dateTmp = new Date(parseInt(societe.contratAppuiDateFin.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['entreprise_contratAppuiDateFin'] = date;
}
formFields['entrepriseLiee_contratAppuiDenomination']                                             = societe.cadre1ContratAppuiInfos.contratAppuiDenomination;
formFields['entrepriseLiee_siren_contratAppui']                                                   = societe.cadre1ContratAppuiInfos.contratAppuiSiren != null ? societe.cadre1ContratAppuiInfos.contratAppuiSiren.split(' ').join('') : '';
formFields['entrepriseLiee_adresse_voie']                                                         = (societe.cadre1ContratAppuiInfos.contratAppuiAdresse.numeroAdresseContratAppui != null ? societe.cadre1ContratAppuiInfos.contratAppuiAdresse.numeroAdresseContratAppui : '')
																									+ ' ' + (societe.cadre1ContratAppuiInfos.contratAppuiAdresse.indiceAdresseContratAppui != null ? societe.cadre1ContratAppuiInfos.contratAppuiAdresse.indiceAdresseContratAppui : '')
																									+ ' ' + (societe.cadre1ContratAppuiInfos.contratAppuiAdresse.typeAdresseContratAppui != null ? societe.cadre1ContratAppuiInfos.contratAppuiAdresse.typeAdresseContratAppui : '')
																									+ ' ' + societe.cadre1ContratAppuiInfos.contratAppuiAdresse.voieAdresseContratAppui;
formFields['entrepriseLiee_adresse_complementVoie']                                               = (societe.cadre1ContratAppuiInfos.contratAppuiAdresse.complementAdresseContratAppui != null ? societe.cadre1ContratAppuiInfos.contratAppuiAdresse.complementAdresseContratAppui : '')
																									+ ' ' + (societe.cadre1ContratAppuiInfos.contratAppuiAdresse.distributionSpecialeAdresseContratAppui != null ? societe.cadre1ContratAppuiInfos.contratAppuiAdresse.distributionSpecialeAdresseContratAppui : '');
formFields['entrepriseLiee_adresse_codePostal']                                                   = societe.cadre1ContratAppuiInfos.contratAppuiAdresse.codePostalAdresseContratAppui;
formFields['entrepriseLiee_adresse_commune']                                                      = societe.cadre1ContratAppuiInfos.contratAppuiAdresse.communeAdresseContratAppui;
}

// Cadre 4

formFields['entreprise_adresseEntreprisePM_voie']                                                 = (siege.cadreAdresseProfessionnelle.numeroAdresseSiege != null ? siege.cadreAdresseProfessionnelle.numeroAdresseSiege : '')
																									+ ' ' + (siege.cadreAdresseProfessionnelle.indiceAdresseSiege != null ? siege.cadreAdresseProfessionnelle.indiceAdresseSiege : '')
																									+ ' ' + (siege.cadreAdresseProfessionnelle.typeAdresseSiege != null ? siege.cadreAdresseProfessionnelle.typeAdresseSiege : '')
																									+ ' ' + siege.cadreAdresseProfessionnelle.voieAdresseSiege
																									+ ' ' + (siege.cadreAdresseProfessionnelle.complementAdresseSiege != null ? siege.cadreAdresseProfessionnelle.complementAdresseSiege : '')
																									+ ' ' + (siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege != null ? siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege : '');
formFields['entreprise_adresseEntreprisePM_codePostal']                                           = siege.cadreAdresseProfessionnelle.codePostalAdresseSiege;
formFields['entreprise_adresseEntreprisePM_commune']                                              = siege.cadreAdresseProfessionnelle.communeAdresseSiege;
formFields['entreprise_adresseEntreprisePM_communeAncienne']									  = siege.cadreAdresseProfessionnelle.communeAncienneAdresseSiege;
formFields['entreprise_adresseEntreprisePMSituation_domicile']                                    = Value('id').of(siege.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? true : false;
formFields['entreprise_adresseEntreprisePMSituation_domiciliation']                               = Value('id').of(siege.activiteLieu).eq('etablissementDomiciliationOui') ? true : false;
formFields['entrepriseLiee_siren']                                                                = siege.entrepriseLieeSiren != null ? siege.entrepriseLieeSiren.split(' ').join('') : '';
formFields['entrepriseLiee_nom']                                                                  = siege.entrepriseLieeNom != null ? siege.entrepriseLieeNom : '';

// Cadre 5

formFields['entreprise_activitesPrincipales']                                                     = siege.entrepriseActivitesPrincipales;

// Cadre 6

formFields['entreprise_fusionScission']                                                           = societe.entrepriseFusionScission ? true : false;

// Cadre 7

var etablissement = $m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite ;
if (siege.formaliteCreationAvecActivite) {
if (siege.etablissementPrincipalDifferentSiege) {
formFields['etablissement_adresse_voie']                                                          = (etablissement.cadreAdresseEtablissement.numeroAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.numeroAdresseEtablissement : '')
																									+ ' ' + (etablissement.cadreAdresseEtablissement.indiceAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.indiceAdresseEtablissement : '')
																									+ ' ' + (etablissement.cadreAdresseEtablissement.typeAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.typeAdresseEtablissement : '')
																									+ ' ' + etablissement.cadreAdresseEtablissement.voieAdresseEtablissement;
formFields['etablissement_adresse_complementVoie']                                                = (etablissement.cadreAdresseEtablissement.complementAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.complementAdresseEtablissement : '')
																									+ ' ' + (etablissement.cadreAdresseEtablissement.distributionSpecialeAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.distributionSpecialeAdresseEtablissement : '');
formFields['etablissement_adresse_codePostal']                                                    = etablissement.cadreAdresseEtablissement.codePostalAdresseEtablissement;
formFields['etablissement_adresse_commune']                                                       = etablissement.cadreAdresseEtablissement.communeAdresseEtablissement;
formFields['etablissement_adresse_communeAncienne']                                               = etablissement.cadreAdresseEtablissement.communeAncienneAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.communeAncienneAdresseEtablissement : '';
}

// Cadre 8

formFields['etablissement_nomCommercialProfessionnel']                                            = etablissement.etablissementNomCommercialProfessionnel != null ? etablissement.etablissementNomCommercialProfessionnel : '';
formFields['etablissement_enseigne']                                                              = etablissement.etablissementEnseigne != null ? etablissement.etablissementEnseigne : '';

// Cadre 9 

if(etablissement.etablissementDateDebutActivite != null) {
	var dateTmp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_dateDebutActivite'] = date;
}
formFields['etablissement_activitePermanenteSaisonniere_permanente']                               = Value('id').of(etablissement.activiteTemps).eq('etablissementActivitePermanenteSaisonnierePermanente') ? true : false;
formFields['etablissement_activitePermanenteSaisonniere_saisonniere']                             = Value('id').of(etablissement.activiteTemps).eq('etablissementActivitePermanenteSaisonniereSaisonniere') ? true : false;
formFields['etablissement_nonSedentariteQualite_ambulant']                                        = etablissement.etablissementNonSedentariteQualiteAmbulant ? true : false;
formFields['etablissement_activites']                                                             = etablissement.etablissementActivites;
formFields['etablissement_activitePlusImportante']                                                = etablissement.etablissementActivitesLaPlusImportante != null ? etablissement.etablissementActivitesLaPlusImportante : '';

if (Value('id').of(societe.entrepriseFormeJuridique).eq('sarl')) {
formFields['etablissement_activiteLieuExercice_magasin']                                          = Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMagasin') ? true : false;
formFields['etablissement_activiteLieuExerciceMagasin']                                           = etablissement.etablissementActiviteLieuExerciceMagasinSurface;
formFields['etablissement_activiteLieuExercice_marche']                                           = Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMarche') ? true : false;
formFields['etablissement_activiteNature_commerceDetail']                                         = Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceDetail') ? true : false;
formFields['etablissement_activiteNature_commerceGros']                                           = Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceGros') ? true : false;
formFields['etablissement_activiteNature_fabricationProduction']                                  = Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureFabricationProduction') ? true : false;
formFields['etablissement_activiteNature_batTravauxPublics']                                      = Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureBatTravauxPublics') ? true : false;
}
formFields['etablissement_activiteNature_cocheAutre']                                             = (Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCocheAutre') or Value('id').of(societe.entrepriseFormeJuridique).eq('selarl') or siege.activiteLiberaleOui) ? true : false;
formFields['etablissement_activiteNature_autre']                                                  =  Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCocheAutre') ? etablissement.etablissementActiviteNatureAutre : ((Value('id').of(societe.entrepriseFormeJuridique).eq('selarl') or siege.activiteLiberaleOui) ? "Profession libérale" : '');

// Cadre 10

var origine = $m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds ;

if (siege.activiteLiberaleOui or Value('id').of(societe.entrepriseFormeJuridique).eq('selarl')) {
formFields['etablissement_origineFonds_creation_activiteLiberale']                                = siege.etablissementPrincipalDifferentSiege ? (Value('id').of(origine.etablisementOrigineFondsLiberal).eq('EtablissementOrigineFondsCreation') ? true : false) : (not Value('id').of(siege.activiteLieu).eq('etablissementDomiciliationNon') ? true : (Value('id').of(origine.etablisementOrigineFondsLiberal).eq('EtablissementOrigineFondsCreation') ? true : false));
formFields['etablissement_origineFonds_reprise_activiteLiberale']                                 = Value('id').of(origine.etablisementOrigineFondsLiberal).eq('EtablissementOrigineFondsRepriseActiviteLiberale') ? true : false;
formFields['entrepriseLiee_siren_precedentExploitant_activiteLiberale']                           = origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant.split(' ').join('') : '';
formFields['entrepriseLiee_entreprisePP_nomNaissance_precedentExploitant_activiteLiberale']       = origine.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant : (origine.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant : '');
formFields['entrepriseLiee_entreprisePP_nomUsage_precedentExploitant_activiteLiberale']           = origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant : '';
formFields['entrepriseLiee_entreprisePP_prenom_precedentExploitant_activiteLiberale']             = origine.precedentExploitant.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant : '';
}

if (not siege.activiteLiberaleOui and Value('id').of(societe.entrepriseFormeJuridique).eq('sarl')) {
formFields['etablissement_origineFonds_creation_fondsCommerceArtisanal']                          = siege.etablissementPrincipalDifferentSiege ? (Value('id').of(origine.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsCreation') ? true : false) : (not Value('id').of(siege.activiteLieu).eq('etablissementDomiciliationNon') ? true : (Value('id').of(origine.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsCreation') ? true : false));
formFields['etablissement_origineFonds_achat_fondsCommerceArtisanal']                             = Value('id').of(origine.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsAchat') ? true : false;
formFields['etablissement_origineFonds_apport_fondsCommerceArtisanal']                            = Value('id').of(origine.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsApport') ? true : false;
if(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParution != null) {
	var dateTmp = new Date(parseInt(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParution.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_journalAnnoncesLegalesDateParution'] = date;
} else if(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParutionBis != null) {
	var dateTmp = new Date(parseInt(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParutionBis.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_journalAnnoncesLegalesDateParution'] = date;
}
formFields['etablissement_journalAnnoncesLegalesNom']                                             = origine.precedentExploitant.etablissementJournalAnnoncesLegalesNom != null ? origine.precedentExploitant.etablissementJournalAnnoncesLegalesNom : (origine.precedentExploitant.etablissementJournalAnnoncesLegalesNomBis != null ? origine.precedentExploitant.etablissementJournalAnnoncesLegalesNomBis : '');
formFields['entrepriseLiee_siren_precedentExploitant_fondsCommerceArtisanal']                     = origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant.split(' ').join('') : '';
formFields['entrepriseLiee_entreprisePP_nomNaissance_precedentExploitant_fondsCommerceArtisanal'] = origine.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant : (origine.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant : '');
formFields['entrepriseLiee_entreprisePP_nomUsage_precedentExploitant_fondsCommerceArtisanal']     = origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant : '';
formFields['entrepriseLiee_entreprisePP_prenom_precedentExploitant_fondsCommerceArtisanal']       = origine.precedentExploitant.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant : '';
formFields['etablissement_origineFonds_locationGerance_fondsCommerceArtisanal']                   = Value('id').of(origine.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsLocationGerance') ? true : false;
formFields['etablissement_origineFonds_geranceMandat_fondsCommerceArtisanal']                     = Value('id').of(origine.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsGeranceMandat') ? true : false;
formFields['etablissement_origineFonds_autre_fondsCommerceArtisanal']                             = Value('id').of(origine.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsCocheAutre') ? true : false;
formFields['etablissement_origineFonds_autre_Libelle']                                            = Value('id').of(origine.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsCocheAutre') ? origine.etablissementOrigineFondsAutre : '';
if(origine.geranceMandat.etablissementLoueurMandantDuFondsDebutContrat != null) {
	var dateTmp = new Date(parseInt(origine.geranceMandat.etablissementLoueurMandantDuFondsDebutContrat.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_loueurMandantDuFondsDebutContrat'] = date;
}
if(origine.geranceMandat.etablissementLoueurMandantDuFondsFinContrat != null) {
	var dateTmp = new Date(parseInt(origine.geranceMandat.etablissementLoueurMandantDuFondsFinContrat.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_loueurMandantDuFondsFinContrat'] = date;
}

if (Value('id').of(origine.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsLocationGerance') or Value('id').of(origine.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsGeranceMandat')) {
formFields['etablissement_loueurMandantDuFondsRenouvellementTaciteReconduction_oui']              = origine.geranceMandat.etablissementLoueurMandantDuFondsRenouvellementTaciteReconductionOui ? true : false;
formFields['etablissement_loueurMandantDuFondsRenouvellementTaciteReconduction_non']              = origine.geranceMandat.etablissementLoueurMandantDuFondsRenouvellementTaciteReconductionOui ? false : true ;
formFields['entrepriseLiee_entreprisePP_nomNaissance_loueurMandantDuFonds']                       = origine.geranceMandat.entrepriseLieeEntreprisePMDenominationLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeEntreprisePMDenominationLoueurMandantDuFonds : (origine.geranceMandat.entrepriseLieeEntreprisePMNomNaissanceLoueurMandantDuFonds !=null ? origine.geranceMandat.entrepriseLieeEntreprisePMNomNaissanceLoueurMandantDuFonds : '');
formFields['entrepriseLiee_entreprisePP_nomUsage_loueurMandantDuFonds']                           = origine.geranceMandat.entrepriseLieeEntreprisePPNomUsageLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeEntreprisePPNomUsageLoueurMandantDuFonds : '';
formFields['entrepriseLiee_entreprisePP_prenom_loueurMandantDuFonds']                             = origine.geranceMandat.entrepriseLieeEntreprisePPPrenom1LoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeEntreprisePPPrenom1LoueurMandantDuFonds : '';
formFields['entrepriseLiee_adresse_voie__loueurMandantDuFonds']                                   = (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.numRueAdresseLoueurMandantDufonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.numRueAdresseLoueurMandantDufonds : '') 
																									+ ' ' + (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.indiceVoieLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.indiceVoieLoueurMandantDuFonds : '')
																									+ ' ' + (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.typeVoieLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.typeVoieLoueurMandantDuFonds : '')
																									+ ' ' + origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.nomVoieLoueurMandantDuFonds
																									+ ' ' + (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.rueComplementAdresseLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.rueComplementAdresseLoueurMandantDuFonds : '')
																									+ ' ' + (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.distributionSpecialeLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.distributionSpecialeLoueurMandantDuFonds : '');                                                                                 
formFields['entrepriseLiee_adresse_codePostal_loueurMandantDuFonds']                              = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.codePostalLoueurMandantDuFonds;
formFields['entrepriseLiee_adresse_commune_loueurMandantDeFonds']                                 = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.communeLoueurMandantDuFonds;
formFields['entrepriseLiee_siren_geranceMandat']                                                  = origine.geranceMandat.entrepriseLieeSirenGeranceMandat != null ? origine.geranceMandat.entrepriseLieeSirenGeranceMandat.split(' ').join('') : '';
formFields['entrepriseLiee_greffeImmatriculation']                                                = origine.geranceMandat.entrepriseLieeGreffeImmatriculation != null ? origine.geranceMandat.entrepriseLieeGreffeImmatriculation : '';
}
}

// Cadre 11

formFields['etablissement_effectifSalariePresence_non']                                           = etablissement.etablissementEffectifSalariePresenceOui ? false : true;
formFields['etablissement_effectifSalariePresence_oui']                                           = etablissement.etablissementEffectifSalariePresenceOui ? true : false;
formFields['etablissement_effectifSalarieNombre']                                                 = etablissement.cadre3EffectifSalarie.etablissementEffectifSalarieNombre;
formFields['etablissement_effectifSalarieApprentis']                                              = etablissement.cadre3EffectifSalarie.etablissementEffectifSalarieApprentis;
formFields['etablissement_effectifSalarieEmbauchePremierSalarie_oui']                             = etablissement.cadre3EffectifSalarie.etablissementEffectifSalarieEmbauchePremierSalarieOui ? true : false;
formFields['etablissement_effectifSalarieEmbauchePremierSalarie_non']                             = etablissement.etablissementEffectifSalariePresenceOui ? (etablissement.cadre3EffectifSalarie.etablissementEffectifSalarieEmbauchePremierSalarieOui ? false : true) : false;
}

// Cadre 12

var gerance = $m0sarl.cadre5DirigeantGroup.cadre5Gerance;

formFields['entreprise_natureGerance_majoritaire']                                                = societe.entrepriseAssocieUniqueGerant ? true : (Value('id').of(gerance.typeGerance).eq('entrepriseNatureGeranceMajoritaire') ? true : false) ;
formFields['entreprise_natureGerance_minoritaireEgalitaire']                                      = Value('id').of(gerance.typeGerance).eq('entrepriseNatureGeranceMinoritaireEgalitaire') ? true : false;
formFields['entreprise_natureGerance_tiers']                                                      = Value('id').of(gerance.typeGerance).eq('entrepriseNatureGeranceTiers') ? true : false;
formFields['entreprise_natureGeranceSocieteAssociee_oui']                                         = gerance.entrepriseNatureGeranceSocieteAssocieeOui ? true : false;
formFields['entreprise_natureGeranceSocieteAssociee_non']                                         = Value('id').of(gerance.typeGerance).eq('entrepriseNatureGeranceMinoritaireEgalitaire') ? (gerance.entrepriseNatureGeranceSocieteAssocieeOui ? false : true) : false;

// Cadre 13 Gérant 1

var dirigeant1 = $m0sarl.cadre5DirigeantGroup.cadreIdentiteDirigeant1;

formFields['personneLiee_PP_nomNaissance_gerant1']                                                = dirigeant1.personneLieePPNomNaissanceGerant;
formFields['personneLiee_PP_nomUsage_gerant1']                                                    = dirigeant1.personneLieePPNomUsageGerant != null ? dirigeant1.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < dirigeant1.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant1.personneLieePPPrenomGerant[i]);}                            
formFields['personneLiee_PP_prenom_gerant1']                                                      = prenoms.toString();
if(dirigeant1.personneLieePPDateNaissanceGerant != null) {
	var dateTmp = new Date(parseInt(dirigeant1.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['personneLiee_PP_dateNaissance_gerant1'] = date;
}
formFields['personneLiee_PP_lieuNaissanceCommnune_gerant1']                                       = dirigeant1.personneLieePPLieuNaissanceCommnuneGerant != null ? dirigeant1.personneLieePPLieuNaissanceCommnuneGerant : dirigeant1.personneLieePersonnePhysiqueLieuNaissanceVille;
formFields['personneLiee_PP_lieuNaissancePays_gerant1']											  =	dirigeant1.personneLieePPLieuNaissanceDepartementGerant != null ? (dirigeant1.personneLieePPLieuNaissanceDepartementGerant.getLabel() + ' ' + "(" + '' + dirigeant1.personneLieePPLieuNaissanceDepartementGerant.getId() + '' + ")") : dirigeant1.personneLieePPLieuNaissancePaysGerant;
formFields['personneLiee_PP_nationalite_gerant1']                                                 = dirigeant1.personneLieePPNationaliteGerant;
formFields['personneLiee_adresse_voie_gerant1']                                                   = (dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdresserueNumeroVoie != null ? dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdresserueNumeroVoie : '') 
																									+ ' ' + (dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdresseIndiceVoie != null ? dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdresseIndiceVoie : '')
																									+ ' ' + (dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdresseTypeVoie != null ? dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdresseTypeVoie : '')
																									+ ' ' + dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdresseNomVoie
																									+ ' ' + (dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiquerueAdresseComplementAdresse != null ? dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiquerueAdresseComplementAdresse : '')
																									+ ' ' + (dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie != null ? dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie : '');
formFields['personneLiee_adresse_codePostal_gerant1']                                             = dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdresseCodePostal != null ? dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdresseCodePostal : '';
formFields['personneLiee_adresse_commune_gerant1']                                                = dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdresseCommune != null ? dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdresseCommune : (dirigeant1.cadre1AdresseDeclarant.personneLieeAdresseVille + ' / ' + dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdressePays);


// Cadre 13 bis Conjoint 1

var conjoint1 = $m0sarl.cadre5DirigeantGroup.cadreIdentiteDirigeant1.cadre2InfosConjoint;

formFields['personneLiee_conjointActiviteEntreprise_oui1']                                        = dirigeant1.conjointRole ? true : false;
formFields['personneLiee_conjointActiviteEntreprise_non1']                                        = dirigeant1.conjointRole ? false : true;
formFields['personneLiee_conjointStatut_salarie1']                                                = (Value('id').of(dirigeant1.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutSalarie') or Value('id').of(dirigeant1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutSalarie')) ? true : false;
formFields['personneLiee_conjointStatut_associe1']                                                = (Value('id').of(dirigeant1.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutAssocie') or Value('id').of(dirigeant1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) ? true : false;
formFields['personneLiee_conjointStatut_collaborateur1']                                          = Value('id').of(dirigeant1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') ? true : false;

if (Value('id').of(dirigeant1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') or Value('id').of(dirigeant1.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutSalarie') or Value('id').of(dirigeant1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutSalarie')) { 
formFields['personneLiee_PP_nomNaissance_conjointPacse1']                                         = conjoint1.personneLieePPNomNaissanceConjointPacse;
formFields['personneLiee_PP_nomUsage_conjointPacse1']                                             = conjoint1.personneLieePPNomUsageConjointPacse != null ? conjoint1.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < conjoint1.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(conjoint1.personneLieePPPrenomConjointPacse[i]);}                            
formFields['personneLiee_PP_prenom_conjointPacse1']                                                      = prenoms.toString();
if(conjoint1.personneLieePPDateNaissanceConjointPacse != null) {
	var dateTmp = new Date(parseInt(conjoint1.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['personneLiee_PP_dateNaissance_conjointPacse1'] = date;
}
formFields['personneLiee_PP_lieuNaissanceCommnune_conjointPacse1']                                = conjoint1.personneLieePPLieuNaissanceCommnuneConjointPacse != null ? conjoint1.personneLieePPLieuNaissanceCommnuneConjointPacse  : conjoint1.personneLieePersonnePhysiqueLieuNaissanceVilleConjointPacse;
formFields['personneLiee_PP_deptNaissance_conjointPacse1']                                        = conjoint1.personneLieePPDeptNaissanceConjointPacse != null ? (conjoint1.personneLieePPDeptNaissanceConjointPacse.getLabel() + ' ' + "(" + '' + conjoint1.personneLieePPDeptNaissanceConjointPacse.getId() + '' + ")") : conjoint1.personneLieePPPaysNaissanceConjointPacse;
formFields['personneLiee_PP_nationalite_conjointPacse1']                                          = conjoint1.personneLieePPNationaliteConjointPacse;
if (conjoint1.adresseConjointDifferente) {
formFields['personneLiee_adresse_voie_conjointPacse1']                                            = (conjoint1.adresseDomicileConjoint.adresseConjointNumeroVoie != null ? conjoint1.adresseDomicileConjoint.adresseConjointNumeroVoie : '') 
																									+ ' ' + (conjoint1.adresseDomicileConjoint.adresseConjointIndiceVoie != null ? conjoint1.adresseDomicileConjoint.adresseConjointIndiceVoie : '')
																									+ ' ' + (conjoint1.adresseDomicileConjoint.adresseConjointTypeVoie != null ? conjoint1.adresseDomicileConjoint.adresseConjointTypeVoie : '')
																									+ ' ' + conjoint1.adresseDomicileConjoint.adresseConjointNomVoie
																									+ ' ' + (conjoint1.adresseDomicileConjoint.adresseConjointComplementVoie != null ? conjoint1.adresseDomicileConjoint.adresseConjointComplementVoie : '')
																									+ ' ' + (conjoint1.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null ? conjoint1.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie : '');
formFields['personneLiee_adresse_codePostal_conjointPacse1']                                      = conjoint1.adresseDomicileConjoint.adresseConjointCodePostal != null ? conjoint1.adresseDomicileConjoint.adresseConjointCodePostal : '';
formFields['personneLiee_adresse_commune_conjointPacse1']                                         = conjoint1.adresseDomicileConjoint.adresseConjointCommune != null ? conjoint1.adresseDomicileConjoint.adresseConjointCommune : (conjoint1.adresseDomicileConjoint.adresseConjointVille + ' / ' + conjoint1.adresseDomicileConjoint.adresseConjointPays);
}
}

// Cadre 14 Gérant 2

var dirigeant2 = $m0sarl.cadre9DirigeantGroup.cadreIdentiteDirigeant2;
var conjoint2 = $m0sarl.cadre9DirigeantGroup.cadreIdentiteDirigeant2.cadre2InfosConjoint ;

if (dirigeant1.autreDirigeant1) {
formFields['personneLiee_PP_nomNaissance_gerant2']                                                = dirigeant2.personneLieePPNomNaissanceGerant;
formFields['personneLiee_PP_nomUsage_gerant2']                                                    = dirigeant2.personneLieePPNomUsageGerant != null ? dirigeant2.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < dirigeant2.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant2.personneLieePPPrenomGerant[i]);}                            
formFields['personneLiee_PP_prenom_gerant2']                                                      = prenoms.toString();
if(dirigeant2.personneLieePPDateNaissanceGerant != null) {
	var dateTmp = new Date(parseInt(dirigeant2.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['personneLiee_PP_dateNaissance_gerant2'] = date;
}
formFields['personneLiee_PP_lieuNaissanceCommnune_gerant2']                                       = dirigeant2.personneLieePPLieuNaissanceCommnuneGerant != null ? dirigeant2.personneLieePPLieuNaissanceCommnuneGerant : dirigeant2.personneLieePersonnePhysiqueLieuNaissanceVille;
formFields['personneLiee_PP_lieuNaissancePays_gerant2']											  =	dirigeant2.personneLieePPLieuNaissanceDepartementGerant != null ? (dirigeant2.personneLieePPLieuNaissanceDepartementGerant.getLabel() + ' ' + "(" + '' + dirigeant2.personneLieePPLieuNaissanceDepartementGerant.getId() + '' + ")") : dirigeant2.personneLieePPLieuNaissancePaysGerant;
formFields['personneLiee_PP_nationalite_gerant2']                                                 = dirigeant2.personneLieePPNationaliteGerant;
formFields['personneLiee_adresse_voie_gerant2']                                                   = (dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdresserueNumeroVoie != null ? dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdresserueNumeroVoie : '') 
																									+ ' ' + (dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdresseIndiceVoie != null ? dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdresseIndiceVoie : '')
																									+ ' ' + (dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdresseTypeVoie != null ? dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdresseTypeVoie : '')
																									+ ' ' + dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdresseNomVoie
																									+ ' ' + (dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiquerueAdresseComplementAdresse != null ? dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiquerueAdresseComplementAdresse : '')
																									+ ' ' + (dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie != null ? dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie : '');
formFields['personneLiee_adresse_codePostal_gerant2']                                             = dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdresseCodePostal != null ? dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdresseCodePostal : '';
formFields['personneLiee_adresse_commune_gerant2']                                                = dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdresseCommune != null ? dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdresseCommune : (dirigeant2.cadre2AdresseDeclarant.personneLieeAdresseVille + ' / ' + dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdressePays);

// Cadre 14 Bis conjoint 2


formFields['personneLiee_conjointActiviteEntreprise_oui2']                                        = dirigeant2.conjointRole ? true : false;
formFields['personneLiee_conjointActiviteEntreprise_non2']                                        = dirigeant2.conjointRole ? false : true;
formFields['personneLiee_conjointStatut_salarie2']                                                = (Value('id').of(dirigeant2.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutSalarie') or Value('id').of(dirigeant2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutSalarie')) ? true : false;
formFields['personneLiee_conjointStatut_associe2']                                                = (Value('id').of(dirigeant2.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutAssocie') or Value('id').of(dirigeant2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) ? true : false;
formFields['personneLiee_conjointStatut_collaborateur2']                                          = Value('id').of(dirigeant2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') ? true : false;

if (Value('id').of(dirigeant2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') or Value('id').of(dirigeant2.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutSalarie') or Value('id').of(dirigeant2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutSalarie')) { 
formFields['personneLiee_PP_nomNaissance_conjointPacse2']                                         = conjoint2.personneLieePPNomNaissanceConjointPacse;
formFields['personneLiee_PP_nomUsage_conjointPacse2']                                             = conjoint2.personneLieePPNomUsageConjointPacse != null ? conjoint2.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < conjoint2.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(conjoint2.personneLieePPPrenomConjointPacse[i]);}                            
formFields['personneLiee_PP_prenom_conjointPacse2']                                                      = prenoms.toString();
if(conjoint2.personneLieePPDateNaissanceConjointPacse != null) {
	var dateTmp = new Date(parseInt(conjoint2.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['personneLiee_PP_dateNaissance_conjointPacse2'] = date;
}
formFields['personneLiee_PP_lieuNaissanceCommnune_conjointPacse2']                                = conjoint2.personneLieePPLieuNaissanceCommnuneConjointPacse != null ? conjoint2.personneLieePPLieuNaissanceCommnuneConjointPacse  : conjoint2.personneLieePersonnePhysiqueLieuNaissanceVilleConjointPacse;
formFields['personneLiee_PP_deptNaissance_conjointPacse2']                                        = conjoint2.personneLieePPDeptNaissanceConjointPacse != null ? (conjoint2.personneLieePPDeptNaissanceConjointPacse.getLabel() + ' ' + "(" + '' + conjoint2.personneLieePPDeptNaissanceConjointPacse.getId() + '' + ")") : conjoint2.personneLieePPPaysNaissanceConjointPacse;
formFields['personneLiee_PP_nationalite_conjointPacse2']                                          = conjoint2.personneLieePPNationaliteConjointPacse;
if (conjoint2.adresseConjointDifferente) {
formFields['personneLiee_adresse_voie_conjointPacse2']                                            = (conjoint2.adresseDomicileConjoint.adresseConjointNumeroVoie != null ? conjoint2.adresseDomicileConjoint.adresseConjointNumeroVoie : '') 
																									+ ' ' + (conjoint2.adresseDomicileConjoint.adresseConjointIndiceVoie != null ? conjoint2.adresseDomicileConjoint.adresseConjointIndiceVoie : '')
																									+ ' ' + (conjoint2.adresseDomicileConjoint.adresseConjointTypeVoie != null ? conjoint2.adresseDomicileConjoint.adresseConjointTypeVoie : '')
																									+ ' ' + conjoint2.adresseDomicileConjoint.adresseConjointNomVoie
																									+ ' ' + (conjoint2.adresseDomicileConjoint.adresseConjointComplementVoie != null ? conjoint2.adresseDomicileConjoint.adresseConjointComplementVoie : '')
																									+ ' ' + (conjoint2.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null ? conjoint2.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie : '');
formFields['personneLiee_adresse_codePostal_conjointPacse2']                                      = conjoint2.adresseDomicileConjoint.adresseConjointCodePostal != null ? conjoint2.adresseDomicileConjoint.adresseConjointCodePostal : '';
formFields['personneLiee_adresse_commune_conjointPacse2']                                         = conjoint2.adresseDomicileConjoint.adresseConjointCommune != null ? conjoint2.adresseDomicileConjoint.adresseConjointCommune : (conjoint2.adresseDomicileConjoint.adresseConjointVille + ' / ' + conjoint2.adresseDomicileConjoint.adresseConjointPays);
}
}
}

// Cadre 15 Fondé de pouvoir 1

var fondePouvoir = $m0sarl.cadre19PeronnePouvoirGroup.cadre6PeronnePouvoir ;
var fondePouvoir1 = $m0sarl.cadre19PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir ;

if (fondePouvoir.personneLieePersonneLieeEtablissement) {
formFields['personneLiee_personnePouvoirLimiteEtablissement_oui1']                                = fondePouvoir1.personneLieePersonnePouvoirLimiteEtablissementOui1 ? true : false;
formFields['personneLiee_personnePouvoirLimiteEtablissement_non1']                                = fondePouvoir1.personneLieePersonnePouvoirLimiteEtablissementOui1 ? false : true;
formFields['personneLiee_PP_nomNaissance_personnePouvoir1']                                       = fondePouvoir1.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
formFields['personneLiee_PP_nomUsage_personnePouvoir1']                                           = fondePouvoir1.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? fondePouvoir1.personneLieePersonnePhysiqueNomUsagePersonnePouvoir : '';
var prenoms=[];
for ( i = 0; i < fondePouvoir1.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(fondePouvoir1.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFields['personneLiee_PP_prenom_personnePouvoir1']                                                      = prenoms.toString();
if(fondePouvoir1.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir != null) {
	var dateTmp = new Date(parseInt(fondePouvoir1.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['personneLiee_PP_dateNaissance_personnePouvoir1'] = date;
}
formFields['personneLiee_PP_lieuNaissanceDepartement_personnePouvoir1']                           = fondePouvoir1.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir != null ? fondePouvoir1.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir.getId() : '';
formFields['personneLiee_PP_lieuNaissanceCommnune_personnePouvoir1']                              = fondePouvoir1.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir != null ? fondePouvoir1.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir  :  (fondePouvoir1.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir + ' / ' + fondePouvoir1.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir);
formFields['personneLiee_PP_nationalite_personnePouvoir1']                                        = fondePouvoir1.personneLieePersonnePhysiqueNationalitePersonnePouvoir;
formFields['personneLiee_PP_adresse_voie_personnePouvoir1']                                       = (fondePouvoir1.cadre6AdressePersonnePouvoir.rueNumeroAdressePersonnePouvoir != null ? fondePouvoir1.cadre6AdressePersonnePouvoir.rueNumeroAdressePersonnePouvoir : '') 
																									+ ' ' + (fondePouvoir1.cadre6AdressePersonnePouvoir.indiceVoiePersonnePouvoir != null ? fondePouvoir1.cadre6AdressePersonnePouvoir.indiceVoiePersonnePouvoir : '')
																									+ ' ' + (fondePouvoir1.cadre6AdressePersonnePouvoir.typeVoiePersonnePouvoir != null ? fondePouvoir1.cadre6AdressePersonnePouvoir.typeVoiePersonnePouvoir : '')
																									+ ' ' + fondePouvoir1.cadre6AdressePersonnePouvoir.nomVoiePersonnePouvoir
																									+ ' ' + (fondePouvoir1.cadre6AdressePersonnePouvoir.rueComplementAdressePersonnePouvoir != null ? fondePouvoir1.cadre6AdressePersonnePouvoir.rueComplementAdressePersonnePouvoir : '')
																									+ ' ' + (fondePouvoir1.cadre6AdressePersonnePouvoir.distriutionSpecialePersonnePouvoir != null ? fondePouvoir1.cadre6AdressePersonnePouvoir.distriutionSpecialePersonnePouvoir : '');
formFields['personneLiee_PP_adresse_codePostal_personnePouvoir1']                                 = fondePouvoir1.cadre6AdressePersonnePouvoir.personneLieeAdresseCodePostalPersonnePouvoir;
formFields['personneLiee_PP_adresse_commune_personnePouvoir1']                                    = fondePouvoir1.cadre6AdressePersonnePouvoir.personneLieeAdresseCommunepersonnePouvoir;
}

// Cadre 16 Options fiscales

var fiscal = $m0sarl.cadre20optFiscGroup.cadre20optFisc ;

formFields['regimeFiscal_regimeImpositionBenefices_rsBIC']                                        = (Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeCommerciale).eq('regimeFiscalRegimeImpositionBeneficesReelBIC') and Value('id').of(fiscal.impositionBenefices.regimeFiscalReel).eq('regimeFiscalRegimeImpositionBeneficesRs')) ? true : false;
formFields['regimeFiscal_regimeImpositionBenefices_rnBIC']                                        = (Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeCommerciale).eq('regimeFiscalRegimeImpositionBeneficesReelBIC') and Value('id').of(fiscal.impositionBenefices.regimeFiscalReel).eq('regimeFiscalRegimeImpositionBeneficesRn')) ? true : false;
formFields['regimeFiscal_regimeImpositionBenefices_dcBNC']                                        = Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberale).eq('regimeFiscalRegimeImpositionBeneficesDcBNC') ? true : false;
formFields['regimeFiscal_regimeImpositionBenefices_rsIS']                                         = ((Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberale).eq('regimeFiscalRegimeImpositionBeneficesOptionIS') or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeCommerciale).eq('regimeFiscalRegimeImpositionBeneficesOptionIS')) and Value('id').of(fiscal.impositionBenefices.regimeFiscalReel).eq('regimeFiscalRegimeImpositionBeneficesRs')) ? true : false;
formFields['regimeFiscal_regimeImpositionBenefices_rnIS']                                         = ((Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberale).eq('regimeFiscalRegimeImpositionBeneficesOptionIS') or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeCommerciale).eq('regimeFiscalRegimeImpositionBeneficesOptionIS')) and Value('id').of(fiscal.impositionBenefices.regimeFiscalReel).eq('regimeFiscalRegimeImpositionBeneficesRn')) ? true : false;
formFields['regimeFiscal_regimeImpositionBeneficesOptionsParticulieres_optionIS']                 = (Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberale).eq('regimeFiscalRegimeImpositionBeneficesOptionIS') or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeCommerciale).eq('regimeFiscalRegimeImpositionBeneficesOptionIS')) ? true : false;
formFields['regimeFiscal_regimeImpositionBeneficesOptionsParticulieres_regimeSocietePersonnes']   = (Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberale).eq('regimeFiscalRegimeImpositionBeneficesDcBNC') or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeCommerciale).eq('regimeFiscalRegimeImpositionBeneficesReelBIC') or societe.optionME) ? true : false;
formFields['regimeFiscal_regimeImpositionBenefices_mBIC']                                         = (societe.optionME and not siege.activiteLiberaleOui and Value('id').of(societe.entrepriseFormeJuridique).eq('sarl')) ? true : false;
formFields['regimeFiscal_regimeImpositionBenefices_mBNC']                                         = (societe.optionME and (siege.activiteLiberaleOui or Value('id').of(societe.entrepriseFormeJuridique).eq('selarl'))) ? true : false;
formFields['regimeFiscal_regimeImpositionTVA_fTVA']                                               = (Value('id').of(fiscal.optionsTVA.regimeTVA1).eq('regimeFiscalRegimeImpositionTVARfTVA') or Value('id').of(fiscal.optionsTVA.regimeTVA2).eq('regimeFiscalRegimeImpositionTVARfTVA') or Value('id').of(fiscal.optionsTVA.regimeTVA3).eq('regimeFiscalRegimeImpositionTVARfTVA') or societe.optionME) ? true : false;
formFields['regimeFiscal_regimeImpositionTVA_rsTVA']                                              = (Value('id').of(fiscal.optionsTVA.regimeTVA1).eq('regimeFiscalRegimeImpositionTVARsTVA') or Value('id').of(fiscal.optionsTVA.regimeTVA3).eq('regimeFiscalRegimeImpositionTVARsTVA')) ? true : false;
formFields['regimeFiscal_regimeImpositionTVA_mrTVA']                                              = Value('id').of(fiscal.optionsTVA.regimeTVA1).eq('regimeFiscalRegimeImpositionTVAMrTVA') ? true : false;
formFields['regimeFiscal_regimeImpositionTVA_rnTVA']                                              = (Value('id').of(fiscal.optionsTVA.regimeTVA2).eq('regimeFiscalRegimeImpositionTVARnTVA') or Value('id').of(fiscal.optionsTVA.regimeTVA3).eq('regimeFiscalRegimeImpositionTVARnTVA')) ? true : false;
formFields['regimeFiscal_regimeImpositionTVAOptionsParticulieres2']                               = fiscal.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres2 ? true : false;
formFields['regimeFiscal_regimeImpositionTVAOptionsParticulieres1']                               = fiscal.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres1 ? true : false;

// Cadre 17 Observations

var correspondance = $m0sarl.cadre10RensCompGroup.cadre10RensComp ;

formFields['formalite_observations']                                                    = correspondance.formaliteObservations;

// Cadres 19 Correspondance

formFields['formalite_correspondanceCadresCoche']                                       = (Value('id').of(correspondance.adresseCorrespond).eq('siege') or Value('id').of(correspondance.adresseCorrespond).eq('etablissement')) ? true : false;
formFields['formalite_correspondanceCadresNumero']                                      = Value('id').of(correspondance.adresseCorrespond).eq('siege') ? "4" : (Value('id').of(correspondance.adresseCorrespond).eq('etablissement') ? "7" : '');
formFields['formalite_correspondanceAdresseCoche']                                      = Value('id').of(correspondance.adresseCorrespond).eq('autre') ? true : false;
if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
formFields['formalite_correspondanceAdresse_voie']                                      = correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance
																						  + ' ' + (correspondance.adresseCorrespondance.numeroRueAdresseCorrespondance != null ? correspondance.adresseCorrespondance.numeroRueAdresseCorrespondance : '')
																						  + ' ' + (correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance : '')
																						  + ' ' + (correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance : '')
																						  + ' ' + correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance;
formFields['formalite_correspondanceAdresse_complementVoie']                            = (correspondance.adresseCorrespondance.rueComplementAdresseCorrespondance != null ? correspondance.adresseCorrespondance.rueComplementAdresseCorrespondance : '')
																						  + ' ' + (correspondance.adresseCorrespondance.distriutionSpecialeVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.distriutionSpecialeVoieAdresseCorrespondance : '');
formFields['formalite_correspondanceAdresse_codePostal']                                = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal;
formFields['formalite_correspondanceAdresse_commune']									= correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune;
}
formFields['formalite_telephone1']                                                      = correspondance.infosSup.formaliteTelephone2;
formFields['formalite_telephone2']                                                      = correspondance.infosSup.formaliteTelephone1 != null ? correspondance.infosSup.formaliteTelephone1 : '';
formFields['formalite_courriel']                                                        = correspondance.infosSup.formaliteFaxCourriel != null ? correspondance.infosSup.formaliteFaxCourriel : (correspondance.infosSup.telecopie != null ? correspondance.infosSup.telecopie : '');

// Cadres 20 - Signature

var signataire = $m0sarl.cadre11SignatureGroup.cadre11Signature ;
var jqpa1 = $m0sarl.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification1;
var jqpa2 = $m0sarl.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification2;
var jqpa3 = $m0sarl.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification3;

formFields['formalite_signataireQualite_representantLegal']                             = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentantLegal') ? true : false;
formFields['formalite_signataireQualite_representantLegal_cadre']                       = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentantLegal') ? ((societe.entrepriseAssocieUniqueGerant or not dirigeant1.autreDirigeant1 or Value('id').of(signataire.representantLegalNumero).eq('gerant1')) ? "13" : (Value('id').of(signataire.representantLegalNumero).eq('gerant2') ? "14" : (Value('id').of(signataire.representantLegalNumero).eq('gerant3') ? "2 M0'" : (Value('id').of(signataire.representantLegalNumero).eq('gerant4') ? "2b M0'" : '')))) : '';
formFields['formalite_signataireQualite_mandataire']                                    = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFields['formalite_signataireNom']						           					= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFields['formalite_signataireAdresse_voie']                                          = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																						  + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																						  + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																						  + ' ' + signataire.adresseMandataire.nomVoieMandataire
																						  + ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																						  + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');
formFields['formalite_signataire_adresseCP']            				                = signataire.adresseMandataire.codePostalMandataire;
formFields['formalite_signataire_adresseCommune']        			                    = signataire.adresseMandataire.villeAdresseMandataire;
}
formFields['formalite_signatureLieu']                                                   = signataire.formaliteSignatureLieu;
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['formalite_signatureDate'] = date;
}
formFields['nombreIntercalaireM0Prime']                                                 = societe.fusionGroup2.fusionAutreSociete2 ? "3" : ((societe.fusionGroup1.fusionAutreSociete or fondePouvoir.infoPeronnePouvoir2.autreDeclarationPersonneLiee2) ? "2" : ((fondePouvoir.infoPeronnePouvoir.autreDeclarationPersonneLiee or dirigeant2.autreDirigeant2 or societe.entrepriseFusionScission) ? "1" : "0"));
formFields['nombreIntercalaireTNS']                                                     = '';
formFields['nombreIntercalaireJQPA']                                                    = (jqpa3.jqpaSituation != null ? "3" : (jqpa2.jqpaSituation != null ? "2" : (jqpa1.jqpaSituation != null ? "1" : "0")));
formFields['nombreIntercalaireNDI']                                                     = (((siege.societeNomDomaine[0].length > 0) and siege.societeNomDomaine != null) or etablissement.etablissementNomDomaine != null) ? "1" : "0";
formFields['nombreIntercalaireMBE']                                                     = signataire.nombreMBE.getLabel();
formFields['signature']                                                                 = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce";

// M0 PRIME SARL 1

var formFieldsPers1 = {}
formFieldsPers1['numeroIntercalaireM0Prime']                                            = "01" 
formFieldsPers1['entreprise_denominationPrime']                                         = societe.entrepriseDenomination;
formFieldsPers1['entreprise_formeJuridiquePrime']                                       = Value('id').of(societe.entrepriseFormeJuridique).eq('sarl') ? (societe.entrepriseAssocieUnique ? "EURL" : "SARL") : (societe.entrepriseAssocieUnique ? "SELARLU" : "SELARL"); 

// Gérant 3

var dirigeant3 = $m0sarl.cadre12DirigeantGroup.cadreIdentiteDirigeant3;
var conjoint3 = $m0sarl.cadre12DirigeantGroup.cadreIdentiteDirigeant3.cadre2InfosConjoint;

if (dirigeant2.autreDirigeant2) {
formFieldsPers1['personneLiee_PP_nomNaissance_gerant1Prime']                                                = dirigeant3.personneLieePPNomNaissanceGerant;
formFieldsPers1['personneLiee_PP_nomUsage_gerant1Prime']                                                    = dirigeant3.personneLieePPNomUsageGerant != null ? dirigeant3.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < dirigeant3.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant3.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['personneLiee_PP_prenom_gerant1Prime']                                                      = prenoms.toString();
if(dirigeant3.personneLieePPDateNaissanceGerant != null) {
	var dateTmp = new Date(parseInt(dirigeant3.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers1['personneLiee_PP_dateNaissance_gerant1Prime'] = date;
}
formFieldsPers1['personneLiee_PP_lieuNaissanceCommnune_gerant1Prime']                                       = dirigeant3.personneLieePPLieuNaissanceCommnuneGerant != null ? (dirigeant3.personneLieePPLieuNaissanceCommnuneGerant + ' ' + "(" + '' + dirigeant3.personneLieePPLieuNaissanceDepartementGerant.getId() + '' + ")")  : (dirigeant3.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant3.personneLieePPLieuNaissancePaysGerant);
formFieldsPers1['personneLiee_PP_nationalite_gerant1Prime']                                                 = dirigeant3.personneLieePPNationaliteGerant;
formFieldsPers1['personneLiee_adresse_voie_gerant1Prime']                                                   = (dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdresserueNumeroVoie != null ? dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdresserueNumeroVoie : '') 
																											+ ' ' + (dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdresseIndiceVoie != null ? dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdresseIndiceVoie : '')
																											+ ' ' + (dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdresseTypeVoie != null ? dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdresseTypeVoie : '')
																											+ ' ' + dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdresseNomVoie
																											+ ' ' + (dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiquerueAdresseComplementAdresse != null ? dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiquerueAdresseComplementAdresse : '')
																											+ ' ' + (dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie != null ? dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie : '');
formFieldsPers1['personneLiee_adresse_codePostal_gerant1Prime']                                             = dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdresseCodePostal != null ? dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdresseCodePostal : '';
formFieldsPers1['personneLiee_adresse_commune_gerant1Prime']                                                = dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdresseCommune != null ? dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdresseCommune : (dirigeant3.cadre3AdresseDeclarant.personneLieeAdresseVille + ' / ' + dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdressePays);

// Conjoint 3 


formFieldsPers1['personneLiee_conjointActiviteEntreprise_oui1Prime']                         = dirigeant3.conjointRole ? true : false;
formFieldsPers1['personneLiee_conjointActiviteEntreprise_non1Prime']                         = dirigeant3.conjointRole ? false : true;
formFieldsPers1['personneLiee_conjointStatut_salarie1Prime']                            = (Value('id').of(dirigeant3.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutSalarie') or Value('id').of(dirigeant3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutSalarie')) ? true : false;
formFieldsPers1['personneLiee_conjointStatut_associe1Prime']                            = (Value('id').of(dirigeant3.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutAssocie') or Value('id').of(dirigeant3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) ? true : false;
formFieldsPers1['personneLiee_conjointStatut_collaborateur1Prime']                      = Value('id').of(dirigeant3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') ? true : false;

if (Value('id').of(dirigeant3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') or Value('id').of(dirigeant3.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutSalarie') or Value('id').of(dirigeant3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutSalarie')) { 
formFieldsPers1['personneLiee_conjointStatut_conjointPacse_collaborateur1Prime']        = Value('id').of(dirigeant3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') ? true : false;
formFieldsPers1['personneLiee_conjointStatut_conjointPacse_associe1Prime']              = false;
formFieldsPers1['personneLiee_PP_nomNaissance_conjointPacse1Prime']                                 = conjoint3.personneLieePPNomNaissanceConjointPacse;
formFieldsPers1['personneLiee_PP_nomUsage_conjointPacse1Prime']                                     = conjoint3.personneLieePPNomUsageConjointPacse != null ? conjoint3.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < conjoint3.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(conjoint3.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers1['personneLiee_PP_prenom_conjointPacse1Prime']                                       = prenoms.toString();
if(conjoint3.personneLieePPDateNaissanceConjointPacse != null) {
	var dateTmp = new Date(parseInt(conjoint3.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers1['personneLiee_PP_dateNaissance_conjointPacse1Prime'] = date;
}
formFieldsPers1['personneLiee_PP_lieuNaissanceCommnune_conjointPacse1Prime']                        = conjoint3.personneLieePPLieuNaissanceCommnuneConjointPacse != null ? (conjoint3.personneLieePPLieuNaissanceCommnuneConjointPacse + ' ' + "(" + '' + conjoint3.personneLieePPDeptNaissanceConjointPacse.getId() + '' + ")") : (conjoint3.personneLieePersonnePhysiqueLieuNaissanceVilleConjointPacse + ' / ' + conjoint3.personneLieePPPaysNaissanceConjointPacse);
formFieldsPers1['personneLiee_PP_nationalite_conjointPacse1Prime']                                  = conjoint3.personneLieePPNationaliteConjointPacse;
if (conjoint3.adresseConjointDifferente) {
formFieldsPers1['personneLiee_adresse_voie_conjointPacse1Prime']                                    = (conjoint3.adresseDomicileConjoint.adresseConjointNumeroVoie != null ? conjoint3.adresseDomicileConjoint.adresseConjointNumeroVoie : '') 
																									+ ' ' + (conjoint3.adresseDomicileConjoint.adresseConjointIndiceVoie != null ? conjoint3.adresseDomicileConjoint.adresseConjointIndiceVoie : '')
																									+ ' ' + (conjoint3.adresseDomicileConjoint.adresseConjointTypeVoie != null ? conjoint3.adresseDomicileConjoint.adresseConjointTypeVoie : '')
																									+ ' ' + conjoint3.adresseDomicileConjoint.adresseConjointNomVoie
																									+ ' ' +	(conjoint3.adresseDomicileConjoint.adresseConjointComplementVoie != null ? conjoint3.adresseDomicileConjoint.adresseConjointComplementVoie : '')
																									+ ' ' + (conjoint3.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null ? conjoint3.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie : '');
formFieldsPers1['personneLiee_adresse_codePostal_conjointPacse1Prime']                              = conjoint3.adresseDomicileConjoint.adresseConjointCodePostal != null ? conjoint3.adresseDomicileConjoint.adresseConjointCodePostal : '';
formFieldsPers1['personneLiee_adresse_commune_conjointPacse1Prime']                                 = conjoint3.adresseDomicileConjoint.adresseConjointCommune != null ? conjoint3.adresseDomicileConjoint.adresseConjointCommune : (conjoint3.adresseDomicileConjoint.adresseConjointVille + ' / ' + conjoint3.adresseDomicileConjoint.adresseConjointPays);
}
}
}

// Gérant 4

var dirigeant4 = $m0sarl.cadre15DirigeantGroup.cadreIdentiteDirigeant4;
var conjoint4 = $m0sarl.cadre15DirigeantGroup.cadreIdentiteDirigeant4.cadre2InfosConjoint;

if (dirigeant3.autreDirigeant3) {
formFieldsPers1['personneLiee_PP_nomNaissance_gerant2Prime']                                                = dirigeant4.personneLieePPNomNaissanceGerant;
formFieldsPers1['personneLiee_PP_nomUsage_gerant2Prime']                                                    = dirigeant4.personneLieePPNomUsageGerant != null ? dirigeant4.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < dirigeant4.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant4.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['personneLiee_PP_prenom_gerant2Prime']                                                      = prenoms.toString();
if(dirigeant4.personneLieePPDateNaissanceGerant != null) {
	var dateTmp = new Date(parseInt(dirigeant4.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers1['personneLiee_PP_dateNaissance_gerant2Prime'] = date;
}
formFieldsPers1['personneLiee_PP_lieuNaissanceCommnune_gerant2Prime']                                       = dirigeant4.personneLieePPLieuNaissanceCommnuneGerant != null ? (dirigeant4.personneLieePPLieuNaissanceCommnuneGerant + ' '+ "(" + ''+ dirigeant4.personneLieePPLieuNaissanceDepartementGerant.getId() + '' + ")")  : (dirigeant4.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant4.personneLieePPLieuNaissancePaysGerant);
formFieldsPers1['personneLiee_PP_nationalite_gerant2Prime']                                                 = dirigeant4.personneLieePPNationaliteGerant;
formFieldsPers1['personneLiee_adresse_voie_gerant2Prime']                                                   = (dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdresserueNumeroVoie != null ? dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdresserueNumeroVoie : '') 
																											+ ' ' + (dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdresseIndiceVoie != null ? dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdresseIndiceVoie : '')
																											+ ' ' + (dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdresseTypeVoie != null ? dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdresseTypeVoie : '')
																											+ ' ' + dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdresseNomVoie
																											+ ' ' + (dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiquerueAdresseComplementAdresse != null ? dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiquerueAdresseComplementAdresse : '')
																											+ ' ' + (dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie != null ? dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie : '');
formFieldsPers1['personneLiee_adresse_codePostal_gerant2Prime']                                             = dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdresseCodePostal != null ? dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdresseCodePostal : '';
formFieldsPers1['personneLiee_adresse_commune_gerant2Prime']                                                = dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdresseCommune != null ? dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdresseCommune : (dirigeant4.cadre4AdresseDeclarant.personneLieeAdresseVille + ' / ' + dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdressePays);

// Conjoint 4


formFieldsPers1['personneLiee_conjointActiviteEntreprise_oui2Prime']                    = dirigeant4.conjointRole ? true : false;
formFieldsPers1['personneLiee_conjointActiviteEntreprise_non2Prime']                    = dirigeant4.conjointRole ? false : true;
formFieldsPers1['personneLiee_conjointStatut_salarie2Prime']                            = (Value('id').of(dirigeant4.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutSalarie') or Value('id').of(dirigeant4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutSalarie')) ? true : false;
formFieldsPers1['personneLiee_conjointStatut_associe2Prime']                            = (Value('id').of(dirigeant4.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutAssocie') or Value('id').of(dirigeant4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) ? true : false;
formFieldsPers1['personneLiee_conjointStatut_collaborateur2Prime']                      = Value('id').of(dirigeant4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') ? true : false;

if (Value('id').of(dirigeant4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') or Value('id').of(dirigeant4.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutSalarie') or Value('id').of(dirigeant4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutSalarie')) { 
formFieldsPers1['personneLiee_conjointStatut_conjointPacse_collaborateur2Prime']        = Value('id').of(dirigeant4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') ? true : false;
formFieldsPers1['personneLiee_conjointStatut_conjointPacse_associe2Prime']              = false;
formFieldsPers1['personneLiee_PP_nomNaissance_conjointPacse2Prime']                                 = conjoint4.personneLieePPNomNaissanceConjointPacse;
formFieldsPers1['personneLiee_PP_nomUsage_conjointPacse2Prime']                                     = conjoint4.personneLieePPNomUsageConjointPacse != null ? conjoint4.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < conjoint4.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(conjoint4.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers1['personneLiee_PP_prenom_conjointPacse2Prime']                                       = prenoms.toString();
if(conjoint4.personneLieePPDateNaissanceConjointPacse != null) {
	var dateTmp = new Date(parseInt(conjoint4.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers1['personneLiee_PP_dateNaissance_conjointPacse2Prime'] = date;
}
formFieldsPers1['personneLiee_PP_lieuNaissanceCommnune_conjointPacse2Prime']                        = conjoint4.personneLieePPLieuNaissanceCommnuneConjointPacse != null ? (conjoint4.personneLieePPLieuNaissanceCommnuneConjointPacse + ' ' + "(" + '' + conjoint4.personneLieePPDeptNaissanceConjointPacse.getId() + '' + ")") : (conjoint4.personneLieePersonnePhysiqueLieuNaissanceVilleConjointPacse + ' / ' + conjoint4.personneLieePPPaysNaissanceConjointPacse);
formFieldsPers1['personneLiee_PP_nationalite_conjointPacse2Prime']                                  = conjoint4.personneLieePPNationaliteConjointPacse;
if (conjoint4.adresseConjointDifferente) {
formFieldsPers1['personneLiee_adresse_voie_conjointPacse2Prime']                                    = (conjoint4.adresseDomicileConjoint.adresseConjointNumeroVoie != null ? conjoint4.adresseDomicileConjoint.adresseConjointNumeroVoie : '') 
																									+ ' ' + (conjoint4.adresseDomicileConjoint.adresseConjointIndiceVoie != null ? conjoint4.adresseDomicileConjoint.adresseConjointIndiceVoie : '')
																									+ ' ' + (conjoint4.adresseDomicileConjoint.adresseConjointTypeVoie != null ? conjoint4.adresseDomicileConjoint.adresseConjointTypeVoie : '')
																									+ ' ' + conjoint4.adresseDomicileConjoint.adresseConjointNomVoie
																									+ ' ' + (conjoint4.adresseDomicileConjoint.adresseConjointComplementVoie != null ? conjoint4.adresseDomicileConjoint.adresseConjointComplementVoie : '')
																									+ ' ' + (conjoint4.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null ? conjoint4.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie : '');
formFieldsPers1['personneLiee_adresse_codePostal_conjointPacse2Prime']                              = conjoint4.adresseDomicileConjoint.adresseConjointCodePostal != null ? conjoint4.adresseDomicileConjoint.adresseConjointCodePostal : '';
formFieldsPers1['personneLiee_adresse_commune_conjointPacse2Prime']                                 = conjoint4.adresseDomicileConjoint.adresseConjointCommune != null ? conjoint4.adresseDomicileConjoint.adresseConjointCommune : (conjoint4.adresseDomicileConjoint.adresseConjointVille + ' / ' + conjoint4.adresseDomicileConjoint.adresseConjointPays);
}
}
}

// Fondé de pouvoir 2

var fondePouvoir2 = $m0sarl.cadre19PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir2 ;

if (fondePouvoir1.autreDeclarationPersonneLiee) {
formFieldsPers1['personnePouvoirOui']                                                        = true;
formFieldsPers1['personneLiee_personnePouvoirLimiteEtablissement_oui1Prime']                 = fondePouvoir2.personneLieePersonnePouvoirLimiteEtablissementOui2 ? true : false;
formFieldsPers1['personneLiee_personnePouvoirLimiteEtablissement_non1Prime']                 = fondePouvoir2.personneLieePersonnePouvoirLimiteEtablissementOui2 ? false : true;
formFieldsPers1['personneLiee_PP_nomNaissance_personnePouvoir1Prime']                        = fondePouvoir2.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
formFieldsPers1['personneLiee_PP_nomUsage_personnePouvoir1Prime']                            = fondePouvoir2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? fondePouvoir2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir : '';
var prenoms=[];
for ( i = 0; i < fondePouvoir2.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(fondePouvoir2.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers1['personneLiee_PP_prenom_personnePouvoir1Prime']                              = prenoms.toString();
if(fondePouvoir2.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir != null) {
	var dateTmp = new Date(parseInt(fondePouvoir2.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers1['personneLiee_PP_dateNaissance_personnePouvoir1Prime'] = date;
}
formFieldsPers1['personneLiee_PP_lieuNaissanceCommnune_personnePouvoir1Prime']                      = fondePouvoir2.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir != null ? (fondePouvoir2.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir + ' ' + "(" + '' + fondePouvoir2.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir.getId() + '' + ")") :  (fondePouvoir2.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir + ' / ' + fondePouvoir2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir);
formFieldsPers1['personneLiee_PP_nationalite_personnePouvoir1Prime']                                = fondePouvoir2.personneLieePersonnePhysiqueNationalitePersonnePouvoir;
formFieldsPers1['personneLiee_PP_adresse_voie_personnePouvoir1Prime']                               = (fondePouvoir2.cadre6AdressePersonnePouvoir.rueNumeroAdressePersonnePouvoir != null ? fondePouvoir2.cadre6AdressePersonnePouvoir.rueNumeroAdressePersonnePouvoir : '') 
																									+ ' ' + (fondePouvoir2.cadre6AdressePersonnePouvoir.indiceVoiePersonnePouvoir != null ? fondePouvoir2.cadre6AdressePersonnePouvoir.indiceVoiePersonnePouvoir : '')
																									+ ' ' + (fondePouvoir2.cadre6AdressePersonnePouvoir.typeVoiePersonnePouvoir != null ? fondePouvoir2.cadre6AdressePersonnePouvoir.typeVoiePersonnePouvoir : '')
																									+ ' ' + fondePouvoir2.cadre6AdressePersonnePouvoir.nomVoiePersonnePouvoir
																									+ ' ' + (fondePouvoir2.cadre6AdressePersonnePouvoir.rueComplementAdressePersonnePouvoir != null ? fondePouvoir2.cadre6AdressePersonnePouvoir.rueComplementAdressePersonnePouvoir : '')
																									+ ' ' + (fondePouvoir2.cadre6AdressePersonnePouvoir.distriutionSpecialePersonnePouvoir != null ? fondePouvoir2.cadre6AdressePersonnePouvoir.distriutionSpecialePersonnePouvoir : '');
formFieldsPers1['personneLiee_PP_adresse_codePostal_personnePouvoir1Prime']                         = fondePouvoir2.cadre6AdressePersonnePouvoir.personneLieeAdresseCodePostalPersonnePouvoir;
formFieldsPers1['personneLiee_PP_adresse_commune_personnePouvoir1Prime']                            = fondePouvoir2.cadre6AdressePersonnePouvoir.personneLieeAdresseCommunepersonnePouvoir;
}

// Fusion - scission société 1

var fusion1 = $m0sarl.cadre1SocieteGroup.cadre1Identite.fusionGroup1 ;

if (societe.entrepriseFusionScission) {
formFieldsPers1['entrepriseLiee_sirenPrime']                                                 = fusion1.entrepriseLieeSiren.split(' ').join('');
formFieldsPers1['entrepriseLiee_greffeImmatriculationPrime']                                 = fusion1.entrepriseLieeGreffe;
formFieldsPers1['entrepriseLiee_entreprisePM_denominationPrime']                             = fusion1.entrepriseLieeEntreprisePMDenomination + ' ' + fusion1.entreperiseLieeEntreprisePMFormeJuridique;
formFieldsPers1['entrepriseLiee_adresse_voie_Prime']                                         = (fusion1.adresseFusionScission.numeroAdresseFusion != null ? fusion1.adresseFusionScission.numeroAdresseFusion : '')
																							 + ' ' + (fusion1.adresseFusionScission.indiceAdresseFusion != null ? fusion1.adresseFusionScission.indiceAdresseFusion : '')
																							 + ' ' + (fusion1.adresseFusionScission.typeAdresseFusion != null ? fusion1.adresseFusionScission.typeAdresseFusion : '')
																							 + ' ' + fusion1.adresseFusionScission.voieAdresseFusion
																							 + ' ' + (fusion1.adresseFusionScission.complementAdresseFusion != null ? fusion1.adresseFusionScission.complementAdresseFusion : '')
																							 + ' ' + (fusion1.adresseFusionScission.distributionSpecialeAdresseFusion != null ? fusion1.adresseFusionScission.distributionSpecialeAdresseFusion : '');
formFieldsPers1['entrepriseLiee_adresse_commune_Prime']                                      = fusion1.adresseFusionScission.codePostalAdresseFusion + ' ' + fusion1.adresseFusionScission.communeAdresseFusion;
}

// M0 PRIME SARL 2

var formFieldsPers2 = {}
formFieldsPers2['numeroIntercalaireM0Prime']                                            = "02" 
formFieldsPers2['entreprise_denominationPrime']                                         = societe.entrepriseDenomination;
formFieldsPers2['entreprise_formeJuridiquePrime']                                       = Value('id').of(societe.entrepriseFormeJuridique).eq('sarl') ? (societe.entrepriseAssocieUnique ? "EURL" : "SARL") : (societe.entrepriseAssocieUnique ? "SELARLU" : "SELARL"); 


// Fondé de pouvoir 3

var fondePouvoir3 = $m0sarl.cadre19PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir3 ;

if (fondePouvoir2.autreDeclarationPersonneLiee2) {
formFieldsPers2['personnePouvoirOui']                                                        = true;
formFieldsPers2['personneLiee_personnePouvoirLimiteEtablissement_oui1Prime']                 = fondePouvoir3.personneLieePersonnePouvoirLimiteEtablissementOui3 ? true : false;
formFieldsPers2['personneLiee_personnePouvoirLimiteEtablissement_non1Prime']                 = fondePouvoir3.personneLieePersonnePouvoirLimiteEtablissementOui3 ? false : true;
formFieldsPers2['personneLiee_PP_nomNaissance_personnePouvoir1Prime']                        = fondePouvoir3.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
formFieldsPers2['personneLiee_PP_nomUsage_personnePouvoir1Prime']                            = fondePouvoir3.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? fondePouvoir3.personneLieePersonnePhysiqueNomUsagePersonnePouvoir : '';
var prenoms=[];
for ( i = 0; i < fondePouvoir3.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(fondePouvoir3.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers2['personneLiee_PP_prenom_personnePouvoir1Prime']                              = prenoms.toString();
if(fondePouvoir3.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir != null) {
	var dateTmp = new Date(parseInt(fondePouvoir3.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers2['personneLiee_PP_dateNaissance_personnePouvoir1Prime'] = date;
}
formFieldsPers2['personneLiee_PP_lieuNaissanceCommnune_personnePouvoir1Prime']                      = fondePouvoir3.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir != null ? (fondePouvoir3.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir + ' ' + "(" + '' + fondePouvoir3.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir.getId() + '' + ")") :  (fondePouvoir3.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir + ' / ' + fondePouvoir3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir);
formFieldsPers2['personneLiee_PP_nationalite_personnePouvoir1Prime']                                = fondePouvoir3.personneLieePersonnePhysiqueNationalitePersonnePouvoir;
formFieldsPers2['personneLiee_PP_adresse_voie_personnePouvoir1Prime']                               = (fondePouvoir3.cadre6AdressePersonnePouvoir.rueNumeroAdressePersonnePouvoir != null ? fondePouvoir3.cadre6AdressePersonnePouvoir.rueNumeroAdressePersonnePouvoir : '') 
																									+ ' ' + (fondePouvoir3.cadre6AdressePersonnePouvoir.indiceVoiePersonnePouvoir != null ? fondePouvoir3.cadre6AdressePersonnePouvoir.indiceVoiePersonnePouvoir : '')
																									+ ' ' + (fondePouvoir3.cadre6AdressePersonnePouvoir.typeVoiePersonnePouvoir != null ? fondePouvoir3.cadre6AdressePersonnePouvoir.typeVoiePersonnePouvoir : '')
																									+ ' ' + fondePouvoir3.cadre6AdressePersonnePouvoir.nomVoiePersonnePouvoir
																									+ ' ' + (fondePouvoir3.cadre6AdressePersonnePouvoir.rueComplementAdressePersonnePouvoir != null ? fondePouvoir3.cadre6AdressePersonnePouvoir.rueComplementAdressePersonnePouvoir : '')
																									+ ' ' + (fondePouvoir3.cadre6AdressePersonnePouvoir.distriutionSpecialePersonnePouvoir != null ? fondePouvoir3.cadre6AdressePersonnePouvoir.distriutionSpecialePersonnePouvoir : '');
formFieldsPers2['personneLiee_PP_adresse_codePostal_personnePouvoir1Prime']                         = fondePouvoir3.cadre6AdressePersonnePouvoir.personneLieeAdresseCodePostalPersonnePouvoir;
formFieldsPers2['personneLiee_PP_adresse_commune_personnePouvoir1Prime']                            = fondePouvoir3.cadre6AdressePersonnePouvoir.personneLieeAdresseCommunepersonnePouvoir;
}

// Fusion - scission société 2

var fusion2 = $m0sarl.cadre1SocieteGroup.cadre1Identite.fusionGroup2;

if (fusion1.fusionAutreSociete) {
formFieldsPers2['entrepriseLiee_sirenPrime']                                                 = fusion2.entrepriseLieeSiren.split(' ').join('');
formFieldsPers2['entrepriseLiee_greffeImmatriculationPrime']                                 = fusion2.entrepriseLieeGreffe;
formFieldsPers2['entrepriseLiee_entreprisePM_denominationPrime']                             = fusion2.entrepriseLieeEntreprisePMDenomination + ' ' + fusion2.entreperiseLieeEntreprisePMFormeJuridique;
formFieldsPers2['entrepriseLiee_adresse_voie_Prime']                                         = (fusion2.adresseFusionScission.numeroAdresseFusion != null ? fusion2.adresseFusionScission.numeroAdresseFusion : '')
																							 + ' ' + (fusion2.adresseFusionScission.indiceAdresseFusion != null ? fusion2.adresseFusionScission.indiceAdresseFusion : '')
																							 + ' ' + (fusion2.adresseFusionScission.typeAdresseFusion != null ? fusion2.adresseFusionScission.typeAdresseFusion : '')
																							 + ' ' + fusion2.adresseFusionScission.voieAdresseFusion
																							 + ' ' + (fusion2.adresseFusionScission.complementAdresseFusion != null ? fusion2.adresseFusionScission.complementAdresseFusion : '')
																							 + ' ' + (fusion2.adresseFusionScission.distributionSpecialeAdresseFusion != null ? fusion2.adresseFusionScission.distributionSpecialeAdresseFusion : '');
formFieldsPers2['entrepriseLiee_adresse_commune_Prime']                                      = fusion2.adresseFusionScission.codePostalAdresseFusion + ' ' + fusion2.adresseFusionScission.communeAdresseFusion;
}

// M0 PRIME SARL 3

var formFieldsPers3 = {}
formFieldsPers3['numeroIntercalaireM0Prime']                                            = "03" 
formFieldsPers3['entreprise_denominationPrime']                                         = societe.entrepriseDenomination;
formFieldsPers3['entreprise_formeJuridiquePrime']                                       = Value('id').of(societe.entrepriseFormeJuridique).eq('sarl') ? (societe.entrepriseAssocieUnique ? "EURL" : "SARL") : (societe.entrepriseAssocieUnique ? "SELARLU" : "SELARL"); 

// Fusion - scission société 3

var fusion3 = $m0sarl.cadre1SocieteGroup.cadre1Identite.fusionGroup3;

if (fusion2.fusionAutreSociete2) {
formFieldsPers3['entrepriseLiee_sirenPrime']                                                 = fusion3.entrepriseLieeSiren.split(' ').join('');
formFieldsPers3['entrepriseLiee_greffeImmatriculationPrime']                                 = fusion3.entrepriseLieeGreffe;
formFieldsPers3['entrepriseLiee_entreprisePM_denominationPrime']                             = fusion3.entrepriseLieeEntreprisePMDenomination + ' ' + fusion3.entreperiseLieeEntreprisePMFormeJuridique;
formFieldsPers3['entrepriseLiee_adresse_voie_Prime']                                         = (fusion3.adresseFusionScission.numeroAdresseFusion != null ? fusion3.adresseFusionScission.numeroAdresseFusion : '')
																							 + ' ' + (fusion3.adresseFusionScission.indiceAdresseFusion != null ? fusion3.adresseFusionScission.indiceAdresseFusion : '')
																							 + ' ' + (fusion3.adresseFusionScission.typeAdresseFusion != null ? fusion3.adresseFusionScission.typeAdresseFusion : '')
																							 + ' ' + fusion3.adresseFusionScission.voieAdresseFusion
																							 + ' ' + (fusion3.adresseFusionScission.complementAdresseFusion != null ? fusion3.adresseFusionScission.complementAdresseFusion : '')
																							 + ' ' + (fusion3.adresseFusionScission.distributionSpecialeAdresseFusion != null ? fusion3.adresseFusionScission.distributionSpecialeAdresseFusion : '');
formFieldsPers3['entrepriseLiee_adresse_commune_Prime']                                      = fusion3.adresseFusionScission.codePostalAdresseFusion + ' ' + fusion3.adresseFusionScission.communeAdresseFusion;
}


// TNS GERANT 1 

//cadre 1
var formFieldsPers4 = {}

formFieldsPers4['formulaireDependanceMOSARL_TNS']                                   = true;
formFieldsPers4['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers4['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers4['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers4['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers4['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers4['formulaireDependanceTNS_TNS']                                      = false;
   
//cadre 2  
                                                                                                   
formFieldsPers4['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 
var socialDirigeant1 = $m0sarl.cadre7DeclarationSocialeGroupGerant1.cadre7DeclarationSociale;
var socialConjoint1 = $m0sarl.cadre8DeclarationSocialeGroupConjointAssocie1.cadre7DeclarationSociale;
                                                                                                
formFieldsPers4['personneLieePPNomNaissance_TNS']                                   = dirigeant1.personneLieePPNomNaissanceGerant;
formFieldsPers4['personneLieePPNomUsage_TNS']                                       = dirigeant1.personneLieePPNomUsageGerant != null ? dirigeant1.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < dirigeant1.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant1.personneLieePPPrenomGerant[i]);}                            
formFieldsPers4['personneLieePPPrenom_TNS']                                         = prenoms.toString();
formFieldsPers4['personneLieeQualite_TNS']                                          = "Gérant";

var nirDeclarant = socialDirigeant1.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers4['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers4['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}

if (Value('id').of(dirigeant1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur')) {
formFieldsPers4['voletSocialConjointCouvertAssuranceMaladieOUI_TNS']                = conjoint1.voletSocialConjointCouvertAssuranceMaladieOui ? true : false;
formFieldsPers4['voletSocialConjointCouvertAssuranceMaladieNON_TNS']                = conjoint1.voletSocialConjointCouvertAssuranceMaladieOui ? false : true;
}
formFieldsPers4['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers4['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers4['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers4['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers4['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers4['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant1.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS != null ? socialDirigeant1.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
formFieldsPers4['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant1.voletSocialActiviteExerceeAnterieurementLibelleTNS !=null ? socialDirigeant1.voletSocialActiviteExerceeAnterieurementLibelleTNS : '';
formFieldsPers4['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant1.voletSocialActiviteExerceeAnterieurementDepartementTNS != null ? socialDirigeant1.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId() : '';
formFieldsPers4['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant1.voletSocialActiviteExerceeAnterieurementCommuneTNS != null ? socialDirigeant1.voletSocialActiviteExerceeAnterieurementCommuneTNS : '';
if(socialDirigeant1.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant1.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers4['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
formFieldsPers4['voletSocialActiviteSimultaneeOUI_TNS']                             = socialDirigeant1.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers4['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant1.voletSocialActiviteSimultaneeOUINON ? false : true;
formFieldsPers4['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers4['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers4['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers4['voletSocialActiviteSimultaneeAutre_TNS']                           = Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers4['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant1.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
formFieldsPers4['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant1.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers4['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant1.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers4['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant1.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers4['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant1.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

//cadre 4    

formFieldsPers4['personneLieeConjointStatutAssocie_TNS']                            = Value('id').of(dirigeant1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie') ? true : false;
formFieldsPers4['personneLieeConjointStatutCollaborateur_TNS']                      = Value('id').of(dirigeant1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') ? true : false;

if (Value('id').of(dirigeant1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur')) {
var nirSARLConjoint = conjoint1.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(nirSARLConjoint != null) {
    nirSARLConjoint = nirSARLConjoint.replace(/ /g, "");
    formFieldsPers4['voletSocialConjointNumeroSecuriteSocial_TNS']       = nirSARLConjoint.substring(0, 13);
    formFieldsPers4['voletSocialConjointNumeroSecuriteSocialCle_TNS']    = nirSARLConjoint.substring(13, 15);
}
} else if (Value('id').of(dirigeant1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) {
var nirSARLConjoint = socialConjoint1.voletSocialNumeroSecuriteSocialTNS;
if(nirSARLConjoint != null) {
    nirSARLConjoint = nirSARLConjoint.replace(/ /g, "");
    formFieldsPers4['voletSocialConjointNumeroSecuriteSocial_TNS']       = nirSARLConjoint.substring(0, 13);
    formFieldsPers4['voletSocialConjointNumeroSecuriteSocialCle_TNS']    = nirSARLConjoint.substring(13, 15);
}
}

if (Value('id').of(dirigeant1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) {
formFieldsPers4['personneLieePPNomNaissanceConjointAssocie_TNS']                     = conjoint1.personneLieePPNomNaissanceConjointPacse;
formFieldsPers4['personneLieePPNomUsageConjointAssocie_TNS']                         = conjoint1.personneLieePPNomUsageConjointPacse != null ? conjoint1.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < conjoint1.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(conjoint1.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers4['personneLieePPPrenomConjointAssocie_TNS']                                                      = prenoms.toString();
if(conjoint1.personneLieePPDateNaissanceConjointPacse != null) {
	var dateTmp = new Date(parseInt(conjoint1.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers4['personneLieePPDateNaissanceConjointAssocie_TNS'] = date;
}
formFieldsPers4['personneLieePPLieuNaissanceCommuneConjointAssocie_TNS']            = conjoint1.personneLieePPLieuNaissanceCommnuneConjointPacse != null ? (conjoint1.personneLieePPLieuNaissanceCommnuneConjointPacse + ' ' + "(" + '' + conjoint1.personneLieePPDeptNaissanceConjointPacse.getId() + '' + ")") : (conjoint1.personneLieePersonnePhysiqueLieuNaissanceVilleConjointPacse + ' / ' + conjoint1.personneLieePPPaysNaissanceConjointPacse);
formFieldsPers4['personneLieePPNationaliteConjointAssocie_TNS']                     = conjoint1.personneLieePPNationaliteConjointPacse;
}

if (societe.optionME) {
formFieldsPers4['periodiciteVersementCotisationMensuelle_TNS']                      = Value('id').of(socialDirigeant1.periodiciteVersementCotisationMensuelleTrimestrielle).eq('periodiciteVersementCotisationMensuelleTNS')  ? true : false;
formFieldsPers4['periodiciteVersementCotisationTrimestrielle_TNS']                  = Value('id').of(socialDirigeant1.periodiciteVersementCotisationMensuelleTrimestrielle).eq('periodiciteVersementCotisationTrimestrielleTNS')  ? true : false;
}

//cadre 5

formFieldsPers4['renseignementsComplementairesObservations_TNS']                    = '';
                                                                                 
//Cadre 6  

formFieldsPers4['renseignementsComplementairesLeDeclarant_TNS']                     = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentantLegal') ? true : false;
formFieldsPers4['renseignementsComplementairesLeMandataire_TNS']                    = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers4['formaliteSignataireNomPrenomDenomination_TNS']						= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers4['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + signataire.adresseMandataire.nomVoieMandataire
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');
formFieldsPers4['formaliteSignataireAdresseComplement_TNS']                         = signataire.adresseMandataire.codePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
formFieldsPers4['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
formFieldsPers4['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers4['formaliteSignatureDate_TNS'] = date;
}

// TNS GERANT 2 

//cadre 1
var formFieldsPers5 = {}

formFieldsPers5['formulaireDependanceMOSARL_TNS']                                   = true;
formFieldsPers5['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers5['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers5['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers5['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers5['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers5['formulaireDependanceTNS_TNS']                                      = false;
   
//cadre 2  
                                                                                                   
formFieldsPers5['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 
var socialDirigeant2 = $m0sarl.cadre10DeclarationSocialeGroupGerant2.cadre7DeclarationSociale;
var socialConjoint2 = $m0sarl.cadre11DeclarationSocialeGroupConjointAssocie2.cadre7DeclarationSociale;
                                                                                                 
formFieldsPers5['personneLieePPNomNaissance_TNS']                                   = dirigeant2.personneLieePPNomNaissanceGerant;
formFieldsPers5['personneLieePPNomUsage_TNS']                                       = dirigeant2.personneLieePPNomUsageGerant != null ? dirigeant2.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < dirigeant2.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant2.personneLieePPPrenomGerant[i]);}                            
formFieldsPers5['personneLieePPPrenom_TNS']                                         = prenoms.toString();
formFieldsPers5['personneLieeQualite_TNS']                                          = "Gérant";

var nirDeclarant = socialDirigeant2.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers5['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers5['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}

if (Value('id').of(dirigeant2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur')) {
formFieldsPers5['voletSocialConjointCouvertAssuranceMaladieOUI_TNS']                = conjoint2.voletSocialConjointCouvertAssuranceMaladieOui ? true : false;
formFieldsPers5['voletSocialConjointCouvertAssuranceMaladieNON_TNS']                = conjoint2.voletSocialConjointCouvertAssuranceMaladieOui ? false : true;
}
formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant2.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS != null ? socialDirigeant2.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
formFieldsPers5['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant2.voletSocialActiviteExerceeAnterieurementLibelleTNS !=null ? socialDirigeant2.voletSocialActiviteExerceeAnterieurementLibelleTNS : '';
formFieldsPers5['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant2.voletSocialActiviteExerceeAnterieurementDepartementTNS != null ? socialDirigeant2.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId() : '';
formFieldsPers5['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant2.voletSocialActiviteExerceeAnterieurementCommuneTNS != null ? socialDirigeant2.voletSocialActiviteExerceeAnterieurementCommuneTNS : '';
if(socialDirigeant2.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant2.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers5['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
formFieldsPers5['voletSocialActiviteSimultaneeOUI_TNS']                             = socialDirigeant2.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers5['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant2.voletSocialActiviteSimultaneeOUINON ? false : true;
formFieldsPers5['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers5['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers5['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers5['voletSocialActiviteSimultaneeAutre_TNS']                           = Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers5['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant2.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
formFieldsPers5['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant2.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers5['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant2.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers5['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant2.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers5['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant2.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

//cadre 4    

formFieldsPers5['personneLieeConjointStatutAssocie_TNS']                            = Value('id').of(dirigeant2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie') ? true : false;
formFieldsPers5['personneLieeConjointStatutCollaborateur_TNS']                      = Value('id').of(dirigeant2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') ? true : false;
                             
if (Value('id').of(dirigeant2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur')) {
var nirSARLConjoint = conjoint2.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(nirSARLConjoint != null) {
    nirSARLConjoint = nirSARLConjoint.replace(/ /g, "");
    formFieldsPers5['voletSocialConjointNumeroSecuriteSocial_TNS']       = nirSARLConjoint.substring(0, 13);
    formFieldsPers5['voletSocialConjointNumeroSecuriteSocialCle_TNS']    = nirSARLConjoint.substring(13, 15);
}
} else if (Value('id').of(dirigeant2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) {
var nirSARLConjoint = socialConjoint2.voletSocialNumeroSecuriteSocialTNS;
if(nirSARLConjoint != null) {
    nirSARLConjoint = nirSARLConjoint.replace(/ /g, "");
    formFieldsPers5['voletSocialConjointNumeroSecuriteSocial_TNS']       = nirSARLConjoint.substring(0, 13);
    formFieldsPers5['voletSocialConjointNumeroSecuriteSocialCle_TNS']    = nirSARLConjoint.substring(13, 15);
}
}

if (Value('id').of(dirigeant2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) {
formFieldsPers5['personneLieePPNomNaissanceConjointAssocie_TNS']                     = conjoint2.personneLieePPNomNaissanceConjointPacse;
formFieldsPers5['personneLieePPNomUsageConjointAssocie_TNS']                         = conjoint2.personneLieePPNomUsageConjointPacse != null ? conjoint2.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < conjoint2.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(conjoint2.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers5['personneLieePPPrenomConjointAssocie_TNS']                                                      = prenoms.toString();
if(conjoint2.personneLieePPDateNaissanceConjointPacse != null) {
	var dateTmp = new Date(parseInt(conjoint2.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers5['personneLieePPDateNaissanceConjointAssocie_TNS'] = date;
}
formFieldsPers5['personneLieePPLieuNaissanceCommuneConjointAssocie_TNS']            = conjoint2.personneLieePPLieuNaissanceCommnuneConjointPacse != null ? (conjoint2.personneLieePPLieuNaissanceCommnuneConjointPacse + ' ' + "(" + '' + conjoint2.personneLieePPDeptNaissanceConjointPacse.getId() + '' + ")") : (conjoint2.personneLieePersonnePhysiqueLieuNaissanceVilleConjointPacse + ' / ' + conjoint2.personneLieePPPaysNaissanceConjointPacse);
formFieldsPers5['personneLieePPNationaliteConjointAssocie_TNS']                     = conjoint2.personneLieePPNationaliteConjointPacse;
}

//cadre 5

formFieldsPers5['renseignementsComplementairesObservations_TNS']                    = '';
                                                                                 
//Cadre 6  

formFieldsPers5['renseignementsComplementairesLeDeclarant_TNS']                     = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentantLegal') ? true : false;
formFieldsPers5['renseignementsComplementairesLeMandataire_TNS']                    = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers5['formaliteSignataireNomPrenomDenomination_TNS']						= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers5['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + signataire.adresseMandataire.nomVoieMandataire
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');
formFieldsPers5['formaliteSignataireAdresseComplement_TNS']                         = signataire.adresseMandataire.codePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
formFieldsPers5['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
formFieldsPers5['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers5['formaliteSignatureDate_TNS'] = date;
}


// TNS GERANT 3

//cadre 1
var formFieldsPers6 = {}

formFieldsPers6['formulaireDependanceMOSARL_TNS']                                   = true;
formFieldsPers6['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers6['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers6['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers6['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers6['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers6['formulaireDependanceTNS_TNS']                                      = false;
   
//cadre 2  
                                                                                                   
formFieldsPers6['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 
var socialDirigeant3 = $m0sarl.cadre13DeclarationSocialeGroupGerant3.cadre7DeclarationSociale;
var socialConjoint3 = $m0sarl.cadre14DeclarationSocialeGroupConjointAssocie3.cadre7DeclarationSociale;
                                                                                                 
formFieldsPers6['personneLieePPNomNaissance_TNS']                                   = dirigeant3.personneLieePPNomNaissanceGerant;
formFieldsPers6['personneLieePPNomUsage_TNS']                                       = dirigeant3.personneLieePPNomUsageGerant != null ? dirigeant3.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < dirigeant3.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant3.personneLieePPPrenomGerant[i]);}                            
formFieldsPers6['personneLieePPPrenom_TNS']                                         = prenoms.toString();
formFieldsPers6['personneLieeQualite_TNS']                                          = "Gérant";

var nirDeclarant = socialDirigeant3.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers6['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers6['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}

if (Value('id').of(dirigeant3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur')) {
formFieldsPers6['voletSocialConjointCouvertAssuranceMaladieOUI_TNS']                = conjoint3.voletSocialConjointCouvertAssuranceMaladieOui ? true : false;
formFieldsPers6['voletSocialConjointCouvertAssuranceMaladieNON_TNS']                = conjoint3.voletSocialConjointCouvertAssuranceMaladieOui ? false : true;
}
formFieldsPers6['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers6['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers6['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers6['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers6['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers6['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant3.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS != null ? socialDirigeant3.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
formFieldsPers6['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant3.voletSocialActiviteExerceeAnterieurementLibelleTNS !=null ? socialDirigeant3.voletSocialActiviteExerceeAnterieurementLibelleTNS : '';
formFieldsPers6['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant3.voletSocialActiviteExerceeAnterieurementDepartementTNS != null ? socialDirigeant3.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId() : '';
formFieldsPers6['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant3.voletSocialActiviteExerceeAnterieurementCommuneTNS != null ? socialDirigeant3.voletSocialActiviteExerceeAnterieurementCommuneTNS : '';
if(socialDirigeant3.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant3.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers6['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
formFieldsPers6['voletSocialActiviteSimultaneeOUI_TNS']                             = socialDirigeant3.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers6['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant3.voletSocialActiviteSimultaneeOUINON ? false : true;
formFieldsPers6['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers6['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers6['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers6['voletSocialActiviteSimultaneeAutre_TNS']                           = Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers6['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant3.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
formFieldsPers6['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant3.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers6['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant3.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers6['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant3.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers6['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant3.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

//cadre 4    

formFieldsPers6['personneLieeConjointStatutAssocie_TNS']                            = Value('id').of(dirigeant3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie') ? true : false;
formFieldsPers6['personneLieeConjointStatutCollaborateur_TNS']                      = Value('id').of(dirigeant3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') ? true : false;
                             
if (Value('id').of(dirigeant3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur')) {
var nirSARLConjoint = conjoint3.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(nirSARLConjoint != null) {
    nirSARLConjoint = nirSARLConjoint.replace(/ /g, "");
    formFieldsPers6['voletSocialConjointNumeroSecuriteSocial_TNS']       = nirSARLConjoint.substring(0, 13);
    formFieldsPers6['voletSocialConjointNumeroSecuriteSocialCle_TNS']    = nirSARLConjoint.substring(13, 15);
}
} else if (Value('id').of(dirigeant3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) {
var nirSARLConjoint = socialConjoint3.voletSocialNumeroSecuriteSocialTNS;
if(nirSARLConjoint != null) {
    nirSARLConjoint = nirSARLConjoint.replace(/ /g, "");
    formFieldsPers6['voletSocialConjointNumeroSecuriteSocial_TNS']       = nirSARLConjoint.substring(0, 13);
    formFieldsPers6['voletSocialConjointNumeroSecuriteSocialCle_TNS']    = nirSARLConjoint.substring(13, 15);
}
}

if (Value('id').of(dirigeant3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) {
formFieldsPers6['personneLieePPNomNaissanceConjointAssocie_TNS']                     = conjoint3.personneLieePPNomNaissanceConjointPacse;
formFieldsPers6['personneLieePPNomUsageConjointAssocie_TNS']                         = conjoint3.personneLieePPNomUsageConjointPacse != null ? conjoint3.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < conjoint3.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(conjoint3.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers6['personneLieePPPrenomConjointAssocie_TNS']                                                      = prenoms.toString();
if(conjoint3.personneLieePPDateNaissanceConjointPacse != null) {
	var dateTmp = new Date(parseInt(conjoint3.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers6['personneLieePPDateNaissanceConjointAssocie_TNS'] = date;
}
formFieldsPers6['personneLieePPLieuNaissanceCommuneConjointAssocie_TNS']            = conjoint3.personneLieePPLieuNaissanceCommnuneConjointPacse != null ? (conjoint3.personneLieePPLieuNaissanceCommnuneConjointPacse + ' ' + "(" + '' + conjoint3.personneLieePPDeptNaissanceConjointPacse.getId() + '' + ")") : (conjoint3.personneLieePersonnePhysiqueLieuNaissanceVilleConjointPacse + ' / ' + conjoint3.personneLieePPPaysNaissanceConjointPacse);
formFieldsPers6['personneLieePPNationaliteConjointAssocie_TNS']                     = conjoint3.personneLieePPNationaliteConjointPacse;
}

//cadre 5

formFieldsPers6['renseignementsComplementairesObservations_TNS']                    = '';
                                                                                 
//Cadre 6  

formFieldsPers6['renseignementsComplementairesLeDeclarant_TNS']                     = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentantLegal') ? true : false;
formFieldsPers6['renseignementsComplementairesLeMandataire_TNS']                    = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers6['formaliteSignataireNomPrenomDenomination_TNS']						= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers6['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + signataire.adresseMandataire.nomVoieMandataire
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');
formFieldsPers6['formaliteSignataireAdresseComplement_TNS']                         = signataire.adresseMandataire.codePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
formFieldsPers6['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
formFieldsPers6['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers6['formaliteSignatureDate_TNS'] = date;
}

// TNS GERANT 4 

//cadre 1
var formFieldsPers7 = {}

formFieldsPers7['formulaireDependanceMOSARL_TNS']                                   = true;
formFieldsPers7['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers7['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers7['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers7['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers7['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers7['formulaireDependanceTNS_TNS']                                      = false;
   
//cadre 2  
                                                                                                   
formFieldsPers7['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 
var socialDirigeant4 = $m0sarl.cadre16DeclarationSocialeGroupGerant4.cadre7DeclarationSociale;
var socialConjoint4 = $m0sarl.cadre17DeclarationSocialeGroupConjointAssocie4.cadre7DeclarationSociale;
                                                                                                 
formFieldsPers7['personneLieePPNomNaissance_TNS']                                   = dirigeant4.personneLieePPNomNaissanceGerant;
formFieldsPers7['personneLieePPNomUsage_TNS']                                       = dirigeant4.personneLieePPNomUsageGerant != null ? dirigeant4.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < dirigeant4.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant4.personneLieePPPrenomGerant[i]);}                            
formFieldsPers7['personneLieePPPrenom_TNS']                                         = prenoms.toString();
formFieldsPers7['personneLieeQualite_TNS']                                          = "Gérant";

var nirDeclarant = socialDirigeant4.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers7['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers7['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}

if (Value('id').of(dirigeant4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur')) {
formFieldsPers7['voletSocialConjointCouvertAssuranceMaladieOUI_TNS']                = conjoint4.voletSocialConjointCouvertAssuranceMaladieOui ? true : false;
formFieldsPers7['voletSocialConjointCouvertAssuranceMaladieNON_TNS']                = conjoint4.voletSocialConjointCouvertAssuranceMaladieOui ? false : true;
}
formFieldsPers7['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers7['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers7['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers7['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers7['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers7['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant4.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS != null ? socialDirigeant4.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
formFieldsPers7['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant4.voletSocialActiviteExerceeAnterieurementLibelleTNS != null ? socialDirigeant4.voletSocialActiviteExerceeAnterieurementLibelleTNS : '';
formFieldsPers7['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant4.voletSocialActiviteExerceeAnterieurementDepartementTNS != null ? socialDirigeant4.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId() : '';
formFieldsPers7['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant4.voletSocialActiviteExerceeAnterieurementCommuneTNS != null ? socialDirigeant4.voletSocialActiviteExerceeAnterieurementCommuneTNS : '';
if(socialDirigeant4.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant4.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers7['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
formFieldsPers7['voletSocialActiviteSimultaneeOUI_TNS']                             = socialDirigeant4.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers7['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant4.voletSocialActiviteSimultaneeOUINON ? false : true;
formFieldsPers7['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers7['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers7['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers7['voletSocialActiviteSimultaneeAutre_TNS']                           = Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers7['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant4.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
formFieldsPers7['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant4.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers7['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant4.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers7['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant4.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers7['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant4.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

//cadre 4    

formFieldsPers7['personneLieeConjointStatutAssocie_TNS']                            = Value('id').of(dirigeant4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie') ? true : false;
formFieldsPers7['personneLieeConjointStatutCollaborateur_TNS']                      = Value('id').of(dirigeant4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') ? true : false;
                             
if (Value('id').of(dirigeant4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur')) {
var nirSARLConjoint = conjoint4.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(nirSARLConjoint != null) {
    nirSARLConjoint = nirSARLConjoint.replace(/ /g, "");
    formFieldsPers7['voletSocialConjointNumeroSecuriteSocial_TNS']       = nirSARLConjoint.substring(0, 13);
    formFieldsPers7['voletSocialConjointNumeroSecuriteSocialCle_TNS']    = nirSARLConjoint.substring(13, 15);
}
} else if (Value('id').of(dirigeant4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) {
var nirSARLConjoint = socialConjoint4.voletSocialNumeroSecuriteSocialTNS;
if(nirSARLConjoint != null) {
    nirSARLConjoint = nirSARLConjoint.replace(/ /g, "");
    formFieldsPers7['voletSocialConjointNumeroSecuriteSocial_TNS']       = nirSARLConjoint.substring(0, 13);
    formFieldsPers7['voletSocialConjointNumeroSecuriteSocialCle_TNS']    = nirSARLConjoint.substring(13, 15);
}
}

if (Value('id').of(dirigeant4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) {
formFieldsPers7['personneLieePPNomNaissanceConjointAssocie_TNS']                     = conjoint4.personneLieePPNomNaissanceConjointPacse;
formFieldsPers7['personneLieePPNomUsageConjointAssocie_TNS']                         = conjoint4.personneLieePPNomUsageConjointPacse != null ? conjoint4.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < conjoint4.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(conjoint4.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers7['personneLieePPPrenomConjointAssocie_TNS']                                                      = prenoms.toString();
if(conjoint4.personneLieePPDateNaissanceConjointPacse != null) {
	var dateTmp = new Date(parseInt(conjoint4.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers7['personneLieePPDateNaissanceConjointAssocie_TNS'] = date;
}
formFieldsPers7['personneLieePPLieuNaissanceCommuneConjointAssocie_TNS']            = conjoint4.personneLieePPLieuNaissanceCommnuneConjointPacse != null ? (conjoint4.personneLieePPLieuNaissanceCommnuneConjointPacse + ' ' + "(" + '' + conjoint4.personneLieePPDeptNaissanceConjointPacse.getId() + '' + ")") : (conjoint4.personneLieePersonnePhysiqueLieuNaissanceVilleConjointPacse + ' / ' + conjoint4.personneLieePPPaysNaissanceConjointPacse);
formFieldsPers7['personneLieePPNationaliteConjointAssocie_TNS']                     = conjoint4.personneLieePPNationaliteConjointPacse;
}

//cadre 5

formFieldsPers7['renseignementsComplementairesObservations_TNS']                    = '';
                                                                                 
//Cadre 6  

formFieldsPers7['renseignementsComplementairesLeDeclarant_TNS']                     = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentantLegal') ? true : false;
formFieldsPers7['renseignementsComplementairesLeMandataire_TNS']                    = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers7['formaliteSignataireNomPrenomDenomination_TNS']						= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers7['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + signataire.adresseMandataire.nomVoieMandataire
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');
formFieldsPers7['formaliteSignataireAdresseComplement_TNS']                         = signataire.adresseMandataire.codePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
formFieldsPers7['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
formFieldsPers7['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers7['formaliteSignatureDate_TNS'] = date;
}

// TNS CONJOINT ASSOCIE 1 

//cadre 1
var formFieldsPers8 = {}

formFieldsPers8['formulaireDependanceMOSARL_TNS']                                   = true;
formFieldsPers8['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers8['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers8['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers8['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers8['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers8['formulaireDependanceTNS_TNS']                                      = false;
                                                                                                                
//cadre 2  
                                                                                                   
formFieldsPers8['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 

formFieldsPers8['personneLieePPNomNaissance_TNS']                                   = conjoint1.personneLieePPNomNaissanceConjointPacse;
formFieldsPers8['personneLieePPNomUsage_TNS']                                       = conjoint1.personneLieePPNomUsageConjointPacse != null ? conjoint1.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < conjoint1.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(conjoint1.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers8['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
formFieldsPers8['personneLieeQualite_TNS']                                          = "Conjoint associé";

var nirConjoint = socialConjoint1.voletSocialNumeroSecuriteSocialTNS;
if(nirConjoint != null) {
    nirConjoint = nirConjoint.replace(/ /g, "");
    formFieldsPers8['voletSocialNumeroSecuriteSocial_TNS']    = nirConjoint.substring(0, 13);
    formFieldsPers8['voletSocialNumeroSecuriteSocialCle_TNS'] = nirConjoint.substring(13, 15);
}

formFieldsPers8['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialConjoint1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers8['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialConjoint1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers8['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialConjoint1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers8['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialConjoint1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers8['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialConjoint1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers8['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialConjoint1.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS != null ? socialConjoint1.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
formFieldsPers8['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialConjoint1.voletSocialActiviteExerceeAnterieurementLibelleTNS != null ? socialConjoint1.voletSocialActiviteExerceeAnterieurementLibelleTNS : '';
formFieldsPers8['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialConjoint1.voletSocialActiviteExerceeAnterieurementDepartementTNS != null ? socialConjoint1.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId() : '';
formFieldsPers8['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialConjoint1.voletSocialActiviteExerceeAnterieurementCommuneTNS != null ? socialConjoint1.voletSocialActiviteExerceeAnterieurementCommuneTNS : '';
if(socialConjoint1.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialConjoint1.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers8['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
formFieldsPers8['voletSocialActiviteSimultaneeOUI_TNS']                             = socialConjoint1.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers8['voletSocialActiviteSimultaneeNON_TNS']                             = socialConjoint1.voletSocialActiviteSimultaneeOUINON ? false : true;
formFieldsPers8['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialConjoint1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers8['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialConjoint1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers8['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialConjoint1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers8['voletSocialActiviteSimultaneeAutre_TNS']                           = Value('id').of(socialConjoint1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers8['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = Value('id').of(socialConjoint1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialConjoint1.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
formFieldsPers8['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialConjoint1.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers8['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialConjoint1.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers8['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialConjoint1.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers8['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialConjoint1.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

//cadre 5

formFieldsPers8['renseignementsComplementairesObservations_TNS']                    = '';
                                                                                 
//Cadre 6  

formFieldsPers8['renseignementsComplementairesLeDeclarant_TNS']                     = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentantLegal') ? true : false;
formFieldsPers8['renseignementsComplementairesLeMandataire_TNS']                    = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers8['formaliteSignataireNomPrenomDenomination_TNS']						= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers8['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + signataire.adresseMandataire.nomVoieMandataire
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');
formFieldsPers8['formaliteSignataireAdresseComplement_TNS']                         = signataire.adresseMandataire.codePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
formFieldsPers8['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
formFieldsPers8['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers8['formaliteSignatureDate_TNS'] = date;
}

// TNS CONJOINT ASSOCIE 2 

//cadre 1
var formFieldsPers9 = {}

formFieldsPers9['formulaireDependanceMOSARL_TNS']                                   = true;
formFieldsPers9['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers9['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers9['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers9['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers9['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers9['formulaireDependanceTNS_TNS']                                      = false;
                                                                                                                
//cadre 2  
                                                                                                   
formFieldsPers9['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;

//cadre 3 

formFieldsPers9['personneLieePPNomNaissance_TNS']                                   = conjoint2.personneLieePPNomNaissanceConjointPacse;
formFieldsPers9['personneLieePPNomUsage_TNS']                                       = conjoint2.personneLieePPNomUsageConjointPacse != null ? conjoint2.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < conjoint2.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(conjoint2.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers9['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
formFieldsPers9['personneLieeQualite_TNS']                                          = "Conjoint associé";

var nirConjoint = socialConjoint2.voletSocialNumeroSecuriteSocialTNS;
if(nirConjoint != null) {
    nirConjoint = nirConjoint.replace(/ /g, "");
    formFieldsPers9['voletSocialNumeroSecuriteSocial_TNS']    = nirConjoint.substring(0, 13);
    formFieldsPers9['voletSocialNumeroSecuriteSocialCle_TNS'] = nirConjoint.substring(13, 15);
}

formFieldsPers9['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialConjoint2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers9['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialConjoint2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers9['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialConjoint2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers9['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialConjoint2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers9['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialConjoint2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers9['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialConjoint2.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS != null ? socialConjoint2.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
formFieldsPers9['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialConjoint2.voletSocialActiviteExerceeAnterieurementLibelleTNS != null ? socialConjoint2.voletSocialActiviteExerceeAnterieurementLibelleTNS : '';
formFieldsPers9['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialConjoint2.voletSocialActiviteExerceeAnterieurementDepartementTNS != null ? socialConjoint2.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId() : '';
formFieldsPers9['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialConjoint2.voletSocialActiviteExerceeAnterieurementCommuneTNS != null ? socialConjoint2.voletSocialActiviteExerceeAnterieurementCommuneTNS : '';
if(socialConjoint2.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialConjoint2.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers9['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
formFieldsPers9['voletSocialActiviteSimultaneeOUI_TNS']                             = socialConjoint2.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers9['voletSocialActiviteSimultaneeNON_TNS']                             = socialConjoint2.voletSocialActiviteSimultaneeOUINON ? false : true;
formFieldsPers9['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialConjoint2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers9['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialConjoint2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers9['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialConjoint2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers9['voletSocialActiviteSimultaneeAutre_TNS']                           = Value('id').of(socialConjoint2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers9['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = Value('id').of(socialConjoint2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialConjoint2.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
formFieldsPers9['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialConjoint2.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers9['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialConjoint2.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers9['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialConjoint2.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers9['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialConjoint2.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

//cadre 5

formFieldsPers9['renseignementsComplementairesObservations_TNS']                    = '';
                                                                                 
//Cadre 6  

formFieldsPers9['renseignementsComplementairesLeDeclarant_TNS']                     = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentantLegal') ? true : false;
formFieldsPers9['renseignementsComplementairesLeMandataire_TNS']                    = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers9['formaliteSignataireNomPrenomDenomination_TNS']						= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers9['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + signataire.adresseMandataire.nomVoieMandataire
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');
formFieldsPers9['formaliteSignataireAdresseComplement_TNS']                         = signataire.adresseMandataire.codePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
formFieldsPers9['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
formFieldsPers9['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers9['formaliteSignatureDate_TNS'] = date;
}

// TNS CONJOINT ASSOCIE 3 

//cadre 1
var formFieldsPers10 = {}

formFieldsPers10['formulaireDependanceMOSARL_TNS']                                   = true;
formFieldsPers10['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers10['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers10['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers10['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers10['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers10['formulaireDependanceTNS_TNS']                                      = false;
                                                                                                                
//cadre 2  
                                                                                                   
formFieldsPers10['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 

formFieldsPers10['personneLieePPNomNaissance_TNS']                                   = conjoint3.personneLieePPNomNaissanceConjointPacse;
formFieldsPers10['personneLieePPNomUsage_TNS']                                       = conjoint3.personneLieePPNomUsageConjointPacse != null ? conjoint3.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < conjoint3.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(conjoint3.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers10['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
formFieldsPers10['personneLieeQualite_TNS']                                          = "Conjoint associé";

var nirConjoint = socialConjoint3.voletSocialNumeroSecuriteSocialTNS;
if(nirConjoint != null) {
    nirConjoint = nirConjoint.replace(/ /g, "");
    formFieldsPers10['voletSocialNumeroSecuriteSocial_TNS']    = nirConjoint.substring(0, 13);
    formFieldsPers10['voletSocialNumeroSecuriteSocialCle_TNS'] = nirConjoint.substring(13, 15);
}

formFieldsPers10['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialConjoint3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers10['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialConjoint3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers10['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialConjoint3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers10['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialConjoint3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers10['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialConjoint3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers10['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialConjoint3.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS != null ? socialConjoint3.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
formFieldsPers10['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialConjoint3.voletSocialActiviteExerceeAnterieurementLibelleTNS != null ? socialConjoint3.voletSocialActiviteExerceeAnterieurementLibelleTNS : '';
formFieldsPers10['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialConjoint3.voletSocialActiviteExerceeAnterieurementDepartementTNS != null ? socialConjoint3.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId() : '';
formFieldsPers10['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialConjoint3.voletSocialActiviteExerceeAnterieurementCommuneTNS != null ? socialConjoint3.voletSocialActiviteExerceeAnterieurementCommuneTNS : '';
if(socialConjoint3.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialConjoint3.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers10['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
formFieldsPers10['voletSocialActiviteSimultaneeOUI_TNS']                             = socialConjoint3.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers10['voletSocialActiviteSimultaneeNON_TNS']                             = socialConjoint3.voletSocialActiviteSimultaneeOUINON ? false : true;
formFieldsPers10['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialConjoint3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers10['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialConjoint3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers10['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialConjoint3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers10['voletSocialActiviteSimultaneeAutre_TNS']                           = Value('id').of(socialConjoint3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers10['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = Value('id').of(socialConjoint3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialConjoint3.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
formFieldsPers10['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialConjoint3.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers10['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialConjoint3.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers10['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialConjoint3.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers10['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialConjoint3.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

//cadre 5

formFieldsPers10['renseignementsComplementairesObservations_TNS']                    = '';
                                                                                 
//Cadre 6  

formFieldsPers10['renseignementsComplementairesLeDeclarant_TNS']                     = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentantLegal') ? true : false;
formFieldsPers10['renseignementsComplementairesLeMandataire_TNS']                    = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers10['formaliteSignataireNomPrenomDenomination_TNS']						= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers10['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + signataire.adresseMandataire.nomVoieMandataire
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');
formFieldsPers10['formaliteSignataireAdresseComplement_TNS']                         = signataire.adresseMandataire.codePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
formFieldsPers10['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
formFieldsPers10['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers10['formaliteSignatureDate_TNS'] = date;
}

// TNS CONJOINT ASSOCIE 4

//cadre 1
var formFieldsPers11 = {}

formFieldsPers11['formulaireDependanceMOSARL_TNS']                                   = true;
formFieldsPers11['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers11['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers11['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers11['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers11['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers11['formulaireDependanceTNS_TNS']                                      = false;
                                                                                                                
//cadre 2  
                                                                                                   
formFieldsPers11['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 

formFieldsPers11['personneLieePPNomNaissance_TNS']                                   = conjoint4.personneLieePPNomNaissanceConjointPacse;
formFieldsPers11['personneLieePPNomUsage_TNS']                                       = conjoint4.personneLieePPNomUsageConjointPacse != null ? conjoint4.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < conjoint4.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(conjoint4.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers11['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
formFieldsPers11['personneLieeQualite_TNS']                                          = "Conjoint associé";

var nirConjoint = socialConjoint4.voletSocialNumeroSecuriteSocialTNS;
if(nirConjoint != null) {
    nirConjoint = nirConjoint.replace(/ /g, "");
    formFieldsPers11['voletSocialNumeroSecuriteSocial_TNS']    = nirConjoint.substring(0, 13);
    formFieldsPers11['voletSocialNumeroSecuriteSocialCle_TNS'] = nirConjoint.substring(13, 15);
}

formFieldsPers11['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialConjoint4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers11['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialConjoint4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers11['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialConjoint4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers11['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialConjoint4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers11['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialConjoint4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers11['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialConjoint4.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS != null ? socialConjoint4.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
formFieldsPers11['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialConjoint4.voletSocialActiviteExerceeAnterieurementLibelleTNS != null ? socialConjoint4.voletSocialActiviteExerceeAnterieurementLibelleTNS : '';
formFieldsPers11['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialConjoint4.voletSocialActiviteExerceeAnterieurementDepartementTNS != null ? socialConjoint4.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId() : '';
formFieldsPers11['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialConjoint4.voletSocialActiviteExerceeAnterieurementCommuneTNS != null ? socialConjoint4.voletSocialActiviteExerceeAnterieurementCommuneTNS : '';
if(socialConjoint4.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialConjoint4.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers11['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
formFieldsPers11['voletSocialActiviteSimultaneeOUI_TNS']                             = socialConjoint4.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers11['voletSocialActiviteSimultaneeNON_TNS']                             = socialConjoint4.voletSocialActiviteSimultaneeOUINON ? false : true;
formFieldsPers11['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialConjoint4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers11['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialConjoint4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers11['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialConjoint4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers11['voletSocialActiviteSimultaneeAutre_TNS']                           = Value('id').of(socialConjoint4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers11['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = Value('id').of(socialConjoint4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialConjoint4.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
formFieldsPers11['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialConjoint4.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers11['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialConjoint4.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers11['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialConjoint4.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers11['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialConjoint4.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

//cadre 5

formFieldsPers11['renseignementsComplementairesObservations_TNS']                    = '';
                                                                                 
//Cadre 6  

formFieldsPers11['renseignementsComplementairesLeDeclarant_TNS']                     = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentantLegal') ? true : false;
formFieldsPers11['renseignementsComplementairesLeMandataire_TNS']                    = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers11['formaliteSignataireNomPrenomDenomination_TNS']						= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers11['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + signataire.adresseMandataire.nomVoieMandataire
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');
formFieldsPers11['formaliteSignataireAdresseComplement_TNS']                         = signataire.adresseMandataire.codePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
formFieldsPers11['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
formFieldsPers11['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers11['formaliteSignatureDate_TNS'] = date;
}

// TNS ASSOCIE UNIQUE

//cadre 1
var formFieldsPers12 = {}

formFieldsPers12['formulaireDependanceMOSARL_TNS']                                   = true;
formFieldsPers12['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers12['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers12['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers12['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers12['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers12['formulaireDependanceTNS_TNS']                                      = false;
                                                                                                                
//cadre 2  
                                                                                                   
formFieldsPers12['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 

var socialAssocieUnique = $m0sarl.cadre18DeclarationSocialeGroupAssocieUnique.cadre7DeclarationSociale;
                                                                                                 
formFieldsPers12['personneLieePPNomNaissance_TNS']                                   = socialAssocieUnique.personneLieePPNomNaissanceTNS;
formFieldsPers12['personneLieePPNomUsage_TNS']                                       = socialAssocieUnique.personneLieePPNomUsageTNS != null ? socialAssocieUnique.personneLieePPNomUsageTNS : '';
var prenoms=[];
for ( i = 0; i < socialAssocieUnique.personneLieePPPrenomTNS.size() ; i++ ){prenoms.push(socialAssocieUnique.personneLieePPPrenomTNS[i]);}                            
formFieldsPers12['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
formFieldsPers12['personneLieeQualite_TNS']                                          = "Associé unique";

var nirAssocie = socialAssocieUnique.voletSocialNumeroSecuriteSocialTNS;
if(nirAssocie != null) {
    nirAssocie = nirAssocie.replace(/ /g, "");
    formFieldsPers12['voletSocialNumeroSecuriteSocial_TNS']    = nirAssocie.substring(0, 13);
    formFieldsPers12['voletSocialNumeroSecuriteSocialCle_TNS'] = nirAssocie.substring(13, 15);
}

formFieldsPers12['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialAssocieUnique.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers12['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialAssocieUnique.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers12['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialAssocieUnique.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers12['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialAssocieUnique.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers12['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialAssocieUnique.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers12['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialAssocieUnique.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS != null ? socialAssocieUnique.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
formFieldsPers12['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialAssocieUnique.voletSocialActiviteExerceeAnterieurementLibelleTNS != null ? socialAssocieUnique.voletSocialActiviteExerceeAnterieurementLibelleTNS : '';
formFieldsPers12['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialAssocieUnique.voletSocialActiviteExerceeAnterieurementDepartementTNS != null ? socialAssocieUnique.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId() : '';
formFieldsPers12['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialAssocieUnique.voletSocialActiviteExerceeAnterieurementCommuneTNS != null ? socialAssocieUnique.voletSocialActiviteExerceeAnterieurementCommuneTNS : '';
if(socialAssocieUnique.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialAssocieUnique.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers12['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
formFieldsPers12['voletSocialActiviteSimultaneeOUI_TNS']                             = socialAssocieUnique.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers12['voletSocialActiviteSimultaneeNON_TNS']                             = socialAssocieUnique.voletSocialActiviteSimultaneeOUINON ? false : true;
formFieldsPers12['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialAssocieUnique.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers12['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialAssocieUnique.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers12['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialAssocieUnique.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers12['voletSocialActiviteSimultaneeAutre_TNS']                           = Value('id').of(socialAssocieUnique.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers12['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = Value('id').of(socialAssocieUnique.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialAssocieUnique.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
formFieldsPers12['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialAssocieUnique.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers12['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialAssocieUnique.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers12['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialAssocieUnique.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers12['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialAssocieUnique.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

//cadre 5
formFieldsPers12['renseignementsComplementairesObservations_TNS']                    = '';
                                                                                 
//Cadre 6  

formFieldsPers12['renseignementsComplementairesLeDeclarant_TNS']                     = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteRepresentantLegal') ? true : false;
formFieldsPers12['renseignementsComplementairesLeMandataire_TNS']                    = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers12['formaliteSignataireNomPrenomDenomination_TNS']						= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers12['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + signataire.adresseMandataire.nomVoieMandataire
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');
formFieldsPers12['formaliteSignataireAdresseComplement_TNS']                         = signataire.adresseMandataire.codePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
formFieldsPers12['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
formFieldsPers12['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers12['formaliteSignatureDate_TNS'] = date;
}

// JQPA 1

//cadre 1
var formFieldsPers13 = {}

// Cadre 1
formFieldsPers13['jqpa_intercalaire']                         = true;
formFieldsPers13['jqpa_formulaire']                           = false;

// Cadre 4
formFieldsPers13['jqpa_entreprise_denomination']              = societe.entrepriseDenomination;
formFieldsPers13['jqpa_entreprise_formeJuridique']            = Value('id').of(societe.entrepriseFormeJuridique).eq('sarl') ? (societe.entrepriseAssocieUnique ? "EURL" : "SARL") : (societe.entrepriseAssocieUnique ? "SELARLU" : "SELARL"); 
formFieldsPers13['jqpa_entreprise_siren']                     = '';

 // Cadre 5A
formFieldsPers13['jqpa_pm_activite']                          = jqpa1.jqpaActiviteJqpaPP;

//Cadre 5B
formFieldsPers13['jqpa_aqpaSituationPM_diplome']              = Value('id').of(jqpa1.jqpaSituation).eq('jqpaSituationDiplome') or Value('id').of(jqpa1.jqpaSituation).eq('jqpaSituationExperience') ? true : false;
formFieldsPers13['jqpa_qualitePersonneQualifieePM_representant'] = Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPRepresentantLegal') ? true : false;
formFieldsPers13['jqpa_qualitePersonneQualifieePM_conjoint']  = Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPConjoint') ? true : false;
formFieldsPers13['jqpa_qualitePersonneQualifieePM_salarie']   = Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') ? true : false;
formFieldsPers13['jqpa_qualitePersonneQualifieePM_autre']     = Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre') ? true : false;
formFieldsPers13['jqpa_qualitePersonneQualifieePMAutre']      = jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifieePPChampAutre;

formFieldsPers13['jqpa_identitePMNomNaissance']               = jqpa1.identitePersonneQualifiee.jqpaIdentitePPNomNaissance != null ? jqpa1.identitePersonneQualifiee.jqpaIdentitePPNomNaissance : '';
formFieldsPers13['jqpa_identitePMNomUsage']                   = jqpa1.identitePersonneQualifiee.jqpaIdentitePPNomUsage != null ? jqpa1.identitePersonneQualifiee.jqpaIdentitePPNomUsage : '';
var prenoms=[];
for ( i = 0; i < jqpa1.identitePersonneQualifiee.jqpaIdentitePPPrenom.size() ; i++ ){prenoms.push(jqpa1.identitePersonneQualifiee.jqpaIdentitePPPrenom[i]);}                            
formFieldsPers13['jqpa_identitePMPrenom']            = prenoms.toString();

if (jqpa1.identitePersonneQualifiee.jqpaIdentitePPDateNaissance !== null) {
	var dateTmp = new Date(parseInt(jqpa1.identitePersonneQualifiee.jqpaIdentitePPDateNaissance.getTimeInMillis()));
	var dateNaissance = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	dateNaissance = dateNaissance.concat(pad(month.toString()));
	dateNaissance = dateNaissance.concat(dateTmp.getFullYear().toString());
	formFieldsPers13['jqpa_identitePMDateNaissance']          = dateNaissance;
}
formFieldsPers13['jqpa_identitePMDepartement']                = jqpa1.identitePersonneQualifiee.jqpaIdentitePPDepartement != null ? jqpa1.identitePersonneQualifiee.jqpaIdentitePPDepartement.getId() : '';
formFieldsPers13['jqpa_identitePMLieuNaissancePays']          = (Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') or Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre')) ? ((jqpa1.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune != null ? jqpa1.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune : jqpa1.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceVille) + ' / ' + jqpa1.identitePersonneQualifiee.jqpaIdentitePPLieuNaissancePays) : '';

// Cadre 5C
formFieldsPers13['jqpa_aqpaSituationPM_engagement']           = Value('id').of(jqpa1.jqpaSituation).eq('jqpaSituationRecrutement') ? true : false;


// JQPA 2

//cadre 1
var formFieldsPers14 = {}

// Cadre 1
formFieldsPers14['jqpa_intercalaire']                         = true;
formFieldsPers14['jqpa_formulaire']                           = false;

// Cadre 4
formFieldsPers14['jqpa_entreprise_denomination']              = societe.entrepriseDenomination;
formFieldsPers14['jqpa_entreprise_formeJuridique']            = Value('id').of(societe.entrepriseFormeJuridique).eq('sarl') ? (societe.entrepriseAssocieUnique ? "EURL" : "SARL") : (societe.entrepriseAssocieUnique ? "SELARLU" : "SELARL"); 
formFieldsPers14['jqpa_entreprise_siren']                     = '';

 // Cadre 5A
formFieldsPers14['jqpa_pm_activite']                          = jqpa2.jqpaActiviteJqpaPP;

//Cadre 5B
formFieldsPers14['jqpa_aqpaSituationPM_diplome']              = Value('id').of(jqpa2.jqpaSituation).eq('jqpaSituationDiplome') or Value('id').of(jqpa2.jqpaSituation).eq('jqpaSituationExperience') ? true : false;
formFieldsPers14['jqpa_qualitePersonneQualifieePM_representant'] = Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPRepresentantLegal') ? true : false;
formFieldsPers14['jqpa_qualitePersonneQualifieePM_conjoint']  = Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPConjoint') ? true : false;
formFieldsPers14['jqpa_qualitePersonneQualifieePM_salarie']   = Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') ? true : false;
formFieldsPers14['jqpa_qualitePersonneQualifieePM_autre']     = Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre') ? true : false;
formFieldsPers14['jqpa_qualitePersonneQualifieePMAutre']      = jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifieePPChampAutre;

formFieldsPers14['jqpa_identitePMNomNaissance']               = jqpa2.identitePersonneQualifiee.jqpaIdentitePPNomNaissance != null ? jqpa2.identitePersonneQualifiee.jqpaIdentitePPNomNaissance : '';
formFieldsPers14['jqpa_identitePMNomUsage']                   = jqpa2.identitePersonneQualifiee.jqpaIdentitePPNomUsage != null ? jqpa2.identitePersonneQualifiee.jqpaIdentitePPNomUsage : '';
var prenoms=[];
for ( i = 0; i < jqpa2.identitePersonneQualifiee.jqpaIdentitePPPrenom.size() ; i++ ){prenoms.push(jqpa2.identitePersonneQualifiee.jqpaIdentitePPPrenom[i]);}                            
formFieldsPers14['jqpa_identitePMPrenom']            = prenoms.toString();

if (jqpa2.identitePersonneQualifiee.jqpaIdentitePPDateNaissance !== null) {
	var dateTmp = new Date(parseInt(jqpa2.identitePersonneQualifiee.jqpaIdentitePPDateNaissance.getTimeInMillis()));
	var dateNaissance = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	dateNaissance = dateNaissance.concat(pad(month.toString()));
	dateNaissance = dateNaissance.concat(dateTmp.getFullYear().toString());
	formFieldsPers14['jqpa_identitePMDateNaissance']          = dateNaissance;
}
formFieldsPers14['jqpa_identitePMDepartement']                = jqpa2.identitePersonneQualifiee.jqpaIdentitePPDepartement != null ? jqpa2.identitePersonneQualifiee.jqpaIdentitePPDepartement.getId() : '';
formFieldsPers14['jqpa_identitePMLieuNaissancePays']          = (Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') or Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre')) ? ((jqpa2.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune != null ? jqpa2.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune : jqpa2.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceVille) + ' / ' + jqpa2.identitePersonneQualifiee.jqpaIdentitePPLieuNaissancePays) : '';

// Cadre 5C
formFieldsPers14['jqpa_aqpaSituationPM_engagement']           = Value('id').of(jqpa2.jqpaSituation).eq('jqpaSituationRecrutement') ? true : false;


// JQPA 3

//cadre 1
var formFieldsPers15 = {}

// Cadre 1
formFieldsPers15['jqpa_intercalaire']                         = true;
formFieldsPers15['jqpa_formulaire']                           = false;

// Cadre 4
formFieldsPers15['jqpa_entreprise_denomination']              = societe.entrepriseDenomination;
formFieldsPers15['jqpa_entreprise_formeJuridique']            = Value('id').of(societe.entrepriseFormeJuridique).eq('sarl') ? (societe.entrepriseAssocieUnique ? "EURL" : "SARL") : (societe.entrepriseAssocieUnique ? "SELARLU" : "SELARL"); 
formFieldsPers15['jqpa_entreprise_siren']                     = '';

 // Cadre 5A
formFieldsPers15['jqpa_pm_activite']                          = jqpa3.jqpaActiviteJqpaPP;

//Cadre 5B
formFieldsPers15['jqpa_aqpaSituationPM_diplome']              = Value('id').of(jqpa3.jqpaSituation).eq('jqpaSituationDiplome') or Value('id').of(jqpa3.jqpaSituation).eq('jqpaSituationExperience') ? true : false;
formFieldsPers15['jqpa_qualitePersonneQualifieePM_representant'] = Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPRepresentantLegal') ? true : false;
formFieldsPers15['jqpa_qualitePersonneQualifieePM_conjoint']  = Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPConjoint') ? true : false;
formFieldsPers15['jqpa_qualitePersonneQualifieePM_salarie']   = Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') ? true : false;
formFieldsPers15['jqpa_qualitePersonneQualifieePM_autre']     = Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre') ? true : false;
formFieldsPers15['jqpa_qualitePersonneQualifieePMAutre']      = jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifieePPChampAutre;

formFieldsPers15['jqpa_identitePMNomNaissance']               = jqpa3.identitePersonneQualifiee.jqpaIdentitePPNomNaissance != null ? jqpa3.identitePersonneQualifiee.jqpaIdentitePPNomNaissance : '';
formFieldsPers15['jqpa_identitePMNomUsage']                   = jqpa3.identitePersonneQualifiee.jqpaIdentitePPNomUsage != null ? jqpa3.identitePersonneQualifiee.jqpaIdentitePPNomUsage : '';
var prenoms=[];
for ( i = 0; i < jqpa3.identitePersonneQualifiee.jqpaIdentitePPPrenom.size() ; i++ ){prenoms.push(jqpa3.identitePersonneQualifiee.jqpaIdentitePPPrenom[i]);}                            
formFieldsPers15['jqpa_identitePMPrenom']            = prenoms.toString();

if (jqpa3.identitePersonneQualifiee.jqpaIdentitePPDateNaissance !== null) {
	var dateTmp = new Date(parseInt(jqpa3.identitePersonneQualifiee.jqpaIdentitePPDateNaissance.getTimeInMillis()));
	var dateNaissance = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	dateNaissance = dateNaissance.concat(pad(month.toString()));
	dateNaissance = dateNaissance.concat(dateTmp.getFullYear().toString());
	formFieldsPers15['jqpa_identitePMDateNaissance']          = dateNaissance;
}
formFieldsPers15['jqpa_identitePMDepartement']                = jqpa3.identitePersonneQualifiee.jqpaIdentitePPDepartement != null ? jqpa3.identitePersonneQualifiee.jqpaIdentitePPDepartement.getId() : '';
formFieldsPers15['jqpa_identitePMLieuNaissancePays']          = (Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') or Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre')) ? ((jqpa3.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune != null ? jqpa3.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune : jqpa3.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceVille) + ' / ' + jqpa3.identitePersonneQualifiee.jqpaIdentitePPLieuNaissancePays) : '';

// Cadre 5C
formFieldsPers15['jqpa_aqpaSituationPM_engagement']           = Value('id').of(jqpa3.jqpaSituation).eq('jqpaSituationRecrutement') ? true : false;


// Intercalaire NDI

// Cadre 1

formFields['complete_immatriculation_ndi']                   = true;
formFields['complete_modification_ndi']                      = false;

// Cadre 2

formFields['denomination_ndi']                               = societe.entrepriseDenomination;
formFields['adresseSiege_voie_ndi']                          = (siege.cadreAdresseProfessionnelle.numeroAdresseSiege != null ? siege.cadreAdresseProfessionnelle.numeroAdresseSiege : '')
															 + ' ' + (siege.cadreAdresseProfessionnelle.indiceAdresseSiege != null ? siege.cadreAdresseProfessionnelle.indiceAdresseSiege : '')
															 + ' ' + (siege.cadreAdresseProfessionnelle.typeAdresseSiege != null ? siege.cadreAdresseProfessionnelle.typeAdresseSiege : '')
															 +' ' + siege.cadreAdresseProfessionnelle.voieAdresseSiege
															 + ' ' + (siege.cadreAdresseProfessionnelle.complementAdresseSiege != null ? siege.cadreAdresseProfessionnelle.complementAdresseSiege : '')
															 + ' ' + (siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege != null ? siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege : '');
formFields['adresseSiege_codePostal_ndi']                    = siege.cadreAdresseProfessionnelle.codePostalAdresseSiege;
formFields['adresseSiege_commune_ndi']                       = siege.cadreAdresseProfessionnelle.communeAdresseSiege;

// Cadre 3
if(etablissement.etablissementNomDomaine != null) {
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTmp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['dateDebutActivite_ndi'] = date;
} else if (signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['dateDebutActivite_ndi'] = date;
}

formFields['adresseEtablissement_voie_ndi']                          = siege.formaliteCreationAvecActivite ?
																	(siege.etablissementPrincipalDifferentSiege ?
																	((etablissement.cadreAdresseEtablissement.numeroAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.numeroAdresseEtablissement : '')
																	+ ' ' + (etablissement.cadreAdresseEtablissement.indiceAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.indiceAdresseEtablissement : '')
																	+ ' ' + (etablissement.cadreAdresseEtablissement.typeAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.typeAdresseEtablissement : '')
																	+ ' ' + etablissement.cadreAdresseEtablissement.voieAdresseEtablissement
																	+ ' ' + (etablissement.cadreAdresseEtablissement.complementAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.complementAdresseEtablissement : '')
																	+ ' ' + (etablissement.cadreAdresseEtablissement.distributionSpecialeAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.distributionSpecialeAdresseEtablissement : '')) : 
																	((siege.cadreAdresseProfessionnelle.numeroAdresseSiege != null ? siege.cadreAdresseProfessionnelle.numeroAdresseSiege : '')
																	+ ' ' + (siege.cadreAdresseProfessionnelle.indiceAdresseSiege != null ? siege.cadreAdresseProfessionnelle.indiceAdresseSiege : '')
																	+ ' ' + (siege.cadreAdresseProfessionnelle.typeAdresseSiege != null ? siege.cadreAdresseProfessionnelle.typeAdresseSiege : '')
																	+' ' + siege.cadreAdresseProfessionnelle.voieAdresseSiege
																	+ ' ' + (siege.cadreAdresseProfessionnelle.complementAdresseSiege != null ? siege.cadreAdresseProfessionnelle.complementAdresseSiege : '')
																	+ ' ' + (siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege != null ? siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege : ''))) : '';
formFields['adresseEtablissement_codePostal_ndi']                   = siege.formaliteCreationAvecActivite ? (siege.etablissementPrincipalDifferentSiege ? etablissement.cadreAdresseEtablissement.codePostalAdresseEtablissement : siege.cadreAdresseProfessionnelle.codePostalAdresseSiege) : '';
formFields['adresseEtablissement_commune_ndi']                      = siege.formaliteCreationAvecActivite ? (siege.etablissementPrincipalDifferentSiege ? etablissement.cadreAdresseEtablissement.communeAdresseEtablissement : siege.cadreAdresseProfessionnelle.communeAdresseSiege) : '';
formFields['nomDomaineEtablissement_ndi']                    = etablissement.etablissementNomDomaine;
formFields['suppressionNomDomaineEtablissement_ndi']         = false;
formFields['remplacementNomDomaineEtablissement_ndi']        = false;
formFields['declarationInitialeNomDomaineEtablissement_ndi'] = true;
}

// Cadre 4

if (siege.societeNomDomaine.size() > 0) {
   if (etablissement.etablissementDateDebutActivite != null and "" !== siege.societeNomDomaine[0]) {
       var dateTmp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
       var date = pad(dateTmp.getDate().toString());
       var month = dateTmp.getMonth() + 1;
       date = date.concat(pad(month.toString()));
       date = date.concat(dateTmp.getFullYear().toString());
       formFields['dateEffetNomDomainePM_ndi'] = date;
   } else if (signataire.formaliteSignatureDate != null and "" !== siege.societeNomDomaine[0]) {
       var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
       var date = pad(dateTmp.getDate().toString());
       var month = dateTmp.getMonth() + 1;
       date = date.concat(pad(month.toString()));
       date = date.concat(dateTmp.getFullYear().toString());
       formFields['dateEffetNomDomainePM_ndi'] = date;
   }
   for (var i = 0; i < siege.societeNomDomaine.size(); i++) {
       formFields['nomDomainePM_ndi[' + i + ']'] = siege.societeNomDomaine[i];
       formFields['nouveauNomDomainePM_ndi[' + i + ']'] = siege.societeNomDomaine[i] !== "" ? true : false;
   }
}



/*
 * Création du dossier avec volet social : ajout du cerfa avec volet social
 */
var cerfaDoc1 = nash.doc //
	.load('models/cerfa_11680-07_M0_SARL_avec_volet_social.pdf') //
	.apply(formFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));

/*
 * Création du dossier sans volet social : ajout du cerfa sans volet social
 */
 
 var cerfaDoc2 = nash.doc //
	.load('models/cerfa_11680-07_M0_SARL_sans_volet_social.pdf') //
	.apply (formFields);
	cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));


/*
 * Ajout de l'intercalaire M0 Prime 1
 */
 
 if (dirigeant2.autreDirigeant2 or societe.entrepriseFusionScission or fondePouvoir1.autreDeclarationPersonneLiee) 
{
	var m0PrimeDoc1 = nash.doc //
		.load('models/cerfa_11772-03_M0prime_SARL_SELARL_creation.pdf') //
		.apply (formFieldsPers1);
	cerfaDoc1.append(m0PrimeDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire M0 Prime 2
 */
 
 if (societe.fusionGroup1.fusionAutreSociete or fondePouvoir.infoPeronnePouvoir2.autreDeclarationPersonneLiee2) 
{
	var m0PrimeDoc2 = nash.doc //
		.load('models/cerfa_11772-03_M0prime_SARL_SELARL_creation.pdf') //
		.apply (formFieldsPers2);
	cerfaDoc1.append(m0PrimeDoc2.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire M0 Prime 3
 */
 
 if (societe.fusionGroup2.fusionAutreSociete2) 
{
	var m0PrimeDoc3 = nash.doc //
		.load('models/cerfa_11772-03_M0prime_SARL_SELARL_creation.pdf') //
		.apply (formFieldsPers3);
	cerfaDoc1.append(m0PrimeDoc3.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Gérant 1
 */
 
 if (societe.entrepriseAssocieUniqueGerant or Value('id').of(gerance.typeGerance).eq('entrepriseNatureGeranceMajoritaire')) 
{
	var tnsDoc1 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers4);
	cerfaDoc1.append(tnsDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Gérant 2
 */
 
 if (Value('id').of(gerance.typeGerance).eq('entrepriseNatureGeranceMajoritaire') and dirigeant1.autreDirigeant1) 
{
	var tnsDoc2 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers5);
	cerfaDoc1.append(tnsDoc2.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Gérant 3
 */
 
 if (Value('id').of(gerance.typeGerance).eq('entrepriseNatureGeranceMajoritaire') and dirigeant2.autreDirigeant2 ) 
{
	var tnsDoc3 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers6);
	cerfaDoc1.append(tnsDoc3.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Gérant 4
 */
 
 if (Value('id').of(gerance.typeGerance).eq('entrepriseNatureGeranceMajoritaire') and dirigeant3.autreDirigeant3 ) 
{
	var tnsDoc4 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers7);
	cerfaDoc1.append(tnsDoc4.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Conjoint Associé Gérant 1
 */
 
 if (Value('id').of(dirigeant1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) 
{
	var tnsDoc5 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers8);
	cerfaDoc1.append(tnsDoc5.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Conjoint Associé Gérant 2
 */
 
 if (Value('id').of(dirigeant2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) 
{
	var tnsDoc6 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers9);
	cerfaDoc1.append(tnsDoc6.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Conjoint Associé Gérant 3
 */
 
 if (Value('id').of(dirigeant3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) 
{
	var tnsDoc7 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers10);
	cerfaDoc1.append(tnsDoc7.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Conjoint Associé Gérant 4
 */
 
 if (Value('id').of(dirigeant4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) 
{
	var tnsDoc8 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers11);
	cerfaDoc1.append(tnsDoc8.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Associé unique non Gérant
 */
 
 if (societe.entrepriseAssocieUnique and societe.entrepriseAssocieUniqueNonGerant) 
{
	var tnsDoc9 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers12);
	cerfaDoc1.append(tnsDoc9.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire JQPA 1
 */
 
 if (jqpa1.jqpaActiviteJqpaPP) 
{
	var jqpaDoc1 = nash.doc //
		.load('models/cerfa_14077-02_JQPA.pdf') //
		.apply (formFieldsPers13);
	cerfaDoc1.append(jqpaDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire JQPA 2
 */
 
 if (jqpa2.jqpaActiviteJqpaPP) 
{
	var jqpaDoc2 = nash.doc //
		.load('models/cerfa_14077-02_JQPA.pdf') //
		.apply (formFieldsPers14);
	cerfaDoc1.append(jqpaDoc2.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire JQPA 3
 */
 
 if (jqpa3.jqpaActiviteJqpaPP) 
{
	var jqpaDoc3 = nash.doc //
		.load('models/cerfa_14077-02_JQPA.pdf') //
		.apply (formFieldsPers15);
	cerfaDoc1.append(jqpaDoc3.save('cerfa.pdf'));
}
 
/*
 * Ajout de l'intercalaire NDI Volet 1
 */
 
 if (etablissement.etablissementNomDomaine != null or (siege.societeNomDomaine[0].length > 0)) {
	var ndiDoc1 = nash.doc //
		.load('models/cerfa_14943-01_NDI_volet1.pdf') //
		.apply (formFields);
	cerfaDoc1.append(ndiDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire NDI Volet 2
 */
 
if (etablissement.etablissementNomDomaine != null or (siege.societeNomDomaine[0].length > 0)) {
	var ndiDoc2 = nash.doc //
		.load('models/cerfa_14943-01_NDI_volet2.pdf') //
		.apply (formFields);
	cerfaDoc1.append(ndiDoc2.save('cerfa.pdf'));
}

var pjUser = [];
var metas = [];

function pushPjPreview(fld) {
	fld.forEach(function (elm) {
        pjUser.push(elm);
        metas.push({'name':'document', 'value': '/'+elm.getAbsolutePath()});
    });
}
 

/*
 * Ajout des PJs
 */


// PJ Dirigeant 1


pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant1);

var pj=$m0sarl.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteRepresentantLegal') and (not $m0sarl.cadre5DirigeantGroup.cadreIdentiteDirigeant1.autreDirigeant1 or Value('id').of($m0sarl.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant1') or $m0sarl.cadre5DirigeantGroup.cadreIdentiteDirigeant1.entrepriseAssocieUniqueNonGerant)) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant1Signataire);
}

var pj=$m0sarl.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire') 
	or (Value('id').of(pj).contains('FormaliteSignataireQualiteRepresentantLegal') 
	and not Value('id').of($m0sarl.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant1')
	and $m0sarl.cadre5DirigeantGroup.cadreIdentiteDirigeant1.autreDirigeant1 )) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant1NonSignataire);
}

var pj=$m0sarl.cadre5DirigeantGroup.cadreIdentiteDirigeant1.statutConjointCollaborateurGeranceMaj ;
if(Value('id').of(pj).contains('personneLieeConjointStatutCollaborateur')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDConjoint1);
}

var pj=$m0sarl.cadre5DirigeantGroup.cadreIdentiteDirigeant1 ;
if(pj.conjointRole == true and (pj.statutConjointCollaborateurGeranceNonMaj != null or pj.statutConjointCollaborateurGeranceMaj != null)) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjChoixStatutConjoint1);
}

// PJ Dirigeant 2

if($m0sarl.cadre5DirigeantGroup.cadreIdentiteDirigeant1.autreDirigeant1) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant2);
}

var pj=$m0sarl.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteRepresentantLegal') and Value('id').of($m0sarl.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant2')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant2Signataire);
}

var pj=$m0sarl.cadre11SignatureGroup.cadre11Signature.soussigne;
if($m0sarl.cadre5DirigeantGroup.cadreIdentiteDirigeant1.autreDirigeant1 and not Value('id').of($m0sarl.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant2')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant2NonSignataire);
}

var pj=$m0sarl.cadre9DirigeantGroup.cadreIdentiteDirigeant2.statutConjointCollaborateurGeranceMaj ;
if(Value('id').of(pj).contains('personneLieeConjointStatutCollaborateur')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDConjoint2);
}

var pj=$m0sarl.cadre9DirigeantGroup.cadreIdentiteDirigeant2 ;
if(pj.conjointRole == true and (pj.statutConjointCollaborateurGeranceNonMaj != null or pj.statutConjointCollaborateurGeranceMaj != null)) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjChoixStatutConjoint2);
}

//PJ Dirigeant 3

if($m0sarl.cadre9DirigeantGroup.cadreIdentiteDirigeant2.autreDirigeant2) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant3);
}

var pj=$m0sarl.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteRepresentantLegal') and Value('id').of($m0sarl.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant3')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant3Signataire);
}

var pj=$m0sarl.cadre11SignatureGroup.cadre11Signature.soussigne;
if($m0sarl.cadre9DirigeantGroup.cadreIdentiteDirigeant2.autreDirigeant2 and not Value('id').of($m0sarl.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant3')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant3NonSignataire);
}

var pj=$m0sarl.cadre12DirigeantGroup.cadreIdentiteDirigeant3.statutConjointCollaborateurGeranceMaj ;
if(Value('id').of(pj).contains('personneLieeConjointStatutCollaborateur')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDConjoint3);
}

var pj=$m0sarl.cadre12DirigeantGroup.cadreIdentiteDirigeant3 ;
if(pj.conjointRole == true and (pj.statutConjointCollaborateurGeranceNonMaj != null or pj.statutConjointCollaborateurGeranceMaj != null)) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjChoixStatutConjoint3);
}

//PJ Dirigeant 4

if($m0sarl.cadre12DirigeantGroup.cadreIdentiteDirigeant3.autreDirigeant3) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant4);
}

var pj=$m0sarl.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteRepresentantLegal') and Value('id').of($m0sarl.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant4')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant4Signataire);
}

var pj=$m0sarl.cadre11SignatureGroup.cadre11Signature.soussigne;
if($m0sarl.cadre12DirigeantGroup.cadreIdentiteDirigeant3.autreDirigeant3 and not Value('id').of($m0sarl.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant4')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant4NonSignataire);
}

var pj=$m0sarl.cadre15DirigeantGroup.cadreIdentiteDirigeant4.statutConjointCollaborateurGeranceMaj ;
if(Value('id').of(pj).contains('personneLieeConjointStatutCollaborateur')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDConjoint4);
}

var pj=$m0sarl.cadre15DirigeantGroup.cadreIdentiteDirigeant4 ;
if(pj.conjointRole == true and (pj.statutConjointCollaborateurGeranceNonMaj != null or pj.statutConjointCollaborateurGeranceMaj != null)) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjChoixStatutConjoint4);
}

if(not dirigeant1.statutPresenceDirigeant or (dirigeant1.autreDirigeant1 and not dirigeant2.statutPresenceDirigeant) or (dirigeant2.autreDirigeant2 and not dirigeant3.statutPresenceDirigeant) or (dirigeant3.autreDirigeant3 and not dirigeant4.statutPresenceDirigeant)) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjNominationDirigeant);
}



// PJ Fondé de pouvoir 1


if($m0sarl.cadre19PeronnePouvoirGroup.cadre6PeronnePouvoir.personneLieePersonneLieeEtablissement) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCPersonnePouvoir);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDPersonnePouvoir);
}

// PJ Fondé de pouvoir 2

if($m0sarl.cadre19PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir.autreDeclarationPersonneLiee) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCPersonnePouvoir2);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDPersonnePouvoir2);
}

// PJ Fondé de pouvoir 3

if($m0sarl.cadre19PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir2.autreDeclarationPersonneLiee2) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCPersonnePouvoir3);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDPersonnePouvoir3);
}


// PJ Mandataire

var pj=$m0sarl.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPouvoir);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire);
}

//PJ Société

pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjStatuts);
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjAnnonceLegaleConstitution);

var pj=$m0sarl.cadre1SocieteGroup.cadre1Identite;
if(pj.entrepriseCapitalApportNature and (pj.entrepriseCapitalApportMoitierCapital or pj.entrepriseCapitalApport30000)) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjRapportCAA);
}

var pj=$m0sarl.cadre1SocieteGroup.cadre1Identite ;
if(pj.entrepriseContratAppuiPresence) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjContratAppui);
}

// PJ Etablissement

var pj=$m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.activiteLieu ;
if(Value('id').of(pj).contains('entrepriseAdresseEntrepriseDomicile')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDomicile);
}

var pj=$m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.activiteLieu ;
if(Value('id').of(pj).contains('etablissementDomiciliationOui')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjContratDomiciliation);
}

if (Value('id').of($m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.activiteLieu).contains('etablissementDomiciliationNon')
 and ($m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.etablissementPrincipalDifferentSiege 
 or not $m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.formaliteCreationAvecActivite 
 or (not $m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.etablissementPrincipalDifferentSiege 
 and ($m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.activiteLiberaleOui
 or Value('id').of($m0sarl.cadre1SocieteGroup.cadre1Identite.entrepriseFormeJuridique).eq('selarl')
 or Value('id').of($m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsCreation') 
 or Value('id').of($m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsCocheAutre')
 or (Value('id').of($m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.etablisementOrigineFondsCommercial).contains('EtablissementOrigineFondsAchat') 
 and $m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.precedentExploitant.fondsArtisanal))))) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjSocieteCreation);
}

var pj=$m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.etablisementOrigineFondsCommercial ;
if($m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.etablissementPrincipalDifferentSiege
and ($m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.activiteLiberaleOui 
or Value('id').of($m0sarl.cadre1SocieteGroup.cadre1Identite.entrepriseFormeJuridique).eq('selarl')
or Value('id').of(pj).contains('EtablissementOrigineFondsCreation')
or Value('id').of(pj).contains('EtablissementOrigineFondsCocheAutre')
or (Value('id').of(pj).contains('EtablissementOrigineFondsAchat') 
and $m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.precedentExploitant.fondsArtisanal))) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjEtablissementCreation);
}

var pj=$m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.etablisementOrigineFondsCommercial ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsLocationGerance')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjLocationGerance);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjLocationGerancePublication);
}

var pj=$m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.etablisementOrigineFondsCommercial ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsGeranceMandat')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjGeranceMandat);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjGeranceMandatPublication);
}

var pj=$m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.etablisementOrigineFondsCommercial ;
if((Value('id').of(pj).contains('EtablissementOrigineFondsAchat')) and $m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.precedentExploitant.cessionFonds) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPlanCession);
}

var pj=$m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.etablisementOrigineFondsCommercial ;
if((Value('id').of(pj).contains('EtablissementOrigineFondsAchat')) and ($m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.precedentExploitant.etablissementJournalAnnoncesLegalesNom != null)) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjAchat);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjAchatPublication);
}

var pj=$m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.etablisementOrigineFondsCommercial ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsApport')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjApport);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjApportPublication);
}

// PJ JQPA

if(Value('id').of(jqpa1.jqpaSituation).eq('jqpaSituationDiplome')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDiplomes1);
}

if(Value('id').of(jqpa1.jqpaSituation).eq('jqpaSituationExperience')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles1);
}

if(Value('id').of(jqpa2.jqpaSituation).eq('jqpaSituationDiplome')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDiplomes2);
}

if(Value('id').of(jqpa2.jqpaSituation).eq('jqpaSituationExperience')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles2);
}

if(Value('id').of(jqpa3.jqpaSituation).eq('jqpaSituationDiplome')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDiplomes3);
}

if(Value('id').of(jqpa3.jqpaSituation).eq('jqpaSituationExperience')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles3);
}

// PJ MBE

pushPjPreview($attachmentPreprocess.attachmentPreprocess.formulaireBE1);

if (not Value('id').of(signataire.nombreMBE).eq('un')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.formulaireBE2);
}

if (not Value('id').of(signataire.nombreMBE).eq('un') and not Value('id').of(signataire.nombreMBE).eq('deux')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.formulaireBE3);
}

if (Value('id').of(signataire.nombreMBE).eq('quatre') or Value('id').of(signataire.nombreMBE).eq('cinq')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.formulaireBE4);
}

if (Value('id').of(signataire.nombreMBE).eq('cinq')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.formulaireBE5);
}

pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDActiviteReglementee);

/*
 * Enregistrement du fichier (en mémoire)
 */

var finalDoc = cerfaDoc1.save('M0_SARL.pdf');
metas.push({'name':'document', 'value': '/3-review/generated/generated.record-1-M0_SARL.pdf'});

//Remove old metas before insert
var metasToDelete = [];
var recordMetas = nash.record.meta() != null ? nash.record.meta().metas : null;
var recordMetasSize = recordMetas != null ? recordMetas.size() : 0;

for (var i = 0; i < recordMetasSize; i++) {
   if (recordMetas.get(i).name == 'document' && !recordMetas.get(i).value.contains("proxy")) {
       metasToDelete.push({'name':'document', 'value': recordMetas.get(i).value});
   }
}
nash.record.removeMeta(metasToDelete);

nash.record.meta(metas);

var data = [ spec.createData({
    id : 'record',
    label : 'Déclaration de constitution d\'une société à responsabilité limitée ou d\'une société d\'exercice libérale à responsabilité limitée',
    description : 'Voici le formulaire obtenu à partir des données saisies.',
    type : 'FileReadOnly',
    value : [ finalDoc ]
}),  spec.createData({
    id : 'attachments',
    label : 'Pièces jointes',
    description : 'Pièces jointes ajoutées au formulaire.',
    type : 'FileReadOnly',
    value : pjUser
}) ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Déclaration de constitution d\'une société à responsabilité limitée ou d\'une société d\'exercice libérale à responsabilité limitée',
    groups : groups
});