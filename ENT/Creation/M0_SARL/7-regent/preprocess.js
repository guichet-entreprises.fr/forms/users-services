//Fichier de mapping des champs Regent pour :
//Version : V2008.11 
//Evenements : 03M,  
//Fichier généré le : 2018-11-02_114826 


function pad(s) { return (s < 10) ? '0' + s : s; }
function sanitize(s){return s.replaceAll("’","'").replaceAll("–","-").replaceAll("­","");}

var regentVersion = "V2008.11";  // (V2008.11, V2016.02) 
var eventRegent = $m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.formaliteCreationAvecActivite ? ($m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.etablissementPrincipalDifferentSiege ? "03M" : "01M") : "02M"; //[] 
var authorityType = "CFE"; // (CFE, TDR)
var authorityId = _INPUT_.dataGroup.codeEDI; // (code EDI)

//Ajout du type de la deuxième autorité
var authorityType2 = "TDR"; // (CFE, TDR) 
//Ajout du code de la deuxième autorité
var authorityId2 = _INPUT_.dataGroup.codeEDI2; // (code EDI) 

var regEx = new RegExp('[0-9]{5}'); 
var societe = $m0sarl.cadre1SocieteGroup.cadre1Identite;
var siege = $m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege; 
var etablissement = $m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite ;
var origine = $m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds ;
var gerance = $m0sarl.cadre5DirigeantGroup.cadre5Gerance;
var dirigeant1 = $m0sarl.cadre5DirigeantGroup.cadreIdentiteDirigeant1;
var conjoint1 = $m0sarl.cadre5DirigeantGroup.cadreIdentiteDirigeant1.cadre2InfosConjoint;
var dirigeant2 = $m0sarl.cadre9DirigeantGroup.cadreIdentiteDirigeant2;
var conjoint2 = $m0sarl.cadre9DirigeantGroup.cadreIdentiteDirigeant2.cadre2InfosConjoint ;
var dirigeant3 = $m0sarl.cadre12DirigeantGroup.cadreIdentiteDirigeant3;
var conjoint3 = $m0sarl.cadre12DirigeantGroup.cadreIdentiteDirigeant3.cadre2InfosConjoint;
var dirigeant4 = $m0sarl.cadre15DirigeantGroup.cadreIdentiteDirigeant4;
var conjoint4 = $m0sarl.cadre15DirigeantGroup.cadreIdentiteDirigeant4.cadre2InfosConjoint;
var fondePouvoir = $m0sarl.cadre19PeronnePouvoirGroup.cadre6PeronnePouvoir ;
var fondePouvoir1 = $m0sarl.cadre19PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir ;
var fondePouvoir2 = $m0sarl.cadre19PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir2 ;
var fondePouvoir3 = $m0sarl.cadre19PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir3 ;
var fiscal = $m0sarl.cadre20optFiscGroup.cadre20optFisc ;
var correspondance = $m0sarl.cadre10RensCompGroup.cadre10RensComp ;
var signataire = $m0sarl.cadre11SignatureGroup.cadre11Signature ;
var jqpa1 = $m0sarl.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification1;
var jqpa2 = $m0sarl.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification2;
var jqpa3 = $m0sarl.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification3;
var fusion1 = $m0sarl.cadre1SocieteGroup.cadre1Identite.fusionGroup1;
var fusion2 = $m0sarl.cadre1SocieteGroup.cadre1Identite.fusionGroup2;
var fusion3 = $m0sarl.cadre1SocieteGroup.cadre1Identite.fusionGroup3;
var socialDirigeant1 = $m0sarl.cadre7DeclarationSocialeGroupGerant1.cadre7DeclarationSociale;
var socialConjoint1 = $m0sarl.cadre8DeclarationSocialeGroupConjointAssocie1.cadre7DeclarationSociale;
var socialDirigeant2 = $m0sarl.cadre10DeclarationSocialeGroupGerant2.cadre7DeclarationSociale;
var socialConjoint2 = $m0sarl.cadre11DeclarationSocialeGroupConjointAssocie2.cadre7DeclarationSociale;
var socialDirigeant3 = $m0sarl.cadre13DeclarationSocialeGroupGerant3.cadre7DeclarationSociale;
var socialConjoint3 = $m0sarl.cadre14DeclarationSocialeGroupConjointAssocie3.cadre7DeclarationSociale;
var socialDirigeant4 = $m0sarl.cadre16DeclarationSocialeGroupGerant4.cadre7DeclarationSociale;
var socialConjoint4 = $m0sarl.cadre17DeclarationSocialeGroupConjointAssocie4.cadre7DeclarationSociale;
var socialAssocieUnique = $m0sarl.cadre18DeclarationSocialeGroupAssocieUnique.cadre7DeclarationSociale;

var regentFields = {}; 

// A mettre toujours sous "Z1611"
regentFields['/REGENT-XML/Emetteur']= "Z1611";

regentFields['/REGENT-XML/Destinataire']= authorityId;

// Complété par destiny
regentFields['/REGENT-XML/DateHeureEmission']= null;
regentFields['/REGENT-XML/VersionMessage']= null;
regentFields['/REGENT-XML/VersionNorme']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Specification/NomService']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Specification/VersionService']= null; 

                                  // Groupe GDF : Groupe données de service

// Sous groupe IDF : Identification de la formalité

// Généré par destiny
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C01']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C03']='0';
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C04']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05']= ((($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA != null
																	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
																	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
																	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
																	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre')
																	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.jqpaDomaineTransport).eq('jqpaTransportAutre'))
																	or ($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA != null
																	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA).eq('nafaAutre')
																	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineAlimentation).eq('alimentationAutre')
																	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineAutresServices).eq('servicesAutre')
																	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineProduction).eq('productionAutre')
																	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.jqpaDomaineTransport).eq('jqpaTransportAutre'))
																	or ($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA != null
																	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA).eq('nafaAutre')
																	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineAlimentation).eq('alimentationAutre')
																	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineAutresServices).eq('servicesAutre')
																	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineProduction).eq('productionAutre')
																	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.jqpaDomaineTransport).eq('jqpaTransportAutre')))
																	and not $m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.optionCMACCI) ? "M" : "C";

if ((($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA != null
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	or ($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA != null
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	or ($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA != null
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.jqpaDomaineTransport).eq('jqpaTransportAutre')))
	and not $m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.optionCMACCI) 	{
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C06']= (($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA != null
																	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
																	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
																	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
																	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre')
																	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.jqpaDomaineTransport).eq('jqpaTransportAutre'))
																	and not $m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.optionCMACCI) ? "O" : "N";
}																	

// Sous groupe EDF : Evènement déclaré
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.1']= siege.formaliteCreationAvecActivite ? (siege.etablissementPrincipalDifferentSiege ? "03M" : "01M") : "02M";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2']= date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2']= date;
}

// Sous groupe DMF : Destinataire de la formalité

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[1]']= authorityId;
if (authorityId != "" and authorityId2 != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[2]']= authorityId2;
}


// Sous groupe ADF : Adresse de correspondance

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C36']= (Value('id').of(correspondance.adresseCorrespond).eq('siege') or Value('id').of(correspondance.adresseCorrespond).eq('etablissement')) ?
																		societe.entrepriseDenomination : correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance;

if (Value('id').of(correspondance.adresseCorrespond).eq('siege')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']= siege.cadreAdresseProfessionnelle.communeAdresseSiege.getId();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('etablissement')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']=etablissement.cadreAdresseEtablissement.communeAdresseEtablissement.getId();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']=correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune.getId();
}	

if ((Value('id').of(correspondance.adresseCorrespond).eq('siege') and siege.cadreAdresseProfessionnelle.numeroAdresseSiege != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('etablissement') and etablissement.cadreAdresseEtablissement.numeroAdresseEtablissement != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.numeroRueAdresseCorrespondance != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.5']= Value('id').of(correspondance.adresseCorrespond).eq('siege') ? siege.cadreAdresseProfessionnelle.numeroAdresseSiege :
																			(Value('id').of(correspondance.adresseCorrespond).eq('etablissement') ? etablissement.cadreAdresseEtablissement.numeroAdresseEtablissement : 
																			correspondance.adresseCorrespondance.numeroRueAdresseCorrespondance);
}
	
if (Value('id').of(correspondance.adresseCorrespond).eq('siege') and siege.cadreAdresseProfessionnelle.indiceAdresseSiege != null) {
   var monId1 = Value('id').of(siege.cadreAdresseProfessionnelle.indiceAdresseSiege)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId1;  
} else if (Value('id').of(correspondance.adresseCorrespond).eq('etablissement') and etablissement.cadreAdresseEtablissement.indiceAdresseEtablissement != null) {
   var monId2 = Value('id').of(etablissement.cadreAdresseEtablissement.indiceAdresseEtablissement)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId2;
} else if (correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance !== null) {
   var monId3 = Value('id').of(correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId3;
}

if ((Value('id').of(correspondance.adresseCorrespond).eq('siege') and siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('etablissement') and etablissement.cadreAdresseEtablissement.distributionSpecialeAdresseEtablissement != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.distriutionSpecialeVoieAdresseCorrespondance != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.7']= Value('id').of(correspondance.adresseCorrespond).eq('siege') ? siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege : 
																				(Value('id').of(correspondance.adresseCorrespond).eq('etablissement') ? etablissement.cadreAdresseEtablissement.distributionSpecialeAdresseEtablissement : 
																				correspondance.adresseCorrespondance.distriutionSpecialeVoieAdresseCorrespondance);
}

if ((Value('id').of(correspondance.adresseCorrespond).eq('siege') and siege.cadreAdresseProfessionnelle.codePostalAdresseSiege != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('etablissement') and etablissement.cadreAdresseEtablissement.codePostalAdresseEtablissement != null)) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8']= Value('id').of(correspondance.adresseCorrespond).eq('siege') ? siege.cadreAdresseProfessionnelle.codePostalAdresseSiege : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('etablissement') ? etablissement.cadreAdresseEtablissement.codePostalAdresseEtablissement : 
																			correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal);
}
	
if ((Value('id').of(correspondance.adresseCorrespond).eq('siege') and siege.cadreAdresseProfessionnelle.complementAdresseSiege != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('etablissement') and etablissement.cadreAdresseEtablissement.complementAdresseEtablissement != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.rueComplementAdresseCorrespondance != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.10']= Value('id').of(correspondance.adresseCorrespond).eq('siege') ? siege.cadreAdresseProfessionnelle.complementAdresseSiege : 
																				(Value('id').of(correspondance.adresseCorrespond).eq('etablissement') ? etablissement.cadreAdresseEtablissement.complementAdresseEtablissement : 
																				correspondance.adresseCorrespondance.rueComplementAdresseCorrespondance);
}
	
if (Value('id').of(correspondance.adresseCorrespond).eq('siege') and siege.cadreAdresseProfessionnelle.typeAdresseSiege !== null) {
   var monId1 = Value('id').of(siege.cadreAdresseProfessionnelle.typeAdresseSiege)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId1;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('etablissement') and etablissement.cadreAdresseEtablissement.typeAdresseEtablissement !== null) {
   var monId2 = Value('id').of(etablissement.cadreAdresseEtablissement.typeAdresseEtablissement)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId2;
} else if (correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance !== null) {
   var monId3 = Value('id').of(correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId3;
}	

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12']= Value('id').of(correspondance.adresseCorrespond).eq('siege') ? siege.cadreAdresseProfessionnelle.voieAdresseSiege : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('etablissement') ? etablissement.cadreAdresseEtablissement.voieAdresseEtablissement : 
																			correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance);
	
if 	(Value('id').of(correspondance.adresseCorrespond).eq('siege')) {																		
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= siege.cadreAdresseProfessionnelle.communeAdresseSiege.getLabel(); 
} else if (Value('id').of(correspondance.adresseCorrespond).eq('etablissement')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= etablissement.cadreAdresseEtablissement.communeAdresseEtablissement.getLabel(); 
} else if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune.getLabel();
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.1[1]']= correspondance.infosSup.formaliteTelephone2.e164.replace('+', '00');

if (correspondance.infosSup.formaliteTelephone1 != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.1[2]']= correspondance.infosSup.formaliteTelephone1.e164.replace('+', '00');
}

if (correspondance.infosSup.telecopie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.2']= correspondance.infosSup.telecopie.e164.replace('+', '00');
}

if (correspondance.infosSup.formaliteFaxCourriel != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.3']= correspondance.infosSup.formaliteFaxCourriel;
}


// Sous groupe SIF : Signature de la formalité

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.1']= Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? 
																			signataire.adresseMandataire.nomPrenomDenominationMandataire : 
																			((Value('id').of(signataire.representantLegalNumero).eq('gerant1') or not dirigeant1.autreDirigeant1) ?
																			(dirigeant1.personneLieePPNomUsageGerant != null ? 
																			(dirigeant1.personneLieePPNomUsageGerant + ' ' + dirigeant1.personneLieePPPrenomGerant[0]) : 
																			(dirigeant1.personneLieePPNomNaissanceGerant + ' ' + dirigeant1.personneLieePPPrenomGerant[0]))
																			:
																			(Value('id').of(signataire.representantLegalNumero).eq('gerant2') ?
																			(dirigeant2.personneLieePPNomUsageGerant != null ? 
																			(dirigeant2.personneLieePPNomUsageGerant + ' ' + dirigeant2.personneLieePPPrenomGerant[0]) : 
																			(dirigeant2.personneLieePPNomNaissanceGerant + ' ' + dirigeant2.personneLieePPPrenomGerant[0]))
																			:
																			(Value('id').of(signataire.representantLegalNumero).eq('gerant3') ?
																			(dirigeant3.personneLieePPNomUsageGerant != null ? 
																			(dirigeant3.personneLieePPNomUsageGerant + ' ' + dirigeant3.personneLieePPPrenomGerant[0]) : 
																			(dirigeant3.personneLieePPNomNaissanceGerant + ' ' + dirigeant3.personneLieePPPrenomGerant[0]))
																			:
																			(Value('id').of(signataire.representantLegalNumero).eq('gerant4') ?
																			(dirigeant4.personneLieePPNomUsageGerant != null ? 
																			(dirigeant4.personneLieePPNomUsageGerant + ' ' + dirigeant4.personneLieePPPrenomGerant[0]) : 
																			(dirigeant4.personneLieePPNomNaissanceGerant + ' ' + dirigeant4.personneLieePPPrenomGerant[0])) : ''))));
																			
if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.2']= "Mandataire";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.3']= signataire.adresseMandataire.villeAdresseMandataire.getId();
if (signataire.adresseMandataire.numeroVoieMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.5']= signataire.adresseMandataire.numeroVoieMandataire;
}
if (signataire.adresseMandataire.indiceVoieMandataire !== null) {
   var monId = Value('id').of(signataire.adresseMandataire.indiceVoieMandataire)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.6']          = monId;
}
if (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.7']= signataire.adresseMandataire.voieDistributionSpecialeMandataire;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.8']= signataire.adresseMandataire.codePostalMandataire;
if (signataire.adresseMandataire.voieComplementMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.10']= signataire.adresseMandataire.voieComplementMandataire;
}
if (signataire.adresseMandataire.typeVoieMandataire !== null) {
   var monId = Value('id').of(signataire.adresseMandataire.typeVoieMandataire)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.11']          = monId;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.12']= signataire.adresseMandataire.nomVoieMandataire;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.13']= signataire.adresseMandataire.villeAdresseMandataire.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C41']= signataire.formaliteSignatureLieu;
if(signataire.formaliteSignatureDate !== null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C42']= date;
}	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C43']= (societe.entrepriseESS ? "M22=O!" : '') +''+(jqpa3.jqpaSituation != null ? "JQPA=3!" : (jqpa2.jqpaSituation != null ? "JQPA=2!" : 
(jqpa1.jqpaSituation != null ? "JQPA=1!" : ''))) +''+ ((siege.societeNomDomaine[0].length > 0) ? "M05=O!" : '') +''+ (etablissement.etablissementNomDomaine != null ? "E06=0!" : '') + '' + (correspondance.formaliteObservations != null ? correspondance.formaliteObservations : '') ;

// Groupe ICM : Identification complète de la personne morale

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/ICM/M01/M01.1']= societe.entrepriseDenomination;
if (societe.entrepriseSigle != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/ICM/M01/M01.2']= societe.entrepriseSigle;
}

// Groupe FJM : Forme juridique de la personne morale (ajout)

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FJM/M21/M21.1']= Value('id').of(societe.entrepriseFormeJuridique).eq('sarl') ? "5499" : "5485";
if (societe.entrepriseAssocieUnique) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FJM/M23']= "O";
}

// Groupe CSM : Caractéristiques de la personne morale (ajout)

if (societe.entrepriseCapitalVariable) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M24/M24.1']= "2";
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M24/M24.2']= societe.entrepriseCapitalMontant +''+ "00";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M24/M24.3']= "eur";
if (societe.entrepriseCapitalVariable) {regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M25']
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M24/M24.4']= societe.entrepriseCapitalVariableMinimum;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M25']= societe.entrepriseDuree;

if(societe.entrepriseDateClotureExerciceSocial != null) {
	var dateTemp = new Date(parseInt(societe.entrepriseDateClotureExerciceSocial.getTimeInMillis()));
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = "--" + month + "-" + day ; 
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M26']= date;
}

if(societe.entrepriseDateCloturePremierExerciceSocial != null) {
	var dateTemp = new Date(parseInt(societe.entrepriseDateCloturePremierExerciceSocial.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M28']          = date;
}	

// Groupe NGM : Nature de la gérance

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/NGM/M27']= (Value('id').of(gerance.typeGerance).eq('entrepriseNatureGeranceMajoritaire') or societe.entrepriseAssocieUniqueGerant) ? "1" :
																			(Value('id').of(gerance.typeGerance).eq('entrepriseNatureGeranceMinoritaireEgalitaire') ?
																			(gerance.entrepriseNatureGeranceSocieteAssocieeOui ? "3" : "4") : 
																			(gerance.entrepriseNatureGeranceSocieteAssocieeOui ? "5" : "6"));

// Groupe FSM : Fusion / Scission de la personne morale 

if (societe.entrepriseFusionScission) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M54']= Value('id').of(societe.fusionScission).eq('fusion') ? "1" : "2";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.1']= fusion1.entrepriseLieeEntreprisePMDenomination;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.2']= fusion1.entreperiseLieeEntreprisePMFormeJuridique.getId();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.3']= fusion1.adresseFusionScission.communeAdresseFusion.getId();
if(fusion1.adresseFusionScission.numeroAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.5']= fusion1.adresseFusionScission.numeroAdresseFusion;
}
if (fusion1.adresseFusionScission.indiceAdresseFusion !== null) {
   var monId = Value('id').of(fusion1.adresseFusionScission.indiceAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.6']          = monId;
}
if(fusion1.adresseFusionScission.distributionSpecialeAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.7']= fusion1.adresseFusionScission.distributionSpecialeAdresseFusion;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.8']= fusion1.adresseFusionScission.codePostalAdresseFusion;
if(fusion1.adresseFusionScission.complementAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.10']= fusion1.adresseFusionScission.complementAdresseFusion;
}
if (fusion1.adresseFusionScission.typeAdresseFusion !== null) {
   var monId = Value('id').of(fusion1.adresseFusionScission.typeAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.11']          = monId;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.12']= fusion1.adresseFusionScission.voieAdresseFusion;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.13']= fusion1.adresseFusionScission.communeAdresseFusion.getLabel();

//Récupérer le EntityId du greffe
var greffeId = fusion1.entrepriseLieeGreffe.id ;
//Appeler Directory pour récupérer l'autorité
_log.info('l entityId du greffe selectionne est {}', greffeId);
var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', greffeId) //
		   .connectionTimeout(10000) //
		   .receiveTimeout(10000) //
		   .accept('json') //
		   .get();
//result
var receiverInfo = response.asObject();
var contactInfo = !receiverInfo.details ? null : receiverInfo.details;
//Récupérer le code EDI
var codeEDIGreffe = !contactInfo.ediCode ? null : contactInfo.ediCode;
_log.info('le code edi du greffe selectionne est {}', codeEDIGreffe);
// À modifier quand Destiny fourni le réferentiel
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.4']=codeEDIGreffe;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.6']= fusion1.entrepriseLieeSiren.split(' ').join('');

if (fusion1.fusionAutreSociete) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.1']= fusion2.entrepriseLieeEntreprisePMDenomination;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.2']= fusion2.entreperiseLieeEntreprisePMFormeJuridique.getId();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.3']= fusion2.adresseFusionScission.communeAdresseFusion.getId();
if(fusion2.adresseFusionScission.numeroAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.5']= fusion2.adresseFusionScission.numeroAdresseFusion;
}
if (fusion2.adresseFusionScission.indiceAdresseFusion !== null) {
   var monId = Value('id').of(fusion2.adresseFusionScission.indiceAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.6']          = monId;
}
if(fusion2.adresseFusionScission.distributionSpecialeAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.7']= fusion2.adresseFusionScission.distributionSpecialeAdresseFusion;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.8']= fusion2.adresseFusionScission.codePostalAdresseFusion;
if(fusion2.adresseFusionScission.complementAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.10']= fusion2.adresseFusionScission.complementAdresseFusion;
}
if (fusion2.adresseFusionScission.typeAdresseFusion !== null) {
   var monId = Value('id').of(fusion2.adresseFusionScission.typeAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.11']          = monId;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.12']= fusion2.adresseFusionScission.voieAdresseFusion;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.13']= fusion2.adresseFusionScission.communeAdresseFusion.getLabel();

//Récupérer le EntityId du greffe
var greffeId = fusion2.entrepriseLieeGreffe.id ;
//Appeler Directory pour récupérer l'autorité
_log.info('l entityId du greffe selectionne est {}', greffeId);
var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', greffeId) //
		   .connectionTimeout(10000) //
		   .receiveTimeout(10000) //
		   .accept('json') //
		   .get();
//result
var receiverInfo = response.asObject();
var contactInfo = !receiverInfo.details ? null : receiverInfo.details;
//Récupérer le code EDI
var codeEDIGreffe = !contactInfo.ediCode ? null : contactInfo.ediCode;
_log.info('le code edi du greffe selectionne est {}', codeEDIGreffe);
// À modifier quand Destiny fourni le réferentiel
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.4']=codeEDIGreffe;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.6']= fusion2.entrepriseLieeSiren.split(' ').join('');
}

if (fusion2.fusionAutreSociete2) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.1']= fusion3.entrepriseLieeEntreprisePMDenomination;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.2']= fusion3.entreperiseLieeEntreprisePMFormeJuridique.getId();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.3']= fusion3.adresseFusionScission.communeAdresseFusion.getId();
if(fusion3.adresseFusionScission.numeroAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.5']= fusion3.adresseFusionScission.numeroAdresseFusion;
}
if (fusion3.adresseFusionScission.indiceAdresseFusion !== null) {
   var monId = Value('id').of(fusion3.adresseFusionScission.indiceAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.6']          = monId;
}
if(fusion3.adresseFusionScission.distributionSpecialeAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.7']= fusion3.adresseFusionScission.distributionSpecialeAdresseFusion;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.8']= fusion3.adresseFusionScission.codePostalAdresseFusion;
if(fusion3.adresseFusionScission.complementAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.10']= fusion3.adresseFusionScission.complementAdresseFusion;
}
if (fusion3.adresseFusionScission.typeAdresseFusion !== null) {
   var monId = Value('id').of(fusion3.adresseFusionScission.typeAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.11']          = monId;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.12']= fusion3.adresseFusionScission.voieAdresseFusion;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.13']= fusion3.adresseFusionScission.communeAdresseFusion.getLabel();

//Récupérer le EntityId du greffe
var greffeId = fusion3.entrepriseLieeGreffe.id ;
//Appeler Directory pour récupérer l'autorité
_log.info('l entityId du greffe selectionne est {}', greffeId);
var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', greffeId) //
		   .connectionTimeout(10000) //
		   .receiveTimeout(10000) //
		   .accept('json') //
		   .get();
//result
var receiverInfo = response.asObject();
var contactInfo = !receiverInfo.details ? null : receiverInfo.details;
//Récupérer le code EDI
var codeEDIGreffe = !contactInfo.ediCode ? null : contactInfo.ediCode;
_log.info('le code edi du greffe selectionne est {}', codeEDIGreffe);
// À modifier quand Destiny fourni le réferentiel
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.4']=codeEDIGreffe;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.6']= fusion3.entrepriseLieeSiren.split(' ').join('');
}
}

// Groupe SIU : Siège de l'entreprise

if (Value('id').of(siege.activiteLieu).eq('etablissementDomiciliationOui')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U10']= siege.entrepriseLieeNom;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U15']= siege.entrepriseLieeSiren.split(' ').join('');
}
if (Value('id').of(siege.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U16']= "O";
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.3']= siege.cadreAdresseProfessionnelle.communeAdresseSiege.getId();
if (siege.cadreAdresseProfessionnelle.numeroAdresseSiege != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.5']= siege.cadreAdresseProfessionnelle.numeroAdresseSiege;
}
if (siege.cadreAdresseProfessionnelle.indiceAdresseSiege !== null) {
   var monId = Value('id').of(siege.cadreAdresseProfessionnelle.indiceAdresseSiege)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.6']          = monId;
}
if (siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.7']= siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.8']= siege.cadreAdresseProfessionnelle.codePostalAdresseSiege;
if (siege.cadreAdresseProfessionnelle.complementAdresseSiege != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.10']= siege.cadreAdresseProfessionnelle.complementAdresseSiege;
}
if (siege.cadreAdresseProfessionnelle.typeAdresseSiege !== null) {
   var monId = Value('id').of(siege.cadreAdresseProfessionnelle.typeAdresseSiege)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.11']          = monId;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.12']= siege.cadreAdresseProfessionnelle.voieAdresseSiege;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.13']= siege.cadreAdresseProfessionnelle.communeAdresseSiege.getLabel();

// Groupe CPU : Caractéristiques de l'entreprise

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U21']= sanitize(siege.entrepriseActivitesPrincipales);
if (etablissement.etablissementEffectifSalariePresenceOui) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U22']= etablissement.cadre3EffectifSalarie.etablissementEffectifSalarieNombre;
}
if (societe.entrepriseContratAppuiPresence) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.1']= "D";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.1']=societe.cadre1ContratAppuiInfos.contratAppuiDenomination;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.2/U40.2.2.3'] = societe.cadre1ContratAppuiInfos.contratAppuiAdresse.communeAdresseContratAppui.getId();
if (societe.cadre1ContratAppuiInfos.contratAppuiAdresse.numeroAdresseContratAppui != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.2/U40.2.2.5']=societe.cadre1ContratAppuiInfos.contratAppuiAdresse.numeroAdresseContratAppui;
}
if (societe.cadre1ContratAppuiInfos.contratAppuiAdresse.indiceAdresseContratAppui != null !== null) {
   var monId1 = Value('id').of(societe.cadre1ContratAppuiInfos.contratAppuiAdresse.indiceAdresseContratAppui)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.2/U40.2.2.6'] = monId1;
}
if (societe.cadre1ContratAppuiInfos.contratAppuiAdresse.distributionSpecialeAdresseContratAppui != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.2/U40.2.2.7']=societe.cadre1ContratAppuiInfos.contratAppuiAdresse.distributionSpecialeAdresseContratAppui;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.2/U40.2.2.8']=societe.cadre1ContratAppuiInfos.contratAppuiAdresse.codePostalAdresseContratAppui;
if (societe.cadre1ContratAppuiInfos.contratAppuiAdresse.complementAdresseContratAppui != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.2/U40.2.2.10']=societe.cadre1ContratAppuiInfos.contratAppuiAdresse.complementAdresseContratAppui;
}
if (societe.cadre1ContratAppuiInfos.contratAppuiAdresse.typeAdresseContratAppui != null !== null) {
   var monId1 = Value('id').of(societe.cadre1ContratAppuiInfos.contratAppuiAdresse.typeAdresseContratAppui)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.2/U40.2.2.11'] = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.2/U40.2.2.12']=societe.cadre1ContratAppuiInfos.contratAppuiAdresse.voieAdresseContratAppui;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.2/U40.2.2.13']=societe.cadre1ContratAppuiInfos.contratAppuiAdresse.communeAdresseContratAppui.getLabel();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.4']=societe.cadre1ContratAppuiInfos.contratAppuiSiren.split(' ').join('');
if(societe.contratAppuiDateFin !== null) {
	    var dateTemp = new Date(parseInt(societe.contratAppuiDateFin.getTimeInMillis()));
	    var year = dateTemp.getFullYear();
	    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	    var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.2/U40.2.5'] = date;
}
}

// RFU : Régime fiscal de l'entreprise

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U31']= societe.optionME ? 
																		((siege.activiteLiberaleOui or Value('id').of(societe.entrepriseFormeJuridique).eq('selarl')) ? "110" : "116")	:
																		(Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberale).eq('regimeFiscalRegimeImpositionBeneficesDcBNC') ? "111" :
																		((Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeCommerciale).eq('regimeFiscalRegimeImpositionBeneficesReelBIC') and Value('id').of(fiscal.impositionBenefices.regimeFiscalReel).eq('regimeFiscalRegimeImpositionBeneficesRs')) ? "112" : 
																		((Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeCommerciale).eq('regimeFiscalRegimeImpositionBeneficesReelBIC') and Value('id').of(fiscal.impositionBenefices.regimeFiscalReel).eq('regimeFiscalRegimeImpositionBeneficesRn')) ? "113" : 
																		(((Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeCommerciale).eq('regimeFiscalRegimeImpositionBeneficesOptionIS') or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberale).eq('regimeFiscalRegimeImpositionBeneficesOptionIS')) and Value('id').of(fiscal.impositionBenefices.regimeFiscalReel).eq('regimeFiscalRegimeImpositionBeneficesRs')) ? "114" : "115"))));

if (Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeCommerciale).eq('regimeFiscalRegimeImpositionBeneficesOptionIS') or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberale).eq('regimeFiscalRegimeImpositionBeneficesOptionIS')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U32']= "211";
} else if (societe.optionME or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberale).eq('regimeFiscalRegimeImpositionBeneficesDcBNC') or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeCommerciale).eq('regimeFiscalRegimeImpositionBeneficesReelBIC')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U32']= "212";
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.1']= (Value('id').of(fiscal.optionsTVA.regimeTVA1).eq('regimeFiscalRegimeImpositionTVARfTVA') or Value('id').of(fiscal.optionsTVA.regimeTVA2).eq('regimeFiscalRegimeImpositionTVARfTVA') or Value('id').of(fiscal.optionsTVA.regimeTVA3).eq('regimeFiscalRegimeImpositionTVARfTVA') or societe.optionME) ? "310" :
																			   ((Value('id').of(fiscal.optionsTVA.regimeTVA1).eq('regimeFiscalRegimeImpositionTVARsTVA') or Value('id').of(fiscal.optionsTVA.regimeTVA3).eq('regimeFiscalRegimeImpositionTVARsTVA')) ? "311" :
																			   ((Value('id').of(fiscal.optionsTVA.regimeTVA2).eq('regimeFiscalRegimeImpositionTVARnTVA') or Value('id').of(fiscal.optionsTVA.regimeTVA3).eq('regimeFiscalRegimeImpositionTVARnTVA')) ? "312" : "313"));

if (fiscal.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres1 or fiscal.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres2) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.2']= fiscal.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres1 ? "410" : "411";
}

if(societe.entrepriseDateClotureExerciceSocial != null) {
	var dateTemp = new Date(parseInt(societe.entrepriseDateClotureExerciceSocial.getTimeInMillis()));
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = "--" + month + "-" + day ; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U37']          = date;
}

// Groupe OIU : Options fiscales avec incidence sociale
if (Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeCommerciale).eq('regimeFiscalRegimeImpositionBeneficesOptionIS') or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberale).eq('regimeFiscalRegimeImpositionBeneficesOptionIS')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OIU/U75']= "211";
} else if (societe.optionME or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberale).eq('regimeFiscalRegimeImpositionBeneficesDcBNC') or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeCommerciale).eq('regimeFiscalRegimeImpositionBeneficesReelBIC')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OIU/U75']= "212";
}


// Groupe GRD : Dirigeant personne physique

// Gérant 1 

// Groupe DIU
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/DIU/U50/U50.1']= "1";
if (dirigeant1.conjointRole) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/DIU/U50/U50.3']= "O";
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/DIU/U51/U51.1']= "30";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/DIU/U52']= "P";

// Groupe IPD
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/IPD/D01/D01.2']= dirigeant1.personneLieePPNomNaissanceGerant;
var prenoms=[];
for (i = 0; i < dirigeant1.personneLieePPPrenomGerant.size() ; i++ ){
	prenoms.push(dirigeant1.personneLieePPPrenomGerant[i]);
} regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/IPD/D01/D01.3']= prenoms;
if (dirigeant1.personneLieePPNomUsageGerant != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/IPD/D01/D01.4']= dirigeant1.personneLieePPNomUsageGerant;
}
if(dirigeant1.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant1.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/IPD/D03/D03.1']= date;
}	
if (not Value('id').of(dirigeant1.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant1.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeant1.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/IPD/D03/D03.2'] = dirigeant1.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant1.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/IPD/D03/D03.3'] = dirigeant1.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant1.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/IPD/D03/D03.4'] = dirigeant1.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/IPD/D04/D04.3']= dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdresseCommune.getId();
} else if (not Value('id').of(dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/IPD/D04/D04.3']= result;
}
if (dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdresserueNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/IPD/D04/D04.5']= dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdresserueNumeroVoie;
}
if (dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/IPD/D04/D04.6']          = monId1;  
} 
if (dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/IPD/D04/D04.7']= dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/IPD/D04/D04.8']= dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdresseCodePostal != null ? dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdresseCodePostal : ".";
if (dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiquerueAdresseComplementAdresse != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/IPD/D04/D04.10']= dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiquerueAdresseComplementAdresse;
}
if (dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/IPD/D04/D04.11']          = monId1;  
} 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/IPD/D04/D04.12']= dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdresseNomVoie;
if 	(Value('id').of(dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {																		
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/IPD/D04/D04.13']= dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/IPD/D04/D04.13']= dirigeant1.cadre1AdresseDeclarant.personneLieeAdresseVille;
}
if (not Value('id').of(dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/IPD/D04/D04.14']= dirigeant1.cadre1AdresseDeclarant.personneLieePersonnePhysiqueAdressePays.getLabel();
}

// Groupe NPD

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/NPD/D21']= dirigeant1.personneLieePPNationaliteGerant;

// Groupe SCD

if (dirigeant1.conjointRole) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/SCD/D40']= Value('id').of(dirigeant1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') ? "1" : 
																										((Value('id').of(dirigeant1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie') or Value('id').of(dirigeant1.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutAssocie')) ? "2" : "3");
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/SCD/D41']= "O";
}

// Groupe ICD

if (dirigeant1.conjointRole and Value('id').of(dirigeant1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/ICD/D42/D42.2']= conjoint1.personneLieePPNomNaissanceConjointPacse;
var prenoms=[];
for (i = 0; i < conjoint1.personneLieePPPrenomConjointPacse.size() ; i++ ){
	prenoms.push(conjoint1.personneLieePPPrenomConjointPacse[i]);
} regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/ICD/D42/D42.3']= prenoms;
if(conjoint1.personneLieePPNomUsageConjointPacse !== null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/ICD/D42/D42.4']= conjoint1.personneLieePPNomUsageConjointPacse;
}
if(conjoint1.personneLieePPDateNaissanceConjointPacse !== null) {
	var dateTemp = new Date(parseInt(conjoint1.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/ICD/D43/D43.1']= date;
}	
if (not Value('id').of(conjoint1.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
	var idPays = conjoint1.personneLieePPPaysNaissanceConjointPacse.getId();
	var result = idPays.match(regEx)[0]; 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/ICD/D43/D43.2']= result;
} else if (Value('id').of(conjoint1.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/ICD/D43/D43.2'] = conjoint1.personneLieePPLieuNaissanceCommnuneConjointPacse.getId();
}		
if (not Value('id').of(conjoint1.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {																					 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/ICD/D43/D43.3'] = conjoint1.personneLieePPPaysNaissanceConjointPacse.getLabel();
}
if (Value('id').of(conjoint1.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/ICD/D43/D43.4'] = conjoint1.personneLieePPLieuNaissanceCommnuneConjointPacse.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/ICD/D44'] = conjoint1.personneLieePPNationaliteConjointPacse;

if (conjoint1.adresseConjointDifferente) {
if (Value('id').of(conjoint1.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/ICD/D45/D45.3']= conjoint1.adresseDomicileConjoint.adresseConjointCommune.getId();
} else if (not Value('id').of(conjoint1.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
		var idPays = conjoint1.adresseDomicileConjoint.adresseConjointPays.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/ICD/D45/D45.3']= result;
}		
if (conjoint1.adresseDomicileConjoint.adresseConjointNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/ICD/D45/D45.5']= conjoint1.adresseDomicileConjoint.adresseConjointNumeroVoie;
}
if (conjoint1.adresseDomicileConjoint.adresseConjointIndiceVoie != null) {
   var monId1 = Value('id').of(conjoint1.adresseDomicileConjoint.adresseConjointIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/ICD/D45/D45.6']          = monId1;  
} 
if (conjoint1.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/ICD/D45/D45.7']= conjoint1.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/ICD/D45/D45.8']= conjoint1.adresseDomicileConjoint.adresseConjointCodePostal != null ? conjoint1.adresseDomicileConjoint.adresseConjointCodePostal : ".";
if (conjoint1.adresseDomicileConjoint.adresseConjointComplementVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/ICD/D45/D45.10']= conjoint1.adresseDomicileConjoint.adresseConjointComplementVoie;
}
if (conjoint1.adresseDomicileConjoint.adresseConjointTypeVoie != null) {
   var monId1 = Value('id').of(conjoint1.adresseDomicileConjoint.adresseConjointTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/ICD/D45/D45.11']          = monId1;  
} 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/ICD/D45/D45.12']= conjoint1.adresseDomicileConjoint.adresseConjointNomVoie;
if 	(Value('id').of(conjoint1.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {																		
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/ICD/D45/D45.13']= conjoint1.adresseDomicileConjoint.adresseConjointCommune.getLabel();
} else if (not Value('id').of(conjoint1.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/ICD/D45/D45.13']= conjoint1.adresseDomicileConjoint.adresseConjointVille;
}
if (not Value('id').of(conjoint1.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/ICD/D45/D45.14']= conjoint1.adresseDomicileConjoint.adresseConjointPays.getLabel();
}
}
}

// Groupe GCS

if (Value('id').of(gerance.typeGerance).eq('entrepriseNatureGeranceMajoritaire') or societe.entrepriseAssocieUniqueGerant) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant1.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/ISS/A10/A10.1']  = nirDeclarant.substring(0, 13)
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/ISS/A10/A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant1.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant1.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant1.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SNS/A21/A21.2']= date;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SNS/A21/A21.3']= socialDirigeant1.voletSocialActiviteExerceeAnterieurementLibelleTNS;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SNS/A21/A21.4']= socialDirigeant1.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SNS/A21/A21.6']= socialDirigeant1.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SNS/A22/A22.3']= Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : 
																												(Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
																												(Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SNS/A22/A22.4']= socialDirigeant1.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SNS/A22/A22.4']= "ENIM";
}
if (socialDirigeant1.voletSocialActiviteSimultaneeOUINON) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SNS/A24/A24.2'] = Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : 
																												(Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
																												(Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
if (Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SNS/A24/A24.3'] = socialDirigeant1.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	
}

// Groupe CAS

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/CAS/A42/A42.1']= ".";
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/CAS/A42/A42.2']= ".";

// Groupe SCS

if (dirigeant1.conjointRole and (Value('id').of(dirigeant1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') or Value('id').of(dirigeant1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie'))) {
if (dirigeant1.conjointRole and Value('id').of(dirigeant1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur')) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SCS/A53/A53.4']= conjoint1.voletSocialConjointCouvertAssuranceMaladieOui ? "O" : "N";
var nirDeclarant = conjoint1.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SCS/A55/A55.1']= nirDeclarant.substring(0, 13);
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SCS/A55/A55.2'] = nirDeclarant.substring(13, 15);
}
}
if (dirigeant1.conjointRole and Value('id').of(dirigeant1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) { 
var nirDeclarant = socialConjoint1.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SCS/A55/A55.1']= nirDeclarant.substring(0, 13);
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SCS/A55/A55.2']= nirDeclarant.substring(13, 15);
}
}
}

// Groupe SAS 

if (dirigeant1.conjointRole and Value('id').of(dirigeant1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SAS/A68/A68.2']= conjoint1.personneLieePPNomNaissanceConjointPacse;
var prenoms=[];
for (i = 0; i < conjoint1.personneLieePPPrenomConjointPacse.size() ; i++ ){
	prenoms.push(conjoint1.personneLieePPPrenomConjointPacse[i]);
} regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SAS/A68/A68.3']= prenoms;
if(conjoint1.personneLieePPNomUsageConjointPacse !== null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SAS/A68/A68.4']= conjoint1.personneLieePPNomUsageConjointPacse;
}
if(conjoint1.personneLieePPDateNaissanceConjointPacse !== null) {
	var dateTemp = new Date(parseInt(conjoint1.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SAS/A69/A69.1']= date;
}	
if (not Value('id').of(conjoint1.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
	var idPays = conjoint1.personneLieePPPaysNaissanceConjointPacse.getId();
	var result = idPays.match(regEx)[0]; 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SAS/A69/A69.2']= result;
} else if (Value('id').of(conjoint1.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SAS/A69/A69.2'] = conjoint1.personneLieePPLieuNaissanceCommnuneConjointPacse.getId();
}		
if (not Value('id').of(conjoint1.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {																					 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SAS/A69/A69.3'] = conjoint1.personneLieePPPaysNaissanceConjointPacse.getLabel();
}
if (Value('id').of(conjoint1.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SAS/A69/A69.4'] = conjoint1.personneLieePPLieuNaissanceCommnuneConjointPacse.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SAS/A70'] = conjoint1.personneLieePPNationaliteConjointPacse;

if (conjoint1.adresseConjointDifferente) {
if (Value('id').of(conjoint1.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SAS/A71/A71.3']= conjoint1.adresseDomicileConjoint.adresseConjointCommune.getId();
} else if (not Value('id').of(conjoint1.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
		var idPays = conjoint1.adresseDomicileConjoint.adresseConjointPays.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SAS/A71/A71.3']= result;
}		
if (conjoint1.adresseDomicileConjoint.adresseConjointNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SAS/A71/A71.5']= conjoint1.adresseDomicileConjoint.adresseConjointNumeroVoie;
}
if (conjoint1.adresseDomicileConjoint.adresseConjointIndiceVoie != null) {
   var monId1 = Value('id').of(conjoint1.adresseDomicileConjoint.adresseConjointIndiceVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SAS/A71/A71.6']          = monId1;  
} 
if (conjoint1.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SAS/A71/A71.7']= conjoint1.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SAS/A71/A71.8']= conjoint1.adresseDomicileConjoint.adresseConjointCodePostal != null ? conjoint1.adresseDomicileConjoint.adresseConjointCodePostal : ".";
if (conjoint1.adresseDomicileConjoint.adresseConjointComplementVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SAS/A71/A71.10']= conjoint1.adresseDomicileConjoint.adresseConjointComplementVoie;
}
if (conjoint1.adresseDomicileConjoint.adresseConjointTypeVoie != null) {
   var monId1 = Value('id').of(conjoint1.adresseDomicileConjoint.adresseConjointTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SAS/A71/A71.11']          = monId1;  
} 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SAS/A71/A71.12']= conjoint1.adresseDomicileConjoint.adresseConjointNomVoie;
if 	(Value('id').of(conjoint1.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {																		
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SAS/A71/A71.13']= conjoint1.adresseDomicileConjoint.adresseConjointCommune.getLabel();
} else if (not Value('id').of(conjoint1.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SAS/A71/A71.13']= conjoint1.adresseDomicileConjoint.adresseConjointVille;
}
if (not Value('id').of(conjoint1.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/SAS/A71/A71.14']= conjoint1.adresseDomicileConjoint.adresseConjointPays.getLabel();
}
}
}

// Groupe MSS

if (societe.entrepriseAssocieUniqueGerant) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/MSS/A31/A31.1'] = societe.optionME ? "O" : "N";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[1]/GCS/MSS/A31/A31.2'] = societe.optionME ? (Value('id').of(socialDirigeant1.periodiciteVersementCotisationMensuelleTrimestrielle).eq('periodiciteVersementCotisationMensuelleTNS') ? "M" : "T") : '.';
}
}


// Gérant 2

if (dirigeant1.autreDirigeant1) { 

// Groupe DIU
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/DIU/U50/U50.1']= "1";
if (dirigeant2.conjointRole) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/DIU/U50/U50.3']= "O";
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/DIU/U51/U51.1']= "30";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/DIU/U52']= "P";

// Groupe IPD
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/IPD/D01/D01.2']= dirigeant2.personneLieePPNomNaissanceGerant;
var prenoms=[];
for (i = 0; i < dirigeant2.personneLieePPPrenomGerant.size() ; i++ ){
	prenoms.push(dirigeant2.personneLieePPPrenomGerant[i]);
} regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/IPD/D01/D01.3']= prenoms;
if (dirigeant2.personneLieePPNomUsageGerant != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/IPD/D01/D01.4']= dirigeant2.personneLieePPNomUsageGerant;
}
if(dirigeant2.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant2.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/IPD/D03/D03.1']= date;
}	
if (not Value('id').of(dirigeant2.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant2.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeant2.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/IPD/D03/D03.2'] = dirigeant2.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant2.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/IPD/D03/D03.3'] = dirigeant2.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant2.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/IPD/D03/D03.4'] = dirigeant2.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/IPD/D04/D04.3']= dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdresseCommune.getId();
} else if (not Value('id').of(dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/IPD/D04/D04.3']= result;
}
if (dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdresserueNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/IPD/D04/D04.5']= dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdresserueNumeroVoie;
}
if (dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/IPD/D04/D04.6']          = monId1;  
} 
if (dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/IPD/D04/D04.7']= dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/IPD/D04/D04.8']= dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdresseCodePostal != null ? dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdresseCodePostal : ".";
if (dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiquerueAdresseComplementAdresse != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/IPD/D04/D04.10']= dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiquerueAdresseComplementAdresse;
}
if (dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/IPD/D04/D04.11']          = monId1;  
} 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/IPD/D04/D04.12']= dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdresseNomVoie;
if 	(Value('id').of(dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {																		
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/IPD/D04/D04.13']= dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/IPD/D04/D04.13']= dirigeant2.cadre2AdresseDeclarant.personneLieeAdresseVille;
}
if (not Value('id').of(dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/IPD/D04/D04.14']= dirigeant2.cadre2AdresseDeclarant.personneLieePersonnePhysiqueAdressePays.getLabel();
}

// Groupe NPD

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/NPD/D21']= dirigeant2.personneLieePPNationaliteGerant;

// Groupe SCD

if (dirigeant2.conjointRole) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/SCD/D40']= Value('id').of(dirigeant2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') ? "1" : 
																										((Value('id').of(dirigeant2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie') or Value('id').of(dirigeant2.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutAssocie')) ? "2" : "3");
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/SCD/D41']= "O";
}

// Groupe ICD

if (dirigeant2.conjointRole and Value('id').of(dirigeant2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/ICD/D42/D42.2']= conjoint2.personneLieePPNomNaissanceConjointPacse;
var prenoms=[];
for (i = 0; i < conjoint2.personneLieePPPrenomConjointPacse.size() ; i++ ){
	prenoms.push(conjoint2.personneLieePPPrenomConjointPacse[i]);
} regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/ICD/D42/D42.3']= prenoms;
if(conjoint2.personneLieePPNomUsageConjointPacse !== null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/ICD/D42/D42.4']= conjoint2.personneLieePPNomUsageConjointPacse;
}
if(conjoint2.personneLieePPDateNaissanceConjointPacse !== null) {
	var dateTemp = new Date(parseInt(conjoint2.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/ICD/D43/D43.1']= date;
}	
if (not Value('id').of(conjoint2.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
	var idPays = conjoint2.personneLieePPPaysNaissanceConjointPacse.getId();
	var result = idPays.match(regEx)[0]; 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/ICD/D43/D43.2']= result;
} else if (Value('id').of(conjoint2.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/ICD/D43/D43.2'] = conjoint2.personneLieePPLieuNaissanceCommnuneConjointPacse.getId();
}		
if (not Value('id').of(conjoint2.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {																					 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/ICD/D43/D43.3'] = conjoint2.personneLieePPPaysNaissanceConjointPacse.getLabel();
}
if (Value('id').of(conjoint2.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/ICD/D43/D43.4'] = conjoint2.personneLieePPLieuNaissanceCommnuneConjointPacse.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/ICD/D44'] = conjoint2.personneLieePPNationaliteConjointPacse;

if (conjoint2.adresseConjointDifferente) {
if (Value('id').of(conjoint2.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/ICD/D45/D45.3']= conjoint2.adresseDomicileConjoint.adresseConjointCommune.getId();
} else if (not Value('id').of(conjoint2.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
		var idPays = conjoint2.adresseDomicileConjoint.adresseConjointPays.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/ICD/D45/D45.3']= result;
}		
if (conjoint2.adresseDomicileConjoint.adresseConjointNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/ICD/D45/D45.5']= conjoint2.adresseDomicileConjoint.adresseConjointNumeroVoie;
}
if (conjoint2.adresseDomicileConjoint.adresseConjointIndiceVoie != null) {
   var monId1 = Value('id').of(conjoint2.adresseDomicileConjoint.adresseConjointIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/ICD/D45/D45.6']          = monId1;  
} 
if (conjoint2.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/ICD/D45/D45.7']= conjoint2.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/ICD/D45/D45.8']= conjoint2.adresseDomicileConjoint.adresseConjointCodePostal != null ? conjoint2.adresseDomicileConjoint.adresseConjointCodePostal : ".";
if (conjoint2.adresseDomicileConjoint.adresseConjointComplementVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/ICD/D45/D45.10']= conjoint2.adresseDomicileConjoint.adresseConjointComplementVoie;
}
if (conjoint2.adresseDomicileConjoint.adresseConjointTypeVoie != null) {
   var monId1 = Value('id').of(conjoint2.adresseDomicileConjoint.adresseConjointTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/ICD/D45/D45.11']          = monId1;  
} 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/ICD/D45/D45.12']= conjoint2.adresseDomicileConjoint.adresseConjointNomVoie;
if 	(Value('id').of(conjoint2.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {																		
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/ICD/D45/D45.13']= conjoint2.adresseDomicileConjoint.adresseConjointCommune.getLabel();
} else if (not Value('id').of(conjoint2.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/ICD/D45/D45.13']= conjoint2.adresseDomicileConjoint.adresseConjointVille;
}
if (not Value('id').of(conjoint2.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/ICD/D45/D45.14']= conjoint2.adresseDomicileConjoint.adresseConjointPays.getLabel();
}
}
}

// Groupe GCS

if (Value('id').of(gerance.typeGerance).eq('entrepriseNatureGeranceMajoritaire')) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant2.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/ISS/A10/A10.1']  = nirDeclarant.substring(0, 13);
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/ISS/A10/A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant2.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant2.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant2.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SNS/A21/A21.2']= date;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SNS/A21/A21.3']= socialDirigeant2.voletSocialActiviteExerceeAnterieurementLibelleTNS;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SNS/A21/A21.4']= socialDirigeant2.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SNS/A21/A21.6']= socialDirigeant2.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SNS/A22/A22.3']= Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : 
																												(Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
																												(Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SNS/A22/A22.4']= socialDirigeant2.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SNS/A22/A22.4']= "ENIM";
}
if (socialDirigeant2.voletSocialActiviteSimultaneeOUINON) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SNS/A24/A24.2'] = Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : 
																												(Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
																												(Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
if (Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SNS/A24/A24.3'] = socialDirigeant2.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	
}

// Groupe CAS

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/CAS/A42/A42.1']= ".";
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/CAS/A42/A42.2']= ".";

// Groupe SCS

if (dirigeant2.conjointRole and (Value('id').of(dirigeant2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') or Value('id').of(dirigeant2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie'))) {
if (dirigeant2.conjointRole and Value('id').of(dirigeant2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur')) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SCS/A53/A53.4']= conjoint2.voletSocialConjointCouvertAssuranceMaladieOui ? "O" : "N";
var nirDeclarant = conjoint2.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SCS/A55/A55.1']= nirDeclarant.substring(0, 13);
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SCS/A55/A55.2'] = nirDeclarant.substring(13, 15);
}
}
if (dirigeant2.conjointRole and Value('id').of(dirigeant2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) { 
var nirDeclarant = socialConjoint2.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SCS/A55/A55.1']= nirDeclarant.substring(0, 13);
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SCS/A55/A55.2']= nirDeclarant.substring(13, 15);
}
}
}

// Groupe SAS 

if (dirigeant2.conjointRole and Value('id').of(dirigeant2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SAS/A68/A68.2']= conjoint2.personneLieePPNomNaissanceConjointPacse;
var prenoms=[];
for (i = 0; i < conjoint2.personneLieePPPrenomConjointPacse.size() ; i++ ){
	prenoms.push(conjoint2.personneLieePPPrenomConjointPacse[i]);
} regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SAS/A68/A68.3']= prenoms;
if(conjoint2.personneLieePPNomUsageConjointPacse !== null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SAS/A68/A68.4']= conjoint2.personneLieePPNomUsageConjointPacse;
}
if(conjoint2.personneLieePPDateNaissanceConjointPacse !== null) {
	var dateTemp = new Date(parseInt(conjoint2.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SAS/A69/A69.1']= date;
}	
if (not Value('id').of(conjoint2.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
	var idPays = conjoint2.personneLieePPPaysNaissanceConjointPacse.getId();
	var result = idPays.match(regEx)[0]; 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SAS/A69/A69.2']= result;
} else if (Value('id').of(conjoint2.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SAS/A69/A69.2'] = conjoint2.personneLieePPLieuNaissanceCommnuneConjointPacse.getId();
}		
if (not Value('id').of(conjoint2.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {																					 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SAS/A69/A69.3'] = conjoint2.personneLieePPPaysNaissanceConjointPacse.getLabel();
}
if (Value('id').of(conjoint2.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SAS/A69/A69.4'] = conjoint2.personneLieePPLieuNaissanceCommnuneConjointPacse.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SAS/A70'] = conjoint2.personneLieePPNationaliteConjointPacse;

if (conjoint2.adresseConjointDifferente) {
if (Value('id').of(conjoint2.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SAS/A71/A71.3']= conjoint2.adresseDomicileConjoint.adresseConjointCommune.getId();
} else if (not Value('id').of(conjoint2.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
		var idPays = conjoint2.adresseDomicileConjoint.adresseConjointPays.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SAS/A71/A71.3']= result;
}		
if (conjoint2.adresseDomicileConjoint.adresseConjointNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SAS/A71/A71.5']= conjoint2.adresseDomicileConjoint.adresseConjointNumeroVoie;
}
if (conjoint2.adresseDomicileConjoint.adresseConjointIndiceVoie != null) {
   var monId1 = Value('id').of(conjoint2.adresseDomicileConjoint.adresseConjointIndiceVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SAS/A71/A71.6']          = monId1;  
} 
if (conjoint2.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SAS/A71/A71.7']= conjoint2.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SAS/A71/A71.8']= conjoint2.adresseDomicileConjoint.adresseConjointCodePostal != null ? conjoint2.adresseDomicileConjoint.adresseConjointCodePostal : ".";
if (conjoint2.adresseDomicileConjoint.adresseConjointComplementVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SAS/A71/A71.10']= conjoint2.adresseDomicileConjoint.adresseConjointComplementVoie;
}
if (conjoint2.adresseDomicileConjoint.adresseConjointTypeVoie != null) {
   var monId1 = Value('id').of(conjoint2.adresseDomicileConjoint.adresseConjointTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SAS/A71/A71.11']          = monId1;  
} 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SAS/A71/A71.12']= conjoint2.adresseDomicileConjoint.adresseConjointNomVoie;
if 	(Value('id').of(conjoint2.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {																		
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SAS/A71/A71.13']= conjoint2.adresseDomicileConjoint.adresseConjointCommune.getLabel();
} else if (not Value('id').of(conjoint2.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SAS/A71/A71.13']= conjoint2.adresseDomicileConjoint.adresseConjointVille;
}
if (not Value('id').of(conjoint2.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[2]/GCS/SAS/A71/A71.14']= conjoint2.adresseDomicileConjoint.adresseConjointPays.getLabel();
}
}
}
}
}

// Gérant 3 

if (dirigeant2.autreDirigeant2) { 

// Groupe DIU
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/DIU/U50/U50.1']= "1";
if (dirigeant3.conjointRole) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/DIU/U50/U50.3']= "O";
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/DIU/U51/U51.1']= "30";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/DIU/U52']= "P";

// Groupe IPD
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/IPD/D01/D01.2']= dirigeant3.personneLieePPNomNaissanceGerant;
var prenoms=[];
for (i = 0; i < dirigeant3.personneLieePPPrenomGerant.size() ; i++ ){
	prenoms.push(dirigeant3.personneLieePPPrenomGerant[i]);
} regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/IPD/D01/D01.3']= prenoms;
if (dirigeant3.personneLieePPNomUsageGerant != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/IPD/D01/D01.4']= dirigeant3.personneLieePPNomUsageGerant;
}
if(dirigeant3.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant3.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/IPD/D03/D03.1']= date;
}	
if (not Value('id').of(dirigeant3.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant3.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeant3.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/IPD/D03/D03.2'] = dirigeant3.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant3.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/IPD/D03/D03.3'] = dirigeant3.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant3.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/IPD/D03/D03.4'] = dirigeant3.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/IPD/D04/D04.3']= dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdresseCommune.getId();
} else if (not Value('id').of(dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/IPD/D04/D04.3']= result;
}
if (dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdresserueNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/IPD/D04/D04.5']= dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdresserueNumeroVoie;
}
if (dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/IPD/D04/D04.6']          = monId1;  
} 
if (dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/IPD/D04/D04.7']= dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/IPD/D04/D04.8']= dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdresseCodePostal != null ? dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdresseCodePostal : ".";
if (dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiquerueAdresseComplementAdresse != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/IPD/D04/D04.10']= dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiquerueAdresseComplementAdresse;
}
if (dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/IPD/D04/D04.11']          = monId1;  
} 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/IPD/D04/D04.12']= dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdresseNomVoie;
if 	(Value('id').of(dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {																		
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/IPD/D04/D04.13']= dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/IPD/D04/D04.13']= dirigeant3.cadre3AdresseDeclarant.personneLieeAdresseVille;
}
if (not Value('id').of(dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/IPD/D04/D04.14']= dirigeant3.cadre3AdresseDeclarant.personneLieePersonnePhysiqueAdressePays.getLabel();
}

// Groupe NPD

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/NPD/D21']= dirigeant3.personneLieePPNationaliteGerant;

// Groupe SCD

if (dirigeant3.conjointRole) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/SCD/D40']= Value('id').of(dirigeant3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') ? "1" : 
																										((Value('id').of(dirigeant3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie') or Value('id').of(dirigeant3.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutAssocie')) ? "2" : "3");
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/SCD/D41']= "O";
}

// Groupe ICD

if (dirigeant3.conjointRole and Value('id').of(dirigeant3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/ICD/D42/D42.2']= conjoint3.personneLieePPNomNaissanceConjointPacse;
var prenoms=[];
for (i = 0; i < conjoint3.personneLieePPPrenomConjointPacse.size() ; i++ ){
	prenoms.push(conjoint3.personneLieePPPrenomConjointPacse[i]);
} regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/ICD/D42/D42.3']= prenoms;
if(conjoint3.personneLieePPNomUsageConjointPacse !== null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/ICD/D42/D42.4']= conjoint3.personneLieePPNomUsageConjointPacse;
}
if(conjoint3.personneLieePPDateNaissanceConjointPacse !== null) {
	var dateTemp = new Date(parseInt(conjoint3.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/ICD/D43/D43.1']= date;
}	
if (not Value('id').of(conjoint3.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
	var idPays = conjoint3.personneLieePPPaysNaissanceConjointPacse.getId();
	var result = idPays.match(regEx)[0]; 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/ICD/D43/D43.2']= result;
} else if (Value('id').of(conjoint3.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/ICD/D43/D43.2'] = conjoint3.personneLieePPLieuNaissanceCommnuneConjointPacse.getId();
}		
if (not Value('id').of(conjoint3.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {																					 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/ICD/D43/D43.3'] = conjoint3.personneLieePPPaysNaissanceConjointPacse.getLabel();
}
if (Value('id').of(conjoint3.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/ICD/D43/D43.4'] = conjoint3.personneLieePPLieuNaissanceCommnuneConjointPacse.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/ICD/D44'] = conjoint3.personneLieePPNationaliteConjointPacse;

if (conjoint3.adresseConjointDifferente) {
if (Value('id').of(conjoint3.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/ICD/D45/D45.3']= conjoint3.adresseDomicileConjoint.adresseConjointCommune.getId();
} else if (not Value('id').of(conjoint3.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
		var idPays = conjoint3.adresseDomicileConjoint.adresseConjointPays.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/ICD/D45/D45.3']= result;
}		
if (conjoint3.adresseDomicileConjoint.adresseConjointNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/ICD/D45/D45.5']= conjoint3.adresseDomicileConjoint.adresseConjointNumeroVoie;
}
if (conjoint3.adresseDomicileConjoint.adresseConjointIndiceVoie != null) {
   var monId1 = Value('id').of(conjoint3.adresseDomicileConjoint.adresseConjointIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/ICD/D45/D45.6']          = monId1;  
} 
if (conjoint3.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/ICD/D45/D45.7']= conjoint3.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/ICD/D45/D45.8']= conjoint3.adresseDomicileConjoint.adresseConjointCodePostal != null ? conjoint3.adresseDomicileConjoint.adresseConjointCodePostal : ".";
if (conjoint3.adresseDomicileConjoint.adresseConjointComplementVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/ICD/D45/D45.10']= conjoint3.adresseDomicileConjoint.adresseConjointComplementVoie;
}
if (conjoint3.adresseDomicileConjoint.adresseConjointTypeVoie != null) {
   var monId1 = Value('id').of(conjoint3.adresseDomicileConjoint.adresseConjointTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/ICD/D45/D45.11']          = monId1;  
} 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/ICD/D45/D45.12']= conjoint3.adresseDomicileConjoint.adresseConjointNomVoie;
if 	(Value('id').of(conjoint3.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {																		
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/ICD/D45/D45.13']= conjoint3.adresseDomicileConjoint.adresseConjointCommune.getLabel();
} else if (not Value('id').of(conjoint3.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/ICD/D45/D45.13']= conjoint3.adresseDomicileConjoint.adresseConjointVille;
}
if (not Value('id').of(conjoint3.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/ICD/D45/D45.14']= conjoint3.adresseDomicileConjoint.adresseConjointPays.getLabel();
}
}
}

// Groupe GCS

if (Value('id').of(gerance.typeGerance).eq('entrepriseNatureGeranceMajoritaire')) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant3.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/ISS/A10/A10.1']  = nirDeclarant.substring(0, 13);
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/ISS/A10/A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant3.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant3.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant3.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SNS/A21/A21.2']= date;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SNS/A21/A21.3']= socialDirigeant3.voletSocialActiviteExerceeAnterieurementLibelleTNS;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SNS/A21/A21.4']= socialDirigeant3.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SNS/A21/A21.6']= socialDirigeant3.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SNS/A22/A22.3']= Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : 
																												(Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
																												(Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SNS/A22/A22.4']= socialDirigeant3.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SNS/A22/A22.4']= "ENIM";
}
if (socialDirigeant3.voletSocialActiviteSimultaneeOUINON) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SNS/A24/A24.2'] = Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : 
																												(Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
																												(Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
if (Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SNS/A24/A24.3'] = socialDirigeant3.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	
}

// Groupe CAS

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/CAS/A42/A42.1']= ".";
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/CAS/A42/A42.2']= ".";

// Groupe SCS

if (dirigeant3.conjointRole and (Value('id').of(dirigeant3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') or Value('id').of(dirigeant3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie'))) {
if (dirigeant3.conjointRole and Value('id').of(dirigeant3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur')) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SCS/A53/A53.4']= conjoint3.voletSocialConjointCouvertAssuranceMaladieOui ? "O" : "N";
var nirDeclarant = conjoint3.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SCS/A55/A55.1']= nirDeclarant.substring(0, 13);
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SCS/A55/A55.2'] = nirDeclarant.substring(13, 15);
}
}
if (dirigeant3.conjointRole and Value('id').of(dirigeant3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) { 
var nirDeclarant = socialConjoint3.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SCS/A55/A55.1']= nirDeclarant.substring(0, 13);
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SCS/A55/A55.2']= nirDeclarant.substring(13, 15);
}
}
}

// Groupe SAS 

if (dirigeant3.conjointRole and Value('id').of(dirigeant3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SAS/A68/A68.2']= conjoint3.personneLieePPNomNaissanceConjointPacse;
var prenoms=[];
for (i = 0; i < conjoint3.personneLieePPPrenomConjointPacse.size() ; i++ ){
	prenoms.push(conjoint3.personneLieePPPrenomConjointPacse[i]);
} regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SAS/A68/A68.3']= prenoms;
if(conjoint3.personneLieePPNomUsageConjointPacse !== null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SAS/A68/A68.4']= conjoint3.personneLieePPNomUsageConjointPacse;
}
if(conjoint3.personneLieePPDateNaissanceConjointPacse !== null) {
	var dateTemp = new Date(parseInt(conjoint3.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SAS/A69/A69.1']= date;
}	
if (not Value('id').of(conjoint3.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
	var idPays = conjoint3.personneLieePPPaysNaissanceConjointPacse.getId();
	var result = idPays.match(regEx)[0]; 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SAS/A69/A69.2']= result;
} else if (Value('id').of(conjoint3.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SAS/A69/A69.2'] = conjoint3.personneLieePPLieuNaissanceCommnuneConjointPacse.getId();
}		
if (not Value('id').of(conjoint3.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {																					 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SAS/A69/A69.3'] = conjoint3.personneLieePPPaysNaissanceConjointPacse.getLabel();
}
if (Value('id').of(conjoint3.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SAS/A69/A69.4'] = conjoint3.personneLieePPLieuNaissanceCommnuneConjointPacse.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SAS/A70'] = conjoint3.personneLieePPNationaliteConjointPacse;

if (conjoint3.adresseConjointDifferente) {
if (Value('id').of(conjoint3.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SAS/A71/A71.3']= conjoint3.adresseDomicileConjoint.adresseConjointCommune.getId();
} else if (not Value('id').of(conjoint3.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
		var idPays = conjoint3.adresseDomicileConjoint.adresseConjointPays.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SAS/A71/A71.3']= result;
}		
if (conjoint3.adresseDomicileConjoint.adresseConjointNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SAS/A71/A71.5']= conjoint3.adresseDomicileConjoint.adresseConjointNumeroVoie;
}
if (conjoint3.adresseDomicileConjoint.adresseConjointIndiceVoie != null) {
   var monId1 = Value('id').of(conjoint3.adresseDomicileConjoint.adresseConjointIndiceVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SAS/A71/A71.6']          = monId1;  
} 
if (conjoint3.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SAS/A71/A71.7']= conjoint3.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SAS/A71/A71.8']= conjoint3.adresseDomicileConjoint.adresseConjointCodePostal != null ? conjoint3.adresseDomicileConjoint.adresseConjointCodePostal : ".";
if (conjoint3.adresseDomicileConjoint.adresseConjointComplementVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SAS/A71/A71.10']= conjoint3.adresseDomicileConjoint.adresseConjointComplementVoie;
}
if (conjoint3.adresseDomicileConjoint.adresseConjointTypeVoie != null) {
   var monId1 = Value('id').of(conjoint3.adresseDomicileConjoint.adresseConjointTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SAS/A71/A71.11']          = monId1;  
} 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SAS/A71/A71.12']= conjoint3.adresseDomicileConjoint.adresseConjointNomVoie;
if 	(Value('id').of(conjoint3.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {																		
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SAS/A71/A71.13']= conjoint3.adresseDomicileConjoint.adresseConjointCommune.getLabel();
} else if (not Value('id').of(conjoint3.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SAS/A71/A71.13']= conjoint3.adresseDomicileConjoint.adresseConjointVille;
}
if (not Value('id').of(conjoint3.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[3]/GCS/SAS/A71/A71.14']= conjoint3.adresseDomicileConjoint.adresseConjointPays.getLabel();
}
}
}
}
}

// Gérant 4

if (dirigeant3.autreDirigeant3) { 

// Groupe DIU
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/DIU/U50/U50.1']= "1";
if (dirigeant4.conjointRole) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/DIU/U50/U50.3']= "O";
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/DIU/U51/U51.1']= "30";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/DIU/U52']= "P";

// Groupe IPD
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/IPD/D01/D01.2']= dirigeant4.personneLieePPNomNaissanceGerant;
var prenoms=[];
for (i = 0; i < dirigeant4.personneLieePPPrenomGerant.size() ; i++ ){
	prenoms.push(dirigeant4.personneLieePPPrenomGerant[i]);
} regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/IPD/D01/D01.3']= prenoms;
if (dirigeant4.personneLieePPNomUsageGerant != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/IPD/D01/D01.4']= dirigeant4.personneLieePPNomUsageGerant;
}
if(dirigeant4.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant4.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/IPD/D03/D03.1']= date;
}	
if (not Value('id').of(dirigeant4.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant4.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeant4.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/IPD/D03/D03.2'] = dirigeant4.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant4.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/IPD/D03/D03.3'] = dirigeant4.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant4.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/IPD/D03/D03.4'] = dirigeant4.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/IPD/D04/D04.3']= dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdresseCommune.getId();
} else if (not Value('id').of(dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/IPD/D04/D04.3']= result;
}
if (dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdresserueNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/IPD/D04/D04.5']= dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdresserueNumeroVoie;
}
if (dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/IPD/D04/D04.6']          = monId1;  
} 
if (dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/IPD/D04/D04.7']= dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdresseDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/IPD/D04/D04.8']= dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdresseCodePostal != null ? dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdresseCodePostal : ".";
if (dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiquerueAdresseComplementAdresse != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/IPD/D04/D04.10']= dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiquerueAdresseComplementAdresse;
}
if (dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/IPD/D04/D04.11']          = monId1;  
} 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/IPD/D04/D04.12']= dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdresseNomVoie;
if 	(Value('id').of(dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {																		
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/IPD/D04/D04.13']= dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/IPD/D04/D04.13']= dirigeant4.cadre4AdresseDeclarant.personneLieeAdresseVille;
}
if (not Value('id').of(dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/IPD/D04/D04.14']= dirigeant4.cadre4AdresseDeclarant.personneLieePersonnePhysiqueAdressePays.getLabel();
}

// Groupe NPD

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/NPD/D21']= dirigeant4.personneLieePPNationaliteGerant;

// Groupe SCD

if (dirigeant4.conjointRole) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/SCD/D40']= Value('id').of(dirigeant4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') ? "1" : 
																										((Value('id').of(dirigeant4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie') or Value('id').of(dirigeant4.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutAssocie')) ? "2" : "3");
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/SCD/D41']= "O";
}

// Groupe ICD

if (dirigeant4.conjointRole and Value('id').of(dirigeant4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/ICD/D42/D42.2']= conjoint4.personneLieePPNomNaissanceConjointPacse;
var prenoms=[];
for (i = 0; i < conjoint4.personneLieePPPrenomConjointPacse.size() ; i++ ){
	prenoms.push(conjoint4.personneLieePPPrenomConjointPacse[i]);
} regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/ICD/D42/D42.3']= prenoms;
if(conjoint4.personneLieePPNomUsageConjointPacse !== null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/ICD/D42/D42.4']= conjoint4.personneLieePPNomUsageConjointPacse;
}
if(conjoint4.personneLieePPDateNaissanceConjointPacse !== null) {
	var dateTemp = new Date(parseInt(conjoint4.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/ICD/D43/D43.1']= date;
}	
if (not Value('id').of(conjoint4.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
	var idPays = conjoint4.personneLieePPPaysNaissanceConjointPacse.getId();
	var result = idPays.match(regEx)[0]; 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/ICD/D43/D43.2']= result;
} else if (Value('id').of(conjoint4.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/ICD/D43/D43.2'] = conjoint4.personneLieePPLieuNaissanceCommnuneConjointPacse.getId();
}		
if (not Value('id').of(conjoint4.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {																					 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/ICD/D43/D43.3'] = conjoint4.personneLieePPPaysNaissanceConjointPacse.getLabel();
}
if (Value('id').of(conjoint4.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/ICD/D43/D43.4'] = conjoint4.personneLieePPLieuNaissanceCommnuneConjointPacse.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/ICD/D44'] = conjoint4.personneLieePPNationaliteConjointPacse;

if (conjoint4.adresseConjointDifferente) {
if (Value('id').of(conjoint4.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/ICD/D45/D45.3']= conjoint4.adresseDomicileConjoint.adresseConjointCommune.getId();
} else if (not Value('id').of(conjoint4.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
		var idPays = conjoint4.adresseDomicileConjoint.adresseConjointPays.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/ICD/D45/D45.3']= result;
}		
if (conjoint4.adresseDomicileConjoint.adresseConjointNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/ICD/D45/D45.5']= conjoint4.adresseDomicileConjoint.adresseConjointNumeroVoie;
}
if (conjoint4.adresseDomicileConjoint.adresseConjointIndiceVoie != null) {
   var monId1 = Value('id').of(conjoint4.adresseDomicileConjoint.adresseConjointIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/ICD/D45/D45.6']          = monId1;  
} 
if (conjoint4.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/ICD/D45/D45.7']= conjoint4.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/ICD/D45/D45.8']= conjoint4.adresseDomicileConjoint.adresseConjointCodePostal != null ? conjoint4.adresseDomicileConjoint.adresseConjointCodePostal : ".";
if (conjoint4.adresseDomicileConjoint.adresseConjointComplementVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/ICD/D45/D45.10']= conjoint4.adresseDomicileConjoint.adresseConjointComplementVoie;
}
if (conjoint4.adresseDomicileConjoint.adresseConjointTypeVoie != null) {
   var monId1 = Value('id').of(conjoint4.adresseDomicileConjoint.adresseConjointTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/ICD/D45/D45.11']          = monId1;  
} 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/ICD/D45/D45.12']= conjoint4.adresseDomicileConjoint.adresseConjointNomVoie;
if 	(Value('id').of(conjoint4.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {																		
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/ICD/D45/D45.13']= conjoint4.adresseDomicileConjoint.adresseConjointCommune.getLabel();
} else if (not Value('id').of(conjoint4.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/ICD/D45/D45.13']= conjoint4.adresseDomicileConjoint.adresseConjointVille;
}
if (not Value('id').of(conjoint4.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/ICD/D45/D45.14']= conjoint4.adresseDomicileConjoint.adresseConjointPays.getLabel();
}
}
}

// Groupe GCS

if (Value('id').of(gerance.typeGerance).eq('entrepriseNatureGeranceMajoritaire')) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant4.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/ISS/A10/A10.1']  = nirDeclarant.substring(0, 13);
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/ISS/A10/A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant4.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant4.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant4.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SNS/A21/A21.2']= date;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SNS/A21/A21.3']= socialDirigeant4.voletSocialActiviteExerceeAnterieurementLibelleTNS;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SNS/A21/A21.4']= socialDirigeant4.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SNS/A21/A21.6']= socialDirigeant4.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SNS/A22/A22.3']= Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : 
																												(Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
																												(Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SNS/A22/A22.4']= socialDirigeant4.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SNS/A22/A22.4']= "ENIM";
}
if (socialDirigeant4.voletSocialActiviteSimultaneeOUINON) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SNS/A24/A24.2'] = Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : 
																												(Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
																												(Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
if (Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SNS/A24/A24.3'] = socialDirigeant4.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	
}

// Groupe CAS

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/CAS/A42/A42.1']= ".";
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/CAS/A42/A42.2']= ".";

// Groupe SCS

if (dirigeant4.conjointRole and (Value('id').of(dirigeant4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') or Value('id').of(dirigeant4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie'))) {
if (dirigeant4.conjointRole and Value('id').of(dirigeant4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur')) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SCS/A53/A53.4']= conjoint4.voletSocialConjointCouvertAssuranceMaladieOui ? "O" : "N";
var nirDeclarant = conjoint4.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SCS/A55/A55.1']= nirDeclarant.substring(0, 13);
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SCS/A55/A55.2'] = nirDeclarant.substring(13, 15);
}
}
if (dirigeant4.conjointRole and Value('id').of(dirigeant4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) { 
var nirDeclarant = socialConjoint4.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SCS/A55/A55.1']= nirDeclarant.substring(0, 13);
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SCS/A55/A55.2']= nirDeclarant.substring(13, 15);
}
}
}

// Groupe SAS 

if (dirigeant4.conjointRole and Value('id').of(dirigeant4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SAS/A68/A68.2']= conjoint4.personneLieePPNomNaissanceConjointPacse;
var prenoms=[];
for (i = 0; i < conjoint4.personneLieePPPrenomConjointPacse.size() ; i++ ){
	prenoms.push(conjoint4.personneLieePPPrenomConjointPacse[i]);
} regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SAS/A68/A68.3']= prenoms;
if(conjoint4.personneLieePPNomUsageConjointPacse !== null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SAS/A68/A68.4']= conjoint4.personneLieePPNomUsageConjointPacse;
}
if(conjoint4.personneLieePPDateNaissanceConjointPacse !== null) {
	var dateTemp = new Date(parseInt(conjoint4.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SAS/A69/A69.1']= date;
}	
if (not Value('id').of(conjoint4.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
	var idPays = conjoint4.personneLieePPPaysNaissanceConjointPacse.getId();
	var result = idPays.match(regEx)[0]; 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SAS/A69/A69.2']= result;
} else if (Value('id').of(conjoint4.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SAS/A69/A69.2'] = conjoint4.personneLieePPLieuNaissanceCommnuneConjointPacse.getId();
}		
if (not Value('id').of(conjoint4.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {																					 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SAS/A69/A69.3'] = conjoint4.personneLieePPPaysNaissanceConjointPacse.getLabel();
}
if (Value('id').of(conjoint4.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SAS/A69/A69.4'] = conjoint4.personneLieePPLieuNaissanceCommnuneConjointPacse.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SAS/A70'] = conjoint4.personneLieePPNationaliteConjointPacse;

if (conjoint4.adresseConjointDifferente) {
if (Value('id').of(conjoint4.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SAS/A71/A71.3']= conjoint4.adresseDomicileConjoint.adresseConjointCommune.getId();
} else if (not Value('id').of(conjoint4.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
		var idPays = conjoint4.adresseDomicileConjoint.adresseConjointPays.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SAS/A71/A71.3']= result;
}		
if (conjoint4.adresseDomicileConjoint.adresseConjointNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SAS/A71/A71.5']= conjoint4.adresseDomicileConjoint.adresseConjointNumeroVoie;
}
if (conjoint4.adresseDomicileConjoint.adresseConjointIndiceVoie != null) {
   var monId1 = Value('id').of(conjoint4.adresseDomicileConjoint.adresseConjointIndiceVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SAS/A71/A71.6']          = monId1;  
} 
if (conjoint4.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SAS/A71/A71.7']= conjoint4.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SAS/A71/A71.8']= conjoint4.adresseDomicileConjoint.adresseConjointCodePostal != null ? conjoint4.adresseDomicileConjoint.adresseConjointCodePostal : ".";
if (conjoint4.adresseDomicileConjoint.adresseConjointComplementVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SAS/A71/A71.10']= conjoint4.adresseDomicileConjoint.adresseConjointComplementVoie;
}
if (conjoint4.adresseDomicileConjoint.adresseConjointTypeVoie != null) {
   var monId1 = Value('id').of(conjoint4.adresseDomicileConjoint.adresseConjointTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SAS/A71/A71.11']          = monId1;  
} 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SAS/A71/A71.12']= conjoint4.adresseDomicileConjoint.adresseConjointNomVoie;
if 	(Value('id').of(conjoint4.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {																		
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SAS/A71/A71.13']= conjoint4.adresseDomicileConjoint.adresseConjointCommune.getLabel();
} else if (not Value('id').of(conjoint4.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SAS/A71/A71.13']= conjoint4.adresseDomicileConjoint.adresseConjointVille;
}
if (not Value('id').of(conjoint4.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[4]/GCS/SAS/A71/A71.14']= conjoint4.adresseDomicileConjoint.adresseConjointPays.getLabel();
}
}
}
}
}

// Fin Regent 02M

if (siege.formaliteCreationAvecActivite) {

// Groupe ICE : Identification complète de l'établissement

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E01']= "1";
if (siege.etablissementPrincipalDifferentSiege) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.3']= etablissement.cadreAdresseEtablissement.communeAdresseEtablissement.getId();
} else if (not siege.etablissementPrincipalDifferentSiege) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.3']= siege.cadreAdresseProfessionnelle.communeAdresseSiege.getId();	
}
if (siege.etablissementPrincipalDifferentSiege and etablissement.cadreAdresseEtablissement.numeroAdresseEtablissement != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5']= etablissement.cadreAdresseEtablissement.numeroAdresseEtablissement;
} else if (not siege.etablissementPrincipalDifferentSiege and siege.cadreAdresseProfessionnelle.numeroAdresseSiege != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5']= siege.cadreAdresseProfessionnelle.numeroAdresseSiege;
}
if (siege.etablissementPrincipalDifferentSiege and etablissement.cadreAdresseEtablissement.indiceAdresseEtablissement != null) {
   var monId1 = Value('id').of(etablissement.cadreAdresseEtablissement.indiceAdresseEtablissement)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.6']          = monId1;  
} else if (not siege.etablissementPrincipalDifferentSiege and siege.cadreAdresseProfessionnelle.indiceAdresseSiege != null) {
   var monId1 = Value('id').of(siege.cadreAdresseProfessionnelle.indiceAdresseSiege)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.6']          = monId1;  
}
if (siege.etablissementPrincipalDifferentSiege and etablissement.cadreAdresseEtablissement.distributionSpecialeAdresseEtablissement != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.7']= etablissement.cadreAdresseEtablissement.distributionSpecialeAdresseEtablissement;
} else if (not siege.etablissementPrincipalDifferentSiege and siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.7']= siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege;
}
if (siege.etablissementPrincipalDifferentSiege) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.8']= etablissement.cadreAdresseEtablissement.codePostalAdresseEtablissement;
} else if (not siege.etablissementPrincipalDifferentSiege) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.8']= siege.cadreAdresseProfessionnelle.codePostalAdresseSiege;
}
if (siege.etablissementPrincipalDifferentSiege and etablissement.cadreAdresseEtablissement.complementAdresseEtablissement != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.10']= etablissement.cadreAdresseEtablissement.complementAdresseEtablissement;
} else if (not siege.etablissementPrincipalDifferentSiege and siege.cadreAdresseProfessionnelle.complementAdresseSiege != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.10']= siege.cadreAdresseProfessionnelle.complementAdresseSiege;
}
if (siege.etablissementPrincipalDifferentSiege and etablissement.cadreAdresseEtablissement.typeAdresseEtablissement != null) {
   var monId1 = Value('id').of(etablissement.cadreAdresseEtablissement.typeAdresseEtablissement)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.11']          = monId1;  
} else if (not siege.etablissementPrincipalDifferentSiege and siege.cadreAdresseProfessionnelle.typeAdresseSiege != null) {
   var monId1 = Value('id').of(siege.cadreAdresseProfessionnelle.typeAdresseSiege)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.11']          = monId1;  
}
if (siege.etablissementPrincipalDifferentSiege) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.12']= etablissement.cadreAdresseEtablissement.voieAdresseEtablissement;
} else if (not siege.etablissementPrincipalDifferentSiege) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.12']= siege.cadreAdresseProfessionnelle.voieAdresseSiege;
}
if (siege.etablissementPrincipalDifferentSiege) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.13']= etablissement.cadreAdresseEtablissement.communeAdresseEtablissement.getLabel();
} else if (not siege.etablissementPrincipalDifferentSiege) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.13']= siege.cadreAdresseProfessionnelle.communeAdresseSiege.getLabel();	
}
if (siege.etablissementPrincipalDifferentSiege) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E04']= "3";
} else if (not siege.etablissementPrincipalDifferentSiege) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E04']= "2";	
}
if (etablissement.etablissementEnseigne != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E05']= etablissement.etablissementEnseigne;
}
if (etablissement.etablissementNomCommercialProfessionnel != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E09']= etablissement.etablissementNomCommercialProfessionnel;
}

// Groupe ORE : Origine de l'établissement

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.1']= (Value('id').of(origine.etablisementOrigineFondsLiberal).eq('EtablissementOrigineFondsCreation') or Value('id').of(origine.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsCreation') or ((Value('id').of(siege.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') or Value('id').of(siege.activiteLieu).eq('etablissementDomiciliationOui')) and not siege.etablissementPrincipalDifferentSiege)) ? "1" : 
																							(Value('id').of(origine.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsAchat') ? "3" : 
																							(Value('id').of(origine.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsApport') ? "4" :
																							(Value('id').of(origine.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsLocationGerance') ? "6" :
																							(Value('id').of(origine.etablisementOrigineFondsLiberal).eq('EtablissementOrigineFondsRepriseActiviteLiberale') ? "8" :
																							(Value('id').of(origine.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsGeranceMandat') ? "F" : "9")))));
if (Value('id').of(origine.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsCocheAutre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.2']= origine.etablissementOrigineFondsAutre;	
}
if ((Value('id').of(origine.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsAchat') or Value('id').of(origine.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsApport')) and (origine.precedentExploitant.etablissementJournalAnnoncesLegalesNom != null or origine.precedentExploitant.etablissementJournalAnnoncesLegalesNomBis != null)) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E22/E22.1']= origine.precedentExploitant.etablissementJournalAnnoncesLegalesNom != null ? origine.precedentExploitant.etablissementJournalAnnoncesLegalesNom : origine.precedentExploitant.etablissementJournalAnnoncesLegalesNomBis;	
if(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParution !== null) {
	var dateTemp = new Date(parseInt(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParution.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E22/E22.2']= date;
} else if(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParutionBis !== null) {
	var dateTemp = new Date(parseInt(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParutionBis.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E22/E22.2']= date;
}	
}

// Groupe PEE : Précédent exploitant de l'établissement

if (Value('id').of(origine.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsAchat') or Value('id').of(origine.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsApport') or Value('id').of(origine.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsLocationGerance')) {
if (origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.1']= origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant.split(' ').join('');
}
if (Value('id').of(origine.precedentExploitant.typePersonnePrecedentExploitant).eq('typePersonnePrecedentExploitantPP')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.2']= origine.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant;
if (origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.3']=  origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.4']= origine.precedentExploitant.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant;
}	
if (Value('id').of(origine.precedentExploitant.typePersonnePrecedentExploitant).eq('typePersonnePrecedentExploitantPM')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.7']= origine.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant;
}
}

// Groupe LGE

if(Value('id').of(origine.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsLocationGerance') or Value('id').of(origine.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsGeranceMandat')){
if(origine.geranceMandat.etablissementLoueurMandantDuFondsDebutContrat !== null) {
	var dateTemp = new Date(parseInt(origine.geranceMandat.etablissementLoueurMandantDuFondsDebutContrat.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ;
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E61/E61.1'] = date;
}
if(origine.geranceMandat.etablissementLoueurMandantDuFondsFinContrat !== null) {
	var dateTemp = new Date(parseInt(origine.geranceMandat.etablissementLoueurMandantDuFondsFinContrat.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ;
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E61/E61.2'] = date;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E61/E61.3'] = origine.geranceMandat.etablissementLoueurMandantDuFondsRenouvellementTaciteReconductionOui ? "O" : "N";
if (Value('id').of(origine.geranceMandat.typePersonneLoueurMandant).eq('typePersonneLoueurMandantPP')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E62'] = origine.geranceMandat.entrepriseLieeEntreprisePMNomNaissanceLoueurMandantDuFonds;
if (origine.geranceMandat.entrepriseLieeEntreprisePPNomUsageLoueurMandantDuFonds != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E64'] = origine.geranceMandat.entrepriseLieeEntreprisePPNomUsageLoueurMandantDuFonds;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E65']= origine.geranceMandat.entrepriseLieeEntreprisePPPrenom1LoueurMandantDuFonds;
}
if (Value('id').of(origine.geranceMandat.typePersonneLoueurMandant).eq('typePersonneLoueurMandantPM')){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E66']=origine.geranceMandat.entrepriseLieeEntreprisePMDenominationLoueurMandantDuFonds;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.3'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.communeLoueurMandantDuFonds.getId();
if (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.numRueAdresseLoueurMandantDufonds != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.5'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.numRueAdresseLoueurMandantDufonds; 
}
if (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.indiceVoieLoueurMandantDuFonds != null) {																							
	var monId1 = Value('id').of(origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.indiceVoieLoueurMandantDuFonds)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.6']=monId1;
}
if (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.distributionSpecialeLoueurMandantDuFonds != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.7'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.distributionSpecialeLoueurMandantDuFonds;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.8'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.codePostalLoueurMandantDuFonds;
if (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.rueComplementAdresseLoueurMandantDuFonds != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.10'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.rueComplementAdresseLoueurMandantDuFonds;
}
if (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.typeVoieLoueurMandantDuFonds != null) {			
	var typeVoie = Value('id').of(origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.typeVoieLoueurMandantDuFonds)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.11']=typeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.12'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.nomVoieLoueurMandantDuFonds;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.13'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.communeLoueurMandantDuFonds.getLabel();
if (Value('id').of(origine.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsGeranceMandat')){
if (origine.geranceMandat.entrepriseLieeSirenGeranceMandat != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E67']=origine.geranceMandat.entrepriseLieeSirenGeranceMandat.split(' ').join('');
}
	//Récupérer le EntityId du greffe
	var greffeId = origine.geranceMandat.entrepriseLieeGreffeImmatriculation.id ;
	//Appeler Directory pour récupérer l'autorité
	_log.info('l entityId du greffe selectionne est {}', greffeId);
	var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', greffeId) //
	           .connectionTimeout(10000) //
	           .receiveTimeout(10000) //
	           .accept('json') //
	           .get();
	//result
	var receiverInfo = response.asObject();
	var contactInfo = !receiverInfo.details ? null : receiverInfo.details;
	//Récupérer le code EDI
	var codeEDIGreffe = !contactInfo.ediCode ? null : contactInfo.ediCode;
	_log.info('le code edi du greffe selectionne est {}', codeEDIGreffe);
	// À modifier quand Destiny fourni le réferentiel
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E68']=codeEDIGreffe;
}
}

// Groupe ACE : Activité de l'établissement

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E70']= sanitize(etablissement.etablissementActivites);
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E71']= etablissement.etablissementActivitesLaPlusImportante != null ? sanitize(etablissement.etablissementActivitesLaPlusImportante) : sanitize(etablissement.etablissementActivites.substring(0, 140));

// Groupe CAE : Complément sur l'activité de l'établissement

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E73/E73.1']= Value('id').of(etablissement.activiteTemps).eq('etablissementActivitePermanenteSaisonnierePermanente') ? "P" : "S";
if (etablissement.etablissementNonSedentariteQualiteAmbulant) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E73/E73.2']= "A";
}
if (Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMagasin') or Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMarche') or Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceDetail')) { 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.1']= "10";
} else if (Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceGros')){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.1']= "09";
} else if (Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureFabricationProduction')){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.1']= "04";
} else if (Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureBatTravauxPublics')){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.1']= "14";
} else if (Value('id').of(societe.entrepriseFormeJuridique).eq('selarl') or siege.activiteLiberaleOui)	 {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.1']= "11";
} else if (Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCocheAutre')){
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.1']= "99";
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.2']= etablissement.etablissementActiviteNatureAutre.substring(0,50);
}
if (Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMagasin')) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E77/E77.1']= "05";
function lpad(str, padString, length) {
  var ch = str;
  while (ch.length < length) {
      ch = padString + ch;
  }
  log.info("lpad : "+ch);
  return ch;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E77/E77.3']= lpad(etablissement.etablissementActiviteLieuExerciceMagasinSurface.toString(), "0", 5);
} else if (Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMarche')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E77/E77.1']= "92";
} else if (Value('id').of(etablissement.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceDetail')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E77/E77.1']= "99";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E77/E77.2']= "sur internet";
}

// Groupe SAE : Salariés de l'établissement

if (etablissement.etablissementEffectifSalariePresenceOui and etablissement.cadre3EffectifSalarie.etablissementEffectifSalarieEmbauchePremierSalarieOui){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E83/E83.1']="1";
}	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E84']=etablissement.etablissementEffectifSalariePresenceOui ? etablissement.cadre3EffectifSalarie.etablissementEffectifSalarieNombre : "0";
if (etablissement.cadre3EffectifSalarie.etablissementEffectifSalarieApprentis != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E85/E85.8']= etablissement.cadre3EffectifSalarie.etablissementEffectifSalarieApprentis;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E86']= etablissement.etablissementEffectifSalariePresenceOui ? "O" : "N";

// Groupe IDE : Identification de la personne liée à l'établissement

// Fondé de pouvoir 1	

if (fondePouvoir.personneLieePersonneLieeEtablissement) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E91/E91.1']= "1";
if(etablissement.etablissementDateDebutActivite !== null) {
    var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
    var year = dateTemp.getFullYear();
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
    var date = year + "-" + month + "-" + day ;
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E91/E91.2']= date;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E92/E92.1']= fondePouvoir1.personneLieePersonnePouvoirLimiteEtablissementOui1 ? "P" : "S";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E93/E93.2']= fondePouvoir1.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
if (fondePouvoir1.personneLieePersonnePhysiqueNomUsagePersonnePouvoir) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E93/E93.4']= fondePouvoir1.personneLieePersonnePhysiqueNomUsagePersonnePouvoir;
}
var prenoms=[];
for (i = 0; i < fondePouvoir1.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){
	prenoms.push(fondePouvoir1.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);
}  regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E93/E93.3']= prenoms;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.3']= fondePouvoir1.cadre6AdressePersonnePouvoir.personneLieeAdresseCommunepersonnePouvoir.getId();
if (fondePouvoir1.cadre6AdressePersonnePouvoir.rueNumeroAdressePersonnePouvoir != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.5']= fondePouvoir1.cadre6AdressePersonnePouvoir.rueNumeroAdressePersonnePouvoir;
}
if (fondePouvoir1.cadre6AdressePersonnePouvoir.indiceVoiePersonnePouvoir != null) {
   var monId1 = Value('id').of(fondePouvoir1.cadre6AdressePersonnePouvoir.indiceVoiePersonnePouvoir)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.6']          = monId1;  
} 
if (fondePouvoir1.cadre6AdressePersonnePouvoir.distriutionSpecialePersonnePouvoir != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.7']= fondePouvoir1.cadre6AdressePersonnePouvoir.distriutionSpecialePersonnePouvoir;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.8']= fondePouvoir1.cadre6AdressePersonnePouvoir.personneLieeAdresseCodePostalPersonnePouvoir;
if (fondePouvoir1.cadre6AdressePersonnePouvoir.rueComplementAdressePersonnePouvoir != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.10']= fondePouvoir1.cadre6AdressePersonnePouvoir.rueComplementAdressePersonnePouvoir;
}
if (fondePouvoir1.cadre6AdressePersonnePouvoir.typeVoiePersonnePouvoir != null) {
   var monId1 = Value('id').of(fondePouvoir1.cadre6AdressePersonnePouvoir.typeVoiePersonnePouvoir)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.11']          = monId1;  
} 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.12']= fondePouvoir1.cadre6AdressePersonnePouvoir.nomVoiePersonnePouvoir;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.13'] = fondePouvoir1.cadre6AdressePersonnePouvoir.personneLieeAdresseCommunepersonnePouvoir.getLabel();
if (fondePouvoir1.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir !== null) {
	var dateTemp = new Date(parseInt(fondePouvoir1.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E95/E95.1']= date;
}		
if (not Value('id').of(fondePouvoir1.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
	var idPays = fondePouvoir1.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E95/E95.2']= result;
} else if (Value('id').of(fondePouvoir1.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E95/E95.2'] = fondePouvoir1.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getId();
}		
if (not Value('id').of(fondePouvoir1.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {																					 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E95/E95.3'] = fondePouvoir1.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getLabel();
}
if (Value('id').of(fondePouvoir1.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E95/E95.4'] = fondePouvoir1.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E96']= fondePouvoir1.personneLieePersonnePhysiqueNationalitePersonnePouvoir;
}

// Fondé de pouvoir 2	

if (fondePouvoir1.autreDeclarationPersonneLiee) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E91/E91.1']= "1";
if(etablissement.etablissementDateDebutActivite !== null) {
    var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
    var year = dateTemp.getFullYear();
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
    var date = year + "-" + month + "-" + day ;
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E91/E91.2']= date;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E92/E92.1']= fondePouvoir2.personneLieePersonnePouvoirLimiteEtablissementOui2 ? "P" : "S";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E93/E93.2']= fondePouvoir2.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
if (fondePouvoir2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E93/E93.4']= fondePouvoir2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir;
}
var prenoms=[];
for (i = 0; i < fondePouvoir2.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){
	prenoms.push(fondePouvoir2.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);
}  regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E93/E93.3']= prenoms;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.3']=fondePouvoir2.cadre6AdressePersonnePouvoir.personneLieeAdresseCommunepersonnePouvoir.getId();
if (fondePouvoir2.cadre6AdressePersonnePouvoir.rueNumeroAdressePersonnePouvoir != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.5']= fondePouvoir2.cadre6AdressePersonnePouvoir.rueNumeroAdressePersonnePouvoir;
}
if (fondePouvoir2.cadre6AdressePersonnePouvoir.indiceVoiePersonnePouvoir != null) {
   var monId1 = Value('id').of(fondePouvoir2.cadre6AdressePersonnePouvoir.indiceVoiePersonnePouvoir)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.6']          = monId1;  
} 
if (fondePouvoir2.cadre6AdressePersonnePouvoir.distriutionSpecialePersonnePouvoir != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.7']= fondePouvoir2.cadre6AdressePersonnePouvoir.distriutionSpecialePersonnePouvoir;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.8']= fondePouvoir2.cadre6AdressePersonnePouvoir.personneLieeAdresseCodePostalPersonnePouvoir;
if (fondePouvoir2.cadre6AdressePersonnePouvoir.rueComplementAdressePersonnePouvoir != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.10']= fondePouvoir2.cadre6AdressePersonnePouvoir.rueComplementAdressePersonnePouvoir;
}
if (fondePouvoir2.cadre6AdressePersonnePouvoir.typeVoiePersonnePouvoir != null) {
   var monId1 = Value('id').of(fondePouvoir2.cadre6AdressePersonnePouvoir.typeVoiePersonnePouvoir)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.11']          = monId1;  
} 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.12']= fondePouvoir2.cadre6AdressePersonnePouvoir.nomVoiePersonnePouvoir;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.13'] = fondePouvoir2.cadre6AdressePersonnePouvoir.personneLieeAdresseCommunepersonnePouvoir.getLabel();
if (fondePouvoir2.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir !== null) {
	var dateTemp = new Date(parseInt(fondePouvoir2.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E95/E95.1']= date;
}		
if (not Value('id').of(fondePouvoir2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
	var idPays = fondePouvoir2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E95/E95.2']= result;
} else if (Value('id').of(fondePouvoir2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E95/E95.2'] = fondePouvoir2.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getId();
}		
if (not Value('id').of(fondePouvoir2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {																					 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E95/E95.3'] = fondePouvoir2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getLabel();
}
if (Value('id').of(fondePouvoir2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E95/E95.4'] = fondePouvoir2.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E96']= fondePouvoir2.personneLieePersonnePhysiqueNationalitePersonnePouvoir;
}

// Fondé de pouvoir 3	

if (fondePouvoir2.autreDeclarationPersonneLiee2) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E91/E91.1']= "1";
if(etablissement.etablissementDateDebutActivite !== null) {
    var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
    var year = dateTemp.getFullYear();
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
    var date = year + "-" + month + "-" + day ;
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E91/E91.2']= date;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E92/E92.1']= fondePouvoir3.personneLieePersonnePouvoirLimiteEtablissementOui3 ? "P" : "S";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E93/E93.2']= fondePouvoir3.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
if (fondePouvoir3.personneLieePersonnePhysiqueNomUsagePersonnePouvoir) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E93/E93.4']= fondePouvoir3.personneLieePersonnePhysiqueNomUsagePersonnePouvoir;
}
var prenoms=[];
for (i = 0; i < fondePouvoir3.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){
	prenoms.push(fondePouvoir3.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);
}  regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E93/E93.3']= prenoms;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.3']=fondePouvoir3.cadre6AdressePersonnePouvoir.personneLieeAdresseCommunepersonnePouvoir.getId();
if (fondePouvoir3.cadre6AdressePersonnePouvoir.rueNumeroAdressePersonnePouvoir != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.5']= fondePouvoir3.cadre6AdressePersonnePouvoir.rueNumeroAdressePersonnePouvoir;
}
if (fondePouvoir3.cadre6AdressePersonnePouvoir.indiceVoiePersonnePouvoir != null) {
   var monId1 = Value('id').of(fondePouvoir3.cadre6AdressePersonnePouvoir.indiceVoiePersonnePouvoir)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.6']          = monId1;  
} 
if (fondePouvoir3.cadre6AdressePersonnePouvoir.distriutionSpecialePersonnePouvoir != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.7']= fondePouvoir3.cadre6AdressePersonnePouvoir.distriutionSpecialePersonnePouvoir;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.8']= fondePouvoir3.cadre6AdressePersonnePouvoir.personneLieeAdresseCodePostalPersonnePouvoir;
if (fondePouvoir3.cadre6AdressePersonnePouvoir.rueComplementAdressePersonnePouvoir != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.10']= fondePouvoir3.cadre6AdressePersonnePouvoir.rueComplementAdressePersonnePouvoir;
}
if (fondePouvoir3.cadre6AdressePersonnePouvoir.typeVoiePersonnePouvoir != null) {
   var monId1 = Value('id').of(fondePouvoir3.cadre6AdressePersonnePouvoir.typeVoiePersonnePouvoir)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.11']          = monId1;  
} 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.12']= fondePouvoir3.cadre6AdressePersonnePouvoir.nomVoiePersonnePouvoir;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.13'] = fondePouvoir3.cadre6AdressePersonnePouvoir.personneLieeAdresseCommunepersonnePouvoir.getLabel();
if (fondePouvoir3.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir !== null) {
	var dateTemp = new Date(parseInt(fondePouvoir3.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E95/E95.1']= date;
}		
if (not Value('id').of(fondePouvoir3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
	var idPays = fondePouvoir3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E95/E95.2']= result;
} else if (Value('id').of(fondePouvoir3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E95/E95.2'] = fondePouvoir3.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getId();
}		
if (not Value('id').of(fondePouvoir3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {																					 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E95/E95.3'] = fondePouvoir3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getLabel();
}
if (Value('id').of(fondePouvoir3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E95/E95.4'] = fondePouvoir3.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E96']= fondePouvoir3.personneLieePersonnePhysiqueNationalitePersonnePouvoir;
}
}

 
 
// Call to the XML-REGENT generation WS 
var response = nash.service.request('${regent.baseUrl}/private/v1/xml-regent/generate/{regentVersion}/{authorityType}/{authorityId}', regentVersion, authorityType, authorityId) 
.dataType('application/json') // 
.accept('json') // 
.param('listTypeEvenement',eventRegent) 
//-->MINE-263 : Permettre à l'équipe FF de voir les problèmes de validation XSD et le regent sur studio
.continueOnError(true) //
.post(JSON.stringify(regentFields)); 

// Record the generated XML-REGENT 
//MOD déplacement du test de la réponse du premier ws
if (response != null && response.status == 200) { 
    var xmlRegentStr = response.asBytes(); 
    nash.record.saveFile("XML_REGENT.xml",xmlRegentStr); 

//debut de l'ajout
 if (authorityId2 != null) {
//MOD extraction du numéro de liasse du premeir regent généré
var numeroLiasse = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02');
numeroLiasse = JSON.parse(numeroLiasse);
_log.info('extracted numero de liasse is {}', numeroLiasse);
 regentFields['/REGENT-XML/Destinataire']= authorityId2;
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05']= "C";
 
 // Filtre regent greffe
var undefined;
// Groupe NGM
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/NGM/M27']= undefined;
// Groupe RFU
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U31']= undefined; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U32']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.1']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.2']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U37']= undefined;
// Groupe OIU
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OIU/U75']= undefined;
// Groupe GCS/ISS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/ISS/A10/A10.1']  = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/ISS/A10/A10.2'] = undefined;
// Groupe GCS/SNS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A21/A21.2']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A21/A21.3']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A21/A21.4']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A21/A21.6']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A22/A22.3']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A22/A22.4']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A24/A24.2']= undefined; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A24/A24.3']= undefined;
// Groupe GCS/CAS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/CAS/A42/A42.1']= undefined;
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/CAS/A42/A42.2']= undefined;
// Groupe GCS/SCS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SCS/A53/A53.4']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SCS/A55/A55.1']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SCS/A55/A55.2'] = undefined;
// Groupe GCS/SAS 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SAS/A68/A68.2']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SAS/A68/A68.3']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SAS/A68/A68.4']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SAS/A69/A69.1']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SAS/A69/A69.2']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SAS/A69/A69.3'] = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SAS/A69/A69.4'] = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SAS/A70'] = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SAS/A71/A71.3']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SAS/A71/A71.5']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SAS/A71/A71.6']= undefined;  
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SAS/A71/A71.7']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SAS/A71/A71.8']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SAS/A71/A71.10']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SAS/A71/A71.11']= undefined;  
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SAS/A71/A71.12']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SAS/A71/A71.13']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SAS/A71/A71.14']= undefined;
// Groupe MSS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/MSS/A31/A31.1'] = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/MSS/A31/A31.2'] = undefined;
// Groupe SAE
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E83/E83.1']=undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E84']=undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E85/E85.8']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E86']= undefined;

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS']  = undefined;


 //MOD modification de l'appel pour la génération du deuxième régent afi de prendre e compte le numéro de liasse du premier
 var response2 = nash.service.request('${regent.baseUrl}/private/v1/xml-regent/generate/{regentVersion}/{authorityType}/{authorityId}', regentVersion, authorityType2, authorityId2) 
.dataType('application/json') // 
.accept('json') // 
.param('listTypeEvenement',eventRegent) 
.param('liasseNumber', numeroLiasse.C02) 
.continueOnError(true)
.post(JSON.stringify(regentFields)); 

	//Début de l'ajout
	//MOD modifier reponse en response2
	if (response2 != null && response2.status == 200) { 
		var xmlRegentStr = response2.asBytes(); 
		nash.record.saveFile("XML_REGENT2.xml",xmlRegentStr);
	}else{
		
		var returnedResponse = response2.asObject();
		_log.info("Call ws regent returned errors  {}", returnedResponse);
		
		
		var regent2 = returnedResponse['regent'];
		var errors2 = returnedResponse['errors']; 
		nash.record.saveFile("XML_REGENT2.xml",regent2.getBytes()); 
		nash.record.saveFile("XML_VALIDATION_ERRORS_2.xml",errors2.getBytes()); 
		
		//-->MINE-263 : Permettre à l'équipe FF de voir les problèmes de validation XSD et le regent sur studio
		
		return spec.create({
		id : 'xmlGenerationConfirmation',
		label : "Xml Regent confirmation message",
		groups : [ spec.createGroup({
				id : 'Regent ',
				description : "Une erreur s'est produite lors de la génération du XML Regent.",
				data : [
				spec.createData({
					id: 'regent',
					label: "XML regent généré",
					type: 'Text',
					mandatory: true,
					value: regent2
				}),spec.createData({
					id: 'errors',
					label: "Erreurs de validations xsd",
					type: 'Text',
					mandatory: false,
					value: errors2
				}),spec.createData({
					id: 'blockingField',
					label: "xsd erroné",
					help:"Ce champs sert à arrêter le process du dossier pour que le support puisse le consulter",
					type: 'StringReadOnly',
					mandatory: true
				})]
			})]
		});
	}	
}
	return spec.create({
	id : 'xmlGenerationConfirmation',
	label : "Xml Regent confirmation message",
	groups : [ spec.createGroup({
		id : 'confirmationMessageOk',
		description : "Le fichier XML Regent a été généré et ajouté au dossier.",
		data : []
		}) ]
	});
}else{
	
	var returnedResponse = response.asObject();
	_log.info("Call ws regent returned errors  {}", returnedResponse);
	
	
	var regent = returnedResponse['regent'];
	var errors = returnedResponse['errors']; 
	nash.record.saveFile("XML_REGENT.xml",regent.getBytes()); 
	nash.record.saveFile("XML_VALIDATION_ERRORS.xml",errors.getBytes()); 
	
	
	return spec.create({
	id : 'xmlGenerationConfirmation',
	label : "Xml Regent confirmation message",
	groups : [ spec.createGroup({
		id : 'Regent ',
		description : "Une erreur s'est produite lors de la génération du XML Regent de la première autorité.",
		data : [
				spec.createData({
					id: 'regent',
					label: "XML regent généré",
					type: 'Text',
					mandatory: true,
					value: regent
				}),spec.createData({
					id: 'errors',
					label: "Erreurs de validations xsd",
					type: 'Text',
					mandatory: false,
					value: errors
				}),spec.createData({
					id: 'blockingField',
					label: "xsd erroné",
					help:"Ce champs sert à arrêter le process du dossier pour que le support puisse le consulter",
					type: 'StringReadOnly',
					mandatory: true
			})]
		}) ]
	});
}	