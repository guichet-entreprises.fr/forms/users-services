// PJ Dirigeant 1

var userDeclarant1;
if ($m0sarl.cadre5DirigeantGroup.cadreIdentiteDirigeant1.personneLieePPNomUsageGerant != null) {
    var userDeclarant1 = $m0sarl.cadre5DirigeantGroup.cadreIdentiteDirigeant1.personneLieePPNomUsageGerant + '  ' + $m0sarl.cadre5DirigeantGroup.cadreIdentiteDirigeant1.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant1 = $m0sarl.cadre5DirigeantGroup.cadreIdentiteDirigeant1.personneLieePPNomNaissanceGerant + '  '+ $m0sarl.cadre5DirigeantGroup.cadreIdentiteDirigeant1.personneLieePPPrenomGerant[0] ;
}

attachment('pjDNCDirigeant1', 'pjDNCDirigeant1',{ label: userDeclarant1, mandatory:"true"}) ;

var pj=$m0sarl.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteRepresentantLegal') 
	and (not $m0sarl.cadre5DirigeantGroup.cadreIdentiteDirigeant1.autreDirigeant1 
or Value('id').of($m0sarl.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant1') 
or $m0sarl.cadre5DirigeantGroup.cadreIdentiteDirigeant1.entrepriseAssocieUniqueNonGerant)) {
    attachment('pjIDDirigeant1Signataire', 'pjIDDirigeant1Signataire', { label: userDeclarant1, mandatory:"true"});
}

var pj=$m0sarl.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire') 
	or (Value('id').of(pj).contains('FormaliteSignataireQualiteRepresentantLegal') 
	and not Value('id').of($m0sarl.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant1')
	and $m0sarl.cadre5DirigeantGroup.cadreIdentiteDirigeant1.autreDirigeant1 )) {
    attachment('pjIDDirigeant1NonSignataire', 'pjIDDirigeant1NonSignataire', { label: userDeclarant1, mandatory:"true"});
}

var pj=$m0sarl.cadre5DirigeantGroup.cadreIdentiteDirigeant1.statutConjointCollaborateurGeranceMaj ;
if(Value('id').of(pj).contains('personneLieeConjointStatutCollaborateur')) {
    attachment('pjIDConjoint1', 'pjIDConjoint1', { label: userDeclarant1, mandatory:"true"});
}

var pj=$m0sarl.cadre5DirigeantGroup.cadreIdentiteDirigeant1 ;
if(pj.conjointRole == true and (pj.statutConjointCollaborateurGeranceNonMaj != null or pj.statutConjointCollaborateurGeranceMaj != null)) {
    attachment('pjChoixStatutConjoint1', 'pjChoixStatutConjoint1', { label: userDeclarant1, mandatory:"true"});
}

// PJ Dirigeant 2

var userDeclarant2;
if ($m0sarl.cadre9DirigeantGroup.cadreIdentiteDirigeant2.personneLieePPNomUsageGerant != null) {
    var userDeclarant2 = $m0sarl.cadre9DirigeantGroup.cadreIdentiteDirigeant2.personneLieePPNomUsageGerant + '  ' + $m0sarl.cadre9DirigeantGroup.cadreIdentiteDirigeant2.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant2 = $m0sarl.cadre9DirigeantGroup.cadreIdentiteDirigeant2.personneLieePPNomNaissanceGerant + '  '+ $m0sarl.cadre9DirigeantGroup.cadreIdentiteDirigeant2.personneLieePPPrenomGerant[0] ;
}

if($m0sarl.cadre5DirigeantGroup.cadreIdentiteDirigeant1.autreDirigeant1) {
    attachment('pjDNCDirigeant2', 'pjDNCDirigeant2',{ label: userDeclarant2, mandatory:"true"}) ;
}

var pj=$m0sarl.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteRepresentantLegal') and Value('id').of($m0sarl.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant2')) {
    attachment('pjIDDirigeant2Signataire', 'pjIDDirigeant2Signataire', { label: userDeclarant2, mandatory:"true"});
}

var pj=$m0sarl.cadre11SignatureGroup.cadre11Signature.soussigne;
if($m0sarl.cadre5DirigeantGroup.cadreIdentiteDirigeant1.autreDirigeant1 and not Value('id').of($m0sarl.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant2')) {
    attachment('pjIDDirigeant2NonSignataire', 'pjIDDirigeant2NonSignataire', { label: userDeclarant2, mandatory:"true"});
}

var pj=$m0sarl.cadre9DirigeantGroup.cadreIdentiteDirigeant2.statutConjointCollaborateurGeranceMaj ;
if(Value('id').of(pj).contains('personneLieeConjointStatutCollaborateur')) {
    attachment('pjIDConjoint2', 'pjIDConjoint2', { label: userDeclarant2, mandatory:"true"});
}

var pj=$m0sarl.cadre9DirigeantGroup.cadreIdentiteDirigeant2 ;
if(pj.conjointRole == true and (pj.statutConjointCollaborateurGeranceNonMaj != null or pj.statutConjointCollaborateurGeranceMaj != null)) {
    attachment('pjChoixStatutConjoint2', 'pjChoixStatutConjoint2', { label: userDeclarant2, mandatory:"true"});
}


//PJ Dirigeant 3

var userDeclarant3;
if ($m0sarl.cadre12DirigeantGroup.cadreIdentiteDirigeant3.personneLieePPNomUsageGerant != null) {
    var userDeclarant3 = $m0sarl.cadre12DirigeantGroup.cadreIdentiteDirigeant3.personneLieePPNomUsageGerant + '  ' + $m0sarl.cadre12DirigeantGroup.cadreIdentiteDirigeant3.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant3 = $m0sarl.cadre12DirigeantGroup.cadreIdentiteDirigeant3.personneLieePPNomNaissanceGerant + '  '+ $m0sarl.cadre12DirigeantGroup.cadreIdentiteDirigeant3.personneLieePPPrenomGerant[0] ;
}

if($m0sarl.cadre9DirigeantGroup.cadreIdentiteDirigeant2.autreDirigeant2) {
    attachment('pjDNCDirigeant3', 'pjDNCDirigeant3',{ label: userDeclarant3, mandatory:"true"}) ;
}

var pj=$m0sarl.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteRepresentantLegal') and Value('id').of($m0sarl.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant3')) {
    attachment('pjIDDirigeant3Signataire', 'pjIDDirigeant3Signataire', { label: userDeclarant3, mandatory:"true"});
}

var pj=$m0sarl.cadre11SignatureGroup.cadre11Signature.soussigne;
if($m0sarl.cadre9DirigeantGroup.cadreIdentiteDirigeant2.autreDirigeant2 and not Value('id').of($m0sarl.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant3')) {
    attachment('pjIDDirigeant3NonSignataire', 'pjIDDirigeant3NonSignataire', { label: userDeclarant3, mandatory:"true"});
}

var pj=$m0sarl.cadre12DirigeantGroup.cadreIdentiteDirigeant3.statutConjointCollaborateurGeranceMaj ;
if(Value('id').of(pj).contains('personneLieeConjointStatutCollaborateur')) {
    attachment('pjIDConjoint3', 'pjIDConjoint3', { label: userDeclarant3, mandatory:"true"});
}

var pj=$m0sarl.cadre12DirigeantGroup.cadreIdentiteDirigeant3 ;
if(pj.conjointRole == true and (pj.statutConjointCollaborateurGeranceNonMaj != null or pj.statutConjointCollaborateurGeranceMaj != null)) {
    attachment('pjChoixStatutConjoint3', 'pjChoixStatutConjoint3', { label: userDeclarant3, mandatory:"true"});
}

//PJ Dirigeant 4

var userDeclarant4;
if ($m0sarl.cadre15DirigeantGroup.cadreIdentiteDirigeant4.personneLieePPNomUsageGerant != null) {
    var userDeclarant4 = $m0sarl.cadre15DirigeantGroup.cadreIdentiteDirigeant4.personneLieePPNomUsageGerant + '  ' + $m0sarl.cadre15DirigeantGroup.cadreIdentiteDirigeant4.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant4 = $m0sarl.cadre15DirigeantGroup.cadreIdentiteDirigeant4.personneLieePPNomNaissanceGerant + '  '+ $m0sarl.cadre15DirigeantGroup.cadreIdentiteDirigeant4.personneLieePPPrenomGerant[0] ;
}

if($m0sarl.cadre12DirigeantGroup.cadreIdentiteDirigeant3.autreDirigeant3) {
    attachment('pjDNCDirigeant4', 'pjDNCDirigeant4',{ label: userDeclarant4, mandatory:"true"}) ;
}

var pj=$m0sarl.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteRepresentantLegal') and Value('id').of($m0sarl.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant4')) {
    attachment('pjIDDirigeant4Signataire', 'pjIDDirigeant4Signataire', { label: userDeclarant4, mandatory:"true"});
}

var pj=$m0sarl.cadre11SignatureGroup.cadre11Signature.soussigne;
if($m0sarl.cadre12DirigeantGroup.cadreIdentiteDirigeant3.autreDirigeant3 and not Value('id').of($m0sarl.cadre11SignatureGroup.cadre11Signature.representantLegalNumero).eq('gerant4')) {
    attachment('pjIDDirigeant4NonSignataire', 'pjIDDirigeant4NonSignataire', { label: userDeclarant4, mandatory:"true"});
}

var pj=$m0sarl.cadre15DirigeantGroup.cadreIdentiteDirigeant4.statutConjointCollaborateurGeranceMaj ;
if(Value('id').of(pj).contains('personneLieeConjointStatutCollaborateur')) {
    attachment('pjIDConjoint4', 'pjIDConjoint4', { label: userDeclarant4, mandatory:"true"});
}

var pj=$m0sarl.cadre15DirigeantGroup.cadreIdentiteDirigeant4 ;
if(pj.conjointRole == true and (pj.statutConjointCollaborateurGeranceNonMaj != null or pj.statutConjointCollaborateurGeranceMaj != null)) {
    attachment('pjChoixStatutConjoint4', 'pjChoixStatutConjoint4', { label: userDeclarant4, mandatory:"true"});
}

var dirigeant1 = $m0sarl.cadre5DirigeantGroup.cadreIdentiteDirigeant1;
var dirigeant2 = $m0sarl.cadre9DirigeantGroup.cadreIdentiteDirigeant2;
var dirigeant3 = $m0sarl.cadre12DirigeantGroup.cadreIdentiteDirigeant3;
var dirigeant4 = $m0sarl.cadre15DirigeantGroup.cadreIdentiteDirigeant4;

if(not dirigeant1.statutPresenceDirigeant or (dirigeant1.autreDirigeant1 and not dirigeant2.statutPresenceDirigeant) or (dirigeant2.autreDirigeant2 and not dirigeant3.statutPresenceDirigeant) or (dirigeant3.autreDirigeant3 and not dirigeant4.statutPresenceDirigeant)) {
    attachment('pjNominationDirigeant', 'pjNominationDirigeant', { mandatory:"true"});
}


// PJ Fondé de pouvoir 1

var user=$m0sarl.cadre19PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir

if($m0sarl.cadre19PeronnePouvoirGroup.cadre6PeronnePouvoir.personneLieePersonneLieeEtablissement) {
    attachment('pjDNCPersonnePouvoir', 'pjDNCPersonnePouvoir', { label : (user.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? user.personneLieePersonnePhysiqueNomUsagePersonnePouvoir : user.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir) + ' '+user.personneLieePersonnePhysiquePrenom1PersonnePouvoir[0], mandatory:"true"});
	attachment('pjIDPersonnePouvoir', 'pjIDPersonnePouvoir', { label : (user.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? user.personneLieePersonnePhysiqueNomUsagePersonnePouvoir : user.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir) + ' '+user.personneLieePersonnePhysiquePrenom1PersonnePouvoir[0], mandatory:"true"});
}

// PJ Fondé de pouvoir 2

var userFondePouvoir2=$m0sarl.cadre19PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir2

if($m0sarl.cadre19PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir.autreDeclarationPersonneLiee) {
	attachment('pjDNCPersonnePouvoir2', 'pjDNCPersonnePouvoir2', { label : (userFondePouvoir2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? userFondePouvoir2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir : userFondePouvoir2.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir) + ' '+userFondePouvoir2.personneLieePersonnePhysiquePrenom1PersonnePouvoir[0], mandatory:"true"});
    attachment('pjIDPersonnePouvoir2', 'pjIDPersonnePouvoir2', { label : (userFondePouvoir2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? userFondePouvoir2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir : userFondePouvoir2.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir) + ' '+userFondePouvoir2.personneLieePersonnePhysiquePrenom1PersonnePouvoir[0], mandatory:"true"});
}

// PJ Fondé de pouvoir 3

var userFondePouvoir3=$m0sarl.cadre19PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir3

if($m0sarl.cadre19PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir2.autreDeclarationPersonneLiee2) {
	attachment('pjDNCPersonnePouvoir3', 'pjDNCPersonnePouvoir3', { label : (userFondePouvoir3.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? userFondePouvoir3.personneLieePersonnePhysiqueNomUsagePersonnePouvoir : userFondePouvoir3.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir) + ' '+userFondePouvoir3.personneLieePersonnePhysiquePrenom1PersonnePouvoir[0], mandatory:"true"});
    attachment('pjIDPersonnePouvoir3', 'pjIDPersonnePouvoir3', { label : (userFondePouvoir3.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? userFondePouvoir3.personneLieePersonnePhysiqueNomUsagePersonnePouvoir : userFondePouvoir3.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir) + ' '+userFondePouvoir3.personneLieePersonnePhysiquePrenom1PersonnePouvoir[0], mandatory:"true"});
}


// PJ Mandataire

var userMandataire=$m0sarl.cadre11SignatureGroup.cadre11Signature.adresseMandataire

var pj=$m0sarl.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
    attachment('pjPouvoir', 'pjPouvoir', { mandatory:"true"});
	attachment('pjIDMandataireSignataire', 'pjIDMandataireSignataire', {label: userMandataire.nomPrenomDenominationMandataire, mandatory:"true"});
}

//PJ Société

attachment('pjStatuts', 'pjStatuts', { mandatory:"true"});
attachment('pjAnnonceLegaleConstitution', 'pjAnnonceLegaleConstitution', { mandatory:"true"});

var pj=$m0sarl.cadre1SocieteGroup.cadre1Identite;
if(pj.entrepriseCapitalApportNature and (pj.entrepriseCapitalApportMoitierCapital or pj.entrepriseCapitalApport30000)) {
    attachment('pjRapportCAA', 'pjRapportCAA', { mandatory:"true"});
}

var pj=$m0sarl.cadre1SocieteGroup.cadre1Identite ;
if(pj.entrepriseContratAppuiPresence) {
    attachment('pjContratAppui', 'pjContratAppui', { mandatory:"true"});
}

// PJ Etablissement

var pj=$m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.activiteLieu ;
if(Value('id').of(pj).contains('entrepriseAdresseEntrepriseDomicile')) {
    attachment('pjDomicile', 'pjDomicile', { mandatory:"true"});
}

var pj=$m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.activiteLieu ;
if(Value('id').of(pj).contains('etablissementDomiciliationOui')) {
    attachment('pjContratDomiciliation', 'pjContratDomiciliation', { mandatory:"true"});
}

if (Value('id').of($m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.activiteLieu).contains('etablissementDomiciliationNon')
 and ($m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.etablissementPrincipalDifferentSiege 
 or not $m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.formaliteCreationAvecActivite 
 or (not $m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.etablissementPrincipalDifferentSiege 
 and ($m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.activiteLiberaleOui 
 or Value('id').of($m0sarl.cadre1SocieteGroup.cadre1Identite.entrepriseFormeJuridique).eq('selarl')
 or Value('id').of($m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsCreation') 
 or Value('id').of($m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsCocheAutre')
 or (Value('id').of($m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.etablisementOrigineFondsCommercial).contains('EtablissementOrigineFondsAchat') 
 and $m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.precedentExploitant.fondsArtisanal))))) {
    attachment('pjSocieteCreation', 'pjSocieteCreation', { mandatory:"true"});
}

var pj=$m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.etablisementOrigineFondsCommercial ;
if($m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.etablissementPrincipalDifferentSiege
and ($m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.activiteLiberaleOui 
or Value('id').of($m0sarl.cadre1SocieteGroup.cadre1Identite.entrepriseFormeJuridique).eq('selarl')
or Value('id').of(pj).contains('EtablissementOrigineFondsCreation')
or Value('id').of(pj).contains('EtablissementOrigineFondsCocheAutre')
or (Value('id').of(pj).contains('EtablissementOrigineFondsAchat') 
and $m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.precedentExploitant.fondsArtisanal))) {
    attachment('pjEtablissementCreation', 'pjEtablissementCreation', { mandatory:"true"});
}

var pj=$m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.etablisementOrigineFondsCommercial ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsLocationGerance')) {
    attachment('pjLocationGerance', 'pjLocationGerance', { mandatory:"true"});
	attachment('pjLocationGerancePublication', 'pjLocationGerancePublication', { mandatory:"true"});
}

var pj=$m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.etablisementOrigineFondsCommercial ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsGeranceMandat')) {
    attachment('pjGeranceMandat', 'pjGeranceMandat', { mandatory:"true"});
	attachment('pjGeranceMandatPublication', 'pjGeranceMandatPublication', { mandatory:"true"});
}

var pj=$m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.etablisementOrigineFondsCommercial ;
if((Value('id').of(pj).contains('EtablissementOrigineFondsAchat')) and $m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.precedentExploitant.cessionFonds) {
    attachment('pjPlanCession', 'pjPlanCession', { mandatory:"true"});
}

var pj=$m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.etablisementOrigineFondsCommercial ;
if((Value('id').of(pj).contains('EtablissementOrigineFondsAchat')) and ($m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.precedentExploitant.etablissementJournalAnnoncesLegalesNom != null)) {
    attachment('pjAchat', 'pjAchat', { mandatory:"true"});
	attachment('pjAchatPublication', 'pjAchatPublication', { mandatory:"true"});
}

var pj=$m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.etablisementOrigineFondsCommercial ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsApport')) {
    attachment('pjApport', 'pjApport', { mandatory:"true"});
	attachment('pjApportPublication', 'pjApportPublication', { mandatory:"true"});
}

// PJ JQPA

var userJqpa1 = $m0sarl.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification1;
if(Value('id').of(userJqpa1.jqpaSituation).eq('jqpaSituationDiplome')) {
	attachment('pjDiplomes1', 'pjDiplomes1', { label : userJqpa1.jqpaActiviteJqpaPP, mandatory:"true"});
}

if(Value('id').of(userJqpa1.jqpaSituation).eq('jqpaSituationExperience')) {
	attachment('pjPiecesUtiles1', 'pjPiecesUtiles1', { label : userJqpa1.jqpaActiviteJqpaPP,mandatory:"true"});
}

var userJqpa2 = $m0sarl.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification2;
if(Value('id').of(userJqpa2.jqpaSituation).eq('jqpaSituationDiplome')) {
	attachment('pjDiplomes2', 'pjDiplomes2', { label : userJqpa2.jqpaActiviteJqpaPP, mandatory:"true"});
}

if(Value('id').of(userJqpa2.jqpaSituation).eq('jqpaSituationExperience')) {
	attachment('pjPiecesUtiles2', 'pjPiecesUtiles2', { label : userJqpa2.jqpaActiviteJqpaPP, mandatory:"true"});
}

var userJqpa3 = $m0sarl.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification3;
if(Value('id').of(userJqpa3.jqpaSituation).eq('jqpaSituationDiplome')) {
	attachment('pjDiplomes3', 'pjDiplomes3', { label : userJqpa3.jqpaActiviteJqpaPP, mandatory:"true"});
}

if(Value('id').of(userJqpa3.jqpaSituation).eq('jqpaSituationExperience')) {
	attachment('pjPiecesUtiles3', 'pjPiecesUtiles3', { label : userJqpa3.jqpaActiviteJqpaPP, mandatory:"true"});
}

// PJ MBE	
var signataire = $m0sarl.cadre11SignatureGroup.cadre11Signature ;

attachment('formulaireBE1', 'formulaireBE1', {mandatory:"true"});

if (not Value('id').of(signataire.nombreMBE).eq('un')) {
attachment('formulaireBE2', 'formulaireBE2', {mandatory:"true"});
}

if (not Value('id').of(signataire.nombreMBE).eq('un') and not Value('id').of(signataire.nombreMBE).eq('deux')) {
attachment('formulaireBE3', 'formulaireBE3', {mandatory:"true"});
}

if (Value('id').of(signataire.nombreMBE).eq('quatre') or Value('id').of(signataire.nombreMBE).eq('cinq')) {
attachment('formulaireBE4', 'formulaireBE4', {mandatory:"true"});
}

if (Value('id').of(signataire.nombreMBE).eq('cinq')) {
attachment('formulaireBE5', 'formulaireBE5', {mandatory:"true"});
}


attachment('pjIDActiviteReglementee', 'pjIDActiviteReglementee', { mandatory:"false"});