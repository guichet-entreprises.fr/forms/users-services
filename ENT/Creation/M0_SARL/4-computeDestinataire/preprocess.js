var attachment= "/3-review/generated/generated.record-1-M0_SARL.pdf";
var adressePro = $m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.cadreAdresseProfessionnelle;
var algo = "trouver destinataire";
var secteur2 = '';
if ((($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA != null
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	or ($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA != null
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	or ($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA != null
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.jqpaDomaineTransport).eq('jqpaTransportAutre')))
	and not $m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.optionCMACCI) 	{
var secteur1 =  "ARTISANAL";
} else if (Value('id').of($m0sarl.cadre1SocieteGroup.cadre1Identite.entrepriseFormeJuridique).eq('selarl')) {
var secteur1 =  "LIBERAL";
} else { var secteur1 =  "COMMERCIAL"; }

if (Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
	or Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
	or Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
	or Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre')
	or Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup1.jqpaDomaineTransport).eq('jqpaTransportAutre')
	or Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA).eq('nafaAutre')
	or Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineAlimentation).eq('alimentationAutre')
	or Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineAutresServices).eq('servicesAutre')
	or Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.nafaDomaineProduction).eq('productionAutre')
	or Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup2.jqpaDomaineTransport).eq('jqpaTransportAutre')
	or Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA).eq('nafaAutre')
	or Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineAlimentation).eq('alimentationAutre')
	or Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineAutresServices).eq('servicesAutre')
	or Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.nafaDomaineProduction).eq('productionAutre') 
	or Value('id').of($m0sarl.cadre3EtablissementActiviteGroup.cadre3EtablissementActivite.nouvelleActiviteGroup3.jqpaDomaineTransport).eq('jqpaTransportAutre')) {
	secteur2 =  "COMMERCIAL";
}
var typePersonne = "PM";
var formJuridique = Value('id').of($m0sarl.cadre1SocieteGroup.cadre1Identite.entrepriseFormeJuridique).eq('sarl') ? "SARL" : "SEL";
var optionCMACCI = "NON";
var formalityWithPayment = false;
var listFraisDestinataires=[];
var listDistinctAuthorities =[];
var destFuncId2 = '';		 
var codeEDI2 = '';
var partnerLabel2 = '';

			 
_log.info("algo is {}",algo);
_log.info("secteur1 is {}",secteur1);
_log.info("typePersonne is {}",typePersonne);
_log.info("formJuridique is {}",formJuridique);
_log.info("optionCMACCI is {}",optionCMACCI);

var codeCommune = '';

codeCommune = adressePro.communeAdresseSiege.getId();


_log.info("codeCommune is {}",codeCommune);


//=================================> calcul des destinataires Début<=========================================
var args = {
    "secteur1" : secteur1,
	"secteur2" : secteur2,
	"typePersonne" : typePersonne,
	"formJuridique" : formJuridique,
	"optionCMACCI":optionCMACCI,
	"codeCommune": codeCommune
};

_log.info("serviceParams  {}",JSON.stringify(args));

// call directory service to find functional id of authority 
var response = nash.service.request('${directory.baseUrl}/v1/algo/{code}/execute',algo) //
        .connectionTimeout(10000) //
        .receiveTimeout(10000)
        .accept('application/json') //
        .post(args);

var receiverInfo = JSON.parse(response.asString());

_log.info("receiverInfo  is {}", receiverInfo);

_log.info("listAuthorities  is {}", receiverInfo.listAuthorities);
_log.info("type of details of listAuthorities is  {}", typeof receiverInfo.listAuthorities);
_log.info("listAuthorities.length is {}",  receiverInfo.listAuthorities.length);

//===============================> Récupérer la liste des autorités distincte (sans doublons pour construire le bloc destiné à la génération du regent XML et le XML-TC <==============================================

	//Début<=========================================	
	for(var i=0; i<receiverInfo.listAuthorities.length; i++){	
		if(listDistinctAuthorities.indexOf(receiverInfo.listAuthorities[i]['authority'])== -1){	
			listDistinctAuthorities.push(receiverInfo.listAuthorities[i]['authority']);
		}
	}	
	_log.info("listAuthorities.length is {}",  listDistinctAuthorities.length);
	_log.info("listDistinctAuthorities  is {}", listDistinctAuthorities);
	//Fin ===========================================>
	
	//Récupérer les informations des destinataires 
		//Début<=========================================
		_log.info("1er destinataire  is {}", listDistinctAuthorities[0]);
		var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', listDistinctAuthorities[0]) //
				   .connectionTimeout(10000) //
				   .receiveTimeout(10000) //
				   .accept('json') //
				   .get();
		//result
		var receiverInfo2 = response.asObject();

		// prepare all information of receiver to create data.xml

		var funcId = !receiverInfo2.entityId ? null : receiverInfo2.entityId;
		var funcLabel = !receiverInfo2.label ? null :receiverInfo2.label;
		var contactInfo = !receiverInfo2.details ? null : receiverInfo2.details;
		var email = !contactInfo.profile.email ? null : contactInfo.profile.email;
		var tel = !contactInfo.profile.tel ? null : contactInfo.profile.tel;
		
		if(receiverInfo.listAuthorities[0]['role'] == "CFE"){
			_log.info("*********Première autorité est un CFE******");	
			var codeEDI = !contactInfo.ediCode ? null : contactInfo.ediCode;
		}else{
			_log.info("*********Première autorité est un TDR******");
			var codeEDI2 = !contactInfo.ediCode ? null : contactInfo.ediCode;	
		}
		
		var partnerLabel = !contactInfo.profile.address.recipientName ? null : contactInfo.profile.address.recipientName;

		if(listDistinctAuthorities.length == 2){
			_log.info("2eme destinataire  is {}", listDistinctAuthorities[1]);
			_log.info("******récupération du second destinataire******");
			var response2 = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', listDistinctAuthorities[1]) //
				   .connectionTimeout(10000) //
				   .receiveTimeout(10000) //
				   .accept('json') //
				   .get();
			//result
			var receiverInfo3 = response2.asObject();

			// prepare all information of receiver to create data.xml
			destFuncId2 = listDistinctAuthorities[1];
			contactInfo = !receiverInfo3.details ? null : receiverInfo3.details;

			if(receiverInfo.listAuthorities[1]['role'] == "CFE"){
				_log.info("*********Deuxième autorité est un CFE******");	
				var codeEDI = !contactInfo.ediCode ? null : contactInfo.ediCode;
			}else{
				_log.info("*********Deuxième autorité est un TDR******");	
				var codeEDI2 = !contactInfo.ediCode ? null : contactInfo.ediCode;	
			}

			 partnerLabel2 = !contactInfo.profile.address.recipientName ? null : contactInfo.profile.address.recipientName;
			
		}	
		var regentXmlTcBloc = spec.createGroup({
									id: 'dataGroup',
									label: "Contenu d'une autorité compétente",
									data: [spec.createData({
											id: 'destFuncId',
											label: "Le code de l'autorité compétente",
											description: 'Code interne',
											type: 'String',
											mandatory: true,
											value: funcId
										}), spec.createData({
											id: 'labelFunc',
											label: "Le libellé fonctionnelle de l'autorité compétente",
											type: 'String',
											mandatory : true,
											value: funcLabel
										}), spec.createData({
											id: 'email',
											label: "L'adresse email de l'autorité compétente",
											type: 'email',
											mandatory:true,
											value: email
										}),	spec.createData({
											id: 'emailPj',
											label: "Le chemin de la pièce jointe",
											type: 'String',
											value: attachment
									}),   spec.createData({
											id: 'tel',
											label: "telephone",
											type: 'String',
											value: tel
									}), spec.createData({
											id: 'partnerLabel',
											label: "Nom autorité",
											type: 'String',
											value: partnerLabel
									}),spec.createData({
											id: 'codeEDI',
											label: "code EDI",
											type: 'String',
											value: codeEDI
									}),
									//début de l'ajout
									spec.createData({
											id: 'destFuncId2',
											label: "code de la deuxième autorité",
											type: 'String',
											value: destFuncId2
									}),spec.createData({
											id: 'codeEDI2',
											label: "code EDI de la deuxième autorité",
											type: 'String',
											value: codeEDI2
									}),spec.createData({
											id: 'partnerLabel2',
											label: "Nom de la deuxième autorité",
											type: 'String',
											value: partnerLabel2
									})
									//fin de l'ajout	
									]
								});
	//Fin ===========================================>	
//=================================>  calcul des destinataires Fin <=========================================

	//=================================> Partie qui sera mise par le rédacteur pour conditionner le couple frais autoprité dans le cas où la formalité est payante et donc le bloc est conditionné

	//Début<=========================================	
//Condition pour dire qu'une formalité contient un paiement

if ($m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.formaliteCreationAvecActivite or not $m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.formaliteCreationAvecActivite) {
		
	//Mettre le Flag de paiement à "vrai"
	_log.info("*********Paiement existant sur la formalité!!******");
	formalityWithPayment =  true;
 
														  
	var listCoupleAutoriteFrais=[];
	for(var i=0; i<receiverInfo.listAuthorities.length; i++){
	
		// Dans ce cas nous avons un seul produit destioné aux greffes qui sont des TDR, on parcours la liste des authorités calculés puis on test chaque role retourné, quand on trouve un TDR on va lui affecter le code du frais de dépot qui nous interesse : 
			_log.info("receiverInfo.listAuthorities[i]['role'] for authority {} is {}", i, receiverInfo.listAuthorities[i]['role']);
			_log.info("secteur1 is {}", secteur1);
			_log.info("secteur2 is {}", secteur2);
			
		// Récupérer le réseau de l'autorité en cours de traitement
		
		var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', receiverInfo.listAuthorities[i]['authority']) //
				   .connectionTimeout(10000) //
				   .receiveTimeout(10000) //
				   .accept('json') //
				   .get();
		//result
		var currentAuthority = response.asObject();

		// prepare all information of receiver to create data.xml
		var details = !currentAuthority.details ? null : currentAuthority.details;
		var ediCode = !details.ediCode ? null : details.ediCode;
		_log.info("ediCode for authority {} is {}", i, ediCode);
		var authorityReseau = ediCode.charAt(0);
		_log.info("authorityReseau for authority {} is {}", i , authorityReseau);	
		
	// Frais pour le RCS et l'autorité est un GREFFE 	
		if(receiverInfo.listAuthorities[i]['role'] == "TDR" && authorityReseau == 'G') {
			
			//Récupérer les informations de chaque autorité calculée pour récupéré son libellé
			_log.info("Authority Code is {}", receiverInfo.listAuthorities[i]['authority']);
			var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', receiverInfo.listAuthorities[i]['authority']) //
						   .connectionTimeout(10000) //
						   .receiveTimeout(10000) //
						   .accept('json') //
						   .get();   
						   
			var authority = response.asObject();
			
			var funcLabel = !authority.label ? null :authority.label;

			_log.info("funcLabel is {}", funcLabel);
			
			if (Value('id').of($m0sarl.cadre1SocieteGroup.cadre1Identite.entrepriseFormeJuridique).eq('sarl')
				and $m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.formaliteCreationAvecActivite
				and (Value('id').of($m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsCreation')
				or Value('id').of($m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.etablisementOrigineFondsLiberal).eq('EtablissementOrigineFondsCreation')
				or ((Value('id').of($m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')
				or Value('id').of($m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.activiteLieu).eq('etablissementDomiciliationOui'))
				and not $m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.etablissementPrincipalDifferentSiege))) {
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRCS003"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
			} else if (not $m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.formaliteCreationAvecActivite
				or (Value('id').of($m0sarl.cadre1SocieteGroup.cadre1Identite.entrepriseFormeJuridique).eq('selarl')
				and (Value('id').of($m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.etablisementOrigineFondsLiberal).eq('EtablissementOrigineFondsCreation')
				or ((Value('id').of($m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')
				or Value('id').of($m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.activiteLieu).eq('etablissementDomiciliationOui'))
				and not $m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.etablissementPrincipalDifferentSiege)))) {
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRCS004"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
			} else if ($m0sarl.cadre2AdresseSiegeGroup.cadre2AdresseSiege.formaliteCreationAvecActivite
				and (Value('id').of($m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsLocationGerance')
				or Value('id').of($m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsGeranceMandat')
				or Value('id').of($m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsAchat')
				or Value('id').of($m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsApport')
				or Value('id').of($m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.etablisementOrigineFondsCommercial).eq('EtablissementOrigineFondsCocheAutre')
				or Value('id').of($m0sarl.cadre4OrigineFondsGroup.cadre4OrigineFonds.etablisementOrigineFondsLiberal).eq('EtablissementOrigineFondsRepriseActiviteLiberale'))){
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRCS005"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
			} 
		
			if ($m0sarl.cadre11SignatureGroup.cadre11Signature.nombreMBE != null){
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRBE001"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
			} 
		
		}	
			
	// Frais pour le RM et l'autorité est une CMA 
		if(receiverInfo.listAuthorities[i]['role'] == "TDR" && secteur1 == "ARTISANAL" && authorityReseau == 'M') {
			_log.info("Access cas : secteur 1 artisanal ===> Frais pour les CMA");
			_log.info("receiverInfo.listAuthorities[i]['role'] for authority {} is {}", i, receiverInfo.listAuthorities[i]['role']);
			_log.info("secteur1 is {}", secteur1);
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : (Date.now() < new Date('2020-01-01T00:00:00')) ? "IRM0001" : "IRM0006"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
		}
	}

	_log.info("listCoupleAutoriteFrais  is {}", listCoupleAutoriteFrais);
	//=================================> Partie qui sera mise par le rédacteur pour conditionner le couple frais autoprité Fin=========================================

	//=================================> Partie générique pour générer le specCreate à passer à paiement Début=========================================										
	for(var i=0; i<listCoupleAutoriteFrais.length; i++){
		//Récupérer les informations des frais
		_log.info(" entree dans la boucle du couple frais autorité {}", i);
		var filters = [];
		var prix = 0;
		filters.push("details.entityId:"+listCoupleAutoriteFrais[i]['frais']);
		var currentFraisAuthority = listCoupleAutoriteFrais[i];
		_log.info(" -- Couple authoriy frais recupere -- is {} for iteration {} ",  currentFraisAuthority, i);
		_log.info(" -- Code Authority -- is {} for iteration {} ",  currentFraisAuthority.authorityCode, i);
		_log.info(" -- filters -- is {}", filters);
		var response = nash.service.request('${directory.baseUrl}/private/v1/repository/{repositoryId}', "frais") //
				   .connectionTimeout(10000) //
				   .receiveTimeout(10000) //
				   .accept('json') //
				   .param("filters",filters)
				   .get();
				   
		var receiverInfo = response.asObject();
		var details = !receiverInfo.content[0].details ? null : receiverInfo.content[0].details;
		_log.info(" -- details -- is {}", details);
		
				if(details != null){
					var entityId = !details.entityId ? null : details.entityId;
					_log.info(" -- entityId -- is {}", entityId);
					var prix = !details.prix ? null : details.prix;
					_log.info(" -- prix -- is {}", prix);
					var reseau = !details.reseau ? null : details.reseau;
					_log.info(" -- reseau -- is {}", reseau);
					var label = !details.label ? null : details.label;
					_log.info(" -- label -- is {}", label);
				
						//Construction de la page à afficher au user
					var  authorityCode = currentFraisAuthority.authorityCode;
					_log.info(" -- authorityCode -- is {}", authorityCode);
					var  authorityLabel = currentFraisAuthority.authorityLabel;
					_log.info(" -- authorityLabel -- is {}", authorityLabel);
					var fraisCode = currentFraisAuthority.frais;
					_log.info(" -- fraisCode -- is {}", fraisCode);
					_log.info(" -- Avant de creer la liste des frais ! {}", i);
					var fraisDestination = spec.createGroup({
											'id':'fraisDestinations['+i+']',
											'label':"Frais à payer",
											'maxOccurs':listCoupleAutoriteFrais.length,
											'data':[
													spec.createData({
														'id': 'authorityCode',
														'label': 'Authorité code :',
														'type': 'StringReadOnly',
														'value': authorityCode
														}),
													spec.createData({
														'id': 'authorityLabel',
														'label': 'Authorité label :',
														'type': 'StringReadOnly',
														'value':authorityLabel
														}),
													spec.createData({
														'id': 'fraisCode',
														'label': 'frais code :',
														'type': 'StringReadOnly',
														'value':fraisCode
														}),
													spec.createData({
														'id': 'fraisLabel',
														'label': 'frais label :',
														'type': 'StringReadOnly',
														'value':label
														}),
													spec.createData({
														'id': 'fraisPrix',
														'label': 'frais prix :',
														'type': 'StringReadOnly',
														'value':""+prix
														}),										
											]
										});
					listFraisDestinataires.push(fraisDestination);
					_log.info("listFraisDestinataires avec un frais pour l'iteration {}  is {}", i, listFraisDestinataires);
				}else{
					_log.error(" -- Error : code with the id {} not found in the repository with the id {}", listCodesFrais[i], "frais");
					return spec.create({
										id : 'codeFraisNotFound',
										label : 'Erreur lors de la recherche des codes de frais',
										groups : [ spec.createGroup({
											id : 'error',
											label : 'Erreur lors de la recherche des codes de frais',
											description : "Une erreur lors de la recherche des codes de frais dans le référentiel, veuillez contacter le support à l'adresse: support@guichet-entreprises.fr.",
											data : []
										}) ]
									});					
				}
	}
	
			var indiceFormalitePayante = spec.createGroup({
										'id':'flagFormalitePayante',
										'label':"la formalité est-elle payante?",
										'data':[
											spec.createData({
											'id': 'flagPayment',
											'label': 'flag du paiement :',
											'type': 'Boolean',
											'value':formalityWithPayment
											})
										]
									});
		listFraisDestinataires.push(indiceFormalitePayante);
		
		_log.info("listFraisDestinataires payant is {}", listFraisDestinataires);
	
}else{
	//Si la formalité n'est pas payante il ne faut afficher aucun frais et passer directement la step finish
	_log.info("***********Formalité n'est pas payante***************!!");
	var indiceFormalitePayante = spec.createGroup({
										'id':'flagFormalitePayante',
										'label':"la formalité est-elle payante?",
										'data':[
											spec.createData({
											'id': 'flagPayment',
											'label': 'flag du paiement :',
											'type': 'Boolean',
											'value':formalityWithPayment
											})
										]
									});
		listFraisDestinataires.push(indiceFormalitePayante);	
		
	var fraisDestination = spec.createGroup({
								'id':'listeFrais',
								'label':"Frais à payer",
								'data':[spec.createData({
											'id': 'aucunFrais',											
											'type': 'StringReadOnly',
											'value':"Aucun service payant n'est inclus dans votre procédure."
											})]
							});
		listFraisDestinataires.push(fraisDestination);
			_log.info("listFraisDestinataires non payant is {}", listFraisDestinataires);
}
//=================================> Partie générique pour générer le specCreate à passer à paiement Fin=========================================

//===========> Partie contenant les informations utilisateur à passer au paiement !A remplire par le redacteur de la formalité! =========================
var civility = " ";
var lastName = $m0sarl.cadre1SocieteGroup.cadre1Identite.entrepriseDenomination;
var firstName = " ";			

var infoUser = spec.createGroup({
				id: 'infoUser',
				label: "Information de l'utilisateur",
				data: [spec.createData({
							id: 'userLastName',
							label: "Nom de l'utilisateur",
							description: "Nom de l'utilisateur",
							type: 'String',
							mandatory: true,
							value: lastName
									}),
						spec.createData({
							id: 'userFirstName',
							label: "Prénom de l'utilisateur",
							type: 'String',
							mandatory : true,
							value: firstName
									}),
						spec.createData({
							id: 'civility',
							label: "Civilité",
							type: 'String',
							mandatory:true,
							value: civility
									})	
					]
			});
//===========> Fin de la Partie contenant les informations utilisateur à passer au paiement !A remplire par le redacteur de la formalité! =========================

return spec.create({
    id : 'computeDestinataire',
    label : "Génération de la liste des frais pour le paiement",
	groups : [ spec.createGroup({
					id : 'information',
					label:"Liste des frais à payer",
					data : listFraisDestinataires
				}),
				regentXmlTcBloc,
				infoUser
	]
});