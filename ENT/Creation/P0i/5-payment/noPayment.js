_log.info("************Fromalité non payante!************");
	
	return spec.create({
									id : "paymentReslt",
									label : "Payment non requis",
									groups : [ spec.createGroup({
										id : 'result',
										label : 'Finalisation',
										description : "Les services incluts dans ce dossier sont gratuit, et donc aucun frais n'est facturé au déclarant. Vous pouvez finaliser votre dossier en cliquant sur le bouton 'étape suivante'",
										data : []
									}) ]
								});	