
//Variables that should be setted by the formality redactor
var authorityType = "CFE"; // (CFE, TDR)
var authorityId = _INPUT_.dataGroup.codeEDI; // (code EDI)
var authorityLabel = _INPUT_.dataGroup.partnerLabel; // (label partenaire)
var destFuncId = _INPUT_.dataGroup.destFuncId; // (code interne de l'autorité)

var authorityType2 = "TDR"; // (CFE, TDR)
var authorityId2 = _INPUT_.dataGroup.codeEDI2; // (code EDI)
var authorityLabel2 = _INPUT_.dataGroup.partnerLabel2; // (label partenaire)
var destFuncId2 = _INPUT_.dataGroup.destFuncId2; // (code interne de l'autorité)

var listAttachments = [] ;


//nom du cerfa rempli 
var cheminLiasseCfePdf = 	_INPUT_.dataGroup.emailPj ;
var liasseCfePdf = cheminLiasseCfePdf.split("/")[3];
//Extraction du numéro de liasse du xml regent
var result = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02');
var parsedResult = JSON.parse(result);
var numeroDeLiasse = parsedResult['C02'];

//formattage des nom de fichier pour l'envoie au greffes
  var dateTemp = new Date();
    var year = dateTemp.getFullYear();
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var dateFormatted = year.toString() + month.toString() + day.toString();
//formatage de l'heure de génération
var timeFormatted = dateTemp.toTimeString().substr(0,8).replace(new RegExp(':', 'g'), '');
//renommage du nom du xml regent 
var xmlRegent1Formatted = 'C1000A1001L' + numeroDeLiasse.substr(6) + 'D' +  dateFormatted + 'H' + timeFormatted + 'TPIJTE' + 'S002' + 'PGUEN' + '.xml' ;
//renommage du cerfa pdf
var pdfCfeFormatted = 'C1000A1001L' + numeroDeLiasse.substr(6) + 'D' +  dateFormatted + 'H' + timeFormatted + 'TPIJTE' + 'S003' + 'PGUEN' + '.pdf' ;

nash.record.saveFile(xmlRegent1Formatted,nash.util.resourceFromPath('/XML_REGENT.xml').getContent() );
nash.record.saveFile(pdfCfeFormatted,nash.util.resourceFromPath(cheminLiasseCfePdf).getContent() );

if (authorityId2) {
	//renommage du deuxième xml regent
	var xmlRegent2Formatted = 'C1000A1001L' + numeroDeLiasse.substr(6) + 'D' +  dateFormatted + 'H' + timeFormatted + 'TPIJTE' + 'S004' + 'PGUEN' + '.xml' ;
	nash.record.saveFile(xmlRegent2Formatted,nash.util.resourceFromPath('/XML_REGENT2.xml').getContent() );
				
}

_log.info("numeroDeLiasse is  {}",numeroDeLiasse);
var listData = []; 
//ajout du xml rgeent 1
var dataElement1 = 	spec.createData({
						'id': "XML_REGENT_CFE",
						'label': 'XML REGENT CFE :',
						'type': 'String',
						'mandatory': true,
						'value':xmlRegent1Formatted
						})	;
listData.push(dataElement1);
var attachementElement = {"id" : xmlRegent1Formatted,  "label" : "/" + xmlRegent1Formatted};
listAttachments.push(attachementElement);
//ajout du pdf 1
var dataElement2 = 	spec.createData({
						'id': "CERFA_PDF_CFE",
						'label': 'CERFA PDF CFE :',
						'type': 'String',
						'mandatory': true,
						'value':pdfCfeFormatted
						})	;
listData.push(dataElement2);
var attachementElement = {"id" : pdfCfeFormatted,  "label" : "/" + pdfCfeFormatted};
listAttachments.push(attachementElement);
//ajout du xml regent tdr si présent 
var j = 4;
if (authorityId2) {
var dataElement3 = 	spec.createData({
						'id': "XML_REGENT_TDR",
						'label': 'XML REGENT TDR :',
						'type': 'String',
						'mandatory': true,
						'value':xmlRegent2Formatted
						})	;
listData.push(dataElement3);
j = j + 1 ;
}


var jsonRecord = nash.util.convertToJson(_record);
_log.info("record info is {}",jsonRecord);

var attachementsGroup = _record.attachmentPreprocess.attachmentPreprocess ; //; 


// liste des PJ pour la step génération xml tc
var listPjData = []; 

//Ajout
//=====>
		for (var cle in attachementsGroup ) {
		var pjGroup = attachementsGroup[cle] ; 
		_log.info("pjGroup is {}",attachementsGroup[cle]);
		_log.info("typeof pjGroup is {}",typeof attachementsGroup[cle]);
		var arrayLenght = pjGroup.length;
		
					for (var i = 0; i < arrayLenght ; i++){ 

					//rename
					var filePath = "/2-attachment/uploaded/attachmentPreprocess." + cle +"-" + pjGroup[i].id + "-" + pjGroup[i].label;
					var indice = "S0";
					var fileType = pjGroup[i].label; 
					fileType = fileType.split('.');
					fileType = fileType[fileType.length - 1];

					_log.info("filePath name of  pjDNCDeclarant splitted is {}",fileType );
					_log.info("typeof of  pjDNCDeclarant  is {}",typeof pjGroup );
					_log.info("pjDNCDeclarant  is {}", pjGroup );

					if (j<10) { indice = indice + "0" + j } else { indice = indice +  j }
					var newFileName = 'C1000A1001L' + numeroDeLiasse.substr(6) + 'D' +  dateFormatted + 'H' + timeFormatted + 'TPIJTE' + indice + 'PGUEN' + '.' + fileType ;

					_log.info("filePath is {}",filePath );
					nash.record.saveFile(newFileName,nash.util.resourceFromPath(filePath).getContent() );

					var pj = 	spec.createData({
											'id': "PJ" + indice,
											'label': 'PJ :',
											'type': 'String',
											'mandatory': true,
											'value':newFileName
											})	;
					listPjData.push(pj);
					var attachementElement = {"id" : newFileName,  "label" : "/" + newFileName};
					listAttachments.push(attachementElement);

					i++;
					j++;
					};
	
		}
//<======

_log.info("listAttachments is {}", listAttachments);


_log.info("attachementsGroup info is {}",attachementsGroup);
_log.info("type of attachementsGroup info is {}",typeof attachementsGroup);

	return spec.create({
	id : 'xmlTcGenerationConfirmation',
	label : "Xml TC Regent confirmation message",
	groups : [ spec.createGroup({
		id : 'confirmationMessageOk',
		description : "Le fichier XML TC a été généré et ajouté au dossier.",
		data : listData
		}),spec.createGroup({
		id : 'pjGenerated',
		description : "liste des pièces justificatives.",
		data : listPjData
		})]
	});
