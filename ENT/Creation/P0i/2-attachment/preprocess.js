// PJ Déclarant

var userDeclarant;
if ($p0i.cadrePersonneIdentiteGroup.cadrePersonneIdentite.personneNomUsage != null) {
    var userDeclarant = $p0i.cadrePersonneIdentiteGroup.cadrePersonneIdentite.personneNomUsage + '  ' + $p0i.cadrePersonneIdentiteGroup.cadrePersonneIdentite.personnePrenom[0] ;
} else {
    var userDeclarant = $p0i.cadrePersonneIdentiteGroup.cadrePersonneIdentite.personneNomNaissance + '  '+ $p0i.cadrePersonneIdentiteGroup.cadrePersonneIdentite.personnePrenom[0] ;
}

var pj=$p0i.cadreSignatureGroup.cadreSignature.signataire;
if(Value('id').of(pj).contains('signataireQualiteDeclarant')) {
    attachment('pjIDDeclarantSignataire', 'pjIDDeclarantSignataire', { label: userDeclarant, mandatory:"true"});
}

var pj=$p0i.cadreSignatureGroup.cadreSignature.signataire;
if(Value('id').of(pj).contains('signataireQualiteMandataire')) {
    attachment('pjIDDeclarantNonSignataire', 'pjIDDeclarantNonSignataire', { label: userDeclarant, mandatory:"true"});
}

// PJ Mandataire

var userMandataire=$p0i.cadreSignatureGroup.cadreSignature.mandataireAdresse
var pj=$p0i.cadreSignatureGroup.cadreSignature.signataire;
if(Value('id').of(pj).contains('signataireQualiteMandataire')) {
    attachment('pjIDMandataireSignataire', 'pjIDMandataireSignataire', {label: userMandataire.mandataireAdresseNomPrenomDenomination, mandatory:"true"});
}

if(Value('id').of(pj).contains('signataireQualiteMandataire') and $p0i.cadreEIRLGroup.cadreEIRL.estEIRL) {
    attachment('pjPouvoir', 'pjPouvoir', { mandatory:"true"});
}

// PJ EIRL

var pj=$p0i.cadreEIRLGroup.cadreEIRL;
if(pj.estEIRL) {
    attachment('pjDAP', 'pjDAP', { mandatory:"true"});
	var dateJour = new Date()
	var dateNaissance = $p0i.cadrePersonneIdentiteGroup.cadrePersonneIdentite.personneDateNaissance;
		if (new Number((new Date().getTime() - new Date(dateNaissance).getTime()) / 31536000000).toFixed(0) <  18 ) {
		attachment('pjAutorisationMineur', 'pjAutorisationMineur', { mandatory:"true"});}
}

var pj=$p0i.cadreEIRLGroup.cadreEIRL.cadreEIRLInformationsComplementaires.contenuDAP;
if(Value('id').of(pj).contains('bienImmo')) {
    attachment('pjDAPActeNotarie', 'pjDAPActeNotarie', { mandatory:"true"});
}

var pj=$p0i.cadreEIRLGroup.cadreEIRL.cadreEIRLInformationsComplementaires.contenuDAP;
if(Value('id').of(pj).contains('bienRapportEvaluation')) {
    attachment('pjDAPRapportEvaluation', 'pjDAPRapportEvaluation', { mandatory:"true"});
}

var pj=$p0i.cadreEIRLGroup.cadreEIRL.cadreEIRLInformationsComplementaires.contenuDAP;
if(Value('id').of(pj).contains('bienCommunIndivis')) {
    attachment('pjDAPAccordTiers', 'pjDAPAccordTiers', { mandatory:"true"});
}