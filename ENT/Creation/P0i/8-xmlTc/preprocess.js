var date = new Date();
var dateHeureGeneration = date.toISOString().slice(0,19);

//Infos dossier
var numeroDossierUnique = nash.record.description().recordUid; 
var nomDossier = nash.record.description().title; 
//Infos de correspondance 
//==========>
//Nom et Prénom de correspondance
var resNomCorrespondant = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C36');
var resultNomCorrespondant = JSON.parse(resNomCorrespondant);
var nomCorrespondant = resultNomCorrespondant['C36'];
//Numéro de voie
var resNumVoie = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.5');
var resultNumVoie = JSON.parse(resNumVoie);
var numeroDeVoie = resultNumVoie['C37.5'];
//Indice de répétition
var resindRep = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6');
var resultindRep = JSON.parse(resindRep);
var indRep = resultindRep['C37.6'];
//Type de voie
var resTypeDeVoie = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11');
var resultTypeDeVoie = JSON.parse(resTypeDeVoie);
var typeDeVoie = resultTypeDeVoie['C37.11'];
//Libellé voie
var resLibelleDeVoie = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12');
var resultLibelleDeVoie = JSON.parse(resLibelleDeVoie);
var libelleDeVoie = resultLibelleDeVoie['C37.12'];
//Libellé de la localité
var resLocalite = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13');
var resultLocalite = JSON.parse(resLocalite);
var localite = resultLocalite['C37.13'];
//Code postal
var resCodePostal = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8');
var resultCodePostal = JSON.parse(resCodePostal);
var codePostal = resultCodePostal['C37.8'];
//complément de localisation
var complementDeLocalisation ;

var resComplLoca = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.10');
var resultComplLoca = JSON.parse(resComplLoca);
var complLoca = resultComplLoca['C37.10'];
if (complLoca){
	complementDeLocalisation = complLoca ;
}
var resDestSpe = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.7');
var resultDestSpe = JSON.parse(resDestSpe);
var destSpe = resultDestSpe['C37.7'];
if (destSpe){
	if(complementDeLocalisation){
		complementDeLocalisation = complementDeLocalisation + '-' + destSpe; 
	}else{
		complementDeLocalisation = destSpe; 
	}
}
//Adresse Email
var resAdresseEmail = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.3');
var resultAdresseEmail = JSON.parse(resAdresseEmail);
var adresseEmail = resultAdresseEmail['C39.3'];
if (adresseEmail == null || adresseEmail == '') {
	adresseEmail= 'no.mail@adresse.fr';
}


//<=========





//Variables that should be setted by the formality redactor
var authorityType = "CFE"; // (CFE, TDR)
var authorityId = _INPUT_.dataGroup.codeEDI; // (code EDI)
var authorityLabel = _INPUT_.dataGroup.partnerLabel; // (label partenaire)
var destFuncId = _INPUT_.dataGroup.destFuncId; // (code interne de l'autorité)

var authorityType2 = "TDR"; // (CFE, TDR)
var authorityId2 = _INPUT_.dataGroup.codeEDI2; // (code EDI)
var authorityLabel2 = _INPUT_.dataGroup.partnerLabel2; // (label partenaire)
var destFuncId2 = _INPUT_.dataGroup.destFuncId2; // (code interne de l'autorité)

var listBindAttachments = [] ;
var attachementElement ;
//nom du cerfa rempli 
var cheminLiasseCfePdf = 	_INPUT_.dataGroup.emailPj ;
var liasseCfePdf = cheminLiasseCfePdf.split("/")[3];




_log.info("dateHeureGeneration is " + dateHeureGeneration);
_log.info("authorityId is  {}", authorityId);
_log.info("authorityLabel is  {}", authorityLabel);
_log.info("nomCorrespondant is  {}", nomCorrespondant);
_log.info("typeDeVoie is  {}", typeDeVoie);
_log.info("libelleDeVoie is  {}", libelleDeVoie);
_log.info("localite is  {}", localite);
_log.info("numeroDeVoie is  {}", numeroDeVoie);
_log.info("codePostal is  {}", codePostal);
_log.info("bureauDistributeur is  {}", localite);
_log.info("adresseEmail is  {}", adresseEmail);
var jsonRecord = nash.util.convertToJson(_record);
_log.info("record info in step 9 is {}",jsonRecord);

var attachementsList = _record.xmlTcGenerationConfirmation.pjGenerated ; // 
var liasseList = _record.xmlTcGenerationConfirmation.confirmationMessageOk ; //
_log.info("attachementsList in step 9 is {}",attachementsList);
_log.info("attachementsList values are  {}",attachementsList.values());
_log.info("typeof attachementsList in step 9 is {}",typeof attachementsList);
_log.info("liasseList in step 9 is {}",liasseList);
_log.info("typeof liasseList in step 9 is {}",typeof liasseList);

var result = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02');
var parsedResult = JSON.parse(result);
var numeroDeLiasse = parsedResult['C02'];

//formattage des nom de fichier pour l'envoie au greffes
var d = new Date;
var month = ((d.getMonth() + 1) < 10) ? "0" + (d.getMonth() + 1) : (d.getMonth() + 1);
var day = (d.getDate() < 10) ? "0" + (d.getDate()) : d.getDate();
var year = d.getFullYear();
var dateFormatted = year.toString() + month.toString() + day.toString();
var timeFormatted = d.toTimeString().substr(0,8).replace(new RegExp(':', 'g'), '');
var xmlRegent1Formatted =  liasseList["XML_REGENT_CFE"];

var pdfCfeFormatted = liasseList["CERFA_PDF_CFE"];

if (authorityId2) {
	var xmlRegent2Formatted = liasseList["XML_REGENT_TDR"];
}

_log.info("numeroDeLiasse is  {}",numeroDeLiasse);

//TODO : Attachment part to be discussed how to generate it and how it should be managed
var indiceCerfaCfe ;
var indiceXmlCfe;
var indiceCerfaTdr;
var indiceXmlTdr;
var indicesPj = [] ;


var xmlTcJson = {}
xmlTcJson["version"] = "V2012.02";
xmlTcJson["emetteur"] = "10001001";
xmlTcJson["destinataire"] = "10000001";
xmlTcJson["dateHeureGenerationXml"] = dateHeureGeneration;
xmlTcJson["commentaire"] = "";


var dossierUnique ={};

var identificationDossierUnique ={};

var identifiantDossierUnique ={};
identifiantDossierUnique["codePartenaireEmetteur"] = "FRONT-OFFICE";
identifiantDossierUnique["numeroDossierUnique"] = numeroDossierUnique;

var correspondant ={};

var adresseCorrespondant ={}
adresseCorrespondant["numeroDeVoie"] = numeroDeVoie				
adresseCorrespondant["indiceDeRepetition"] = indRep				
adresseCorrespondant["typeDeVoie"] = typeDeVoie	
adresseCorrespondant["libelleDeVoie"] = libelleDeVoie			
adresseCorrespondant["localite"] = localite	
adresseCorrespondant["complementDeLocalisation"] = complementDeLocalisation
adresseCorrespondant["codePostal"] = codePostal	
adresseCorrespondant["bureauDistributeur"] = localite		
adresseCorrespondant["adresseEmail"] = adresseEmail	

correspondant["identiteCorrespondant"] = { "nomCorrespondant": nomCorrespondant};	
correspondant["adresseCorrespondant"] = adresseCorrespondant;

identificationDossierUnique["identifiantDossierUnique"] = identifiantDossierUnique;
identificationDossierUnique["typeDossierUnique"] = "D1";
identificationDossierUnique["nomDossier"] = nomDossier;
identificationDossierUnique["correspondant"] = correspondant;

var destinataireDossierUnique = [];

var oneDestinataire = {};

var codeDestinataire = {};
codeDestinataire["codePartenaire"] = authorityId;
codeDestinataire["codeEdi"] = authorityId;
codeDestinataire["libelleIntervenant"] = authorityLabel;

oneDestinataire["roleDestinataire"] = authorityType;
oneDestinataire["codeDestinataire"] = codeDestinataire;

destinataireDossierUnique.push(oneDestinataire);

//cas d'un deuxième destinataire
if (authorityId2) {
	var secondDestinataire = {};
	var codeDestinataire2 = {};
	codeDestinataire2["codePartenaire"] = authorityId2;
	codeDestinataire2["codeEdi"] = authorityId2;
	codeDestinataire2["libelleIntervenant"] = authorityLabel2;

	secondDestinataire["roleDestinataire"] = authorityType2;
	secondDestinataire["codeDestinataire"] = codeDestinataire2;

	destinataireDossierUnique.push(secondDestinataire);
}
var pieceJointes =[];
var indicePj = 2;

//liasse cfe xml
var pieceJointe1={};
pieceJointe1["indicePieceJointe"] = indicePj;
pieceJointe1["typePieceJointe"] = "LIASSE_CFE_EDI";
pieceJointe1["formatPieceJointe"] = "XML";
pieceJointe1["fichierPieceJointe"] = xmlRegent1Formatted;
indiceXmlCfe = indicePj;
indicePj++;
attachementElement = {"id" : xmlRegent1Formatted,  "label" : "/" + xmlRegent1Formatted};
listBindAttachments.push(attachementElement);


//liasse cfe pdf
var pieceJointe3={};
pieceJointe3["indicePieceJointe"] = indicePj;
pieceJointe3["typePieceJointe"] = "LIASSE_CFE_PDF";
pieceJointe3["formatPieceJointe"] = "PDF";
pieceJointe3["fichierPieceJointe"] = pdfCfeFormatted;
indiceCerfaCfe = indicePj;
indicePj++
attachementElement = {"id" : pdfCfeFormatted,  "label" : "/" + pdfCfeFormatted};
listBindAttachments.push(attachementElement);			


pieceJointes.push(pieceJointe1);
pieceJointes.push(pieceJointe3);


if (authorityId2) {
	// liasse tdr xml
	var pieceJointe2={};
	pieceJointe2["indicePieceJointe"] = indicePj;
	pieceJointe2["typePieceJointe"] = "LIASSE_TDR_EDI";
	pieceJointe2["formatPieceJointe"] = "XML";
	pieceJointe2["fichierPieceJointe"] = xmlRegent2Formatted;//"XML_REGENT2.xml";
	indiceXmlTdr = indicePj;
	indicePj++
	attachementElement = {"id" : xmlRegent2Formatted,  "label" : "/" + xmlRegent2Formatted};
	listBindAttachments.push(attachementElement);		

	pieceJointes.push(pieceJointe2);
}

//Ajout des pièces justificatives 
//var attachmentsNames = attachementsList.keys(); 
//_log.info("attachmentsNames in step 9 is {}",attachmentsNames);
//_log.info("attachmentsNames length in step 9 is {}",attachmentsNames.size());
//_log.info("typeof attachmentsNames in step 9 is {}",typeof attachmentsNames);	
for (var cle in attachementsList ) {
	_log.info("attachmentsNames[i] in step 9 is {}",attachementsList[cle]);
	_log.info("typeof attachmentsNames[i] in step 9 is {}",typeof attachementsList[cle]);
	var fileType = attachementsList[cle]; 
	fileType = fileType.split('.');
	fileType = fileType[fileType.length - 1];			


	var pieceJointeUser={};
	pieceJointeUser["indicePieceJointe"] = indicePj;
	pieceJointeUser["typePieceJointe"] = "AUTRE_PJ";
	pieceJointeUser["formatPieceJointe"] = fileType;
	pieceJointeUser["fichierPieceJointe"] = attachementsList[cle];
	indicesPj.push(indicePj);
	indicePj++; 
	attachementElement = {"id" : attachementsList[cle],  "label" : "/" + attachementsList[cle]};
	listBindAttachments.push(attachementElement);			
	pieceJointes.push(pieceJointeUser);

}

var dossierCfe={};

var destinataireDossierCfe=[];

//dossier destinataire cfe 1
var oneDestinataireDossierCfe = {};


var indicePieceJointeCFE = [];
indicePieceJointeCFE.push(indiceCerfaCfe);

//insérer les indices des PJ pour ce partenaire
var indicesPjLength = indicesPj.length;
for (var i = 0; i < indicesPjLength; i++){
	indicePieceJointeCFE.push(indicesPj[i]);
}


oneDestinataireDossierCfe["indicePieceJointe"]=indicePieceJointeCFE;
oneDestinataireDossierCfe["roleDestinataire"]= authorityType;
oneDestinataireDossierCfe["codeEdiDestinataire"]= authorityId;				
oneDestinataireDossierCfe["indiceLiasseXml"]=indiceXmlCfe;

destinataireDossierCfe.push(oneDestinataireDossierCfe);	

//dossier destinataire cfe 1
if (authorityId2) {
	var oneDestinataireDossierCfe2 = {};


	var indicePieceJointeTDR = [];
	indicePieceJointeTDR.push(indiceCerfaCfe);
	//insérer les indices des PJ pour ce partenaire
	for (var i = 0; i < indicesPjLength; i++){
		indicePieceJointeTDR.push(indicesPj[i]);
	}


	oneDestinataireDossierCfe2["indicePieceJointe"]=indicePieceJointeTDR;
	oneDestinataireDossierCfe2["roleDestinataire"]= authorityType2;
	oneDestinataireDossierCfe2["codeEdiDestinataire"]= authorityId2;				
	//oneDestinataireDossierCfe2["paiementDossier2"]=paiementDossier2;
	oneDestinataireDossierCfe2["indiceLiasseXml"]=indiceXmlTdr;
	destinataireDossierCfe.push(oneDestinataireDossierCfe2);
}


dossierCfe["numeroDeLiasse"] = numeroDeLiasse;			 
dossierCfe["dateHeureDepot"] = dateHeureGeneration;			 
dossierCfe["referenceLiasseFo"] = numeroDossierUnique;			 
dossierCfe["destinataireDossierCfe"] = destinataireDossierCfe;	

dossierUnique["identificationDossierUnique"] = identificationDossierUnique;
dossierUnique["destinataireDossierUnique"] = destinataireDossierUnique;
dossierUnique["pieceJointe"] = pieceJointes;
dossierUnique["dossierCfe"] = dossierCfe;

xmlTcJson["dossierUnique"] = dossierUnique;			





_log.info("xmlTcJson is  {}", xmlTcJson);
_log.info("JSON.stringify(xmlTcJson) is  {}", JSON.stringify(xmlTcJson));

//Call to the XML-TC generation WS 
var response = nash.service.request('${regent.baseUrl}/private/v1/xml-tc/generate')
.dataType('application/json') //
.accept('application/json') //
.post(JSON.stringify(xmlTcJson));
var xmlTcFormatted = 'C1000A1001L' + numeroDeLiasse.substr(6) + 'D' +  dateFormatted + 'H' + timeFormatted + 'TXMLTC' + 'S001' + 'PGUEN' + '.xml' ;
//Record the generated XML-REGENT 
if (response != null && response.status == 200) { 
	var xmlTC = response.asBytes(); 
	nash.record.saveFile(xmlTcFormatted,xmlTC); 

	attachementElement = {"id" : xmlTcFormatted,  "label" : "/" + xmlTcFormatted};
	listBindAttachments.push(attachementElement);	
	var dmtduName = 'C1000A1001L' + numeroDeLiasse.substr(6) + 'D' +  dateFormatted + 'H' + timeFormatted + 'TDMTDUPGUEN' + '.zip';
	//bind return 
	var output1 = nash.instance.load("output1.xml")
	output1.bind("parameters",{

		"content" : {
			"attachment" : "/" + pdfCfeFormatted,
			"regentAttachment" : "/" + xmlRegent1Formatted,
			"entry" : listBindAttachments
		}
	});


	output1.bind("result", {
		"funcId" : destFuncId,
		"codeEdi" : authorityId
	});


	//bind return 2nd authorityI
	if (destFuncId2) {
		//bind return 
		var output2 = nash.instance.load("output2.xml")
		output2.bind("parameters",{

			"content" : {
				"attachment" : "/" + pdfCfeFormatted,
				"regentAttachment" : "/" + xmlRegent2Formatted,
				"entry" : listBindAttachments
			}
		});


		output2.bind("result", {
			"funcId" : destFuncId2,
			"codeEdi" : authorityId2
		});
	}



	return spec.create({
		id : 'xmlTcGenerationConfirmation',
		label : "Xml TC Regent confirmation message",
		groups : [ spec.createGroup({
			id : 'confirmationMessageOk',
			description : "Le fichier XML TC a été généré et ajouté au dossier.",
			data : []
		}) ]
	});
}else{
	return spec.create({
		id : 'xmlTcGenerationConfirmation',
		label : "Xml TC Regent confirmation message",
		groups : [ spec.createGroup({
			id : 'confirmationMessageKo',
			description : "Une erreur a été relevé lors de la génération du XML TC.",
			data : []
		}) ]
	});
}