var formFields = {};
var cerfaFields = {};
function pad(s) { return (s < 10) ? '0' + s : s; }


// Cadre - Activité non salariée

var TnsOui = $p0i.cadrePersonneIdentiteGroup.cadrePersonneTNSOui;

formFields['formalite_evenement_oui']                                                   = TnsOui.personneActiviteTNSOui ? true : false;
formFields['formalite_evenement_non']                                                   = TnsOui.personneActiviteTNSOui ? false : true;
formFields['voletSocial_activiteExerceeAnterieurementSIREN']                            = TnsOui.personneActiviteTNSSIREN != null ? TnsOui.personneActiviteTNSSIREN.split(' ').join('') : '';

// Cadre- Identité

var identite = $p0i.cadrePersonneIdentiteGroup.cadrePersonneIdentite;
formFields['personneLiee_personnePhysique_nomNaissance']                                = identite.personneNomNaissance;
formFields['personneLiee_personnePhysique_nomUsage']                                    = identite.personneNomUsage;

var prenoms=[];
for ( i = 0; i < identite.personnePrenom.size() ; i++ ){prenoms.push(identite.personnePrenom[i]);}                            
formFields['personneLiee_personnePhysique_prenom1']                                      = prenoms.toString();

formFields['personneLiee_personnePhysique_civiliteMasculin']                            = Value('id').of(identite.personneCivilite).eq('civiliteMasculin') ? true : false;
formFields['personneLiee_personnePhysique_civiliteFeminin']                             = Value('id').of(identite.personneCivilite).eq('civiliteFeminin') ? true : false;
formFields['personneLiee_personnePhysique_nationalite']                                 = identite.personneNationalite;

if(identite.personneDateNaissance != null) {
	var dateTmp = new Date(parseInt(identite.personneDateNaissance.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['personneLiee_personnePhysique_dateNaissance'] = date;
}
formFields['personneLiee_personnePhysique_lieuNaissanceDepartement']                    = identite.personneDepartementNaissance != null ? identite.personneDepartementNaissance.getId() : '';
formFields['personneLiee_personnePhysique_lieuNaissanceCommune']                        = (identite.personneCommuneNaissance != null ?  identite.personneCommuneNaissance : identite.personneVilleNaissance) + ' / ' + identite.personnePaysNaissance;

var adresse = $p0i.cadrePersonneIdentiteGroup.cadrePersonneAdresse;

formFields['personneLiee_adresse_voie']                                              = (adresse.personneAdresseNumeroVoie != null ? adresse.personneAdresseNumeroVoie : '') 
																						+ ' ' + (adresse.personneAdresseIndiceVoie != null ? adresse.personneAdresseIndiceVoie : '') 
																						+ ' ' + (adresse.personneAdresseTypeVoie != null ? adresse.personneAdresseTypeVoie : '') 
																						+ ' ' + adresse.personneAdresseNomVoie
																						+ ' ' + (adresse.personneAdresseComplementVoie != null ? adresse.personneAdresseComplementVoie : '') 
																						+ ' ' + (adresse.personneAdresseDistributionSpeciale != null ? adresse.personneAdresseDistributionSpeciale : '');
formFields['personneLiee_adresse_codePostal']                                           = adresse.personneAdresseCodePostal;
formFields['personneLiee_adresse_commune']                                              = adresse.personneAdresseCommune != null ? adresse.personneAdresseCommune : (adresse.personneAdresseVille + ' / ' + adresse.personneAdressePays);
formFields['personneLiee_adresse_communeAncienne']                                      = adresse.personneAdresseCommuneAncienne != null ? adresse.personneAdresseCommuneAncienne : '';



//Cadre 3 - Conjoint

/* var conjoint = $p0i.cadre2conjointGroup.cadre2Conjoint;

formFields['personneLiee_conjointCollaborateurSalarie_collaborateur']                   = Value('id').of(conjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieCollaborateur') ? true : false;
formFields['personneLiee_conjointCollaborateurSalarie_salarie']                         = Value('id').of(conjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieSalarie') ? true : false;
formFields['personneLiee_personnePhysique_nomNaissance_conjointPacse']                  = conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueNomNaissanceConjointPacse;
formFields['personneLiee_personnePhysique_nomUsage_conjointPacse']                      = conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueNomUsageConjointPacse;
var prenoms=[];
for ( i = 0; i < conjoint.cadre2InfosConjoint.personneLieePersonnePhysiquePrenom1ConjointPacse.size() ; i++ ){prenoms.push(conjoint.cadre2InfosConjoint.personneLieePersonnePhysiquePrenom1ConjointPacse[i]);}                            
formFields['personneLiee_personnePhysique_prenom1_conjointPacse']                                      = prenoms.toString();
if(conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueDateNaissanceConjointPacse != null) {
	var dateTmp = new Date(parseInt(conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueDateNaissanceConjointPacse.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['personneLiee_personnePhysique_dateNaissance_conjointPacse'] = date;
}
formFields['personneLiee_personnePhysique_lieuNaissanceDepartement_conjointPacse']  = (conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueLieuNaissanceDepartementConjointPacse != null ? conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueLieuNaissanceDepartementConjointPacse.getId() : '');

formFields['personneLiee_personnePhysique_lieuNaissanceCommune_conjointPacse']      = Value('id').of(conjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieCollaborateur') ? ((conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueLieuNaissanceCommuneConjointPacse != null ? conjoint.cadre2InfosConjoint.personneLieePersonnePhysiqueLieuNaissanceCommuneConjointPacse : conjoint.cadre2InfosConjoint.personneLieePPLieuNaissanceVilleConjointPacse)
																					+ ' / ' + conjoint.cadre2InfosConjoint.personneLieePersonnePhysiquePaysNaissanceConjointPacse) : '' ; */
                                                                                  
//Cadre 4 - ACCRE
//formFields['entreprise_demandeACCRE']                               = $p0i.cadre1IdentiteGroup.cadre1Identite.entrepriseDemandeACCRE ? true : false;

//Cadre - EIRL

formFields['estEIRL']  = $p0i.cadreEIRLGroup.cadreEIRL.estEIRL ? true : false;

//Cadres - Adresse pro

var adressePro = $p0i.cadreActiviteGroup.cadreActiviteAdresse;

formFields['entreprise_adresse_domicile']                                     = Value('id').of(adressePro.activiteLieu).eq('activiteAdresseDomicile') ? true : false;
formFields['entreprise_adresse_pro']                                           = Value('id').of(adressePro.activiteLieu).eq('activiteAdressePro') ? true : false;

formFields['entrepriseLiee_adresse_voie']                                              =(adressePro.cadreActiviteAdresseProfessionnelle.activiteAdresseNumeroVoie != null ? adressePro.cadreActiviteAdresseProfessionnelle.activiteAdresseNumeroVoie : '')
																				            + ' ' + (adressePro.cadreActiviteAdresseProfessionnelle.activiteAdresseIndiceVoie != null ? adressePro.cadreActiviteAdresseProfessionnelle.activiteAdresseIndiceVoie : '')
																							+ ' ' + (adressePro.cadreActiviteAdresseProfessionnelle.activiteAdresseTypeVoie != null ? adressePro.cadreActiviteAdresseProfessionnelle.activiteAdresseTypeVoie : '')
																				            + ' ' + (adressePro.cadreActiviteAdresseProfessionnelle.activiteAdresseNomVoie != null ? adressePro.cadreActiviteAdresseProfessionnelle.activiteAdresseNomVoie : '')
																							+ ' ' + (adressePro.cadreActiviteAdresseProfessionnelle.activiteAdresseComplementVoie != null ? adressePro.cadreActiviteAdresseProfessionnelle.activiteAdresseComplementVoie : '')
																				            + ' ' + (adressePro.cadreActiviteAdresseProfessionnelle.activiteAdresseDistributionSpeciale != null ? adressePro.cadreActiviteAdresseProfessionnelle.activiteAdresseDistributionSpeciale : '');
formFields['entrepriseLiee_adresse_codePostal']                                          = adressePro.cadreActiviteAdresseProfessionnelle.activiteAdresseCodePostal;
formFields['entrepriseLiee_adresse_commune']                                             = adressePro.cadreActiviteAdresseProfessionnelle.activiteAdresseCommune;
formFields['entrepriseLiee_adresse_communeAncienne']                                     = adressePro.cadreActiviteAdresseProfessionnelle.activiteAdresseCommuneAncienne != null ? adressePro.cadreActiviteAdresseProfessionnelle.activiteAdresseCommuneAncienne : '';


// Cadre - Activité

var activite = $p0i.cadreActiviteGroup.cadreActiviteDescription ;

if(activite.activiteDateDebut != null) {
	var dateTmp = new Date(parseInt(activite.activiteDateDebut.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_dateDebutActivite'] = date;
}	
formFields['entreprise_activites']                                             	= activite.activitesExercees;
formFields['entreprise_activitePlusImportante']                                 = activite.activitePrincipale;
formFields['entreprise_activitePermanenteSaisonniere_permanente']               = Value('id').of(activite.activitePermanenteSaisonniere).eq('activitePermanente') ? true : false;
formFields['entreprise_activitePermanenteSaisonniere_saisonniere']              = Value('id').of(activite.activitePermanenteSaisonniere).eq('activiteSaisonniere') ? true : false;
//formFields['etablissment_nonSedentariteQualite_nonSedentaire']                          = activite.etablissmentNonSedentariteQualiteNonSedentaire ? true : false;

var origine = $p0i.cadreActiviteGroup.cadreActiviteOrigine;

formFields['entreprise_origineFonds_creation']                               	= Value('id').of(origine.activiteOrigine).eq('activiteOrigineCreation') ? true : false;
formFields['entreprise_origineFonds_reprise']                                 	= Value('id').of(origine.activiteOrigine).eq('activiteOrigineReprise') ? true : false;

formFields['entreprise_siren_precedentExploitant']                             	= origine.precedentExploitant.precedentExploitantSiren != null ? origine.precedentExploitant.precedentExploitantSiren.split(' ').join('') : '';
formFields['entreprise_denomination_precedentExploitant']                      	= origine.precedentExploitant.precedentExploitantDenomination;
formFields['entreprise_nomNaissance_precedentExploitant']              			= origine.precedentExploitant.precedentExploitantNomNaissance != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant : (origine.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant : '');
formFields['entreprise_nomUsage_precedentExploitant']                  			= origine.precedentExploitant.precedentExploitantNomUsage;
//formFields['entreprise_prenom1_precedentExploitant']                         	= origine.precedentExploitant.precedentExploitantPrenom1;
var prenoms=[];
for ( i = 0; i < origine.precedentExploitantPrenom1.size() ; i++ ){prenoms.push(origine.precedentExploitantPrenom1[i]);}                            
formFields['entreprise_prenom1_precedentExploitant']                                      = prenoms.toString();

// Cadre - Exploitation en commun
formFields['estEIRL']  = $p0i.cadreExploitationCommunGroup.exploitation.estIndivision ? true : false;
var exploitation = $p0i.cadreExploitationCommunGroup.exploitation.cadreExploitationCommun;
formFields['entreprise_nom_exploitationCommun'] 			= exploitation.cadreExploitationCommunNom.exploitationCommunNom;
formFields['entreprise_siren_exploitationCommun'] 			= exploitation.cadreExploitationCommunNom.exploitationCommunSiren != null ? exploitation.cadreExploitationCommunNom.exploitationCommunSiren.split(' ').join('') : '';

// cadres multiples //
var coexploitant=[];
for ( i = 0; i < exploitation.cadreCoexploitantIdentite.size() ; i++ )
{coexploitant.push(exploitation.cadreCoexploitantIdentite[i]);    
var cadreCoexploitant = exploitation.cadreCoexploitantIdentite[i];                   
formFields['coexploitant_nom_naissance'+i]   = cadreCoexploitant.coexploitantIdentite.coexploitantNomNaissance;
formFields['coexploitant_nom_usage'+i]   = cadreCoexploitant.coexploitantIdentite.coexploitantNomUsage;

var prenomsCo=[];
for ( i = 0; i < cadreCoexploitant.coexploitantIdentite.coexploitantPrenom.size() ; i++ )
{prenomsCo.push(cadreCoexploitant.coexploitantIdentite.coexploitantPrenom[i]);}                            
formFields['coexploitant_prenom'+i']  = prenomsCo.toString();

//formFields['coexploitant_prenom'+i]   = cadreCoexploitant.coexploitantIdentite.coexploitantPrenom;
formFields['coexploitant_date_naissance'+i]   = cadreCoexploitant.coexploitantIdentite.coexploitantDateNaissance;
formFields['coexploitant_pays_naissance'+i]   = cadreCoexploitant.coexploitantIdentite.coexploitantPaysNaissance;
formFields['coexploitant_departement_naissance'+i]   = cadreCoexploitant.coexploitantIdentite.coexploitantDepartementNaissance != null ? cadreCoexploitant.coexploitantIdentite.coexploitantDepartementNaissance.getId() : '';
formFields['coexploitant_commune_naissance'+i]   = (cadreCoexploitant.coexploitantIdentite.coexploitantCommuneNaissance!= null ?  cadreCoexploitant.coexploitantIdentite.coexploitantCommuneNaissance: cadreCoexploitant.coexploitantIdentite.coexploitantVilleNaissance) + ' / ' + cadreCoexploitant.coexploitantIdentite.coexploitantPaysNaissance;
formFields['coexploitant_ville_naissance'+i]   = cadreCoexploitant.coexploitantIdentite.coexploitantVilleNaissance;
formFields['coexploitant_adresse_voie'+i]                                              = (cadreCoexploitant.coexploitantAdresse.coexploitantAdresseNumVoie != null ? cadreCoexploitant.coexploitantAdresse.coexploitantAdresseNumVoie : '') 
																						+ ' ' + (cadreCoexploitant.coexploitantAdresse.coexploitantAdresseIndiceVoie != null ? cadreCoexploitant.coexploitantAdresse.coexploitantAdresseIndiceVoie : '') 
																						+ ' ' + (cadreCoexploitant.coexploitantAdresse.coexploitantAdresseTypeVoie != null ? cadreCoexploitant.coexploitantAdresse.coexploitantAdresseTypeVoie : '') 
																						+ ' ' + cadreCoexploitant.coexploitantAdresse.coexploitantAdresseNomVoie
																						+ ' ' + (cadreCoexploitant.coexploitantAdresse.coexploitantAdresseComplementVoie != null ? cadreCoexploitant.coexploitantAdresse.coexploitantAdresseComplementVoie : '') 
																						+ ' ' + (cadreCoexploitant.coexploitantAdresse.coexploitantAdresseDistributionSpeciale != null ? cadreCoexploitant.coexploitantAdresse.coexploitantAdresseDistributionSpeciale : '');
formFields['coexploitant_adresse_codePostal'+i]                                           = cadreCoexploitant.coexploitantAdresse.coexploitantAdresseCodePostal;
formFields['coexploitant_adresse_commune'+i]                                              = cadreCoexploitant.coexploitantAdresse.coexploitantAdresseCommune != null ? cadreCoexploitant.coexploitantAdresse.coexploitantAdresseCommune : (cadreCoexploitant.coexploitantAdresse.coexploitantAdresseVille + ' / ' + cadreCoexploitant.coexploitantAdresse.coexploitantAdressePays);
formFields['coexploitant_adresse_communeAncienne'+i]                                      = cadreCoexploitant.coexploitantAdresse.coexploitantAdresseCommuneAncienne != null ? cadreCoexploitant.coexploitantAdresse.coexploitantAdresseCommuneAncienne : (cadreCoexploitant.coexploitantAdresse.coexploitantAdresseCommune + ' / ' + cadreCoexploitant.coexploitantAdresse.coexploitantAdresseCommuneAncienne);
}
//Cadre 8 - Déclaration sociale
/* 

var social = $p0i.cadre7DeclarationSocialeGroup.cadre7DeclarationSociale;

var nirDeclarant = social.voletSocialNumeroSecuriteSociale;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFields['voletSocial_numeroSecuriteSociale']                = nirDeclarant.substring(0, 13);
    formFields['voletSocial_numeroSecuriteSocialeCle']             = nirDeclarant.substring(13, 15);
}
formFields['voletSocial_titreSejour_numero']                                            = social.voletSocialTitreSejourNumero;
formFields['voletSocial_titreSejour_lieuDelivranceCommune']                             = social.voletSocialTitreSejourLieuDelivranceCommune;
if(social.voletSocialTitreSejourDateExpiration != null) {
	var dateTmp = new Date(parseInt(social.voletSocialTitreSejourDateExpiration.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['voletSocial_titreSejour_dateExpiration'] = date;
}
formFields['voletSocial_activiteAutreQueDeclaree_Oui']                                 = social.voletSocialActiviteAutreQueDeclareeStatutOui ? true : false;
formFields['voletSocial_activiteAutreQueDeclaree_Non']                                  = social.voletSocialActiviteAutreQueDeclareeStatutOui ? false : true;
formFields['voletSocial_activiteAutreQueDeclareeStatut_salarie']                        = Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutSalarie') ? true : false;
formFields['voletSocial_activiteAutreQueDeclareeStatut_salarieAgricole']                = Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutSalarieAgricole') ? true : false;
formFields['voletSocial_activiteAutreQueDeclareeStatut_retraitePensionne']              = Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutRetraitePensionne') ? true : false;
formFields['voletSocial_activiteAutreQueDeclareeStatut_autre'] 				            = Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutCocheAutre') ? true : false;
formFields['voletSocial_activiteAutreQueDeclareeStatutAutre']                           = social.voletSocialActiviteAutreQueDeclareeStatutAutre;
formFields['voletSocial_optionMicroSocialVersement_mensuel']           	             	= Value('id').of(social.optionMicroSocialSimplifie).eq('VoletSocialOptionMicroSocialVersementMensuel') ? true : false;
formFields['voletSocial_optionMicroSocialVersement_trimestriel']                		= Value('id').of(social.optionMicroSocialSimplifie).eq('VoletSocialOptionMicroSocialVersementTrimestriel') ? true : false;
formFields['voletSocial_conjointCouvertAssuranceMaladie_oui']                           = Value('id').of($p0i.cadre2conjointGroup.cadre2Conjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieCollaborateur') ? (conjoint.cadre2InfosConjoint.voletSocialConjointCouvertAssuranceMaladieOui ? true : false) : false;
formFields['voletSocial_conjointCouvertAssuranceMaladie_non']                           = Value('id').of($p0i.cadre2conjointGroup.cadre2Conjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieCollaborateur') ? (conjoint.cadre2InfosConjoint.voletSocialConjointCouvertAssuranceMaladieOui ? false : true) : false;

var nirConjoint = conjoint.cadre2InfosConjoint.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(nirConjoint != null) {
    nirConjoint = nirConjoint.replace(/ /g, "");
    formFields['voletSocial_conjointCollaborateurNumeroSecuriteSociale']                = nirConjoint.substring(0, 13);
    formFields['voletSocial_conjointCollaborateurNumeroSecuriteSocialeCle']             = nirConjoint.substring(13, 15);
} */



//Cadre - Option fiscale hors EIRL

var optionsFiscalesSelect = $p0i.cadreOptionsFiscalesHorsEirlGroup.cadreOptionsFiscalesHorsEirl;
formFields['OptionsFiscalesHorsEirlBNC']  = Value('id').of(optionsFiscalesSelect.optionsFiscalesHorsEirl).eq('OptionsFiscalesHorsEirlBNC') ? true : false;
formFields['OptionsFiscalesHorsEirlBIC']  = Value('id').of(optionsFiscalesSelect.optionsFiscalesHorsEirl).eq('optionsFiscalesHorsEirlBIC') ? true : false;

var optionsFiscalesBic = $p0i.cadreOptionsFiscalesHorsEirlGroup.cadreOptionsFiscalesHorsEirl.cadreoptionsFiscalesHorsEirlBIC;
var declarationAffectation = $p0i.cadreEIRLGroup.cadreEIRL.cadreEirlDap.eirlDeclarationPatrimoine;

if($p0i.cadreEIRLGroup.cadreEIRL.estEIRL and not declarationAffectation.eirlObjetPartiel) {	
formFields['regimeFiscal_regimeImpositionBenefices_mBic']                               = Value('id').of(optionsFiscalesBic.impositionBenefices.regimeFiscalImpositionBenefice).eq('regimeFiscalImpositionBeneficesMBIC') ? true : false;
formFields['regimeFiscal_regimeImpositionBenefices_rsBic']                              = Value('id').of(optionsFiscalesBic.impositionBenefices.regimeFiscalImpositionBenefice).eq('regimeFiscalImpositionBeneficesRsBIC') ? true : false;
formFields['regimeFiscal_regimeImpositionBenefices_rnBic']                              = Value('id').of(optionsFiscalesBic.impositionBenefices.regimeFiscalImpositionBenefice).eq('regimeFiscalImpositionBeneficesRnBIC') ? true : false;
		if (optionsFiscalesBic.impositionBenefices.regimeFiscalDateClotureExerciceComptable !== null) {
			var dateTmp = new Date(parseInt(optionsFiscalesBic.impositionBenefices.regimeFiscalDateClotureExerciceComptable.getTimeInMillis()));
			var dateClotureEc = pad(dateTmp.getDate().toString());
			var month = dateTmp.getMonth() + 1;
			dateClotureEc = dateClotureEc.concat(pad(month.toString()));
			formFields['regimeFiscal_dateClotureExerciceComptableBic']          = dateClotureEc;
		}
formFields['regimeFiscal_regimeImpositionTVA_rfTVABic']                                    = Value('id').of(optionsFiscalesBic.impositionBenefices.regimeFiscalImpositionBenefice).eq('regimeFiscalImpositionBeneficesMBIC') ? true : 
																						((Value('id').of(optionsFiscales.optionsTVA.regimeTVA1).eq('regimeFiscalRegimeImpositionTVARfTVA') or Value('id').of(optionsFiscalesBic.optionsTVA.regimeTVA2).eq('regimeFiscalRegimeImpositionTVARfTVA')) ? true : false); 
formFields['regimeFiscal_regimeImpositionTVA_rsTVABic']                                    = Value('id').of(optionsFiscales.optionsTVA.regimeTVA1).eq('regimeFiscalRegimeImpositionTVARsTVA') ? true : false;
formFields['regimeFiscal_regimeImpositionTVA_mrTVABic']                                    = Value('id').of(optionsFiscales.optionsTVA.regimeTVA1).eq('regimeFiscalRegimeImpositionTVAMrTVA') ? true : false;
formFields['regimeFiscal_regimeImpositionTVA_rnTVABic']                                    = Value('id').of(optionsFiscales.optionsTVA.regimeTVA2).eq('regimeFiscalRegimeImpositionTVARnTVA') ? true : false;
formFields['regimeFiscal_regimeImpositionTVAOptionsParticulieres2Bic']                     = optionsFiscales.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres2 ? true : false;
formFields['regimeFiscal_regimeImpositionTVAOptionsParticulieres1Bic']                     = optionsFiscales.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres1 ? true : false;
// option fiscale Bnc
var optionsFiscalesBnc = $p0i.cadreOptionsFiscalesHorsEirlGroup.cadreOptionsFiscalesHorsEirl.cadreOptionsFiscalesHorsEirlBNC;
formFields['regimeFiscal_regimeImpositionBeneficesOptionsParticulieres_optionCreanceDetteBnc'] = optionsFiscalesBnc.impositionBenefices.regimeFiscalImpositionBeneficesOptionsParticulieresOptionCreanceDette ? true : false;
formFields['regimeFiscal_regimeImpositionBenefices_dcBnc']                                  = Value('id').of(optionsFiscalesBnc.impositionBenefices.regimeFiscalImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesDcBNC') ? true : false;
formFields['regimeFiscal_regimeImpositionBenefices_rsBnc']                                  = Value('id').of(optionsFiscalesBnc.impositionBenefices.regimeFiscalImpositionBenefice).eq('regimeFiscalImpositionBeneficesRsBNC') ? true : false;
formFields['regimeFiscal_regimeImpositionTVA_rfTVABnc']                                        = Value('id').of(optionsFiscalesBnc.impositionBenefices.regimeFiscalImpositionBenefice).eq('regimeFiscalImpositionBeneficesRsBNC') ? true : (Value('id').of(optionsFiscales.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARfTVA') ? true : false);
formFields['regimeFiscal_regimeImpositionTVA_rsTVABnc']                                        = Value('id').of(optionsFiscalesBnc.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARsTVA') ? true : false;
formFields['regimeFiscal_regimeImpositionTVA_rnTVABnc']                                        = Value('id').of(optionsFiscalesBnc.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARnTVA') ? true : false;
formFields['regimeFiscal_regimeImpositionTVAOptionsParticulieres1Bnc']                         = optionsFiscalesBnc.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres1 ? true : false;
}


//Cadre - Observations

var correspondance = $p0i.cadreRensCompGroup.cadreRensComp ;

formFields['formalite_observations']                                                    = correspondance.rensCompObservations;


// Cadre - Adresse de correspondance
//formFields['formalite_correspondance_cadre2']                                           = Value('id').of(correspondance.rensCompAdresseCorrespondance).eq('domi') ? true : false;
//formFields['formalite_correspondance_cadre5']                                           = Value('id').of(correspondance.rensCompAdresseCorrespondance).eq('prof') ? true : false;
formFields['formalite_correspondance_cadre']   											= Value('id').of(correspondance.rensCompAdresseCorrespondance).eq('domi') or Value('id').of(correspondance.rensCompAdresseCorrespondance).eq('prof') ? true : false;
formFields['formalite_correspondance_cadreNum']                                         = Value('id').of(correspondance.rensCompAdresseCorrespondance).eq('domi') ? "2" : (Value('id').of(correspondance.rensCompAdresseCorrespondance).eq('prof') ? "5" : ' ');

formFields['formalite_correspondance_autre']                                           	= Value('id').of(correspondance.rensCompAdresseCorrespondance).eq('autre') ? true : false;
//formFields['formalite_correspondanceAdresse']                                           = Value('id').of(correspondance.rensCompAdresseCorrespondance).eq('autre') ? true : false;
formFields['formalite_correspondanceAdresse_nom']                                       = correspondance.correspondanceAdresse.correspondanceAdresseNomPrenomDenomination != null ? correspondance.correspondanceAdresse.correspondanceAdresseNomPrenomDenomination : '';
formFields['formalite_correspondanceAdresse_voie_complement']                           = (correspondance.correspondanceAdresse.correspondanceAdresseNumeroVoie != null ? correspondance.correspondanceAdresse.correspondanceAdresseNumeroVoie : '')
																				        + ' ' + (correspondance.correspondanceAdresse.correspondanceAdresseIndiceVoie != null ? correspondance.correspondanceAdresse.correspondanceAdresseIndiceVoie : '')
																				        + ' ' + (correspondance.correspondanceAdresse.correspondanceAdresseTypeVoie != null ? correspondance.correspondanceAdresse.correspondanceAdresseTypeVoie : '')
																						+ ' ' + (correspondance.correspondanceAdresse.correspondanceAdresseNomVoie  != null ? correspondance.correspondanceAdresse.correspondanceAdresseNomVoie :'')
                                                                                        + ' ' + (correspondance.correspondanceAdresse.correspondanceAdresseComplementVoie != null ? correspondance.correspondanceAdresse.correspondanceAdresseComplementVoie : '') 
																						+ ' ' + (correspondance.correspondanceAdresse.correspondanceAdresseDistributionSpeciale != null ? correspondance.correspondanceAdresse.correspondanceAdresseDistributionSpeciale : '');
formFields['formalite_correspondanceAdresse_codePostal']                                = correspondance.correspondanceAdresse.correspondanceAdresseCodePostal;
formFields['formalite_correspondanceAdresse_commune']									= correspondance.correspondanceAdresse.correspondanceAdresseCommune;
formFields['formalite_telephone1']                                                      = correspondance.infosSup.infosSupTelephone1 != null ? correspondance.infosSup.infosSupTelephone1 : '';
formFields['formalite_telephone2']                                                      = correspondance.infosSup.infosSupTelephone2;
formFields['formalite_fax_courriel']                                                    = correspondance.infosSup.infosSupFaxCourriel != null ? correspondance.infosSup.infosSupFaxCourriel : correspondance.infosSup.infosSupTelecopie;


//Cadre - Non diffusion

var signataire = $p0i.cadreSignatureGroup.cadreSignature ;

formFields['formalite_nonDiffusionInformation']                                         = signataire.nonDiffusionInformation ? true : false;


//Cadre - Signature

formFields['formalite_signataireQualite_declarant']                                  = Value('id').of(signataire.signataire). eq('signataireQualiteDeclarant') ? true : false;
formFields['formalite_signataireQualite_mandataire']                                 = Value('id').of(signataire.signataire). eq('signataireQualiteMandataire') ? true : false;
// formFields['nomPrenomDenominationMandataire']										 = Value('id').of(signataire.signataire). eq('signataireQualiteMandataire') ? signataire.mandataireAdresse.mandataireAdresseNomPrenomDenomination : '';	
// formFields['adresseMandataire_voie']                                                 = Value('id').of(signataire.signataire). eq('signataireQualiteMandataire') ? 
																					// ((signataire.mandataireAdresse.mandataireAdresseNumeroVoie != null ? signataire.mandataireAdresse.mandataireAdresseNumeroVoie + ' ' : '') 
																					// + ' ' + (signataire.mandataireAdresse.mandataireAdresseNumeroVoie != null ? signataire.mandataireAdresse.mandataireAdresseNumeroVoie : '')
																					// + ' ' + (signataire.mandataireAdresse.mandataireAdresseTypeVoie != null ? signataire.mandataireAdresse.mandataireAdresseTypeVoie : '')
																					// + ' ' + (signataire.mandataireAdresse.mandataireAdresseNomVoie != null ? signataire.mandataireAdresse.mandataireAdresseNomVoie : '')
// mandataireAdressecomplementVoie														+ ' ' + (signataire.mandataireAdresse.mandataireAdresseDistributionSpeciale != null ? signataire.mandataireAdresse.mandataireAdresseDistributionSpeciale : '')) : '';
// formFields['adresseMandataire_commune']                                             = Value('id').of(signataire.signataire). eq('signataireQualiteMandataire') ? 
																					// ((signataire.mandataireAdresse.mandataireAdresseCodePostal != null ? signataire.mandataireAdresse.mandataireAdresseCodePostal : '')
																					// + ' ' + (signataire.mandataireAdresse.mandataireAdresseCommune != null ? signataire.mandataireAdresse.mandataireAdresseCommune : '')) : '';

formFields['nomPrenomDenominationAdresseMandataire']	 = Value('id').of(signataire.signataire). eq('signataireQualiteMandataire') ?
															((Value('id').of(signataire.signataire). eq('signataireQualiteMandataire') ? signataire.mandataireAdresse.mandataireAdresseNomPrenomDenomination + ' ' : '')
															+ ' ' + (signataire.mandataireAdresse.mandataireAdresseNumeroVoie != null ? signataire.mandataireAdresse.mandataireAdresseNumeroVoie + ' ' : '') 
															+ ' ' + (signataire.mandataireAdresse.mandataireAdresseNumeroVoie != null ? signataire.mandataireAdresse.mandataireAdresseNumeroVoie : '')
															+ ' ' + (signataire.mandataireAdresse.mandataireAdresseTypeVoie != null ? signataire.mandataireAdresse.mandataireAdresseTypeVoie : '')
															+ ' ' + (signataire.mandataireAdresse.mandataireAdresseNomVoie != null ? signataire.mandataireAdresse.mandataireAdresseNomVoie : '')
															+ ' ' + (signataire.mandataireAdresse.mandataireAdresseDistributionSpeciale != null ? signataire.mandataireAdresse.mandataireAdresseDistributionSpeciale : '')
															+ ' ' + (signataire.mandataireAdresse.mandataireAdresseCodePostal != null ? signataire.mandataireAdresse.mandataireAdresseCodePostal : '')
															+ ' ' + (signataire.mandataireAdresse.mandataireAdresseCommune != null ? signataire.mandataireAdresse.mandataireAdresseCommune : '')) : '';



formFields['formalite_signatureLieu']                                               = signataire.signatureLieu;
if(signataire.signatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.signatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['formalite_signatureDate'] = date;
}
formFields['estEIRL_oui']                                                               = $p0i.cadreEIRLGroup.cadreEIRL.estEIRL ? true : false;
formFields['estEIRL_non']                                                               = $p0i.cadreEIRLGroup.cadreEIRL.estEIRL ? false : true;
formFields['entreprise_intercalaireP0_nombre']                              = "0";
formFields['signature']                                                     = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce";


// Intercalaire PEIRL

//Cadre - Déclaration ou modification d'affectation de patrimoine

formFields['eirl_complete_p0i']                                                          = true;
formFields['eirl_complete_P2p4']                                                            = false;
formFields['declaration_initiale']                                                          = $p0i.cadreEIRLGroup.cadreEIRL.estEIRL ? true : false;
formFields['declaration_modification']                                                      = false;


// Cadre - Rappel d'identification

formFields['eirl_nomNaissance']                                = identite.personneNomNaissance;
formFields['eirl_nomUsage']                                     = identite.personneNomUsage;

var prenoms=[];
for ( i = 0; i < identite.personnePrenom.size() ; i++ ){prenoms.push(identite.personnePrenom[i]);}                            
formFields['eirl_prenom']                                     = prenoms.toString();

// Cadre - Déclaration d'affectation de patrimoine
//pour rappel var declarationAffectation = $p0i.cadreEIRLGroup.cadreEIRL.cadreEirlDap.eirlDeclarationPatrimoine;

formFields['eirl_statutEIRL_declarationInitiale']                                           = Value('id').of(declarationAffectation.eirlStatut).eq('eirlDeclarationInitiale') ? true : false;
formFields['eirl_statutEIRL_reprise']                                                       = Value('id').of(declarationAffectation.eirlStatut).eq('eirlReprise') ? true : false;
formFields['eirl_denomination']                                                             = declarationAffectation.eirlDeclarationPatrimoine.eirlDenomination;
if (declarationAffectation.eirlDeclarationPatrimoine.eirlDateClotureExerciceComptable !== null) {
    var dateTmp = new Date(parseInt(declarationAffectation.eirlDeclarationPatrimoine.eirlDateClotureExerciceComptable.getTimeInMillis()));
    var dateClotureEc = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateClotureEc = dateClotureEc.concat(pad(month.toString()));
    formFields['eirl_dateClotureExerciceComptable']          = dateClotureEc;
}
formFields['eirl_objet']                                                                    = declarationAffectation.eirlDeclarationPatrimoine.eirlObjetPartiel ? declarationAffectation.eirlDeclarationPatrimoine.eirlObjet : $p0i.cadreActiviteGroup.cadreActiviteDescription.activitesExercees;
formFields['eirl_precedentEIRLDenomination']                                                = declarationAffectation.eirlReprisePatrimoine.eirlPrecedentDenomination;
formFields['eirl_precedentEIRLLieuImmatriculation']                                         = declarationAffectation.eirlReprisePatrimoine.eirlPrecedentLieuImmatriculation;
formFields['eirl_precedentEIRLSIREN']                                                       = declarationAffectation.eirlReprisePatrimoine.eirlPrecedentSIREN != null ? (declarationAffectation.eirlReprisePatrimoine.eirlPrecedentSIREN.split(' ').join('')) : '';

// Cadre - Options fiscales

var eirlRegimeImposition = $p0i.cadreEIRLGroup.cadreEIRL.cadreEirlOptionsFiscales;

formFields['eirlOptionsFiscalesBNC']  = Value('id').of(eirlRegimeImposition.eirlOptionsFiscales).eq('eirlOptionsFiscalesBNC') ? true : false;
formFields['eirlOptionsFiscalesBIC']  = Value('id').of(eirlRegimeImposition.eirlOptionsFiscales).eq('eirlOptionsFiscalesBIC') ? true : false;

//option BNC
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rsBnc']                                  = Value('id').of(eirlRegimeImposition.eirlImpositionBeneficesBNC.regimeFiscalImpositionBeneficeBnc).eq('regimeFiscalImpositionBeneficesRsBNC') ? true : false;
//formFields['regimeFiscal_regimeImpositionBeneficesVersementLiberatoire']                    = optionsFiscales.impositionBenefices.regimeFiscalRegimeImpositionBeneficesVersementLiberatoire ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_dcBnc']                                  = Value('id').of(eirlRegimeImposition.eirlImpositionBeneficesBNC.regimeFiscalImpositionBeneficeBnc).eq('regimeFiscalImpositionBeneficesDcBN') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBeneficesOptionsParticulieres_optionCreanceDetteBnc'] = eirlRegimeImposition.eirlImpositionBeneficesBNC.regimeFiscalImpositionBeneficesOptionsParticulieresOptionCreanceDette ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBeneficesOptionsParticulieres_optionISBnc']           = Value('id').of(eirlRegimeImposition.eirlImpositionBeneficesBNC.regimeFiscalImpositionBeneficeBnc).eq('regimeFiscalImpositionBeneficesOptionsParticulieresOptionIS') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rsISBnc']                                   = Value('id').of(eirlRegimeImposition.eirlImpositionBeneficesBNC.regimeFiscalIS).eq('regimeFiscalImpositionBeneficesRsIS') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rnISBnc']                                   = Value('id').of(eirlRegimeImposition.eirlImpositionBeneficesBNC.regimeFiscalIS).eq('regimeFiscalImpositionBeneficesRnIS') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVA_rfTVABnc']                                        = Value('id').of(eirlRegimeImposition.eirlImpositionBeneficesBNC.optionsTVAbnc.regimeTVA).eq('regimeFiscalRegimeImpositionTVARfTVA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVA_rsTVABnc']                                        = Value('id').of(eirlRegimeImposition.eirlImpositionBeneficesBNC.optionsTVAbnc.regimeTVA).eq('regimeFiscalRegimeImpositionTVARsTVA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVA_rnTVABnc']                                        = Value('id').of(eirlRegimeImposition.eirlImpositionBeneficesBNC.optionsTVAbnc.regimeTVA).eq('regimeFiscalRegimeImpositionTVARnTVA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVAOptionsParticulieres1Bnc']                         = eirlRegimeImposition.eirlImpositionBeneficesBNC.optionsTVAbnc.regimeFiscalRegimeImpositionTVAOptionsParticulieres1DC ? true : false;

//option BIC
formFields['eirl_regimeFiscal_regimeImpositionBenefices_mBIC']                              =  Value('id').of(eirlRegimeImposition.eirlImpositionBeneficesBIC.regimeFiscalImpositionBeneficeBic).eq('regimeFiscalImpositionBeneficesMBIC') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rsBIC']                             = (Value('id').of(eirlRegimeImposition.eirlImpositionBeneficesBIC.regimeFiscalImpositionBeneficeBic).eq('regimeFiscalImpositionBeneficesReelBIC') and Value('id').of(eirlRegimeImposition.eirlImpositionBeneficesBIC.regimeFiscalReel).eq('regimeFiscalImpositionBeneficesRs')) ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rnBIC']                             = (Value('id').of(eirlRegimeImposition.eirlImpositionBeneficesBIC.regimeFiscalImpositionBeneficeBic).eq('regimeFiscalImpositionBeneficesReelBIC') and Value('id').of(eirlRegimeImposition.eirlImpositionBeneficesBIC.regimeFiscalReel).eq('regimeFiscalImpositionBeneficesRn')) ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_optionISBic']                          =  Value('id').of(eirlRegimeImposition.eirlImpositionBeneficesBIC.regimeFiscalImpositionBeneficeBic).eq('regimeFiscalImpositionBeneficesOptionIS') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rsISBic']                              = (Value('id').of(eirlRegimeImposition.eirlImpositionBeneficesBIC.regimeFiscalImpositionBeneficeBic).eq('regimeFiscalImpositionBeneficesOptionIS') and Value('id').of(eirlRegimeImposition.eirlImpositionBeneficesBIC.regimeFiscalReel).eq('regimeFiscalImpositionBeneficesRs')) ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rnISBic']                              = (Value('id').of(eirlRegimeImposition.eirlImpositionBeneficesBIC.regimeFiscalImpositionBeneficeBic).eq('regimeFiscalImpositionBeneficesOptionIS') and Value('id').of(eirlRegimeImposition.eirlImpositionBeneficesBIC.regimeFiscalReel).eq('regimeFiscalImpositionBeneficesRn')) ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVA_rfTVABic']                                   =  Value('id').of(eirlRegimeImposition.eirlImpositionBeneficesBIC.regimeFiscalImpositionBeneficeBic).eq('regimeFiscalImpositionBeneficesMBIC') ? true : (Value('id').of(eirlRegimeImposition.optionsTVAbic.regimeTVA1).eq('regimeFiscalRegimeImpositionTVARfTVA') ? true : (Value('id').of(eirlRegimeImposition.optionsTVAbic.regimeTVA2).eq('regimeFiscalRegimeImpositionTVARfTVA') ? true : false));
formFields['eirl_regimeFiscal_regimeImpositionTVA_rsTVABic']                                   =  Value('id').of(eirlRegimeImposition.eirlImpositionBeneficesBIC.optionsTVAbic.regimeTVA1).eq('regimeFiscalRegimeImpositionTVARsTVA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVA_mrTVABic']                                   = Value('id').of(eirlRegimeImposition.eirlImpositionBeneficesBIC.optionsTVAbic.regimeTVA1).eq('regimeFiscalRegimeImpositionTVAMrTVA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVA_rnTVABic']                                   = Value('id').of(eirlRegimeImposition.eirlImpositionBeneficesBIC.optionsTVAbic.regimeTVA2).eq('regimeFiscalRegimeImpositionTVARnTVA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVAOptionsParticulieres3Bic']                    = eirlRegimeImposition.eirlImpositionBeneficesBIC.optionsTVAbic.regimeFiscalRegimeImpositionTVAOptionsParticulieres3 ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVAOptionsParticulieres2Bic']                    = eirlRegimeImposition.eirlImpositionBeneficesBIC.optionsTVAbic.regimeFiscalRegimeImpositionTVAOptionsParticulieres2 ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVAOptionsParticulieres1Bic']                    = eirlRegimeImposition.eirlImpositionBeneficesBIC.optionsTVAbic.regimeFiscalRegimeImpositionTVAOptionsParticulieres1 ? true : false;


/*
 * Création du dossier avec volet social : ajout du cerfa avec volet social
 */
var cerfaDoc1 = nash.doc //
	.load('models/cerfa_11921-04_p0i_avec_option_fisc.pdf') //
	.apply(formFields);

/*
 * Création du dossier sans volet social : ajout du cerfa sans volet social
 */
 
 var cerfaDoc2 = nash.doc //
	.load('models/cerfa_11921-04_p0i_sans_option_fisc.pdf') //
	.apply (formFields);
	cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));


/*
 * Ajout de l'intercalaire PEIRL Impôt avec option fiscale
 */
if ($p0i.cadreEIRLGroup.cadreEIRL.estEIRL == true)
{
	var peirlImpotDoc = nash.doc //
		.load('models/cerfa_14217-02_peirlImpot_avec_option_fisc.pdf') //
		.apply (formFields);
	cerfaDoc1.append(peirlImpotDoc.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire PEIRL Impôt sans option fiscale
 */
if ($p0i.cadreEIRLGroup.cadreEIRL.estEIRL == true)
{
	var peirlImpotDoc = nash.doc //
		.load('models/cerfa_14217-02_peirlImpot_sans_option_fisc.pdf') //
		.apply (formFields);
	cerfaDoc1.append(peirlImpotDoc.save('cerfa.pdf'));
}

function appendPj(fld) {
	fld.forEach(function (elm) {
        cerfaDoc1.append(elm);
    });
}

/*
 * Ajout des PJs
 */

// PJ Déclarant

var pj=$p0i.cadreSignatureGroup.cadreSignature.signataire;


if(Value('id').of(pj).contains('signataireQualiteDeclarant')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantSignataire);
}

if(Value('id').of(pj).contains('signataireQualiteMandataire')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantNonSignataire);
} 

// PJ Mandataire

if(Value('id').of(pj).contains('signataireQualiteMandataire')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire);
}

if(Value('id').of(pj).contains('signataireQualiteMandataire') and $p0i.cadreEIRLGroup.cadreEIRL.estEIRL) {
    appendPj($attachmentPreprocess.attachmentPreprocess.pjPouvoir);
}

// PJ Eirl

var pj=$p0i.cadreEIRLGroup.cadreEIRL;
if(pj.estEIRL) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjDAP);
	appendPj($attachmentPreprocess.attachmentPreprocess.pjAutorisationMineur);
}

/* var pj=$p0i.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine.cadreEIRLInformationsComplementaires.contenuDAP;
if(Value('id').of(pj).contains('bienImmo')) {
    appendPj($attachmentPreprocess.attachmentPreprocess.pjDAPActeNotarie);
}

var pj=$p0i.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine.cadreEIRLInformationsComplementaires.contenuDAP;
if(Value('id').of(pj).contains('bienRapportEvaluation')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjDAPRapportEvaluation);
}

var pj=$p0i.cadreEIRLGroup.cadreEIRL.cadre1DeclarationAffectationPatrimoine.cadreEIRLInformationsComplementaires.contenuDAP;
if(Value('id').of(pj).contains('bienCommunIndivis')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjDAPAccordTiers);
}
 */


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDoc = cerfaDoc1.save('P0i_Creation.pdf');


return spec.create({
    id : 'review',
    label : 'Déclaration de début d\'activité non salariée indépendante',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'La démarche de déclaration de début d\'activité non salariée indépendante est maintenant terminée.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDoc ]
        }) ]
    }) ]
});
