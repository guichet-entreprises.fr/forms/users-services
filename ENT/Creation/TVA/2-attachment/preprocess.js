attachment('preuveActiviteFrance', 'preuveActiviteFrance');
attachment('certifInscriRegitreCommerce', 'certifInscriRegitreCommerce', {
	mandatory : "false"
});
attachment('copieStatut', 'copieStatut', {
	mandatory : "false"
});

attachment('pjID', 'pjID', {
	mandatory : "false"
});

var ajouMandataire = $mandataire.mandatairePage.mandatireOuiNon;

if (ajouMandataire.ajouterMandataire) {

	attachment('pjMandat', 'pjMandat', {
		mandatory : "true"
	});
}
