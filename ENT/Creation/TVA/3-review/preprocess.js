function pad(s) { return (s < 10) ? '0' + s : s; }
var civiliteNomPrenom = $mandataire.etatCivilGarantPage.etatCivilGarantGroupe.nomGarant +' '+ $mandataire.etatCivilGarantPage.etatCivilGarantGroupe.prenmsGarant;
var formFields = {};



/*******************************************************************************
 * Identification
 ******************************************************************************/
var cheminIdentification = $mandataire.identificationPage.identificationGroupe;

formFields['numTVACodePays']                          = cheminIdentification.numTVACodePays;
formFields['numTVA']                          		  = cheminIdentification.numTVA;
formFields['numIDRegistreCommerce']                   = cheminIdentification.numIDRegistreCommerce;
formFields['numTVAFrance']                            = cheminIdentification.numTVAFrance;


/*******************************************************************************
 * Dénomination de la societé ou de l'entreprise individuelle
 ******************************************************************************/
var cheminSocieteOuEntIndiv = $mandataire.denomSocieteENTIndivPage.denomSocieteENTIndivGroupe;

formFields['denominationSociete']                     = cheminSocieteOuEntIndiv.denominationSociete;
formFields['formeJuridique']                          = cheminSocieteOuEntIndiv.formeJuridique;
formFields['adresseInternet']						  = cheminSocieteOuEntIndiv.adresseInternet;


/*******************************************************************************
 * État civil du garant de la société
 ******************************************************************************/
var cheminGarantSociete = $mandataire.etatCivilGarantPage.etatCivilGarantGroupe;

formFields['nomGarant']                               = cheminGarantSociete.nomGarant;
formFields['prenmsGarant']                            = cheminGarantSociete.prenmsGarant;


if(cheminGarantSociete.dateNaissanceGarant != null) {
	var dateTemp = new Date(parseInt (cheminGarantSociete.dateNaissanceGarant.getTimeInMillis()));
	   	var monthTemp = dateTemp.getMonth() + 1;
	    	var month = pad(monthTemp.toString());
	   	var day =  pad(dateTemp.getDate().toString());
	    	var year = dateTemp.getFullYear();
	    	formFields['jourNaissanceGarant'] = day;
	    	formFields['moisNaissanceGarant'] = month;
	    	formFields['anneeNaissanceGarant'] = year;
}

formFields['villeNaissanceGarant']                    = cheminGarantSociete.villeNaissanceGarant;
formFields['paysNaissanceGarant']                     = cheminGarantSociete.paysNaissanceGarant;


/*******************************************************************************
 * Coordonnées du siège social
 ******************************************************************************/
var cheminSiegeSocial = $mandataire.coordonneesSiegeSocialPage.coordonneesSiegeSocialGroupe;


formFields['adesseSiegeSocial']                       = cheminSiegeSocial.numVoieSiegeSocial +' '+ (cheminSiegeSocial.complementAdresseSiegeSocial != null ? cheminSiegeSocial.complementAdresseSiegeSocial : ' ') +' '+(cheminSiegeSocial.codePostalSiegeSocial != null ? cheminSiegeSocial.codePostalSiegeSocial : " ") +' '+ cheminSiegeSocial.communeSiegeSocial +' '+ cheminSiegeSocial.paysSiegeSocial;
formFields['telephoneSiegeSocial']                    = cheminSiegeSocial.telephoneSiegeSocial;
formFields['telecopieSiegeSocial']                    = cheminSiegeSocial.telecopieSiegeSocial;
formFields['mailSiegeSocial']                         = cheminSiegeSocial.mailSiegeSocial;


/*******************************************************************************
 * Nature et conditions d'exercice de l'activité
 ******************************************************************************/
var cheminExoActivite = $mandataire.natureExercicePage.natureExerciceGroupe;


formFields['detailActiviteFrance']                    = cheminExoActivite.detailActiviteFrance;
formFields['activiteOcasionnelle']                    = Value('id').of(cheminExoActivite.faconDexercer).eq('activiteOcasionnelle');
formFields['activiteReguliere']                    	  = Value('id').of(cheminExoActivite.faconDexercer).eq('activiteReguliere');

if(cheminExoActivite.datePremiereDeclarationFrance != null) {
	var dateTemp = new Date(parseInt (cheminExoActivite.datePremiereDeclarationFrance.getTimeInMillis()));
	   	var monthTemp = dateTemp.getMonth() + 1;
	    	var month = pad(monthTemp.toString());
	   	var day =  pad(dateTemp.getDate().toString());
	    	var year = dateTemp.getFullYear();
	    	formFields['jourPremiereOperation'] = day;
	    	formFields['moisPremiereOperation'] = month;
	    	formFields['anneePremiereOperation'] = year;
}

formFields['typeClienteleIDFr']                       = Value('id').of(cheminExoActivite.typeClientele).contains('typeClienteleIDFr') ? true : false;
formFields['typeClienteleParticulier']                = Value('id').of(cheminExoActivite.typeClientele).contains('typeClienteleParticulier') ? true : false;
formFields['typeClientelNonIDFr']                     = Value('id').of(cheminExoActivite.typeClientele).contains('typeClientelNonIDFr') ? true : false;
formFields['aicFranceOui']                            = cheminExoActivite.aicFranceOuiNon ? true : false;
formFields['aicFranceNon']                            = cheminExoActivite.aicFranceOuiNon ? false : true;
formFields['licExportFranceOui']                      = cheminExoActivite.licExportFranceOuiNon ? true : false;
formFields['licExportFranceNon']                      = cheminExoActivite.licExportFranceOuiNon ? false : true;
formFields['debOui']                                  = cheminExoActivite.debOuiNon ? true : false;
formFields['debNon']                                  = cheminExoActivite.debOuiNon ? false : true;
formFields['chiffreAffaireVente']                     = cheminExoActivite.chiffreAffaireVente;
formFields['chiffreAffairePresta']                    = cheminExoActivite.chiffreAffairePresta;


/*******************************************************************************
 * Coordonnées du mandataire
 ******************************************************************************/
var cheminMandataire = $mandataire.mandatairePage.mandataireGroupe;


formFields['nomPrenomOuDenominationMandataire']       = cheminMandataire.denominatioMandataire != null ? cheminMandataire.denominatioMandataire : (cheminMandataire.nomMandataire +' '+ cheminMandataire.prenomMandataire);
formFields['personneOuserviceContactMandataire']      = cheminMandataire.PersonneOuserviceContactMandataire;
formFields['adresseMandataire']                       = cheminMandataire.numVoieMandataire +' '+ (cheminMandataire.complementAdresseMandataire != null ? cheminMandataire.complementAdresseMandataire :'') +' '+ (cheminMandataire.codePostalMandataire != null ? cheminMandataire.codePostalMandataire : '')+' '+ cheminMandataire.communeMandataire +' '+ cheminMandataire.paysMandataire;
formFields['telephoneMandataire']                     = cheminMandataire.telephoneMandataire;
formFields['faxMandataire']                           = cheminMandataire.faxMandataire;
formFields['mailMandataire']                          = cheminMandataire.mailMandataire;


/*******************************************************************************
 * Adresse de correspondance
 ******************************************************************************/
var cheminAdrCorrespondance = $mandataire.adresseCorrespondancePage.adresseCorrespondanceGroupe;

formFields['personneOuServiceContacteCorrespondance'] = cheminAdrCorrespondance.personneOuServiceContacteCorrespondance;
formFields['denominationCorrespondance']              = cheminAdrCorrespondance.denominationCorrespondance;
formFields['adresseCorrespondance']                   = cheminAdrCorrespondance.numVoieCorrespondance +' '+ (cheminAdrCorrespondance.complementAdesseCorrespondance != null ? cheminAdrCorrespondance.complementAdesseCorrespondance :'') +' '+ (cheminAdrCorrespondance.codePostalCorrespondance != null ? cheminAdrCorrespondance.codePostalCorrespondance : '')+' '+ cheminAdrCorrespondance.communeCorrespondance +' '+ cheminAdrCorrespondance.paysCorrespondance;


/*******************************************************************************
 * Coordonées du comptable qui remplira les déclaration de TVA
 ******************************************************************************/
var cheminComptable = $mandataire.coordonneesComptablePage.coordonneesComptableGroupe;

formFields['nomPrenomOuDenomComptable']               = cheminComptable.denominationComptable != null ? cheminComptable.denominationComptable : ((cheminComptable.nomComptable != null ? cheminComptable.nomComptable : ' ') +' '+ (cheminComptable.prenomComptable != null ? cheminComptable.prenomComptable : ' '));
formFields['personneOuServiceContactComptable']		  = cheminComptable.personneOuServiceContactComptable
formFields['adresseComptable']                        = (cheminComptable.numVoieComptable != null ? cheminComptable.numVoieComptable :' ') +' '+ (cheminComptable.complementAdresseComptable != null ? cheminComptable.complementAdresseComptable : ' ')+' '+ (cheminComptable.codePostalComptable != null ? cheminComptable.codePostalComptable : ' ') +' '+cheminComptable.communeComptable != null ? cheminComptable.communeComptable : ' ';
formFields['telephoneComptable']                      = cheminComptable.telephoneComptable;
formFields['faxComptable']                            = cheminComptable.telecopieComptable;
formFields['emailComptable']                          = cheminComptable.mailComptable;


/*******************************************************************************
 * Signature
 ******************************************************************************/
var cheminSignature = $mandataire.signaturePage.signatureGroupe;

if(cheminSignature.dateSignature != null) {
	var dateTemp = new Date(parseInt (cheminSignature.dateSignature.getTimeInMillis()));
	   	var monthTemp = dateTemp.getMonth() + 1;
	    	var month = pad(monthTemp.toString());
	   	var day =  pad(dateTemp.getDate().toString());
	    	var year = dateTemp.getFullYear();
	    	formFields['jourSignature'] = day;
	    	formFields['moisSignature'] = month;
	    	formFields['anneeSignature'] = year;
}

formFields['lieuSignature']                           = cheminSignature.lieuSignature;
 formFields['signature'] 							  = civiliteNomPrenom;


/*******************************************************************************
 * Chargement du courrier d'accompagnement à destination de l'AC
 ******************************************************************************/

var cerfaDoc = nash.doc //
	.load('models/Courrier_au_premier_dossier_v2.1_GE.pdf') //
	.apply({
	dateSignature : $mandataire.signaturePage.signatureGroupe.dateSignature ,
	autoriteHabilitee  : "DIRECTION GENERALE DES FINANCES PUBLIQUES ", 
	autoriteHabilitee1 : "DIRECTION DES RESIDENTS A L’ETRANGER ET DES SERVICES GENERAUX ",
	autoriteHabilitee2 : "Service des Impôts des Entreprises Etrangères ",
	autoriteHabilitee3 :"10 rue du Centre - TSA 20011 93465 NOISY LE GRAND Cedex FRANCE" ,
	demandeContexte : "Déclaration d'inscription auprès de l'administration fiscale par un assujetti établi dans un autre Etat membre de l'Union européenne que la France ne disposant pas d'établissement stable en France.",
	civiliteNomPrenom : civiliteNomPrenom
	});


var finalDoc = nash.doc //
.load('models/cerfa_15415-01.pdf') //
.apply (formFields);


cerfaDoc.append(finalDoc.save('cerfa.pdf'));	

/*******************************************************************************
 * Pieces jointes
 ******************************************************************************/

function appendPj(fld) {
    fld.forEach(function (elm) {
    	cerfaDoc.append(elm);
    });
}

 appendPj($attachmentPreprocess.attachmentPreprocess.preuveActiviteFrance);
 appendPj($attachmentPreprocess.attachmentPreprocess.certifInscriRegitreCommerce);
 appendPj($attachmentPreprocess.attachmentPreprocess.copieStatut);
 appendPj($attachmentPreprocess.attachmentPreprocess.pjID);

 if (cheminExoActivite.ajouterMandataire) {
 appendPj($attachmentPreprocess.attachmentPreprocess.pjMandat);
 }


/*******************************************************************************
 * Enregistrement du fichier (en mémoire)
 ******************************************************************************/

var finalDocItem = cerfaDoc.save('cerfa_15415-01.pdf');	


/*******************************************************************************
 * Persistance des données obtenues
 ******************************************************************************/

return spec.create({
id : 'review',
label : 'Déclaration d\'inscription auprès de l\'administration fiscale par un assujetti établi dans un autre état membre de l\'Union européenne que la France ne disposant pas d\'établissement stable en France et réalisant des opérations imposables à la TVA en France ou devant y accomplir des obligations déclaratives.',
groups : [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : [ spec.createData({
        id : 'formulaire',
        label : "Déclaration d\'inscription auprès de l\'administration fiscale par un assujetti établi dans un autre état membre de l\'Union européenne que la France ne disposant pas d\'établissement stable en France et réalisant des opérations imposables à la TVA en France ou devant y accomplir des obligations déclaratives.",
        description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
        type : 'FileReadOnly',
        value : [ finalDocItem ]
    }) ]
}) ]
});