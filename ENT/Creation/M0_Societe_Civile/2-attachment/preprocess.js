// PJ Dirigeant personne physique 1

var societe = $m0SCI.cadre1SocieteGroup.cadre1Identite;
var dirigeant1 = $m0SCI.dirigeant1.cadreIdentiteDirigeant;
var dirigeant2 = $m0SCI.dirigeant2.cadreIdentiteDirigeant;
var dirigeant3 = $m0SCI.dirigeant3.cadreIdentiteDirigeant;
var dirigeant4 = $m0SCI.dirigeant4.cadreIdentiteDirigeant;
var dirigeant5 = $m0SCI.dirigeant5.cadreIdentiteDirigeant;
var dirigeant6 = $m0SCI.dirigeant6.cadreIdentiteDirigeant;
var dirigeant7 = $m0SCI.dirigeant7.cadreIdentiteDirigeant;
var dirigeant8 = $m0SCI.dirigeant8.cadreIdentiteDirigeant;

var userDeclarant1;
if ($m0SCI.dirigeant1.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant1 = $m0SCI.dirigeant1.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + $m0SCI.dirigeant1.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if ($m0SCI.dirigeant1.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant1 = $m0SCI.dirigeant1.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ $m0SCI.dirigeant1.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} 

if(Value('id').of($m0SCI.dirigeant1.cadreIdentiteDirigeant.personnaliteJuridique).eq('personnePhysique')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585')) {

	if (not Value('id').of($m0SCI.dirigeant1.cadreIdentiteDirigeant.personneLieeQualite).eq('75')) {
		attachment('pjDNCDirigeant1', 'pjDNCDirigeant1',{ label: userDeclarant1, mandatory:"true"}) ;
	}
	if ($m0SCI.dirigeant1.cadreIdentiteDirigeant.dirigeantSignataire) {
		attachment('pjIDDirigeant1Signataire', 'pjIDDirigeant1Signataire', { label: userDeclarant1, mandatory:"true"});
	}
	if (not $m0SCI.dirigeant1.cadreIdentiteDirigeant.dirigeantSignataire) {
		attachment('pjIDDirigeant1NonSignataire', 'pjIDDirigeant1NonSignataire', { label: userDeclarant1, mandatory:"true"});
	}
}


// PJ Dirigeant personne physique 2

var userDeclarant2;
if ($m0SCI.dirigeant2.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant2 = $m0SCI.dirigeant2.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + $m0SCI.dirigeant2.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if ($m0SCI.dirigeant2.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant2 = $m0SCI.dirigeant2.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ $m0SCI.dirigeant2.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} 

if(Value('id').of($m0SCI.dirigeant2.cadreIdentiteDirigeant.personnaliteJuridique).eq('personnePhysique')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585')) {

	if (not Value('id').of($m0SCI.dirigeant2.cadreIdentiteDirigeant.personneLieeQualite).eq('75')) {
		attachment('pjDNCDirigeant2', 'pjDNCDirigeant2',{ label: userDeclarant2, mandatory:"true"}) ;
	}
	if ($m0SCI.dirigeant2.cadreIdentiteDirigeant.dirigeantSignataire) {
		attachment('pjIDDirigeant2Signataire', 'pjIDDirigeant2Signataire', { label: userDeclarant2, mandatory:"true"});
	}
	if (not $m0SCI.dirigeant2.cadreIdentiteDirigeant.dirigeantSignataire) {
		attachment('pjIDDirigeant2NonSignataire', 'pjIDDirigeant2NonSignataire', { label: userDeclarant2, mandatory:"true"});
	}
}


//PJ Dirigeant personne physique 3

var userDeclarant3;
if ($m0SCI.dirigeant3.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant3 = $m0SCI.dirigeant3.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + $m0SCI.dirigeant3.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if ($m0SCI.dirigeant3.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant3 = $m0SCI.dirigeant3.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ $m0SCI.dirigeant3.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of($m0SCI.dirigeant3.cadreIdentiteDirigeant.personnaliteJuridique).eq('personnePhysique')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and Value('id').of($m0SCI.dirigeant2.cadreIdentiteDirigeant.personneLieeEtablissement).eq('oui')) {

	if (not Value('id').of($m0SCI.dirigeant3.cadreIdentiteDirigeant.personneLieeQualite).eq('75')) {
		attachment('pjDNCDirigeant3', 'pjDNCDirigeant3',{ label: userDeclarant3, mandatory:"true"}) ;
	}
	if ($m0SCI.dirigeant3.cadreIdentiteDirigeant.dirigeantSignataire) {
		attachment('pjIDDirigeant3Signataire', 'pjIDDirigeant3Signataire', { label: userDeclarant3, mandatory:"true"});
	}
	if (not $m0SCI.dirigeant3.cadreIdentiteDirigeant.dirigeantSignataire) {
		attachment('pjIDDirigeant3NonSignataire', 'pjIDDirigeant3NonSignataire', { label: userDeclarant3, mandatory:"true"});
	}
}


//PJ Dirigeant personne physique 4

var userDeclarant4;
if ($m0SCI.dirigeant4.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant4 = $m0SCI.dirigeant4.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + $m0SCI.dirigeant4.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if ($m0SCI.dirigeant4.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant4 = $m0SCI.dirigeant4.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ $m0SCI.dirigeant4.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of($m0SCI.dirigeant4.cadreIdentiteDirigeant.personnaliteJuridique).eq('personnePhysique')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and Value('id').of($m0SCI.dirigeant3.cadreIdentiteDirigeant.personneLieeEtablissement).eq('oui')) {

	if (not Value('id').of($m0SCI.dirigeant4.cadreIdentiteDirigeant.personneLieeQualite).eq('75')) {
		attachment('pjDNCDirigeant4', 'pjDNCDirigeant4',{ label: userDeclarant4, mandatory:"true"}) ;
	}
	if ($m0SCI.dirigeant4.cadreIdentiteDirigeant.dirigeantSignataire) {
		attachment('pjIDDirigeant4Signataire', 'pjIDDirigeant4Signataire', { label: userDeclarant4, mandatory:"true"});
	}
	if (not $m0SCI.dirigeant4.cadreIdentiteDirigeant.dirigeantSignataire) {
		attachment('pjIDDirigeant4NonSignataire', 'pjIDDirigeant4NonSignataire', { label: userDeclarant4, mandatory:"true"});
	}
}

//PJ Dirigeant personne physique 5

var userDeclarant5;
if ($m0SCI.dirigeant5.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant5 = $m0SCI.dirigeant5.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + $m0SCI.dirigeant5.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if ($m0SCI.dirigeant5.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant5 = $m0SCI.dirigeant5.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ $m0SCI.dirigeant5.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of($m0SCI.dirigeant5.cadreIdentiteDirigeant.personnaliteJuridique).eq('personnePhysique')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and Value('id').of($m0SCI.dirigeant4.cadreIdentiteDirigeant.personneLieeEtablissement).eq('oui')) {

	if (not Value('id').of($m0SCI.dirigeant5.cadreIdentiteDirigeant.personneLieeQualite).eq('75')) {
		attachment('pjDNCDirigeant5', 'pjDNCDirigeant5',{ label: userDeclarant5, mandatory:"true"}) ;
	}
	if ($m0SCI.dirigeant5.cadreIdentiteDirigeant.dirigeantSignataire) {
		attachment('pjIDDirigeant5Signataire', 'pjIDDirigeant5Signataire', { label: userDeclarant5, mandatory:"true"});
	}
	if (not $m0SCI.dirigeant5.cadreIdentiteDirigeant.dirigeantSignataire) {
		attachment('pjIDDirigeant5NonSignataire', 'pjIDDirigeant5NonSignataire', { label: userDeclarant5, mandatory:"true"});
	}
}

//PJ Dirigeant personne physique 6

var userDeclarant6;
if ($m0SCI.dirigeant6.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant6 = $m0SCI.dirigeant6.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + $m0SCI.dirigeant6.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if ($m0SCI.dirigeant6.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant6 = $m0SCI.dirigeant6.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ $m0SCI.dirigeant6.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of($m0SCI.dirigeant6.cadreIdentiteDirigeant.personnaliteJuridique).eq('personnePhysique')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and Value('id').of($m0SCI.dirigeant5.cadreIdentiteDirigeant.personneLieeEtablissement).eq('oui')) {

	if (not Value('id').of($m0SCI.dirigeant6.cadreIdentiteDirigeant.personneLieeQualite).eq('75')) {
		attachment('pjDNCDirigeant6', 'pjDNCDirigeant6',{ label: userDeclarant6, mandatory:"true"}) ;
	}
	if ($m0SCI.dirigeant6.cadreIdentiteDirigeant.dirigeantSignataire) {
		attachment('pjIDDirigeant6Signataire', 'pjIDDirigeant6Signataire', { label: userDeclarant6, mandatory:"true"});
	}
	if (not $m0SCI.dirigeant6.cadreIdentiteDirigeant.dirigeantSignataire) {
		attachment('pjIDDirigeant6NonSignataire', 'pjIDDirigeant6NonSignataire', { label: userDeclarant6, mandatory:"true"});
	}
}

//PJ Dirigeant personne physique 7

var userDeclarant7;
if ($m0SCI.dirigeant7.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant7 = $m0SCI.dirigeant7.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + $m0SCI.dirigeant7.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if ($m0SCI.dirigeant7.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant7 = $m0SCI.dirigeant7.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ $m0SCI.dirigeant7.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of($m0SCI.dirigeant7.cadreIdentiteDirigeant.personnaliteJuridique).eq('personnePhysique')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and Value('id').of($m0SCI.dirigeant6.cadreIdentiteDirigeant.personneLieeEtablissement).eq('oui')) {

	if (not Value('id').of($m0SCI.dirigeant7.cadreIdentiteDirigeant.personneLieeQualite).eq('75')) {
		attachment('pjDNCDirigeant7', 'pjDNCDirigeant7',{ label: userDeclarant7, mandatory:"true"}) ;
	}
	if ($m0SCI.dirigeant7.cadreIdentiteDirigeant.dirigeantSignataire) {
		attachment('pjIDDirigeant7Signataire', 'pjIDDirigeant7Signataire', { label: userDeclarant7, mandatory:"true"});
	}
	if (not $m0SCI.dirigeant7.cadreIdentiteDirigeant.dirigeantSignataire) {
		attachment('pjIDDirigeant7NonSignataire', 'pjIDDirigeant7NonSignataire', { label: userDeclarant7, mandatory:"true"});
	}
}

//PJ Dirigeant personne physique 8

var userDeclarant8;
if ($m0SCI.dirigeant8.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant8 = $m0SCI.dirigeant8.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + $m0SCI.dirigeant8.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if ($m0SCI.dirigeant8.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant8 = $m0SCI.dirigeant8.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ $m0SCI.dirigeant8.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of($m0SCI.dirigeant8.cadreIdentiteDirigeant.personnaliteJuridique).eq('personnePhysique')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and Value('id').of($m0SCI.dirigeant7.cadreIdentiteDirigeant.personneLieeEtablissement).eq('oui')) {

	if (not Value('id').of($m0SCI.dirigeant8.cadreIdentiteDirigeant.personneLieeQualite).eq('75')) {
		attachment('pjDNCDirigeant8', 'pjDNCDirigeant8',{ label: userDeclarant8, mandatory:"true"}) ;
	}
	if ($m0SCI.dirigeant8.cadreIdentiteDirigeant.dirigeantSignataire) {
		attachment('pjIDDirigeant8Signataire', 'pjIDDirigeant8Signataire', { label: userDeclarant8, mandatory:"true"});
	}
	if (not $m0SCI.dirigeant8.cadreIdentiteDirigeant.dirigeantSignataire) {
		attachment('pjIDDirigeant8NonSignataire', 'pjIDDirigeant8NonSignataire', { label: userDeclarant8, mandatory:"true"});
	}
}

//PJ Dirigeant personne physique 9

var dirigeantAutre1 = $m0SCI.dirigeantAutre1.cadreIdentiteDirigeant;
var dirigeantAutre2 = $m0SCI.dirigeantAutre2.cadreIdentiteDirigeant;
var dirigeantAutre3 = $m0SCI.dirigeantAutre3.cadreIdentiteDirigeant;
var dirigeantAutre4 = $m0SCI.dirigeantAutre4.cadreIdentiteDirigeant;
var dirigeantAutre5 = $m0SCI.dirigeantAutre5.cadreIdentiteDirigeant;
var dirigeantAutre6 = $m0SCI.dirigeantAutre6.cadreIdentiteDirigeant;
var dirigeantAutre7 = $m0SCI.dirigeantAutre7.cadreIdentiteDirigeant;
var dirigeantAutre8 = $m0SCI.dirigeantAutre8.cadreIdentiteDirigeant;
var dirigeantAutre9 = $m0SCI.dirigeantAutre9.cadreIdentiteDirigeant;
var dirigeantAutre10 = $m0SCI.dirigeantAutre10.cadreIdentiteDirigeant;
var dirigeantAutre11 = $m0SCI.dirigeantAutre11.cadreIdentiteDirigeant;
var dirigeantAutre12 = $m0SCI.dirigeantAutre12.cadreIdentiteDirigeant;

var userDeclarant9;
if (dirigeantAutre1.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant9 = dirigeantAutre1.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeantAutre1.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeantAutre1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant9 = dirigeantAutre1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ dirigeantAutre1.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant9 = dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  '+ dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant9 = dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  '+ dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of(dirigeantAutre1.personnaliteJuridique).eq('personnePhysique')
	and not Value('id').of(dirigeantAutre1.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre1.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre1.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre1.personneLieeQualiteBis).eq('72'))
	or ((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre1.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre1.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre1.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre1.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre1.personneLieeQualiteBis).eq('66')
	or Value('id').of(dirigeantAutre1.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre1.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre1.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre1.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre1.personneLieeQualiteBis).eq('51'))	{

	if (not Value('id').of(dirigeantAutre1.personneLieeQualite).eq('75')) {
		attachment('pjDNCDirigeant9', 'pjDNCDirigeant9',{ label: userDeclarant9, mandatory:"true"}) ;
	}
	if (dirigeantAutre1.dirigeantSignataire) {
		attachment('pjIDDirigeant9Signataire', 'pjIDDirigeant9Signataire', { label: userDeclarant9, mandatory:"true"});
	}
	if (not dirigeantAutre1.dirigeantSignataire) {
		attachment('pjIDDirigeant9NonSignataire', 'pjIDDirigeant9NonSignataire', { label: userDeclarant9, mandatory:"true"});
	}
}

//PJ Dirigeant personne physique 10

var userDeclarant10;
if (dirigeantAutre2.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant10 = dirigeantAutre2.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeantAutre2.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeantAutre2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant10 = dirigeantAutre2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ dirigeantAutre2.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant10 = dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  '+ dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant10 = dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  '+ dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of(dirigeantAutre2.personnaliteJuridique).eq('personnePhysique')
	and not Value('id').of(dirigeantAutre2.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre2.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre2.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre2.personneLieeQualiteBis).eq('72'))
	or ((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre2.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre2.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre2.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre2.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre2.personneLieeQualiteBis).eq('66')
	or Value('id').of(dirigeantAutre2.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre2.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre2.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre2.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre2.personneLieeQualiteBis).eq('51'))	{

	if (not Value('id').of(dirigeantAutre2.personneLieeQualite).eq('75')) {
		attachment('pjDNCDirigeant10', 'pjDNCDirigeant10',{ label: userDeclarant10, mandatory:"true"}) ;
	}
	if (dirigeantAutre2.dirigeantSignataire) {
		attachment('pjIDDirigeant10Signataire', 'pjIDDirigeant10Signataire', { label: userDeclarant10, mandatory:"true"});
	}
	if (not dirigeantAutre2.dirigeantSignataire) {
		attachment('pjIDDirigeant10NonSignataire', 'pjIDDirigeant10NonSignataire', { label: userDeclarant10, mandatory:"true"});
	}
}

//PJ Dirigeant personne physique 11

var userDeclarant11;
if (dirigeantAutre3.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant11 = dirigeantAutre3.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeantAutre3.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeantAutre3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant11 = dirigeantAutre3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ dirigeantAutre3.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant11 = dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  '+ dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant11 = dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  '+ dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of(dirigeantAutre3.personnaliteJuridique).eq('personnePhysique')
	and not Value('id').of(dirigeantAutre3.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre3.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre3.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre3.personneLieeQualiteBis).eq('72'))
	or ((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre3.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre3.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre3.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre3.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre3.personneLieeQualiteBis).eq('66')
	or Value('id').of(dirigeantAutre3.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre3.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre3.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre3.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre3.personneLieeQualiteBis).eq('51'))	{

	if (not Value('id').of(dirigeantAutre3.personneLieeQualite).eq('75')) {
		attachment('pjDNCDirigeant11', 'pjDNCDirigeant11',{ label: userDeclarant11, mandatory:"true"}) ;
	}
	if (dirigeantAutre3.dirigeantSignataire) {
		attachment('pjIDDirigeant11Signataire', 'pjIDDirigeant11Signataire', { label: userDeclarant11, mandatory:"true"});
	}
	if (not dirigeantAutre3.dirigeantSignataire) {
		attachment('pjIDDirigeant11NonSignataire', 'pjIDDirigeant11NonSignataire', { label: userDeclarant11, mandatory:"true"});
	}
}

//PJ Dirigeant personne physique 12

var userDeclarant12;
if (dirigeantAutre4.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant12 = dirigeantAutre4.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeantAutre4.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeantAutre4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant12 = dirigeantAutre4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ dirigeantAutre4.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant12 = dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  '+ dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant12 = dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  '+ dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of(dirigeantAutre4.personnaliteJuridique).eq('personnePhysique')
	and not Value('id').of(dirigeantAutre4.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre4.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre4.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre4.personneLieeQualiteBis).eq('72'))
	or ((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre4.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre4.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre4.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre4.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre4.personneLieeQualiteBis).eq('66')
	or Value('id').of(dirigeantAutre4.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre4.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre4.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre4.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre4.personneLieeQualiteBis).eq('51'))	{

	if (not Value('id').of(dirigeantAutre4.personneLieeQualite).eq('75')) {
		attachment('pjDNCDirigeant12', 'pjDNCDirigeant12',{ label: userDeclarant12, mandatory:"true"}) ;
	}
	if (dirigeantAutre4.dirigeantSignataire) {
		attachment('pjIDDirigeant12Signataire', 'pjIDDirigeant12Signataire', { label: userDeclarant12, mandatory:"true"});
	}
	if (not dirigeantAutre4.dirigeantSignataire) {
		attachment('pjIDDirigeant12NonSignataire', 'pjIDDirigeant12NonSignataire', { label: userDeclarant12, mandatory:"true"});
	}
}

//PJ Dirigeant personne physique 13

var userDeclarant13;
if (dirigeantAutre5.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant13 = dirigeantAutre5.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeantAutre5.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeantAutre5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant13 = dirigeantAutre5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ dirigeantAutre5.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant13 = dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  '+ dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant13 = dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  '+ dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of(dirigeantAutre5.personnaliteJuridique).eq('personnePhysique')
	and not Value('id').of(dirigeantAutre5.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre5.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre5.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre5.personneLieeQualiteBis).eq('72'))
	or ((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre5.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre5.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre5.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre5.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre5.personneLieeQualiteBis).eq('66')
	or Value('id').of(dirigeantAutre5.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre5.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre5.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre5.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre5.personneLieeQualiteBis).eq('51'))	{

	if (not Value('id').of(dirigeantAutre5.personneLieeQualite).eq('75')) {
		attachment('pjDNCDirigeant13', 'pjDNCDirigeant13',{ label: userDeclarant13, mandatory:"true"}) ;
	}
	if (dirigeantAutre5.dirigeantSignataire) {
		attachment('pjIDDirigeant13Signataire', 'pjIDDirigeant13Signataire', { label: userDeclarant13, mandatory:"true"});
	}
	if (not dirigeantAutre5.dirigeantSignataire) {
		attachment('pjIDDirigeant13NonSignataire', 'pjIDDirigeant13NonSignataire', { label: userDeclarant13, mandatory:"true"});
	}
}

//PJ Dirigeant personne physique 14

var userDeclarant14;
if (dirigeantAutre6.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant14 = dirigeantAutre6.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeantAutre6.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeantAutre6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant14 = dirigeantAutre6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ dirigeantAutre6.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant14 = dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  '+ dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant14 = dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  '+ dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of(dirigeantAutre6.personnaliteJuridique).eq('personnePhysique')
	and not Value('id').of(dirigeantAutre6.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre6.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre6.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre6.personneLieeQualiteBis).eq('72'))
	or ((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre6.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre6.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre6.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre6.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre6.personneLieeQualiteBis).eq('66')
	or Value('id').of(dirigeantAutre6.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre6.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre6.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre6.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre6.personneLieeQualiteBis).eq('51'))	{

	if (not Value('id').of(dirigeantAutre6.personneLieeQualite).eq('75')) {
		attachment('pjDNCDirigeant14', 'pjDNCDirigeant14',{ label: userDeclarant14, mandatory:"true"}) ;
	}
	if (dirigeantAutre6.dirigeantSignataire) {
		attachment('pjIDDirigeant14Signataire', 'pjIDDirigeant14Signataire', { label: userDeclarant14, mandatory:"true"});
	}
	if (not dirigeantAutre6.dirigeantSignataire) {
		attachment('pjIDDirigeant14NonSignataire', 'pjIDDirigeant14NonSignataire', { label: userDeclarant14, mandatory:"true"});
	}
}

//PJ Dirigeant personne physique 15

var userDeclarant15;
if (dirigeantAutre7.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant15 = dirigeantAutre7.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeantAutre7.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeantAutre7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant15 = dirigeantAutre7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ dirigeantAutre7.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant15 = dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  '+ dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant15 = dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  '+ dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of(dirigeantAutre7.personnaliteJuridique).eq('personnePhysique')
	and not Value('id').of(dirigeantAutre7.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre7.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre7.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre7.personneLieeQualiteBis).eq('72'))
	or ((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre7.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre7.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre7.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre7.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre7.personneLieeQualiteBis).eq('66')
	or Value('id').of(dirigeantAutre7.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre7.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre7.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre7.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre7.personneLieeQualiteBis).eq('51'))	{

	if (not Value('id').of(dirigeantAutre7.personneLieeQualite).eq('75')) {
		attachment('pjDNCDirigeant15', 'pjDNCDirigeant15',{ label: userDeclarant15, mandatory:"true"}) ;
	}
	if (dirigeantAutre7.dirigeantSignataire) {
		attachment('pjIDDirigeant15Signataire', 'pjIDDirigeant15Signataire', { label: userDeclarant15, mandatory:"true"});
	}
	if (not dirigeantAutre7.dirigeantSignataire) {
		attachment('pjIDDirigeant15NonSignataire', 'pjIDDirigeant15NonSignataire', { label: userDeclarant15, mandatory:"true"});
	}
}

//PJ Dirigeant personne physique 16

var userDeclarant16;
if (dirigeantAutre8.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant16 = dirigeantAutre8.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeantAutre8.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeantAutre8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant16 = dirigeantAutre8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ dirigeantAutre8.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant16 = dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  '+ dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant16 = dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  '+ dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of(dirigeantAutre8.personnaliteJuridique).eq('personnePhysique')
	and not Value('id').of(dirigeantAutre8.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre8.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre8.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre8.personneLieeQualiteBis).eq('72'))
	or ((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre8.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre8.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre8.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre8.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre8.personneLieeQualiteBis).eq('66')
	or Value('id').of(dirigeantAutre8.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre8.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre8.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre8.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre8.personneLieeQualiteBis).eq('51'))	{

	if (not Value('id').of(dirigeantAutre8.personneLieeQualite).eq('75')) {
		attachment('pjDNCDirigeant16', 'pjDNCDirigeant16',{ label: userDeclarant16, mandatory:"true"}) ;
	}
	if (dirigeantAutre8.dirigeantSignataire) {
		attachment('pjIDDirigeant16Signataire', 'pjIDDirigeant16Signataire', { label: userDeclarant16, mandatory:"true"});
	}
	if (not dirigeantAutre8.dirigeantSignataire) {
		attachment('pjIDDirigeant16NonSignataire', 'pjIDDirigeant16NonSignataire', { label: userDeclarant16, mandatory:"true"});
	}
}

//PJ Dirigeant personne physique 17

var userDeclarant17;
if (dirigeantAutre9.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant17 = dirigeantAutre9.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeantAutre9.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeantAutre9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant17 = dirigeantAutre9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ dirigeantAutre9.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant17 = dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  '+ dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant17 = dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  '+ dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of(dirigeantAutre9.personnaliteJuridique).eq('personnePhysique')
	and not Value('id').of(dirigeantAutre9.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre9.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre9.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre9.personneLieeQualiteBis).eq('72'))
	or ((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre9.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre9.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre9.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre9.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre9.personneLieeQualiteBis).eq('66')
	or Value('id').of(dirigeantAutre9.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre9.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre9.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre9.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre9.personneLieeQualiteBis).eq('51'))	{

	if (not Value('id').of(dirigeantAutre9.personneLieeQualite).eq('75')) {
		attachment('pjDNCDirigeant17', 'pjDNCDirigeant17',{ label: userDeclarant17, mandatory:"true"}) ;
	}
	if (dirigeantAutre9.dirigeantSignataire) {
		attachment('pjIDDirigeant17Signataire', 'pjIDDirigeant17Signataire', { label: userDeclarant17, mandatory:"true"});
	}
	if (not dirigeantAutre9.dirigeantSignataire) {
		attachment('pjIDDirigeant17NonSignataire', 'pjIDDirigeant17NonSignataire', { label: userDeclarant17, mandatory:"true"});
	}
}

//PJ Dirigeant personne physique 18

var userDeclarant18;
if (dirigeantAutre10.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant18 = dirigeantAutre10.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeantAutre10.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeantAutre10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant18 = dirigeantAutre10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ dirigeantAutre10.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant18 = dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  '+ dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant18 = dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  '+ dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of(dirigeantAutre10.personnaliteJuridique).eq('personnePhysique')
	and not Value('id').of(dirigeantAutre10.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre10.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre10.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre10.personneLieeQualiteBis).eq('72'))
	or ((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre10.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre10.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre10.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre10.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre10.personneLieeQualiteBis).eq('66')
	or Value('id').of(dirigeantAutre10.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre10.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre10.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre10.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre10.personneLieeQualiteBis).eq('51'))	{

	if (not Value('id').of(dirigeantAutre10.personneLieeQualite).eq('75')) {
		attachment('pjDNCDirigeant18', 'pjDNCDirigeant18',{ label: userDeclarant18, mandatory:"true"}) ;
	}
	if (dirigeantAutre10.dirigeantSignataire) {
		attachment('pjIDDirigeant18Signataire', 'pjIDDirigeant18Signataire', { label: userDeclarant18, mandatory:"true"});
	}
	if (not dirigeantAutre10.dirigeantSignataire) {
		attachment('pjIDDirigeant18NonSignataire', 'pjIDDirigeant18NonSignataire', { label: userDeclarant18, mandatory:"true"});
	}
}

//PJ Dirigeant personne physique 19

var userDeclarant19;
if (dirigeantAutre11.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant19 = dirigeantAutre11.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeantAutre11.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeantAutre11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant19 = dirigeantAutre11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ dirigeantAutre11.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant19 = dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  '+ dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant19 = dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  '+ dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of(dirigeantAutre11.personnaliteJuridique).eq('personnePhysique')
	and not Value('id').of(dirigeantAutre11.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre11.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre11.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre11.personneLieeQualiteBis).eq('72'))
	or ((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre11.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre11.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre11.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre11.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre11.personneLieeQualiteBis).eq('66')
	or Value('id').of(dirigeantAutre11.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre11.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre11.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre11.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre11.personneLieeQualiteBis).eq('51'))	{

	if (not Value('id').of(dirigeantAutre11.personneLieeQualite).eq('75')) {
		attachment('pjDNCDirigeant19', 'pjDNCDirigeant19',{ label: userDeclarant19, mandatory:"true"}) ;
	}
	if (dirigeantAutre11.dirigeantSignataire) {
		attachment('pjIDDirigeant19Signataire', 'pjIDDirigeant19Signataire', { label: userDeclarant19, mandatory:"true"});
	}
	if (not dirigeantAutre11.dirigeantSignataire) {
		attachment('pjIDDirigeant19NonSignataire', 'pjIDDirigeant19NonSignataire', { label: userDeclarant19, mandatory:"true"});
	}
}

//PJ Dirigeant personne physique 20

var userDeclarant20;
if (dirigeantAutre12.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant20 = dirigeantAutre12.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeantAutre12.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeantAutre12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant20 = dirigeantAutre12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  '+ dirigeantAutre12.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant20 = dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  '+ dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else {
    var userDeclarant20 = dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  '+ dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if((Value('id').of(dirigeantAutre12.personnaliteJuridique).eq('personnePhysique')
	and not Value('id').of(dirigeantAutre12.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre12.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre12.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre12.personneLieeQualiteBis).eq('72'))
	or ((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre12.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre12.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre12.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre12.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre12.personneLieeQualiteBis).eq('66')
	or Value('id').of(dirigeantAutre12.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre12.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre12.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre12.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre12.personneLieeQualiteBis).eq('51'))	{

	if (not Value('id').of(dirigeantAutre12.personneLieeQualite).eq('75')) {
		attachment('pjDNCDirigeant20', 'pjDNCDirigeant20',{ label: userDeclarant20, mandatory:"true"}) ;
	}
	if (dirigeantAutre12.dirigeantSignataire) {
		attachment('pjIDDirigeant20Signataire', 'pjIDDirigeant20Signataire', { label: userDeclarant20, mandatory:"true"});
	}
	if (not dirigeantAutre12.dirigeantSignataire) {
		attachment('pjIDDirigeant20NonSignataire', 'pjIDDirigeant20NonSignataire', { label: userDeclarant20, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 1 

var userMorale1=$m0SCI.dirigeant1.cadreIdentiteDirigeant.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of($m0SCI.dirigeant1.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale'))	{
    attachment('pjExtraitImmatriculationRCS1', 'pjExtraitImmatriculationRCS1',{ label: userMorale1, mandatory:"true"}) ;
	if ($m0SCI.dirigeant1.cadreIdentiteDirigeant.dirigeantSignataire) {
	    attachment('pjPersonneMoraleSignataire1', 'pjPersonneMoraleSignataire1', { label: userMorale1, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 2 

var userMorale2=$m0SCI.dirigeant2.cadreIdentiteDirigeant.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of($m0SCI.dirigeant2.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale'))	{
    attachment('pjExtraitImmatriculationRCS2', 'pjExtraitImmatriculationRCS2',{ label: userMorale2, mandatory:"true"}) ;
	if ($m0SCI.dirigeant2.cadreIdentiteDirigeant.dirigeantSignataire) {
	    attachment('pjPersonneMoraleSignataire2', 'pjPersonneMoraleSignataire2', { label: userMorale2, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 3 

var userMorale3=$m0SCI.dirigeant3.cadreIdentiteDirigeant.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of($m0SCI.dirigeant3.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale'))	{
    attachment('pjExtraitImmatriculationRCS3', 'pjExtraitImmatriculationRCS3',{ label: userMorale3, mandatory:"true"}) ;
	if ($m0SCI.dirigeant3.cadreIdentiteDirigeant.dirigeantSignataire) {
	    attachment('pjPersonneMoraleSignataire3', 'pjPersonneMoraleSignataire3', { label: userMorale3, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 4 

var userMorale4=$m0SCI.dirigeant4.cadreIdentiteDirigeant.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of($m0SCI.dirigeant4.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale'))	{
    attachment('pjExtraitImmatriculationRCS4', 'pjExtraitImmatriculationRCS4',{ label: userMorale4, mandatory:"true"}) ;
	if ($m0SCI.dirigeant4.cadreIdentiteDirigeant.dirigeantSignataire) {
	    attachment('pjPersonneMoraleSignataire4', 'pjPersonneMoraleSignataire4', { label: userMorale4, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 5 

var userMorale5=$m0SCI.dirigeant5.cadreIdentiteDirigeant.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of($m0SCI.dirigeant5.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale'))	{
    attachment('pjExtraitImmatriculationRCS5', 'pjExtraitImmatriculationRCS5',{ label: userMorale5, mandatory:"true"}) ;
	if ($m0SCI.dirigeant5.cadreIdentiteDirigeant.dirigeantSignataire) {
	    attachment('pjPersonneMoraleSignataire5', 'pjPersonneMoraleSignataire5', { label: userMorale5, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 6 

var userMorale6=$m0SCI.dirigeant6.cadreIdentiteDirigeant.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of($m0SCI.dirigeant6.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale'))	{
    attachment('pjExtraitImmatriculationRCS6', 'pjExtraitImmatriculationRCS6',{ label: userMorale6, mandatory:"true"}) ;
	if ($m0SCI.dirigeant6.cadreIdentiteDirigeant.dirigeantSignataire) {
	    attachment('pjPersonneMoraleSignataire6', 'pjPersonneMoraleSignataire6', { label: userMorale6, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 7 

var userMorale7=$m0SCI.dirigeant7.cadreIdentiteDirigeant.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of($m0SCI.dirigeant7.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale'))	{
    attachment('pjExtraitImmatriculationRCS7', 'pjExtraitImmatriculationRCS7',{ label: userMorale7, mandatory:"true"}) ;
	if ($m0SCI.dirigeant7.cadreIdentiteDirigeant.dirigeantSignataire) {
	    attachment('pjPersonneMoraleSignataire7', 'pjPersonneMoraleSignataire7', { label: userMorale7, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 8 

var userMorale8=$m0SCI.dirigeant8.cadreIdentiteDirigeant.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of($m0SCI.dirigeant8.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale'))	{
    attachment('pjExtraitImmatriculationRCS8', 'pjExtraitImmatriculationRCS8',{ label: userMorale8, mandatory:"true"}) ;
	if ($m0SCI.dirigeant8.cadreIdentiteDirigeant.dirigeantSignataire) {
	    attachment('pjPersonneMoraleSignataire8', 'pjPersonneMoraleSignataire8', { label: userMorale8, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 9 

var userMorale9=$m0SCI.dirigeantAutre1.cadreIdentiteDirigeant.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of($m0SCI.dirigeantAutre1.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale')
	and not Value('id').of(dirigeantAutre1.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre1.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre1.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre1.personneLieeQualiteBis).eq('72'))	{
    attachment('pjExtraitImmatriculationRCS9', 'pjExtraitImmatriculationRCS9',{ label: userMorale9, mandatory:"true"}) ;
	if ($m0SCI.dirigeantAutre1.cadreIdentiteDirigeant.dirigeantSignataire) {
	    attachment('pjPersonneMoraleSignataire9', 'pjPersonneMoraleSignataire9', { label: userMorale9, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 10

var userMorale10=$m0SCI.dirigeantAutre2.cadreIdentiteDirigeant.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of($m0SCI.dirigeantAutre2.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale')
	and not Value('id').of(dirigeantAutre2.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre2.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre2.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre2.personneLieeQualiteBis).eq('72'))	{
    attachment('pjExtraitImmatriculationRCS10', 'pjExtraitImmatriculationRCS10',{ label: userMorale10, mandatory:"true"}) ;
	if ($m0SCI.dirigeantAutre2.cadreIdentiteDirigeant.dirigeantSignataire) {
	    attachment('pjPersonneMoraleSignataire10', 'pjPersonneMoraleSignataire10', { label: userMorale10, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 11

var userMorale11=$m0SCI.dirigeantAutre3.cadreIdentiteDirigeant.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of($m0SCI.dirigeantAutre3.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale')
	and not Value('id').of(dirigeantAutre3.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre3.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre3.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre3.personneLieeQualiteBis).eq('72'))	{
    attachment('pjExtraitImmatriculationRCS11', 'pjExtraitImmatriculationRCS11',{ label: userMorale11, mandatory:"true"}) ;
	if ($m0SCI.dirigeantAutre3.cadreIdentiteDirigeant.dirigeantSignataire) {
	    attachment('pjPersonneMoraleSignataire11', 'pjPersonneMoraleSignataire11', { label: userMorale11, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 12 

var userMorale12=$m0SCI.dirigeantAutre4.cadreIdentiteDirigeant.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of($m0SCI.dirigeantAutre4.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale')
	and not Value('id').of(dirigeantAutre4.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre4.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre4.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre4.personneLieeQualiteBis).eq('72'))	{
    attachment('pjExtraitImmatriculationRCS12', 'pjExtraitImmatriculationRCS12',{ label: userMorale12, mandatory:"true"}) ;
	if ($m0SCI.dirigeantAutre4.cadreIdentiteDirigeant.dirigeantSignataire) {
	    attachment('pjPersonneMoraleSignataire12', 'pjPersonneMoraleSignataire12', { label: userMorale12, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 13

var userMorale13=$m0SCI.dirigeantAutre5.cadreIdentiteDirigeant.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of($m0SCI.dirigeantAutre5.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale')
	and not Value('id').of(dirigeantAutre5.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre5.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre5.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre5.personneLieeQualiteBis).eq('72'))	{
    attachment('pjExtraitImmatriculationRCS13', 'pjExtraitImmatriculationRCS13',{ label: userMorale13, mandatory:"true"}) ;
	if ($m0SCI.dirigeantAutre5.cadreIdentiteDirigeant.dirigeantSignataire) {
	    attachment('pjPersonneMoraleSignataire13', 'pjPersonneMoraleSignataire13', { label: userMorale13, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 14

var userMorale14=$m0SCI.dirigeantAutre6.cadreIdentiteDirigeant.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of($m0SCI.dirigeantAutre6.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale')
	and not Value('id').of(dirigeantAutre6.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre6.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre6.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre6.personneLieeQualiteBis).eq('72'))	{
    attachment('pjExtraitImmatriculationRCS14', 'pjExtraitImmatriculationRCS14',{ label: userMorale14, mandatory:"true"}) ;
	if ($m0SCI.dirigeantAutre6.cadreIdentiteDirigeant.dirigeantSignataire) {
	    attachment('pjPersonneMoraleSignataire14', 'pjPersonneMoraleSignataire14', { label: userMorale14, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 15

var userMorale15=$m0SCI.dirigeantAutre7.cadreIdentiteDirigeant.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of($m0SCI.dirigeantAutre7.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale')
	and not Value('id').of(dirigeantAutre7.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre7.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre7.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre7.personneLieeQualiteBis).eq('72'))	{
    attachment('pjExtraitImmatriculationRCS15', 'pjExtraitImmatriculationRCS15',{ label: userMorale15, mandatory:"true"}) ;
	if ($m0SCI.dirigeantAutre7.cadreIdentiteDirigeant.dirigeantSignataire) {
	    attachment('pjPersonneMoraleSignataire15', 'pjPersonneMoraleSignataire15', { label: userMorale15, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 16

var userMorale16=$m0SCI.dirigeantAutre8.cadreIdentiteDirigeant.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of($m0SCI.dirigeantAutre8.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale')
	and not Value('id').of(dirigeantAutre8.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre8.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre8.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre8.personneLieeQualiteBis).eq('72'))	{
    attachment('pjExtraitImmatriculationRCS16', 'pjExtraitImmatriculationRCS16',{ label: userMorale16, mandatory:"true"}) ;
	if ($m0SCI.dirigeantAutre8.cadreIdentiteDirigeant.dirigeantSignataire) {
	    attachment('pjPersonneMoraleSignataire16', 'pjPersonneMoraleSignataire16', { label: userMorale16, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 17

var userMorale17=$m0SCI.dirigeantAutre9.cadreIdentiteDirigeant.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of($m0SCI.dirigeantAutre9.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale')
	and not Value('id').of(dirigeantAutre9.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre9.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre9.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre9.personneLieeQualiteBis).eq('72'))	{
    attachment('pjExtraitImmatriculationRCS17', 'pjExtraitImmatriculationRCS17',{ label: userMorale17, mandatory:"true"}) ;
	if ($m0SCI.dirigeantAutre9.cadreIdentiteDirigeant.dirigeantSignataire) {
	    attachment('pjPersonneMoraleSignataire17', 'pjPersonneMoraleSignataire17', { label: userMorale17, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 18

var userMorale18=$m0SCI.dirigeantAutre10.cadreIdentiteDirigeant.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of($m0SCI.dirigeantAutre10.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale')
	and not Value('id').of(dirigeantAutre10.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre10.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre10.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre10.personneLieeQualiteBis).eq('72'))	{
    attachment('pjExtraitImmatriculationRCS18', 'pjExtraitImmatriculationRCS18',{ label: userMorale18, mandatory:"true"}) ;
	if ($m0SCI.dirigeantAutre10.cadreIdentiteDirigeant.dirigeantSignataire) {
	    attachment('pjPersonneMoraleSignataire18', 'pjPersonneMoraleSignataire18', { label: userMorale18, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 19 

var userMorale19=$m0SCI.dirigeantAutre11.cadreIdentiteDirigeant.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of($m0SCI.dirigeantAutre11.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale')
	and not Value('id').of(dirigeantAutre11.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre11.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre11.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre11.personneLieeQualiteBis).eq('72'))	{
    attachment('pjExtraitImmatriculationRCS19', 'pjExtraitImmatriculationRCS19',{ label: userMorale19, mandatory:"true"}) ;
	if ($m0SCI.dirigeantAutre11.cadreIdentiteDirigeant.dirigeantSignataire) {
	    attachment('pjPersonneMoraleSignataire19', 'pjPersonneMoraleSignataire19', { label: userMorale19, mandatory:"true"});
	}
}

// PJ Dirigeant Personne Morale 20 

var userMorale20=$m0SCI.dirigeantAutre12.cadreIdentiteDirigeant.civilitePersonneMorale.personneLieePMDenomination ;

if(Value('id').of($m0SCI.dirigeantAutre12.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale')
	and not Value('id').of(dirigeantAutre12.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre12.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre12.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre12.personneLieeQualiteBis).eq('72'))	{
    attachment('pjExtraitImmatriculationRCS20', 'pjExtraitImmatriculationRCS20',{ label: userMorale20, mandatory:"true"}) ;
	if ($m0SCI.dirigeantAutre12.cadreIdentiteDirigeant.dirigeantSignataire) {
	    attachment('pjPersonneMoraleSignataire20', 'pjPersonneMoraleSignataire20', { label: userMorale20, mandatory:"true"});
	}
}

// Nomination dirigeants

if((not $m0SCI.dirigeant1.cadreIdentiteDirigeant.statutPresenceDirigeant and $m0SCI.dirigeant1.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeant1.cadreIdentiteDirigeant.personneLieeQualite).eq('75')) 
	or (not $m0SCI.dirigeant2.cadreIdentiteDirigeant.statutPresenceDirigeant and $m0SCI.dirigeant2.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeant2.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not $m0SCI.dirigeant3.cadreIdentiteDirigeant.statutPresenceDirigeant and $m0SCI.dirigeant3.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeant3.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not $m0SCI.dirigeant4.cadreIdentiteDirigeant.statutPresenceDirigeant and $m0SCI.dirigeant4.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeant4.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not $m0SCI.dirigeant5.cadreIdentiteDirigeant.statutPresenceDirigeant and $m0SCI.dirigeant5.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeant5.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not $m0SCI.dirigeant6.cadreIdentiteDirigeant.statutPresenceDirigeant and $m0SCI.dirigeant6.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeant6.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not $m0SCI.dirigeant7.cadreIdentiteDirigeant.statutPresenceDirigeant and $m0SCI.dirigeant7.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeant7.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not $m0SCI.dirigeant8.cadreIdentiteDirigeant.statutPresenceDirigeant and $m0SCI.dirigeant8.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeant8.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not dirigeantAutre1.statutPresenceDirigeant and $m0SCI.dirigeantAutre1.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeantAutre1.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not dirigeantAutre2.statutPresenceDirigeant and $m0SCI.dirigeantAutre2.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeantAutre2.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not dirigeantAutre3.statutPresenceDirigeant and $m0SCI.dirigeantAutre3.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeantAutre3.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not dirigeantAutre4.statutPresenceDirigeant and $m0SCI.dirigeantAutre4.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeantAutre4.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not dirigeantAutre5.statutPresenceDirigeant and $m0SCI.dirigeantAutre5.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeantAutre5.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not dirigeantAutre6.statutPresenceDirigeant and $m0SCI.dirigeantAutre6.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeantAutre6.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not dirigeantAutre7.statutPresenceDirigeant and $m0SCI.dirigeantAutre7.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeantAutre7.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not dirigeantAutre8.statutPresenceDirigeant and $m0SCI.dirigeantAutre8.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeantAutre8.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not dirigeantAutre9.statutPresenceDirigeant and $m0SCI.dirigeantAutre9.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeantAutre9.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not dirigeantAutre10.statutPresenceDirigeant and $m0SCI.dirigeantAutre10.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeantAutre10.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not dirigeantAutre11.statutPresenceDirigeant and $m0SCI.dirigeantAutre11.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeantAutre11.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not dirigeantAutre12.statutPresenceDirigeant and $m0SCI.dirigeantAutre12.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeantAutre12.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))) {
	attachment('pjNominationDirigeant', 'pjNominationDirigeant', { mandatory:"true"});
}

// Pj CAC Titulaire 

if(Value('id').of($m0SCI.dirigeantAutre1.cadreIdentiteDirigeant.personneLieeQualite).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre2.cadreIdentiteDirigeant.personneLieeQualite).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre3.cadreIdentiteDirigeant.personneLieeQualite).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre4.cadreIdentiteDirigeant.personneLieeQualite).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre5.cadreIdentiteDirigeant.personneLieeQualite).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre6.cadreIdentiteDirigeant.personneLieeQualite).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre7.cadreIdentiteDirigeant.personneLieeQualite).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre8.cadreIdentiteDirigeant.personneLieeQualite).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre9.cadreIdentiteDirigeant.personneLieeQualite).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre10.cadreIdentiteDirigeant.personneLieeQualite).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre11.cadreIdentiteDirigeant.personneLieeQualite).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre12.cadreIdentiteDirigeant.personneLieeQualite).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre1.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre2.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre3.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre4.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre5.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre6.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre7.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre8.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre9.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre10.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre11.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre12.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('71')) {
	attachment('pjLettreAcceptationCACTitulaire', 'pjLettreAcceptationCACTitulaire', { mandatory:"true"});
	attachment('pjJustificatifInscriptionCACTitulaire', 'pjJustificatifInscriptionCACTitulaire', { mandatory:"false"});
}	

// Pj CAC Suppléant

if(Value('id').of($m0SCI.dirigeantAutre1.cadreIdentiteDirigeant.personneLieeQualite).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre2.cadreIdentiteDirigeant.personneLieeQualite).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre3.cadreIdentiteDirigeant.personneLieeQualite).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre4.cadreIdentiteDirigeant.personneLieeQualite).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre5.cadreIdentiteDirigeant.personneLieeQualite).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre6.cadreIdentiteDirigeant.personneLieeQualite).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre7.cadreIdentiteDirigeant.personneLieeQualite).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre8.cadreIdentiteDirigeant.personneLieeQualite).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre9.cadreIdentiteDirigeant.personneLieeQualite).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre10.cadreIdentiteDirigeant.personneLieeQualite).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre11.cadreIdentiteDirigeant.personneLieeQualite).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre12.cadreIdentiteDirigeant.personneLieeQualite).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre1.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre2.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre3.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre4.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre5.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre6.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre7.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre8.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre9.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre10.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre11.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre12.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('72')) {
	attachment('pjLettreAcceptationCACSuppleant', 'pjLettreAcceptationCACSuppleant', { mandatory:"true"});
    attachment('pjJustificatifInscriptionCACSuppleant', 'pjJustificatifInscriptionCACSuppleant', { mandatory:"false"});
}

// PJ Mandataire

var userMandataire=$m0SCI.cadre11SignatureGroup.cadre11Signature.adresseMandataire.nomPrenomDenominationMandataire;

if (not dirigeant1.dirigeantSignataire and not dirigeant2.dirigeantSignataire and not dirigeant3.dirigeantSignataire and not dirigeant4.dirigeantSignataire and not dirigeant5.dirigeantSignataire and not dirigeant6.dirigeantSignataire and not dirigeant7.dirigeantSignataire and not dirigeant8.dirigeantSignataire and not dirigeantAutre1.dirigeantSignataire and not dirigeantAutre2.dirigeantSignataire and not dirigeantAutre3.dirigeantSignataire and not dirigeantAutre4.dirigeantSignataire and not dirigeantAutre5.dirigeantSignataire and not dirigeantAutre6.dirigeantSignataire and not dirigeantAutre7.dirigeantSignataire and not dirigeantAutre8.dirigeantSignataire and not dirigeantAutre9.dirigeantSignataire and not dirigeantAutre10.dirigeantSignataire and not dirigeantAutre11.dirigeantSignataire and not dirigeantAutre12.dirigeantSignataire) {
    attachment('pjPouvoir', 'pjPouvoir', { mandatory:"true"});
	attachment('pjIDMandataireSignataire', 'pjIDMandataireSignataire', {label: userMandataire, mandatory:"true"});
}

//PJ Société

attachment('pjStatuts', 'pjStatuts', { mandatory:"true"});
attachment('pjAnnonceLegaleConstitution', 'pjAnnonceLegaleConstitution', { mandatory:"true"});
attachment('pjIDActiviteReglementee', 'pjIDActiviteReglementee', { mandatory:"false"});

// PJ Etablissement

var pj=$m0SCI.cadreEntrepriseSiegeGroup.cadreEntrepriseSiege.activiteLieu ;
if(Value('id').of(pj).contains('entrepriseAdresseEntrepriseDomicile')) {
    attachment('pjDomicile', 'pjDomicile', { mandatory:"true"});
}

var pj=$m0SCI.cadreEntrepriseSiegeGroup.cadreEntrepriseSiege.activiteLieu ;
if(Value('id').of(pj).contains('etablissementDomiciliationOui')) {
    attachment('pjContratDomiciliation', 'pjContratDomiciliation', { mandatory:"true"});
}

if (Value('id').of($m0SCI.cadreEntrepriseSiegeGroup.cadreEntrepriseSiege.activiteLieu).contains('etablissementDomiciliationNon')) {
    attachment('pjSocieteCreation', 'pjSocieteCreation', { mandatory:"true"});
}

if($m0SCI.cadreEntrepriseSiegeGroup.cadreEntrepriseSiege.etablissementPrincipalDifferentSiege) {
    attachment('pjEtablissementCreation', 'pjEtablissementCreation', { mandatory:"true"});
}

// PJ MBE	
var signataire = $m0SCI.cadre11SignatureGroup.cadre11Signature ;

attachment('formulaireBE1', 'formulaireBE1', {mandatory:"true"});

if (not Value('id').of(signataire.nombreMBE).eq('un')) {
attachment('formulaireBE2', 'formulaireBE2', {mandatory:"true"});
}

if (not Value('id').of(signataire.nombreMBE).eq('un') and not Value('id').of(signataire.nombreMBE).eq('deux')) {
attachment('formulaireBE3', 'formulaireBE3', {mandatory:"true"});
}

if (Value('id').of(signataire.nombreMBE).eq('quatre') or Value('id').of(signataire.nombreMBE).eq('cinq')) {
attachment('formulaireBE4', 'formulaireBE4', {mandatory:"true"});
}

if (Value('id').of(signataire.nombreMBE).eq('cinq')) {
attachment('formulaireBE5', 'formulaireBE5', {mandatory:"true"});
}
