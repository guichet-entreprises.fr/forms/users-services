//Fichier de mapping des champs Regent pour :
//Version : V2008.11 
//Evenements : 03M,  
//Fichier généré le : 2018-11-02_114826 

function sanitize(s){return s.replaceAll("’","'").replaceAll("–","-").replaceAll("­","");}

function pad(s) { return (s < 10) ? '0' + s : s; }
var societe = $m0SCI.cadre1SocieteGroup.cadre1Identite;
var siege = $m0SCI.cadreEntrepriseSiegeGroup.cadreEntrepriseSiege; 

var regentVersion = "V2008.11";  // (V2008.11, V2016.02) 
var eventRegent = siege.formaliteCreationAvecActivite ? (siege.etablissementPrincipalDifferentSiege ? "03M" : "01M") : "02M"; //[] 
var authorityType = "CFE"; // (CFE, TDR)
var authorityId = _INPUT_.dataGroup.codeEDI; // (code EDI)

//Ajout du type de la deuxième autorité
var authorityType2 = "TDR"; // (CFE, TDR) 
//Ajout du code de la deuxième autorité
var authorityId2 = _INPUT_.dataGroup.codeEDI2; // (code EDI) 

var regEx = new RegExp('[0-9]{5}'); 
var etablissement = $m0SCI.cadreEtablissementActiviteGroup.cadreEtablissementActivite ;
var origine = $m0SCI.cadre4OrigineFondsGroup.cadre4OrigineFonds ;
var dirigeant1 = $m0SCI.dirigeant1.cadreIdentiteDirigeant;
var dirigeant2 = $m0SCI.dirigeant2.cadreIdentiteDirigeant;
var dirigeant3 = $m0SCI.dirigeant3.cadreIdentiteDirigeant;
var dirigeant4 = $m0SCI.dirigeant4.cadreIdentiteDirigeant;
var dirigeant5 = $m0SCI.dirigeant5.cadreIdentiteDirigeant;
var dirigeant6 = $m0SCI.dirigeant6.cadreIdentiteDirigeant;
var dirigeant7 = $m0SCI.dirigeant7.cadreIdentiteDirigeant;
var dirigeant8 = $m0SCI.dirigeant8.cadreIdentiteDirigeant;
var dirigeantAutre1 = $m0SCI.dirigeantAutre1.cadreIdentiteDirigeant;
var dirigeantAutre2 = $m0SCI.dirigeantAutre2.cadreIdentiteDirigeant;
var dirigeantAutre3 = $m0SCI.dirigeantAutre3.cadreIdentiteDirigeant;
var dirigeantAutre4 = $m0SCI.dirigeantAutre4.cadreIdentiteDirigeant;
var dirigeantAutre5 = $m0SCI.dirigeantAutre5.cadreIdentiteDirigeant;
var dirigeantAutre6 = $m0SCI.dirigeantAutre6.cadreIdentiteDirigeant;
var dirigeantAutre7 = $m0SCI.dirigeantAutre7.cadreIdentiteDirigeant;
var dirigeantAutre8 = $m0SCI.dirigeantAutre8.cadreIdentiteDirigeant;
var dirigeantAutre9 = $m0SCI.dirigeantAutre9.cadreIdentiteDirigeant;
var dirigeantAutre10 = $m0SCI.dirigeantAutre10.cadreIdentiteDirigeant;
var dirigeantAutre11 = $m0SCI.dirigeantAutre11.cadreIdentiteDirigeant;
var dirigeantAutre12 = $m0SCI.dirigeantAutre12.cadreIdentiteDirigeant;
var fiscal = $m0SCI.cadre20optFiscGroup.cadre20optFisc ;
var correspondance = $m0SCI.cadre10RensCompGroup.cadre10RensComp ;
var signataire = $m0SCI.cadre11SignatureGroup.cadre11Signature ;
var fusion1 = $m0SCI.cadre1SocieteGroup.cadre1Identite.fusionGroup1;
var fusion2 = $m0SCI.cadre1SocieteGroup.cadre1Identite.fusionGroup2;
var fusion3 = $m0SCI.cadre1SocieteGroup.cadre1Identite.fusionGroup3;
var socialDirigeant1 = dirigeant1.cadre7DeclarationSociale;
var socialDirigeant2 = dirigeant2.cadre7DeclarationSociale;
var socialDirigeant3 = dirigeant3.cadre7DeclarationSociale;
var socialDirigeant4 = dirigeant4.cadre7DeclarationSociale;
var socialDirigeant5 = dirigeant5.cadre7DeclarationSociale;
var socialDirigeant6 = dirigeant6.cadre7DeclarationSociale;
var socialDirigeant7 = dirigeant7.cadre7DeclarationSociale;
var socialDirigeant8 = dirigeant8.cadre7DeclarationSociale;
var socialDirigeant9 = dirigeantAutre1.cadre7DeclarationSociale;
var socialDirigeant10 = dirigeantAutre2.cadre7DeclarationSociale;
var socialDirigeant11 = dirigeantAutre3.cadre7DeclarationSociale;
var socialDirigeant12 = dirigeantAutre4.cadre7DeclarationSociale;
var socialDirigeant13 = dirigeantAutre5.cadre7DeclarationSociale;
var socialDirigeant14 = dirigeantAutre6.cadre7DeclarationSociale;
var socialDirigeant15 = dirigeantAutre7.cadre7DeclarationSociale;
var socialDirigeant16 = dirigeantAutre8.cadre7DeclarationSociale;
var socialDirigeant17 = dirigeantAutre9.cadre7DeclarationSociale;
var socialDirigeant18 = dirigeantAutre10.cadre7DeclarationSociale;
var socialDirigeant19 = dirigeantAutre11.cadre7DeclarationSociale;
var socialDirigeant20 = dirigeantAutre12.cadre7DeclarationSociale;

var regentFields = {}; 

// A mettre toujours sous "Z1611"
regentFields['/REGENT-XML/Emetteur']= "Z1611";

regentFields['/REGENT-XML/Destinataire']= authorityId;

// Complété par destiny
regentFields['/REGENT-XML/DateHeureEmission']= null;
regentFields['/REGENT-XML/VersionMessage']= null;
regentFields['/REGENT-XML/VersionNorme']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Specification/NomService']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Specification/VersionService']= null; 

                                  // Groupe GDF : Groupe données de service

// Sous groupe IDF : Identification de la formalité

// Généré par destiny
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C01']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C03']='0';
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C04']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05']= "C";

// Sous groupe EDF : Evènement déclaré
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.1']= siege.formaliteCreationAvecActivite ? (siege.etablissementPrincipalDifferentSiege ? "03M" : "01M") : "02M"; 
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2']= date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2']= date;
}

// Sous groupe DMF : Destinataire de la formalité

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[1]']= authorityId;
if (authorityId != "" and authorityId2 != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[2]']= authorityId2;
}


// Sous groupe ADF : Adresse de correspondance

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C36']= (Value('id').of(correspondance.adresseCorrespond).eq('siege') or Value('id').of(correspondance.adresseCorrespond).eq('etablissement')) ?
																		societe.entrepriseDenomination : correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance;

if (Value('id').of(correspondance.adresseCorrespond).eq('siege')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']= siege.cadreAdresseProfessionnelle.communeAdresseSiege.getId();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('etablissement')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']=etablissement.cadreAdresseEtablissement.communeAdresseEtablissement.getId();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']=correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune.getId();
}	

if ((Value('id').of(correspondance.adresseCorrespond).eq('siege') and siege.cadreAdresseProfessionnelle.numeroAdresseSiege != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('etablissement') and etablissement.cadreAdresseEtablissement.numeroAdresseEtablissement != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.numeroRueAdresseCorrespondance != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.5']= Value('id').of(correspondance.adresseCorrespond).eq('siege') ? siege.cadreAdresseProfessionnelle.numeroAdresseSiege :
																			(Value('id').of(correspondance.adresseCorrespond).eq('etablissement') ? etablissement.cadreAdresseEtablissement.numeroAdresseEtablissement : 
																			correspondance.adresseCorrespondance.numeroRueAdresseCorrespondance);
}
	
if (Value('id').of(correspondance.adresseCorrespond).eq('siege') and siege.cadreAdresseProfessionnelle.indiceAdresseSiege != null) {
   var monId1 = Value('id').of(siege.cadreAdresseProfessionnelle.indiceAdresseSiege)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId1;  
} else if (Value('id').of(correspondance.adresseCorrespond).eq('etablissement') and etablissement.cadreAdresseEtablissement.indiceAdresseEtablissement != null) {
   var monId2 = Value('id').of(etablissement.cadreAdresseEtablissement.indiceAdresseEtablissement)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId2;
} else if (correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance !== null) {
   var monId3 = Value('id').of(correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId3;
}

if ((Value('id').of(correspondance.adresseCorrespond).eq('siege') and siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('etablissement') and etablissement.cadreAdresseEtablissement.distributionSpecialeAdresseEtablissement != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.distriutionSpecialeVoieAdresseCorrespondance != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.7']= Value('id').of(correspondance.adresseCorrespond).eq('siege') ? siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege : 
																				(Value('id').of(correspondance.adresseCorrespond).eq('etablissement') ? etablissement.cadreAdresseEtablissement.distributionSpecialeAdresseEtablissement : 
																				correspondance.adresseCorrespondance.distriutionSpecialeVoieAdresseCorrespondance);
}

if (Value('id').of(correspondance.adresseCorrespond).eq('siege')
	or (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('etablissement') and etablissement.cadreAdresseEtablissement.codePostalAdresseEtablissement != null)) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8']= Value('id').of(correspondance.adresseCorrespond).eq('siege') ? (siege.cadreAdresseProfessionnelle.codePostalAdresseSiege != null ? siege.cadreAdresseProfessionnelle.codePostalAdresseSiege : ".") : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('etablissement') ? etablissement.cadreAdresseEtablissement.codePostalAdresseEtablissement : 
																			correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal);
}
	
if ((Value('id').of(correspondance.adresseCorrespond).eq('siege') and siege.cadreAdresseProfessionnelle.complementAdresseSiege != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('etablissement') and etablissement.cadreAdresseEtablissement.complementAdresseEtablissement != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.rueComplementAdresseCorrespondance != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.10']= Value('id').of(correspondance.adresseCorrespond).eq('siege') ? siege.cadreAdresseProfessionnelle.complementAdresseSiege : 
																				(Value('id').of(correspondance.adresseCorrespond).eq('etablissement') ? etablissement.cadreAdresseEtablissement.complementAdresseEtablissement : 
																				correspondance.adresseCorrespondance.rueComplementAdresseCorrespondance);
}
	
if (Value('id').of(correspondance.adresseCorrespond).eq('siege') and siege.cadreAdresseProfessionnelle.typeAdresseSiege !== null) {
   var monId1 = Value('id').of(siege.cadreAdresseProfessionnelle.typeAdresseSiege)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId1;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('etablissement') and etablissement.cadreAdresseEtablissement.typeAdresseEtablissement !== null) {
   var monId2 = Value('id').of(etablissement.cadreAdresseEtablissement.typeAdresseEtablissement)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId2;
} else if (correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance !== null) {
   var monId3 = Value('id').of(correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId3;
}	

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12']= Value('id').of(correspondance.adresseCorrespond).eq('siege') ? siege.cadreAdresseProfessionnelle.voieAdresseSiege : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('etablissement') ? etablissement.cadreAdresseEtablissement.voieAdresseEtablissement : 
																			correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance);
	
if 	(Value('id').of(correspondance.adresseCorrespond).eq('siege')) {																		
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= siege.cadreAdresseProfessionnelle.communeAdresseSiege.getLabel(); 
} else if (Value('id').of(correspondance.adresseCorrespond).eq('etablissement')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= etablissement.cadreAdresseEtablissement.communeAdresseEtablissement.getLabel(); 
} else if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune.getLabel();
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.1[1]']= correspondance.infosSup.formaliteTelephone2.e164.replace('+', '00');

if (correspondance.infosSup.formaliteTelephone1 != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.1[2]']= correspondance.infosSup.formaliteTelephone1.e164.replace('+', '00');
}

if (correspondance.infosSup.telecopie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.2']= correspondance.infosSup.telecopie.e164.replace('+', '00');
}

if (correspondance.infosSup.formaliteFaxCourriel != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.3']= correspondance.infosSup.formaliteFaxCourriel;
}


// Sous groupe SIF : Signature de la formalité

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.1']= signataire.adresseMandataire.nomPrenomDenominationMandataire != null ? 
																			signataire.adresseMandataire.nomPrenomDenominationMandataire : 
																			(dirigeant1.dirigeantSignataire ?
																			(dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			dirigeant1.civilitePersonneMorale.personneLieePMDenomination)) :
																			(dirigeant2.dirigeantSignataire ?
																			(dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			dirigeant2.civilitePersonneMorale.personneLieePMDenomination)) :
																			(dirigeant3.dirigeantSignataire ?
																			(dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			dirigeant3.civilitePersonneMorale.personneLieePMDenomination)) :
																			(dirigeant4.dirigeantSignataire ?
																			(dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			dirigeant4.civilitePersonneMorale.personneLieePMDenomination)) :
																			(dirigeant5.dirigeantSignataire ?
																			(dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			dirigeant5.civilitePersonneMorale.personneLieePMDenomination)) :
																			(dirigeant6.dirigeantSignataire ?
																			(dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			dirigeant6.civilitePersonneMorale.personneLieePMDenomination)) :
																			(dirigeant7.dirigeantSignataire ?
																			(dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			dirigeant7.civilitePersonneMorale.personneLieePMDenomination)) :
																			(dirigeant8.dirigeantSignataire ?
																			(dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			dirigeant8.civilitePersonneMorale.personneLieePMDenomination)) :
																			(dirigeantAutre1.dirigeantSignataire ?
																			(dirigeantAutre1.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeantAutre1.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeantAutre1.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeantAutre1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeantAutre1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeantAutre1.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			(dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? 
																			(dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + ' ' + dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) : 
																			(dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ?
																			(dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + ' ' + dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) :
																			dirigeantAutre1.civilitePersonneMorale.personneLieePMDenomination)))) :
																			(dirigeantAutre2.dirigeantSignataire ?
																			(dirigeantAutre2.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeantAutre2.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeantAutre2.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeantAutre2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeantAutre2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeantAutre2.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			(dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? 
																			(dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + ' ' + dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) : 
																			(dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ?
																			(dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + ' ' + dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) :
																			dirigeantAutre2.civilitePersonneMorale.personneLieePMDenomination)))) :
																			(dirigeantAutre3.dirigeantSignataire ?
																			(dirigeantAutre3.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeantAutre3.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeantAutre3.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeantAutre3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeantAutre3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeantAutre3.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			(dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? 
																			(dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + ' ' + dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) : 
																			(dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ?
																			(dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + ' ' + dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) :
																			dirigeantAutre3.civilitePersonneMorale.personneLieePMDenomination)))) :
																			(dirigeantAutre4.dirigeantSignataire ?
																			(dirigeantAutre4.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeantAutre4.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeantAutre4.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeantAutre4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeantAutre4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeantAutre4.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			(dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? 
																			(dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + ' ' + dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) : 
																			(dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ?
																			(dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + ' ' + dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) :
																			dirigeantAutre4.civilitePersonneMorale.personneLieePMDenomination)))) :
																			(dirigeantAutre5.dirigeantSignataire ?
																			(dirigeantAutre5.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeantAutre5.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeantAutre5.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeantAutre5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeantAutre5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeantAutre5.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			(dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? 
																			(dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + ' ' + dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) : 
																			(dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ?
																			(dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + ' ' + dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) :
																			dirigeantAutre5.civilitePersonneMorale.personneLieePMDenomination)))) :
																			(dirigeantAutre6.dirigeantSignataire ?
																			(dirigeantAutre6.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeantAutre6.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeantAutre6.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeantAutre6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeantAutre6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeantAutre6.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			(dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? 
																			(dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + ' ' + dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) : 
																			(dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ?
																			(dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + ' ' + dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) :
																			dirigeantAutre6.civilitePersonneMorale.personneLieePMDenomination)))) :
																			(dirigeantAutre7.dirigeantSignataire ?
																			(dirigeantAutre7.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeantAutre7.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeantAutre7.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeantAutre7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeantAutre7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeantAutre7.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			(dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? 
																			(dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + ' ' + dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) : 
																			(dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ?
																			(dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + ' ' + dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) :
																			dirigeantAutre7.civilitePersonneMorale.personneLieePMDenomination)))) :
																			(dirigeantAutre8.dirigeantSignataire ?
																			(dirigeantAutre8.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeantAutre8.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeantAutre8.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeantAutre8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeantAutre8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeantAutre8.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			(dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? 
																			(dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + ' ' + dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) : 
																			(dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ?
																			(dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + ' ' + dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) :
																			dirigeantAutre8.civilitePersonneMorale.personneLieePMDenomination)))) :
																			(dirigeantAutre9.dirigeantSignataire ?
																			(dirigeantAutre9.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeantAutre9.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeantAutre9.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeantAutre9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeantAutre9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeantAutre9.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			(dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? 
																			(dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + ' ' + dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) : 
																			(dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ?
																			(dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + ' ' + dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) :
																			dirigeantAutre9.civilitePersonneMorale.personneLieePMDenomination)))) :
																			(dirigeantAutre10.dirigeantSignataire ?
																			(dirigeantAutre10.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeantAutre10.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeantAutre10.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeantAutre10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeantAutre10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeantAutre10.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			(dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? 
																			(dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + ' ' + dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) : 
																			(dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ?
																			(dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + ' ' + dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) :
																			dirigeantAutre10.civilitePersonneMorale.personneLieePMDenomination)))) :
																			(dirigeantAutre11.dirigeantSignataire ?
																			(dirigeantAutre11.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeantAutre11.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeantAutre11.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeantAutre11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeantAutre11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeantAutre11.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			(dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? 
																			(dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + ' ' + dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) : 
																			(dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ?
																			(dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + ' ' + dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) :
																			dirigeantAutre11.civilitePersonneMorale.personneLieePMDenomination)))) :
																			(dirigeantAutre12.dirigeantSignataire ?
																			(dirigeantAutre12.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? 
																			(dirigeantAutre12.civilitePersonnePhysique.personneLieePPNomUsageGerant + ' ' + dirigeantAutre12.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) : 
																			(dirigeantAutre12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ?
																			(dirigeantAutre12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + ' ' + dirigeantAutre12.civilitePersonnePhysique.personneLieePPPrenomGerant[0]) :
																			(dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? 
																			(dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + ' ' + dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) : 
																			(dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ?
																			(dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + ' ' + dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0]) :
																			dirigeantAutre12.civilitePersonneMorale.personneLieePMDenomination)))) : ''))))))))))))))))))));

if (not dirigeant1.dirigeantSignataire and not dirigeant2.dirigeantSignataire and not dirigeant3.dirigeantSignataire and not dirigeant4.dirigeantSignataire and not dirigeant5.dirigeantSignataire and not dirigeant6.dirigeantSignataire and not dirigeant7.dirigeantSignataire and not dirigeant8.dirigeantSignataire and not dirigeantAutre1.dirigeantSignataire and not dirigeantAutre2.dirigeantSignataire and not dirigeantAutre3.dirigeantSignataire and not dirigeantAutre4.dirigeantSignataire and not dirigeantAutre5.dirigeantSignataire and not dirigeantAutre6.dirigeantSignataire and not dirigeantAutre7.dirigeantSignataire and not dirigeantAutre8.dirigeantSignataire and not dirigeantAutre9.dirigeantSignataire and not dirigeantAutre10.dirigeantSignataire and not dirigeantAutre11.dirigeantSignataire and not dirigeantAutre12.dirigeantSignataire) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.2']= "Mandataire";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.3']= signataire.adresseMandataire.villeAdresseMandataire.getId();
if (signataire.adresseMandataire.numeroVoieMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.5']= signataire.adresseMandataire.numeroVoieMandataire;
}
if (signataire.adresseMandataire.indiceVoieMandataire !== null) {
   var monId = Value('id').of(signataire.adresseMandataire.indiceVoieMandataire)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.6']          = monId;
}
if (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.7']= signataire.adresseMandataire.voieDistributionSpecialeMandataire;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.8']= signataire.adresseMandataire.codePostalMandataire;
if (signataire.adresseMandataire.voieComplementMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.10']= signataire.adresseMandataire.voieComplementMandataire;
}
if (signataire.adresseMandataire.typeVoieMandataire !== null) {
   var monId = Value('id').of(signataire.adresseMandataire.typeVoieMandataire)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.11']          = monId;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.12']= signataire.adresseMandataire.nomVoieMandataire;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.13']= signataire.adresseMandataire.villeAdresseMandataire.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C41']= signataire.formaliteSignatureLieu;
if(signataire.formaliteSignatureDate !== null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C42']= date;
}	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C43']= ((siege.societeNomDomaine[0].length > 0) ? "M05=O!" : '') +''+ (etablissement.etablissementNomDomaine != null ? "E06=0!" : '') + '' + (correspondance.formaliteObservations != null ? correspondance.formaliteObservations : '') ;

// Groupe ICM : Identification complète de la personne morale

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/ICM/M01/M01.1']= societe.entrepriseDenomination;
if (societe.entrepriseSigle != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/ICM/M01/M01.2']= societe.entrepriseSigle;
}

// Groupe FJM : Forme juridique de la personne morale (ajout)

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FJM/M21/M21.1']= societe.entrepriseFormeJuridique.getId();

// Groupe CSM : Caractéristiques de la personne morale (ajout)

if (societe.entrepriseCapitalVariable or Value('id').of(societe.entrepriseFormeJuridique).eq('6316')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M24/M24.1']= "2";
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M24/M24.2']= societe.entrepriseCapitalMontant +''+ "00";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M24/M24.3']= "eur";
if (societe.entrepriseCapitalVariable) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M24/M24.4']= societe.entrepriseCapitalVariableMinimum;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M25']= societe.entrepriseDuree;

// Groupe FSM : Fusion / Scission de la personne morale 

if (societe.entrepriseFusionScission) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M54']= Value('id').of(societe.fusionScission).eq('fusion') ? "1" : "2";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.1']= fusion1.entrepriseLieeEntreprisePMDenomination;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.2']= fusion1.entreperiseLieeEntreprisePMFormeJuridique.getId();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.3']= fusion1.adresseFusionScission.communeAdresseFusion.getId();
if(fusion1.adresseFusionScission.numeroAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.5']= fusion1.adresseFusionScission.numeroAdresseFusion;
}
if (fusion1.adresseFusionScission.indiceAdresseFusion !== null) {
   var monId = Value('id').of(fusion1.adresseFusionScission.indiceAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.6']          = monId;
}
if(fusion1.adresseFusionScission.distributionSpecialeAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.7']= fusion1.adresseFusionScission.distributionSpecialeAdresseFusion;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.8']= fusion1.adresseFusionScission.codePostalAdresseFusion;
if(fusion1.adresseFusionScission.complementAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.10']= fusion1.adresseFusionScission.complementAdresseFusion;
}
if (fusion1.adresseFusionScission.typeAdresseFusion !== null) {
   var monId = Value('id').of(fusion1.adresseFusionScission.typeAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.11']          = monId;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.12']= fusion1.adresseFusionScission.voieAdresseFusion;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.13']= fusion1.adresseFusionScission.communeAdresseFusion.getLabel();

//Récupérer le EntityId du greffe
var greffeId = fusion1.entrepriseLieeGreffe.id ;
//Appeler Directory pour récupérer l'autorité
_log.info('l entityId du greffe selectionne est {}', greffeId);
var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', greffeId) //
		   .connectionTimeout(10000) //
		   .receiveTimeout(10000) //
		   .accept('json') //
		   .get();
//result
var receiverInfo = response.asObject();
var contactInfo = !receiverInfo.details ? null : receiverInfo.details;
//Récupérer le code EDI
var codeEDIGreffe = !contactInfo.ediCode ? null : contactInfo.ediCode;
_log.info('le code edi du greffe selectionne est {}', codeEDIGreffe);
// À modifier quand Destiny fourni le réferentiel
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.4']=codeEDIGreffe;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.6']= fusion1.entrepriseLieeSiren.split(' ').join('');

if (fusion1.fusionAutreSociete) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.1']= fusion2.entrepriseLieeEntreprisePMDenomination;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.2']= fusion2.entreperiseLieeEntreprisePMFormeJuridique.getId();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.3']= fusion2.adresseFusionScission.communeAdresseFusion.getId();
if(fusion2.adresseFusionScission.numeroAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.5']= fusion2.adresseFusionScission.numeroAdresseFusion;
}
if (fusion2.adresseFusionScission.indiceAdresseFusion !== null) {
   var monId = Value('id').of(fusion2.adresseFusionScission.indiceAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.6']          = monId;
}
if(fusion2.adresseFusionScission.distributionSpecialeAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.7']= fusion2.adresseFusionScission.distributionSpecialeAdresseFusion;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.8']= fusion2.adresseFusionScission.codePostalAdresseFusion;
if(fusion2.adresseFusionScission.complementAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.10']= fusion2.adresseFusionScission.complementAdresseFusion;
}
if (fusion2.adresseFusionScission.typeAdresseFusion !== null) {
   var monId = Value('id').of(fusion2.adresseFusionScission.typeAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.11']          = monId;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.12']= fusion2.adresseFusionScission.voieAdresseFusion;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.13']= fusion2.adresseFusionScission.communeAdresseFusion.getLabel();

//Récupérer le EntityId du greffe
var greffeId = fusion2.entrepriseLieeGreffe.id ;
//Appeler Directory pour récupérer l'autorité
_log.info('l entityId du greffe selectionne est {}', greffeId);
var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', greffeId) //
		   .connectionTimeout(10000) //
		   .receiveTimeout(10000) //
		   .accept('json') //
		   .get();
//result
var receiverInfo = response.asObject();
var contactInfo = !receiverInfo.details ? null : receiverInfo.details;
//Récupérer le code EDI
var codeEDIGreffe = !contactInfo.ediCode ? null : contactInfo.ediCode;
_log.info('le code edi du greffe selectionne est {}', codeEDIGreffe);
// À modifier quand Destiny fourni le réferentiel
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.4']=codeEDIGreffe;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.6']= fusion2.entrepriseLieeSiren.split(' ').join('');
}

if (fusion2.fusionAutreSociete2) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.1']= fusion3.entrepriseLieeEntreprisePMDenomination;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.2']= fusion3.entreperiseLieeEntreprisePMFormeJuridique.getId();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.3']= fusion3.adresseFusionScission.communeAdresseFusion.getId();
if(fusion3.adresseFusionScission.numeroAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.5']= fusion3.adresseFusionScission.numeroAdresseFusion;
}
if (fusion3.adresseFusionScission.indiceAdresseFusion !== null) {
   var monId = Value('id').of(fusion3.adresseFusionScission.indiceAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.6']          = monId;
}
if(fusion3.adresseFusionScission.distributionSpecialeAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.7']= fusion3.adresseFusionScission.distributionSpecialeAdresseFusion;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.8']= fusion3.adresseFusionScission.codePostalAdresseFusion;
if(fusion3.adresseFusionScission.complementAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.10']= fusion3.adresseFusionScission.complementAdresseFusion;
}
if (fusion3.adresseFusionScission.typeAdresseFusion !== null) {
   var monId = Value('id').of(fusion3.adresseFusionScission.typeAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.11']          = monId;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.12']= fusion3.adresseFusionScission.voieAdresseFusion;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.13']= fusion3.adresseFusionScission.communeAdresseFusion.getLabel();

//Récupérer le EntityId du greffe
var greffeId = fusion3.entrepriseLieeGreffe.id ;
//Appeler Directory pour récupérer l'autorité
_log.info('l entityId du greffe selectionne est {}', greffeId);
var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', greffeId) //
		   .connectionTimeout(10000) //
		   .receiveTimeout(10000) //
		   .accept('json') //
		   .get();
//result
var receiverInfo = response.asObject();
var contactInfo = !receiverInfo.details ? null : receiverInfo.details;
//Récupérer le code EDI
var codeEDIGreffe = !contactInfo.ediCode ? null : contactInfo.ediCode;
_log.info('le code edi du greffe selectionne est {}', codeEDIGreffe);
// À modifier quand Destiny fourni le réferentiel
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.4']=codeEDIGreffe;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.6']= fusion3.entrepriseLieeSiren.split(' ').join('');
}
}

// Groupe SIU : Siège de l'entreprise

if (Value('id').of(siege.activiteLieu).eq('etablissementDomiciliationOui')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U10']= siege.domiciliataireNom;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U15']= siege.domiciliataireSiren.split(' ').join('');
}
if (Value('id').of(siege.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U16']= "O";
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.3']= siege.cadreAdresseProfessionnelle.communeAdresseSiege.getId();
if (siege.cadreAdresseProfessionnelle.numeroAdresseSiege != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.5']= siege.cadreAdresseProfessionnelle.numeroAdresseSiege;
}
if (siege.cadreAdresseProfessionnelle.indiceAdresseSiege !== null) {
   var monId = Value('id').of(siege.cadreAdresseProfessionnelle.indiceAdresseSiege)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.6']          = monId;
}
if (siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.7']= siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.8']= siege.cadreAdresseProfessionnelle.codePostalAdresseSiege;
if (siege.cadreAdresseProfessionnelle.complementAdresseSiege != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.10']= siege.cadreAdresseProfessionnelle.complementAdresseSiege;
}
if (siege.cadreAdresseProfessionnelle.typeAdresseSiege !== null) {
   var monId = Value('id').of(siege.cadreAdresseProfessionnelle.typeAdresseSiege)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.11']          = monId;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.12']= siege.cadreAdresseProfessionnelle.voieAdresseSiege;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.13']= siege.cadreAdresseProfessionnelle.communeAdresseSiege.getLabel();

// Groupe CPU : Caractéristiques de l'entreprise

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U21']= sanitize(siege.entrepriseActivitesPrincipales);
if (siege.etablissementEffectifSalariePresenceOui) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U22']= siege.cadreEffectifSalarie.etablissementEffectifSalarieNombre;
}

// RFU : Régime fiscal de l'entreprise

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U31']= (Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberale).eq('revenuFoncier') or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberaleBis).eq('revenuFoncier')) ? "120" :
																		 ((Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberale).eq('BNC') or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberaleBis).eq('BNC')) ? "111" :
																		 (((Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberale).eq('BIC') or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberaleBis).eq('BIC')) and Value('id').of(fiscal.impositionBenefices.regimeFiscalReel).eq('regimeFiscalRegimeImpositionBeneficesRs')) ? "112" :
																		 (((Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberale).eq('BIC') or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberaleBis).eq('BIC')) and Value('id').of(fiscal.impositionBenefices.regimeFiscalReel).eq('regimeFiscalRegimeImpositionBeneficesRn')) ? "113" :
																		 ((Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberaleBis).eq('IS') and Value('id').of(fiscal.impositionBenefices.regimeFiscalReel).eq('regimeFiscalRegimeImpositionBeneficesRs')) ? "114" : "115"))));

if (Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberaleBis).eq('IS')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U32']= "211";
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.1']= (Value('id').of(fiscal.optionsTVA.regimeTVA1).eq('regimeFiscalRegimeImpositionTVARfTVA') or Value('id').of(fiscal.optionsTVA.regimeTVA2).eq('regimeFiscalRegimeImpositionTVARfTVA') or Value('id').of(fiscal.optionsTVA.regimeTVA3).eq('regimeFiscalRegimeImpositionTVARfTVA')) ? "310" :
																			   ((Value('id').of(fiscal.optionsTVA.regimeTVA1).eq('regimeFiscalRegimeImpositionTVARsTVA') or Value('id').of(fiscal.optionsTVA.regimeTVA3).eq('regimeFiscalRegimeImpositionTVARsTVA')) ? "311" :
																			   ((Value('id').of(fiscal.optionsTVA.regimeTVA2).eq('regimeFiscalRegimeImpositionTVARnTVA') or Value('id').of(fiscal.optionsTVA.regimeTVA3).eq('regimeFiscalRegimeImpositionTVARnTVA')) ? "312" : "313"));

if (fiscal.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres1 or fiscal.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres2) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.2']= fiscal.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres1 ? "410" : "411";
}

// Groupe OIU : Options fiscales avec incidence sociale

if (Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberaleBis).eq('IS')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OIU/U75']= "211";
}

// Groupe GRD : Dirigeant personne physique

var occurrencesGRD = [];
var occurrence = {};

// Dirigeant 1 

if (Value('id').of(dirigeant1.personnaliteJuridique).eq('personnePhysique')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585')) {

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant1.personneLieeQualite.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
var prenoms=[];
for (i = 0; i < dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
if (dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
if(dirigeant1.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null ) {
	var dateTemp = new Date(parseInt(dirigeant1.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['D03.3'] = dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeant1.civilitePersonnePhysique.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (socialDirigeant1.voletSocialNumeroSecuriteSocialTNS != null) {
				
// Groupe ISS
	
var nirDeclarant = socialDirigeant1.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant1.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant1.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant1.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant1.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant1.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant1.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant1.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant1.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant1.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 2

if (Value('id').of(dirigeant2.personnaliteJuridique).eq('personnePhysique')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585')) {

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant2.personneLieeQualite.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
var prenoms=[];
for (i = 0; i < dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
if (dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
if(dirigeant2.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null ) {
	var dateTemp = new Date(parseInt(dirigeant2.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['D03.3'] = dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeant2.civilitePersonnePhysique.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (socialDirigeant2.voletSocialNumeroSecuriteSocialTNS != null) {
				
// Groupe ISS
	
var nirDeclarant = socialDirigeant2.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant2.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant2.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant2.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant2.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant2.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant2.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant2.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant2.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant2.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 3

if ((Value('id').of(dirigeant3.personnaliteJuridique).eq('personnePhysique')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and Value('id').of(dirigeant2.personneLieeEtablissement).eq('oui'))	{

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant3.personneLieeQualite.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
var prenoms=[];
for (i = 0; i < dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
if (dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
if(dirigeant3.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null ) {
	var dateTemp = new Date(parseInt(dirigeant3.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['D03.3'] = dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeant3.civilitePersonnePhysique.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (socialDirigeant3.voletSocialNumeroSecuriteSocialTNS != null) {
				
// Groupe ISS
	
var nirDeclarant = socialDirigeant3.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant3.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant3.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant3.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant3.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant3.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant3.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant3.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant3.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant3.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 4

if ((Value('id').of(dirigeant4.personnaliteJuridique).eq('personnePhysique')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and Value('id').of(dirigeant3.personneLieeEtablissement).eq('oui'))	{

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant4.personneLieeQualite.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
var prenoms=[];
for (i = 0; i < dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
if (dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
if(dirigeant4.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null ) {
	var dateTemp = new Date(parseInt(dirigeant4.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['D03.3'] = dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeant4.civilitePersonnePhysique.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (socialDirigeant4.voletSocialNumeroSecuriteSocialTNS != null) {
				
// Groupe ISS
	
var nirDeclarant = socialDirigeant4.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant4.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant4.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant4.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant4.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant4.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant4.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant4.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant4.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant4.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 5

if ((Value('id').of(dirigeant5.personnaliteJuridique).eq('personnePhysique')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and Value('id').of(dirigeant4.personneLieeEtablissement).eq('oui'))	{

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant5.personneLieeQualite.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
var prenoms=[];
for (i = 0; i < dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
if (dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
if(dirigeant5.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null ) {
	var dateTemp = new Date(parseInt(dirigeant5.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['D03.3'] = dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeant5.civilitePersonnePhysique.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (socialDirigeant5.voletSocialNumeroSecuriteSocialTNS != null) {
				
// Groupe ISS
	
var nirDeclarant = socialDirigeant5.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant5.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant5.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant5.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant5.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant5.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant5.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant5.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant5.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant5.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 6

if ((Value('id').of(dirigeant6.personnaliteJuridique).eq('personnePhysique')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and Value('id').of(dirigeant5.personneLieeEtablissement).eq('oui'))	{

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant6.personneLieeQualite.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
var prenoms=[];
for (i = 0; i < dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
if (dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
if(dirigeant6.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null ) {
	var dateTemp = new Date(parseInt(dirigeant6.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['D03.3'] = dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeant6.civilitePersonnePhysique.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (socialDirigeant6.voletSocialNumeroSecuriteSocialTNS != null) {
				
// Groupe ISS
	
var nirDeclarant = socialDirigeant6.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant6.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant6.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant6.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant6.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant6.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant6.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant6.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant6.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant6.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 7

if ((Value('id').of(dirigeant7.personnaliteJuridique).eq('personnePhysique')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and Value('id').of(dirigeant6.personneLieeEtablissement).eq('oui'))	{

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant7.personneLieeQualite.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
var prenoms=[];
for (i = 0; i < dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
if (dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
if(dirigeant7.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null ) {
	var dateTemp = new Date(parseInt(dirigeant7.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['D03.3'] = dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeant7.civilitePersonnePhysique.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (socialDirigeant7.voletSocialNumeroSecuriteSocialTNS != null) {
				
// Groupe ISS
	
var nirDeclarant = socialDirigeant7.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant7.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant7.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant7.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant7.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant7.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant7.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant7.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant7.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant7.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant7.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant7.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant7.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant7.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 8

if ((Value('id').of(dirigeant8.personnaliteJuridique).eq('personnePhysique')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and Value('id').of(dirigeant7.personneLieeEtablissement).eq('oui'))	{

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant8.personneLieeQualite.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
var prenoms=[];
for (i = 0; i < dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
if (dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
if(dirigeant8.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null ) {
	var dateTemp = new Date(parseInt(dirigeant8.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['D03.3'] = dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeant8.civilitePersonnePhysique.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (socialDirigeant8.voletSocialNumeroSecuriteSocialTNS != null) {
				
// Groupe ISS
	
var nirDeclarant = socialDirigeant8.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant8.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant8.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant8.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant8.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant8.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant8.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant8.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant8.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant8.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant8.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant8.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant8.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant8.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 9

if ((Value('id').of(societe.entrepriseFormeJuridique).eq('6316') 
	or dirigeant2.autrePersonneLieeEtablissement or dirigeant3.autrePersonneLieeEtablissement 
	or dirigeant4.autrePersonneLieeEtablissement or dirigeant5.autrePersonneLieeEtablissement 
	or dirigeant6.autrePersonneLieeEtablissement or dirigeant7.autrePersonneLieeEtablissement 
	or dirigeant8.autrePersonneLieeEtablissement)
	and (((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre1.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre1.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre1.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre1.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre1.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre1.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre1.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre1.personneLieeQualiteBis).eq('51')
	or Value('id').of(dirigeantAutre1.personnaliteJuridique).eq('personnePhysique'))) {

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeantAutre1.personneLieeQualite != null ? dirigeantAutre1.personneLieeQualite.getId() : dirigeantAutre1.personneLieeQualiteBis.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeantAutre1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeantAutre1.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre1.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeantAutre1.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre1.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
} else if (dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
}
if (dirigeantAutre1.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeantAutre1.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre1.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(dirigeantAutre1.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre1.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
} else if(dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeantAutre1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeantAutre1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeantAutre1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeantAutre1.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeantAutre1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeantAutre1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeantAutre1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeantAutre1.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeantAutre1.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeantAutre1.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (socialDirigeant9.voletSocialNumeroSecuriteSocialTNS != null) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant9.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant9.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant9.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant9.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant9.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant9.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant9.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant9.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant9.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant9.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant9.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant9.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant9.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant9.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 10

if ((Value('id').of(societe.entrepriseFormeJuridique).eq('6316') 
	or dirigeantAutre1.autrePersonneLieeEtablissement)
	and (((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre2.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre2.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre2.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre2.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre2.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre2.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre2.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre2.personneLieeQualiteBis).eq('51')
	or Value('id').of(dirigeantAutre2.personnaliteJuridique).eq('personnePhysique'))) {

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeantAutre2.personneLieeQualite != null ? dirigeantAutre2.personneLieeQualite.getId() : dirigeantAutre2.personneLieeQualiteBis.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeantAutre2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeantAutre2.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre2.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeantAutre2.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre2.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
} else if (dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
}
if (dirigeantAutre2.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeantAutre2.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre2.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(dirigeantAutre2.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre2.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
} else if(dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeantAutre2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeantAutre2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeantAutre2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeantAutre2.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeantAutre2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeantAutre2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeantAutre2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeantAutre2.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeantAutre2.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeantAutre2.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (socialDirigeant10.voletSocialNumeroSecuriteSocialTNS != null) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant10.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant10.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant10.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant10.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant10.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant10.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant10.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant10.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant10.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant10.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant10.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant10.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant10.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant10.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 11

if (dirigeantAutre2.autrePersonneLieeEtablissement
	and (((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre3.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre3.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre3.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre3.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre3.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre3.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre3.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre3.personneLieeQualiteBis).eq('51')
	or Value('id').of(dirigeantAutre3.personnaliteJuridique).eq('personnePhysique'))) {

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeantAutre3.personneLieeQualite != null ? dirigeantAutre3.personneLieeQualite.getId() : dirigeantAutre3.personneLieeQualiteBis.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeantAutre3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeantAutre3.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre3.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeantAutre3.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre3.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
} else if (dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
}
if (dirigeantAutre3.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeantAutre3.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre3.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(dirigeantAutre3.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre3.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
} else if(dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeantAutre3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeantAutre3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeantAutre3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeantAutre3.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeantAutre3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeantAutre3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeantAutre3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeantAutre3.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeantAutre3.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeantAutre3.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (socialDirigeant11.voletSocialNumeroSecuriteSocialTNS != null) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant11.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant11.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant11.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant11.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant11.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant11.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant11.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant11.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant11.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant11.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant11.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant11.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant11.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant11.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 12

if (dirigeantAutre3.autrePersonneLieeEtablissement
	and (((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre4.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre4.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre4.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre4.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre4.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre4.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre4.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre4.personneLieeQualiteBis).eq('51')
	or Value('id').of(dirigeantAutre4.personnaliteJuridique).eq('personnePhysique'))) {

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeantAutre4.personneLieeQualite != null ? dirigeantAutre4.personneLieeQualite.getId() : dirigeantAutre4.personneLieeQualiteBis.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeantAutre4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeantAutre4.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre4.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeantAutre4.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre4.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
} else if (dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
}
if (dirigeantAutre4.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeantAutre4.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre4.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(dirigeantAutre4.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre4.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
} else if(dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeantAutre4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeantAutre4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeantAutre4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeantAutre4.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeantAutre4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeantAutre4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeantAutre4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeantAutre4.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeantAutre4.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeantAutre4.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (socialDirigeant12.voletSocialNumeroSecuriteSocialTNS != null) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant12.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant12.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant12.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant12.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant12.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant12.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant12.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant12.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant12.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant12.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant12.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant12.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant12.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant12.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 13

if (dirigeantAutre4.autrePersonneLieeEtablissement
	and (((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre5.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre5.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre5.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre5.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre5.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre5.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre5.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre5.personneLieeQualiteBis).eq('51')
	or Value('id').of(dirigeantAutre5.personnaliteJuridique).eq('personnePhysique'))) {

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeantAutre5.personneLieeQualite != null ? dirigeantAutre5.personneLieeQualite.getId() : dirigeantAutre5.personneLieeQualiteBis.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeantAutre5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeantAutre5.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre5.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeantAutre5.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre5.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
} else if (dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
}
if (dirigeantAutre5.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeantAutre5.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre5.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(dirigeantAutre5.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre5.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
} else if(dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeantAutre5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeantAutre5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeantAutre5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeantAutre5.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeantAutre5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeantAutre5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeantAutre5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeantAutre5.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeantAutre5.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeantAutre5.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (socialDirigeant13.voletSocialNumeroSecuriteSocialTNS != null) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant13.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant13.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant13.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant13.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant13.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant13.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant13.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant13.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant13.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant13.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant13.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant13.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant13.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant13.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant13.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant13.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant13.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant13.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant13.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 14

if (dirigeantAutre5.autrePersonneLieeEtablissement
	and (((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre6.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre6.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre6.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre6.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre6.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre6.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre6.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre6.personneLieeQualiteBis).eq('51')
	or Value('id').of(dirigeantAutre6.personnaliteJuridique).eq('personnePhysique'))) {

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeantAutre6.personneLieeQualite != null ? dirigeantAutre6.personneLieeQualite.getId() : dirigeantAutre6.personneLieeQualiteBis.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeantAutre6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeantAutre6.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre6.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeantAutre6.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre6.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
} else if (dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
}
if (dirigeantAutre6.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeantAutre6.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre6.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(dirigeantAutre6.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre6.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
} else if(dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeantAutre6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeantAutre6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeantAutre6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeantAutre6.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeantAutre6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeantAutre6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeantAutre6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeantAutre6.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeantAutre6.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeantAutre6.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (socialDirigeant14.voletSocialNumeroSecuriteSocialTNS != null) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant14.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant14.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant14.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant14.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant14.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant14.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant14.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant14.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant14.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant14.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant14.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant14.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant14.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant14.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant14.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant14.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant14.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant14.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant14.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 15

if (dirigeantAutre6.autrePersonneLieeEtablissement
	and (((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre7.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre7.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre7.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre7.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre7.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre7.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre7.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre7.personneLieeQualiteBis).eq('51')
	or Value('id').of(dirigeantAutre7.personnaliteJuridique).eq('personnePhysique'))) {

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeantAutre7.personneLieeQualite != null ? dirigeantAutre7.personneLieeQualite.getId() : dirigeantAutre7.personneLieeQualiteBis.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeantAutre7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeantAutre7.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre7.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeantAutre7.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre7.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
} else if (dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
}
if (dirigeantAutre7.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeantAutre7.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre7.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(dirigeantAutre7.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre7.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
} else if(dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeantAutre7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeantAutre7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeantAutre7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeantAutre7.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeantAutre7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeantAutre7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeantAutre7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeantAutre7.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeantAutre7.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeantAutre7.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (socialDirigeant15.voletSocialNumeroSecuriteSocialTNS != null) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant15.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant15.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant15.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant15.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant15.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant15.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant15.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant15.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant15.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant15.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant15.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant15.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant15.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant15.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant15.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant15.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant15.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant15.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant15.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 16

if (dirigeantAutre7.autrePersonneLieeEtablissement
	and (((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre8.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre8.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre8.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre8.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre8.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre8.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre8.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre8.personneLieeQualiteBis).eq('51')
	or Value('id').of(dirigeantAutre8.personnaliteJuridique).eq('personnePhysique'))) {

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeantAutre8.personneLieeQualite != null ? dirigeantAutre8.personneLieeQualite.getId() : dirigeantAutre8.personneLieeQualiteBis.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeantAutre8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeantAutre8.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre8.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeantAutre8.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre8.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
} else if (dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
}
if (dirigeantAutre8.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeantAutre8.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre8.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(dirigeantAutre8.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre8.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
} else if(dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeantAutre8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeantAutre8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeantAutre8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeantAutre8.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeantAutre8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeantAutre8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeantAutre8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeantAutre8.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeantAutre8.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeantAutre8.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (socialDirigeant16.voletSocialNumeroSecuriteSocialTNS != null) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant16.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant16.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant16.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant16.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant16.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant16.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant16.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant16.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant16.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant16.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant16.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant16.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant16.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant16.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant16.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant16.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant16.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant16.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant16.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 17

if (dirigeantAutre8.autrePersonneLieeEtablissement
	and (((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre9.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre9.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre9.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre9.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre9.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre9.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre9.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre9.personneLieeQualiteBis).eq('51')
	or Value('id').of(dirigeantAutre9.personnaliteJuridique).eq('personnePhysique'))) {

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeantAutre9.personneLieeQualite != null ? dirigeantAutre9.personneLieeQualite.getId() : dirigeantAutre9.personneLieeQualiteBis.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeantAutre9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeantAutre9.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre9.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeantAutre9.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre9.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
} else if (dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
}
if (dirigeantAutre9.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeantAutre9.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre9.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(dirigeantAutre9.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre9.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
} else if(dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeantAutre9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeantAutre9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeantAutre9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeantAutre9.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeantAutre9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeantAutre9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeantAutre9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeantAutre9.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeantAutre9.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeantAutre9.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (socialDirigeant17.voletSocialNumeroSecuriteSocialTNS != null) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant17.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant17.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant17.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant17.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant17.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant17.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant17.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant17.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant17.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant17.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant17.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant17.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant17.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant17.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant17.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant17.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant17.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant17.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant17.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 18

if (dirigeantAutre9.autrePersonneLieeEtablissement
	and (((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre10.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre10.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre10.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre10.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre10.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre10.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre10.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre10.personneLieeQualiteBis).eq('51')
	or Value('id').of(dirigeantAutre10.personnaliteJuridique).eq('personnePhysique'))) {

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeantAutre10.personneLieeQualite != null ? dirigeantAutre10.personneLieeQualite.getId() : dirigeantAutre10.personneLieeQualiteBis.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeantAutre10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeantAutre10.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre10.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeantAutre10.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre10.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
} else if (dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
}
if (dirigeantAutre10.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeantAutre10.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre10.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(dirigeantAutre10.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre10.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
} else if(dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeantAutre10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeantAutre10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeantAutre10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeantAutre10.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeantAutre10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeantAutre10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeantAutre10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeantAutre10.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeantAutre10.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeantAutre10.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (socialDirigeant18.voletSocialNumeroSecuriteSocialTNS != null) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant18.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant18.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant18.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant18.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant18.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant18.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant18.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant18.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant18.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant18.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant18.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant18.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant18.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant18.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant18.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant18.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant18.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant18.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant18.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 19

if (dirigeantAutre10.autrePersonneLieeEtablissement
	and (((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre11.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre11.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre11.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre11.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre11.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre11.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre11.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre11.personneLieeQualiteBis).eq('51')
	or Value('id').of(dirigeantAutre11.personnaliteJuridique).eq('personnePhysique'))) {

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeantAutre11.personneLieeQualite != null ? dirigeantAutre11.personneLieeQualite.getId() : dirigeantAutre11.personneLieeQualiteBis.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeantAutre11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeantAutre11.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre11.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeantAutre11.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre11.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
} else if (dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
}
if (dirigeantAutre11.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeantAutre11.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre11.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(dirigeantAutre11.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre11.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
} else if(dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeantAutre11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeantAutre11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeantAutre11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeantAutre11.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeantAutre11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeantAutre11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeantAutre11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeantAutre11.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeantAutre11.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeantAutre11.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (socialDirigeant19.voletSocialNumeroSecuriteSocialTNS != null) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant19.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant19.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant19.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant19.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant19.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant19.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant19.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant19.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant19.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant19.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant19.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant19.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant19.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant19.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant19.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant19.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant19.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant19.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant19.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

// Dirigeant 20

if (dirigeantAutre11.autrePersonneLieeEtablissement
	and (((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre12.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre12.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre12.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre12.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre12.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre12.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre12.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre12.personneLieeQualiteBis).eq('51')
	or Value('id').of(dirigeantAutre12.personnaliteJuridique).eq('personnePhysique'))) {

//Groupe DIU : Dirigeant de l'entreprise
occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeantAutre12.personneLieeQualite != null ? dirigeantAutre12.personneLieeQualite.getId() : dirigeantAutre12.personneLieeQualiteBis.getId();
occurrence['U52'] = "P";

// Groupe IPD : Identification du dirigeant de l'entreprise

occurrence['D01.2'] = dirigeantAutre12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeantAutre12.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre12.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeantAutre12.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre12.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
} else if (dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['D01.3']= prenoms;
}
if (dirigeantAutre12.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['D01.4'] = dirigeantAutre12.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre12.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(dirigeantAutre12.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre12.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
} else if(dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['D03.1']= date;
}	
if (not Value('id').of(dirigeantAutre12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeantAutre12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeantAutre12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeantAutre12.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['D03.2']= result;
} else if (Value('id').of(dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['D03.2'] = dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeantAutre12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeantAutre12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['D03.3'] = dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeantAutre12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeantAutre12.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['D03.4'] = dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
if (Value('id').of(dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.3']= dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['D04.3']= result;
}
if (dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['D04.5']= dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['D04.6']          = monId1;  
} 
if (dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['D04.7']= dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['D04.8']= dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['D04.10']= dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['D04.11']          = monId1;  
} 
occurrence['D04.12']= dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.13']= dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['D04.14']= dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD

occurrence['D21']= dirigeantAutre12.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeantAutre12.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;

// Groupe GCS
		
if (socialDirigeant20.voletSocialNumeroSecuriteSocialTNS != null) {
	
// Groupe ISS
	
var nirDeclarant = socialDirigeant20.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant !=null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
occurrence['A10.1']  = nirDeclarant.substring(0, 13)
occurrence['A10.2'] = nirDeclarant.substring(13, 15);
}

// Groupe SNS

if (socialDirigeant20.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant20.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTemp = new Date(parseInt(socialDirigeant20.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['A21.2']= date;
}
occurrence['A21.3']= socialDirigeant20.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['A21.4']= socialDirigeant20.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['A21.6']= socialDirigeant20.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['A22.3']= Value('id').of(socialDirigeant20.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" : (Value('id').of(socialDirigeant20.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
					(Value('id').of(socialDirigeant20.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : "X"));
if (Value('id').of(socialDirigeant20.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['A22.4']= socialDirigeant20.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
} else if (Value('id').of(socialDirigeant20.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['A22.4']= "ENIM";
}
if (socialDirigeant20.voletSocialActiviteSimultaneeOUINON) {
occurrence['A24.2'] = Value('id').of(socialDirigeant20.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" : (Value('id').of(socialDirigeant20.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
					(Value('id').of(socialDirigeant20.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : "9"));
}
if (Value('id').of(socialDirigeant20.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {																												
occurrence['A24.3'] = socialDirigeant20.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}	

// Groupe CAS

occurrence['A42.1']= ".";
}

occurrencesGRD.push(occurrence);
occurrence = {};
}

occurrencesGRD.forEach(function (value, i) {
	var idx = i+1;
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/DIU/U50/U50.1']= value['U50.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/DIU/U50/U50.2']= value['U50.2'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/DIU/U51/U51.1']= value['U51.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/DIU/U52']= value['U52'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D01/D01.2']= value['D01.2'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D01/D01.3']= value['D01.3'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D01/D01.4']= value['D01.4'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D03/D03.1']= value['D03.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D03/D03.2']= value['D03.2'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D03/D03.3']= value['D03.3'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D03/D03.4']= value['D03.4'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.3']= value['D04.3'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.5']= value['D04.5'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.6']= value['D04.6'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.7']= value['D04.7'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.8']= value['D04.8'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.10']= value['D04.10'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.11']= value['D04.11'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.12']= value['D04.12'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.13']= value['D04.13'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.14']= value['D04.14'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/NPD/D21']= value['D21'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/ISS/A10/A10.1']= value['A10.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/ISS/A10/A10.2']= value['A10.2'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A21/A21.2']= value['A21.2'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A21/A21.3']= value['A21.3'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A21/A21.4']= value['A21.4'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A21/A21.6']= value['A21.6'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A22/A22.3']= value['A22.3'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A22/A22.4']= value['A22.4'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A24/A24.2']= value['A24.2'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A24/A24.3']= value['A24.3'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/CAS/A42/A42.1']= value['A42.1'];
});

// Groupe GDR : Dirigeant personne morale

var occurrencesGDR = [];
var occurrence = {};

// Dirigeant 1 

if (Value('id').of(dirigeant1.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant1.personneLieeQualite.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeant1.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeant1.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
occurrence['R22.2']= dirigeant1.civilitePersonneMorale.personneLieePMNumeroIdentification;
occurrence['R22.3']= dirigeant1.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 2 

if (Value('id').of(dirigeant2.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant2.personneLieeQualite.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeant2.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeant2.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
occurrence['R22.2']= dirigeant2.civilitePersonneMorale.personneLieePMNumeroIdentification;
occurrence['R22.3']= dirigeant2.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 3 

if (Value('id').of(dirigeant3.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant3.personneLieeQualite.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeant3.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeant3.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
occurrence['R22.2']= dirigeant3.civilitePersonneMorale.personneLieePMNumeroIdentification;
occurrence['R22.3']= dirigeant3.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 4

if (Value('id').of(dirigeant4.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant4.personneLieeQualite.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeant4.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeant4.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
occurrence['R22.2']= dirigeant4.civilitePersonneMorale.personneLieePMNumeroIdentification;
occurrence['R22.3']= dirigeant4.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 5 

if (Value('id').of(dirigeant5.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant5.personneLieeQualite.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeant5.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeant5.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
occurrence['R22.2']= dirigeant5.civilitePersonneMorale.personneLieePMNumeroIdentification;
occurrence['R22.3']= dirigeant5.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 6 

if (Value('id').of(dirigeant6.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant6.personneLieeQualite.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeant6.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeant6.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
occurrence['R22.2']= dirigeant6.civilitePersonneMorale.personneLieePMNumeroIdentification;
occurrence['R22.3']= dirigeant6.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 7 

if (Value('id').of(dirigeant7.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant7.personneLieeQualite.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeant7.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeant7.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
occurrence['R22.2']= dirigeant7.civilitePersonneMorale.personneLieePMNumeroIdentification;
occurrence['R22.3']= dirigeant7.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 8

if (Value('id').of(dirigeant8.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeant8.personneLieeQualite.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeant8.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeant8.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
occurrence['R22.2']= dirigeant8.civilitePersonneMorale.personneLieePMNumeroIdentification;
occurrence['R22.3']= dirigeant8.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 9 

if (Value('id').of(dirigeantAutre1.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeantAutre1.personneLieeQualite != null ? dirigeantAutre1.personneLieeQualite.getId() : dirigeantAutre1.personneLieeQualiteBis.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeantAutre1.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeantAutre1.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
occurrence['R22.2']= dirigeantAutre1.civilitePersonneMorale.personneLieePMNumeroIdentification;
occurrence['R22.3']= dirigeantAutre1.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 10

if (Value('id').of(dirigeantAutre2.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeantAutre2.personneLieeQualite != null ? dirigeantAutre2.personneLieeQualite.getId() : dirigeantAutre2.personneLieeQualiteBis.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeantAutre2.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeantAutre2.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
occurrence['R22.2']= dirigeantAutre2.civilitePersonneMorale.personneLieePMNumeroIdentification;
occurrence['R22.3']= dirigeantAutre2.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 11 

if (Value('id').of(dirigeantAutre3.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeantAutre3.personneLieeQualite != null ? dirigeantAutre3.personneLieeQualite.getId() : dirigeantAutre3.personneLieeQualiteBis.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeantAutre3.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeantAutre3.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
occurrence['R22.2']= dirigeantAutre3.civilitePersonneMorale.personneLieePMNumeroIdentification;
occurrence['R22.3']= dirigeantAutre3.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 12 

if (Value('id').of(dirigeantAutre4.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeantAutre4.personneLieeQualite != null ? dirigeantAutre4.personneLieeQualite.getId() : dirigeantAutre4.personneLieeQualiteBis.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeantAutre4.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeantAutre4.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
occurrence['R22.2']= dirigeantAutre4.civilitePersonneMorale.personneLieePMNumeroIdentification;
occurrence['R22.3']= dirigeantAutre4.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 13 

if (Value('id').of(dirigeantAutre5.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeantAutre5.personneLieeQualite != null ? dirigeantAutre5.personneLieeQualite.getId() : dirigeantAutre5.personneLieeQualiteBis.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeantAutre5.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeantAutre5.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
occurrence['R22.2']= dirigeantAutre5.civilitePersonneMorale.personneLieePMNumeroIdentification;
occurrence['R22.3']= dirigeantAutre5.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 14 

if (Value('id').of(dirigeantAutre6.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeantAutre6.personneLieeQualite != null ? dirigeantAutre6.personneLieeQualite.getId() : dirigeantAutre6.personneLieeQualiteBis.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeantAutre6.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeantAutre6.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
occurrence['R22.2']= dirigeantAutre6.civilitePersonneMorale.personneLieePMNumeroIdentification;
occurrence['R22.3']= dirigeantAutre6.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 15 

if (Value('id').of(dirigeantAutre7.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeantAutre7.personneLieeQualite != null ? dirigeantAutre7.personneLieeQualite.getId() : dirigeantAutre7.personneLieeQualiteBis.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeantAutre7.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeantAutre7.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
occurrence['R22.2']= dirigeantAutre7.civilitePersonneMorale.personneLieePMNumeroIdentification;
occurrence['R22.3']= dirigeantAutre7.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 16 

if (Value('id').of(dirigeantAutre8.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeantAutre8.personneLieeQualite != null ? dirigeantAutre8.personneLieeQualite.getId() : dirigeantAutre8.personneLieeQualiteBis.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeantAutre8.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeantAutre8.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
occurrence['R22.2']= dirigeantAutre8.civilitePersonneMorale.personneLieePMNumeroIdentification;
occurrence['R22.3']= dirigeantAutre8.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 17 

if (Value('id').of(dirigeantAutre9.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeantAutre9.personneLieeQualite != null ? dirigeantAutre9.personneLieeQualite.getId() : dirigeantAutre9.personneLieeQualiteBis.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeantAutre9.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeantAutre9.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
occurrence['R22.2']= dirigeantAutre9.civilitePersonneMorale.personneLieePMNumeroIdentification;
occurrence['R22.3']= dirigeantAutre9.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 18 

if (Value('id').of(dirigeantAutre10.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeantAutre10.personneLieeQualite != null ? dirigeantAutre10.personneLieeQualite.getId() : dirigeantAutre10.personneLieeQualiteBis.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeantAutre10.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeantAutre10.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
occurrence['R22.2']= dirigeantAutre10.civilitePersonneMorale.personneLieePMNumeroIdentification;
occurrence['R22.3']= dirigeantAutre10.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 19 

if (Value('id').of(dirigeantAutre11.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeantAutre11.personneLieeQualite != null ? dirigeantAutre11.personneLieeQualite.getId() : dirigeantAutre11.personneLieeQualiteBis.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeantAutre11.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeantAutre11.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
occurrence['R22.2']= dirigeantAutre11.civilitePersonneMorale.personneLieePMNumeroIdentification;
occurrence['R22.3']= dirigeantAutre11.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 20 

if (Value('id').of(dirigeantAutre12.personnaliteJuridique).eq('personneMorale')) {

//Groupe DIU : Dirigeant de l'entreprise

occurrence['U50.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['U50.2'] = date;
}
occurrence['U51.1'] = dirigeantAutre12.personneLieeQualite != null ? dirigeantAutre12.personneLieeQualite.getId() : dirigeantAutre12.personneLieeQualiteBis.getId();
occurrence['U52'] = "M";

//Groupe ICR : Identification complète de la personne morale dirigeante

occurrence['R01.1'] = dirigeantAutre12.civilitePersonneMorale.personneLieePMDenomination;
if (Value('id').of(dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.3']= dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['R02.3']= result;
}
if (dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['R02.5']= dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['R02.6']          = monId1;  
} 
if (dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['R02.7']= dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['R02.8']= dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['R02.10']= dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['R02.11']          = monId1;  
} 
occurrence['R02.12']= dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.13']= dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['R02.14']= dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['R03'] = dirigeantAutre12.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
occurrence['R22.2']= dirigeantAutre12.civilitePersonneMorale.personneLieePMNumeroIdentification;
occurrence['R22.3']= dirigeantAutre12.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}


occurrencesGDR.forEach(function (value, i) {
	var idx = i+1;
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/DIU/U50/U50.1']= value['U50.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/DIU/U50/U50.2']= value['U50.2'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/DIU/U51/U51.1']= value['U51.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/DIU/U52']= value['U52'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R01/R01.1']= value['R01.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.3']= value['R02.3'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.5']= value['R02.5'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.6']= value['R02.6'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.7']= value['R02.7'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.8']= value['R02.8'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.10']= value['R02.10'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.11']= value['R02.11'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.12']= value['R02.12'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.13']= value['R02.13'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.14']= value['R02.14'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R03']= value['R03'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R22/R22.2']= value['R22.2'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R22/R22.3']= value['R22.3'];
});


// Fin Regent 02M

if (siege.formaliteCreationAvecActivite) {
	
// Groupe ICE : Identification complète de l'établissement

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E01']= "1";
if (siege.etablissementPrincipalDifferentSiege) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.3']= etablissement.cadreAdresseEtablissement.communeAdresseEtablissement.getId();
if (etablissement.cadreAdresseEtablissement.numeroAdresseEtablissement != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5']= etablissement.cadreAdresseEtablissement.numeroAdresseEtablissement;
}
if (etablissement.cadreAdresseEtablissement.indiceAdresseEtablissement != null) {
   var monId1 = Value('id').of(etablissement.cadreAdresseEtablissement.indiceAdresseEtablissement)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.6']          = monId1;  
}
if (etablissement.cadreAdresseEtablissement.distributionSpecialeAdresseEtablissement != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.7']= etablissement.cadreAdresseEtablissement.distributionSpecialeAdresseEtablissement;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.8']= etablissement.cadreAdresseEtablissement.codePostalAdresseEtablissement;
if (etablissement.cadreAdresseEtablissement.complementAdresseEtablissement != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.10']= etablissement.cadreAdresseEtablissement.complementAdresseEtablissement;
}
if (etablissement.cadreAdresseEtablissement.typeAdresseEtablissement != null) {
   var monId1 = Value('id').of(etablissement.cadreAdresseEtablissement.typeAdresseEtablissement)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.11']          = monId1;  
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.12']= etablissement.cadreAdresseEtablissement.voieAdresseEtablissement;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.13']= etablissement.cadreAdresseEtablissement.communeAdresseEtablissement.getLabel();
} else if (not siege.etablissementPrincipalDifferentSiege) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.3']= siege.cadreAdresseProfessionnelle.communeAdresseSiege.getId();	
if (siege.cadreAdresseProfessionnelle.numeroAdresseSiege != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5']= siege.cadreAdresseProfessionnelle.numeroAdresseSiege;
}
if (siege.cadreAdresseProfessionnelle.indiceAdresseSiege != null) {
   var monId1 = Value('id').of(siege.cadreAdresseProfessionnelle.indiceAdresseSiege)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.6']          = monId1;  
}
if (siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.7']= siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.8']= siege.cadreAdresseProfessionnelle.codePostalAdresseSiege;
if (siege.cadreAdresseProfessionnelle.complementAdresseSiege != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.10']= siege.cadreAdresseProfessionnelle.complementAdresseSiege;
}
if (siege.cadreAdresseProfessionnelle.typeAdresseSiege != null) {
   var monId1 = Value('id').of(siege.cadreAdresseProfessionnelle.typeAdresseSiege)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.11']          = monId1;  
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.12']= siege.cadreAdresseProfessionnelle.voieAdresseSiege;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.13']= siege.cadreAdresseProfessionnelle.communeAdresseSiege.getLabel();	
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E04']= siege.etablissementPrincipalDifferentSiege ? "3" : "2";

// Groupe ORE : Origine de l'établissement

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.1']= (Value('id').of(origine.etablissementOrigineFondsLiberal).eq('EtablissementOrigineFondsCreation') or ((Value('id').of(siege.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') or Value('id').of(siege.activiteLieu).eq('etablissementDomiciliationOui')) and not siege.etablissementPrincipalDifferentSiege)) ? "1" : "8";

// Groupe ACE : Activité de l'établissement

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E70']= sanitize(etablissement.etablissementActivitePlusImportante + '' + "." + ' ' + (etablissement.etablissementActivitesAutres != null ? etablissement.etablissementActivitesAutres : ''));
if ((Value('id').of(societe.entrepriseFormeJuridique).eq('6540') or
	Value('id').of(societe.entrepriseFormeJuridique).eq('6541') or
	Value('id').of(societe.entrepriseFormeJuridique).eq('6542') or
	Value('id').of(societe.entrepriseFormeJuridique).eq('6543') or
	Value('id').of(societe.entrepriseFormeJuridique).eq('6599'))
	and etablissement.questionActivitePrincipaleImmo != null)	{
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E71']= Value('id').of(etablissement.questionActivitePrincipaleImmo).eq('etablissementActiviteImmobiliereLocation') ? etablissement.questionActivitePrincipaleLocation.getLabel() : 
																					   (Value('id').of(etablissement.questionActivitePrincipaleImmo).eq('etablissementActiviteImmobilierePromotion') ? etablissement.questionActivitePrincipalePromotion.getLabel() : etablissement.questionActivitePrincipaleImmo.getLabel());
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E71']= etablissement.etablissementActivitePlusImportante;
}

// Groupe CAE : Complément sur l'activité de l'établissement

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E73/E73.1']= "P";

// Groupe SAE : Salariés de l'établissement

if (siege.etablissementEffectifSalariePresenceOui and siege.cadreEffectifSalarie.etablissementEffectifSalarieEmbauchePremierSalarieOui){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E83/E83.1']="1";
}	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E84']= siege.etablissementEffectifSalariePresenceOui ? siege.cadreEffectifSalarie.etablissementEffectifSalarieNombre : "0";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E86']= siege.etablissementEffectifSalariePresenceOui ? "O" : "N";

// Groupe IDE : Identification de la personne liée à l'établissement

var occurrencesIDE = [];
var occurrence = {};

// Fondé de pouvoir 1	

if (Value('id').of(dirigeantAutre1.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre1.personneLieeQualiteBis).eq('66')) {

occurrence['E91.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
}
occurrence['E92.1'] = dirigeantAutre1.civilitePersonnePhysique.personneLieePersonnePouvoirLimiteEtablissementOui ? "P" : "S";
occurrence['E93.2'] = dirigeantAutre1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (dirigeantAutre1.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['E93.4'] = dirigeantAutre1.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for (i = 0; i < dirigeantAutre1.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre1.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['E93.3']= prenoms;
if (Value('id').of(dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.3']= dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['E94.3']= result;
}
if (dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['E94.5']= dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['E94.6']          = monId1;  
} 
if (dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['E94.7']= dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['E94.8']= dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['E94.10']= dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['E94.11']          = monId1;  
} 
occurrence['E94.12']= dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.14']= dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
if(dirigeantAutre1.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre1.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['E95.1']= date;
}
if (not Value('id').of(dirigeantAutre1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeantAutre1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['E95.2']= result;
} else if (Value('id').of(dirigeantAutre1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['E95.2'] = dirigeantAutre1.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeantAutre1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['E95.3'] = dirigeantAutre1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeantAutre1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['E95.4'] = dirigeantAutre1.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
occurrence['E96']= dirigeantAutre1.civilitePersonnePhysique.personneLieePPNationaliteGerant;
occurrencesIDE.push(occurrence);
occurrence = {};
}

// Fondé de pouvoir 2	

if (Value('id').of(dirigeantAutre2.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre2.personneLieeQualiteBis).eq('66')) {

occurrence['E91.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
}
occurrence['E92.1'] = dirigeantAutre2.civilitePersonnePhysique.personneLieePersonnePouvoirLimiteEtablissementOui ? "P" : "S";
occurrence['E93.2'] = dirigeantAutre2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (dirigeantAutre2.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['E93.4'] = dirigeantAutre2.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for (i = 0; i < dirigeantAutre2.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre2.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['E93.3']= prenoms;
if (Value('id').of(dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.3']= dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['E94.3']= result;
}
if (dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['E94.5']= dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['E94.6']          = monId1;  
} 
if (dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['E94.7']= dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['E94.8']= dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['E94.10']= dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['E94.11']          = monId1;  
} 
occurrence['E94.12']= dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.14']= dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
if(dirigeantAutre2.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre2.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['E95.1']= date;
}
if (not Value('id').of(dirigeantAutre2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeantAutre2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['E95.2']= result;
} else if (Value('id').of(dirigeantAutre2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['E95.2'] = dirigeantAutre2.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeantAutre2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['E95.3'] = dirigeantAutre2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeantAutre2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['E95.4'] = dirigeantAutre2.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
occurrence['E96']= dirigeantAutre2.civilitePersonnePhysique.personneLieePPNationaliteGerant;
occurrencesIDE.push(occurrence);
occurrence = {};
}

// Fondé de pouvoir 3	

if (Value('id').of(dirigeantAutre3.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre3.personneLieeQualiteBis).eq('66')) {

occurrence['E91.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
}
occurrence['E92.1'] = dirigeantAutre3.civilitePersonnePhysique.personneLieePersonnePouvoirLimiteEtablissementOui ? "P" : "S";
occurrence['E93.2'] = dirigeantAutre3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (dirigeantAutre3.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['E93.4'] = dirigeantAutre3.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for (i = 0; i < dirigeantAutre3.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre3.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['E93.3']= prenoms;
if (Value('id').of(dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.3']= dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['E94.3']= result;
}
if (dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['E94.5']= dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['E94.6']          = monId1;  
} 
if (dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['E94.7']= dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['E94.8']= dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['E94.10']= dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['E94.11']          = monId1;  
} 
occurrence['E94.12']= dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.14']= dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
if(dirigeantAutre3.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre3.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['E95.1']= date;
}
if (not Value('id').of(dirigeantAutre3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeantAutre3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['E95.2']= result;
} else if (Value('id').of(dirigeantAutre3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['E95.2'] = dirigeantAutre3.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeantAutre3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['E95.3'] = dirigeantAutre3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeantAutre3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['E95.4'] = dirigeantAutre3.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
occurrence['E96']= dirigeantAutre3.civilitePersonnePhysique.personneLieePPNationaliteGerant;
occurrencesIDE.push(occurrence);
occurrence = {};
}

// Fondé de pouvoir 4	

if (Value('id').of(dirigeantAutre4.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre4.personneLieeQualiteBis).eq('66')) {

occurrence['E91.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
}
occurrence['E92.1'] = dirigeantAutre4.civilitePersonnePhysique.personneLieePersonnePouvoirLimiteEtablissementOui ? "P" : "S";
occurrence['E93.2'] = dirigeantAutre4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (dirigeantAutre4.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['E93.4'] = dirigeantAutre4.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for (i = 0; i < dirigeantAutre4.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre4.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['E93.3']= prenoms;
if (Value('id').of(dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.3']= dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['E94.3']= result;
}
if (dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['E94.5']= dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['E94.6']          = monId1;  
} 
if (dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['E94.7']= dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['E94.8']= dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['E94.10']= dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['E94.11']          = monId1;  
} 
occurrence['E94.12']= dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.14']= dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
if(dirigeantAutre4.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre4.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['E95.1']= date;
}
if (not Value('id').of(dirigeantAutre4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeantAutre4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['E95.2']= result;
} else if (Value('id').of(dirigeantAutre4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['E95.2'] = dirigeantAutre4.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeantAutre4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['E95.3'] = dirigeantAutre4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeantAutre4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['E95.4'] = dirigeantAutre4.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
occurrence['E96']= dirigeantAutre4.civilitePersonnePhysique.personneLieePPNationaliteGerant;
occurrencesIDE.push(occurrence);
occurrence = {};
}

// Fondé de pouvoir 5	

if (Value('id').of(dirigeantAutre5.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre5.personneLieeQualiteBis).eq('66')) {

occurrence['E91.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
}
occurrence['E92.1'] = dirigeantAutre5.civilitePersonnePhysique.personneLieePersonnePouvoirLimiteEtablissementOui ? "P" : "S";
occurrence['E93.2'] = dirigeantAutre5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (dirigeantAutre5.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['E93.4'] = dirigeantAutre5.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for (i = 0; i < dirigeantAutre5.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre5.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['E93.3']= prenoms;
if (Value('id').of(dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.3']= dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['E94.3']= result;
}
if (dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['E94.5']= dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['E94.6']          = monId1;  
} 
if (dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['E94.7']= dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['E94.8']= dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['E94.10']= dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['E94.11']          = monId1;  
} 
occurrence['E94.12']= dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.14']= dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
if(dirigeantAutre5.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre5.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['E95.1']= date;
}
if (not Value('id').of(dirigeantAutre5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeantAutre5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['E95.2']= result;
} else if (Value('id').of(dirigeantAutre5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['E95.2'] = dirigeantAutre5.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeantAutre5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['E95.3'] = dirigeantAutre5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeantAutre5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['E95.4'] = dirigeantAutre5.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
occurrence['E96']= dirigeantAutre5.civilitePersonnePhysique.personneLieePPNationaliteGerant;
occurrencesIDE.push(occurrence);
occurrence = {};
}

// Fondé de pouvoir 6	

if (Value('id').of(dirigeantAutre6.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre6.personneLieeQualiteBis).eq('66')) {

occurrence['E91.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
}
occurrence['E92.1'] = dirigeantAutre6.civilitePersonnePhysique.personneLieePersonnePouvoirLimiteEtablissementOui ? "P" : "S";
occurrence['E93.2'] = dirigeantAutre6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (dirigeantAutre6.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['E93.4'] = dirigeantAutre6.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for (i = 0; i < dirigeantAutre6.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre6.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['E93.3']= prenoms;
if (Value('id').of(dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.3']= dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['E94.3']= result;
}
if (dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['E94.5']= dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['E94.6']          = monId1;  
} 
if (dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['E94.7']= dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['E94.8']= dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['E94.10']= dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['E94.11']          = monId1;  
} 
occurrence['E94.12']= dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.14']= dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
if(dirigeantAutre6.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre6.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['E95.1']= date;
}
if (not Value('id').of(dirigeantAutre6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeantAutre6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['E95.2']= result;
} else if (Value('id').of(dirigeantAutre6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['E95.2'] = dirigeantAutre6.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeantAutre6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['E95.3'] = dirigeantAutre6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeantAutre6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['E95.4'] = dirigeantAutre6.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
occurrence['E96']= dirigeantAutre6.civilitePersonnePhysique.personneLieePPNationaliteGerant;
occurrencesIDE.push(occurrence);
occurrence = {};
}

// Fondé de pouvoir 7	

if (Value('id').of(dirigeantAutre7.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre7.personneLieeQualiteBis).eq('66')) {

occurrence['E91.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
}
occurrence['E92.1'] = dirigeantAutre7.civilitePersonnePhysique.personneLieePersonnePouvoirLimiteEtablissementOui ? "P" : "S";
occurrence['E93.2'] = dirigeantAutre7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (dirigeantAutre7.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['E93.4'] = dirigeantAutre7.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for (i = 0; i < dirigeantAutre7.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre7.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['E93.3']= prenoms;
if (Value('id').of(dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.3']= dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['E94.3']= result;
}
if (dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['E94.5']= dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['E94.6']          = monId1;  
} 
if (dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['E94.7']= dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['E94.8']= dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['E94.10']= dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['E94.11']          = monId1;  
} 
occurrence['E94.12']= dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.14']= dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
if(dirigeantAutre7.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre7.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['E95.1']= date;
}
if (not Value('id').of(dirigeantAutre7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeantAutre7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['E95.2']= result;
} else if (Value('id').of(dirigeantAutre7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['E95.2'] = dirigeantAutre7.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeantAutre7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['E95.3'] = dirigeantAutre7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeantAutre7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['E95.4'] = dirigeantAutre7.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
occurrence['E96']= dirigeantAutre7.civilitePersonnePhysique.personneLieePPNationaliteGerant;
occurrencesIDE.push(occurrence);
occurrence = {};
}

// Fondé de pouvoir 8	

if (Value('id').of(dirigeantAutre8.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre8.personneLieeQualiteBis).eq('66')) {

occurrence['E91.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
}
occurrence['E92.1'] = dirigeantAutre8.civilitePersonnePhysique.personneLieePersonnePouvoirLimiteEtablissementOui ? "P" : "S";
occurrence['E93.2'] = dirigeantAutre8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (dirigeantAutre8.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['E93.4'] = dirigeantAutre8.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for (i = 0; i < dirigeantAutre8.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre8.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['E93.3']= prenoms;
if (Value('id').of(dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.3']= dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['E94.3']= result;
}
if (dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['E94.5']= dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['E94.6']          = monId1;  
} 
if (dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['E94.7']= dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['E94.8']= dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['E94.10']= dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['E94.11']          = monId1;  
} 
occurrence['E94.12']= dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.14']= dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
if(dirigeantAutre8.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre8.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['E95.1']= date;
}
if (not Value('id').of(dirigeantAutre8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeantAutre8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['E95.2']= result;
} else if (Value('id').of(dirigeantAutre8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['E95.2'] = dirigeantAutre8.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeantAutre8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['E95.3'] = dirigeantAutre8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeantAutre8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['E95.4'] = dirigeantAutre8.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
occurrence['E96']= dirigeantAutre8.civilitePersonnePhysique.personneLieePPNationaliteGerant;
occurrencesIDE.push(occurrence);
occurrence = {};
}

// Fondé de pouvoir 9	

if (Value('id').of(dirigeantAutre9.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre9.personneLieeQualiteBis).eq('66')) {

occurrence['E91.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
}
occurrence['E92.1'] = dirigeantAutre9.civilitePersonnePhysique.personneLieePersonnePouvoirLimiteEtablissementOui ? "P" : "S";
occurrence['E93.2'] = dirigeantAutre9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (dirigeantAutre9.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['E93.4'] = dirigeantAutre9.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for (i = 0; i < dirigeantAutre9.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre9.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['E93.3']= prenoms;
if (Value('id').of(dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.3']= dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['E94.3']= result;
}
if (dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['E94.5']= dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['E94.6']          = monId1;  
} 
if (dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['E94.7']= dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['E94.8']= dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['E94.10']= dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['E94.11']          = monId1;  
} 
occurrence['E94.12']= dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.14']= dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
if(dirigeantAutre9.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre9.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['E95.1']= date;
}
if (not Value('id').of(dirigeantAutre9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeantAutre9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['E95.2']= result;
} else if (Value('id').of(dirigeantAutre9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['E95.2'] = dirigeantAutre9.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeantAutre9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['E95.3'] = dirigeantAutre9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeantAutre9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['E95.4'] = dirigeantAutre9.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
occurrence['E96']= dirigeantAutre9.civilitePersonnePhysique.personneLieePPNationaliteGerant;
occurrencesIDE.push(occurrence);
occurrence = {};
}

// Fondé de pouvoir 10	

if (Value('id').of(dirigeantAutre10.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre10.personneLieeQualiteBis).eq('66')) {

occurrence['E91.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
}
occurrence['E92.1'] = dirigeantAutre10.civilitePersonnePhysique.personneLieePersonnePouvoirLimiteEtablissementOui ? "P" : "S";
occurrence['E93.2'] = dirigeantAutre10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (dirigeantAutre10.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['E93.4'] = dirigeantAutre10.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for (i = 0; i < dirigeantAutre10.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre10.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['E93.3']= prenoms;
if (Value('id').of(dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.3']= dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['E94.3']= result;
}
if (dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['E94.5']= dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['E94.6']          = monId1;  
} 
if (dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['E94.7']= dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['E94.8']= dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['E94.10']= dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['E94.11']          = monId1;  
} 
occurrence['E94.12']= dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.14']= dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
if(dirigeantAutre10.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre10.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['E95.1']= date;
}
if (not Value('id').of(dirigeantAutre10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeantAutre10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['E95.2']= result;
} else if (Value('id').of(dirigeantAutre10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['E95.2'] = dirigeantAutre10.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeantAutre10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['E95.3'] = dirigeantAutre10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeantAutre10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['E95.4'] = dirigeantAutre10.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
occurrence['E96']= dirigeantAutre10.civilitePersonnePhysique.personneLieePPNationaliteGerant;
occurrencesIDE.push(occurrence);
occurrence = {};
}

// Fondé de pouvoir 11	

if (Value('id').of(dirigeantAutre11.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre11.personneLieeQualiteBis).eq('66')) {

occurrence['E91.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
}
occurrence['E92.1'] = dirigeantAutre11.civilitePersonnePhysique.personneLieePersonnePouvoirLimiteEtablissementOui ? "P" : "S";
occurrence['E93.2'] = dirigeantAutre11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (dirigeantAutre11.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['E93.4'] = dirigeantAutre11.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for (i = 0; i < dirigeantAutre11.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre11.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['E93.3']= prenoms;
if (Value('id').of(dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.3']= dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['E94.3']= result;
}
if (dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['E94.5']= dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['E94.6']          = monId1;  
} 
if (dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['E94.7']= dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['E94.8']= dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['E94.10']= dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['E94.11']          = monId1;  
} 
occurrence['E94.12']= dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.14']= dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
if(dirigeantAutre11.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre11.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['E95.1']= date;
}
if (not Value('id').of(dirigeantAutre11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeantAutre11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['E95.2']= result;
} else if (Value('id').of(dirigeantAutre11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['E95.2'] = dirigeantAutre11.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeantAutre11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['E95.3'] = dirigeantAutre11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeantAutre11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['E95.4'] = dirigeantAutre11.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
occurrence['E96']= dirigeantAutre11.civilitePersonnePhysique.personneLieePPNationaliteGerant;
occurrencesIDE.push(occurrence);
occurrence = {};
}

// Fondé de pouvoir 12	

if (Value('id').of(dirigeantAutre12.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre12.personneLieeQualiteBis).eq('66')) {

occurrence['E91.1'] = "1";
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
} else if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['E91.2'] = date;
}
occurrence['E92.1'] = dirigeantAutre12.civilitePersonnePhysique.personneLieePersonnePouvoirLimiteEtablissementOui ? "P" : "S";
occurrence['E93.2'] = dirigeantAutre12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (dirigeantAutre12.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['E93.4'] = dirigeantAutre12.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for (i = 0; i < dirigeantAutre12.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre12.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['E93.3']= prenoms;
if (Value('id').of(dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.3']= dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['E94.3']= result;
}
if (dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['E94.5']= dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['E94.6']          = monId1;  
} 
if (dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['E94.7']= dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['E94.8']= dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['E94.10']= dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['E94.11']          = monId1;  
} 
occurrence['E94.12']= dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.13']= dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['E94.14']= dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
if(dirigeantAutre12.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeantAutre12.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['E95.1']= date;
}
if (not Value('id').of(dirigeantAutre12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeantAutre12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['E95.2']= result;
} else if (Value('id').of(dirigeantAutre12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['E95.2'] = dirigeantAutre12.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeantAutre12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
	occurrence['E95.3'] = dirigeantAutre12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeantAutre12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['E95.4'] = dirigeantAutre12.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
occurrence['E96']= dirigeantAutre12.civilitePersonnePhysique.personneLieePPNationaliteGerant;
occurrencesIDE.push(occurrence);
occurrence = {};
}

occurrencesIDE.forEach(function (value, i) {
	var idx = i+1;
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E91/E91.1']= value['E91.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E91/E91.2']= value['E91.2'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E92/E92.1']= value['E92.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E93/E93.2']= value['E93.2'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E93/E93.4']= value['E93.4'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E93/E93.3']= value['E93.3'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E94/E94.3']= value['E94.3'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E94/E94.5']= value['E94.5'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E94/E94.6']= value['E94.6'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E94/E94.7']= value['E94.7'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E94/E94.8']= value['E94.8'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E94/E94.10']= value['E94.10'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E94/E94.11']= value['E94.11'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E94/E94.12']= value['E94.12'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E94/E94.13']= value['E94.13'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E94/E94.14']= value['E94.14'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E95/E95.1']= value['E95.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E95/E95.2']= value['E95.2'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E95/E95.3']= value['E95.3'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E95/E95.4']= value['E95.4'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E96']= value['E96'];
});
}
 
 
// Call to the XML-REGENT generation WS 
var response = nash.service.request('${regent.baseUrl}/private/v1/xml-regent/generate/{regentVersion}/{authorityType}/{authorityId}', regentVersion, authorityType, authorityId) 
.dataType('application/json') // 
.accept('json') // 
.param('listTypeEvenement',eventRegent) 
//-->MINE-263 : Permettre à l'équipe FF de voir les problèmes de validation XSD et le regent sur studio
.continueOnError(true) //
.post(JSON.stringify(regentFields)); 

// Record the generated XML-REGENT 
//MOD déplacement du test de la réponse du premier ws
if (response != null && response.status == 200) { 
    var xmlRegentStr = response.asBytes(); 
    nash.record.saveFile("XML_REGENT.xml",xmlRegentStr); 

//debut de l'ajout
 if (authorityId2 != null) {
//MOD extraction du numéro de liasse du premeir regent généré
var numeroLiasse = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02');
numeroLiasse = JSON.parse(numeroLiasse);
_log.info('extracted numero de liasse is {}', numeroLiasse);
 regentFields['/REGENT-XML/Destinataire']= authorityId2;
 
 // Filtre regent greffe
var undefined;
// Groupe RFU
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U31']= undefined; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U32']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.1']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/RFU/U33/U33.2']= undefined;
// Groupe OIU
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OIU/U75']= undefined;
// Groupe GCS/ISS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/ISS/A10/A10.1']  = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/ISS/A10/A10.2'] = undefined;
// Groupe GCS/SNS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A21/A21.2']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A21/A21.3']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A21/A21.4']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A21/A21.6']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A22/A22.3']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A22/A22.4']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A24/A24.2']= undefined; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/SNS/A24/A24.3']= undefined;
// Groupe GCS/CAS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante/GCS/CAS/A42/A42.1']= undefined;
// Groupe SAE
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E83/E83.1']=undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E84']=undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E86']= undefined;

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS']  = undefined;


 //MOD modification de l'appel pour la génération du deuxième régent afi de prendre e compte le numéro de liasse du premier
 var response2 = nash.service.request('${regent.baseUrl}/private/v1/xml-regent/generate/{regentVersion}/{authorityType}/{authorityId}', regentVersion, authorityType2, authorityId2) 
.dataType('application/json') // 
.accept('json') // 
.param('listTypeEvenement',eventRegent) 
.param('liasseNumber', numeroLiasse.C02) 
.continueOnError(true)
.post(JSON.stringify(regentFields)); 

	//Début de l'ajout
	//MOD modifier reponse en response2
	if (response2 != null && response2.status == 200) { 
		var xmlRegentStr = response2.asBytes(); 
		nash.record.saveFile("XML_REGENT2.xml",xmlRegentStr);
	}else{
		
		var returnedResponse = response2.asObject();
		_log.info("Call ws regent returned errors  {}", returnedResponse);
		
		
		var regent2 = returnedResponse['regent'];
		var errors2 = returnedResponse['errors']; 
		nash.record.saveFile("XML_REGENT2.xml",regent2.getBytes()); 
		nash.record.saveFile("XML_VALIDATION_ERRORS_2.xml",errors2.getBytes()); 
		
		//-->MINE-263 : Permettre à l'équipe FF de voir les problèmes de validation XSD et le regent sur studio
		
		return spec.create({
		id : 'xmlGenerationConfirmation',
		label : "Xml Regent confirmation message",
		groups : [ spec.createGroup({
				id : 'Regent ',
				description : "Une erreur s'est produite lors de la génération du XML Regent.",
				data : [
				spec.createData({
					id: 'regent',
					label: "XML regent généré",
					type: 'Text',
					mandatory: true,
					value: regent2
				}),spec.createData({
					id: 'errors',
					label: "Erreurs de validations xsd",
					type: 'Text',
					mandatory: false,
					value: errors2
				}),spec.createData({
					id: 'blockingField',
					label: "xsd erroné",
					help:"Ce champs sert à arrêter le process du dossier pour que le support puisse le consulter",
					type: 'StringReadOnly',
					mandatory: true
				})]
			})]
		});
	}	
}
	return spec.create({
	id : 'xmlGenerationConfirmation',
	label : "Xml Regent confirmation message",
	groups : [ spec.createGroup({
		id : 'confirmationMessageOk',
		description : "Le fichier XML Regent a été généré et ajouté au dossier.",
		data : []
		}) ]
	});
}else{
	
	var returnedResponse = response.asObject();
	_log.info("Call ws regent returned errors  {}", returnedResponse);
	
	
	var regent = returnedResponse['regent'];
	var errors = returnedResponse['errors']; 
	nash.record.saveFile("XML_REGENT.xml",regent.getBytes()); 
	nash.record.saveFile("XML_VALIDATION_ERRORS.xml",errors.getBytes()); 
	
	
	return spec.create({
	id : 'xmlGenerationConfirmation',
	label : "Xml Regent confirmation message",
	groups : [ spec.createGroup({
		id : 'Regent ',
		description : "Une erreur s'est produite lors de la génération du XML Regent de la première autorité.",
		data : [
				spec.createData({
					id: 'regent',
					label: "XML regent généré",
					type: 'Text',
					mandatory: true,
					value: regent
				}),spec.createData({
					id: 'errors',
					label: "Erreurs de validations xsd",
					type: 'Text',
					mandatory: false,
					value: errors
				}),spec.createData({
					id: 'blockingField',
					label: "xsd erroné",
					help:"Ce champs sert à arrêter le process du dossier pour que le support puisse le consulter",
					type: 'StringReadOnly',
					mandatory: true
			})]
		}) ]
	});
}	