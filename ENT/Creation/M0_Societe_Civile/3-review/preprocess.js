function pad(s) { return (s < 10) ? '0' + s : s; }
var formFields = {};

// Cadre 1 - Forme juridique

var societe = $m0SCI.cadre1SocieteGroup.cadre1Identite;
var siege = $m0SCI.cadreEntrepriseSiegeGroup.cadreEntrepriseSiege; 

formFields['entreprise_formeJuridique_SCImmobiliere']                                         = Value('id').of(societe.entrepriseFormeJuridique).eq('6540') ? true : false;
formFields['entreprise_formeJuridique_SCConstructionVente']                                   = Value('id').of(societe.entrepriseFormeJuridique).eq('6541') ? true : false;
formFields['entreprise_formeJuridique_SCProfessionnelle']                                     = (Value('id').of(societe.entrepriseFormeJuridique).eq('6561') or
																								Value('id').of(societe.entrepriseFormeJuridique).eq('6562') or
																								Value('id').of(societe.entrepriseFormeJuridique).eq('6563') or
																								Value('id').of(societe.entrepriseFormeJuridique).eq('6564') or
																								Value('id').of(societe.entrepriseFormeJuridique).eq('6565') or 
																								Value('id').of(societe.entrepriseFormeJuridique).eq('6566') or 
																								Value('id').of(societe.entrepriseFormeJuridique).eq('6567') or 
																								Value('id').of(societe.entrepriseFormeJuridique).eq('6568') or
																								Value('id').of(societe.entrepriseFormeJuridique).eq('6569') or 
																								Value('id').of(societe.entrepriseFormeJuridique).eq('6571')	or 
																								Value('id').of(societe.entrepriseFormeJuridique).eq('6572')	or 
																								Value('id').of(societe.entrepriseFormeJuridique).eq('6573')	or
																								Value('id').of(societe.entrepriseFormeJuridique).eq('6574') or 
																								Value('id').of(societe.entrepriseFormeJuridique).eq('6575') or 
																								Value('id').of(societe.entrepriseFormeJuridique).eq('6576') or 
																								Value('id').of(societe.entrepriseFormeJuridique).eq('6577') or 
																								Value('id').of(societe.entrepriseFormeJuridique).eq('6578') or 
																								Value('id').of(societe.entrepriseFormeJuridique).eq('6585')) ? true : false;
formFields['entreprise_formeJuridique_SCMoyens']                                              = Value('id').of(societe.entrepriseFormeJuridique).eq('6589') ? true : false;
formFields['entreprise_formeJuridique_SCGroupementFoncier']                                   = Value('id').of(societe.entrepriseFormeJuridique).eq('6534') ? true : false;
formFields['entreprise_formeJuridique_GroupementForestier']                                   = Value('id').of(societe.entrepriseFormeJuridique).eq('6536') ? true : false;
formFields['entreprise_formeJuridique_CUMA']                                                  = Value('id').of(societe.entrepriseFormeJuridique).eq('6316') ? true : false;

if (Value('id').of(societe.entrepriseFormeJuridique).eq('6521') or
	Value('id').of(societe.entrepriseFormeJuridique).eq('6532') or
	Value('id').of(societe.entrepriseFormeJuridique).eq('6539') or 
	Value('id').of(societe.entrepriseFormeJuridique).eq('6542') or
	Value('id').of(societe.entrepriseFormeJuridique).eq('6599') or 
	Value('id').of(societe.entrepriseFormeJuridique).eq('6543') or 
	Value('id').of(societe.entrepriseFormeJuridique).eq('6544') or 
	Value('id').of(societe.entrepriseFormeJuridique).eq('6554') or 
	Value('id').of(societe.entrepriseFormeJuridique).eq('6558') or 
	Value('id').of(societe.entrepriseFormeJuridique).eq('6560')) {
formFields['entreprise_formeJuridique_autre']                       = true;
if (Value('id').of(societe.entrepriseFormeJuridique).eq('6599')) {
formFields['entreprise_formeJuridique_autre_preciser']              = societe.autreFormeJuridique;
} else {
formFields['entreprise_formeJuridique_autre_preciser']              = societe.entrepriseFormeJuridique;
}
}

// Cadre 2 - Déclaration relative à la société

formFields['entreprise_denomination']                                                       = societe.entrepriseDenomination;
formFields['entreprise_sigle']                                                              = societe.entrepriseSigle != null ? societe.entrepriseSigle : '';
formFields['entreprise_formeJuridique']                                                     = societe.entrepriseFormeJuridique;
formFields['entreprise_duree']                                                              = societe.entrepriseDuree + ' ' + "ans";
formFields['entreprise_capitalMontant']                                                     = societe.entrepriseCapitalMontant + ' ' + "euros";
formFields['entreprise_capitalVariableMinimum']                                             = societe.entrepriseCapitalVariable ? (societe.entrepriseCapitalVariableMinimum + ' ' + "euros") : '';
formFields['entreprise_societeMission']                                                     = societe.entrepriseMission ? true : false;

// Cadre 3 - Fusion

formFields['entreprise_fusionScission']                                                     = societe.entrepriseFusionScission ? true : false;

// Cadre 4 - Objet social

formFields['entreprise_activitesPrincipales']                                               = siege.entrepriseActivitesPrincipales;

// Cadre 5 - Effectif salarié

formFields['etablissement_effectifSalariePresence_non']                                           = siege.etablissementEffectifSalariePresenceOui ? false : true;

if (siege.etablissementEffectifSalariePresenceOui) {
formFields['etablissement_effectifSalariePresence_oui']                                           = true;
formFields['etablissement_effectifSalarieNombre']                                                 = siege.cadreEffectifSalarie.etablissementEffectifSalarieNombre;
}
formFields['etablissement_effectifSalarieEmbauchePremierSalarie_oui']                             = siege.cadreEffectifSalarie.etablissementEffectifSalarieEmbauchePremierSalarieOui ? true : false;
formFields['etablissement_effectifSalarieEmbauchePremierSalarie_non']                             = siege.etablissementEffectifSalariePresenceOui ? (siege.cadreEffectifSalarie.etablissementEffectifSalarieEmbauchePremierSalarieOui ? false : true) : false;

// Cadre 6 Adresse du siège

formFields['entreprise_adresseEntreprisePM_voie']                                           = (siege.cadreAdresseProfessionnelle.numeroAdresseSiege != null ? siege.cadreAdresseProfessionnelle.numeroAdresseSiege : '')
																							+ ' ' + (siege.cadreAdresseProfessionnelle.indiceAdresseSiege != null ? siege.cadreAdresseProfessionnelle.indiceAdresseSiege : '')
																							+ ' ' + (siege.cadreAdresseProfessionnelle.typeAdresseSiege != null ? siege.cadreAdresseProfessionnelle.typeAdresseSiege : '')
																							+ ' ' + siege.cadreAdresseProfessionnelle.voieAdresseSiege
																							+ ' ' + (siege.cadreAdresseProfessionnelle.complementAdresseSiege != null ? siege.cadreAdresseProfessionnelle.complementAdresseSiege : '')
																							+ ' ' + (siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege != null ? siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege : '');
formFields['entreprise_adresseEntreprisePM_codePostal']                                     = siege.cadreAdresseProfessionnelle.codePostalAdresseSiege;
formFields['entreprise_adresseEntreprisePM_commune']                                        = siege.cadreAdresseProfessionnelle.communeAdresseSiege;
formFields['entreprise_adresseEntreprisePM_communeAncienne']                                = siege.cadreAdresseProfessionnelle.communeAncienneAdresseSiege != null ? siege.cadreAdresseProfessionnelle.communeAncienneAdresseSiege : '';
formFields['entreprise_adresseEntreprisePMSituation_domicile']                              = Value('id').of(siege.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? true : false;

if (Value('id').of(siege.activiteLieu).eq('etablissementDomiciliationOui')) {
formFields['entreprise_adresseEntreprisePMSituation_domiciliation']                         = true;
formFields['entrepriseLiee_siren_entrepriseDomiciliation']                                  = siege.domiciliataireSiren.split(' ').join('');
formFields['entrepriseLiee_nom']                                                            = siege.domiciliataireNom;
}

// Cadre 7 - Evenements

formFields['etablissement_adresseEtablissementEstAdresseSiege_non']                           = siege.formaliteCreationAvecActivite ? (siege.etablissementPrincipalDifferentSiege ? true : false) : false;
formFields['etablissement_adresseEtablissementEstAdresseSiege_oui']                           = siege.formaliteCreationAvecActivite ? (siege.etablissementPrincipalDifferentSiege ? false : true) : false;
formFields['formalite_evenement']                                                             = siege.formaliteCreationAvecActivite ? false : true;

// Cadre 8 - Activité

var etablissement = $m0SCI.cadreEtablissementActiviteGroup.cadreEtablissementActivite ;

if (siege.formaliteCreationAvecActivite) {

if(etablissement.etablissementDateDebutActivite != null) {
	var dateTmp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_dateDebutActivite'] = date;
}
formFields['etablissement_activitePlusImportante']                                             = etablissement.etablissementActivitePlusImportante;
formFields['etablissement_AutreActivites']                                                     = etablissement.etablissementActivitesAutres!= null ? etablissement.etablissementActivitesAutres : '';
formFields['etablissement_activiteImmobiliere_location']                                      = Value('id').of(etablissement.questionActivitePrincipaleImmo).eq('etablissementActiviteImmobiliereLocation') ? true : false;
formFields['etablissement_activiteImmobiliere_promotion']                                     = Value('id').of(etablissement.questionActivitePrincipaleImmo).eq('etablissementActiviteImmobilierePromotion') ? true : false;
formFields['etablissement_activiteImmobiliere_realisation']                                   = Value('id').of(etablissement.questionActivitePrincipaleImmo).eq('etablissementActiviteImmobiliereRealisation') ? true : false;
formFields['etablissement_activiteImmobiliere_support']                                       = Value('id').of(etablissement.questionActivitePrincipaleImmo).eq('etablissementActiviteImmobiliereSupport') ? true : false;
formFields['etablissement_activiteImmobiliere_locationLogements']                             = Value('id').of(etablissement.questionActivitePrincipaleImmo).eq('etablissementActiviteImmobiliereLocation') ? (Value('id').of(etablissement.questionActivitePrincipaleLocation).eq('locationLogements') ? true : false) : false;
formFields['etablissement_activiteImmobiliere_locationTerrains']                              = Value('id').of(etablissement.questionActivitePrincipaleImmo).eq('etablissementActiviteImmobiliereLocation') ? (Value('id').of(etablissement.questionActivitePrincipaleLocation).eq('locationTerrains') ? true : false) : false;
formFields['etablissement_activiteImmobiliere_promotionBureaux']                              = Value('id').of(etablissement.questionActivitePrincipaleImmo).eq('etablissementActiviteImmobilierePromotion') ? (Value('id').of(etablissement.questionActivitePrincipalePromotion).eq('promotionBureau') ? true : false) : false;
formFields['etablissement_activiteImmobiliere_promotionLogements']                            = Value('id').of(etablissement.questionActivitePrincipaleImmo).eq('etablissementActiviteImmobilierePromotion') ? (Value('id').of(etablissement.questionActivitePrincipalePromotion).eq('promotionLogement') ? true : false) : false;
formFields['etablissement_activiteImmobiliere_promotionAutres']                               = Value('id').of(etablissement.questionActivitePrincipaleImmo).eq('etablissementActiviteImmobilierePromotion') ? (Value('id').of(etablissement.questionActivitePrincipalePromotion).eq('promotionAutres') ? true : false) : false;

// Cadre 9 - Adresse Etablissement

if (siege.etablissementPrincipalDifferentSiege) {
formFields['etablissement_adresse_voie']                                                          = (etablissement.cadreAdresseEtablissement.numeroAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.numeroAdresseEtablissement : '')
																									+ ' ' + (etablissement.cadreAdresseEtablissement.indiceAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.indiceAdresseEtablissement : '')
																									+ ' ' + (etablissement.cadreAdresseEtablissement.typeAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.typeAdresseEtablissement : '')
																									+ ' ' + etablissement.cadreAdresseEtablissement.voieAdresseEtablissement
																									+ ' ' + (etablissement.cadreAdresseEtablissement.complementAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.complementAdresseEtablissement : '')
																									+ ' ' + (etablissement.cadreAdresseEtablissement.distributionSpecialeAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.distributionSpecialeAdresseEtablissement : '');
formFields['etablissement_adresse_codePostal']                                                    = etablissement.cadreAdresseEtablissement.codePostalAdresseEtablissement;
formFields['etablissement_adresse_commune']                                                       = etablissement.cadreAdresseEtablissement.communeAdresseEtablissement;
formFields['etablissement_adresse_communeAncienne']                                               = etablissement.cadreAdresseEtablissement.communeAncienneAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.communeAncienneAdresseEtablissement : '';
}

// Cadre 10 - Origine du fonds

var origine = $m0SCI.cadre4OrigineFondsGroup.cadre4OrigineFonds ;

formFields['etablissement_origineFonds_creation']                                = Value('id').of(origine.etablissementOrigineFondsLiberal).eq('EtablissementOrigineFondsRepriseActiviteLiberale') ? false : true;

if (Value('id').of(origine.etablissementOrigineFondsLiberal).eq('EtablissementOrigineFondsRepriseActiviteLiberale')) {
formFields['etablissement_origineFonds_reprise']                                 = true;
formFields['reprisePartielle']                                                   = Value('id').of(origine.typeReprise).eq('reprisePartielle') ? origine.reprisePartielleActivite : '';
formFields['entrepriseLiee_siren_precedentExploitant']                           = origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant.split(' ').join('');
formFields['entrepriseLiee_entreprisePP_nomNaissance']      					 = origine.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant : '';
formFields['entrepriseLiee_entreprisePP_nomUsage']      					     = origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant : '';
formFields['entrepriseLiee_entreprisePP_prenom1']                                = origine.precedentExploitant.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant : '';
formFields['entrepriseLiee_entreprisePM_denomination']                           = origine.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant : '';
}
}

// Cadre 11 Dirigeant 1 (hors CUMA)

var dirigeant1 = $m0SCI.dirigeant1.cadreIdentiteDirigeant;
if (not Value('id').of(societe.entrepriseFormeJuridique).eq('6316')) {
formFields['personneLiee_qualite_associe1']                                                   = (Value('id').of(dirigeant1.personneLieeQualite).eq('75') or Value('id').of(dirigeant1.personneLieeQualite).eq('29')) ? true : false ;
formFields['personneLiee_qualite_gerant1']                                                    = (Value('id').of(dirigeant1.personneLieeQualite).eq('30') or Value('id').of(dirigeant1.personneLieeQualite).eq('29')) ? true : false;

formFields['personneLiee_PP_nomNaissance1']                                                      = dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant1.civilitePersonneMorale.personneLieePMDenomination) ;
formFields['personneLiee_PP_nomUsage1']                                                          = dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFields['personneLiee_PP_prenom1']                                = prenoms.toString();

if (dirigeant1.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant1.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFields['personneLiee_PP_dateNaissance1']          = date1;
}  
formFields['personneLiee_PP_lieuNaissanceCommune1']                                              = dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? (dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' + "(" +''+ dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : (dirigeant1.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? (dirigeant1.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : '');
formFields['personneLiee_PP_nationalite1']                                                       = dirigeant1.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant1.civilitePersonnePhysique.personneLieePPNationaliteGerant : '';
formFields['personneLiee_dirigeant1_adresse_voie']                                               = (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
																									+ ' ' + (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
																									+ ' ' + (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
																									+ ' ' + dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNomVoie
																									+ ' ' + (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
																									+ ' ' + (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFields['personneLiee_dirigeant1_adresse_codePostal']                                         = dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFields['personneLiee_dirigeant1_adresse_commune']                                            = dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays) ;
if (Value('id').of(dirigeant1.personnaliteJuridique).eq('personneMorale')) {
formFields['personneLiee_PM_formeJuridique1']                                                    =  dirigeant1.civilitePersonneMorale.personneLieePMFormeJuridique;
formFields['personneLiee_PM_lieuImmatriculation1']                                               = dirigeant1.civilitePersonneMorale.personneLieePMNumeroIdentification + ' ' + dirigeant1.civilitePersonneMorale.personneLieePMLieuImmatriculation;
}
}

// Cadre 12 Dirigeant 2

var dirigeant2 = $m0SCI.dirigeant2.cadreIdentiteDirigeant;
if (not Value('id').of(societe.entrepriseFormeJuridique).eq('6316')) {
formFields['personneLiee_qualite_associe2']                                                   = (Value('id').of(dirigeant2.personneLieeQualite).eq('75') or Value('id').of(dirigeant2.personneLieeQualite).eq('29')) ? true : false ;
formFields['personneLiee_qualite_gerant2']                                                    = (Value('id').of(dirigeant2.personneLieeQualite).eq('30') or Value('id').of(dirigeant2.personneLieeQualite).eq('29')) ? true : false;

formFields['personneLiee_PP_nomNaissance2']                                                      = dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant2.civilitePersonneMorale.personneLieePMDenomination) ;
formFields['personneLiee_PP_nomUsage2']                                                          = dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFields['personneLiee_PP_prenom2']                                = prenoms.toString();

if (dirigeant2.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant2.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFields['personneLiee_PP_dateNaissance2']          = date1;
}  
formFields['personneLiee_PP_lieuNaissanceCommune2']                                              = dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? (dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' + "(" +''+ dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : (dirigeant2.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? (dirigeant2.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : '');
formFields['personneLiee_PP_nationalite2']                                                       = dirigeant2.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant2.civilitePersonnePhysique.personneLieePPNationaliteGerant : '';
formFields['personneLiee_dirigeant2_adresse_voie']                                               = (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
																									+ ' ' + (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
																									+ ' ' + (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
																									+ ' ' + dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNomVoie
																									+ ' ' + (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
																									+ ' ' + (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFields['personneLiee_dirigeant2_adresse_codePostal']                                         = dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFields['personneLiee_dirigeant2_adresse_commune']                                            = dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays) ;
if (Value('id').of(dirigeant2.personnaliteJuridique).eq('personneMorale')) {
formFields['personneLiee_PM_formeJuridique2']                                                    = dirigeant2.civilitePersonneMorale.personneLieePMFormeJuridique;
formFields['personneLiee_PM_lieuImmatriculation2']                                               = dirigeant2.civilitePersonneMorale.personneLieePMNumeroIdentification + ' ' + dirigeant2.civilitePersonneMorale.personneLieePMLieuImmatriculation;
}
}

// Cadre 13 Dirigeant 3

var dirigeant3 = $m0SCI.dirigeant3.cadreIdentiteDirigeant;
if (Value('id').of(dirigeant2.personneLieeEtablissement).eq('oui')) {
formFields['personneLiee_qualite_associe3']                                                   = (Value('id').of(dirigeant3.personneLieeQualite).eq('75') or Value('id').of(dirigeant3.personneLieeQualite).eq('29')) ? true : false ;
formFields['personneLiee_qualite_gerant3']                                                    = (Value('id').of(dirigeant3.personneLieeQualite).eq('30') or Value('id').of(dirigeant3.personneLieeQualite).eq('29')) ? true : false;

formFields['personneLiee_PP_nomNaissance3']                                                      = dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant3.civilitePersonneMorale.personneLieePMDenomination) ;
formFields['personneLiee_PP_nomUsage3']                                                          = dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFields['personneLiee_PP_prenom3']                                = prenoms.toString();

if (dirigeant3.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant3.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFields['personneLiee_PP_dateNaissance3']          = date1;
}  
formFields['personneLiee_PP_lieuNaissanceCommune3']                                              = dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? (dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' + "(" +''+ dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : (dirigeant3.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? (dirigeant3.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : '');
formFields['personneLiee_PP_nationalite3']                                                       = dirigeant3.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant3.civilitePersonnePhysique.personneLieePPNationaliteGerant : '';
formFields['personneLiee_dirigeant3_adresse_voie']                                               = (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
																									+ ' ' + (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
																									+ ' ' + (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
																									+ ' ' + dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNomVoie
																									+ ' ' + (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
																									+ ' ' + (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFields['personneLiee_dirigeant3_adresse_codePostal']                                         = dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFields['personneLiee_dirigeant3_adresse_commune']                                            = dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays) ;
if (Value('id').of(dirigeant3.personnaliteJuridique).eq('personneMorale')) {
formFields['personneLiee_PM_formeJuridique3']                                                    = dirigeant3.civilitePersonneMorale.personneLieePMFormeJuridique;
formFields['personneLiee_PM_lieuImmatriculation3']                                               = dirigeant3.civilitePersonneMorale.personneLieePMNumeroIdentification + ' ' + dirigeant3.civilitePersonneMorale.personneLieePMLieuImmatriculation;
}
}

// Cadre 14 - Dirigeant 4

var dirigeant4 = $m0SCI.dirigeant4.cadreIdentiteDirigeant;
if (Value('id').of(dirigeant3.personneLieeEtablissement).eq('oui')) {
formFields['personneLiee_qualite_associe4']                                                   = (Value('id').of(dirigeant4.personneLieeQualite).eq('75') or Value('id').of(dirigeant4.personneLieeQualite).eq('29')) ? true : false ;
formFields['personneLiee_qualite_gerant4']                                                    = (Value('id').of(dirigeant4.personneLieeQualite).eq('30') or Value('id').of(dirigeant4.personneLieeQualite).eq('29')) ? true : false;

formFields['personneLiee_PP_nomNaissance4']                                                      = dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant4.civilitePersonneMorale.personneLieePMDenomination) ;
formFields['personneLiee_PP_nomUsage4']                                                          = dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFields['personneLiee_PP_prenom4']                                = prenoms.toString();

if (dirigeant4.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant4.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFields['personneLiee_PP_dateNaissance4']          = date1;
}  
formFields['personneLiee_PP_lieuNaissanceCommune4']                                              = dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? (dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' + "(" +''+ dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : (dirigeant4.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? (dirigeant4.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : '');
formFields['personneLiee_PP_nationalite4']                                                       = dirigeant4.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant4.civilitePersonnePhysique.personneLieePPNationaliteGerant : '';
formFields['personneLiee_dirigeant4_adresse_voie']                                               = (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
																									+ ' ' + (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
																									+ ' ' + (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
																									+ ' ' + dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNomVoie
																									+ ' ' + (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
																									+ ' ' + (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFields['personneLiee_dirigeant4_adresse_codePostal']                                         = dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFields['personneLiee_dirigeant4_adresse_commune']                                            = dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays) ;
if (Value('id').of(dirigeant4.personnaliteJuridique).eq('personneMorale')) {
formFields['personneLiee_PM_formeJuridique4']                                                    = dirigeant4.civilitePersonneMorale.personneLieePMFormeJuridique;
formFields['personneLiee_PM_lieuImmatriculation4']                                               = dirigeant4.civilitePersonneMorale.personneLieePMNumeroIdentification + ' ' + dirigeant4.civilitePersonneMorale.personneLieePMLieuImmatriculation;
}
}

// Cadre 15 - Dirigeant 5

var dirigeant5 = $m0SCI.dirigeant5.cadreIdentiteDirigeant;
if (Value('id').of(dirigeant4.personneLieeEtablissement).eq('oui')) {
formFields['personneLiee_qualite_associe5']                                                   = (Value('id').of(dirigeant5.personneLieeQualite).eq('75') or Value('id').of(dirigeant5.personneLieeQualite).eq('29')) ? true : false ;
formFields['personneLiee_qualite_gerant5']                                                    = (Value('id').of(dirigeant5.personneLieeQualite).eq('30') or Value('id').of(dirigeant5.personneLieeQualite).eq('29')) ? true : false;

formFields['personneLiee_PP_nomNaissance5']                                                      = dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant5.civilitePersonneMorale.personneLieePMDenomination) ;
formFields['personneLiee_PP_nomUsage5']                                                          = dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFields['personneLiee_PP_prenom5']                                = prenoms.toString();

if (dirigeant5.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant5.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFields['personneLiee_PP_dateNaissance5']          = date1;
}  
formFields['personneLiee_PP_lieuNaissanceCommune5']                                              = dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? (dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' + "(" +''+ dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : (dirigeant5.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? (dirigeant5.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : '');
formFields['personneLiee_PP_nationalite5']                                                       = dirigeant5.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant5.civilitePersonnePhysique.personneLieePPNationaliteGerant : '';
formFields['personneLiee_dirigeant5_adresse_voie']                                               = (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
																									+ ' ' + (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
																									+ ' ' + (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
																									+ ' ' + dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNomVoie
																									+ ' ' + (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
																									+ ' ' + (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFields['personneLiee_dirigeant5_adresse_codePostal']                                         = dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFields['personneLiee_dirigeant5_adresse_commune']                                            = dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays) ;
if (Value('id').of(dirigeant5.personnaliteJuridique).eq('personneMorale')) {
formFields['personneLiee_PM_formeJuridique5']                                                    = dirigeant5.civilitePersonneMorale.personneLieePMFormeJuridique;
formFields['personneLiee_PM_lieuImmatriculation5']                                               = dirigeant5.civilitePersonneMorale.personneLieePMNumeroIdentification + ' ' + dirigeant5.civilitePersonneMorale.personneLieePMLieuImmatriculation;
}
}

// Cadre 16 - Dirigeant 6

var dirigeant6 = $m0SCI.dirigeant6.cadreIdentiteDirigeant;
if (Value('id').of(dirigeant5.personneLieeEtablissement).eq('oui')) {
formFields['personneLiee_qualite_associe6']                                                   = (Value('id').of(dirigeant6.personneLieeQualite).eq('75') or Value('id').of(dirigeant6.personneLieeQualite).eq('29')) ? true : false ;
formFields['personneLiee_qualite_gerant6']                                                    = (Value('id').of(dirigeant6.personneLieeQualite).eq('30') or Value('id').of(dirigeant6.personneLieeQualite).eq('29')) ? true : false;

formFields['personneLiee_PP_nomNaissance6']                                                      = dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant6.civilitePersonneMorale.personneLieePMDenomination) ;
formFields['personneLiee_PP_nomUsage6']                                                          = dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFields['personneLiee_PP_prenom6']                                = prenoms.toString();

if (dirigeant6.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant6.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFields['personneLiee_PP_dateNaissance6']          = date1;
}  
formFields['personneLiee_PP_lieuNaissanceCommune6']                                              = dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? (dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' + "(" +''+ dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : (dirigeant6.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? (dirigeant6.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : '');
formFields['personneLiee_PP_nationalite6']                                                       = dirigeant6.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant6.civilitePersonnePhysique.personneLieePPNationaliteGerant : '';
formFields['personneLiee_dirigeant6_adresse_voie']                                               = (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
																									+ ' ' + (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant6.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
																									+ ' ' + (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant6.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
																									+ ' ' + dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNomVoie
																									+ ' ' + (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant6.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
																									+ ' ' + (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant6.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFields['personneLiee_dirigeant6_adresse_codePostal']                                         = dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFields['personneLiee_dirigeant6_adresse_commune']                                            = dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays) ;
if (Value('id').of(dirigeant6.personnaliteJuridique).eq('personneMorale')) {
formFields['personneLiee_PM_formeJuridique6']                                                    = dirigeant6.civilitePersonneMorale.personneLieePMFormeJuridique;
formFields['personneLiee_PM_lieuImmatriculation6']                                               = dirigeant6.civilitePersonneMorale.personneLieePMNumeroIdentification + ' ' + dirigeant6.civilitePersonneMorale.personneLieePMLieuImmatriculation;
}
}

// Cadre 17 - Dirigeant 7

var dirigeant7 = $m0SCI.dirigeant7.cadreIdentiteDirigeant;
if (Value('id').of(dirigeant6.personneLieeEtablissement).eq('oui')) {
formFields['personneLiee_qualite_associe7']                                                   = (Value('id').of(dirigeant7.personneLieeQualite).eq('75') or Value('id').of(dirigeant7.personneLieeQualite).eq('29')) ? true : false ;
formFields['personneLiee_qualite_gerant7']                                                    = (Value('id').of(dirigeant7.personneLieeQualite).eq('30') or Value('id').of(dirigeant7.personneLieeQualite).eq('29')) ? true : false;

formFields['personneLiee_PP_nomNaissance7']                                                      = dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant7.civilitePersonneMorale.personneLieePMDenomination) ;
formFields['personneLiee_PP_nomUsage7']                                                          = dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFields['personneLiee_PP_prenom7']                                = prenoms.toString();

if (dirigeant7.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant7.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFields['personneLiee_PP_dateNaissance7']          = date1;
}  
formFields['personneLiee_PP_lieuNaissanceCommune7']                                              = dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? (dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' + "(" +''+ dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : (dirigeant7.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? (dirigeant7.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : '');
formFields['personneLiee_PP_nationalite7']                                                       = dirigeant7.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant7.civilitePersonnePhysique.personneLieePPNationaliteGerant : '';
formFields['personneLiee_dirigeant7_adresse_voie']                                               = (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
																									+ ' ' + (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant7.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
																									+ ' ' + (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant7.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
																									+ ' ' + dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNomVoie
																									+ ' ' + (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant7.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
																									+ ' ' + (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant7.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFields['personneLiee_dirigeant7_adresse_codePostal']                                         = dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFields['personneLiee_dirigeant7_adresse_commune']                                            = dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays) ;
if (Value('id').of(dirigeant7.personnaliteJuridique).eq('personneMorale')) {
formFields['personneLiee_PM_formeJuridique7']                                                    = dirigeant7.civilitePersonneMorale.personneLieePMFormeJuridique;
formFields['personneLiee_PM_lieuImmatriculation7']                                               = dirigeant7.civilitePersonneMorale.personneLieePMNumeroIdentification + ' ' + dirigeant7.civilitePersonneMorale.personneLieePMLieuImmatriculation;
}
}
	

// Cadre 18 - Dirigeant 8

var dirigeant8 = $m0SCI.dirigeant8.cadreIdentiteDirigeant;
if (Value('id').of(dirigeant7.personneLieeEtablissement).eq('oui')) {
formFields['personneLiee_qualite_associe8']                                                   = (Value('id').of(dirigeant8.personneLieeQualite).eq('75') or Value('id').of(dirigeant8.personneLieeQualite).eq('29')) ? true : false ;
formFields['personneLiee_qualite_gerant8']                                                    = (Value('id').of(dirigeant8.personneLieeQualite).eq('30') or Value('id').of(dirigeant8.personneLieeQualite).eq('29')) ? true : false;

formFields['personneLiee_PP_nomNaissance8']                                                      = dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant8.civilitePersonneMorale.personneLieePMDenomination) ;
formFields['personneLiee_PP_nomUsage8']                                                          = dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFields['personneLiee_PP_prenom8']                                = prenoms.toString();

if (dirigeant8.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant8.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFields['personneLiee_PP_dateNaissance8']          = date1;
}  
formFields['personneLiee_PP_lieuNaissanceCommune8']                                              = dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? (dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' + "(" +''+ dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : (dirigeant8.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? (dirigeant8.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : '');
formFields['personneLiee_PP_nationalite8']                                                       = dirigeant8.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant8.civilitePersonnePhysique.personneLieePPNationaliteGerant : '';
formFields['personneLiee_dirigeant8_adresse_voie']                                               = (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
																									+ ' ' + (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant8.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
																									+ ' ' + (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant8.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
																									+ ' ' + dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNomVoie
																									+ ' ' + (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant8.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
																									+ ' ' + (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant8.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFields['personneLiee_dirigeant8_adresse_codePostal']                                         = dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFields['personneLiee_dirigeant8_adresse_commune']                                            = dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays) ;
if (Value('id').of(dirigeant8.personnaliteJuridique).eq('personneMorale')) {
formFields['personneLiee_PM_formeJuridique8']                                                    = dirigeant8.civilitePersonneMorale.personneLieePMFormeJuridique;
formFields['personneLiee_PM_lieuImmatriculation8']                                               = dirigeant8.civilitePersonneMorale.personneLieePMNumeroIdentification + ' ' + dirigeant8.civilitePersonneMorale.personneLieePMLieuImmatriculation;
}
}


// Cadre 19 Options fiscales

var fiscal = $m0SCI.cadre20optFiscGroup.cadre20optFisc ;

formFields['regimeFiscal_regimeImpositionBenefices_rF']                                       = (Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberale).eq('revenuFoncier') or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberaleBis).eq('revenuFoncier')) ? true : false;
formFields['regimeFiscal_regimeImpositionBenefices_dcBNC']                                    = (Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberale).eq('BNC') or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberaleBis).eq('BNC')) ? true : false;
if (Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberale).eq('BIC') or Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberaleBis).eq('BIC')) {
formFields['regimeFiscal_regimeImpositionBenefices_rsBIC']                                    = Value('id').of(fiscal.impositionBenefices.regimeFiscalReel).eq('regimeFiscalRegimeImpositionBeneficesRs') ? true : false;
formFields['regimeFiscal_regimeImpositionBenefices_rnBIC']                                    = Value('id').of(fiscal.impositionBenefices.regimeFiscalReel).eq('regimeFiscalRegimeImpositionBeneficesRn') ? true : false;
}
formFields['regimeFiscal_regimeImpositionBeneficesOptionsParticulieres_optionIS']             = Value('id').of(fiscal.impositionBenefices.regimeFiscalRegimeImpositionBeneficeLiberaleBis).eq('IS') ? true : false;

formFields['regimeFiscal_regimeImpositionTVA_rfTVA']                                              = (Value('id').of(fiscal.optionsTVA.regimeTVA1).eq('regimeFiscalRegimeImpositionTVARfTVA') or Value('id').of(fiscal.optionsTVA.regimeTVA2).eq('regimeFiscalRegimeImpositionTVARfTVA') or Value('id').of(fiscal.optionsTVA.regimeTVA3).eq('regimeFiscalRegimeImpositionTVARfTVA')) ? true : false;
formFields['regimeFiscal_regimeImpositionTVA_rsTVA']                                              = (Value('id').of(fiscal.optionsTVA.regimeTVA1).eq('regimeFiscalRegimeImpositionTVARsTVA') or Value('id').of(fiscal.optionsTVA.regimeTVA3).eq('regimeFiscalRegimeImpositionTVARsTVA')) ? true : false;
formFields['regimeFiscal_regimeImpositionTVA_mrTVA']                                              = Value('id').of(fiscal.optionsTVA.regimeTVA1).eq('regimeFiscalRegimeImpositionTVAMrTVA') ? true : false;
formFields['regimeFiscal_regimeImpositionTVA_rnTVA']                                              = (Value('id').of(fiscal.optionsTVA.regimeTVA2).eq('regimeFiscalRegimeImpositionTVARnTVA') or Value('id').of(fiscal.optionsTVA.regimeTVA3).eq('regimeFiscalRegimeImpositionTVARnTVA')) ? true : false;
formFields['regimeFiscal_regimeImpositionTVAOptionsParticulieres2']                               = fiscal.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres2 ? true : false;
formFields['regimeFiscal_regimeImpositionTVAOptionsParticulieres1']                               = fiscal.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres1 ? true : false;


// Cadre 20 Observations

var correspondance = $m0SCI.cadre10RensCompGroup.cadre10RensComp ;

formFields['formalite_observations']                                                    = correspondance.formaliteObservations;

// Cadres 21 Correspondance

formFields['numeroCadreDeclarationAdresse_renseignementComplementaire']                 =  Value('id').of(correspondance.adresseCorrespond).eq('siege') ? "6" : (Value('id').of(correspondance.adresseCorrespond).eq('etablissement') ? "9" : '');
if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
formFields['formalite_correspondanceNomVoie']											= correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance
																						+ ' ' + (correspondance.adresseCorrespondance.numeroRueAdresseCorrespondance != null ? correspondance.adresseCorrespondance.numeroRueAdresseCorrespondance : '')
																						+ ' ' + (correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance : '')
																						+ ' ' + (correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance : '')
																						+ ' ' + correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance;
formFields['formalite_correspondanceAdresseComplement']									= (correspondance.adresseCorrespondance.rueComplementAdresseCorrespondance != null ? correspondance.adresseCorrespondance.rueComplementAdresseCorrespondance : '')
																						+ ' ' + (correspondance.adresseCorrespondance.distriutionSpecialeVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.distriutionSpecialeVoieAdresseCorrespondance : '');
formFields['formalite_correspondanceAdresse_codePostal']                                = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal;
formFields['formalite_correspondanceAdresse_commune']									= correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune != null ? correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune : '';
}
formFields['formalite_telephone1']                                                      = (correspondance.infosSup.formaliteTelephone1 != null ? correspondance.infosSup.formaliteTelephone1 : '');
formFields['formalite_telephone2']                                                      = correspondance.infosSup.formaliteTelephone2;
formFields['formalite_courriel']                                                    = correspondance.infosSup.formaliteFaxCourriel != null ? correspondance.infosSup.formaliteFaxCourriel : (correspondance.infosSup.telecopie != null ? correspondance.infosSup.telecopie : '');

// Cadres 22 - Signature

var signataire = $m0SCI.cadre11SignatureGroup.cadre11Signature ;
var dirigeantAutre1 = $m0SCI.dirigeantAutre1.cadreIdentiteDirigeant;
var dirigeantAutre2 = $m0SCI.dirigeantAutre2.cadreIdentiteDirigeant;
var dirigeantAutre3 = $m0SCI.dirigeantAutre3.cadreIdentiteDirigeant;
var dirigeantAutre4 = $m0SCI.dirigeantAutre4.cadreIdentiteDirigeant;
var dirigeantAutre5 = $m0SCI.dirigeantAutre5.cadreIdentiteDirigeant;
var dirigeantAutre6 = $m0SCI.dirigeantAutre6.cadreIdentiteDirigeant;
var dirigeantAutre7 = $m0SCI.dirigeantAutre7.cadreIdentiteDirigeant;
var dirigeantAutre8 = $m0SCI.dirigeantAutre8.cadreIdentiteDirigeant;
var dirigeantAutre9 = $m0SCI.dirigeantAutre9.cadreIdentiteDirigeant;
var dirigeantAutre10 = $m0SCI.dirigeantAutre10.cadreIdentiteDirigeant;
var dirigeantAutre11 = $m0SCI.dirigeantAutre11.cadreIdentiteDirigeant;
var dirigeantAutre12 = $m0SCI.dirigeantAutre12.cadreIdentiteDirigeant;

formFields['leRepresentantLegal']                                                       = (dirigeant1.dirigeantSignataire or dirigeant2.dirigeantSignataire or dirigeant3.dirigeantSignataire or dirigeant4.dirigeantSignataire or dirigeant5.dirigeantSignataire or dirigeant6.dirigeantSignataire or dirigeant7.dirigeantSignataire or dirigeant8.dirigeantSignataire or dirigeantAutre1.dirigeantSignataire or dirigeantAutre2.dirigeantSignataire or dirigeantAutre3.dirigeantSignataire or dirigeantAutre4.dirigeantSignataire or dirigeantAutre5.dirigeantSignataire or dirigeantAutre6.dirigeantSignataire or dirigeantAutre7.dirigeantSignataire or dirigeantAutre8.dirigeantSignataire or dirigeantAutre9.dirigeantSignataire or dirigeantAutre10.dirigeantSignataire or dirigeantAutre11.dirigeantSignataire or dirigeantAutre12.dirigeantSignataire) ? true : false;
formFields['numeroDeclarationCadreRepresentantLegal']                                   = dirigeant1.dirigeantSignataire ? "11" : 
																						(dirigeant2.dirigeantSignataire ? "12" : 
																						(dirigeant3.dirigeantSignataire ? "13" : 
																						(dirigeant4.dirigeantSignataire ? "14" : 
																						(dirigeant5.dirigeantSignataire ? "15" : 
																						(dirigeant6.dirigeantSignataire ? "16" : 
																						(dirigeant7.dirigeantSignataire ? "17" : 
																						(dirigeant8.dirigeantSignataire ? "18" :
																						(dirigeantAutre1.dirigeantSignataire ? "2M0'" :
																						(dirigeantAutre2.dirigeantSignataire ? "3M0'" :
																						(dirigeantAutre3.dirigeantSignataire ? "4M0'" :
																						(dirigeantAutre4.dirigeantSignataire ? "5M0'" :
																						(dirigeantAutre5.dirigeantSignataire ? "6M0'" :
																						(dirigeantAutre6.dirigeantSignataire ? "7M0'" :
																						(dirigeantAutre7.dirigeantSignataire ? "2M0'" :
																						(dirigeantAutre8.dirigeantSignataire ? "3M0'" :
																						(dirigeantAutre9.dirigeantSignataire ? "4M0'" :
																						(dirigeantAutre10.dirigeantSignataire ? "5M0'" :
																						(dirigeantAutre11.dirigeantSignataire ? "6M0'" :
																						(dirigeantAutre12.dirigeantSignataire ? "7M0'" : '')))))))))))))))))));
																						
if (not dirigeant1.dirigeantSignataire and not dirigeant2.dirigeantSignataire and not dirigeant3.dirigeantSignataire and not dirigeant4.dirigeantSignataire and not dirigeant5.dirigeantSignataire and not dirigeant6.dirigeantSignataire and not dirigeant7.dirigeantSignataire and not dirigeant8.dirigeantSignataire and not dirigeantAutre1.dirigeantSignataire and not dirigeantAutre2.dirigeantSignataire and not dirigeantAutre3.dirigeantSignataire and not dirigeantAutre4.dirigeantSignataire and not dirigeantAutre5.dirigeantSignataire and not dirigeantAutre6.dirigeantSignataire and not dirigeantAutre7.dirigeantSignataire and not dirigeantAutre8.dirigeantSignataire and not dirigeantAutre9.dirigeantSignataire and not dirigeantAutre10.dirigeantSignataire and not dirigeantAutre11.dirigeantSignataire and not dirigeantAutre12.dirigeantSignataire) {
formFields['leMandataire']                                                              =  true;
formFields['mandataireAyantProcuration_Nom']						           			= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFields['mandataireAyantProcuration_adresseVoie']                                    = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																						+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																						+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																						+ ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																						+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																						+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');
formFields['mandataireAyantProcuration_adresseCP']	    								= (signataire.adresseMandataire.codePostalMandataire != null ? signataire.adresseMandataire.codePostalMandataire : '');
formFields['mandataireAyantProcuration_adresseCommune']									= (signataire.adresseMandataire.villeAdresseMandataire != null ? signataire.adresseMandataire.villeAdresseMandataire : '');
}
formFields['formalite_signatureLieu']                                                   = signataire.formaliteSignatureLieu;
if (signataire.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFields['formalite_signatureDate']          = date1;
}  
// A conditionner selon l'activité
formFields['nombreIntercalaireM0Prime']                                                 = dirigeantAutre6.autrePersonneLieeEtablissement ? "1" : ((Value('id').of(societe.entrepriseFormeJuridique).eq('6316') or societe.entrepriseFusionScission or dirigeant2.autrePersonneLieeEtablissement or dirigeant3.autrePersonneLieeEtablissement or dirigeant4.autrePersonneLieeEtablissement or dirigeant5.autrePersonneLieeEtablissement or dirigeant6.autrePersonneLieeEtablissement or dirigeant7.autrePersonneLieeEtablissement or dirigeant8.autrePersonneLieeEtablissement) ? "1" : "0");

var socialDirigeant = [];

['dirigeant1', 'dirigeant2', 'dirigeant3', 'dirigeant4', 'dirigeant5', 'dirigeant6', 'dirigeant7', 'dirigeant8', 'dirigeantAutre1', 'dirigeantAutre2', 'dirigeantAutre3', 'dirigeantAutre4', 'dirigeantAutre5', 'dirigeantAutre6', 'dirigeantAutre7', 'dirigeantAutre8', 'dirigeantAutre9', 'dirigeantAutre10', 'dirigeantAutre11', 'dirigeantAutre12'].forEach(function(tns){
	if (null != $m0SCI[tns] and $m0SCI[tns].cadreIdentiteDirigeant.cadre7DeclarationSociale.voletSocialNumeroSecuriteSocialTNS != null) {
		socialDirigeant.push($m0SCI[tns]);
	}
});
formFields['nombreVoletTNS']                                                            = (socialDirigeant.length > 19) ? "20" : 
																					   ((socialDirigeant.length > 18) ? "19" : 
																					   ((socialDirigeant.length > 17) ? "18" : 
																					   ((socialDirigeant.length > 16) ? "17" : 
																					   ((socialDirigeant.length > 15) ? "16" : 
																					   ((socialDirigeant.length > 14) ? "15" : 
																					   ((socialDirigeant.length > 13) ? "14" : 
																					   ((socialDirigeant.length > 12) ? "13" : 
																					   ((socialDirigeant.length > 11) ? "12" : 
																					   ((socialDirigeant.length > 10) ? "11" :
																					   ((socialDirigeant.length > 9) ? "10" : 
																					   ((socialDirigeant.length > 8) ? "9" : 
																					   ((socialDirigeant.length > 7) ? "8" : 
																					   ((socialDirigeant.length > 6) ? "7" : 
																					   ((socialDirigeant.length > 5) ? "6" : 
																					   ((socialDirigeant.length > 4) ? "5" : 
																					   ((socialDirigeant.length > 3) ? "4" : 
																					   ((socialDirigeant.length > 2) ? "3" : 
																					   ((socialDirigeant.length > 1) ? "2" : 
																					   ((socialDirigeant.length > 0) ? "1" : "0")))))))))))))))))));
formFields['nombreIntercalaireMBE']                                                     = signataire.nombreMBE.getLabel();
formFields['signature']                                                                 = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce";


// M0 prime 1

var formFieldsPers1 = {}

// Cadre 1

formFieldsPers1['numeroIntercalaire_M0sc']                   = "01";
formFieldsPers1['entrepriseDenomination_M0sc']               = societe.entrepriseDenomination;
formFieldsPers1['entrepriseFormeJuridique_M0sc']             = societe.entrepriseFormeJuridique;

// Cadre 2 - Autre Dirigeant 1M0

if (Value('id').of(societe.entrepriseFormeJuridique).eq('6316') or dirigeant2.autrePersonneLieeEtablissement or dirigeant3.autrePersonneLieeEtablissement or dirigeant4.autrePersonneLieeEtablissement or dirigeant5.autrePersonneLieeEtablissement or dirigeant6.autrePersonneLieeEtablissement or dirigeant7.autrePersonneLieeEtablissement or dirigeant8.autrePersonneLieeEtablissement) {
formFieldsPers1['personneLieeQualiteGerantPrime[0]']           = (Value('id').of(dirigeantAutre1.personneLieeQualite).eq('30') or Value('id').of(dirigeantAutre1.personneLieeQualite).eq('29')) ? true : false;
formFieldsPers1['personneLieeQualiteAssociePrime[0]']          = (Value('id').of(dirigeantAutre1.personneLieeQualite).eq('75') or Value('id').of(dirigeantAutre1.personneLieeQualite).eq('29')) ? true : false;

if (not Value('id').of(dirigeantAutre1.personneLieeQualite).eq('30') and not Value('id').of(dirigeantAutre1.personneLieeQualite).eq('29') and not Value('id').of(dirigeantAutre1.personneLieeQualite).eq('75')) {
formFieldsPers1['personneLieeQualiteAutrePrime[0]']            = true;
formFieldsPers1['personneLieeQualiteLibellePrime[0]']          = dirigeantAutre1.personneLieeQualite != null ? dirigeantAutre1.personneLieeQualite : dirigeantAutre1.personneLieeQualiteBis;
}
formFieldsPers1['personneLieePPNomNaissancePrime[0]']          = dirigeantAutre1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeantAutre1.civilitePersonneMorale.personneLieePMDenomination);
formFieldsPers1['personneLieePPNomUsagePrime[0]']              = dirigeantAutre1.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre1.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '');

if (dirigeantAutre1.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre1.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre1.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre1.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['personneLieePPPrenomPrime[0]']                                                      = prenoms.toString();
} else if (dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['personneLieePPPrenomPrime[0]']                                                      = prenoms.toString();
}

if (dirigeantAutre1.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeantAutre1.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['personneLieePPDateNaissancePrime[0]']          = date1;
}  else if (dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['personneLieePPDateNaissancePrime[0]']          = date2;
}
formFieldsPers1['personneLieePPLieuNaissancePrime[0]'] = dirigeantAutre1.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ?
													(dirigeantAutre1.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant 
													+ ' ' + "(" + '' + dirigeantAutre1.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() + '' + ")") :	
													(dirigeantAutre1.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
													(dirigeantAutre1.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille 
													+ ' / ' + dirigeantAutre1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) :	
													(dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ?
													(dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant 
													+ ' ' + "(" + '' + dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId()	+ '' + ")") :	
													(dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
													(dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille 
													+ ' / ' + dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers1['personneLieePPNationalitePrime[0]']     = dirigeantAutre1.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeantAutre1.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
formFieldsPers1['personneLieeAdresseVoie[0]']    		= (dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
													+ ' ' + (dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
													+ ' ' + (dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
													+ ' ' + dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseNomVoie
													+ ' ' + (dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
													+ ' ' + (dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers1['personneLieeAdresseCP[0]']         		= dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers1['personneLieeAdresseCommune[0]']         = dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeantAutre1.cadreAdresseDirigeant.dirigeantAdressePays);
formFieldsPers1['personneLieePMformeJuridique[0]']       = dirigeantAutre1.civilitePersonneMorale.personneLieePMFormeJuridique != null ? dirigeantAutre1.civilitePersonneMorale.personneLieePMFormeJuridique : '';
formFieldsPers1['personneLieePMLieuImmat[0]']            = dirigeantAutre1.civilitePersonneMorale.personneLieePMLieuImmatriculation != null ? dirigeantAutre1.civilitePersonneMorale.personneLieePMLieuImmatriculation : '';
formFieldsPers1['personneLieePMNumeroImmat[0]']          = dirigeantAutre1.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeantAutre1.civilitePersonneMorale.personneLieePMNumeroIdentification : '';
}

// Cadre 3 - Autre dirigeant 2M0

if (Value('id').of(societe.entrepriseFormeJuridique).eq('6316') or dirigeantAutre1.autrePersonneLieeEtablissement) {
formFieldsPers1['personneLieeQualiteGerantPrime[1]']           = (Value('id').of(dirigeantAutre2.personneLieeQualite).eq('30') or Value('id').of(dirigeantAutre2.personneLieeQualite).eq('29')) ? true : false;
formFieldsPers1['personneLieeQualiteAssociePrime[1]']          = (Value('id').of(dirigeantAutre2.personneLieeQualite).eq('75') or Value('id').of(dirigeantAutre2.personneLieeQualite).eq('29')) ? true : false;

if (not Value('id').of(dirigeantAutre2.personneLieeQualite).eq('30') and not Value('id').of(dirigeantAutre2.personneLieeQualite).eq('29') and not Value('id').of(dirigeantAutre2.personneLieeQualite).eq('75')) {
formFieldsPers1['personneLieeQualiteAutrePrime[1]']            = true;
formFieldsPers1['personneLieeQualiteLibellePrime[1]']          = dirigeantAutre2.personneLieeQualite != null ? dirigeantAutre2.personneLieeQualite : dirigeantAutre2.personneLieeQualiteBis;
}
formFieldsPers1['personneLieePPNomNaissancePrime[1]']          = dirigeantAutre2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeantAutre2.civilitePersonneMorale.personneLieePMDenomination);
formFieldsPers1['personneLieePPNomUsagePrime[1]']              = dirigeantAutre2.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre2.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '');

if (dirigeantAutre2.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre2.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre2.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre2.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['personneLieePPPrenomPrime[1]']                                                      = prenoms.toString();
} else if (dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['personneLieePPPrenomPrime[1]']                                                      = prenoms.toString();
}

if (dirigeantAutre2.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeantAutre2.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['personneLieePPDateNaissancePrime[1]']          = date1;
}  else if (dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['personneLieePPDateNaissancePrime[1]']          = date2;
}
formFieldsPers1['personneLieePPLieuNaissancePrime[1]'] = dirigeantAutre2.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ?
													(dirigeantAutre2.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant 
													+ ' ' + "(" + '' + dirigeantAutre2.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() + '' + ")") :	
													(dirigeantAutre2.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
													(dirigeantAutre2.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille 
													+ ' / ' + dirigeantAutre2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) :	
													(dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ?
													(dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant 
													+ ' ' + "(" + '' + dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId()	+ '' + ")") :	
													(dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
													(dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille 
													+ ' / ' + dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers1['personneLieePPNationalitePrime[1]']     = dirigeantAutre2.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeantAutre2.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
formFieldsPers1['personneLieeAdresseVoie[1]']    		= (dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
													+ ' ' + (dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
													+ ' ' + (dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
													+ ' ' + dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseNomVoie
													+ ' ' + (dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
													+ ' ' + (dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers1['personneLieeAdresseCP[1]']         		= dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers1['personneLieeAdresseCommune[1]']         = dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeantAutre2.cadreAdresseDirigeant.dirigeantAdressePays);
formFieldsPers1['personneLieePMformeJuridique[1]']       = dirigeantAutre2.civilitePersonneMorale.personneLieePMFormeJuridique != null ? dirigeantAutre2.civilitePersonneMorale.personneLieePMFormeJuridique : '';
formFieldsPers1['personneLieePMLieuImmat[1]']            = dirigeantAutre2.civilitePersonneMorale.personneLieePMLieuImmatriculation != null ? dirigeantAutre2.civilitePersonneMorale.personneLieePMLieuImmatriculation : '';
formFieldsPers1['personneLieePMNumeroImmat[1]']          = dirigeantAutre2.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeantAutre2.civilitePersonneMorale.personneLieePMNumeroIdentification : '';
}

// Cadre 4 - Autre dirigeant 3M0

if (dirigeantAutre2.autrePersonneLieeEtablissement) {
formFieldsPers1['personneLieeQualiteGerantPrime[2]']           = (Value('id').of(dirigeantAutre3.personneLieeQualite).eq('30') or Value('id').of(dirigeantAutre3.personneLieeQualite).eq('29')) ? true : false;
formFieldsPers1['personneLieeQualiteAssociePrime[2]']          = (Value('id').of(dirigeantAutre3.personneLieeQualite).eq('75') or Value('id').of(dirigeantAutre3.personneLieeQualite).eq('29')) ? true : false;

if (not Value('id').of(dirigeantAutre3.personneLieeQualite).eq('30') and not Value('id').of(dirigeantAutre3.personneLieeQualite).eq('29') and not Value('id').of(dirigeantAutre3.personneLieeQualite).eq('75')) {
formFieldsPers1['personneLieeQualiteAutrePrime[2]']            = true;
formFieldsPers1['personneLieeQualiteLibellePrime[2]']          = dirigeantAutre3.personneLieeQualite != null ? dirigeantAutre3.personneLieeQualite : dirigeantAutre3.personneLieeQualiteBis;
}
formFieldsPers1['personneLieePPNomNaissancePrime[2]']          = dirigeantAutre3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeantAutre3.civilitePersonneMorale.personneLieePMDenomination);
formFieldsPers1['personneLieePPNomUsagePrime[2]']              = dirigeantAutre3.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre3.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '');

if (dirigeantAutre3.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre3.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre3.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre3.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['personneLieePPPrenomPrime[2]']                                                      = prenoms.toString();
} else if (dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['personneLieePPPrenomPrime[2]']                                                      = prenoms.toString();
}

if (dirigeantAutre3.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeantAutre3.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['personneLieePPDateNaissancePrime[2]']          = date1;
}  else if (dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['personneLieePPDateNaissancePrime[2]']          = date2;
}
formFieldsPers1['personneLieePPLieuNaissancePrime[2]'] = dirigeantAutre3.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ?
													(dirigeantAutre3.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant 
													+ ' ' + "(" + '' + dirigeantAutre3.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() + '' + ")") :	
													(dirigeantAutre3.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
													(dirigeantAutre3.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille 
													+ ' / ' + dirigeantAutre3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) :	
													(dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ?
													(dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant 
													+ ' ' + "(" + '' + dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId()	+ '' + ")") :	
													(dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
													(dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille 
													+ ' / ' + dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers1['personneLieePPNationalitePrime[2]']     = dirigeantAutre3.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeantAutre3.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
formFieldsPers1['personneLieeAdresseVoie[2]']    		= (dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
													+ ' ' + (dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
													+ ' ' + (dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
													+ ' ' + dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseNomVoie
													+ ' ' + (dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
													+ ' ' + (dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers1['personneLieeAdresseCP[2]']         		= dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers1['personneLieeAdresseCommune[2]']         = dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeantAutre3.cadreAdresseDirigeant.dirigeantAdressePays);
formFieldsPers1['personneLieePMformeJuridique[2]']       = dirigeantAutre3.civilitePersonneMorale.personneLieePMFormeJuridique != null ? dirigeantAutre3.civilitePersonneMorale.personneLieePMFormeJuridique : '';
formFieldsPers1['personneLieePMLieuImmat[2]']            = dirigeantAutre3.civilitePersonneMorale.personneLieePMLieuImmatriculation != null ? dirigeantAutre3.civilitePersonneMorale.personneLieePMLieuImmatriculation : '';
formFieldsPers1['personneLieePMNumeroImmat[2]']          = dirigeantAutre3.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeantAutre3.civilitePersonneMorale.personneLieePMNumeroIdentification : '';
}

// Cadre 5 - Autre dirigeant 4M0

if (dirigeantAutre3.autrePersonneLieeEtablissement) {
formFieldsPers1['personneLieeQualiteGerantPrime[3]']           = (Value('id').of(dirigeantAutre4.personneLieeQualite).eq('30') or Value('id').of(dirigeantAutre4.personneLieeQualite).eq('29')) ? true : false;
formFieldsPers1['personneLieeQualiteAssociePrime[3]']          = (Value('id').of(dirigeantAutre4.personneLieeQualite).eq('75') or Value('id').of(dirigeantAutre4.personneLieeQualite).eq('29')) ? true : false;

if (not Value('id').of(dirigeantAutre4.personneLieeQualite).eq('30') and not Value('id').of(dirigeantAutre4.personneLieeQualite).eq('29') and not Value('id').of(dirigeantAutre4.personneLieeQualite).eq('75')) {
formFieldsPers1['personneLieeQualiteAutrePrime[3]']            = true;
formFieldsPers1['personneLieeQualiteLibellePrime[3]']          = dirigeantAutre4.personneLieeQualite != null ? dirigeantAutre4.personneLieeQualite : dirigeantAutre4.personneLieeQualiteBis;
}
formFieldsPers1['personneLieePPNomNaissancePrime[3]']          = dirigeantAutre4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeantAutre4.civilitePersonneMorale.personneLieePMDenomination);
formFieldsPers1['personneLieePPNomUsagePrime[3]']              = dirigeantAutre4.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre4.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '');

if (dirigeantAutre4.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre4.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre4.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre4.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['personneLieePPPrenomPrime[3]']                                                      = prenoms.toString();
} else if (dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['personneLieePPPrenomPrime[3]']                                                      = prenoms.toString();
}

if (dirigeantAutre4.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeantAutre4.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['personneLieePPDateNaissancePrime[3]']          = date1;
}  else if (dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['personneLieePPDateNaissancePrime[3]']          = date2;
}
formFieldsPers1['personneLieePPLieuNaissancePrime[3]'] = dirigeantAutre4.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ?
													(dirigeantAutre4.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant 
													+ ' ' + "(" + '' + dirigeantAutre4.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() + '' + ")") :	
													(dirigeantAutre4.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
													(dirigeantAutre4.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille 
													+ ' / ' + dirigeantAutre4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) :	
													(dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ?
													(dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant 
													+ ' ' + "(" + '' + dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId()	+ '' + ")") :	
													(dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
													(dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille 
													+ ' / ' + dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers1['personneLieePPNationalitePrime[3]']     = dirigeantAutre4.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeantAutre4.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
formFieldsPers1['personneLieeAdresseVoie[3]']    		= (dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
													+ ' ' + (dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
													+ ' ' + (dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
													+ ' ' + dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseNomVoie
													+ ' ' + (dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
													+ ' ' + (dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers1['personneLieeAdresseCP[3]']         		= dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers1['personneLieeAdresseCommune[3]']         = dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeantAutre4.cadreAdresseDirigeant.dirigeantAdressePays);
formFieldsPers1['personneLieePMformeJuridique[3]']       = dirigeantAutre4.civilitePersonneMorale.personneLieePMFormeJuridique != null ? dirigeantAutre4.civilitePersonneMorale.personneLieePMFormeJuridique : '';
formFieldsPers1['personneLieePMLieuImmat[3]']            = dirigeantAutre4.civilitePersonneMorale.personneLieePMLieuImmatriculation != null ? dirigeantAutre4.civilitePersonneMorale.personneLieePMLieuImmatriculation : '';
formFieldsPers1['personneLieePMNumeroImmat[3]']          = dirigeantAutre4.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeantAutre4.civilitePersonneMorale.personneLieePMNumeroIdentification : '';
}

// Cadre 6 - Autre dirigeant 5M0

if (dirigeantAutre4.autrePersonneLieeEtablissement) {
formFieldsPers1['personneLieeQualiteGerantPrime[4]']           = (Value('id').of(dirigeantAutre5.personneLieeQualite).eq('30') or Value('id').of(dirigeantAutre5.personneLieeQualite).eq('29')) ? true : false;
formFieldsPers1['personneLieeQualiteAssociePrime[4]']          = (Value('id').of(dirigeantAutre5.personneLieeQualite).eq('75') or Value('id').of(dirigeantAutre5.personneLieeQualite).eq('29')) ? true : false;

if (not Value('id').of(dirigeantAutre5.personneLieeQualite).eq('30') and not Value('id').of(dirigeantAutre5.personneLieeQualite).eq('29') and not Value('id').of(dirigeantAutre5.personneLieeQualite).eq('75')) {
formFieldsPers1['personneLieeQualiteAutrePrime[4]']            = true;
formFieldsPers1['personneLieeQualiteLibellePrime[4]']          = dirigeantAutre5.personneLieeQualite != null ? dirigeantAutre5.personneLieeQualite : dirigeantAutre5.personneLieeQualiteBis;
}
formFieldsPers1['personneLieePPNomNaissancePrime[4]']          = dirigeantAutre5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeantAutre5.civilitePersonneMorale.personneLieePMDenomination);
formFieldsPers1['personneLieePPNomUsagePrime[4]']              = dirigeantAutre5.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre5.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '');

if (dirigeantAutre5.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre5.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre5.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre5.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['personneLieePPPrenomPrime[4]']                                                      = prenoms.toString();
} else if (dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['personneLieePPPrenomPrime[4]']                                                      = prenoms.toString();
}

if (dirigeantAutre5.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeantAutre5.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['personneLieePPDateNaissancePrime[4]']          = date1;
}  else if (dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['personneLieePPDateNaissancePrime[4]']          = date2;
}
formFieldsPers1['personneLieePPLieuNaissancePrime[4]'] = dirigeantAutre5.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ?
													(dirigeantAutre5.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant 
													+ ' ' + "(" + '' + dirigeantAutre5.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() + '' + ")") :	
													(dirigeantAutre5.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
													(dirigeantAutre5.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille 
													+ ' / ' + dirigeantAutre5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) :	
													(dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ?
													(dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant 
													+ ' ' + "(" + '' + dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId()	+ '' + ")") :	
													(dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
													(dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille 
													+ ' / ' + dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers1['personneLieePPNationalitePrime[4]']     = dirigeantAutre5.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeantAutre5.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
formFieldsPers1['personneLieeAdresseVoie[4]']    		= (dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
													+ ' ' + (dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
													+ ' ' + (dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
													+ ' ' + dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseNomVoie
													+ ' ' + (dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
													+ ' ' + (dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers1['personneLieeAdresseCP[4]']         		= dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers1['personneLieeAdresseCommune[4]']         = dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeantAutre5.cadreAdresseDirigeant.dirigeantAdressePays);
formFieldsPers1['personneLieePMformeJuridique[4]']       = dirigeantAutre5.civilitePersonneMorale.personneLieePMFormeJuridique != null ? dirigeantAutre5.civilitePersonneMorale.personneLieePMFormeJuridique : '';
formFieldsPers1['personneLieePMLieuImmat[4]']            = dirigeantAutre5.civilitePersonneMorale.personneLieePMLieuImmatriculation != null ? dirigeantAutre5.civilitePersonneMorale.personneLieePMLieuImmatriculation : '';
formFieldsPers1['personneLieePMNumeroImmat[4]']          = dirigeantAutre5.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeantAutre5.civilitePersonneMorale.personneLieePMNumeroIdentification : '';
}

// Cadre 7 - Autre dirigeant 6M0

if (dirigeantAutre5.autrePersonneLieeEtablissement) {
formFieldsPers1['personneLieeQualiteGerantPrime[5]']           = (Value('id').of(dirigeantAutre6.personneLieeQualite).eq('30') or Value('id').of(dirigeantAutre6.personneLieeQualite).eq('29')) ? true : false;
formFieldsPers1['personneLieeQualiteAssociePrime[5]']          = (Value('id').of(dirigeantAutre6.personneLieeQualite).eq('75') or Value('id').of(dirigeantAutre6.personneLieeQualite).eq('29')) ? true : false;

if (not Value('id').of(dirigeantAutre6.personneLieeQualite).eq('30') and not Value('id').of(dirigeantAutre6.personneLieeQualite).eq('29') and not Value('id').of(dirigeantAutre6.personneLieeQualite).eq('75')) {
formFieldsPers1['personneLieeQualiteAutrePrime[5]']            = true;
formFieldsPers1['personneLieeQualiteLibellePrime[5]']          = dirigeantAutre6.personneLieeQualite != null ? dirigeantAutre6.personneLieeQualite : dirigeantAutre6.personneLieeQualiteBis;
}
formFieldsPers1['personneLieePPNomNaissancePrime[5]']          = dirigeantAutre6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeantAutre6.civilitePersonneMorale.personneLieePMDenomination);
formFieldsPers1['personneLieePPNomUsagePrime[5]']              = dirigeantAutre6.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre6.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '');

if (dirigeantAutre6.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre6.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre6.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre6.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['personneLieePPPrenomPrime[5]']                                                      = prenoms.toString();
} else if (dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['personneLieePPPrenomPrime[5]']                                                      = prenoms.toString();
}

if (dirigeantAutre6.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeantAutre6.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['personneLieePPDateNaissancePrime[5]']          = date1;
}  else if (dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['personneLieePPDateNaissancePrime[5]']          = date2;
}
formFieldsPers1['personneLieePPLieuNaissancePrime[5]'] = dirigeantAutre6.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ?
													(dirigeantAutre6.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant 
													+ ' ' + "(" + '' + dirigeantAutre6.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() + '' + ")") :	
													(dirigeantAutre6.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
													(dirigeantAutre6.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille 
													+ ' / ' + dirigeantAutre6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) :	
													(dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ?
													(dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant 
													+ ' ' + "(" + '' + dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId()	+ '' + ")") :	
													(dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
													(dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille 
													+ ' / ' + dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers1['personneLieePPNationalitePrime[5]']     = dirigeantAutre6.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeantAutre6.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
formFieldsPers1['personneLieeAdresseVoie[5]']    		= (dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
													+ ' ' + (dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
													+ ' ' + (dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
													+ ' ' + dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseNomVoie
													+ ' ' + (dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
													+ ' ' + (dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers1['personneLieeAdresseCP[5]']         		= dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers1['personneLieeAdresseCommune[5]']         = dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeantAutre6.cadreAdresseDirigeant.dirigeantAdressePays);
formFieldsPers1['personneLieePMformeJuridique[5]']       = dirigeantAutre6.civilitePersonneMorale.personneLieePMFormeJuridique != null ? dirigeantAutre6.civilitePersonneMorale.personneLieePMFormeJuridique : '';
formFieldsPers1['personneLieePMLieuImmat[5]']            = dirigeantAutre6.civilitePersonneMorale.personneLieePMLieuImmatriculation != null ? dirigeantAutre6.civilitePersonneMorale.personneLieePMLieuImmatriculation : '';
formFieldsPers1['personneLieePMNumeroImmat[5]']          = dirigeantAutre6.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeantAutre6.civilitePersonneMorale.personneLieePMNumeroIdentification : '';
}

// Cadre 8 -  Fusion

var fusion1 = $m0SCI.cadre1SocieteGroup.cadre1Identite.fusionGroup1;
var fusion2 = $m0SCI.cadre1SocieteGroup.cadre1Identite.fusionGroup2;
var fusion3 = $m0SCI.cadre1SocieteGroup.cadre1Identite.fusionGroup3;

if(societe.entrepriseFusionScission) {
formFieldsPers1['entrepriseLieeNomPrime[0]']                    = fusion1.entrepriseLieeEntreprisePMDenomination
																+ ' ' + fusion1.entreperiseLieeEntreprisePMFormeJuridique
																+ ' ' + fusion1.entrepriseLieeSiren
																+ ' ' + fusion1.entrepriseLieeGreffe;
formFieldsPers1['entrepriseLieeAdressePrime[0]']                  = (fusion1.adresseFusionScission.numeroAdresseFusion != null ? fusion1.adresseFusionScission.numeroAdresseFusion : '')
																+ ' ' + (fusion1.adresseFusionScission.indiceAdresseFusion != null ? fusion1.adresseFusionScission.indiceAdresseFusion : '')
																+ ' ' + (fusion1.adresseFusionScission.typeAdresseFusion != null ? fusion1.adresseFusionScission.typeAdresseFusion : '')
																+ ' ' + fusion1.adresseFusionScission.voieAdresseFusion
																+ ' ' + (fusion1.adresseFusionScission.complementAdresseFusion != null ? fusion1.adresseFusionScission.complementAdresseFusion : '')
																+ ' ' + (fusion1.adresseFusionScission.distributionSpecialeAdresseFusion != null ? fusion1.adresseFusionScission.distributionSpecialeAdresseFusion : '')
																+ ' ' + (fusion1.adresseFusionScission.codePostalAdresseFusion != null ? fusion1.adresseFusionScission.codePostalAdresseFusion : '')
																+ ' ' + (fusion1.adresseFusionScission.communeAdresseFusion != null ? fusion1.adresseFusionScission.communeAdresseFusion : '');

if(fusion1.fusionAutreSociete) {
formFieldsPers1['entrepriseLieeNomPrime[1]']                    = fusion2.entrepriseLieeEntreprisePMDenomination
																+ ' ' + fusion2.entreperiseLieeEntreprisePMFormeJuridique
																+ ' ' + fusion2.entrepriseLieeSiren
																+ ' ' + fusion2.entrepriseLieeGreffe;
formFieldsPers1['entrepriseLieeAdressePrime[1]']                  = (fusion2.adresseFusionScission.numeroAdresseFusion != null ? fusion2.adresseFusionScission.numeroAdresseFusion : '')
																+ ' ' + (fusion2.adresseFusionScission.indiceAdresseFusion != null ? fusion2.adresseFusionScission.indiceAdresseFusion : '')
																+ ' ' + (fusion2.adresseFusionScission.typeAdresseFusion != null ? fusion2.adresseFusionScission.typeAdresseFusion : '')
																+ ' ' + fusion2.adresseFusionScission.voieAdresseFusion
																+ ' ' + (fusion2.adresseFusionScission.complementAdresseFusion != null ? fusion2.adresseFusionScission.complementAdresseFusion : '')
																+ ' ' + (fusion2.adresseFusionScission.distributionSpecialeAdresseFusion != null ? fusion2.adresseFusionScission.distributionSpecialeAdresseFusion : '')
																+ ' ' + (fusion2.adresseFusionScission.codePostalAdresseFusion != null ? fusion2.adresseFusionScission.codePostalAdresseFusion : '')
																+ ' ' + (fusion2.adresseFusionScission.communeAdresseFusion != null ? fusion2.adresseFusionScission.communeAdresseFusion : '');
}

if(fusion2.fusionAutreSociete2) {
formFieldsPers1['entrepriseLieeNomPrime[2]']                    = fusion3.entrepriseLieeEntreprisePMDenomination
																+ ' ' + fusion3.entreperiseLieeEntreprisePMFormeJuridique
																+ ' ' + fusion3.entrepriseLieeSiren
																+ ' ' + fusion3.entrepriseLieeGreffe;
formFieldsPers1['entrepriseLieeAdressePrime[2]']                  = (fusion3.adresseFusionScission.numeroAdresseFusion != null ? fusion3.adresseFusionScission.numeroAdresseFusion : '')
																+ ' ' + (fusion3.adresseFusionScission.indiceAdresseFusion != null ? fusion3.adresseFusionScission.indiceAdresseFusion : '')
																+ ' ' + (fusion3.adresseFusionScission.typeAdresseFusion != null ? fusion3.adresseFusionScission.typeAdresseFusion : '')
																+ ' ' + fusion3.adresseFusionScission.voieAdresseFusion
																+ ' ' + (fusion3.adresseFusionScission.complementAdresseFusion != null ? fusion3.adresseFusionScission.complementAdresseFusion : '')
																+ ' ' + (fusion3.adresseFusionScission.distributionSpecialeAdresseFusion != null ? fusion3.adresseFusionScission.distributionSpecialeAdresseFusion : '')
																+ ' ' + (fusion3.adresseFusionScission.codePostalAdresseFusion != null ? fusion3.adresseFusionScission.codePostalAdresseFusion : '')
																+ ' ' + (fusion3.adresseFusionScission.communeAdresseFusion != null ? fusion3.adresseFusionScission.communeAdresseFusion : '');
}
}

// M0 prime 2

var formFieldsPers2 = {}

// Cadre 1

formFieldsPers2['numeroIntercalaire_M0sc']                   = "01";
formFieldsPers2['entrepriseDenomination_M0sc']               = societe.entrepriseDenomination;
formFieldsPers2['entrepriseFormeJuridique_M0sc']             = societe.entrepriseFormeJuridique;

// Cadre 2 - Autre Dirigeant 7M0

if (dirigeantAutre6.autrePersonneLieeEtablissement) {
formFieldsPers2['personneLieeQualiteGerantPrime[0]']           = (Value('id').of(dirigeantAutre7.personneLieeQualite).eq('30') or Value('id').of(dirigeantAutre7.personneLieeQualite).eq('29')) ? true : false;
formFieldsPers2['personneLieeQualiteAssociePrime[0]']          = (Value('id').of(dirigeantAutre7.personneLieeQualite).eq('75') or Value('id').of(dirigeantAutre7.personneLieeQualite).eq('29')) ? true : false;

if (not Value('id').of(dirigeantAutre7.personneLieeQualite).eq('30') and not Value('id').of(dirigeantAutre7.personneLieeQualite).eq('29') and not Value('id').of(dirigeantAutre7.personneLieeQualite).eq('75')) {
formFieldsPers2['personneLieeQualiteAutrePrime[0]']            = true;
formFieldsPers2['personneLieeQualiteLibellePrime[0]']          = dirigeantAutre7.personneLieeQualite != null ? dirigeantAutre7.personneLieeQualite : dirigeantAutre7.personneLieeQualiteBis;
}
formFieldsPers2['personneLieePPNomNaissancePrime[0]']          = dirigeantAutre7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeantAutre7.civilitePersonneMorale.personneLieePMDenomination);
formFieldsPers2['personneLieePPNomUsagePrime[0]']              = dirigeantAutre7.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre7.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '');

if (dirigeantAutre7.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre7.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre7.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre7.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers2['personneLieePPPrenomPrime[0]']                                                      = prenoms.toString();
} else if (dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers2['personneLieePPPrenomPrime[0]']                                                      = prenoms.toString();
}

if (dirigeantAutre7.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeantAutre7.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['personneLieePPDateNaissancePrime[0]']          = date1;
}  else if (dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['personneLieePPDateNaissancePrime[0]']          = date2;
}
formFieldsPers2['personneLieePPLieuNaissancePrime[0]'] = dirigeantAutre7.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ?
													(dirigeantAutre7.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant 
													+ ' ' + "(" + '' + dirigeantAutre7.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() + '' + ")") :	
													(dirigeantAutre7.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
													(dirigeantAutre7.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille 
													+ ' / ' + dirigeantAutre7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) :	
													(dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ?
													(dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant 
													+ ' ' + "(" + '' + dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId()	+ '' + ")") :	
													(dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
													(dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille 
													+ ' / ' + dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers2['personneLieePPNationalitePrime[0]']     = dirigeantAutre7.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeantAutre7.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
formFieldsPers2['personneLieeAdresseVoie[0]']    		= (dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
													+ ' ' + (dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
													+ ' ' + (dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
													+ ' ' + dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseNomVoie
													+ ' ' + (dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
													+ ' ' + (dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers2['personneLieeAdresseCP[0]']         		= dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers2['personneLieeAdresseCommune[0]']         = dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeantAutre7.cadreAdresseDirigeant.dirigeantAdressePays);
formFieldsPers2['personneLieePMformeJuridique[0]']       = dirigeantAutre7.civilitePersonneMorale.personneLieePMFormeJuridique != null ? dirigeantAutre7.civilitePersonneMorale.personneLieePMFormeJuridique : '';
formFieldsPers2['personneLieePMLieuImmat[0]']            = dirigeantAutre7.civilitePersonneMorale.personneLieePMLieuImmatriculation != null ? dirigeantAutre7.civilitePersonneMorale.personneLieePMLieuImmatriculation : '';
formFieldsPers2['personneLieePMNumeroImmat[0]']          = dirigeantAutre7.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeantAutre7.civilitePersonneMorale.personneLieePMNumeroIdentification : '';
}

// Cadre 3 - Autre dirigeant 8M0

if (dirigeantAutre7.autrePersonneLieeEtablissement) {
formFieldsPers2['personneLieeQualiteGerantPrime[1]']           = (Value('id').of(dirigeantAutre8.personneLieeQualite).eq('30') or Value('id').of(dirigeantAutre8.personneLieeQualite).eq('29')) ? true : false;
formFieldsPers2['personneLieeQualiteAssociePrime[1]']          = (Value('id').of(dirigeantAutre8.personneLieeQualite).eq('75') or Value('id').of(dirigeantAutre8.personneLieeQualite).eq('29')) ? true : false;

if (not Value('id').of(dirigeantAutre8.personneLieeQualite).eq('30') and not Value('id').of(dirigeantAutre8.personneLieeQualite).eq('29') and not Value('id').of(dirigeantAutre8.personneLieeQualite).eq('75')) {
formFieldsPers2['personneLieeQualiteAutrePrime[1]']            = true;
formFieldsPers2['personneLieeQualiteLibellePrime[1]']          = dirigeantAutre8.personneLieeQualite != null ? dirigeantAutre8.personneLieeQualite : dirigeantAutre8.personneLieeQualiteBis;
}
formFieldsPers2['personneLieePPNomNaissancePrime[1]']          = dirigeantAutre8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeantAutre8.civilitePersonneMorale.personneLieePMDenomination);
formFieldsPers2['personneLieePPNomUsagePrime[1]']              = dirigeantAutre8.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre8.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '');

if (dirigeantAutre8.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre8.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre8.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre8.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers2['personneLieePPPrenomPrime[1]']                                                      = prenoms.toString();
} else if (dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers2['personneLieePPPrenomPrime[1]']                                                      = prenoms.toString();
}

if (dirigeantAutre8.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeantAutre8.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['personneLieePPDateNaissancePrime[1]']          = date1;
}  else if (dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['personneLieePPDateNaissancePrime[1]']          = date2;
}
formFieldsPers2['personneLieePPLieuNaissancePrime[1]'] = dirigeantAutre8.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ?
													(dirigeantAutre8.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant 
													+ ' ' + "(" + '' + dirigeantAutre8.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() + '' + ")") :	
													(dirigeantAutre8.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
													(dirigeantAutre8.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille 
													+ ' / ' + dirigeantAutre8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) :	
													(dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ?
													(dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant 
													+ ' ' + "(" + '' + dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId()	+ '' + ")") :	
													(dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
													(dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille 
													+ ' / ' + dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers2['personneLieePPNationalitePrime[1]']     = dirigeantAutre8.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeantAutre8.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
formFieldsPers2['personneLieeAdresseVoie[1]']    		= (dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
													+ ' ' + (dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
													+ ' ' + (dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
													+ ' ' + dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseNomVoie
													+ ' ' + (dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
													+ ' ' + (dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers2['personneLieeAdresseCP[1]']         		= dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers2['personneLieeAdresseCommune[1]']         = dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeantAutre8.cadreAdresseDirigeant.dirigeantAdressePays);
formFieldsPers2['personneLieePMformeJuridique[1]']       = dirigeantAutre8.civilitePersonneMorale.personneLieePMFormeJuridique != null ? dirigeantAutre8.civilitePersonneMorale.personneLieePMFormeJuridique : '';
formFieldsPers2['personneLieePMLieuImmat[1]']            = dirigeantAutre8.civilitePersonneMorale.personneLieePMLieuImmatriculation != null ? dirigeantAutre8.civilitePersonneMorale.personneLieePMLieuImmatriculation : '';
formFieldsPers2['personneLieePMNumeroImmat[1]']          = dirigeantAutre8.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeantAutre8.civilitePersonneMorale.personneLieePMNumeroIdentification : '';
}

// Cadre 4 - Autre dirigeant 9M0

if (dirigeantAutre8.autrePersonneLieeEtablissement) {
formFieldsPers2['personneLieeQualiteGerantPrime[2]']           = (Value('id').of(dirigeantAutre9.personneLieeQualite).eq('30') or Value('id').of(dirigeantAutre9.personneLieeQualite).eq('29')) ? true : false;
formFieldsPers2['personneLieeQualiteAssociePrime[2]']          = (Value('id').of(dirigeantAutre9.personneLieeQualite).eq('75') or Value('id').of(dirigeantAutre9.personneLieeQualite).eq('29')) ? true : false;

if (not Value('id').of(dirigeantAutre9.personneLieeQualite).eq('30') and not Value('id').of(dirigeantAutre9.personneLieeQualite).eq('29') and not Value('id').of(dirigeantAutre9.personneLieeQualite).eq('75')) {
formFieldsPers2['personneLieeQualiteAutrePrime[2]']            = true;
formFieldsPers2['personneLieeQualiteLibellePrime[2]']          = dirigeantAutre9.personneLieeQualite != null ? dirigeantAutre9.personneLieeQualite : dirigeantAutre9.personneLieeQualiteBis;
}
formFieldsPers2['personneLieePPNomNaissancePrime[2]']          = dirigeantAutre9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeantAutre9.civilitePersonneMorale.personneLieePMDenomination);
formFieldsPers2['personneLieePPNomUsagePrime[2]']              = dirigeantAutre9.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre9.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '');

if (dirigeantAutre9.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre9.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre9.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre9.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers2['personneLieePPPrenomPrime[2]']                                                      = prenoms.toString();
} else if (dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers2['personneLieePPPrenomPrime[2]']                                                      = prenoms.toString();
}

if (dirigeantAutre9.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeantAutre9.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['personneLieePPDateNaissancePrime[2]']          = date1;
}  else if (dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['personneLieePPDateNaissancePrime[2]']          = date2;
}
formFieldsPers2['personneLieePPLieuNaissancePrime[2]'] = dirigeantAutre9.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ?
													(dirigeantAutre9.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant 
													+ ' ' + "(" + '' + dirigeantAutre9.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() + '' + ")") :	
													(dirigeantAutre9.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
													(dirigeantAutre9.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille 
													+ ' / ' + dirigeantAutre9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) :	
													(dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ?
													(dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant 
													+ ' ' + "(" + '' + dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId()	+ '' + ")") :	
													(dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
													(dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille 
													+ ' / ' + dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers2['personneLieePPNationalitePrime[2]']     = dirigeantAutre9.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeantAutre9.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
formFieldsPers2['personneLieeAdresseVoie[2]']    		= (dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
													+ ' ' + (dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
													+ ' ' + (dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
													+ ' ' + dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseNomVoie
													+ ' ' + (dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
													+ ' ' + (dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers2['personneLieeAdresseCP[2]']         		= dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers2['personneLieeAdresseCommune[2]']         = dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeantAutre9.cadreAdresseDirigeant.dirigeantAdressePays);
formFieldsPers2['personneLieePMformeJuridique[2]']       = dirigeantAutre9.civilitePersonneMorale.personneLieePMFormeJuridique != null ? dirigeantAutre9.civilitePersonneMorale.personneLieePMFormeJuridique : '';
formFieldsPers2['personneLieePMLieuImmat[2]']            = dirigeantAutre9.civilitePersonneMorale.personneLieePMLieuImmatriculation != null ? dirigeantAutre9.civilitePersonneMorale.personneLieePMLieuImmatriculation : '';
formFieldsPers2['personneLieePMNumeroImmat[2]']          = dirigeantAutre9.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeantAutre9.civilitePersonneMorale.personneLieePMNumeroIdentification : '';
}

// Cadre 5 - Autre dirigeant 10M0

if (dirigeantAutre9.autrePersonneLieeEtablissement) {
formFieldsPers2['personneLieeQualiteGerantPrime[3]']           = (Value('id').of(dirigeantAutre10.personneLieeQualite).eq('30') or Value('id').of(dirigeantAutre10.personneLieeQualite).eq('29')) ? true : false;
formFieldsPers2['personneLieeQualiteAssociePrime[3]']          = (Value('id').of(dirigeantAutre10.personneLieeQualite).eq('75') or Value('id').of(dirigeantAutre10.personneLieeQualite).eq('29')) ? true : false;

if (not Value('id').of(dirigeantAutre10.personneLieeQualite).eq('30') and not Value('id').of(dirigeantAutre10.personneLieeQualite).eq('29') and not Value('id').of(dirigeantAutre10.personneLieeQualite).eq('75')) {
formFieldsPers2['personneLieeQualiteAutrePrime[3]']            = true;
formFieldsPers2['personneLieeQualiteLibellePrime[3]']          = dirigeantAutre10.personneLieeQualite != null ? dirigeantAutre10.personneLieeQualite : dirigeantAutre10.personneLieeQualiteBis;
}
formFieldsPers2['personneLieePPNomNaissancePrime[3]']          = dirigeantAutre10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeantAutre10.civilitePersonneMorale.personneLieePMDenomination);
formFieldsPers2['personneLieePPNomUsagePrime[3]']              = dirigeantAutre10.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre10.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '');

if (dirigeantAutre10.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre10.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre10.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre10.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers2['personneLieePPPrenomPrime[3]']                                                      = prenoms.toString();
} else if (dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers2['personneLieePPPrenomPrime[3]']                                                      = prenoms.toString();
}

if (dirigeantAutre10.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeantAutre10.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['personneLieePPDateNaissancePrime[3]']          = date1;
}  else if (dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['personneLieePPDateNaissancePrime[3]']          = date2;
}
formFieldsPers2['personneLieePPLieuNaissancePrime[3]'] = dirigeantAutre10.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ?
													(dirigeantAutre10.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant 
													+ ' ' + "(" + '' + dirigeantAutre10.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() + '' + ")") :	
													(dirigeantAutre10.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
													(dirigeantAutre10.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille 
													+ ' / ' + dirigeantAutre10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) :	
													(dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ?
													(dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant 
													+ ' ' + "(" + '' + dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId()	+ '' + ")") :	
													(dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
													(dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille 
													+ ' / ' + dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers2['personneLieePPNationalitePrime[3]']     = dirigeantAutre10.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeantAutre10.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
formFieldsPers2['personneLieeAdresseVoie[3]']    		= (dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
													+ ' ' + (dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
													+ ' ' + (dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
													+ ' ' + dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseNomVoie
													+ ' ' + (dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
													+ ' ' + (dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers2['personneLieeAdresseCP[3]']         		= dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers2['personneLieeAdresseCommune[3]']         = dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeantAutre10.cadreAdresseDirigeant.dirigeantAdressePays);
formFieldsPers2['personneLieePMformeJuridique[3]']       = dirigeantAutre10.civilitePersonneMorale.personneLieePMFormeJuridique != null ? dirigeantAutre10.civilitePersonneMorale.personneLieePMFormeJuridique : '';
formFieldsPers2['personneLieePMLieuImmat[3]']            = dirigeantAutre10.civilitePersonneMorale.personneLieePMLieuImmatriculation != null ? dirigeantAutre10.civilitePersonneMorale.personneLieePMLieuImmatriculation : '';
formFieldsPers2['personneLieePMNumeroImmat[3]']          = dirigeantAutre10.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeantAutre10.civilitePersonneMorale.personneLieePMNumeroIdentification : '';
}

// Cadre 6 - Autre dirigeant 11M0

if (dirigeantAutre10.autrePersonneLieeEtablissement) {
formFieldsPers2['personneLieeQualiteGerantPrime[4]']           = (Value('id').of(dirigeantAutre11.personneLieeQualite).eq('30') or Value('id').of(dirigeantAutre11.personneLieeQualite).eq('29')) ? true : false;
formFieldsPers2['personneLieeQualiteAssociePrime[4]']          = (Value('id').of(dirigeantAutre11.personneLieeQualite).eq('75') or Value('id').of(dirigeantAutre11.personneLieeQualite).eq('29')) ? true : false;

if (not Value('id').of(dirigeantAutre11.personneLieeQualite).eq('30') and not Value('id').of(dirigeantAutre11.personneLieeQualite).eq('29') and not Value('id').of(dirigeantAutre11.personneLieeQualite).eq('75')) {
formFieldsPers2['personneLieeQualiteAutrePrime[4]']            = true;
formFieldsPers2['personneLieeQualiteLibellePrime[4]']          = dirigeantAutre11.personneLieeQualite != null ? dirigeantAutre11.personneLieeQualite : dirigeantAutre11.personneLieeQualiteBis;
}
formFieldsPers2['personneLieePPNomNaissancePrime[4]']          = dirigeantAutre11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeantAutre11.civilitePersonneMorale.personneLieePMDenomination);
formFieldsPers2['personneLieePPNomUsagePrime[4]']              = dirigeantAutre11.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre11.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '');

if (dirigeantAutre11.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre11.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre11.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre11.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers2['personneLieePPPrenomPrime[4]']                                                      = prenoms.toString();
} else if (dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers2['personneLieePPPrenomPrime[4]']                                                      = prenoms.toString();
}

if (dirigeantAutre11.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeantAutre11.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['personneLieePPDateNaissancePrime[4]']          = date1;
}  else if (dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['personneLieePPDateNaissancePrime[4]']          = date2;
}
formFieldsPers2['personneLieePPLieuNaissancePrime[4]'] = dirigeantAutre11.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ?
													(dirigeantAutre11.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant 
													+ ' ' + "(" + '' + dirigeantAutre11.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() + '' + ")") :	
													(dirigeantAutre11.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
													(dirigeantAutre11.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille 
													+ ' / ' + dirigeantAutre11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) :	
													(dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ?
													(dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant 
													+ ' ' + "(" + '' + dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId()	+ '' + ")") :	
													(dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
													(dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille 
													+ ' / ' + dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers2['personneLieePPNationalitePrime[4]']     = dirigeantAutre11.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeantAutre11.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
formFieldsPers2['personneLieeAdresseVoie[4]']    		= (dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
													+ ' ' + (dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
													+ ' ' + (dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
													+ ' ' + dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseNomVoie
													+ ' ' + (dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
													+ ' ' + (dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers2['personneLieeAdresseCP[4]']         		= dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers2['personneLieeAdresseCommune[4]']         = dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeantAutre11.cadreAdresseDirigeant.dirigeantAdressePays);
formFieldsPers2['personneLieePMformeJuridique[4]']       = dirigeantAutre11.civilitePersonneMorale.personneLieePMFormeJuridique != null ? dirigeantAutre11.civilitePersonneMorale.personneLieePMFormeJuridique : '';
formFieldsPers2['personneLieePMLieuImmat[4]']            = dirigeantAutre11.civilitePersonneMorale.personneLieePMLieuImmatriculation != null ? dirigeantAutre11.civilitePersonneMorale.personneLieePMLieuImmatriculation : '';
formFieldsPers2['personneLieePMNumeroImmat[4]']          = dirigeantAutre11.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeantAutre11.civilitePersonneMorale.personneLieePMNumeroIdentification : '';
}

// Cadre 7 - Autre dirigeant 12M0

if (dirigeantAutre11.autrePersonneLieeEtablissement) {
formFieldsPers2['personneLieeQualiteGerantPrime[5]']           = (Value('id').of(dirigeantAutre12.personneLieeQualite).eq('30') or Value('id').of(dirigeantAutre12.personneLieeQualite).eq('29')) ? true : false;
formFieldsPers2['personneLieeQualiteAssociePrime[5]']          = (Value('id').of(dirigeantAutre12.personneLieeQualite).eq('75') or Value('id').of(dirigeantAutre12.personneLieeQualite).eq('29')) ? true : false;

if (not Value('id').of(dirigeantAutre12.personneLieeQualite).eq('30') and not Value('id').of(dirigeantAutre12.personneLieeQualite).eq('29') and not Value('id').of(dirigeantAutre12.personneLieeQualite).eq('75')) {
formFieldsPers2['personneLieeQualiteAutrePrime[5]']            = true;
formFieldsPers2['personneLieeQualiteLibellePrime[5]']          = dirigeantAutre12.personneLieeQualite != null ? dirigeantAutre12.personneLieeQualite : dirigeantAutre12.personneLieeQualiteBis;
}
formFieldsPers2['personneLieePPNomNaissancePrime[5]']          = dirigeantAutre12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeantAutre12.civilitePersonneMorale.personneLieePMDenomination);
formFieldsPers2['personneLieePPNomUsagePrime[5]']              = dirigeantAutre12.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre12.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '');

if (dirigeantAutre12.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre12.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre12.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre12.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers2['personneLieePPPrenomPrime[5]']                                                      = prenoms.toString();
} else if (dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers2['personneLieePPPrenomPrime[5]']                                                      = prenoms.toString();
}

if (dirigeantAutre12.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeantAutre12.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['personneLieePPDateNaissancePrime[5]']          = date1;
}  else if (dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['personneLieePPDateNaissancePrime[5]']          = date2;
}
formFieldsPers2['personneLieePPLieuNaissancePrime[5]'] = dirigeantAutre12.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ?
													(dirigeantAutre12.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant 
													+ ' ' + "(" + '' + dirigeantAutre12.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() + '' + ")") :	
													(dirigeantAutre12.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
													(dirigeantAutre12.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille 
													+ ' / ' + dirigeantAutre12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) :	
													(dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ?
													(dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant 
													+ ' ' + "(" + '' + dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId()	+ '' + ")") :	
													(dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
													(dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille 
													+ ' / ' + dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers2['personneLieePPNationalitePrime[5]']     = dirigeantAutre12.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeantAutre12.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
formFieldsPers2['personneLieeAdresseVoie[5]']    		= (dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
													+ ' ' + (dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
													+ ' ' + (dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
													+ ' ' + dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseNomVoie
													+ ' ' + (dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
													+ ' ' + (dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers2['personneLieeAdresseCP[5]']         		= dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers2['personneLieeAdresseCommune[5]']         = dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeantAutre12.cadreAdresseDirigeant.dirigeantAdressePays);
formFieldsPers2['personneLieePMformeJuridique[5]']       = dirigeantAutre12.civilitePersonneMorale.personneLieePMFormeJuridique != null ? dirigeantAutre12.civilitePersonneMorale.personneLieePMFormeJuridique : '';
formFieldsPers2['personneLieePMLieuImmat[5]']            = dirigeantAutre12.civilitePersonneMorale.personneLieePMLieuImmatriculation != null ? dirigeantAutre12.civilitePersonneMorale.personneLieePMLieuImmatriculation : '';
formFieldsPers2['personneLieePMNumeroImmat[5]']          = dirigeantAutre12.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeantAutre12.civilitePersonneMorale.personneLieePMNumeroIdentification : '';
}


// TNS GERANT 1 

var formFieldsPers3 = {}
var socialDirigeant1 = dirigeant1.cadre7DeclarationSociale;
                                                                            
if (socialDirigeant1.voletSocialNumeroSecuriteSocialTNS != null) {

//cadre 1
var formFieldsPers3 = {}
formFieldsPers3['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers3['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers3['formulaireDependanceM0SotCiv_TNS']                                 = true;
formFieldsPers3['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers3['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers3['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers3['formulaireDependanceTNS_TNS']                                      = false;
  
//cadre 2  
                                                                                                   
formFieldsPers3['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 

formFieldsPers3['personneLieePPNomNaissance_TNS']                                   = dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
formFieldsPers3['personneLieePPNomUsage_TNS']                                       = dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers3['personneLieePPPrenom_TNS']                                = prenoms.toString();
formFieldsPers3['personneLieeQualite_TNS']                                          = dirigeant1.personneLieeQualite;

var nirDeclarant = socialDirigeant1.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers3['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers3['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers3['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers3['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers3['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers3['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;;
if (Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
formFieldsPers3['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = true;
formFieldsPers3['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant1.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
}
if (socialDirigeant1.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers3['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant1.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers3['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant1.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers3['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant1.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant1.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant1.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers3['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers3['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant1.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant1.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers3['voletSocialActiviteSimultaneeOUI_TNS']                             = true;
formFieldsPers3['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers3['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers3['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
if (Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
formFieldsPers3['voletSocialActiviteSimultaneeAutre_TNS']                           = true;
formFieldsPers3['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = socialDirigeant1.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}
formFieldsPers3['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant1.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers3['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant1.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers3['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant1.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers3['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant1.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;
                                                                                  
//Cadre 7  

formFieldsPers3['renseignementsComplementairesLeDeclarant_TNS']                     = (dirigeant1.dirigeantSignataire or dirigeant2.dirigeantSignataire or dirigeant3.dirigeantSignataire or dirigeant4.dirigeantSignataire or dirigeant5.dirigeantSignataire or dirigeant6.dirigeantSignataire or dirigeant7.dirigeantSignataire or dirigeant8.dirigeantSignataire or dirigeantAutre1.dirigeantSignataire or dirigeantAutre2.dirigeantSignataire or dirigeantAutre3.dirigeantSignataire or dirigeantAutre4.dirigeantSignataire or dirigeantAutre5.dirigeantSignataire or dirigeantAutre6.dirigeantSignataire or dirigeantAutre7.dirigeantSignataire or dirigeantAutre8.dirigeantSignataire or dirigeantAutre9.dirigeantSignataire or dirigeantAutre10.dirigeantSignataire or dirigeantAutre11.dirigeantSignataire or dirigeantAutre12.dirigeantSignataire) ? true : false;
if (not dirigeant1.dirigeantSignataire and not dirigeant2.dirigeantSignataire and not dirigeant3.dirigeantSignataire and not dirigeant4.dirigeantSignataire and not dirigeant5.dirigeantSignataire and not dirigeant6.dirigeantSignataire and not dirigeant7.dirigeantSignataire and not dirigeant8.dirigeantSignataire and not dirigeantAutre1.dirigeantSignataire and not dirigeantAutre2.dirigeantSignataire and not dirigeantAutre3.dirigeantSignataire and not dirigeantAutre4.dirigeantSignataire and not dirigeantAutre5.dirigeantSignataire and not dirigeantAutre6.dirigeantSignataire and not dirigeantAutre7.dirigeantSignataire and not dirigeantAutre8.dirigeantSignataire and not dirigeantAutre9.dirigeantSignataire and not dirigeantAutre10.dirigeantSignataire and not dirigeantAutre11.dirigeantSignataire and not dirigeantAutre12.dirigeantSignataire) {
formFieldsPers3['renseignementsComplementairesLeMandataire_TNS']                    = true;
formFieldsPers3['formaliteSignataireNomPrenomDenomination_TNS']	          			= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers3['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');	
formFieldsPers3['formaliteSignataireAdresseComplement_TNS']							= (signataire.adresseMandataire.codePostalMandataire != null ? signataire.adresseMandataire.codePostalMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.villeAdresseMandataire != null ? signataire.adresseMandataire.villeAdresseMandataire : '');
}
formFieldsPers3['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;

if (signataire.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers3['formaliteSignatureDate_TNS']          = date1;
}  
formFieldsPers3['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
}

// TNS GERANT 2 

var formFieldsPers4 = {}
var socialDirigeant2 = dirigeant2.cadre7DeclarationSociale;
                                                                            
if (socialDirigeant2.voletSocialNumeroSecuriteSocialTNS != null) {

//cadre 1
var formFieldsPers4 = {}
formFieldsPers4['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers4['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers4['formulaireDependanceM0SotCiv_TNS']                                 = true;
formFieldsPers4['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers4['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers4['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers4['formulaireDependanceTNS_TNS']                                      = false;
  
//cadre 2  
                                                                                                   
formFieldsPers4['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 

formFieldsPers4['personneLieePPNomNaissance_TNS']                                   = dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
formFieldsPers4['personneLieePPNomUsage_TNS']                                       = dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers4['personneLieePPPrenom_TNS']                                = prenoms.toString();
formFieldsPers4['personneLieeQualite_TNS']                                          = dirigeant2.personneLieeQualite;

var nirDeclarant = socialDirigeant2.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers4['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers4['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers4['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers4['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers4['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers4['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;;
if (Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
formFieldsPers4['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = true;
formFieldsPers4['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant2.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
}
if (socialDirigeant2.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers4['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant2.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers4['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant2.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers4['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant2.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant2.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant2.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers4['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers4['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant2.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant2.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers4['voletSocialActiviteSimultaneeOUI_TNS']                             = true;
formFieldsPers4['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers4['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers4['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
if (Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
formFieldsPers4['voletSocialActiviteSimultaneeAutre_TNS']                           = true;
formFieldsPers4['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = socialDirigeant2.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}
formFieldsPers4['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant2.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers4['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant2.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers4['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant2.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers4['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant2.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;
                                                                                  
//Cadre 7  

formFieldsPers4['renseignementsComplementairesLeDeclarant_TNS']                     = (dirigeant1.dirigeantSignataire or dirigeant2.dirigeantSignataire or dirigeant3.dirigeantSignataire or dirigeant4.dirigeantSignataire or dirigeant5.dirigeantSignataire or dirigeant6.dirigeantSignataire or dirigeant7.dirigeantSignataire or dirigeant8.dirigeantSignataire or dirigeantAutre1.dirigeantSignataire or dirigeantAutre2.dirigeantSignataire or dirigeantAutre3.dirigeantSignataire or dirigeantAutre4.dirigeantSignataire or dirigeantAutre5.dirigeantSignataire or dirigeantAutre6.dirigeantSignataire or dirigeantAutre7.dirigeantSignataire or dirigeantAutre8.dirigeantSignataire or dirigeantAutre9.dirigeantSignataire or dirigeantAutre10.dirigeantSignataire or dirigeantAutre11.dirigeantSignataire or dirigeantAutre12.dirigeantSignataire) ? true : false;
if (not dirigeant1.dirigeantSignataire and not dirigeant2.dirigeantSignataire and not dirigeant3.dirigeantSignataire and not dirigeant4.dirigeantSignataire and not dirigeant5.dirigeantSignataire and not dirigeant6.dirigeantSignataire and not dirigeant7.dirigeantSignataire and not dirigeant8.dirigeantSignataire and not dirigeantAutre1.dirigeantSignataire and not dirigeantAutre2.dirigeantSignataire and not dirigeantAutre3.dirigeantSignataire and not dirigeantAutre4.dirigeantSignataire and not dirigeantAutre5.dirigeantSignataire and not dirigeantAutre6.dirigeantSignataire and not dirigeantAutre7.dirigeantSignataire and not dirigeantAutre8.dirigeantSignataire and not dirigeantAutre9.dirigeantSignataire and not dirigeantAutre10.dirigeantSignataire and not dirigeantAutre11.dirigeantSignataire and not dirigeantAutre12.dirigeantSignataire) {
formFieldsPers4['renseignementsComplementairesLeMandataire_TNS']                    = true;
formFieldsPers4['formaliteSignataireNomPrenomDenomination_TNS']	          			= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers4['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');	
formFieldsPers4['formaliteSignataireAdresseComplement_TNS']							= (signataire.adresseMandataire.codePostalMandataire != null ? signataire.adresseMandataire.codePostalMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.villeAdresseMandataire != null ? signataire.adresseMandataire.villeAdresseMandataire : '');
}
formFieldsPers4['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
if (signataire.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers4['formaliteSignatureDate_TNS']          = date1;
}  
formFieldsPers4['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
}

// TNS GERANT 3 

var formFieldsPers5 = {}
var socialDirigeant3 = dirigeant3.cadre7DeclarationSociale;
                                                                            
if (socialDirigeant3.voletSocialNumeroSecuriteSocialTNS != null) {

//cadre 1
var formFieldsPers5 = {}
formFieldsPers5['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers5['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers5['formulaireDependanceM0SotCiv_TNS']                                 = true;
formFieldsPers5['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers5['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers5['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers5['formulaireDependanceTNS_TNS']                                      = false;
  
//cadre 2  
                                                                                                   
formFieldsPers5['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 

formFieldsPers5['personneLieePPNomNaissance_TNS']                                   = dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
formFieldsPers5['personneLieePPNomUsage_TNS']                                       = dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers5['personneLieePPPrenom_TNS']                                = prenoms.toString();
formFieldsPers5['personneLieeQualite_TNS']                                          = dirigeant3.personneLieeQualite;

var nirDeclarant = socialDirigeant3.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers5['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers5['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;;
if (Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = true;
formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant3.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
}
if (socialDirigeant3.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers5['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant3.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers5['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant3.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers5['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant3.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant3.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant3.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers5['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers5['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant3.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant3.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers5['voletSocialActiviteSimultaneeOUI_TNS']                             = true;
formFieldsPers5['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers5['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers5['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
if (Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
formFieldsPers5['voletSocialActiviteSimultaneeAutre_TNS']                           = true;
formFieldsPers5['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = socialDirigeant3.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}
formFieldsPers5['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant3.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers5['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant3.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers5['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant3.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers5['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant3.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;
                                                                                  
//Cadre 7  

formFieldsPers5['renseignementsComplementairesLeDeclarant_TNS']                     = (dirigeant1.dirigeantSignataire or dirigeant2.dirigeantSignataire or dirigeant3.dirigeantSignataire or dirigeant4.dirigeantSignataire or dirigeant5.dirigeantSignataire or dirigeant6.dirigeantSignataire or dirigeant7.dirigeantSignataire or dirigeant8.dirigeantSignataire or dirigeantAutre1.dirigeantSignataire or dirigeantAutre2.dirigeantSignataire or dirigeantAutre3.dirigeantSignataire or dirigeantAutre4.dirigeantSignataire or dirigeantAutre5.dirigeantSignataire or dirigeantAutre6.dirigeantSignataire or dirigeantAutre7.dirigeantSignataire or dirigeantAutre8.dirigeantSignataire or dirigeantAutre9.dirigeantSignataire or dirigeantAutre10.dirigeantSignataire or dirigeantAutre11.dirigeantSignataire or dirigeantAutre12.dirigeantSignataire) ? true : false;
if (not dirigeant1.dirigeantSignataire and not dirigeant2.dirigeantSignataire and not dirigeant3.dirigeantSignataire and not dirigeant4.dirigeantSignataire and not dirigeant5.dirigeantSignataire and not dirigeant6.dirigeantSignataire and not dirigeant7.dirigeantSignataire and not dirigeant8.dirigeantSignataire and not dirigeantAutre1.dirigeantSignataire and not dirigeantAutre2.dirigeantSignataire and not dirigeantAutre3.dirigeantSignataire and not dirigeantAutre4.dirigeantSignataire and not dirigeantAutre5.dirigeantSignataire and not dirigeantAutre6.dirigeantSignataire and not dirigeantAutre7.dirigeantSignataire and not dirigeantAutre8.dirigeantSignataire and not dirigeantAutre9.dirigeantSignataire and not dirigeantAutre10.dirigeantSignataire and not dirigeantAutre11.dirigeantSignataire and not dirigeantAutre12.dirigeantSignataire) {
formFieldsPers5['renseignementsComplementairesLeMandataire_TNS']                    = true;
formFieldsPers5['formaliteSignataireNomPrenomDenomination_TNS']	          			= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers5['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');	
formFieldsPers5['formaliteSignataireAdresseComplement_TNS']							= (signataire.adresseMandataire.codePostalMandataire != null ? signataire.adresseMandataire.codePostalMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.villeAdresseMandataire != null ? signataire.adresseMandataire.villeAdresseMandataire : '');
}
formFieldsPers5['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
if (signataire.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers5['formaliteSignatureDate_TNS']          = date1;
}  
formFieldsPers5['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
}

// TNS GERANT 4 

var formFieldsPers6 = {}
var socialDirigeant4 = dirigeant4.cadre7DeclarationSociale;
                                                                            
if (socialDirigeant4.voletSocialNumeroSecuriteSocialTNS != null) {

//cadre 1
var formFieldsPers6 = {}
formFieldsPers6['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers6['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers6['formulaireDependanceM0SotCiv_TNS']                                 = true;
formFieldsPers6['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers6['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers6['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers6['formulaireDependanceTNS_TNS']                                      = false;
  
//cadre 2  
                                                                                                   
formFieldsPers6['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 

formFieldsPers6['personneLieePPNomNaissance_TNS']                                   = dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
formFieldsPers6['personneLieePPNomUsage_TNS']                                       = dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers6['personneLieePPPrenom_TNS']                                = prenoms.toString();
formFieldsPers6['personneLieeQualite_TNS']                                          = dirigeant4.personneLieeQualite;

var nirDeclarant = socialDirigeant4.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers6['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers6['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers6['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers6['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers6['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers6['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;;
if (Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
formFieldsPers6['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = true;
formFieldsPers6['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant4.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
}
if (socialDirigeant4.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers6['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant4.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers6['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant4.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers6['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant4.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant4.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant4.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers6['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers6['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant4.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant4.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers6['voletSocialActiviteSimultaneeOUI_TNS']                             = true;
formFieldsPers6['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers6['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers6['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
if (Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
formFieldsPers6['voletSocialActiviteSimultaneeAutre_TNS']                           = true;
formFieldsPers6['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = socialDirigeant4.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}
formFieldsPers6['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant4.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers6['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant4.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers6['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant4.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers6['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant4.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;
                                                                                  
//Cadre 7  

formFieldsPers6['renseignementsComplementairesLeDeclarant_TNS']                     = (dirigeant1.dirigeantSignataire or dirigeant2.dirigeantSignataire or dirigeant3.dirigeantSignataire or dirigeant4.dirigeantSignataire or dirigeant5.dirigeantSignataire or dirigeant6.dirigeantSignataire or dirigeant7.dirigeantSignataire or dirigeant8.dirigeantSignataire or dirigeantAutre1.dirigeantSignataire or dirigeantAutre2.dirigeantSignataire or dirigeantAutre3.dirigeantSignataire or dirigeantAutre4.dirigeantSignataire or dirigeantAutre5.dirigeantSignataire or dirigeantAutre6.dirigeantSignataire or dirigeantAutre7.dirigeantSignataire or dirigeantAutre8.dirigeantSignataire or dirigeantAutre9.dirigeantSignataire or dirigeantAutre10.dirigeantSignataire or dirigeantAutre11.dirigeantSignataire or dirigeantAutre12.dirigeantSignataire) ? true : false;
if (not dirigeant1.dirigeantSignataire and not dirigeant2.dirigeantSignataire and not dirigeant3.dirigeantSignataire and not dirigeant4.dirigeantSignataire and not dirigeant5.dirigeantSignataire and not dirigeant6.dirigeantSignataire and not dirigeant7.dirigeantSignataire and not dirigeant8.dirigeantSignataire and not dirigeantAutre1.dirigeantSignataire and not dirigeantAutre2.dirigeantSignataire and not dirigeantAutre3.dirigeantSignataire and not dirigeantAutre4.dirigeantSignataire and not dirigeantAutre5.dirigeantSignataire and not dirigeantAutre6.dirigeantSignataire and not dirigeantAutre7.dirigeantSignataire and not dirigeantAutre8.dirigeantSignataire and not dirigeantAutre9.dirigeantSignataire and not dirigeantAutre10.dirigeantSignataire and not dirigeantAutre11.dirigeantSignataire and not dirigeantAutre12.dirigeantSignataire) {
formFieldsPers6['renseignementsComplementairesLeMandataire_TNS']                    = true;
formFieldsPers6['formaliteSignataireNomPrenomDenomination_TNS']	          			= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers6['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');	
formFieldsPers6['formaliteSignataireAdresseComplement_TNS']							= (signataire.adresseMandataire.codePostalMandataire != null ? signataire.adresseMandataire.codePostalMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.villeAdresseMandataire != null ? signataire.adresseMandataire.villeAdresseMandataire : '');
}
formFieldsPers6['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
if (signataire.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['formaliteSignatureDate_TNS']          = date1;
}  
formFieldsPers6['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
}

// TNS GERANT 5 

var formFieldsPers7 = {}
var socialDirigeant5 = dirigeant5.cadre7DeclarationSociale;
                                                                            
if (socialDirigeant5.voletSocialNumeroSecuriteSocialTNS != null) {

//cadre 1
var formFieldsPers7 = {}
formFieldsPers7['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers7['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers7['formulaireDependanceM0SotCiv_TNS']                                 = true;
formFieldsPers7['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers7['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers7['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers7['formulaireDependanceTNS_TNS']                                      = false;
  
//cadre 2  
                                                                                                   
formFieldsPers7['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 

formFieldsPers7['personneLieePPNomNaissance_TNS']                                   = dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
formFieldsPers7['personneLieePPNomUsage_TNS']                                       = dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers7['personneLieePPPrenom_TNS']                                = prenoms.toString();
formFieldsPers7['personneLieeQualite_TNS']                                          = dirigeant5.personneLieeQualite;

var nirDeclarant = socialDirigeant5.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers7['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers7['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers7['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers7['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers7['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers7['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;;
if (Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
formFieldsPers7['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = true;
formFieldsPers7['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant5.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
}
if (socialDirigeant5.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers7['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant5.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers7['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant5.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers7['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant5.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant5.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant5.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers7['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers7['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant5.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant5.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers7['voletSocialActiviteSimultaneeOUI_TNS']                             = true;
formFieldsPers7['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers7['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers7['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
if (Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
formFieldsPers7['voletSocialActiviteSimultaneeAutre_TNS']                           = true;
formFieldsPers7['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = socialDirigeant5.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}
formFieldsPers7['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant5.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers7['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant5.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers7['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant5.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers7['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant5.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;
                                                                                  
//Cadre 7  

formFieldsPers7['renseignementsComplementairesLeDeclarant_TNS']                     = (dirigeant1.dirigeantSignataire or dirigeant2.dirigeantSignataire or dirigeant3.dirigeantSignataire or dirigeant4.dirigeantSignataire or dirigeant5.dirigeantSignataire or dirigeant6.dirigeantSignataire or dirigeant7.dirigeantSignataire or dirigeant8.dirigeantSignataire or dirigeantAutre1.dirigeantSignataire or dirigeantAutre2.dirigeantSignataire or dirigeantAutre3.dirigeantSignataire or dirigeantAutre4.dirigeantSignataire or dirigeantAutre5.dirigeantSignataire or dirigeantAutre6.dirigeantSignataire or dirigeantAutre7.dirigeantSignataire or dirigeantAutre8.dirigeantSignataire or dirigeantAutre9.dirigeantSignataire or dirigeantAutre10.dirigeantSignataire or dirigeantAutre11.dirigeantSignataire or dirigeantAutre12.dirigeantSignataire) ? true : false;
if (not dirigeant1.dirigeantSignataire and not dirigeant2.dirigeantSignataire and not dirigeant3.dirigeantSignataire and not dirigeant4.dirigeantSignataire and not dirigeant5.dirigeantSignataire and not dirigeant6.dirigeantSignataire and not dirigeant7.dirigeantSignataire and not dirigeant8.dirigeantSignataire and not dirigeantAutre1.dirigeantSignataire and not dirigeantAutre2.dirigeantSignataire and not dirigeantAutre3.dirigeantSignataire and not dirigeantAutre4.dirigeantSignataire and not dirigeantAutre5.dirigeantSignataire and not dirigeantAutre6.dirigeantSignataire and not dirigeantAutre7.dirigeantSignataire and not dirigeantAutre8.dirigeantSignataire and not dirigeantAutre9.dirigeantSignataire and not dirigeantAutre10.dirigeantSignataire and not dirigeantAutre11.dirigeantSignataire and not dirigeantAutre12.dirigeantSignataire) {
formFieldsPers7['renseignementsComplementairesLeMandataire_TNS']                    = true;
formFieldsPers7['formaliteSignataireNomPrenomDenomination_TNS']	          			= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers7['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');	
formFieldsPers7['formaliteSignataireAdresseComplement_TNS']							= (signataire.adresseMandataire.codePostalMandataire != null ? signataire.adresseMandataire.codePostalMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.villeAdresseMandataire != null ? signataire.adresseMandataire.villeAdresseMandataire : '');
}
formFieldsPers7['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
if (signataire.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['formaliteSignatureDate_TNS']          = date1;
}  
formFieldsPers7['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
}

// TNS GERANT 6 

var formFieldsPers8 = {}
var socialDirigeant6 = dirigeant6.cadre7DeclarationSociale;
                                                                            
if (socialDirigeant6.voletSocialNumeroSecuriteSocialTNS != null) {

//cadre 1
var formFieldsPers8 = {}
formFieldsPers8['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers8['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers8['formulaireDependanceM0SotCiv_TNS']                                 = true;
formFieldsPers8['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers8['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers8['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers8['formulaireDependanceTNS_TNS']                                      = false;
  
//cadre 2  
                                                                                                   
formFieldsPers8['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 

formFieldsPers8['personneLieePPNomNaissance_TNS']                                   = dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
formFieldsPers8['personneLieePPNomUsage_TNS']                                       = dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers8['personneLieePPPrenom_TNS']                                = prenoms.toString();
formFieldsPers8['personneLieeQualite_TNS']                                          = dirigeant6.personneLieeQualite;

var nirDeclarant = socialDirigeant6.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers8['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers8['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers8['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers8['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers8['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers8['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;;
if (Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
formFieldsPers8['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = true;
formFieldsPers8['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant6.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
}
if (socialDirigeant6.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers8['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant6.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers8['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant6.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers8['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant6.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant6.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant6.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers8['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers8['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant6.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant6.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers8['voletSocialActiviteSimultaneeOUI_TNS']                             = true;
formFieldsPers8['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers8['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers8['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
if (Value('id').of(socialDirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
formFieldsPers8['voletSocialActiviteSimultaneeAutre_TNS']                           = true;
formFieldsPers8['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = socialDirigeant6.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}
formFieldsPers8['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant6.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers8['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant6.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers8['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant6.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers8['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant6.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;
                                                                                  
//Cadre 7  

formFieldsPers8['renseignementsComplementairesLeDeclarant_TNS']                     = (dirigeant1.dirigeantSignataire or dirigeant2.dirigeantSignataire or dirigeant3.dirigeantSignataire or dirigeant4.dirigeantSignataire or dirigeant5.dirigeantSignataire or dirigeant6.dirigeantSignataire or dirigeant7.dirigeantSignataire or dirigeant8.dirigeantSignataire or dirigeantAutre1.dirigeantSignataire or dirigeantAutre2.dirigeantSignataire or dirigeantAutre3.dirigeantSignataire or dirigeantAutre4.dirigeantSignataire or dirigeantAutre5.dirigeantSignataire or dirigeantAutre6.dirigeantSignataire or dirigeantAutre7.dirigeantSignataire or dirigeantAutre8.dirigeantSignataire or dirigeantAutre9.dirigeantSignataire or dirigeantAutre10.dirigeantSignataire or dirigeantAutre11.dirigeantSignataire or dirigeantAutre12.dirigeantSignataire) ? true : false;
if (not dirigeant1.dirigeantSignataire and not dirigeant2.dirigeantSignataire and not dirigeant3.dirigeantSignataire and not dirigeant4.dirigeantSignataire and not dirigeant5.dirigeantSignataire and not dirigeant6.dirigeantSignataire and not dirigeant7.dirigeantSignataire and not dirigeant8.dirigeantSignataire and not dirigeantAutre1.dirigeantSignataire and not dirigeantAutre2.dirigeantSignataire and not dirigeantAutre3.dirigeantSignataire and not dirigeantAutre4.dirigeantSignataire and not dirigeantAutre5.dirigeantSignataire and not dirigeantAutre6.dirigeantSignataire and not dirigeantAutre7.dirigeantSignataire and not dirigeantAutre8.dirigeantSignataire and not dirigeantAutre9.dirigeantSignataire and not dirigeantAutre10.dirigeantSignataire and not dirigeantAutre11.dirigeantSignataire and not dirigeantAutre12.dirigeantSignataire) {
formFieldsPers8['renseignementsComplementairesLeMandataire_TNS']                    = true;
formFieldsPers8['formaliteSignataireNomPrenomDenomination_TNS']	          			= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers8['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');	
formFieldsPers8['formaliteSignataireAdresseComplement_TNS']							= (signataire.adresseMandataire.codePostalMandataire != null ? signataire.adresseMandataire.codePostalMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.villeAdresseMandataire != null ? signataire.adresseMandataire.villeAdresseMandataire : '');
}
formFieldsPers8['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
if (signataire.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers8['formaliteSignatureDate_TNS']          = date1;
}  
formFieldsPers8['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
}

// TNS GERANT 7 

var formFieldsPers9 = {}
var socialDirigeant7 = dirigeant7.cadre7DeclarationSociale;
                                                                            
if (socialDirigeant7.voletSocialNumeroSecuriteSocialTNS != null) {

//cadre 1
var formFieldsPers9 = {}
formFieldsPers9['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers9['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers9['formulaireDependanceM0SotCiv_TNS']                                 = true;
formFieldsPers9['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers9['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers9['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers9['formulaireDependanceTNS_TNS']                                      = false;
  
//cadre 2  
                                                                                                   
formFieldsPers9['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 

formFieldsPers9['personneLieePPNomNaissance_TNS']                                   = dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
formFieldsPers9['personneLieePPNomUsage_TNS']                                       = dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers9['personneLieePPPrenom_TNS']                                = prenoms.toString();
formFieldsPers9['personneLieeQualite_TNS']                                          = dirigeant7.personneLieeQualite;

var nirDeclarant = socialDirigeant7.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers9['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers9['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers9['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers9['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers9['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers9['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;;
if (Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
formFieldsPers9['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = true;
formFieldsPers9['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant7.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
}
if (socialDirigeant7.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers9['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant7.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers9['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant7.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers9['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant7.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant7.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant7.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers9['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers9['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant7.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant7.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers9['voletSocialActiviteSimultaneeOUI_TNS']                             = true;
formFieldsPers9['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant7.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers9['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant7.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers9['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant7.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
if (Value('id').of(socialDirigeant7.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
formFieldsPers9['voletSocialActiviteSimultaneeAutre_TNS']                           = true;
formFieldsPers9['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = socialDirigeant7.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}
formFieldsPers9['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant7.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers9['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant7.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers9['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant7.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers9['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant7.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;
                                                                                  
//Cadre 7  

formFieldsPers9['renseignementsComplementairesLeDeclarant_TNS']                     = (dirigeant1.dirigeantSignataire or dirigeant2.dirigeantSignataire or dirigeant3.dirigeantSignataire or dirigeant4.dirigeantSignataire or dirigeant5.dirigeantSignataire or dirigeant6.dirigeantSignataire or dirigeant7.dirigeantSignataire or dirigeant8.dirigeantSignataire or dirigeantAutre1.dirigeantSignataire or dirigeantAutre2.dirigeantSignataire or dirigeantAutre3.dirigeantSignataire or dirigeantAutre4.dirigeantSignataire or dirigeantAutre5.dirigeantSignataire or dirigeantAutre6.dirigeantSignataire or dirigeantAutre7.dirigeantSignataire or dirigeantAutre8.dirigeantSignataire or dirigeantAutre9.dirigeantSignataire or dirigeantAutre10.dirigeantSignataire or dirigeantAutre11.dirigeantSignataire or dirigeantAutre12.dirigeantSignataire) ? true : false;
if (not dirigeant1.dirigeantSignataire and not dirigeant2.dirigeantSignataire and not dirigeant3.dirigeantSignataire and not dirigeant4.dirigeantSignataire and not dirigeant5.dirigeantSignataire and not dirigeant6.dirigeantSignataire and not dirigeant7.dirigeantSignataire and not dirigeant8.dirigeantSignataire and not dirigeantAutre1.dirigeantSignataire and not dirigeantAutre2.dirigeantSignataire and not dirigeantAutre3.dirigeantSignataire and not dirigeantAutre4.dirigeantSignataire and not dirigeantAutre5.dirigeantSignataire and not dirigeantAutre6.dirigeantSignataire and not dirigeantAutre7.dirigeantSignataire and not dirigeantAutre8.dirigeantSignataire and not dirigeantAutre9.dirigeantSignataire and not dirigeantAutre10.dirigeantSignataire and not dirigeantAutre11.dirigeantSignataire and not dirigeantAutre12.dirigeantSignataire) {
formFieldsPers9['renseignementsComplementairesLeMandataire_TNS']                    = true;
formFieldsPers9['formaliteSignataireNomPrenomDenomination_TNS']	          			= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers9['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');	
formFieldsPers9['formaliteSignataireAdresseComplement_TNS']							= (signataire.adresseMandataire.codePostalMandataire != null ? signataire.adresseMandataire.codePostalMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.villeAdresseMandataire != null ? signataire.adresseMandataire.villeAdresseMandataire : '');
}
formFieldsPers9['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
if (signataire.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers9['formaliteSignatureDate_TNS']          = date1;
}  
formFieldsPers9['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
}

// TNS GERANT 8 

var formFieldsPers10 = {}
var socialDirigeant8 = dirigeant8.cadre7DeclarationSociale;
                                                                            
if (socialDirigeant8.voletSocialNumeroSecuriteSocialTNS != null) {

//cadre 1
var formFieldsPers10 = {}
formFieldsPers10['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers10['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers10['formulaireDependanceM0SotCiv_TNS']                                 = true;
formFieldsPers10['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers10['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers10['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers10['formulaireDependanceTNS_TNS']                                      = false;
  
//cadre 2  
                                                                                                   
formFieldsPers10['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 

formFieldsPers10['personneLieePPNomNaissance_TNS']                                   = dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
formFieldsPers10['personneLieePPNomUsage_TNS']                                       = dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers10['personneLieePPPrenom_TNS']                                = prenoms.toString();
formFieldsPers10['personneLieeQualite_TNS']                                          = dirigeant8.personneLieeQualite;

var nirDeclarant = socialDirigeant8.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers10['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers10['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers10['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers10['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers10['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers10['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;;
if (Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
formFieldsPers10['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = true;
formFieldsPers10['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant8.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
}
if (socialDirigeant8.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers10['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant8.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers10['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant8.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers10['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant8.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant8.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant8.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers10['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers10['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant8.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant8.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers10['voletSocialActiviteSimultaneeOUI_TNS']                             = true;
formFieldsPers10['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant8.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers10['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant8.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers10['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant8.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
if (Value('id').of(socialDirigeant8.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
formFieldsPers10['voletSocialActiviteSimultaneeAutre_TNS']                           = true;
formFieldsPers10['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = socialDirigeant8.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}
formFieldsPers10['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant8.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers10['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant8.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers10['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant8.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers10['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant8.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;
                                                                                  
//Cadre 7  

formFieldsPers10['renseignementsComplementairesLeDeclarant_TNS']                     = (dirigeant1.dirigeantSignataire or dirigeant2.dirigeantSignataire or dirigeant3.dirigeantSignataire or dirigeant4.dirigeantSignataire or dirigeant5.dirigeantSignataire or dirigeant6.dirigeantSignataire or dirigeant7.dirigeantSignataire or dirigeant8.dirigeantSignataire or dirigeantAutre1.dirigeantSignataire or dirigeantAutre2.dirigeantSignataire or dirigeantAutre3.dirigeantSignataire or dirigeantAutre4.dirigeantSignataire or dirigeantAutre5.dirigeantSignataire or dirigeantAutre6.dirigeantSignataire or dirigeantAutre7.dirigeantSignataire or dirigeantAutre8.dirigeantSignataire or dirigeantAutre9.dirigeantSignataire or dirigeantAutre10.dirigeantSignataire or dirigeantAutre11.dirigeantSignataire or dirigeantAutre12.dirigeantSignataire) ? true : false;
if (not dirigeant1.dirigeantSignataire and not dirigeant2.dirigeantSignataire and not dirigeant3.dirigeantSignataire and not dirigeant4.dirigeantSignataire and not dirigeant5.dirigeantSignataire and not dirigeant6.dirigeantSignataire and not dirigeant7.dirigeantSignataire and not dirigeant8.dirigeantSignataire and not dirigeantAutre1.dirigeantSignataire and not dirigeantAutre2.dirigeantSignataire and not dirigeantAutre3.dirigeantSignataire and not dirigeantAutre4.dirigeantSignataire and not dirigeantAutre5.dirigeantSignataire and not dirigeantAutre6.dirigeantSignataire and not dirigeantAutre7.dirigeantSignataire and not dirigeantAutre8.dirigeantSignataire and not dirigeantAutre9.dirigeantSignataire and not dirigeantAutre10.dirigeantSignataire and not dirigeantAutre11.dirigeantSignataire and not dirigeantAutre12.dirigeantSignataire) {
formFieldsPers10['renseignementsComplementairesLeMandataire_TNS']                    = true;
formFieldsPers10['formaliteSignataireNomPrenomDenomination_TNS']	          			= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers10['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');	
formFieldsPers10['formaliteSignataireAdresseComplement_TNS']							= (signataire.adresseMandataire.codePostalMandataire != null ? signataire.adresseMandataire.codePostalMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.villeAdresseMandataire != null ? signataire.adresseMandataire.villeAdresseMandataire : '');
}
formFieldsPers10['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
if (signataire.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers10['formaliteSignatureDate_TNS']          = date1;
}  
formFieldsPers10['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
}

// TNS GERANT 9 

var formFieldsPers11 = {}
var socialDirigeant9 = dirigeantAutre1.cadre7DeclarationSociale;
                                                                            
if (socialDirigeant9.voletSocialNumeroSecuriteSocialTNS != null) {

//cadre 1
var formFieldsPers11 = {}
formFieldsPers11['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers11['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers11['formulaireDependanceM0SotCiv_TNS']                                 = true;
formFieldsPers11['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers11['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers11['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers11['formulaireDependanceTNS_TNS']                                      = false;
  
//cadre 2  
                                                                                                   
formFieldsPers11['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 

formFieldsPers11['personneLieePPNomNaissance_TNS']                                   = dirigeantAutre1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeantAutre1.civilitePersonneMorale.personneLieePMDenomination);
formFieldsPers11['personneLieePPNomUsage_TNS']                                       = dirigeantAutre1.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre1.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '');
if (dirigeantAutre1.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre1.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre1.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre1.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers11['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers11['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers11['personneLieeQualite_TNS']                                          = dirigeantAutre1.personneLieeQualite != null ? dirigeantAutre1.personneLieeQualite : dirigeantAutre1.personneLieeQualiteBis;

var nirDeclarant = socialDirigeant9.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers11['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers11['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers11['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers11['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers11['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers11['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;;
if (Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
formFieldsPers11['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = true;
formFieldsPers11['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant9.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
}
if (socialDirigeant9.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers11['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant9.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers11['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant9.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers11['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant9.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant9.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant9.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers11['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers11['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant9.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant9.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers11['voletSocialActiviteSimultaneeOUI_TNS']                             = true;
formFieldsPers11['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant9.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers11['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant9.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers11['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant9.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
if (Value('id').of(socialDirigeant9.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
formFieldsPers11['voletSocialActiviteSimultaneeAutre_TNS']                           = true;
formFieldsPers11['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = socialDirigeant9.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}
formFieldsPers11['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant9.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers11['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant9.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers11['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant9.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers11['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant9.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;
                                                                                  
//Cadre 7  

formFieldsPers11['renseignementsComplementairesLeDeclarant_TNS']                     = (dirigeant1.dirigeantSignataire or dirigeant2.dirigeantSignataire or dirigeant3.dirigeantSignataire or dirigeant4.dirigeantSignataire or dirigeant5.dirigeantSignataire or dirigeant6.dirigeantSignataire or dirigeant7.dirigeantSignataire or dirigeant8.dirigeantSignataire or dirigeantAutre1.dirigeantSignataire or dirigeantAutre2.dirigeantSignataire or dirigeantAutre3.dirigeantSignataire or dirigeantAutre4.dirigeantSignataire or dirigeantAutre5.dirigeantSignataire or dirigeantAutre6.dirigeantSignataire or dirigeantAutre7.dirigeantSignataire or dirigeantAutre8.dirigeantSignataire or dirigeantAutre9.dirigeantSignataire or dirigeantAutre10.dirigeantSignataire or dirigeantAutre11.dirigeantSignataire or dirigeantAutre12.dirigeantSignataire) ? true : false;
if (not dirigeant1.dirigeantSignataire and not dirigeant2.dirigeantSignataire and not dirigeant3.dirigeantSignataire and not dirigeant4.dirigeantSignataire and not dirigeant5.dirigeantSignataire and not dirigeant6.dirigeantSignataire and not dirigeant7.dirigeantSignataire and not dirigeant8.dirigeantSignataire and not dirigeantAutre1.dirigeantSignataire and not dirigeantAutre2.dirigeantSignataire and not dirigeantAutre3.dirigeantSignataire and not dirigeantAutre4.dirigeantSignataire and not dirigeantAutre5.dirigeantSignataire and not dirigeantAutre6.dirigeantSignataire and not dirigeantAutre7.dirigeantSignataire and not dirigeantAutre8.dirigeantSignataire and not dirigeantAutre9.dirigeantSignataire and not dirigeantAutre10.dirigeantSignataire and not dirigeantAutre11.dirigeantSignataire and not dirigeantAutre12.dirigeantSignataire) {
formFieldsPers11['renseignementsComplementairesLeMandataire_TNS']                    = true;
formFieldsPers11['formaliteSignataireNomPrenomDenomination_TNS']	          			= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers11['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');	
formFieldsPers11['formaliteSignataireAdresseComplement_TNS']							= (signataire.adresseMandataire.codePostalMandataire != null ? signataire.adresseMandataire.codePostalMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.villeAdresseMandataire != null ? signataire.adresseMandataire.villeAdresseMandataire : '');
}
formFieldsPers11['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
if (signataire.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers11['formaliteSignatureDate_TNS']          = date1;
}  
formFieldsPers11['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
}

// TNS GERANT 10 

var formFieldsPers12 = {}
var socialDirigeant10 = dirigeantAutre2.cadre7DeclarationSociale;
                                                                            
if (socialDirigeant10.voletSocialNumeroSecuriteSocialTNS != null) {

//cadre 1
var formFieldsPers12 = {}
formFieldsPers12['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers12['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers12['formulaireDependanceM0SotCiv_TNS']                                 = true;
formFieldsPers12['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers12['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers12['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers12['formulaireDependanceTNS_TNS']                                      = false;
  
//cadre 2  
                                                                                                   
formFieldsPers12['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 

formFieldsPers12['personneLieePPNomNaissance_TNS']                                   = dirigeantAutre2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeantAutre2.civilitePersonneMorale.personneLieePMDenomination);
formFieldsPers12['personneLieePPNomUsage_TNS']                                       = dirigeantAutre2.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre2.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '');
if (dirigeantAutre2.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre2.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre2.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre2.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers12['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers12['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers12['personneLieeQualite_TNS']                                          = dirigeantAutre2.personneLieeQualite != null ? dirigeantAutre2.personneLieeQualite : dirigeantAutre2.personneLieeQualiteBis;

var nirDeclarant = socialDirigeant10.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers12['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers12['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers12['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers12['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers12['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers12['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;;
if (Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
formFieldsPers12['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = true;
formFieldsPers12['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant10.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
}
if (socialDirigeant10.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers12['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant10.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers12['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant10.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers12['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant10.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant10.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant10.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers12['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers12['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant10.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant10.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers12['voletSocialActiviteSimultaneeOUI_TNS']                             = true;
formFieldsPers12['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant10.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers12['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant10.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers12['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant10.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
if (Value('id').of(socialDirigeant10.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
formFieldsPers12['voletSocialActiviteSimultaneeAutre_TNS']                           = true;
formFieldsPers12['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = socialDirigeant10.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}
formFieldsPers12['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant10.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers12['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant10.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers12['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant10.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers12['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant10.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;
                                                                                  
//Cadre 7  

formFieldsPers12['renseignementsComplementairesLeDeclarant_TNS']                     = (dirigeant1.dirigeantSignataire or dirigeant2.dirigeantSignataire or dirigeant3.dirigeantSignataire or dirigeant4.dirigeantSignataire or dirigeant5.dirigeantSignataire or dirigeant6.dirigeantSignataire or dirigeant7.dirigeantSignataire or dirigeant8.dirigeantSignataire or dirigeantAutre1.dirigeantSignataire or dirigeantAutre2.dirigeantSignataire or dirigeantAutre3.dirigeantSignataire or dirigeantAutre4.dirigeantSignataire or dirigeantAutre5.dirigeantSignataire or dirigeantAutre6.dirigeantSignataire or dirigeantAutre7.dirigeantSignataire or dirigeantAutre8.dirigeantSignataire or dirigeantAutre9.dirigeantSignataire or dirigeantAutre10.dirigeantSignataire or dirigeantAutre11.dirigeantSignataire or dirigeantAutre12.dirigeantSignataire) ? true : false;
if (not dirigeant1.dirigeantSignataire and not dirigeant2.dirigeantSignataire and not dirigeant3.dirigeantSignataire and not dirigeant4.dirigeantSignataire and not dirigeant5.dirigeantSignataire and not dirigeant6.dirigeantSignataire and not dirigeant7.dirigeantSignataire and not dirigeant8.dirigeantSignataire and not dirigeantAutre1.dirigeantSignataire and not dirigeantAutre2.dirigeantSignataire and not dirigeantAutre3.dirigeantSignataire and not dirigeantAutre4.dirigeantSignataire and not dirigeantAutre5.dirigeantSignataire and not dirigeantAutre6.dirigeantSignataire and not dirigeantAutre7.dirigeantSignataire and not dirigeantAutre8.dirigeantSignataire and not dirigeantAutre9.dirigeantSignataire and not dirigeantAutre10.dirigeantSignataire and not dirigeantAutre11.dirigeantSignataire and not dirigeantAutre12.dirigeantSignataire) {
formFieldsPers12['renseignementsComplementairesLeMandataire_TNS']                    = true;
formFieldsPers12['formaliteSignataireNomPrenomDenomination_TNS']	          			= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers12['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');	
formFieldsPers12['formaliteSignataireAdresseComplement_TNS']							= (signataire.adresseMandataire.codePostalMandataire != null ? signataire.adresseMandataire.codePostalMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.villeAdresseMandataire != null ? signataire.adresseMandataire.villeAdresseMandataire : '');
}
formFieldsPers12['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
if (signataire.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers12['formaliteSignatureDate_TNS']          = date1;
}  
formFieldsPers12['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
}

// TNS GERANT 11

var formFieldsPers13 = {}
var socialDirigeant11 = dirigeantAutre3.cadre7DeclarationSociale;
                                                                            
if (socialDirigeant11.voletSocialNumeroSecuriteSocialTNS != null) {

//cadre 1
var formFieldsPers13 = {}
formFieldsPers13['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers13['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers13['formulaireDependanceM0SotCiv_TNS']                                 = true;
formFieldsPers13['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers13['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers13['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers13['formulaireDependanceTNS_TNS']                                      = false;
  
//cadre 2  
                                                                                                   
formFieldsPers13['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 

formFieldsPers13['personneLieePPNomNaissance_TNS']                                   = dirigeantAutre3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeantAutre3.civilitePersonneMorale.personneLieePMDenomination);
formFieldsPers13['personneLieePPNomUsage_TNS']                                       = dirigeantAutre3.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre3.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '');
if (dirigeantAutre3.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre3.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre3.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre3.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers13['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers13['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers13['personneLieeQualite_TNS']                                          = dirigeantAutre3.personneLieeQualite != null ? dirigeantAutre3.personneLieeQualite : dirigeantAutre3.personneLieeQualiteBis;

var nirDeclarant = socialDirigeant11.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers13['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers13['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers13['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers13['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers13['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers13['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;;
if (Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
formFieldsPers13['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = true;
formFieldsPers13['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant11.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
}
if (socialDirigeant11.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers13['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant11.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers13['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant11.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers13['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant11.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant11.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant11.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers13['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers13['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant11.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant11.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers13['voletSocialActiviteSimultaneeOUI_TNS']                             = true;
formFieldsPers13['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant11.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers13['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant11.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers13['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant11.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
if (Value('id').of(socialDirigeant11.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
formFieldsPers13['voletSocialActiviteSimultaneeAutre_TNS']                           = true;
formFieldsPers13['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = socialDirigeant11.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}
formFieldsPers13['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant11.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers13['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant11.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers13['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant11.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers13['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant11.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;
                                                                                  
//Cadre 7  

formFieldsPers13['renseignementsComplementairesLeDeclarant_TNS']                     = (dirigeant1.dirigeantSignataire or dirigeant2.dirigeantSignataire or dirigeant3.dirigeantSignataire or dirigeant4.dirigeantSignataire or dirigeant5.dirigeantSignataire or dirigeant6.dirigeantSignataire or dirigeant7.dirigeantSignataire or dirigeant8.dirigeantSignataire or dirigeantAutre1.dirigeantSignataire or dirigeantAutre2.dirigeantSignataire or dirigeantAutre3.dirigeantSignataire or dirigeantAutre4.dirigeantSignataire or dirigeantAutre5.dirigeantSignataire or dirigeantAutre6.dirigeantSignataire or dirigeantAutre7.dirigeantSignataire or dirigeantAutre8.dirigeantSignataire or dirigeantAutre9.dirigeantSignataire or dirigeantAutre10.dirigeantSignataire or dirigeantAutre11.dirigeantSignataire or dirigeantAutre12.dirigeantSignataire) ? true : false;
if (not dirigeant1.dirigeantSignataire and not dirigeant2.dirigeantSignataire and not dirigeant3.dirigeantSignataire and not dirigeant4.dirigeantSignataire and not dirigeant5.dirigeantSignataire and not dirigeant6.dirigeantSignataire and not dirigeant7.dirigeantSignataire and not dirigeant8.dirigeantSignataire and not dirigeantAutre1.dirigeantSignataire and not dirigeantAutre2.dirigeantSignataire and not dirigeantAutre3.dirigeantSignataire and not dirigeantAutre4.dirigeantSignataire and not dirigeantAutre5.dirigeantSignataire and not dirigeantAutre6.dirigeantSignataire and not dirigeantAutre7.dirigeantSignataire and not dirigeantAutre8.dirigeantSignataire and not dirigeantAutre9.dirigeantSignataire and not dirigeantAutre10.dirigeantSignataire and not dirigeantAutre11.dirigeantSignataire and not dirigeantAutre12.dirigeantSignataire) {
formFieldsPers13['renseignementsComplementairesLeMandataire_TNS']                    = true;
formFieldsPers13['formaliteSignataireNomPrenomDenomination_TNS']	          			= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers13['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');	
formFieldsPers13['formaliteSignataireAdresseComplement_TNS']							= (signataire.adresseMandataire.codePostalMandataire != null ? signataire.adresseMandataire.codePostalMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.villeAdresseMandataire != null ? signataire.adresseMandataire.villeAdresseMandataire : '');
}
formFieldsPers13['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
if (signataire.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers13['formaliteSignatureDate_TNS']          = date1;
}  
formFieldsPers13['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
}

// TNS GERANT 12

var formFieldsPers14 = {}
var socialDirigeant12 = dirigeantAutre4.cadre7DeclarationSociale;
                                                                            
if (socialDirigeant12.voletSocialNumeroSecuriteSocialTNS != null) {

//cadre 1
var formFieldsPers14 = {}
formFieldsPers14['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers14['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers14['formulaireDependanceM0SotCiv_TNS']                                 = true;
formFieldsPers14['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers14['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers14['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers14['formulaireDependanceTNS_TNS']                                      = false;
  
//cadre 2  
                                                                                                   
formFieldsPers14['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 

formFieldsPers14['personneLieePPNomNaissance_TNS']                                   = dirigeantAutre4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeantAutre4.civilitePersonneMorale.personneLieePMDenomination);
formFieldsPers14['personneLieePPNomUsage_TNS']                                       = dirigeantAutre4.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre4.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '');
if (dirigeantAutre4.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre4.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre4.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre4.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers14['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers14['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers14['personneLieeQualite_TNS']                                          = dirigeantAutre4.personneLieeQualite != null ? dirigeantAutre4.personneLieeQualite : dirigeantAutre4.personneLieeQualiteBis;

var nirDeclarant = socialDirigeant12.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers14['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers14['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers14['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers14['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers14['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers14['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;;
if (Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
formFieldsPers14['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = true;
formFieldsPers14['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant12.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
}
if (socialDirigeant12.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers14['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant12.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers14['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant12.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers14['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant12.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant12.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant12.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers14['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers14['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant12.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant12.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers14['voletSocialActiviteSimultaneeOUI_TNS']                             = true;
formFieldsPers14['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant12.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers14['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant12.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers14['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant12.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
if (Value('id').of(socialDirigeant12.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
formFieldsPers14['voletSocialActiviteSimultaneeAutre_TNS']                           = true;
formFieldsPers14['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = socialDirigeant12.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}
formFieldsPers14['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant12.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers14['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant12.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers14['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant12.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers14['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant12.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;
                                                                                  
//Cadre 7  

formFieldsPers14['renseignementsComplementairesLeDeclarant_TNS']                     = (dirigeant1.dirigeantSignataire or dirigeant2.dirigeantSignataire or dirigeant3.dirigeantSignataire or dirigeant4.dirigeantSignataire or dirigeant5.dirigeantSignataire or dirigeant6.dirigeantSignataire or dirigeant7.dirigeantSignataire or dirigeant8.dirigeantSignataire or dirigeantAutre1.dirigeantSignataire or dirigeantAutre2.dirigeantSignataire or dirigeantAutre3.dirigeantSignataire or dirigeantAutre4.dirigeantSignataire or dirigeantAutre5.dirigeantSignataire or dirigeantAutre6.dirigeantSignataire or dirigeantAutre7.dirigeantSignataire or dirigeantAutre8.dirigeantSignataire or dirigeantAutre9.dirigeantSignataire or dirigeantAutre10.dirigeantSignataire or dirigeantAutre11.dirigeantSignataire or dirigeantAutre12.dirigeantSignataire) ? true : false;
if (not dirigeant1.dirigeantSignataire and not dirigeant2.dirigeantSignataire and not dirigeant3.dirigeantSignataire and not dirigeant4.dirigeantSignataire and not dirigeant5.dirigeantSignataire and not dirigeant6.dirigeantSignataire and not dirigeant7.dirigeantSignataire and not dirigeant8.dirigeantSignataire and not dirigeantAutre1.dirigeantSignataire and not dirigeantAutre2.dirigeantSignataire and not dirigeantAutre3.dirigeantSignataire and not dirigeantAutre4.dirigeantSignataire and not dirigeantAutre5.dirigeantSignataire and not dirigeantAutre6.dirigeantSignataire and not dirigeantAutre7.dirigeantSignataire and not dirigeantAutre8.dirigeantSignataire and not dirigeantAutre9.dirigeantSignataire and not dirigeantAutre10.dirigeantSignataire and not dirigeantAutre11.dirigeantSignataire and not dirigeantAutre12.dirigeantSignataire) {
formFieldsPers14['renseignementsComplementairesLeMandataire_TNS']                    = true;
formFieldsPers14['formaliteSignataireNomPrenomDenomination_TNS']	          			= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers14['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');	
formFieldsPers14['formaliteSignataireAdresseComplement_TNS']							= (signataire.adresseMandataire.codePostalMandataire != null ? signataire.adresseMandataire.codePostalMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.villeAdresseMandataire != null ? signataire.adresseMandataire.villeAdresseMandataire : '');
}
formFieldsPers14['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
if (signataire.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers14['formaliteSignatureDate_TNS']          = date1;
}  
formFieldsPers14['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
}

// TNS GERANT 13

var formFieldsPers15 = {}
var socialDirigeant13 = dirigeantAutre5.cadre7DeclarationSociale;
                                                                            
if (socialDirigeant13.voletSocialNumeroSecuriteSocialTNS != null) {

//cadre 1
var formFieldsPers15 = {}
formFieldsPers15['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers15['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers15['formulaireDependanceM0SotCiv_TNS']                                 = true;
formFieldsPers15['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers15['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers15['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers15['formulaireDependanceTNS_TNS']                                      = false;
  
//cadre 2  
                                                                                                   
formFieldsPers15['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 

formFieldsPers15['personneLieePPNomNaissance_TNS']                                   = dirigeantAutre5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeantAutre5.civilitePersonneMorale.personneLieePMDenomination);
formFieldsPers15['personneLieePPNomUsage_TNS']                                       = dirigeantAutre5.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre5.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '');
if (dirigeantAutre5.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre5.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre5.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre5.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers15['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers15['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers15['personneLieeQualite_TNS']                                          = dirigeantAutre5.personneLieeQualite != null ? dirigeantAutre5.personneLieeQualite : dirigeantAutre5.personneLieeQualiteBis;

var nirDeclarant = socialDirigeant13.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers15['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers15['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers15['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant13.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers15['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant13.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers15['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant13.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers15['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant13.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;;
if (Value('id').of(socialDirigeant13.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
formFieldsPers15['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = true;
formFieldsPers15['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant13.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
}
if (socialDirigeant13.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers15['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant13.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers15['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant13.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers15['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant13.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant13.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant13.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers15['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers15['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant13.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant13.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers15['voletSocialActiviteSimultaneeOUI_TNS']                             = true;
formFieldsPers15['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant13.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers15['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant13.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers15['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant13.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
if (Value('id').of(socialDirigeant13.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
formFieldsPers15['voletSocialActiviteSimultaneeAutre_TNS']                           = true;
formFieldsPers15['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = socialDirigeant13.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}
formFieldsPers15['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant13.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers15['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant13.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers15['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant13.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers15['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant13.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;
                                                                                  
//Cadre 7  

formFieldsPers15['renseignementsComplementairesLeDeclarant_TNS']                     = (dirigeant1.dirigeantSignataire or dirigeant2.dirigeantSignataire or dirigeant3.dirigeantSignataire or dirigeant4.dirigeantSignataire or dirigeant5.dirigeantSignataire or dirigeant6.dirigeantSignataire or dirigeant7.dirigeantSignataire or dirigeant8.dirigeantSignataire or dirigeantAutre1.dirigeantSignataire or dirigeantAutre2.dirigeantSignataire or dirigeantAutre3.dirigeantSignataire or dirigeantAutre4.dirigeantSignataire or dirigeantAutre5.dirigeantSignataire or dirigeantAutre6.dirigeantSignataire or dirigeantAutre7.dirigeantSignataire or dirigeantAutre8.dirigeantSignataire or dirigeantAutre9.dirigeantSignataire or dirigeantAutre10.dirigeantSignataire or dirigeantAutre11.dirigeantSignataire or dirigeantAutre12.dirigeantSignataire) ? true : false;
if (not dirigeant1.dirigeantSignataire and not dirigeant2.dirigeantSignataire and not dirigeant3.dirigeantSignataire and not dirigeant4.dirigeantSignataire and not dirigeant5.dirigeantSignataire and not dirigeant6.dirigeantSignataire and not dirigeant7.dirigeantSignataire and not dirigeant8.dirigeantSignataire and not dirigeantAutre1.dirigeantSignataire and not dirigeantAutre2.dirigeantSignataire and not dirigeantAutre3.dirigeantSignataire and not dirigeantAutre4.dirigeantSignataire and not dirigeantAutre5.dirigeantSignataire and not dirigeantAutre6.dirigeantSignataire and not dirigeantAutre7.dirigeantSignataire and not dirigeantAutre8.dirigeantSignataire and not dirigeantAutre9.dirigeantSignataire and not dirigeantAutre10.dirigeantSignataire and not dirigeantAutre11.dirigeantSignataire and not dirigeantAutre12.dirigeantSignataire) {
formFieldsPers15['renseignementsComplementairesLeMandataire_TNS']                    = true;
formFieldsPers15['formaliteSignataireNomPrenomDenomination_TNS']	          			= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers15['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');	
formFieldsPers15['formaliteSignataireAdresseComplement_TNS']							= (signataire.adresseMandataire.codePostalMandataire != null ? signataire.adresseMandataire.codePostalMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.villeAdresseMandataire != null ? signataire.adresseMandataire.villeAdresseMandataire : '');
}
formFieldsPers15['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
if (signataire.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers15['formaliteSignatureDate_TNS']          = date1;
}  
formFieldsPers15['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
}

// TNS GERANT 14

var formFieldsPers16 = {}
var socialDirigeant14 = dirigeantAutre6.cadre7DeclarationSociale;
                                                                            
if (socialDirigeant14.voletSocialNumeroSecuriteSocialTNS != null) {

//cadre 1
var formFieldsPers16 = {}
formFieldsPers16['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers16['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers16['formulaireDependanceM0SotCiv_TNS']                                 = true;
formFieldsPers16['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers16['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers16['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers16['formulaireDependanceTNS_TNS']                                      = false;
  
//cadre 2  
                                                                                                   
formFieldsPers16['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 

formFieldsPers16['personneLieePPNomNaissance_TNS']                                   = dirigeantAutre6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeantAutre6.civilitePersonneMorale.personneLieePMDenomination);
formFieldsPers16['personneLieePPNomUsage_TNS']                                       = dirigeantAutre6.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre6.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '');
if (dirigeantAutre6.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre6.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre6.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre6.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers16['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers16['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers16['personneLieeQualite_TNS']                                          = dirigeantAutre6.personneLieeQualite != null ? dirigeantAutre6.personneLieeQualite : dirigeantAutre6.personneLieeQualiteBis;

var nirDeclarant = socialDirigeant14.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers16['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers16['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers16['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant14.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers16['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant14.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers16['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant14.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers16['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant14.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;;
if (Value('id').of(socialDirigeant14.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
formFieldsPers16['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = true;
formFieldsPers16['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant14.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
}
if (socialDirigeant14.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers16['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant14.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers16['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant14.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers16['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant14.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant14.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant14.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers16['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers16['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant14.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant14.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers16['voletSocialActiviteSimultaneeOUI_TNS']                             = true;
formFieldsPers16['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant14.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers16['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant14.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers16['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant14.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
if (Value('id').of(socialDirigeant14.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
formFieldsPers16['voletSocialActiviteSimultaneeAutre_TNS']                           = true;
formFieldsPers16['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = socialDirigeant14.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}
formFieldsPers16['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant14.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers16['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant14.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers16['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant14.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers16['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant14.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;
                                                                                  
//Cadre 7  

formFieldsPers16['renseignementsComplementairesLeDeclarant_TNS']                     = (dirigeant1.dirigeantSignataire or dirigeant2.dirigeantSignataire or dirigeant3.dirigeantSignataire or dirigeant4.dirigeantSignataire or dirigeant5.dirigeantSignataire or dirigeant6.dirigeantSignataire or dirigeant7.dirigeantSignataire or dirigeant8.dirigeantSignataire or dirigeantAutre1.dirigeantSignataire or dirigeantAutre2.dirigeantSignataire or dirigeantAutre3.dirigeantSignataire or dirigeantAutre4.dirigeantSignataire or dirigeantAutre5.dirigeantSignataire or dirigeantAutre6.dirigeantSignataire or dirigeantAutre7.dirigeantSignataire or dirigeantAutre8.dirigeantSignataire or dirigeantAutre9.dirigeantSignataire or dirigeantAutre10.dirigeantSignataire or dirigeantAutre11.dirigeantSignataire or dirigeantAutre12.dirigeantSignataire) ? true : false;
if (not dirigeant1.dirigeantSignataire and not dirigeant2.dirigeantSignataire and not dirigeant3.dirigeantSignataire and not dirigeant4.dirigeantSignataire and not dirigeant5.dirigeantSignataire and not dirigeant6.dirigeantSignataire and not dirigeant7.dirigeantSignataire and not dirigeant8.dirigeantSignataire and not dirigeantAutre1.dirigeantSignataire and not dirigeantAutre2.dirigeantSignataire and not dirigeantAutre3.dirigeantSignataire and not dirigeantAutre4.dirigeantSignataire and not dirigeantAutre5.dirigeantSignataire and not dirigeantAutre6.dirigeantSignataire and not dirigeantAutre7.dirigeantSignataire and not dirigeantAutre8.dirigeantSignataire and not dirigeantAutre9.dirigeantSignataire and not dirigeantAutre10.dirigeantSignataire and not dirigeantAutre11.dirigeantSignataire and not dirigeantAutre12.dirigeantSignataire) {
formFieldsPers16['renseignementsComplementairesLeMandataire_TNS']                    = true;
formFieldsPers16['formaliteSignataireNomPrenomDenomination_TNS']	          			= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers16['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');	
formFieldsPers16['formaliteSignataireAdresseComplement_TNS']							= (signataire.adresseMandataire.codePostalMandataire != null ? signataire.adresseMandataire.codePostalMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.villeAdresseMandataire != null ? signataire.adresseMandataire.villeAdresseMandataire : '');
}
formFieldsPers16['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
if (signataire.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers16['formaliteSignatureDate_TNS']          = date1;
}  
formFieldsPers16['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
}

// TNS GERANT 15

var formFieldsPers17 = {}
var socialDirigeant15 = dirigeantAutre7.cadre7DeclarationSociale;
                                                                            
if (socialDirigeant15.voletSocialNumeroSecuriteSocialTNS != null) {

//cadre 1
var formFieldsPers17 = {}
formFieldsPers17['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers17['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers17['formulaireDependanceM0SotCiv_TNS']                                 = true;
formFieldsPers17['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers17['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers17['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers17['formulaireDependanceTNS_TNS']                                      = false;
  
//cadre 2  
                                                                                                   
formFieldsPers17['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 

formFieldsPers17['personneLieePPNomNaissance_TNS']                                   = dirigeantAutre7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeantAutre7.civilitePersonneMorale.personneLieePMDenomination);
formFieldsPers17['personneLieePPNomUsage_TNS']                                       = dirigeantAutre7.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre7.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '');
if (dirigeantAutre7.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre7.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre7.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre7.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers17['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers17['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers17['personneLieeQualite_TNS']                                          = dirigeantAutre7.personneLieeQualite != null ? dirigeantAutre7.personneLieeQualite : dirigeantAutre7.personneLieeQualiteBis;

var nirDeclarant = socialDirigeant15.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers17['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers17['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers17['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant15.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers17['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant15.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers17['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant15.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers17['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant15.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;;
if (Value('id').of(socialDirigeant15.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
formFieldsPers17['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = true;
formFieldsPers17['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant15.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
}
if (socialDirigeant15.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers17['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant15.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers17['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant15.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers17['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant15.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant15.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant15.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers17['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers17['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant15.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant15.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers17['voletSocialActiviteSimultaneeOUI_TNS']                             = true;
formFieldsPers17['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant15.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers17['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant15.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers17['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant15.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
if (Value('id').of(socialDirigeant15.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
formFieldsPers17['voletSocialActiviteSimultaneeAutre_TNS']                           = true;
formFieldsPers17['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = socialDirigeant15.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}
formFieldsPers17['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant15.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers17['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant15.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers17['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant15.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers17['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant15.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;
                                                                                  
//Cadre 7  

formFieldsPers17['renseignementsComplementairesLeDeclarant_TNS']                     = (dirigeant1.dirigeantSignataire or dirigeant2.dirigeantSignataire or dirigeant3.dirigeantSignataire or dirigeant4.dirigeantSignataire or dirigeant5.dirigeantSignataire or dirigeant6.dirigeantSignataire or dirigeant7.dirigeantSignataire or dirigeant8.dirigeantSignataire or dirigeantAutre1.dirigeantSignataire or dirigeantAutre2.dirigeantSignataire or dirigeantAutre3.dirigeantSignataire or dirigeantAutre4.dirigeantSignataire or dirigeantAutre5.dirigeantSignataire or dirigeantAutre6.dirigeantSignataire or dirigeantAutre7.dirigeantSignataire or dirigeantAutre8.dirigeantSignataire or dirigeantAutre9.dirigeantSignataire or dirigeantAutre10.dirigeantSignataire or dirigeantAutre11.dirigeantSignataire or dirigeantAutre12.dirigeantSignataire) ? true : false;
if (not dirigeant1.dirigeantSignataire and not dirigeant2.dirigeantSignataire and not dirigeant3.dirigeantSignataire and not dirigeant4.dirigeantSignataire and not dirigeant5.dirigeantSignataire and not dirigeant6.dirigeantSignataire and not dirigeant7.dirigeantSignataire and not dirigeant8.dirigeantSignataire and not dirigeantAutre1.dirigeantSignataire and not dirigeantAutre2.dirigeantSignataire and not dirigeantAutre3.dirigeantSignataire and not dirigeantAutre4.dirigeantSignataire and not dirigeantAutre5.dirigeantSignataire and not dirigeantAutre6.dirigeantSignataire and not dirigeantAutre7.dirigeantSignataire and not dirigeantAutre8.dirigeantSignataire and not dirigeantAutre9.dirigeantSignataire and not dirigeantAutre10.dirigeantSignataire and not dirigeantAutre11.dirigeantSignataire and not dirigeantAutre12.dirigeantSignataire) {
formFieldsPers17['renseignementsComplementairesLeMandataire_TNS']                    = true;
formFieldsPers17['formaliteSignataireNomPrenomDenomination_TNS']	          			= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers17['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');	
formFieldsPers17['formaliteSignataireAdresseComplement_TNS']							= (signataire.adresseMandataire.codePostalMandataire != null ? signataire.adresseMandataire.codePostalMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.villeAdresseMandataire != null ? signataire.adresseMandataire.villeAdresseMandataire : '');
}
formFieldsPers17['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
if (signataire.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers17['formaliteSignatureDate_TNS']          = date1;
}  
formFieldsPers17['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
}

// TNS GERANT 16

var formFieldsPers18 = {}
var socialDirigeant16 = dirigeantAutre8.cadre7DeclarationSociale;
                                                                            
if (socialDirigeant16.voletSocialNumeroSecuriteSocialTNS != null) {

//cadre 1
var formFieldsPers18 = {}
formFieldsPers18['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers18['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers18['formulaireDependanceM0SotCiv_TNS']                                 = true;
formFieldsPers18['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers18['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers18['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers18['formulaireDependanceTNS_TNS']                                      = false;
  
//cadre 2  
                                                                                                   
formFieldsPers18['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 

formFieldsPers18['personneLieePPNomNaissance_TNS']                                   = dirigeantAutre8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeantAutre8.civilitePersonneMorale.personneLieePMDenomination);
formFieldsPers18['personneLieePPNomUsage_TNS']                                       = dirigeantAutre8.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre8.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '');
if (dirigeantAutre8.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre8.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre8.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre8.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers18['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers18['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers18['personneLieeQualite_TNS']                                          = dirigeantAutre8.personneLieeQualite != null ? dirigeantAutre8.personneLieeQualite : dirigeantAutre8.personneLieeQualiteBis;

var nirDeclarant = socialDirigeant16.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers18['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers18['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers18['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant16.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers18['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant16.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers18['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant16.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers18['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant16.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;;
if (Value('id').of(socialDirigeant16.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
formFieldsPers18['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = true;
formFieldsPers18['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant16.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
}
if (socialDirigeant16.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers18['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant16.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers18['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant16.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers18['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant16.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant16.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant16.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers18['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers18['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant16.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant16.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers18['voletSocialActiviteSimultaneeOUI_TNS']                             = true;
formFieldsPers18['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant16.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers18['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant16.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers18['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant16.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
if (Value('id').of(socialDirigeant16.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
formFieldsPers18['voletSocialActiviteSimultaneeAutre_TNS']                           = true;
formFieldsPers18['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = socialDirigeant16.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}
formFieldsPers18['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant16.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers18['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant16.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers18['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant16.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers18['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant16.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;
                                                                                  
//Cadre 7  

formFieldsPers18['renseignementsComplementairesLeDeclarant_TNS']                     = (dirigeant1.dirigeantSignataire or dirigeant2.dirigeantSignataire or dirigeant3.dirigeantSignataire or dirigeant4.dirigeantSignataire or dirigeant5.dirigeantSignataire or dirigeant6.dirigeantSignataire or dirigeant7.dirigeantSignataire or dirigeant8.dirigeantSignataire or dirigeantAutre1.dirigeantSignataire or dirigeantAutre2.dirigeantSignataire or dirigeantAutre3.dirigeantSignataire or dirigeantAutre4.dirigeantSignataire or dirigeantAutre5.dirigeantSignataire or dirigeantAutre6.dirigeantSignataire or dirigeantAutre7.dirigeantSignataire or dirigeantAutre8.dirigeantSignataire or dirigeantAutre9.dirigeantSignataire or dirigeantAutre10.dirigeantSignataire or dirigeantAutre11.dirigeantSignataire or dirigeantAutre12.dirigeantSignataire) ? true : false;
if (not dirigeant1.dirigeantSignataire and not dirigeant2.dirigeantSignataire and not dirigeant3.dirigeantSignataire and not dirigeant4.dirigeantSignataire and not dirigeant5.dirigeantSignataire and not dirigeant6.dirigeantSignataire and not dirigeant7.dirigeantSignataire and not dirigeant8.dirigeantSignataire and not dirigeantAutre1.dirigeantSignataire and not dirigeantAutre2.dirigeantSignataire and not dirigeantAutre3.dirigeantSignataire and not dirigeantAutre4.dirigeantSignataire and not dirigeantAutre5.dirigeantSignataire and not dirigeantAutre6.dirigeantSignataire and not dirigeantAutre7.dirigeantSignataire and not dirigeantAutre8.dirigeantSignataire and not dirigeantAutre9.dirigeantSignataire and not dirigeantAutre10.dirigeantSignataire and not dirigeantAutre11.dirigeantSignataire and not dirigeantAutre12.dirigeantSignataire) {
formFieldsPers18['renseignementsComplementairesLeMandataire_TNS']                    = true;
formFieldsPers18['formaliteSignataireNomPrenomDenomination_TNS']	          			= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers18['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');	
formFieldsPers18['formaliteSignataireAdresseComplement_TNS']							= (signataire.adresseMandataire.codePostalMandataire != null ? signataire.adresseMandataire.codePostalMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.villeAdresseMandataire != null ? signataire.adresseMandataire.villeAdresseMandataire : '');
}
formFieldsPers18['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
if (signataire.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers18['formaliteSignatureDate_TNS']          = date1;
}  
formFieldsPers18['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
}

// TNS GERANT 17

var formFieldsPers19 = {}
var socialDirigeant17 = dirigeantAutre9.cadre7DeclarationSociale;
                                                                            
if (socialDirigeant17.voletSocialNumeroSecuriteSocialTNS != null) {

//cadre 1
var formFieldsPers19 = {}
formFieldsPers19['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers19['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers19['formulaireDependanceM0SotCiv_TNS']                                 = true;
formFieldsPers19['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers19['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers19['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers19['formulaireDependanceTNS_TNS']                                      = false;
  
//cadre 2  
                                                                                                   
formFieldsPers19['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 

formFieldsPers19['personneLieePPNomNaissance_TNS']                                   = dirigeantAutre9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeantAutre9.civilitePersonneMorale.personneLieePMDenomination);
formFieldsPers19['personneLieePPNomUsage_TNS']                                       = dirigeantAutre9.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre9.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '');
if (dirigeantAutre9.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre9.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre9.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre9.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers19['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers19['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers19['personneLieeQualite_TNS']                                          = dirigeantAutre9.personneLieeQualite != null ? dirigeantAutre9.personneLieeQualite : dirigeantAutre9.personneLieeQualiteBis;

var nirDeclarant = socialDirigeant17.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers19['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers19['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers19['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant17.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers19['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant17.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers19['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant17.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers19['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant17.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;;
if (Value('id').of(socialDirigeant17.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
formFieldsPers19['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = true;
formFieldsPers19['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant17.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
}
if (socialDirigeant17.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers19['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant17.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers19['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant17.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers19['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant17.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant17.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant17.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers19['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers19['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant17.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant17.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers19['voletSocialActiviteSimultaneeOUI_TNS']                             = true;
formFieldsPers19['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant17.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers19['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant17.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers19['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant17.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
if (Value('id').of(socialDirigeant17.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
formFieldsPers19['voletSocialActiviteSimultaneeAutre_TNS']                           = true;
formFieldsPers19['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = socialDirigeant17.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}
formFieldsPers19['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant17.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers19['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant17.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers19['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant17.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers19['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant17.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;
                                                                                  
//Cadre 7  

formFieldsPers19['renseignementsComplementairesLeDeclarant_TNS']                     = (dirigeant1.dirigeantSignataire or dirigeant2.dirigeantSignataire or dirigeant3.dirigeantSignataire or dirigeant4.dirigeantSignataire or dirigeant5.dirigeantSignataire or dirigeant6.dirigeantSignataire or dirigeant7.dirigeantSignataire or dirigeant8.dirigeantSignataire or dirigeantAutre1.dirigeantSignataire or dirigeantAutre2.dirigeantSignataire or dirigeantAutre3.dirigeantSignataire or dirigeantAutre4.dirigeantSignataire or dirigeantAutre5.dirigeantSignataire or dirigeantAutre6.dirigeantSignataire or dirigeantAutre7.dirigeantSignataire or dirigeantAutre8.dirigeantSignataire or dirigeantAutre9.dirigeantSignataire or dirigeantAutre10.dirigeantSignataire or dirigeantAutre11.dirigeantSignataire or dirigeantAutre12.dirigeantSignataire) ? true : false;
if (not dirigeant1.dirigeantSignataire and not dirigeant2.dirigeantSignataire and not dirigeant3.dirigeantSignataire and not dirigeant4.dirigeantSignataire and not dirigeant5.dirigeantSignataire and not dirigeant6.dirigeantSignataire and not dirigeant7.dirigeantSignataire and not dirigeant8.dirigeantSignataire and not dirigeantAutre1.dirigeantSignataire and not dirigeantAutre2.dirigeantSignataire and not dirigeantAutre3.dirigeantSignataire and not dirigeantAutre4.dirigeantSignataire and not dirigeantAutre5.dirigeantSignataire and not dirigeantAutre6.dirigeantSignataire and not dirigeantAutre7.dirigeantSignataire and not dirigeantAutre8.dirigeantSignataire and not dirigeantAutre9.dirigeantSignataire and not dirigeantAutre10.dirigeantSignataire and not dirigeantAutre11.dirigeantSignataire and not dirigeantAutre12.dirigeantSignataire) {
formFieldsPers19['renseignementsComplementairesLeMandataire_TNS']                    = true;
formFieldsPers19['formaliteSignataireNomPrenomDenomination_TNS']	          			= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers19['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');	
formFieldsPers19['formaliteSignataireAdresseComplement_TNS']							= (signataire.adresseMandataire.codePostalMandataire != null ? signataire.adresseMandataire.codePostalMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.villeAdresseMandataire != null ? signataire.adresseMandataire.villeAdresseMandataire : '');
}
formFieldsPers19['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
if (signataire.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers19['formaliteSignatureDate_TNS']          = date1;
}  
formFieldsPers19['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
}

// TNS GERANT 18

var formFieldsPers20 = {}
var socialDirigeant18 = dirigeantAutre10.cadre7DeclarationSociale;
                                                                            
if (socialDirigeant18.voletSocialNumeroSecuriteSocialTNS != null) {

//cadre 1
var formFieldsPers20 = {}
formFieldsPers20['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers20['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers20['formulaireDependanceM0SotCiv_TNS']                                 = true;
formFieldsPers20['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers20['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers20['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers20['formulaireDependanceTNS_TNS']                                      = false;
  
//cadre 2  
                                                                                                   
formFieldsPers20['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 

formFieldsPers20['personneLieePPNomNaissance_TNS']                                   = dirigeantAutre10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeantAutre10.civilitePersonneMorale.personneLieePMDenomination);
formFieldsPers20['personneLieePPNomUsage_TNS']                                       = dirigeantAutre10.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre10.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '');
if (dirigeantAutre10.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre10.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre10.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre10.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers20['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers20['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers20['personneLieeQualite_TNS']                                          = dirigeantAutre10.personneLieeQualite != null ? dirigeantAutre10.personneLieeQualite : dirigeantAutre10.personneLieeQualiteBis;

var nirDeclarant = socialDirigeant18.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers20['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers20['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers20['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant18.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers20['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant18.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers20['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant18.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers20['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant18.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;;
if (Value('id').of(socialDirigeant18.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
formFieldsPers20['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = true;
formFieldsPers20['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant18.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
}
if (socialDirigeant18.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers20['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant18.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers20['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant18.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers20['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant18.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant18.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant18.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers20['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers20['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant18.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant18.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers20['voletSocialActiviteSimultaneeOUI_TNS']                             = true;
formFieldsPers20['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant18.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers20['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant18.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers20['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant18.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
if (Value('id').of(socialDirigeant18.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
formFieldsPers20['voletSocialActiviteSimultaneeAutre_TNS']                           = true;
formFieldsPers20['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = socialDirigeant18.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}
formFieldsPers20['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant18.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers20['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant18.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers20['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant18.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers20['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant18.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;
                                                                                  
//Cadre 7  

formFieldsPers20['renseignementsComplementairesLeDeclarant_TNS']                     = (dirigeant1.dirigeantSignataire or dirigeant2.dirigeantSignataire or dirigeant3.dirigeantSignataire or dirigeant4.dirigeantSignataire or dirigeant5.dirigeantSignataire or dirigeant6.dirigeantSignataire or dirigeant7.dirigeantSignataire or dirigeant8.dirigeantSignataire or dirigeantAutre1.dirigeantSignataire or dirigeantAutre2.dirigeantSignataire or dirigeantAutre3.dirigeantSignataire or dirigeantAutre4.dirigeantSignataire or dirigeantAutre5.dirigeantSignataire or dirigeantAutre6.dirigeantSignataire or dirigeantAutre7.dirigeantSignataire or dirigeantAutre8.dirigeantSignataire or dirigeantAutre9.dirigeantSignataire or dirigeantAutre10.dirigeantSignataire or dirigeantAutre11.dirigeantSignataire or dirigeantAutre12.dirigeantSignataire) ? true : false;
if (not dirigeant1.dirigeantSignataire and not dirigeant2.dirigeantSignataire and not dirigeant3.dirigeantSignataire and not dirigeant4.dirigeantSignataire and not dirigeant5.dirigeantSignataire and not dirigeant6.dirigeantSignataire and not dirigeant7.dirigeantSignataire and not dirigeant8.dirigeantSignataire and not dirigeantAutre1.dirigeantSignataire and not dirigeantAutre2.dirigeantSignataire and not dirigeantAutre3.dirigeantSignataire and not dirigeantAutre4.dirigeantSignataire and not dirigeantAutre5.dirigeantSignataire and not dirigeantAutre6.dirigeantSignataire and not dirigeantAutre7.dirigeantSignataire and not dirigeantAutre8.dirigeantSignataire and not dirigeantAutre9.dirigeantSignataire and not dirigeantAutre10.dirigeantSignataire and not dirigeantAutre11.dirigeantSignataire and not dirigeantAutre12.dirigeantSignataire) {
formFieldsPers20['renseignementsComplementairesLeMandataire_TNS']                    = true;
formFieldsPers20['formaliteSignataireNomPrenomDenomination_TNS']	          			= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers20['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');	
formFieldsPers20['formaliteSignataireAdresseComplement_TNS']							= (signataire.adresseMandataire.codePostalMandataire != null ? signataire.adresseMandataire.codePostalMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.villeAdresseMandataire != null ? signataire.adresseMandataire.villeAdresseMandataire : '');
}
formFieldsPers20['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
if (signataire.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers20['formaliteSignatureDate_TNS']          = date1;
}  
formFieldsPers20['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
}

// TNS GERANT 19

var formFieldsPers21 = {}
var socialDirigeant19 = dirigeantAutre11.cadre7DeclarationSociale;
                                                                            
if (socialDirigeant19.voletSocialNumeroSecuriteSocialTNS != null) {

//cadre 1
var formFieldsPers21 = {}
formFieldsPers21['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers21['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers21['formulaireDependanceM0SotCiv_TNS']                                 = true;
formFieldsPers21['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers21['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers21['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers21['formulaireDependanceTNS_TNS']                                      = false;
  
//cadre 2  
                                                                                                   
formFieldsPers21['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 

formFieldsPers21['personneLieePPNomNaissance_TNS']                                   = dirigeantAutre11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeantAutre11.civilitePersonneMorale.personneLieePMDenomination);
formFieldsPers21['personneLieePPNomUsage_TNS']                                       = dirigeantAutre11.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre11.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '');
if (dirigeantAutre11.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre11.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre11.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre11.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers21['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers21['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers21['personneLieeQualite_TNS']                                          = dirigeantAutre11.personneLieeQualite != null ? dirigeantAutre11.personneLieeQualite : dirigeantAutre11.personneLieeQualiteBis;

var nirDeclarant = socialDirigeant19.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers21['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers21['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers21['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant19.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers21['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant19.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers21['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant19.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers21['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant19.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;;
if (Value('id').of(socialDirigeant19.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
formFieldsPers21['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = true;
formFieldsPers21['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant19.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
}
if (socialDirigeant19.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers21['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant19.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers21['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant19.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers21['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant19.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant19.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant19.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers21['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers21['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant19.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant19.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers21['voletSocialActiviteSimultaneeOUI_TNS']                             = true;
formFieldsPers21['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant19.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers21['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant19.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers21['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant19.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
if (Value('id').of(socialDirigeant19.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
formFieldsPers21['voletSocialActiviteSimultaneeAutre_TNS']                           = true;
formFieldsPers21['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = socialDirigeant19.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}
formFieldsPers21['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant19.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers21['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant19.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers21['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant19.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers21['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant19.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;
                                                                                  
//Cadre 7  

formFieldsPers21['renseignementsComplementairesLeDeclarant_TNS']                     = (dirigeant1.dirigeantSignataire or dirigeant2.dirigeantSignataire or dirigeant3.dirigeantSignataire or dirigeant4.dirigeantSignataire or dirigeant5.dirigeantSignataire or dirigeant6.dirigeantSignataire or dirigeant7.dirigeantSignataire or dirigeant8.dirigeantSignataire or dirigeantAutre1.dirigeantSignataire or dirigeantAutre2.dirigeantSignataire or dirigeantAutre3.dirigeantSignataire or dirigeantAutre4.dirigeantSignataire or dirigeantAutre5.dirigeantSignataire or dirigeantAutre6.dirigeantSignataire or dirigeantAutre7.dirigeantSignataire or dirigeantAutre8.dirigeantSignataire or dirigeantAutre9.dirigeantSignataire or dirigeantAutre10.dirigeantSignataire or dirigeantAutre11.dirigeantSignataire or dirigeantAutre12.dirigeantSignataire) ? true : false;
if (not dirigeant1.dirigeantSignataire and not dirigeant2.dirigeantSignataire and not dirigeant3.dirigeantSignataire and not dirigeant4.dirigeantSignataire and not dirigeant5.dirigeantSignataire and not dirigeant6.dirigeantSignataire and not dirigeant7.dirigeantSignataire and not dirigeant8.dirigeantSignataire and not dirigeantAutre1.dirigeantSignataire and not dirigeantAutre2.dirigeantSignataire and not dirigeantAutre3.dirigeantSignataire and not dirigeantAutre4.dirigeantSignataire and not dirigeantAutre5.dirigeantSignataire and not dirigeantAutre6.dirigeantSignataire and not dirigeantAutre7.dirigeantSignataire and not dirigeantAutre8.dirigeantSignataire and not dirigeantAutre9.dirigeantSignataire and not dirigeantAutre10.dirigeantSignataire and not dirigeantAutre11.dirigeantSignataire and not dirigeantAutre12.dirigeantSignataire) {
formFieldsPers21['renseignementsComplementairesLeMandataire_TNS']                    = true;
formFieldsPers21['formaliteSignataireNomPrenomDenomination_TNS']	          			= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers21['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');	
formFieldsPers21['formaliteSignataireAdresseComplement_TNS']							= (signataire.adresseMandataire.codePostalMandataire != null ? signataire.adresseMandataire.codePostalMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.villeAdresseMandataire != null ? signataire.adresseMandataire.villeAdresseMandataire : '');
}
formFieldsPers21['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
if (signataire.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers21['formaliteSignatureDate_TNS']          = date1;
}  
formFieldsPers21['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
}

// TNS GERANT 20

var formFieldsPers22 = {}
var socialDirigeant20 = dirigeantAutre12.cadre7DeclarationSociale;
                                                                            
if (socialDirigeant20.voletSocialNumeroSecuriteSocialTNS != null) {

//cadre 1
var formFieldsPers22 = {}
formFieldsPers22['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers22['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers22['formulaireDependanceM0SotCiv_TNS']                                 = true;
formFieldsPers22['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers22['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers22['formulaireDependanceM3SARL_TNS']                                   = false;
formFieldsPers22['formulaireDependanceTNS_TNS']                                      = false;
  
//cadre 2  
                                                                                                   
formFieldsPers22['entrepriseDenomination_TNS']                                            = societe.entrepriseDenomination;
                                                                                                                
//cadre 3 

formFieldsPers22['personneLieePPNomNaissance_TNS']                                   = dirigeantAutre12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantAutre12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : dirigeantAutre12.civilitePersonneMorale.personneLieePMDenomination);
formFieldsPers22['personneLieePPNomUsage_TNS']                                       = dirigeantAutre12.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantAutre12.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '');
if (dirigeantAutre12.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantAutre12.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre12.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre12.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers22['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantAutre12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers22['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers22['personneLieeQualite_TNS']                                          = dirigeantAutre12.personneLieeQualite != null ? dirigeantAutre12.personneLieeQualite : dirigeantAutre12.personneLieeQualiteBis;

var nirDeclarant = socialDirigeant20.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers22['voletSocialNumeroSecuriteSocial_TNS']    = nirDeclarant.substring(0, 13);
    formFieldsPers22['voletSocialNumeroSecuriteSocialCle_TNS'] = nirDeclarant.substring(13, 15);
}
formFieldsPers22['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant20.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;																					
formFieldsPers22['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant20.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers22['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant20.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers22['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant20.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;;
if (Value('id').of(socialDirigeant20.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
formFieldsPers22['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = true;
formFieldsPers22['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = socialDirigeant20.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
}
if (socialDirigeant20.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers22['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant20.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers22['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant20.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers22['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant20.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant20.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null ) {
	var dateTmp = new Date(parseInt(socialDirigeant20.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers22['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers22['voletSocialActiviteSimultaneeNON_TNS']                             = socialDirigeant20.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant20.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers22['voletSocialActiviteSimultaneeOUI_TNS']                             = true;
formFieldsPers22['voletSocialActiviteSimultaneeSalarie_TNS']                         = Value('id').of(socialDirigeant20.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers22['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                 = Value('id').of(socialDirigeant20.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers22['voletSocialActiviteSimultaneeRetraitePensionne_TNS']               = Value('id').of(socialDirigeant20.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
if (Value('id').of(socialDirigeant20.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
formFieldsPers22['voletSocialActiviteSimultaneeAutre_TNS']                           = true;
formFieldsPers22['voletSocialActiviteSimultaneeAutrePrecision_TNS']                  = socialDirigeant20.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}
formFieldsPers22['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant20.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers22['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant20.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers22['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']            = Value('id').of(socialDirigeant20.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers22['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']            = Value('id').of(socialDirigeant20.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;
                                                                                  
//Cadre 7  

formFieldsPers22['renseignementsComplementairesLeDeclarant_TNS']                     = (dirigeant1.dirigeantSignataire or dirigeant2.dirigeantSignataire or dirigeant3.dirigeantSignataire or dirigeant4.dirigeantSignataire or dirigeant5.dirigeantSignataire or dirigeant6.dirigeantSignataire or dirigeant7.dirigeantSignataire or dirigeant8.dirigeantSignataire or dirigeantAutre1.dirigeantSignataire or dirigeantAutre2.dirigeantSignataire or dirigeantAutre3.dirigeantSignataire or dirigeantAutre4.dirigeantSignataire or dirigeantAutre5.dirigeantSignataire or dirigeantAutre6.dirigeantSignataire or dirigeantAutre7.dirigeantSignataire or dirigeantAutre8.dirigeantSignataire or dirigeantAutre9.dirigeantSignataire or dirigeantAutre10.dirigeantSignataire or dirigeantAutre11.dirigeantSignataire or dirigeantAutre12.dirigeantSignataire) ? true : false;
if (not dirigeant1.dirigeantSignataire and not dirigeant2.dirigeantSignataire and not dirigeant3.dirigeantSignataire and not dirigeant4.dirigeantSignataire and not dirigeant5.dirigeantSignataire and not dirigeant6.dirigeantSignataire and not dirigeant7.dirigeantSignataire and not dirigeant8.dirigeantSignataire and not dirigeantAutre1.dirigeantSignataire and not dirigeantAutre2.dirigeantSignataire and not dirigeantAutre3.dirigeantSignataire and not dirigeantAutre4.dirigeantSignataire and not dirigeantAutre5.dirigeantSignataire and not dirigeantAutre6.dirigeantSignataire and not dirigeantAutre7.dirigeantSignataire and not dirigeantAutre8.dirigeantSignataire and not dirigeantAutre9.dirigeantSignataire and not dirigeantAutre10.dirigeantSignataire and not dirigeantAutre11.dirigeantSignataire and not dirigeantAutre12.dirigeantSignataire) {
formFieldsPers22['renseignementsComplementairesLeMandataire_TNS']                    = true;
formFieldsPers22['formaliteSignataireNomPrenomDenomination_TNS']	          			= signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers22['formaliteSignataireAdresse_TNS']                                   = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');	
formFieldsPers22['formaliteSignataireAdresseComplement_TNS']							= (signataire.adresseMandataire.codePostalMandataire != null ? signataire.adresseMandataire.codePostalMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.villeAdresseMandataire != null ? signataire.adresseMandataire.villeAdresseMandataire : '');
}
formFieldsPers22['formaliteSignatureLieu_TNS']                                       = signataire.formaliteSignatureLieu;
if (signataire.formaliteSignatureDate != null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers22['formaliteSignatureDate_TNS']          = date1;
}  
formFieldsPers22['signature_TNS']                                                    = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce.";
}

// Intercalaire NDI

// Cadre 1

formFields['complete_immatriculation_ndi']                   = true;
formFields['complete_modification_ndi']                      = false;

// Cadre 2

formFields['denomination_ndi']                               = societe.entrepriseDenomination;
formFields['adresseSiege_voie_ndi']                          = (siege.cadreAdresseProfessionnelle.numeroAdresseSiege != null ? siege.cadreAdresseProfessionnelle.numeroAdresseSiege : '')
															 + ' ' + (siege.cadreAdresseProfessionnelle.indiceAdresseSiege != null ? siege.cadreAdresseProfessionnelle.indiceAdresseSiege : '')
															 + ' ' + (siege.cadreAdresseProfessionnelle.typeAdresseSiege != null ? siege.cadreAdresseProfessionnelle.typeAdresseSiege : '')
															 +' ' + siege.cadreAdresseProfessionnelle.voieAdresseSiege
															 + ' ' + (siege.cadreAdresseProfessionnelle.complementAdresseSiege != null ? siege.cadreAdresseProfessionnelle.complementAdresseSiege : '')
															 + ' ' + (siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege != null ? siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege : '');
formFields['adresseSiege_codePostal_ndi']                    = siege.cadreAdresseProfessionnelle.codePostalAdresseSiege;
formFields['adresseSiege_commune_ndi']                       = siege.cadreAdresseProfessionnelle.communeAdresseSiege;

// Cadre 3
if(etablissement.etablissementNomDomaine != null) {
if(etablissement.etablissementDateDebutActivite != null) {
	var dateTmp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['dateDebutActivite_ndi'] = date;
} else if (signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['dateDebutActivite_ndi'] = date;
}

formFields['adresseEtablissement_voie_ndi']                          = (siege.formaliteCreationAvecActivite and siege.etablissementPrincipalDifferentSiege) ?
																	((etablissement.cadreAdresseEtablissement.numeroAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.numeroAdresseEtablissement : '')
																	+ ' ' + (etablissement.cadreAdresseEtablissement.indiceAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.indiceAdresseEtablissement : '')
																	+ ' ' + (etablissement.cadreAdresseEtablissement.typeAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.typeAdresseEtablissement : '')
																	+ ' ' + etablissement.cadreAdresseEtablissement.voieAdresseEtablissement
																	+ ' ' + (etablissement.cadreAdresseEtablissement.complementAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.complementAdresseEtablissement : '')
																	+ ' ' + (etablissement.cadreAdresseEtablissement.distributionSpecialeAdresseEtablissement != null ? etablissement.cadreAdresseEtablissement.distributionSpecialeAdresseEtablissement : '')) : 
																	((siege.cadreAdresseProfessionnelle.numeroAdresseSiege != null ? siege.cadreAdresseProfessionnelle.numeroAdresseSiege : '')
																	+ ' ' + (siege.cadreAdresseProfessionnelle.indiceAdresseSiege != null ? siege.cadreAdresseProfessionnelle.indiceAdresseSiege : '')
																	+ ' ' + (siege.cadreAdresseProfessionnelle.typeAdresseSiege != null ? siege.cadreAdresseProfessionnelle.typeAdresseSiege : '')
																	+' ' + siege.cadreAdresseProfessionnelle.voieAdresseSiege
																	+ ' ' + (siege.cadreAdresseProfessionnelle.complementAdresseSiege != null ? siege.cadreAdresseProfessionnelle.complementAdresseSiege : '')
																	+ ' ' + (siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege != null ? siege.cadreAdresseProfessionnelle.distributionSpecialeAdresseSiege : ''));
formFields['adresseEtablissement_codePostal_ndi']                   = (siege.formaliteCreationAvecActivite or Value('id').of(societe.entrepriseFormeJuridique).eq('5785') or Value('id').of(societe.entrepriseFormeJuridique).eq('5585') or Value('id').of(societe.entrepriseFormeJuridique).eq('5685') or Value('id').of(societe.entrepriseFormeJuridique).eq('5385') or Value('id').of(societe.entrepriseFormeJuridique).eq('3110') or Value('id').of(societe.entrepriseFormeJuridique).eq('3120') or Value('id').of(societe.entrepriseFormeJuridique).eq('5800')) ?
																	etablissement.cadreAdresseEtablissement.codePostalAdresseEtablissement : siege.cadreAdresseProfessionnelle.codePostalAdresseSiege;
formFields['adresseEtablissement_commune_ndi']                      = (siege.formaliteCreationAvecActivite and siege.etablissementPrincipalDifferentSiege) ?
																	etablissement.cadreAdresseEtablissement.communeAdresseEtablissement : siege.cadreAdresseProfessionnelle.communeAdresseSiege;
formFields['nomDomaineEtablissement_ndi']                    = etablissement.etablissementNomDomaine;
formFields['suppressionNomDomaineEtablissement_ndi']         = false;
formFields['remplacementNomDomaineEtablissement_ndi']        = false;
formFields['declarationInitialeNomDomaineEtablissement_ndi'] = true;
}

// Cadre 4

if (siege.societeNomDomaine.size() > 0) {
   if (etablissement.etablissementDateDebutActivite != null and "" !== siege.societeNomDomaine[0]) {
       var dateTmp = new Date(parseInt(etablissement.etablissementDateDebutActivite.getTimeInMillis()));
       var date = pad(dateTmp.getDate().toString());
       var month = dateTmp.getMonth() + 1;
       date = date.concat(pad(month.toString()));
       date = date.concat(dateTmp.getFullYear().toString());
       formFields['dateEffetNomDomainePM_ndi'] = date;
   } else if (signataire.formaliteSignatureDate != null and "" !== siege.societeNomDomaine[0]) {
       var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
       var date = pad(dateTmp.getDate().toString());
       var month = dateTmp.getMonth() + 1;
       date = date.concat(pad(month.toString()));
       date = date.concat(dateTmp.getFullYear().toString());
       formFields['dateEffetNomDomainePM_ndi'] = date;
   }
   for (var i = 0; i < siege.societeNomDomaine.size(); i++) {
       formFields['nomDomainePM_ndi[' + i + ']'] = siege.societeNomDomaine[i];
       formFields['nouveauNomDomainePM_ndi[' + i + ']'] = siege.societeNomDomaine[i] !== "" ? true : false;
   }
}

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));

/*
 * Création du dossier sans option fiscale : ajout du cerfa sans option fiscale
 */
 
 var cerfaDoc1 = nash.doc //
	.load('models/cerfa_13958-04_M0_societe_civile_creation_avec_volet_fiscal.pdf') //
	.apply (formFields);

/*
 * Création du dossier sans option fiscale : ajout du cerfa sans option fiscale
 */
 
 var cerfaDoc2 = nash.doc //
	.load('models/cerfa_13958-04_M0_societe_civile_creation_sans_volet_fiscal.pdf') //
	.apply (formFields);
	cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));

/*
 * Ajout de l'intercalaire M0 Prime 1
 */
 
 if (societe.entrepriseFusionScission or Value('id').of(societe.entrepriseFormeJuridique).eq('6316') or dirigeant2.autrePersonneLieeEtablissement or dirigeant3.autrePersonneLieeEtablissement or dirigeant4.autrePersonneLieeEtablissement or dirigeant5.autrePersonneLieeEtablissement or dirigeant6.autrePersonneLieeEtablissement or dirigeant7.autrePersonneLieeEtablissement or dirigeant8.autrePersonneLieeEtablissement)
{
	var m0PrimeDoc1 = nash.doc //
		.load('models/cerfa_14067-02_M0Prime_societe_civile_creation1.pdf') //
		.apply (formFieldsPers1);
	cerfaDoc1.append(m0PrimeDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire M0 Prime 2
 */
 
 if (dirigeantAutre6.autrePersonneLieeEtablissement)
{
	var m0PrimeDoc2 = nash.doc //
		.load('models/cerfa_14067-02_M0Prime_societe_civile_creation1.pdf') //
		.apply (formFieldsPers2);
	cerfaDoc1.append(m0PrimeDoc2.save('cerfa.pdf'));
}


/*
 * Ajout de l'intercalaire TNS Dirigeant 1
 */
 
 if (socialDirigeant1.voletSocialNumeroSecuriteSocialTNS != null)
{
	var tnsDoc1 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers3);
	cerfaDoc1.append(tnsDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 2
 */
 
 if (socialDirigeant2.voletSocialNumeroSecuriteSocialTNS != null)
{
	var tnsDoc2 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers4);
	cerfaDoc1.append(tnsDoc2.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 3
 */
 
 if (socialDirigeant3.voletSocialNumeroSecuriteSocialTNS != null)
{
	var tnsDoc3 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers5);
	cerfaDoc1.append(tnsDoc3.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 4
 */
 
 if (socialDirigeant4.voletSocialNumeroSecuriteSocialTNS != null)
{
	var tnsDoc4 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers6);
	cerfaDoc1.append(tnsDoc4.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 5
 */
 
 if (socialDirigeant5.voletSocialNumeroSecuriteSocialTNS != null)
{
	var tnsDoc5 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers7);
	cerfaDoc1.append(tnsDoc5.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 6
 */
 
 if (socialDirigeant6.voletSocialNumeroSecuriteSocialTNS != null)
{
	var tnsDoc6 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers8);
	cerfaDoc1.append(tnsDoc6.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 7
 */
 
 if (socialDirigeant7.voletSocialNumeroSecuriteSocialTNS != null)
{
	var tnsDoc7 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers9);
	cerfaDoc1.append(tnsDoc7.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 8
 */
 
 if (socialDirigeant8.voletSocialNumeroSecuriteSocialTNS != null)
{
	var tnsDoc8 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers10);
	cerfaDoc1.append(tnsDoc8.save('cerfa.pdf'));
}


/*
 * Ajout de l'intercalaire TNS Dirigeant 9
 */
 
 if (socialDirigeant9.voletSocialNumeroSecuriteSocialTNS != null)
{
	var tnsDoc9 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers11);
	cerfaDoc1.append(tnsDoc9.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 10
 */
 
 if (socialDirigeant10.voletSocialNumeroSecuriteSocialTNS != null)
{
	var tnsDoc10 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers12);
	cerfaDoc1.append(tnsDoc10.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 11
 */
 
 if (socialDirigeant11.voletSocialNumeroSecuriteSocialTNS != null)
{
	var tnsDoc11 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers13);
	cerfaDoc1.append(tnsDoc11.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 12
 */
 
 if (socialDirigeant12.voletSocialNumeroSecuriteSocialTNS != null)
{
	var tnsDoc12 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers14);
	cerfaDoc1.append(tnsDoc12.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 13
 */
 
 if (socialDirigeant13.voletSocialNumeroSecuriteSocialTNS != null)
{
	var tnsDoc13 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers15);
	cerfaDoc1.append(tnsDoc13.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 14
 */
 
 if (socialDirigeant14.voletSocialNumeroSecuriteSocialTNS != null)
{
	var tnsDoc14 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers16);
	cerfaDoc1.append(tnsDoc14.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 15
 */
 
 if (socialDirigeant15.voletSocialNumeroSecuriteSocialTNS != null)
{
	var tnsDoc15 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers17);
	cerfaDoc1.append(tnsDoc15.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 16
 */
 
 if (socialDirigeant16.voletSocialNumeroSecuriteSocialTNS != null)
{
	var tnsDoc16 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers18);
	cerfaDoc1.append(tnsDoc16.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 17
 */
 
 if (socialDirigeant17.voletSocialNumeroSecuriteSocialTNS != null)
{
	var tnsDoc17 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers19);
	cerfaDoc1.append(tnsDoc17.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 18
 */
 
 if (socialDirigeant18.voletSocialNumeroSecuriteSocialTNS != null)
{
	var tnsDoc18 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers20);
	cerfaDoc1.append(tnsDoc18.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 19
 */
 
 if (socialDirigeant19.voletSocialNumeroSecuriteSocialTNS != null)
{
	var tnsDoc19 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers21);
	cerfaDoc1.append(tnsDoc19.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 20
 */
 
 if (socialDirigeant20.voletSocialNumeroSecuriteSocialTNS != null)
{
	var tnsDoc20 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers22);
	cerfaDoc1.append(tnsDoc20.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire NDI Volet 1
 */
 
 if (etablissement.etablissementNomDomaine != null or (siege.societeNomDomaine[0].length > 0)) {
	var ndiDoc1 = nash.doc //
		.load('models/cerfa_14943-01_NDI_volet1.pdf') //
		.apply (formFields);
	cerfaDoc1.append(ndiDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire NDI Volet 2
 */
 
if (etablissement.etablissementNomDomaine != null or (siege.societeNomDomaine[0].length > 0)) {
	var ndiDoc2 = nash.doc //
		.load('models/cerfa_14943-01_NDI_volet2.pdf') //
		.apply (formFields);
	cerfaDoc1.append(ndiDoc2.save('cerfa.pdf'));
}


var pjUser = [];
var metas = [];
function pushPjPreview(fld) {
	fld.forEach(function (elm) {
        pjUser.push(elm);
        metas.push({'name':'document', 'value': '/'+elm.getAbsolutePath()});
    });
}

/*
 * Ajout des PJs
 */
 
// PJ Dirigeant personne physique 1

if(Value('id').of($m0SCI.dirigeant1.cadreIdentiteDirigeant.personnaliteJuridique).eq('personnePhysique')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585')) {

	if (not Value('id').of($m0SCI.dirigeant1.cadreIdentiteDirigeant.personneLieeQualite).eq('75')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant1);
	}
	if ($m0SCI.dirigeant1.cadreIdentiteDirigeant.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant1Signataire);
}
	if (not $m0SCI.dirigeant1.cadreIdentiteDirigeant.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant1NonSignataire);
	}
}

// PJ Dirigeant personne physique 2

if(Value('id').of($m0SCI.dirigeant2.cadreIdentiteDirigeant.personnaliteJuridique).eq('personnePhysique')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585')) {

	if (not Value('id').of($m0SCI.dirigeant2.cadreIdentiteDirigeant.personneLieeQualite).eq('75')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant2);
	}
	if ($m0SCI.dirigeant2.cadreIdentiteDirigeant.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant2Signataire);
	}
	if (not $m0SCI.dirigeant2.cadreIdentiteDirigeant.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant2NonSignataire);
	}
}

// PJ Dirigeant personne physique 3

if((Value('id').of($m0SCI.dirigeant3.cadreIdentiteDirigeant.personnaliteJuridique).eq('personnePhysique')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and Value('id').of($m0SCI.dirigeant2.cadreIdentiteDirigeant.personneLieeEtablissement).eq('oui')) {

	if (not Value('id').of($m0SCI.dirigeant3.cadreIdentiteDirigeant.personneLieeQualite).eq('75')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant3);
	}
	if ($m0SCI.dirigeant3.cadreIdentiteDirigeant.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant3Signataire);
	}
	if (not $m0SCI.dirigeant3.cadreIdentiteDirigeant.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant3NonSignataire);
	}
}

// PJ Dirigeant personne physique 4

if((Value('id').of($m0SCI.dirigeant4.cadreIdentiteDirigeant.personnaliteJuridique).eq('personnePhysique')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and Value('id').of($m0SCI.dirigeant3.cadreIdentiteDirigeant.personneLieeEtablissement).eq('oui'))	{

	if (not Value('id').of($m0SCI.dirigeant4.cadreIdentiteDirigeant.personneLieeQualite).eq('75')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant4);
	}
	if ($m0SCI.dirigeant4.cadreIdentiteDirigeant.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant4Signataire);
	}
	if (not $m0SCI.dirigeant4.cadreIdentiteDirigeant.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant4NonSignataire);
	}
}

// PJ Dirigeant personne physique 5

if((Value('id').of($m0SCI.dirigeant5.cadreIdentiteDirigeant.personnaliteJuridique).eq('personnePhysique')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and Value('id').of($m0SCI.dirigeant4.cadreIdentiteDirigeant.personneLieeEtablissement).eq('oui')) {

	if (not Value('id').of($m0SCI.dirigeant5.cadreIdentiteDirigeant.personneLieeQualite).eq('75')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant5);
	}
	if ($m0SCI.dirigeant5.cadreIdentiteDirigeant.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant5Signataire);
	}
	if (not $m0SCI.dirigeant5.cadreIdentiteDirigeant.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant5NonSignataire);
	}
}

// PJ Dirigeant personne physique 6

if((Value('id').of($m0SCI.dirigeant6.cadreIdentiteDirigeant.personnaliteJuridique).eq('personnePhysique')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and Value('id').of($m0SCI.dirigeant5.cadreIdentiteDirigeant.personneLieeEtablissement).eq('oui'))	{

	if (not Value('id').of($m0SCI.dirigeant6.cadreIdentiteDirigeant.personneLieeQualite).eq('75')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant6);
	}
	if ($m0SCI.dirigeant6.cadreIdentiteDirigeant.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant6Signataire);
	}
	if (not $m0SCI.dirigeant6.cadreIdentiteDirigeant.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant6NonSignataire);
	}
}

// PJ Dirigeant personne physique 7

if((Value('id').of($m0SCI.dirigeant7.cadreIdentiteDirigeant.personnaliteJuridique).eq('personnePhysique')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and Value('id').of($m0SCI.dirigeant6.cadreIdentiteDirigeant.personneLieeEtablissement).eq('oui')){

	if (not Value('id').of($m0SCI.dirigeant7.cadreIdentiteDirigeant.personneLieeQualite).eq('75')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant7);
	}
	if ($m0SCI.dirigeant7.cadreIdentiteDirigeant.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant7Signataire);
	}
	if (not $m0SCI.dirigeant7.cadreIdentiteDirigeant.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant7NonSignataire);
	}
}

// PJ Dirigeant personne physique 8

if((Value('id').of($m0SCI.dirigeant8.cadreIdentiteDirigeant.personnaliteJuridique).eq('personnePhysique')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and Value('id').of($m0SCI.dirigeant7.cadreIdentiteDirigeant.personneLieeEtablissement).eq('oui'))	{

	if (not Value('id').of($m0SCI.dirigeant8.cadreIdentiteDirigeant.personneLieeQualite).eq('75')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant8);
	}
	if ($m0SCI.dirigeant8.cadreIdentiteDirigeant.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant8Signataire);
	}
	if (not $m0SCI.dirigeant8.cadreIdentiteDirigeant.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant8NonSignataire);
	}
}

//PJ Dirigeant personne physique 9
if((Value('id').of(dirigeantAutre1.personnaliteJuridique).eq('personnePhysique')
	and not Value('id').of(dirigeantAutre1.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre1.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre1.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre1.personneLieeQualiteBis).eq('72'))
	or ((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre1.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre1.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre1.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre1.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre1.personneLieeQualiteBis).eq('66')
	or Value('id').of(dirigeantAutre1.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre1.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre1.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre1.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre1.personneLieeQualiteBis).eq('51'))	{

	if (not Value('id').of(dirigeantAutre1.personneLieeQualite).eq('75')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant9);
	}
	if (dirigeantAutre1.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant9Signataire);
	}
	if (not dirigeantAutre1.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant9NonSignataire);
	}
}

//PJ Dirigeant personne physique 10

if((Value('id').of(dirigeantAutre2.personnaliteJuridique).eq('personnePhysique')
	and not Value('id').of(dirigeantAutre2.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre2.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre2.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre2.personneLieeQualiteBis).eq('72'))
	or ((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre2.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre2.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre2.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre2.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre2.personneLieeQualiteBis).eq('66')
	or Value('id').of(dirigeantAutre2.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre2.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre2.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre2.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre2.personneLieeQualiteBis).eq('51'))	{

	if (not Value('id').of(dirigeantAutre2.personneLieeQualite).eq('75')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant10);
	}
	if (dirigeantAutre2.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant10Signataire);
	}
	if (not dirigeantAutre2.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant10NonSignataire);
	}
}

//PJ Dirigeant personne physique 11

if((Value('id').of(dirigeantAutre3.personnaliteJuridique).eq('personnePhysique')
	and not Value('id').of(dirigeantAutre3.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre3.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre3.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre3.personneLieeQualiteBis).eq('72'))
	or ((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre3.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre3.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre3.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre3.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre3.personneLieeQualiteBis).eq('66')
	or Value('id').of(dirigeantAutre3.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre3.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre3.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre3.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre3.personneLieeQualiteBis).eq('51'))	{

	if (not Value('id').of(dirigeantAutre3.personneLieeQualite).eq('75')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant11);
	}
	if (dirigeantAutre3.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant11Signataire);
	}
	if (not dirigeantAutre3.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant11NonSignataire);
	}
}

//PJ Dirigeant personne physique 12

if((Value('id').of(dirigeantAutre4.personnaliteJuridique).eq('personnePhysique')
	and not Value('id').of(dirigeantAutre4.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre4.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre4.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre4.personneLieeQualiteBis).eq('72'))
	or ((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre4.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre4.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre4.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre4.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre4.personneLieeQualiteBis).eq('66')
	or Value('id').of(dirigeantAutre4.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre4.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre4.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre4.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre4.personneLieeQualiteBis).eq('51'))	{

	if (not Value('id').of(dirigeantAutre4.personneLieeQualite).eq('75')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant12);
	}
	if (dirigeantAutre4.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant12Signataire);
	}
	if (not dirigeantAutre4.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant12NonSignataire);
	}
}

//PJ Dirigeant personne physique 13

if((Value('id').of(dirigeantAutre5.personnaliteJuridique).eq('personnePhysique')
	and not Value('id').of(dirigeantAutre5.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre5.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre5.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre5.personneLieeQualiteBis).eq('72'))
	or ((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre5.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre5.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre5.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre5.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre5.personneLieeQualiteBis).eq('66')
	or Value('id').of(dirigeantAutre5.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre5.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre5.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre5.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre5.personneLieeQualiteBis).eq('51'))	{

	if (not Value('id').of(dirigeantAutre5.personneLieeQualite).eq('75')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant13);
	}
	if (dirigeantAutre5.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant13Signataire);
	}
	if (not dirigeantAutre5.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant13NonSignataire);
	}
}

//PJ Dirigeant personne physique 14

if((Value('id').of(dirigeantAutre6.personnaliteJuridique).eq('personnePhysique')
	and not Value('id').of(dirigeantAutre6.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre6.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre6.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre6.personneLieeQualiteBis).eq('72'))
	or ((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre6.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre6.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre6.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre6.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre6.personneLieeQualiteBis).eq('66')
	or Value('id').of(dirigeantAutre6.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre6.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre6.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre6.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre6.personneLieeQualiteBis).eq('51'))	{

	if (not Value('id').of(dirigeantAutre6.personneLieeQualite).eq('75')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant14);
	}
	if (dirigeantAutre6.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant14Signataire);
	}
	if (not dirigeantAutre6.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant14NonSignataire);
	}
}

//PJ Dirigeant personne physique 15

if((Value('id').of(dirigeantAutre7.personnaliteJuridique).eq('personnePhysique')
	and not Value('id').of(dirigeantAutre7.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre7.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre7.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre7.personneLieeQualiteBis).eq('72'))
	or ((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre7.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre7.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre7.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre7.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre7.personneLieeQualiteBis).eq('66')
	or Value('id').of(dirigeantAutre7.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre7.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre7.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre7.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre7.personneLieeQualiteBis).eq('51'))	{

	if (not Value('id').of(dirigeantAutre7.personneLieeQualite).eq('75')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant15);
	}
	if (dirigeantAutre7.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant15Signataire);
	}
	if (not dirigeantAutre7.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant15NonSignataire);
	}
}

//PJ Dirigeant personne physique 16

if((Value('id').of(dirigeantAutre8.personnaliteJuridique).eq('personnePhysique')
	and not Value('id').of(dirigeantAutre8.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre8.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre8.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre8.personneLieeQualiteBis).eq('72'))
	or ((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre8.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre8.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre8.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre8.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre8.personneLieeQualiteBis).eq('66')
	or Value('id').of(dirigeantAutre8.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre8.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre8.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre8.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre8.personneLieeQualiteBis).eq('51'))	{

	if (not Value('id').of(dirigeantAutre8.personneLieeQualite).eq('75')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant16);
	}
	if (dirigeantAutre8.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant16Signataire);
	}
	if (not dirigeantAutre8.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant16NonSignataire);
	}
}

//PJ Dirigeant personne physique 17

if((Value('id').of(dirigeantAutre9.personnaliteJuridique).eq('personnePhysique')
	and not Value('id').of(dirigeantAutre9.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre9.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre9.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre9.personneLieeQualiteBis).eq('72'))
	or ((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre9.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre9.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre9.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre9.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre9.personneLieeQualiteBis).eq('66')
	or Value('id').of(dirigeantAutre9.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre9.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre9.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre9.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre9.personneLieeQualiteBis).eq('51'))	{

	if (not Value('id').of(dirigeantAutre9.personneLieeQualite).eq('75')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant17);
	}
	if (dirigeantAutre9.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant17Signataire);
	}
	if (not dirigeantAutre9.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant17NonSignataire);
	}
}

//PJ Dirigeant personne physique 18

if((Value('id').of(dirigeantAutre10.personnaliteJuridique).eq('personnePhysique')
	and not Value('id').of(dirigeantAutre10.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre10.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre10.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre10.personneLieeQualiteBis).eq('72'))
	or ((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre10.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre10.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre10.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre10.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre10.personneLieeQualiteBis).eq('66')
	or Value('id').of(dirigeantAutre10.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre10.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre10.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre10.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre10.personneLieeQualiteBis).eq('51'))	{

	if (not Value('id').of(dirigeantAutre10.personneLieeQualite).eq('75')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant18);
	}
	if (dirigeantAutre10.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant18Signataire);
	}
	if (not dirigeantAutre10.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant18NonSignataire);
	}
}

//PJ Dirigeant personne physique 19

if((Value('id').of(dirigeantAutre11.personnaliteJuridique).eq('personnePhysique')
	and not Value('id').of(dirigeantAutre11.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre11.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre11.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre11.personneLieeQualiteBis).eq('72'))
	or ((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre11.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre11.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre11.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre11.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre11.personneLieeQualiteBis).eq('66')
	or Value('id').of(dirigeantAutre11.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre11.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre11.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre11.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre11.personneLieeQualiteBis).eq('51'))	{

	if (not Value('id').of(dirigeantAutre11.personneLieeQualite).eq('75')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant19);
	}
	if (dirigeantAutre11.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant19Signataire);
	}
	if (not dirigeantAutre11.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant19NonSignataire);
	}
}

//PJ Dirigeant personne physique 20

if((Value('id').of(dirigeantAutre12.personnaliteJuridique).eq('personnePhysique')
	and not Value('id').of(dirigeantAutre12.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre12.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre12.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre12.personneLieeQualiteBis).eq('72'))
	or ((Value('id').of(societe.entrepriseFormeJuridique).eq('6561')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6562')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6563')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6564')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6565')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6566')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6567')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6568')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6569')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6571')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6572')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6573')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6574')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6575')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6576')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6577')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6578')
	or Value('id').of(societe.entrepriseFormeJuridique).eq('6585'))
	and (Value('id').of(dirigeantAutre12.personneLieeQualite).eq('30')
	or Value('id').of(dirigeantAutre12.personneLieeQualite).eq('29')
	or Value('id').of(dirigeantAutre12.personneLieeQualite).eq('75')))
	or Value('id').of(dirigeantAutre12.personneLieeQualite).eq('66')
	or Value('id').of(dirigeantAutre12.personneLieeQualiteBis).eq('66')
	or Value('id').of(dirigeantAutre12.personneLieeQualiteBis).eq('63')
	or Value('id').of(dirigeantAutre12.personneLieeQualiteBis).eq('52')
	or Value('id').of(dirigeantAutre12.personneLieeQualiteBis).eq('61')
	or Value('id').of(dirigeantAutre12.personneLieeQualiteBis).eq('60')
	or Value('id').of(dirigeantAutre12.personneLieeQualiteBis).eq('51'))	{

	if (not Value('id').of(dirigeantAutre12.personneLieeQualite).eq('75')) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant20);
	}
	if (dirigeantAutre12.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant20Signataire);
	}
	if (not dirigeantAutre12.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDirigeant20NonSignataire);
	}
}

// PJ Dirigeant Personne Morale 1 

if(Value('id').of($m0SCI.dirigeant1.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale'))	{
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS1);
	if ($m0SCI.dirigeant1.cadreIdentiteDirigeant.dirigeantSignataire) {
		pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire1);
	}
}

// PJ Dirigeant Personne Morale 2 

if(Value('id').of($m0SCI.dirigeant2.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale'))	{
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS2);
	if ($m0SCI.dirigeant2.cadreIdentiteDirigeant.dirigeantSignataire) {
	    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire2);
	}
}

// PJ Dirigeant Personne Morale 3 

if(Value('id').of($m0SCI.dirigeant3.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale'))	{
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS3);
	if ($m0SCI.dirigeant3.cadreIdentiteDirigeant.dirigeantSignataire) {
	    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire3);
	}
}

// PJ Dirigeant Personne Morale 4 

if(Value('id').of($m0SCI.dirigeant4.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale'))	{
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS4);
	if ($m0SCI.dirigeant4.cadreIdentiteDirigeant.dirigeantSignataire) {
	    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire4);
	}
}

// PJ Dirigeant Personne Morale 5 

if(Value('id').of($m0SCI.dirigeant5.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale'))	{
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS5);
	if ($m0SCI.dirigeant5.cadreIdentiteDirigeant.dirigeantSignataire) {
	    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire5);
	}
}

// PJ Dirigeant Personne Morale 6 

if(Value('id').of($m0SCI.dirigeant6.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale'))	{
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS6);
	if ($m0SCI.dirigeant6.cadreIdentiteDirigeant.dirigeantSignataire) {
	    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire6);
	}
}

// PJ Dirigeant Personne Morale 7 

if(Value('id').of($m0SCI.dirigeant7.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale'))	{
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS7);
	if ($m0SCI.dirigeant7.cadreIdentiteDirigeant.dirigeantSignataire) {
	    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire7);
	}
}

// PJ Dirigeant Personne Morale 8 

if(Value('id').of($m0SCI.dirigeant8.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale'))	{
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS8);
	if ($m0SCI.dirigeant8.cadreIdentiteDirigeant.dirigeantSignataire) {
	    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire8);
	}
}

// PJ Dirigeant Personne Morale 9 

if(Value('id').of($m0SCI.dirigeantAutre1.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale')
	and not Value('id').of(dirigeantAutre1.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre1.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre1.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre1.personneLieeQualiteBis).eq('72'))	{
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS9);
	if ($m0SCI.dirigeantAutre1.cadreIdentiteDirigeant.dirigeantSignataire) {
	    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire9);
	}
}

// PJ Dirigeant Personne Morale 10

if(Value('id').of($m0SCI.dirigeantAutre2.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale')
	and not Value('id').of(dirigeantAutre2.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre2.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre2.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre2.personneLieeQualiteBis).eq('72'))	{
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS10);
	if ($m0SCI.dirigeantAutre2.cadreIdentiteDirigeant.dirigeantSignataire) {
	    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire10);
	}
}

// PJ Dirigeant Personne Morale 11

if(Value('id').of($m0SCI.dirigeantAutre3.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale')
	and not Value('id').of(dirigeantAutre3.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre3.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre3.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre3.personneLieeQualiteBis).eq('72'))	{
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS11);
	if ($m0SCI.dirigeantAutre3.cadreIdentiteDirigeant.dirigeantSignataire) {
	    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire11);
	}
}

// PJ Dirigeant Personne Morale 12 

if(Value('id').of($m0SCI.dirigeantAutre4.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale')
	and not Value('id').of(dirigeantAutre4.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre4.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre4.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre4.personneLieeQualiteBis).eq('72'))	{
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS12);
	if ($m0SCI.dirigeantAutre4.cadreIdentiteDirigeant.dirigeantSignataire) {
	    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire12);
	}
}

// PJ Dirigeant Personne Morale 13

if(Value('id').of($m0SCI.dirigeantAutre5.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale')
	and not Value('id').of(dirigeantAutre5.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre5.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre5.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre5.personneLieeQualiteBis).eq('72'))	{
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS13);
	if ($m0SCI.dirigeantAutre5.cadreIdentiteDirigeant.dirigeantSignataire) {
	    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire13);
	}
}

// PJ Dirigeant Personne Morale 14

if(Value('id').of($m0SCI.dirigeantAutre6.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale')
	and not Value('id').of(dirigeantAutre6.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre6.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre6.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre6.personneLieeQualiteBis).eq('72'))	{
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS14);
	if ($m0SCI.dirigeantAutre6.cadreIdentiteDirigeant.dirigeantSignataire) {
	    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire14);
	}
}

// PJ Dirigeant Personne Morale 15

if(Value('id').of($m0SCI.dirigeantAutre7.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale')
	and not Value('id').of(dirigeantAutre7.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre7.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre7.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre7.personneLieeQualiteBis).eq('72'))	{
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS15);
	if ($m0SCI.dirigeantAutre7.cadreIdentiteDirigeant.dirigeantSignataire) {
	    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire15);
	}
}

// PJ Dirigeant Personne Morale 16

if(Value('id').of($m0SCI.dirigeantAutre8.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale')
	and not Value('id').of(dirigeantAutre8.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre8.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre8.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre8.personneLieeQualiteBis).eq('72'))	{
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS16);
	if ($m0SCI.dirigeantAutre8.cadreIdentiteDirigeant.dirigeantSignataire) {
	    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire16);
	}
}

// PJ Dirigeant Personne Morale 17

if(Value('id').of($m0SCI.dirigeantAutre9.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale')
	and not Value('id').of(dirigeantAutre9.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre9.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre9.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre9.personneLieeQualiteBis).eq('72'))	{
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS17);
	if ($m0SCI.dirigeantAutre9.cadreIdentiteDirigeant.dirigeantSignataire) {
	    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire17);
	}
}

// PJ Dirigeant Personne Morale 18

if(Value('id').of($m0SCI.dirigeantAutre10.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale')
	and not Value('id').of(dirigeantAutre10.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre10.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre10.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre10.personneLieeQualiteBis).eq('72'))	{
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS18);
	if ($m0SCI.dirigeantAutre10.cadreIdentiteDirigeant.dirigeantSignataire) {
	    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire18);
	}
}

// PJ Dirigeant Personne Morale 19 

if(Value('id').of($m0SCI.dirigeantAutre11.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale')
	and not Value('id').of(dirigeantAutre11.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre11.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre11.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre11.personneLieeQualiteBis).eq('72'))	{
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS19);
	if ($m0SCI.dirigeantAutre11.cadreIdentiteDirigeant.dirigeantSignataire) {
	    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire19);
	}
}

// PJ Dirigeant Personne Morale 20 

if(Value('id').of($m0SCI.dirigeantAutre12.cadreIdentiteDirigeant.personnaliteJuridique).eq('personneMorale')
	and not Value('id').of(dirigeantAutre12.personneLieeQualite).eq('71')
	and not Value('id').of(dirigeantAutre12.personneLieeQualite).eq('72')
	and not Value('id').of(dirigeantAutre12.personneLieeQualiteBis).eq('71')
	and not Value('id').of(dirigeantAutre12.personneLieeQualiteBis).eq('72'))	{
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS20);
	if ($m0SCI.dirigeantAutre12.cadreIdentiteDirigeant.dirigeantSignataire) {
	    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPersonneMoraleSignataire20);
	}
}

// Nomination dirigeants

if((not $m0SCI.dirigeant1.cadreIdentiteDirigeant.statutPresenceDirigeant and $m0SCI.dirigeant1.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeant1.cadreIdentiteDirigeant.personneLieeQualite).eq('75')) 
	or (not $m0SCI.dirigeant2.cadreIdentiteDirigeant.statutPresenceDirigeant and $m0SCI.dirigeant2.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeant2.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not $m0SCI.dirigeant3.cadreIdentiteDirigeant.statutPresenceDirigeant and $m0SCI.dirigeant3.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeant3.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not $m0SCI.dirigeant4.cadreIdentiteDirigeant.statutPresenceDirigeant and $m0SCI.dirigeant4.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeant4.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not $m0SCI.dirigeant5.cadreIdentiteDirigeant.statutPresenceDirigeant and $m0SCI.dirigeant5.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeant5.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not $m0SCI.dirigeant6.cadreIdentiteDirigeant.statutPresenceDirigeant and $m0SCI.dirigeant6.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeant6.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not $m0SCI.dirigeant7.cadreIdentiteDirigeant.statutPresenceDirigeant and $m0SCI.dirigeant7.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeant7.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not $m0SCI.dirigeant8.cadreIdentiteDirigeant.statutPresenceDirigeant and $m0SCI.dirigeant8.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeant8.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not dirigeantAutre1.statutPresenceDirigeant and $m0SCI.dirigeantAutre1.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeantAutre1.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not dirigeantAutre2.statutPresenceDirigeant and $m0SCI.dirigeantAutre2.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeantAutre2.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not dirigeantAutre3.statutPresenceDirigeant and $m0SCI.dirigeantAutre3.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeantAutre3.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not dirigeantAutre4.statutPresenceDirigeant and $m0SCI.dirigeantAutre4.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeantAutre4.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not dirigeantAutre5.statutPresenceDirigeant and $m0SCI.dirigeantAutre5.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeantAutre5.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not dirigeantAutre6.statutPresenceDirigeant and $m0SCI.dirigeantAutre6.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeantAutre6.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not dirigeantAutre7.statutPresenceDirigeant and $m0SCI.dirigeantAutre7.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeantAutre7.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not dirigeantAutre8.statutPresenceDirigeant and $m0SCI.dirigeantAutre8.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeantAutre8.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not dirigeantAutre9.statutPresenceDirigeant and $m0SCI.dirigeantAutre9.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeantAutre9.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not dirigeantAutre10.statutPresenceDirigeant and $m0SCI.dirigeantAutre10.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeantAutre10.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not dirigeantAutre11.statutPresenceDirigeant and $m0SCI.dirigeantAutre11.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeantAutre11.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))
	or (not dirigeantAutre12.statutPresenceDirigeant and $m0SCI.dirigeantAutre12.cadreIdentiteDirigeant.personneLieeQualite != null and not Value('id').of($m0SCI.dirigeantAutre12.cadreIdentiteDirigeant.personneLieeQualite).eq('75'))) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjNominationDirigeant);
}

// Pj CAC Titulaire 

if(Value('id').of($m0SCI.dirigeantAutre1.cadreIdentiteDirigeant.personneLieeQualite).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre2.cadreIdentiteDirigeant.personneLieeQualite).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre3.cadreIdentiteDirigeant.personneLieeQualite).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre4.cadreIdentiteDirigeant.personneLieeQualite).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre5.cadreIdentiteDirigeant.personneLieeQualite).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre6.cadreIdentiteDirigeant.personneLieeQualite).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre7.cadreIdentiteDirigeant.personneLieeQualite).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre8.cadreIdentiteDirigeant.personneLieeQualite).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre9.cadreIdentiteDirigeant.personneLieeQualite).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre10.cadreIdentiteDirigeant.personneLieeQualite).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre11.cadreIdentiteDirigeant.personneLieeQualite).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre12.cadreIdentiteDirigeant.personneLieeQualite).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre1.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre2.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre3.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre4.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre5.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre6.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre7.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre8.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre9.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre10.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre11.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('71')
	or Value('id').of($m0SCI.dirigeantAutre12.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('71')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjLettreAcceptationCACTitulaire);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjJustificatifInscriptionCACTitulaire);
}	

// Pj CAC Suppléant

if(Value('id').of($m0SCI.dirigeantAutre1.cadreIdentiteDirigeant.personneLieeQualite).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre2.cadreIdentiteDirigeant.personneLieeQualite).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre3.cadreIdentiteDirigeant.personneLieeQualite).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre4.cadreIdentiteDirigeant.personneLieeQualite).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre5.cadreIdentiteDirigeant.personneLieeQualite).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre6.cadreIdentiteDirigeant.personneLieeQualite).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre7.cadreIdentiteDirigeant.personneLieeQualite).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre8.cadreIdentiteDirigeant.personneLieeQualite).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre9.cadreIdentiteDirigeant.personneLieeQualite).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre10.cadreIdentiteDirigeant.personneLieeQualite).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre11.cadreIdentiteDirigeant.personneLieeQualite).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre12.cadreIdentiteDirigeant.personneLieeQualite).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre1.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre2.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre3.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre4.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre5.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre6.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre7.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre8.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre9.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre10.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre11.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('72')
	or Value('id').of($m0SCI.dirigeantAutre12.cadreIdentiteDirigeant.personneLieeQualiteBis).eq('72')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjLettreAcceptationCACSuppleant);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjJustificatifInscriptionCACSuppleant);
}

// PJ Mandataire

if (not dirigeant1.dirigeantSignataire and not dirigeant2.dirigeantSignataire and not dirigeant3.dirigeantSignataire and not dirigeant4.dirigeantSignataire and not dirigeant5.dirigeantSignataire and not dirigeant6.dirigeantSignataire and not dirigeant7.dirigeantSignataire and not dirigeant8.dirigeantSignataire and not dirigeantAutre1.dirigeantSignataire and not dirigeantAutre2.dirigeantSignataire and not dirigeantAutre3.dirigeantSignataire and not dirigeantAutre4.dirigeantSignataire and not dirigeantAutre5.dirigeantSignataire and not dirigeantAutre6.dirigeantSignataire and not dirigeantAutre7.dirigeantSignataire and not dirigeantAutre8.dirigeantSignataire and not dirigeantAutre9.dirigeantSignataire and not dirigeantAutre10.dirigeantSignataire and not dirigeantAutre11.dirigeantSignataire and not dirigeantAutre12.dirigeantSignataire) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPouvoir);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire);
}

//PJ Société

pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjStatuts);
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjAnnonceLegaleConstitution);
pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDActiviteReglementee);

// PJ Etablissement

var pj=$m0SCI.cadreEntrepriseSiegeGroup.cadreEntrepriseSiege.activiteLieu ;
if(Value('id').of(pj).contains('entrepriseAdresseEntrepriseDomicile')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDomicile);
}

if(Value('id').of(pj).contains('etablissementDomiciliationOui')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjContratDomiciliation);
}

if (Value('id').of($m0SCI.cadreEntrepriseSiegeGroup.cadreEntrepriseSiege.activiteLieu).contains('etablissementDomiciliationNon')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjSocieteCreation);
}

if($m0SCI.cadreEntrepriseSiegeGroup.cadreEntrepriseSiege.etablissementPrincipalDifferentSiege) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjEtablissementCreation);
}

// PJ MBE

pushPjPreview($attachmentPreprocess.attachmentPreprocess.formulaireBE1);

if (not Value('id').of(signataire.nombreMBE).eq('un')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.formulaireBE2);
}

if (not Value('id').of(signataire.nombreMBE).eq('un') and not Value('id').of(signataire.nombreMBE).eq('deux')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.formulaireBE3);
}

if (Value('id').of(signataire.nombreMBE).eq('quatre') or Value('id').of(signataire.nombreMBE).eq('cinq')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.formulaireBE4);
}

if (Value('id').of(signataire.nombreMBE).eq('cinq')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.formulaireBE5);
}


/*
 * Enregistrement du fichier (en mémoire)
 */

var finalDoc = cerfaDoc1.save('M0_Societe_civile.pdf');

//Remove old metas before insert
var metasToDelete = [];
var recordMetas = nash.record.meta() != null ? nash.record.meta().metas : null;
var recordMetasSize = recordMetas != null ? recordMetas.size() : 0;

for (var i = 0; i < recordMetasSize; i++) {
   if (recordMetas.get(i).name == 'document' && !recordMetas.get(i).value.contains("proxy")) {
       metasToDelete.push({'name':'document', 'value': recordMetas.get(i).value});
   }
}

nash.record.removeMeta(metasToDelete);
metas.push({'name':'document', 'value': '/3-review/generated/generated.record-1-M0_Societe_civile.pdf'});

nash.record.meta(metas);
var data = [ spec.createData({
    id : 'record',
    label : 'Déclaration de création d\'une société civile',
    description : 'Voici le formulaire obtenu à partir des données saisies.',
    type : 'FileReadOnly',
    value : [ finalDoc ]
}),  spec.createData({
    id : 'attachments',
    label : 'Pièces jointes',
    description : 'Pièces jointes ajoutées au formulaire.',
    type : 'FileReadOnly',
    value : pjUser
}) ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Déclaration de création d\'une société civile',
    groups : groups
});