// input functionnal id of authority
var grp = _INPUT_.functionalId;


var destFuncId = !grp.funcId ? '' : grp.funcId;

// récupérer le code de la deuxième autorité
var destFuncId2 = !grp.funcId2 ? '' : grp.funcId2;


_log.info("destFuncId is  {}",destFuncId);

// call directory with funcId to find all information of Authorithy

var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', destFuncId) //
	           .connectionTimeout(10000) //
	           .receiveTimeout(10000) //
	           .accept('json') //
	           .get();

//result

var receiverInfo = response.asObject();

// prepare all information of receiver to create data.xml

var funcId = !receiverInfo.entityId ? null : receiverInfo.entityId;
var funcLabel = !receiverInfo.label ? null :receiverInfo.label;
var contactInfo = !receiverInfo.details ? null : receiverInfo.details;
var email = !contactInfo.profile.email ? null : contactInfo.profile.email;
var tel = !contactInfo.profile.tel ? null : contactInfo.profile.tel;
var codeEDI = !contactInfo.ediCode ? null : contactInfo.ediCode;

_log.info("profile is  {}",contactInfo.profile);

var partnerLabel = !contactInfo.profile.address.recipientName ? null : contactInfo.profile.address.recipientName;
var authCP = !contactInfo.profile.address.cityNumber ? null : contactInfo.profile.address.cityNumber;
var authAddressName = !contactInfo.profile.address.addressName ? null : contactInfo.profile.address.addressName;
var authAddressNameCompl = !contactInfo.profile.address.addressNameCompl ? null : contactInfo.profile.address.addressNameCompl;
var authSpecial = !contactInfo.profile.address.special ? null : contactInfo.profile.address.special;
var authCity = !contactInfo.profile.address.cityName ? null : contactInfo.profile.address.cityName;

var authAdress = authAddressName + " " + authAddressNameCompl + " " + authSpecial;

var attachment = !grp.attachment ? null : grp.attachment;

//Début de l'ajout : Récupération des information de la deuxième autorité
var codeEDI2 = "";
if (destFuncId2 != "") { 
var response2 = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', destFuncId2) //
	           .connectionTimeout(10000) //
	           .receiveTimeout(10000) //
	           .accept('json') //
	           .get();


var receiverInfo2 = response2.asObject();
var contactInfo2 = !receiverInfo2.details ? null : receiverInfo2.details;
var partnerLabel2 = !contactInfo2.profile.address.recipientName ? null : contactInfo2.profile.address.recipientName;
codeEDI2 = !contactInfo2.ediCode ? null : contactInfo2.ediCode;
}
//Fin de l'ajout de l'ajout : Récupération des information de la deuxième autorité


// create data.xml with information of receiver

return spec.create({
		id: 'group',
		label: "les informations d'une autorité compétente",
		groups: [spec.createGroup({
				id: 'dataGroup',
				label: "Contenu d'une autorité compétente",
				data: [spec.createData({
						id: 'destFuncId',
						label: "Le code de l'autorité compétente",
						description: 'Code interne',
						type: 'String',
						mandatory: true,
						value: funcId
					}), spec.createData({
						id: 'labelFunc',
						label: "Le libellé fonctionnelle de l'autorité compétente",
						type: 'String',
						mandatory : true,
						value: funcLabel
					}), spec.createData({
						id: 'email',
						label: "L'adresse email de l'autorité compétente",
						type: 'email',
						mandatory:true,
						value: email
					}),	spec.createData({
					    id: 'emailPj',
					    label: "Le chemin de la pièce jointe",
					    type: 'String',
					    value: attachment
				}),   spec.createData({
					    id: 'tel',
					    label: "telephone",
					    type: 'String',
					    value: tel
				}), spec.createData({
					    id: 'partnerLabel',
					    label: "Nom autorité",
					    type: 'String',
					    value: partnerLabel
				}),spec.createData({
					    id: 'codeEDI',
					    label: "code EDI",
					    type: 'String',
					    value: codeEDI
				}),
				//début de l'ajout
				spec.createData({
						id: 'destFuncId2',
						label: "code de la deuxième autorité",
						type: 'String',
						value: destFuncId2
				}),spec.createData({
						id: 'codeEDI2',
						label: "code EDI de la deuxième autorité",
						type: 'String',
						value: codeEDI2
				}),spec.createData({
						id: 'partnerLabel2',
						label: "Nom de la deuxième autorité",
						type: 'String',
						value: partnerLabel2
				})
				//fin de l'ajout
				
				
				]
			})]
	});

