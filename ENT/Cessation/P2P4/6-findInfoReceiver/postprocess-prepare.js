//check information of receiver

var destFuncId = _INPUT_.dataGroup.destFuncId;
var telAuth = _INPUT_.dataGroup.tel;
var partnerLabel = _INPUT_.dataGroup.partnerLabel;

// input email of receiver
email = _INPUT_.dataGroup.email

emailObject = "Nouveau dossier disponible"
emailContent = "<html>"+"Bonjour "+destFuncId +",<br\><br\>" +"Le nouveau dossier " + nash.record.description().recordUid + " est disponible sur dashboard-back-office.<br/>" +"Pour y accéder cliquer sur le lien : <a href=\"https://backoffice-dashboard.dev.guichet-partenaires.fr\">cliquez ici</a>"+"<html>"

emailPj = _INPUT_.dataGroup.emailPj

// create data.xml content data-to-send with exchange service

return spec.create({
		id: 'prepareSend',
		label: "Préparation de l'envoi du message",
		groups: [spec.createGroup({
				id: 'dataGroup',
				label: "Le Contenu du message",
				data: [spec.createData({
						id: 'email',
						label: "L'adresse électronique de l'autorité compétente ",
						type: 'Email',
						mandatory: true,
						value: email
					}), spec.createData({
						id: 'title',
						label: "Description de la demande",
						type: 'String',
						value: emailObject
					}), spec.createData({
						id: 'body',
						label: "Le contenu du message",
						type: 'Text',
						value: emailContent
					}), spec.createData({
						id: 'attachment',
						label: "Le chemin de la pièce jointe",
						type: 'String',
						value: emailPj
					}),spec.createData({
						id: 'destFuncId',
						label: "Code de l'autorité compétente",
						type: 'String',
						value: destFuncId
					}),spec.createData({
						id: 'telAuth',
						label: "telephone autorité",
						type: 'String',
						value: telAuth
					}) ,spec.createData({
					    id: 'partnerLabel',
					    label: "Nom autorité",
					    type: 'String',
					    value: partnerLabel
					}) ,spec.createData({
					    id: 'regentAttachment',
					    label: "Xml Regent",
					    type: 'String',
					    value: "/XML_REGENT.xml"
					})
				]
			})]
});