var formFields = {};
var cerfaFields = {};
function pad(s) { return (s < 10) ? '0' + s : s; }

// Cadre 0 - Objet de la formalité

var identite = $p2p4rad.cadre1Identite.cadre1RappelIdentification;
var infoCessation = $p2p4rad.cadre3InformationsGroup.cadre3Informations;

formFields['p2p4modif']                                              = false;
formFields['activiteLiberale']                                       = true;
formFields['activiteArtisanale']                                     = false;
formFields['activiteCommerciale']                                    = false;
formFields['p2p4rad']                                                = true;

// Cadre 1 - Rappel d'identification

formFields['siren']                                                  = identite.siren.split(' ').join('');
formFields['personneLiee_personnePhysique_nomNaissance']             = identite.personneLieePersonnePhysiqueNomNaissance;
formFields['personneLiee_personnePhysique_nomUsage']                 = identite.personneLieePersonnePhysiqueNomUsage;
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFields['personneLiee_personnePhysique_prenom1']                       = prenoms.toString();
if (identite.personneLieePersonnePhysiqueDateNaissance !== null) {
    var dateTmp = new Date(parseInt(identite.personneLieePersonnePhysiqueDateNaissance.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['personneLiee_personnePhysique_dateNaissance']          = date;
}
formFields['personneLiee_personnePhysique_lieuNaissanceDepartement'] = identite.personneLieePersonnePhysiqueLieuNaissanceDepartement != null ? identite.personneLieePersonnePhysiqueLieuNaissanceDepartement.getId() : '';
formFields['personneLiee_personnePhysique_lieuNaissanceCommune']     = identite.personneLieePersonnePhysiqueLieuNaissanceCommune != null ? (identite.personneLieePersonnePhysiqueLieuNaissanceCommune + ' / ' + identite.personneLieePersonnePhysiqueLieuNaissancePays) : (identite.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + identite.personneLieePersonnePhysiqueLieuNaissancePays);


// Cadre 2

formFields['personneLiee_personnePhysique_lieuDepotImpot']            = identite.sieLieu;

// Cadres 3

if (infoCessation.dateCessation !== null) {
    var dateTmp = new Date(parseInt(infoCessation.dateCessation.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['dateCessation']          = date;
}


// Cadre 5 - EIRL

var etablissement = $p2p4rad.cadre2AdresseEtablissementGroup.cadreAdresseEtablissement ;
var adressePro = $p2p4rad.cadre2AdresseEtablissementGroup.cadreAdresseEtablissement.adresseEtablissementPrincipal ;


if (infoCessation.dateCessation !== null and infoCessation.estEIRLOuiNon) {
    var dateTmp = new Date(parseInt(infoCessation.dateCessation.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date;
}
formFields['eirl_immatriculation']                                                   = false;
formFields['eirl_modification']                                                      = infoCessation.estEIRLOuiNon ? true : false;

// Cadre 8 - Observations

var correspondance = $p2p4rad.cadre8RensCompGroup.cadre8RensComp;

formFields['observations']                                           = correspondance.formaliteObservations != null ? correspondance.formaliteObservations : '';

// Cadre 9 - Adresse de correspondance

formFields['adressesCorrespondanceAutre']                            = true;
formFields['adresseCorrespondance_voie1']                            = Value('id').of(correspondance.adresseCorrespond).eq('prof') ? 
																	((adressePro.etablissementAdresseNumeroVoie != null ? adressePro.etablissementAdresseNumeroVoie : '') 
																	+ ' ' + (adressePro.etablissementAdresseIndiceVoie != null ? adressePro.etablissementAdresseIndiceVoie : '')
																	+ ' ' + (adressePro.etablissementAdresseTypeVoie != null ? adressePro.etablissementAdresseTypeVoie : '')
																	+ ' ' + adressePro.etablissementAdresseNomVoie) : 
																	(correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance
																	+ ' ' + (correspondance.adresseCorrespondance.numeroRueAdresseCorrespondance != null ? correspondance.adresseCorrespondance.numeroRueAdresseCorrespondance : '') 
																	+ ' ' + (correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance : '')
																	+ ' ' + (correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance : '')
																	+ ' ' + correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance);
formFields['adresseCorrespondance_voie2']                            = Value('id').of(correspondance.adresseCorrespond).eq('prof') ? 
																	((adressePro.etablissementAdresseComplementVoie != null ? adressePro.etablissementAdresseComplementVoie : '') 
																	+ ' ' + (adressePro.etablissementAdresseDistriutionSpecialeVoie != null ? adressePro.etablissementAdresseDistriutionSpecialeVoie : '')) : 
																	((correspondance.adresseCorrespondance.rueComplementAdresseCorrespondance != null ? correspondance.adresseCorrespondance.rueComplementAdresseCorrespondance : '') 
																	+ ' ' + (correspondance.adresseCorrespondance.distriutionSpecialeVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.distriutionSpecialeVoieAdresseCorrespondance : ''));
formFields['adresseCorrespondance_codePostal']                       = Value('id').of(correspondance.adresseCorrespond).eq('prof') ? (adressePro.etablissementAdresseCodePostal) : (correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal);
formFields['adresseCorrespondance_commune']                          = Value('id').of(correspondance.adresseCorrespond).eq('prof') ? (adressePro.etablissementAdresseCommune) : (correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune);
formFields['telephone1']                                             = correspondance.infosSup.formaliteTelephone1 != null ? correspondance.infosSup.formaliteTelephone1 : '';
formFields['telephone2']                                             = correspondance.infosSup.formaliteTelephone2;
formFields['courriel']                                               = correspondance.infosSup.formaliteCourriel != null ? correspondance.infosSup.formaliteCourriel : (correspondance.infosSup.telecopie != null ? correspondance.infosSup.telecopie : '');


// Cadre 11 - Signataire

var signataire = $p2p4rad.cadre9SignatureGroup.cadre9Signature;

formFields['signataireDeclarant']                                    = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFields['signataireMandataire']                                   = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? true : false;
formFields['signataireNom']    				                    	 = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? signataire.adresseMandataire.nomPrenomDenominationMandataire : '';
formFields['signataireAdresse']                                      = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? 
																	 ((signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																	 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																	 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																	 + ' ' + signataire.adresseMandataire.nomVoieMandataire) : '';
formFields['signataireAdresse2']									 = (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																	 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '');
formFields['signataireAdresse3']                                     = (Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') or Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteAutre')) ? 
																	 (signataire.adresseMandataire.codePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire) : '';
formFields['signatureDate']                                          = signataire.formaliteSignatureDate;
formFields['signatureLieu']                                          = signataire.formaliteSignatureLieu;
formFields['estEIRL_oui']                                            = infoCessation.estEIRLOuiNon ? true : false;
formFields['estEIRL_non']                                            = infoCessation.estEIRLOuiNon ? false : true;
formFields['signature']                                              = '';


// Intercalaire PEIRL ME
								
//Cadre 1 - Déclaration ou modification d'affectation de patrimoine

formFields['eirl_complete_P2p4']                                                            = true;
formFields['eirl_complete_p0PLME']                                                          = false;
formFields['declaration_initiale']															= false;
formFields['declaration_modification']                                                      = true;

// Cadre 2 - Rappel d'identification

formFields['eirl_siren']                                                                    = identite.siren.split(' ').join('');
formFields['eirl_nomNaissance']                                                             = identite.personneLieePersonnePhysiqueNomNaissance;
formFields['eirl_nomUsage']                                                                 = identite.personneLieePersonnePhysiqueNomUsage;
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFields['eirl_prenom']                       = prenoms.toString();

// Cadre 4 - Rappel d'identification reltif à l'EIRL

var rappelIdentification = $p2p4rad.cadre3InformationsGroup.cadre3Informations.cadreAffectationPatrimoine.cadreRappelIdentificationEIRL;

formFields['eirl_rappelDenomination']                                                       = rappelIdentification.eirlRappelDenomination;
formFields['eirl_rappelAdresse']                                                            = Value('id').of($p2p4rad.cadre3InformationsGroup.cadre3Informations.cadreAffectationPatrimoine.cadreRappelIdentificationEIRL.eirlAdresse).eq('eirlAdresseEntreprise') ? 
																							((adressePro.etablissementAdresseNumeroVoie != null ? adressePro.etablissementAdresseNumeroVoie : '') 
																							+ ' ' + (adressePro.etablissementAdresseIndiceVoie != null ? adressePro.etablissementAdresseIndiceVoie : '')
																							+ ' ' + (adressePro.etablissementAdresseTypeVoie != null ? adressePro.etablissementAdresseTypeVoie : '')
																							+ ' ' + adressePro.etablissementAdresseNomVoie
																							+ ' ' + (adressePro.etablissementAdresseComplementVoie != null ? adressePro.etablissementAdresseComplementVoie : '')
																							+ ' ' + (adressePro.etablissementAdresseDistriutionSpecialeVoie != null ? adressePro.etablissementAdresseDistriutionSpecialeVoie : '')
																							+ ' ' +  adressePro.etablissementAdresseCodePostal
																							+ ' ' + adressePro.etablissementAdresseCommune)
																							: ((rappelIdentification.cadreEirlRappelAdresse.adresseNumeroVoieEIRL != null ? rappelIdentification.cadreEirlRappelAdresse.adresseNumeroVoieEIRL : '') 
																				            + ' ' + (rappelIdentification.cadreEirlRappelAdresse.adresseIndiceVoieEIRL != null ? rappelIdentification.cadreEirlRappelAdresse.adresseIndiceVoieEIRL : '')
																				            + ' ' + (rappelIdentification.cadreEirlRappelAdresse.adresseTypeVoieEIRL != null ? rappelIdentification.cadreEirlRappelAdresse.adresseTypeVoieEIRL : '')
																		                    + ' ' + rappelIdentification.cadreEirlRappelAdresse.adresseNomVoieEIRL
																							+ ' ' + (rappelIdentification.cadreEirlRappelAdresse.adresseComplementVoieEIRL != null ? rappelIdentification.cadreEirlRappelAdresse.adresseComplementVoieEIRL : '')
																							+ ' ' + (rappelIdentification.cadreEirlRappelAdresse.adresseDistriutionSpecialeVoieEIRL != null ? rappelIdentification.cadreEirlRappelAdresse.adresseDistriutionSpecialeVoieEIRL : '')
																							+ ' ' + rappelIdentification.cadreEirlRappelAdresse.adresseCodePostalEIRL
																							+ ' ' + rappelIdentification.cadreEirlRappelAdresse.adresseCommuneEIRL);
formFields['eirl_rappelLieuImmatriculation']                                                = rappelIdentification.eirlRappelLieuImmatriculation;

// Cadre 5 - Déclaration de cessation

var finDAP = $p2p4rad.cadre3InformationsGroup.cadre3Informations.cadreAffectationPatrimoine.finEIRLDeclaration;
																							
if (infoCessation.dateCessation !== null and infoCessation.estEIRLOuiNon) {
    var dateTmp = new Date(parseInt(infoCessation.dateCessation.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateFinEIRL']          = dateModif;
}
formFields['finEIRL_renonciationSansPoursuite']                                             = Value('id').of(finDAP.motifFinEIRL).contains('FinEIRLRenonciationSansPoursuite') ? true : false;
formFields['finEIRL_cessionPP']                                                             = Value('id').of(finDAP.motifFinEIRL).contains('FinEIRLCessionPP') ? true : false;
formFields['finEIRL_cessionPM']                                                             = Value('id').of(finDAP.motifFinEIRL).contains('FinEIRLCessionPM') ? true : false;	


/*
 * Création du dossier avec volet social : ajout du cerfa avec volet social
 */
 
var cerfaDoc1 = nash.doc //
	.load('models/cerfa_13905-04_p2p4_avec_volet_social.pdf') //
	.apply(formFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));

/*
 * Création du dossier sans volet social : ajout du cerfa sans volet social
 */
 
 var cerfaDoc2 = nash.doc //
	.load('models/cerfa_13905-04_p2p4_sans_volet_social.pdf') //
	.apply (formFields);
	cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));

/*
 * Ajout de l'intercalaire PEIRL ME avec option fiscale
 */
if ($p2p4rad.cadre3InformationsGroup.cadre3Informations.estEIRLOuiNon)
{
	var peirlMEDoc1 = nash.doc //
		.load('models/cerfa_14214-03_peirlme_avec_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(peirlMEDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire PEIRL ME sans option fiscale
 */
if ($p2p4rad.cadre3InformationsGroup.cadre3Informations.estEIRLOuiNon)
{
	var peirlMEDoc2 = nash.doc //
		.load('models/cerfa_14214-03_peirlme_sans_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(peirlMEDoc2.save('cerfa.pdf'));
}

/*
 * Ajout des PJs
 */

// PJ Déclarant

var pj=$p2p4rad.cadre9SignatureGroup.cadre9Signature.soussigne;

var pjUser = [];
var metas = [];

function pushPjPreview(fld) {
	fld.forEach(function (elm) {
        pjUser.push(elm);
		metas.push({'name':'document', 'value': '/'+elm.getAbsolutePath()});
    });
}

if(Value('id').of(pj).contains('FormaliteSignataireQualiteDeclarant')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantSignataire);
}

// PJ Mandataire

if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPouvoir);
}


/*
 * Enregistrement du fichier (en mémoire)
 */

var finalDoc = cerfaDoc1.save('P2P4_Cessation.pdf');

// Remove old metas before insert
var metasToDelete = [];
var recordMetas = nash.record.meta() != null ? nash.record.meta().metas : null;
var recordMetasSize = recordMetas != null ? recordMetas.size() : 0;

for (var i = 0; i < recordMetasSize; i++) {
   if (recordMetas.get(i).name == 'document') {
       metasToDelete.push({'name':'document', 'value': recordMetas.get(i).value});
   }
}
nash.record.removeMeta(metasToDelete);

nash.record.meta(metas);

var data = [ spec.createData({
    id : 'record',
    label : 'Déclaration de cessation d\'activité d\'une entreprise libérale ou d\'une personne non immatriculée à un registre public en micro-entrepreneur',
    description : 'Voici le formulaire obtenu à partir des données saisies.',
    type : 'FileReadOnly',
    value : [ finalDoc ]
}),  spec.createData({
    id : 'attachments',
    label : 'Pièces jointes',
    description : 'Pièces jointes ajoutées au formulaire.',
    type : 'FileReadOnly',
    value : pjUser
}) ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Déclaration de cessation d\'activité d\'une entreprise libérale ou d\'une personne non immatriculée à un registre public en micro-entrepreneur',
    groups : groups
});