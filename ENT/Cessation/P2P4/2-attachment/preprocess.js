// PJ Déclarant

var userDeclarant;
if ($p2p4rad.cadre1Identite.cadre1RappelIdentification.personneLieePersonnePhysiqueNomUsage != null) {
    var userDeclarant = $p2p4rad.cadre1Identite.cadre1RappelIdentification.personneLieePersonnePhysiqueNomUsage + '  ' + $p2p4rad.cadre1Identite.cadre1RappelIdentification.personneLieePersonnePhysiquePrenom1[0] ;
} else {
    var userDeclarant = $p2p4rad.cadre1Identite.cadre1RappelIdentification.personneLieePersonnePhysiqueNomNaissance + '  '+ $p2p4rad.cadre1Identite.cadre1RappelIdentification.personneLieePersonnePhysiquePrenom1[0] ;
}

var pj=$p2p4rad.cadre9SignatureGroup.cadre9Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteDeclarant')) {
    attachment('pjIDDeclarantSignataire', 'pjIDDeclarantSignataire', { label: userDeclarant, mandatory:"true"});
}

// PJ Mandataire

var userMandataire=$p2p4rad.cadre9SignatureGroup.cadre9Signature.adresseMandataire

var pj=$p2p4rad.cadre9SignatureGroup.cadre9Signature.soussigne;
if (Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
    attachment('pjIDMandataireSignataire', 'pjIDMandataireSignataire', {label: userMandataire.nomPrenomDenominationMandataire, mandatory:"true"});
	attachment('pjPouvoir', 'pjPouvoir', {mandatory:"true"});
}