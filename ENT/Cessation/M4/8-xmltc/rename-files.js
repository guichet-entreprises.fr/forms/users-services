//--------------- FUNCTIONS -------------------//
function pad(s) { return (s < 10) ? '0' + s : s; }

function formatDate() {
	var dateTemp = new Date();
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	return {
		'date' : year.toString() + month.toString() + day.toString(),
		'time' : dateTemp.toTimeString().substr(0,8).replace(new RegExp(':', 'g'), '')
	};
}

function extractLiasse(authority) {
	return JSON.parse(nash.xml.extract('/XML_REGENT_' + authority.ediCode + '.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02'))['C02'];
}

function getReferentials() {
	return {
		'C': {
			'recipient' : '0016A1002',
			'cerfa' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PMGUN'
			},
			'xmlregent' :{
				'typepj' : 'TFORMA',
				'suffixe' : 'PMGUN'
			},
			'others' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PMGUN'
			}
		},

		'G': {
			'recipient' : '1000A1001',
			'cerfa' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PGUEN'
			},
			'xmlregent' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PGUEN'
			},
			'others' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PGUEN'
			}
		},
		
		'X': {
			'recipient' : '0016A1004',
			'cerfa' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PMGUN'
			},
			'xmlregent' :{
				'typepj' : 'TFORMA',
				'suffixe' : 'PMGUN'
			},
			'others' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PMGUN'
			}
		},
		
		'U': {
			'recipient' : '0016A1005',
			'cerfa' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PMGUN'
			},
			'xmlregent' :{
				'typepj' : 'TFORMA',
				'suffixe' : 'PMGUN'
			},
			'others' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PMGUN'
			}
		},
		
		'M': {
			'recipient' : '0016A1003',
			'cerfa' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PMGUN'
			},
			'xmlregent' :{
				'typepj' : 'TFORMA',
				'suffixe' : 'PMGUN'
			},
			'others' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PMGUN'
			}
		}
	};
}

function displayCerfa(codeEdi, liasse, index) {
	var referentials = getReferentials();
	var network = codeEdi.charAt(0);
	var typepj = referentials[network]['cerfa']['typepj'];
	var suffixe = referentials[network]['cerfa']['suffixe'];
	
	var display = formatDate();
	return 'C' + referentials[network]['recipient'] + 'L' + liasse.substr(6) + 'D' +  display.date + 'H' + display.time + typepj + 'S00' + index + suffixe + '.pdf';
} 

function displayRegent(codeEdi, liasse, index) {
	var referentials = getReferentials();
	var network = codeEdi.charAt(0);
	var typepj = referentials[network]['xmlregent']['typepj'];
	var suffixe = referentials[network]['xmlregent']['suffixe'];
	
	var display = formatDate();
	return 'C' + referentials[network]['recipient'] + 'L' + liasse.substr(6) + 'D' +  display.date + 'H' + display.time + typepj + 'S00' + index + suffixe + '.xml';
}

function displayUserAttachment(codeEdi, liasse, index, extensionFile) {
	var referentials = getReferentials();	
	var network = codeEdi.charAt(0);
	var typepj = referentials[network]['others']['typepj'];
	var suffixe = referentials[network]['others']['suffixe'];
	
	var display = formatDate();
	return 'C' + referentials[network]['recipient'] + 'L' + liasse.substr(6) + 'D' +  display.date + 'H' + display.time + typepj + index + suffixe + '.' + extensionFile;
}

//--------------- FUNCTIONS -------------------//

//--------------- Variables init --------------//
var authorities = $computeDestinataire.context.authorities.authority;
var cerfaSigned = nash.util.resourceFromPath('/4-signature/generated/proxyResult.files-1-PROXY_SIGNED_DOCUMENT.pdf');
log.info('cerfaSigned : {}', cerfaSigned);

var userAttachmentGroup = _record.preprocess.preprocess;
var indiceUserAttachment = "S0";
var documents = [];
var computeDestinataire = nash.instance.load('/5-payment/computeDestinataire.xml');
//--------------- Variables init --------------//

//--------------- Rename CERFA and REGENT attachment --------------//
for (var idx = 0; idx < authorities.size(); idx++ ) {
	var otherFiles = [];
	var indexFile = 2;
	var authority = authorities.get(idx);
	var liasse = extractLiasse(authority);
	
	//-->Creating REGENT with new name into record
	var newRegentFileName = displayRegent(authority.ediCode, liasse, indexFile);	
	nash.record.saveFile(newRegentFileName, nash.util.resourceFromPath('/XML_REGENT_' + authority.ediCode + '.xml').getContent() );
	log.debug('Rename REGENT as : {}', newRegentFileName);
	indexFile++;
	
	var newCerfaFileName = displayCerfa(authority.ediCode, liasse, indexFile);
	nash.record.saveFile(newCerfaFileName, cerfaSigned.getContent() );
	log.debug('Rename CERFA as {}', newCerfaFileName);
	indexFile++;
	
	//--------------- Rename user attachment --------------//
	for (var attachment in userAttachmentGroup ) {
		log.debug('Start renaming user attachment');
		var pjGroup = userAttachmentGroup[attachment];

		for (var i = 0; i < pjGroup.length; i++) {
			var filePath = '/2-attachment/uploaded/preprocess.' + attachment + '-' + pjGroup[ i ].id + '-' + pjGroup[ i ].label;
			var fileType = pjGroup[i].label.split('.');
			var extensionFile = fileType[fileType.length - 1];

			var indiceUserAttachment = ( indexFile < 10 ) ? ('S00' + indexFile) : ('S0' + indexFile);
			log.info('indice user attachment : {}', indiceUserAttachment);
			
			var userAttachmentFileName = displayUserAttachment(authority.ediCode, liasse, indiceUserAttachment, extensionFile);
			nash.record.saveFile(userAttachmentFileName, nash.util.resourceFromPath(filePath).getContent() );
			log.debug('Rename user attachment as {}', userAttachmentFileName);	
			indexFile++;
			
			otherFiles.push(userAttachmentFileName);
		}
	}
	//--------------- Rename user attachment --------------//
	
	//--------------- Bind results --------------//	
	computeDestinataire.bind('context.authorities.authority[' + idx + ']', {
		'documents' : {
			'cerfa' : newCerfaFileName,
			'regent' : newRegentFileName,
			'others' : otherFiles.join(';')
		}
	});
	//--------------- Bind results --------------//
}

//--------------- Rename CERFA and REGENT attachment --------------//

