
//--------------- Variables init --------------//
var algo = "trouver Cfe";
var typePersonne = "PM";
var attachment = "/4-signature/generated/proxyResult.files-1-PROXY_SIGNED_DOCUMENT.pdf";
var optionCMACCI = "NON";
var codeCommune = $m4.cadre2AdresseEtablissementGroup.cadreAdresseEtablissement.adresseEtablissementPrincipal.commune.getId();
//--------------- Variables init --------------//

//--------------- Referentials --------------//
var referentials = [
	{
		'key' : 'SAS',
		'values' : [
			'5710', '5720'
		]
	},	
	{
		'key' : 'SCA',	
		'values' : [
			'5308', '5309'
		]
	},
	{
		'key' : 'SNC',	
		'values' : [
			'5202', '5203'
		]
	},
	{
		'key' : 'SCS',	
		'values' : [
			'5306', '5307'
		]
	},
	{
		'key' : 'SARL',	
		'values' : [
			'5499', 
			'5410',
			'5415',
			'5422',
			'5426',
			'5430',
			'5431',
			'5432',
			'5442',
			'5443',
			'5451',
			'5453',
			'5454',
			'5455',
			'5458',
			'5459',
			'5460'
		]
	},
	{
		'key' : 'SEL',	
		'values' : [
			'5485',
			'5785',
			'5585',
			'5685',
			'5385',
			'6589',
			'6521',
			'6532',
			'6539',
			'6540',
			'6541',
			'6542',
			'6599',
			'6543',
			'6544',
			'6554',
			'6558',
			'6560',
			'6561',
			'6562',
			'6563',
			'6564',
			'6565',
			'6566',
			'6567',
			'6568',
			'6569',
			'6571',
			'6572',
			'6573',
			'6574',
			'6575',
			'6576',
			'6577',
			'6578',
			'6585',
			'6534',
			'6536',
			'6316'
		]
	},
	{
		'key' : 'SA',	
		'values' : null
	}
];
//--------------- Referentials --------------//

//--------------- Calling Pushing box --------------//
pushingbox();
//--------------- Calling Pushing box --------------//

//--------------- Determing input parameters to Directory algorithm --------------//
var formJuridique = "SA";
referentials.forEach(function (item) {
	if (null != item.values && item.values.indexOf($m4.cadre1Identite.cadre1RappelIdentification.entrepriseFormeJuridique.getId()) != -1) {
		formJuridique = item.key;
	}
}); 

if ([
'5485',
'5785',
'5585',
'5685',
'5385',
'6589',
'6521',
'6532',
'6539',
'6540',
'6541',
'6542',
'6599',
'6543',
'6544',
'6554',
'6558',
'6560',
'6561',
'6562',
'6563',
'6564',
'6565',
'6566',
'6567',
'6568',
'6569',
'6571',
'6572',
'6573',
'6574',
'6575',
'6576',
'6577',
'6578',
'6585',
'6534',
'6536',
'6316'
].indexOf($m4.cadre1Identite.cadre1RappelIdentification.entrepriseFormeJuridique.getId()) != -1) 	{
	var secteur1 =  "LIBERAL";
} else if ($m4.cadre1Identite.cadre1RappelIdentification.immatriculationRM) {
	var secteur1 =  "ARTISANAL";
} else { 
	var secteur1 =  "COMMERCIAL";
}

log.info("algo is {}", algo);
log.info("secteur1 is {}", secteur1);
log.info("typePersonne is {}", typePersonne);
log.info("formJuridique is {}", formJuridique);
log.info("optionCMACCI is {}", optionCMACCI);
log.info("codeCommune is {}", codeCommune);
//--------------- Determing input parameters to Directory algorithm --------------//

return spec.create({
	id : 'prepareSend',
	label : "Préparation de la recherche du destinataire",
	groups : [ spec.createGroup({
		id : 'view',
		label : "Informations",
		data : [ spec.createData({
			id : 'algo',
			label : "Algo",
			type : 'String',
			mandatory : true,
			value : algo
		}), spec.createData({
			id : 'secteur1',
			label : "Secteur",
			type : 'String',
			value : secteur1
		}), spec.createData({
			id : 'typePersonne',
			label : "Type personne",
			type : 'String',
			value : typePersonne
		}), spec.createData({
			id : 'formJuridique',
			label : "Forme juridique",
			type : 'String',
			value : formJuridique
		}), spec.createData({
			id : 'optionCMACCI',
			label : "Option CMACCI",
			type : 'String',
			value : optionCMACCI
		}), spec.createData({
			id : 'codeCommune',
			label : "Code commune",
			type : 'String',
			value : codeCommune
		}), spec.createData({
			id : 'attachment',
			label : "Pièce jointe",
			type : 'String',
			value : attachment
		}) ]
	}) ]
});
