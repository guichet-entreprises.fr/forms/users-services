//--------------- FUNCTIONS -------------------//
function getEvent() {
	//-->28M : Transmission du patrimoine à l’associé unique restant
	//-->41M : Disparition de la personne morale par suite de fusion ou de scission
	//-->42M : Disparition de la personne morale
	//-->43M : Fermeture de l’établissement principal d’une société étrangère
	
	if (Value('id').of($m4.cadre1Identite.cadre1RappelIdentification.entrepriseFormeJuridique).eq('3120') or Value('id').of($m4.cadre1Identite.cadre1RappelIdentification.entrepriseFormeJuridique).eq('5800') or Value('id').of($m4.cadre1Identite.cadre1RappelIdentification.entrepriseFormeJuridique).eq('3110')) { return '43M'; }
	if ( $m4.cadre3InformationsGroup.cadre3Informations.motifCessation.getId() == 'fusion' or $m4.cadre3InformationsGroup.cadre3Informations.motifCessation.getId() == 'scission') { return '41M'; }
	if ( $m4.cadre3InformationsGroup.cadre3Informations.motifCessation.getId() == 'transfertPatrimoine' ) { return '28M'; }
	
	return '42M';
}
//--------------- FUNCTIONS -------------------//

//--------------- MAIN -------------------//
var eventCode = getEvent();
log.info('Found REGENT event : {}', eventCode);
nash.instance //
	.load('event.xml') //
	.bind('event', {
		'code' : eventCode,
		'version' : 'V2008.11'
	}
	);
