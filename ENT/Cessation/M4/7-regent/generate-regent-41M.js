//--------------- FUNCTIONS -------------------//
function pad(s) { return (s < 10) ? '0' + s : s; }

function parseDate(dateField, separator) {
	if (null == dateField) {
		return '';
	}
	
	if (null == separator) {
		separator = '';
	}
	
    var dateTmp = new Date(parseInt(dateField.getTimeInMillis()));	
	var date = dateTmp.getFullYear().toString();
	date = date.concat(separator);
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(separator);
    date = date.concat(pad(dateTmp.getDate().toString()));
	
    return date;
}

function getFieldValue(field) { return null == field ? '' : field; }

function buildRegent(eventRegent, regentFields, regentVersion, authorityType, authorityId, liasse) {			
	var response = null;
	try {		
		response = nash.service.request('${regent.baseUrl}/private/v1/xml-regent/generate/{regentVersion}/{authorityType}/{authorityId}', regentVersion, authorityType, authorityId) //
			.connectionTimeout(10000) //
			.receiveTimeout(10000) //
			.accept('json') //
			.dataType('application/json') // 
			.continueOnError(true) //
			.param('listTypeEvenement', eventRegent) //
			.param('liasseNumber', liasse) //
			.post(JSON.stringify(regentFields)); 
		;
	} catch (e) {	
		log.error(e);			
		throw 'An technical error occured when generating XML Regent for authority : ' + authorityId;
	}
	
	var xmlRegentFileName = 'XML_REGENT_' + authorityId + '.xml';
	var xmlValidationErrorsFileName = 'XML_VALIDATION_ERRORS_' + authorityId + '.xml';
	
	if (response != null && response.getStatus() == 200) {
		nash.record.saveFile(xmlRegentFileName, response.asBytes());
		return '/' + xmlRegentFileName;
	} else {
		var returnedResponse = response.asObject();
		nash.record.saveFile(xmlRegentFileName, returnedResponse['regent'].getBytes());
		nash.record.saveFile(xmlValidationErrorsFileName, returnedResponse['errors'].getBytes());
		
		log.error('XML Regent generation failed : files are generated.');
	}
	throw 'Unsuccessful XML Regent generation for authority : ' + authorityId;
}
//--------------- FUNCTIONS -------------------//

//--------------- Variables init --------------//
var regentVersion = _input.event.version;  // (V2008.11, V2016.02)
var regEx = new RegExp('[0-9]{5}');
var eventRegent = _input.event;
var authorityRegentFields = [];

//--------------- Variables init --------------//

//--------------- Main --------------//
log.info('Build regent fields for event : {}', eventRegent.code);
var authorities = $computeDestinataire.context.authorities.authority;
var ediCodes = [];
var liasse = null;
	for (var idx = 0; idx < authorities.size(); idx++ ) { ediCodes.push(authorities.get(idx).ediCode); }

for (var idx = 0; idx < authorities.size(); idx++ ) {
	var authority =  authorities.get(idx);
	log.debug('Prepare regent fields for authority : {}', authority.ediCode);
		
	//--------------- Default regent fields value --------------//
	var regentFields = [];
	regentFields.push({ 'path' : '/REGENT-XML/Emetteur', 'value' : 'Z1611'});
	regentFields.push({ 'path' : '/REGENT-XML/Destinataire', 'value' : authority.ediCode});
	regentFields.push({ 'path' : '/REGENT-XML/DateHeureEmission', 'value' : null});
	regentFields.push({ 'path' : '/REGENT-XML/VersionMessage', 'value' : null});
	regentFields.push({ 'path' : '/REGENT-XML/VersionNorme', 'value' : null});
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Specification/NomService', 'value' : null});
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Specification/VersionService', 'value' : null});
	//--------------- Default regent fields value --------------//

	//Module Occur O Observations
	//GDF 1 O
	//ICM 1 O
	//FJM 1 O
	//FSM 1 O
	//CAM 1 O
	//JGM 1 C
	//IPU 1 O
	//SIU 1 O
	//CPU 1 O
	//LIU 1 C
	//GRD 999 C
	//GFE 999 C

	//-->GDF : Groupe Données de Service
	//IDF Identification de la formalité 1 O
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C01', 'value' : null});
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02', 'value' : null});
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C03', 'value' :'0'});
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C04', 'value' : null});

	typeLiasse = 'C';
	var entrepriseFormeJuridique = $m4.cadre1Identite.cadre1RappelIdentification.entrepriseFormeJuridique;
	if ( Value('id').of(entrepriseFormeJuridique).eq('5485')
						or Value('id').of(entrepriseFormeJuridique).eq('5785')
						or Value('id').of(entrepriseFormeJuridique).eq('5585')
						or Value('id').of(entrepriseFormeJuridique).eq('5685')
						or Value('id').of(entrepriseFormeJuridique).eq('5385')
						or Value('id').of(entrepriseFormeJuridique).eq('6589')
						or Value('id').of(entrepriseFormeJuridique).eq('6521')
						or Value('id').of(entrepriseFormeJuridique).eq('6532')
						or Value('id').of(entrepriseFormeJuridique).eq('6539')
						or Value('id').of(entrepriseFormeJuridique).eq('6540')
						or Value('id').of(entrepriseFormeJuridique).eq('6541')
						or Value('id').of(entrepriseFormeJuridique).eq('6542')
						or Value('id').of(entrepriseFormeJuridique).eq('6599')
						or Value('id').of(entrepriseFormeJuridique).eq('6543')
						or Value('id').of(entrepriseFormeJuridique).eq('6544')
						or Value('id').of(entrepriseFormeJuridique).eq('6554')
						or Value('id').of(entrepriseFormeJuridique).eq('6558')
						or Value('id').of(entrepriseFormeJuridique).eq('6560')
						or Value('id').of(entrepriseFormeJuridique).eq('6561')
						or Value('id').of(entrepriseFormeJuridique).eq('6562')
						or Value('id').of(entrepriseFormeJuridique).eq('6563')
						or Value('id').of(entrepriseFormeJuridique).eq('6564')
						or Value('id').of(entrepriseFormeJuridique).eq('6565')
						or Value('id').of(entrepriseFormeJuridique).eq('6566')
						or Value('id').of(entrepriseFormeJuridique).eq('6567')
						or Value('id').of(entrepriseFormeJuridique).eq('6568')
						or Value('id').of(entrepriseFormeJuridique).eq('6569')
						or Value('id').of(entrepriseFormeJuridique).eq('6571')
						or Value('id').of(entrepriseFormeJuridique).eq('6572')
						or Value('id').of(entrepriseFormeJuridique).eq('6573')
						or Value('id').of(entrepriseFormeJuridique).eq('6574')
						or Value('id').of(entrepriseFormeJuridique).eq('6575')
						or Value('id').of(entrepriseFormeJuridique).eq('6576')
						or Value('id').of(entrepriseFormeJuridique).eq('6577')
						or Value('id').of(entrepriseFormeJuridique).eq('6578')
						or Value('id').of(entrepriseFormeJuridique).eq('6585')
						or Value('id').of(entrepriseFormeJuridique).eq('6534')
						or Value('id').of(entrepriseFormeJuridique).eq('6536')
						or Value('id').of(entrepriseFormeJuridique).eq('6316')) 	{
		typeLiasse =  "C";
	} else if ($m4.cadre1Identite.cadre1RappelIdentification.immatriculationRM){
		typeLiasse =  "M";
	}
	// En cas de deuxième autorité
	if (idx == 1) {
		typeLiasse = 'C';
	}
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05', 'value' : typeLiasse});
	
	if ($m4.cadre1Identite.cadre1RappelIdentification.immatriculationRM and idx == 0) {
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C06', 'value' : 'N'});
	}
		
	//EDF Evénement déclaré dans la formalité 1 O
	var dateLiquidation = $m4.cadre3InformationsGroup.cadre3Informations.dateLiquidation;
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.1', 'value' : eventRegent.code});
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2', 'value' : parseDate(dateLiquidation, '-')});

	//DMF Destinataire de la formalité 1 O
	for (var ediCodeIdx = 0; ediCodeIdx < ediCodes.length; ediCodeIdx++ ) {
		var ediCode = ediCodes[ediCodeIdx];
		regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[' + ( ediCodeIdx + 1 ) + ']' , 'value' : ediCode});
	}
	
	//ADF Adresse de correspondance 1 C si elle existe
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C36', 'value' : Value('id').of($m4.cadre8RensCompGroup.cadre8RensComp.adresseCorrespond).eq('autre') ? //
		$m4.cadre8RensCompGroup.cadre8RensComp.adresseCorrespondance.nomPrenomDenomination : //
		$m4.cadre1Identite.cadre1RappelIdentification.entrepriseDenomination});
	
	
	var adresseCorrespondance = $m4.cadre2AdresseEtablissementGroup.cadreAdresseEtablissement.adresseEtablissementPrincipal;
	if ( Value('id').of($m4.cadre8RensCompGroup.cadre8RensComp.adresseCorrespond).eq('autre') ) {
		adresseCorrespondance = $m4.cadre8RensCompGroup.cadre8RensComp.adresseCorrespondance;
	}
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3', 'value' : adresseCorrespondance.commune.getId()});
	
	if (null != adresseCorrespondance.numeroVoie) { regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.5', 'value' : getFieldValue(adresseCorrespondance.numeroVoie)});}	
	if (adresseCorrespondance.indiceVoie !== null) {
			var monId1 = Value('id').of(adresseCorrespondance.indiceVoie)._eval();
		regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6', 'value' : monId1}); 
		}
	if (null != adresseCorrespondance.distributionSpecialeVoie) { regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.7', 'value' : getFieldValue(adresseCorrespondance.distributionSpecialeVoie)}); }
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8', 'value' : getFieldValue(adresseCorrespondance.codePostal)});
	if (null != adresseCorrespondance.complementVoie) { regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.10', 'value' : getFieldValue(adresseCorrespondance.complementVoie)}); }
	if (adresseCorrespondance.typeVoie !== null) {
			var monId1 = Value('id').of(adresseCorrespondance.typeVoie)._eval();
		regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11', 'value' : monId1}); 
		}
	if (null != adresseCorrespondance.nomVoie) { regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12', 'value' : getFieldValue(adresseCorrespondance.nomVoie)}); }		
	log.debug('Commune de correspondance : {}', adresseCorrespondance.commune);
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13', 'value' : adresseCorrespondance.commune.getLabel()});
	
	var infosSup = $m4.cadre8RensCompGroup.cadre8RensComp.infosSup;
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.1[1]', 'value' : infosSup.telephone2.e164.replace('+', '00')});

	if (null != infosSup.telephone1) {
		regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.1[2]', 'value' : infosSup.telephone1.e164.replace('+', '00')});
	}

	if (null != infosSup.telecopie) {
		regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.2', 'value' : infosSup.telecopie.e164.replace('+', '00')});
	}

	if (null != infosSup.courriel) {
		regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.3', 'value' : infosSup.courriel});
	}
	
	//SIF Signature de la formalité 1 O
	var signature = $m4.cadre9SignatureGroup.cadre9Signature;		
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.1', 'value' : signature.adresseMandataire.nomPrenomDenomination}); 
																												 
																																 
	
	if ( Value('id').of(signature.soussigne). eq('mandataire') ) {
													  
		regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.2', 'value' : "Mandataire"});
	} else if( Value('id').of(signature.soussigne). eq('autrePersonne') ) {
		regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.2', 'value' : "Autre personne"});
	}
	
	if ( Value('id').of(signature.soussigne). eq('mandataire') or Value('id').of(signature.soussigne). eq('autrePersonne') ) {
		var adresseMandataire = signature.adresseMandataire;
		regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.3', 'value' : adresseMandataire.commune.getId()});
  
		if (adresseMandataire.numeroVoie != null) {
			regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.5', 'value' : getFieldValue(adresseMandataire.numeroVoie)});
		}
  
		if (adresseMandataire.indiceVoie !== null) {
			var monId1 = Value('id').of(adresseMandataire.indiceVoie)._eval();
		   regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.6', 'value' : monId1});
		}
		if (adresseMandataire.distributionSpecialeVoie != null) {
			regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.7', 'value' : getFieldValue(adresseMandataire.distributionSpecialeVoie)});
		}
		
		regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.8', 'value' : adresseMandataire.codePostal});
		
		if (adresseMandataire.complementVoie != null) {
			regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.10', 'value' : getFieldValue(adresseMandataire.complementVoie)});
		}
		
		if (adresseMandataire.typeVoie !== null) {
			var monId1 = Value('id').of(adresseMandataire.typeVoie)._eval();
		   regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.11', 'value' : monId1});
		}
  
		regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.12', 'value' : getFieldValue(adresseMandataire.nomVoie)});
		regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.13', 'value' : adresseMandataire.commune.getLabel()});
	}
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C41', 'value' : signature.lieu});
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C42', 'value' : parseDate(signature.date, '-')});
	
	if (null != $m4.cadre8RensCompGroup.cadre8RensComp.observations ) { 
		regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C43', 'value' : $m4.cadre8RensCompGroup.cadre8RensComp.observations});
	}
	
	//ICM
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/ICM/M01/M01.1', 'value' : $m4.cadre1Identite.cadre1RappelIdentification.entrepriseDenomination});
	if ( $m4.cadre3InformationsGroup.cadre3Informations.activiteAmbulant ) {
		regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/ICM/M02', 'value' : 'O'});
	}

	//FJM
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FJM/M21/M21.1', 'value' : entrepriseFormeJuridique.getId() });
	if ( Value('id').of(entrepriseFormeJuridique).eq('5720') ) {
		regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FJM/M23', 'value' : 'O'});
	}
	
	//FSM
	var motifCessation = $m4.cadre3InformationsGroup.cadre3Informations.motifCessation;
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M54', 'value' : (Value('id').of(motifCessation).eq('fusion') ? '1' : '2') });
	
	//CAM
	var dateCessation = $m4.cadre3InformationsGroup.cadre3Informations.dateCessation;
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CAM/M58', 'value' : parseDate(dateCessation, '-') });
	
	//JGM
	//-->No fields
	
	//IPU
	
	//Récupérer le EntityId du greffe
	var greffeId = $m4.cadre1Identite.cadre1RappelIdentification.immatRCSGreffe.id ;
	//Appeler Directory pour récupérer l'autorité
	_log.info('l entityId du greffe selectionne est {}', greffeId);
	var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', greffeId) //
			   .connectionTimeout(10000) //
			   .receiveTimeout(10000) //
			   .accept('json') //
			   .get();
	//result
	var receiverInfo = response.asObject();
	var contactInfo = !receiverInfo.details ? null : receiverInfo.details;
	//Récupérer le code EDI
	var codeEDIGreffe = !contactInfo.ediCode ? null : contactInfo.ediCode;
	_log.info('le code edi du greffe selectionne est {}', codeEDIGreffe);
	// À modifier quand Destiny fourni le réferentiel
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U01/U01.1', 'value' : codeEDIGreffe });
	
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U02', 'value' : $m4.cadre1Identite.cadre1RappelIdentification.siren.split(' ').join('') });
	
	if ($m4.cadre1Identite.cadre1RappelIdentification.immatriculationRM) {
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U04', 'value' : $m4.cadre1Identite.cadre1RappelIdentification.immatRMCMA.getId() });
	}
	
	var presenceSalaries = (null != $m4.cadre3InformationsGroup.cadre3Informations.presenceSalaries && $m4.cadre3InformationsGroup.cadre3Informations.presenceSalaries) ? 'O' : 'N';
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U06', 'value' : presenceSalaries });
	
	//SIU
	var adresseCorrespondance = $m4.cadre2AdresseEtablissementGroup.cadreAdresseEtablissement.adresseEtablissementPrincipal;
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.3', 'value' : adresseCorrespondance.commune.getId()});
	
	if (null != adresseCorrespondance.numeroVoie) { regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.5', 'value' : getFieldValue(adresseCorrespondance.numeroVoie)});}	
	if (adresseCorrespondance.indiceVoie !== null) {
			var monId1 = Value('id').of(adresseCorrespondance.indiceVoie)._eval();
		regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.6', 'value' : monId1}); 
		}
	if (null != adresseCorrespondance.distributionSpecialeVoie) { regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.7', 'value' : getFieldValue(adresseCorrespondance.distributionSpecialeVoie)}); }
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.8', 'value' : getFieldValue(adresseCorrespondance.codePostal)});
	if (null != adresseCorrespondance.complementVoie) { regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.10', 'value' : getFieldValue(adresseCorrespondance.complementVoie)}); }
	if (adresseCorrespondance.typeVoie !== null) {
			var monId1 = Value('id').of(adresseCorrespondance.typeVoie)._eval();
		regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.11', 'value' : monId1}); 
		}
	if (null != adresseCorrespondance.nomVoie) { regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.12', 'value' : getFieldValue(adresseCorrespondance.nomVoie)}); }		
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.13', 'value' : adresseCorrespondance.commune.getLabel()});
	//CPU
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U27', 'value' : presenceSalaries });
	
	//GRD	
	//DIU
	var personneAffilieeOuiNon = $m4.cadre7DeclarationSocialeGroup.cadre7DeclarationSocialeComp.personneAffilieeOuiNon;
	if ( personneAffilieeOuiNon ) {
		var personneAffiliees = $m4.cadre7DeclarationSocialeGroup.cadre7DeclarationSocialeComp.personneAffiliee;
		for (var personneAffilieeIdx = 0; personneAffilieeIdx <  personneAffiliees.size(); personneAffilieeIdx++) {
			var personneAffiliee = personneAffiliees.get(personneAffilieeIdx);
			
			regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + (personneAffilieeIdx + 1) + ']/DIU/U50/U50.1', 'value' : '2' });
			regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + (personneAffilieeIdx + 1) + ']/DIU/U51/U51.1', 'value' : personneAffiliee.personneLieeQualite.getId() });
			regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + (personneAffilieeIdx + 1) + ']/DIU/U52', 'value' : 'P' });
			
	//IPD		
		if (null != personneAffiliee.nomNaissance) { regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + (personneAffilieeIdx + 1) + ']/IPD/D01/D01.2', 'value' : personneAffiliee.nomNaissance }); }
			
			if (null != personneAffiliee.prenom) {	
				var prenoms = [];				
				for (var  i = 0; i < personneAffiliee.prenom.size() ; i++ ) { prenoms.push( personneAffiliee.prenom[i] ); } 
				regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + (personneAffilieeIdx + 1) + ']/IPD/D01/D01.3', 'value' : prenoms });
}
			
			if (null != personneAffiliee.nomUsage) { regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + (personneAffilieeIdx + 1) + ']/IPD/D01/D01.4', 'value' : personneAffiliee.nomUsage }); }
			if (null != personneAffiliee.dateNaissance) { regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + (personneAffilieeIdx + 1) + ']/IPD/D03/D03.1', 'value' : parseDate(personneAffiliee.dateNaissance, '-') }); }
			
			if ( Value('id').of(personneAffiliee.lieuNaissancePays).eq('FRXXXXX') ) {
				regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + (personneAffilieeIdx + 1) + ']/IPD/D03/D03.2', 'value' : personneAffiliee.lieuNaissanceCommune.getId() });
				regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + (personneAffilieeIdx + 1) + ']/IPD/D03/D03.4', 'value' : personneAffiliee.lieuNaissanceCommune.getLabel() });
			} else {
				var idPays = personneAffiliee.lieuNaissancePays.getId();
				var result = idPays.match(regEx)[0]; 
				regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + (personneAffilieeIdx + 1) + ']/IPD/D03/D03.2', 'value' : result });
				regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + (personneAffilieeIdx + 1) + ']/IPD/D03/D03.3', 'value' : personneAffiliee.lieuNaissancePays.getLabel() });
			}
			
		// NPD	
		/* if (null != personneAffiliee.nationalite) { regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + (idx + 1) + ']/NPD/D21', 'value' : personneAffiliee.nationalite }); }
		*/
	//GCS		
			log.debug('Authority {} with role {}', authority.ediCode, authority.role);
			if (null != personneAffiliee.numeroSS && (authority.role == 'CFE' || authority.role == 'CFE+TDR')) { 
				var nir = getFieldValue(personneAffiliee.numeroSS).replace(/ /g, '');
				regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + (personneAffilieeIdx + 1) + ']/GCS/ISS/A10/A10.1', 'value' : nir.substring(0, 13) });
				regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + (personneAffilieeIdx + 1) + ']/GCS/ISS/A10/A10.2', 'value' : nir.substring(13, 15) });
				regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + (personneAffilieeIdx + 1) + ']/GCS/CAS/A42/A42.1', 'value' : '.' });
			}
			}
	}
	
	//EtablissementFerme (GFE)
	
	// IAE
	if ($m4.cadre2AdresseEtablissementGroup.cadreAdresseEtablissement.autreEtablissementFerme &&
		null != $m4.cadre2AdresseEtablissementGroup.cadreAdresseEtablissement.autreEtablissement && 
		$m4.cadre2AdresseEtablissementGroup.cadreAdresseEtablissement.autreEtablissement.size() > 0) {
		var autreEtablissementGroup = $m4.cadre2AdresseEtablissementGroup.cadreAdresseEtablissement.autreEtablissement;
		for (var autreEtablissementIdx = 0; autreEtablissementIdx < autreEtablissementGroup.size(); autreEtablissementIdx++) {
			var autreEtablissement = autreEtablissementGroup.get(autreEtablissementIdx);
			regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[' + (autreEtablissementIdx + 1) + ']/IAE/E12/E12.3', 'value' : autreEtablissement.commune.getId()});
			if (autreEtablissement.numeroVoie != null) {
			regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[' + (autreEtablissementIdx + 1) + ']/IAE/E12/E12.5', 'value' : autreEtablissement.numeroVoie });
			}
			if (autreEtablissement.indiceVoie !== null) {
			var monId1 = Value('id').of(autreEtablissement.indiceVoie)._eval();
			regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[' + (autreEtablissementIdx + 1) + ']/IAE/E12/E12.6', 'value' : monId1 });
			}
			if (autreEtablissement.distributionSpecialeVoie != null) {
			regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[' + (autreEtablissementIdx + 1) + ']/IAE/E12/E12.7', 'value' : autreEtablissement.distributionSpecialeVoie });
			}
			regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[' + (autreEtablissementIdx + 1) + ']/IAE/E12/E12.8', 'value' : autreEtablissement.codePostal });
			if (autreEtablissement.complementVoie != null) {
			regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[' + (autreEtablissementIdx + 1) + ']/IAE/E12/E12.10', 'value' : autreEtablissement.complementVoie });
			}
			if (autreEtablissement.typeVoie !== null) {
			var monId1 = Value('id').of(autreEtablissement.typeVoie)._eval();
			regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[' + (autreEtablissementIdx + 1) + ']/IAE/E12/E12.11', 'value' : monId1 });
			}	
			regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[' + (autreEtablissementIdx + 1) + ']/IAE/E12/E12.12', 'value' : autreEtablissement.nomVoie });
			regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[' + (autreEtablissementIdx + 1) + ']/IAE/E12/E12.13', 'value' : autreEtablissement.commune.getLabel() });
			regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[' + (autreEtablissementIdx + 1) + ']/IAE/E13/E13.1', 'value' : "000000000" });
			regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[' + (autreEtablissementIdx + 1) + ']/IAE/E13/E13.2', 'value' : "00000" });
					
			if(autreEtablissement.dateCessationEmploi !== null) {
			var dateTemp = new Date(parseInt(autreEtablissement.dateCessationEmploi.getTimeInMillis()));
			var year = dateTemp.getFullYear();
			var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
			var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
			var date = year + "-" + month + "-" + day ;
			regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[' + (autreEtablissementIdx + 1) + ']/IAE/E14', 'value' : date });
			}

			//regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[' + (autreEtablissementIdx + 1) + ']/IAE/E17', 'value' : null });

			// DEE

			if (Value('id').of(autreEtablissement.destination).eq('destinationSuppression')) {
			regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[' + (autreEtablissementIdx + 1) + ']/DEE/E41/E41.1', 'value' : "C" });
			} else if (Value('id').of(autreEtablissement.destination).eq('destinationVente')) {
			regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[' + (autreEtablissementIdx + 1) + ']/DEE/E41/E41.1', 'value' : "3" });
			} else if (Value('id').of(autreEtablissement.destination).eq('destinationAutre')) {
			regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[' + (autreEtablissementIdx + 1) + ']/DEE/E41/E41.1', 'value' : "9" });
			regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[' + (autreEtablissementIdx + 1) + ']/DEE/E41/E41.2', 'value' : autreEtablissement.autreLibelle });
			}
			
		}
	}

	
	var regentFieldsInput = {};
	regentFields.forEach(function (field) { regentFieldsInput[field.path] = field.value; });
	log.debug('Display regent fields for authority code {} : {}', authority.ediCode, regentFieldsInput);
	var xmlRegentFile = buildRegent(eventRegent.code, regentFieldsInput, eventRegent.version, authority.role, authority.ediCode, liasse);
	
	liasse = JSON.parse(nash.xml.extract(xmlRegentFile, '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02')).C02;
	log.info('Extract liasse for authority ediCode {} : {}', authority.ediCode, liasse);
}

return spec.create({
	id : 'xmlGenerationConfirmation',
	label : "Xml Regent confirmation message",
	groups : [
		spec.createGroup({
			id : 'confirmationMessageOk',
			description : "Le fichier XML Regent a été généré et ajouté au dossier.",
			data : []
		}) 
	]
});
//--------------- Main --------------//
