if ( Value('id').of($m4.cadre9SignatureGroup.cadre9Signature.soussigne).eq('representantLegal') ) {
    attachment('pjIDDeclarantSignataire', 'pjIDDeclarantSignataire', { mandatory:"true"});
} else {
    attachment('pjIDMandataireSignataire', 'pjIDMandataireSignataire', { mandatory:"true"});
}

if ( Value('id').of($m4.cadre9SignatureGroup.cadre9Signature.soussigne).eq('mandataire') ) {
    attachment('pjPouvoir', 'pjPouvoir', { mandatory:"true"});
}

if ( Value('id').of($m4.cadre3InformationsGroup.cadre3Informations.motifCessation).eq('clotureLiquiation')
		or Value('id').of($m4.cadre3InformationsGroup.cadre3Informations.motifCessation).eq('autre')) {
    attachment('pjExemplaireClotureOperation', 'pjExemplaireClotureOperation', { mandatory:"true"});
    attachment('pjExemplaireCompteCloture', 'pjExemplaireCompteCloture', { mandatory:"true"});
    attachment('pjParution', 'pjParution', { mandatory:"true"});
}

if ( Value('id').of($m4.cadre3InformationsGroup.cadre3Informations.motifCessation).eq('fusion')) {
    if (not $m4.cadre3InformationsGroup.cadre3Informations.fusionSimplifiee) {
	attachment('pjPVFusion', 'pjPVFusion', { mandatory:"true"});
    }
	attachment('pjTraiteFusion', 'pjTraiteFusion', { mandatory:"true"});
    attachment('pjParution', 'pjParution', { mandatory:"true"});
	attachment('pjDeclarationConformite', 'pjDeclarationConformite', { mandatory:"true"});
    attachment('pjKbis', 'pjKbis', { mandatory:"true"});
    }
	
if (Value('id').of($m4.cadre3InformationsGroup.cadre3Informations.motifCessation).eq('scission')) {
	attachment('pjPVScission', 'pjPVScission', { mandatory:"true"});
	attachment('pjParution', 'pjParution', { mandatory:"true"});
	attachment('pjDeclarationConformite', 'pjDeclarationConformite', { mandatory:"true"});
    attachment('pjPVPouvoir', 'pjPVPouvoir', { mandatory:"true"});
    attachment('pjTraiteScission', 'pjTraiteScission', { mandatory:"false"});
    }
