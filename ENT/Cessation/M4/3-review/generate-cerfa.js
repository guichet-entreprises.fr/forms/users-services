//--------------- FUNCTIONS -------------------//
function pad(s) { return (s < 10) ? '0' + s : s; }

function parseDate(dateField, separator) {
	if (null == dateField) {
		return '';
	}
	
	if (null == separator) {
		separator = '';
	}
	
    var dateTmp = new Date(parseInt(dateField.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
	date = date.concat(separator);
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(separator);
	date = date.concat(dateTmp.getFullYear().toString());
	
    return date;
}

function pushPjPreview(fld) { fld.forEach(function (elm) { pjUser.push(elm); metas.push({'name':'document', 'value': '/'+elm.getAbsolutePath()}); }); }

function updateMetas(metas) {	
	//Remove old metas before insert
	var metasToDelete = [];
	var recordMetas = nash.record.meta() != null ? nash.record.meta().metas : null;
	var recordMetasSize = recordMetas != null ? recordMetas.size() : 0;

	for (var i = 0; i < recordMetasSize; i++) {
		if (recordMetas.get(i).name == 'document') {
			metasToDelete.push({'name':'document', 'value': recordMetas.get(i).value});
		}
	}
	nash.record.removeMeta(metasToDelete);

	// Insert new metas
	nash.record.meta(metas);
}

function getFieldValue(field) { return null == field ? '' : field; }

//--------------- Variables init -------------------//
var pdfFields = {};
var pjUser = [];
var metas = [];

//--------------- Rappel d'identification -------------------//
var identite = $m4.cadre1Identite.cadre1RappelIdentification;
pdfFields['entreprise_siren']                          = identite.siren.split(' ').join('');
pdfFields['entreprise_formeJuridique']                 = identite.entrepriseFormeJuridique;
pdfFields['entreprise_denomination']                   = identite.entrepriseDenomination;

pdfFields['immatRCS']                                  = true;
pdfFields['immatRCSGreffe']                            = identite.immatRCSGreffe;
pdfFields['immatRM']                                   = identite.immatriculationRM ? true : false;
pdfFields['immatRMCMA']                                = getFieldValue(identite.immatRMCMA);
pdfFields['immatRMDept']                               = identite.immatriculationRM ? identite.immatRMCMA : '';
pdfFields['immatRMDeptNum']                            = identite.immatriculationRM ? identite.immatRMCMA.getId() : '';

var adresseEtablissementPrincipal = $m4.cadre2AdresseEtablissementGroup.cadreAdresseEtablissement.adresseEtablissementPrincipal;

var adresseEtablissementPrincipalVoie = getFieldValue(adresseEtablissementPrincipal.numeroVoie) + //
														' ' + getFieldValue(adresseEtablissementPrincipal.indiceVoie) + //
														' ' + getFieldValue(adresseEtablissementPrincipal.typeVoie) + //
														' ' + getFieldValue(adresseEtablissementPrincipal.nomVoie) + //
														' ' + getFieldValue(adresseEtablissementPrincipal.complementVoie) + //
														' ' + getFieldValue(adresseEtablissementPrincipal.distributionSpecialeVoie) //
														;
													
pdfFields['entreprise_adresseEntreprisePM_voie']       = adresseEtablissementPrincipalVoie;
pdfFields['entreprise_adresseEntreprisePM_codePostal'] = adresseEtablissementPrincipal.codePostal;
pdfFields['entreprise_adresseEntreprisePM_commune']    = adresseEtablissementPrincipal.commune;

//--------------- Déclaration relative à la personne -------------------//
var infoCessation = $m4.cadre3InformationsGroup.cadre3Informations;
pdfFields['dateCessation']                             = parseDate(infoCessation.dateCessation);
pdfFields['dateCessationOperation']                    = parseDate(infoCessation.dateLiquidation);
pdfFields['activite_ambulant']                         = infoCessation.activiteAmbulant ? true : false;
if (not Value('id').of($m4.cadre1Identite.cadre1RappelIdentification.entrepriseFormeJuridique).eq('5800') and not Value('id').of($m4.cadre1Identite.cadre1RappelIdentification.entrepriseFormeJuridique).eq('3120') and not Value('id').of($m4.cadre1Identite.cadre1RappelIdentification.entrepriseFormeJuridique).eq('3110')) {
pdfFields['presenceSalarie_oui']                       = infoCessation.presenceSalaries ? true : false;
pdfFields['presenceSalarie_non']                       = !infoCessation.presenceSalaries ? true : false;
}
['clotureLiquiation', 'transfertPatrimoine', 'fusion', 'scission', 'autre'].forEach(function (key) {
	pdfFields['cessation_' + key]                      = Value('id').of(infoCessation.motifCessation).eq(key) ? true : false;
});

//--------------- Déclaration relative à la fermeture d'établissements -------------------//
if ( $m4.cadre2AdresseEtablissementGroup.cadreAdresseEtablissement.autreEtablissementFerme ) {
	var autreEtablissements = $m4.cadre2AdresseEtablissementGroup.cadreAdresseEtablissement.autreEtablissement;
	for (var idx = 0; idx < autreEtablissements.size(); idx++) {
		var etablissement = autreEtablissements.get(idx);
		pdfFields['etablissement_adresseVoie[' + idx + ']']         =   getFieldValue(etablissement.numeroVoie) + //
																		' ' + getFieldValue(etablissement.indiceVoie) + //
																		' ' + getFieldValue(etablissement.typeVoie) + //
																		' ' + getFieldValue(etablissement.nomVoie) + //
																		' ' + getFieldValue(etablissement.complementVoie) + //
																		' ' + getFieldValue(etablissement.distributionSpecialeVoie)
																		;
																				
		pdfFields['etablissement_adresseCP[' + idx + ']']               	 = etablissement.codePostal;
		pdfFields['etablissement_adresseCommune[' + idx + ']']               = etablissement.commune;
		
		['destinationSuppression', 'destinationVente', 'destinationAutre'].forEach(function (key) {
			pdfFields['etablissement_' + key + '[' + idx + ']']               = Value('id').of(etablissement.destination).eq(key) ? true : false;
		});
			
		if ( Value('id').of(etablissement.destination).eq('destinationAutre') ) {				
			pdfFields['etablissement_destinationAutreLibelle[' + idx + ']']  = etablissement.autreLibelle;
		}
		
		pdfFields['etablissement_cessationEmploi[' + idx + ']']              = parseDate(etablissement.dateCessationEmploi);
	}
}

//--------------- Personnes affiliées au régime TNS -------------------//
var declarationSociale = $m4.cadre7DeclarationSocialeGroup.cadre7DeclarationSocialeComp;
if ( declarationSociale.personneAffilieeOuiNon ) {
	var personneAffiliees = declarationSociale.personneAffiliee;
	for (var idx = 0; idx < personneAffiliees.size(); idx++) {
		var personneAffiliee = personneAffiliees.get(idx);
			
		pdfFields['dirigeant_nomNaissance[' + idx + ']']                 = getFieldValue(personneAffiliee.nomNaissance);
		pdfFields['dirigeant_nomUsage[' + idx + ']']                     = getFieldValue(personneAffiliee.nomUsage);
		
		var prenoms=[];
		if (null != personneAffiliee.prenom) {					
			for ( i = 0; i < personneAffiliee.prenom.size() ; i++ ){prenoms.push(personneAffiliee.prenom[i]);}                            
			pdfFields['dirigeant_prenom[' + idx + ']']                       = prenoms.toString();
		}
		
		pdfFields['dirigeant_dateNaissance[' + idx + ']']                = parseDate(personneAffiliee.dateNaissance);
		
		pdfFields['dirigeant_lieuNaissance[' + idx + ']']                = getFieldValue(personneAffiliee.lieuNaissancePays) + //
																		' / ' + (null != getFieldValue(personneAffiliee.lieuNaissanceCommune) && ''!= getFieldValue(personneAffiliee.lieuNaissanceCommune) ? getFieldValue(personneAffiliee.lieuNaissanceCommune) : getFieldValue(personneAffiliee.lieuNaissanceVille));
		
		var nir = getFieldValue(personneAffiliee.numeroSS).replace(/ /g, '');

		pdfFields['dirigeant_numSS[' + idx + ']']                        = nir.substring(0, 13);
		pdfFields['dirigeant_numSSCle[' + idx + ']']                     = nir.substring(13, 15);
		//pdfFields['formalite_nombreIntercalairesMPrime']       = page1.formaliteNombreIntercalairesMPrime;
	}
}

//--------------- Renseignements complémentaires -------------------//
var renseignementsComp = $m4.cadre8RensCompGroup.cadre8RensComp;
if ( Value('id').of(renseignementsComp.adresseCorrespond).eq('autre') ) {
	var adresseCorrespondance = renseignementsComp.adresseCorrespondance;
	pdfFields['adresseCorrespondance_voie1']               = getFieldValue(adresseCorrespondance.nomPrenomDenomination) + //
															' ' + getFieldValue(adresseCorrespondance.numeroVoie) + //
															' ' + getFieldValue(adresseCorrespondance.indiceVoie) + //
															' ' + getFieldValue(adresseCorrespondance.typeVoie) + //
															' ' + getFieldValue(adresseCorrespondance.nomVoie)
															;
	
	pdfFields['adresseCorrespondance_voie2']               = getFieldValue(adresseCorrespondance.complementVoie) + ' ' + getFieldValue(adresseCorrespondance.distributionSpecialeVoie);
	
	pdfFields['adresseCorrespondance_codePostal']          = adresseCorrespondance.codePostal;
	pdfFields['adresseCorrespondance_commune']             = adresseCorrespondance.commune;
} else if ( Value('id').of(renseignementsComp.adresseCorrespond).eq('prof') ) {
	pdfFields['adresseCorrespondance_voie1']               = adresseEtablissementPrincipalVoie;
	pdfFields['adresseCorrespondance_voie2']               = '';
	pdfFields['adresseCorrespondance_codePostal']          = adresseEtablissementPrincipal.codePostal;
	pdfFields['adresseCorrespondance_commune']             = adresseEtablissementPrincipal.commune;
}

pdfFields['formalite_observations']                    = getFieldValue(renseignementsComp.observations);
pdfFields['formalite_telephone1']                      = getFieldValue(renseignementsComp.infosSup.telephone1);
pdfFields['formalite_telephone2']                      = getFieldValue(renseignementsComp.infosSup.telephone2);

var faxCourriel = getFieldValue(renseignementsComp.infosSup.courriel);
pdfFields['formalite_fax_courriel']                    =  (null != faxCourriel && ''!= faxCourriel) ? faxCourriel : getFieldValue(renseignementsComp.infosSup.telecopie);

var signature = $m4.cadre9SignatureGroup.cadre9Signature;
['representantLegal', 'mandataire', 'autrePersonne'].forEach(function (key) {
	pdfFields['formalite_' + key]                     = Value('id').of(signature.soussigne).eq(key) ? true : false;
});
pdfFields['formalite_signatureDate']                   = parseDate(signature.date, '/');
pdfFields['formalite_signatureLieu']                   = signature.lieu;

var adresseMandataire = signature.adresseMandataire;
pdfFields['formalite_signataireNom']                = getFieldValue(adresseMandataire.nomPrenomDenomination);	
pdfFields['formalite_signataireAdresse_voie']       = getFieldValue(adresseMandataire.numeroVoie) + //
													' ' + getFieldValue(adresseMandataire.indiceVoie) + //
													' ' + getFieldValue(adresseMandataire.typeVoie) + //
													' ' + getFieldValue(adresseMandataire.nomVoie) + //
													' ' + getFieldValue(adresseMandataire.complementVoie) + //
													' ' + getFieldValue(adresseMandataire.distributionSpecialeVoie)
													;
pdfFields['formalite_signataireAdresse_commune']       = adresseMandataire.codePostal + ' ' + adresseMandataire.commune;

if ( Value('id').of($m4.cadre9SignatureGroup.cadre9Signature.soussigne).eq('representantLegal') ) {
    pushPjPreview($preprocess.preprocess.pjIDDeclarantSignataire);
} else {
    pushPjPreview($preprocess.preprocess.pjIDMandataireSignataire);
}

if ( Value('id').of($m4.cadre9SignatureGroup.cadre9Signature.soussigne).eq('mandataire') ) {
    pushPjPreview($preprocess.preprocess.pjPouvoir);
}

if ( Value('id').of($m4.cadre3InformationsGroup.cadre3Informations.motifCessation).eq('clotureLiquiation')
		or Value('id').of($m4.cadre3InformationsGroup.cadre3Informations.motifCessation).eq('autre')) {
	pushPjPreview($preprocess.preprocess.pjExemplaireClotureOperation);
    pushPjPreview($preprocess.preprocess.pjExemplaireCompteCloture);
    pushPjPreview($preprocess.preprocess.pjParution);
}

if ( Value('id').of($m4.cadre3InformationsGroup.cadre3Informations.motifCessation).eq('fusion')) {
    if (not $m4.cadre3InformationsGroup.cadre3Informations.fusionSimplifiee) {
	pushPjPreview($preprocess.preprocess.pjPVFusion);
    }
	pushPjPreview($preprocess.preprocess.pjTraiteFusion);
    pushPjPreview($preprocess.preprocess.pjParution);
    pushPjPreview($preprocess.preprocess.pjDeclarationConformite);
    pushPjPreview($preprocess.preprocess.pjKbis);
    }
	
if (Value('id').of($m4.cadre3InformationsGroup.cadre3Informations.motifCessation).eq('scission')) {
	pushPjPreview($preprocess.preprocess.pjPVScission);
    pushPjPreview($preprocess.preprocess.pjParution);
    pushPjPreview($preprocess.preprocess.pjDeclarationConformite);
    pushPjPreview($preprocess.preprocess.pjPVPouvoir);
    pushPjPreview($preprocess.preprocess.pjTraiteScission);
    }


//--------------- CERFA GENERATION -------------------//
//-->L'ordre des pages est le suivant : M4 avec / M4 sans / M' avec / M' sans
//-->Si au moins 2 établissements fermés ou bien 2 personnes affiliées alors ajout des cerfa dit "prime" avec et sans volet
var generateCerfaPrime = 
	( $m4.cadre2AdresseEtablissementGroup.cadreAdresseEtablissement.autreEtablissementFerme && $m4.cadre2AdresseEtablissementGroup.cadreAdresseEtablissement.autreEtablissement.size() > 2) ||
	( declarationSociale.personneAffilieeOuiNon && declarationSociale.personneAffiliee.size() > 2) ? true : false;
	
//-->Cerfa générés pour tous les cas
var cerfaAvecVolet = nash.doc.load('models/cerfa_11685-02_M4_avec_volet_social.pdf').apply(pdfFields);
var cerfaSansVolet = nash.doc.load('models/cerfa_11685-02_M4_sans_volet_social.pdf').apply(pdfFields);
cerfaAvecVolet.append(cerfaSansVolet.save('cerfaSansVolet.pdf'));

if (generateCerfaPrime) {
	log.debug('Génération des CERFA Prime : avec et sans volet social');
	pdfFields['formeJuridique_mPrime']                 = identite.entrepriseFormeJuridique;
	pdfFields['denomination_mPrime']                   = identite.entrepriseDenomination;

	cerfaAvecVolet.append(nash.doc.load('models/cerfa_11681-02_Mprime_avec_volet_social.pdf').apply(pdfFields).save('cerfaPrimeAvecVolet.pdf'));
	cerfaAvecVolet.append(nash.doc.load('models/cerfa_11681-02_Mprime_sans_volet_social.pdf').apply(pdfFields).save('cerfaPrimeSansVolet.pdf'));

}

var finalDoc = cerfaAvecVolet.save('Cessation_M4.pdf');

//--------------- METADATA DOCUMENT -------------------//
updateMetas(metas);

var data = [ spec.createData({
    id : 'record',
    label : "Déclaration de radiation d'une personne morale (hors activité principale agricole)",
    description : 'Voici le formulaire obtenu à partir des données saisies.',
    type : 'FileReadOnly',
    value : [ finalDoc ]
}),  spec.createData({
    id : 'attachments',
    label : 'Pièces jointes',
    description : 'Pièces jointes ajoutées au formulaire.',
    type : 'FileReadOnly',
    value : pjUser
}) ];


var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : "Déclaration de radiation d'une personne morale (hors activité principale agricole)",
    groups : groups
});
