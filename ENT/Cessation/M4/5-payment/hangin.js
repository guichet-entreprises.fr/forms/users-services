log.debug('Hanging payment pre-process');

//--------------- FUNCTIONS -------------------//
function updateMetas(proxyResult) {
	if (null == proxyResult || null == proxyResult.files) { return; }
	
	var metas = [];
	var proxyFiles = proxyResult.files;
	for(var i=0; i<proxyResult.files.size(); i++){
		if (!proxyResult.files.get(i).getLabel().endsWith(".pdf")) {
			proxyFiles.remove(i);
		} else {
			metas.push({'name':'document', 'value': '/'+proxyFiles.get(i).getAbsolutePath()});
		}
	}
	//-->Ajout du recu du paiement dans les méta
	if (metas.length > 0) {
		nash.record.meta(metas);
	}
	return proxyFiles;
}

function specCreateSuccessData(proxyResult) {
	return spec.create({
		id: 'review',
		label: "Règlement des frais liés au traitement de votre dossier",
		groups: [
			spec.createGroup({
				id: 'proxyResult',
									label: "Règlement des frais liés au traitement de votre dossier",
									description: "Votre paiement a bien été pris en compte. <strong>Attention, votre dossier n'est pas encore finalisé et n’a pas été transmis au(x) service(s) compétent(s). Pour envoyer votre dossier au(x) service(s) compétent(s), veuillez passer à l’étape suivante en cliquant sur le bouton « Etape suivante ».</strong>",
				data: [
					spec.createData({
						id: 'files',
						description:'Veuillez trouver ci-dessous le récapitulatif de votre paiement.',
						type: 'FileReadOnly',
						value:  updateMetas(proxyResult)
					})
				]
			})
		]
	});
}

function specCreateReplayData() {
	return spec.create({
		id: 'review',
		label: "Règlement des frais liés au traitement de votre dossier",
		groups: [spec.createGroup({
			id: 'proxyResult',
			label: "Récapitulatif de votre paiement",
			description: "Pour effectuer une nouvelle tentative de paiement ci-dessous, veuillez cliquer sur le bouton « Etape suivante » ci-dessous.",
			data: [	]
		})]
	});
}

function specCreateCriticalData(proxyResult) {		
	return spec.create({
		id: 'review',
		label: "Règlement des frais liés au traitement de votre dossier",
		groups: [spec.createGroup({
			id: 'proxyResult',
			label: "Récapitulatif de votre paiement",
			data: [
				spec.createData({
					id: 'files',
					label : 'Résultat de la tentative de paiement',
					description : "Une erreur est survenue lors du règlement des frais associés au dossier. Votre dossier a été mis en attente. Vous allez être redirigé vers votre tableau de bord. Nos équipes sont mobilisées pour vous permettre de régler vos frais et de finaliser votre dossier dans les meilleurs délais. Vous avez également la possibilité de consulter et de télécharger un justificatif d'annulation de paiement depuis votre tableau de bord.",
					type: 'FileReadOnly',
					value:  ( (null != proxyResult && null != proxyResult.files) ? updateMetas(proxyResult) : null )
				})
			]
		})]
	});
}

function specCreateBackToDashboardData(proxyResult) {	
	return spec.create({
		id: 'review',
		label: "Règlement des frais liés au traitement de votre dossier",
		groups: [spec.createGroup({
			id: 'proxyResult',
			label: "Récapitulatif de votre paiement",
			data: [
				spec.createData({
					id: 'files',
					label : 'Résultat de la tentative de paiement',
					description : "Une erreur est survenue lors du règlement des frais associés au dossier. Vous allez être redirigé vers votre tableau de bord.",
					type: 'FileReadOnly',
					value:  ( (null != proxyResult && null != proxyResult.files) ? updateMetas(proxyResult) : null )
				})
			]
		})]
	});
}
//--------------- FUNCTIONS -------------------//


//--------------- Variables init --------------//
var proxyResult = _INPUT_.proxyResult;
//--------------- Variables init --------------//
log.debug('Proxy result response : {}', proxyResult);

if (proxyResult.status == 'SUCCESS') {
	return specCreateSuccessData(proxyResult);
	
} else if (proxyResult.status == 'TECHNICAL' || proxyResult.status == 'FRAUD') {
	return specCreateReplayData();
	
} else if (proxyResult.status == 'CRITICAL') {
	return specCreateCriticalData(proxyResult);
	
} else {
	return specCreateBackToDashboardData(proxyResult);
}
