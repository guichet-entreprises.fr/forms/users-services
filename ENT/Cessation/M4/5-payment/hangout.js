//--------------- FUNCTIONS -------------------//
function getUser(author) {			
	var response = null;
	try {		
		response = nash.service.request('${account.ge.baseUrl.read}/private/users/{id}', nash.record.description().author) //
		   .connectionTimeout(10000) //
		   .receiveTimeout(10000) //
		   .accept('text') //
		   .get()
		;
	} catch (e) {	
		log.error(e);			
		throw 'An technical error occured when calling account with author : ' + author;
	}
	if (response != null && response.getStatus() == 200) {
		return response.asObject();
	}
	throw 'Cannot find any author : ' + author;
}
//--------------- FUNCTIONS -------------------//

//--------------- Variables init --------------//
var recordUid = nash.record.description().recordUid; 
var items = [];
var authorities = _input.context.payment.authority;
//--------------- Variables init --------------//

var user = getUser(nash.record.description().author);

for (var i = 0; i < authorities.size(); i++) {
	var authority = authorities.get(i);
	var item = {  
		'partnerLabel' : authority.label,
		'ediCode' : authority.ediCode,
		'name' : authority.rate.code,
		'description' : authority.rate.label,
		'price': authority.rate.price
	};

	items.push(item);
}

//--------------- Building the cart --------------//
var cart = {
	'recordUid' : recordUid,  
	'dossierId' : recordUid,
	'email' : user.email,
	'userCivility' : user.civility,
	'userLastName' : user.lastName,
	'userFirstName' : user.firstName,
	'description' : nash.record.description().title,
	'paymentType' :'DB',
	'items' : items,	                    
	'reference' : recordUid
};

log.info('The cart built is : {}', cart);
		
//--------------- Hangout to the proxy --------------//
return nash.hangout.pay({'cart' : JSON.stringify(cart)});
