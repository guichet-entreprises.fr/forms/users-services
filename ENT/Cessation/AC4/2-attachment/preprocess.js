// PJ Déclarant

var userDeclarant;
if ($ac4PP.cadre1Identite.cadre1RappelIdentification.personneLieePersonnePhysiqueNomUsage != null) {
    var userDeclarant = $ac4PP.cadre1Identite.cadre1RappelIdentification.personneLieePersonnePhysiqueNomUsage + '  ' + $ac4PP.cadre1Identite.cadre1RappelIdentification.personneLieePersonnePhysiquePrenom1[0] ;
} else { 
    var userDeclarant = $ac4PP.cadre1Identite.cadre1RappelIdentification.personneLieePersonnePhysiqueNomNaissance + ' '+ $ac4PP.cadre1Identite.cadre1RappelIdentification.personneLieePersonnePhysiquePrenom1[0] ;
}

var pj=$ac4PP.cadre9SignatureGroup.cadre9Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteDeclarant')) {
    attachment('pjIDDeclarantSignataire', 'pjIDDeclarantSignataire', { label: userDeclarant, mandatory:"true"});
}

// PJ Mandataire

var pj=$ac4PP.cadre9SignatureGroup.cadre9Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
    attachment('pjPouvoir', 'pjPouvoir', { mandatory:"true"});
}

var userMandataire=$ac4PP.cadre9SignatureGroup.cadre9Signature.adresseMandataire

var pj=$ac4PP.cadre9SignatureGroup.cadre9Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire') or Value('id').of(pj).contains('FormaliteSignataireQualiteAutre')) {
    attachment('pjIDMandataireSignataire', 'pjIDMandataireSignataire', {label: userMandataire.nomPrenomDenominationMandataire, mandatory:"true"});
}