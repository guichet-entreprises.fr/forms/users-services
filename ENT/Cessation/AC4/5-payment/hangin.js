var recordUid = nash.record.description().recordUid;
var loader = nash.record;
_log.info("************INPUT : {}************", _INPUT_);
	
_log.info("************Fromalité payante!************");	
// check inout information
var proxyResult = _INPUT_.proxyResult;
_log.info("proxyResult is {}", proxyResult);
_log.info("proxyResult status  is {}", proxyResult.status);	
	
var metas = [];

var nameToCheck = recordUid+"_recu.pdf";
_log.info("nameToCheck is {}", nameToCheck);
var fileExists = false ;
if (null != proxyResult.files) {
	for(var i=0; i<proxyResult.files.length; i++){
	var indice = i+1;
	_log.info("label is {}", proxyResult.files[i].label);
	_log.info("proxyResult {} element is {}", i, proxyResult.files[i]);
		if(proxyResult.files[i].label.indexOf(nameToCheck)!=-1){
			var	fileName = "proxyResult.files-"+indice+"-"+proxyResult.files[i].label;
			loader.removeMeta([{'name':'document', 'value': '/'+proxyResult.files[i].getAbsolutePath()}]);
			metas.push({'name':'document', 'value': '/'+proxyResult.files[i].getAbsolutePath()});
			fileExists = true ;
		}
	}
	//-->Ajout du recu du paiement dans les méta
	if (metas.length > 0) {
		nash.record.meta(metas);
	}
	_log.info("file name is {}", fileName);
}
			
if (proxyResult.status == 'SUCCESS') {
	
	if (fileExists){

		var proxyFiles = proxyResult.files;
		_log.info("proxyResult.files {} ",proxyResult.files);
		_log.info("proxyResult.files.size() {} ",proxyResult.files.size());
		for(var i=0; i<proxyResult.files.size(); i++){
			_log.info("element : {} ",proxyResult.files.get(i));
			_log.info("inside for: if end with pdf {} ",proxyResult.files.get(i).getLabel().endsWith(".pdf"));
			if (!proxyResult.files.get(i).getLabel().endsWith(".pdf")) {
				proxyFiles.remove(i);
			}
		}
		
		return spec.create({
							id: 'review',
							label: "Règlement des frais liés au traitement de votre dossier",
							groups: [spec.createGroup({
									id: 'proxyResult',
									label: "Règlement des frais liés au traitement de votre dossier",
									description: "Votre paiement a bien été pris en compte. <strong>Attention, votre dossier n'est pas encore finalisé et n’a pas été transmis au(x) service(s) compétent(s). Pour envoyer votre dossier au(x) service(s) compétent(s), veuillez passer à l’étape suivante en cliquant sur le bouton « Etape suivante ».</strong>",
									data: [
										spec.createData({
											id: 'files',
											description:'Le justificatif de votre paiement ci-dessous est téléchargeable.',
											type: 'FileReadOnly',
											value:  proxyFiles
										})
									]
								})]
						});	
					
	}else{
		return spec.create({
						id: 'review',
						label: "Règlement des frais liés au traitement de votre dossier",
						groups: [spec.createGroup({
								id: 'generatedFile',
								label: "Règlement des frais liés au traitement de votre dossier",
							description: "Votre paiement a bien été pris en compte. <strong>Attention, votre dossier n'est pas encore finalisé et n’a pas été transmis au(x) service(s) compétent(s). Pour envoyer votre dossier au(x) service(s) compétent(s), veuillez passer à l’étape suivante en cliquant sur le bouton « Etape suivante ».</strong>",
								data: [	]
							})]
					});	
	}
	
} else if (proxyResult.status == 'TECHNICAL' || proxyResult.status == 'FRAUD') {
	return spec.create({
		id: 'review',
		label: "Règlement des frais liés au traitement de votre dossier",
		groups: [spec.createGroup({
				id: 'generatedFile',
				label: "Récapitulatif de votre paiement",
				description: "Pour effectuer une nouvelle tentative de paiement ci-dessous, veuillez cliquer sur le bouton « Etape suivante » ci-dessous.",
				data: [	]
			})]
	});
} else if (proxyResult.status == 'CRITICAL') {
	
	if (fileExists) {

		var metas = [];
		var proxyFiles = proxyResult.files;
		for(var i=0; i<proxyResult.files.size(); i++){
			if (!proxyResult.files.get(i).getLabel().endsWith(".pdf")) {
				proxyFiles.remove(i);
			} else {
				loader.removeMeta([{'name':'document', 'value': '/'+proxyFiles.get(i).getAbsolutePath()}]);
				metas.push({'name':'document', 'value': '/'+proxyFiles.get(i).getAbsolutePath()});
			}
		}
		//-->Ajout du recu du paiement dans les méta
		if (metas.length > 0) {
			nash.record.meta(metas);
		}
		
		return spec.create({
							id: 'review',
							label: "Règlement des frais liés au traitement de votre dossier",
							groups: [spec.createGroup({
									id: 'proxyResult',
									label: "Règlement des frais liés au traitement de votre dossier",
									description: "Votre dossier requiert une intervention de l'équipe Guichet Entreprises. Une alerte est transmise à notre équipe d'intervention. Votre dossier est mis en attente pour le moment. Vous recevrez prochainement un courriel vous informant de la résolution du problème.",
									data: [
										spec.createData({
											id: 'files',
											label : 'Échec de la tentative de paiement',
											description : "Le justificatif de votre paiement ci-dessous est téléchargeable.",
											type: 'FileReadOnly',
											value:  proxyFiles
										})
									]
								})]
						});	
					
	} else {
		return spec.create({
						id: 'paymentResult',
						label: "Règlement des frais liés au traitement de votre dossier",
						groups: [spec.createGroup({
								id : 'proxyResult',
								label: "Règlement des frais liés au traitement de votre dossier",
								description: "Votre dossier requiert une intervention de l'équipe Guichet Entreprises. Une alerte est transmise à notre équipe d'intervention. Votre dossier est mis en attente pour le moment. Vous recevrez prochainement un courriel vous informant de la résolution du problème.",
								data : []
							})]
					});	
	}
} else {
	
	if (fileExists) {

		var metas = [];
		var proxyFiles = proxyResult.files;
		for(var i=0; i<proxyResult.files.size(); i++){
			if (!proxyResult.files.get(i).getLabel().endsWith(".pdf")) {
				proxyFiles.remove(i);
			} else {
				loader.removeMeta([{'name':'document', 'value': '/'+proxyFiles.get(i).getAbsolutePath()}]);
				metas.push({'name':'document', 'value': '/'+proxyFiles.get(i).getAbsolutePath()});
			}
		}
		//-->Ajout du recu du paiement dans les méta
		if (metas.length > 0) {
			nash.record.meta(metas);
		}
		
		return spec.create({
							id: 'review',
							label: "Règlement des frais liés au traitement de votre dossier",
							groups: [spec.createGroup({
									id: 'proxyResult',
									label: "Règlement des frais liés au traitement de votre dossier",
									description: "Votre dossier requiert une intervention de l'équipe Guichet Entreprises. Une alerte est transmise à notre équipe d'intervention. Votre dossier est mis en attente pour le moment. Vous recevrez prochainement un courriel vous informant de la résolution du problème.",
									data: [
										spec.createData({
											id: 'files',
											label : 'Échec de la tentative de paiement',
											description : "Le justificatif de votre paiement ci-dessous est téléchargeable.",
											type: 'FileReadOnly',
											value:  proxyFiles
										})
									]
								})]
						});	
					
	} else {
		return spec.create({
						id: 'paymentResult',
						label: "Règlement des frais liés au traitement de votre dossier",
						groups: [spec.createGroup({
								id : 'proxyResult',
								label: "Règlement des frais liés au traitement de votre dossier",
								description : "Votre dossier requiert une intervention de l'équipe Guichet Entreprises. Une alerte est transmise à notre équipe d'intervention. Votre dossier est mis en attente pour le moment. Vous recevrez prochainement un courriel vous informant de la résolution du problème.",
								data : []
							})]
					});	
	}
}
