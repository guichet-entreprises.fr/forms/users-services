<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns="http://www.ge.fr/schema/1.2/form-manager" xmlns:ref="http://www.ge.fr/schema/1.2/form-manager" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.ge.fr/schema/1.2/form-manager http://svn.projet-ge.fr:8080/archiva/repository/xsd/form-manager/form-manager/1.2/form-manager-1.2.xsd">

	<xsl:output method="xml" indent="yes" />

	<xsl:template match="/formalityPayment">
		<form id="payment" label="Paiement">
			<default>
				<data mandatory="true" type="StringReadOnly" />
			</default>
			<group id="infoGenGroup" label="Informations générales">
				<group id="infoGen" label="Informations générales de la transaction">
					<data id="recordUid" label="Identiant du dossier :" type="StringReadOnly">
						<value>
							<xsl:value-of select="recordUid" />
						</value>
					</data>
					<data id="transactionId" label="Identiant de la transaction :" type="StringReadOnly">
						<value>
							<xsl:value-of select="transactionId" />
						</value>
					</data>
					<data id="transactionDate" label="Date de la transaction :" type="StringReadOnly">
						<value>
							<xsl:value-of select="transactionDate" />
						</value>
					</data>
					<data id="totalAmount" label="Montant total de la transaction :" type="StringReadOnly">
						<value>
							<xsl:value-of select="totalAmount" />
						</value>
					</data>
				</group>
			
				<group id="infoPartners" label="Informations des paiements par partenaire">
					<xsl:for-each select="partnerPayment">						
						<group>
							<xsl:attribute name="id">partnerPayment[<xsl:value-of select="position()-1" />]</xsl:attribute>
							<xsl:attribute name="label">Informations complémentaires du partenaire</xsl:attribute>
							<xsl:attribute name="min-occurs">1</xsl:attribute>
							<data id="partnerId" label="Identifiant du partenaire:" type="StringReadOnly">
								<value>
									<xsl:value-of select="partnerId" />
								</value>
							</data>
							<data id="description" label="Description:" type="StringReadOnly">
								<value>
									<xsl:value-of select="description" />
								</value>
							</data>
							<data id="paymentAmount" label="Sous-paiement :" type="StringReadOnly">
								<value>
									<xsl:value-of select="paymentAmount" />
								</value>
							</data>
						</group>
					</xsl:for-each>
				</group>
			</group>
		</form>
	</xsl:template>
</xsl:stylesheet>