//Fichier de mapping des champs Regent pour :
//Version : V2008.11 
//Evenements : 41P,  
//Fichier généré le : 2018-12-12_132850 

function pad(s) { return (s < 10) ? '0' + s : s; }

var regentVersion = "V2008.11";  // (V2008.11, V2016.02)
var eventRegent = "41P"; 
var authorityType = "CFE"; // (CFE, TDR)
var authorityId = _INPUT_.dataGroup.codeEDI; // (code EDI)

//Ajout du type de la deuxième autorité
//var authorityType2 = "TDR"; // (CFE, TDR) 
//Ajout du code de la deuxième autorité
//var authorityId2 = _INPUT_.dataGroup.codeEDI2; // (code EDI) 

 
var regEx = new RegExp('[0-9]{5}'); 
var identite = $ac4PP.cadre1Identite.cadre1RappelIdentification;
var infoCessation = $ac4PP.cadre3InformationsGroup.cadre3Informations;
var etablissement = $ac4PP.cadre2AdresseEtablissementGroup.cadreAdresseEtablissement ;
var adressePro = $ac4PP.cadre2AdresseEtablissementGroup.cadreAdresseEtablissement.adresseEtablissementPrincipal ;
var correspondance = $ac4PP.cadre8RensCompGroup.cadre8RensComp;
var signataire = $ac4PP.cadre9SignatureGroup.cadre9Signature;
var rappelIdentification = $ac4PP.cadre3InformationsGroup.cadre3Informations.cadreAffectationPatrimoine.cadreRappelIdentificationEIRL;
var finDAP = $ac4PP.cadre3InformationsGroup.cadre3Informations.cadreAffectationPatrimoine.finEIRLDeclaration;



var regentFields = {}; 
regentFields['/REGENT-XML/Emetteur']= "Z1611";

/*******************************************************************************
 * À récuperer depuis directory
 ******************************************************************************/
regentFields['/REGENT-XML/Destinataire']= authorityId;


/*******************************************************************************
 * Complété par destiny
 ******************************************************************************/
regentFields['/REGENT-XML/DateHeureEmission']=null;
regentFields['/REGENT-XML/VersionMessage']=null;
regentFields['/REGENT-XML/VersionNorme']=null;
regentFields['/REGENT-XML/ServiceApplicatif/Specification/NomService']=null;
regentFields['/REGENT-XML/ServiceApplicatif/Specification/VersionService']=null;


/*******************************************************************************
 * Groupe GDF : Groupe données de service Sous groupe IDF : Identification de la
 * formalité
 ******************************************************************************/
// Généré par destiny
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C01']=null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02']=null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C03']= "0";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C04']=null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05']= "R";


regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.1']= "41P";

if(infoCessation.dateCessation !== null) {
    var dateTemp = new Date(parseInt(infoCessation.dateCessation.getTimeInMillis()));
    var year = dateTemp.getFullYear();
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
    var date = year + "-" + month + "-" + day ;
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2']= date;
}

/*******************************************************************************
 * Sous groupe DMF : Destinataire de la formalité
 ******************************************************************************/
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[1]']= authorityId;
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[2]']= authorityId2;


/*******************************************************************************
 * Sous groupe ADF : Adresse de correspondance
 ******************************************************************************/


regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C36']= correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance != null ? correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance : (identite.personneLieePersonnePhysiqueNomUsage != null ? (identite.personneLieePersonnePhysiqueNomUsage + ' ' + identite.personneLieePersonnePhysiquePrenom1[0]) : (identite.personneLieePersonnePhysiqueNomNaissance + ' ' + identite.personneLieePersonnePhysiquePrenom1[0]));

if (Value('id').of(correspondance.adresseCorrespond).eq('prof')) {
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']=adressePro.etablissementAdresseCommune.getId();
}
else if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']=correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune.getId();
}
																	
if ((Value('id').of(correspondance.adresseCorrespond).eq('prof') and adressePro.etablissementAdresseNumeroVoie != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.numeroRueAdresseCorrespondance != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.5']= Value('id').of(correspondance.adresseCorrespond).eq('prof') ? adressePro.etablissementAdresseNumeroVoie : 
																				correspondance.adresseCorrespondance.numeroRueAdresseCorrespondance;
	}
	
if (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adressePro.etablissementAdresseIndiceVoie !== null) {
   var monId1 = Value('id').of(adressePro.etablissementAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId1;
}  
else if (correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance !== null) {
   var monId2 = Value('id').of(correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId2;
}

if ((Value('id').of(correspondance.adresseCorrespond).eq('prof') and adressePro.etablissementAdresseDistriutionSpecialeVoie != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.distriutionSpecialeVoieAdresseCorrespondance != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.7']= Value('id').of(correspondance.adresseCorrespond).eq('prof') ? adressePro.etablissementAdresseDistriutionSpecialeVoie : 
																				correspondance.adresseCorrespondance.distriutionSpecialeVoieAdresseCorrespondance;
}

if ((Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adressePro.etablissementAdresseCodePostal != null)) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8']= Value('id').of(correspondance.adresseCorrespond).eq('prof') ? adressePro.etablissementAdresseCodePostal : 
																			correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal;
}

/*
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.9']= null;
*/

if ((Value('id').of(correspondance.adresseCorrespond).eq('prof') and adressePro.etablissementAdresseComplementVoie != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.rueComplementAdresseCorrespondance != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.10']= Value('id').of(correspondance.adresseCorrespond).eq('prof') ? adressePro.etablissementAdresseComplementVoie : 
																				correspondance.adresseCorrespondance.rueComplementAdresseCorrespondance;
}

if (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adressePro.etablissementAdresseTypeVoie !== null) {
   var monId1 = Value('id').of(adressePro.etablissementAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId1;
}  
else if (correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance !== null) {
   var monId2 = Value('id').of(correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId2;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12']= Value('id').of(correspondance.adresseCorrespond).eq('prof') ? adressePro.etablissementAdresseNomVoie : 
																			correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance;
																			
if (Value('id').of(correspondance.adresseCorrespond).eq('prof')) {
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= adressePro.etablissementAdresseCommune.getLabel();
}
else if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune.getLabel();
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.1[1]']= correspondance.infosSup.formaliteTelephone2.e164.replace('+', '00');

if (correspondance.infosSup.formaliteTelephone1 != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.1[2]']= correspondance.infosSup.formaliteTelephone1.e164.replace('+', '00');
}

if (correspondance.infosSup.telecopie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.2']= correspondance.infosSup.telecopie.e164.replace('+', '00');
}

if (correspondance.infosSup.formaliteCourriel != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.3']= correspondance.infosSup.formaliteCourriel;
}


/*******************************************************************************
 * Signature de la Formalité
 ******************************************************************************/

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.1']= (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') or Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteAutre')) ? signataire.adresseMandataire.nomPrenomDenominationMandataire : (identite.personneLieePersonnePhysiqueNomUsage != null ? (identite.personneLieePersonnePhysiqueNomUsage + ' ' + identite.personneLieePersonnePhysiquePrenom1[0]) : (identite.personneLieePersonnePhysiqueNomNaissance + ' ' + identite.personneLieePersonnePhysiquePrenom1[0]));

if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') or Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteAutre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.2']= Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteAutre') ? "Autre personne" : "Mandataire";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.3']= signataire.adresseMandataire.villeAdresseMandataire.getId();

if (signataire.adresseMandataire.numeroVoieMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.5']= signataire.adresseMandataire.numeroVoieMandataire;
}

if (signataire.adresseMandataire.indiceVoieMandataire !== null) {
   var monId = Value('id').of(signataire.adresseMandataire.indiceVoieMandataire)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.6']          = monId;
}

if (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.7']= signataire.adresseMandataire.voieDistributionSpecialeMandataire;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.8']= signataire.adresseMandataire.codePostalMandataire;

if (signataire.adresseMandataire.voieComplementMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.10']= signataire.adresseMandataire.voieComplementMandataire;
}

if (signataire.adresseMandataire.typeVoieMandataire !== null) {
   var monId = Value('id').of(signataire.adresseMandataire.typeVoieMandataire)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.11']          = monId;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.12']= signataire.adresseMandataire.nomVoieMandataire;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.13']= signataire.adresseMandataire.villeAdresseMandataire.getLabel();
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C41']= signataire.formaliteSignatureLieu;

if(signataire.formaliteSignatureDate !== null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C42']= date;
}

if (correspondance.formaliteObservations != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C43']= correspondance.formaliteObservations;
}


/*******************************************************************************
 * Identification Complète de la personne Physique
 ******************************************************************************/
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.1']= Value('id').of(identite.civilite).eq('PersonneLieePersonnePhysiqueCiviliteFeminin') ? "2" : "1";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.2']= identite.personneLieePersonnePhysiqueNomNaissance;

var prenoms=[];
for (i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){
	prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);
}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.3']= prenoms;

if(identite.personneLieePersonnePhysiqueNomUsage != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P02/P02.1']= identite.personneLieePersonnePhysiqueNomUsage;
}

if(identite.personneLieePersonnePhysiqueDateNaissance !== null) {
    var dateTemp = new Date(parseInt(identite.personneLieePersonnePhysiqueDateNaissance.getTimeInMillis()));
    var year = dateTemp.getFullYear();
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
    var date = year + "-" + month + "-" + day ;
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.1']= date;
}

if (not Value('id').of(identite.personneLieePersonnePhysiqueLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = identite.personneLieePersonnePhysiqueLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.2']= result;
} else if (Value('id').of(identite.personneLieePersonnePhysiqueLieuNaissancePays).eq('FRXXXXX')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.2'] = identite.personneLieePersonnePhysiqueLieuNaissanceCommune.getId();
}																						 
																					 
if (not Value('id').of(identite.personneLieePersonnePhysiqueLieuNaissancePays).eq('FRXXXXX')) {																					 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.3'] = identite.personneLieePersonnePhysiqueLieuNaissancePays.getLabel();
}

if (Value('id').of(identite.personneLieePersonnePhysiqueLieuNaissancePays).eq('FRXXXXX')) {	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.4']= identite.personneLieePersonnePhysiqueLieuNaissanceCommune.getLabel();
}


/*******************************************************************************
 * Domicile personnel ou commune de rattachement pour les forains
 ******************************************************************************/
 
// non concerné pour une radiation
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04']=null;
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P05']=null;


/*******************************************************************************
 * Déclaration d'affectation du patrimoine (si option pour l'EIRL)
 ******************************************************************************/

if ($ac4PP.cadre3InformationsGroup.cadre3Informations.estEIRLOuiNon) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P70']= "F";

if(infoCessation.dateCessation !== null) {
var dateTemp = new Date(parseInt(infoCessation.dateCessation.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P77']= date;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P79']= Value('id').of(finDAP.motifFinEIRL).eq('FinEIRLRenonciationSansPoursuite') ? "2" :
																			   (Value('id').of(finDAP.motifFinEIRL).eq('FinEIRLCessionPP') ? "3" : "4");
}

/*******************************************************************************
 * Cessation totale d'activité de la personne physique
 ******************************************************************************/

if(infoCessation.dateCessationEmploiSalarie !== null) {
var dateTemp = new Date(parseInt(infoCessation.dateCessationEmploiSalarie.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/CAP/P63']= date;
}

/*******************************************************************************
 * Justification pour le RCS des déclarations relatives à la personne Physique
 ******************************************************************************/
// regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/JGP/J00']=null;


/*******************************************************************************
 * Immatriculation Sécurité Sociale du travailleur non salarié
 ******************************************************************************/

//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A10/A10.1'] = ".";
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A10/A10.2'] = nirDeclarant.substring(13, 15);


/*******************************************************************************
 * Choix de l’organisme d’Assurance maladie du TNS - MSA
 ******************************************************************************/

//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/CAS/A42/A42.1']= ".";
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/CAS/A42/A42.2']= ".";


/*******************************************************************************
 * Immatriculations principales de l'entreprise 
 ******************************************************************************/

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U02']= identite.siren.split(' ').join('');
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U06']= infoCessation.presenceSalaries ? "O" : "N";


/*******************************************************************************
 * Caractéristiques de l’Entreprise
 ******************************************************************************/

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U27'] = infoCessation.presenceSalaries ? "O" : "N";

/*******************************************************************************
 * Lieu d'imposition de l'entreprise
 ******************************************************************************/

//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/LIU/U36/U36.2']= identite.sieLieu;
		

/*******************************************************************************
 * Groupe GFE Groupe établissement fermé
 ******************************************************************************/

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E12/E12.3'] = adressePro.etablissementAdresseCommune.getId();
																	
if (adressePro.etablissementAdresseNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E12/E12.5'] = adressePro.etablissementAdresseNumeroVoie;
}
	
if (adressePro.etablissementAdresseIndiceVoie !== null) {
   var monId1 = Value('id').of(adressePro.etablissementAdresseIndiceVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E12/E12.6']          = monId1;
}  

if (adressePro.etablissementAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E12/E12.7']= adressePro.etablissementAdresseDistriutionSpecialeVoie;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E12/E12.8']= adressePro.etablissementAdresseCodePostal;

if (adressePro.etablissementAdresseComplementVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E12/E12.10']= adressePro.etablissementAdresseComplementVoie ;
}

if (adressePro.etablissementAdresseTypeVoie !== null) {
   var monId1 = Value('id').of(adressePro.etablissementAdresseTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E12/E12.11']          = monId1;
} 

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E12/E12.12']= adressePro.etablissementAdresseNomVoie;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E12/E12.13']= adressePro.etablissementAdresseCommune.getLabel();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E13/E13.1'] = "000000000";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E13/E13.2'] = "00000";

if(infoCessation.dateCessationEmploiSalarie !== null) {
var dateTemp = new Date(parseInt(infoCessation.dateCessationEmploiSalarie.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E14']= date;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E17']= "3";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/DEE/E41/E41.1']= "C";


 
//-->DEST-689 : Afficher les erreurs XSD dans la formalité quand il y'a une erreur de génération

// Call to the XML-REGENT generation WS 
var response = nash.service.request('${regent.baseUrl}/private/v1/xml-regent/generate/{regentVersion}/{authorityType}/{authorityId}', regentVersion, authorityType, authorityId) 
.dataType('application/json') // 
.accept('json') // 
.param('listTypeEvenement',eventRegent) 
//-->MINE-263 : Permettre à l'équipe FF de voir les problèmes de validation XSD et le regent sur studio
.continueOnError(true) //
.post(JSON.stringify(regentFields)); 

// Record the generated XML-REGENT 
//MOD déplacement du test de la réponse du premier ws
if (response != null && response.status == 200) { 
    var xmlRegentStr = response.asBytes(); 
    nash.record.saveFile("XML_REGENT.xml",xmlRegentStr); 

	

	return spec.create({
	id : 'xmlGenerationConfirmation',
	label : "Xml Regent confirmation message",
	groups : [ spec.createGroup({
		id : 'confirmationMessageOk',
		description : "Le fichier XML Regent a été généré et ajouté au dossier.",
		data : []
		}) ]
	});
	
}else{
	
	var returnedResponse = response.asObject();
	_log.info("Call ws regent returned errors  {}", returnedResponse);
	
	
	var regent = returnedResponse['regent'];
	var errors = returnedResponse['errors']; 
	nash.record.saveFile("XML_REGENT.xml",regent.getBytes()); 
	nash.record.saveFile("XML_VALIDATION_ERRORS.xml",errors.getBytes()); 
	
	
	return spec.create({
	id : 'xmlGenerationConfirmation',
	label : "Xml Regent confirmation message",
	groups : [ spec.createGroup({
		id : 'Regent ',
		description : "Une erreur s'est produite lors de la génération du XML Regent de la première autorité.",
		data : [
				spec.createData({
					id: 'regent',
					label: "XML regent généré",
					type: 'Text',
					mandatory: true,
					value: regent
				}),spec.createData({
					id: 'errors',
					label: "Erreurs de validations xsd",
					type: 'Text',
					mandatory: false,
					value: errors
				}),spec.createData({
					id: 'blockingField',
					label: "xsd erroné",
					help:"Ce champs sert à arrêter le process du dossier pour que le support puisse le consulter",
					type: 'StringReadOnly',
					mandatory: true
			})]
		}) ]
	});
}