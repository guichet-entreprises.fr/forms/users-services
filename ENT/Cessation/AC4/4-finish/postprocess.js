// prepare info to send

var adressePro = $ac4PP.cadre2AdresseEtablissementGroup.cadreAdresseEtablissement.adresseEtablissementPrincipal ;

var algo = "trouver Cfe";

var secteur1 = "Liberal";

var typePersonne = "PM";

var formJuridique = $ac4PP.cadre3InformationsGroup.cadre3Informations.estEIRLOuiNon ? "EIRL" : "EI";

var optionCMACCI = "NON";


var codeCommune = '';
_log.info("commune adresse pro {} ",adressePro.etablissementAdresseCommune);

if (adressePro.etablissementAdresseCommune != null){
	_log.info("adresse activite entreprise");
	codeCommune = adressePro.etablissementAdresseCommune.getId();
}

var attachement = "/4-signature/generated/proxyResult.files-1-PROXY_SIGNED_DOCUMENT.pdf";

return spec.create({
	id : 'prepareSend',
	label : "Préparation de la recherche du destinataire",
	groups : [ spec.createGroup({
		id : 'view',
		label : "Informations",
		data : [ spec.createData({
			id : 'algo',
			label : "Algo",
			type : 'String',
			mandatory : true,
			value : algo
		}), spec.createData({
			id : 'secteur1',
			label : "Secteur",
			type : 'String',
			value : secteur1
		}), spec.createData({
			id : 'typePersonne',
			label : "Type personne",
			type : 'String',
			value : typePersonne
		}), spec.createData({
			id : 'formJuridique',
			label : "Forme juridique",
			type : 'String',
			value : formJuridique
		}), spec.createData({
			id : 'optionCMACCI',
			label : "Option CMACCI",
			type : 'String',
			value : optionCMACCI
		}), spec.createData({
			id : 'codeCommune',
			label : "Code commune",
			type : 'String',
			value : codeCommune
		}), spec.createData({
			id : 'attachement',
			label : "Pièce jointe",
			type : 'String',
			value : attachement
		}) ]
	}) ]
});