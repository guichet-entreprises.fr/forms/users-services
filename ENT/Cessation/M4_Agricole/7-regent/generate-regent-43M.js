//--------------- FUNCTIONS -------------------//
function pad(s) { return (s < 10) ? '0' + s : s; }

function parseDate(dateField, separator) {
	if (null == dateField) {
		return '';
	}
	
	if (null == separator) {
		separator = '';
	}
	
    var dateTmp = new Date(parseInt(dateField.getTimeInMillis()));	
	var date = dateTmp.getFullYear().toString();
	date = date.concat(separator);
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(separator);
    date = date.concat(pad(dateTmp.getDate().toString()));
	
    return date;
}

function getFieldValue(field) { return null == field ? '' : field; }

function buildRegent(eventRegent, regentFields, regentVersion, authorityType, authorityId, liasse) {
	var response = null;
	try {		
		response = nash.service.request('${regent.baseUrl}/private/v1/xml-regent/generate/{regentVersion}/{authorityType}/{authorityId}', regentVersion, authorityType, authorityId) //
			.connectionTimeout(10000) //
			.receiveTimeout(10000) //
			.accept('json') //
			.dataType('application/json') // 
			.continueOnError(true) //
			.param('listTypeEvenement', eventRegent) //
			.param('liasseNumber', liasse) //
			.post(JSON.stringify(regentFields)); 
		;
	} catch (e) {	
		log.error(e);			
		throw 'An technical error occured when generating XML Regent for authority : ' + authorityId;
	}
	
	var xmlRegentFileName = 'XML_REGENT_' + authorityId + '.xml';
	var xmlValidationErrorsFileName = 'XML_VALIDATION_ERRORS_' + authorityId + '.xml';
	
	if (response != null && response.getStatus() == 200) {
		nash.record.saveFile(xmlRegentFileName, response.asBytes());
		return '/' + xmlRegentFileName;
	} else {
		var returnedResponse = response.asObject();
		nash.record.saveFile(xmlRegentFileName, returnedResponse['regent'].getBytes());
		nash.record.saveFile(xmlValidationErrorsFileName, returnedResponse['errors'].getBytes());
		
		log.error('XML Regent generation failed : files are generated.');
	}
	throw 'Unsuccessful XML Regent generation for authority : ' + authorityId;
}
//--------------- FUNCTIONS -------------------//

//--------------- Variables init --------------//
var regentVersion = _input.event.version;  // (V2008.11, V2016.02)
var regEx = new RegExp('[0-9]{5}');
var eventRegent = _input.event;
var authorityRegentFields = [];

//--------------- Variables init --------------//

//--------------- Main --------------//
log.info('Build regent fields for event : {}', eventRegent.code);
var authorities = $computeDestinataire.context.authorities.authority;
var ediCodes = [];
for (var idx = 0; idx < authorities.size(); idx++ ) { ediCodes.push(authorities.get(idx).ediCode); }

for (var idx = 0; idx < authorities.size(); idx++ ) {
	var authority =  authorities.get(idx);
	log.debug('Prepare regent fields for authority : {}', authority.ediCode);
		
	//--------------- Default regent fields value --------------//
	var regentFields = [];
	regentFields.push({ 'path' : '/REGENT-XML/Emetteur', 'value' : 'Z1611'});
	regentFields.push({ 'path' : '/REGENT-XML/Destinataire', 'value' : authority.ediCode});
	regentFields.push({ 'path' : '/REGENT-XML/DateHeureEmission', 'value' : null});
	regentFields.push({ 'path' : '/REGENT-XML/VersionMessage', 'value' : null});
	regentFields.push({ 'path' : '/REGENT-XML/VersionNorme', 'value' : null});
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Specification/NomService', 'value' : null});
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Specification/VersionService', 'value' : null});
	//--------------- Default regent fields value --------------//

	//Module Occur O Observations
	//GDF 1 O
	//ICM 1 O
	//FJM 1 O
	//IPU 1 O
	//SIU 1 O
	//LIU 1 C
	//GFE 999 C

	//-->GDF : Groupe Données de Service
	//IDF Identification de la formalité 1 O
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C01', 'value' : null});
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02', 'value' : null});
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C03', 'value' :'0'});
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C04', 'value' : null});

	typeLiasse = 'C';
	var entrepriseFormeJuridique = $m4.cadre1Identite.cadre1RappelIdentification.entrepriseFormeJuridique;
	if ( Value('id').of(entrepriseFormeJuridique).eq('5485')
						or Value('id').of(entrepriseFormeJuridique).eq('5785')
						or Value('id').of(entrepriseFormeJuridique).eq('5585')
						or Value('id').of(entrepriseFormeJuridique).eq('5685')
						or Value('id').of(entrepriseFormeJuridique).eq('5385')
						or Value('id').of(entrepriseFormeJuridique).eq('6589')
						or Value('id').of(entrepriseFormeJuridique).eq('6521')
						or Value('id').of(entrepriseFormeJuridique).eq('6532')
						or Value('id').of(entrepriseFormeJuridique).eq('6539')
						or Value('id').of(entrepriseFormeJuridique).eq('6540')
						or Value('id').of(entrepriseFormeJuridique).eq('6541')
						or Value('id').of(entrepriseFormeJuridique).eq('6542')
						or Value('id').of(entrepriseFormeJuridique).eq('6599')
						or Value('id').of(entrepriseFormeJuridique).eq('6543')
						or Value('id').of(entrepriseFormeJuridique).eq('6544')
						or Value('id').of(entrepriseFormeJuridique).eq('6554')
						or Value('id').of(entrepriseFormeJuridique).eq('6558')
						or Value('id').of(entrepriseFormeJuridique).eq('6560')
						or Value('id').of(entrepriseFormeJuridique).eq('6561')
						or Value('id').of(entrepriseFormeJuridique).eq('6562')
						or Value('id').of(entrepriseFormeJuridique).eq('6563')
						or Value('id').of(entrepriseFormeJuridique).eq('6564')
						or Value('id').of(entrepriseFormeJuridique).eq('6565')
						or Value('id').of(entrepriseFormeJuridique).eq('6566')
						or Value('id').of(entrepriseFormeJuridique).eq('6567')
						or Value('id').of(entrepriseFormeJuridique).eq('6568')
						or Value('id').of(entrepriseFormeJuridique).eq('6569')
						or Value('id').of(entrepriseFormeJuridique).eq('6571')
						or Value('id').of(entrepriseFormeJuridique).eq('6572')
						or Value('id').of(entrepriseFormeJuridique).eq('6573')
						or Value('id').of(entrepriseFormeJuridique).eq('6574')
						or Value('id').of(entrepriseFormeJuridique).eq('6575')
						or Value('id').of(entrepriseFormeJuridique).eq('6576')
						or Value('id').of(entrepriseFormeJuridique).eq('6577')
						or Value('id').of(entrepriseFormeJuridique).eq('6578')
						or Value('id').of(entrepriseFormeJuridique).eq('6585')
						or Value('id').of(entrepriseFormeJuridique).eq('6534')
						or Value('id').of(entrepriseFormeJuridique).eq('6536')
						or Value('id').of(entrepriseFormeJuridique).eq('6316')) 	{
		typeLiasse =  "R";
	} else if ($m4.cadre1Identite.cadre1RappelIdentification.immatriculationRM){
		typeLiasse =  "M";
	}
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05', 'value' : typeLiasse});
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C06', 'value' : 'N'});

		
	//EDF Evénement déclaré dans la formalité 1 O
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.1', 'value' : eventRegent.code});
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.2', 'value' : parseDate($m4.cadre9SignatureGroup.cadre9Signature.date, '-')});

	//DMF Destinataire de la formalité 1 O
	for (var ediCodeIdx = 0; ediCodeIdx < ediCodes.length; ediCodeIdx++ ) {
		var ediCode = ediCodes[ediCodeIdx];
		regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[' + ( ediCodeIdx + 1 ) + ']' , 'value' : ediCode});
	}
	
	//ADF Adresse de correspondance 1 C si elle existe
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C36', 'value' : Value('id').of($m4.cadre8RensCompGroup.cadre8RensComp.adresseCorrespond).eq('autre') ? //
		$m4.cadre8RensCompGroup.cadre8RensComp.adresseCorrespondance.nomPrenomDenomination : //
		$m4.cadre1Identite.cadre1RappelIdentification.entrepriseDenomination});
	
	
	var adresseCorrespondance = $m4.cadre2AdresseEtablissementGroup.cadreAdresseEtablissement.adresseEtablissementPrincipal;
	if ( Value('id').of($m4.cadre8RensCompGroup.cadre8RensComp.adresseCorrespond).eq('autre') ) {
		adresseCorrespondance = $m4.cadre8RensCompGroup.cadre8RensComp.adresseCorrespondance;
	}
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3', 'value' : adresseCorrespondance.commune.getId()});
	
	if (null != adresseCorrespondance.numeroVoie) { regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.5', 'value' : getFieldValue(adresseCorrespondance.numeroVoie)});}	
	if (null != adresseCorrespondance.indiceVoie) { regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6', 'value' : getFieldValue(adresseCorrespondance.indiceVoie)}); }
	if (null != adresseCorrespondance.distributionSpecialeVoie) { regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.7', 'value' : getFieldValue(adresseCorrespondance.distributionSpecialeVoie)}); }
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8', 'value' : getFieldValue(adresseCorrespondance.codePostal)});
	if (null != adresseCorrespondance.complementVoie) { regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.10', 'value' : getFieldValue(adresseCorrespondance.complementVoie)}); }
	if (null != adresseCorrespondance.typeVoie) { regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11', 'value' : getFieldValue(adresseCorrespondance.typeVoie)}); }
	if (null != adresseCorrespondance.nomVoie) { regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12', 'value' : getFieldValue(adresseCorrespondance.nomVoie)}); }		
	
	log.debug('Commune de correspondance : {}', adresseCorrespondance.commune);
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13', 'value' : adresseCorrespondance.commune.getLabel()});
	
	var infosSup = $m4.cadre8RensCompGroup.cadre8RensComp.infosSup;
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.1[1]', 'value' : infosSup.telephone2.e164.replace('+', '00')});

	if (null != infosSup.telephone1) {
		regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.1[2]', 'value' : infosSup.telephone1.e164.replace('+', '00')});
	}

	if (null != infosSup.telecopie) {
		regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.2', 'value' : infosSup.telecopie.e164.replace('+', '00')});
	}

	if (null != infosSup.faxCourriel) {
		regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.3', 'value' : infosSup.faxCourriel});
	}
	
	//SIF Signature de la formalité 1 O
	var signature = $m4.cadre9SignatureGroup.cadre9Signature;		
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.1', 'value' : signature.nomPrenomDenomination}); 
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C41', 'value' : signature.lieu});
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C42', 'value' : parseDate(signature.date, '-')});
	
	if ( Value('id').of(signature.soussigne). eq('mandataire') ) {
		var adresseMandataire = signature.adresseMandataire;
		regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.2', 'value' : "Mandataire"});
		regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.3', 'value' : adresseMandataire.commune.getId()});
		
		if (adresseMandataire.numeroVoie != null) {
			regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.5', 'value' : getFieldValue(adresseMandataire.numeroVoie)});
		}
		
		if (adresseMandataire.indiceVoie !== null) {
		   regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.6', 'value' : adresseMandataire.indiceVoie});
		}
		
		if (adresseMandataire.distributionSpecialeVoie != null) {
			regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.7', 'value' : getFieldValue(adresseMandataire.distributionSpecialeVoie)});
		}
		
		regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.8', 'value' : adresseMandataire.codePostal});
		
		if (adresseMandataire.complementVoie != null) {
			regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.10', 'value' : getFieldValue(adresseMandataire.complementVoie)});
		}
		
		if (adresseMandataire.typeVoie !== null) {
		   regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.11', 'value' : adresseMandataire.typeVoie});
		}
		
		regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.12', 'value' : getFieldValue(adresseMandataire.nomVoie)});
		regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.13', 'value' : adresseMandataire.commune});
	}
	
	if (null != $m4.cadre8RensCompGroup.cadre8RensComp.observations ) { 
		regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C43', 'value' : $m4.cadre8RensCompGroup.cadre8RensComp.observations});
	}
	
	//ICM
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/ICM/M01/M01.1', 'value' : $m4.cadre1Identite.cadre1RappelIdentification.entrepriseDenomination});
	if ( $m4.cadre3InformationsGroup.cadre3Informations.activiteAmbulant ) {
		regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/ICM/M02', 'value' : 'O'});
	}
	
	//CAM
	var dateCessation = $m4.cadre3InformationsGroup.cadre3Informations.dateCessation;
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CAM/M58', 'value' : parseDate(dateCessation, '-') });
	
	//IPU
	var presenceSalaries = (null != $m4.cadre3InformationsGroup.cadre3Informations.presenceSalaries && $m4.cadre3InformationsGroup.cadre3Informations.presenceSalaries) ? 'O' : 'N';
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U02', 'value' : $m4.cadre1Identite.cadre1RappelIdentification.siren.split(' ').join('') });
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U06', 'value' : presenceSalaries });
	
	//SIU
	regentFields.push({ 'path' : '/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U10', 'value' : $m4.cadre1Identite.cadre1RappelIdentification.entrepriseDenomination });
	
	
	var regentFieldsInput = {};
	regentFields.forEach(function (field) { regentFieldsInput[field.path] = field.value; });
	var liasse = null;
	log.debug('Display regent fields for authority code {} : {}', authority.ediCode, regentFieldsInput);
	var xmlRegentFile = buildRegent(eventRegent.code, regentFieldsInput, eventRegent.version, authority.role, authority.ediCode, liasse);
	
	liasse = JSON.parse(nash.xml.extract(xmlRegentFile, '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02')).C02;
	log.info('Extract liasse for authority ediCode {} : {}', authority.ediCode, liasse);
}

return spec.create({
	id : 'xmlGenerationConfirmation',
	label : "Xml Regent confirmation message",
	groups : [
		spec.createGroup({
			id : 'confirmationMessageOk',
			description : "Le fichier XML Regent a été généré et ajouté au dossier.",
			data : []
		}) 
	]
});
//--------------- Main --------------//
