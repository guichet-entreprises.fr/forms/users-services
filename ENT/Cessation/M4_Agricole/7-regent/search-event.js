//--------------- FUNCTIONS -------------------//
function getEvent() {		
	if ( [  '5800', '3220' ].indexOf($m4Agr.cadre1Identite.cadre1RappelIdentification.entrepriseFormeJuridique.getId()) != -1 ) { return '43M'; }
	
	if ( $m4Agr.cadre3InformationsGroup.cadre3Informations.motifCessation.getId() == 'fusion' ) { return '41M'; }
	
	return '42M';
}
//--------------- FUNCTIONS -------------------//

//--------------- MAIN -------------------//
var eventCode = getEvent();
log.info('Found REGENT event : {}', eventCode);
nash.instance //
	.load('event.xml') //
	.bind('event', {
		'code' : eventCode,
		'version' : 'V2016.02'
	}
);
