//--------------- FUNCTIONS -------------------//
function getFieldValue(field) { return null == field ? '' : field; }

function formatDate() {
	var dateTemp = new Date();
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	return {
		'date' : year.toString() + month.toString() + day.toString(),
		'time' : dateTemp.toTimeString().substr(0,8).replace(new RegExp(':', 'g'), '')
	};
}

function displayXMLTC(liasse) {
	var display = formatDate();
	return 'C1000A1001L' + liasse.substr(6) + 'D' +  display.date + 'H' + display.time + 'TXMLTCS001PGUEN.xml';
}

function buildXMLTC(liasse, xmlTcAsJson) {			
	var response = null;
	try {		
		response = nash.service.request('${regent.baseUrl}/private/v1/xml-tc/generate') //
			.connectionTimeout(10000) //
			.receiveTimeout(10000) //
			.accept('json') //
			.dataType('application/json') // 
			.post(JSON.stringify(xmlTcAsJson)); 
		;
	} catch (e) {	
		log.error(e);			
		throw 'An technical error occured when generating XML TC for liasse : ' + liasse;
	}
		
	if (response != null && response.getStatus() == 200) {
		var xmlTCName = displayXMLTC(liasse);
		nash.record.saveFile(xmlTCName, response.asBytes());
		return xmlTCName;
	}
	throw 'Unsuccessful XML TC generation for liasse : ' + liasse;
}

function link(uid, reference, author) {		
	var trackerResponse = null;
	try {
		trackerResponse = nash.service.request('${tracker.baseUrl}/v1/uid/{uid}/ref/{ref}', uid, reference) //
			.dataType('application/json') //
			.accept('json') //
			.param('author', author) //
			.post(null) //
		;
	} catch (e) {
		log.error('An technical error occured when calling tracker with uid {}, ref {} and author {}', uid, reference, author);	
		log.error(e);			
		return null;
	}
	if (trackerResponse != null && trackerResponse.getStatus() == 200) {			
		return trackerResponse.asString();
	}
	return null;
}
//--------------- FUNCTIONS -------------------//

//--------------- Variables init --------------//
var dateHeureGeneration = new Date().toISOString().slice(0,19);
var numeroDossierUnique = nash.record.description().recordUid; 
var nomDossier = nash.record.description().title;
var authorities = $computeDestinataire.context.authorities.authority;
var documents = $computeDestinataire.context.documents;
var indicesPj = [];
var outputUserAttachment = [];
//--------------- Variables init --------------// 

//--------------- Main --------------//
var destinataireDossierUnique = [];
for (var idx = 0; idx < authorities.size(); idx++ ) {
	var authority = authorities.get(idx);
	var oneDestinataire = {};
	var codeDestinataire = {
		'codePartenaire' : authority.ediCode,
		'codeEdi' : authority.ediCode,		
		'libelleIntervenant' : authority.label
	};
	oneDestinataire["roleDestinataire"] = authority.role;
	oneDestinataire["codeDestinataire"] = codeDestinataire;
	destinataireDossierUnique.push(oneDestinataire);	
}

var pieceJointes = [];
var indicePj = 2;
var indiceXmlCfe = 0;
var indiceCerfaCfe = 0;
var indiceXmlTdr = 0;
for (var idx = 0; idx < authorities.size(); idx++ ) {
	var authority = authorities.get(idx);
	if (authority.ediCode.charAt(0) == 'G') {
		pieceJointes.push({
			'indicePieceJointe' : indicePj,
			'typePieceJointe' : 'LIASSE_CFE_EDI',
			'formatPieceJointe' : 'XML',
			'fichierPieceJointe' : documents.regent.get(idx).rename
		});
		indiceXmlCfe = indicePj;
		indicePj++;
		
		pieceJointes.push({
			'indicePieceJointe' : indicePj,
			'typePieceJointe' : 'LIASSE_CFE_PDF',
			'formatPieceJointe' : 'PDF',
			'fichierPieceJointe' : documents.cerfa.rename
		});
		indiceCerfaCfe = indicePj;
		indicePj++;
	} else {		
		pieceJointes.push({
			'indicePieceJointe' : indicePj,
			'typePieceJointe' : 'LIASSE_TDR_EDI',
			'formatPieceJointe' : 'XML',
			'fichierPieceJointe' : documents.regent.get(idx).rename
		});
		indiceXmlTdr = indicePj;
		indicePj++;
	}
}

for (var idx = 0; idx < documents.others.size(); idx++ ) {
	var userAttachment = documents.others.get(idx);
	var fileType = userAttachment.rename.split('.');
	var extensionFile = fileType[fileType.length - 1];		
	pieceJointes.push({
		'indicePieceJointe' : indicePj,
		'typePieceJointe' : 'AUTRE_PJ',
		'formatPieceJointe' : extensionFile,
		'fichierPieceJointe' : userAttachment.rename
	});
	indicesPj.push(indicePj);
	indicePj++;
	outputUserAttachment.push({'id' : 'attachement[' + idx + ']',  'label' : '/' + userAttachment.rename});
}

for (var idx = 0; idx < authorities.size(); idx++ ) {
	var authority = authorities.get(idx);
	if (authority.ediCode.charAt(0) != 'G') { continue; }
		
	var xmlRegentFileName = '/XML_REGENT_' + authority.ediCode + '.xml';
	
	var result = nash.xml.extract(xmlRegentFileName, '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02');
	var parsedResult = JSON.parse(result);
	var numeroDeLiasse = parsedResult['C02'];	
	log.debug("numeroDeLiasse : {}", numeroDeLiasse);
	
	//Relier le numéro de dossier interne au numéro de liasse dans tracker
	link(numeroDossierUnique, numeroDeLiasse,  nash.record.description().author);

	//Infos de correspondance 
	// Nom et Prénom de correspondance
	var resNomCorrespondant = nash.xml.extract(xmlRegentFileName, '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C36');
	var resultNomCorrespondant = JSON.parse(resNomCorrespondant);
	var nomCorrespondant = resultNomCorrespondant['C36'];
	
	//===> [Début] Partié générique pour récupérer le nom du signataire <=====//
	//Information signataire paiement
	var resNomSignataire = nash.xml.extract(xmlRegentFileName, '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.1');
	var resultNomSignataire = JSON.parse(resNomSignataire);
	var nomSignataire = resultNomSignataire['C40.1'];
	//===> [Fin] Partié générique pour récupérer le nom du signataire <=====//

	//===> [Début] Partié générique pour récupérer l'adresse du signataire/Mandataire <=====//
	//Informations d'adresse signataire/Mandataire
	var signature = $m4Agr.cadre9SignatureGroup.cadre9Signature;
	if ( Value('id').of(signature.soussigne).eq('mandataire') ) {
		
		// Numéro de voie
		var resNumVoiePayeur = nash.xml.extract(xmlRegentFileName, '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.5');
		var resultNumVoiePayeur = JSON.parse(resNumVoiePayeur);
		var numeroDeVoiePayeur = resultNumVoiePayeur['C40.3.5'];
		//Indice de répétition
		var resindRepPayeur = nash.xml.extract(xmlRegentFileName, '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.6');
		var resultindRepPayeur = JSON.parse(resindRepPayeur);
		var indRepPayeur = resultindRepPayeur['C40.3.6'];
		// Type de voie
		var resTypeDeVoiePayeur = nash.xml.extract(xmlRegentFileName, '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.11');
		var resultTypeDeVoiePayeur = JSON.parse(resTypeDeVoiePayeur);
		var typeDeVoiePayeur = resultTypeDeVoiePayeur['C40.3.11'];
		// Libellé voie
		var resLibelleDeVoiePayeur = nash.xml.extract(xmlRegentFileName, '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.12');
		var resultLibelleDeVoiePayeur = JSON.parse(resLibelleDeVoiePayeur);
		var libelleDeVoiePayeur = resultLibelleDeVoiePayeur['C40.3.12'];
		// Libellé de la localité
		var resLocalitePayeur = nash.xml.extract(xmlRegentFileName, '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.13');
		var resultLocalitePayeur = JSON.parse(resLocalitePayeur);
		var localitePayeur = resultLocalitePayeur['C40.3.13'];
		// Code postal
		var resCodePostalPayeur = nash.xml.extract(xmlRegentFileName, '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.8');
		var resultCodePostalPayeur = JSON.parse(resCodePostalPayeur);
		var codePostalPayeur = resultCodePostalPayeur['C40.3.8'];
		// Code pays
		var resCodePaysPayeur = nash.xml.extract(xmlRegentFileName, '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.14');
		var resultCodePaysPayeur = JSON.parse(resCodePaysPayeur);
		var codePaysPayeur = resultCodePaysPayeur['C40.3.14'];
		if (codePaysPayeur == null || codePaysPayeur == '') {
		codePaysPayeur= 'FR';
		}
		//complément de localisation
		var complementDeLocalisationPayeur;

		var resComplLocaPayeur = nash.xml.extract(xmlRegentFileName, '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.10');
		var resultComplLocaPayeur = JSON.parse(resComplLocaPayeur);
		var complLocaPayeur = resultComplLocaPayeur['C40.3.10'];
		if (complLocaPayeur){
			complementDeLocalisationPayeur = complLocaPayeur ;
		}
		var resDestSpePayeur = nash.xml.extract(xmlRegentFileName, '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.7');
		var resultDestSpePayeur = JSON.parse(resDestSpePayeur);
		var destSpePayeur = resultDestSpePayeur['C40.3.7'];
		if (destSpePayeur) {
			if (complementDeLocalisationPayeur) {
				complementDeLocalisationPayeur = complementDeLocalisationPayeur + '-' + destSpePayeur; 
			} else {
				complementDeLocalisationPayeur = destSpePayeur; 
			}
		}
		// Adresse Email
		var resAdresseEmailPayeur = nash.xml.extract(xmlRegentFileName, '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.3');
		var resultAdresseEmailPayeur = JSON.parse(resAdresseEmailPayeur);
		var adresseEmailPayeur = resultAdresseEmailPayeur['C39.3'];
		if (adresseEmailPayeur == null || adresseEmailPayeur == '') {
			adresseEmailPayeur= 'no.mail@adresse.fr';
		}
		//===> [Fin] Partié générique pour récupérer l'adresse du signataire/Mandataire <=====//
	} else {
		//===> [Début] Partié Adaptable en fonction de la formalité à remplire par le rédacteur de formalité pour récupérer l'adresse du Déclarant <=====//
		var adressePayeur = $m4Agr.cadre2AdresseEtablissementGroup.cadreAdresseEtablissement.adresseEtablissementPrincipal;
		
		// Numéro de voie
		var numeroDeVoiePayeur = adressePayeur.numeroVoie != null ? adressePayeur.numeroVoie : undefined;
		//Indice de répétition
		var indRepPayeur = adressePayeur.indiceVoie != null ? adressePayeur.indiceVoie.getId() : undefined;
		// Type de voie
		var typeDeVoiePayeur = adressePayeur.typeVoie != null ? adressePayeur.typeVoie.getId() : undefined;
		// Libellé voie
		var libelleDeVoiePayeur = adressePayeur.nomVoie != null ? adressePayeur.nomVoie : undefined;
		// Code postal
		var codePostalPayeur = adressePayeur.codePostal != null ? adressePayeur.codePostal : undefined;
		// Code pays
		var codePaysPayeur = "FR";
		//complément de localisation
		var complementDeLocalisationPayeur = (adressePayeur.complementVoie != null ? adressePayeur.complementVoie : undefined)
            + '-' + (adressePayeur.distriutionSpecialeVoie != null ? adressePayeur.distriutionSpecialeVoie : undefined);
		// Adresse Email
		var resAdresseEmailPayeur = nash.xml.extract(xmlRegentFileName, '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.3');
		var resultAdresseEmailPayeur = JSON.parse(resAdresseEmailPayeur);
		var adresseEmailPayeur = resultAdresseEmailPayeur['C39.3'];
		if (adresseEmailPayeur == null || adresseEmailPayeur == '') {
			adresseEmailPayeur = 'no.mail@adresse.fr';
		}
		//===> [Fin] Partié Adaptable en fonction de la formalité à remplire par le rédacteur de formalité pour récupérer l'adresse du Déclarant <=====//
	}
	
	//Information adresse correspondant								   
	// Numéro de voie
	var resNumVoie = nash.xml.extract(xmlRegentFileName, '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.5');
	var resultNumVoie = JSON.parse(resNumVoie);
	var numeroDeVoie = resultNumVoie['C37.5'];
	//Indice de répétition
	var resindRep = nash.xml.extract(xmlRegentFileName, '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6');
	var resultindRep = JSON.parse(resindRep);
	var indRep = resultindRep['C37.6'];
	// Type de voie
	var resTypeDeVoie = nash.xml.extract(xmlRegentFileName, '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11');
	var resultTypeDeVoie = JSON.parse(resTypeDeVoie);
	var typeDeVoie = resultTypeDeVoie['C37.11'];
	// Libellé voie
	var resLibelleDeVoie = nash.xml.extract(xmlRegentFileName, '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12');
	var resultLibelleDeVoie = JSON.parse(resLibelleDeVoie);
	var libelleDeVoie = resultLibelleDeVoie['C37.12'];
	// Libellé de la localité
	var resLocalite = nash.xml.extract(xmlRegentFileName, '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13');
	var resultLocalite = JSON.parse(resLocalite);
	var localite = resultLocalite['C37.13'];
	// Code postal
	var resCodePostal = nash.xml.extract(xmlRegentFileName, '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8');
	var resultCodePostal = JSON.parse(resCodePostal);
	var codePostal = resultCodePostal['C37.8'];
	// Code pays
	var resCodePays = nash.xml.extract(xmlRegentFileName, '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.14');
	var resultCodePays = JSON.parse(resCodePays);
	var codePays = resultCodePays['C37.14'];
	if (codePays == null || codePays == '') {
		codePays= 'FR';
	}
	
	//complément de localisation
	var complementDeLocalisation ;

	var resComplLoca = nash.xml.extract(xmlRegentFileName, '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.10');
	var resultComplLoca = JSON.parse(resComplLoca);
	var complLoca = resultComplLoca['C37.10'];
	if (complLoca) {
		complementDeLocalisation = complLoca ;
	}
	var resDestSpe = nash.xml.extract(xmlRegentFileName, '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.7');
	var resultDestSpe = JSON.parse(resDestSpe);
	var destSpe = resultDestSpe['C37.7'];
	if (destSpe) {
		if (complementDeLocalisation) {
			complementDeLocalisation = complementDeLocalisation + '-' + destSpe; 
		}else{
			complementDeLocalisation = destSpe; 
		}
	}
	// Adresse Email
	var resAdresseEmail = nash.xml.extract(xmlRegentFileName, '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.3');
	var resultAdresseEmail = JSON.parse(resAdresseEmail);
	var adresseEmail = resultAdresseEmail['C39.3'];
	if (adresseEmail == null || adresseEmail == '') {
		adresseEmail = 'no.mail@adresse.fr';
	}
	//<=========
	
	log.debug("dateHeureGeneration : {}", dateHeureGeneration);
	log.debug("nomCorrespondant : {}", nomCorrespondant);
	log.debug("typeDeVoie : {}", typeDeVoie);
	log.debug("libelleDeVoie : {}", libelleDeVoie);
	log.debug("localite : {}", localite);
	log.debug("numeroDeVoie is  {}", numeroDeVoie);
	log.debug("codePostal : {}", codePostal);
	log.debug("bureauDistributeur is  {}", localite);
	log.debug("codePays : {}", codePays);
	log.debug("adresseEmail : {}", adresseEmail);
	
	var xmlTcJson = {};
	xmlTcJson["version"] = "V2012.02";
	xmlTcJson["emetteur"] = "10001001";
	xmlTcJson["destinataire"] = "10001001";
	xmlTcJson["dateHeureGenerationXml"] = dateHeureGeneration;
	xmlTcJson["commentaire"] = "Commentaire";

	var dossierUnique = {};
	var identificationDossierUnique = {};
	var identifiantDossierUnique = {};
	identifiantDossierUnique["codePartenaireEmetteur"] = "FRONT-OFFICE";
	identifiantDossierUnique["numeroDossierUnique"] = numeroDossierUnique;

	var correspondant = {};
	var adresseCorrespondant = {}
	adresseCorrespondant["numeroDeVoie"] = numeroDeVoie
	adresseCorrespondant["indiceDeRepetition"] = indRep
	adresseCorrespondant["typeDeVoie"] = typeDeVoie
	adresseCorrespondant["libelleDeVoie"] = libelleDeVoie
	adresseCorrespondant["localite"] = localite
	adresseCorrespondant["complementDeLocalisation"] = complementDeLocalisation
	adresseCorrespondant["codePostal"] = codePostal
	adresseCorrespondant["bureauDistributeur"] = localite
	adresseCorrespondant["codePays"] = codePays
	adresseCorrespondant["adresseEmail"] = adresseEmail

	correspondant["identiteCorrespondant"] = {
		"nomCorrespondant" : nomSignataire
	};
	correspondant["adresseCorrespondant"] = adresseCorrespondant;

	identificationDossierUnique["identifiantDossierUnique"] = identifiantDossierUnique;
	identificationDossierUnique["typeDossierUnique"] = "D1";
	identificationDossierUnique["nomDossier"] = nomDossier;
	identificationDossierUnique["correspondant"] = correspondant;
}

//Intégration du paiement dans le XML-TC si l'autorité a reçu un paiement	
var dossierCfe = {};
var destinataireDossierCfe = [];

//dossier destinataire cfe 1
var oneDestinataireDossierCfe = {};
var indicePieceJointeCFE = [];
indicePieceJointeCFE.push(indiceCerfaCfe);

//insérer les indices des PJ pour ce partenaire
var indicesPjLength = indicesPj.length;
for (var i = 0; i < indicesPjLength; i++) {
	indicePieceJointeCFE.push(indicesPj[i]);
}

//Bloc à rajouter pour les paiements de la première autorité					
//Récupérer la liste des codes autorités présentes dans le data generated contenant les informations de paiement
for (var idx = 0; idx < authorities.size(); idx++ ) {
	var authority = authorities.get(idx);
	if (authority.ediCode.charAt(0) != 'G') { continue; }
	
	//Récupérer l'adresse du payeur : 
	var adressePayeur = {
		'numeroDeVoie' : numeroDeVoiePayeur,
		'typeDeVoie' : typeDeVoiePayeur,
		'libelleDeVoie' : libelleDeVoiePayeur,
		'localite' : localitePayeur,
		'codePostal' : codePostalPayeur,
		'bureauDistributeur' : localitePayeur,
		'codePays' : codePaysPayeur,
		'adresseEmail' : adresseEmailPayeur
	};
	
	//Récupérer les informations de paiements effectués dans le dossier et leurs informations
	var generalInformation = (!_input.infoGenGroup || !_input.infoGenGroup.infoGen) ? null : _input.infoGenGroup.infoGen;
	var referencePaiement = (!generalInformation || !generalInformation.transactionId) ? 'XXXXXXXXXXXXXXX' : generalInformation.transactionId;
	var dateHeurePaiement = (!generalInformation || !generalInformation.transactionDate) ? null : generalInformation.transactionDate;
	var listPayments = (!_input.infoGenGroup || !_input.infoGenGroup.infoPartners.partnerPayment) ? [] : _input.infoGenGroup.infoPartners.partnerPayment;
	var paymentPartnerIdFirstAuthority = authority.ediCode;
	
	//3- Faire la somme des paiements pour cette autorité
	var totalAmountPaymentsAuthority1 = 0.00;
	for (var i = 0; i < listPayments.length; i++) {
		//Poster sur tracker le détail des paiements effectués pour une autorité:
		log.debug("Payment partner id for the {} payment : {}", i, listPayments[i]['partnerId']);

		if (listPayments[i]['partnerId'] == paymentPartnerIdFirstAuthority) {
			log.debug("amount for this payment : {}", listPayments[i]['paymentAmount']);
			totalAmountPaymentsAuthority1 += parseFloat(listPayments[i]['paymentAmount']);
		}
		log.debug("totalAmountPaymentsAuthority1 : {}", totalAmountPaymentsAuthority1);
	}
	var frenchAmount = totalAmountPaymentsAuthority1.toFixed(2).replace('.', ',');
	var paiementDossier = {
		'identitePayeur' : {
			'nomPayeur' : nomSignataire
		},
		'adressePayeur' : adressePayeur,
		'montantFormalite' : frenchAmount,
		'referencePaiement' : referencePaiement,
		'dateHeurePaiement' : dateHeurePaiement
	};
	oneDestinataireDossierCfe["paiementDossier"] = paiementDossier;
	oneDestinataireDossierCfe["indicePieceJointe"] = indicePieceJointeCFE;
	oneDestinataireDossierCfe["roleDestinataire"] = authority.role;
	oneDestinataireDossierCfe["codeEdiDestinataire"] = authority.ediCode;
	oneDestinataireDossierCfe["indiceLiasseXml"] = indiceXmlCfe;
	destinataireDossierCfe.push(oneDestinataireDossierCfe);
	break;
}
dossierCfe["numeroDeLiasse"] = numeroDeLiasse;
dossierCfe["dateHeureDepot"] = dateHeureGeneration;
dossierCfe["referenceLiasseFo"] = numeroDossierUnique;
dossierCfe["destinataireDossierCfe"] = destinataireDossierCfe;

dossierUnique["identificationDossierUnique"] = identificationDossierUnique;
dossierUnique["destinataireDossierUnique"] = destinataireDossierUnique;
dossierUnique["pieceJointe"] = pieceJointes;
dossierUnique["dossierCfe"] = dossierCfe;

xmlTcJson["dossierUnique"] = dossierUnique;

log.debug("xmlTcJson : {}", xmlTcJson);
log.debug("JSON.stringify(xmlTcJson) : {}", JSON.stringify(xmlTcJson));

//--------------- Build the XMLTC file --------------//
var xmlTcFormatted = buildXMLTC(numeroDeLiasse, xmlTcJson);
//--------------- Build the XMLTC file --------------//

//--------------- Prepare input files for Markov transitions --------------//
var denomination = $m4Agr.cadre1Identite.cadre1RappelIdentification.entrepriseDenomination;
for (var idx = 0; idx < authorities.size(); idx++ ) {
	var authority = authorities.get(idx);
	var output = nash.instance.load('output' + idx +'.xml');
	output.bind("parameters", {
		"attachment" : {
			"cerfa" : '/' + documents.cerfa.rename,
			"regent" : '/' + documents.regent.get(idx).rename,
			"others" : outputUserAttachment
		}
	});
	
	output.bind("result", {
		"funcId" : authority.code,
		"codeEdi" : authority.ediCode,
		"denomination" : denomination
	});

	if (authority.ediCode.charAt(0) == 'G') {		
		output.bind("parameters", {
			"attachment" : {
				"xmltc" : '/' + xmlTcFormatted
			}
		});
	}
}
//--------------- Prepare input files for Markov transitions --------------//

//--------------- Main --------------//

