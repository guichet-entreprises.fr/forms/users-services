//--------------- FUNCTIONS -------------------//
function pad(s) { return (s < 10) ? '0' + s : s; }

function formatDate() {
	var dateTemp = new Date();
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	return {
		'date' : year.toString() + month.toString() + day.toString(),
		'time' : dateTemp.toTimeString().substr(0,8).replace(new RegExp(':', 'g'), '')
	};
}

function extractLiasse(authority) {
	return JSON.parse(nash.xml.extract('/XML_REGENT_' + authority.ediCode + '.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02'))['C02'];
}

function displayCerfa(liasse, index) {
	var display = formatDate();
	return 'C1000A1001L' + liasse.substr(6) + 'D' +  display.date + 'H' + display.time + 'TPIJTES00' + index + 'PGUEN.pdf';
} 

function displayRegent(liasse, index) {
	var display = formatDate();
	return 'C1000A1001L' + liasse.substr(6) + 'D' +  display.date + 'H' + display.time + 'TPIJTES00' + index + 'PGUEN.xml';
}

function displayUserAttachment(liasse, index, extensionFile) {
	var display = formatDate();
	return 'C1000A1001L' + liasse.substr(6) + 'D' +  display.date + 'H' + display.time + 'TPIJTE' + index + 'PGUEN' + '.' + extensionFile;
}

//--------------- FUNCTIONS -------------------//

//--------------- Variables init --------------//
var authorities = $computeDestinataire.context.authorities.authority;
var cerfaSigned = nash.util.resourceFromPath('/4-signature/generated/proxyResult.files-1-PROXY_SIGNED_DOCUMENT.pdf');
log.info('cerfaSigned : {}', cerfaSigned);

var userAttachmentGroup = _record.preprocess.preprocess;
var indexFile = 2;
var indiceUserAttachment = "S0";
var documents = [];
var computeDestinataire = nash.instance.load('/5-payment/computeDestinataire.xml');
//--------------- Variables init --------------//

//--------------- Rename CERFA and REGENT attachment --------------//
//-->Creating REGENT with new name into record
var liasse = extractLiasse(authorities.get(0));
var newCerfaFileName = displayCerfa(liasse, indexFile);
nash.record.saveFile(newCerfaFileName, cerfaSigned.getContent() );
indexFile++;
	
var regentFiles = [];
for (var idx = 0; idx < authorities.size(); idx++ ) {
	var authority = authorities.get(idx);
	var liasse = extractLiasse(authority);
	
	//-->Creating REGENT with new name into record
	var newRegentFileName = displayRegent(liasse, indexFile);
	nash.record.saveFile(newRegentFileName, nash.util.resourceFromPath('/XML_REGENT_' + authority.ediCode + '.xml').getContent() );
	indexFile++;
			
	regentFiles.push({
		'name' : '/XML_REGENT_' + authority.ediCode + '.xml',
		'rename' : newRegentFileName
	});
}
//--------------- Rename CERFA and REGENT attachment --------------//

//--------------- Rename user attachment --------------//
var otherFiles = [];
for (var attachment in userAttachmentGroup ) {
	var pjGroup = userAttachmentGroup[attachment];

	for (var i = 0; i < pjGroup.length; i++) {
		var filePath = '/2-attachment/uploaded/preprocess.' + attachment + '-' + pjGroup[ i ].id + '-' + pjGroup[ i ].label;
		var fileType = pjGroup[i].label.split('.');
		var extensionFile = fileType[fileType.length - 1];

		var indiceUserAttachment = ( indexFile < 10 ) ? ('S00' + indexFile) : ('S0' + indexFile);
		log.info('indice user attachment : {}', indiceUserAttachment);
		
		var userAttachmentFileName = displayUserAttachment(liasse, indiceUserAttachment, extensionFile);
		nash.record.saveFile(userAttachmentFileName, nash.util.resourceFromPath(filePath).getContent() );
		
		indexFile++;
			
		otherFiles.push({
			'name' : filePath,
			'rename' : userAttachmentFileName
		});
	}
}
//--------------- Rename user attachment --------------//


//--------------- Bind results --------------//
log.debug('Renamed documents : {}', documents);
computeDestinataire.bind('context', {
	'documents' : {
		'cerfa' : {
			'name' : cerfaSigned.getAbsolutePath(),
			'rename' : newCerfaFileName
		},
		'regent' : regentFiles,
		'others' : otherFiles
	}
});
//--------------- Bind results --------------//
