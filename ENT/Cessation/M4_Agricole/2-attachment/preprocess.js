if ( Value('id').of($m4Agr.cadre9SignatureGroup.cadre9Signature.soussigne).eq('representantLegal') ) {
    attachment('pjIDDeclarantSignataire', 'pjIDDeclarantSignataire', { mandatory:"true"});
} else {
    attachment('pjIDMandataireSignataire', 'pjIDMandataireSignataire', { mandatory:"true"});
}

if ( Value('id').of($m4Agr.cadre9SignatureGroup.cadre9Signature.soussigne).eq('mandataire') ) {
    attachment('pjPouvoir', 'pjPouvoir', { mandatory:"true"});
}

if ( not Value('id').of($m4Agr.cadre3InformationsGroup.cadre3Informations.motifCessation).eq('transfertPatrimoine') ) {
    attachment('pjExemplaireClotureOperation', 'pjExemplaireClotureOperation', { mandatory:"true"});
    attachment('pjParution', 'pjParution', { mandatory:"true"});
}
