//check inout information

var loader = nash.record;

loader.remove("/4-signature/proxy-calling.xml");
loader.remove("/4-signature/stamp-calling.xml");
log.info("Remove proxy calling and proxy result files");

var uid = loader.description().getRecordUid();
var stepId = loader.currentStepPosition();

log.info("Back to signature step for the record {}", uid);

return { url: "/record/" + uid + "/" + stepId + "/page/0" };
