//--------------- FUNCTIONS -------------------//
function getUser(author) {			
	var response = null;
	try {		
		response = nash.service.request('${account.ge.baseUrl.read}/private/users/{id}', nash.record.description().author) //
		   .connectionTimeout(10000) //
		   .receiveTimeout(10000) //
		   .accept('text') //
		   .get()
		;
	} catch (e) {	
		log.error(e);			
		throw 'An technical error occured when calling account with author : ' + author;
	}
	if (response != null && response.getStatus() == 200) {
		return response.asObject();
	}
	throw 'Cannot find any author : ' + author;
}
//--------------- FUNCTIONS -------------------//

//--------------- Variables init --------------//
var docs = $review.generated.record;
var files = [];
//--------------- Variables init --------------//

//--------------- Prepare the hangout --------------//
for (var i = 0; i < docs.length; ++i) {
    files.push({
        'document': docs[i],
        'zoneId': 'signature'
    });
}

var user = getUser(nash.record.description().author);
//--------------- Prepare the hangout --------------//
		
//--------------- Hangout to the proxy --------------//
return nash.hangout.stamp({
    'files' : files,
    'civility' : user.civility,
    'lastName' : user.lastName,
    'firstName' : user.firstName,
    'email' : user.email,
    'phone' : user.phone.replace('+', '00')
});
