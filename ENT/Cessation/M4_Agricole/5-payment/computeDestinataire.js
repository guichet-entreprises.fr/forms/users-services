//--------------- FUNCTIONS -------------------//
function getAuthority(authorityId) {			
	var response = null;
	try {
		response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', authorityId) //
			   .connectionTimeout(10000) //
			   .receiveTimeout(10000) //
			   .accept('json') //
			   .get()
		;
	} catch (e) {	
		log.error(e);			
		throw 'An technical error occured when calling directory with authority uid : ' + authorityId;
	}
	if (response != null && response.getStatus() == 200) {			
		return response.asObject();
	}
	throw 'Cannot find any authority uid : ' + authorityId;
}

function getRate(rateCode) {			
	var response = null;
	try {		
		response = nash.service.request('${directory.baseUrl}/private/v1/repository/{repositoryId}', "frais") //
		   .connectionTimeout(10000) //
		   .receiveTimeout(10000) //
		   .accept('json') //
		   .param("filters", "details.entityId:" + rateCode) //
		   .get()
		;
	} catch (e) {	
		log.error(e);			
		throw 'An technical error occured when calling directory with rate code : ' + rateCode;
	}
	if (response != null && response.getStatus() == 200) {			
		var rateContent = response.asObject().content[0];
		log.debug('Rate content : {}', rateContent);
		return rateContent;
	}
	throw 'Cannot find any rate code : ' + rateCode;
}
//--------------- FUNCTIONS -------------------//

//--------------- Variables init --------------//
var algo = "trouver destinataire";
var typePersonne = "PM";
var optionCMACCI = "NON";
var attachment = "/4-signature/generated/proxyResult.files-1-PROXY_SIGNED_DOCUMENT.pdf";
var codeCommune = $m4Agr.cadre2AdresseEtablissementGroup.cadreAdresseEtablissement.adresseEtablissementPrincipal.commune.getId();
var mandatory = (not Value('id').of($m4Agr.cadre3InformationsGroup.cadre3Informations.motifCessation).eq('transfertPatrimoine'));
var authorityDetails = [];
var paymentAuthorityDetails = [];
var listDistinctAuthorities =[];
var destFuncId2 = '';		 
var codeEDI2 = '';
var partnerLabel2 = '';

//--------------- Referentials --------------//
var referentials = [
	{
		'key' : 'SAS',
		'values' : [
			'5710', '5720'
		]
	},	
	{
		'key' : 'SCA',	
		'values' : [
			'5308', '5309'
		]
	},
	{
		'key' : 'SNC',	
		'values' : [
			'5202', '5203'
		]
	},
	{
		'key' : 'SCS',	
		'values' : [
			'5306', '5307'
		]
	},
	{
		'key' : 'SARL',	
		'values' : [
			'5499', 
			'5410',
			'5415',
			'5422',
			'5426',
			'5430',
			'5431',
			'5432',
			'5442',
			'5443',
			'5451',
			'5453',
			'5454',
			'5455',
			'5458',
			'5459',
			'5460'
		]
	},
	{
		'key' : 'SEL',	
		'values' : [
			'5485',
			'5785',
			'5585',
			'5685',
			'5385',
			'6589',
			'6521',
			'6532',
			'6539',
			'6540',
			'6541',
			'6542',
			'6599',
			'6543',
			'6544',
			'6554',
			'6558',
			'6560',
			'6561',
			'6562',
			'6563',
			'6564',
			'6565',
			'6566',
			'6567',
			'6568',
			'6569',
			'6571',
			'6572',
			'6573',
			'6574',
			'6575',
			'6576',
			'6577',
			'6578',
			'6585',
			'6534',
			'6536',
			'6316'
		]
	},
	{
		'key' : 'SA',	
		'values' : null
	}
];
//--------------- Referentials --------------//

//--------------- Determing input parameters to Directory algorithm --------------//
var formJuridique = "SA";
referentials.forEach(function (item) {
	if (null != item.values && item.values.indexOf($m4Agr.cadre1Identite.cadre1RappelIdentification.entrepriseFormeJuridique.getId()) != -1) {
		formJuridique = item.key;
	}
}); 

if ([
'5485',
'5785',
'5585',
'5685',
'5385',
'6589',
'6521',
'6532',
'6539',
'6540',
'6541',
'6542',
'6599',
'6543',
'6544',
'6554',
'6558',
'6560',
'6561',
'6562',
'6563',
'6564',
'6565',
'6566',
'6567',
'6568',
'6569',
'6571',
'6572',
'6573',
'6574',
'6575',
'6576',
'6577',
'6578',
'6585',
'6534',
'6536',
'6316'
].indexOf($m4Agr.cadre1Identite.cadre1RappelIdentification.entrepriseFormeJuridique.getId()) != -1) 	{
	var secteur1 =  "LIBERAL";
} else if ($m4Agr.cadre1Identite.cadre1RappelIdentification.immatriculationRM) {
	var secteur1 =  "ARTISANAL";
} else { 
	var secteur1 =  "COMMERCIAL";
}

log.info("algo is {}", algo);
log.info("secteur1 is {}", secteur1);
log.info("typePersonne is {}", typePersonne);
log.info("formJuridique is {}", formJuridique);
log.info("optionCMACCI is {}", optionCMACCI);
log.info("codeCommune is {}", codeCommune);
//--------------- Determing input parameters to Directory algorithm --------------//

//----------------------- Checking a payment is mandatory based on target authorities  --------------//
var args = {
    "secteur1" : secteur1,
	"typePersonne" : typePersonne,
	"formJuridique" : formJuridique,
	"optionCMACCI" : optionCMACCI,
	"codeCommune" : codeCommune
};

var algoResponse = nash.service.request('${directory.baseUrl}/v1/algo/{code}/execute', algo) //
        .connectionTimeout(10000) //
        .receiveTimeout(10000)
        .accept('application/json') //
        .post(args);

var listAuthoritiesAsJSON = JSON.parse(algoResponse.asString());
log.info("Authorities list : {}", listAuthoritiesAsJSON.listAuthorities);

for (var i = 0; i < listAuthoritiesAsJSON.listAuthorities.length; i++) {	
	/*
	if (listDistinctAuthorities.indexOf(listAuthoritiesAsJSON.listAuthorities[i]['authority'])== -1) {
		listDistinctAuthorities.push(listAuthoritiesAsJSON.listAuthorities[i]['authority']);
	}
	*/
	
	var authorityId = listAuthoritiesAsJSON.listAuthorities[i]['authority'];
	var roleAuthority = listAuthoritiesAsJSON.listAuthorities[i]['role'];
	if (Object.keys(listDistinctAuthorities).indexOf(authorityId) == -1) {
		listDistinctAuthorities[authorityId] = [ roleAuthority ];
	} else {
		var roles = listDistinctAuthorities[authorityId];
		roles.push(roleAuthority);
		listDistinctAuthorities[authorityId] = roles;
	}
}


var computeDestinataire = nash.instance.load('computeDestinataire.xml');
computeDestinataire.bind('context.payment', {
	'mandatory' : mandatory
});

Object.keys(listDistinctAuthorities).forEach(function (authorityId) {
	var authority = getAuthority(authorityId);
	var details = !authority.details ? null : authority.details;
	var authorityLabel = !authority.label ? null :authority.label;
	var codeEdi = !details.ediCode ? null : details.ediCode;
	var reseau = codeEdi.charAt(0);
			
	//-->Bind the authority list
	authorityDetails.push({
		'code' : authorityId,
		'label' : authorityLabel,
		'ediCode' : codeEdi,
		'role' : listDistinctAuthorities[authorityId].join('+')
	});
	
	if (mandatory && reseau == 'G') {		
		var defaultRate = getRate('IRCS032');
		var defaultRateLabel = !defaultRate.label ? null : defaultRate.label;
		var defaultRatePrice = !defaultRate.details.prix ? null : defaultRate.details.prix;
		
		paymentAuthorityDetails.push({
			'code' : authorityId,
			'label' : authorityLabel,
			'ediCode' : codeEdi,
			'rate' : {
				'code' : 'IRCS032',
				'label' : defaultRateLabel,
				'price' : defaultRatePrice
			}
		});
	
		//-->We need to pay for only one authority : GREFFE
		//-->We apply 2 rates : IRCS032 (by default) and IRCS033 (multiplied by the number of closed establissement from step 1)
		var totalAutreEtablissement = $m4Agr.cadre2AdresseEtablissementGroup.cadreAdresseEtablissement.autreEtablissement.size();
		if (totalAutreEtablissement > 0) {
			var additionnalRate = getRate('IRCS033');
			var additionnalRateLabel = !additionnalRate.label ? null : additionnalRate.label;
			var additionnalRatePrice = !additionnalRate.details.prix ? null : additionnalRate.details.prix;
		
			var totalAdditionnalRatePrice = (null != additionnalRatePrice) ? '' + (parseFloat(additionnalRatePrice) * totalAutreEtablissement) : null;
			paymentAuthorityDetails.push({
				'code' : authorityId,
				'label' : authorityLabel,
				'ediCode' : codeEdi,
				'rate' : {
					'code' : 'IRCS033',
					'label' : additionnalRateLabel,
					'price' : totalAdditionnalRatePrice
				}
			});
		}
		//<--
	}
});

if (mandatory) {
	//-->Bind the payment details
	computeDestinataire.bind('context.payment', {
		'authority' : paymentAuthorityDetails
	});
}

//-->Bind the authorities list
computeDestinataire.bind('context.authorities', {
	'authority' : authorityDetails
});
//----------------------- Checking a payment is mandatory --------------//

