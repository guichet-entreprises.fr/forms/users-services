log.info('Règlement des frais de dossiers gratuits');

return spec.create({
	id : "paymentReslt",
	label : "Règlement des frais de dossiers gratuits",
	groups : [ spec.createGroup({
		id : 'result',
		label : 'Finalisation de votre dossier',
		description : "Vous pouvez finaliser votre dossier en cliquant sur le bouton 'Etape suivante'.",
		data : []
	}) ]
});	