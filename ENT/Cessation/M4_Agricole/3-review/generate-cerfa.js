//--------------- FUNCTIONS -------------------//
function pad(s) { return (s < 10) ? '0' + s : s; }

function parseDate(dateField, separator) {
	if (null == dateField) {
		return '';
	}
	
	if (null == separator) {
		separator = '';
	}
	
    var dateTmp = new Date(parseInt(dateField.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
	date = date.concat(separator);
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(separator);
	date = date.concat(dateTmp.getFullYear().toString());
	
    return date;
}

function pushPjPreview(fld) { fld.forEach(function (elm) { pjUser.push(elm); metas.push({'name':'document', 'value': '/'+elm.getAbsolutePath()}); }); }

function updateMetas(metas) {	
	//Remove old metas before insert
	var metasToDelete = [];
	var recordMetas = nash.record.meta() != null ? nash.record.meta().metas : null;
	var recordMetasSize = recordMetas != null ? recordMetas.size() : 0;

	for (var i = 0; i < recordMetasSize; i++) {
		if (recordMetas.get(i).name == 'document') {
			metasToDelete.push({'name':'document', 'value': recordMetas.get(i).value});
		}
	}
	nash.record.removeMeta(metasToDelete);

	// Insert new metas
	nash.record.meta(metas);
}

function getFieldValue(field) { return null == field ? '' : field; }

//--------------- Variables init -------------------//
var pdfFields = {};
var pjUser = [];
var metas = [];

//--------------- Rappel d'identification -------------------//
var identite = $m4Agr.cadre1Identite.cadre1RappelIdentification;
pdfFields['entreprise_siren']                          = identite.siren.split(' ').join('');
pdfFields['entreprise_formeJuridique']                 = identite.entrepriseFormeJuridique;
pdfFields['entreprise_denomination']                   = identite.entrepriseDenomination;

pdfFields['immatRCS']                                  = true;
pdfFields['immatRCSGreffe']                            = identite.immatRCSGreffe;
pdfFields['immatRM']                                   = identite.immatriculationRM ? true : false;
pdfFields['immatRMCMA']                                = getFieldValue(identite.immatRMCMA);
pdfFields['immatRMDept']                               = identite.immatriculationRM ? identite.immatRMCMA : '';
pdfFields['immatRMDeptNum']                            = identite.immatriculationRM ? identite.immatRMCMA.getId() : '';
pdfFields['activiteViticole_oui']                      = identite.activiteViticole;
pdfFields['activiteViticole_non']                      = identite.activiteViticole ? false : true;
pdfFields['activiteElevage_oui']                       = identite.activiteElevage;
pdfFields['activiteElevage_non']                       = identite.activiteElevage ? false : true;
pdfFields['numeroExploitation']                        = getFieldValue(identite.numeroExploitation);
pdfFields['numeroDetenteur']                           = getFieldValue(identite.numeroElevage);
pdfFields['entreprise_1erEtablissement']               = false;
pdfFields['entreprise_siege']                          = true;

var cadreAdresseEtablissement = $m4Agr.cadre2AdresseEtablissementGroup.cadreAdresseEtablissement;
var adresseEtablissementPrincipal = cadreAdresseEtablissement.adresseEtablissementPrincipal;
var adresseEtablissementPrincipalVoie = getFieldValue(adresseEtablissementPrincipal.numeroVoie) + //
														' ' + getFieldValue(adresseEtablissementPrincipal.indiceVoie) + //
														' ' + getFieldValue(adresseEtablissementPrincipal.typeVoie) + //
														' ' + getFieldValue(adresseEtablissementPrincipal.complementVoie) + //
														' ' + getFieldValue(adresseEtablissementPrincipal.distributionSpecialeVoie) //
														;
													
pdfFields['entreprise_adresseEntreprisePM_voie']       = adresseEtablissementPrincipalVoie;
pdfFields['entreprise_adresseEntreprisePM_codePostal'] = adresseEtablissementPrincipal.codePostal;
pdfFields['entreprise_adresseEntreprisePM_commune']    = adresseEtablissementPrincipal.commune;

//--------------- Déclaration relative à la personne -------------------//
var infoCessation = $m4Agr.cadre3InformationsGroup.cadre3Informations;
pdfFields['dateCessation']                             = parseDate(infoCessation.dateCessation);
pdfFields['dateCessationOperation']                    = parseDate(infoCessation.dateCessationOperation);
pdfFields['presenceSalarie_oui']                       = infoCessation.presenceSalaries ? true : false;
pdfFields['presenceSalarie_non']                       = !infoCessation.presenceSalaries ? true : false;

['clotureLiquiation', 'transfertPatrimoine', 'fusion', 'scission', 'autre'].forEach(function (key) {
	pdfFields['cessation_' + key]                      = Value('id').of(infoCessation.motifCessation).eq(key) ? true : false;
});

//--------------- Déclaration relative à la fermeture d'établissements -------------------//
if ( $m4Agr.cadre2AdresseEtablissementGroup.cadreAdresseEtablissement.autreEtablissementFerme ) {
	var autreEtablissements = $m4Agr.cadre2AdresseEtablissementGroup.cadreAdresseEtablissement.autreEtablissement;
	log.info("Nombre total d'établissements fermés : {}", autreEtablissements.size());
	for (var idx = 0; idx < autreEtablissements.size(); idx++) {
		var etablissement = autreEtablissements.get(idx);
		//-->
		
		pdfFields['etablissement_adresseVoie1[' + idx + ']']         =   getFieldValue(etablissement.etablissementAdresseVoie) + //
																		' ' + getFieldValue(etablissement.indiceVoie) + //
																		' ' + getFieldValue(etablissement.typeVoie) + //
																		' ' + getFieldValue(etablissement.nomVoie) //
																		;
		pdfFields['etablissement_adresseVoie2[' + idx + ']']         =   getFieldValue(etablissement.complementVoie) + //
																		' ' + getFieldValue(etablissement.distributionSpecialeVoie) //
																		;
																				
		pdfFields['etablissement_adresseCP[' + idx + ']']               	 = etablissement.codePostal;
		pdfFields['etablissement_adresseCommune[' + idx + ']']               = etablissement.commune;
		
		['destinationSuppression', 'destinationVente', 'destinationAutre'].forEach(function (key) {
			pdfFields['etablissement_' + key + '[' + idx + ']']               = Value('id').of(etablissement.destination).eq(key) ? true : false;
		});
			
		if ( Value('id').of(etablissement.destination).eq('destinationAutre') ) {				
			pdfFields['etablissement_destinationAutreLibelle[' + idx + ']']  = etablissement.autreLibelle;
		}
		
		pdfFields['etablissement_cessationEmploi[' + idx + ']']              = parseDate(etablissement.dateCessationEmploi);
	}
}

//--------------- Renseignements complémentaires -------------------//
var renseignementsComp = $m4Agr.cadre8RensCompGroup.cadre8RensComp;
if ( Value('id').of(renseignementsComp.adresseCorrespond).eq('autre') ) {
	var adresseCorrespondance = renseignementsComp.adresseCorrespondance;
	pdfFields['adresseCorrespondance_voie1']               = getFieldValue(adresseCorrespondance.nomPrenomDenomination) + //
															' ' + getFieldValue(adresseCorrespondance.numeroVoie) + //
															' ' + getFieldValue(adresseCorrespondance.indiceVoie) + //
															' ' + getFieldValue(adresseCorrespondance.typeVoie) + //
															' ' + getFieldValue(adresseCorrespondance.nomVoie)
															;
	
	pdfFields['adresseCorrespondance_voie2']               = getFieldValue(adresseCorrespondance.complementVoie) + ' ' + getFieldValue(adresseCorrespondance.distributionSpecialeVoie);
	
	pdfFields['adresseCorrespondance_codePostal']          = adresseCorrespondance.codePostal;
	pdfFields['adresseCorrespondance_commune']             = adresseCorrespondance.commune;
} else if ( Value('id').of(renseignementsComp.adresseCorrespond).eq('prof') ) {

	pdfFields['adresseCorrespondance_declaree']            = true;
	pdfFields['adresseCorrespondance_declareeNumero']     = '2';
}

pdfFields['formalite_observations']                    = getFieldValue(renseignementsComp.observations);
pdfFields['formalite_telephone1']                      = getFieldValue(renseignementsComp.infosSup.telephone1);
pdfFields['formalite_telephone2']                      = getFieldValue(renseignementsComp.infosSup.telephone2);

var faxCourriel = getFieldValue(renseignementsComp.infosSup.faxCourriel);
pdfFields['formalite_fax_courriel']                    =  (null != faxCourriel && ''!= faxCourriel) ? faxCourriel : getFieldValue(renseignementsComp.infosSup.telecopie);

var signature = $m4Agr.cadre9SignatureGroup.cadre9Signature;
['representantLegal', 'mandataire', 'autrePersonne'].forEach(function (key) {
	pdfFields['formalite_' + key]                     = Value('id').of(signature.soussigne).eq(key) ? true : false;
});
pdfFields['formalite_signatureDate']                   = parseDate(signature.date, '/');
pdfFields['formalite_signatureLieu']                   = signature.lieu;


var adresseMandataire = signature.adresseMandataire;
pdfFields['formalite_signataireNom']                = getFieldValue(adresseMandataire.nomPrenomDenomination);	
pdfFields['formalite_signataireAdresse_voie']       = getFieldValue(adresseMandataire.numeroVoie) + //
													' ' + getFieldValue(adresseMandataire.indiceVoie) + //
													' ' + getFieldValue(adresseMandataire.typeVoie) + //
													' ' + getFieldValue(adresseMandataire.nomVoie) + //
													' ' + getFieldValue(adresseMandataire.complementVoie) + //
													' ' + getFieldValue(adresseMandataire.distributionSpecialeVoie)
													;
//-->Code postal nécessaire ??
pdfFields['formalite_signataireAdresse_commune']       = adresseMandataire.commune;

if ( Value('id').of($m4Agr.cadre9SignatureGroup.cadre9Signature.soussigne).eq('representantLegal') ) {
    pushPjPreview($preprocess.preprocess.pjIDDeclarantSignataire);
} else {
    pushPjPreview($preprocess.preprocess.pjIDMandataireSignataire);
}

//--------------- CERFA GENERATION -------------------//
//-->L'ordre des pages est le suivant : M4 avec / M4 sans / M' avec / M' sans
//-->Si au moins 2 établissements fermés ou bien 2 personnes affiliées alors ajout des cerfa dit "prime" avec et sans volet
var generateCerfaPrime = 
	( $m4Agr.cadre2AdresseEtablissementGroup.cadreAdresseEtablissement.autreEtablissementFerme && $m4Agr.cadre2AdresseEtablissementGroup.cadreAdresseEtablissement.autreEtablissement.size() > 2) ||
	( $m4Agr.cadre2AdresseEtablissementGroup.cadreAdresseEtablissement.autreEtablissement.size() > 2) ? true : false;
	
//-->Cerfa générés pour tous les cas
var cerfaAvecVolet = nash.doc.load('models/cerfa_11929-03_M4agricole_avec_volet_social.pdf').apply(pdfFields);
var cerfaSansVolet = nash.doc.load('models/cerfa_11929-03_M4agricole_sans_volet_social.pdf').apply(pdfFields);
cerfaAvecVolet.append(cerfaSansVolet.save('cerfaSansVolet.pdf'));

if (generateCerfaPrime) {
	log.debug('Génération des CERFA Prime : avec et sans volet social');
	cerfaAvecVolet.append(nash.doc.load('models/cerfa_11681-02_Mprime_avec_volet_social.pdf').apply(pdfFields).save('cerfaPrimeAvecVolet.pdf'));
	cerfaAvecVolet.append(nash.doc.load('models/cerfa_11681-02_Mprime_sans_volet_social.pdf').apply(pdfFields).save('cerfaPrimeSansVolet.pdf'));
}

var finalDoc = cerfaAvecVolet.save('Cessation_M4.pdf');

//--------------- METADATA DOCUMENT -------------------//
updateMetas(metas);

var data = [ spec.createData({
    id : 'record',
    label : "Déclaration de radiation d'une personne morale ayant une activité principale agricole",
    description : 'Voici le formulaire obtenu à partir des données saisies.',
    type : 'FileReadOnly',
    value : [ finalDoc ]
}),  spec.createData({
    id : 'attachments',
    label : 'Pièces jointes',
    description : 'Pièces jointes ajoutées au formulaire.',
    type : 'FileReadOnly',
    value : pjUser
}) ];


var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : "Déclaration de radiation d'une personne morale ayant une activité principale agricole",
    groups : groups
});
