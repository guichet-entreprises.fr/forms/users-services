var formFields = {};
var cerfaFields = {};
function pad(s) { return (s < 10) ? '0' + s : s; }

// Cadre 1 - Rappel d'identification

var identite = $p4Agricole.cadre1Identite.cadre1RappelIdentification;
var infoCessation = $p4Agricole.cadre3InformationsGroup.cadre3Informations;

formFields['siren']                                                  = identite.siren.split(' ').join('');
formFields['viticoleOui']                                            = identite.activiteViticole ? true : false;
formFields['viticoleNon']                                            = identite.activiteViticole ? false : true;
formFields['elevageOui']                                             = identite.activiteElevage ? true : false;
formFields['elevageNon']                                             = identite.activiteElevage ? false : true;
formFields['numeroExploitation']                                     = identite.numeroExploitation != null ? identite.numeroExploitation : '';
formFields['numeroDetenteur']                                        = identite.numeroElevage != null ? identite.numeroElevage : '';

// Cadre 2

formFields['personneLiee_personnePhysique_nomNaissance']             = identite.personneLieePersonnePhysiqueNomNaissance;
formFields['personneLiee_personnePhysique_nomUsage']                 = identite.personneLieePersonnePhysiqueNomUsage;
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFields['personneLiee_personnePhysique_prenom1']                       = prenoms.toString();
if (identite.personneLieePersonnePhysiqueDateNaissance !== null) {
    var dateTmp = new Date(parseInt(identite.personneLieePersonnePhysiqueDateNaissance.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['personneLiee_personnePhysique_dateNaissance']          = date;
}
formFields['personneLiee_personnePhysique_lieuNaissanceDepartement'] = identite.personneLieePersonnePhysiqueLieuNaissanceDepartement != null ? identite.personneLieePersonnePhysiqueLieuNaissanceDepartement.getId() : '';
formFields['personneLiee_personnePhysique_lieuNaissanceCommune']     = identite.personneLieePersonnePhysiqueLieuNaissanceCommune != null ? (identite.personneLieePersonnePhysiqueLieuNaissanceCommune + ' / ' + identite.personneLieePersonnePhysiqueLieuNaissancePays) : (identite.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + identite.personneLieePersonnePhysiqueLieuNaissancePays);


// Cadre 3

formFields['estEIRLOuiNon']                                          = infoCessation.estEIRLOuiNon ? true : false;


// Cadre 4

if (infoCessation.dateCessation !== null) {
    var dateTmp = new Date(parseInt(infoCessation.dateCessation.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['dateCessation']          = date;
}
formFields['cessationSuiteDeces']                                    = infoCessation.cessationSuiteDeces ? true : false;


// Cadre 5 Declaration relative à la fermeture d'établissement

var etablissement = $p4Agricole.cadre2AdresseEtablissementGroup.cadreAdresseEtablissement ;
var adressePro = $p4Agricole.cadre2AdresseEtablissementGroup.cadreAdresseEtablissement.adresseEtablissementPrincipal ;

formFields['etablissementPrincipal_voie']                            = (adressePro.etablissementAdresseNumeroVoie != null ? adressePro.etablissementAdresseNumeroVoie : '') 
																	+ ' ' + (adressePro.etablissementAdresseIndiceVoie != null ? adressePro.etablissementAdresseIndiceVoie : '')
																	+ ' ' + (adressePro.etablissementAdresseTypeVoie != null ? adressePro.etablissementAdresseTypeVoie : '')
																	+ ' ' + adressePro.etablissementAdresseNomVoie
																	+ ' ' + (adressePro.etablissementAdresseComplementVoie != null ? adressePro.etablissementAdresseComplementVoie : '')
																	+ ' ' + (adressePro.etablissementAdresseDistriutionSpecialeVoie != null ? adressePro.etablissementAdresseDistriutionSpecialeVoie : '');
formFields['etablissementPrincipal_codePostal']                      = adressePro.etablissementAdresseCodePostal;
formFields['etablissementPrincipal_commune']                         = adressePro.etablissementAdresseCommune;
formFields['etablissementPrincipal_destinationTransmissionConjoint'] = Value('id').of(etablissement.destinationEtablissement).eq('etablissementPrincipalDestinationTransmission') ? true : false;
formFields['etablissementPrincipal_destinationCession']              = Value('id').of(etablissement.destinationEtablissement).eq('etablissementPrincipalDestinationCession') ? true : false;
formFields['etablissementPrincipal_destinationAutre']                = Value('id').of(etablissement.destinationEtablissement).eq('etablissementPrincipalDestinationAutre') ? true : false;
formFields['etablissementPrincipal_autreLibelle']                    = Value('id').of(etablissement.destinationEtablissement).eq('etablissementPrincipalDestinationAutre') ? etablissement.etablissementPrincipalAutreLibelle : '';

// Cadres 5 bis

var etablissementAutre = $p4Agricole.cadre2AdresseEtablissementGroup.cadreAdresseAutreEtablissement;
var etablissementAutreAdresse = $p4Agricole.cadre2AdresseEtablissementGroup.cadreAdresseAutreEtablissement.adresseAutreEtablissement;

if (etablissement.autreEtablissementFerme) {
formFields['autreEtablissement_voie']                                = (etablissementAutreAdresse.autreEtablissementAdresseNumeroVoie != null ? etablissementAutreAdresse.autreEtablissementAdresseNumeroVoie : '') 
																	+ ' ' + (etablissementAutreAdresse.autreEtablissementAdresseIndiceVoie != null ? etablissementAutreAdresse.autreEtablissementAdresseIndiceVoie : '')
																	+ ' ' + (etablissementAutreAdresse.autreEtablissementAdresseTypeVoie != null ? etablissementAutreAdresse.autreEtablissementAdresseTypeVoie : '')
																	+ ' ' + etablissementAutreAdresse.autreEtablissementAdresseNomVoie
																	+ ' ' + (etablissementAutreAdresse.autreEtablissementAdresseComplementVoie != null ? etablissementAutreAdresse.autreEtablissementAdresseComplementVoie : '')
																	+ ' ' + (etablissementAutreAdresse.autreEtablissementAdresseDistriutionSpecialeVoie != null ? etablissementAutreAdresse.autreEtablissementAdresseDistriutionSpecialeVoie : '');
formFields['autreEtablissement_codePostal']                          = etablissementAutreAdresse.autreEtablissementAdresseCodePostal;
formFields['autreEtablissement_commune']                             = etablissementAutreAdresse.autreEtablissementAdresseCommune
formFields['autreEtablissement_destinationTransmissionConjoint']     = Value('id').of(etablissementAutre.destinationAutreEtablissement).eq('autreEtablissementDestinationTransmission') ? true : false;
formFields['autreEtablissement_destinationCession']                  = Value('id').of(etablissementAutre.destinationAutreEtablissement).eq('autreEtablissementDestinationCession') ? true : false;
formFields['autreEtablissement_destinationAutre']                    = Value('id').of(etablissementAutre.destinationAutreEtablissement).eq('autreEtablissementDestinationAutre') ? true : false;
formFields['autreEtablissement_autreLibelle']                        = Value('id').of(etablissementAutre.destinationAutreEtablissement).eq('autreEtablissementDestinationAutre') ? etablissementAutre.autreEtablissementAutreLibelle : '';
}

// Cadre 6 - Observations

var correspondance = $p4Agricole.cadre8RensCompGroup.cadre8RensComp;

formFields['observations']                                           = correspondance.formaliteObservations != null ? correspondance.formaliteObservations : '';

// Cadre 7 - Adresse de correspondance

formFields['correspondance_cadreCoche']                              = Value('id').of(correspondance.adresseCorrespond).eq('prof') ? true : false;
formFields['correspondance_cadreNumero']                             = Value('id').of(correspondance.adresseCorrespond).eq('prof') ? "5" : '';
formFields['correspondance_cocheAutre']                              = Value('id').of(correspondance.adresseCorrespond).eq('autre') ? true : false;
formFields['adresseCorrespondance_voie1']                            = Value('id').of(correspondance.adresseCorrespond).eq('autre') ? 
																	(correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance
																	+ ' ' + (correspondance.adresseCorrespondance.numeroRueAdresseCorrespondance != null ? correspondance.adresseCorrespondance.numeroRueAdresseCorrespondance : '') 
																	+ ' ' + (correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance : '')
																	+ ' ' + (correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance : '')
																	+ ' ' + correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance) : '';
formFields['adresseCorrespondance_voie2']                            = Value('id').of(correspondance.adresseCorrespond).eq('autre') ? 
																	((correspondance.adresseCorrespondance.rueComplementAdresseCorrespondance != null ? correspondance.adresseCorrespondance.rueComplementAdresseCorrespondance : '') 
																	+ ' ' + (correspondance.adresseCorrespondance.distriutionSpecialeVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.distriutionSpecialeVoieAdresseCorrespondance : '')) : '';
formFields['adresseCorrespondance_codePostal']                       = Value('id').of(correspondance.adresseCorrespond).eq('autre') ? (correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal) : '';
formFields['adresseCorrespondance_commune']                          = Value('id').of(correspondance.adresseCorrespond).eq('autre') ? (correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune) : '';
formFields['telephone1']                                             = correspondance.infosSup.formaliteTelephone1 != null ? correspondance.infosSup.formaliteTelephone1 : '';
formFields['telephone2']                                             = correspondance.infosSup.formaliteTelephone2;
formFields['fax']                                                    = correspondance.infosSup.formaliteCourriel != null ? correspondance.infosSup.formaliteCourriel : (correspondance.infosSup.telecopie != null ? correspondance.infosSup.telecopie : '');

// Cadre 10 - Signataire

var signataire = $p4Agricole.cadre9SignatureGroup.cadre9Signature;

formFields['signataireDeclarant']                                    = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFields['signataireMandataire']                                   = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? true : false;
formFields['mandataire_voie']    				                 	 = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? signataire.adresseMandataire.nomPrenomDenominationMandataire : '';
formFields['mandataire_complement']                                  = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? 
																	 ((signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																	 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																	 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																	 + ' ' + signataire.adresseMandataire.nomVoieMandataire
																	 + ' ' + (signataire.adresseMandataire.voieComplementMandataire != null ? signataire.adresseMandataire.voieComplementMandataire : '')
																	 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeMandataire : '')): '';
formFields['mandataire_commune']                                     = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? 
																	 (signataire.adresseMandataire.codePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire) : '';
formFields['signatureDate']                                          = signataire.formaliteSignatureDate;
formFields['signatureLieu']                                          = signataire.formaliteSignatureLieu;
formFields['estEIRL_oui']                                            = infoCessation.estEIRLOuiNon ? true : false;
formFields['estEIRL_non']                                            = infoCessation.estEIRLOuiNon ? false : true;
formFields['intercalaireNombre']                                     = "0";
formFields['signature']                                              = '';
formFields['nombreEIRL']                                             = infoCessation.estEIRLOuiNon ? "1" : "0";


// Intercalaire PEIRL CMB

//Cadre 1 - Déclaration ou modification d'affectation de patrimoine

formFields['formulaire_dependance_P0Agricole']                                              = false;
formFields['formulaire_dependance_P2Agricole']                                              = false;
formFields['formulaire_dependance_P4Agricole']                                              = true;
formFields['declaration_initiale']															= false;
formFields['declaration_modification']                                                      = true;


// Cadre 2 - Rappel d'identification

formFields['eirl_siren']                                                                    = identite.siren.split(' ').join('');
formFields['eirl_nomNaissance']                                                             = identite.personneLieePersonnePhysiqueNomNaissance;
formFields['eirl_nomUsage']                                                                 = identite.personneLieePersonnePhysiqueNomUsage;
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFields['eirl_prenom']                       = prenoms.toString();

// Cadre 4 - Rappel d'identification reltif à l'EIRL

var rappelIdentification = $p4Agricole.cadre3InformationsGroup.cadre3Informations.cadreAffectationPatrimoine.cadreRappelIdentificationEIRL;

formFields['eirl_rappelDenomination']                                                       = rappelIdentification.eirlRappelDenomination;
formFields['eirl_rappelAdresse']                                                            = Value('id').of($p4Agricole.cadre3InformationsGroup.cadre3Informations.cadreAffectationPatrimoine.cadreRappelIdentificationEIRL.eirlAdresse).eq('eirlAdresseEntreprise') ? 
																							((adressePro.etablissementAdresseNumeroVoie != null ? adressePro.etablissementAdresseNumeroVoie : '') 
																							+ ' ' + (adressePro.etablissementAdresseIndiceVoie != null ? adressePro.etablissementAdresseIndiceVoie : '')
																							+ ' ' + (adressePro.etablissementAdresseTypeVoie != null ? adressePro.etablissementAdresseTypeVoie : '')
																							+ ' ' + adressePro.etablissementAdresseNomVoie
																							+ ' ' + (adressePro.etablissementAdresseComplementVoie != null ? adressePro.etablissementAdresseComplementVoie : '')
																							+ ' ' + (adressePro.etablissementAdresseDistriutionSpecialeVoie != null ? adressePro.etablissementAdresseDistriutionSpecialeVoie : '')
																							+ ' ' +  adressePro.etablissementAdresseCodePostal
																							+ ' ' + adressePro.etablissementAdresseCommune)
																							: ((rappelIdentification.cadreEirlRappelAdresse.adresseNumeroVoieEIRL != null ? rappelIdentification.cadreEirlRappelAdresse.adresseNumeroVoieEIRL : '') 
																				            + ' ' + (rappelIdentification.cadreEirlRappelAdresse.adresseIndiceVoieEIRL != null ? rappelIdentification.cadreEirlRappelAdresse.adresseIndiceVoieEIRL : '')
																				            + ' ' + (rappelIdentification.cadreEirlRappelAdresse.adresseTypeVoieEIRL != null ? rappelIdentification.cadreEirlRappelAdresse.adresseTypeVoieEIRL : '')
																		                    + ' ' + rappelIdentification.cadreEirlRappelAdresse.adresseNomVoieEIRL
																							+ ' ' + (rappelIdentification.cadreEirlRappelAdresse.adresseComplementVoieEIRL != null ? rappelIdentification.cadreEirlRappelAdresse.adresseComplementVoieEIRL : '')
																							+ ' ' + (rappelIdentification.cadreEirlRappelAdresse.adresseDistriutionSpecialeVoieEIRL != null ? rappelIdentification.cadreEirlRappelAdresse.adresseDistriutionSpecialeVoieEIRL : '')
																							+ ' ' + rappelIdentification.cadreEirlRappelAdresse.adresseCodePostalEIRL
																							+ ' ' + rappelIdentification.cadreEirlRappelAdresse.adresseCommuneEIRL);
formFields['eirl_rappelLieuImmatriculation']                                                = rappelIdentification.eirlRappelLieuImmatriculation;

// Cadre 5 - Déclaration de cessation

var finDAP = $p4Agricole.cadre3InformationsGroup.cadre3Informations.cadreAffectationPatrimoine.finEIRLDeclaration;
																							
if (infoCessation.dateCessation !== null and infoCessation.estEIRLOuiNon) {
    var dateTmp = new Date(parseInt(infoCessation.dateCessation.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateFinEIRL']          = dateModif;
}
formFields['finEIRL_renonciationSansPoursuite']                                             = Value('id').of(finDAP.motifFinEIRL).contains('FinEIRLRenonciationSansPoursuite') ? true : false;
formFields['finEIRL_cessionPP']                                                             = Value('id').of(finDAP.motifFinEIRL).contains('FinEIRLCessionPP') ? true : false;
formFields['finEIRL_cessionPM']                                                             = Value('id').of(finDAP.motifFinEIRL).contains('FinEIRLCessionPM') ? true : false;	



/*
 * Création du dossier avec volet social : ajout du cerfa avec volet social
 */
 
var cerfaDoc1 = nash.doc //
	.load('models/cerfa_11936-03_P4_agricole.pdf') //
	.apply(formFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));



/*
 * Ajout de l'intercalaire PEIRL ME avec option fiscale
 */
if ($p4Agricole.cadre3InformationsGroup.cadre3Informations.estEIRLOuiNon)
{
	var peirlMEDoc1 = nash.doc //
		.load('models/cerfa_14216-06_peirl_agricole_avec_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(peirlMEDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire PEIRL ME sans option fiscale
 */
if ($p4Agricole.cadre3InformationsGroup.cadre3Informations.estEIRLOuiNon)
{
	var peirlMEDoc2 = nash.doc //
		.load('models/cerfa_14216-06_peirl_agricole_sans_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(peirlMEDoc2.save('cerfa.pdf'));
}

/*
 * Ajout des PJs
 */

// PJ Déclarant

var pj=$p4Agricole.cadre9SignatureGroup.cadre9Signature.soussigne;

var pjUser = [];

var metas = [];

function pushPjPreview(fld) {
	fld.forEach(function (elm) {
        pjUser.push(elm);
        metas.push({'name':'document', 'value': '/'+elm.getAbsolutePath()});
    });
}

if(Value('id').of(pj).contains('FormaliteSignataireQualiteDeclarant')) {
    _log.info("pjIDDeclarantSignataire is  {}", $attachmentPreprocess.attachmentPreprocess.pjIDDeclarantSignataire);
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantSignataire);
}

// PJ Mandataire

if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire') or Value('id').of(pj).contains('FormaliteSignataireQualiteAutre')) {
    _log.info("pjIDMandataireSignataire is  {}", $attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire);
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire);
}
	
if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
    _log.info("pjPouvoir is  {}", $attachmentPreprocess.attachmentPreprocess.pjPouvoir);
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPouvoir);
}
	
/*
 * Enregistrement du fichier (en mémoire)
 */

var finalDoc = cerfaDoc1.save('P4_Agricole_Cessation.pdf');

//Remove old metas before insert
var metasToDelete = [];
var recordMetas = nash.record.meta() != null ? nash.record.meta().metas : null;
var recordMetasSize = recordMetas != null ? recordMetas.size() : 0;

for (var i = 0; i < recordMetasSize; i++) {
    if (recordMetas.get(i).name == 'document') {
        metasToDelete.push({'name':'document', 'value': recordMetas.get(i).value});
    }
}
nash.record.removeMeta(metasToDelete);

// Insert new metas
nash.record.meta(metas);

var data = [ spec.createData({
    id : 'record',
    label : 'Déclaration de cessation d\'activité agricole',
    description : 'Voici le formulaire obtenu à partir des données saisies.',
    type : 'FileReadOnly',
    value : [ finalDoc ]
}),  spec.createData({
    id : 'attachments',
    label : 'Pièces jointes',
    description : 'Pièces jointes ajoutées au formulaire.',
    type : 'FileReadOnly',
    value : pjUser
}) ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Déclaration de cessation d\'activité agricole',
    groups : groups
});