// PJ Déclarant

var userDeclarant;
if ($p4cmb.cadre1Identite.cadre1RappelIdentification.personneLieePersonnePhysiqueNomUsage != null) {
    var userDeclarant = $p4cmb.cadre1Identite.cadre1RappelIdentification.personneLieePersonnePhysiqueNomUsage + '  ' + $p4cmb.cadre1Identite.cadre1RappelIdentification.personneLieePersonnePhysiquePrenom1[0] ;
} else { 
    var userDeclarant = $p4cmb.cadre1Identite.cadre1RappelIdentification.personneLieePersonnePhysiqueNomNaissance + ' '+ $p4cmb.cadre1Identite.cadre1RappelIdentification.personneLieePersonnePhysiquePrenom1[0] ;
}

var pj=$p4cmb.cadre9SignatureGroup.cadre9Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteDeclarant')) {
    attachment('pjIDDeclarantSignataire', 'pjIDDeclarantSignataire', { label: userDeclarant, mandatory:"true"});
}

// PJ Mandataire

var pj=$p4cmb.cadre9SignatureGroup.cadre9Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
    attachment('pjPouvoir', 'pjPouvoir', { mandatory:"true"});
}

var userMandataire=$p4cmb.cadre9SignatureGroup.cadre9Signature.adresseMandataire

var pj=$p4cmb.cadre9SignatureGroup.cadre9Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire') or Value('id').of(pj).contains('FormaliteSignataireQualiteAutre')) {
    attachment('pjIDMandataireSignataire', 'pjIDMandataireSignataire', {label: userMandataire.nomPrenomDenominationMandataire, mandatory:"true"});
}