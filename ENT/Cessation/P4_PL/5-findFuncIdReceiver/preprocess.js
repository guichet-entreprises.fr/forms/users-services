//prepare info to send 

var algo = _INPUT_.view.algo;

var secteur1 = _INPUT_.view.secteur1;

var typePersonne = _INPUT_.view.typePersonne;

var formJuridique = _INPUT_.view.formJuridique;

var optionCMACCI = _INPUT_.view.optionCMACCI;

var codeCommune = _INPUT_.view.codeCommune;

var attachement = _INPUT_.view.attachement;

var args = {
    "secteur1" : secteur1,
	"typePersonne" : typePersonne,
	"formJuridique" : formJuridique,
	"optionCMACCI":optionCMACCI,
	"codeCommune": codeCommune
};


_log.info("serviceParams  {}",JSON.stringify(args));

// call directory service to find functional id of authority 
var response = nash.service.request('${directory.baseUrl}/v1/algo/{code}/execute',algo) //
        .connectionTimeout(10000) //
        .receiveTimeout(10000)
        .accept('text') //
        .post(args);

		
_log.info("response  {}",response);

// Result value of functional id 
var codeAuthority = response.asString();


_log.info("codeAuthority  {}",codeAuthority);

/* 
//Début Calcule deuxième destinataire
var typePersonne2 = "PM";
var args1 = {
    "secteur1" : secteur1,
	"typePersonne" : typePersonne2,
	"formJuridique" : formJuridique,
	"optionCMACCI":optionCMACCI,
	"codeCommune": codeCommune
};
*/

var response1 = nash.service.request('${directory.baseUrl}/v1/authority/{entityId}', codeCommune) //
	.accept('json') //
	.get();
	
var receiverInfo = response1.asObject();

logger.info("receiverInfo is  {}", receiverInfo);
logger.info("type of details of receiveInfo is  {}",typeof receiverInfo.details);


var details = !receiverInfo.details ? null : receiverInfo.details;

// convert map to json
var input = {}
details.forEach(function(key, value){
        input[key] = value
});

args.details = JSON.stringify(input);
logger.info("args passed to trouver autorite code {}",args);
args.secteurCfe = "GREFFE";

if ($p4pl.cadre3InformationsGroup.cadre3Informations.estEIRLOuiNon) {
// call directory service to find codeAuthority
var response2 = nash.service.request('${directory.baseUrl}/v1/algo/{code}/execute',"trouver AutoriteCode") //
        .accept('text') //
        .post(args);
// Result value of functional id 
var codeAuthority2 = response2.asString();
}



//Fin Calcule deuxième destinataire

// create  data.xml  with functional id of authority

return spec.create({
	id: 'findDest',
	label: "Etablissement de l'autorité compétente",
	groups: [spec.createGroup({
			id: 'functionalId',
			label: "Identification  de l'autorité compétente",
			data: [spec.createData({
					id: 'funcId',
					label: "Code de l'autorité compétente",
					description: 'Code interne',
					type: 'String',
					value: codeAuthority
				}),
				// Ajout du code de la 2ème autorité
				spec.createData({
					id: 'funcId2',
					label: "Code de la deuxième autorité compétente",
					description: 'Code interne',
					type: 'String',
					value: codeAuthority2
				}), // fin de l'ajout
				spec.createData({
					id: 'attachment',
					label: "Le chemin de la pièce jointe",
					type: 'String',
					value: attachement
				}),
			]
		})]
});