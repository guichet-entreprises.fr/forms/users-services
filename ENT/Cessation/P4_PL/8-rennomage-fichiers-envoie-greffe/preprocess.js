// init

referentials = {
		'C': {
			'cerfa' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PMGUN'
			},
			'xmltc' :{
				'typepj' : 'TXMLTC',
				'suffixe' : 'PMGUN'
			} ,
			'xmlregent' :{
				'typepj' : 'TFORMA',
				'suffixe' : 'PMGUN'
			} ,
			'others' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PMGUN'
			} ,
		},

		'G': {
			'cerfa' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PGUEN'
			},
			'xmltc' :{
				'typepj' : 'TXMLTC',
				'suffixe' : 'PGUEN'
			} ,
			'xmlregent' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PGUEN'
			} ,
			'others' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PGUEN'
			} ,
		},
		'X': {
			'cerfa' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PMGUN'
			},
			'xmltc' :{
				'typepj' : 'TXMLTC',
				'suffixe' : 'PMGUN'
			} ,
			'xmlregent' :{
				'typepj' : 'TFORMA',
				'suffixe' : 'PMGUN'
			} ,
			'others' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PMGUN'
			} ,
		},
		'U': {
			'cerfa' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PMGUN'
			},
			'xmltc' :{
				'typepj' : 'TXMLTC',
				'suffixe' : 'PMGUN'
			} ,
			'xmlregent' :{
				'typepj' : 'TFORMA',
				'suffixe' : 'PMGUN'
			} ,
			'others' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PMGUN'
			} ,
		},
		'M': {
			'cerfa' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PMGUN'
			},
			'xmltc' :{
				'typepj' : 'TXMLTC',
				'suffixe' : 'PMGUN'
			} ,
			'xmlregent' :{
				'typepj' : 'TFORMA',
				'suffixe' : 'PMGUN'
			} ,
			'others' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PMGUN'
			} ,
		},
		
	};

var cpPj=2;
//--> get file name of PJ : 
function getFileName(codeEdi, numLiasse, numOfpj, extension, fileType) {
	// --->
	_log.info("getFileName()");
	_log.info("Params[{}] [{}] [{}] [{}] [{}]", codeEdi, numLiasse, numOfpj,extension, fileType);

	var indiceOfpj;
	if (numOfpj > 9) {
		indiceOfpj = "0" + numOfpj;
	} else {
		indiceOfpj = "00" + numOfpj;
	}
	var network = codeEdi.charAt(0);
	var typepj = referentials[network][fileType]['typepj'];
	var suffixe = referentials[network][fileType]['suffixe'];
	var dateTemp = new Date();
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0"+ (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var dateFormatted = year.toString() + month.toString() + day.toString();
	// formatage de l'heure de génération
	var timeFormatted = dateTemp.toTimeString().substr(0, 8).replace(new RegExp(':', 'g'), '');

	var numberOfRecipients;
	switch (network) {
	
	case "G":
		numberOfRecipients = "1000A1001";
		break;
		
	case "C":
		numberOfRecipients = "0016A1002";
		break;
		
	case "M":
		numberOfRecipients = "0016A1003";
		break;
	case "X":
		numberOfRecipients = "0016A1004";
		break;
		
	case "U":
		numberOfRecipients = "0016A1005";
		break;
	}

	var label = 'C'+numberOfRecipients +'L'+ numLiasse.substr(6) + 'D' + dateFormatted + 'H'+ timeFormatted + typepj + 'S' + indiceOfpj + suffixe + "."+ extension.toLowerCase();
	_log.info("File name: {} ",label);
	
	return label;
}

//Variables that should be setted by the formality redactor
var authorityType = "CFE"; // (CFE, TDR)
var authorityId1 = _INPUT_.dataGroup.codeEDI; // (code EDI)
var authorityLabel = _INPUT_.dataGroup.partnerLabel; // (label partenaire)
var destFuncId = _INPUT_.dataGroup.destFuncId; // (code interne de l'autorité)
var authorityType2 = "TDR"; // (CFE, TDR)
var authorityId2 = _INPUT_.dataGroup.codeEDI2; // (code EDI)
var authorityLabel2 = _INPUT_.dataGroup.partnerLabel2; // (label partenaire)
var destFuncId2 = _INPUT_.dataGroup.destFuncId2; // (code interne de l'autorité)
var listAttachments = [] ;

//path of cerfa 
var cheminLiasseCfePdf = 	_INPUT_.dataGroup.emailPj ;

//Extraction du numéro de liasse du xml regent
var result = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02');
var parsedResult = JSON.parse(result);
var numeroDeLiasse1 = parsedResult['C02'];

//--> get new name of regent

var xmlRegent1Formatted = getFileName(authorityId1,numeroDeLiasse1,2,"xml","xmlregent");
//--> get new name of cerfa
var pdfCfeFormatted = getFileName(authorityId1,numeroDeLiasse1,3,"pdf","cerfa") ;
//--> save pdf and regent with the new names
nash.record.saveFile(xmlRegent1Formatted,nash.util.resourceFromPath('/XML_REGENT.xml').getContent());
nash.record.saveFile(pdfCfeFormatted,nash.util.resourceFromPath(cheminLiasseCfePdf).getContent() );

//--> if we have second authority we do the same think as before 
if (authorityId2) {
	//Extraction du numéro de liasse du xml regent
	var result = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02');
	var parsedResult = JSON.parse(result);
	var numeroDeLiasse2 = parsedResult['C02'];
	
	//--> get new name of regent
	var xmlRegent2Formatted = getFileName(authorityId2,numeroDeLiasse2,2,"xml","xmlregent");
	//--> get new name of cerfa
	var pdf2CfeFormatted = getFileName(authorityId2,numeroDeLiasse2,3,"pdf","cerfa") ;
	//--> save pdf and regent with the new names
	nash.record.saveFile(xmlRegent2Formatted,nash.util.resourceFromPath('/XML_REGENT2.xml').getContent());
	nash.record.saveFile(pdf2CfeFormatted,nash.util.resourceFromPath(cheminLiasseCfePdf).getContent() );
		
}



var listData = []; 
//ajout du xml rgeent 1
var dataElement1 = 	spec.createData({
						'id': "XML_REGENT_CFE",
						'label': 'XML REGENT CFE :',
						'type': 'String',
						'mandatory': true,
						'value':xmlRegent1Formatted
						})	;
listData.push(dataElement1);
var attachementElement = {"id" : xmlRegent1Formatted,  "label" : "/" + xmlRegent1Formatted};
listAttachments.push(attachementElement);
//ajout du pdf 1
var dataElement2 = 	spec.createData({
						'id': "CERFA_PDF_CFE",
						'label': 'CERFA PDF CFE :',
						'type': 'String',
						'mandatory': true,
						'value':pdfCfeFormatted
						})	;
listData.push(dataElement2);
var attachementElement = {"id" : pdfCfeFormatted,  "label" : "/" + pdfCfeFormatted};
listAttachments.push(attachementElement);
//ajout du xml regent tdr si présent 

if (authorityId2) {
var dataElement3 = 	spec.createData({
						'id': "XML_REGENT_TDR",
						'label': 'XML REGENT TDR :',
						'type': 'String',
						'mandatory': true,
						'value':xmlRegent2Formatted
						})	;
listData.push(dataElement3);
var attachementElement = {"id" : xmlRegent2Formatted,  "label" : "/" + xmlRegent2Formatted};
listAttachments.push(attachementElement);

var dataElement4 = 	spec.createData({
	'id': "CERFA_PDF_TDR",
	'label': 'CERFA PDF TDR :',
	'type': 'String',
	'mandatory': true,
	'value':pdf2CfeFormatted
	})	;
listData.push(dataElement4);
var attachementElement = {"id" : pdf2CfeFormatted,  "label" : "/" + pdf2CfeFormatted};
listAttachments.push(attachementElement);
}


var jsonRecord = nash.util.convertToJson(_record);
_log.info("record info is {}",jsonRecord);

var attachementsGroup = _record.attachmentPreprocess.attachmentPreprocess ; //; 


// liste des PJ pour la step génération xml tc
var list1PjData = []; 
var list2PjData = [];
//Ajout
//=====>

var indice=4;
		for (var cle in attachementsGroup ) {
			var pjGroup = attachementsGroup[cle] ; 
			_log.info("pjGroup length {} content  is {}",pjGroup.length, pjGroup);

			if (null != pjGroup && 0 != pjGroup.length) {
		
					//rename
					var fileType = "pdf";
					var filePath = "/2-attachment/generated/attachmentPreprocess." + cle +"-1-postprocess."+fileType;


					_log.info("filePath name of  pjDNCDeclarant splitted is {}",fileType );
					_log.info("typeof of  pjDNCDeclarant  is {}",typeof pjGroup );
					_log.info("pjDNCDeclarant  is {}", pjGroup );

					newNameOfpj1=getFileName(authorityId1,numeroDeLiasse1,indice,fileType,"others");
					
					
					_log.info("filePath is {}",filePath );
					nash.record.saveFile(newNameOfpj1,nash.util.resourceFromPath(filePath).getContent() );

					var pj = 	spec.createData({
											'id': "PJ_CFE_" + indice,
											'label': 'PJ_CFE :',
											'type': 'String',
											'mandatory': true,
											'value':newNameOfpj1
											})	;
					list1PjData.push(pj);
					var attachementElement = {"id" : newNameOfpj1,  "label" : "/" + newNameOfpj1};
					listAttachments.push(attachementElement);

					if (authorityId2) {
						newNameOfpj2=getFileName(authorityId2,numeroDeLiasse2,indice,fileType,"others");
						_log.info("filePath is {}",filePath );
						nash.record.saveFile(newNameOfpj2,nash.util.resourceFromPath(filePath).getContent() );
						var pj = 	spec.createData({
							'id': "PJ_TDR_" + indice,
							'label': 'PJ_TDR :',
							'type': 'String',
							'mandatory': true,
							'value':newNameOfpj2
							})	;
						list2PjData.push(pj);
						var attachementElement = {"id" : newNameOfpj2,  "label" : "/" + newNameOfpj2};
						listAttachments.push(attachementElement);
					}
					
				indice++;
			}
		}
//<======

_log.info("listAttachments is {}", listAttachments);


_log.info("attachementsGroup info is {}",attachementsGroup);
_log.info("type of attachementsGroup info is {}",typeof attachementsGroup);

	return spec.create({
	id : 'xmlTcGenerationConfirmation',
	label : "Xml TC Regent confirmation message",
	groups : [ spec.createGroup({
		id : 'confirmationMessageOk',
		description : "Le fichier XML TC a été généré et ajouté au dossier.",
		data : listData
		}),spec.createGroup({
		id : 'pjGeneratedAutority1',
		description : "liste des pièces justificatives.",
		data : list1PjData
		}),spec.createGroup({
		id : 'pjGeneratedAutority2',
		description : "liste des pièces justificatives.",
		data : list2PjData
		})
		
		]
	});
