// PJ Déclarant

var userDeclarant;
if ($p2agri.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNouveauNomUsage != null and Value('id').of($p2agri.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) {
    var userDeclarant = $p2agri.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNouveauNomUsage + '  ' + (Value('id').of($p2agri.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNomPrenom).contains('modifPrenom') ? $p2agri.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNouveauPrenoms[0] : $p2agri.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiquePrenom1[0]) ;
} else if ($p2agri.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiqueNomUsage != null and not Value('id').of($p2agri.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) {
    var userDeclarant = $p2agri.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiqueNomUsage + '  ' + (Value('id').of($p2agri.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNomPrenom).contains('modifPrenom') ? $p2agri.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNouveauPrenoms[0] : $p2agri.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiquePrenom1[0]) ;
} else {
    var userDeclarant = (Value('id').of($p2agri.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNomPrenom).contains('modifNomNaissance') ? $p2agri.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNouveauNomNaissance : $p2agri.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiqueNomNaissance) + '  '+ (Value('id').of($p2agri.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNomPrenom).contains('modifPrenom') ? $p2agri.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNouveauPrenoms[0] : $p2agri.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiquePrenom1[0]) ;
}

var pj=$p2agri.cadre9SignatureGroup.cadre9Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteDeclarant')) {
    attachment('pjIDDeclarantSignataire', 'pjIDDeclarantSignataire', { label: userDeclarant, mandatory:"true"});
}

var pj=$p2agri.cadre9SignatureGroup.cadre9Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire') and (Value('id').of($p2agri.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationPerso.objetSituationPerso).contains('10P') 
	or Value('id').of($p2agri.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationPerso.objetSituationPerso).contains('15P') 
	or Value('id').of($p2agri.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationPerso.objetSituationPerso).contains('17P'))) {
    attachment('pjIDDeclarantNonSignataire', 'pjIDDeclarantNonSignataire', { label: userDeclarant, mandatory:"true"});
}

// PJ Mandataire

var userMandataire=$p2agri.cadre9SignatureGroup.cadre9Signature.adresseMandataire

var pj=$p2agri.cadre9SignatureGroup.cadre9Signature.soussigne;
if (Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
    attachment('pjIDMandataireSignataire', 'pjIDMandataireSignataire', {label: userMandataire.nomPrenomDenominationMandataire, mandatory:"true"});
    attachment('pjPouvoir', 'pjPouvoir', {mandatory:"true"});
}

// PJ CONJOINT

if (Value('id').of($p2agri.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationPerso.objetSituationPerso).contains('30P') and $p2agri.cadre3ModificationConjointGroup.cadre3ModificationConjoint.statutConjointCollaborateur != null) {
    attachment('pjChoixStatutConjoint', 'pjChoixStatutConjoint', {mandatory:"true"});
}

// PJ EIRL

var pj=$p2agri.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEIRL.objetEIRL;
if ((Value('id').of($p2agri.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).contains('modifEIRL') and Value('id').of($p2agri.cadre1RappelIdentificationGroup.cadre1RappelIdentification.optionEirl).eq('optionEirlNon')) 
	or Value('id').of(pj).contains('modificationEIRL') 
	or $p2agri.cadre1ObjetModificationGroup.cadre1ObjetModification.entrepriseEIRL) {
    attachment('pjDAP', 'pjDAP', { mandatory:"true"});
}

var pj=$p2agri.cadre4DeclarationAffectationPatrimoineGroup.informationsComplementaires.contenuDAP;
if(Value('id').of(pj).contains('bienImmo') or Value('id').of($p2agri.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.contenuDAP).contains('bienImmo')) {
    attachment('pjDAPActeNotarie', 'pjDAPActeNotarie', { mandatory:"true"});
}

var pj=$p2agri.cadre4DeclarationAffectationPatrimoineGroup.informationsComplementaires.contenuDAP;
if(Value('id').of(pj).contains('bienRapportEvaluation') or Value('id').of($p2agri.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.contenuDAP).contains('bienRapportEvaluation')) {
    attachment('pjDAPRapportEvaluation', 'pjDAPRapportEvaluation', { mandatory:"true"});
}

var pj=$p2agri.cadre4DeclarationAffectationPatrimoineGroup.informationsComplementaires.contenuDAP;
if(Value('id').of(pj).contains('bienCommunIndivis') or Value('id').of($p2agri.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.contenuDAP).contains('bienCommunIndivis')) {
    attachment('pjDAPAccordTiers', 'pjDAPAccordTiers', { mandatory:"true"});
}