var formFields = {};
var cerfaFields = {};
function pad(s) { return (s < 10) ? '0' + s : s; }

// Cadre 1 - Objet de la formalité

var objet= $p2agri.cadre1ObjetModificationGroup.cadre1ObjetModification;
formFields['modifSituationpersonnelle']                                  = Value('id').of(objet.objetModification).contains('situationPerso') ? true : false;
formFields['modifEtablissement']                                         = Value('id').of(objet.objetModification).contains('etablissement')  ? true : false;
formFields['modifAutre']                                                 = (Value('id').of(objet.objetModification).contains('modifEIRL') or Value('id').of(objet.objetModification).contains('dateActivite')) ? true : false;
formFields['modifAutreLibelle'] = (Value('id').of(objet.objetModification).contains('modifEIRL') ? "Option EIRL" : Value('id').of(objet.objetModification).contains('dateActivite') ? "Date début activité" : '');

// Cadre 2 - Rappel d'identification de l'entreprise

var identite= $p2agri.cadre1RappelIdentificationGroup.cadre1RappelIdentification;
formFields['siren']                    	= identite.siren.split(' ').join('');
formFields['activiteAgriElevage']      	= Value('id').of(identite.activiteAgricole).eq('activiteElevage') ? true : false;
formFields['activiteAgriViticole']     	= Value('id').of(identite.activiteAgricole).eq('activiteViticole') ? true : false;
formFields['declarationFondsAgriOui']  	= Value('id').of(identite.declarationFondsAgri).eq('declarationFondsAgriOui') ? true : false;
formFields['declarationFondsAgriNon'] 	= Value('id').of(identite.declarationFondsAgri).eq('declarationFondsAgriNon') ? true : false;
formFields['activiteLocBiensRuraux']	= Value('id').of(identite.activiteLocBiensRuraux).eq('activiteLocBiensRurauxOui')? true : false;	

// Cadre 3A 

var modifIdentiteDeclarant = $p2agri.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle;

if (modifIdentiteDeclarant.modifIdentite.modifDateIdentite !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifIdentite.modifDateIdentite.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateIdentite']          = date;
}

formFields['personneLiee_personnePhysique_nomNaissance']             = (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P') and Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifNomNaissance')) ? modifIdentiteDeclarant.modifIdentite.modifNouveauNomNaissance : identite.personneLieePersonnePhysiqueNomNaissance;
formFields['personneLiee_personnePhysique_nomUsage']                 = (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P') and Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPseudonyme).contains('modifNomUsage')) ? (modifIdentiteDeclarant.modifIdentite.modifNouveauNomUsage != null ? modifIdentiteDeclarant.modifIdentite.modifNouveauNomUsage : '') : (identite.personneLieePersonnePhysiqueNomUsage != null ? identite.personneLieePersonnePhysiqueNomUsage : '');
formFields['personneLiee_personnePhysique_pseudonyme']                 = (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P') and Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPseudonyme).contains('modifPseudonyme')) ? (modifIdentiteDeclarant.modifIdentite.modifNouveauPseudonyme != null ? modifIdentiteDeclarant.modifIdentite.modifNouveauPseudonyme : '') : (identite.personneLieePersonnePhysiquePseudonyme != null ? identite.personneLieePersonnePhysiquePseudonyme : '');
if (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom')) {	
var prenoms=[];
for ( i = 0; i < modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms.size() ; i++ ){prenoms.push(modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms[i]);}                            
formFields['personneLiee_personnePhysique_prenom1']                                      = prenoms.toString();
} else if (not Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom')) {
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                          
formFields['personneLiee_personnePhysique_prenom1']                                      = prenoms.toString();
}
if (identite.personneLieePersonnePhysiqueDateNaissance !== null) {
    var dateTmp = new Date(parseInt(identite.personneLieePersonnePhysiqueDateNaissance.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['personneLiee_personnePhysique_dateNaissance']          = date;
}
formFields['personneLiee_personnePhysique_lieuNaissanceDepartement'] = identite.personneLieePPLieuNaissanceDepartement != null ? identite.personneLieePPLieuNaissanceDepartement.getId() : '';
formFields['personneLiee_personnePhysique_lieuNaissanceCommune']     = (identite.personneLieePPLieuNaissanceCommune != null ?  identite.personneLieePPLieuNaissanceCommune : identite.personneLieePPLieuNaissanceVille) + ' / ' + identite.personneLieePPLieuNaissancePays;

// Cadre 3B - Rappel de l'ancienne identification du chef d'entreprise

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P') 
	or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) {
formFields['modifAncienNomNaissance']                                    = identite.personneLieePersonnePhysiqueNomNaissance;
formFields['modifAncienNomUsage']                                        = identite.personneLieePersonnePhysiqueNomUsage != null ? identite.personneLieePersonnePhysiqueNomUsage : '';
var prenoms=[];
		for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                         
formFields['modifAncienPrenoms']   = prenoms.toString();	
formFields['modifAncienPseudonyme']  = identite.personneLieePersonnePhysiquePseudonyme != null ? identite.personneLieePersonnePhysiquePseudonyme : '';
	}

// Cadre 4 - Modification situation personnelle

// Modification du domicile

var etablissement = $p2agri.cadre6EtablissementGroup.infoEtablissementOld;
var newAdresseDom = $p2agri.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifDomicile.newAdresseDomicile;

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P')) {
	if (modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
		var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
		var date = pad(dateTmp.getDate().toString());
		var month = dateTmp.getMonth() + 1;
		date = date.concat(pad(month.toString()));
		date = date.concat(dateTmp.getFullYear().toString());
		formFields['modifDateDomicile']          = date;
	}
formFields['personneLiee_adresse_voie']                             = (newAdresseDom.personneLieeAdresseVoieNew != null ? newAdresseDom.personneLieeAdresseVoieNew : '') 
																	+ ' ' + (newAdresseDom.personneLieeAdresseIndiceVoieNew != null ? newAdresseDom.personneLieeAdresseIndiceVoieNew : '')
																	+ ' ' + (newAdresseDom.personneLieeAdresseTypeVoieNew != null ? newAdresseDom.personneLieeAdresseTypeVoieNew : '')
																	+ ' ' + (newAdresseDom.personneLieeAdresseNomVoieNew != null ? newAdresseDom.personneLieeAdresseNomVoieNew : '')
																	+ ' ' + (newAdresseDom.personneLieeAdresseComplementAdresseNew != null ? newAdresseDom.personneLieeAdresseComplementAdresseNew : '')
																	+ ' ' + (newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null ? newAdresseDom.personneLieeAdresseDistriutionSpecialeNew : '');
formFields['personneLiee_adresse_codePostal']                       = newAdresseDom.personneLieeAdresseCodePostalNew;
formFields['personneLiee_adresse_commune']                          = newAdresseDom.personneLieeAdresseCommuneNew;
formFields['personneLiee_adresse_commune_AncienneCommune']          = newAdresseDom.personneLieeAdresseCommuneNewAncienne != null ? newAdresseDom.personneLieeAdresseCommuneNewAncienne : '';
formFields['personneLiee_adresse_domicileMemeDept']          = newAdresseDom.domicileMemeDept != null ? newAdresseDom.domicileMemeDept : '';
} else if (objet.entrepriseDomicile) {

var newAdresseDom2 = $p2agri.cadre6EtablissementGroup.infoEtablissementNew.adresseEtablissementNew;
		if (etablissement.modifDateAdresseEntreprise !== null) {
			var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
			var date = pad(dateTmp.getDate().toString());
			var month = dateTmp.getMonth() + 1;
			date = date.concat(pad(month.toString()));
			date = date.concat(dateTmp.getFullYear().toString());
			formFields['modifDateDomicile']          = date;
		}

formFields['personneLiee_adresse_voie']                             = (newAdresseDom2.etablissementAdresseNumeroVoieNew != null ? newAdresseDom2.etablissementAdresseNumeroVoieNew : '') 
																	+ ' ' + (newAdresseDom2.etablissementAdresseIndiceVoieNew != null ? newAdresseDom2.etablissementAdresseIndiceVoieNew : '')
																	+ ' ' + (newAdresseDom2.etablissementAdresseTypeVoieNew != null ? newAdresseDom2.etablissementAdresseTypeVoieNew : '')
																	+ ' ' + (newAdresseDom2.etablissementAdresseNomVoieNew != null ? newAdresseDom2.etablissementAdresseNomVoieNew : '')
																	+ ' ' + (newAdresseDom2.etablissementAdresseComplementVoieNew != null ? newAdresseDom2.etablissementAdresseComplementVoieNew : '')
																	+ ' ' + (newAdresseDom2.etablissementAdresseDistriutionSpecialeVoieNew != null ? newAdresseDom2.etablissementAdresseDistriutionSpecialeVoieNew : '');
formFields['personneLiee_adresse_codePostal']                       = newAdresseDom2.etablissementAdresseCodePostalNew;
formFields['personneLiee_adresse_commune']                          = newAdresseDom2.etablissementAdresseCommuneNew;
}

// Nouvelle nationalité

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('17P')) {
if (modifIdentiteDeclarant.modifNationalite.modifDateNationalite !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifNationalite.modifDateNationalite.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateNationalite']          = date;
}
formFields['modifNouvelleNationalite']                                   = modifIdentiteDeclarant.modifNationalite.modifNewNationalite;
}
// Cadre 6 - Conjoint

var conjoint = $p2agri.cadre3ModificationConjointGroup.cadre3ModificationConjoint;

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('30P')) {
	if (conjoint.modifDateConjoint !== null) {
    var dateTmp = new Date(parseInt(conjoint.modifDateConjoint.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateConjoint']          = date;
}
formFields['statutConjointCollaborateur']           = Value ('id').of(conjoint.statutConjointCollaborateur).eq('1') ? true : false;
formFields['statutConjointSalarie']           		= Value ('id').of(conjoint.statutConjointCollaborateur).eq('3') ? true : false;
formFields['statutConjointCoExploitant']          	= Value ('id').of(conjoint.statutConjointCollaborateur).eq('4') ? true : false;
}

// Cadre 5 - EIRL

if ($p2agri.cadre4DeclarationAffectationPatrimoineGroup.cadreDeclarationAffectationPatrimoine.modifDateDeclarationEIRL !== null) {
    var dateTmp = new Date(parseInt($p2agri.cadre4DeclarationAffectationPatrimoineGroup.cadreDeclarationAffectationPatrimoine.modifDateDeclarationEIRL.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date1;
} else if ($p2agri.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.modifDateEIRL !== null) {
    var dateTmp = new Date(parseInt($p2agri.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.modifDateEIRL.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date2;
}  /*else if ($p2agri.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.poursuiteEIRLGroup.modifDatePoursuiteEIRL !== null) {
    var dateTmp = new Date(parseInt($p2agri.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.poursuiteEIRLGroup.modifDatePoursuiteEIRL.getTimeInMillis()));
    var date3 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date3 = date3.concat(pad(month.toString()));
	date3 = date3.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date3;
}*/  else if ($p2agri.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.finEIRLGroup.finEIRLDeclaration.modifDateFinEIRL !== null) {
    var dateTmp = new Date(parseInt($p2agri.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.finEIRLGroup.finEIRLDeclaration.modifDateFinEIRL.getTimeInMillis()));
    var date4 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date4 = date4.concat(pad(month.toString()));
	date4 = date4.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date4;
}	else if (objet.entrepriseEIRL and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('11P') and etablissement.modifDateAdresseEntreprise !== null) {
    var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
    var date5 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date5 = date5.concat(pad(month.toString()));
	date5 = date5.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date5;
} else if (objet.entrepriseEIRL and modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
    var date6 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date6 = date6.concat(pad(month.toString()));
	date6 = date6.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date6;
} 
/* formFields['eirl_immatriculation'] = (Value('id').of(objet.objetModification).contains('modifEIRL') and Value('id').of(identite.optionEirl).eq('optionEirlNon')) ? true : false;
formFields['eirl_modification'] = (Value('id').of(objet.objetModification).contains('modifEIRL') and Value('id').of(identite.optionEirl).eq('optionEirlOui')) ? true : false; */

//Deuxième version :
formFields['eirl_immatriculation'] = Value('id').of(objet.objetModification).contains('modifEIRL') ? (Value('id').of(identite.optionEirl).eq('optionEirlNon') ? true : false) : false;
formFields['eirl_modification'] = Value('id').of(objet.objetModification).contains('modifEIRL') ? (Value('id').of(identite.optionEirl).eq('optionEirlOui') ? true : false) : false;
/* 
<if>Value('id').of($cadre1ObjetModification.objetModification).contains('modifEIRL') and Value('id').of($form.cadre1RappelIdentificationGroup.cadre1RappelIdentification.optionEirl).eq('optionEirlOui') */

// Cadre 7 - Déclaration relative à l'exploitation 

var activiteInfo = $p2agri.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite;


formFields['modifConcerneTransfert']   = (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') or objet.domicileEntreprise) ? true : false;
formFields['modifConcerneOuverture']  = Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') ? true : false;
formFields['modifConcerneFermeture']  = Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P') ? true : false;
formFields['modifConcerneActivite']    = (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite') 
																		or activiteInfo.modifActiviteTransfert) ? true : false;
formFields['bailleurBiensRuraux']     = Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('bailleurBiensRuraux') ? true : false;

// Cadre 8 - Etablissement transféré ou fermé

var adresseEntreprise = $p2agri.cadre1RappelIdentificationGroup.cadre1RappelIdentification.adresseEtablissementPrincipal;

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P') 
	or objet.domicileEntreprise) {
		if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P')) and etablissement.modifDateAdresseEntreprise !== null) {
			var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
			var date = pad(dateTmp.getDate().toString());
			var month = dateTmp.getMonth() + 1;
			date = date.concat(pad(month.toString()));
			date = date.concat(dateTmp.getFullYear().toString());
			formFields['modifDateEtablissementTransfererFermer']          = date;
		} else if (objet.domicileEntreprise and modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
			var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
			var date = pad(dateTmp.getDate().toString());
			var month = dateTmp.getMonth() + 1;
			date = date.concat(pad(month.toString()));
			date = date.concat(dateTmp.getFullYear().toString());
			formFields['modifDateEtablissementTransfererFermer']          = date;
		} 
formFields['modifCategarieEtablissementTransfererFermer_principal']      = (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')
or objet.domicileEntreprise or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement2 ).eq('fermeturePrincipal')  
or objet.domicileEntreprise) ? true : false;
formFields['modifCategarieEtablissementTransfererFermer_secondaire']     = (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire') 
or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement2 ).eq('fermetureSecondaire')  
/* or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).contains('80P') */)? true : false;

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')) 
or objet.domicileEntreprise
or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement2 ).eq('fermeturePrincipal'))
) {
formFields['modifAncienneAdresseEtablissement_adresseVoie']             = (adresseEntreprise.etablissementAdresseNumeroVoie != null ? adresseEntreprise.etablissementAdresseNumeroVoie : '')
																		+ ' ' + (adresseEntreprise.etablissementAdresseIndiceVoie != null ? adresseEntreprise.etablissementAdresseIndiceVoie : '')
																		+ ' ' + (adresseEntreprise.etablissementAdresseTypeVoie != null ? adresseEntreprise.etablissementAdresseTypeVoie : '')
																		+ ' ' + (adresseEntreprise.etablissementAdresseNomVoie != null ? adresseEntreprise.etablissementAdresseNomVoie : '');
formFields['modifAncienneAdresseEtablissement_adresseComplement']        = (adresseEntreprise.etablissementAdresseComplementVoie != null ? adresseEntreprise.etablissementAdresseComplementVoie : '')
																		+ ' ' + (adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null ? adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie : '');
formFields['modifAncienneAdresseEtablissement_adresseCP']               = adresseEntreprise.etablissementAdresseCodePostal;
formFields['modifAncienneAdresseEtablissement_adresseCommune']          = adresseEntreprise.etablissementAdresseCommune;
} else if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')) or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P')) {
formFields['modifAncienneAdresseEtablissement_adresseVoie']             = (etablissement.adresseEtablissementOld.etablissementAdresseNumeroVoieOld != null ? etablissement.adresseEtablissementOld.etablissementAdresseNumeroVoieOld : '') 
																		+ ' ' + (etablissement.adresseEtablissementOld.etablissementAdresseIndiceVoieOld != null ? etablissement.adresseEtablissementOld.etablissementAdresseIndiceVoieOld : '')
																		+ ' ' + (etablissement.adresseEtablissementOld.etablissementAdresseTypeVoieOld != null ? etablissement.adresseEtablissementOld.etablissementAdresseTypeVoieOld : '')
																		+ ' ' + (etablissement.adresseEtablissementOld.etablissementAdresseNomVoieOld != null ? etablissement.adresseEtablissementOld.etablissementAdresseNomVoieOld : '');
formFields['modifAncienneAdresseEtablissement_adresseComplement']       = (etablissement.adresseEtablissementOld.etablissementAdresseComplementVoieOld != null ? etablissement.adresseEtablissementOld.etablissementAdresseComplementVoieOld : '')
																		+ ' ' + (etablissement.adresseEtablissementOld.etablissementAdresseDistriutionSpecialeVoieOld != null ? etablissement.adresseEtablissementOld.etablissementAdresseDistriutionSpecialeVoieOld : '');
formFields['modifAncienneAdresseEtablissement_adresseCP']               = etablissement.adresseEtablissementOld.etablissementAdresseCodePostalOld;
formFields['modifAncienneAdresseEtablissement_adresseCommune']          = etablissement.adresseEtablissementOld.etablissementAdresseCommuneOld;
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') or objet.domicileEntreprise) {
formFields['modifDestinationEtablissementTransfert_cessation'] = (Value('id').of(etablissement.destinationEtablissement.destinationEtablissement1).eq('ferme') or Value('id').of(etablissement.destinationEtablissement.destinationEtablissement2).eq('ferme')) ? true : false;
formFields['modifDestinationEtablissementTransfert_autre']               = (Value('id').of(etablissement.destinationEtablissement.destinationEtablissement1).eq('autre') or Value('id').of(etablissement.destinationEtablissement.destinationEtablissement2).eq('autre')) ? true : false;
formFields['modifDestinationEtablissementTransfert_autreLibelle']        = (Value('id').of(etablissement.destinationEtablissement.destinationEtablissement1).eq('autre') or Value('id').of(etablissement.destinationEtablissement.destinationEtablissement2).eq('autre')) ? etablissement.destinationEtablissement.destinationAutre : ''; 

formFields['modifDestinationEtablissement_devientPrincipal']             = Value('id').of(etablissement.destinationEtablissement.destinationEtablissement2).eq('devientPrincipal') ? true : false;
formFields['modifDestinationEtablissement_devientSecondaire']            = Value('id').of(etablissement.destinationEtablissement.destinationEtablissement1).eq('devientSecondaire') ? true : false;
}

/* if Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P') {
formFields['modifDestinationEtablissementFerme_supprimer']               = Value('id').of(etablissement.destinationEtablissement3).eq('supprime') ? true : false;
formFields['modifDestinationEtablissementFerme_vendu']                   = Value('id').of(etablissement.destinationEtablissement3).eq('vendu') ? true : false;
formFields['modifDestinationEtablissementFerme_autre']                   = Value('id').of(etablissement.destinationEtablissement3).eq('autre') ? true : false;
formFields['modifDestinationEtablissementFerme_autreLibelle']            = Value('id').of(etablissement.destinationEtablissement3).eq('autre') ? etablissement.destinationAutre : '';
if (etablissement.dateCessationEmploi !== null) {
    var dateTmp = new Date(parseInt(etablissement.dateCessationEmploi.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDestinationEtablissementDateFinEmploiSalarie']          = date;
}
} */
}

// Cadre 9 - Etablissement créé ou modifié

var etablissementNew =$p2agri.cadre6EtablissementGroup.infoEtablissementNew;
var activite = $p2agri.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite;

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
	or (Value('id').of(objet.objetModification).contains('dateActivite') and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 !== null or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 !== null))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')
	or objet.domicileEntreprise) {
				if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissement.modifDateAdresseEntreprise !== null) {
					var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
					var date = pad(dateTmp.getDate().toString());
					var month = dateTmp.getMonth() + 1;
					date = date.concat(pad(month.toString()));
					date = date.concat(dateTmp.getFullYear().toString());
					formFields['modifDateEtablissementOuvertModifier']          = date;
				} else if (objet.domicileEntreprise and modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
					var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
					var date = pad(dateTmp.getDate().toString());
					var month = dateTmp.getMonth() + 1;
					date = date.concat(pad(month.toString()));
					date = date.concat(dateTmp.getFullYear().toString());
					formFields['modifDateEtablissementOuvertModifier']          = date;
				} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') and etablissementNew.modifDateAdresseOuverture !== null) {
					var dateTmp = new Date(parseInt(etablissementNew.modifDateAdresseOuverture.getTimeInMillis()));
					var date = pad(dateTmp.getDate().toString());
					var month = dateTmp.getMonth() + 1;
					date = date.concat(pad(month.toString()));
					date = date.concat(dateTmp.getFullYear().toString());
					formFields['modifDateEtablissementOuvertModifier']          = date;
				} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite') and activiteInfo.cadreActivite.modifDateActivite !== null) {
					var dateTmp = new Date(parseInt(activiteInfo.cadreActivite.modifDateActivite.getTimeInMillis()));
					var date = pad(dateTmp.getDate().toString());
					var month = dateTmp.getMonth() + 1;
					date = date.concat(pad(month.toString()));
					date = date.concat(dateTmp.getFullYear().toString());
					formFields['modifDateEtablissementOuvertModifier']          = date;
				}  else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P') and activiteInfo.cadreEtablissementIdentification.modifDateIdentification !== null) {
					var dateTmp = new Date(parseInt(activiteInfo.cadreEtablissementIdentification.modifDateIdentification.getTimeInMillis()));
					var date = pad(dateTmp.getDate().toString());
					var month = dateTmp.getMonth() + 1;
					date = date.concat(pad(month.toString()));
					date = date.concat(dateTmp.getFullYear().toString());
					formFields['modifDateEtablissementOuvertModifier']          = date;
				} else if (Value('id').of(objet.objetModification).contains('dateActivite') and objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 !== null) {
					var dateTmp = new Date(parseInt(objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1.getTimeInMillis()));
					var date = pad(dateTmp.getDate().toString());
					var month = dateTmp.getMonth() + 1;
					date = date.concat(pad(month.toString()));
					date = date.concat(dateTmp.getFullYear().toString());
					formFields['modifDateEtablissementOuvertModifier']          = date;
				}  else if (Value('id').of(objet.objetModification).contains('dateActivite') and objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 !== null) {
					var dateTmp = new Date(parseInt(objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2.getTimeInMillis()));
					var date = pad(dateTmp.getDate().toString());
					var month = dateTmp.getMonth() + 1;
					date = date.concat(pad(month.toString()));
					date = date.concat(dateTmp.getFullYear().toString());
					formFields['modifDateEtablissementOuvertModifier']          = date;
				} 

		if (objet.domicileEntreprise) {
		formFields['modifNouvelleAdresseEtablissement_voieAdresse']             = (newAdresseDom.personneLieeAdresseVoieNew != null ? newAdresseDom.personneLieeAdresseVoieNew : '') 
																				+ ' ' + (newAdresseDom.personneLieeAdresseIndiceVoieNew != null ? newAdresseDom.personneLieeAdresseIndiceVoieNew : '')
																				+ ' ' + (newAdresseDom.personneLieeAdresseTypeVoieNew != null ? newAdresseDom.personneLieeAdresseTypeVoieNew : '')
																				+ ' ' + (newAdresseDom.personneLieeAdresseNomVoieNew != null ? newAdresseDom.personneLieeAdresseNomVoieNew : '')
																				+ ' ' + (newAdresseDom.personneLieeAdresseComplementAdresseNew != null ? newAdresseDom.personneLieeAdresseComplementAdresseNew : '')
																				+ ' ' + (newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null ? newAdresseDom.personneLieeAdresseDistriutionSpecialeNew : '');
		formFields['modifNouvelleAdresseEtablissement_cpAdresse']               = newAdresseDom.personneLieeAdresseCodePostalNew;
		formFields['modifNouvelleAdresseEtablissement_communeAdresse']          = newAdresseDom.personneLieeAdresseCommuneNew;
		} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
				  or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') 
				  or ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
				  or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P'))  
				  and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))) {
		formFields['modifNouvelleAdresseEtablissement_voieAdresse']             = (etablissementNew.adresseEtablissementNew.etablissementAdresseNumeroVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseNumeroVoieNew : '') 
																				+ ' ' + (etablissementNew.adresseEtablissementNew.etablissementAdresseIndiceVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseIndiceVoieNew : '')
																				+ ' ' + (etablissementNew.adresseEtablissementNew.etablissementAdresseTypeVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseTypeVoieNew : '')
																				+ ' ' + (etablissementNew.adresseEtablissementNew.etablissementAdresseNomVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseNomVoieNew : '')
																				+ ' ' + (etablissementNew.adresseEtablissementNew.etablissementAdresseComplementVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseComplementVoieNew : '')
																				+ ' ' + (etablissementNew.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew : '');
		formFields['modifNouvelleAdresseEtablissement_cpAdresse']               = etablissementNew.adresseEtablissementNew.etablissementAdresseCodePostalNew;
		formFields['modifNouvelleAdresseEtablissement_communeAdresse']          = etablissementNew.adresseEtablissementNew.etablissementAdresseCommuneNew;
		} else if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
			or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P'))  
			and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
			or (Value('id').of(objet.objetModification).contains('dateActivite') and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 != null or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 !== null)))	{
		formFields['modifNouvelleAdresseEtablissement_voieAdresse']             = (adresseEntreprise.etablissementAdresseNumeroVoie != null ? adresseEntreprise.etablissementAdresseNumeroVoie : '')
																				+ ' ' + (adresseEntreprise.etablissementAdresseIndiceVoie != null ? adresseEntreprise.etablissementAdresseIndiceVoie : '')
																				+ ' ' + (adresseEntreprise.etablissementAdresseTypeVoie != null ? adresseEntreprise.etablissementAdresseTypeVoie : '')
																				+ ' ' + (adresseEntreprise.etablissementAdresseNomVoie != null ? adresseEntreprise.etablissementAdresseNomVoie : '')
																				+ ' ' + (adresseEntreprise.etablissementAdresseComplementVoie != null ? adresseEntreprise.etablissementAdresseComplementVoie : '')
																				+ ' ' + (adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null ? adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie : '');
		formFields['modifNouvelleAdresseEtablissement_cpAdresse']               = adresseEntreprise.etablissementAdresseCodePostal;
		formFields['modifNouvelleAdresseEtablissement_communeAdresse']          = adresseEntreprise.etablissementAdresseCommune;
		}

/* if (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') 
	or (Value('id').of(objet.objetModification).contains('dateActivite') 
	and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 != null 
	or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 !== null))) {
formFields['modifCategarieEtablissementModifier_principal']              = true;
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')) {
formFields['modifCategarieEtablissementModifier_secondaire']             = true;
} 
}*/
/* 		if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
		and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') 
		and Value('id').of(etablissementNew.origineEtablissement).eq('etablissementOuverture')) or objet.domicileEntreprise) {
formFields['modifCategorieEtablissementOuvert_principal'] = true;
		} */
		
formFields['modifCategorieEtablissementOuvert_principal'] = ((Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') and Value('id').of(etablissementNew.origineEtablissement).eq('etablissementOuverture'))
or (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') and Value('id').of(etablissementNew.origineEtablissement).eq('etablissementExistant')) or objet.domicileEntreprise) ? true : false;	

/* 		if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
		and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire') 
		and Value('id').of(etablissementNew.origineEtablissement).eq('etablissementOuverture')) 
		or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')) {
formFields['modifCategorieEtablissementOuvert_secondaire']= true;
		} */
formFields['modifCategorieEtablissementOuvert_secondaire']= ((Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire') and Value('id').of(etablissementNew.origineEtablissement).eq('etablissementOuverture'))
		or (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire') and Value('id').of(etablissementNew.origineEtablissement).eq('etablissementExistant')) 
		or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')) ? true : false;		
}

// Cadre 10 - Activité

if(Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
or Value('id').of($p2agri.cadre6EtablissementGroup.infoEtablissementNew.origineEtablissement).eq('etablissementOuverture')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P') 
	// and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement2 ).eq('fermeturePrincipal'))
or objet.domicileEntreprise) {		    

//Date de modification 		
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite') and activite.modifDateActivite !== null) {
    var dateTmp = new Date(parseInt(activite.modifDateActivite.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateActivite']          = date;
}  else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissement.modifDateAdresseEntreprise !== null) {
    var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateActivite']          = date;
}  else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') and etablissementNew.modifDateAdresseOuverture !== null) {
    var dateTmp = new Date(parseInt(etablissementNew.modifDateAdresseOuverture.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateActivite']          = date;
} else if (objet.domicileEntreprise and modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateActivite']          = date;
} 
//Activité principale exercée:
formFields['etablissement_activitePlusImportanteAgricole_cereales']                         = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricoleCereales') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_riz']                              = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricoleRiz') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_legumes']                          = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricoleLegumes') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_canneSucre']                       = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricoleCanneSucre') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_tabac']                            = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricoleTabac') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_plantesFibres']                    = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricolePlantesFibres') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_autresCulturesNonPermanentes']     = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricoleAutresCulturesNonPermanentes') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_vigne']                            = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricoleVigne') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_fruitsTropicaux']                  = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricoleFruitsTropicaux') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_agrumes']                          = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricoleAgrumes') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_fruitsPepins']                     = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricoleFruitsPepins') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_fruitsOleagineux']                 = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricoleFruitsOleagineux') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_autresFruitsArbres']               = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricoleAutresFruitsArbres') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_plantesBoisson']                   = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricolePlantesBoisson') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_plantesEpicesAromatiques']         = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricolePlantesEpicesAromatiques') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_autresCulturesPermanentes']        = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricoleAutresCulturesPermanentes') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_autresCulturesPermanentes_Autres'] = Value('id').of(activite.etablissementActivitesPrincipales2).eq('etablissementActivitePlusImportanteAgricoleAutresCulturesPermanentes') ? activite.etablissementActivitePlusImportanteAgricoleAutresCulturesPermanentesAutres : '';

formFields['etablissement_activitePlusImportanteAgricole_vachesLaitieres']                  = Value('id').of(activite.etablissementActivitesPrincipales3).eq('etablissementActivitePlusImportanteAgricoleVachesLaitieres') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_autresBovins']                     = Value('id').of(activite.etablissementActivitesPrincipales3).eq('etablissementActivitePlusImportanteAgricoleAutresBovins') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_chevaux']                          = Value('id').of(activite.etablissementActivitesPrincipales3).eq('etablissementActivitePlusImportanteAgricoleChevaux') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_chameaux']                         = Value('id').of(activite.etablissementActivitesPrincipales3).eq('etablissementActivitePlusImportanteAgricoleChameaux') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_ovinsCaprins']                     = Value('id').of(activite.etablissementActivitesPrincipales3).eq('etablissementActivitePlusImportanteAgricoleOvinsCaprins') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_porcins']                          = Value('id').of(activite.etablissementActivitesPrincipales3).eq('etablissementActivitePlusImportanteAgricolePorcins') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_volailles']                        = Value('id').of(activite.etablissementActivitesPrincipales3).eq('etablissementActivitePlusImportanteAgricoleVolailles') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_aquacultureMer']                   = Value('id').of(activite.etablissementActivitesPrincipales3).eq('etablissementActivitePlusImportanteAgricoleAquacultureMer') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_aquacultureEauDouce']              = Value('id').of(activite.etablissementActivitesPrincipales3).eq('etablissementActivitePlusImportanteAgricoleAquacultureEauDouce') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_autresAnimaux']                    = Value('id').of(activite.etablissementActivitesPrincipales3).eq('etablissementActivitePlusImportanteAgricoleAutresAnimaux') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_autresAnimaux_Autres']             = Value('id').of(activite.etablissementActivitesPrincipales3).eq('etablissementActivitePlusImportanteAgricoleAutresAnimaux') ? activite.etablissementActivitePlusImportanteAgricoleAutresAnimauxAutres : '';

formFields['etablissement_activitePlusImportanteAgricole_cultureElevageAssocies']           = Value('id').of(activite.etablissementActivitesPrincipales4).eq('etablissementActivitePlusImportanteAgricoleCultureElevageAssocies') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_activitesPepinieres']              = Value('id').of(activite.etablissementActivitesPrincipales4).eq('etablissementActivitePlusImportanteAgricoleActivitesPepinieres') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_sylviculture']                     = Value('id').of(activite.etablissementActivitesPrincipales4).eq('etablissementActivitePlusImportanteAgricoleSylviculture') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_bailleurBiensRuraux']              = Value('id').of(activite.etablissementActivitesPrincipales4).eq('etablissementActivitePlusImportanteAgricoleBailleurBiensRuraux') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_loueurCheptel']                    = Value('id').of(activite.etablissementActivitesPrincipales4).eq('etablissementActivitePlusImportanteAgricoleLoueurCheptel') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_autre']                            = Value('id').of(activite.etablissementActivitesPrincipales4).eq('etablissementActivitePlusImportanteAgricoleAutre') ? true : false;
formFields['etablissement_activitePlusImportanteAgricole_AutresPreciser']                   = Value('id').of(activite.etablissementActivitesPrincipales4).eq('etablissementActivitePlusImportanteAgricoleAutre') ? activite.etablissementActivitePlusImportanteAgricoleAutresPreciser : '';
formFields['activiteSecondaire']                                                            = activite.activiteSecondaire != null ? activite.activiteSecondaire : '';

//L'activité principale de cet établissement devient-elle l'activité principale de l'entreprise

formFields['activiteDevientPrincipale_oui']    = activite.activiteDevientPrincipale ? true : false ;
formFields['activiteDevientPrincipale_non']    = activite.activiteDevientPrincipale ? false : true;

//En cas de modification d'activité
formFields['modifActivite_adjonction']  = Value('id').of(activite.activiteEvenement).eq('61P') ? true : false;
formFields['modifActivite_suppression']  = Value('id').of(activite.activiteEvenement).eq('62P') ? true : false;
formFields['modifActivite_suppressionDisparition'] = Value('id').of(activite.activiteMotifSuppression).eq('62PActiviteDisparition') ? true : false;
formFields['modifActivite_suppressionVente'] =  Value('id').of(activite.activiteMotifSuppression).eq('62PActiviteVente') ? true : false;
formFields['modifActivite_suppressionReprise'] = Value('id').of(activite.activiteMotifSuppression).eq('62PActiviteReprise') ? true : false;
formFields['modifActivite_suppressionAutre'] = Value('id').of(activite.activiteMotifSuppression).eq('62PActiviteAutre') ? true : false;
formFields['modifActivite_suppressionAutreLibelle'] = Value('id').of(activite.activiteMotifSuppression).eq('62PActiviteAutre') ? activite.activiteMotifSuppressionAutre : '';

//En cas de cessation totale :
var activiteCessationTotale =$p2agri.cadre6EtablissementGroup.infoEtablissementOld.cessationTotale;
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement2 ).eq('fermeturePrincipal'))
/* 	and not Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))  */{
formFields['cessationActivite_conservationCheptel'] = Value('id').of(activiteCessationTotale.cessationTotale1).eq('conservationCheptel') ? true : false; 
formFields['cessationActivite_conservationExploitationSubsistance'] =  Value('id').of(activiteCessationTotale.cessationTotale1).eq('conservationExploitationSubsistance') ? true : false;
}
}// Fin du cadre Activité

// Cadre 11 - Nom de l'exploitation

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P') or activiteInfo.modifEtablissementNomExploitation) {
		if (activiteInfo.cadreEtablissementIdentification.modifDateIdentification !== null) {
			var dateTmp = new Date(parseInt(activiteInfo.cadreEtablissementIdentification.modifDateIdentification.getTimeInMillis()));
			var date = pad(dateTmp.getDate().toString());
			var month = dateTmp.getMonth() + 1;
			date = date.concat(pad(month.toString()));
			date = date.concat(dateTmp.getFullYear().toString());
			formFields['modifDateNomExploitation']          = date;
		} 
		formFields['etablissement_nomExploitation']   = activiteInfo.cadreEtablissementIdentification.modifNomExploitation;
} else if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') or objet.domicileEntreprise
	or Value('id').of(etablissementNew.origineEtablissement).eq('etablissementOuverture')) and activite.modifNomExploitationBis != null) {
		if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissement.modifDateAdresseEntreprise !== null) {
			var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
			var date = pad(dateTmp.getDate().toString());
			var month = dateTmp.getMonth() + 1;
			date = date.concat(pad(month.toString()));
			date = date.concat(dateTmp.getFullYear().toString());
			formFields['modifDateNomExploitation']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') and etablissementNew.modifDateAdresseOuverture !== null) {
			var dateTmp = new Date(parseInt(etablissementNew.modifDateAdresseOuverture.getTimeInMillis()));
			var date = pad(dateTmp.getDate().toString());
			var month = dateTmp.getMonth() + 1;
			date = date.concat(pad(month.toString()));
			date = date.concat(dateTmp.getFullYear().toString());
			formFields['modifDateNomExploitation']       = date;
		} 
		formFields['etablissement_nomExploitation']   = activite.modifNomExploitationBis;
} 

// Cadre 12 - Origine de l'exploitation ou de l'activité

var origine = $p2agri.cadre7EtablissementActiviteGroup.cadre4OrigineExploitation;

if (Value('id').of(etablissementNew.origineEtablissement).eq('etablissementOuverture')
				or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
				or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
			    or activiteInfo.modifActiviteTransfert
				or objet.domicileEntreprise) {
formFields['etablissement_origineFonds_creation'] = (Value('id').of(origine.etablisementOrigineExploitation).eq('etablissementOrigineExploitationCreation') or objet.domicileEntreprise) ? true : false;
formFields['etablissement_origineFonds_poursuiteConjoint'] = Value('id').of(origine.etablisementOrigineExploitation).eq('etablissementOrigineExploitationPoursuiteConjoint') ? true : false;
formFields['etablissement_origineFonds_reprise'] = Value('id').of(origine.etablisementOrigineExploitation).eq('etablissementOrigineExploitationRepriseTotalePartielle') ? true : false;
formFields['etablissement_origineFonds_autre'] = Value('id').of(origine.etablisementOrigineExploitation).eq('etablissementOrigineExploitationAutre') ? true : false;
formFields['etablissement_origineFonds_autreLibelle'] = Value('id').of(etablissement.destinationEtablissement1).eq('etablissementOrigineExploitationAutre')  ? origine.etablissementOrigineFondsLibelle : '';

if (Value('id').of(origine.etablisementOrigineExploitation).eq('etablissementOrigineExploitationRepriseTotalePartielle')) {
formFields['siren_precedentExploitant']  = origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant.split(' ').join('');
formFields['NumDetenteurElevage_precedentExploitant']  = origine.precedentExploitant.entrepriseLieeNumeroDetenteurPrecedentExploitant != null ? (origine.precedentExploitant.entrepriseLieeNumeroDetenteurPrecedentExploitant) : '';
formFields['NumExploitation_precedentExploitant']  =  origine.precedentExploitant.entrepriseLieeNumeroExploitationPrecedentExploitant != null ? (origine.precedentExploitant.entrepriseLieeNumeroExploitationPrecedentExploitant) : '';
}
if (Value('id').of(origine.etablisementOrigineExploitation).eq('etablissementOrigineExploitationRepriseTotalePartielle') and Value('id').of(origine.precedentExploitant.typePersonnePrecedentExploitant).eq('typePersonnePrecedentExploitantPP')) {
formFields['nomNaissance_precedentExploitant'] = origine.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant;
formFields['nomUsage_precedentExploitant']  = origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant : '';
formFields['prenom_precedentExploitant'] = origine.precedentExploitant.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant;
}
if (Value('id').of(origine.etablisementOrigineExploitation).eq('etablissementOrigineExploitationRepriseTotalePartielle') and Value('id').of(origine.precedentExploitant.typePersonnePrecedentExploitant).eq('typePersonnePrecedentExploitantPM')) {
formFields['denomination_precedentExploitant']  = origine.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant;
}
}

// Cadre 13 - Effectif salarié

 if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')){
formFields['modifEtablissementOuvert_effectifSalarie']      = activite.etablissementEffectifSalariePresenceOui ? activite.cadreEffectifSalarie.etablissementEffectifSalarieNombre : ' ';
}

// Cadre 14 - Mise en location de biens ruraux

var bailleur = $p2agri.cadre14MiseEnLocationBiensRurauxGroup ;

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('bailleurBiensRuraux')){
  	if (bailleur.cadre14MiseEnLocationBiensRuraux.dateMiseLocation !== null) {
			var dateTmp = new Date(parseInt(bailleur.cadre14MiseEnLocationBiensRuraux.dateMiseLocation.getTimeInMillis()));
			var date = pad(dateTmp.getDate().toString());
			var month = dateTmp.getMonth() + 1;
			date = date.concat(pad(month.toString()));
			date = date.concat(dateTmp.getFullYear().toString());
			formFields['dateMiseEnLocation']          = date;
		} 
formFields['biensMisLocationTotalite'] = Value('id').of(bailleur.cadre14MiseEnLocationBiensRuraux.biensMisLocation).eq('totalite') ? true : false;
formFields['biensMisLocationPartie'] = Value('id').of(bailleur.cadre14MiseEnLocationBiensRuraux.biensMisLocation).eq('partie') ? true : false;
formFields['biensMisLocationPaiementBase'] = Value('id').of(bailleur.cadre14MiseEnLocationBiensRuraux.optionPaiementDecouple).eq('droitPaiementBase') ? true : false;
formFields['biensMisLocationRevenusFonciers'] = Value('id').of(bailleur.cadre14MiseEnLocationBiensRuraux.optionPaiementDecouple).eq('revenusFonciers') ? true : false;

// Adresse du bien loué
formFields['bienLoue_adresse_voie']  = (bailleur.adresseBienLoue.numeroVoieAdresseBienLoue != null ? bailleur.adresseBienLoue.numeroVoieAdresseBienLoue : '') 
																	+ ' ' + (bailleur.adresseBienLoue.indiceVoieAdresseBienLoue != null ? bailleur.adresseBienLoue.indiceVoieAdresseBienLoue : '')
																	+ ' ' + (bailleur.adresseBienLoue.typeVoieAdresseBienLoue != null ? bailleur.adresseBienLoue.typeVoieAdresseBienLoue : '')
																	+ ' ' + (bailleur.adresseBienLoue.nomVoieAdresseBienLoue != null ? bailleur.adresseBienLoue.nomVoieAdresseBienLoue : '')
																	+ ' ' + (bailleur.adresseBienLoue.voieComplementAdresseBienLoue != null ? bailleur.adresseBienLoue.voieComplementAdresseBienLoue : '')
																	+ ' ' + (bailleur.adresseBienLoue.voieDistributionSpecialeAdresseBienLoue != null ? bailleur.adresseBienLoue.voieDistributionSpecialeAdresseBienLoue : '');
formFields['bienLoue_adresse_codePostal']   = bailleur.adresseBienLoue.codePostalAdresseBienLoue;
formFields['bienLoue_adresse_commune']      = bailleur.adresseBienLoue.communeAdresseBienLoue;

// Preneur de bail
if (Value('id').of(bailleur.preneurBail.typePersonnePreneurBail).eq('typePersonnePreneurBailPP')) {
formFields['preneurBail_identification'] = (bailleur.preneurBail.exploitationNomNaissancePreneurBail != null ? bailleur.preneurBail.exploitationNomNaissancePreneurBail : '')
+ ' ' + (bailleur.preneurBail.exploitationNomUsagePreneurBail != null ? bailleur.preneurBail.exploitationNomUsagePreneurBail : '')
+ ' ' + (bailleur.preneurBail.exploitationPrenom1PreneurBail != null ? bailleur.preneurBail.exploitationPrenom1PreneurBail : '');
}
if (Value('id').of(bailleur.preneurBail.typePersonnePreneurBail).eq('typePersonnePreneurBailPM')) {
formFields['preneurBail_identification']  = bailleur.preneurBail.exploitationDenominationPreneurBail;
}
formFields['siren_preneurBail']  = bailleur.preneurBail.exploitationSirenPreneurBail.split(' ').join('');
} //Fin cadre des bailleurs ruraux
  
// Cadre 15 - Observations - renseignements complémentaires

var correspondance = $p2agri.cadre8RensCompGroup.cadre8RensComp;
var signataire = $p2agri.cadre9SignatureGroup.cadre9Signature;

if (Value('id').of(objet.objetModification).contains('dateActivite') and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 !== null or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 !== null)) {
if (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateObservations']          = date;
} else if (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateObservations']          = date;
}
formFields['observations']                 = "Modification de la date de début d'activité" + ' / ' +  (correspondance.formaliteObservations != null ? correspondance.formaliteObservations : '');
} else if (not Value('id').of(objet.objetModification).contains('dateActivite') and objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 == null and objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 == null) {	
formFields['observations']                 = correspondance.formaliteObservations != null ? correspondance.formaliteObservations : '';
}


// Cadre 16 - Adresse de correspondance

formFields['adresseCorrespondanceCadre_numero']     = Value('id').of(correspondance.adresseCorrespond).eq('domi') ? "4" : 
															((Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement') 
															or(Value('id').of(correspondance.adresseCorrespond).eq('prof') 
															and (objet.domicileEntreprise 
															or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
															and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))))) ? "8" :
															(Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') or Value('id').of(objet.objetModification).contains('dateActivite')) ? "9" : '');
formFields['adresseCorrespondanceCadre_cocheDom']   = Value('id').of(correspondance.adresseCorrespond).eq('domi') ? true : false;
															
formFields['adresseCorrespondanceCadre_cocheProf']  = (Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement') 
															or Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') 
															or (Value('id').of(correspondance.adresseCorrespond).eq('prof') 
															and (objet.domicileEntreprise 
															or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
															and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))))) ? true : false;															
															
formFields['adressesCorrespondanceAutre']                 = (Value('id').of(correspondance.adresseCorrespond).eq('autre') 
															or (Value('id').of(correspondance.adresseCorrespond).eq('prof') 
															and not Value('id').of(objet.objetModification).contains('dateActivite')
															and not objet.domicileEntreprise 
															and (not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
															or not Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')))) ? true : false;

if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
formFields['adresseCorrespondance_voie1']                 = correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance
															+ ' ' + (correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance : '')
															+ ' ' + (correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance : '')
															+ ' ' + (correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance : '')
															+ ' ' + (correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance : '');
formFields['adresseCorrespondance_voie2']                 = (correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance != null ? correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance : '')
															+ ' ' + (correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance != null ? correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance : '');
formFields['adresseCorrespondance_codePostal']            = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal;
formFields['adresseCorrespondance_commune']               = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('prof') 
			and not Value('id').of(objet.objetModification).contains('dateActivite')
			and not objet.domicileEntreprise 
			and (not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
			or not Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))) {
formFields['adresseCorrespondance_voie1']             = (adresseEntreprise.etablissementAdresseNumeroVoie != null ? adresseEntreprise.etablissementAdresseNumeroVoie : '')
														+ ' ' + (adresseEntreprise.etablissementAdresseIndiceVoie != null ? adresseEntreprise.etablissementAdresseIndiceVoie : '')
														+ ' ' + (adresseEntreprise.etablissementAdresseTypeVoie != null ? adresseEntreprise.etablissementAdresseTypeVoie : '')
														+ ' ' + (adresseEntreprise.etablissementAdresseNomVoie != null ? adresseEntreprise.etablissementAdresseNomVoie : '');
formFields['adresseCorrespondance_voie2']			  = (adresseEntreprise.etablissementAdresseComplementVoie != null ? adresseEntreprise.etablissementAdresseComplementVoie : '')
														+ ' ' + (adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null ? adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie : '');
formFields['adresseCorrespondance_codePostal']        = adresseEntreprise.etablissementAdresseCodePostal;
formFields['adresseCorrespondance_commune']           = adresseEntreprise.etablissementAdresseCommune;
}

formFields['telephone1']                                                             = correspondance.infosSup.formaliteTelephone1 != null ? correspondance.infosSup.formaliteTelephone1 : '';
formFields['telephone2']                                                             = correspondance.infosSup.formaliteTelephone2;
formFields['courriel']                                                               = correspondance.infosSup.formaliteFaxCourriel != null ? correspondance.infosSup.formaliteFaxCourriel : (correspondance.infosSup.telecopie != null ? correspondance.infosSup.telecopie : '');



// Cadre 17 - Diffusion informations

formFields['diffusion_information']  = Value('id').of(signataire.diffusionInfo).eq('formaliteNonDiffusionInformationOui') ? true : false;
formFields['non_diffusion_information']  = Value('id').of(signataire.diffusionInfo).eq('formaliteNonDiffusionInformationNon') ? true : false;

// Cadre 18 - Signataire

formFields['signataireDeclarant']                                                    = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
if (Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
formFields['signataireMandataire']                                                   = true;
formFields['signataireNom']										                     = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFields['signataireAdresse']                                                      = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '');
formFields['signataireAdresse2']                                                     = (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
																					  + ' ' + (signataire.adresseMandataire.dataCodePostalMandataire != null ?
																					  signataire.adresseMandataire.dataCodePostalMandataire : '')
																					  + ' ' + (signataire.adresseMandataire.villeAdresseMandataire  != null ?
																					 signataire.adresseMandataire.villeAdresseMandataire : '');
}
if (signataire.formaliteSignatureDate !== null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['signatureDate']          = date;
}
formFields['signatureLieu']                                                          = signataire.formaliteSignatureLieu;
formFields['estEIRL_oui']  = Value('id').of(objet.objetModification).contains('modifEIRL') ? true : false;
formFields['estEIRL_non']  = Value('id').of(objet.objetModification).contains('modifEIRL') ? false : true;
formFields['signature']                                                              = '';



                          // Intercalaire PEIRL AGRICOLE
								
//Cadre 1 - Déclaration ou modification d'affectation de patrimoine

formFields['formulaire_dependance_P2Agricole']   = true;
/* formFields['eirl_complete_p0PLME']  = false; */
formFields['declaration_initiale'] = Value('id').of(objet.objetModification).contains('modifEIRL') ? (Value('id').of(identite.optionEirl).eq('optionEirlNon') ? true : false) : false;
formFields['declaration_modification'] = (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') or Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('finEIRL') or objet.entrepriseEIRL) ? true : false;


// Cadre 2 - Rappel d'identification

formFields['eirl_siren']                                                  = identite.siren.split(' ').join('');
formFields['eirl_nomNaissance']                                           = identite.personneLieePersonnePhysiqueNomNaissance;
formFields['eirl_nomUsage']                                               = identite.personneLieePersonnePhysiqueNomUsage != null ? identite.personneLieePersonnePhysiqueNomUsage : '';
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFields['eirl_prenom']  = prenoms.toString();


// Cadre 3 - Déclaration d'affectation de patrimoine

var declarationAffectation = $p2agri.cadre4DeclarationAffectationPatrimoineGroup.cadreDeclarationAffectationPatrimoine;

if (Value('id').of(objet.objetModification).contains('modifEIRL') and Value('id').of(identite.optionEirl).eq('optionEirlNon')){ 
formFields['eirl_statutEIRL_declarationInitiale']= Value('id').of(declarationAffectation.eirlStatut).eq('eirlStatutDeclarationInitiale') ? true : false;
formFields['eirl_statutEIRL_reprise']= Value('id').of(declarationAffectation.eirlStatut).eq('eirlStatutReprise') ? true : false;
formFields['eirl_denomination'] = declarationAffectation.declarationPatrimoine.eirlDenomination;
if (declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable !== null) {
    var dateTmp = new Date(parseInt(declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable.getTimeInMillis()));
    var dateClotureEc = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateClotureEc = dateClotureEc.concat(pad(month.toString()));
    formFields['eirl_dateClotureExerciceComptable']          = dateClotureEc;
}
formFields['eirl_objet']  = declarationAffectation.declarationPatrimoine.eirlObjet;

if (Value('id').of(declarationAffectation.eirlStatut).eq('eirlStatutReprise')) {
formFields['eirl_precedentEIRLDenomination'] = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLDenomination;
formFields['eirl_precedentEIRLLieuImmatriculation']  = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLLieuImmatriculation;
formFields['eirl_precedentEIRLSIREN'] = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN != null ? (declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN.split(' ').join('')) : '';
}
}

// Cadre 4 - Rappel d'identification reltif à l'EIRL

var rappelIdentification = $p2agri.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreRappelIdentificationEIRL;

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') or Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('finEIRL') or objet.entrepriseEIRL) { 
formFields['eirl_rappelDenomination']                   = rappelIdentification.eirlRappelDenomination;
formFields['eirl_rappelLieuImmatriculation']            = rappelIdentification.eirlRappelLieuImmatriculation;

if (objet.cadreObjetModificationEIRL.memeAdresseEIRL or objet.entrepriseEIRL) {
formFields['eirl_rappelAdresse']                    = (adresseEntreprise.etablissementAdresseNumeroVoie != null ? adresseEntreprise.etablissementAdresseNumeroVoie : '')
													+ ' ' + (adresseEntreprise.etablissementAdresseIndiceVoie != null ? adresseEntreprise.etablissementAdresseIndiceVoie : '')
													+ ' ' + (adresseEntreprise.etablissementAdresseTypeVoie != null ? adresseEntreprise.etablissementAdresseTypeVoie : '')
													+ ' ' + (adresseEntreprise.etablissementAdresseNomVoie != null ? adresseEntreprise.etablissementAdresseNomVoie : '')
													+ ' ' + (adresseEntreprise.etablissementAdresseComplementVoie != null ? adresseEntreprise.etablissementAdresseComplementVoie : '')
													+ ' ' + (adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null ? adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie : '')
													+ ' ' + adresseEntreprise.etablissementAdresseCodePostal
													+ ' ' + adresseEntreprise.etablissementAdresseCommune;
} else if (not objet.cadreObjetModificationEIRL.memeAdresseEIRL) {
formFields['eirl_rappelAdresse']                    = (rappelIdentification.cadreEirlRappelAdresse.numeroAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.numeroAdresseEirl : '')
													+ ' ' + (rappelIdentification.cadreEirlRappelAdresse.indiceVoieAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.indiceVoieAdresseEirl : '')
													+ ' ' + (rappelIdentification.cadreEirlRappelAdresse.typeVoieAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.typeVoieAdresseEirl : '')
													+ ' ' + rappelIdentification.cadreEirlRappelAdresse.nomVoieAdresseEirl
													+ ' ' + (rappelIdentification.cadreEirlRappelAdresse.complementAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.complementAdresseEirl : '')
													+ ' ' + (rappelIdentification.cadreEirlRappelAdresse.distriutionSpecialeAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.distriutionSpecialeAdresseEirl : '')
													+ ' ' + rappelIdentification.cadreEirlRappelAdresse.codePostalAdresseEirl
													+ ' ' + rappelIdentification.cadreEirlRappelAdresse.communeAdresseEirl;
}
}

// Cadre 5 et 6- Déclaration de modification

var modifEIRL = $p2agri.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL;                                         

if(Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDenoEIRL')) {
if (modifEIRL.modifDateEIRL !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateDenoEIRL']          = dateModif;
}
formFields['new_DenoEIRL']                                                                  = modifEIRL.newDenoEIRL;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDateCloture')) {
if (modifEIRL.modifDateEIRL !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateClotureExerciceComptableEIRL']          = dateModif;
}                                         
if (modifEIRL.eirlNewDateClotureExerciceComptable !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.eirlNewDateClotureExerciceComptable.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    formFields['eirl_newDateClotureExerciceComptable']          = dateModif;
}
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifObjet')) {
if (modifEIRL.modifDateEIRL !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateObjetEIRL']          = dateModif;
}
formFields['new_objetEIRL']                                                                 = modifEIRL.newObjetEIRL;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDAP')) {
if (modifEIRL.modifDateEIRL !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateDAP']          = dateModif;
} 
formFields['modif_DAP']                                                                     = true;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse')) {
if (modifEIRL.modifDateEIRL !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateAdresseEIRL']          = dateModif;
}	
formFields['new_adresseEIRL']                           = (modifEIRL.newAdresseEIRL.numeroAdresseEirlNew != null ? modifEIRL.newAdresseEIRL.numeroAdresseEirlNew : '')
														+ ' ' + (modifEIRL.newAdresseEIRL.indiceVoieAdresseEirlNew != null ? modifEIRL.newAdresseEIRL.indiceVoieAdresseEirlNew : '')
														+ ' ' + (modifEIRL.newAdresseEIRL.typeVoieAdresseEirlNew != null ? modifEIRL.newAdresseEIRL.typeVoieAdresseEirlNew : '')
														+ ' ' + modifEIRL.newAdresseEIRL.nomVoieAdresseEirlNew;
formFields['new_adresseEIRL2']                          = (modifEIRL.newAdresseEIRL.complementAdresseEirlNew != null ? modifEIRL.newAdresseEIRL.complementAdresseEirlNew : '')
														+ ' ' + (modifEIRL.newAdresseEIRL.distriutionSpecialeAdresseEirlNew != null ? modifEIRL.newAdresseEIRL.distriutionSpecialeAdresseEirlNew : '')
														+ ' ' + modifEIRL.newAdresseEIRL.codePostalAdresseEirlNew
														+ ' ' + modifEIRL.newAdresseEIRL.communeAdresseEirlNew;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('11P') and objet.entrepriseEIRL) {
if (etablissement.modifDateAdresseEntreprise !== null) {
    var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateAdresseEIRL']          = date;
}
formFields['new_adresseEIRL']                             = (etablissement.adresseEtablissementNew.etablissementAdresseNumeroVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseNumeroVoieNew : '') 
														  + ' ' + (etablissement.adresseEtablissementNew.etablissementAdresseIndiceVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseIndiceVoieNew : '')
														  + ' ' + (etablissement.adresseEtablissementNew.etablissementAdresseTypeVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseTypeVoieNew : '')
														  + ' ' + (etablissement.adresseEtablissementNew.etablissementAdresseNomVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseNomVoieNew : '');
formFields['new_adresseEIRL2']							  = (etablissement.adresseEtablissementNew.etablissementAdresseComplementVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseComplementVoieNew : '')
														+ ' ' + (etablissement.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew : '')
														+ ' ' + etablissement.adresseEtablissementNew.etablissementAdresseCodePostalNew
														+ ' ' + etablissement.adresseEtablissementNew.etablissementAdresseCommuneNew;
} else if (objet.domicileEntreprise and objet.entrepriseEIRL) {
if (modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateAdresseEIRL']          = date;
}
formFields['new_adresseEIRL']                             = (newAdresseDom.personneLieeAdresseVoieNew != null ? newAdresseDom.personneLieeAdresseVoieNew : '') 
														  + ' ' + (newAdresseDom.personneLieeAdresseIndiceVoieNew != null ? newAdresseDom.personneLieeAdresseIndiceVoieNew : '')
														  + ' ' + (newAdresseDom.personneLieeAdresseTypeVoieNew != null ? newAdresseDom.personneLieeAdresseTypeVoieNew : '')
														  + ' ' + newAdresseDom.personneLieeAdresseNomVoieNew;
formFields['new_adresseEIRL2']							  = (newAdresseDom.personneLieeAdresseComplementAdresseNew != null ? newAdresseDom.personneLieeAdresseComplementAdresseNew : '')
														  + ' ' + (newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null ? newAdresseDom.personneLieeAdresseDistriutionSpecialeNew : '')
														  + ' ' + newAdresseDom.personneLieeAdresseCodePostalNew
														  + ' ' + newAdresseDom.personneLieeAdresseCommuneNew;
}

var finDAP = $p2agri.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.finEIRLGroup;
if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('finEIRL')) { 																							
if (finDAP.finEIRLDeclaration.modifDateFinEIRL !== null) {
    var dateTmp = new Date(parseInt(finDAP.finEIRLDeclaration.modifDateFinEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateFinEIRL']          = dateModif;
}
formFields['finEIRL_renonciationAvecPoursuite']  = Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).contains('finEIRL') ? (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLRenonciationAvecPoursuite') ? true : false) : false;
formFields['finEIRL_renonciationSansPoursuite']   = Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).contains('finEIRL') ? (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLRenonciationSansPoursuite') ? true : false) : false;
formFields['finEIRL_cessionPP']                    = Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).contains('finEIRL') ? (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLCessionPP') ? true : false) : false;
formFields['finEIRL_cessionPM']                    = Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).contains('finEIRL') ? (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLCessionPM') ? true : false) : false;
}	

//A SUPPRIMER
/* if (Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('poursuiteEIRL')) {
if (modifEIRL.poursuiteEIRLGroup.modifDatePoursuiteEIRL !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.poursuiteEIRLGroup.modifDatePoursuiteEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDatePoursuiteEIRL']          = dateModif;
}
formFields['modif_pouruiteEIRL']                                                            = true;
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFields['EIRL_poursuiteNomPrenom']                                                       = modifEIRL.poursuiteEIRLGroup.eIRLPoursuiteNom + ' ' + prenoms.toString();
}
*/

// Cadre 7 - Options fiscales EIRL

var impotEIRL = $p2agri.cadre4DeclarationAffectationPatrimoineGroup.cadre3OptionsFiscalesEIRL;

formFields['eirl_regimeFiscal_regimeImpositionBenefices_mBA']  = Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesMBA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rsBA']                                   = Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesRsBA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rnBA']                                   = Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesRnBA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_ffBA']                                   = Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesffBA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rfBA']                                   = Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesRfBA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_optionIS'] = Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesOptionIS') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rsIS']                                   = Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeIS).eq('eirlRegimeFiscalRegimeImpositionBeneficesRsIS') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rnIS']                                   = Value('id').of(impotEIRL.impositionBeneficesEIRL.regimeIS).eq('eirlRegimeFiscalRegimeImpositionBeneficesRnIS') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVA_remboursementForfaitaire']                     = Value('id').of(impotEIRL.optionsTVA.regimeTVA1).eq('eirlRegimeFiscalRegimeImpositionTVARemboursementForfaitaire') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVA_impositionObligatoire']                        = Value('id').of(impotEIRL.optionsTVA.regimeTVA1).eq('eirlRegimeFiscalRegimeImpositionTVAImpositionObligatoire') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVAOptionsParticulieres4']                         = Value('id').of(impotEIRL.optionsTVA.optionTVAvolontaire).eq('eirlRegimeFiscalRegimeImpositionTVAOptionsParticulieres4') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVAOptionsParticulieres3']                         = impotEIRL.optionsTVA.eirlRegimeFiscalRegimeImpositionTVAOptionsParticulieres3 ? true : false;
if(impotEIRL.optionsTVA.eirlRegimeFiscalDateClotureExerciceComptable !== null) {
	var dateTmp = new Date(parseInt(impotEIRL.optionsTVA.eirlRegimeFiscalDateClotureExerciceComptable.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['eirl_regimeFiscal_dateClotureExerciceComptable'] = date;
}

formFields['eirl_regimeFiscal_regimeImpositionTVAOptionDepotTrimestriel'] = Value('id').of(impotEIRL.optionsTVA.optionTVADepot).eq('eirlRegimeFiscalRegimeImpositionTVADepotTrimestriel') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVAOptionDepotMensuel']                             = Value('id').of(impotEIRL.optionsTVA.optionTVADepot).eq('eirlRegimeFiscalRegimeImpositionTVADepotMensuel') ? true : false;

formFields['eirl_regimeFiscal_regimeImpositionTVAOptionsBailleurRuraux']                         = Value('id').of(impotEIRL.optionsTVA.optionTVAvolontaire).eq('eirlRegimeFiscalRegimeImpositionTVAOptionsBailleurRuraux') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVA_rfTVA']                                        = Value('id').of(impotEIRL.optionsTVA.regimeTVA2).eq('eirlRegimeFiscalRegimeImpositionTVARfTVA') ? true : (Value('id').of(impotEIRL.optionsTVA.regimeTVA3).eq('eirlRegimeFiscalRegimeImpositionTVARfTVA') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBIC.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesMBIC') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBIC.regimeTVA1).eq('eirlRegimeFiscalRegimeImpositionTVARfTVA') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBIC.regimeTVA2).eq('eirlRegimeFiscalRegimeImpositionTVARfTVA') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBNC.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesmBNC') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBNC.regimeTVA).eq('eirlRegimeFiscalRegimeImpositionTVARfTVA') ? true : false))))));
formFields['eirl_regimeFiscal_regimeImpositionTVA_rsTVA']                                        = Value('id').of(impotEIRL.optionsTVA.regimeTVA2).eq('eirlRegimeFiscalRegimeImpositionTVARsTVA') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBIC.regimeTVA1).eq('eirlRegimeFiscalRegimeImpositionTVARsTVA') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBNC.regimeTVA).eq('eirlRegimeFiscalRegimeImpositionTVARsTVA') ? true : false));
formFields['eirl_regimeFiscal_regimeImpositionTVA_mrTVA']                                        = Value('id').of(impotEIRL.optionsTVA.regimeTVA2).eq('eirlRegimeFiscalRegimeImpositionTVAMrTVA') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBIC.regimeTVA1).eq('eirlRegimeFiscalRegimeImpositionTVAMrTVA') ? true : false);
formFields['eirl_regimeFiscal_regimeImpositionTVA_rnTVA']                                        = Value('id').of(impotEIRL.optionsTVA.regimeTVA3).eq('eirlRegimeFiscalRegimeImpositionTVARnTVA') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBIC.regimeTVA2).eq('eirlRegimeFiscalRegimeImpositionTVARnTVA') ? true : (Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBNC.regimeTVA).eq('eirlRegimeFiscalRegimeImpositionTVARnTVA') ? true : false));
formFields['eirl_regimeFiscal_regimeImpositionTVAOptionsParticulieres2']                         = impotEIRL.optionsTVA.eirlRegimeFiscalRegimeImpositionTVAOptionsParticulieres2 ? true : (impotEIRL.impositionAccessoire.impositionBeneficesBIC.eirlRegimeFiscalRegimeImpositionTVAOptionsParticulieres2 ? true : false);
formFields['eirl_regimeFiscal_regimeImpositionTVAOptionsParticulieres1']                         = impotEIRL.optionsTVA.eirlregimeFiscalRegimeImpositionTVAOptionsParticulieres1 ? true : (impotEIRL.impositionAccessoire.impositionBeneficesBIC.eirlregimeFiscalRegimeImpositionTVAOptionsParticulieres1 ? true : (impotEIRL.impositionAccessoire.impositionBeneficesBNC.eirlRegimeFiscalRegimeImpositionTVAOptionsParticulieres1 ? true : false));
formFields['eirl_regimeFiscal_regimeImpositionBenefices_mBIC']                                   = Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBIC.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesMBIC') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rsBIC']                                  = Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBIC.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesRsBIC') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rnBIC']                                  = Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBIC.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesRnBIC') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_dcBNC']                                  = Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBNC.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesDcBNC') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBeneficesOptionsParticulieres_optionCreanceDette'] = impotEIRL.impositionAccessoire.impositionBeneficesBNC.eirlRegimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionCreanceDette ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_mBNC']                                   = Value('id').of(impotEIRL.impositionAccessoire.impositionBeneficesBNC.regimeFiscalRegimeImpositionBenefice).eq('eirlRegimeFiscalRegimeImpositionBeneficesmBNC') ? true : false;


/*
 * Création du dossier avec volet social : ajout du cerfa avec volet social
 */
 
var cerfaDoc1 = nash.doc //
	.load('models/cerfa_11935-06_p2agri_avec_volet_social.pdf') //
	.apply(formFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));

/*
 * Création du dossier sans volet social : ajout du cerfa sans volet social

 
 var cerfaDoc2 = nash.doc //
	.load('models/cerfa_11935-06_p2agri_sans_volet_social.pdf') //
	.apply (formFields);
	cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));
 */
/*
 * Ajout de l'intercalaire PEIRL  avec option fiscale
 */
if (Value('id').of(objet.objetModification).contains('modifEIRL') or objet.entrepriseEIRL) {
	var peirlMEDoc1 = nash.doc //
		.load('models/cerfa_14216-05_peirl_agricole_avec_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(peirlMEDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire PEIRL sans option fiscale
 */
if (Value('id').of(objet.objetModification).contains('modifEIRL') or objet.entrepriseEIRL) {
	var peirlMEDoc2 = nash.doc //
		.load('models/cerfa_14216-05_peirl_agricole_sans_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(peirlMEDoc2.save('cerfa.pdf'));
}

function appendPj(fld) {
	fld.forEach(function (elm) {
        cerfaDoc1.append(elm);
    });
}

/*
 * Ajout des PJs
 */

// PJ Déclarant

var pj=$p2agri.cadre9SignatureGroup.cadre9Signature.soussigne;
var pjUser = [];
var metas = [];

function pushPjPreview(fld) {
	fld.forEach(function (elm) {
        pjUser.push(elm);
        metas.push({'name':'document', 'value': '/'+elm.getAbsolutePath()});
		metas.push({'name':'userAttachments', 'value': '/'+elm.getAbsolutePath()});
    });
}

if(Value('id').of(pj).contains('FormaliteSignataireQualiteDeclarant')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantSignataire);
}

if (Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire') 
	and (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P') 
	or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P'))) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantNonSignataire);
} 

// PJ Mandataire

if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPouvoir);
}

// PJ CONJOINT

if (Value('id').of($p2agri.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationPerso.objetSituationPerso).contains('30P') and $p2agri.cadre3ModificationConjointGroup.cadre3ModificationConjoint.statutConjointCollaborateur != null) {
 	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjChoixStatutConjoint);
}

// PJ Eirl

var pj=$p2agri.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEIRL.objetEIRL;
if ((Value('id').of($p2agri.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).contains('modifEIRL') and Value('id').of($p2agri.cadre1RappelIdentificationGroup.cadre1RappelIdentification.optionEirl).eq('optionEirlNon')) 
	or Value('id').of(pj).contains('modificationEIRL') 
	or $p2agri.cadre1ObjetModificationGroup.cadre1ObjetModification.entrepriseEIRL) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAP);
}
/* var pj=$p2pl.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEIRL.objetEIRL;
if($p2pl.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetDeclarationEIRL.declarationEIRL or Value('id').of(pj).contains('modificationEIRL') or $p2pl.cadre1ObjetModificationGroup.cadre1ObjetModification.entrepriseEIRL) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAP);
} */
var pj=$p2agri.cadre4DeclarationAffectationPatrimoineGroup.informationsComplementaires.contenuDAP;
if(Value('id').of(pj).contains('bienImmo') or Value('id').of($p2agri.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.contenuDAP).contains('bienImmo')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAPActeNotarie);
}

var pj=$p2agri.cadre4DeclarationAffectationPatrimoineGroup.informationsComplementaires.contenuDAP;
if(Value('id').of(pj).contains('bienRapportEvaluation') or Value('id').of($p2agri.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.contenuDAP).contains('bienRapportEvaluation')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAPRapportEvaluation);
}

var pj=$p2agri.cadre4DeclarationAffectationPatrimoineGroup.informationsComplementaires.contenuDAP;
if(Value('id').of(pj).contains('bienCommunIndivis') or Value('id').of($p2agri.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.contenuDAP).contains('bienCommunIndivis')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAPAccordTiers);
}

/*
 * Enregistrement du fichier (en mémoire)
 */

var finalDoc = cerfaDoc1.save('p2agri_Modification.pdf');

//Remove old metas before insert
var metasToDelete = [];
var recordMetas = nash.record.meta() != null ? nash.record.meta().metas : null;
var recordMetasSize = recordMetas != null ? recordMetas.size() : 0;

for (var i = 0; i < recordMetasSize; i++) {
if (recordMetas.get(i).name == 'document') {
    metasToDelete.push({'name':'document', 'value': recordMetas.get(i).value});
}
}
nash.record.removeMeta(metasToDelete);

log.info("Metas : {}", metas);

nash.record.meta(metas);

var data = [ spec.createData({
    id : 'record',
    label : 'Déclaration de modification d\'une entreprise agricole ou d\'une activité de bailleur de biens ruraux',
    description : 'Voici le formulaire obtenu à partir des données saisies.',
    type : 'FileReadOnly',
    value : [ finalDoc ]
}), spec.createData({
    id : 'attachments',
    label : 'Pièces jointes',
    description : 'Pièces jointes ajoutées au formulaire.',
    type : 'FileReadOnly',
    value : pjUser
}) ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Déclaration de modification d\'une entreprise agricole ou d\'une activité de bailleur de biens ruraux',
    groups : groups
});