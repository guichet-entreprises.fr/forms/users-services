//prepare info to send 

var adresseNew = $p2agri.cadre6EtablissementGroup.infoEtablissementNew.adresseEtablissementNew;
var adressePro = $p2agri.cadre1RappelIdentificationGroup.cadre1RappelIdentification.adresseEtablissementPrincipal;
var adresseEIRL = $p2agri.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreRappelIdentificationEIRL.cadreEirlRappelAdresse;
var adresseDom = $p2agri.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifDomicile.newAdresseDomicile;

var algo = "trouver Cfe";
var secteur1 = "Agricole";
var typePersonne = "PP";
var formJuridique = Value('id').of($p2agri.cadre1RappelIdentificationGroup.cadre1RappelIdentification.optionEirl).eq('optionEirlOui') ? "EIRL" : "EI";
var optionCMACCI = "NON";
var codeCommune = (Value('id').of($p2agri.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).eq('etablissement') 
				and Value('id').of($p2agri.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEtablissement.objetEtablissement).eq('11P')) ? adresseNew.etablissementAdresseCommuneNew.getId() : 
				($p2agri.cadre1ObjetModificationGroup.cadre1ObjetModification.domicileEntreprise ? adresseDom.personneLieeAdresseCommuneNew.getId() : 
				((Value('id').of($p2agri.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).eq('modifEIRL')
				and Value('id').of($p2agri.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEIRL.objetModificationEIRL).eq('modifAdresse')
				and not Value('id').of($p2agri.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).eq('situationPerso')
				and not Value('id').of($p2agri.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).eq('etablissement')
				and not Value('id').of($p2agri.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).eq('dateActivite')) ?	adresseEIRL.communeAdresseEirl.getId() : adressePro.etablissementAdresseCommune.getId()));

/* Ajout code pour afficher nom/dénomination dans BO */
var identite= $p2agri.cadre1RappelIdentificationGroup.cadre1RappelIdentification;
var modifIdentiteDeclarant = $p2agri.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle;
var nomDenomination = modifIdentiteDeclarant.modifIdentite.modifNouveauNomNaissance != null ? modifIdentiteDeclarant.modifIdentite.modifNouveauNomNaissance : identite.personneLieePersonnePhysiqueNomNaissance;
var prenomDenomination = (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom')) ? modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms[0] : identite.personneLieePersonnePhysiquePrenom1[0];
var denomination = nomDenomination + ' ' + prenomDenomination;
_log.info("nomDenomination is  {}", nomDenomination);
_log.info("prenomDenomination is  {}", prenomDenomination);
_log.info("denomination is  {}", denomination);

/** Other files */
function findUserAttachments() {
	var values = [];
	var metas = nash.record.meta();
	log.debug('meta list => {}', null == metas ? null : metas.metas);
	if (null != metas && null != metas.metas) {
		var valueMap = {};
		metas.metas.forEach(function (meta) {
			log.debug('  ---> meta {} : {}', meta.name, meta.value);
			if ('userAttachments' == meta.name) {
				valueMap[meta.value] = true;
			}
		})
		Object.keys(valueMap).forEach(function (elm) {
			values.push({
				id: elm.replaceAll('^(.*/)([^/]+)$', '$2'), //
				label: elm //
			});
		});
	}
	return values;
}

var data = nash.instance.load("output.xml");
data.bind("parameters",{
	"algo" : {
		"algo" : algo,
		"formJuridique" : formJuridique,
		"optionCMACCI" : "NON",
		"codeCommune" : codeCommune,
		"typePersonne" : typePersonne,
		"secteur1" : secteur1
	},
	"attachment" : {
		"cerfa" : '/4-signature/generated/proxyResult.files-1-PROXY_SIGNED_DOCUMENT.pdf',
		"others" : findUserAttachments()
	}
});
data.bind("result",{
	"funcId" : ""
})
data.bind("result", {
	"denomination" : denomination
});

/* Ancienne version :

var attachement= "/3-review/generated/generated.record-1-P2_Modification.pdf";
return spec.create({
		id: 'prepareSend',
		label: "Préparation de la recherche du destinataire",
		groups: [spec.createGroup({
				id: 'view',
				label: "Informations",
				data: [spec.createData({
						id: 'algo',
						label: "Algo",
						type: 'String',
						mandatory: true,
						value: algo
					}), spec.createData({
						id: 'secteur1',
						label: "Secteur",
						type: 'String',
						value: secteur1
					}), spec.createData({
						id: 'typePersonne',
						label: "Type personne",
						type: 'String',
						value: typePersonne
					}), spec.createData({
						id: 'formJuridique',
						label: "Forme juridique",
						type: 'String',
						value: formJuridique
					}),spec.createData({
						id: 'optionCMACCI',
						label: "Option CMACCI",
						type: 'String',
						value: optionCMACCI
					}),spec.createData({
						id: 'codeCommune',
						label: "Code commune",
						type: 'String',
						value: codeCommune
					}) ,spec.createData({
					    id: 'attachement',
					    label: "Pièce jointe",
					    type: 'String',
					    value: attachement
					})
				]
			})]
});*/