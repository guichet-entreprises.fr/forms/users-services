var attachment= "/4-signature/generated/proxyResult.files-1-PROXY_SIGNED_DOCUMENT.pdf";
var adresseNew = $p2p4modif.cadre6EtablissementGroup.modificationEtablissement.adresseEtablissementNew;
var adressePro = $p2p4modif.cadre1RappelIdentificationGroup.cadre1RappelIdentification.adresseEtablissementPrincipal;
var adresseEIRL = $p2p4modif.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreRappelIdentificationEIRL.cadreEirlRappelAdresse;
var adresseDom = $p2p4modif.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifDomicile.newAdresseDomicile;
var algo = "trouver destinataire";
var secteur1 = "Liberal";
var typePersonne = "PP";
var formJuridique = (($p2p4modif.cadre1RappelIdentificationGroup.cadre1RappelIdentification.formeJuridique
and (Value('id').of($p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL')
or Value('id').of($p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEIRL.objetEIRL).eq('finEIRL'))) 
or $p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.entrepriseEIRL
or $p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetDeclarationEIRL.declarationEIRL) ? "EIRL" : "EI";
var optionCMACCI = "NON";
var formalityWithPayment = false;
var listFraisDestinataires=[];
var listDistinctAuthorities =[];
var destFuncId2 = '';		 
var codeEDI2 = '';
var partnerLabel2 = '';

			 
_log.info("algo is {}",algo);
_log.info("secteur1 is {}",secteur1);
_log.info("typePersonne is {}",typePersonne);
_log.info("formJuridique is {}",formJuridique);
_log.info("optionCMACCI is {}",optionCMACCI);

var codeCommune = (Value('id').of($p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).eq('etablissement') 
				and Value('id').of($p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEtablissement.objetEtablissement).eq('11P')) ? adresseNew.etablissementAdresseCommuneNew.getId() : 
				($p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.domicileEntreprise ? adresseDom.personneLieeAdresseCommuneNew.getId() : 
				((Value('id').of($p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).eq('modifEIRL')
				and Value('id').of($p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEIRL.objetModificationEIRL).eq('modifAdresse')
				and not Value('id').of($p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).eq('situationPerso')
				and not Value('id').of($p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).eq('etablissement')
				and not Value('id').of($p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).eq('dateActivite')) ?	adresseEIRL.communeAdresseEirl.getId() : adressePro.etablissementAdresseCommune.getId()));


_log.info("codeCommune is {}",codeCommune);


//=================================> calcul des destinataires Début<=========================================
var args = {
    "secteur1" : secteur1,
	"typePersonne" : typePersonne,
	"formJuridique" : formJuridique,
	"optionCMACCI":optionCMACCI,
	"codeCommune": codeCommune
};

_log.info("serviceParams  {}",JSON.stringify(args));

// call directory service to find functional id of authority 
var response = nash.service.request('${directory.baseUrl}/v1/algo/{code}/execute',algo) //
        .connectionTimeout(10000) //
        .receiveTimeout(10000)
        .accept('application/json') //
        .post(args);

var receiverInfo = JSON.parse(response.asString());

_log.info("receiverInfo  is {}", receiverInfo);

_log.info("listAuthorities  is {}", receiverInfo.listAuthorities);
_log.info("type of details of listAuthorities is  {}", typeof receiverInfo.listAuthorities);
_log.info("listAuthorities.length is {}",  receiverInfo.listAuthorities.length);

//===============================> Récupérer laliste des autorités distincte (sans doublons pour construire le bloc destiné à la génération du regent XML et le XML-TC <==============================================

	//Début<=========================================	
	for(var i=0; i<receiverInfo.listAuthorities.length; i++){	
		if(listDistinctAuthorities.indexOf(receiverInfo.listAuthorities[i]['authority'])== -1){	
			listDistinctAuthorities.push(receiverInfo.listAuthorities[i]['authority']);
		}
	}	
	_log.info("listAuthorities.length is {}",  listDistinctAuthorities.length);
	_log.info("listDistinctAuthorities  is {}", listDistinctAuthorities);
	//Fin ===========================================>
	
	//Récupérer les informations des destinataires 
		//Début<=========================================
		_log.info("1er destinataire  is {}", listDistinctAuthorities[0]);
		var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', listDistinctAuthorities[0]) //
				   .connectionTimeout(10000) //
				   .receiveTimeout(10000) //
				   .accept('json') //
				   .get();
		//result
		var receiverInfo2 = response.asObject();

		// prepare all information of receiver to create data.xml

		var funcId = !receiverInfo2.entityId ? null : receiverInfo2.entityId;
		var funcLabel = !receiverInfo2.label ? null :receiverInfo2.label;
		var contactInfo = !receiverInfo2.details ? null : receiverInfo2.details;
		var email = !contactInfo.profile.email ? null : contactInfo.profile.email;
		var tel = !contactInfo.profile.tel ? null : contactInfo.profile.tel;
		
		if(receiverInfo.listAuthorities[0]['role'] == "CFE"){
			_log.info("*********Première autorité est un CFE******");	
			var codeEDI = !contactInfo.ediCode ? null : contactInfo.ediCode;
		}else{
			_log.info("*********Première autorité est un TDR******");
			var codeEDI2 = !contactInfo.ediCode ? null : contactInfo.ediCode;	
		}
		
		var partnerLabel = !contactInfo.profile.address.recipientName ? null : contactInfo.profile.address.recipientName;

		if(listDistinctAuthorities.length == 2){
			_log.info("2eme destinataire  is {}", listDistinctAuthorities[1]);
			_log.info("******récupération du second destinataire******");
			var response2 = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', listDistinctAuthorities[1]) //
				   .connectionTimeout(10000) //
				   .receiveTimeout(10000) //
				   .accept('json') //
				   .get();
			//result
			var receiverInfo3 = response2.asObject();

			// prepare all information of receiver to create data.xml
			destFuncId2 = listDistinctAuthorities[1];
			contactInfo = !receiverInfo3.details ? null : receiverInfo3.details;

			if(receiverInfo.listAuthorities[1]['role'] == "CFE"){
				_log.info("*********Deuxième autorité est un CFE******");	
				var codeEDI = !contactInfo.ediCode ? null : contactInfo.ediCode;
			}else{
				_log.info("*********Deuxième autorité est un TDR******");	
				var codeEDI2 = !contactInfo.ediCode ? null : contactInfo.ediCode;	
			}

			 partnerLabel2 = !contactInfo.profile.address.recipientName ? null : contactInfo.profile.address.recipientName;
			
		}	
		var regentXmlTcBloc = spec.createGroup({
									id: 'dataGroup',
									label: "Contenu d'une autorité compétente",
									data: [spec.createData({
											id: 'destFuncId',
											label: "Le code de l'autorité compétente",
											description: 'Code interne',
											type: 'String',
											mandatory: true,
											value: funcId
										}), spec.createData({
											id: 'labelFunc',
											label: "Le libellé fonctionnelle de l'autorité compétente",
											type: 'String',
											mandatory : true,
											value: funcLabel
										}), spec.createData({
											id: 'email',
											label: "L'adresse email de l'autorité compétente",
											type: 'email',
											mandatory:true,
											value: email
										}),	spec.createData({
											id: 'emailPj',
											label: "Le chemin de la pièce jointe",
											type: 'String',
											value: attachment
									}),   spec.createData({
											id: 'tel',
											label: "telephone",
											type: 'String',
											value: tel
									}), spec.createData({
											id: 'partnerLabel',
											label: "Nom autorité",
											type: 'String',
											value: partnerLabel
									}),spec.createData({
											id: 'codeEDI',
											label: "code EDI",
											type: 'String',
											value: codeEDI
									}),
									//début de l'ajout
									spec.createData({
											id: 'destFuncId2',
											label: "code de la deuxième autorité",
											type: 'String',
											value: destFuncId2
									}),spec.createData({
											id: 'codeEDI2',
											label: "code EDI de la deuxième autorité",
											type: 'String',
											value: codeEDI2
									}),spec.createData({
											id: 'partnerLabel2',
											label: "Nom de la deuxième autorité",
											type: 'String',
											value: partnerLabel2
									})
									//fin de l'ajout	
									]
								});
	//Fin ===========================================>	
//=================================>  calcul des destinataires Fin <=========================================

	//=================================> Partie qui sera mise par le rédacteur pour conditionner le couple frais autoprité dans le cas où la formalité est payante et donc le bloc est conditionné

	//Début<=========================================	
//Condition pour dire qu'une formalité contient un paiement
if((Value('id').of($p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).contains('modifEIRL') and not Value('id').of($p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEIRL.objetEIRL).eq('finEIRL')) or $p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.entrepriseEIRL){
	//Mettre le Flag de paiement à "vrai"
	_log.info("*********Paiement existant sur la formalité!!******");
	formalityWithPayment =  true;
	
	for(var i=0; i<receiverInfo.listAuthorities.length; i++){
	var listCoupleAutoriteFrais=[];

		// Dans ce cas nous avons un seul produit destioné aux greffes qui sont des TDR, on parcours la liste des authorités calculés puis on test chaque role retourné, quand on trouve un TDR on va lui affecter le code du frais de dépot qui nous interesse : 
		if(receiverInfo.listAuthorities[i]['role'] == "TDR"){
			//Récupérer les informations de chaque autorité calculée pour récupéré son libellé
			_log.info("Authority Code is {}", receiverInfo.listAuthorities[i]['authority']);
			var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', receiverInfo.listAuthorities[i]['authority']) //
						   .connectionTimeout(10000) //
						   .receiveTimeout(10000) //
						   .accept('json') //
						   .get();   
						   
			var authority = response.asObject();
			
			var funcLabel = !authority.label ? null :authority.label;

			_log.info("funcLabel is {}", funcLabel);
			
			if (Value('id').of($p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).contains('modifEIRL') and $p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetDeclarationEIRL.declarationEIRL) {
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRSEI02"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
			} else if (Value('id').of($p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') or $p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.entrepriseEIRL) {
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "DDMEI01"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
			}
		}
	}

	_log.info("listCoupleAutoriteFrais  is {}", listCoupleAutoriteFrais);
	//=================================> Partie qui sera mise par le rédacteur pour conditionner le couple frais autoprité Fin=========================================

	//=================================> Partie générique pour générer le specCreate à passer à paiement Début=========================================										
	for(var i=0; i<listCoupleAutoriteFrais.length; i++){
		//Récupérer les informations du frais
		
		var filters = [];
		filters.push("details.entityId:"+listCoupleAutoriteFrais[i]['frais']);
		_log.info(" -- filters -- is {}", filters);
		var response = nash.service.request('${directory.baseUrl}/private/v1/repository/{repositoryId}', "frais") //
				   .connectionTimeout(10000) //
				   .receiveTimeout(10000) //
				   .accept('json') //
				   .param("filters",filters)
				   .get();
				   
		var receiverInfo = response.asObject();
		var details = !receiverInfo.content[0].details ? null : receiverInfo.content[0].details;
		_log.info(" -- details -- is {}", details);
		
		if(details != null){
			
			var prix = !details.prix ? null : details.prix;
			_log.info(" -- prix -- is {}", prix);
			var reseau = !details.reseau ? null : details.reseau;
			_log.info(" -- reseau -- is {}", reseau);
			var label = !details.label ? null : details.label;
			_log.info(" -- label -- is {}", label);
		}else{
			_log.error(" -- Error : code with the id {} not found in the repository with the id {}", listCodesFrais[i], "frais");
			return spec.create({
								id : 'codeFraisNotFound',
								label : 'Erreur lors de la recherche des codes de frais',
								groups : [ spec.createGroup({
									id : 'error',
									label : 'Erreur lors de la recherche des codes de frais',
									description : "Une erreur lors de la recherche des codes de frais dans le référentiel, veuillez contacter le support à l'adresse: support@guichet-entreprises.fr.",
									data : []
								}) ]
							});					
		}
			
		//Construction de la page à afficher au user
		var indiceFormalitePayante = spec.createGroup({
										'id':'flagFormalitePayante',
										'label':"la formalité est-elle payante?",
										'data':[
											spec.createData({
											'id': 'flagPayment',
											'label': 'flag du paiement :',
											'type': 'Boolean',
											'value':formalityWithPayment
											})
										]
									});
		listFraisDestinataires.push(indiceFormalitePayante);	
		
		var fraisDestination = spec.createGroup({
								'id':'fraisDestinations['+i+']',
								'label':"Frais à payer",
								'minOccurs':"0",
								'maxOccurs':listCoupleAutoriteFrais.length,
								'data':[
										spec.createData({
											'id': 'authorityCode',
											'label': 'Authorité code :',
											'type': 'StringReadOnly',
											'value':listCoupleAutoriteFrais[i]['authorityCode']
											}),
										spec.createData({
											'id': 'authorityLabel',
											'label': 'Authorité label :',
											'type': 'StringReadOnly',
											'value':listCoupleAutoriteFrais[i]['authorityLabel']
											}),
										spec.createData({
											'id': 'fraisCode',
											'label': 'frais code :',
											'type': 'StringReadOnly',
											'value':listCoupleAutoriteFrais[i]['frais']
											}),
										spec.createData({
											'id': 'fraisLabel',
											'label': 'frais label :',
											'type': 'StringReadOnly',
											'value':label
											}),
										spec.createData({
											'id': 'fraisPrix',
											'label': 'frais prix :',
											'type': 'StringReadOnly',
											'value':""+prix
											}),										
								]
							});
		listFraisDestinataires.push(fraisDestination);
	}
	_log.info("listFraisDestinataires payant is {}", listFraisDestinataires);
}else{
	//Si la formalité n'est pas payante il ne faut afficher aucun frais et passer directement la step finish
	_log.info("***********Formalité n'est pas payante***************!!");
	var indiceFormalitePayante = spec.createGroup({
										'id':'flagFormalitePayante',
										'label':"la formalité est-elle payante?",
										'data':[
											spec.createData({
											'id': 'flagPayment',
											'label': 'flag du paiement :',
											'type': 'Boolean',
											'value':formalityWithPayment
											})
										]
									});
		listFraisDestinataires.push(indiceFormalitePayante);	
		
	var fraisDestination = spec.createGroup({
								'id':'listeFrais',
								'label':"Frais à payer",
								'data':[spec.createData({
											'id': 'aucunFrais',											
											'type': 'StringReadOnly',
											'value':"Aucun service payant n'est inclus dans votre procédure."
											})]
							});
		listFraisDestinataires.push(fraisDestination);
			_log.info("listFraisDestinataires non payant is {}", listFraisDestinataires);
}
//=================================> Partie générique pour générer le specCreate à passer à paiement Fin=========================================

//===========> Partie contenant les informations utilisateur à passer au paiement !A remplire par le redacteur de la formalité! =========================
var lastName = ($p2p4modif.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiqueNomUsage  != "" && $p2p4modif.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiqueNomUsage  != null)  ? $p2p4modif.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiqueNomUsage  :  $p2p4modif.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiqueNomNaissance;
var firstName = $p2p4modif.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiquePrenom1[0];
var civility = " ";

var infoUser = spec.createGroup({
				id: 'infoUser',
				label: "Information de l'utilisateur",
				data: [spec.createData({
							id: 'userLastName',
							label: "Nom de l'utilisateur",
							description: "Nom de l'utilisateur",
							type: 'String',
							mandatory: true,
							value: lastName
									}),
						spec.createData({
							id: 'userFirstName',
							label: "Prénom de l'utilisateur",
							type: 'String',
							mandatory : true,
							value: firstName
									}),
						spec.createData({
							id: 'civility',
							label: "Civilité",
							type: 'String',
							mandatory:true,
							value: civility
									})	
					]
			});
//===========> Fin de la Partie contenant les informations utilisateur à passer au paiement !A remplire par le redacteur de la formalité! =========================

return spec.create({
    id : 'computeDestinataire',
    label : "Génération de la liste des frais pour le paiement",
	groups : [ spec.createGroup({
					id : 'information',
					label:"Liste des frais à payer",
					data : listFraisDestinataires
				}),
				regentXmlTcBloc,
				infoUser
	]
});