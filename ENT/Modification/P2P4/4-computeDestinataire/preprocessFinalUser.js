var listFraisDestinataires = [];
var formalityWithPayment = _INPUT_.information.flagFormalitePayante.flagPayment;
_log.info("formalityWithPayment  is {}", formalityWithPayment);

//Si laformalité est payante on affiche à l'utilisateur la liste des frais  à payer
if(formalityWithPayment == true){
	_log.info("Entrée dans la boucle pour affichage final des frais");
	_log.info("_INPUT_  is {}", _INPUT_);
	
	
	var fraisDestination = spec.createGroup({
								'id':'paymentFlag',
								'label':"Frais à payer pour le dossier",
								'data':[spec.createData({
											'id': 'fraisPayant',											
											'type': 'StringReadOnly',
											'value':"Pour le traitement de votre dossier, les paiements suivants devront être effectués :"
											})]
							});
	listFraisDestinataires.push(fraisDestination);
	

	for(var i=0; i<_INPUT_.information.fraisDestinations.size(); i++){
		
		var coupleFraisDestination = _INPUT_.information.fraisDestinations[i];
		_log.info("coupleFraisDestination  is {}", coupleFraisDestination);

		var authorityCode = coupleFraisDestination.authorityCode;
		_log.info("authorityCode  is {}", authorityCode);	
		
		var authorityLabel = coupleFraisDestination.authorityLabel;
		_log.info("authorityLabel  is {}", authorityLabel);	
			
		var fraisCode = coupleFraisDestination.fraisCode;
		_log.info("fraisCode  is {}", fraisCode);	
			

		var fraisLabel = coupleFraisDestination.fraisLabel;
		_log.info("fraisLabel  is {}", fraisLabel);	
			
		var	fraisPrix = coupleFraisDestination.fraisPrix;

		_log.info("fraisPrix  is {}", fraisPrix);

		var fraisDestination = spec.createGroup({
								'id':'fraisDestinations'+i,
								'label':"Frais à payer pour l'autorité "+ authorityLabel,
								'data':[
										spec.createData({
											'id': 'authorityLabel',
											'label': 'Autorité à payer :',
											'type': 'StringReadOnly',
											'value': authorityLabel
											}),
										spec.createData({
											'id': 'frais',
											'label': 'Service à payer :',
											'type': 'StringReadOnly',
											'value': fraisLabel
											}),
										spec.createData({
											'id': 'fraisPrix',
											'label': 'Montant à payer (en euros) :',
											'type': 'StringReadOnly',
											'value': ""+fraisPrix
											})						
								]
							});
		listFraisDestinataires.push(fraisDestination);
	}


}else{
	_log.info("Afficher message de gratuité des services de la formalité");
	
	
	var fraisDestination = spec.createGroup({
								'id':'paymentFlag',
								'label':"Frais à payer pour le dossier",
								'data':[spec.createData({
											'id': 'aucunFrais',											
											'type': 'StringReadOnly',
											'value':"Aucun service payant n'est inclus dans votre procédure."
											})]
							});
		listFraisDestinataires.push(fraisDestination);
}

return spec.create({
    id : 'listFrais',
    label : "Liste des services à payer",
	groups : [ spec.createGroup({
					id : 'fraisInformation',
					label:"Liste des frais à payer",
					data : listFraisDestinataires
				})
	]
});