var formFields = {};
var cerfaFields = {};
function pad(s) { return (s < 10) ? '0' + s : s; }

// Cadre 0 - Objet de la formalité

var objet= $p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification;

formFields['p2p4modif']                                              = true;
formFields['activiteLiberale']                                       = true;
formFields['activiteArtisanale']                                    = false;
formFields['activiteCommerciale']                                    = false;

formFields['p2p4rad']                                                = false;

// Cadre 1 - Rappel d'identification

var identite= $p2p4modif.cadre1RappelIdentificationGroup.cadre1RappelIdentification;

formFields['siren']                                                  = identite.siren.split(' ').join('');
formFields['personneLiee_personnePhysique_nomNaissance']             = identite.personneLieePersonnePhysiqueNomNaissance;
formFields['personneLiee_personnePhysique_nomUsage']                 = identite.personneLieePersonnePhysiqueNomUsage != null ? identite.personneLieePersonnePhysiqueNomUsage : '';
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFields['personneLiee_personnePhysique_prenom1']                                      = prenoms.toString();

if (identite.personneLieePersonnePhysiqueDateNaissance !== null) {
    var dateTmp = new Date(parseInt(identite.personneLieePersonnePhysiqueDateNaissance.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['personneLiee_personnePhysique_dateNaissance']          = date;
}
formFields['personneLiee_personnePhysique_lieuNaissanceDepartement'] = identite.personneLieePPLieuNaissanceDepartement != null ? identite.personneLieePPLieuNaissanceDepartement.getId() : '';
formFields['personneLiee_personnePhysique_lieuNaissanceCommune']     = (identite.personneLieePPLieuNaissanceCommune != null ?  identite.personneLieePPLieuNaissanceCommune : identite.personneLieePPLieuNaissanceVille) + ' / ' + identite.personneLieePPLieuNaissancePays;
formFields['personneLiee_personnePhysique_lieuDepotImpot']           = identite.personneLieePersonnePhysiqueLieuDepotImpot;

// Cadres 3

var modifIdentiteDeclarant = $p2p4modif.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle;
    
if (modifIdentiteDeclarant.modifIdentite.modifDateIdentite !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifIdentite.modifDateIdentite.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateIdentite']          = date;
}
formFields['modifNouveauNomNaissance']                               = (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P') or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) ? (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifNomNaissance') ? modifIdentiteDeclarant.modifIdentite.modifNouveauNomNaissance : identite.personneLieePersonnePhysiqueNomNaissance) : '';
formFields['modifNouveauNomUsage']                                   = Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P') ? modifIdentiteDeclarant.modifIdentite.modifNouveauNomUsage : (Value('id').of(objet.objetModification).contains('10P') ? identite.personneLieePersonnePhysiqueNomUsage : '');

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P') 
	or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) {
if (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom')) {	
var prenoms=[];
for ( i = 0; i < modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms.size() ; i++ ){prenoms.push(modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms[i]);}                            
formFields['modifNouveauPrenoms']                                      = prenoms.toString();
} else if (not Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom')) {
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFields['modifNouveauPrenoms']                                      = prenoms.toString();
}
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P')) {
if (modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateDomicile']          = date;
}

var newAdresseDom = $p2p4modif.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifDomicile.newAdresseDomicile;
formFields['personneLiee_adresse_voie']                             = (newAdresseDom.personneLieeAdresseVoieNew != null ? newAdresseDom.personneLieeAdresseVoieNew : '') 
																	+ ' ' + (newAdresseDom.personneLieeAdresseIndiceVoieNew != null ? newAdresseDom.personneLieeAdresseIndiceVoieNew : '')
																	+ ' ' + (newAdresseDom.personneLieeAdresseTypeVoieNew != null ? newAdresseDom.personneLieeAdresseTypeVoieNew : '')
																	+ ' ' + (newAdresseDom.personneLieeAdresseNomVoieNew != null ? newAdresseDom.personneLieeAdresseNomVoieNew : '')
																	+ ' ' + (newAdresseDom.personneLieeAdresseComplementAdresseNew != null ? newAdresseDom.personneLieeAdresseComplementAdresseNew : '')
																	+ ' ' + (newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null ? newAdresseDom.personneLieeAdresseDistriutionSpecialeNew : '');
formFields['personneLiee_adresse_codePostal']                       = newAdresseDom.personneLieeAdresseCodePostalNew;
formFields['personneLiee_adresse_commune']                          = newAdresseDom.personneLieeAdresseCommuneNew;
} else if (objet.entrepriseDomicile) {
var newAdresseDom2 = $p2p4modif.cadre6EtablissementGroup.modificationEtablissement;
if (newAdresseDom2.modifDateAdresseEntreprise !== null) {
    var dateTmp = new Date(parseInt(newAdresseDom2.modifDateAdresseEntreprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateDomicile']          = date;
}

formFields['personneLiee_adresse_voie']                             = (newAdresseDom2.adresseEtablissementNew.etablissementAdresseNumeroVoieNew != null ? newAdresseDom2.adresseEtablissementNew.etablissementAdresseNumeroVoieNew : '') 
																	+ ' ' + (newAdresseDom2.adresseEtablissementNew.etablissementAdresseIndiceVoieNew != null ? newAdresseDom2.adresseEtablissementNew.etablissementAdresseIndiceVoieNew : '')
																	+ ' ' + (newAdresseDom2.adresseEtablissementNew.etablissementAdresseTypeVoieNew != null ? newAdresseDom2.adresseEtablissementNew.etablissementAdresseTypeVoieNew : '')
																	+ ' ' + (newAdresseDom2.adresseEtablissementNew.etablissementAdresseNomVoieNew != null ? newAdresseDom2.adresseEtablissementNew.etablissementAdresseNomVoieNew : '')
																	+ ' ' + (newAdresseDom2.adresseEtablissementNew.etablissementAdresseComplementVoieNew != null ? newAdresseDom2.adresseEtablissementNew.etablissementAdresseComplementVoieNew : '')
																	+ ' ' + (newAdresseDom2.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew != null ? newAdresseDom2.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew : '');
formFields['personneLiee_adresse_codePostal']                       = newAdresseDom2.adresseEtablissementNew.etablissementAdresseCodePostalNew;
formFields['personneLiee_adresse_commune']                          = newAdresseDom2.adresseEtablissementNew.etablissementAdresseCommuneNew;
}

// Cadre 4- Conjoint

var conjoint = $p2p4modif.cadre3ModificationConjointGroup.cadre3ModificationConjoint;

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('30P')) {
if (Value ('id').of(conjoint.objetModifConjoint).eq('modifMentionNouveauConjoint')) {
if (conjoint.modifDateConjoint !== null and Value ('id').of(conjoint.objetModifConjoint).eq('modifMentionNouveauConjoint')) {
    var dateTmp = new Date(parseInt(conjoint.modifDateConjoint.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateConjoint']          = date;
}
formFields['modifNomNaissanceConjoint']                                              = conjoint.modifNomNaissanceConjoint;
formFields['modifNomUsageConjoint']                                                  = conjoint.modifNomUsageConjoint != null ? conjoint.modifNomUsageConjoint : '';
var prenoms=[];
for ( i = 0; i < conjoint.modifPrenomConjoint.size() ; i++ ){prenoms.push(conjoint.modifPrenomConjoint[i]);}                            
formFields['modifPrenomConjoint']                                      = prenoms.toString();
if ($p2p4modif.cadre3ModificationConjointGroup.cadre3ModificationConjoint.modifDateNaissanceConjoint !== null) {
    var dateTmp = new Date(parseInt($p2p4modif.cadre3ModificationConjointGroup.cadre3ModificationConjoint.modifDateNaissanceConjoint.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateNaissanceConjoint']          = date;
}
formFields['modifLieuNaissanceDepartementConjoint']                                  = conjoint.modifLieuNaissanceDepartementConjoint != null ? conjoint.modifLieuNaissanceDepartementConjoint.getId() : '';
formFields['modifLieuNaissanceCommuneConjoint']                                      = conjoint.modifLieuNaissanceCommuneConjoint != null ? conjoint.modifLieuNaissanceCommuneConjoint : (conjoint.modifLieuNaissanceVilleConjoint + ' / ' + conjoint.modifLieuNaissancePaysConjoint);
formFields['modifMentionNouveauConjoint']                                            = true;
}

formFields['suppressionMentionConjoint']           = Value ('id').of(conjoint.objetModifConjoint).eq('suppressionMentionConjoint') ? true : false;
if (conjoint.modifDateConjoint !== null and Value ('id').of(conjoint.objetModifConjoint).eq('suppressionMentionConjoint')) {
    var dateTmp = new Date(parseInt(conjoint.modifDateConjoint.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['suppresionDateConjoint']          = date;
}
}

var etablissement = $p2p4modif.cadre6EtablissementGroup.modificationEtablissement;

// Cadre 5 - EIRL

if ($p2p4modif.cadre4DeclarationAffectationPatrimoineGroup.cadreDeclarationAffectationPatrimoine.modifDateDeclarationEIRL !== null) {
    var dateTmp = new Date(parseInt($p2p4modif.cadre4DeclarationAffectationPatrimoineGroup.cadreDeclarationAffectationPatrimoine.modifDateDeclarationEIRL.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date1;
} else if ($p2p4modif.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.modifDateEIRL !== null) {
    var dateTmp = new Date(parseInt($p2p4modif.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.modifDateEIRL.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date2;
}  /*else if ($p2p4modif.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.poursuiteEIRLGroup.modifDatePoursuiteEIRL !== null) {
    var dateTmp = new Date(parseInt($p2p4modif.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.poursuiteEIRLGroup.modifDatePoursuiteEIRL.getTimeInMillis()));
    var date3 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date3 = date3.concat(pad(month.toString()));
	date3 = date3.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date3;
}*/  else if ($p2p4modif.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.finEIRLGroup.finEIRLDeclaration.modifDateFinEIRL !== null) {
    var dateTmp = new Date(parseInt($p2p4modif.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.finEIRLGroup.finEIRLDeclaration.modifDateFinEIRL.getTimeInMillis()));
    var date4 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date4 = date4.concat(pad(month.toString()));
	date4 = date4.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date4;
}	else if (objet.entrepriseEIRL and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('11P') and etablissement.modifDateAdresseEntreprise !== null) {
    var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
    var date5 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date5 = date5.concat(pad(month.toString()));
	date5 = date5.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date5;
} else if (objet.entrepriseEIRL and modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
    var date6 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date6 = date6.concat(pad(month.toString()));
	date6 = date6.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date6;
} 
formFields['eirl_immatriculation']                                                   = Value('id').of(objet.objetModification).contains('modifEIRL') ? (objet.cadreObjetDeclarationEIRL.declarationEIRL ? true : false) : false;
formFields['eirl_modification']                                                      = (Value('id').of(objet.objetModification).contains('modifEIRL') or objet.entrepriseEIRL) ? (objet.cadreObjetDeclarationEIRL.declarationEIRL ? false : true) : false;

// Cadre 6 - Etablissement 


var etablissement = $p2p4modif.cadre6EtablissementGroup.modificationEtablissement;

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).contains('11P')) {
formFields['adresseProfessionnelleOui']                              = (objet.entrepriseDomicile or objet.domicileEntreprise or etablissement.adresseProfessionnelleOui) ? true : false;
formFields['adresseProfessionnelleNon']                              = (objet.entrepriseDomicile or objet.domicileEntreprise or etablissement.adresseProfessionnelleOui) ? false : true;
if (etablissement.modifDateAdresseEntreprise !== null) {
    var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateAdresseEntreprise']          = date;
}
formFields['entrepriseLiee_adresse_nomVoie']                             = (etablissement.adresseEtablissementNew.etablissementAdresseNumeroVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseNumeroVoieNew : '') 
																	+ ' ' + (etablissement.adresseEtablissementNew.etablissementAdresseIndiceVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseIndiceVoieNew : '')
																	+ ' ' + (etablissement.adresseEtablissementNew.etablissementAdresseTypeVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseTypeVoieNew : '')
																	+ ' ' + (etablissement.adresseEtablissementNew.etablissementAdresseNomVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseNomVoieNew : '')
																	+ ' ' + (etablissement.adresseEtablissementNew.etablissementAdresseComplementVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseComplementVoieNew : '')
																	+ ' ' + (etablissement.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew : '');
formFields['entrepriseLiee_adresse_codePostal']                       = etablissement.adresseEtablissementNew.etablissementAdresseCodePostalNew;
formFields['entrepriseLiee_adresse_commune']                          = etablissement.adresseEtablissementNew.etablissementAdresseCommuneNew;
} else if (objet.domicileEntreprise) {
formFields['adresseProfessionnelleOui']                              = true;
formFields['adresseProfessionnelleNon']                              = false;
if (modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateAdresseEntreprise']          = date;
}
formFields['entrepriseLiee_adresse_nomVoie']                             = (newAdresseDom.personneLieeAdresseVoieNew != null ? newAdresseDom.personneLieeAdresseVoieNew : '') 
																	+ ' ' + (newAdresseDom.personneLieeAdresseIndiceVoieNew != null ? newAdresseDom.personneLieeAdresseIndiceVoieNew : '')
																	+ ' ' + (newAdresseDom.personneLieeAdresseTypeVoieNew != null ? newAdresseDom.personneLieeAdresseTypeVoieNew : '')
																	+ ' ' + (newAdresseDom.personneLieeAdresseNomVoieNew != null ? newAdresseDom.personneLieeAdresseNomVoieNew : '')
																	+ ' ' + (newAdresseDom.personneLieeAdresseComplementAdresseNew != null ? newAdresseDom.personneLieeAdresseComplementAdresseNew : '')
																	+ ' ' + (newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null ? newAdresseDom.personneLieeAdresseDistriutionSpecialeNew : '');
formFields['entrepriseLiee_adresse_codePostal']                       = newAdresseDom.personneLieeAdresseCodePostalNew;
formFields['entrepriseLiee_adresse_commune']                          = newAdresseDom.personneLieeAdresseCommuneNew;
}

var adresseEntreprise = $p2p4modif.cadre1RappelIdentificationGroup.cadre1RappelIdentification.adresseEtablissementPrincipal;

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('11P') or objet.domicileEntreprise) {
var adresseEntreprise = $p2p4modif.cadre1RappelIdentificationGroup.cadre1RappelIdentification.adresseEtablissementPrincipal;
formFields['modifAncienneAdresseEntreprise_voie']                                    = (adresseEntreprise.etablissementAdresseNumeroVoie != null ? adresseEntreprise.etablissementAdresseNumeroVoie : '')
																						+ ' ' + (adresseEntreprise.etablissementAdresseIndiceVoie != null ? adresseEntreprise.etablissementAdresseIndiceVoie : '')
																						+ ' ' + (adresseEntreprise.etablissementAdresseTypeVoie != null ? adresseEntreprise.etablissementAdresseTypeVoie : '')
																						+ ' ' + (adresseEntreprise.etablissementAdresseNomVoie != null ? adresseEntreprise.etablissementAdresseNomVoie : '')
																						+ ' ' + (adresseEntreprise.etablissementAdresseComplementVoie != null ? adresseEntreprise.etablissementAdresseComplementVoie : '')
																						+ ' ' + (adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null ? adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie : '');
formFields['modifAncienneAdresseEntreprise_codePostal']                              = adresseEntreprise.etablissementAdresseCodePostal;
formFields['modifAncienneAdresseEntreprise_commune']                                 = adresseEntreprise.etablissementAdresseCommune;
}



// Cadre 7 - Activité

var activite = $p2p4modif.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite;

if (activite.modifDateActivite !== null) {
    var dateTmp = new Date(parseInt(activite.modifDateActivite.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateActivite']          = date;
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')) { 
formFields['entreprise_activitePermanenteSaisonniere_permanente']                    = Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonnierePermanente') ? true : false;
formFields['entreprise_activitePermanenteSaisonniere_saisonniere']                   = Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonniereSaisonniere') ? true : false;
formFields['etablissement_nonSedentariteQualite_nonSedentaire']                      = activite.etablissementNonSedentariteQualiteNonSedentaire ? true : false;
formFields['etablissement_activites']                                                = Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite') ? activite.etablissementActivitesNew : '';

/*
formFields['etablissement_activiteLieuExercice_magasin']                             = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMagasin') ? true : false;
formFields['etablissement_activiteLieuExerciceMagasin']                              = activite.etablissementActiviteLieuExerciceMagasinSurface;
formFields['etablissement_activiteLieuExercice_marche']                              = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMarche') ? true : false;
formFields['etablissement_activiteNature_commerceDetail']                            = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceDetail') ? true : false;
formFields['etablissement_activiteNature_commerceGros']                              = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceGros') ? true : false;
formFields['etablissement_activiteNature_fabricationProduction']                     = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureFabricationProduction') ? true : false;
formFields['etablissement_activiteNature_batTravauxPublics']                         = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureBatTravauxPublics') ? true : false;
formFields['etablissement_activiteNature_cocheAutre']                                = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCocheAutre') ? true : false;
formFields['etablissement_activiteNature_autre']                                     = activite.etablissementActiviteNatureAutre;
*/
}

// Cadre 8 - Observation

var correspondance = $p2p4modif.cadre8RensCompGroup.cadre8RensComp;
var signataire = $p2p4modif.cadre9SignatureGroup.cadre9Signature;

if (Value('id').of(objet.objetModification).contains('dateActivite') and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 !== null or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 !== null)) {
if (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateObservations']          = date;
} else if (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateObservations']          = date;
}
formFields['textModifDate']                 = "Modification de la date de début d'activité au :";
formFields['activiteDate']                  = (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 != null ? objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 : objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2);
if (modifIdentiteDeclarant.modifNationalite.modifNewNationalite != null) {
formFields['textModifDate2']                = "/ Nouvelle nationalité :" + ' ' + modifIdentiteDeclarant.modifNationalite.modifNewNationalite + ' ' + "au";
formFields['activiteDate2']                 = modifIdentiteDeclarant.modifNationalite.modifDateNationalite;
formFields['textModifDate3']                = correspondance.formaliteObservations != null ? ("Observation :" + ' ' + correspondance.formaliteObservations) : '';
} else if (modifIdentiteDeclarant.modifNationalite.modifNewNationalite == null) {
formFields['observations2']                 = correspondance.formaliteObservations != null ? ("/" + ' ' + correspondance.formaliteObservations) : '';
}
} else if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('17P')) {
if (modifIdentiteDeclarant.modifNationalite.modifDateNationalite !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifNationalite.modifDateNationalite.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateObservations']          = date;
}
formFields['observations']                                                           = ("Modification de nationalité :" + ' ' +  modifIdentiteDeclarant.modifNationalite.modifNewNationalite) + ' / ' +  (correspondance.formaliteObservations != null ? correspondance.formaliteObservations : '');
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('17P') and not Value('id').of(objet.objetModification).contains('dateActivite') and objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 == null and objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 == null) {	
formFields['observations']                                                           = correspondance.formaliteObservations != null ? correspondance.formaliteObservations : '';
}


// Cadre 9 - Adresse de correspondance

formFields['adresseCorrespondanceCadre_coche']                                       = Value('id').of(correspondance.adresseCorrespond).eq('domi') ? true : false;
formFields['adresseCorrespondanceCadre_numero']                                      = Value('id').of(correspondance.adresseCorrespond).eq('domi') ? "3" : '';
formFields['adressesCorrespondanceAutre']                                            = not Value('id').of(correspondance.adresseCorrespond).eq('domi') ? true : false;

if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
formFields['adresseCorrespondance_voie1']                 = correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance
															+ ' ' + (correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance : '')
															+ ' ' + (correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance : '')
															+ ' ' + (correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance : '')
															+ ' ' + (correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance : '');
formFields['adresseCorrespondance_voie2']                 = (correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance != null ? correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance : '')
															+ ' ' + (correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance != null ? correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance : '');
formFields['adresseCorrespondance_codePostal']            = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal;
formFields['adresseCorrespondance_commune']               = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('prof') and not Value('id').of(correspondance.correspondanceEtablissement).eq('newEtablissement')) {
formFields['adresseCorrespondance_voie1']                 = (adresseEntreprise.etablissementAdresseNumeroVoie != null ? adresseEntreprise.etablissementAdresseNumeroVoie : '')
														  + ' ' + (adresseEntreprise.etablissementAdresseIndiceVoie != null ? adresseEntreprise.etablissementAdresseIndiceVoie : '')
														  + ' ' + (adresseEntreprise.etablissementAdresseTypeVoie != null ? adresseEntreprise.etablissementAdresseTypeVoie : '')
														  + ' ' + (adresseEntreprise.etablissementAdresseNomVoie != null ? adresseEntreprise.etablissementAdresseNomVoie : '');
formFields['adresseCorrespondance_voie2']                 =	(adresseEntreprise.etablissementAdresseComplementVoie != null ? adresseEntreprise.etablissementAdresseComplementVoie : '')
														  + ' ' + (adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null ? adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie : '');
formFields['adresseCorrespondance_codePostal']            = adresseEntreprise.etablissementAdresseCodePostal;
formFields['adresseCorrespondance_commune']               = adresseEntreprise.etablissementAdresseCommune;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('prof') and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('11P') and Value('id').of(correspondance.correspondanceEtablissement).eq('newEtablissement')) {
formFields['adresseCorrespondance_voie1']                 = (etablissement.adresseEtablissementNew.etablissementAdresseNumeroVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseNumeroVoieNew : '') 
														  + ' ' + (etablissement.adresseEtablissementNew.etablissementAdresseIndiceVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseIndiceVoieNew : '')
														  + ' ' + (etablissement.adresseEtablissementNew.etablissementAdresseTypeVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseTypeVoieNew : '')
														  + ' ' + (etablissement.adresseEtablissementNew.etablissementAdresseNomVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseNomVoieNew : '');
formFields['adresseCorrespondance_voie2']                 = (etablissement.adresseEtablissementNew.etablissementAdresseComplementVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseComplementVoieNew : '')
														  + ' ' + (etablissement.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew : '');
formFields['adresseCorrespondance_codePostal']            = etablissement.adresseEtablissementNew.etablissementAdresseCodePostalNew;
formFields['adresseCorrespondance_commune']               = etablissement.adresseEtablissementNew.etablissementAdresseCommuneNew;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('prof') and objet.domicileEntreprise and Value('id').of(correspondance.correspondanceEtablissement).eq('newEtablissement')) {
formFields['adresseCorrespondance_voie1']                 = (newAdresseDom.personneLieeAdresseVoieNew != null ? newAdresseDom.personneLieeAdresseVoieNew : '') 
														  + ' ' + (newAdresseDom.personneLieeAdresseIndiceVoieNew != null ? newAdresseDom.personneLieeAdresseIndiceVoieNew : '')
														  + ' ' + (newAdresseDom.personneLieeAdresseTypeVoieNew != null ? newAdresseDom.personneLieeAdresseTypeVoieNew : '')
														  + ' ' + (newAdresseDom.personneLieeAdresseNomVoieNew != null ? newAdresseDom.personneLieeAdresseNomVoieNew : '');
formFields['adresseCorrespondance_voie2']                 = (newAdresseDom.personneLieeAdresseComplementAdresseNew != null ? newAdresseDom.personneLieeAdresseComplementAdresseNew : '')
														  + ' ' + (newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null ? newAdresseDom.personneLieeAdresseDistriutionSpecialeNew : '');
formFields['adresseCorrespondance_codePostal']            = newAdresseDom.personneLieeAdresseCodePostalNew;
formFields['adresseCorrespondance_commune']               = newAdresseDom.personneLieeAdresseCommuneNew;
}

formFields['telephone1']                                                             = correspondance.infosSup.formaliteTelephone2;
formFields['telephone2']                                                             = correspondance.infosSup.formaliteTelephone1 != null ? correspondance.infosSup.formaliteTelephone1 : '';
formFields['courriel']                                                               = correspondance.infosSup.formaliteFaxCourriel != null ? correspondance.infosSup.formaliteFaxCourriel : (correspondance.infosSup.telecopie != null ? correspondance.infosSup.telecopie : '');

// Cadre 10 - Diffusion informations

formFields['diffusion_information']                                                   = Value('id').of(signataire.diffusionInfo).eq('formaliteNonDiffusionInformationOui') ? true : false;
formFields['non_diffusion_information']                                                = Value('id').of(signataire.diffusionInfo).eq('formaliteNonDiffusionInformationNon') ? true : false;

// Cadre 11 - Signataire

formFields['signataireDeclarant']                                                    = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
if (Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
formFields['signataireMandataire']                                                   = true;
formFields['signataireNom']										                     = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFields['signataireAdresse']                                                      = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '');
formFields['signataireAdresse2']                                                     = (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '');
formFields['signataireAdresse3']                                                     = signataire.adresseMandataire.dataCodePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
formFields['signatureDate']                                                          = signataire.formaliteSignatureDate;
formFields['signatureLieu']                                                          = signataire.formaliteSignatureLieu;
formFields['estEIRL_oui']                                                            = Value('id').of(objet.objetModification).contains('modifEIRL') ? true : false;
formFields['estEIRL_non']                                                            = Value('id').of(objet.objetModification).contains('modifEIRL') ? false : true;
formFields['signature']                                                              = '';

                          // Intercalaire PEIRL ME
								
//Cadre 1 - Déclaration ou modification d'affectation de patrimoine

formFields['eirl_complete_P2p4']                                                            = true;
formFields['eirl_complete_p0PLME']                                                          = false;
formFields['declaration_initiale']															= objet.cadreObjetDeclarationEIRL.declarationEIRL ? true : false;
formFields['declaration_modification']                                                      = (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') or Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('finEIRL') or objet.entrepriseEIRL) ? true : false;


// Cadre 2 - Rappel d'identification

formFields['eirl_siren']                                                  = identite.siren.split(' ').join('');
formFields['eirl_nomNaissance']                                           = identite.personneLieePersonnePhysiqueNomNaissance;
formFields['eirl_nomUsage']                                               = identite.personneLieePersonnePhysiqueNomUsage != null ? identite.personneLieePersonnePhysiqueNomUsage : '';
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFields['eirl_prenom']                                      = prenoms.toString();


// Cadre 3 - Déclaration d'affectation de patrimoine

var declarationAffectation = $p2p4modif.cadre4DeclarationAffectationPatrimoineGroup.cadreDeclarationAffectationPatrimoine;

if (objet.cadreObjetDeclarationEIRL.declarationEIRL) { 
formFields['eirl_statutEIRL_declarationInitiale']                                           = Value('id').of(declarationAffectation.eirlStatut).eq('EirlStatutEIRLDeclarationInitiale') ? true : false;
formFields['eirl_statutEIRL_reprise']                                                       = Value('id').of(declarationAffectation.eirlStatut).eq('EirlStatutEIRLReprise') ? true : false;
formFields['eirl_denomination']                                                             = declarationAffectation.declarationPatrimoine.eirlDenomination;
if (declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable !== null) {
    var dateTmp = new Date(parseInt(declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable.getTimeInMillis()));
    var dateClotureEc = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateClotureEc = dateClotureEc.concat(pad(month.toString()));
    formFields['eirl_dateClotureExerciceComptable']          = dateClotureEc;
}
formFields['eirl_objet']                                                                    = declarationAffectation.declarationPatrimoine.eirlObjet;

if (Value('id').of(declarationAffectation.eirlStatut).eq('EirlStatutEIRLReprise')) {
formFields['eirl_precedentEIRLDenomination']                                                = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLDenomination;
formFields['eirl_precedentEIRLLieuImmatriculation']                                         = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLLieuImmatriculation;
formFields['eirl_precedentEIRLSIREN']                                                       = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN != null ? (declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN.split(' ').join('')) : '';
}
}

// Cadre 4 - Rappel d'identification reltif à l'EIRL

var rappelIdentification = $p2p4modif.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreRappelIdentificationEIRL;

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') or Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('finEIRL') or objet.entrepriseEIRL) { 
formFields['eirl_rappelDenomination']                   = rappelIdentification.eirlRappelDenomination;
formFields['eirl_rappelLieuImmatriculation']            = rappelIdentification.eirlRappelLieuImmatriculation;

if (objet.cadreObjetModificationEIRL.memeAdresseEIRL or objet.entrepriseEIRL) {
formFields['eirl_rappelAdresse']                    = (adresseEntreprise.etablissementAdresseNumeroVoie != null ? adresseEntreprise.etablissementAdresseNumeroVoie : '')
													+ ' ' + (adresseEntreprise.etablissementAdresseIndiceVoie != null ? adresseEntreprise.etablissementAdresseIndiceVoie : '')
													+ ' ' + (adresseEntreprise.etablissementAdresseTypeVoie != null ? adresseEntreprise.etablissementAdresseTypeVoie : '')
													+ ' ' + (adresseEntreprise.etablissementAdresseNomVoie != null ? adresseEntreprise.etablissementAdresseNomVoie : '')
													+ ' ' + (adresseEntreprise.etablissementAdresseComplementVoie != null ? adresseEntreprise.etablissementAdresseComplementVoie : '')
													+ ' ' + (adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null ? adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie : '')
													+ ' ' + adresseEntreprise.etablissementAdresseCodePostal
													+ ' ' + adresseEntreprise.etablissementAdresseCommune;
} else if (not objet.cadreObjetModificationEIRL.memeAdresseEIRL) {
formFields['eirl_rappelAdresse']                    = (rappelIdentification.cadreEirlRappelAdresse.numeroAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.numeroAdresseEirl : '')
													+ ' ' + (rappelIdentification.cadreEirlRappelAdresse.indiceVoieAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.indiceVoieAdresseEirl : '')
													+ ' ' + (rappelIdentification.cadreEirlRappelAdresse.typeVoieAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.typeVoieAdresseEirl : '')
													+ ' ' + rappelIdentification.cadreEirlRappelAdresse.nomVoieAdresseEirl
													+ ' ' + (rappelIdentification.cadreEirlRappelAdresse.complementAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.complementAdresseEirl : '')
													+ ' ' + (rappelIdentification.cadreEirlRappelAdresse.distriutionSpecialeAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.distriutionSpecialeAdresseEirl : '')
													+ ' ' + rappelIdentification.cadreEirlRappelAdresse.codePostalAdresseEirl
													+ ' ' + rappelIdentification.cadreEirlRappelAdresse.communeAdresseEirl;
}
}

// Cadre 5 et 6- Déclaration de modification

var modifEIRL = $p2p4modif.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL;                                         

if(Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDenoEIRL')) {
if (modifEIRL.modifDateEIRL !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateDenoEIRL']          = dateModif;
}
formFields['new_DenoEIRL']                                                                  = modifEIRL.newDenoEIRL;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDateCloture')) {
if (modifEIRL.modifDateEIRL !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateClotureExerciceComptableEIRL']          = dateModif;
}                                         
if (modifEIRL.eirlNewDateClotureExerciceComptable !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.eirlNewDateClotureExerciceComptable.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    formFields['eirl_newDateClotureExerciceComptable']          = dateModif;
}
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifObjet')) {
if (modifEIRL.modifDateEIRL !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateObjetEIRL']          = dateModif;
}
formFields['new_objetEIRL']                                                                 = modifEIRL.newObjetEIRL;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDAP')) {
if (modifEIRL.modifDateEIRL !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateDAP']          = dateModif;
} 
formFields['modif_DAP']                                                                     = true;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse')) {
if (modifEIRL.modifDateEIRL !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateAdresseEIRL']          = dateModif;
}	
formFields['new_adresseEIRL']                           = (modifEIRL.newAdresseEIRL.numeroAdresseEirlNew != null ? modifEIRL.newAdresseEIRL.numeroAdresseEirlNew : '')
														+ ' ' + (modifEIRL.newAdresseEIRL.indiceVoieAdresseEirlNew != null ? modifEIRL.newAdresseEIRL.indiceVoieAdresseEirlNew : '')
														+ ' ' + (modifEIRL.newAdresseEIRL.typeVoieAdresseEirlNew != null ? modifEIRL.newAdresseEIRL.typeVoieAdresseEirlNew : '')
														+ ' ' + modifEIRL.newAdresseEIRL.nomVoieAdresseEirlNew;
formFields['new_adresseEIRL2']                          = (modifEIRL.newAdresseEIRL.complementAdresseEirlNew != null ? modifEIRL.newAdresseEIRL.complementAdresseEirlNew : '')
														+ ' ' + (modifEIRL.newAdresseEIRL.distriutionSpecialeAdresseEirlNew != null ? modifEIRL.newAdresseEIRL.distriutionSpecialeAdresseEirlNew : '')
														+ ' ' + modifEIRL.newAdresseEIRL.codePostalAdresseEirlNew
														+ ' ' + modifEIRL.newAdresseEIRL.communeAdresseEirlNew;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('11P') and objet.entrepriseEIRL) {
if (etablissement.modifDateAdresseEntreprise !== null) {
    var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateAdresseEIRL']          = date;
}
formFields['new_adresseEIRL']                             = (etablissement.adresseEtablissementNew.etablissementAdresseNumeroVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseNumeroVoieNew : '') 
														  + ' ' + (etablissement.adresseEtablissementNew.etablissementAdresseIndiceVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseIndiceVoieNew : '')
														  + ' ' + (etablissement.adresseEtablissementNew.etablissementAdresseTypeVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseTypeVoieNew : '')
														  + ' ' + (etablissement.adresseEtablissementNew.etablissementAdresseNomVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseNomVoieNew : '');
formFields['new_adresseEIRL2']							  = (etablissement.adresseEtablissementNew.etablissementAdresseComplementVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseComplementVoieNew : '')
														+ ' ' + (etablissement.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew : '')
														+ ' ' + etablissement.adresseEtablissementNew.etablissementAdresseCodePostalNew
														+ ' ' + etablissement.adresseEtablissementNew.etablissementAdresseCommuneNew;
} else if (objet.domicileEntreprise and objet.entrepriseEIRL) {
if (modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateAdresseEIRL']          = date;
}
formFields['new_adresseEIRL']                             = (newAdresseDom.personneLieeAdresseVoieNew != null ? newAdresseDom.personneLieeAdresseVoieNew : '') 
														  + ' ' + (newAdresseDom.personneLieeAdresseIndiceVoieNew != null ? newAdresseDom.personneLieeAdresseIndiceVoieNew : '')
														  + ' ' + (newAdresseDom.personneLieeAdresseTypeVoieNew != null ? newAdresseDom.personneLieeAdresseTypeVoieNew : '')
														  + ' ' + newAdresseDom.personneLieeAdresseNomVoieNew;
formFields['new_adresseEIRL2']							  = (newAdresseDom.personneLieeAdresseComplementAdresseNew != null ? newAdresseDom.personneLieeAdresseComplementAdresseNew : '')
														  + ' ' + (newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null ? newAdresseDom.personneLieeAdresseDistriutionSpecialeNew : '')
														  + ' ' + newAdresseDom.personneLieeAdresseCodePostalNew
														  + ' ' + newAdresseDom.personneLieeAdresseCommuneNew;
}

var finDAP = $p2p4modif.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.finEIRLGroup;
if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('finEIRL')) { 																							
if (finDAP.finEIRLDeclaration.modifDateFinEIRL !== null) {
    var dateTmp = new Date(parseInt(finDAP.finEIRLDeclaration.modifDateFinEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateFinEIRL']          = dateModif;
}
formFields['finEIRL_renonciationAvecPoursuite']                                             = Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).contains('finEIRL') ? (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLRenonciationAvecPoursuite') ? true : false) : false;
formFields['finEIRL_renonciationSansPoursuite']                                             = Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).contains('finEIRL') ? (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLRenonciationSansPoursuite') ? true : false) : false;
formFields['finEIRL_cessionPP']                                                             = Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).contains('finEIRL') ? (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLCessionPP') ? true : false) : false;
formFields['finEIRL_cessionPM']                                                             = Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).contains('finEIRL') ? (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLCessionPM') ? true : false) : false;
}	

/* if (Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('poursuiteEIRL')) {
if (modifEIRL.poursuiteEIRLGroup.modifDatePoursuiteEIRL !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.poursuiteEIRLGroup.modifDatePoursuiteEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDatePoursuiteEIRL']          = dateModif;
}
formFields['modif_pouruiteEIRL']                                                            = true;
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFields['EIRL_poursuiteNomPrenom']                                                       = modifEIRL.poursuiteEIRLGroup.eIRLPoursuiteNom + ' ' + prenoms.toString();
}
*/

// Cadre 7 - Options fiscales

if (objet.cadreObjetDeclarationEIRL.declarationEIRL) {
formFields['eirl_regimeFiscal_regimeImpositionBeneficesVersementLiberatoire_oui']           = $p2p4modif.cadre4DeclarationAffectationPatrimoineGroup.impositionBenefices.regimeFiscalRegimeImpositionBeneficesVersementLiberatoire ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBeneficesVersementLiberatoire_non']           = $p2p4modif.cadre4DeclarationAffectationPatrimoineGroup.impositionBenefices.regimeFiscalRegimeImpositionBeneficesVersementLiberatoire ? false : true;
}

/*
 * Création du dossier avec volet social : ajout du cerfa avec volet social
 */
 
var cerfaDoc1 = nash.doc //
	.load('models/cerfa_13905-04_p2p4_avec_volet_social.pdf') //
	.apply(formFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));

/*
 * Création du dossier sans volet social : ajout du cerfa sans volet social
 */
 
 var cerfaDoc2 = nash.doc //
	.load('models/cerfa_13905-04_p2p4_sans_volet_social.pdf') //
	.apply (formFields);
	cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));

/*
 * Ajout de l'intercalaire PEIRL ME avec option fiscale
 */
if (Value('id').of(objet.objetModification).contains('modifEIRL') or objet.entrepriseEIRL) {
	var peirlMEDoc1 = nash.doc //
		.load('models/cerfa_14214-03_peirlme_avec_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(peirlMEDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire PEIRL ME sans option fiscale
 */
if (Value('id').of(objet.objetModification).contains('modifEIRL') or objet.entrepriseEIRL) {
	var peirlMEDoc2 = nash.doc //
		.load('models/cerfa_14214-03_peirlme_sans_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(peirlMEDoc2.save('cerfa.pdf'));
}

/*
 * Ajout des PJs
 */

// PJ Déclarant

var pj=$p2p4modif.cadre9SignatureGroup.cadre9Signature.soussigne;

var pjUser = [];
var metas = [];

function pushPjPreview(fld) {
	fld.forEach(function (elm) {
        pjUser.push(elm);
        metas.push({'name':'document', 'value': '/'+elm.getAbsolutePath()});
    });
}

if(Value('id').of(pj).contains('FormaliteSignataireQualiteDeclarant')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantSignataire);
}

if (Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire') 
	and (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P') 
	or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P'))) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantNonSignataire);
} 

// PJ Mandataire

if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPouvoir);
}

// PJ Eirl

var pj=$p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEIRL.objetEIRL;
if($p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetDeclarationEIRL.declarationEIRL or Value('id').of(pj).contains('modificationEIRL') or $p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.entrepriseEIRL) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAP);
}

var pj=$p2p4modif.cadre4DeclarationAffectationPatrimoineGroup.informationsComplementaires.contenuDAP;
if(Value('id').of(pj).contains('bienImmo') or Value('id').of($p2p4modif.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.contenuDAP).contains('bienImmo')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAPActeNotarie);
}

var pj=$p2p4modif.cadre4DeclarationAffectationPatrimoineGroup.informationsComplementaires.contenuDAP;
if(Value('id').of(pj).contains('bienRapportEvaluation') or Value('id').of($p2p4modif.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.contenuDAP).contains('bienRapportEvaluation')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAPRapportEvaluation);
}

var pj=$p2p4modif.cadre4DeclarationAffectationPatrimoineGroup.informationsComplementaires.contenuDAP;
if(Value('id').of(pj).contains('bienCommunIndivis') or Value('id').of($p2p4modif.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.contenuDAP).contains('bienCommunIndivis')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAPAccordTiers);
}

/*
 * Enregistrement du fichier (en mémoire)
 */

var finalDoc = cerfaDoc1.save('P2P4_Modification.pdf');

// Remove old metas before insert
var metasToDelete = [];
var recordMetas = nash.record.meta() != null ? nash.record.meta().metas : null;
var recordMetasSize = recordMetas != null ? recordMetas.size() : 0;

for (var i = 0; i < recordMetasSize; i++) {
   if (recordMetas.get(i).name == 'document' && !recordMetas.get(i).value.contains("proxy") ) {
       metasToDelete.push({'name':'document', 'value': recordMetas.get(i).value});
   }
}
nash.record.removeMeta(metasToDelete);

nash.record.meta(metas);

var data = [ spec.createData({
    id : 'record',
    label : 'Déclaration de modification d\'une entreprise libérale ou d\'une personne non immatriculée à un registre public en micro-entrepreneur',
    description : 'Voici le formulaire obtenu à partir des données saisies.',
    type : 'FileReadOnly',
    value : [ finalDoc ]
}),  spec.createData({
    id : 'attachments',
    label : 'Pièces jointes',
    description : 'Pièces jointes ajoutées au formulaire.',
    type : 'FileReadOnly',
    value : pjUser
}) ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Déclaration de modification d\'une entreprise libérale ou d\'une personne non immatriculée à un registre public en micro-entrepreneur',
    groups : groups
});