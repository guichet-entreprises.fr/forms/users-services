//prepare info to send 

var adresseNew = $p2p4modif.cadre6EtablissementGroup.modificationEtablissement.adresseEtablissementNew;
var adressePro = $p2p4modif.cadre1RappelIdentificationGroup.cadre1RappelIdentification.adresseEtablissementPrincipal;
var adresseEIRL = $p2p4modif.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreRappelIdentificationEIRL.cadreEirlRappelAdresse;
var adresseDom = $p2p4modif.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifDomicile.newAdresseDomicile;

var algo = "trouver destinataire";

var secteur1 = "Liberal";

var typePersonne = "PP";

var formJuridique = (($p2p4modif.cadre1RappelIdentificationGroup.cadre1RappelIdentification.formeJuridique
and (Value('id').of($p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL')
or Value('id').of($p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEIRL.objetEIRL).eq('finEIRL'))) 
or $p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.entrepriseEIRL
or $p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetDeclarationEIRL.declarationEIRL) ? "EIRL" : "EI";

var optionCMACCI = "NON";

var codeCommune = (Value('id').of($p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).eq('etablissement') 
				and Value('id').of($p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEtablissement.objetEtablissement).eq('11P')) ? adresseNew.etablissementAdresseCommuneNew.getId() : 
				($p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.domicileEntreprise ? adresseDom.personneLieeAdresseCommuneNew.getId() : 
				((Value('id').of($p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).eq('modifEIRL')
				and Value('id').of($p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEIRL.objetModificationEIRL).eq('modifAdresse')
				and not Value('id').of($p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).eq('situationPerso')
				and not Value('id').of($p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).eq('etablissement')
				and not Value('id').of($p2p4modif.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).eq('dateActivite')) ?	adresseEIRL.communeAdresseEirl.getId() : adressePro.etablissementAdresseCommune.getId()));

var attachement= "/4-signature/generated/proxyResult.files-1-PROXY_SIGNED_DOCUMENT.pdf";

return spec.create({
		id: 'prepareSend',
		label: "Préparation de la recherche du destinataire",
		groups: [spec.createGroup({
				id: 'view',
				label: "Informations",
				data: [spec.createData({
						id: 'algo',
						label: "Algo",
						type: 'String',
						mandatory: true,
						value: algo
					}), spec.createData({
						id: 'secteur1',
						label: "Secteur",
						type: 'String',
						value: secteur1
					}), spec.createData({
						id: 'typePersonne',
						label: "Type personne",
						type: 'String',
						value: typePersonne
					}), spec.createData({
						id: 'formJuridique',
						label: "Forme juridique",
						type: 'String',
						value: formJuridique
					}),spec.createData({
						id: 'optionCMACCI',
						label: "Option CMACCI",
						type: 'String',
						value: optionCMACCI
					}),spec.createData({
						id: 'codeCommune',
						label: "Code commune",
						type: 'String',
						value: codeCommune
					}) ,spec.createData({
					    id: 'attachement',
					    label: "Pièce jointe",
					    type: 'String',
					    value: attachement
					})
				]
			})]
});