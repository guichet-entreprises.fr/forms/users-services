// check inout information

log.info("Hangin input : {}", _INPUT_);

var proxyResult = _INPUT_.proxyResult;
if (proxyResult.status == 'OK') {

	var loader = nash.record;
	loader.removeMeta([{'name':'document', 'value': '/'+$review.generated.record[0].getAbsolutePath()}]);
	
	var metas = [];
	loader.removeMeta([{'name':'document', 'value': '/'+proxyResult.files[0].getAbsolutePath()}]);
	metas.push({'name':'document', 'value': '/'+proxyResult.files[0].getAbsolutePath()});
	nash.record.meta(metas);
	return spec.create({
		id : 'signature',
		label : 'Signature électronique',
		groups : [ spec.createGroup({
			id : 'proxyResult',
			label : 'Signature électronique',
			description : "La signature électronique s'est déroulée avec succès. Veuillez passer à l'étape suivante de votre dossier",
			data : []
		}) ]
	});	
} else {
	return spec.create({
		id : 'signature',
		label : 'Signature électronique',
		groups : [ spec.createGroup({
			id : 'proxyResult',
			label : 'Résultat',
			warnings : [ { description : 'Une erreur est survenue lors de la signature électronique. En validant cette étape, vous allez re-exécuter la procédure de signature électronique.' } ],
			data : []
		}) ]
	});	
}