var formFields = {};
var formFieldsPers1  = {};
var formFieldsPers2  = {};
var formFieldsPers3  = {};
var formFieldsPers4  = {};
var formFieldsPers5  = {};
var formFieldsPers6  = {};
var formFieldsPers7  = {};
var formFieldsPers8  = {};
var formFieldsPers9  = {};
var formFieldsPers10  = {};
var formFieldsPers11 = {};
var formFieldsPers12  = {};
var formFieldsPers13  = {};
var formFieldsPers14  = {};
var formFieldsPers15  = {};
var formFieldsPers16  = {};
var formFieldsPers17  = {};
function pad(s) { return (s < 10) ? '0' + s : s; }

var objet = $M2.cadreModificationSocieteGroup.cadre1ObjetModification;
var identite = $M2.cadreRappelIdentificationGroup.cadre1RappelIdentification;
var dissolution = $M2.modifDissolutionGroup.modifDissolution;
var modifDeno = $M2.cadreModificationPMGroup.cadre2ModificationPM.modificationDeno;
var modifFormeJuridique = $M2.cadreModificationPMGroup.cadre2ModificationPM.modifFormeJuridique;
var modifCapital = $M2.cadreModificationPMGroup.cadre2ModificationPM.modifCapital;
var modifDuree = $M2.cadreModificationPMGroup.cadre2ModificationPM.modificationDuree;
var modifEss = $M2.cadreModificationPMGroup.cadre2ModificationPM.modifEss;
var modifSocieteMission = $M2.cadreModificationPMGroup.cadre2ModificationPM.modifSocieteMission;
var modifContratAppui = $M2.cadreModificationPMGroup.cadre2ModificationPM.modifContratAppui;
var modifRenouvellementMaintienRCS = $M2.cadreModificationPMGroup.cadre2ModificationPM.modifRenouvellementMaintienRCS;
var siege = $M2.cadreRappelIdentificationGroup.cadre1RappelIdentification.adresseSiege;
var fusion = $M2.cadreModificationFusionGroup.cadreModificationFusion;
var fermeture = $M2.cadreEtablissementFermetureGroup.cadreEtablissementFermeture;
var fondsDonne = $M2.cadreFondsDonneLocationGeranceGroup.cadreFondsDonneLocationGerance;
var ouverture = $M2.cadreEtablissementOuvertureGroup.cadreEtablissementOuverture;
var activiteInfo = $M2.cadreEtablissementActiviteGroup.cadre7EtablissementActivite;
var activite = $M2.cadreEtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite;
var origine = $M2.cadre4OrigineFondsGroup.cadre4OrigineFonds;
var dirigeantSARL1 = $M2.dirigeant1Sarl.cadreIdentiteDirigeant;
var dirigeantSARL2 = $M2.dirigeant2Sarl.cadreIdentiteDirigeant;
var dirigeantSARL3 = $M2.dirigeant3Sarl.cadreIdentiteDirigeant;
var dirigeantSARL4 = $M2.dirigeant4Sarl.cadreIdentiteDirigeant;
var dirigeantSARL5 = $M2.dirigeant5Sarl.cadreIdentiteDirigeant;
var conjoint1 = $M2.dirigeant1Sarl.cadreIdentiteDirigeant.cadre2InfosConjoint;
var conjoint2 = $M2.dirigeant2Sarl.cadreIdentiteDirigeant.cadre2InfosConjoint;
var conjoint3 = $M2.dirigeant3Sarl.cadreIdentiteDirigeant.cadre2InfosConjoint;
var conjoint4 = $M2.dirigeant4Sarl.cadreIdentiteDirigeant.cadre2InfosConjoint;
var conjoint5 = $M2.dirigeant5Sarl.cadreIdentiteDirigeant.cadre2InfosConjoint;
var dirigeant1 = $M2.dirigeant1.cadreIdentiteDirigeant;
var dirigeant2 = $M2.dirigeant2.cadreIdentiteDirigeant;
var dirigeant3 = $M2.dirigeant3.cadreIdentiteDirigeant;
var dirigeant4 = $M2.dirigeant4.cadreIdentiteDirigeant;
var dirigeant5 = $M2.dirigeant5.cadreIdentiteDirigeant;
var dirigeant6 = $M2.dirigeant6.cadreIdentiteDirigeant;
var dirigeant7 = $M2.dirigeant7.cadreIdentiteDirigeant;
var dirigeant8 = $M2.dirigeant8.cadreIdentiteDirigeant;
var dirigeant9 = $M2.dirigeant9.cadreIdentiteDirigeant;
var dirigeant10 = $M2.dirigeant10.cadreIdentiteDirigeant;
var dirigeant11 = $M2.dirigeant11.cadreIdentiteDirigeant;
var dirigeant12 = $M2.dirigeant12.cadreIdentiteDirigeant;
var socialDirigeant1 = $M2.cadreDeclarationSociale1.cadre7DeclarationSociale;
var socialDirigeant2 = $M2.cadreDeclarationSociale2.cadre7DeclarationSociale;
var socialDirigeant3 = $M2.cadreDeclarationSociale3.cadre7DeclarationSociale;
var socialDirigeant4 = $M2.cadreDeclarationSociale4.cadre7DeclarationSociale;
var socialDirigeant5 = $M2.cadreDeclarationSociale5.cadre7DeclarationSociale;
var socialDirigeant6 = $M2.cadreDeclarationSociale6.cadre7DeclarationSociale;
var socialDirigeant7 = $M2.cadreDeclarationSociale7.cadre7DeclarationSociale;
var socialDirigeant8 = $M2.cadreDeclarationSociale8.cadre7DeclarationSociale;
var socialDirigeant9 = $M2.cadreDeclarationSociale9.cadre7DeclarationSociale;
var socialDirigeant10 = $M2.cadreDeclarationSociale10.cadre7DeclarationSociale;
var socialDirigeant11 = $M2.cadreDeclarationSociale11.cadre7DeclarationSociale;
var socialDirigeant12 = $M2.cadreDeclarationSociale12.cadre7DeclarationSociale;
var socialAssocie1 = $M2.cadreDeclarationSocialeConjointAssocie1.cadre7DeclarationSociale;
var socialAssocie2 = $M2.cadreDeclarationSocialeConjointAssocie2.cadre7DeclarationSociale;
var socialAssocie3 = $M2.cadreDeclarationSocialeConjointAssocie3.cadre7DeclarationSociale;
var socialAssocie4 = $M2.cadreDeclarationSocialeConjointAssocie4.cadre7DeclarationSociale;
var socialAssocie5 = $M2.cadreDeclarationSocialeConjointAssocie5.cadre7DeclarationSociale;
var correspondance = $M2.cadre8RensCompGroup.cadre8RensComp;
var signataire = $M2.cadre9SignatureGroup.cadre9Signature;
var jqpa1 = $M2.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification1;																					
var jqpa2 = $M2.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification2;																					
var jqpa3 = $M2.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification3;																					

// Génération du M2

// Cadre 1 - Objet de la formalité

formFields['modifSociete']                                              = (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('10M') 
																		or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')
																		or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('capital')
																		or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('fusion')) ? true : false;
formFields['modifSiege']                                         		= (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
																			and (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege')
																			or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal'))) ? true : false;
formFields['modifRepriseActivite']                                      = ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
																			and Value('id').of(objet.cadreObjetModificationEtablissement.priseRepriseActivite).eq('reprise'))
																			or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M')
																			and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissementBis).eq('activitePrincipal'))) ? true : false;
formFields['modifPriseActivite']                           	            = (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
																			and Value('id').of(objet.cadreObjetModificationEtablissement.priseRepriseActivite).eq('prise')) ? true : false;
formFields['modifEtablissement']                                        = ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
																			and (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')
																			or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')))
																			or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
																			or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M')
																			or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80M')
																			or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60M')
																			or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('81M')
																			or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('82M')
																			or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('84M')
																			or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68M')
																			or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63M')
																			or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64M')
																			or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M')
																			and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissementBis).eq('activiteSecondaire')))	? true : false;
formFields['modifDissolution']                                    		= Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') ? true : false;
formFields['dissolution_avecPoursuite']                                 = (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M')
																		and not dissolution.dissoMiseEnSommeil
																		and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('40M')) ? true : false;
formFields['dissolution_sansPoursuite']                                 = (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M')
																		and (dissolution.dissoMiseEnSommeil
																		or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('40M'))) ? true : false;
formFields['modifMiseEnSommeil']                                        = Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('40M') ? true : false;
formFields['modifGIE_GEIE']                                             = false;
formFields['modifAutre']                                                = (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('16M') 
																		or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('18M')
																		or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('societeMission')
																		or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('37M')
																		or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('23M')
																		or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('29M')) ? true : false;
formFields['modifAutre_libelle']                                        = (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('16M')
																		and Value('id').of(modifDuree.modifDureeExerciceSocial).contains('modifDuree')) ? "Modificiation de la durée" :
																		((Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('16M')
																		and Value('id').of(modifDuree.modifDureeExerciceSocial).contains('modifExerciceSocial')) ? "Modificiation de la date de clôture de l'exercice social" :
																		((Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('18M')
																		and Value('id').of(modifEss.modifEssAdhesionSortie).eq('modifEssAdhesion'))	? "Adhésion à l'ESS" :
																		((Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('18M')
																		and Value('id').of(modifEss.modifEssAdhesionSortie).eq('modifEssSortie'))	? "Sortie du champs de l'ESS" :
																		(Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('37M') ? "Suppression de la mention du contrat d'appui" :
																		(Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('23M') ? "Demande de renouvellement du maintien au RCS" :
																		(Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('29M') ? "Modification de la date de début d'activité" : 
																		((Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('societeMission')
																		and Value('id').of(modifSocieteMission.modifSocieteMissionAdhesionSortie).eq('modifSocieteMissionAdhesion')) ? "Obtention de la qualité de société à mission" :
																		((Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('societeMission')
																		and Value('id').of(modifSocieteMission.modifSocieteMissionAdhesionSortie).eq('modifSocieteMissionSortie')) ? "Perte de la qualité de société à mission" : ''))))))));

// Cadre 2 - Rappel d'identification

formFields['entreprise_siren']                                       = identite.siren.split(' ').join('');
formFields['immatRCS']                                               = true;
formFields['immatRCSGreffe']                                         = identite.immatRCSGreffe;
formFields['immatRM']                                                = identite.immatriculationRM ? true : false;
formFields['immatRMDept']                                            = identite.immatriculationRM ? identite.immatRMCMA : '';
formFields['immatRMDeptNum']                                         = identite.immatriculationRM ? identite.immatRMCMA.getId() : '';

var greffeSecondaires=[];
for ( i = 0; i < identite.greffeSecondaire.size() ; i++ ){greffeSecondaires.push(identite.greffeSecondaire[i]);}                            
formFields['greffeSecondaire']                                      = greffeSecondaires.toString();
formFields['entreprise_siege']                                      = (Value('id').of(identite.entrepriseFormeJuridique).eq('5800') or Value('id').of(identite.entrepriseFormeJuridique).eq('3110') or Value('id').of(identite.entrepriseFormeJuridique).eq('3120')) ? false : true;
formFields['entreprise_1erEtablissement']                           = (Value('id').of(identite.entrepriseFormeJuridique).eq('5800') or Value('id').of(identite.entrepriseFormeJuridique).eq('3110') or Value('id').of(identite.entrepriseFormeJuridique).eq('3120')) ? true : false;
formFields['entreprise_denomination']                               = identite.entrepriseDenomination + ' ' + (identite.entrepriseSigle != null ? ('/' + ' ' + identite.entrepriseSigle) : '');
formFields['entreprise_formeJuridique']                             = identite.entrepriseFormeJuridique.getLabel();
formFields['entreprise_adresseEntreprisePM_voie1']                  = (siege.siegeAdresseNumeroVoie != null ? siege.siegeAdresseNumeroVoie : '')
																	+ ' ' + (siege.siegeAdresseIndiceVoie != null ? siege.siegeAdresseIndiceVoie : '')
																	+ ' ' + (siege.siegeAdresseTypeVoie != null ? siege.siegeAdresseTypeVoie : '')
																	+ ' ' + siege.siegeAdresseNomVoie
																	+ ' ' + (siege.siegeAdresseComplementVoie != null ? siege.siegeAdresseComplementVoie : '')
																	+ ' ' + (siege.siegeAdresseDistriutionSpecialeVoie != null ? siege.siegeAdresseDistriutionSpecialeVoie : '');
formFields['entreprise_adresseEntreprisePM_codePostal']             = siege.siegeAdresseCodePostal;
formFields['entreprise_adresseEntreprisePM_commune']                = siege.siegeAdresseCommune;
formFields['entreprise_adresseEntreprisePM_communeAncienne']        = siege.communeAncienneAdresseSiege != null ? siege.communeAncienneAdresseSiege : '';

// Cadre 3 Associé unique

if (identite.entrepriseAssocieUnique or Value('id').of(identite.entrepriseFormeJuridique).eq('5720')) {
formFields['entreprise_associeUniqueNonDirigeant']                   = (identite.associeUniqueDirigeant1 or identite.associeUniqueDirigeant2) ? false : true;
formFields['entreprise_associeUniqueDirigeant']                      = (identite.associeUniqueDirigeant1 or identite.associeUniqueDirigeant2) ? true : false;
}

// Cadre 4 Dénomination / sigle / forme juridique / durée / date de clôture de l'exercice social

if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('10M')) {
if (modifDeno.modifDateDeno !== null) {
    var dateTmp = new Date(parseInt(modifDeno.modifDateDeno.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_denomination']          = date;
}
formFields['newDenomination']                                                        = Value('id').of(modifDeno.modifDenoSigle).contains('modifDeno') ? modifDeno.newDenomination : '';
formFields['newSigle']                                                               = Value('id').of(modifDeno.modifDenoSigle).contains('modifSigle') ? modifDeno.newSigle : '';
}

if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')) {
if (modifFormeJuridique.modifDateFormeJuridique !== null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_formeJuridique']          = date;
}
formFields['newFormeJuridique']                                                      = modifFormeJuridique.newFormeJuridique;
if (modifFormeJuridique.entrepriseReduiteAssocieUnique1) {
if (modifFormeJuridique.modifDateFormeJuridique !== null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_societeReduiteAssocieUnique']          = date;
}
formFields['newCocheSocieteReduiteAssocieUnique']                                    = true;
}
}

if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('16M')) {
if (Value('id').of(modifDuree.modifDureeExerciceSocial).contains('modifDuree')) {
if (modifDuree.modifDateDuree !== null) {
    var dateTmp = new Date(parseInt(modifDuree.modifDateDuree.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_duree']          = date;
}
formFields['newDuree']                                                        = modifDuree.newDuree;
}
if (Value('id').of(modifDuree.modifDureeExerciceSocial).contains('modifExerciceSocial')) {
if (modifDuree.modifDateDuree !== null) {
    var dateTmp = new Date(parseInt(modifDuree.modifDateDuree.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_clotueExerciceComptable']          = date;
}
if (modifDuree.newExerciceSocial !== null) {
    var dateTmp = new Date(parseInt(modifDuree.newExerciceSocial.getTimeInMillis()));
    var dateClotureEc = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateClotureEc = dateClotureEc.concat(pad(month.toString()));
    formFields['newDateClotueExerciceComptable']          = dateClotureEc;
}
}
}

// Cadre 5 Capital

if (Value('id').of(modifCapital.modifCapitalType).contains('15M') or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('fusion')) {
if (modifCapital.modifDateCapital !== null) {
    var dateTmp = new Date(parseInt(modifCapital.modifDateCapital.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_capital']          = date;
} else if (fusion.modifDateFusion !== null) {
    var dateTmp = new Date(parseInt(fusion.modifDateFusion.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_capital']          = date;
}
formFields['newCapital']                                                             = modifCapital.newCapitalMontant != null ? (modifCapital.newCapitalMontant + ' ' + "euros") : (fusion.entrepriseCapitalMontant + ' ' + "euros");
formFields['newCapitalMin']                                                          = modifCapital.newCapitalVariable ? (modifCapital.newCapitalVariableMinimum + ' ' + "euros") : (fusion.entrepriseCapitalVariable ? (fusion.entrepriseCapitalVariableMinimum + ' ' + "euros") : '');
}

if (Value('id').of(modifCapital.modifCapitalType).contains('25M')) {
if (modifCapital.modifDateContinuation !== null) {
    var dateTmp = new Date(parseInt(modifCapital.modifDateContinuation.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_continuation']          = date;
}
formFields['newCocheContinuation']                                                   = true;
}

if (Value('id').of(modifCapital.modifCapitalType).contains('26M')) {
if (modifCapital.modifDateReconstitution !== null) {
    var dateTmp = new Date(parseInt(modifCapital.modifDateReconstitution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_reconstitutionCapitaux']          = date;
}
formFields['newCocheReconstitutionCapitaux']                                                   = true;
}

// Cadre 6 ESS / Société à Mission

if (Value('id').of(modifEss.modifEssAdhesionSortie).eq('modifEssAdhesion')) {
if (modifEss.modifDateESS !== null) {
    var dateTmp = new Date(parseInt(modifEss.modifDateESS.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_adhesionESS']          = date;
}
formFields['newCocheAdhesionESS']                                                    = true;
}

if (Value('id').of(modifEss.modifEssAdhesionSortie).eq('modifEssSortie')) {
if (modifEss.modifDateESS !== null) {
    var dateTmp = new Date(parseInt(modifEss.modifDateESS.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_sortieESS']          = date;
}
formFields['newCocheSortieESS']                                                    = true;
}

if (Value('id').of(modifSocieteMission.modifSocieteMissionAdhesionSortie).eq('modifSocieteMissionAdhesion')) {
if (modifSocieteMission.modifDateSocieteMission !== null) {
    var dateTmp = new Date(parseInt(modifSocieteMission.modifDateSocieteMission.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_adhesionSocieteMission']          = date;
}
formFields['newCocheAdhesionSocieteMission']                                                    = true;
}

if (Value('id').of(modifSocieteMission.modifSocieteMissionAdhesionSortie).eq('modifSocieteMissionSortie')) {
if (modifSocieteMission.modifDateSocieteMission !== null) {
    var dateTmp = new Date(parseInt(modifSocieteMission.modifDateSocieteMission.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_sortieSocieteMission']          = date;
}
formFields['newCocheSortieSocieteMission']                                                    = true;
}

// Cadre 7 Fusion

if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('fusion')) {
if (fusion.modifDateFusion !== null) {
    var dateTmp = new Date(parseInt(fusion.modifDateFusion.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_fusion']          = date;
}
formFields['newCocheFusion']                                                         = Value('id').of(fusion.fusionScission).eq('fusion') ? true : false;
formFields['newCocheScission']                                                       = Value('id').of(fusion.fusionScission).eq('scission') ? true : false;
formFields['newCocheOperationAvecAugmentationCapital']                               = fusion.fusionAugmentationCapital ? true  : false;
}

// Cadre 8 Mise en sommeil

if (dissolution.dissoMiseEnSommeil or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('40M')) {
if (fermeture.modifDateMiseEnSommeil !== null) {
    var dateTmp = new Date(parseInt(fermeture.modifDateMiseEnSommeil.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_miseEnSommeil']          = date;
} 
}

// Cadre 9 Dissolution

if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M')) {
if (dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateDissolution']          = date;
}
formFields['dissolution_avecPoursuiteBis']                              = (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M')
																		and not dissolution.dissoMiseEnSommeil
																		and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('40M')) ? true : false;
formFields['dissolution_sansPoursuiteBis']                              = (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M')
																		and (dissolution.dissoMiseEnSommeil
																		or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('40M'))) ? true : false;
formFields['dissolution_nomJournal']                                                 = dissolution.dissolutionJournalAnnoncesLegalesNom;
if (dissolution.dissolutionJournalAnnoncesLegalesDateParution !== null) {
    var dateTmp = new Date(parseInt(dissolution.dissolutionJournalAnnoncesLegalesDateParution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['dissolution_dateJournal']          = date;
} 
formFields['adresseLiquidation_siege']                                               = Value('id').of(dissolution.adresseLiquidationLieu).eq('siege') ? true : false;
formFields['adresseLiquidation_liquidateur']                                         = Value('id').of(dissolution.adresseLiquidationLieu).eq('liquidateur') ? true : false;
if (Value('id').of(dissolution.adresseLiquidationLieu).eq('autre')) {
formFields['adresseLiquidation_autre']                                               = true;
formFields['adresseLiquidation_autreAdresse']                                        = (dissolution.adresseLiquidation.numeroVoieAdresseLiquidation != null ? dissolution.adresseLiquidation.numeroVoieAdresseLiquidation : '')
																						+ ' ' + (dissolution.adresseLiquidation.indiceVoieAdresseLiquidation != null ? dissolution.adresseLiquidation.indiceVoieAdresseLiquidation : '')
																						+ ' ' + (dissolution.adresseLiquidation.typeVoieAdresseLiquidation != null ? dissolution.adresseLiquidation.typeVoieAdresseLiquidation : '')
																						+ ' ' + dissolution.adresseLiquidation.nomVoieAdresseLiquidation
																						+ ' ' + (dissolution.adresseLiquidation.voieComplementAdresseLiquidation != null ? dissolution.adresseLiquidation.voieComplementAdresseLiquidation : '')
																						+ ' ' + (dissolution.adresseLiquidation.voieDistributionSpecialeAdresseLiquidation != null ? dissolution.adresseLiquidation.voieDistributionSpecialeAdresseLiquidation : '')
																						+ ' ' + dissolution.adresseLiquidation.codePostalAdresseLiquidation
																						+ ' ' + dissolution.adresseLiquidation.communeAdresseLiquidation;
}
formFields['dissolutionCocheTransmissionPatrimoine']                                 = dissolution.dissolutionTransmissionUniverselle ? true : false;
}

// Cadre 10 Contrat d'appui

if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('37M')) {
if (modifContratAppui.modifDateAppui !== null) {
    var dateTmp = new Date(parseInt(modifContratAppui.modifDateAppui.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_ruptureContratAppui']          = date;
} 
}

// Cadre 11 - Déclaration relative à un établissement ou à l'activité 

formFields['modifConcerneOuverture']                                                 = (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M')
																					or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
																					and not objet.cadreObjetModificationEtablissement.lieuActiviteSiege)) ? true : false;
formFields['modifConcerneFermeture']                                                 = (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80M') 
																					or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('81M')
																					or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('40M')
																					or dissolution.dissoMiseEnSommeil) ? true : false;
formFields['modifConcerneModification']                                              = (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
																					or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63M')
																					or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60M')
																					or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
																					and objet.cadreObjetModificationEtablissement.lieuActiviteSiege)) ? true : false;
formFields['modifConcerneTransfert']                                                 = Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') ? true : false;
formFields['modifConcerneLocationGerance']                                           = (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M')
																					or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64M'))
																					and Value('id').of(objet.cadreObjetModificationEtablissement.typeContratReprise).eq('repriseLocation'))
																					or ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('82M')
																					or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('84M')
																					or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68M'))
																					and Value('id').of(fondsDonne.fondsDonneType).eq('fondsDonneLocation'))) ? true : false;
formFields['modifConcerneGeranceMandat']                                             = (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M')
																					or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64M'))
																					and Value('id').of(objet.cadreObjetModificationEtablissement.typeContratReprise).eq('repriseGerance'))
																					or ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('82M')
																					or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('84M')
																					or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68M'))
																					and Value('id').of(fondsDonne.fondsDonneType).eq('fondsDonneGerance'))) ? true : false;

// Cadre 12 - Etablissement transféré ou fermé

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80M') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('81M') 
	or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('40M')
	or dissolution.dissoMiseEnSommeil) {

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and fermeture.modifDateTransfert !== null) {
    var dateTmp = new Date(parseInt(fermeture.modifDateTransfert.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementTransfererFermer']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80M') and fermeture.modifDateFermeture !== null) {
    var dateTmp = new Date(parseInt(fermeture.modifDateFermeture.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementTransfererFermer']          = date;
} else if ((Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('40M')	or dissolution.dissoMiseEnSommeil) and fermeture.modifDateMiseEnSommeil !== null) {
    var dateTmp = new Date(parseInt(fermeture.modifDateMiseEnSommeil.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementTransfererFermer']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).contains('81M') and fermeture.modifDateFinActiviteSiege !== null) {
    var dateTmp = new Date(parseInt(fermeture.modifDateFinActiviteSiege.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementTransfererFermer']          = date;
}

formFields['modifCategarieEtablissementTransfererFermer_siege']                      = (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege') and not fermeture.lieuEtablissementPrincipal) ? true : false;
formFields['modifCategarieEtablissementTransfererFermer_siegeEtPrincipal']           = (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal') 
																						or ((Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') 
																						or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('40M')
																						or dissolution.dissoMiseEnSommeil)
																						and fermeture.lieuEtablissementPrincipal
																						and not Value('id').of(identite.entrepriseFormeJuridique).eq('5800') 
																						and not Value('id').of(identite.entrepriseFormeJuridique).eq('3110') 
																						and not Value('id').of(identite.entrepriseFormeJuridique).eq('3120'))
																						or (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege') 
																						and fermeture.lieuEtablissementPrincipal))	? true : false;
formFields['modifCategarieEtablissementTransfererFermer_principal']                  = ((((Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')
																						or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('40M')
																						or dissolution.dissoMiseEnSommeil)
																						and not fermeture.lieuEtablissementPrincipal) 
																						or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('81M'))
																						and not Value('id').of(identite.entrepriseFormeJuridique).eq('5800') 
																						and not Value('id').of(identite.entrepriseFormeJuridique).eq('3110') 
																						and not Value('id').of(identite.entrepriseFormeJuridique).eq('3120')) ? true : false;
formFields['modifCategarieEtablissementTransfererFermer_secondaire']                 = (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire') or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80M'))? true : false;
formFields['modifCategarieEtablissementTransfererFermer_1erEtablissementFrance']     = (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') and (Value('id').of(identite.entrepriseFormeJuridique).eq('5800') or Value('id').of(identite.entrepriseFormeJuridique).eq('3110') or Value('id').of(identite.entrepriseFormeJuridique).eq('3120'))) ? true : false;

if (((Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('40M')
	or dissolution.dissoMiseEnSommeil
	or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	and not fermeture.lieuEtablissementPrincipal)
	or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')) {
formFields['modifAncienneAdresseEtablissement_adresseVoie']             = (fermeture.cadreAdresseEtablissementFerme.numeroAdresseEtablissement != null ? fermeture.cadreAdresseEtablissementFerme.numeroAdresseEtablissement : '')
																		+ ' ' + (fermeture.cadreAdresseEtablissementFerme.indiceAdresseEtablissement != null ? fermeture.cadreAdresseEtablissementFerme.indiceAdresseEtablissement : '')
																		+ ' ' + (fermeture.cadreAdresseEtablissementFerme.typeAdresseEtablissement != null ? fermeture.cadreAdresseEtablissementFerme.typeAdresseEtablissement : '')
																		+ ' ' + (fermeture.cadreAdresseEtablissementFerme.voieAdresseEtablissement != null ? fermeture.cadreAdresseEtablissementFerme.voieAdresseEtablissement : '');
formFields['modifAncienneAdresseEtablissement_adresseComplement']        = (fermeture.cadreAdresseEtablissementFerme.complementAdresseEtablissement != null ? fermeture.cadreAdresseEtablissementFerme.complementAdresseEtablissement : '')
																		+ ' ' + (fermeture.cadreAdresseEtablissementFerme.distributionSpecialeAdresseEtablissement != null ? fermeture.cadreAdresseEtablissementFerme.distributionSpecialeAdresseEtablissement : '');
formFields['modifAncienneAdresseEtablissement_codePostal']               = fermeture.cadreAdresseEtablissementFerme.codePostalAdresseEtablissement;
formFields['modifAncienneAdresseEtablisseement_commune']        		 = fermeture.cadreAdresseEtablissementFerme.communeAdresseEtablissement;
formFields['modifAncienneAdresseEtablisseement_communeAncienne']         = fermeture.cadreAdresseEtablissementFerme.communeAncienneAdresseEtablissement;
} else if (((Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('40M')
	or dissolution.dissoMiseEnSommeil
	or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	and fermeture.lieuEtablissementPrincipal)
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('81M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege')
	or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal')) {
formFields['modifAncienneAdresseEtablissement_adresseVoie']         = (siege.siegeAdresseNumeroVoie != null ? siege.siegeAdresseNumeroVoie : '')
																	+ ' ' + (siege.siegeAdresseIndiceVoie != null ? siege.siegeAdresseIndiceVoie : '')
																	+ ' ' + (siege.siegeAdresseTypeVoie != null ? siege.siegeAdresseTypeVoie : '')
																	+ ' ' + siege.siegeAdresseNomVoie;
formFields['modifAncienneAdresseEtablissement_adresseComplement']   = (siege.siegeAdresseComplementVoie != null ? siege.siegeAdresseComplementVoie : '')
																	+ ' ' + (siege.siegeAdresseDistriutionSpecialeVoie != null ? siege.siegeAdresseDistriutionSpecialeVoie : '');
formFields['modifAncienneAdresseEtablissement_codePostal']          = siege.siegeAdresseCodePostal;
formFields['modifAncienneAdresseEtablisseement_commune']            = siege.siegeAdresseCommune;
formFields['modifAncienneAdresseEtablisseement_communeAncienne']    = siege.communeAncienneAdresseSiege != null ? siege.communeAncienneAdresseSiege : '';
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')) {
formFields['modifDestinationEtablissement_vendu']               = (Value('id').of(fermeture.destinationEtablissement2).eq('vendu') 
																or Value('id').of(fermeture.destinationEtablissement3).eq('vendu')
																or Value('id').of(fermeture.destinationEtablissement4).eq('vendu')
																or Value('id').of(fermeture.destinationEtablissement5).eq('vendu')
																or Value('id').of(fermeture.destinationEtablissement6).eq('vendu')
																or Value('id').of(fermeture.destinationEtablissement7).eq('vendu')) ? true : false;
formFields['modifDestinationEtablissement_ferme']               = (Value('id').of(fermeture.destinationEtablissement2).eq('ferme') 
																or Value('id').of(fermeture.destinationEtablissement3).eq('ferme')
																or Value('id').of(fermeture.destinationEtablissement4).eq('ferme')
																or Value('id').of(fermeture.destinationEtablissement5).eq('ferme')
																or Value('id').of(fermeture.destinationEtablissement6).eq('ferme')
																or Value('id').of(fermeture.destinationEtablissement7).eq('ferme')) ? true : false;
formFields['modifDestinationEtablissement_autre']               = (Value('id').of(fermeture.destinationEtablissement2).eq('autre') 
																or Value('id').of(fermeture.destinationEtablissement3).eq('autre')
																or Value('id').of(fermeture.destinationEtablissement3).eq('miseEnLocationPartielle')
																or Value('id').of(fermeture.destinationEtablissement7).eq('miseEnLocationPartielle')
																or Value('id').of(fermeture.destinationEtablissement3).eq('miseEnLocationTotale')
																or Value('id').of(fermeture.destinationEtablissement4).eq('autre')
																or Value('id').of(fermeture.destinationEtablissement5).eq('autre')
																or Value('id').of(fermeture.destinationEtablissement6).eq('autre')
																or Value('id').of(fermeture.destinationEtablissement7).eq('autre')) ? true : false;
formFields['modifDestinationEtablissementAutre_libelle']        = (Value('id').of(fermeture.destinationEtablissement2).eq('autre') 
																or Value('id').of(fermeture.destinationEtablissement3).eq('autre')
																or Value('id').of(fermeture.destinationEtablissement4).eq('autre')
																or Value('id').of(fermeture.destinationEtablissement5).eq('autre')
																or Value('id').of(fermeture.destinationEtablissement6).eq('autre')
																or Value('id').of(fermeture.destinationEtablissement7).eq('autre')) ? fermeture.destinationAutre : 
																((Value('id').of(fermeture.destinationEtablissement3).eq('miseEnLocationPartielle') 
																or Value('id').of(fermeture.destinationEtablissement7).eq('miseEnLocationPartielle')) ? "Mise en location-gérance ou en gérance-mandat du fonds" : 
																(Value('id').of(fermeture.destinationEtablissement3).eq('miseEnLocationTotale') ? "Mise en location-gérance ou en gérance-mandat du fonds unique avec maintien au RCS" : ''));
formFields['modifDestinationEtablissement_devientSiege']        = Value('id').of(fermeture.destinationEtablissement5).eq('devientSiege') ? true : false;
formFields['modifDestinationEtablissement_devientPrincipal']    = Value('id').of(fermeture.destinationEtablissement3).eq('devientPrincipal') ? true : false;
formFields['modifDestinationEtablissement_devientSecondaire']   = (Value('id').of(fermeture.destinationEtablissement4).eq('devientSecondaire')
																or Value('id').of(fermeture.destinationEtablissement5).eq('devientSecondaire')
																or Value('id').of(fermeture.destinationEtablissement6).eq('devientSecondaire')) ? true : false;
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80M') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('81M') 
	or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('40M')
	or dissolution.dissoMiseEnSommeil) {
formFields['modifDestinationEtablissement_supprime']               = Value('id').of(fermeture.destinationEtablissement1).eq('supprime') ? true : false;
formFields['modifDestinationEtablissement_vendu2']                 = Value('id').of(fermeture.destinationEtablissement1).eq('vendu') ? true : false;
formFields['modifDestinationEtablissement_autre2']                 = Value('id').of(fermeture.destinationEtablissement1).eq('autre') ? true : false;
formFields['modifDestinationEtablissementAutre2_libelle']          = Value('id').of(fermeture.destinationEtablissement1).eq('autre') ? fermeture.destinationAutre : '';
if (fermeture.dateCessationEmploi !== null) {
    var dateTmp = new Date(parseInt(fermeture.dateCessationEmploi.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDestinationEtablissementDateFinEmploiSalarie']          = date;
}
}
}

// Cadre 13 - Etablissement créé ou modifié

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64M')) {

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and fermeture.modifDateTransfert !== null) {
    var dateTmp = new Date(parseInt(fermeture.modifDateTransfert.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite') and ouverture.modifDatePriseActiviteEtablissement !== null) {
	var dateTmp = new Date(parseInt(ouverture.modifDatePriseActiviteEtablissement.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite') and activiteInfo.modifDatePriseActiviteSiege !== null) {
	var dateTmp = new Date(parseInt(activiteInfo.modifDatePriseActiviteSiege.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M') and ouverture.modifDateAdresseOuverture !== null) {
	var dateTmp = new Date(parseInt(ouverture.modifDateAdresseOuverture.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite') and activite.modifDateActivite !== null) {
	var dateTmp = new Date(parseInt(activite.modifDateActivite.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M') and ouverture.modifDateRepriseExploitation !== null) {
    var dateTmp = new Date(parseInt(ouverture.modifDateRepriseExploitation.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60M') and activiteInfo.cadreEtablissementIdentification.modifDateIdentification !== null) {
    var dateTmp = new Date(parseInt(activiteInfo.cadreEtablissementIdentification.modifDateIdentification.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63M') and activiteInfo.modifDateAcquisition !== null) {
    var dateTmp = new Date(parseInt(activiteInfo.modifDateAcquisition.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64M') and activiteInfo.modifDateRenouvellement !== null) {
    var dateTmp = new Date(parseInt(activiteInfo.modifDateRenouvellement.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
}
if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
	and not objet.cadreObjetModificationEtablissement.lieuActiviteSiege)
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M')
	or ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64M'))
	and not objet.cadreObjetModificationEtablissement.lieuSiege)) {
formFields['modifNouvelleAdresseEtablissement_voie']                    = (ouverture.cadreAdresseEtablissementOuvert.numeroAdresseEtablissement != null ? ouverture.cadreAdresseEtablissementOuvert.numeroAdresseEtablissement : '')
																		+ ' ' + (ouverture.cadreAdresseEtablissementOuvert.indiceAdresseEtablissement != null ? ouverture.cadreAdresseEtablissementOuvert.indiceAdresseEtablissement : '')
																		+ ' ' + (ouverture.cadreAdresseEtablissementOuvert.typeAdresseEtablissement != null ? ouverture.cadreAdresseEtablissementOuvert.typeAdresseEtablissement : '')
																		+ ' ' + ouverture.cadreAdresseEtablissementOuvert.voieAdresseEtablissement
																		+ ' ' + (ouverture.cadreAdresseEtablissementOuvert.complementAdresseEtablissement != null ? ouverture.cadreAdresseEtablissementOuvert.complementAdresseEtablissement : '')
																		+ ' ' + (ouverture.cadreAdresseEtablissementOuvert.distributionSpecialeAdresseEtablissement != null ? ouverture.cadreAdresseEtablissementOuvert.distributionSpecialeAdresseEtablissement : '');
formFields['modifNouvelleAdresseEtablissement_codePostal']              = ouverture.cadreAdresseEtablissementOuvert.codePostalAdresseEtablissement;
formFields['modifNouvelleAdresseEtablisseement_commune']                = ouverture.cadreAdresseEtablissementOuvert.communeAdresseEtablissement;
} else if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
	and objet.cadreObjetModificationEtablissement.lieuActiviteSiege)
	or ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64M'))
	and objet.cadreObjetModificationEtablissement.lieuSiege)) {
formFields['modifNouvelleAdresseEtablissement_voie']         		= (siege.siegeAdresseNumeroVoie != null ? siege.siegeAdresseNumeroVoie : '')
																	+ ' ' + (siege.siegeAdresseIndiceVoie != null ? siege.siegeAdresseIndiceVoie : '')
																	+ ' ' + (siege.siegeAdresseTypeVoie != null ? siege.siegeAdresseTypeVoie : '')
																	+ ' ' + siege.siegeAdresseNomVoie
																	+ ' ' + (siege.siegeAdresseComplementVoie != null ? siege.siegeAdresseComplementVoie : '')
																	+ ' ' + (siege.siegeAdresseDistriutionSpecialeVoie != null ? siege.siegeAdresseDistriutionSpecialeVoie : '');
formFields['modifNouvelleAdresseEtablissement_codePostal']          = siege.siegeAdresseCodePostal;
formFields['modifNouvelleAdresseEtablisseement_commune']            = siege.siegeAdresseCommune;
}
if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
	and Value('id').of(ouverture.etablissementDejaConnu).eq('oui'))
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M')
	and objet.cadreObjetModificationEtablissement.lieuSiege)
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64M')
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
	and objet.cadreObjetModificationEtablissement.lieuActiviteSiege))	{
formFields['modifCategarieEtablissementOuvert_effectifPresenceOui']      = activite.etablissementEffectifSalariePresenceOui ? true : (ouverture.etablissementEffectifPresenceOui ? true : false);
formFields['modifCategarieEtablissementOuvert_effectifPresenceNon']      = activite.etablissementEffectifSalariePresenceOui ? false : (ouverture.etablissementEffectifPresenceOui ? false : true);
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M')) {

formFields['modifCategarieEtablissementOuvert_siege']              = Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege') ? true : false;
formFields['modifCategarieEtablissementOuvert_siegeEtPrincipal']   = (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal') or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite') and objet.cadreObjetModificationEtablissement.lieuActiviteSiege) or (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissementBis).eq('activitePrincipal') and objet.cadreObjetModificationEtablissement.lieuSiege)) ? true : false;
formFields['modifCategarieEtablissementOuvert_principal']          = (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite') and not objet.cadreObjetModificationEtablissement.lieuActiviteSiege) or (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissementBis).eq('activitePrincipal') and not objet.cadreObjetModificationEtablissement.lieuSiege)) ? true : false;
formFields['modifCategarieEtablissementOuvert_secondaire']         = (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire') or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M') or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissementBis).eq('activiteSecondaire')) ? true : false;
}
if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
	and Value('id').of(ouverture.etablissementDejaConnu).eq('non')
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M')
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M')
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissementBis).eq('activiteSecondaire'))) {
formFields['modifCategarieEtablissementOuvert_fondePouvoirOui']                      = (ouverture.ajoutFondePouvoir) ? true : false ;
formFields['modifCategarieEtablissementOuvert_fondePouvoirNon']                      = (ouverture.ajoutFondePouvoir) ? false : true;
}
if (origine.creationDomiciliation) {
formFields['modifCategarieEtablissementOuvert_domiciliation']                        = true;
formFields['domiciliataire_nom']                                                     = origine.entrepriseLieeNom;
formFields['domiciliataire_siren']                                                   = origine.entrepriseLieeSiren.split(' ').join('');
}
}
	
// Cadre 14 - Activité

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
and Value('id').of(ouverture.etablissementDejaConnu).eq('non'))
or activiteInfo.modifActiviteTransfert or activiteInfo.modifActiviteReprise
or activiteInfo.modifActiviteAcquisition or activiteInfo.modifRepriseExploitation
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M')) {
							    
if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or activiteInfo.modifActiviteTransfert or activiteInfo.modifActiviteReprise
	or activiteInfo.modifActiviteAcquisition or activiteInfo.modifRepriseExploitation)
	and activite.modifDateActivite != null)	{
    var dateTmp = new Date(parseInt(activite.modifDateActivite.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_activite']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and fermeture.modifDateTransfert !== null) {
    var dateTmp = new Date(parseInt(fermeture.modifDateTransfert.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_activite']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M') and ouverture.modifDateAdresseOuverture !== null) {
    var dateTmp = new Date(parseInt(ouverture.modifDateAdresseOuverture.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_activite']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M') and ouverture.modifDateRepriseExploitation !== null) {
    var dateTmp = new Date(parseInt(ouverture.modifDateRepriseExploitation.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_activite']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite') and ouverture.modifDatePriseActiviteEtablissement !== null) {
    var dateTmp = new Date(parseInt(ouverture.modifDatePriseActiviteEtablissement.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_activite']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite') and activiteInfo.modifDatePriseActiviteSiege !== null) {
    var dateTmp = new Date(parseInt(activiteInfo.modifDatePriseActiviteSiege.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_activite']          = date;
} 

formFields['etablissement_activites']                                    = activite.etablissementActivitesAutres;
if (activite.etablissementActivitesPrincipales2 != null) {
formFields['etablissement_activitePlusImportante']                       = activite.etablissementActivitesPrincipales2;
}
formFields['entreprise_activitePermanenteSaisonniere_permanente']        = Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonnierePermanente') ? true : false;
formFields['entreprise_activitePermanenteSaisonniere_saisonniere']       = Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonniereSaisonniere') ? true : false;
formFields['etablissment_nonSedentariteQualite_nonSedentaire']           = activite.etablissementNonSedentariteQualiteNonSedentaire ? true : false;
formFields['etablissement_activiteLieuExercice_magasin']                 = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMagasin') ? true : false;
formFields['etablissement_activiteLieuExerciceMagasin']                  = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMagasin') ? activite.etablissementActiviteLieuExerciceMagasinSurface : '';
formFields['etablissement_activiteLieuExercice_marche']                  = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMarche') ? true : false;
formFields['etablissement_activiteLieuExercice_internet']                = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceDetail') ? true : false;
formFields['etablissement_activiteNature_commerceGros']                  = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceGros') ? true : false;
formFields['etablissement_activiteNature_batTravauxPublics']             = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureBatTravauxPublics') ? true : false;
formFields['etablissement_activiteNature_fabricationProduction']         = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureFabricationProduction') ? true : false;
formFields['etablissement_activiteNature_cocheAutre']                    = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCocheAutre') ? true : false;
formFields['etablissement_activiteNature_autre']                         = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCocheAutre') ? activite.etablissementActiviteNatureAutre : '';
if (Value('id').of(activite.activiteEvenement).eq('61M') or Value('id').of(activite.activiteEvenement).eq('61M62M') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') and activiteInfo.modifActiviteReprise)) {
formFields['modifActiviteSuiteAdjonction']                                           = true;
}
if (Value('id').of(activite.activiteEvenement).eq('62M') or Value('id').of(activite.activiteEvenement).eq('61M62M')) { 
formFields['modifActiviteSuiteSuppression']                                          = true;
formFields['modifActiviteSuiteSuppression_disparition']                              = Value('id').of(activite.suppressionActivitePar).eq('suppressionParDisparition') ? true : false;
formFields['modifActiviteSuiteSuppression_vente']                                    = Value('id').of(activite.suppressionActivitePar).eq('suppressionParVente') ? true : false;
formFields['modifActiviteSuiteSuppression_reprise']                                  = Value('id').of(activite.suppressionActivitePar).eq('suppressionParReprise') ? true : false;
formFields['modifActiviteSuiteSuppression_autre']                                    = Value('id').of(activite.suppressionActivitePar).eq('suppressionParAutre') ? true : false;
formFields['modifActiviteSuiteSuppression_autreLibelle']                             = Value('id').of(activite.suppressionActivitePar).eq('suppressionParAutre') ? activite.suppressionActiviteAutre : '';
}
}

// Cadres 15 - Enseigne / Nom commercial

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60M') 
	or activiteInfo.modifEtablissementEnseigne 
	or activite.etablissementNomCommercial != null
	or activite.etablissementEnseigne != null) {
		
if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60M') 
	or activiteInfo.modifEtablissementEnseigne) and activiteInfo.cadreEtablissementIdentification.modifDateIdentification) {
    var dateTmp = new Date(parseInt(activiteInfo.cadreEtablissementIdentification.modifDateIdentification.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_nomEtablissement']          = date;
} else if ((activite.etablissementNomCommercial != null or activite.etablissementEnseigne != null) and fermeture.modifDateTransfert !== null) {
    var dateTmp = new Date(parseInt(fermeture.modifDateTransfert.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_nomEtablissement']          = date;
} else if ((activite.etablissementNomCommercial != null or activite.etablissementEnseigne != null) and ouverture.modifDateAdresseOuverture !== null) {
    var dateTmp = new Date(parseInt(ouverture.modifDateAdresseOuverture.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_nomEtablissement']          = date;
} else if ((activite.etablissementNomCommercial != null or activite.etablissementEnseigne != null) and ouverture.modifDatePriseActiviteEtablissement !== null) {
    var dateTmp = new Date(parseInt(ouverture.modifDatePriseActiviteEtablissement.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_nomEtablissement']          = date;
} else if ((activite.etablissementNomCommercial != null or activite.etablissementEnseigne != null) and activiteInfo.modifDatePriseActiviteSiege !== null) {
    var dateTmp = new Date(parseInt(activiteInfo.modifDatePriseActiviteSiege.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_nomEtablissement']          = date;
} 
formFields['etablissement_enseigne']                                     = activite.etablissementEnseigne != null ? activite.etablissementEnseigne : (Value('id').of(activiteInfo.cadreEtablissementIdentification.modifNomEnseigne).contains('modifEnseigne') ? activiteInfo.cadreEtablissementIdentification.modifEnseigneNew : '');
formFields['etablissement_nomCommercialProfessionnel']                   = activite.etablissementNomCommercial != null ? activite.etablissementNomCommercial : (Value('id').of(activiteInfo.cadreEtablissementIdentification.modifNomEnseigne).contains('modifNomCommercial') ? activiteInfo.cadreEtablissementIdentification.modifNomCommercialNew : '');
} 

// Cadre 16 - Effectif salarié

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
	and not objet.cadreObjetModificationEtablissement.lieuActiviteSiege)
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
	and Value('id').of(ouverture.etablissementDejaConnu).eq('non'))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M')) {
if (activite.etablissementEffectifSalariePresenceOui) {					
formFields['effectifSalarieEtablissementCree']                = activite.cadreEffectifSalarie.etablissementEffectifSalarieNombre;
if (activite.cadreEffectifSalarie.etablissementDateEmbauche != null) {
    var dateTmp = new Date(parseInt(activite.cadreEffectifSalarie.etablissementDateEmbauche.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['dateEmbauche1erSalarie']          = date;
}
formFields['effectifSalarieTotal']                            = activite.cadreEffectifSalarie.totalEffectifSalarieNombre;
formFields['effectifSalarieApprentis']                        = activite.cadreEffectifSalarie.etablissementEffectifSalarieApprentis != null ? activite.cadreEffectifSalarie.etablissementEffectifSalarieApprentis : '';
formFields['apprentiseffectifSalarieVTC']                     = activite.cadreEffectifSalarie.etablissementEffectifSalarieVRP != null ? activite.cadreEffectifSalarie.etablissementEffectifSalarieVRP : '';
} else {
formFields['effectifSalarieEtablissementCree']                = "0";
}
}
	
// Cadre 17 - origine de l'activité

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
	and Value('id').of(ouverture.etablissementDejaConnu).eq('non'))
	or Value('id').of(activite.activiteEvenement).eq('61M')
	or Value('id').of(activite.activiteEvenement).eq('61M62M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64M')) {

if (Value('id').of(activite.activiteEvenement).eq('61M') or Value('id').of(activite.activiteEvenement).eq('61M62M') or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M') or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')) {
formFields['etablissement_origineFonds_creation']                        = Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation') ? true : false;
formFields['etablissement_origineFonds_achat']                           = Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsAchat') ? true : false;
formFields['etablissement_origineFonds_apport']                          = Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsApport') ? true : false;
formFields['etablissement_origineFonds_locationGerance']                 = Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsLocationGerance') ? true : false;
formFields['etablissement_origineFonds_geranceMandat']                   = Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsGeranceMandat') ? true : false;
formFields['etablissement_origineFonds_autre']                           = Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCocheAutre') ? true : false;
formFields['etablissement_origineFonds_autreLibelle']                    = Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCocheAutre') ? activite.etablissementOrigineFondsAutre : '';
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M')) {		
formFields['etablissement_origineFonds_autre']                           = true;
formFields['etablissement_origineFonds_autreLibelle']                    = Value('id').of(objet.cadreObjetModificationEtablissement.typeContratReprise).eq('repriseLocation') ? "Reprise de l'exploitation du fonds mis en location-gérance" : "Reprise de l'exploitation du fonds mis en gérance-mandat";
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63M')) {		
formFields['etablissement_origineFonds_autre']                           = true;
formFields['etablissement_origineFonds_autreLibelle']                    = "Acquisition du fonds par l'exploitant";
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64M')) {		
formFields['etablissement_origineFonds_autre']                           = true;
formFields['etablissement_origineFonds_autreLibelle']                    = Value('id').of(objet.cadreObjetModificationEtablissement.typeContratReprise).eq('repriseLocation') ? "Renouvellement du contrat de location-gérance" : "Renouvellement du contrat de gérance-mandat";
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')) {		
formFields['etablissement_origineFonds_creation']                        = Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsCreation') ? true : false;
formFields['etablissement_origineFonds_achat']                           = Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsAchat') ? true : false;
formFields['etablissement_origineFonds_apport']                          = Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsApport') ? true : false;
formFields['etablissement_origineFonds_reprise']                        = Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsreprise') ? true : false;
formFields['etablissement_origineFonds_locationGerance']                 = Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsLocationGerance') ? true : false;
formFields['etablissement_origineFonds_geranceMandat']                   = Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsGeranceMandat') ? true : false;
formFields['etablissement_origineFonds_autre']                           = Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsCocheAutre') ? true : false;
formFields['etablissement_origineFonds_autreLibelle']                    = Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsCocheAutre') ? activite.etablissementOrigineFondsAutre : '';
}		
if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsAchat')
	or Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsApport')
	or Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsLocationGerance')
	or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsAchat')
	or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsApport')
	or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsLocationGerance')
	or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsreprise')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M')) {
formFields['entrepriseLiee_entreprisePP_nom_precedentExploitant']        = Value('id').of(origine.precedentExploitant.typePersonnePrecedentExploitant).eq('typePersonnePrecedentExploitantPP') ? origine.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant : origine.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant;
formFields['entrepriseLiee_entreprisePP_nomUsage_precedentExploitant']   = (Value('id').of(origine.precedentExploitant.typePersonnePrecedentExploitant).eq('typePersonnePrecedentExploitantPP') and origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant != null) ? origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant : '';
formFields['entrepriseLiee_entreprisePP_prenom1_precedentExploitant']    = Value('id').of(origine.precedentExploitant.typePersonnePrecedentExploitant).eq('typePersonnePrecedentExploitantPP') ? origine.precedentExploitant.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant : '';
formFields['entrepriseLiee_siren_precedentExploitant']                   = origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant.split(' ').join('');
}
if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsAchat') or Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsApport') or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsAchat')	or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsApport')) {  
if (origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParution != null) {
	var dateTmp = new Date(parseInt(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParution.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_journalAnnoncesLegalesDateParution'] = date;
formFields['etablissement_journalAnnoncesLegalesNom']              = origine.precedentExploitant.etablissementJournalAnnoncesLegalesNom;
} else if(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParutionBis != null) {
	var dateTmp = new Date(parseInt(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParutionBis.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_journalAnnoncesLegalesDateParution'] = date;
formFields['etablissement_journalAnnoncesLegalesNom']              = origine.precedentExploitant.etablissementJournalAnnoncesLegalesNomBis;
}
}
if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsLocationGerance')
	or Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsGeranceMandat')
	or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsLocationGerance')
	or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsGeranceMandat')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64M')) {
if	(origine.geranceMandat.etablissementLoueurMandantDuFondsDebutContrat) {
	var dateTmp = new Date(parseInt(origine.geranceMandat.etablissementLoueurMandantDuFondsDebutContrat.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_loueurMandantDuFondsDebutContrat'] = date;
}
if (origine.geranceMandat.etablissementLoueurMandantDuFondsFinContrat != null) {
	var dateTmp = new Date(parseInt(origine.geranceMandat.etablissementLoueurMandantDuFondsFinContrat.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_loueurMandantDuFondsFinContrat'] = date;
}
formFields['etablissement_loueurMandantDuFondsRenouvellementTaciteReconduction_oui']    = origine.geranceMandat.etablissementLoueurMandantDuFondsRenouvellementTaciteReconductionOui ? true : false;
formFields['etablissement_loueurMandantDuFondsRenouvellementTaciteReconduction_non']    = origine.geranceMandat.etablissementLoueurMandantDuFondsRenouvellementTaciteReconductionOui ? false : true;
formFields['entrepriseLiee_entreprisePM_nom_loueurMandantDuFonds']                      = origine.geranceMandat.entrepriseLieeEntreprisePMDenominationLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeEntreprisePMDenominationLoueurMandantDuFonds : (origine.geranceMandat.entrepriseLieeEntreprisePMNomNaissanceLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeEntreprisePMNomNaissanceLoueurMandantDuFonds : '');
formFields['entrepriseLiee_entreprisePP_nomUsage_loueurMandantDuFonds']                 = origine.geranceMandat.entrepriseLieeEntreprisePPNomUsageLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeEntreprisePPNomUsageLoueurMandantDuFonds : '';
formFields['entrepriseLiee_entreprisePP_prenom1_loueurMandantDuFonds']                  = origine.geranceMandat.entrepriseLieeEntreprisePPPrenom1LoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeEntreprisePPPrenom1LoueurMandantDuFonds : '';
formFields['entrepriseLiee_adresse_nomVoie_loueurMandantDuFonds']                       = (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.numRueAdresseLoueurMandantDufonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.numRueAdresseLoueurMandantDufonds : '') 
																						+ ' ' + (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.indiceVoieLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.indiceVoieLoueurMandantDuFonds : '') 
																						+ ' ' + (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.typeVoieLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.typeVoieLoueurMandantDuFonds : '') 
																						+ ' ' + (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.nomVoieLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.nomVoieLoueurMandantDuFonds : '')                                                                                                                              
																				        + ' ' + (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.rueComplementAdresseLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.rueComplementAdresseLoueurMandantDuFonds : '') 
																						+ ' ' + (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.distributionSpecialeLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.distributionSpecialeLoueurMandantDuFonds : '');
formFields['entrepriseLiee_adresse_codePostal_loueurMandantDuFonds']                    = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.codePostalLoueurMandantDuFonds;
formFields['entrepriseLiee_adresse_commune_loueurMandantDuFonds']                       = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.communeLoueurMandantDuFonds;
}
if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsGeranceMandat')
	or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsGeranceMandat')
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64M')
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeContratReprise).eq('repriseGerance'))) {
formFields['entrepriseLiee_siren_geranceMandat']                                        = origine.geranceMandat.entrepriseLieeSirenGeranceMandat != null ? origine.geranceMandat.entrepriseLieeSirenGeranceMandat.split(' ').join('') : '';
formFields['entrepriseLiee_greffeImmatriculation']                                      = origine.geranceMandat.entrepriseLieeGreffeImmatriculation;
}
}

// Cadre 18 - Fonds donné en location-gérance

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('82M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('84M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68M')
	or Value('id').of(fermeture.destinationEtablissement3).eq('miseEnLocationPartielle')
	or Value('id').of(fermeture.destinationEtablissement7).eq('miseEnLocationPartielle')
	or Value('id').of(fermeture.destinationEtablissement3).eq('miseEnLocationTotale')){
		
if((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('82M') or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('84M') or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68M')) and fondsDonne.dateMiseEnLocation != null) {
	var dateTmp = new Date(parseInt(fondsDonne.dateMiseEnLocation.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['modifDate_miseEnGerance'] = date;
} else if ((Value('id').of(fermeture.destinationEtablissement3).eq('miseEnLocationPartielle') or Value('id').of(fermeture.destinationEtablissement7).eq('miseEnLocationPartielle') or Value('id').of(fermeture.destinationEtablissement3).eq('miseEnLocationTotale')) and fermeture.modifDateTransfert != null) {
	var dateTmp = new Date(parseInt(fermeture.modifDateTransfert.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['modifDate_miseEnGerance'] = date;
} 
if ((Value('id').of(objet.cadreObjetModificationEtablissement.fondsDonneEtablissement).eq('fondsDonneSecondaire') or not fondsDonne.lieuPrincipal)
and not Value('id').of(fermeture.destinationEtablissement3).eq('miseEnLocationPartielle')
and not Value('id').of(fermeture.destinationEtablissement3).eq('miseEnLocationTotale')
and not Value('id').of(fermeture.destinationEtablissement7).eq('miseEnLocationPartielle'))	{
formFields['modifMiseEnGerance_voie']                                                = (fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseNumeroVoie != null ? fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseNumeroVoie : '')
																					+ ' ' + (fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseIndiceVoie != null ? fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseIndiceVoie : '')
																					+ ' ' + (fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseTypeVoie != null ? fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseTypeVoie : '')
																					+ ' ' + fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseNomVoie
																					+ ' ' + (fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseComplementVoie != null ? fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseComplementVoie : '')
																					+ ' ' + (fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseDistriutionSpecialeVoie != null ? fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseDistriutionSpecialeVoie : '');
formFields['modifMiseEnGerance_codePostal']                                          = fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseCodePostal;
formFields['modifMiseEnGerance_commune']                                             = fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseCommune;
} else if ((Value('id').of(fermeture.destinationEtablissement3).eq('miseEnLocationPartielle') or Value('id').of(fermeture.destinationEtablissement7).eq('miseEnLocationPartielle') or Value('id').of(fermeture.destinationEtablissement3).eq('miseEnLocationTotale')) and fermeture.cadreAdresseEtablissementFerme.communeAdresseEtablissement != null) {	
formFields['modifMiseEnGerance_voie']                                                = (fermeture.cadreAdresseEtablissementFerme.numeroAdresseEtablissement != null ? fermeture.cadreAdresseEtablissementFerme.numeroAdresseEtablissement : '')
																					+ ' ' + (fermeture.cadreAdresseEtablissementFerme.indiceAdresseEtablissement != null ? fermeture.cadreAdresseEtablissementFerme.indiceAdresseEtablissement : '')
																					+ ' ' + (fermeture.cadreAdresseEtablissementFerme.typeAdresseEtablissement != null ? fermeture.cadreAdresseEtablissementFerme.typeAdresseEtablissement : '')
																					+ ' ' + fermeture.cadreAdresseEtablissementFerme.voieAdresseEtablissement
																					+ ' ' + (fermeture.cadreAdresseEtablissementFerme.complementAdresseEtablissement != null ? fermeture.cadreAdresseEtablissementFerme.complementAdresseEtablissement : '')
																					+ ' ' + (fermeture.cadreAdresseEtablissementFerme.distributionSpecialeAdresseEtablissement != null ? fermeture.cadreAdresseEtablissementFerme.distributionSpecialeAdresseEtablissement : '');
formFields['modifMiseEnGerance_codePostal']                                          = fermeture.cadreAdresseEtablissementFerme.codePostalAdresseEtablissement;
formFields['modifMiseEnGerance_commune']                                             = fermeture.cadreAdresseEtablissementFerme.communeAdresseEtablissement;
} else {
formFields['modifMiseEnGerance_voie']                				= (siege.siegeAdresseNumeroVoie != null ? siege.siegeAdresseNumeroVoie : '')
																	+ ' ' + (siege.siegeAdresseIndiceVoie != null ? siege.siegeAdresseIndiceVoie : '')
																	+ ' ' + (siege.siegeAdresseTypeVoie != null ? siege.siegeAdresseTypeVoie : '')
																	+ ' ' + siege.siegeAdresseNomVoie
																	+ ' ' + (siege.siegeAdresseComplementVoie != null ? siege.siegeAdresseComplementVoie : '')
																	+ ' ' + (siege.siegeAdresseDistriutionSpecialeVoie != null ? siege.siegeAdresseDistriutionSpecialeVoie : '');
formFields['modifMiseEnGerance_codePostal']  			            = siege.siegeAdresseCodePostal;
formFields['modifMiseEnGerance_commune']                            = siege.siegeAdresseCommune;
}	
formFields['modifNomLocataireGerant']                                                = fondsDonne.fondsDonneInformation.fondsDonneLocataireDenomination != null ? fondsDonne.fondsDonneInformation.fondsDonneLocataireDenomination : 
																					(fondsDonne.fondsDonneInformation.fondsDonneLocataireNomUsage != null ? 
																					(fondsDonne.fondsDonneInformation.fondsDonneLocataireNomUsage + ' ' + "né(e)" + ' ' + fondsDonne.fondsDonneInformation.fondsDonneLocataireNomNaissance + ' ' + fondsDonne.fondsDonneInformation.fondsDonneLocatairePrenom) : 
																					(fondsDonne.fondsDonneInformation.fondsDonneLocataireNomNaissance + ' ' + fondsDonne.fondsDonneInformation.fondsDonneLocatairePrenom));
formFields['modifMiseEnGerance_totaliteFonds']                                       = (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('84M')
																					or Value('id').of(fermeture.destinationEtablissement3).eq('miseEnLocationTotale')
																					or Value('id').of(fondsDonne.infosChangement).eq('totalite')) ? true : false;
formFields['modifMiseEnGerance_PartieFonds']                                         = (Value('id').of(fondsDonne.infosChangement).eq('partie') 
																					or Value('id').of(fermeture.destinationEtablissement3).eq('miseEnLocationPartielle')
																					or Value('id').of(fermeture.destinationEtablissement7).eq('miseEnLocationPartielle'))? true : false;
formFields['modifMiseEnGerance_PartieFondsLibelle']                                  = (Value('id').of(fondsDonne.infosChangement).eq('partie') 
																					or Value('id').of(fermeture.destinationEtablissement3).eq('miseEnLocationPartielle')
																					or Value('id').of(fermeture.destinationEtablissement7).eq('miseEnLocationPartielle')) ? fondsDonne.fondsDonnePartie : '';
formFields['modifMiseEnGerance_etablissementPrincipal']                              = (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('84M')
																					or Value('id').of(fermeture.destinationEtablissement3).eq('miseEnLocationTotale')
																					or Value('id').of(fermeture.destinationEtablissement3).eq('miseEnLocationPartielle')
																					or Value('id').of(objet.cadreObjetModificationEtablissement.fondsDonneEtablissement).eq('fondsDonnePrincipal')) ? true : false;
formFields['modifMiseEnGerance_etablissementSecondaire']                             = (Value('id').of(fermeture.destinationEtablissement7).eq('miseEnLocationPartielle')
																					or Value('id').of(objet.cadreObjetModificationEtablissement.fondsDonneEtablissement).eq('fondsDonneSecondaire')) ? true : false ;
formFields['modifMiseEnGerancePresenceSalarie_oui']                                  = fondsDonne.fondsDonneEffectifSalariePresenceOui ? true : (activite.etablissementEffectifSalariePresenceOui ? true : false);
formFields['modifMiseEnGerancePresenceSalarie_non']                                  = fondsDonne.fondsDonneEffectifSalariePresenceOui ? false : (activite.etablissementEffectifSalariePresenceOui ? false : true);

if (Value('id').of(fondsDonne.fondsDonneType).eq('fondsDonneGerance')) {
formFields['modifGerantMandataire_siren']                                            = fondsDonne.fondsDonneInformation.fondsDonneSirenGeranceMandat.split(' ').join('');
formFields['modifGerantMandataire_greffe']                                           = fondsDonne.fondsDonneInformation.fondsDonneGreffeGeranceMandat;
formFields['modifGerantMandataire_adresseVoie']                                      = (fondsDonne.fondsDonneInformation.adresseGerantMandataire.gerantMandataireAdresseNumeroVoie !=  null ? fondsDonne.fondsDonneInformation.adresseGerantMandataire.gerantMandataireAdresseNumeroVoie : '')
																					+ ' ' + (fondsDonne.fondsDonneInformation.adresseGerantMandataire.gerantMandataireAdresseIndiceVoie !=  null ? fondsDonne.fondsDonneInformation.adresseGerantMandataire.gerantMandataireAdresseIndiceVoie : '')
																					+ ' ' + (fondsDonne.fondsDonneInformation.adresseGerantMandataire.gerantMandataireAdresseTypeVoie !=  null ? fondsDonne.fondsDonneInformation.adresseGerantMandataire.gerantMandataireAdresseTypeVoie : '')
																					+ ' ' + fondsDonne.fondsDonneInformation.adresseGerantMandataire.gerantMandataireAdresseNomVoie
																					+ ' ' + (fondsDonne.fondsDonneInformation.adresseGerantMandataire.gerantMandataireAdresseComplementVoie !=  null ? fondsDonne.fondsDonneInformation.adresseGerantMandataire.gerantMandataireAdresseComplementVoie : '')
																					+ ' ' + (fondsDonne.fondsDonneInformation.adresseGerantMandataire.gerantMandataireDistriutionSpecialeVoie !=  null ? fondsDonne.fondsDonneInformation.adresseGerantMandataire.gerantMandataireDistriutionSpecialeVoie : '')																					;
formFields['modifGerantMandataire_adresseCodePostal']                                = fondsDonne.fondsDonneInformation.adresseGerantMandataire.gerantMandataireAdresseCodePostal;
formFields['modifGerantMandataire_adresseCommune']                                   = fondsDonne.fondsDonneInformation.adresseGerantMandataire.gerantMandataireAdresseCommune;
}	
}

// Cadre 19 - Dirigeant

if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and
	not dissolution.dissolutionTransmissionUniverselle) {
if (dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_dirigeant']          = date;
}
formFields['dirigeant_nouveau']                                                      = dissolution.cadreIdentiteLiquidateur.liquidateurDejaConnu ? false : true;
formFields['dirigeant_partant']                                                      = false;
formFields['dirigeant_modif']                                                        = false;
formFields['dirigeant_maintenu']                                                     = dissolution.cadreIdentiteLiquidateur.liquidateurDejaConnu ? true : false;
formFields['dirigeant_maintenuAncienneQualite']                                      = dissolution.cadreIdentiteLiquidateur.liquidateurDejaConnu ? dissolution.cadreIdentiteLiquidateur.liquidateurAncienneQualite.getLabel() : '';
formFields['dirigeant_qualite']                                                      = "Liquidateur";
if (Value('id').of(dissolution.cadreIdentiteLiquidateur.liquidateurPersonnaliteJuridique).eq('personnePhysique')) {
formFields['dirigeant_nomNaissance']                                                 = dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPNomNaissanceLiquidateur;
formFields['dirigeant_nomUsage']                                                     = dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPNomUsageLiquidateur != null ? dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPNomUsageLiquidateur : '';
var prenoms=[];
for ( i = 0; i < dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPPrenomLiquidateur.size() ; i++ ){prenoms.push(dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPPrenomLiquidateur[i]);}                            
formFields['dirigeant_prenom']                                      = prenoms.toString();
if (dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPDateNaissanceLiquidateur !== null) {
    var dateTmp = new Date(parseInt(dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPDateNaissanceLiquidateur.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['dirigeant_dateNaissance']          = date;
}
formFields['dirigeant_lieuNaissance']                                                = dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneLiquidateur != null ? (dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneLiquidateur + ' ' + '(' + '' + dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementLiquidateur.getId() + '' + ')') : (dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPLieuNaissancePaysLiquidateur.getLabel());
formFields['dirigeant_nationalite']                                                  = dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPNationaliteLiquidateur;
var nirDeclarant = dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant != null and dissolution.cadreIdentiteLiquidateur.liquidateurDejaConnu) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFields['dirigeantPartant_numSS']                = nirDeclarant.substring(0, 13);
    formFields['dirigeantPartant_numSSCle']             = nirDeclarant.substring(13, 15);
}
} else if (Value('id').of(dissolution.cadreIdentiteLiquidateur.liquidateurPersonnaliteJuridique).eq('personneMorale')) {
formFields['dirigeant_denoFormeJuridique']                                           = dissolution.cadreIdentiteLiquidateur.civilitePersonneMorale.personneLieePMDenomination + ' / ' + dissolution.cadreIdentiteLiquidateur.civilitePersonneMorale.personneLieePMFormeJuridique.getLabel();
formFields['dirigeant_PM_immatriculation']                                           = dissolution.cadreIdentiteLiquidateur.civilitePersonneMorale.personneLieePMLieuImmatriculation + ' / ' + dissolution.cadreIdentiteLiquidateur.civilitePersonneMorale.personneLieePMNumeroIdentification.split(' ').join('');
formFields['dirigeant_PM_presenceRepresentant']                                      = false;
}
formFields['dirigeant_adresseVoie']                                                  = (dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseNumeroVoie != null ? dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseNumeroVoie : '')
																					+ ' ' + (dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseIndiceVoie != null ? dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseIndiceVoie : '')
																					+ ' ' + (dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseTypeVoie != null ? dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseTypeVoie : '')
																					+ ' ' + dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseNomVoie
																					+ ' ' + (dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseComplementAdresse != null ? dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseComplementAdresse : '')
																					+ ' ' + (dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseDistriutionSpecialeVoie != null ? dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseDistriutionSpecialeVoie : '');
formFields['dirigeant_adresseCP']                                                    = dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseCodePostal;
formFields['dirigeant_adresseCommune']                                               = dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseCommune;
formFields['dirigeant_partantBis']                                                   = false;
formFields['dirigeantPartant_nom']                                                   = '';
} else if (ouverture.ajoutFondePouvoir
and not $M2.dirigeant1Sarl.autrePersonneLieeEtablissement
and not $M2.dirigeant1.autrePersonneLieeEtablissement) {
if (ouverture.ajoutFondePouvoir and ouverture.modifDateAdresseOuverture != null) {
    var dateTmp = new Date(parseInt(ouverture.modifDateAdresseOuverture.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_dirigeant']          = date;
} else if (ouverture.ajoutFondePouvoir and ouverture.modifDateRepriseExploitation != null) {
    var dateTmp = new Date(parseInt(ouverture.modifDateRepriseExploitation.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_dirigeant']          = date;
}else if (ouverture.ajoutFondePouvoir and fermeture.modifDateTransfert != null) {
    var dateTmp = new Date(parseInt(fermeture.modifDateTransfert.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_dirigeant']          = date;
}else if (ouverture.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite') and ouverture.modifDatePriseActiviteEtablissement != null) {
    var dateTmp = new Date(parseInt(ouverture.modifDatePriseActiviteEtablissement.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_dirigeant']          = date;
}
formFields['dirigeant_nouveau']                                                      = true;
formFields['dirigeant_partant']                                                      = false;
formFields['dirigeant_modif']                                                        = false;
formFields['dirigeant_maintenu']                                                     = false;
formFields['dirigeant_maintenuAncienneQualite']                                      = '';
formFields['dirigeant_qualite']                                                      = "Fondé de pouvoir";
formFields['dirigeant_nomNaissance']                                                 = dirigeantSARL1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeantSARL1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : '');
formFields['dirigeant_nomUsage']                                                     = dirigeantSARL1.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantSARL1.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant : '');
if (dirigeantSARL1.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeantSARL1.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeantSARL1.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantSARL1.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFields['dirigeant_prenom']                                      = prenoms.toString();
} else if (dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFields['dirigeant_prenom']                                      = prenoms.toString();
}
if (dirigeantSARL1.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeantSARL1.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['dirigeant_dateNaissance']          = date;
} else if (dirigeant1.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeant1.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['dirigeant_dateNaissance']          = date;
}
formFields['dirigeant_lieuNaissance']                            = dirigeantSARL1.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(dirigeantSARL1.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' + '(' + '' + dirigeantSARL1.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() + '' + ')') : 
																(dirigeantSARL1.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
																(dirigeantSARL1.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeantSARL1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel()) : 
																(dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ?
																(dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' + '(' + '' + dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() + '' + ')') : 
																(dirigeant1.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
																(dirigeant1.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel()) : '')));
formFields['dirigeant_nationalite']                              = dirigeantSARL1.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeantSARL1.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeant1.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant1.civilitePersonnePhysique.personneLieePPNationaliteGerant : '');
formFields['dirigeantPartant_numSS']                = '';
formFields['dirigeantPartant_numSSCle']             = '';
if (dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
formFields['dirigeant_adresseVoie']                             = (dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
																+ ' ' + (dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
																+ ' ' + (dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
																+ ' ' + dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseNomVoie
																+ ' ' + (dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
																+ ' ' + (dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFields['dirigeant_adresseCP']                               = dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFields['dirigeant_adresseCommune']                          = dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseVille != null ? (dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdressePays) : '');
} else if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
formFields['dirigeant_adresseVoie']                             = (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
																+ ' ' + (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
																+ ' ' + (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
																+ ' ' + dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNomVoie
																+ ' ' + (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
																+ ' ' + (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFields['dirigeant_adresseCP']                               = dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFields['dirigeant_adresseCommune']                          = dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseVille != null ? (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays) : '');
}
formFields['dirigeant_partantBis']                                                   = false;
formFields['dirigeantPartant_nom']                                                   = '';
}

// Cadre 20 - Observation

if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('29M') and objet.cadreModifDateActivite.newDateDebutActivite !== null) {
    var dateTmp = new Date(parseInt(objet.cadreModifDateActivite.newDateDebutActivite.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_observations']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('23M') and modifRenouvellementMaintienRCS.modifDateRenouvellementMaintienRCS1 !== null) {
    var dateTmp = new Date(parseInt(modifRenouvellementMaintienRCS.modifDateRenouvellementMaintienRCS1.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDate_observations']          = date;
	var dateRenouvellementMaintien = date;
}

if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('23M') and modifRenouvellementMaintienRCS.modifDateRenouvellementMaintienRCS1 !== null) {
    var dateTmp = new Date(parseInt(modifRenouvellementMaintienRCS.modifDateRenouvellementMaintienRCS1.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    var dateRenouvellementMaintien = date;
}

if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('29M') and Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('23M')) {
formFields['observations']                 = "Modification de la date de début d'activité" + ' / ' +  dateRenouvellementMaintien + ' ' + "Demande de renouvellement du maintien au RCS" + '' + (correspondance.formaliteObservations != null ? '' + ' / ' + correspondance.formaliteObservations : '');
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('29M') and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('23M')) {
formFields['observations']                 = "Modification de la date de début d'activité" + '' + (correspondance.formaliteObservations != null ? '' + ' / ' + correspondance.formaliteObservations : '');
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('29M') and Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('23M')) {
formFields['observations']                 = "Demande de renouvellement du maintien au RCS" + '' + (correspondance.formaliteObservations != null ? '' + ' / ' + correspondance.formaliteObservations : '');
} else {	
formFields['observations']                 = correspondance.formaliteObservations != null ? correspondance.formaliteObservations : '';
}

// Cadre 21 - Adresse de correspondance

formFields['adresseCorrespondanceCadre_coche']            = (Value('id').of(correspondance.adresseCorrespond).eq('siege') 
															or Value('id').of(correspondance.adresseCorrespond).eq('liquidation') 
															or Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement')
															or Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') 
															or Value('id').of(correspondance.adresseCorrespond).eq('fondsDonneEtablissement')) ? true : false;
formFields['adresseCorrespondanceCadre_numero']           = Value('id').of(correspondance.adresseCorrespond).eq('siege') ? "2" : 
															(Value('id').of(correspondance.adresseCorrespond).eq('siege') ? "9" :
															(Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement') ? "12" :
															(Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') ? "13" : 
															(Value('id').of(correspondance.adresseCorrespond).eq('fondsDonneEtablissement') ? "18" : ''))));
formFields['adressesCorrespondanceAutre']                 = Value('id').of(correspondance.adresseCorrespond).eq('autre') ? true : false;
if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
formFields['adresseCorrespondance_voie1']                 = correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance
															+ ' ' + (correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance : '')
															+ ' ' + (correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance : '')
															+ ' ' + (correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance : '')
															+ ' ' + (correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance : '');
formFields['adresseCorrespondance_voie2']                 = (correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance != null ? correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance : '')
															+ ' ' + (correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance != null ? correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance : '');
formFields['adresseCorrespondance_codePostal']            = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal;
formFields['adresseCorrespondance_commune']               = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune;
}
formFields['telephone2']                                                             = correspondance.infosSup.formaliteTelephone1 != null ? correspondance.infosSup.formaliteTelephone1 : '';
formFields['telephone1']                                                             = correspondance.infosSup.formaliteTelephone2;
formFields['courriel']                                                               = correspondance.infosSup.formaliteFaxCourriel != null ? correspondance.infosSup.formaliteFaxCourriel : (correspondance.infosSup.telecopie != null ? correspondance.infosSup.telecopie : '');

// Cadre 20 - Signataire

formFields['signataireDeclarant']                                                    = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFields['signataireMandataire']                                                   = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? true : false;
formFields['signataireAutre']                                                        = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteAutre') ? true : false;
formFields['nomPrenomDenominationMandataire']					                     = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFields['signataireAdresse1']                                                     = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '');
																					 + ' ' + (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '');
formFields['formalite_signataireAdresse_CP']  		       							 = signataire.adresseMandataire.dataCodePostalMandataire != null ? signataire.adresseMandataire.dataCodePostalMandataire : '';
formFields['formalite_signataireAdresse_commune']  		    						 = signataire.adresseMandataire.villeAdresseMandataire != null ? signataire.adresseMandataire.villeAdresseMandataire : '';

if (signataire.formaliteSignatureDate !== null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['signatureDate']          = date;
}
formFields['signatureLieu']                                                          = signataire.formaliteSignatureLieu;
if (not dissolution.cadreIdentiteLiquidateur.autreDirigeant
and (not ouverture.ajoutFondePouvoir or (ouverture.ajoutFondePouvoir
and not $M2.dirigeant1Sarl.autrePersonneLieeEtablissement
and not $M2.dirigeant1.autrePersonneLieeEtablissement))
and not  Value('id').of(objet.objetModification).contains('dirigeant')) {
formFields['intercalaireNombreM3']                                                   = "0";
formFields['intercalaireNombreTNS']                                                  = "0";
}
formFields['intercalaireNombreJQPA']                                                 = jqpa3.jqpaActiviteJqpaPP != null ? "3" : (jqpa2.jqpaActiviteJqpaPP != null ? "2" : (jqpa1.jqpaActiviteJqpaPP != null ? "1" : '0'));
formFields['nombreIntercalaireMBE']                                                  = signataire.nombreMBE != null ? signataire.nombreMBE.getLabel() : "0";
formFields['signature']                                                              = '';

 
// Imprimé M3 sarl 

if (((dissolution.cadreIdentiteLiquidateur.autreDirigeant
	or Value('id').of(objet.objetModification).contains('dirigeant')
	or (ouverture.ajoutFondePouvoir and ($M2.dirigeant1Sarl.autrePersonneLieeEtablissement or $M2.dirigeant1.autrePersonneLieeEtablissement)))
	and (Value('id').of(identite.entrepriseFormeJuridique).eq('5499') or Value('id').of(identite.entrepriseFormeJuridique).eq('5410')
	or Value('id').of(identite.entrepriseFormeJuridique).eq('5415')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5422')
	or Value('id').of(identite.entrepriseFormeJuridique).eq('5426')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5430')
	or Value('id').of(identite.entrepriseFormeJuridique).eq('5431')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5432')
	or Value('id').of(identite.entrepriseFormeJuridique).eq('5442')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5443')
	or Value('id').of(identite.entrepriseFormeJuridique).eq('5451')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5453')
	or Value('id').of(identite.entrepriseFormeJuridique).eq('5454')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5455')
	or Value('id').of(identite.entrepriseFormeJuridique).eq('5458')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5459')
	or Value('id').of(identite.entrepriseFormeJuridique).eq('5460')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5485'))
	and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M'))
	or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')
	and (modifFormeJuridique.formeJuridiqueModifDirigeant
	or Value('id').of(objet.objetModification).contains('dirigeant')
	or (ouverture.ajoutFondePouvoir and ($M2.dirigeant1Sarl.autrePersonneLieeEtablissement or $M2.dirigeant1.autrePersonneLieeEtablissement)))
	and (Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5499') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5410')
	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5415')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5422')
	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5426')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5430')
	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5431')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5432')
	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5442')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5443')
	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5451')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5453')
	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5454')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5455')
	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5458')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5459')
	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5460')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5485')))) {

// Cadre 1

																	 
formFieldsPers1['demandeModificationRM_M3']                   = identite.immatriculationRM ? true : false;
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
formFieldsPers1['demandeModificationM3']                      = false;
formFieldsPers1['intercalaireNumM3']                          = "1";
formFieldsPers1['demandeIntercalaireM3']                      = true;
} else {
formFieldsPers1['demandeModificationM3']                      = true;
formFieldsPers1['intercalaireNumM3']                          = '';
formFieldsPers1['demandeIntercalaireM3']                      = false;
} 
 
// Cadre 2

formFieldsPers1['entreprise_sirenM3']                                     = identite.siren.split(' ').join('');
formFieldsPers1['immatRCS_M3']                                            = true;
formFieldsPers1['immatRCSGreffe_M3']                                      = identite.immatRCSGreffe;
formFieldsPers1['immatRM_M3']                                             = identite.immatriculationRM ? true : false;
formFieldsPers1['immatRMDept_M3']                                         = identite.immatriculationRM ? identite.immatRMCMA : '';
formFieldsPers1['immatRMDeptNum_M3']                                      = identite.immatriculationRM ? identite.immatRMCMA.getId() : '';
formFieldsPers1['entreprise_denominationM3']                              = identite.entrepriseDenomination;
formFieldsPers1['entreprise_formeJuridiqueM3']                            = identite.entrepriseFormeJuridique.getLabel();

// Cadre 3

if (dirigeantSARL1.cadreGerance.typeGeranceModifiee or dirigeantSARL2.cadreGerance.typeGeranceModifiee
	or dirigeantSARL3.cadreGerance.typeGeranceModifiee or dirigeantSARL4.cadreGerance.typeGeranceModifiee
	or dirigeantSARL5.cadreGerance.typeGeranceModifiee) { 
formFieldsPers1['modifNatureGeranceM3_oui']                   = true;
formFieldsPers1['modifNatureGeranceM3_non']                   = false;
formFieldsPers1['natureGeranceM3_NonMaj']                     = (Value('id').of(dirigeantSARL1.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')
																or Value('id').of(dirigeantSARL2.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')
																or Value('id').of(dirigeantSARL3.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')
																or Value('id').of(dirigeantSARL4.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')
																or Value('id').of(dirigeantSARL5.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')) ? true : false;
formFieldsPers1['natureGeranceM3_Maj']                        = (Value('id').of(dirigeantSARL1.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire')
																or Value('id').of(dirigeantSARL2.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire')
																or Value('id').of(dirigeantSARL3.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire')
																or Value('id').of(dirigeantSARL4.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire')
																or Value('id').of(dirigeantSARL5.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire')) ? true : false;
formFieldsPers1['natureGeranceM3_societeAssociee']            = ((Value('id').of(dirigeantSARL1.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')
																or Value('id').of(dirigeantSARL2.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')
																or Value('id').of(dirigeantSARL3.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')
																or Value('id').of(dirigeantSARL4.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')
																or Value('id').of(dirigeantSARL5.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire'))
																and (dirigeantSARL1.cadreGerance.entrepriseNatureGeranceSocieteAssocieeOui or dirigeantSARL2.cadreGerance.entrepriseNatureGeranceSocieteAssocieeOui
																or dirigeantSARL3.cadreGerance.entrepriseNatureGeranceSocieteAssocieeOui or dirigeantSARL4.cadreGerance.entrepriseNatureGeranceSocieteAssocieeOui
																or dirigeantSARL5.cadreGerance.entrepriseNatureGeranceSocieteAssocieeOui)) ? true : false;
}

var gerants = [];

['dirigeant1Sarl', 'dirigeant2Sarl', 'dirigeant3Sarl', 'dirigeant4Sarl', 'dirigeant5Sarl'].forEach(function(gerant){
	if (null != $M2[gerant] and ((Value('id').of($M2[gerant].cadreIdentiteDirigeant.personneLieeQualite).eq('30') or Value('id').of($M2[gerant].cadreIdentiteDirigeant.personneLieeQualite).eq('40'))
		or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and Value('id').of($M2[gerant].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant') and $M2[gerant].cadreIdentiteDirigeant.personneLieeQualiteAncienne2 != null))) {
		gerants.push($M2[gerant]);
}
});

// Dirigeant 1

// Cadre 4 

if (gerants.length > 0) {

if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['modifDate_dirigeantM3[0]']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null and modifFormeJuridique.formeJuridiqueModifDirigeant) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['modifDate_dirigeantM3[0]']          = date;
} else if (gerants[0].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(gerants[0].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['modifDate_dirigeantM3[0]']          = date;
}
formFieldsPers1['dirigeant_nouveauM3[0]']                     = Value('id').of(gerants[0].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau') ? true : false;
formFieldsPers1['dirigeant_partantM3[0]']                     = Value('id').of(gerants[0].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers1['dirigeant_modifM3[0]']                       = (Value('id').of(gerants[0].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantModif') or Value('id').of(gerants[0].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantMaintenu')) ? true : false;
if (not Value('id').of(gerants[0].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers1['dirigeant_nomNaissanceM3[0]']                = gerants[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
formFieldsPers1['dirigeant_nomUsageM3[0]']                    = gerants[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? gerants[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < gerants[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(gerants[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['dirigeant_prenomM3[0]']                      = prenoms.toString();
if (gerants[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(gerants[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['dirigeant_dateNaissanceM3[0]']          = date;
}
formFieldsPers1['dirigeant_lieuNaissanceM3[0]']               = gerants[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(gerants[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' + '(' + '' + gerants[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() + '' + ')') : 
																(gerants[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + gerants[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant);
formFieldsPers1['dirigeant_nationaliteM3[0]']                 = gerants[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNationaliteGerant;
formFieldsPers1['dirigeant_adresseVoieM3[0]']                 = (gerants[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? gerants[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
															  + ' ' + (gerants[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? gerants[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
															  + ' ' + (gerants[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? gerants[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
															  + ' ' + gerants[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNomVoie
															  + ' ' + (gerants[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? gerants[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
															  + ' ' + (gerants[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? gerants[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers1['dirigeant_adresseCPM3[0]']         = gerants[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? gerants[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers1['dirigeant_adresseCommuneM3[0]']              = gerants[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? gerants[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune : (gerants[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille != null ? (gerants[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + gerants[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays) : '');
}
if (Value('id').of(gerants[0].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers1['dirigeant_partantBisM3[0]']                  = true;
formFieldsPers1['dirigeantPartant_nomNaissanceM3[0]']         = gerants[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
formFieldsPers1['dirigeantPartant_nomUsageM3[0]']             = gerants[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? gerants[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < gerants[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(gerants[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['dirigeantPartant_prenomM3[0]']                           = prenoms.toString();
}
var nirDeclarant = gerants[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers1['dirigeantPartant_numSSM3[0]']                = nirDeclarant.substring(0, 13);
    formFieldsPers1['dirigeantPartant_numSSCleM3[0]']             = nirDeclarant.substring(13, 15);
}

// Cadre 5 

if ((Value('id').of(gerants[0].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau') or (Value('id').of(gerants[0].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantMaintenu') and Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M'))) and gerants[0].cadreIdentiteDirigeant.conjoint) {
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['modifDate_conjointM3[0]']          = date;
} else if (gerants[0].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(gerants[0].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['modifDate_conjointM3[0]']          = date;
} 
formFieldsPers1['conjointM3_activiteDansEntrepriseOui[0]']    = gerants[0].cadreIdentiteDirigeant.conjointRole ? true : false;
formFieldsPers1['conjointM3_activiteDansEntrepriseNon[0]']    = gerants[0].cadreIdentiteDirigeant.conjointRole ? false : true;
formFieldsPers1['conjoint_collaborateurM3[0]']                = Value('id').of(gerants[0].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') ? true : false;
formFieldsPers1['conjoint_salarieM3[0]']                      = (Value('id').of(gerants[0].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutSalarie') or Value('id').of(gerants[0].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutSalarie')) ? true : false;
formFieldsPers1['conjoint_associeM3[0]']                      = (Value('id').of(gerants[0].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie') or Value('id').of(gerants[0].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutAssocie')) ? true : false;
} 
 
// Cadre 6 

if (Value('id').of(gerants[0].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') 
or Value('id').of(gerants[0].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutSalarie') 
or Value('id').of(gerants[0].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutSalarie')
or gerants[0].cadreIdentiteDirigeant.civilitePersonnePhysique.modifConjoint) {
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['modifDate_modifConjointM3[0]']          = date;
} else if (gerants[0].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(gerants[0].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['modifDate_modifConjointM3[0]']          = date;
} 
if (not Value('id').of(gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.objetModifConjoint).eq('suppressionMentionConjoint')) { 
formFieldsPers1['conjoint_nouveauM3[0]']                      = true;
formFieldsPers1['conjoint_partantM3[0]']                      = false;
formFieldsPers1['conjoint_nomNaissanceM3[0]']                 = gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomNaissanceConjointPacse;
formFieldsPers1['conjoint_nomUsageM3[0]']                     = gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse != null ? gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers1['conjoint_prenomM3[0]']                      = prenoms.toString();
if (gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPDateNaissanceConjointPacse != null) {
    var dateTmp = new Date(parseInt(gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['conjoint_dateNaissanceM3[0]']          = date;
}
formFieldsPers1['conjoint_lieuNaissanceM3[0]']                = gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPLieuNaissanceCommnuneConjointPacse != null ? (gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPLieuNaissanceCommnuneConjointPacse + ' ' + "(" + '' + gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPDeptNaissanceConjointPacse.getId() + '' + ")") : 
															 (gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePersonnePhysiqueLieuNaissanceVilleConjointPacse + ' / ' + gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPPaysNaissanceConjointPacse);
formFieldsPers1['conjoint_nationaliteM3[0]']                  = gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNationaliteConjointPacse;
if (gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseConjointDifferente) {
formFieldsPers1['conjoint_adresseVoieM3[0]']                  = (gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointNumeroVoie != null ? gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointNumeroVoie : '')
															  + ' ' + (gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointIndiceVoie != null ? gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointIndiceVoie : '')
															  + ' ' + (gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointTypeVoie != null ? gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointTypeVoie : '')
															  + ' ' + gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointNomVoie
															  + ' ' + (gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointComplementVoie != null ? gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointComplementVoie : '')
															  + ' ' + (gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null ? gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie : '');
formFieldsPers1['conjoint_adresseCPM3[0]']                    = gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointCodePostal != null ? gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointCodePostal : '';
formFieldsPers1['conjoint_adresseCommuneM3[0]']               = gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointCommune != null ? gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointCommune : (gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointVille + ' / ' + gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointPays);
}}
if (Value('id').of(gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.objetModifConjoint).eq('suppressionMentionConjoint')) { 
formFieldsPers1['conjoint_partantM3[0]']                      = true;
formFieldsPers1['conjoint_partantBis[0]']                     = true;
formFieldsPers1['conjointPartant_nomNaissanceM3[0]']                 = gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomNaissanceConjointPacse;
formFieldsPers1['conjointPartant_nomUsageM3[0]']                     = gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse != null ? gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers1['conjointPartant_prenomM3[0]']                      = prenoms.toString();
var nirDeclarant = gerants[0].cadreIdentiteDirigeant.cadre2InfosConjoint.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers1['conjoint_numSSM3[0]']                = nirDeclarant.substring(0, 13);
    formFieldsPers1['conjoint_numSSCleM3[0]']             = nirDeclarant.substring(13, 15);
}
}
formFieldsPers1['conjoint_rappelGerantM3[0]']                 = '';
} 
}


// Dirigeant 2

// Cadre 7

if (gerants.length > 1) {

if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['modifDate_dirigeantM3[1]']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null and modifFormeJuridique.formeJuridiqueModifDirigeant) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['modifDate_dirigeantM3[1]']          = date;
} else if (gerants[1].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(gerants[1].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['modifDate_dirigeantM3[1]']          = date;
}
formFieldsPers1['dirigeant_nouveauM3[1]']                     = Value('id').of(gerants[1].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau') ? true : false;
formFieldsPers1['dirigeant_partantM3[1]']                     = Value('id').of(gerants[1].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers1['dirigeant_modifM3[1]']                       = (Value('id').of(gerants[1].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantModif') or Value('id').of(gerants[1].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantMaintenu')) ? true : false;
if (not Value('id').of(gerants[1].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers1['dirigeant_nomNaissanceM3[1]']                = gerants[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
formFieldsPers1['dirigeant_nomUsageM3[1]']                    = gerants[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? gerants[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < gerants[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(gerants[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['dirigeant_prenomM3[1]']                      = prenoms.toString();
if (gerants[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(gerants[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['dirigeant_dateNaissanceM3[1]']          = date;
}
formFieldsPers1['dirigeant_lieuNaissanceM3[1]']               = gerants[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(gerants[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' + '(' + '' + gerants[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() + '' + ')') : 
																(gerants[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + gerants[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant);
formFieldsPers1['dirigeant_nationaliteM3[1]']                 = gerants[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNationaliteGerant;
formFieldsPers1['dirigeant_adresseVoieM3[1]']                 = (gerants[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? gerants[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
															  + ' ' + (gerants[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? gerants[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
															  + ' ' + (gerants[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? gerants[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
															  + ' ' + gerants[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNomVoie
															  + ' ' + (gerants[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? gerants[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
															  + ' ' + (gerants[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? gerants[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers1['dirigeant_adresseCPM3[1]']         = gerants[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? gerants[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers1['dirigeant_adresseCommuneM3[1]']              = gerants[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? gerants[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune : (gerants[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille != null ? (gerants[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + gerants[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays) : '');
}
if (Value('id').of(gerants[1].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers1['dirigeant_partantBisM3[1]']                  = true;
formFieldsPers1['dirigeantPartant_nomNaissanceM3[1]']         = gerants[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
formFieldsPers1['dirigeantPartant_nomUsageM3[1]']             = gerants[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? gerants[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < gerants[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(gerants[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['dirigeantPartant_prenomM3[1]']                           = prenoms.toString();
}
var nirDeclarant = gerants[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers1['dirigeantPartant_numSSM3[1]']                = nirDeclarant.substring(0, 13);
    formFieldsPers1['dirigeantPartant_numSSCleM3[1]']             = nirDeclarant.substring(13, 15);
}

// Cadre 8 

if ((Value('id').of(gerants[1].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau') or (Value('id').of(gerants[1].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantMaintenu') and Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M'))) and gerants[1].cadreIdentiteDirigeant.conjoint) {
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['modifDate_conjointM3[1]']          = date;
} else if (gerants[1].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(gerants[1].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['modifDate_conjointM3[1]']          = date;
} 
formFieldsPers1['conjointM3_activiteDansEntrepriseOui[1]']    = gerants[1].cadreIdentiteDirigeant.conjointRole ? true : false;
formFieldsPers1['conjointM3_activiteDansEntrepriseNon[1]']    = gerants[1].cadreIdentiteDirigeant.conjointRole ? false : true;
formFieldsPers1['conjoint_collaborateurM3[1]']                = Value('id').of(gerants[1].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') ? true : false;
formFieldsPers1['conjoint_salarieM3[1]']                      = (Value('id').of(gerants[1].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutSalarie') or Value('id').of(gerants[1].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutSalarie')) ? true : false;
formFieldsPers1['conjoint_associeM3[1]']                      = (Value('id').of(gerants[1].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie') or Value('id').of(gerants[1].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutAssocie')) ? true : false;
} 
 
// Cadre 9 

if (Value('id').of(gerants[1].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') 
or Value('id').of(gerants[1].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutSalarie') 
or Value('id').of(gerants[1].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutSalarie')
or gerants[1].cadreIdentiteDirigeant.civilitePersonnePhysique.modifConjoint) {
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['modifDate_modifConjointM3[1]']          = date;
} else if (gerants[1].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(gerants[1].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['modifDate_modifConjointM3[1]']          = date;
} 
if (not Value('id').of(gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.objetModifConjoint).eq('suppressionMentionConjoint')) { 
formFieldsPers1['conjoint_nouveauM3[1]']                      = true;
formFieldsPers1['conjoint_partantM3[1]']                      = false;
formFieldsPers1['conjoint_nomNaissanceM3[1]']                 = gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomNaissanceConjointPacse;
formFieldsPers1['conjoint_nomUsageM3[1]']                     = gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse != null ? gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers1['conjoint_prenomM3[1]']                      = prenoms.toString();
if (gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPDateNaissanceConjointPacse != null) {
    var dateTmp = new Date(parseInt(gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['conjoint_dateNaissanceM3[1]']          = date;
}
formFieldsPers1['conjoint_lieuNaissanceM3[1]']                = gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPLieuNaissanceCommnuneConjointPacse != null ? (gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPLieuNaissanceCommnuneConjointPacse + ' ' + "(" + '' + gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPDeptNaissanceConjointPacse.getId() + '' + ")") : 
															 (gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePersonnePhysiqueLieuNaissanceVilleConjointPacse + ' / ' + gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPPaysNaissanceConjointPacse);
formFieldsPers1['conjoint_nationaliteM3[1]']                  = gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNationaliteConjointPacse;
if (gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseConjointDifferente) {
formFieldsPers1['conjoint_adresseVoieM3[1]']                  = (gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointNumeroVoie != null ? gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointNumeroVoie : '')
															  + ' ' + (gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointIndiceVoie != null ? gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointIndiceVoie : '')
															  + ' ' + (gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointTypeVoie != null ? gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointTypeVoie : '')
															  + ' ' + gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointNomVoie
															  + ' ' + (gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointComplementVoie != null ? gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointComplementVoie : '')
															  + ' ' + (gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null ? gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie : '');
formFieldsPers1['conjoint_adresseCPM3[1]']                    = gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointCodePostal != null ? gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointCodePostal : '';
formFieldsPers1['conjoint_adresseCommuneM3[1]']               = gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointCommune != null ? gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointCommune : (gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointVille + ' / ' + gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointPays);
}}
if (Value('id').of(gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.objetModifConjoint).eq('suppressionMentionConjoint')) { 
formFieldsPers1['conjoint_partantM3[1]']                      = true;
formFieldsPers1['conjoint_partantBis[1]']                     = true;
formFieldsPers1['conjointPartant_nomNaissanceM3[1]']                 = gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomNaissanceConjointPacse;
formFieldsPers1['conjointPartant_nomUsageM3[1]']                     = gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse != null ? gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers1['conjointPartant_prenomM3[1]']                      = prenoms.toString();
var nirDeclarant = gerants[1].cadreIdentiteDirigeant.cadre2InfosConjoint.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers1['conjoint_numSSM3[1]']                = nirDeclarant.substring(0, 13);
    formFieldsPers1['conjoint_numSSCleM3[1]']             = nirDeclarant.substring(13, 15);
}
}
formFieldsPers1['conjoint_rappelGerantM3[1]']                 = '';
} 
}


// Cadre 10

var pouvoir = [];

['dirigeant1Sarl', 'dirigeant2Sarl', 'dirigeant3Sarl', 'dirigeant4Sarl', 'dirigeant5Sarl'].forEach(function(pouvoirs){
	if(pouvoirs=='dirigeant1Sarl' and (Value('id').of($M2[pouvoirs].cadreIdentiteDirigeant.personneLieeQualite).eq('66') or ouverture.ajoutFondePouvoir)){
		pouvoir.push($M2[pouvoirs]);	
	}
	if ( pouvoirs!='dirigeant1Sarl' and null != $M2[pouvoirs] and Value('id').of($M2[pouvoirs].cadreIdentiteDirigeant.personneLieeQualite).eq('66')) {
		pouvoir.push($M2[pouvoirs]);	
	}
});

if (pouvoir.length > 0) {
if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['fondePouvoir_dateModifM3']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['fondePouvoir_dateModifM3']          = date;
} else if (ouverture.ajoutFondePouvoir and ouverture.modifDateAdresseOuverture != null) {
    var dateTmp = new Date(parseInt(ouverture.modifDateAdresseOuverture.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['fondePouvoir_dateModifM3']          = date;
} else if (ouverture.ajoutFondePouvoir and ouverture.modifDateRepriseExploitation != null) {
    var dateTmp = new Date(parseInt(ouverture.modifDateRepriseExploitation.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['fondePouvoir_dateModifM3']          = date;
} else if (pouvoir[0].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(pouvoir[0].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['fondePouvoir_dateModifM3']          = date;
}
formFieldsPers1['fondePouvoir_nouveauM3']                     = Value('id').of(pouvoir[0].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant') ? false : true;
formFieldsPers1['fondePouvoir_partantM3']                     = Value('id').of(pouvoir[0].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers1['fondePouvoir_nomNaissanceM3']                = pouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
formFieldsPers1['fondePouvoir_nomUsageM3']                    = pouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? pouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < pouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(pouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['fondePouvoir_prenomM3']                      = prenoms.toString();
if (pouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(pouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['fondePouvoir_dateNaissanceM3']          = date;
}
formFieldsPers1['fondePouvoir_lieuNaissanceM3']               = pouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(pouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' + '(' + '' + pouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() + '' + ')') : 
																(pouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + pouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel());
formFieldsPers1['fondePouvoir_nationaliteM3']                 = pouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNationaliteGerant;
formFieldsPers1['fondePouvoir_adresseVoieM3']                 = (pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
															  + ' ' + (pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
															  + ' ' + (pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
															  + ' ' + pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNomVoie
															  + ' ' + (pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
															  + ' ' + (pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers1['fondePouvoir_adresseCPM3']         = pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers1['fondePouvoir_adresseCommuneM3']              = pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune : (pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille != null ? (pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays) : '');

if (ouverture.ajoutFondePouvoir and pouvoir[0].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOui) {
formFieldsPers1['fondePouvoir_etablissementAdresseVoieM3']           = (ouverture.cadreAdresseEtablissementOuvert.numeroAdresseEtablissement != null ? ouverture.cadreAdresseEtablissementOuvert.numeroAdresseEtablissement : '')
																+ ' ' + (ouverture.cadreAdresseEtablissementOuvert.indiceAdresseEtablissement != null ? ouverture.cadreAdresseEtablissementOuvert.indiceAdresseEtablissement : '')
																+ ' ' + (ouverture.cadreAdresseEtablissementOuvert.typeAdresseEtablissement != null ? ouverture.cadreAdresseEtablissementOuvert.typeAdresseEtablissement : '')
																+ ' ' + ouverture.cadreAdresseEtablissementOuvert.voieAdresseEtablissement
																+ ' ' + (ouverture.cadreAdresseEtablissementOuvert.complementAdresseEtablissement != null ? ouverture.cadreAdresseEtablissementOuvert.complementAdresseEtablissement : '')
																+ ' ' + (ouverture.cadreAdresseEtablissementOuvert.distributionSpecialeAdresseEtablissement != null ? ouverture.cadreAdresseEtablissementOuvert.distributionSpecialeAdresseEtablissement : '');
formFieldsPers1['fondePouvoir_etablissementAdresseCPM3']             = ouverture.cadreAdresseEtablissementOuvert.codePostalAdresseEtablissement;
formFieldsPers1['fondePouvoir_etablissementAdresseCommuneM3']        = ouverture.cadreAdresseEtablissementOuvert.communeAdresseEtablissement;
} else if (Value('id').of(pouvoir[0].cadreIdentiteDirigeant.personneLieeQualite).eq('66') and pouvoir[0].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
formFieldsPers1['fondePouvoir_etablissementAdresseVoieM3']    = (pouvoir[0].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNumeroVoie != null ? pouvoir[0].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNumeroVoie : '')
																+ ' ' + (pouvoir[0].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseIndiceVoie != null ? pouvoir[0].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseIndiceVoie : '')
																+ ' ' + (pouvoir[0].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseTypeVoie != null ? pouvoir[0].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseTypeVoie : '')
																+ ' ' + pouvoir[0].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNomVoie
																+ ' ' + (pouvoir[0].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseComplementAdresse != null ? pouvoir[0].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseComplementAdresse : '')
																+ ' ' + (pouvoir[0].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseDistriutionSpecialeVoie != null ? pouvoir[0].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseDistriutionSpecialeVoie : '');
formFieldsPers1['fondePouvoir_etablissementAdresseCPM3']      = pouvoir[0].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseCodePostal;
formFieldsPers1['fondePouvoir_etablissementAdresseCommuneM3'] = pouvoir[0].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseCommune;
}
}

// Cadre 11

var socialSARL = [];

['cadreDeclarationSociale1', 'cadreDeclarationSocialeConjointAssocie1', 'cadreDeclarationSociale2', 'cadreDeclarationSocialeConjointAssocie2', 'cadreDeclarationSociale3', 'cadreDeclarationSocialeConjointAssocie3', 'cadreDeclarationSociale4', 'cadreDeclarationSocialeConjointAssocie4', 'cadreDeclarationSociale5', 'cadreDeclarationSocialeConjointAssocie5'].forEach(function(tns){
	if (null != $M2[tns] and $M2[tns].cadre7DeclarationSociale.voletSocialNumeroSecuriteSocialTNS != null) {
		socialSARL.push($M2[tns]);
	}
	
});

if (not Value('id').of(objet.objetModification).contains('situationPM') and not Value('id').of(objet.objetModification).contains('etablissement')) {

formFieldsPers1['observationsM3']                             = correspondance.formaliteObservations != null ? correspondance.formaliteObservations : '';;

// Cadre 12

formFieldsPers1['adresseCorrespondanceCadre_cocheM3']            = (Value('id').of(correspondance.adresseCorrespond).eq('siege') 
																or Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement')
																or Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') 
																or Value('id').of(correspondance.adresseCorrespond).eq('fondsDonneEtablissement')) ? true : false;
formFieldsPers1['adresseCorrespondanceCadre_numeroM3']           = Value('id').of(correspondance.adresseCorrespond).eq('siege') ? "2" : 
																(Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement') ? "12" :
																(Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') ? "13" : 
																(Value('id').of(correspondance.adresseCorrespond).eq('fondsDonneEtablissement') ? "18" : '')));
formFieldsPers1['adressesCorrespondanceAutreM3']                 = Value('id').of(correspondance.adresseCorrespond).eq('autre') ? true : false;
if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
formFieldsPers1['adresseCorrespondance_voie1M3']                 = correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance
																 + ' ' + (correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance : '')
																 + ' ' + (correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance : '')
																 + ' ' + (correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance : '')
																 + ' ' + (correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance : '');
formFieldsPers1['adresseCorrespondance_voie2M3']              	  = (correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance != null ? correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance : '')
																 + ' ' + (correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance != null ? correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance : '');
formFieldsPers1['adresseCorrespondance_codePostalM3']            = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal;
formFieldsPers1['adresseCorrespondance_communeM3']               = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune;
}
formFieldsPers1['telephone2M3']                                                             = correspondance.infosSup.formaliteTelephone1 != null ? correspondance.infosSup.formaliteTelephone1 : '';
formFieldsPers1['telephone1M3']                                                             = correspondance.infosSup.formaliteTelephone2;
formFieldsPers1['courrielM3']                                                               = correspondance.infosSup.formaliteFaxCourriel != null ? correspondance.infosSup.formaliteFaxCourriel : (correspondance.infosSup.telecopie != null ? correspondance.infosSup.telecopie : '');

// Cadre 13

formFieldsPers1['signataireDeclarantM3']                                            = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFieldsPers1['signataireDeclarantCadreM3']                 						= '';
formFieldsPers1['signataireMandataireM3']                                           = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? true : false;
formFieldsPers1['nomPrenomDenominationMandataireM3']					            = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers1['signataireAdresseVoieM3']                                          = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '');
formFieldsPers1['signataireAdresseCommuneM3']                 						= signataire.adresseMandataire.villeAdresseMandataire;
formFieldsPers1['signataireAdresseCPM3']  						                    = signataire.adresseMandataire.dataCodePostalMandataire;

if (signataire.formaliteSignatureDate !== null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['signatureDateM3']          = date;
}
formFieldsPers1['signatureLieuM3']                                                        = signataire.formaliteSignatureLieu;
formFieldsPers1['intercalaireNombreM3_M3']                                                = (pouvoir.length > 4) ? "4" : 
																							((pouvoir.length > 3) ? "3" : 
																							(((pouvoir.length > 2) or (gerants.length > 4)) ? "2" : 
																							(((pouvoir.length > 1) or (gerants.length > 2)) ? "1" : "0")));
formFieldsPers1['intercalaireNombreTNS_M3']                                          = (socialSARL.length > 9) ? "10" : 
																					   ((socialSARL.length > 8) ? "9" : 
																					   ((socialSARL.length > 7) ? "8" : 
																					   ((socialSARL.length > 6) ? "7" : 
																					   ((socialSARL.length > 5) ? "6" : 
																					   ((socialSARL.length > 4) ? "5" : 
																					   ((socialSARL.length > 3) ? "4" : 
																					   ((socialSARL.length > 2) ? "3" : 
																					   ((socialSARL.length > 1) ? "2" : 
																					   ((socialSARL.length > 0) ? "1" : "0")))))))));
formFieldsPers1['intercalaireNombreJQPA_M3']                                         = jqpa3.jqpaActiviteJqpaPP != null ? "3" : (jqpa2.jqpaActiviteJqpaPP != null ? "2" : (jqpa1.jqpaActiviteJqpaPP != null ? "1" : '0'));
formFieldsPers1['signatureM3']                                                         = '';
}

formFields['intercalaireNombreM3']                                               		  = (pouvoir.length > 4) ? "5" : 
																							((pouvoir.length > 3) ? "4" : 
																							(((pouvoir.length > 2) or (gerants.length > 4)) ? "3" : 
																							(((pouvoir.length > 1) or (gerants.length > 2)) ? "2" : 
																							(((pouvoir.length > 0) or (gerants.length > 0) or $M2.modifDissolutionGroup.modifDissolution.cadreIdentiteLiquidateur.autreDirigeant) ? "1" : "0"))));
formFields['intercalaireNombreTNS']                                                  = (socialSARL.length > 9) ? "10" : 
																					   ((socialSARL.length > 8) ? "9" : 
																					   ((socialSARL.length > 7) ? "8" : 
																					   ((socialSARL.length > 6) ? "7" : 
																					   ((socialSARL.length > 5) ? "6" : 
																					   ((socialSARL.length > 4) ? "5" : 
																					   ((socialSARL.length > 3) ? "4" : 
																					   ((socialSARL.length > 2) ? "3" : 
																					   ((socialSARL.length > 1) ? "2" : 
																					   ((socialSARL.length > 0) ? "1" : "0")))))))));
																						

// Imprimé M3 SARL 2

if ((pouvoir.length > 1) or (gerants.length > 2)) {

// Cadre 1

formFieldsPers2['demandeModificationM3']                      = false;
formFieldsPers2['demandeModificationRM_M3']                   = identite.immatriculationRM ? true : false;
formFieldsPers2['demandeIntercalaireM3']                      = true;
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
formFieldsPers2['intercalaireNumM3']                          = "2";
} else {
formFieldsPers2['intercalaireNumM3']                          = "1";
} 
 
// Cadre 2

formFieldsPers2['entreprise_sirenM3']                                     = identite.siren.split(' ').join('');
formFieldsPers2['immatRCS_M3']                                            = true;
formFieldsPers2['immatRCSGreffe_M3']                                      = identite.immatRCSGreffe;
formFieldsPers2['immatRM_M3']                                             = identite.immatriculationRM ? true : false;
formFieldsPers2['immatRMDept_M3']                                         = identite.immatriculationRM ? identite.immatRMCMA : '';
formFieldsPers2['immatRMDeptNum_M3']                                      = identite.immatriculationRM ? identite.immatRMCMA.getId() : '';
formFieldsPers2['entreprise_denominationM3']                              = identite.entrepriseDenomination;
formFieldsPers2['entreprise_formeJuridiqueM3']                            = identite.entrepriseFormeJuridique.getLabel();

// Cadre 3

if (dirigeantSARL1.cadreGerance.typeGeranceModifiee or dirigeantSARL2.cadreGerance.typeGeranceModifiee
	or dirigeantSARL3.cadreGerance.typeGeranceModifiee or dirigeantSARL4.cadreGerance.typeGeranceModifiee
	or dirigeantSARL5.cadreGerance.typeGeranceModifiee) { 
formFieldsPers2['modifNatureGeranceM3_oui']                   = true;
formFieldsPers2['modifNatureGeranceM3_non']                   = false;
formFieldsPers2['natureGeranceM3_NonMaj']                     = (Value('id').of(dirigeantSARL1.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')
																or Value('id').of(dirigeantSARL2.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')
																or Value('id').of(dirigeantSARL3.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')
																or Value('id').of(dirigeantSARL4.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')
																or Value('id').of(dirigeantSARL5.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')) ? true : false;
formFieldsPers2['natureGeranceM3_Maj']                        = (Value('id').of(dirigeantSARL1.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire')
																or Value('id').of(dirigeantSARL2.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire')
																or Value('id').of(dirigeantSARL3.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire')
																or Value('id').of(dirigeantSARL4.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire')
																or Value('id').of(dirigeantSARL5.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire')) ? true : false;
formFieldsPers2['natureGeranceM3_societeAssociee']            = ((Value('id').of(dirigeantSARL1.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')
																or Value('id').of(dirigeantSARL2.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')
																or Value('id').of(dirigeantSARL3.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')
																or Value('id').of(dirigeantSARL4.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')
																or Value('id').of(dirigeantSARL5.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire'))
																and (dirigeantSARL1.cadreGerance.entrepriseNatureGeranceSocieteAssocieeOui or dirigeantSARL2.cadreGerance.entrepriseNatureGeranceSocieteAssocieeOui
																or dirigeantSARL3.cadreGerance.entrepriseNatureGeranceSocieteAssocieeOui or dirigeantSARL4.cadreGerance.entrepriseNatureGeranceSocieteAssocieeOui
																or dirigeantSARL5.cadreGerance.entrepriseNatureGeranceSocieteAssocieeOui)) ? true : false;
}

// Dirigeant 3

// Cadre 4 

if (gerants.length > 2) {

if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['modifDate_dirigeantM3[0]']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null and modifFormeJuridique.formeJuridiqueModifDirigeant) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['modifDate_dirigeantM3[0]']          = date;
} else if (gerants[2].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(gerants[2].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['modifDate_dirigeantM3[0]']          = date;
}
formFieldsPers2['dirigeant_nouveauM3[0]']                     = Value('id').of(gerants[2].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau') ? true : false;
formFieldsPers2['dirigeant_partantM3[0]']                     = Value('id').of(gerants[2].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers2['dirigeant_modifM3[0]']                       = (Value('id').of(gerants[2].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantModif') or Value('id').of(gerants[2].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantMaintenu')) ? true : false;
if (not Value('id').of(gerants[2].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers2['dirigeant_nomNaissanceM3[0]']                = gerants[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
formFieldsPers2['dirigeant_nomUsageM3[0]']                    = gerants[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? gerants[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < gerants[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(gerants[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers2['dirigeant_prenomM3[0]']                      = prenoms.toString();
if (gerants[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(gerants[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['dirigeant_dateNaissanceM3[0]']          = date;
}
formFieldsPers2['dirigeant_lieuNaissanceM3[0]']               = gerants[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(gerants[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' + '(' + '' + gerants[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() + '' + ')') : 
																(gerants[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + gerants[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant);
formFieldsPers2['dirigeant_nationaliteM3[0]']                 = gerants[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNationaliteGerant;
formFieldsPers2['dirigeant_adresseVoieM3[0]']                 = (gerants[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? gerants[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
															  + ' ' + (gerants[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? gerants[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
															  + ' ' + (gerants[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? gerants[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
															  + ' ' + gerants[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNomVoie
															  + ' ' + (gerants[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? gerants[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
															  + ' ' + (gerants[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? gerants[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers2['dirigeant_adresseCPM3[0]']                   = gerants[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? gerants[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers2['dirigeant_adresseCommuneM3[0]']              = gerants[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? gerants[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune : (gerants[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille != null ? (gerants[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + gerants[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays) : '');
}
if (Value('id').of(gerants[2].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers2['dirigeant_partantBisM3[0]']                  = true;
formFieldsPers2['dirigeantPartant_nomNaissanceM3[0]']         = gerants[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
formFieldsPers2['dirigeantPartant_nomUsageM3[0]']             = gerants[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? gerants[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < gerants[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(gerants[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers2['dirigeantPartant_prenomM3[0]']                           = prenoms.toString();
}
var nirDeclarant = gerants[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers2['dirigeantPartant_numSSM3[0]']                = nirDeclarant.substring(0, 13);
    formFieldsPers2['dirigeantPartant_numSSCleM3[0]']             = nirDeclarant.substring(13, 15);
}

// Cadre 5 

if ((Value('id').of(gerants[2].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau') or (Value('id').of(gerants[2].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantMaintenu') and Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M'))) and gerants[2].cadreIdentiteDirigeant.conjoint) {
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['modifDate_conjointM3[0]']          = date;
} else if (gerants[2].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(gerants[2].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['modifDate_conjointM3[0]']          = date;
} 
formFieldsPers2['conjointM3_activiteDansEntrepriseOui[0]']    = gerants[2].cadreIdentiteDirigeant.conjointRole ? true : false;
formFieldsPers2['conjointM3_activiteDansEntrepriseNon[0]']    = gerants[2].cadreIdentiteDirigeant.conjointRole ? false : true;
formFieldsPers2['conjoint_collaborateurM3[0]']                = Value('id').of(gerants[2].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') ? true : false;
formFieldsPers2['conjoint_salarieM3[0]']                      = (Value('id').of(gerants[2].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutSalarie') or Value('id').of(gerants[2].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutSalarie')) ? true : false;
formFieldsPers2['conjoint_associeM3[0]']                      = (Value('id').of(gerants[2].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie') or Value('id').of(gerants[2].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutAssocie')) ? true : false;
} 
 
// Cadre 6 

if (Value('id').of(gerants[2].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') 
or Value('id').of(gerants[2].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutSalarie') 
or Value('id').of(gerants[2].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutSalarie')
or gerants[2].cadreIdentiteDirigeant.civilitePersonnePhysique.modifConjoint) {
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['modifDate_modifConjointM3[0]']          = date;
} else if (gerants[2].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(gerants[2].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['modifDate_modifConjointM3[0]']          = date;
} 
if (not Value('id').of(gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.objetModifConjoint).eq('suppressionMentionConjoint')) { 
formFieldsPers2['conjoint_nouveauM3[0]']                      = true;
formFieldsPers2['conjoint_partantM3[0]']                      = false;
formFieldsPers2['conjoint_nomNaissanceM3[0]']                 = gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomNaissanceConjointPacse;
formFieldsPers2['conjoint_nomUsageM3[0]']                     = gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse != null ? gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers2['conjoint_prenomM3[0]']                      = prenoms.toString();
if (gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPDateNaissanceConjointPacse != null) {
    var dateTmp = new Date(parseInt(gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['conjoint_dateNaissanceM3[0]']          = date;
}
formFieldsPers2['conjoint_lieuNaissanceM3[0]']                = gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPLieuNaissanceCommnuneConjointPacse != null ? (gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPLieuNaissanceCommnuneConjointPacse + ' ' + "(" + '' + gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPDeptNaissanceConjointPacse.getId() + '' + ")") : 
															 (gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePersonnePhysiqueLieuNaissanceVilleConjointPacse + ' / ' + gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPPaysNaissanceConjointPacse);
formFieldsPers2['conjoint_nationaliteM3[0]']                  = gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNationaliteConjointPacse;
if (gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseConjointDifferente) {
formFieldsPers2['conjoint_adresseVoieM3[0]']                  = (gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointNumeroVoie != null ? gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointNumeroVoie : '')
															  + ' ' + (gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointIndiceVoie != null ? gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointIndiceVoie : '')
															  + ' ' + (gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointTypeVoie != null ? gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointTypeVoie : '')
															  + ' ' + gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointNomVoie
															  + ' ' + (gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointComplementVoie != null ? gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointComplementVoie : '')
															  + ' ' + (gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null ? gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie : '');
formFieldsPers2['conjoint_adresseCPM3[0]']                    = gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointCodePostal != null ? gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointCodePostal : '';
formFieldsPers2['conjoint_adresseCommuneM3[0]']               = gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointCommune != null ? gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointCommune : (gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointVille + ' / ' + gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointPays);
}}
if (Value('id').of(gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.objetModifConjoint).eq('suppressionMentionConjoint')) { 
formFieldsPers2['conjoint_partantM3[0]']                      = true;
formFieldsPers2['conjoint_partantBis[0]']                     = true;
formFieldsPers2['conjointPartant_nomNaissanceM3[0]']                 = gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomNaissanceConjointPacse;
formFieldsPers2['conjointPartant_nomUsageM3[0]']                     = gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse != null ? gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers2['conjointPartant_prenomM3[0]']                      = prenoms.toString();
var nirDeclarant = gerants[2].cadreIdentiteDirigeant.cadre2InfosConjoint.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers2['conjoint_numSSM3[0]']                = nirDeclarant.substring(0, 13);
    formFieldsPers2['conjoint_numSSCleM3[0]']             = nirDeclarant.substring(13, 15);
}
}
formFieldsPers2['conjoint_rappelGerantM3[0]']                 = '';
} 
}


// Dirigeant 4

// Cadre 7

if (gerants.length > 3) {

if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['modifDate_dirigeantM3[1]']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null and modifFormeJuridique.formeJuridiqueModifDirigeant) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['modifDate_dirigeantM3[1]']          = date;
} else if (gerants[3].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(gerants[3].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['modifDate_dirigeantM3[1]']          = date;
}
formFieldsPers2['dirigeant_nouveauM3[1]']                     = Value('id').of(gerants[3].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau') ? true : false;
formFieldsPers2['dirigeant_partantM3[1]']                     = Value('id').of(gerants[3].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers2['dirigeant_modifM3[1]']                       = (Value('id').of(gerants[3].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantModif') or Value('id').of(gerants[3].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantMaintenu')) ? true : false;
if (not Value('id').of(gerants[3].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers2['dirigeant_nomNaissanceM3[1]']                = gerants[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
formFieldsPers2['dirigeant_nomUsageM3[1]']                    = gerants[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? gerants[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < gerants[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(gerants[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers2['dirigeant_prenomM3[1]']                      = prenoms.toString();
if (gerants[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(gerants[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['dirigeant_dateNaissanceM3[1]']          = date;
}
formFieldsPers2['dirigeant_lieuNaissanceM3[1]']               = gerants[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(gerants[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' + '(' + '' + gerants[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() + '' + ')') : 
																(gerants[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + gerants[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant);
formFieldsPers2['dirigeant_nationaliteM3[1]']                 = gerants[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNationaliteGerant;
formFieldsPers2['dirigeant_adresseVoieM3[1]']                 = (gerants[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? gerants[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
															  + ' ' + (gerants[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? gerants[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
															  + ' ' + (gerants[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? gerants[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
															  + ' ' + gerants[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNomVoie
															  + ' ' + (gerants[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? gerants[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
															  + ' ' + (gerants[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? gerants[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers2['dirigeant_adresseCPM3[1]']         = gerants[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? gerants[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers2['dirigeant_adresseCommuneM3[1]']              = gerants[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? gerants[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune : (gerants[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille != null ? (gerants[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + gerants[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays) : '');
}
if (Value('id').of(gerants[3].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers2['dirigeant_partantBisM3[1]']                  = true;
formFieldsPers2['dirigeantPartant_nomNaissanceM3[1]']         = gerants[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
formFieldsPers2['dirigeantPartant_nomUsageM3[1]']             = gerants[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? gerants[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < gerants[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(gerants[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers2['dirigeantPartant_prenomM3[1]']                           = prenoms.toString();
}
var nirDeclarant = gerants[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers2['dirigeantPartant_numSSM3[1]']                = nirDeclarant.substring(0, 13);
    formFieldsPers2['dirigeantPartant_numSSCleM3[1]']             = nirDeclarant.substring(13, 15);
}

// Cadre 8 

if ((Value('id').of(gerants[3].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau') or (Value('id').of(gerants[3].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantMaintenu') and Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M'))) and gerants[3].cadreIdentiteDirigeant.conjoint) {
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['modifDate_conjointM3[1]']          = date;
} else if (gerants[3].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(gerants[3].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['modifDate_conjointM3[1]']          = date;
} 
formFieldsPers2['conjointM3_activiteDansEntrepriseOui[1]']    = gerants[3].cadreIdentiteDirigeant.conjointRole ? true : false;
formFieldsPers2['conjointM3_activiteDansEntrepriseNon[1]']    = gerants[3].cadreIdentiteDirigeant.conjointRole ? false : true;
formFieldsPers2['conjoint_collaborateurM3[1]']                = Value('id').of(gerants[3].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') ? true : false;
formFieldsPers2['conjoint_salarieM3[1]']                      = (Value('id').of(gerants[3].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutSalarie') or Value('id').of(gerants[3].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutSalarie')) ? true : false;
formFieldsPers2['conjoint_associeM3[1]']                      = (Value('id').of(gerants[3].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie') or Value('id').of(gerants[3].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutAssocie')) ? true : false;
} 
 
// Cadre 9 

if (Value('id').of(gerants[3].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') 
or Value('id').of(gerants[3].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutSalarie') 
or Value('id').of(gerants[3].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutSalarie')
or gerants[3].cadreIdentiteDirigeant.civilitePersonnePhysique.modifConjoint) {
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['modifDate_modifConjointM3[1]']          = date;
} else if (gerants[3].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(gerants[3].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['modifDate_modifConjointM3[1]']          = date;
} 
if (not Value('id').of(gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.objetModifConjoint).eq('suppressionMentionConjoint')) { 
formFieldsPers2['conjoint_nouveauM3[1]']                      = true;
formFieldsPers2['conjoint_partantM3[1]']                      = false;
formFieldsPers2['conjoint_nomNaissanceM3[1]']                 = gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomNaissanceConjointPacse;
formFieldsPers2['conjoint_nomUsageM3[1]']                     = gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse != null ? gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers2['conjoint_prenomM3[1]']                      = prenoms.toString();
if (gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPDateNaissanceConjointPacse != null) {
    var dateTmp = new Date(parseInt(gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['conjoint_dateNaissanceM3[1]']          = date;
}
formFieldsPers2['conjoint_lieuNaissanceM3[1]']                = gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPLieuNaissanceCommnuneConjointPacse != null ? (gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPLieuNaissanceCommnuneConjointPacse + ' ' + "(" + '' + gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPDeptNaissanceConjointPacse.getId() + '' + ")") : 
															 (gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePersonnePhysiqueLieuNaissanceVilleConjointPacse + ' / ' + gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPPaysNaissanceConjointPacse);
formFieldsPers2['conjoint_nationaliteM3[1]']                  = gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNationaliteConjointPacse;
if (gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseConjointDifferente) {
formFieldsPers2['conjoint_adresseVoieM3[1]']                  = (gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointNumeroVoie != null ? gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointNumeroVoie : '')
															  + ' ' + (gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointIndiceVoie != null ? gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointIndiceVoie : '')
															  + ' ' + (gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointTypeVoie != null ? gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointTypeVoie : '')
															  + ' ' + gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointNomVoie
															  + ' ' + (gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointComplementVoie != null ? gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointComplementVoie : '')
															  + ' ' + (gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null ? gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie : '');
formFieldsPers2['conjoint_adresseCPM3[1]']                    = gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointCodePostal != null ? gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointCodePostal : '';
formFieldsPers2['conjoint_adresseCommuneM3[1]']               = gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointCommune != null ? gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointCommune : (gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointVille + ' / ' + gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointPays);
}}
if (Value('id').of(gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.objetModifConjoint).eq('suppressionMentionConjoint')) { 
formFieldsPers2['conjoint_partantM3[1]']                      = true;
formFieldsPers2['conjoint_partantBis[1]']                     = true;
formFieldsPers2['conjointPartant_nomNaissanceM3[1]']                 = gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomNaissanceConjointPacse;
formFieldsPers2['conjointPartant_nomUsageM3[1]']                     = gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse != null ? gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers2['conjointPartant_prenomM3[1]']                      = prenoms.toString();
var nirDeclarant = gerants[3].cadreIdentiteDirigeant.cadre2InfosConjoint.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers2['conjoint_numSSM3[1]']                = nirDeclarant.substring(0, 13);
    formFieldsPers2['conjoint_numSSCleM3[1]']             = nirDeclarant.substring(13, 15);
}
}
formFieldsPers2['conjoint_rappelGerantM3[1]']                 = '';
} 
}


// Cadre 10

if (pouvoir.length > 1) {
if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['fondePouvoir_dateModifM3']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['fondePouvoir_dateModifM3']          = date;
} else if (pouvoir[1].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(pouvoir[1].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['fondePouvoir_dateModifM3']          = date;
}
formFieldsPers2['fondePouvoir_nouveauM3']                     = Value('id').of(pouvoir[1].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant') ? false : true;
formFieldsPers2['fondePouvoir_partantM3']                     = Value('id').of(pouvoir[1].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers2['fondePouvoir_nomNaissanceM3']                = pouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
formFieldsPers2['fondePouvoir_nomUsageM3']                    = pouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? pouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < pouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(pouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers2['fondePouvoir_prenomM3']                      = prenoms.toString();
if (pouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(pouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['fondePouvoir_dateNaissanceM3']          = date;
}
formFieldsPers2['fondePouvoir_lieuNaissanceM3']               = pouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(pouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' + '(' + '' + pouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() + '' + ')') : 
																(pouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + pouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel());
formFieldsPers2['fondePouvoir_nationaliteM3']                 = pouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNationaliteGerant;
formFieldsPers2['fondePouvoir_adresseVoieM3']                 = (pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
															  + ' ' + (pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
															  + ' ' + (pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
															  + ' ' + pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNomVoie
															  + ' ' + (pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
															  + ' ' + (pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers2['fondePouvoir_adresseCPM3']         = pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers2['fondePouvoir_adresseCommuneM3']              = pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune : (pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille != null ? (pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays) : '');

if (Value('id').of(pouvoir[1].cadreIdentiteDirigeant.personneLieeQualite).eq('66') and pouvoir[1].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
formFieldsPers2['fondePouvoir_etablissementAdresseVoieM3']    = (pouvoir[1].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNumeroVoie != null ? pouvoir[1].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNumeroVoie : '')
																+ ' ' + (pouvoir[1].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseIndiceVoie != null ? pouvoir[1].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseIndiceVoie : '')
																+ ' ' + (pouvoir[1].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseTypeVoie != null ? pouvoir[1].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseTypeVoie : '')
																+ ' ' + pouvoir[1].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNomVoie
																+ ' ' + (pouvoir[1].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseComplementAdresse != null ? pouvoir[1].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseComplementAdresse : '')
																+ ' ' + (pouvoir[1].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseDistriutionSpecialeVoie != null ? pouvoir[1].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseDistriutionSpecialeVoie : '');
formFieldsPers2['fondePouvoir_etablissementAdresseCPM3']      = pouvoir[1].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseCodePostal;
formFieldsPers2['fondePouvoir_etablissementAdresseCommuneM3'] = pouvoir[1].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseCommune;
}
}
} 
 
// Imprimé M3 SARL 3

if ((pouvoir.length > 2) or (gerants.length > 4)) {

// Cadre 1

formFieldsPers3['demandeModificationM3']                      = false;
formFieldsPers3['demandeModificationRM_M3']                   = identite.immatriculationRM ? true : false;
formFieldsPers3['demandeIntercalaireM3']                      = true;
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
formFieldsPers3['intercalaireNumM3']                          = "3";
} else {
formFieldsPers3['intercalaireNumM3']                          = "2";
} 
 
// Cadre 2

formFieldsPers3['entreprise_sirenM3']                                     = identite.siren.split(' ').join('');
formFieldsPers3['immatRCS_M3']                                            = true;
formFieldsPers3['immatRCSGreffe_M3']                                      = identite.immatRCSGreffe;
formFieldsPers3['immatRM_M3']                                             = identite.immatriculationRM ? true : false;
formFieldsPers3['immatRMDept_M3']                                         = identite.immatriculationRM ? identite.immatRMCMA : '';
formFieldsPers3['immatRMDeptNum_M3']                                      = identite.immatriculationRM ? identite.immatRMCMA.getId() : '';
formFieldsPers3['entreprise_denominationM3']                              = identite.entrepriseDenomination;
formFieldsPers3['entreprise_formeJuridiqueM3']                            = identite.entrepriseFormeJuridique.getLabel();

// Cadre 3

if (dirigeantSARL1.cadreGerance.typeGeranceModifiee or dirigeantSARL2.cadreGerance.typeGeranceModifiee
	or dirigeantSARL3.cadreGerance.typeGeranceModifiee or dirigeantSARL4.cadreGerance.typeGeranceModifiee
	or dirigeantSARL5.cadreGerance.typeGeranceModifiee) { 
formFieldsPers3['modifNatureGeranceM3_oui']                   = true;
formFieldsPers3['modifNatureGeranceM3_non']                   = false;
formFieldsPers3['natureGeranceM3_NonMaj']                     = (Value('id').of(dirigeantSARL1.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')
																or Value('id').of(dirigeantSARL2.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')
																or Value('id').of(dirigeantSARL3.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')
																or Value('id').of(dirigeantSARL4.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')
																or Value('id').of(dirigeantSARL5.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')) ? true : false;
formFieldsPers3['natureGeranceM3_Maj']                        = (Value('id').of(dirigeantSARL1.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire')
																or Value('id').of(dirigeantSARL2.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire')
																or Value('id').of(dirigeantSARL3.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire')
																or Value('id').of(dirigeantSARL4.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire')
																or Value('id').of(dirigeantSARL5.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire')) ? true : false;
formFieldsPers3['natureGeranceM3_societeAssociee']            = ((Value('id').of(dirigeantSARL1.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')
																or Value('id').of(dirigeantSARL2.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')
																or Value('id').of(dirigeantSARL3.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')
																or Value('id').of(dirigeantSARL4.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')
																or Value('id').of(dirigeantSARL5.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire'))
																and (dirigeantSARL1.cadreGerance.entrepriseNatureGeranceSocieteAssocieeOui or dirigeantSARL2.cadreGerance.entrepriseNatureGeranceSocieteAssocieeOui
																or dirigeantSARL3.cadreGerance.entrepriseNatureGeranceSocieteAssocieeOui or dirigeantSARL4.cadreGerance.entrepriseNatureGeranceSocieteAssocieeOui
																or dirigeantSARL5.cadreGerance.entrepriseNatureGeranceSocieteAssocieeOui)) ? true : false;
}

// Dirigeant 5

// Cadre 4 

if (gerants.length > 4) {

if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers3['modifDate_dirigeantM3[0]']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null and modifFormeJuridique.formeJuridiqueModifDirigeant) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers3['modifDate_dirigeantM3[0]']          = date;
} else if (gerants[4].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(gerants[4].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers3['modifDate_dirigeantM3[0]']          = date;
}
formFieldsPers3['dirigeant_nouveauM3[0]']                     = Value('id').of(gerants[4].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau') ? true : false;
formFieldsPers3['dirigeant_partantM3[0]']                     = Value('id').of(gerants[4].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers3['dirigeant_modifM3[0]']                       = (Value('id').of(gerants[4].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantModif') or Value('id').of(gerants[4].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantMaintenu')) ? true : false;
if (not Value('id').of(gerants[4].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers3['dirigeant_nomNaissanceM3[0]']                = gerants[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
formFieldsPers3['dirigeant_nomUsageM3[0]']                    = gerants[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? gerants[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < gerants[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(gerants[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers3['dirigeant_prenomM3[0]']                      = prenoms.toString();
if (gerants[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(gerants[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers3['dirigeant_dateNaissanceM3[0]']          = date;
}
formFieldsPers3['dirigeant_lieuNaissanceM3[0]']               = gerants[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(gerants[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' + '(' + '' + gerants[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() + '' + ')') : 
																(gerants[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + gerants[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant);
formFieldsPers3['dirigeant_nationaliteM3[0]']                 = gerants[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNationaliteGerant;
formFieldsPers3['dirigeant_adresseVoieM3[0]']                 = (gerants[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? gerants[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
															  + ' ' + (gerants[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? gerants[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
															  + ' ' + (gerants[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? gerants[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
															  + ' ' + gerants[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNomVoie
															  + ' ' + (gerants[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? gerants[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
															  + ' ' + (gerants[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? gerants[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers3['dirigeant_adresseCPM3[0]']                   = gerants[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? gerants[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers3['dirigeant_adresseCommuneM3[0]']              = gerants[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? gerants[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune : (gerants[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille != null ? (gerants[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + gerants[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays) : '');
}
if (Value('id').of(gerants[4].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers3['dirigeant_partantBisM3[0]']                  = true;
formFieldsPers3['dirigeantPartant_nomNaissanceM3[0]']         = gerants[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
formFieldsPers3['dirigeantPartant_nomUsageM3[0]']             = gerants[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? gerants[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < gerants[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(gerants[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers3['dirigeantPartant_prenomM3[0]']                           = prenoms.toString();
}
var nirDeclarant = gerants[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers3['dirigeantPartant_numSSM3[0]']                = nirDeclarant.substring(0, 13);
    formFieldsPers3['dirigeantPartant_numSSCleM3[0]']             = nirDeclarant.substring(13, 15);
}

// Cadre 5 

if ((Value('id').of(gerants[4].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau') or (Value('id').of(gerants[4].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantMaintenu') and Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M'))) and gerants[4].cadreIdentiteDirigeant.conjoint) {
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers3['modifDate_conjointM3[0]']          = date;
} else if (gerants[4].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(gerants[4].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers3['modifDate_conjointM3[0]']          = date;
} 
formFieldsPers3['conjointM3_activiteDansEntrepriseOui[0]']    = gerants[4].cadreIdentiteDirigeant.conjointRole ? true : false;
formFieldsPers3['conjointM3_activiteDansEntrepriseNon[0]']    = gerants[4].cadreIdentiteDirigeant.conjointRole ? false : true;
formFieldsPers3['conjoint_collaborateurM3[0]']                = Value('id').of(gerants[4].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') ? true : false;
formFieldsPers3['conjoint_salarieM3[0]']                      = (Value('id').of(gerants[4].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutSalarie') or Value('id').of(gerants[4].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutSalarie')) ? true : false;
formFieldsPers3['conjoint_associeM3[0]']                      = (Value('id').of(gerants[4].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie') or Value('id').of(gerants[4].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutAssocie')) ? true : false;
} 
 
// Cadre 6 

if (Value('id').of(gerants[4].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') 
or Value('id').of(gerants[4].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutSalarie') 
or Value('id').of(gerants[4].cadreIdentiteDirigeant.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutSalarie')
or gerants[4].cadreIdentiteDirigeant.civilitePersonnePhysique.modifConjoint) {
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers3['modifDate_modifConjointM3[0]']          = date;
} else if (gerants[4].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(gerants[4].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers3['modifDate_modifConjointM3[0]']          = date;
} 
if (not Value('id').of(gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.objetModifConjoint).eq('suppressionMentionConjoint')) { 
formFieldsPers3['conjoint_nouveauM3[0]']                      = true;
formFieldsPers3['conjoint_partantM3[0]']                      = false;
formFieldsPers3['conjoint_nomNaissanceM3[0]']                 = gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomNaissanceConjointPacse;
formFieldsPers3['conjoint_nomUsageM3[0]']                     = gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse != null ? gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers3['conjoint_prenomM3[0]']                      = prenoms.toString();
if (gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPDateNaissanceConjointPacse != null) {
    var dateTmp = new Date(parseInt(gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers3['conjoint_dateNaissanceM3[0]']          = date;
}
formFieldsPers3['conjoint_lieuNaissanceM3[0]']                = gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPLieuNaissanceCommnuneConjointPacse != null ? (gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPLieuNaissanceCommnuneConjointPacse + ' ' + "(" + '' + gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPDeptNaissanceConjointPacse.getId() + '' + ")") : 
															 (gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePersonnePhysiqueLieuNaissanceVilleConjointPacse + ' / ' + gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPPaysNaissanceConjointPacse);
formFieldsPers3['conjoint_nationaliteM3[0]']                  = gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNationaliteConjointPacse;
if (gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseConjointDifferente) {
formFieldsPers3['conjoint_adresseVoieM3[0]']                  = (gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointNumeroVoie != null ? gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointNumeroVoie : '')
															  + ' ' + (gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointIndiceVoie != null ? gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointIndiceVoie : '')
															  + ' ' + (gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointTypeVoie != null ? gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointTypeVoie : '')
															  + ' ' + gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointNomVoie
															  + ' ' + (gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointComplementVoie != null ? gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointComplementVoie : '')
															  + ' ' + (gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null ? gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie : '');
formFieldsPers3['conjoint_adresseCPM3[0]']                    = gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointCodePostal != null ? gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointCodePostal : '';
formFieldsPers3['conjoint_adresseCommuneM3[0]']               = gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointCommune != null ? gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointCommune : (gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointVille + ' / ' + gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.adresseDomicileConjoint.adresseConjointPays);
}}
if (Value('id').of(gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.objetModifConjoint).eq('suppressionMentionConjoint')) { 
formFieldsPers3['conjoint_partantM3[0]']                      = true;
formFieldsPers3['conjoint_partantBis[0]']                     = true;
formFieldsPers3['conjointPartant_nomNaissanceM3[0]']                 = gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomNaissanceConjointPacse;
formFieldsPers3['conjointPartant_nomUsageM3[0]']                     = gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse != null ? gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers3['conjointPartant_prenomM3[0]']                      = prenoms.toString();
var nirDeclarant = gerants[4].cadreIdentiteDirigeant.cadre2InfosConjoint.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers3['conjoint_numSSM3[0]']                = nirDeclarant.substring(0, 13);
    formFieldsPers3['conjoint_numSSCleM3[0]']             = nirDeclarant.substring(13, 15);
}
}
formFieldsPers3['conjoint_rappelGerantM3[0]']                 = '';
} 
}


// Cadre 10

if (pouvoir.length > 2) {
if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers3['fondePouvoir_dateModifM3']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers3['fondePouvoir_dateModifM3']          = date;
} else if (pouvoir[2].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(pouvoir[2].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers3['fondePouvoir_dateModifM3']          = date;
}
formFieldsPers3['fondePouvoir_nouveauM3']                     = Value('id').of(pouvoir[2].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant') ? false : true;
formFieldsPers3['fondePouvoir_partantM3']                     = Value('id').of(pouvoir[2].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers3['fondePouvoir_nomNaissanceM3']                = pouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
formFieldsPers3['fondePouvoir_nomUsageM3']                    = pouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? pouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < pouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(pouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers3['fondePouvoir_prenomM3']                      = prenoms.toString();
if (pouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(pouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers3['fondePouvoir_dateNaissanceM3']          = date;
}
formFieldsPers3['fondePouvoir_lieuNaissanceM3']               = pouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(pouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' + '(' + '' + pouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() + '' + ')') : 
																(pouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + pouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel());
formFieldsPers3['fondePouvoir_nationaliteM3']                 = pouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNationaliteGerant;
formFieldsPers3['fondePouvoir_adresseVoieM3']                 = (pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
															  + ' ' + (pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
															  + ' ' + (pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
															  + ' ' + pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNomVoie
															  + ' ' + (pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
															  + ' ' + (pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers3['fondePouvoir_adresseCPM3']         = pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers3['fondePouvoir_adresseCommuneM3']              = pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune : (pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille != null ? (pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays) : '');

if (Value('id').of(pouvoir[2].cadreIdentiteDirigeant.personneLieeQualite).eq('66') and pouvoir[2].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
formFieldsPers3['fondePouvoir_etablissementAdresseVoieM3']    = (pouvoir[2].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNumeroVoie != null ? pouvoir[2].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNumeroVoie : '')
																+ ' ' + (pouvoir[2].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseIndiceVoie != null ? pouvoir[2].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseIndiceVoie : '')
																+ ' ' + (pouvoir[2].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseTypeVoie != null ? pouvoir[2].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseTypeVoie : '')
																+ ' ' + pouvoir[2].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNomVoie
																+ ' ' + (pouvoir[2].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseComplementAdresse != null ? pouvoir[2].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseComplementAdresse : '')
																+ ' ' + (pouvoir[2].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseDistriutionSpecialeVoie != null ? pouvoir[2].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseDistriutionSpecialeVoie : '');
formFieldsPers3['fondePouvoir_etablissementAdresseCPM3']      = pouvoir[2].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseCodePostal;
formFieldsPers3['fondePouvoir_etablissementAdresseCommuneM3'] = pouvoir[2].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseCommune;
}
}
} 
 
// Imprimé M3 SARL 4

if (pouvoir.length > 3) {

// Cadre 1

formFieldsPers4['demandeModificationM3']                      = false;
formFieldsPers4['demandeModificationRM_M3']                   = identite.immatriculationRM ? true : false;
formFieldsPers4['demandeIntercalaireM3']                      = true;
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
formFieldsPers4['intercalaireNumM3']                          = "4";
} else {
formFieldsPers4['intercalaireNumM3']                          = "3";
} 
 
// Cadre 2

formFieldsPers4['entreprise_sirenM3']                                     = identite.siren.split(' ').join('');
formFieldsPers4['immatRCS_M3']                                            = true;
formFieldsPers4['immatRCSGreffe_M3']                                      = identite.immatRCSGreffe;
formFieldsPers4['immatRM_M3']                                             = identite.immatriculationRM ? true : false;
formFieldsPers4['immatRMDept_M3']                                         = identite.immatriculationRM ? identite.immatRMCMA : '';
formFieldsPers4['immatRMDeptNum_M3']                                      = identite.immatriculationRM ? identite.immatRMCMA.getId() : '';
formFieldsPers4['entreprise_denominationM3']                              = identite.entrepriseDenomination;
formFieldsPers4['entreprise_formeJuridiqueM3']                            = identite.entrepriseFormeJuridique.getLabel();

// Cadre 10

if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers4['fondePouvoir_dateModifM3']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers4['fondePouvoir_dateModifM3']          = date;
} else if (pouvoir[3].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(pouvoir[3].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers4['fondePouvoir_dateModifM3']          = date;
}
formFieldsPers4['fondePouvoir_nouveauM3']                     = Value('id').of(pouvoir[3].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant') ? false : true;
formFieldsPers4['fondePouvoir_partantM3']                     = Value('id').of(pouvoir[3].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers4['fondePouvoir_nomNaissanceM3']                = pouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
formFieldsPers4['fondePouvoir_nomUsageM3']                    = pouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? pouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < pouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(pouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers4['fondePouvoir_prenomM3']                      = prenoms.toString();
if (pouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(pouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers4['fondePouvoir_dateNaissanceM3']          = date;
}
formFieldsPers4['fondePouvoir_lieuNaissanceM3']               = pouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(pouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' + '(' + '' + pouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() + '' + ')') : 
																(pouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + pouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel());
formFieldsPers4['fondePouvoir_nationaliteM3']                 = pouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNationaliteGerant;
formFieldsPers4['fondePouvoir_adresseVoieM3']                 = (pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
															  + ' ' + (pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
															  + ' ' + (pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
															  + ' ' + pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNomVoie
															  + ' ' + (pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
															  + ' ' + (pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers4['fondePouvoir_adresseCPM3']         = pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers4['fondePouvoir_adresseCommuneM3']              = pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune : (pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille != null ? (pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays) : '');

if (Value('id').of(pouvoir[3].cadreIdentiteDirigeant.personneLieeQualite).eq('66') and pouvoir[3].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
formFieldsPers4['fondePouvoir_etablissementAdresseVoieM3']    = (pouvoir[3].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNumeroVoie != null ? pouvoir[3].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNumeroVoie : '')
																+ ' ' + (pouvoir[3].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseIndiceVoie != null ? pouvoir[3].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseIndiceVoie : '')
																+ ' ' + (pouvoir[3].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseTypeVoie != null ? pouvoir[3].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseTypeVoie : '')
																+ ' ' + pouvoir[3].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNomVoie
																+ ' ' + (pouvoir[3].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseComplementAdresse != null ? pouvoir[3].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseComplementAdresse : '')
																+ ' ' + (pouvoir[3].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseDistriutionSpecialeVoie != null ? pouvoir[3].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseDistriutionSpecialeVoie : '');
formFieldsPers4['fondePouvoir_etablissementAdresseCPM3']      = pouvoir[3].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseCodePostal;
formFieldsPers4['fondePouvoir_etablissementAdresseCommuneM3'] = pouvoir[3].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseCommune;
}
}

// Imprimé M3 SARL 5

if (pouvoir.length > 4) {

// Cadre 1

formFieldsPers5['demandeModificationM3']                      = false;
formFieldsPers5['demandeModificationRM_M3']                   = identite.immatriculationRM ? true : false;
formFieldsPers5['demandeIntercalaireM3']                      = true;
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
formFieldsPers5['intercalaireNumM3']                          = "5";
} else {
formFieldsPers5['intercalaireNumM3']                          = "4";
} 
 
// Cadre 2

formFieldsPers5['entreprise_sirenM3']                                     = identite.siren.split(' ').join('');
formFieldsPers5['immatRCS_M3']                                            = true;
formFieldsPers5['immatRCSGreffe_M3']                                      = identite.immatRCSGreffe;
formFieldsPers5['immatRM_M3']                                             = identite.immatriculationRM ? true : false;
formFieldsPers5['immatRMDept_M3']                                         = identite.immatriculationRM ? identite.immatRMCMA : '';
formFieldsPers5['immatRMDeptNum_M3']                                      = identite.immatriculationRM ? identite.immatRMCMA.getId() : '';
formFieldsPers5['entreprise_denominationM3']                              = identite.entrepriseDenomination;
formFieldsPers5['entreprise_formeJuridiqueM3']                            = identite.entrepriseFormeJuridique.getLabel();

// Cadre 10

if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers5['fondePouvoir_dateModifM3']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers5['fondePouvoir_dateModifM3']          = date;
} else if (pouvoir[4].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(pouvoir[4].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers5['fondePouvoir_dateModifM3']          = date;
}
formFieldsPers5['fondePouvoir_nouveauM3']                     = Value('id').of(pouvoir[4].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant') ? false : true;
formFieldsPers5['fondePouvoir_partantM3']                     = Value('id').of(pouvoir[4].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers5['fondePouvoir_nomNaissanceM3']                = pouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
formFieldsPers5['fondePouvoir_nomUsageM3']                    = pouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? pouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < pouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(pouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers5['fondePouvoir_prenomM3']                      = prenoms.toString();
if (pouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(pouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers5['fondePouvoir_dateNaissanceM3']          = date;
}
formFieldsPers5['fondePouvoir_lieuNaissanceM3']               = pouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(pouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' + '(' + '' + pouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() + '' + ')') : 
																(pouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + pouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel());
formFieldsPers5['fondePouvoir_nationaliteM3']                 = pouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNationaliteGerant;
formFieldsPers5['fondePouvoir_adresseVoieM3']                 = (pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
															  + ' ' + (pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
															  + ' ' + (pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
															  + ' ' + pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNomVoie
															  + ' ' + (pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
															  + ' ' + (pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers5['fondePouvoir_adresseCPM3']         = pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers5['fondePouvoir_adresseCommuneM3']              = pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune : (pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille != null ? (pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays) : '');

if (Value('id').of(pouvoir[4].cadreIdentiteDirigeant.personneLieeQualite).eq('66') and pouvoir[4].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
formFieldsPers5['fondePouvoir_etablissementAdresseVoieM3']    = (pouvoir[4].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNumeroVoie != null ? pouvoir[4].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNumeroVoie : '')
																+ ' ' + (pouvoir[4].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseIndiceVoie != null ? pouvoir[4].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseIndiceVoie : '')
																+ ' ' + (pouvoir[4].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseTypeVoie != null ? pouvoir[4].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseTypeVoie : '')
																+ ' ' + pouvoir[4].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNomVoie
																+ ' ' + (pouvoir[4].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseComplementAdresse != null ? pouvoir[4].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseComplementAdresse : '')
																+ ' ' + (pouvoir[4].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseDistriutionSpecialeVoie != null ? pouvoir[4].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseDistriutionSpecialeVoie : '');
formFieldsPers5['fondePouvoir_etablissementAdresseCPM3']      = pouvoir[4].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseCodePostal;
formFieldsPers5['fondePouvoir_etablissementAdresseCommuneM3'] = pouvoir[4].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseCommune;
}
}
 
} 
 
// Imprimé M3 Hors SARL 

if (((dissolution.cadreIdentiteLiquidateur.autreDirigeant
	or Value('id').of(objet.objetModification).contains('dirigeant')
	or (ouverture.ajoutFondePouvoir and ($M2.dirigeant1Sarl.autrePersonneLieeEtablissement or $M2.dirigeant1.autrePersonneLieeEtablissement)))
	and not Value('id').of(identite.entrepriseFormeJuridique).eq('5499') and not Value('id').of(identite.entrepriseFormeJuridique).eq('5410')
	and not Value('id').of(identite.entrepriseFormeJuridique).eq('5415') and not Value('id').of(identite.entrepriseFormeJuridique).eq('5422')
	and not Value('id').of(identite.entrepriseFormeJuridique).eq('5426') and not Value('id').of(identite.entrepriseFormeJuridique).eq('5430')
	and not Value('id').of(identite.entrepriseFormeJuridique).eq('5431') and not Value('id').of(identite.entrepriseFormeJuridique).eq('5432')
	and not Value('id').of(identite.entrepriseFormeJuridique).eq('5442') and not Value('id').of(identite.entrepriseFormeJuridique).eq('5443')
	and not Value('id').of(identite.entrepriseFormeJuridique).eq('5451') and not Value('id').of(identite.entrepriseFormeJuridique).eq('5453')
	and not Value('id').of(identite.entrepriseFormeJuridique).eq('5454') and not Value('id').of(identite.entrepriseFormeJuridique).eq('5455')
	and not Value('id').of(identite.entrepriseFormeJuridique).eq('5458') and not Value('id').of(identite.entrepriseFormeJuridique).eq('5459')
	and not Value('id').of(identite.entrepriseFormeJuridique).eq('5460') and not Value('id').of(identite.entrepriseFormeJuridique).eq('5485')
	and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M'))
	or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')
	and (modifFormeJuridique.formeJuridiqueModifDirigeant
	or Value('id').of(objet.objetModification).contains('dirigeant')
	or (ouverture.ajoutFondePouvoir and ($M2.dirigeant1Sarl.autrePersonneLieeEtablissement or $M2.dirigeant1.autrePersonneLieeEtablissement)))
	and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5499') and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5410')
	and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5415') and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5422')
	and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5426') and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5430')
	and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5431') and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5432')
	and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5442') and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5443')
	and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5451') and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5453')
	and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5454') and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5455')
	and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5458') and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5459')
	and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5460') and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5485'))) {
 
// Cadre 1

																	 
formFieldsPers6['demandeModificationRM_M3']                   = identite.immatriculationRM ? true : false;
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
formFieldsPers6['demandeModificationM3']                      = false;
formFieldsPers6['intercalaireNumM3']                          = "1";
formFieldsPers6['demandeIntercalaireM3']                      = true;
} else {
formFieldsPers6['demandeModificationM3']                      = true;
formFieldsPers6['intercalaireNumM3']                          = '';
formFieldsPers6['demandeIntercalaireM3']                      = false;
} 
 
// Cadre 2

formFieldsPers6['entreprise_denominationM3']                              = identite.entrepriseDenomination;

// Cadre 3

formFieldsPers6['entreprise_sirenM3']                                     = identite.siren.split(' ').join('');
formFieldsPers6['immatRCS_M3']                                            = true;
formFieldsPers6['immatRCSGreffe_M3']                                      = identite.immatRCSGreffe;
formFieldsPers6['immatRM_M3']                                             = identite.immatriculationRM ? true : false;
formFieldsPers6['immatRMDept_M3']                                         = identite.immatriculationRM ? identite.immatRMCMA : '';
formFieldsPers6['immatRMDeptNum_M3']                                      = identite.immatriculationRM ? identite.immatRMCMA.getId() : '';
formFieldsPers6['entreprise_formeJuridiqueM3']                            = identite.entrepriseFormeJuridique.getLabel();
formFieldsPers6['entreprise_adresseEntreprisePM_voieM3']                  = (siege.siegeAdresseNumeroVoie != null ? siege.siegeAdresseNumeroVoie : '')
																		  + ' ' + (siege.siegeAdresseIndiceVoie != null ? siege.siegeAdresseIndiceVoie : '')
																		  + ' ' + (siege.siegeAdresseTypeVoie != null ? siege.siegeAdresseTypeVoie : '')
																		  + ' ' + siege.siegeAdresseNomVoie
																		  + ' ' + (siege.siegeAdresseComplementVoie != null ? siege.siegeAdresseComplementVoie : '')
																		  + ' ' + (siege.siegeAdresseDistriutionSpecialeVoie != null ? siege.siegeAdresseDistriutionSpecialeVoie : '');
formFieldsPers6['entreprise_adresseEntreprisePM_codePostalM3']           = siege.siegeAdresseCodePostal;
formFieldsPers6['entreprise_adresseEntreprisePM_communeM3']              = siege.siegeAdresseCommune;
formFieldsPers6['entreprise_adresseEntreprisePM_communeAncienneM3']      = siege.communeAncienneAdresseSiege != null ? siege.communeAncienneAdresseSiege : '';

// Dirigeant 1

// Cadre 4

if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['modifDate_dirigeantM3[0]']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['modifDate_dirigeantM3[0]']          = date;
}  else if (ouverture.ajoutFondePouvoir and ouverture.modifDateAdresseOuverture != null) {
    var dateTmp = new Date(parseInt(ouverture.modifDateAdresseOuverture.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['modifDate_dirigeantM3']          = date;
} else if (ouverture.ajoutFondePouvoir and ouverture.modifDateRepriseExploitation != null) {
    var dateTmp = new Date(parseInt(ouverture.modifDateRepriseExploitation.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['modifDate_dirigeantM3']          = date;
}else if (dirigeant1.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(dirigeant1.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['modifDate_dirigeantM3[0]']          = date;
}
formFieldsPers6['dirigeant_nouveauM3[0]']                     = (ouverture.ajoutFondePouvoir or Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantNouveau')) ? true : false;
formFieldsPers6['dirigeant_partantM3[0]']                     = Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers6['dirigeant_modifM3[0]']                       = Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantModif') ? true : false;
formFieldsPers6['dirigeant_maintenuM3[0]']                    = Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantMaintenu') ? true : false;

if (not Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers6['dirigeant_maintenuAncienneQualiteM3[0]']     = Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantMaintenu') ? dirigeant1.personneLieeQualiteAncienne : '';
formFieldsPers6['dirigeant_qualiteM3[0]']                     = Value('id').of(dirigeant1.personneLieeQualite).eq('30') ? "Gérant" :
																(Value('id').of(dirigeant1.personneLieeQualite).eq('29') ? "Gérant et associé indéfiniment responsable" :
																(Value('id').of(dirigeant1.personneLieeQualite).eq('28') ? "Gérant et associé indéfiniment et solidairement responsable" :
																(Value('id').of(dirigeant1.personneLieeQualite).eq('75') ? "Associé indéfiniment responsable" :
																(Value('id').of(dirigeant1.personneLieeQualite).eq('74') ? "Associé indéfiniment et solidairement responsable" : dirigeant1.personneLieeQualite))));

if (Value('id').of(dirigeant1.personnaliteJuridique).eq('personnePhysique') or dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
formFieldsPers6['dirigeant_nomNaissanceM3[0]']                                                      = dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant ;
formFieldsPers6['dirigeant_nomUsageM3[0]']                                                          = dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers6['dirigeant_prenomM3[0]']                                                      = prenoms.toString();
} else if (dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers6['dirigeant_prenomM3[0]']                                                      = prenoms.toString();
}
if (dirigeant1.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant1.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['dirigeant_dateNaissanceM3[0]']          = date1;
}  else if (dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['dirigeant_dateNaissanceM3[0]']          = date2;
}
formFieldsPers6['dirigeant_lieuNaissanceM3[0]']                 = dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																(dirigeant1.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? 
																(dirigeant1.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : 
																(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
																(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers6['dirigeant_nationaliteM3[0]']                   = dirigeant1.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant1.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
}
if (Value('id').of(dirigeant1.personnaliteJuridique).eq('personneMorale')) {
formFieldsPers6['dirigeant_denoFormeJuridiqueM3[0]']                = dirigeant1.civilitePersonneMorale.personneLieePMDenomination + ' / ' +dirigeant1.civilitePersonneMorale.personneLieePMFormeJuridique;
formFieldsPers6['dirigeant_PM_immatriculationNumM3[0]']             = dirigeant1.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant1.civilitePersonneMorale.personneLieePMNumeroIdentification : (dirigeant1.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee != null ? (dirigeant1.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee + ' ' + dirigeant1.civilitePersonneMorale.personneLieePMNomNonImmatriculee) : '');
formFieldsPers6['dirigeant_PM_immatriculationLieuM3[0]']            = dirigeant1.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant1.civilitePersonneMorale.personneLieePMLieuImmatriculation : (dirigeant1.civilitePersonneMorale.personneLieePMLieuImmatriculation + ' / ' + dirigeant1.civilitePersonneMorale.personneLieePMPaysNonImmatriculee);
}
formFieldsPers6['dirigeant_adresseVoieM3[0]']                 = (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
															  + ' ' + (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
															  + ' ' + (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
															  + ' ' + dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNomVoie
															  + ' ' + (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
															  + ' ' + (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers6['dirigeant_adresseCPM3[0]']         		  = dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers6['dirigeant_adresseCommuneM3[0]']              = dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseVille != null ? (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays) : '');
}
if (Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers6['dirigeant_partantBisM3[0]']                        = true;
formFieldsPers6['dirigeantPartant_nomNaissanceM3[0]']               = dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : '') ;
formFieldsPers6['dirigeantPartant_nomUsageM3[0]']                   = dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers6['dirigeantPartant_prenomM3[0]']                                                      = prenoms.toString();
} else if (dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers6['dirigeantPartant_prenomM3[0]']                                                      = prenoms.toString();
}
formFieldsPers6['dirigeantPartant_denoFormeJuridique[0]']           = dirigeant1.civilitePersonneMorale.personneLieePMDenomination != null ? (dirigeant1.civilitePersonneMorale.personneLieePMDenomination + ' / ' +dirigeant1.civilitePersonneMorale.personneLieePMFormeJuridique) : '';
}

var nirDeclarant = dirigeant1.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers6['dirigeantPartant_numSSM3[0]']                = nirDeclarant.substring(0, 13);
    formFieldsPers6['dirigeantPartant_numSSCleM3[0]']             = nirDeclarant.substring(13, 15);
}
var nirDeclarant2 = dirigeant1.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant2 != null) {
    nirDeclarant2 = nirDeclarant2.replace(/ /g, "");
    formFieldsPers6['dirigeantPartant_numSSM3[0]']                = nirDeclarant2.substring(0, 13);
    formFieldsPers6['dirigeantPartant_numSSCleM3[0]']             = nirDeclarant2.substring(13, 15);
}

// Dirigeant 2

// Cadre 5

if ($M2.dirigeant1.autrePersonneLieeEtablissement) {
if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['modifDate_dirigeantM3[1]']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['modifDate_dirigeantM3[1]']          = date;
} else if (dirigeant2.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(dirigeant2.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['modifDate_dirigeantM3[1]']          = date;
}
formFieldsPers6['dirigeant_nouveauM3[1]']                     = Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantNouveau') ? true : false;
formFieldsPers6['dirigeant_partantM3[1]']                     = Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers6['dirigeant_modifM3[1]']                       = Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantModif') ? true : false;
formFieldsPers6['dirigeant_maintenuM3[1]']                    = Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantMaintenu') ? true : false;

if (not Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers6['dirigeant_maintenuAncienneQualiteM3[1]']     = Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantMaintenu') ? dirigeant2.personneLieeQualiteAncienne : '';
formFieldsPers6['dirigeant_qualiteM3[1]']                     = Value('id').of(dirigeant2.personneLieeQualite).eq('30') ? "Gérant" :
																(Value('id').of(dirigeant2.personneLieeQualite).eq('29') ? "Gérant et associé indéfiniment responsable" :
																(Value('id').of(dirigeant2.personneLieeQualite).eq('28') ? "Gérant et associé indéfiniment et solidairement responsable" :
																(Value('id').of(dirigeant2.personneLieeQualite).eq('75') ? "Associé indéfiniment responsable" :
																(Value('id').of(dirigeant2.personneLieeQualite).eq('74') ? "Associé indéfiniment et solidairement responsable" : dirigeant2.personneLieeQualite))));

if (Value('id').of(dirigeant2.personnaliteJuridique).eq('personnePhysique') or dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
formFieldsPers6['dirigeant_nomNaissanceM3[1]']                                                      = dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant ;
formFieldsPers6['dirigeant_nomUsageM3[1]']                                                          = dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers6['dirigeant_prenomM3[1]']                                                      = prenoms.toString();
} else if (dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers6['dirigeant_prenomM3[1]']                                                      = prenoms.toString();
}
if (dirigeant2.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant2.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['dirigeant_dateNaissanceM3[1]']          = date1;
}  else if (dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['dirigeant_dateNaissanceM3[1]']          = date2;
}
formFieldsPers6['dirigeant_lieuNaissanceM3[1]']                 = dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																(dirigeant2.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? 
																(dirigeant2.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : 
																(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
																(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers6['dirigeant_nationaliteM3[1]']                   = dirigeant2.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant2.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
}
if (Value('id').of(dirigeant2.personnaliteJuridique).eq('personneMorale')) {
formFieldsPers6['dirigeant_denoFormeJuridiqueM3[1]']                = dirigeant2.civilitePersonneMorale.personneLieePMDenomination + ' / ' + dirigeant2.civilitePersonneMorale.personneLieePMFormeJuridique;
formFieldsPers6['dirigeant_PM_immatriculationNumM3[1]']             = dirigeant2.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant2.civilitePersonneMorale.personneLieePMNumeroIdentification : (dirigeant2.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee != null ? (dirigeant2.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee + ' ' + dirigeant2.civilitePersonneMorale.personneLieePMNomNonImmatriculee) : '');
formFieldsPers6['dirigeant_PM_immatriculationLieuM3[1]']            = dirigeant2.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant2.civilitePersonneMorale.personneLieePMLieuImmatriculation : (dirigeant2.civilitePersonneMorale.personneLieePMLieuImmatriculation + ' / ' + dirigeant2.civilitePersonneMorale.personneLieePMPaysNonImmatriculee);
}
formFieldsPers6['dirigeant_adresseVoieM3[1]']                 = (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
															  + ' ' + (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
															  + ' ' + (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
															  + ' ' + dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNomVoie
															  + ' ' + (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
															  + ' ' + (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers6['dirigeant_adresseCPM3[1]']         		  = dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers6['dirigeant_adresseCommuneM3[1]']              = dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseVille != null ? (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays) : '');
}
if (Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers6['dirigeant_partantBisM3[1]']                        = true;
formFieldsPers6['dirigeantPartant_nomNaissanceM3[1]']               = dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : '') ;
formFieldsPers6['dirigeantPartant_nomUsageM3[1]']                   = dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers6['dirigeantPartant_prenomM3[1]']                                                      = prenoms.toString();
} else if (dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers6['dirigeantPartant_prenomM3[1]']                                                      = prenoms.toString();
}
formFieldsPers6['dirigeantPartant_denoFormeJuridique[1]']           = dirigeant2.civilitePersonneMorale.personneLieePMDenomination != null ? (dirigeant2.civilitePersonneMorale.personneLieePMDenomination + ' / ' + dirigeant2.civilitePersonneMorale.personneLieePMFormeJuridique) : '';
}

var nirDeclarant = dirigeant2.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers6['dirigeantPartant_numSSM3[1]']                = nirDeclarant.substring(0, 13);
    formFieldsPers6['dirigeantPartant_numSSCleM3[1]']             = nirDeclarant.substring(13, 15);
}
var nirDeclarant2 = dirigeant2.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant2 != null) {
    nirDeclarant2 = nirDeclarant2.replace(/ /g, "");
    formFieldsPers6['dirigeantPartant_numSSM3[1]']                = nirDeclarant2.substring(0, 13);
    formFieldsPers6['dirigeantPartant_numSSCleM3[1]']             = nirDeclarant2.substring(13, 15);
}
}

// Dirigeant 3

// Cadre 6

if ($M2.dirigeant2.autrePersonneLieeEtablissement) {
if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['modifDate_dirigeantM3[2]']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['modifDate_dirigeantM3[2]']          = date;
} else if (dirigeant3.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(dirigeant3.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['modifDate_dirigeantM3[2]']          = date;
}
formFieldsPers6['dirigeant_nouveauM3[2]']                     = Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantNouveau') ? true : false;
formFieldsPers6['dirigeant_partantM3[2]']                     = Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers6['dirigeant_modifM3[2]']                       = Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantModif') ? true : false;
formFieldsPers6['dirigeant_maintenuM3[2]']                    = Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantMaintenu') ? true : false;

if (not Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers6['dirigeant_maintenuAncienneQualiteM3[2]']     = Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantMaintenu') ? dirigeant3.personneLieeQualiteAncienne : '';
formFieldsPers6['dirigeant_qualiteM3[2]']                     = Value('id').of(dirigeant3.personneLieeQualite).eq('30') ? "Gérant" :
															(Value('id').of(dirigeant3.personneLieeQualite).eq('29') ? "Gérant et associé indéfiniment responsable" :
															(Value('id').of(dirigeant3.personneLieeQualite).eq('28') ? "Gérant et associé indéfiniment et solidairement responsable" :
															(Value('id').of(dirigeant3.personneLieeQualite).eq('75') ? "Associé indéfiniment responsable" :
															(Value('id').of(dirigeant3.personneLieeQualite).eq('74') ? "Associé indéfiniment et solidairement responsable" : dirigeant3.personneLieeQualite))));

if (Value('id').of(dirigeant3.personnaliteJuridique).eq('personnePhysique') or dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
formFieldsPers6['dirigeant_nomNaissanceM3[2]']                                                      = dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant ;
formFieldsPers6['dirigeant_nomUsageM3[2]']                                                          = dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers6['dirigeant_prenomM3[2]']                                                      = prenoms.toString();
} else if (dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers6['dirigeant_prenomM3[2]']                                                      = prenoms.toString();
}
if (dirigeant3.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant3.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['dirigeant_dateNaissanceM3[2]']          = date1;
}  else if (dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['dirigeant_dateNaissanceM3[2]']          = date2;
}
formFieldsPers6['dirigeant_lieuNaissanceM3[2]']                 = dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																(dirigeant3.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? 
																(dirigeant3.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : 
																(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
																(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers6['dirigeant_nationaliteM3[2]']                   = dirigeant3.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant3.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
}
if (Value('id').of(dirigeant3.personnaliteJuridique).eq('personneMorale')) {
formFieldsPers6['dirigeant_denoFormeJuridiqueM3[2]']                = dirigeant3.civilitePersonneMorale.personneLieePMDenomination + ' / ' + dirigeant3.civilitePersonneMorale.personneLieePMFormeJuridique;
formFieldsPers6['dirigeant_PM_immatriculationNumM3[2]']             = dirigeant3.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant3.civilitePersonneMorale.personneLieePMNumeroIdentification : (dirigeant3.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee != null ? (dirigeant3.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee + ' ' + dirigeant3.civilitePersonneMorale.personneLieePMNomNonImmatriculee) : '');
formFieldsPers6['dirigeant_PM_immatriculationLieuM3[2]']            = dirigeant3.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant3.civilitePersonneMorale.personneLieePMLieuImmatriculation : (dirigeant3.civilitePersonneMorale.personneLieePMLieuImmatriculation + ' / ' + dirigeant3.civilitePersonneMorale.personneLieePMPaysNonImmatriculee);
}
formFieldsPers6['dirigeant_adresseVoieM3[2]']                 = (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
															  + ' ' + (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
															  + ' ' + (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
															  + ' ' + dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNomVoie
															  + ' ' + (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
															  + ' ' + (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers6['dirigeant_adresseCPM3[2]']         		  = dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers6['dirigeant_adresseCommuneM3[2]']              = dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseVille != null ? (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays) : '');
}
if (Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers6['dirigeant_partantBisM3[2]']                        = true;
formFieldsPers6['dirigeantPartant_nomNaissanceM3[2]']               = dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : '') ;
formFieldsPers6['dirigeantPartant_nomUsageM3[2]']                   = dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers6['dirigeantPartant_prenomM3[2]']                                                      = prenoms.toString();
} else if (dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers6['dirigeantPartant_prenomM3[2]']                                                      = prenoms.toString();
}
formFieldsPers6['dirigeantPartant_denoFormeJuridique[2]']           = dirigeant3.civilitePersonneMorale.personneLieePMDenomination != null ? (dirigeant3.civilitePersonneMorale.personneLieePMDenomination + ' / ' + dirigeant3.civilitePersonneMorale.personneLieePMFormeJuridique) : '';
}

var nirDeclarant = dirigeant3.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers6['dirigeantPartant_numSSM3[2]']                = nirDeclarant.substring(0, 13);
    formFieldsPers6['dirigeantPartant_numSSCleM3[2]']             = nirDeclarant.substring(13, 15);
}
var nirDeclarant2 = dirigeant3.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant2 != null) {
    nirDeclarant2 = nirDeclarant2.replace(/ /g, "");
    formFieldsPers6['dirigeantPartant_numSSM3[2]']                = nirDeclarant2.substring(0, 13);
    formFieldsPers6['dirigeantPartant_numSSCleM3[2]']             = nirDeclarant2.substring(13, 15);
}
}

// Dirigeant 4

// Cadre 7

if ($M2.dirigeant3.autrePersonneLieeEtablissement) {
if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['modifDate_dirigeantM3[3]']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['modifDate_dirigeantM3[3]']          = date;
} else if (dirigeant4.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(dirigeant4.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['modifDate_dirigeantM3[3]']          = date;
}
formFieldsPers6['dirigeant_nouveauM3[3]']                     = Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantNouveau') ? true : false;
formFieldsPers6['dirigeant_partantM3[3]']                     = Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers6['dirigeant_modifM3[3]']                       = Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantModif') ? true : false;
formFieldsPers6['dirigeant_maintenuM3[3]']                    = Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantMaintenu') ? true : false;

if (not Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers6['dirigeant_maintenuAncienneQualiteM3[3]']     = Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantMaintenu') ? dirigeant4.personneLieeQualiteAncienne : '';
formFieldsPers6['dirigeant_qualiteM3[3]']                     = Value('id').of(dirigeant4.personneLieeQualite).eq('30') ? "Gérant" :
															(Value('id').of(dirigeant4.personneLieeQualite).eq('29') ? "Gérant et associé indéfiniment responsable" :
															(Value('id').of(dirigeant4.personneLieeQualite).eq('28') ? "Gérant et associé indéfiniment et solidairement responsable" :
															(Value('id').of(dirigeant4.personneLieeQualite).eq('75') ? "Associé indéfiniment responsable" :
															(Value('id').of(dirigeant4.personneLieeQualite).eq('74') ? "Associé indéfiniment et solidairement responsable" : dirigeant4.personneLieeQualite))));

if (Value('id').of(dirigeant4.personnaliteJuridique).eq('personnePhysique') or dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
formFieldsPers6['dirigeant_nomNaissanceM3[3]']                                                      = dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant ;
formFieldsPers6['dirigeant_nomUsageM3[3]']                                                          = dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers6['dirigeant_prenomM3[3]']                                                      = prenoms.toString();
} else if (dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers6['dirigeant_prenomM3[3]']                                                      = prenoms.toString();
}
if (dirigeant4.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant4.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['dirigeant_dateNaissanceM3[3]']          = date1;
}  else if (dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['dirigeant_dateNaissanceM3[3]']          = date2;
}
formFieldsPers6['dirigeant_lieuNaissanceM3[3]']                 = dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																(dirigeant4.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? 
																(dirigeant4.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : 
																(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
																(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers6['dirigeant_nationaliteM3[3]']                   = dirigeant4.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant4.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
}
if (Value('id').of(dirigeant4.personnaliteJuridique).eq('personneMorale')) {
formFieldsPers6['dirigeant_denoFormeJuridiqueM3[3]']                = dirigeant4.civilitePersonneMorale.personneLieePMDenomination + ' / ' + dirigeant4.civilitePersonneMorale.personneLieePMFormeJuridique;
formFieldsPers6['dirigeant_PM_immatriculationNumM3[3]']             = dirigeant4.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant4.civilitePersonneMorale.personneLieePMNumeroIdentification : (dirigeant4.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee != null ? (dirigeant4.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee + ' ' + dirigeant4.civilitePersonneMorale.personneLieePMNomNonImmatriculee) : '');
formFieldsPers6['dirigeant_PM_immatriculationLieuM3[3]']            = dirigeant4.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant4.civilitePersonneMorale.personneLieePMLieuImmatriculation : (dirigeant4.civilitePersonneMorale.personneLieePMLieuImmatriculation + ' / ' + dirigeant4.civilitePersonneMorale.personneLieePMPaysNonImmatriculee);
}
formFieldsPers6['dirigeant_adresseVoieM3[3]']                 = (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
															  + ' ' + (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
															  + ' ' + (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
															  + ' ' + dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNomVoie
															  + ' ' + (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
															  + ' ' + (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers6['dirigeant_adresseCPM3[3]']         		  = dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers6['dirigeant_adresseCommuneM3[3]']              = dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseVille != null ? (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays) : '');
}
if (Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers6['dirigeant_partantBisM3[3]']                        = true;
formFieldsPers6['dirigeantPartant_nomNaissanceM3[3]']               = dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : '') ;
formFieldsPers6['dirigeantPartant_nomUsageM3[3]']                   = dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers6['dirigeantPartant_prenomM3[3]']                                                      = prenoms.toString();
} else if (dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers6['dirigeantPartant_prenomM3[3]']                                                      = prenoms.toString();
}
formFieldsPers6['dirigeantPartant_denoFormeJuridique[3]']           = dirigeant4.civilitePersonneMorale.personneLieePMDenomination != null ? (dirigeant4.civilitePersonneMorale.personneLieePMDenomination + ' / ' + dirigeant4.civilitePersonneMorale.personneLieePMFormeJuridique) : '';
}

var nirDeclarant = dirigeant4.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers6['dirigeantPartant_numSSM3[3]']                = nirDeclarant.substring(0, 13);
    formFieldsPers6['dirigeantPartant_numSSCleM3[3]']             = nirDeclarant.substring(13, 15);
}
var nirDeclarant2 = dirigeant4.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant2 != null) {
    nirDeclarant2 = nirDeclarant2.replace(/ /g, "");
    formFieldsPers6['dirigeantPartant_numSSM3[3]']                = nirDeclarant2.substring(0, 13);
    formFieldsPers6['dirigeantPartant_numSSCleM3[3]']             = nirDeclarant2.substring(13, 15);
}
}

// Dirigeant 5

// Cadre 8

if ($M2.dirigeant4.autrePersonneLieeEtablissement) {
if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['modifDate_dirigeantM3[4]']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['modifDate_dirigeantM3[4]']          = date;
} else if (dirigeant5.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(dirigeant5.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['modifDate_dirigeantM3[4]']          = date;
}
formFieldsPers6['dirigeant_nouveauM3[4]']                     = Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantNouveau') ? true : false;
formFieldsPers6['dirigeant_partantM3[4]']                     = Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers6['dirigeant_modifM3[4]']                       = Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantModif') ? true : false;
formFieldsPers6['dirigeant_maintenuM3[4]']                    = Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantMaintenu') ? true : false;

if (not Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers6['dirigeant_maintenuAncienneQualiteM3[4]']     = Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantMaintenu') ? dirigeant5.personneLieeQualiteAncienne : '';
formFieldsPers6['dirigeant_qualiteM3[4]']                     = Value('id').of(dirigeant5.personneLieeQualite).eq('30') ? "Gérant" :
															(Value('id').of(dirigeant5.personneLieeQualite).eq('29') ? "Gérant et associé indéfiniment responsable" :
															(Value('id').of(dirigeant5.personneLieeQualite).eq('28') ? "Gérant et associé indéfiniment et solidairement responsable" :
															(Value('id').of(dirigeant5.personneLieeQualite).eq('75') ? "Associé indéfiniment responsable" :
															(Value('id').of(dirigeant5.personneLieeQualite).eq('74') ? "Associé indéfiniment et solidairement responsable" : dirigeant5.personneLieeQualite))));

if (Value('id').of(dirigeant5.personnaliteJuridique).eq('personnePhysique') or dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
formFieldsPers6['dirigeant_nomNaissanceM3[4]']                                                      = dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant ;
formFieldsPers6['dirigeant_nomUsageM3[4]']                                                          = dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers6['dirigeant_prenomM3[4]']                                                      = prenoms.toString();
} else if (dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers6['dirigeant_prenomM3[4]']                                                      = prenoms.toString();
}
if (dirigeant5.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant5.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['dirigeant_dateNaissanceM3[4]']          = date1;
}  else if (dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['dirigeant_dateNaissanceM3[4]']          = date2;
}
formFieldsPers6['dirigeant_lieuNaissanceM3[4]']                 = dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																(dirigeant5.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? 
																(dirigeant5.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : 
																(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
																(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers6['dirigeant_nationaliteM3[4]']                   = dirigeant5.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant5.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
}
if (Value('id').of(dirigeant5.personnaliteJuridique).eq('personneMorale')) {
formFieldsPers6['dirigeant_denoFormeJuridiqueM3[4]']                = dirigeant5.civilitePersonneMorale.personneLieePMDenomination + ' / ' + dirigeant5.civilitePersonneMorale.personneLieePMFormeJuridique;
formFieldsPers6['dirigeant_PM_immatriculationNumM3[4]']             = dirigeant5.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant5.civilitePersonneMorale.personneLieePMNumeroIdentification : (dirigeant5.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee != null ? (dirigeant5.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee + ' ' + dirigeant5.civilitePersonneMorale.personneLieePMNomNonImmatriculee) : '');
formFieldsPers6['dirigeant_PM_immatriculationLieuM3[4]']            = dirigeant5.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant5.civilitePersonneMorale.personneLieePMLieuImmatriculation : (dirigeant5.civilitePersonneMorale.personneLieePMLieuImmatriculation + ' / ' + dirigeant5.civilitePersonneMorale.personneLieePMPaysNonImmatriculee);
}
formFieldsPers6['dirigeant_adresseVoieM3[4]']                 = (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
															  + ' ' + (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
															  + ' ' + (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
															  + ' ' + dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNomVoie
															  + ' ' + (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
															  + ' ' + (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers6['dirigeant_adresseCPM3[4]']         		  = dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers6['dirigeant_adresseCommuneM3[4]']              = dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseVille != null ? (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays) : '');
}
if (Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers6['dirigeant_partantBisM3[4]']                        = true;
formFieldsPers6['dirigeantPartant_nomNaissanceM3[4]']               = dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : '') ;
formFieldsPers6['dirigeantPartant_nomUsageM3[4]']                   = dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers6['dirigeantPartant_prenomM3[4]']                                                      = prenoms.toString();
} else if (dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers6['dirigeantPartant_prenomM3[4]']                                                      = prenoms.toString();
}
formFieldsPers6['dirigeantPartant_denoFormeJuridique[4]']           = dirigeant5.civilitePersonneMorale.personneLieePMDenomination != null ? (dirigeant5.civilitePersonneMorale.personneLieePMDenomination + ' / ' + dirigeant5.civilitePersonneMorale.personneLieePMFormeJuridique) : '';
}

var nirDeclarant = dirigeant5.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers6['dirigeantPartant_numSSM3[4]']                = nirDeclarant.substring(0, 13);
    formFieldsPers6['dirigeantPartant_numSSCleM3[4]']             = nirDeclarant.substring(13, 15);
}
var nirDeclarant2 = dirigeant5.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant2 != null) {
    nirDeclarant2 = nirDeclarant2.replace(/ /g, "");
    formFieldsPers6['dirigeantPartant_numSSM3[4]']                = nirDeclarant2.substring(0, 13);
    formFieldsPers6['dirigeantPartant_numSSCleM3[4]']             = nirDeclarant2.substring(13, 15);
}
}

// Représentant permanent 1

// Cadre 9

var representants = [];

var cadreDirigeants = [
{
	'cadre': '4',
	'dirigeant' : null == $M2.dirigeant1 ? null : $M2.dirigeant1
},{
	'cadre': '5',
	'dirigeant' : null == $M2.dirigeant2 ? null : $M2.dirigeant2
},{
	'cadre': '6',
	'dirigeant' : null == $M2.dirigeant3 ? null : $M2.dirigeant3
},{
	'cadre': '7',
	'dirigeant' : null == $M2.dirigeant4 ? null : $M2.dirigeant4
},{
	'cadre': '8',
	'dirigeant' : null == $M2.dirigeant5 ? null : $M2.dirigeant5
},{
	'cadre': '4',
	'dirigeant' : null == $M2.dirigeant6 ? null : $M2.dirigeant6
},{
	'cadre': '5',
	'dirigeant' : null == $M2.dirigeant7 ? null : $M2.dirigeant7
},{
	'cadre': '6',
	'dirigeant' : null == $M2.dirigeant8 ? null : $M2.dirigeant8
},{
	'cadre': '7',
	'dirigeant' : null == $M2.dirigeant9 ? null : $M2.dirigeant9
},{
	'cadre': '8',
	'dirigeant' : null == $M2.dirigeant10 ? null : $M2.dirigeant10
},{
	'cadre': '4',
	'dirigeant' : null == $M2.dirigeant11 ? null : $M2.dirigeant11
},{
	'cadre': '5',
	'dirigeant' : null == $M2.dirigeant12 ? null : $M2.dirigeant12
}];

for (var idx in cadreDirigeants) {
	var cadreDirigeant = cadreDirigeants[idx];
	var dirigeant = cadreDirigeant['dirigeant'];
	if (null != dirigeant.cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomNaissance) {
		representants.push(cadreDirigeant);	
	}
}

log.info("Number of representants : {}", representants.length);
log.info("Representants : {}", representants);

if (representants.length > 0) {
formFieldsPers6['representantPermanent_numeroCadre[0]']          = representants[0]['cadre'];
formFieldsPers6['representantPermanent_duM2[0]']                 = false;
formFieldsPers6['representantPermanent_duM3[0]']                 = true;
if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['representantPermanent_dateModif[0]']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['representantPermanent_dateModif[0]']          = date;
} else if (representants[0]['dirigeant'].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(representants[0]['dirigeant'].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['representantPermanent_dateModif[0]']          = date;
}
formFieldsPers6['representantPermanent_nouveau[0]']              = (Value('id').of(representants[0]['dirigeant'].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau') or Value('id').of(representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantNouveau')) ? true : false ;
formFieldsPers6['representantPermanent_partant[0]']              = Value('id').of(representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers6['representantPermanent_modif[0]']                = Value('id').of(representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif') ? true : false;
formFieldsPers6['representantPermanent_nomNaissance[0]']         = representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomNaissance;
formFieldsPers6['representantPermanent_nomUsage[0]']             = representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage != null ? representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage : '';
if (representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom != null) {
var prenoms=[];
for ( i = 0; i < representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom[i]);}                            
formFieldsPers6['representantPermanent_prenom[0]']                                                      = prenoms.toString();
}
formFieldsPers6['representantPermanent_nationalite[0]']          = representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNationalite;
if (representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['representantPermanent_dateNaissance[0]']          = date1;
}
formFieldsPers6['representantPermanent_lieuNaissanceCommune[0]'] = representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune != null ? 
																	(representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune + ' ' +  "(" + '' + representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceDepartement.getId() +''+ ")") :
																	(representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissancePays);
formFieldsPers6['representantPermanent_adresseVoie[0]']          = (representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null ? representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie : '')
																	+ ' ' + (representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null ? representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie : '')
																	+ ' ' + (representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null ? representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie : '')
																	+ ' ' + representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNomVoie
																	+ ' ' + (representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null ? representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie : '')
																	+ ' ' + (representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null ? representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale : '');
formFieldsPers6['representantPermanent_adresseCP[0]']            = representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
formFieldsPers6['representantPermanent_adresseCommune[0]']       = representants[0]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune;
}

// Représentant permanent 2

// Cadre 10

if (representants.length > 1) {
formFieldsPers6['representantPermanent_numeroCadre[1]']          = representants[1]['cadre'];
formFieldsPers6['representantPermanent_duM2[1]']                 = false;
formFieldsPers6['representantPermanent_duM3[1]']                 = true;
if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['representantPermanent_dateModif[1]']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['representantPermanent_dateModif[1]']          = date;
} else if (representants[1]['dirigeant'].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(representants[1]['dirigeant'].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['representantPermanent_dateModif[1]']          = date;
}
formFieldsPers6['representantPermanent_nouveau[1]']              = (Value('id').of(representants[1]['dirigeant'].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau') or Value('id').of(representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantNouveau')) ? true : false ;
formFieldsPers6['representantPermanent_partant[1]']              = Value('id').of(representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers6['representantPermanent_modif[1]']                = Value('id').of(representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif') ? true : false;
formFieldsPers6['representantPermanent_nomNaissance[1]']         = representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomNaissance;
formFieldsPers6['representantPermanent_nomUsage[1]']             = representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage != null ? representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage : '';
if (representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom != null) {
var prenoms=[];
for ( i = 0; i < representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom[i]);}                            
formFieldsPers6['representantPermanent_prenom[1]']                                                      = prenoms.toString();
}
formFieldsPers6['representantPermanent_nationalite[1]']          = representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNationalite;
if (representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['representantPermanent_dateNaissance[1]']          = date1;
}
formFieldsPers6['representantPermanent_lieuNaissanceCommune[1]'] = representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune != null ? 
																	(representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune + ' ' +  "(" + '' + representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceDepartement.getId() +''+ ")") :
																	(representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissancePays);
formFieldsPers6['representantPermanent_adresseVoie[1]']          = (representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null ? representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie : '')
																	+ ' ' + (representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null ? representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie : '')
																	+ ' ' + (representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null ? representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie : '')
																	+ ' ' + representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNomVoie
																	+ ' ' + (representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null ? representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie : '')
																	+ ' ' + (representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null ? representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale : '');
formFieldsPers6['representantPermanent_adresseCP[1]']            = representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
formFieldsPers6['representantPermanent_adresseCommune[1]']       = representants[1]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune;
}

// Cadre 11 Précision fondé de pouvoir 1

var fondePouvoir = [];

['dirigeant1', 'dirigeant2', 'dirigeant3', 'dirigeant4', 'dirigeant5', 'dirigeant6', 'dirigeant7', 'dirigeant8', 'dirigeant9', 'dirigeant10', 'dirigeant11', 'dirigeant12'].forEach(function(fondePouvoirs){
	if(fondePouvoirs=='dirigeant1' and (Value('id').of($M2[fondePouvoirs].cadreIdentiteDirigeant.personneLieeQualite).eq('66') or ouverture.ajoutFondePouvoir)){
		fondePouvoir.push($M2[fondePouvoirs]);	
	}
	if ( fondePouvoirs!='dirigeant1' and null != $M2[fondePouvoirs] and Value('id').of($M2[fondePouvoirs].cadreIdentiteDirigeant.personneLieeQualite).eq('66')) {
		fondePouvoir.push($M2[fondePouvoirs]);	
	}
});

/*
var cadrePouvoirs = [
{
	'cadres': '4',
	'pouvoirDirigeant' : (Value('id').of($M2.dirigeant1.cadreIdentiteDirigeant.personneLieeQualite).eq('66') or not ouverture.ajoutFondePouvoir) ? null : $M2.dirigeant1
},{
	'cadres': '5',no
	'pouvoirDirigeant' : (Value('id').of($M2.dirigeant2.cadreIdentiteDirigeant.personneLieeQualite).eq('66')) ? null : $M2.dirigeant2
},{
	'cadres': '6',
	'pouvoirDirigeant' : (Value('id').of($M2.dirigeant3.cadreIdentiteDirigeant.personneLieeQualite).eq('66')) ? null : $M2.dirigeant3
},{
	'cadres': '7',
	'pouvoirDirigeant' : (Value('id').of($M2.dirigeant4.cadreIdentiteDirigeant.personneLieeQualite).eq('66')) ? null : $M2.dirigeant4
},{
	'cadres': '8',
	'pouvoirDirigeant' : (Value('id').of($M2.dirigeant5.cadreIdentiteDirigeant.personneLieeQualite).eq('66')) ? null : $M2.dirigeant5
},{
	'cadres': '4',
	'pouvoirDirigeant' : (Value('id').of($M2.dirigeant6.cadreIdentiteDirigeant.personneLieeQualite).eq('66')) ? null : $M2.dirigeant6
},{
	'cadres': '5',
	'pouvoirDirigeant' : (Value('id').of($M2.dirigeant7.cadreIdentiteDirigeant.personneLieeQualite).eq('66')) ? null : $M2.dirigeant7
},{
	'cadres': '6',
	'pouvoirDirigeant' : (Value('id').of($M2.dirigeant8.cadreIdentiteDirigeant.personneLieeQualite).eq('66')) ? null : $M2.dirigeant8
},{
	'cadres': '7',
	'pouvoirDirigeant' : (Value('id').of($M2.dirigeant9.cadreIdentiteDirigeant.personneLieeQualite).eq('66')) ? null : $M2.dirigeant9
},{
	'cadres': '8',
	'pouvoirDirigeant' : (Value('id').of($M2.dirigeant10.cadreIdentiteDirigeant.personneLieeQualite).eq('66')) ? null : $M2.dirigeant10
},{
	'cadres': '4',
	'pouvoirDirigeant' : (Value('id').of($M2.dirigeant11.cadreIdentiteDirigeant.personneLieeQualite).eq('66')) ? null : $M2.dirigeant11
},{
	'cadres': '5',
	'pouvoirDirigeant' : (Value('id').of($M2.dirigeant12.cadreIdentiteDirigeant.personneLieeQualite).eq('66')) ? null : $M2.dirigeant12
}];

for (var idx in cadrePouvoirs) {
	var cadrePouvoir = cadrePouvoirs[idx];
	var pouvoirDirigeant = cadrePouvoir['pouvoirDirigeant'];
	if (null != pouvoirDirigeant.cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissance.personneLieePPNomNaissanceGerant) {
		fondePouvoir.push(cadrePouvoir);	
}}
*/
if ((fondePouvoir.length > 0) and (fondePouvoir[0].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOui or fondePouvoir[0].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis)) {
formFieldsPers6['fondePouvoir_declareCadre']                        = fondePouvoir[0]['cadres'];
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['fondePouvoir_dateModifM3[0]']          = date;
}  else if (ouverture.ajoutFondePouvoir and ouverture.modifDateAdresseOuverture != null) {
    var dateTmp = new Date(parseInt(ouverture.modifDateAdresseOuverture.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['fondePouvoir_dateModifM3']          = date;
} else if (ouverture.ajoutFondePouvoir and ouverture.modifDateRepriseExploitation != null) {
    var dateTmp = new Date(parseInt(ouverture.modifDateRepriseExploitation.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['fondePouvoir_dateModifM3']          = date;
} else if (fondePouvoir[0].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(fondePouvoir[0].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['fondePouvoir_dateModifM3[0]']          = date;
}
if (ouverture.ajoutFondePouvoir and fondePouvoir[0].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOui) {
formFieldsPers6['fondePouvoir_etablissementAdresseVoieM3']           = (ouverture.cadreAdresseEtablissementOuvert.numeroAdresseEtablissement != null ? ouverture.cadreAdresseEtablissementOuvert.numeroAdresseEtablissement : '')
																+ ' ' + (ouverture.cadreAdresseEtablissementOuvert.indiceAdresseEtablissement != null ? ouverture.cadreAdresseEtablissementOuvert.indiceAdresseEtablissement : '')
																+ ' ' + (ouverture.cadreAdresseEtablissementOuvert.typeAdresseEtablissement != null ? ouverture.cadreAdresseEtablissementOuvert.typeAdresseEtablissement : '')
																+ ' ' + ouverture.cadreAdresseEtablissementOuvert.voieAdresseEtablissement
																+ ' ' + (ouverture.cadreAdresseEtablissementOuvert.complementAdresseEtablissement != null ? ouverture.cadreAdresseEtablissementOuvert.complementAdresseEtablissement : '')
																+ ' ' + (ouverture.cadreAdresseEtablissementOuvert.distributionSpecialeAdresseEtablissement != null ? ouverture.cadreAdresseEtablissementOuvert.distributionSpecialeAdresseEtablissement : '');
formFieldsPers6['fondePouvoir_etablissementAdresseCPM3']             = ouverture.cadreAdresseEtablissementOuvert.codePostalAdresseEtablissement;
formFieldsPers6['fondePouvoir_etablissementAdresseCommuneM3']        = ouverture.cadreAdresseEtablissementOuvert.communeAdresseEtablissement;
} else if (Value('id').of(fondePouvoir[0].cadreIdentiteDirigeant.personneLieeQualite).eq('66') and fondePouvoir[0].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
formFieldsPers6['fondePouvoir_etablissementAdresseVoieM3']    = (fondePouvoir[0].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNumeroVoie != null ? fondePouvoir[0].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNumeroVoie : '')
																+ ' ' + (fondePouvoir[0].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseIndiceVoie != null ? fondePouvoir[0].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseIndiceVoie : '')
																+ ' ' + (fondePouvoir[0].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseTypeVoie != null ? fondePouvoir[0].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseTypeVoie : '')
																+ ' ' + fondePouvoir[0].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNomVoie
																+ ' ' + (fondePouvoir[0].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseComplementAdresse != null ? fondePouvoir[0].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseComplementAdresse : '')
																+ ' ' + (fondePouvoir[0].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseDistriutionSpecialeVoie != null ? fondePouvoir[0].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseDistriutionSpecialeVoie : '');
formFieldsPers6['fondePouvoir_etablissementAdresseCPM3']      = fondePouvoir[0].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseCodePostal;
formFieldsPers6['fondePouvoir_etablissementAdresseCommuneM3'] = fondePouvoir[0].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseCommune;
}
}


// Cadre 12

var social = [];

['cadreDeclarationSociale1', 'cadreDeclarationSociale2', 'cadreDeclarationSociale3', 'cadreDeclarationSociale4', 'cadreDeclarationSociale5', 'cadreDeclarationSociale6', 'cadreDeclarationSociale7', 'cadreDeclarationSociale8', 'cadreDeclarationSociale9', 'cadreDeclarationSociale10', 'cadreDeclarationSociale11', 'cadreDeclarationSociale12'].forEach(function(socialtns){
	if (null != $M2[socialtns] and $M2[socialtns].cadre7DeclarationSociale.voletSocialNumeroSecuriteSocialTNS != null) {
		social.push($M2[socialtns]);
	}
	
});

if (not Value('id').of(objet.objetModification).contains('situationPM') and not Value('id').of(objet.objetModification).contains('etablissement')) {

formFieldsPers6['observationsM3']                             = correspondance.formaliteObservations != null ? correspondance.formaliteObservations : '';;

// Cadre 13

formFieldsPers6['adresseCorrespondanceCadre_cocheM3']            = (Value('id').of(correspondance.adresseCorrespond).eq('siege') 
																or Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement')
																or Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') 
																or Value('id').of(correspondance.adresseCorrespond).eq('fondsDonneEtablissement')) ? true : false;
formFieldsPers6['adresseCorrespondanceCadre_numeroM3']           = Value('id').of(correspondance.adresseCorrespond).eq('siege') ? "2" : 
																(Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement') ? "12" :
																(Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') ? "13" : 
																(Value('id').of(correspondance.adresseCorrespond).eq('fondsDonneEtablissement') ? "18" : '')));
formFieldsPers6['adressesCorrespondanceAutreM3']                 = Value('id').of(correspondance.adresseCorrespond).eq('autre') ? true : false;
if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
formFieldsPers6['adresseCorrespondance_voie1M3']                 = correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance
																 + ' ' + (correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance : '')
																 + ' ' + (correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance : '')
																 + ' ' + (correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance : '')
																 + ' ' + (correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance : '');
formFieldsPers6['adresseCorrespondance_voie2M3']              	  = (correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance != null ? correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance : '')
																 + ' ' + (correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance != null ? correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance : '');
formFieldsPers6['adresseCorrespondance_codePostalM3']            = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal;
formFieldsPers6['adresseCorrespondance_communeM3']               = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune;
}
formFieldsPers6['telephone2M3']                                                             = correspondance.infosSup.formaliteTelephone1 != null ? correspondance.infosSup.formaliteTelephone1 : '';
formFieldsPers6['telephone1M3']                                                             = correspondance.infosSup.formaliteTelephone2;
formFieldsPers6['courrielM3']                                                               = correspondance.infosSup.formaliteFaxCourriel != null ? correspondance.infosSup.formaliteFaxCourriel : (correspondance.infosSup.telecopie != null ? correspondance.infosSup.telecopie : '');

// Cadre 14

formFieldsPers6['signataireDeclarantM3']                                            = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFieldsPers6['signataireDeclarantCadreM3']                 						= '';
formFieldsPers6['signataireMandataireM3']                                           = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? true : false;
formFieldsPers6['nomPrenomDenominationMandataireM3']					            = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers6['signataireAdresseM3']                                          = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																					+ ' ' + (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					+ ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
																					+ ' ' + signataire.adresseMandataire.villeAdresseMandataire
																					+ ' ' + signataire.adresseMandataire.dataCodePostalMandataire;

if (signataire.formaliteSignatureDate !== null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['signatureDateM3']          = date;
}
formFieldsPers6['signatureLieuM3']                                                        = signataire.formaliteSignatureLieu;
formFieldsPers6['intercalaireNombreM3_M3']                                                = ((representants.length > 10) or (fondePouvoir.length > 5))? "5" : 
																							(((representants.length > 8) or (fondePouvoir.length > 4))? "4" : 
																							(((representants.length > 6) or (fondePouvoir.length > 3)) ? "3" : 
																							(((representants.length > 4) or $M2.dirigeant10.autrePersonneLieeEtablissement or (fondePouvoir.length > 2)) ? "2" : 
																							(((representants.length > 2) or $M2.dirigeant5.autrePersonneLieeEtablissement or (fondePouvoir.length > 1)) ? "1" :"0"))));
formFieldsPers6['intercalaireNombreTNS_M3']                                          = (social.length > 11) ? "12" : 
																					   ((social.length > 10) ? "11" : 
																					   ((social.length > 9) ? "10" : 
																					   ((social.length > 8) ? "9" : 
																					   ((social.length > 7) ? "8" : 
																					   ((social.length > 6) ? "7" : 
																					   ((social.length > 5) ? "6" : 
																					   ((social.length > 4) ? "5" : 
																					   ((social.length > 3) ? "4" : 
																					   ((social.length > 2) ? "3" :
																					   ((social.length > 1) ? "2" :
																					   ((social.length > 0) ? "1" :"0")))))))))));
formFieldsPers6['signatureM3']                                                         = '';
}

formFields['intercalaireNombreM3']                                               		  = ((representants.length > 10) or (fondePouvoir.length > 5))? "6" : 
																							(((representants.length > 8) or (fondePouvoir.length > 4))? "5" : 
																							(((representants.length > 6) or (fondePouvoir.length > 3)) ? "4" : 
																							(((representants.length > 4) or $M2.dirigeant10.autrePersonneLieeEtablissement or (fondePouvoir.length > 2)) ? "3" : 
																							(((representants.length > 2) or $M2.dirigeant5.autrePersonneLieeEtablissement or (fondePouvoir.length > 1)) ? "2" :
																							(((representants.length > 0) or $M2.dirigeant1.autrePersonneLieeEtablissement or $M2.modifDissolutionGroup.modifDissolution.cadreIdentiteLiquidateur.autreDirigeant or (fondePouvoir.length > 0)) ? "1" :"0")))));
formFields['intercalaireNombreTNS']                                                  = (social.length > 11) ? "12" : 
																					   ((social.length > 10) ? "11" : 
																					   ((social.length > 9) ? "10" : 
																					   ((social.length > 8) ? "9" : 
																					   ((social.length > 7) ? "8" : 
																					   ((social.length > 6) ? "7" : 
																					   ((social.length > 5) ? "6" : 
																					   ((social.length > 4) ? "5" : 
																					   ((social.length > 3) ? "4" : 
																					   ((social.length > 2) ? "3" :
																					   ((social.length > 1) ? "2" :
																					   ((social.length > 0) ? "1" :"0")))))))))));






// Imprimé M3 Hors SARL 2

if ($M2.dirigeant5.autrePersonneLieeEtablissement or (representants.length > 2) or (fondePouvoir.length > 1)) {

// Cadre 1

formFieldsPers7['demandeModificationM3']                      = false;
formFieldsPers7['demandeModificationRM_M3']                   = identite.immatriculationRM ? true : false;
formFieldsPers7['demandeIntercalaireM3']                      = true;
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
formFieldsPers7['intercalaireNumM3']                          = "2";
} else {
formFieldsPers7['intercalaireNumM3']                          = "1";
} 
 
// Cadre 2

formFieldsPers7['entreprise_denominationM3']                              = identite.entrepriseDenomination;

// Cadre 3

formFieldsPers7['entreprise_sirenM3']                                     = identite.siren.split(' ').join('');
formFieldsPers7['immatRCS_M3']                                            = true;
formFieldsPers7['immatRCSGreffe_M3']                                      = identite.immatRCSGreffe;
formFieldsPers7['immatRM_M3']                                             = identite.immatriculationRM ? true : false;
formFieldsPers7['immatRMDept_M3']                                         = identite.immatriculationRM ? identite.immatRMCMA : '';
formFieldsPers7['immatRMDeptNum_M3']                                      = identite.immatriculationRM ? identite.immatRMCMA.getId() : '';
formFieldsPers7['entreprise_formeJuridiqueM3']                            = identite.entrepriseFormeJuridique.getLabel();
formFieldsPers7['entreprise_adresseEntreprisePM_voieM3']                  = (siege.siegeAdresseNumeroVoie != null ? siege.siegeAdresseNumeroVoie : '')
																		  + ' ' + (siege.siegeAdresseIndiceVoie != null ? siege.siegeAdresseIndiceVoie : '')
																		  + ' ' + (siege.siegeAdresseTypeVoie != null ? siege.siegeAdresseTypeVoie : '')
																		  + ' ' + siege.siegeAdresseNomVoie
																		  + ' ' + (siege.siegeAdresseComplementVoie != null ? siege.siegeAdresseComplementVoie : '')
																		  + ' ' + (siege.siegeAdresseDistriutionSpecialeVoie != null ? siege.siegeAdresseDistriutionSpecialeVoie : '');
formFieldsPers7['entreprise_adresseEntreprisePM_codePostalM3']           = siege.siegeAdresseCodePostal;
formFieldsPers7['entreprise_adresseEntreprisePM_communeM3']              = siege.siegeAdresseCommune;
formFieldsPers7['entreprise_adresseEntreprisePM_communeAncienneM3']      = siege.communeAncienneAdresseSiege != null ? siege.communeAncienneAdresseSiege : '';

// Dirigeant 6

// Cadre 4

if ($M2.dirigeant5.autrePersonneLieeEtablissement) {

if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['modifDate_dirigeantM3[0]']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['modifDate_dirigeantM3[0]']          = date;
} else if (dirigeant6.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(dirigeant6.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['modifDate_dirigeantM3[0]']          = date;
}
formFieldsPers7['dirigeant_nouveauM3[0]']                     = (ouverture.ajoutFondePouvoir or Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantNouveau')) ? true : false;
formFieldsPers7['dirigeant_partantM3[0]']                     = Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers7['dirigeant_modifM3[0]']                       = Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantModif') ? true : false;
formFieldsPers7['dirigeant_maintenuM3[0]']                    = Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantMaintenu') ? true : false;

if (not Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers7['dirigeant_maintenuAncienneQualiteM3[0]']     = Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantMaintenu') ? dirigeant6.personneLieeQualiteAncienne : '';
formFieldsPers7['dirigeant_qualiteM3[0]']                     = Value('id').of(dirigeant6.personneLieeQualite).eq('30') ? "Gérant" :
															(Value('id').of(dirigeant6.personneLieeQualite).eq('29') ? "Gérant et associé indéfiniment responsable" :
															(Value('id').of(dirigeant6.personneLieeQualite).eq('28') ? "Gérant et associé indéfiniment et solidairement responsable" :
															(Value('id').of(dirigeant6.personneLieeQualite).eq('75') ? "Associé indéfiniment responsable" :
															(Value('id').of(dirigeant6.personneLieeQualite).eq('74') ? "Associé indéfiniment et solidairement responsable" : dirigeant6.personneLieeQualite))));

if (Value('id').of(dirigeant6.personnaliteJuridique).eq('personnePhysique') or dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
formFieldsPers7['dirigeant_nomNaissanceM3[0]']                                                      = dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant ;
formFieldsPers7['dirigeant_nomUsageM3[0]']                                                          = dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers7['dirigeant_prenomM3[0]']                                                      = prenoms.toString();
} else if (dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers7['dirigeant_prenomM3[0]']                                                      = prenoms.toString();
}
if (dirigeant6.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant6.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['dirigeant_dateNaissanceM3[0]']          = date1;
}  else if (dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['dirigeant_dateNaissanceM3[0]']          = date2;
}
formFieldsPers7['dirigeant_lieuNaissanceM3[0]']                 = dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																(dirigeant6.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? 
																(dirigeant6.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : 
																(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
																(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers7['dirigeant_nationaliteM3[0]']                   = dirigeant6.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant6.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
}
if (Value('id').of(dirigeant6.personnaliteJuridique).eq('personneMorale')) {
formFieldsPers7['dirigeant_denoFormeJuridiqueM3[0]']                = dirigeant6.civilitePersonneMorale.personneLieePMDenomination + ' / ' +dirigeant6.civilitePersonneMorale.personneLieePMFormeJuridique;
formFieldsPers7['dirigeant_PM_immatriculationNumM3[0]']             = dirigeant6.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant6.civilitePersonneMorale.personneLieePMNumeroIdentification : (dirigeant6.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee != null ? (dirigeant6.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee + ' ' + dirigeant6.civilitePersonneMorale.personneLieePMNomNonImmatriculee) : '');
formFieldsPers7['dirigeant_PM_immatriculationLieuM3[0]']            = dirigeant6.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant6.civilitePersonneMorale.personneLieePMLieuImmatriculation : (dirigeant6.civilitePersonneMorale.personneLieePMLieuImmatriculation + ' / ' + dirigeant6.civilitePersonneMorale.personneLieePMPaysNonImmatriculee);
}
formFieldsPers7['dirigeant_adresseVoieM3[0]']                 = (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
															  + ' ' + (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant6.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
															  + ' ' + (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant6.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
															  + ' ' + dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNomVoie
															  + ' ' + (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant6.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
															  + ' ' + (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant6.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers7['dirigeant_adresseCPM3[0]']         		  = dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers7['dirigeant_adresseCommuneM3[0]']              = dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseVille != null ? (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays) : '');
}
if (Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers7['dirigeant_partantBisM3[0]']                        = true;
formFieldsPers7['dirigeantPartant_nomNaissanceM3[0]']               = dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : '') ;
formFieldsPers7['dirigeantPartant_nomUsageM3[0]']                   = dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers7['dirigeantPartant_prenomM3[0]']                                                      = prenoms.toString();
} else if (dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers7['dirigeantPartant_prenomM3[0]']                                                      = prenoms.toString();
}
formFieldsPers7['dirigeantPartant_denoFormeJuridique[0]']           = dirigeant6.civilitePersonneMorale.personneLieePMDenomination != null ? (dirigeant6.civilitePersonneMorale.personneLieePMDenomination + ' / ' +dirigeant6.civilitePersonneMorale.personneLieePMFormeJuridique) : '';
}

var nirDeclarant = dirigeant6.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers7['dirigeantPartant_numSSM3[0]']                = nirDeclarant.substring(0, 13);
    formFieldsPers7['dirigeantPartant_numSSCleM3[0]']             = nirDeclarant.substring(13, 15);
}
var nirDeclarant2 = dirigeant6.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant2 != null) {
    nirDeclarant2 = nirDeclarant2.replace(/ /g, "");
    formFieldsPers7['dirigeantPartant_numSSM3[0]']                = nirDeclarant2.substring(0, 13);
    formFieldsPers7['dirigeantPartant_numSSCleM3[0]']             = nirDeclarant2.substring(13, 15);
}
}

// Dirigeant 7

// Cadre 5

if ($M2.dirigeant6.autrePersonneLieeEtablissement) {
if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['modifDate_dirigeantM3[1]']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['modifDate_dirigeantM3[1]']          = date;
} else if (dirigeant7.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(dirigeant7.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['modifDate_dirigeantM3[1]']          = date;
}
formFieldsPers7['dirigeant_nouveauM3[1]']                     = Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantNouveau') ? true : false;
formFieldsPers7['dirigeant_partantM3[1]']                     = Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers7['dirigeant_modifM3[1]']                       = Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantModif') ? true : false;
formFieldsPers7['dirigeant_maintenuM3[1]']                    = Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantMaintenu') ? true : false;

if (not Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers7['dirigeant_maintenuAncienneQualiteM3[1]']     = Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantMaintenu') ? dirigeant7.personneLieeQualiteAncienne : '';
formFieldsPers7['dirigeant_qualiteM3[1]']                     = Value('id').of(dirigeant7.personneLieeQualite).eq('30') ? "Gérant" :
															(Value('id').of(dirigeant7.personneLieeQualite).eq('29') ? "Gérant et associé indéfiniment responsable" :
															(Value('id').of(dirigeant7.personneLieeQualite).eq('28') ? "Gérant et associé indéfiniment et solidairement responsable" :
															(Value('id').of(dirigeant7.personneLieeQualite).eq('75') ? "Associé indéfiniment responsable" :
															(Value('id').of(dirigeant7.personneLieeQualite).eq('74') ? "Associé indéfiniment et solidairement responsable" : dirigeant7.personneLieeQualite))));

if (Value('id').of(dirigeant7.personnaliteJuridique).eq('personnePhysique') or dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
formFieldsPers7['dirigeant_nomNaissanceM3[1]']                                                      = dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant ;
formFieldsPers7['dirigeant_nomUsageM3[1]']                                                          = dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers7['dirigeant_prenomM3[1]']                                                      = prenoms.toString();
} else if (dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers7['dirigeant_prenomM3[1]']                                                      = prenoms.toString();
}
if (dirigeant7.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant7.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['dirigeant_dateNaissanceM3[1]']          = date1;
}  else if (dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['dirigeant_dateNaissanceM3[1]']          = date2;
}
formFieldsPers7['dirigeant_lieuNaissanceM3[1]']                 = dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																(dirigeant7.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? 
																(dirigeant7.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : 
																(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
																(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers7['dirigeant_nationaliteM3[1]']                   = dirigeant7.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant7.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
}
if (Value('id').of(dirigeant7.personnaliteJuridique).eq('personneMorale')) {
formFieldsPers7['dirigeant_denoFormeJuridiqueM3[1]']                = dirigeant7.civilitePersonneMorale.personneLieePMDenomination + ' / ' + dirigeant7.civilitePersonneMorale.personneLieePMFormeJuridique;
formFieldsPers7['dirigeant_PM_immatriculationNumM3[1]']             = dirigeant7.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant7.civilitePersonneMorale.personneLieePMNumeroIdentification : (dirigeant7.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee != null ? (dirigeant7.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee + ' ' + dirigeant7.civilitePersonneMorale.personneLieePMNomNonImmatriculee) : '');
formFieldsPers7['dirigeant_PM_immatriculationLieuM3[1]']            = dirigeant7.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant7.civilitePersonneMorale.personneLieePMLieuImmatriculation : (dirigeant7.civilitePersonneMorale.personneLieePMLieuImmatriculation + ' / ' + dirigeant7.civilitePersonneMorale.personneLieePMPaysNonImmatriculee);
}
formFieldsPers7['dirigeant_adresseVoieM3[1]']                 = (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
															  + ' ' + (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant7.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
															  + ' ' + (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant7.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
															  + ' ' + dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNomVoie
															  + ' ' + (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant7.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
															  + ' ' + (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant7.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers7['dirigeant_adresseCPM3[1]']         		  = dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers7['dirigeant_adresseCommuneM3[1]']              = dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseVille != null ? (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays) : '');
}
if (Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers7['dirigeant_partantBisM3[1]']                        = true;
formFieldsPers7['dirigeantPartant_nomNaissanceM3[1]']               = dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : '') ;
formFieldsPers7['dirigeantPartant_nomUsageM3[1]']                   = dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers7['dirigeantPartant_prenomM3[1]']                                                      = prenoms.toString();
} else if (dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers7['dirigeantPartant_prenomM3[1]']                                                      = prenoms.toString();
}
formFieldsPers7['dirigeantPartant_denoFormeJuridique[1]']           = dirigeant7.civilitePersonneMorale.personneLieePMDenomination != null ? (dirigeant7.civilitePersonneMorale.personneLieePMDenomination + ' / ' + dirigeant7.civilitePersonneMorale.personneLieePMFormeJuridique) : '';
}

var nirDeclarant = dirigeant7.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers7['dirigeantPartant_numSSM3[1]']                = nirDeclarant.substring(0, 13);
    formFieldsPers7['dirigeantPartant_numSSCleM3[1]']             = nirDeclarant.substring(13, 15);
}
var nirDeclarant2 = dirigeant7.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant2 != null) {
    nirDeclarant2 = nirDeclarant2.replace(/ /g, "");
    formFieldsPers7['dirigeantPartant_numSSM3[1]']                = nirDeclarant2.substring(0, 13);
    formFieldsPers7['dirigeantPartant_numSSCleM3[1]']             = nirDeclarant2.substring(13, 15);
}
}

// Dirigeant 8

// Cadre 6

if ($M2.dirigeant7.autrePersonneLieeEtablissement) {
if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['modifDate_dirigeantM3[2]']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['modifDate_dirigeantM3[2]']          = date;
} else if (dirigeant8.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(dirigeant8.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['modifDate_dirigeantM3[2]']          = date;
}
formFieldsPers7['dirigeant_nouveauM3[2]']                     = Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantNouveau') ? true : false;
formFieldsPers7['dirigeant_partantM3[2]']                     = Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers7['dirigeant_modifM3[2]']                       = Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantModif') ? true : false;
formFieldsPers7['dirigeant_maintenuM3[2]']                    = Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantMaintenu') ? true : false;

if (not Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers7['dirigeant_maintenuAncienneQualiteM3[2]']     = Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantMaintenu') ? dirigeant8.personneLieeQualiteAncienne : '';
formFieldsPers7['dirigeant_qualiteM3[2]']                     = Value('id').of(dirigeant8.personneLieeQualite).eq('30') ? "Gérant" :
															(Value('id').of(dirigeant8.personneLieeQualite).eq('29') ? "Gérant et associé indéfiniment responsable" :
															(Value('id').of(dirigeant8.personneLieeQualite).eq('28') ? "Gérant et associé indéfiniment et solidairement responsable" :
															(Value('id').of(dirigeant8.personneLieeQualite).eq('75') ? "Associé indéfiniment responsable" :
															(Value('id').of(dirigeant8.personneLieeQualite).eq('74') ? "Associé indéfiniment et solidairement responsable" : dirigeant8.personneLieeQualite))));

if (Value('id').of(dirigeant8.personnaliteJuridique).eq('personnePhysique') or dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
formFieldsPers7['dirigeant_nomNaissanceM3[2]']                                                      = dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant ;
formFieldsPers7['dirigeant_nomUsageM3[2]']                                                          = dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers7['dirigeant_prenomM3[2]']                                                      = prenoms.toString();
} else if (dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers7['dirigeant_prenomM3[2]']                                                      = prenoms.toString();
}
if (dirigeant8.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant8.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['dirigeant_dateNaissanceM3[2]']          = date1;
}  else if (dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['dirigeant_dateNaissanceM3[2]']          = date2;
}
formFieldsPers7['dirigeant_lieuNaissanceM3[2]']                 = dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																(dirigeant8.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? 
																(dirigeant8.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : 
																(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
																(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers7['dirigeant_nationaliteM3[2]']                   = dirigeant8.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant8.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
}
if (Value('id').of(dirigeant8.personnaliteJuridique).eq('personneMorale')) {
formFieldsPers7['dirigeant_denoFormeJuridiqueM3[2]']                = dirigeant8.civilitePersonneMorale.personneLieePMDenomination + ' / ' + dirigeant8.civilitePersonneMorale.personneLieePMFormeJuridique;
formFieldsPers7['dirigeant_PM_immatriculationNumM3[2]']             = dirigeant8.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant8.civilitePersonneMorale.personneLieePMNumeroIdentification : (dirigeant8.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee != null ? (dirigeant8.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee + ' ' + dirigeant8.civilitePersonneMorale.personneLieePMNomNonImmatriculee) : '');
formFieldsPers7['dirigeant_PM_immatriculationLieuM3[2]']            = dirigeant8.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant8.civilitePersonneMorale.personneLieePMLieuImmatriculation : (dirigeant8.civilitePersonneMorale.personneLieePMLieuImmatriculation + ' / ' + dirigeant8.civilitePersonneMorale.personneLieePMPaysNonImmatriculee);
}
formFieldsPers7['dirigeant_adresseVoieM3[2]']                 = (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
															  + ' ' + (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant8.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
															  + ' ' + (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant8.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
															  + ' ' + dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNomVoie
															  + ' ' + (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant8.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
															  + ' ' + (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant8.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers7['dirigeant_adresseCPM3[2]']         		  = dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers7['dirigeant_adresseCommuneM3[2]']              = dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseVille != null ? (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays) : '');
}
if (Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers7['dirigeant_partantBisM3[2]']                        = true;
formFieldsPers7['dirigeantPartant_nomNaissanceM3[2]']               = dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : '') ;
formFieldsPers7['dirigeantPartant_nomUsageM3[2]']                   = dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers7['dirigeantPartant_prenomM3[2]']                                                      = prenoms.toString();
} else if (dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers7['dirigeantPartant_prenomM3[2]']                                                      = prenoms.toString();
}
formFieldsPers7['dirigeantPartant_denoFormeJuridique[2]']           = dirigeant8.civilitePersonneMorale.personneLieePMDenomination != null ? (dirigeant8.civilitePersonneMorale.personneLieePMDenomination + ' / ' + dirigeant8.civilitePersonneMorale.personneLieePMFormeJuridique) : '';
}

var nirDeclarant = dirigeant8.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers7['dirigeantPartant_numSSM3[2]']                = nirDeclarant.substring(0, 13);
    formFieldsPers7['dirigeantPartant_numSSCleM3[2]']             = nirDeclarant.substring(13, 15);
}
var nirDeclarant2 = dirigeant8.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant2 != null) {
    nirDeclarant2 = nirDeclarant2.replace(/ /g, "");
    formFieldsPers7['dirigeantPartant_numSSM3[2]']                = nirDeclarant2.substring(0, 13);
    formFieldsPers7['dirigeantPartant_numSSCleM3[2]']             = nirDeclarant2.substring(13, 15);
}
}

// Dirigeant 9

// Cadre 7

if ($M2.dirigeant8.autrePersonneLieeEtablissement) {
if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['modifDate_dirigeantM3[3]']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['modifDate_dirigeantM3[3]']          = date;
} else if (dirigeant9.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(dirigeant9.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['modifDate_dirigeantM3[3]']          = date;
}
formFieldsPers7['dirigeant_nouveauM3[3]']                     = Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantNouveau') ? true : false;
formFieldsPers7['dirigeant_partantM3[3]']                     = Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers7['dirigeant_modifM3[3]']                       = Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantModif') ? true : false;
formFieldsPers7['dirigeant_maintenuM3[3]']                    = Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantMaintenu') ? true : false;

if (not Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers7['dirigeant_maintenuAncienneQualiteM3[3]']     = Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantMaintenu') ? dirigeant9.personneLieeQualiteAncienne : '';
formFieldsPers7['dirigeant_qualiteM3[3]']                     = Value('id').of(dirigeant9.personneLieeQualite).eq('30') ? "Gérant" :
															(Value('id').of(dirigeant9.personneLieeQualite).eq('29') ? "Gérant et associé indéfiniment responsable" :
															(Value('id').of(dirigeant9.personneLieeQualite).eq('28') ? "Gérant et associé indéfiniment et solidairement responsable" :
															(Value('id').of(dirigeant9.personneLieeQualite).eq('75') ? "Associé indéfiniment responsable" :
															(Value('id').of(dirigeant9.personneLieeQualite).eq('74') ? "Associé indéfiniment et solidairement responsable" : dirigeant9.personneLieeQualite))));

if (Value('id').of(dirigeant9.personnaliteJuridique).eq('personnePhysique') or dirigeant9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
formFieldsPers7['dirigeant_nomNaissanceM3[3]']                                                      = dirigeant9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant ;
formFieldsPers7['dirigeant_nomUsageM3[3]']                                                          = dirigeant9.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant9.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers7['dirigeant_prenomM3[3]']                                                      = prenoms.toString();
} else if (dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers7['dirigeant_prenomM3[3]']                                                      = prenoms.toString();
}
if (dirigeant9.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant9.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['dirigeant_dateNaissanceM3[3]']          = date1;
}  else if (dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['dirigeant_dateNaissanceM3[3]']          = date2;
}
formFieldsPers7['dirigeant_lieuNaissanceM3[3]']                 = dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																(dirigeant9.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? 
																(dirigeant9.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : 
																(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
																(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers7['dirigeant_nationaliteM3[3]']                   = dirigeant9.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant9.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
}
if (Value('id').of(dirigeant9.personnaliteJuridique).eq('personneMorale')) {
formFieldsPers7['dirigeant_denoFormeJuridiqueM3[3]']                = dirigeant9.civilitePersonneMorale.personneLieePMDenomination + ' / ' + dirigeant9.civilitePersonneMorale.personneLieePMFormeJuridique;
formFieldsPers7['dirigeant_PM_immatriculationNumM3[3]']             = dirigeant9.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant9.civilitePersonneMorale.personneLieePMNumeroIdentification : (dirigeant9.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee != null ? (dirigeant9.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee + ' ' + dirigeant9.civilitePersonneMorale.personneLieePMNomNonImmatriculee) : '');
formFieldsPers7['dirigeant_PM_immatriculationLieuM3[3]']            = dirigeant9.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant9.civilitePersonneMorale.personneLieePMLieuImmatriculation : (dirigeant9.civilitePersonneMorale.personneLieePMLieuImmatriculation + ' / ' + dirigeant9.civilitePersonneMorale.personneLieePMPaysNonImmatriculee);
}
formFieldsPers7['dirigeant_adresseVoieM3[3]']                 = (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant9.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
															  + ' ' + (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant9.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
															  + ' ' + (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant9.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
															  + ' ' + dirigeant9.cadreAdresseDirigeant.dirigeantAdresseNomVoie
															  + ' ' + (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant9.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
															  + ' ' + (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant9.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers7['dirigeant_adresseCPM3[3]']         		  = dirigeant9.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant9.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers7['dirigeant_adresseCommuneM3[3]']              = dirigeant9.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant9.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseVille != null ? (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays) : '');
}
if (Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers7['dirigeant_partantBisM3[3]']                        = true;
formFieldsPers7['dirigeantPartant_nomNaissanceM3[3]']               = dirigeant9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : '') ;
formFieldsPers7['dirigeantPartant_nomUsageM3[3]']                   = dirigeant9.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant9.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers7['dirigeantPartant_prenomM3[3]']                                                      = prenoms.toString();
} else if (dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers7['dirigeantPartant_prenomM3[3]']                                                      = prenoms.toString();
}
formFieldsPers7['dirigeantPartant_denoFormeJuridique[3]']           = dirigeant9.civilitePersonneMorale.personneLieePMDenomination != null ? (dirigeant9.civilitePersonneMorale.personneLieePMDenomination + ' / ' + dirigeant9.civilitePersonneMorale.personneLieePMFormeJuridique) : '';
}

var nirDeclarant = dirigeant9.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers7['dirigeantPartant_numSSM3[3]']                = nirDeclarant.substring(0, 13);
    formFieldsPers7['dirigeantPartant_numSSCleM3[3]']             = nirDeclarant.substring(13, 15);
}
var nirDeclarant2 = dirigeant9.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant2 != null) {
    nirDeclarant2 = nirDeclarant2.replace(/ /g, "");
    formFieldsPers7['dirigeantPartant_numSSM3[3]']                = nirDeclarant2.substring(0, 13);
    formFieldsPers7['dirigeantPartant_numSSCleM3[3]']             = nirDeclarant2.substring(13, 15);
}
}

// Dirigeant 10

// Cadre 8

if ($M2.dirigeant9.autrePersonneLieeEtablissement) {
if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['modifDate_dirigeantM3[4]']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['modifDate_dirigeantM3[4]']          = date;
} else if (dirigeant10.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(dirigeant10.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['modifDate_dirigeantM3[4]']          = date;
}
formFieldsPers7['dirigeant_nouveauM3[4]']                     = Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantNouveau') ? true : false;
formFieldsPers7['dirigeant_partantM3[4]']                     = Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers7['dirigeant_modifM3[4]']                       = Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantModif') ? true : false;
formFieldsPers7['dirigeant_maintenuM3[4]']                    = Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantMaintenu') ? true : false;

if (not Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers7['dirigeant_maintenuAncienneQualiteM3[4]']     = Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantMaintenu') ? dirigeant10.personneLieeQualiteAncienne : '';
formFieldsPers7['dirigeant_qualiteM3[4]']                     = Value('id').of(dirigeant10.personneLieeQualite).eq('30') ? "Gérant" :
															(Value('id').of(dirigeant10.personneLieeQualite).eq('29') ? "Gérant et associé indéfiniment responsable" :
															(Value('id').of(dirigeant10.personneLieeQualite).eq('28') ? "Gérant et associé indéfiniment et solidairement responsable" :
															(Value('id').of(dirigeant10.personneLieeQualite).eq('75') ? "Associé indéfiniment responsable" :
															(Value('id').of(dirigeant10.personneLieeQualite).eq('74') ? "Associé indéfiniment et solidairement responsable" : dirigeant10.personneLieeQualite))));

if (Value('id').of(dirigeant10.personnaliteJuridique).eq('personnePhysique') or dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
formFieldsPers7['dirigeant_nomNaissanceM3[4]']                                                      = dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant ;
formFieldsPers7['dirigeant_nomUsageM3[4]']                                                          = dirigeant10.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant10.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers7['dirigeant_prenomM3[4]']                                                      = prenoms.toString();
} else if (dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers7['dirigeant_prenomM3[4]']                                                      = prenoms.toString();
}
if (dirigeant10.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant10.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['dirigeant_dateNaissanceM3[4]']          = date1;
}  else if (dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['dirigeant_dateNaissanceM3[4]']          = date2;
}
formFieldsPers7['dirigeant_lieuNaissanceM3[4]']                 = dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																(dirigeant10.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? 
																(dirigeant10.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : 
																(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
																(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers7['dirigeant_nationaliteM3[4]']                   = dirigeant10.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant10.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
}
if (Value('id').of(dirigeant10.personnaliteJuridique).eq('personneMorale')) {
formFieldsPers7['dirigeant_denoFormeJuridiqueM3[4]']                = dirigeant10.civilitePersonneMorale.personneLieePMDenomination + ' / ' + dirigeant10.civilitePersonneMorale.personneLieePMFormeJuridique;
formFieldsPers7['dirigeant_PM_immatriculationNumM3[4]']             = dirigeant10.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant10.civilitePersonneMorale.personneLieePMNumeroIdentification : (dirigeant10.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee != null ? (dirigeant10.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee + ' ' + dirigeant10.civilitePersonneMorale.personneLieePMNomNonImmatriculee) : '');
formFieldsPers7['dirigeant_PM_immatriculationLieuM3[4]']            = dirigeant10.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant10.civilitePersonneMorale.personneLieePMLieuImmatriculation : (dirigeant10.civilitePersonneMorale.personneLieePMLieuImmatriculation + ' / ' + dirigeant10.civilitePersonneMorale.personneLieePMPaysNonImmatriculee);
}
formFieldsPers7['dirigeant_adresseVoieM3[4]']                 = (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant10.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
															  + ' ' + (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant10.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
															  + ' ' + (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant10.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
															  + ' ' + dirigeant10.cadreAdresseDirigeant.dirigeantAdresseNomVoie
															  + ' ' + (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant10.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
															  + ' ' + (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant10.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers7['dirigeant_adresseCPM3[4]']         		  = dirigeant10.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant10.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers7['dirigeant_adresseCommuneM3[4]']              = dirigeant10.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant10.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseVille != null ? (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays) : '');
}
if (Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers7['dirigeant_partantBisM3[4]']                        = true;
formFieldsPers7['dirigeantPartant_nomNaissanceM3[4]']               = dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : '') ;
formFieldsPers7['dirigeantPartant_nomUsageM3[4]']                   = dirigeant10.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant10.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers7['dirigeantPartant_prenomM3[4]']                                                      = prenoms.toString();
} else if (dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers7['dirigeantPartant_prenomM3[4]']                                                      = prenoms.toString();
}
formFieldsPers7['dirigeantPartant_denoFormeJuridique[4]']           = dirigeant10.civilitePersonneMorale.personneLieePMDenomination != null ? (dirigeant10.civilitePersonneMorale.personneLieePMDenomination + ' / ' + dirigeant10.civilitePersonneMorale.personneLieePMFormeJuridique) : '';
}

var nirDeclarant = dirigeant10.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers7['dirigeantPartant_numSSM3[4]']                = nirDeclarant.substring(0, 13);
    formFieldsPers7['dirigeantPartant_numSSCleM3[4]']             = nirDeclarant.substring(13, 15);
}
var nirDeclarant2 = dirigeant10.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant2 != null) {
    nirDeclarant2 = nirDeclarant2.replace(/ /g, "");
    formFieldsPers7['dirigeantPartant_numSSM3[4]']                = nirDeclarant2.substring(0, 13);
    formFieldsPers7['dirigeantPartant_numSSCleM3[4]']             = nirDeclarant2.substring(13, 15);
}
}

// Représentant permanent 3

// Cadre 9


if (representants.length > 2) {
formFieldsPers7['representantPermanent_numeroCadre[0]']          = representants[2]['cadre'];
formFieldsPers7['representantPermanent_duM2[0]']                 = false;
formFieldsPers7['representantPermanent_duM3[0]']                 = true;
if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['representantPermanent_dateModif[0]']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['representantPermanent_dateModif[0]']          = date;
} else if (representants[2]['dirigeant'].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(representants[2]['dirigeant'].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['representantPermanent_dateModif[0]']          = date;
}
formFieldsPers7['representantPermanent_nouveau[0]']              = (Value('id').of(representants[2]['dirigeant'].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau') or Value('id').of(representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantNouveau')) ? true : false ;
formFieldsPers7['representantPermanent_partant[0]']              = Value('id').of(representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers7['representantPermanent_modif[0]']                = Value('id').of(representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif') ? true : false;
formFieldsPers7['representantPermanent_nomNaissance[0]']         = representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomNaissance;
formFieldsPers7['representantPermanent_nomUsage[0]']             = representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage != null ? representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage : '';
if (representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom != null) {
var prenoms=[];
for ( i = 0; i < representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom[i]);}                            
formFieldsPers7['representantPermanent_prenom[0]']                                                      = prenoms.toString();
}
formFieldsPers7['representantPermanent_nationalite[0]']          = representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNationalite;
if (representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['representantPermanent_dateNaissance[0]']          = date1;
}
formFieldsPers7['representantPermanent_lieuNaissanceCommune[0]'] = representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune != null ? 
																	(representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune + ' ' +  "(" + '' + representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceDepartement.getId() +''+ ")") :
																	(representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissancePays);
formFieldsPers7['representantPermanent_adresseVoie[0]']          = (representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null ? representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie : '')
																	+ ' ' + (representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null ? representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie : '')
																	+ ' ' + (representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null ? representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie : '')
																	+ ' ' + representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNomVoie
																	+ ' ' + (representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null ? representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie : '')
																	+ ' ' + (representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null ? representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale : '');
formFieldsPers7['representantPermanent_adresseCP[0]']            = representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
formFieldsPers7['representantPermanent_adresseCommune[0]']       = representants[2]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune;
}

// Représentant permanent 4

// Cadre 10

if (representants.length > 3) {
formFieldsPers7['representantPermanent_numeroCadre[1]']          = representants[3]['cadre'];
formFieldsPers7['representantPermanent_duM2[1]']                 = false;
formFieldsPers7['representantPermanent_duM3[1]']                 = true;
if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['representantPermanent_dateModif[1]']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['representantPermanent_dateModif[1]']          = date;
} else if (representants[3]['dirigeant'].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(representants[3]['dirigeant'].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['representantPermanent_dateModif[1]']          = date;
}
formFieldsPers7['representantPermanent_nouveau[1]']              = (Value('id').of(representants[3]['dirigeant'].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau') or Value('id').of(representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantNouveau')) ? true : false ;
formFieldsPers7['representantPermanent_partant[1]']              = Value('id').of(representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers7['representantPermanent_modif[1]']                = Value('id').of(representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif') ? true : false;
formFieldsPers7['representantPermanent_nomNaissance[1]']         = representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomNaissance;
formFieldsPers7['representantPermanent_nomUsage[1]']             = representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage != null ? representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage : '';
if (representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom != null) {
var prenoms=[];
for ( i = 0; i < representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom[i]);}                            
formFieldsPers7['representantPermanent_prenom[1]']                                                      = prenoms.toString();
}
formFieldsPers7['representantPermanent_nationalite[1]']          = representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNationalite;
if (representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['representantPermanent_dateNaissance[1]']          = date1;
}
formFieldsPers7['representantPermanent_lieuNaissanceCommune[1]'] = representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune != null ? 
																	(representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune + ' ' +  "(" + '' + representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceDepartement.getId() +''+ ")") :
																	(representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissancePays);
formFieldsPers7['representantPermanent_adresseVoie[1]']          = (representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null ? representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie : '')
																	+ ' ' + (representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null ? representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie : '')
																	+ ' ' + (representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null ? representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie : '')
																	+ ' ' + representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNomVoie
																	+ ' ' + (representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null ? representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie : '')
																	+ ' ' + (representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null ? representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale : '');
formFieldsPers7['representantPermanent_adresseCP[1]']            = representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
formFieldsPers7['representantPermanent_adresseCommune[1]']       = representants[3]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune;
}

// Cadre 11 Précision fondé de pouvoir 2

if ((fondePouvoir.length > 1) and fondePouvoir[1].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
formFieldsPers7['fondePouvoir_declareCadre']                        = fondePouvoir[1]['cadres'];
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['fondePouvoir_dateModifM3[0]']          = date;
} else if (fondePouvoir[1].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(fondePouvoir[1].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['fondePouvoir_dateModifM3[0]']          = date;
}
formFieldsPers7['fondePouvoir_etablissementAdresseVoieM3']    = (fondePouvoir[1].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNumeroVoie != null ? fondePouvoir[1].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNumeroVoie : '')
																+ ' ' + (fondePouvoir[1].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseIndiceVoie != null ? fondePouvoir[1].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseIndiceVoie : '')
																+ ' ' + (fondePouvoir[1].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseTypeVoie != null ? fondePouvoir[1].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseTypeVoie : '')
																+ ' ' + fondePouvoir[1].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNomVoie
																+ ' ' + (fondePouvoir[1].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseComplementAdresse != null ? fondePouvoir[1].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseComplementAdresse : '')
																+ ' ' + (fondePouvoir[1].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseDistriutionSpecialeVoie != null ? fondePouvoir[1].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseDistriutionSpecialeVoie : '');
formFieldsPers7['fondePouvoir_etablissementAdresseCPM3']      = fondePouvoir[1].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseCodePostal;
formFieldsPers7['fondePouvoir_etablissementAdresseCommuneM3'] = fondePouvoir[1].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseCommune;
}
}

// Imprimé M3 Hors SARL 3

if ($M2.dirigeant10.autrePersonneLieeEtablissement or (representants.length > 4) or (fondePouvoir.length > 2)) {

// Cadre 1

formFieldsPers8['demandeModificationM3']                      = false;
formFieldsPers8['demandeModificationRM_M3']                   = identite.immatriculationRM ? true : false;
formFieldsPers8['demandeIntercalaireM3']                      = true;
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
formFieldsPers8['intercalaireNumM3']                          = "3";
} else {
formFieldsPers8['intercalaireNumM3']                          = "2";
} 
 
// Cadre 2

formFieldsPers8['entreprise_denominationM3']                              = identite.entrepriseDenomination;

// Cadre 3

formFieldsPers8['entreprise_sirenM3']                                     = identite.siren.split(' ').join('');
formFieldsPers8['immatRCS_M3']                                            = true;
formFieldsPers8['immatRCSGreffe_M3']                                      = identite.immatRCSGreffe;
formFieldsPers8['immatRM_M3']                                             = identite.immatriculationRM ? true : false;
formFieldsPers8['immatRMDept_M3']                                         = identite.immatriculationRM ? identite.immatRMCMA : '';
formFieldsPers8['immatRMDeptNum_M3']                                      = identite.immatriculationRM ? identite.immatRMCMA.getId() : '';
formFieldsPers8['entreprise_formeJuridiqueM3']                            = identite.entrepriseFormeJuridique.getLabel();
formFieldsPers8['entreprise_adresseEntreprisePM_voieM3']                  = (siege.siegeAdresseNumeroVoie != null ? siege.siegeAdresseNumeroVoie : '')
																		  + ' ' + (siege.siegeAdresseIndiceVoie != null ? siege.siegeAdresseIndiceVoie : '')
																		  + ' ' + (siege.siegeAdresseTypeVoie != null ? siege.siegeAdresseTypeVoie : '')
																		  + ' ' + siege.siegeAdresseNomVoie
																		  + ' ' + (siege.siegeAdresseComplementVoie != null ? siege.siegeAdresseComplementVoie : '')
																		  + ' ' + (siege.siegeAdresseDistriutionSpecialeVoie != null ? siege.siegeAdresseDistriutionSpecialeVoie : '');
formFieldsPers8['entreprise_adresseEntreprisePM_codePostalM3']           = siege.siegeAdresseCodePostal;
formFieldsPers8['entreprise_adresseEntreprisePM_communeM3']              = siege.siegeAdresseCommune;
formFieldsPers8['entreprise_adresseEntreprisePM_communeAncienneM3']      = siege.communeAncienneAdresseSiege != null ? siege.communeAncienneAdresseSiege : '';

// Dirigeant 11

// Cadre 4

if ($M2.dirigeant10.autrePersonneLieeEtablissement) {

if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers8['modifDate_dirigeantM3[0]']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers8['modifDate_dirigeantM3[0]']          = date;
} else if (dirigeant11.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(dirigeant11.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers8['modifDate_dirigeantM3[0]']          = date;
}
formFieldsPers8['dirigeant_nouveauM3[0]']                     = (ouverture.ajoutFondePouvoir or Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantNouveau')) ? true : false;
formFieldsPers8['dirigeant_partantM3[0]']                     = Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers8['dirigeant_modifM3[0]']                       = Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantModif') ? true : false;
formFieldsPers8['dirigeant_maintenuM3[0]']                    = Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantMaintenu') ? true : false;

if (not Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers8['dirigeant_maintenuAncienneQualiteM3[0]']     = Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantMaintenu') ? dirigeant11.personneLieeQualiteAncienne : '';
formFieldsPers8['dirigeant_qualiteM3[0]']                     = Value('id').of(dirigeant11.personneLieeQualite).eq('30') ? "Gérant" :
															(Value('id').of(dirigeant11.personneLieeQualite).eq('29') ? "Gérant et associé indéfiniment responsable" :
															(Value('id').of(dirigeant11.personneLieeQualite).eq('28') ? "Gérant et associé indéfiniment et solidairement responsable" :
															(Value('id').of(dirigeant11.personneLieeQualite).eq('75') ? "Associé indéfiniment responsable" :
															(Value('id').of(dirigeant11.personneLieeQualite).eq('74') ? "Associé indéfiniment et solidairement responsable" : dirigeant11.personneLieeQualite))));

if (Value('id').of(dirigeant11.personnaliteJuridique).eq('personnePhysique') or dirigeant11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
formFieldsPers8['dirigeant_nomNaissanceM3[0]']                                                      = dirigeant11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant ;
formFieldsPers8['dirigeant_nomUsageM3[0]']                                                          = dirigeant11.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant11.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers8['dirigeant_prenomM3[0]']                                                      = prenoms.toString();
} else if (dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers8['dirigeant_prenomM3[0]']                                                      = prenoms.toString();
}
if (dirigeant11.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant11.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers8['dirigeant_dateNaissanceM3[0]']          = date1;
}  else if (dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers8['dirigeant_dateNaissanceM3[0]']          = date2;
}
formFieldsPers8['dirigeant_lieuNaissanceM3[0]']                 = dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																(dirigeant11.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? 
																(dirigeant11.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : 
																(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
																(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers8['dirigeant_nationaliteM3[0]']                   = dirigeant11.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant11.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
}
if (Value('id').of(dirigeant11.personnaliteJuridique).eq('personneMorale')) {
formFieldsPers8['dirigeant_denoFormeJuridiqueM3[0]']                = dirigeant11.civilitePersonneMorale.personneLieePMDenomination + ' / ' +dirigeant11.civilitePersonneMorale.personneLieePMFormeJuridique;
formFieldsPers8['dirigeant_PM_immatriculationNumM3[0]']             = dirigeant11.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant11.civilitePersonneMorale.personneLieePMNumeroIdentification : (dirigeant11.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee != null ? (dirigeant11.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee + ' ' + dirigeant11.civilitePersonneMorale.personneLieePMNomNonImmatriculee) : '');
formFieldsPers8['dirigeant_PM_immatriculationLieuM3[0]']            = dirigeant11.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant11.civilitePersonneMorale.personneLieePMLieuImmatriculation : (dirigeant11.civilitePersonneMorale.personneLieePMLieuImmatriculation + ' / ' + dirigeant11.civilitePersonneMorale.personneLieePMPaysNonImmatriculee);
}
formFieldsPers8['dirigeant_adresseVoieM3[0]']                 = (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant11.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
															  + ' ' + (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant11.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
															  + ' ' + (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant11.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
															  + ' ' + dirigeant11.cadreAdresseDirigeant.dirigeantAdresseNomVoie
															  + ' ' + (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant11.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
															  + ' ' + (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant11.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers8['dirigeant_adresseCPM3[0]']         		  = dirigeant11.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant11.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers8['dirigeant_adresseCommuneM3[0]']              = dirigeant11.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant11.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseVille != null ? (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays) : '');
}
if (Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers8['dirigeant_partantBisM3[0]']                        = true;
formFieldsPers8['dirigeantPartant_nomNaissanceM3[0]']               = dirigeant11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : '') ;
formFieldsPers8['dirigeantPartant_nomUsageM3[0]']                   = dirigeant11.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant11.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers8['dirigeantPartant_prenomM3[0]']                                                      = prenoms.toString();
} else if (dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers8['dirigeantPartant_prenomM3[0]']                                                      = prenoms.toString();
}
formFieldsPers8['dirigeantPartant_denoFormeJuridique[0]']           = dirigeant11.civilitePersonneMorale.personneLieePMDenomination != null ? (dirigeant11.civilitePersonneMorale.personneLieePMDenomination + ' / ' +dirigeant11.civilitePersonneMorale.personneLieePMFormeJuridique) : '';
}

var nirDeclarant = dirigeant11.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers8['dirigeantPartant_numSSM3[0]']                = nirDeclarant.substring(0, 13);
    formFieldsPers8['dirigeantPartant_numSSCleM3[0]']             = nirDeclarant.substring(13, 15);
}
var nirDeclarant2 = dirigeant11.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant2 != null) {
    nirDeclarant2 = nirDeclarant2.replace(/ /g, "");
    formFieldsPers8['dirigeantPartant_numSSM3[0]']                = nirDeclarant2.substring(0, 13);
    formFieldsPers8['dirigeantPartant_numSSCleM3[0]']             = nirDeclarant2.substring(13, 15);
}
}

// Dirigeant 12

// Cadre 5

if ($M2.dirigeant11.autrePersonneLieeEtablissement) {
if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers8['modifDate_dirigeantM3[1]']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers8['modifDate_dirigeantM3[1]']          = date;
} else if (dirigeant12.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(dirigeant12.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers8['modifDate_dirigeantM3[1]']          = date;
}
formFieldsPers8['dirigeant_nouveauM3[1]']                     = Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantNouveau') ? true : false;
formFieldsPers8['dirigeant_partantM3[1]']                     = Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers8['dirigeant_modifM3[1]']                       = Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantModif') ? true : false;
formFieldsPers8['dirigeant_maintenuM3[1]']                    = Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantMaintenu') ? true : false;

if (not Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers8['dirigeant_maintenuAncienneQualiteM3[1]']     = Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantMaintenu') ? dirigeant12.personneLieeQualiteAncienne : '';
formFieldsPers8['dirigeant_qualiteM3[1]']                     = Value('id').of(dirigeant12.personneLieeQualite).eq('30') ? "Gérant" :
															(Value('id').of(dirigeant12.personneLieeQualite).eq('29') ? "Gérant et associé indéfiniment responsable" :
															(Value('id').of(dirigeant12.personneLieeQualite).eq('28') ? "Gérant et associé indéfiniment et solidairement responsable" :
															(Value('id').of(dirigeant12.personneLieeQualite).eq('75') ? "Associé indéfiniment responsable" :
															(Value('id').of(dirigeant12.personneLieeQualite).eq('74') ? "Associé indéfiniment et solidairement responsable" : dirigeant12.personneLieeQualite))));

if (Value('id').of(dirigeant12.personnaliteJuridique).eq('personnePhysique') or dirigeant12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
formFieldsPers8['dirigeant_nomNaissanceM3[1]']                                                      = dirigeant12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant ;
formFieldsPers8['dirigeant_nomUsageM3[1]']                                                          = dirigeant12.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant12.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers8['dirigeant_prenomM3[1]']                                                      = prenoms.toString();
} else if (dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers8['dirigeant_prenomM3[1]']                                                      = prenoms.toString();
}
if (dirigeant12.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
    var dateTmp = new Date(parseInt(dirigeant12.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers8['dirigeant_dateNaissanceM3[1]']          = date1;
}  else if (dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
    var dateTmp = new Date(parseInt(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFieldsPers8['dirigeant_dateNaissanceM3[1]']          = date2;
}
formFieldsPers8['dirigeant_lieuNaissanceM3[1]']                 = dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant != null ? 
																(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant + ' ' +  "(" + '' + dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceDepartementGerant.getId() +''+ ")") : 
																(dirigeant12.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille != null ? 
																(dirigeant12.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant) : 
																(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille != null ?
																(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant) : '')));
formFieldsPers8['dirigeant_nationaliteM3[1]']                   = dirigeant12.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant12.civilitePersonnePhysique.personneLieePPNationaliteGerant : (dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant != null ? dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant : '');
}
if (Value('id').of(dirigeant12.personnaliteJuridique).eq('personneMorale')) {
formFieldsPers8['dirigeant_denoFormeJuridiqueM3[1]']                = dirigeant12.civilitePersonneMorale.personneLieePMDenomination + ' / ' + dirigeant12.civilitePersonneMorale.personneLieePMFormeJuridique;
formFieldsPers8['dirigeant_PM_immatriculationNumM3[1]']             = dirigeant12.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant12.civilitePersonneMorale.personneLieePMNumeroIdentification : (dirigeant12.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee != null ? (dirigeant12.civilitePersonneMorale.personneLieePMNumeroNonImmatriculee + ' ' + dirigeant12.civilitePersonneMorale.personneLieePMNomNonImmatriculee) : '');
formFieldsPers8['dirigeant_PM_immatriculationLieuM3[1]']            = dirigeant12.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant12.civilitePersonneMorale.personneLieePMLieuImmatriculation : (dirigeant12.civilitePersonneMorale.personneLieePMLieuImmatriculation + ' / ' + dirigeant12.civilitePersonneMorale.personneLieePMPaysNonImmatriculee);
}
formFieldsPers8['dirigeant_adresseVoieM3[1]']                 = (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null ? dirigeant12.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie : '')
															  + ' ' + (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null ? dirigeant12.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie : '')
															  + ' ' + (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null ? dirigeant12.cadreAdresseDirigeant.dirigeantAdresseTypeVoie : '')
															  + ' ' + dirigeant12.cadreAdresseDirigeant.dirigeantAdresseNomVoie
															  + ' ' + (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null ? dirigeant12.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse : '')
															  + ' ' + (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null ? dirigeant12.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie : '');
formFieldsPers8['dirigeant_adresseCPM3[1]']         		  = dirigeant12.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant12.cadreAdresseDirigeant.dirigeantAdresseCodePostal : '';
formFieldsPers8['dirigeant_adresseCommuneM3[1]']              = dirigeant12.cadreAdresseDirigeant.dirigeantAdresseCommune != null ? dirigeant12.cadreAdresseDirigeant.dirigeantAdresseCommune : (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseVille != null ? (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseVille + ' / ' + dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays) : '');
}
if (Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantPartant')) {
formFieldsPers8['dirigeant_partantBisM3[1]']                        = true;
formFieldsPers8['dirigeantPartant_nomNaissanceM3[1]']               = dirigeant12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : (dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null ? dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant : '') ;
formFieldsPers8['dirigeantPartant_nomUsageM3[1]']                   = dirigeant12.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant12.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers8['dirigeantPartant_prenomM3[1]']                                                      = prenoms.toString();
} else if (dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers8['dirigeantPartant_prenomM3[1]']                                                      = prenoms.toString();
}
formFieldsPers8['dirigeantPartant_denoFormeJuridique[1]']           = dirigeant12.civilitePersonneMorale.personneLieePMDenomination != null ? (dirigeant12.civilitePersonneMorale.personneLieePMDenomination + ' / ' + dirigeant12.civilitePersonneMorale.personneLieePMFormeJuridique) : '';
}

var nirDeclarant = dirigeant12.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers8['dirigeantPartant_numSSM3[1]']                = nirDeclarant.substring(0, 13);
    formFieldsPers8['dirigeantPartant_numSSCleM3[1]']             = nirDeclarant.substring(13, 15);
}
var nirDeclarant2 = dirigeant12.civilitePersonnePhysiqueBIS.personneLieePersonnePhysiqueLieuNumSS;
if(nirDeclarant2 != null) {
    nirDeclarant2 = nirDeclarant2.replace(/ /g, "");
    formFieldsPers8['dirigeantPartant_numSSM3[1]']                = nirDeclarant2.substring(0, 13);
    formFieldsPers8['dirigeantPartant_numSSCleM3[1]']             = nirDeclarant2.substring(13, 15);
}
}

// Représentant permanent 5

// Cadre 9

if (representants.length > 4) {
formFieldsPers8['representantPermanent_numeroCadre[0]']          = representants[4]['cadre'];
formFieldsPers8['representantPermanent_duM2[0]']                 = false;
formFieldsPers8['representantPermanent_duM3[0]']                 = true;
if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers8['representantPermanent_dateModif[0]']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers8['representantPermanent_dateModif[0]']          = date;
} else if (representants[4]['dirigeant'].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(representants[4]['dirigeant'].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers8['representantPermanent_dateModif[0]']          = date;
}
formFieldsPers8['representantPermanent_nouveau[0]']              = (Value('id').of(representants[4]['dirigeant'].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau') or Value('id').of(representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantNouveau')) ? true : false ;
formFieldsPers8['representantPermanent_partant[0]']              = Value('id').of(representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers8['representantPermanent_modif[0]']                = Value('id').of(representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif') ? true : false;
formFieldsPers8['representantPermanent_nomNaissance[0]']         = representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomNaissance;
formFieldsPers8['representantPermanent_nomUsage[0]']             = representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage != null ? representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage : '';
if (representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom != null) {
var prenoms=[];
for ( i = 0; i < representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom[i]);}                            
formFieldsPers8['representantPermanent_prenom[0]']                                                      = prenoms.toString();
}
formFieldsPers8['representantPermanent_nationalite[0]']          = representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNationalite;
if (representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers8['representantPermanent_dateNaissance[0]']          = date1;
}
formFieldsPers8['representantPermanent_lieuNaissanceCommune[0]'] = representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune != null ? 
																	(representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune + ' ' +  "(" + '' + representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceDepartement.getId() +''+ ")") :
																	(representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissancePays);
formFieldsPers8['representantPermanent_adresseVoie[0]']          = (representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null ? representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie : '')
																	+ ' ' + (representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null ? representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie : '')
																	+ ' ' + (representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null ? representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie : '')
																	+ ' ' + representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNomVoie
																	+ ' ' + (representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null ? representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie : '')
																	+ ' ' + (representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null ? representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale : '');
formFieldsPers8['representantPermanent_adresseCP[0]']            = representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
formFieldsPers8['representantPermanent_adresseCommune[0]']       = representants[4]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune;
}

// Représentant permanent 6

// Cadre 10

if (representants.length > 5) {
formFieldsPers8['representantPermanent_numeroCadre[1]']          = representants[5]['cadre'];
formFieldsPers8['representantPermanent_duM2[1]']                 = false;
formFieldsPers8['representantPermanent_duM3[1]']                 = true;
if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers8['representantPermanent_dateModif[1]']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers8['representantPermanent_dateModif[1]']          = date;
} else if (representants[5]['dirigeant'].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(representants[5]['dirigeant'].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers8['representantPermanent_dateModif[1]']          = date;
}
formFieldsPers8['representantPermanent_nouveau[1]']              = (Value('id').of(representants[5]['dirigeant'].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau') or Value('id').of(representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantNouveau')) ? true : false ;
formFieldsPers8['representantPermanent_partant[1]']              = Value('id').of(representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers8['representantPermanent_modif[1]']                = Value('id').of(representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif') ? true : false;
formFieldsPers8['representantPermanent_nomNaissance[1]']         = representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomNaissance;
formFieldsPers8['representantPermanent_nomUsage[1]']             = representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage != null ? representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage : '';
if (representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom != null) {
var prenoms=[];
for ( i = 0; i < representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom[i]);}                            
formFieldsPers8['representantPermanent_prenom[1]']                                                      = prenoms.toString();
}
formFieldsPers8['representantPermanent_nationalite[1]']          = representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNationalite;
if (representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers8['representantPermanent_dateNaissance[1]']          = date1;
}
formFieldsPers8['representantPermanent_lieuNaissanceCommune[1]'] = representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune != null ? 
																	(representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune + ' ' +  "(" + '' + representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceDepartement.getId() +''+ ")") :
																	(representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissancePays);
formFieldsPers8['representantPermanent_adresseVoie[1]']          = (representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null ? representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie : '')
																	+ ' ' + (representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null ? representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie : '')
																	+ ' ' + (representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null ? representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie : '')
																	+ ' ' + representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNomVoie
																	+ ' ' + (representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null ? representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie : '')
																	+ ' ' + (representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null ? representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale : '');
formFieldsPers8['representantPermanent_adresseCP[1]']            = representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
formFieldsPers8['representantPermanent_adresseCommune[1]']       = representants[5]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune;
}

// Cadre 11 Précision fondé de pouvoir 3

if ((fondePouvoir.length > 2) and fondePouvoir[2].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
formFieldsPers8['fondePouvoir_declareCadre']                        = fondePouvoir[2]['cadres'];
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers8['fondePouvoir_dateModifM3[0]']          = date;
} else if (fondePouvoir[2].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(fondePouvoir[2].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers8['fondePouvoir_dateModifM3[0]']          = date;
}
formFieldsPers8['fondePouvoir_etablissementAdresseVoieM3']    = (fondePouvoir[2].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNumeroVoie != null ? fondePouvoir[2].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNumeroVoie : '')
																+ ' ' + (fondePouvoir[2].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseIndiceVoie != null ? fondePouvoir[2].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseIndiceVoie : '')
																+ ' ' + (fondePouvoir[2].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseTypeVoie != null ? fondePouvoir[2].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseTypeVoie : '')
																+ ' ' + fondePouvoir[2].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNomVoie
																+ ' ' + (fondePouvoir[2].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseComplementAdresse != null ? fondePouvoir[2].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseComplementAdresse : '')
																+ ' ' + (fondePouvoir[2].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseDistriutionSpecialeVoie != null ? fondePouvoir[2].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseDistriutionSpecialeVoie : '');
formFieldsPers8['fondePouvoir_etablissementAdresseCPM3']      = fondePouvoir[2].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseCodePostal;
formFieldsPers8['fondePouvoir_etablissementAdresseCommuneM3'] = fondePouvoir[2].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseCommune;
}
}

// Imprimé M3 Hors SARL 4

if ((representants.length > 6) or (fondePouvoir.length > 3)) {

// Cadre 1

formFieldsPers9['demandeModificationM3']                      = false;
formFieldsPers9['demandeModificationRM_M3']                   = identite.immatriculationRM ? true : false;
formFieldsPers9['demandeIntercalaireM3']                      = true;
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
formFieldsPers9['intercalaireNumM3']                          = "4";
} else {
formFieldsPers9['intercalaireNumM3']                          = "3";
} 
 
// Cadre 2

formFieldsPers9['entreprise_denominationM3']                              = identite.entrepriseDenomination;

// Cadre 3

formFieldsPers9['entreprise_sirenM3']                                     = identite.siren.split(' ').join('');
formFieldsPers9['immatRCS_M3']                                            = true;
formFieldsPers9['immatRCSGreffe_M3']                                      = identite.immatRCSGreffe;
formFieldsPers9['immatRM_M3']                                             = identite.immatriculationRM ? true : false;
formFieldsPers9['immatRMDept_M3']                                         = identite.immatriculationRM ? identite.immatRMCMA : '';
formFieldsPers9['immatRMDeptNum_M3']                                      = identite.immatriculationRM ? identite.immatRMCMA.getId() : '';
formFieldsPers9['entreprise_formeJuridiqueM3']                            = identite.entrepriseFormeJuridique.getLabel();
formFieldsPers9['entreprise_adresseEntreprisePM_voieM3']                  = (siege.siegeAdresseNumeroVoie != null ? siege.siegeAdresseNumeroVoie : '')
																		  + ' ' + (siege.siegeAdresseIndiceVoie != null ? siege.siegeAdresseIndiceVoie : '')
																		  + ' ' + (siege.siegeAdresseTypeVoie != null ? siege.siegeAdresseTypeVoie : '')
																		  + ' ' + siege.siegeAdresseNomVoie
																		  + ' ' + (siege.siegeAdresseComplementVoie != null ? siege.siegeAdresseComplementVoie : '')
																		  + ' ' + (siege.siegeAdresseDistriutionSpecialeVoie != null ? siege.siegeAdresseDistriutionSpecialeVoie : '');
formFieldsPers9['entreprise_adresseEntreprisePM_codePostalM3']           = siege.siegeAdresseCodePostal;
formFieldsPers9['entreprise_adresseEntreprisePM_communeM3']              = siege.siegeAdresseCommune;
formFieldsPers9['entreprise_adresseEntreprisePM_communeAncienneM3']      = siege.communeAncienneAdresseSiege != null ? siege.communeAncienneAdresseSiege : '';

// Représentant permanent 7

// Cadre 9

if (representants.length > 6) {
formFieldsPers9['representantPermanent_numeroCadre[0]']          = representants[6]['cadre'];
formFieldsPers9['representantPermanent_duM2[0]']                 = false;
formFieldsPers9['representantPermanent_duM3[0]']                 = true;
if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers9['representantPermanent_dateModif[0]']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers9['representantPermanent_dateModif[0]']          = date;
} else if (representants[6]['dirigeant'].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(representants[6]['dirigeant'].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers9['representantPermanent_dateModif[0]']          = date;
}
formFieldsPers9['representantPermanent_nouveau[0]']              = (Value('id').of(representants[6]['dirigeant'].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau') or Value('id').of(representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantNouveau')) ? true : false ;
formFieldsPers9['representantPermanent_partant[0]']              = Value('id').of(representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers9['representantPermanent_modif[0]']                = Value('id').of(representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif') ? true : false;
formFieldsPers9['representantPermanent_nomNaissance[0]']         = representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomNaissance;
formFieldsPers9['representantPermanent_nomUsage[0]']             = representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage != null ? representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage : '';
if (representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom != null) {
var prenoms=[];
for ( i = 0; i < representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom[i]);}                            
formFieldsPers9['representantPermanent_prenom[0]']                                                      = prenoms.toString();
}
formFieldsPers9['representantPermanent_nationalite[0]']          = representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNationalite;
if (representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers9['representantPermanent_dateNaissance[0]']          = date1;
}
formFieldsPers9['representantPermanent_lieuNaissanceCommune[0]'] = representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune != null ? 
																	(representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune + ' ' +  "(" + '' + representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceDepartement.getId() +''+ ")") :
																	(representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissancePays);
formFieldsPers9['representantPermanent_adresseVoie[0]']          = (representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null ? representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie : '')
																	+ ' ' + (representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null ? representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie : '')
																	+ ' ' + (representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null ? representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie : '')
																	+ ' ' + representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNomVoie
																	+ ' ' + (representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null ? representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie : '')
																	+ ' ' + (representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null ? representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale : '');
formFieldsPers9['representantPermanent_adresseCP[0]']            = representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
formFieldsPers9['representantPermanent_adresseCommune[0]']       = representants[6]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune;
}

// Représentant permanent 8

// Cadre 10

if (representants.length > 7) {
formFieldsPers9['representantPermanent_numeroCadre[1]']          = representants[7]['cadre'];
formFieldsPers9['representantPermanent_duM2[1]']                 = false;
formFieldsPers9['representantPermanent_duM3[1]']                 = true;
if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers9['representantPermanent_dateModif[1]']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers9['representantPermanent_dateModif[1]']          = date;
} else if (representants[7]['dirigeant'].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(representants[7]['dirigeant'].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers9['representantPermanent_dateModif[1]']          = date;
}
formFieldsPers9['representantPermanent_nouveau[1]']              = (Value('id').of(representants[7]['dirigeant'].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau') or Value('id').of(representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantNouveau')) ? true : false ;
formFieldsPers9['representantPermanent_partant[1]']              = Value('id').of(representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers9['representantPermanent_modif[1]']                = Value('id').of(representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif') ? true : false;
formFieldsPers9['representantPermanent_nomNaissance[1]']         = representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomNaissance;
formFieldsPers9['representantPermanent_nomUsage[1]']             = representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage != null ? representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage : '';
if (representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom != null) {
var prenoms=[];
for ( i = 0; i < representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom[i]);}                            
formFieldsPers9['representantPermanent_prenom[1]']                                                      = prenoms.toString();
}
formFieldsPers9['representantPermanent_nationalite[1]']          = representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNationalite;
if (representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers9['representantPermanent_dateNaissance[1]']          = date1;
}
formFieldsPers9['representantPermanent_lieuNaissanceCommune[1]'] = representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune != null ? 
																	(representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune + ' ' +  "(" + '' + representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceDepartement.getId() +''+ ")") :
																	(representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissancePays);
formFieldsPers9['representantPermanent_adresseVoie[1]']          = (representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null ? representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie : '')
																	+ ' ' + (representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null ? representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie : '')
																	+ ' ' + (representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null ? representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie : '')
																	+ ' ' + representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNomVoie
																	+ ' ' + (representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null ? representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie : '')
																	+ ' ' + (representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null ? representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale : '');
formFieldsPers9['representantPermanent_adresseCP[1]']            = representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
formFieldsPers9['representantPermanent_adresseCommune[1]']       = representants[7]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune;
}

// Cadre 11 Précision fondé de pouvoir 4

if ((fondePouvoir.length > 3) and fondePouvoir[3].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
formFieldsPers9['fondePouvoir_declareCadre']                        = fondePouvoir[3]['cadres'];
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers9['fondePouvoir_dateModifM3[0]']          = date;
} else if (fondePouvoir[3].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(fondePouvoir[3].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers9['fondePouvoir_dateModifM3[0]']          = date;
}
formFieldsPers9['fondePouvoir_etablissementAdresseVoieM3']    = (fondePouvoir[3].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNumeroVoie != null ? fondePouvoir[3].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNumeroVoie : '')
																+ ' ' + (fondePouvoir[3].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseIndiceVoie != null ? fondePouvoir[3].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseIndiceVoie : '')
																+ ' ' + (fondePouvoir[3].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseTypeVoie != null ? fondePouvoir[3].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseTypeVoie : '')
																+ ' ' + fondePouvoir[3].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNomVoie
																+ ' ' + (fondePouvoir[3].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseComplementAdresse != null ? fondePouvoir[3].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseComplementAdresse : '')
																+ ' ' + (fondePouvoir[3].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseDistriutionSpecialeVoie != null ? fondePouvoir[3].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseDistriutionSpecialeVoie : '');
formFieldsPers9['fondePouvoir_etablissementAdresseCPM3']      = fondePouvoir[3].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseCodePostal;
formFieldsPers9['fondePouvoir_etablissementAdresseCommuneM3'] = fondePouvoir[3].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseCommune;
}
}

// Imprimé M3 Hors SARL 5

if ((representants.length > 8) or (fondePouvoir.length > 4)) {

// Cadre 1

formFieldsPers10['demandeModificationM3']                      = false;
formFieldsPers10['demandeModificationRM_M3']                   = identite.immatriculationRM ? true : false;
formFieldsPers10['demandeIntercalaireM3']                      = true;
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
formFieldsPers10['intercalaireNumM3']                          = "5";
} else {
formFieldsPers10['intercalaireNumM3']                          = "4";
} 
 
// Cadre 2

formFieldsPers10['entreprise_denominationM3']                              = identite.entrepriseDenomination;

// Cadre 3

formFieldsPers10['entreprise_sirenM3']                                     = identite.siren.split(' ').join('');
formFieldsPers10['immatRCS_M3']                                            = true;
formFieldsPers10['immatRCSGreffe_M3']                                      = identite.immatRCSGreffe;
formFieldsPers10['immatRM_M3']                                             = identite.immatriculationRM ? true : false;
formFieldsPers10['immatRMDept_M3']                                         = identite.immatriculationRM ? identite.immatRMCMA : '';
formFieldsPers10['immatRMDeptNum_M3']                                      = identite.immatriculationRM ? identite.immatRMCMA.getId() : '';
formFieldsPers10['entreprise_formeJuridiqueM3']                            = identite.entrepriseFormeJuridique.getLabel();
formFieldsPers10['entreprise_adresseEntreprisePM_voieM3']                  = (siege.siegeAdresseNumeroVoie != null ? siege.siegeAdresseNumeroVoie : '')
																		  + ' ' + (siege.siegeAdresseIndiceVoie != null ? siege.siegeAdresseIndiceVoie : '')
																		  + ' ' + (siege.siegeAdresseTypeVoie != null ? siege.siegeAdresseTypeVoie : '')
																		  + ' ' + siege.siegeAdresseNomVoie
																		  + ' ' + (siege.siegeAdresseComplementVoie != null ? siege.siegeAdresseComplementVoie : '')
																		  + ' ' + (siege.siegeAdresseDistriutionSpecialeVoie != null ? siege.siegeAdresseDistriutionSpecialeVoie : '');
formFieldsPers10['entreprise_adresseEntreprisePM_codePostalM3']           = siege.siegeAdresseCodePostal;
formFieldsPers10['entreprise_adresseEntreprisePM_communeM3']              = siege.siegeAdresseCommune;
formFieldsPers10['entreprise_adresseEntreprisePM_communeAncienneM3']      = siege.communeAncienneAdresseSiege != null ? siege.communeAncienneAdresseSiege : '';

// Représentant permanent 9

// Cadre 9

if (representants.length > 8) {
formFieldsPers10['representantPermanent_numeroCadre[0]']          = representants[8]['cadre'];
formFieldsPers10['representantPermanent_duM2[0]']                 = false;
formFieldsPers10['representantPermanent_duM3[0]']                 = true;
if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers10['representantPermanent_dateModif[0]']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers10['representantPermanent_dateModif[0]']          = date;
} else if (representants[8]['dirigeant'].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(representants[8]['dirigeant'].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers10['representantPermanent_dateModif[0]']          = date;
}
formFieldsPers10['representantPermanent_nouveau[0]']              = (Value('id').of(representants[8]['dirigeant'].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau') or Value('id').of(representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantNouveau')) ? true : false ;
formFieldsPers10['representantPermanent_partant[0]']              = Value('id').of(representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers10['representantPermanent_modif[0]']                = Value('id').of(representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif') ? true : false;
formFieldsPers10['representantPermanent_nomNaissance[0]']         = representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomNaissance;
formFieldsPers10['representantPermanent_nomUsage[0]']             = representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage != null ? representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage : '';
if (representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom != null) {
var prenoms=[];
for ( i = 0; i < representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom[i]);}                            
formFieldsPers10['representantPermanent_prenom[0]']                                                      = prenoms.toString();
}
formFieldsPers10['representantPermanent_nationalite[0]']          = representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNationalite;
if (representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers10['representantPermanent_dateNaissance[0]']          = date1;
}
formFieldsPers10['representantPermanent_lieuNaissanceCommune[0]'] = representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune != null ? 
																	(representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune + ' ' +  "(" + '' + representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceDepartement.getId() +''+ ")") :
																	(representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissancePays);
formFieldsPers10['representantPermanent_adresseVoie[0]']          = (representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null ? representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie : '')
																	+ ' ' + (representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null ? representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie : '')
																	+ ' ' + (representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null ? representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie : '')
																	+ ' ' + representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNomVoie
																	+ ' ' + (representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null ? representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie : '')
																	+ ' ' + (representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null ? representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale : '');
formFieldsPers10['representantPermanent_adresseCP[0]']            = representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
formFieldsPers10['representantPermanent_adresseCommune[0]']       = representants[8]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune;
}

// Représentant permanent 10

// Cadre 10

if (representants.length > 9) {
formFieldsPers10['representantPermanent_numeroCadre[1]']          = representants[9]['cadre'];
formFieldsPers10['representantPermanent_duM2[1]']                 = false;
formFieldsPers10['representantPermanent_duM3[1]']                 = true;
if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers10['representantPermanent_dateModif[1]']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers10['representantPermanent_dateModif[1]']          = date;
} else if (representants[9]['dirigeant'].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(representants[9]['dirigeant'].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers10['representantPermanent_dateModif[1]']          = date;
}
formFieldsPers10['representantPermanent_nouveau[1]']              = (Value('id').of(representants[9]['dirigeant'].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau') or Value('id').of(representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantNouveau')) ? true : false ;
formFieldsPers10['representantPermanent_partant[1]']              = Value('id').of(representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers10['representantPermanent_modif[1]']                = Value('id').of(representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif') ? true : false;
formFieldsPers10['representantPermanent_nomNaissance[1]']         = representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomNaissance;
formFieldsPers10['representantPermanent_nomUsage[1]']             = representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage != null ? representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage : '';
if (representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom != null) {
var prenoms=[];
for ( i = 0; i < representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom[i]);}                            
formFieldsPers10['representantPermanent_prenom[1]']                                                      = prenoms.toString();
}
formFieldsPers10['representantPermanent_nationalite[1]']          = representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNationalite;
if (representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers10['representantPermanent_dateNaissance[1]']          = date1;
}
formFieldsPers10['representantPermanent_lieuNaissanceCommune[1]'] = representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune != null ? 
																	(representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune + ' ' +  "(" + '' + representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceDepartement.getId() +''+ ")") :
																	(representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissancePays);
formFieldsPers10['representantPermanent_adresseVoie[1]']          = (representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null ? representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie : '')
																	+ ' ' + (representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null ? representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie : '')
																	+ ' ' + (representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null ? representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie : '')
																	+ ' ' + representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNomVoie
																	+ ' ' + (representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null ? representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie : '')
																	+ ' ' + (representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null ? representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale : '');
formFieldsPers10['representantPermanent_adresseCP[1]']            = representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
formFieldsPers10['representantPermanent_adresseCommune[1]']       = representants[9]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune;
}

// Cadre 11 Précision fondé de pouvoir 5

if ((fondePouvoir.length > 4) and fondePouvoir[4].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
formFieldsPers10['fondePouvoir_declareCadre']                        = fondePouvoir[4]['cadres'];
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers10['fondePouvoir_dateModifM3[0]']          = date;
} else if (fondePouvoir[4].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(fondePouvoir[4].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers10['fondePouvoir_dateModifM3[0]']          = date;
}
formFieldsPers10['fondePouvoir_etablissementAdresseVoieM3']    = (fondePouvoir[4].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNumeroVoie != null ? fondePouvoir[4].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNumeroVoie : '')
																+ ' ' + (fondePouvoir[4].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseIndiceVoie != null ? fondePouvoir[4].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseIndiceVoie : '')
																+ ' ' + (fondePouvoir[4].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseTypeVoie != null ? fondePouvoir[4].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseTypeVoie : '')
																+ ' ' + fondePouvoir[4].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNomVoie
																+ ' ' + (fondePouvoir[4].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseComplementAdresse != null ? fondePouvoir[4].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseComplementAdresse : '')
																+ ' ' + (fondePouvoir[4].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseDistriutionSpecialeVoie != null ? fondePouvoir[4].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseDistriutionSpecialeVoie : '');
formFieldsPers10['fondePouvoir_etablissementAdresseCPM3']      = fondePouvoir[4].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseCodePostal;
formFieldsPers10['fondePouvoir_etablissementAdresseCommuneM3'] = fondePouvoir[4].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseCommune;
}
}

// Imprimé M3 Hors SARL 6

if ((representants.length > 10) or (fondePouvoir.length > 5)) {

// Cadre 1

formFieldsPers11['demandeModificationM3']                      = false;
formFieldsPers11['demandeModificationRM_M3']                   = identite.immatriculationRM ? true : false;
formFieldsPers11['demandeIntercalaireM3']                      = true;
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
formFieldsPers11['intercalaireNumM3']                          = "6";
} else {
formFieldsPers11['intercalaireNumM3']                          = "5";
} 
 
// Cadre 2

formFieldsPers11['entreprise_denominationM3']                              = identite.entrepriseDenomination;

// Cadre 3

formFieldsPers11['entreprise_sirenM3']                                     = identite.siren.split(' ').join('');
formFieldsPers11['immatRCS_M3']                                            = true;
formFieldsPers11['immatRCSGreffe_M3']                                      = identite.immatRCSGreffe;
formFieldsPers11['immatRM_M3']                                             = identite.immatriculationRM ? true : false;
formFieldsPers11['immatRMDept_M3']                                         = identite.immatriculationRM ? identite.immatRMCMA : '';
formFieldsPers11['immatRMDeptNum_M3']                                      = identite.immatriculationRM ? identite.immatRMCMA.getId() : '';
formFieldsPers11['entreprise_formeJuridiqueM3']                            = identite.entrepriseFormeJuridique.getLabel();
formFieldsPers11['entreprise_adresseEntreprisePM_voieM3']                  = (siege.siegeAdresseNumeroVoie != null ? siege.siegeAdresseNumeroVoie : '')
																		  + ' ' + (siege.siegeAdresseIndiceVoie != null ? siege.siegeAdresseIndiceVoie : '')
																		  + ' ' + (siege.siegeAdresseTypeVoie != null ? siege.siegeAdresseTypeVoie : '')
																		  + ' ' + siege.siegeAdresseNomVoie
																		  + ' ' + (siege.siegeAdresseComplementVoie != null ? siege.siegeAdresseComplementVoie : '')
																		  + ' ' + (siege.siegeAdresseDistriutionSpecialeVoie != null ? siege.siegeAdresseDistriutionSpecialeVoie : '');
formFieldsPers11['entreprise_adresseEntreprisePM_codePostalM3']           = siege.siegeAdresseCodePostal;
formFieldsPers11['entreprise_adresseEntreprisePM_communeM3']              = siege.siegeAdresseCommune;
formFieldsPers11['entreprise_adresseEntreprisePM_communeAncienneM3']      = siege.communeAncienneAdresseSiege != null ? siege.communeAncienneAdresseSiege : '';

// Représentant permanent 11

// Cadre 9

if (representants.length > 10) {
formFieldsPers11['representantPermanent_numeroCadre[0]']          = representants[10]['cadre'];
formFieldsPers11['representantPermanent_duM2[0]']                 = false;
formFieldsPers11['representantPermanent_duM3[0]']                 = true;
if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers11['representantPermanent_dateModif[0]']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers11['representantPermanent_dateModif[0]']          = date;
} else if (representants[10]['dirigeant'].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(representants[10]['dirigeant'].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers11['representantPermanent_dateModif[0]']          = date;
}
formFieldsPers11['representantPermanent_nouveau[0]']              = (Value('id').of(representants[10]['dirigeant'].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau') or Value('id').of(representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantNouveau')) ? true : false ;
formFieldsPers11['representantPermanent_partant[0]']              = Value('id').of(representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers11['representantPermanent_modif[0]']                = Value('id').of(representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif') ? true : false;
formFieldsPers11['representantPermanent_nomNaissance[0]']         = representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomNaissance;
formFieldsPers11['representantPermanent_nomUsage[0]']             = representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage != null ? representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage : '';
if (representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom != null) {
var prenoms=[];
for ( i = 0; i < representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom[i]);}                            
formFieldsPers11['representantPermanent_prenom[0]']                                                      = prenoms.toString();
}
formFieldsPers11['representantPermanent_nationalite[0]']          = representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNationalite;
if (representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers11['representantPermanent_dateNaissance[0]']          = date1;
}
formFieldsPers11['representantPermanent_lieuNaissanceCommune[0]'] = representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune != null ? 
																	(representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune + ' ' +  "(" + '' + representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceDepartement.getId() +''+ ")") :
																	(representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissancePays);
formFieldsPers11['representantPermanent_adresseVoie[0]']          = (representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null ? representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie : '')
																	+ ' ' + (representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null ? representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie : '')
																	+ ' ' + (representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null ? representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie : '')
																	+ ' ' + representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNomVoie
																	+ ' ' + (representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null ? representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie : '')
																	+ ' ' + (representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null ? representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale : '');
formFieldsPers11['representantPermanent_adresseCP[0]']            = representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
formFieldsPers11['representantPermanent_adresseCommune[0]']       = representants[10]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune;
}

// Représentant permanent 12

// Cadre 10

if (representants.length > 11) {
formFieldsPers11['representantPermanent_numeroCadre[1]']          = representants[11]['cadre'];
formFieldsPers11['representantPermanent_duM2[1]']                 = false;
formFieldsPers11['representantPermanent_duM3[1]']                 = true;
if (dissolution.cadreIdentiteLiquidateur.autreDirigeant and dissolution.modifDateDissolution !== null) {
    var dateTmp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers11['representantPermanent_dateModif[1]']          = date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers11['representantPermanent_dateModif[1]']          = date;
} else if (representants[11]['dirigeant'].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(representants[11]['dirigeant'].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers11['representantPermanent_dateModif[1]']          = date;
}
formFieldsPers11['representantPermanent_nouveau[1]']              = (Value('id').of(representants[11]['dirigeant'].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau') or Value('id').of(representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantNouveau')) ? true : false ;
formFieldsPers11['representantPermanent_partant[1]']              = Value('id').of(representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant') ? true : false;
formFieldsPers11['representantPermanent_modif[1]']                = Value('id').of(representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif') ? true : false;
formFieldsPers11['representantPermanent_nomNaissance[1]']         = representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomNaissance;
formFieldsPers11['representantPermanent_nomUsage[1]']             = representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage != null ? representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNomUsage : '';
if (representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom != null) {
var prenoms=[];
for ( i = 0; i < representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPPrenom[i]);}                            
formFieldsPers11['representantPermanent_prenom[1]']                                                      = prenoms.toString();
}
formFieldsPers11['representantPermanent_nationalite[1]']          = representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPNationalite;
if (representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance != null) {
    var dateTmp = new Date(parseInt(representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFieldsPers11['representantPermanent_dateNaissance[1]']          = date1;
}
formFieldsPers11['representantPermanent_lieuNaissanceCommune[1]'] = representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune != null ? 
																	(representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceCommnune + ' ' +  "(" + '' + representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissanceDepartement.getId() +''+ ")") :
																	(representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePersonnePhysiqueLieuNaissanceVille + ' / ' + representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.personneLieePPLieuNaissancePays);
formFieldsPers11['representantPermanent_adresseVoie[1]']          = (representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null ? representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNumeroVoie : '')
																	+ ' ' + (representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null ? representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantIndiceVoie : '')
																	+ ' ' + (representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null ? representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantTypeVoie : '')
																	+ ' ' + representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantNomVoie
																	+ ' ' + (representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null ? representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantComplementVoie : '')
																	+ ' ' + (representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null ? representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale : '');
formFieldsPers11['representantPermanent_adresseCP[1]']            = representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
formFieldsPers11['representantPermanent_adresseCommune[1]']       = representants[11]['dirigeant'].cadreIdentiteDirigeant.civiliteRepresentant.adresseRepresentant.representantAdresseCommune;
}

// Cadre 11 Précision fondé de pouvoir 6

if ((fondePouvoir.length > 5) and fondePouvoir[5].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
formFieldsPers11['fondePouvoir_declareCadre']                        = fondePouvoir[5]['cadres'];
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
    var dateTmp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers11['fondePouvoir_dateModifM3[0]']          = date;
} else if (fondePouvoir[5].cadreIdentiteDirigeant.dateModifDirigeant != null) {
    var dateTmp = new Date(parseInt(fondePouvoir[5].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers11['fondePouvoir_dateModifM3[0]']          = date;
}
formFieldsPers11['fondePouvoir_etablissementAdresseVoieM3']    = (fondePouvoir[5].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNumeroVoie != null ? fondePouvoir[5].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNumeroVoie : '')
																+ ' ' + (fondePouvoir[5].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseIndiceVoie != null ? fondePouvoir[5].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseIndiceVoie : '')
																+ ' ' + (fondePouvoir[5].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseTypeVoie != null ? fondePouvoir[5].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseTypeVoie : '')
																+ ' ' + fondePouvoir[5].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseNomVoie
																+ ' ' + (fondePouvoir[5].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseComplementAdresse != null ? fondePouvoir[5].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseComplementAdresse : '')
																+ ' ' + (fondePouvoir[5].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseDistriutionSpecialeVoie != null ? fondePouvoir[5].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseDistriutionSpecialeVoie : '');
formFieldsPers11['fondePouvoir_etablissementAdresseCPM3']      = fondePouvoir[5].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseCodePostal;
formFieldsPers11['fondePouvoir_etablissementAdresseCommuneM3'] = fondePouvoir[5].cadreIdentiteDirigeant.cadreAdressePouvoirLimite.limiteAdresseCommune;
}
}
}
	
// M prime

if (fermeture.autreFermeture or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('fusion')) {
formFields['denomination_mPrime']                              = identite.entrepriseDenomination;
formFields['formeJuridique_mPrime']                            = identite.entrepriseFormeJuridique.getLabel();

if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('fusion')) {
formFields['dénominationFusion_mPrime[0]']                    = fusion.fusionGroup1.entrepriseLieeEntreprisePMDenomination;
formFields['formeJuridiqueFusion_mPrime[0]']                  = fusion.fusionGroup1.entreperiseLieeEntreprisePMFormeJuridique;
formFields['adresseFusion_mPrime[0]']                         = (fusion.fusionGroup1.adresseFusionScission.numeroAdresseFusion != null ? fusion.fusionGroup1.adresseFusionScission.numeroAdresseFusion : '')
																+ ' ' + (fusion.fusionGroup1.adresseFusionScission.indiceAdresseFusion != null ? fusion.fusionGroup1.adresseFusionScission.indiceAdresseFusion : '')
																+ ' ' + (fusion.fusionGroup1.adresseFusionScission.typeAdresseFusion != null ? fusion.fusionGroup1.adresseFusionScission.typeAdresseFusion : '')
																+ ' ' + fusion.fusionGroup1.adresseFusionScission.voieAdresseFusion
																+ ' ' + (fusion.fusionGroup1.adresseFusionScission.complementAdresseFusion != null ? fusion.fusionGroup1.adresseFusionScission.complementAdresseFusion : '')
																+ ' ' + (fusion.fusionGroup1.adresseFusionScission.distributionSpecialeAdresseFusion != null ? fusion.fusionGroup1.adresseFusionScission.distributionSpecialeAdresseFusion : '')
																+ ' ' + fusion.fusionGroup1.adresseFusionScission.codePostalAdresseFusion
																+ ' ' + fusion.fusionGroup1.adresseFusionScission.communeAdresseFusion;
formFields['sirenFusion_mPrime[0]']                           = fusion.fusionGroup1.entrepriseLieeSiren.split(' ').join('');
formFields['greffeFusion_mPrime[0]']                          = fusion.fusionGroup1.entrepriseLieeGreffe;

if (fusion.fusionGroup1.fusionAutreSociete) {
formFields['dénominationFusion_mPrime[1]']                    = fusion.fusionGroup2.entrepriseLieeEntreprisePMDenomination;
formFields['formeJuridiqueFusion_mPrime[1]']                  = fusion.fusionGroup2.entreperiseLieeEntreprisePMFormeJuridique;
formFields['adresseFusion_mPrime[1]']                         = (fusion.fusionGroup2.adresseFusionScission.numeroAdresseFusion != null ? fusion.fusionGroup2.adresseFusionScission.numeroAdresseFusion : '')
																+ ' ' + (fusion.fusionGroup2.adresseFusionScission.indiceAdresseFusion != null ? fusion.fusionGroup2.adresseFusionScission.indiceAdresseFusion : '')
																+ ' ' + (fusion.fusionGroup2.adresseFusionScission.typeAdresseFusion != null ? fusion.fusionGroup2.adresseFusionScission.typeAdresseFusion : '')
																+ ' ' + fusion.fusionGroup2.adresseFusionScission.voieAdresseFusion
																+ ' ' + (fusion.fusionGroup2.adresseFusionScission.complementAdresseFusion != null ? fusion.fusionGroup2.adresseFusionScission.complementAdresseFusion : '')
																+ ' ' + (fusion.fusionGroup2.adresseFusionScission.distributionSpecialeAdresseFusion != null ? fusion.fusionGroup2.adresseFusionScission.distributionSpecialeAdresseFusion : '')
																+ ' ' + fusion.fusionGroup2.adresseFusionScission.codePostalAdresseFusion
																+ ' ' + fusion.fusionGroup2.adresseFusionScission.communeAdresseFusion;
formFields['sirenFusion_mPrime[1]']                           = fusion.fusionGroup2.entrepriseLieeSiren.split(' ').join('');
formFields['greffeFusion_mPrime[1]']                          = fusion.fusionGroup2.entrepriseLieeGreffe;
}

if (fusion.fusionGroup2.fusionAutreSociete2) {
formFields['dénominationFusion_mPrime[2]']                    = fusion.fusionGroup3.entrepriseLieeEntreprisePMDenomination;
formFields['formeJuridiqueFusion_mPrime[2]']                  = fusion.fusionGroup3.entreperiseLieeEntreprisePMFormeJuridique;
formFields['adresseFusion_mPrime[2]']                         = (fusion.fusionGroup3.adresseFusionScission.numeroAdresseFusion != null ? fusion.fusionGroup3.adresseFusionScission.numeroAdresseFusion : '')
																+ ' ' + (fusion.fusionGroup3.adresseFusionScission.indiceAdresseFusion != null ? fusion.fusionGroup3.adresseFusionScission.indiceAdresseFusion : '')
																+ ' ' + (fusion.fusionGroup3.adresseFusionScission.typeAdresseFusion != null ? fusion.fusionGroup3.adresseFusionScission.typeAdresseFusion : '')
																+ ' ' + fusion.fusionGroup3.adresseFusionScission.voieAdresseFusion
																+ ' ' + (fusion.fusionGroup3.adresseFusionScission.complementAdresseFusion != null ? fusion.fusionGroup3.adresseFusionScission.complementAdresseFusion : '')
																+ ' ' + (fusion.fusionGroup3.adresseFusionScission.distributionSpecialeAdresseFusion != null ? fusion.fusionGroup3.adresseFusionScission.distributionSpecialeAdresseFusion : '')
																+ ' ' + fusion.fusionGroup3.adresseFusionScission.codePostalAdresseFusion
																+ ' ' + fusion.fusionGroup3.adresseFusionScission.communeAdresseFusion;
formFields['sirenFusion_mPrime[2]']                           = fusion.fusionGroup3.entrepriseLieeSiren.split(' ').join('');
formFields['greffeFusion_mPrime[2]']                          = fusion.fusionGroup3.entrepriseLieeGreffe;
}

if (fusion.fusionGroup3.fusionAutreSociete3) {
formFields['dénominationFusion_mPrime[3]']                    = fusion.fusionGroup4.entrepriseLieeEntreprisePMDenomination;
formFields['formeJuridiqueFusion_mPrime[3]']                  = fusion.fusionGroup4.entreperiseLieeEntreprisePMFormeJuridique;
formFields['adresseFusion_mPrime[3]']                         = (fusion.fusionGroup4.adresseFusionScission.numeroAdresseFusion != null ? fusion.fusionGroup4.adresseFusionScission.numeroAdresseFusion : '')
																+ ' ' + (fusion.fusionGroup4.adresseFusionScission.indiceAdresseFusion != null ? fusion.fusionGroup4.adresseFusionScission.indiceAdresseFusion : '')
																+ ' ' + (fusion.fusionGroup4.adresseFusionScission.typeAdresseFusion != null ? fusion.fusionGroup4.adresseFusionScission.typeAdresseFusion : '')
																+ ' ' + fusion.fusionGroup4.adresseFusionScission.voieAdresseFusion
																+ ' ' + (fusion.fusionGroup4.adresseFusionScission.complementAdresseFusion != null ? fusion.fusionGroup4.adresseFusionScission.complementAdresseFusion : '')
																+ ' ' + (fusion.fusionGroup4.adresseFusionScission.distributionSpecialeAdresseFusion != null ? fusion.fusionGroup4.adresseFusionScission.distributionSpecialeAdresseFusion : '')
																+ ' ' + fusion.fusionGroup4.adresseFusionScission.codePostalAdresseFusion
																+ ' ' + fusion.fusionGroup4.adresseFusionScission.communeAdresseFusion;
formFields['sirenFusion_mPrime[3]']                           = fusion.fusionGroup4.entrepriseLieeSiren.split(' ').join('');
formFields['greffeFusion_mPrime[3]']                          = fusion.fusionGroup4.entrepriseLieeGreffe;
}

if (fusion.fusionGroup4.fusionAutreSociete4) {
formFields['dénominationFusion_mPrime[4]']                    = fusion.fusionGroup5.entrepriseLieeEntreprisePMDenomination;
formFields['formeJuridiqueFusion_mPrime[4]']                  = fusion.fusionGroup5.entreperiseLieeEntreprisePMFormeJuridique;
formFields['adresseFusion_mPrime[4]']                         = (fusion.fusionGroup5.adresseFusionScission.numeroAdresseFusion != null ? fusion.fusionGroup5.adresseFusionScission.numeroAdresseFusion : '')
																+ ' ' + (fusion.fusionGroup5.adresseFusionScission.indiceAdresseFusion != null ? fusion.fusionGroup5.adresseFusionScission.indiceAdresseFusion : '')
																+ ' ' + (fusion.fusionGroup5.adresseFusionScission.typeAdresseFusion != null ? fusion.fusionGroup5.adresseFusionScission.typeAdresseFusion : '')
																+ ' ' + fusion.fusionGroup5.adresseFusionScission.voieAdresseFusion
																+ ' ' + (fusion.fusionGroup5.adresseFusionScission.complementAdresseFusion != null ? fusion.fusionGroup5.adresseFusionScission.complementAdresseFusion : '')
																+ ' ' + (fusion.fusionGroup5.adresseFusionScission.distributionSpecialeAdresseFusion != null ? fusion.fusionGroup5.adresseFusionScission.distributionSpecialeAdresseFusion : '')
																+ ' ' + fusion.fusionGroup5.adresseFusionScission.codePostalAdresseFusion
																+ ' ' + fusion.fusionGroup5.adresseFusionScission.communeAdresseFusion;
formFields['sirenFusion_mPrime[4]']                           = fusion.fusionGroup5.entrepriseLieeSiren.split(' ').join('');
formFields['greffeFusion_mPrime[4]']                          = fusion.fusionGroup5.entrepriseLieeGreffe;
}

if (fusion.fusionGroup5.fusionAutreSociete5) {
formFields['dénominationFusion_mPrime[5]']                    = fusion.fusionGroup6.entrepriseLieeEntreprisePMDenomination;
formFields['formeJuridiqueFusion_mPrime[5]']                  = fusion.fusionGroup6.entreperiseLieeEntreprisePMFormeJuridique;
formFields['adresseFusion_mPrime[5]']                         = (fusion.fusionGroup6.adresseFusionScission.numeroAdresseFusion != null ? fusion.fusionGroup6.adresseFusionScission.numeroAdresseFusion : '')
																+ ' ' + (fusion.fusionGroup6.adresseFusionScission.indiceAdresseFusion != null ? fusion.fusionGroup6.adresseFusionScission.indiceAdresseFusion : '')
																+ ' ' + (fusion.fusionGroup6.adresseFusionScission.typeAdresseFusion != null ? fusion.fusionGroup6.adresseFusionScission.typeAdresseFusion : '')
																+ ' ' + fusion.fusionGroup6.adresseFusionScission.voieAdresseFusion
																+ ' ' + (fusion.fusionGroup6.adresseFusionScission.complementAdresseFusion != null ? fusion.fusionGroup6.adresseFusionScission.complementAdresseFusion : '')
																+ ' ' + (fusion.fusionGroup6.adresseFusionScission.distributionSpecialeAdresseFusion != null ? fusion.fusionGroup6.adresseFusionScission.distributionSpecialeAdresseFusion : '')
																+ ' ' + fusion.fusionGroup6.adresseFusionScission.codePostalAdresseFusion
																+ ' ' + fusion.fusionGroup6.adresseFusionScission.communeAdresseFusion;
formFields['sirenFusion_mPrime[5]']                           = fusion.fusionGroup6.entrepriseLieeSiren.split(' ').join('');
formFields['greffeFusion_mPrime[5]']                          = fusion.fusionGroup6.entrepriseLieeGreffe;
}
}

if (fermeture.autreFermeture) {
formFields['etablissement_adresseVoie_mPrime[0]']             = (fermeture.cadreAdresseEtablissementFerme2.numeroAdresseEtablissement != null ? fermeture.cadreAdresseEtablissementFerme2.numeroAdresseEtablissement : '')
																		+ ' ' + (fermeture.cadreAdresseEtablissementFerme2.indiceAdresseEtablissement != null ? fermeture.cadreAdresseEtablissementFerme2.indiceAdresseEtablissement : '')
																		+ ' ' + (fermeture.cadreAdresseEtablissementFerme2.typeAdresseEtablissement != null ? fermeture.cadreAdresseEtablissementFerme2.typeAdresseEtablissement : '')
																		+ ' ' + (fermeture.cadreAdresseEtablissementFerme2.voieAdresseEtablissement != null ? fermeture.cadreAdresseEtablissementFerme2.voieAdresseEtablissement : '')
																		+ ' ' + (fermeture.cadreAdresseEtablissementFerme2.complementAdresseEtablissement != null ? fermeture.cadreAdresseEtablissementFerme2.complementAdresseEtablissement : '')
																		+ ' ' + (fermeture.cadreAdresseEtablissementFerme2.distributionSpecialeAdresseEtablissement != null ? fermeture.cadreAdresseEtablissementFerme2.distributionSpecialeAdresseEtablissement : '');
formFields['etablissement_adresseCP_mPrime[0]']                      = fermeture.cadreAdresseEtablissementFerme2.codePostalAdresseEtablissement;
formFields['etablissement_adresseCommune_mPrime[0]']        		 = fermeture.cadreAdresseEtablissementFerme2.communeAdresseEtablissement;
formFields['etablissement_destinationSuppression_mPrime[0]']       = Value('id').of(fermeture.destinationEtablissementAutre1).eq('supprime') ? true : false;
formFields['etablissement_destinationVente_mPrime[0]']             = Value('id').of(fermeture.destinationEtablissementAutre1).eq('vendu') ? true : false;
formFields['etablissement_destinationAutre_mPrime[0]']                 = Value('id').of(fermeture.destinationEtablissementAutre1).eq('autre') ? true : false;
formFields['etablissement_destinationAutreLibelle_mPrime[0]']          = Value('id').of(fermeture.destinationEtablissementAutre1).eq('autre') ? fermeture.destinationAutre2 : '';
if (fermeture.dateCessationEmploi2 !== null) {
    var dateTmp = new Date(parseInt(fermeture.dateCessationEmploi2.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['etablissement_cessationEmploi_mPrime[0]']          = date;
}
}

if (fermeture.autreFermeture2) {
formFields['etablissement_adresseVoie_mPrime[1]']             = (fermeture.cadreAdresseEtablissementFerme3.numeroAdresseEtablissement != null ? fermeture.cadreAdresseEtablissementFerme3.numeroAdresseEtablissement : '')
																		+ ' ' + (fermeture.cadreAdresseEtablissementFerme3.indiceAdresseEtablissement != null ? fermeture.cadreAdresseEtablissementFerme3.indiceAdresseEtablissement : '')
																		+ ' ' + (fermeture.cadreAdresseEtablissementFerme3.typeAdresseEtablissement != null ? fermeture.cadreAdresseEtablissementFerme3.typeAdresseEtablissement : '')
																		+ ' ' + (fermeture.cadreAdresseEtablissementFerme3.voieAdresseEtablissement != null ? fermeture.cadreAdresseEtablissementFerme3.voieAdresseEtablissement : '')
																		+ ' ' + (fermeture.cadreAdresseEtablissementFerme3.complementAdresseEtablissement != null ? fermeture.cadreAdresseEtablissementFerme3.complementAdresseEtablissement : '')
																		+ ' ' + (fermeture.cadreAdresseEtablissementFerme3.distributionSpecialeAdresseEtablissement != null ? fermeture.cadreAdresseEtablissementFerme3.distributionSpecialeAdresseEtablissement : '');
formFields['etablissement_adresseCP_mPrime[1]']                      = fermeture.cadreAdresseEtablissementFerme3.codePostalAdresseEtablissement;
formFields['etablissement_adresseCommune_mPrime[1]']        		 = fermeture.cadreAdresseEtablissementFerme3.communeAdresseEtablissement;
formFields['etablissement_destinationSuppression_mPrime[1]']       = Value('id').of(fermeture.destinationEtablissementAutre2).eq('supprime') ? true : false;
formFields['etablissement_destinationVente_mPrime[1]']             = Value('id').of(fermeture.destinationEtablissementAutre2).eq('vendu') ? true : false;
formFields['etablissement_destinationAutre_mPrime[1]']                 = Value('id').of(fermeture.destinationEtablissementAutre2).eq('autre') ? true : false;
formFields['etablissement_destinationAutreLibelle_mPrime[1]']          = Value('id').of(fermeture.destinationEtablissementAutre2).eq('autre') ? fermeture.destinationAutre3 : '';
if (fermeture.dateCessationEmploi3 !== null) {
    var dateTmp = new Date(parseInt(fermeture.dateCessationEmploi3.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['etablissement_cessationEmploi_mPrime[1]']          = date;
}
}
}

// TNS Dirigeant et conjoint associé de SARL

if (((Value('id').of(identite.entrepriseFormeJuridique).eq('5499') or Value('id').of(identite.entrepriseFormeJuridique).eq('5410')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5415')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5422')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5426')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5430')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5431')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5432')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5442')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5443')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5451')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5453')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5454')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5455')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5458')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5459')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5460')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5485'))
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M'))
or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')
and (Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5499') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5410')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5415')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5422')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5426')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5430')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5431')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5432')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5442')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5443')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5451')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5453')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5454')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5455')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5458')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5459')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5460')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5485')))) {

// Dirigeant 1 SARL

if (socialDirigeant1.voletSocialNumeroSecuriteSocialTNS != null) {

// Cadres 1

formFieldsPers1['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers1['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers1['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers1['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers1['formulaireDependanceTNS_TNS']                                      = false;
formFieldsPers1['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers1['formulaireDependanceM3SARL_TNS']                                   = true;

// Cadres 2
formFieldsPers1['entrepriseDenomination_TNS']                                       = identite.entrepriseDenomination;
formFieldsPers1['numeroUniqueIdentification_TNS']                                   = identite.siren.split(' ').join('');

// Cadre 3
formFieldsPers1['personneLieePPNomNaissance_TNS']                                    = dirigeantSARL1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
formFieldsPers1['personneLieePPNomUsage_TNS']                                        = dirigeantSARL1.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantSARL1.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < dirigeantSARL1.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantSARL1.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['personneLieePPPrenom_TNS']                                      = prenoms.toString();
formFieldsPers1['personneLieeQualite_TNS']                                       = "Gérant";

var nirDeclarant = socialDirigeant1.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers1['voletSocialNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers1['voletSocialNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}

if (Value('id').of(dirigeantSARL1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') and not Value('id').of(dirigeantSARL1.cadre2InfosConjoint.objetModifConjoint).eq('suppressionMentionConjoint')) {
formFieldsPers1['voletSocialConjointCouvertAssuranceMaladieOUI_TNS']                = dirigeantSARL1.cadre2InfosConjoint.voletSocialConjointCouvertAssuranceMaladieOui ? true : false;
formFieldsPers1['voletSocialConjointCouvertAssuranceMaladieNON_TNS']                = dirigeantSARL1.cadre2InfosConjoint.voletSocialConjointCouvertAssuranceMaladieOui ? false : true;
}

formFieldsPers1['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;
formFieldsPers1['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers1['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers1['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers1['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers1['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant1.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
if (socialDirigeant1.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers1['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant1.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers1['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant1.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers1['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant1.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant1.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTmp = new Date(parseInt(socialDirigeant1.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers1['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers1['voletSocialActiviteSimultaneeOUI_TNS']                            = socialDirigeant1.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers1['voletSocialActiviteSimultaneeNON_TNS']                            = socialDirigeant1.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant1.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers1['voletSocialActiviteSimultaneeSalarie_TNS']                        = Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers1['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                = Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers1['voletSocialActiviteSimultaneeRetraitePensionne_TNS']              = Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers1['voletSocialActiviteSimultaneeAutre_TNS'] 				          = Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers1['voletSocialActiviteSimultaneeAutrePrecision_TNS']                 = Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant1.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
}
formFieldsPers1['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant1.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers1['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant1.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers1['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant1.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers1['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant1.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

// Cadre 4

if (Value('id').of(dirigeantSARL1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur')) {
formFieldsPers1['personneLieeConjointStatutCollaborateur_TNS']                      = true;
var nirDeclarant = dirigeantSARL1.cadre2InfosConjoint.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers1['voletSocialConjointNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers1['voletSocialConjointNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}
} else if (Value('id').of(dirigeantSARL1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) {
formFieldsPers1['personneLieeConjointStatutAssocie_TNS']                            = true;
var nirDeclarant = socialAssocie1.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers1['voletSocialConjointNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers1['voletSocialConjointNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}
formFieldsPers1['personneLieePPNomNaissanceConjointAssocie_TNS']                    = dirigeantSARL1.cadre2InfosConjoint.personneLieePPNomNaissanceConjointPacse;
formFieldsPers1['personneLieePPNomUsageConjointAssocie_TNS']                        = dirigeantSARL1.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse != null ? dirigeantSARL1.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < dirigeantSARL1.cadre2InfosConjoint.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(dirigeantSARL1.cadre2InfosConjoint.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers1['personneLieePPPrenomConjointAssocie_TNS']                                      = prenoms.toString();
if (dirigeantSARL1.cadre2InfosConjoint.personneLieePPDateNaissanceConjointPacse != null) {
    var dateTmp = new Date(parseInt(dirigeantSARL1.cadre2InfosConjoint.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['personneLieePPDateNaissanceConjointAssocie_TNS']          = date;
}
formFieldsPers1['personneLieePPLieuNaissanceCommuneConjointAssocie_TNS']               = dirigeantSARL1.cadre2InfosConjoint.personneLieePPLieuNaissanceCommnuneConjointPacse != null ? 
																						(dirigeantSARL1.cadre2InfosConjoint.personneLieePPLieuNaissanceCommnuneConjointPacse + ' ' + '(' + '' + dirigeantSARL1.cadre2InfosConjoint.personneLieePPDeptNaissanceConjointPacse.getId() + '' + ')') : 
																						(dirigeantSARL1.cadre2InfosConjoint.personneLieePersonnePhysiqueLieuNaissanceVilleConjointPacse + ' / ' + dirigeantSARL1.cadre2InfosConjoint.personneLieePPPaysNaissanceConjointPacse.getLabel());
formFieldsPers1['personneLieePPNationaliteConjointAssocie_TNS']                     = dirigeantSARL1.cadre2InfosConjoint.personneLieePPNationaliteConjointPacse;
}

// Cadre 5

formFieldsPers1['renseignementsComplementairesObservations_TNS']                    = '';

// Cadre 6
formFieldsPers1['renseignementsComplementairesLeDeclarant_TNS']                      = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFieldsPers1['renseignementsComplementairesLeMandataire_TNS']                     = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers1['formaliteSignataireNomPrenomDenomination_TNS']					     = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers1['formaliteSignataireAdresse_TNS']                                    = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '');
formFieldsPers1['formaliteSignataireAdresseComplement_TNS']  						 = (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
																					 + ' ' + signataire.adresseMandataire.dataCodePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;

}
if (signataire.formaliteSignatureDate !== null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['formaliteSignatureDate_TNS']          = date;
}
formFieldsPers1['formaliteSignatureLieu_TNS']                                         = signataire.formaliteSignatureLieu;
formFieldsPers1['signature_TNS']                                         = '';
}

// Dirigeant 2 SARL

if (socialDirigeant2.voletSocialNumeroSecuriteSocialTNS != null) {

// Cadres 1

formFieldsPers2['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers2['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers2['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers2['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers2['formulaireDependanceTNS_TNS']                                      = false;
formFieldsPers2['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers2['formulaireDependanceM3SARL_TNS']                                   = true;

// Cadres 2
formFieldsPers2['entrepriseDenomination_TNS']                                       = identite.entrepriseDenomination;
formFieldsPers2['numeroUniqueIdentification_TNS']                                   = identite.siren.split(' ').join('');

// Cadre 3
formFieldsPers2['personneLieePPNomNaissance_TNS']                                    = dirigeantSARL2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
formFieldsPers2['personneLieePPNomUsage_TNS']                                        = dirigeantSARL2.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantSARL2.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < dirigeantSARL2.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantSARL2.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers2['personneLieePPPrenom_TNS']                                      = prenoms.toString();
formFieldsPers2['personneLieeQualite_TNS']                                       = "Gérant";

var nirDeclarant = socialDirigeant2.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers2['voletSocialNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers2['voletSocialNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}

if (Value('id').of(dirigeantSARL2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') and not Value('id').of(dirigeantSARL2.cadre2InfosConjoint.objetModifConjoint).eq('suppressionMentionConjoint')) {
formFieldsPers2['voletSocialConjointCouvertAssuranceMaladieOUI_TNS']                = dirigeantSARL2.cadre2InfosConjoint.voletSocialConjointCouvertAssuranceMaladieOui ? true : false;
formFieldsPers2['voletSocialConjointCouvertAssuranceMaladieNON_TNS']                = dirigeantSARL2.cadre2InfosConjoint.voletSocialConjointCouvertAssuranceMaladieOui ? false : true;
}

formFieldsPers2['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;
formFieldsPers2['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers2['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers2['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers2['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers2['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant2.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
if (socialDirigeant2.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers2['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant2.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers2['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant2.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers2['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant2.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant2.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTmp = new Date(parseInt(socialDirigeant2.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers2['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers2['voletSocialActiviteSimultaneeOUI_TNS']                            = socialDirigeant2.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers2['voletSocialActiviteSimultaneeNON_TNS']                            = socialDirigeant2.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant2.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers2['voletSocialActiviteSimultaneeSalarie_TNS']                        = Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers2['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                = Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers2['voletSocialActiviteSimultaneeRetraitePensionne_TNS']              = Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers2['voletSocialActiviteSimultaneeAutre_TNS'] 				           = Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers2['voletSocialActiviteSimultaneeAutrePrecision_TNS']                 = Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant2.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
}
formFieldsPers2['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant2.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers2['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant2.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers2['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant2.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers2['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant2.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

// Cadre 4

if (Value('id').of(dirigeantSARL2.conjoint.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur')) {
formFieldsPers2['personneLieeConjointStatutCollaborateur_TNS']                      = true;
var nirDeclarant = dirigeantSARL2.cadre2InfosConjoint.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers2['voletSocialConjointNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers2['voletSocialConjointNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}
} else if (Value('id').of(dirigeantSARL2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) {
formFieldsPers2['personneLieeConjointStatutAssocie_TNS']                            = true;
var nirDeclarant = socialAssocie2.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers2['voletSocialConjointNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers2['voletSocialConjointNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}
formFieldsPers2['personneLieePPNomNaissanceConjointAssocie_TNS']                    = dirigeantSARL2.cadre2InfosConjoint.personneLieePPNomNaissanceConjointPacse;
formFieldsPers2['personneLieePPNomUsageConjointAssocie_TNS']                        = dirigeantSARL2.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse != null ? dirigeantSARL2.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < dirigeantSARL2.cadre2InfosConjoint.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(dirigeantSARL2.cadre2InfosConjoint.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers2['personneLieePPPrenomConjointAssocie_TNS']                                      = prenoms.toString();
if (dirigeantSARL2.cadre2InfosConjoint.personneLieePPDateNaissanceConjointPacse != null) {
    var dateTmp = new Date(parseInt(dirigeantSARL2.cadre2InfosConjoint.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['personneLieePPDateNaissanceConjointAssocie_TNS']          = date;
}
formFieldsPers2['personneLieePPLieuNaissanceCommuneConjointAssocie_TNS']         = dirigeantSARL2.cadre2InfosConjoint.personneLieePPLieuNaissanceCommnuneConjointPacse != null ? 
																					(dirigeantSARL2.cadre2InfosConjoint.personneLieePPLieuNaissanceCommnuneConjointPacse + ' ' + '(' + '' + dirigeantSARL2.cadre2InfosConjoint.personneLieePPDeptNaissanceConjointPacse.getId() + '' + ')') : 
																					(dirigeantSARL2.cadre2InfosConjoint.personneLieePersonnePhysiqueLieuNaissanceVilleConjointPacse + ' / ' + dirigeantSARL2.cadre2InfosConjoint.personneLieePPPaysNaissanceConjointPacse.getLabel());
formFieldsPers2['personneLieePPNationaliteConjointAssocie_TNS']                      = dirigeantSARL2.cadre2InfosConjoint.personneLieePPNationaliteConjointPacse;
}

// Cadre 5

formFieldsPers2['renseignementsComplementairesObservations_TNS']                    = '';

// Cadre 6
formFieldsPers2['renseignementsComplementairesLeDeclarant_TNS']                      = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFieldsPers2['renseignementsComplementairesLeMandataire_TNS']                     = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers2['formaliteSignataireNomPrenomDenomination_TNS']					     = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers2['formaliteSignataireAdresse_TNS']                                    = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '');
formFieldsPers2['formaliteSignataireAdresseComplement_TNS']  						 = (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
																					 + ' ' + signataire.adresseMandataire.dataCodePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
if (signataire.formaliteSignatureDate !== null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['formaliteSignatureDate_TNS']          = date;
}
formFieldsPers2['formaliteSignatureLieu_TNS']                                         = signataire.formaliteSignatureLieu;
formFieldsPers2['signature_TNS']                                         = '';
}

// Dirigeant 3 SARL

if (socialDirigeant3.voletSocialNumeroSecuriteSocialTNS != null) {

// Cadres 1

formFieldsPers3['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers3['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers3['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers3['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers3['formulaireDependanceTNS_TNS']                                      = false;
formFieldsPers3['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers3['formulaireDependanceM3SARL_TNS']                                   = true;

// Cadres 2
formFieldsPers3['entrepriseDenomination_TNS']                                       = identite.entrepriseDenomination;
formFieldsPers3['numeroUniqueIdentification_TNS']                                   = identite.siren.split(' ').join('');

// Cadre 3
formFieldsPers3['personneLieePPNomNaissance_TNS']                                    = dirigeantSARL3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
formFieldsPers3['personneLieePPNomUsage_TNS']                                        = dirigeantSARL3.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantSARL3.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < dirigeantSARL3.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantSARL3.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers3['personneLieePPPrenom_TNS']                                      = prenoms.toString();
formFieldsPers3['personneLieeQualite_TNS']                                       = "Gérant";

var nirDeclarant = socialDirigeant3.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers3['voletSocialNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers3['voletSocialNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}

if (Value('id').of(dirigeantSARL3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') and not Value('id').of(dirigeantSARL3.cadre2InfosConjoint.objetModifConjoint).eq('suppressionMentionConjoint')) {
formFieldsPers3['voletSocialConjointCouvertAssuranceMaladieOUI_TNS']                = dirigeantSARL3.cadre2InfosConjoint.voletSocialConjointCouvertAssuranceMaladieOui ? true : false;
formFieldsPers3['voletSocialConjointCouvertAssuranceMaladieNON_TNS']                = dirigeantSARL3.cadre2InfosConjoint.voletSocialConjointCouvertAssuranceMaladieOui ? false : true;
}

formFieldsPers3['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;
formFieldsPers3['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers3['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers3['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers3['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers3['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant3.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
if (socialDirigeant3.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers3['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant3.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers3['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant3.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers3['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant3.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant3.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTmp = new Date(parseInt(socialDirigeant3.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers3['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers3['voletSocialActiviteSimultaneeOUI_TNS']                            = socialDirigeant3.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers3['voletSocialActiviteSimultaneeNON_TNS']                            = socialDirigeant3.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant3.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers3['voletSocialActiviteSimultaneeSalarie_TNS']                        = Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers3['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                = Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers3['voletSocialActiviteSimultaneeRetraitePensionne_TNS']              = Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers3['voletSocialActiviteSimultaneeAutre_TNS'] 				           = Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers3['voletSocialActiviteSimultaneeAutrePrecision_TNS']                 = Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant3.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
}
formFieldsPers3['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant3.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers3['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant3.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers3['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant3.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers3['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant3.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

// Cadre 4

if (Value('id').of(dirigeantSARL3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur')) {
formFieldsPers3['personneLieeConjointStatutCollaborateur_TNS']                      = true;
var nirDeclarant = dirigeantSARL3.cadre2InfosConjoint.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers3['voletSocialConjointNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers3['voletSocialConjointNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}
} else if (Value('id').of(dirigeantSARL3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) {
formFieldsPers3['personneLieeConjointStatutAssocie_TNS']                            = true;
var nirDeclarant = socialAssocie3.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers3['voletSocialConjointNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers3['voletSocialConjointNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}
formFieldsPers3['personneLieePPNomNaissanceConjointAssocie_TNS']                    = dirigeantSARL3.cadre2InfosConjoint.personneLieePPNomNaissanceConjointPacse;
formFieldsPers3['personneLieePPNomUsageConjointAssocie_TNS']                        = dirigeantSARL3.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse != null ? dirigeantSARL3.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < dirigeantSARL3.cadre2InfosConjoint.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(dirigeantSARL3.cadre2InfosConjoint.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers3['personneLieePPPrenomConjointAssocie_TNS']                                      = prenoms.toString();
if (dirigeantSARL3.cadre2InfosConjoint.personneLieePPDateNaissanceConjointPacse != null) {
    var dateTmp = new Date(parseInt(dirigeantSARL3.cadre2InfosConjoint.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers3['personneLieePPDateNaissanceConjointAssocie_TNS']          = date;
}
formFieldsPers3['personneLieePPLieuNaissanceCommuneConjointAssocie_TNS']         = dirigeantSARL3.cadre2InfosConjoint.personneLieePPLieuNaissanceCommnuneConjointPacse != null ? 
																					(dirigeantSARL3.cadre2InfosConjoint.personneLieePPLieuNaissanceCommnuneConjointPacse + ' ' + '(' + '' + dirigeantSARL3.cadre2InfosConjoint.personneLieePPDeptNaissanceConjointPacse.getId() + '' + ')') : 
																					(dirigeantSARL3.cadre2InfosConjoint.personneLieePersonnePhysiqueLieuNaissanceVilleConjointPacse + ' / ' + dirigeantSARL3.cadre2InfosConjoint.personneLieePPPaysNaissanceConjointPacse.getLabel());
formFieldsPers3['personneLieePPNationaliteConjointAssocie_TNS']                      = dirigeantSARL3.cadre2InfosConjoint.personneLieePPNationaliteConjointPacse;
}

// Cadre 5

formFieldsPers3['renseignementsComplementairesObservations_TNS']                    = '';

// Cadre 6
formFieldsPers3['renseignementsComplementairesLeDeclarant_TNS']                      = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFieldsPers3['renseignementsComplementairesLeMandataire_TNS']                     = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers3['formaliteSignataireNomPrenomDenomination_TNS']					     = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers3['formaliteSignataireAdresse_TNS']                                    = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '');
formFieldsPers3['formaliteSignataireAdresseComplement_TNS']  						 = (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
																					 + ' ' + signataire.adresseMandataire.dataCodePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
if (signataire.formaliteSignatureDate !== null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers3['formaliteSignatureDate_TNS']          = date;
}
formFieldsPers3['formaliteSignatureLieu_TNS']                                         = signataire.formaliteSignatureLieu;
formFieldsPers3['signature_TNS']                                         = '';
}

// Dirigeant 4 SARL

if (socialDirigeant4.voletSocialNumeroSecuriteSocialTNS != null) {

// Cadres 1

formFieldsPers4['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers4['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers4['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers4['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers4['formulaireDependanceTNS_TNS']                                      = false;
formFieldsPers4['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers4['formulaireDependanceM3SARL_TNS']                                   = true;

// Cadres 2
formFieldsPers4['entrepriseDenomination_TNS']                                       = identite.entrepriseDenomination;
formFieldsPers4['numeroUniqueIdentification_TNS']                                   = identite.siren.split(' ').join('');

// Cadre 3
formFieldsPers4['personneLieePPNomNaissance_TNS']                                    = dirigeantSARL4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
formFieldsPers4['personneLieePPNomUsage_TNS']                                        = dirigeantSARL4.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantSARL4.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < dirigeantSARL4.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantSARL4.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers4['personneLieePPPrenom_TNS']                                      = prenoms.toString();
formFieldsPers4['personneLieeQualite_TNS']                                       = "Gérant";

var nirDeclarant = socialDirigeant4.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers4['voletSocialNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers4['voletSocialNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}

if (Value('id').of(dirigeantSARL4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') and not Value('id').of(dirigeantSARL4.cadre2InfosConjoint.objetModifConjoint).eq('suppressionMentionConjoint')) {
formFieldsPers4['voletSocialConjointCouvertAssuranceMaladieOUI_TNS']                = dirigeantSARL4.cadre2InfosConjoint.voletSocialConjointCouvertAssuranceMaladieOui ? true : false;
formFieldsPers4['voletSocialConjointCouvertAssuranceMaladieNON_TNS']                = dirigeantSARL4.cadre2InfosConjoint.voletSocialConjointCouvertAssuranceMaladieOui ? false : true;
}

formFieldsPers4['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;
formFieldsPers4['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers4['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers4['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers4['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers4['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant4.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
if (socialDirigeant4.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers4['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant4.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers4['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant4.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers4['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant4.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant4.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTmp = new Date(parseInt(socialDirigeant4.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers4['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers4['voletSocialActiviteSimultaneeOUI_TNS']                            = socialDirigeant4.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers4['voletSocialActiviteSimultaneeNON_TNS']                            = socialDirigeant4.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant4.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers4['voletSocialActiviteSimultaneeSalarie_TNS']                        = Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers4['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                = Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers4['voletSocialActiviteSimultaneeRetraitePensionne_TNS']              = Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers4['voletSocialActiviteSimultaneeAutre_TNS'] 				           = Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers4['voletSocialActiviteSimultaneeAutrePrecision_TNS']                 = Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant4.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
}
formFieldsPers4['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant4.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers4['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant4.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers4['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant4.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers4['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant4.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

// Cadre 4

if (Value('id').of(dirigeantSARL4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur')) {
formFieldsPers4['personneLieeConjointStatutCollaborateur_TNS']                      = true;
var nirDeclarant = dirigeantSARL4.cadre2InfosConjoint.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers4['voletSocialConjointNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers4['voletSocialConjointNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}
} else if (Value('id').of(dirigeantSARL4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) {
formFieldsPers4['personneLieeConjointStatutAssocie_TNS']                            = true;
var nirDeclarant = socialAssocie4.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers4['voletSocialConjointNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers4['voletSocialConjointNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}
formFieldsPers4['personneLieePPNomNaissanceConjointAssocie_TNS']                    = dirigeantSARL4.cadre2InfosConjoint.personneLieePPNomNaissanceConjointPacse;
formFieldsPers4['personneLieePPNomUsageConjointAssocie_TNS']                        = dirigeantSARL4.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse != null ? dirigeantSARL4.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < dirigeantSARL4.cadre2InfosConjoint.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(dirigeantSARL4.cadre2InfosConjoint.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers4['personneLieePPPrenomConjointAssocie_TNS']                                      = prenoms.toString();
if (dirigeantSARL4.cadre2InfosConjoint.personneLieePPDateNaissanceConjointPacse != null) {
    var dateTmp = new Date(parseInt(dirigeantSARL4.cadre2InfosConjoint.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers4['personneLieePPDateNaissanceConjointAssocie_TNS']          = date;
}
formFieldsPers4['personneLieePPLieuNaissanceCommuneConjointAssocie_TNS']         = dirigeantSARL4.cadre2InfosConjoint.personneLieePPLieuNaissanceCommnuneConjointPacse != null ? 
																					(dirigeantSARL4.cadre2InfosConjoint.personneLieePPLieuNaissanceCommnuneConjointPacse + ' ' + '(' + '' + dirigeantSARL4.cadre2InfosConjoint.personneLieePPDeptNaissanceConjointPacse.getId() + '' + ')') : 
																					(dirigeantSARL4.cadre2InfosConjoint.personneLieePersonnePhysiqueLieuNaissanceVilleConjointPacse + ' / ' + dirigeantSARL4.cadre2InfosConjoint.personneLieePPPaysNaissanceConjointPacse.getLabel());
formFieldsPers4['personneLieePPNationaliteConjointAssocie_TNS']                      = dirigeantSARL4.cadre2InfosConjoint.personneLieePPNationaliteConjointPacse;
}

// Cadre 5

formFieldsPers4['renseignementsComplementairesObservations_TNS']                    = '';

// Cadre 6
formFieldsPers4['renseignementsComplementairesLeDeclarant_TNS']                      = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFieldsPers4['renseignementsComplementairesLeMandataire_TNS']                     = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers4['formaliteSignataireNomPrenomDenomination_TNS']					     = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers4['formaliteSignataireAdresse_TNS']                                    = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '');
formFieldsPers4['formaliteSignataireAdresseComplement_TNS']  						 = (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
																					 + ' ' + signataire.adresseMandataire.dataCodePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
if (signataire.formaliteSignatureDate !== null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers4['formaliteSignatureDate_TNS']          = date;
}
formFieldsPers4['formaliteSignatureLieu_TNS']                                         = signataire.formaliteSignatureLieu;
formFieldsPers4['signature_TNS']                                         = '';
}

// Dirigeant 5 SARL

if (socialDirigeant5.voletSocialNumeroSecuriteSocialTNS != null) {

// Cadres 1

formFieldsPers5['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers5['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers5['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers5['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers5['formulaireDependanceTNS_TNS']                                      = false;
formFieldsPers5['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers5['formulaireDependanceM3SARL_TNS']                                   = true;

// Cadres 2
formFieldsPers5['entrepriseDenomination_TNS']                                       = identite.entrepriseDenomination;
formFieldsPers5['numeroUniqueIdentification_TNS']                                   = identite.siren.split(' ').join('');

// Cadre 3
formFieldsPers5['personneLieePPNomNaissance_TNS']                                    = dirigeantSARL5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
formFieldsPers5['personneLieePPNomUsage_TNS']                                        = dirigeantSARL5.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeantSARL5.civilitePersonnePhysique.personneLieePPNomUsageGerant : '';
var prenoms=[];
for ( i = 0; i < dirigeantSARL5.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantSARL5.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers5['personneLieePPPrenom_TNS']                                      = prenoms.toString();
formFieldsPers5['personneLieeQualite_TNS']                                       = "Gérant";

var nirDeclarant = socialDirigeant5.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers5['voletSocialNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers5['voletSocialNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}

if (Value('id').of(dirigeantSARL5.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') and not Value('id').of(dirigeantSARL5.cadre2InfosConjoint.objetModifConjoint).eq('suppressionMentionConjoint')) {
formFieldsPers5['voletSocialConjointCouvertAssuranceMaladieOUI_TNS']                = dirigeantSARL5.cadre2InfosConjoint.voletSocialConjointCouvertAssuranceMaladieOui ? true : false;
formFieldsPers5['voletSocialConjointCouvertAssuranceMaladieNON_TNS']                = dirigeantSARL5.cadre2InfosConjoint.voletSocialConjointCouvertAssuranceMaladieOui ? false : true;
}

formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;
formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant5.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
if (socialDirigeant5.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers5['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant5.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers5['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant5.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers5['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant5.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant5.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTmp = new Date(parseInt(socialDirigeant5.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers5['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers5['voletSocialActiviteSimultaneeOUI_TNS']                            = socialDirigeant5.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers5['voletSocialActiviteSimultaneeNON_TNS']                            = socialDirigeant5.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant5.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers5['voletSocialActiviteSimultaneeSalarie_TNS']                        = Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers5['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                = Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers5['voletSocialActiviteSimultaneeRetraitePensionne_TNS']              = Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers5['voletSocialActiviteSimultaneeAutre_TNS'] 				           = Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers5['voletSocialActiviteSimultaneeAutrePrecision_TNS']                 = Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant5.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
}
formFieldsPers5['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant5.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers5['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant5.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers5['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant5.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers5['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant5.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

// Cadre 4

if (Value('id').of(dirigeantSARL5.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur')) {
formFieldsPers5['personneLieeConjointStatutCollaborateur_TNS']                      = true;
var nirDeclarant = dirigeantSARL5.cadre2InfosConjoint.voletSocialConjointCollaborateurNumeroSecuriteSociale;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers5['voletSocialConjointNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers5['voletSocialConjointNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}
} else if (Value('id').of(dirigeantSARL5.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) {
formFieldsPers5['personneLieeConjointStatutAssocie_TNS']                            = true;
var nirDeclarant = socialAssocie5.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers5['voletSocialConjointNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers5['voletSocialConjointNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}
formFieldsPers5['personneLieePPNomNaissanceConjointAssocie_TNS']                    = dirigeantSARL5.cadre2InfosConjoint.personneLieePPNomNaissanceConjointPacse;
formFieldsPers5['personneLieePPNomUsageConjointAssocie_TNS']                        = dirigeantSARL5.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse != null ? dirigeantSARL5.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < dirigeantSARL5.cadre2InfosConjoint.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(dirigeantSARL5.cadre2InfosConjoint.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers5['personneLieePPPrenomConjointAssocie_TNS']                                      = prenoms.toString();
if (dirigeantSARL5.cadre2InfosConjoint.personneLieePPDateNaissanceConjointPacse != null) {
    var dateTmp = new Date(parseInt(dirigeantSARL5.cadre2InfosConjoint.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers5['personneLieePPDateNaissanceConjointAssocie_TNS']          = date;
}
formFieldsPers5['personneLieePPLieuNaissanceCommuneConjointAssocie_TNS']         = dirigeantSARL5.cadre2InfosConjoint.personneLieePPLieuNaissanceCommnuneConjointPacse != null ? 
																					(dirigeantSARL5.cadre2InfosConjoint.personneLieePPLieuNaissanceCommnuneConjointPacse + ' ' + '(' + '' + dirigeantSARL5.cadre2InfosConjoint.personneLieePPDeptNaissanceConjointPacse.getId() + '' + ')') : 
																					(dirigeantSARL5.cadre2InfosConjoint.personneLieePersonnePhysiqueLieuNaissanceVilleConjointPacse + ' / ' + dirigeantSARL5.cadre2InfosConjoint.personneLieePPPaysNaissanceConjointPacse.getLabel());
formFieldsPers5['personneLieePPNationaliteConjointAssocie_TNS']                      = dirigeantSARL5.cadre2InfosConjoint.personneLieePPNationaliteConjointPacse;
}

// Cadre 5

formFieldsPers5['renseignementsComplementairesObservations_TNS']                    = '';

// Cadre 6
formFieldsPers5['renseignementsComplementairesLeDeclarant_TNS']                      = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFieldsPers5['renseignementsComplementairesLeMandataire_TNS']                     = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers5['formaliteSignataireNomPrenomDenomination_TNS']					     = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers5['formaliteSignataireAdresse_TNS']                                    = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '');
formFieldsPers5['formaliteSignataireAdresseComplement_TNS']  						 = (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
																					 + ' ' + signataire.adresseMandataire.dataCodePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
if (signataire.formaliteSignatureDate !== null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers5['formaliteSignatureDate_TNS']          = date;
}
formFieldsPers5['formaliteSignatureLieu_TNS']                                         = signataire.formaliteSignatureLieu;
formFieldsPers5['signature_TNS']                                         = '';
}

// Conjoint Associé 1 SARL

if (socialAssocie1.voletSocialNumeroSecuriteSocialTNS != null) {

// Cadres 1

formFieldsPers13['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers13['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers13['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers13['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers13['formulaireDependanceTNS_TNS']                                      = false;
formFieldsPers13['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers13['formulaireDependanceM3SARL_TNS']                                   = true;

// Cadres 2
formFieldsPers13['entrepriseDenomination_TNS']                                       = identite.entrepriseDenomination;
formFieldsPers13['numeroUniqueIdentification_TNS']                                   = identite.siren.split(' ').join('');

// Cadre 3
formFieldsPers13['personneLieePPNomNaissance_TNS']                                    = dirigeantSARL1.cadre2InfosConjoint.personneLieePPNomNaissanceConjointPacse;
formFieldsPers13['personneLieePPNomUsage_TNS']                                        = dirigeantSARL1.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse != null ? dirigeantSARL1.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < dirigeantSARL1.cadre2InfosConjoint.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(dirigeantSARL1.cadre2InfosConjoint.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers13['personneLieePPPrenom_TNS']                                      = prenoms.toString();
formFieldsPers13['personneLieeQualite_TNS']                                       = "Conjoint associé";

var nirDeclarant = socialAssocie1.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers13['voletSocialNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers13['voletSocialNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}
formFieldsPers13['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialAssocie1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;
formFieldsPers13['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialAssocie1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers13['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialAssocie1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers13['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialAssocie1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers13['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialAssocie1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers13['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = Value('id').of(socialAssocie1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialAssocie1.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
if (socialAssocie1.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers13['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialAssocie1.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers13['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialAssocie1.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers13['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialAssocie1.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialAssocie1.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTmp = new Date(parseInt(socialAssocie1.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers13['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers13['voletSocialActiviteSimultaneeOUI_TNS']                            = socialAssocie1.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers13['voletSocialActiviteSimultaneeNON_TNS']                            = socialAssocie1.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialAssocie1.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers13['voletSocialActiviteSimultaneeSalarie_TNS']                        = Value('id').of(socialAssocie1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers13['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                = Value('id').of(socialAssocie1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers13['voletSocialActiviteSimultaneeRetraitePensionne_TNS']              = Value('id').of(socialAssocie1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers13['voletSocialActiviteSimultaneeAutre_TNS'] 				           = Value('id').of(socialAssocie1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers13['voletSocialActiviteSimultaneeAutrePrecision_TNS']                 = Value('id').of(socialAssocie1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialAssocie1.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
}
formFieldsPers13['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialAssocie1.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers13['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialAssocie1.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers13['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialAssocie1.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers13['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialAssocie1.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

// Cadre 5

formFieldsPers13['renseignementsComplementairesObservations_TNS']                    = '';

// Cadre 6
formFieldsPers13['renseignementsComplementairesLeDeclarant_TNS']                      = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFieldsPers13['renseignementsComplementairesLeMandataire_TNS']                     = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers13['formaliteSignataireNomPrenomDenomination_TNS']					     = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers13['formaliteSignataireAdresse_TNS']                                    = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '');
formFieldsPers13['formaliteSignataireAdresseComplement_TNS']  						 = (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
																					 + ' ' + signataire.adresseMandataire.dataCodePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
if (signataire.formaliteSignatureDate !== null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers13['formaliteSignatureDate_TNS']          = date;
}
formFieldsPers13['formaliteSignatureLieu_TNS']                                         = signataire.formaliteSignatureLieu;
formFieldsPers13['signature_TNS']                                         = '';
}

// Conjoint Associé 2 SARL

if (socialAssocie2.voletSocialNumeroSecuriteSocialTNS != null) {

// Cadres 1

formFieldsPers14['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers14['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers14['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers14['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers14['formulaireDependanceTNS_TNS']                                      = false;
formFieldsPers14['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers14['formulaireDependanceM3SARL_TNS']                                   = true;

// Cadres 2
formFieldsPers14['entrepriseDenomination_TNS']                                       = identite.entrepriseDenomination;
formFieldsPers14['numeroUniqueIdentification_TNS']                                   = identite.siren.split(' ').join('');

// Cadre 3
formFieldsPers14['personneLieePPNomNaissance_TNS']                                    = dirigeantSARL2.cadre2InfosConjoint.personneLieePPNomNaissanceConjointPacse;
formFieldsPers14['personneLieePPNomUsage_TNS']                                        = dirigeantSARL2.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse != null ? dirigeantSARL2.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < dirigeantSARL2.cadre2InfosConjoint.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(dirigeantSARL2.cadre2InfosConjoint.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers14['personneLieePPPrenom_TNS']                                      = prenoms.toString();
formFieldsPers14['personneLieeQualite_TNS']                                       = "Conjoint associé";

var nirDeclarant = socialAssocie2.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers14['voletSocialNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers14['voletSocialNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}
formFieldsPers14['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialAssocie2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;
formFieldsPers14['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialAssocie2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers14['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialAssocie2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers14['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialAssocie2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers14['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialAssocie2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers14['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = Value('id').of(socialAssocie2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialAssocie2.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
if (socialAssocie2.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers14['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialAssocie2.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers14['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialAssocie2.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers14['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialAssocie2.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialAssocie2.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTmp = new Date(parseInt(socialAssocie2.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers14['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers14['voletSocialActiviteSimultaneeOUI_TNS']                            = socialAssocie2.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers14['voletSocialActiviteSimultaneeNON_TNS']                            = socialAssocie2.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialAssocie2.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers14['voletSocialActiviteSimultaneeSalarie_TNS']                        = Value('id').of(socialAssocie2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers14['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                = Value('id').of(socialAssocie2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers14['voletSocialActiviteSimultaneeRetraitePensionne_TNS']              = Value('id').of(socialAssocie2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers14['voletSocialActiviteSimultaneeAutre_TNS'] 				            = Value('id').of(socialAssocie2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers14['voletSocialActiviteSimultaneeAutrePrecision_TNS']                 = Value('id').of(socialAssocie2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialAssocie2.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
}
formFieldsPers14['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialAssocie2.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers14['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialAssocie2.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers14['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialAssocie2.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers14['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialAssocie2.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

// Cadre 5

formFieldsPers14['renseignementsComplementairesObservations_TNS']                    = '';

// Cadre 6
formFieldsPers14['renseignementsComplementairesLeDeclarant_TNS']                      = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFieldsPers14['renseignementsComplementairesLeMandataire_TNS']                     = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers14['formaliteSignataireNomPrenomDenomination_TNS']					     = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers14['formaliteSignataireAdresse_TNS']                                    = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '');
formFieldsPers14['formaliteSignataireAdresseComplement_TNS']  						 = (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
																					 + ' ' + signataire.adresseMandataire.dataCodePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
if (signataire.formaliteSignatureDate !== null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers14['formaliteSignatureDate_TNS']          = date;
}
formFieldsPers14['formaliteSignatureLieu_TNS']                                         = signataire.formaliteSignatureLieu;
formFieldsPers14['signature_TNS']                                         = '';
}

// Conjoint Associé 3 SARL

if (socialAssocie3.voletSocialNumeroSecuriteSocialTNS != null) {

// Cadres 1

formFieldsPers15['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers15['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers15['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers15['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers15['formulaireDependanceTNS_TNS']                                      = false;
formFieldsPers15['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers15['formulaireDependanceM3SARL_TNS']                                   = true;

// Cadres 2
formFieldsPers15['entrepriseDenomination_TNS']                                       = identite.entrepriseDenomination;
formFieldsPers15['numeroUniqueIdentification_TNS']                                   = identite.siren.split(' ').join('');

// Cadre 3
formFieldsPers15['personneLieePPNomNaissance_TNS']                                    = dirigeantSARL3.cadre2InfosConjoint.personneLieePPNomNaissanceConjointPacse;
formFieldsPers15['personneLieePPNomUsage_TNS']                                        = dirigeantSARL3.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse != null ? dirigeantSARL3.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < dirigeantSARL3.cadre2InfosConjoint.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(dirigeantSARL3.cadre2InfosConjoint.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers15['personneLieePPPrenom_TNS']                                      = prenoms.toString();
formFieldsPers15['personneLieeQualite_TNS']                                       = "Conjoint associé";

var nirDeclarant = socialAssocie3.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers15['voletSocialNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers15['voletSocialNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}
formFieldsPers15['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialAssocie3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;
formFieldsPers15['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialAssocie3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers15['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialAssocie3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers15['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialAssocie3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers15['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialAssocie3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers15['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = Value('id').of(socialAssocie3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialAssocie3.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
if (socialAssocie3.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers15['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialAssocie3.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers15['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialAssocie3.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers15['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialAssocie3.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialAssocie3.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTmp = new Date(parseInt(socialAssocie3.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers15['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers15['voletSocialActiviteSimultaneeOUI_TNS']                            = socialAssocie3.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers15['voletSocialActiviteSimultaneeNON_TNS']                            = socialAssocie3.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialAssocie3.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers15['voletSocialActiviteSimultaneeSalarie_TNS']                        = Value('id').of(socialAssocie3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers15['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                = Value('id').of(socialAssocie3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers15['voletSocialActiviteSimultaneeRetraitePensionne_TNS']              = Value('id').of(socialAssocie3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers15['voletSocialActiviteSimultaneeAutre_TNS'] 				           = Value('id').of(socialAssocie3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers15['voletSocialActiviteSimultaneeAutrePrecision_TNS']                 = Value('id').of(socialAssocie3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialAssocie3.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
}
formFieldsPers15['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialAssocie3.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers15['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialAssocie3.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers15['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialAssocie3.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers15['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialAssocie3.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

// Cadre 5

formFieldsPers15['renseignementsComplementairesObservations_TNS']                    = '';

// Cadre 6
formFieldsPers15['renseignementsComplementairesLeDeclarant_TNS']                      = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFieldsPers15['renseignementsComplementairesLeMandataire_TNS']                     = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers15['formaliteSignataireNomPrenomDenomination_TNS']					     = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers15['formaliteSignataireAdresse_TNS']                                    = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '');
formFieldsPers15['formaliteSignataireAdresseComplement_TNS']  						 = (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
																					 + ' ' + signataire.adresseMandataire.dataCodePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
if (signataire.formaliteSignatureDate !== null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers15['formaliteSignatureDate_TNS']          = date;
}
formFieldsPers15['formaliteSignatureLieu_TNS']                                         = signataire.formaliteSignatureLieu;
formFieldsPers15['signature_TNS']                                         = '';
}

// Conjoint Associé 4 SARL

if (socialAssocie4.voletSocialNumeroSecuriteSocialTNS != null) {

// Cadres 1

formFieldsPers16['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers16['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers16['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers16['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers16['formulaireDependanceTNS_TNS']                                      = false;
formFieldsPers16['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers16['formulaireDependanceM3SARL_TNS']                                   = true;

// Cadres 2
formFieldsPers16['entrepriseDenomination_TNS']                                       = identite.entrepriseDenomination;
formFieldsPers16['numeroUniqueIdentification_TNS']                                   = identite.siren.split(' ').join('');

// Cadre 3
formFieldsPers16['personneLieePPNomNaissance_TNS']                                    = dirigeantSARL4.cadre2InfosConjoint.personneLieePPNomNaissanceConjointPacse;
formFieldsPers16['personneLieePPNomUsage_TNS']                                        = dirigeantSARL4.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse != null ? dirigeantSARL4.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < dirigeantSARL4.cadre2InfosConjoint.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(dirigeantSARL4.cadre2InfosConjoint.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers16['personneLieePPPrenom_TNS']                                      = prenoms.toString();
formFieldsPers16['personneLieeQualite_TNS']                                       = "Conjoint associé";

var nirDeclarant = socialAssocie4.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers16['voletSocialNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers16['voletSocialNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}
formFieldsPers16['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialAssocie4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;
formFieldsPers16['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialAssocie4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers16['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialAssocie4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers16['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialAssocie4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers16['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialAssocie4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers16['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = Value('id').of(socialAssocie4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialAssocie4.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
if (socialAssocie4.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers16['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialAssocie4.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers16['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialAssocie4.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers16['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialAssocie4.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialAssocie4.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTmp = new Date(parseInt(socialAssocie4.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers16['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers16['voletSocialActiviteSimultaneeOUI_TNS']                            = socialAssocie4.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers16['voletSocialActiviteSimultaneeNON_TNS']                            = socialAssocie4.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialAssocie4.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers16['voletSocialActiviteSimultaneeSalarie_TNS']                        = Value('id').of(socialAssocie4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers16['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                = Value('id').of(socialAssocie4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers16['voletSocialActiviteSimultaneeRetraitePensionne_TNS']              = Value('id').of(socialAssocie4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers16['voletSocialActiviteSimultaneeAutre_TNS'] 				           = Value('id').of(socialAssocie4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers16['voletSocialActiviteSimultaneeAutrePrecision_TNS']                 = Value('id').of(socialAssocie4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialAssocie4.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
}
formFieldsPers16['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialAssocie4.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers16['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialAssocie4.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers16['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialAssocie4.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers16['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialAssocie4.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

// Cadre 5

formFieldsPers16['renseignementsComplementairesObservations_TNS']                    = '';

// Cadre 6
formFieldsPers16['renseignementsComplementairesLeDeclarant_TNS']                      = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFieldsPers16['renseignementsComplementairesLeMandataire_TNS']                     = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers16['formaliteSignataireNomPrenomDenomination_TNS']					     = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers16['formaliteSignataireAdresse_TNS']                                    = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '');
formFieldsPers16['formaliteSignataireAdresseComplement_TNS']  						 = (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
																					 + ' ' + signataire.adresseMandataire.dataCodePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
if (signataire.formaliteSignatureDate !== null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers16['formaliteSignatureDate_TNS']          = date;
}
formFieldsPers16['formaliteSignatureLieu_TNS']                                         = signataire.formaliteSignatureLieu;
formFieldsPers16['signature_TNS']                                         = '';
}

// Conjoint Associé 5 SARL

if (socialAssocie5.voletSocialNumeroSecuriteSocialTNS != null) {

// Cadres 1

formFieldsPers17['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers17['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers17['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers17['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers17['formulaireDependanceTNS_TNS']                                      = false;
formFieldsPers17['formulaireDependanceM3_TNS']                                       = false;
formFieldsPers17['formulaireDependanceM3SARL_TNS']                                   = true;

// Cadres 2
formFieldsPers17['entrepriseDenomination_TNS']                                       = identite.entrepriseDenomination;
formFieldsPers17['numeroUniqueIdentification_TNS']                                   = identite.siren.split(' ').join('');

// Cadre 3
formFieldsPers17['personneLieePPNomNaissance_TNS']                                    = dirigeantSARL5.cadre2InfosConjoint.personneLieePPNomNaissanceConjointPacse;
formFieldsPers17['personneLieePPNomUsage_TNS']                                        = dirigeantSARL5.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse != null ? dirigeantSARL5.cadre2InfosConjoint.personneLieePPNomUsageConjointPacse : '';
var prenoms=[];
for ( i = 0; i < dirigeantSARL5.cadre2InfosConjoint.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(dirigeantSARL5.cadre2InfosConjoint.personneLieePPPrenomConjointPacse[i]);}                            
formFieldsPers17['personneLieePPPrenom_TNS']                                      = prenoms.toString();
formFieldsPers17['personneLieeQualite_TNS']                                       = "Conjoint associé";

var nirDeclarant = socialAssocie5.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers17['voletSocialNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers17['voletSocialNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}
formFieldsPers17['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialAssocie5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;
formFieldsPers17['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialAssocie5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers17['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialAssocie5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers17['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialAssocie5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers17['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialAssocie5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers17['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = Value('id').of(socialAssocie5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialAssocie5.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
if (socialAssocie5.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers17['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialAssocie5.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers17['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialAssocie5.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers17['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialAssocie5.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialAssocie5.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTmp = new Date(parseInt(socialAssocie5.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers17['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers17['voletSocialActiviteSimultaneeOUI_TNS']                            = socialAssocie5.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers17['voletSocialActiviteSimultaneeNON_TNS']                            = socialAssocie5.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialAssocie5.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers17['voletSocialActiviteSimultaneeSalarie_TNS']                        = Value('id').of(socialAssocie5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers17['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                = Value('id').of(socialAssocie5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers17['voletSocialActiviteSimultaneeRetraitePensionne_TNS']              = Value('id').of(socialAssocie5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers17['voletSocialActiviteSimultaneeAutre_TNS'] 				           = Value('id').of(socialAssocie5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers17['voletSocialActiviteSimultaneeAutrePrecision_TNS']                 = Value('id').of(socialAssocie5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialAssocie5.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
}
formFieldsPers17['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialAssocie5.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers17['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialAssocie5.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers17['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialAssocie5.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers17['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialAssocie5.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

// Cadre 5

formFieldsPers17['renseignementsComplementairesObservations_TNS']                    = '';

// Cadre 6
formFieldsPers17['renseignementsComplementairesLeDeclarant_TNS']                      = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFieldsPers17['renseignementsComplementairesLeMandataire_TNS']                     = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers17['formaliteSignataireNomPrenomDenomination_TNS']					     = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers17['formaliteSignataireAdresse_TNS']                                    = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '');
formFieldsPers17['formaliteSignataireAdresseComplement_TNS']  						 = (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
																					 + ' ' + signataire.adresseMandataire.dataCodePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
if (signataire.formaliteSignatureDate !== null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers17['formaliteSignatureDate_TNS']          = date;
}
formFieldsPers17['formaliteSignatureLieu_TNS']                                         = signataire.formaliteSignatureLieu;
formFieldsPers17['signature_TNS']                                         = '';
}
}

// TNS Dirigeant HORS SARL

if ((not Value('id').of(identite.entrepriseFormeJuridique).eq('5499') and not Value('id').of(identite.entrepriseFormeJuridique).eq('5410')
and not Value('id').of(identite.entrepriseFormeJuridique).eq('5415') and not Value('id').of(identite.entrepriseFormeJuridique).eq('5422')
and not Value('id').of(identite.entrepriseFormeJuridique).eq('5426') and not Value('id').of(identite.entrepriseFormeJuridique).eq('5430')
and not Value('id').of(identite.entrepriseFormeJuridique).eq('5431') and not Value('id').of(identite.entrepriseFormeJuridique).eq('5432')
and not Value('id').of(identite.entrepriseFormeJuridique).eq('5442') and not Value('id').of(identite.entrepriseFormeJuridique).eq('5443')
and not Value('id').of(identite.entrepriseFormeJuridique).eq('5451') and not Value('id').of(identite.entrepriseFormeJuridique).eq('5453')
and not Value('id').of(identite.entrepriseFormeJuridique).eq('5454') and not Value('id').of(identite.entrepriseFormeJuridique).eq('5455')
and not Value('id').of(identite.entrepriseFormeJuridique).eq('5458') and not Value('id').of(identite.entrepriseFormeJuridique).eq('5459')
and not Value('id').of(identite.entrepriseFormeJuridique).eq('5460') and not Value('id').of(identite.entrepriseFormeJuridique).eq('5485')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M'))
or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')
and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5499') and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5410')
and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5415') and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5422')
and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5426') and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5430')
and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5431') and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5432')
and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5442') and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5443')
and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5451') and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5453')
and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5454') and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5455')
and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5458') and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5459')
and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5460') and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5485'))) {

// Dirigeant 1

if (socialDirigeant1.voletSocialNumeroSecuriteSocialTNS != null) {

// Cadres 1

formFieldsPers1['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers1['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers1['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers1['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers1['formulaireDependanceTNS_TNS']                                      = false;
formFieldsPers1['formulaireDependanceM3_TNS']                                       = true;
formFieldsPers1['formulaireDependanceM3SARL_TNS']                                   = false;

// Cadres 2
formFieldsPers1['entrepriseDenomination_TNS']                                       = identite.entrepriseDenomination;
formFieldsPers1['numeroUniqueIdentification_TNS']                                   = identite.siren.split(' ').join('');


// Cadre 3

formFieldsPers1['personneLieePPNomNaissance_TNS']                = dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant ;
formFieldsPers1['personneLieePPNomUsage_TNS']                    = dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers1['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers1['personneLieeQualite_TNS']                                       = dirigeant1.personneLieeQualite.getLabel();

var nirDeclarant = socialDirigeant1.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers1['voletSocialNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers1['voletSocialNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}
formFieldsPers1['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;
formFieldsPers1['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers1['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers1['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers1['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers1['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant1.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
if (socialDirigeant1.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers1['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant1.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers1['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant1.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers1['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant1.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant1.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTmp = new Date(parseInt(socialDirigeant1.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers1['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers1['voletSocialActiviteSimultaneeOUI_TNS']                            = socialDirigeant1.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers1['voletSocialActiviteSimultaneeNON_TNS']                            = socialDirigeant1.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant1.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers1['voletSocialActiviteSimultaneeSalarie_TNS']                        = Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers1['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                = Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers1['voletSocialActiviteSimultaneeRetraitePensionne_TNS']              = Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers1['voletSocialActiviteSimultaneeAutre_TNS'] 				          = Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers1['voletSocialActiviteSimultaneeAutrePrecision_TNS']                 = Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant1.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
}
formFieldsPers1['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant1.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers1['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant1.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers1['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant1.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers1['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant1.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

// Cadre 5

formFieldsPers1['renseignementsComplementairesObservations_TNS']                    = '';

// Cadre 6
formFieldsPers1['renseignementsComplementairesLeDeclarant_TNS']                      = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFieldsPers1['renseignementsComplementairesLeMandataire_TNS']                     = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers1['formaliteSignataireNomPrenomDenomination_TNS']					     = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers1['formaliteSignataireAdresse_TNS']                                    = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '');
formFieldsPers1['formaliteSignataireAdresseComplement_TNS']  						 = (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
																					 + ' ' + signataire.adresseMandataire.dataCodePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;

}
if (signataire.formaliteSignatureDate !== null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['formaliteSignatureDate_TNS']          = date;
}
formFieldsPers1['formaliteSignatureLieu_TNS']                                         = signataire.formaliteSignatureLieu;
formFieldsPers1['signature_TNS']                                         = '';
}

// Dirigeant 2

if (socialDirigeant2.voletSocialNumeroSecuriteSocialTNS != null) {

// Cadres 1

formFieldsPers2['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers2['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers2['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers2['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers2['formulaireDependanceTNS_TNS']                                      = false;
formFieldsPers2['formulaireDependanceM3_TNS']                                       = true;
formFieldsPers2['formulaireDependanceM3SARL_TNS']                                   = false;

// Cadres 2
formFieldsPers2['entrepriseDenomination_TNS']                                       = identite.entrepriseDenomination;
formFieldsPers2['numeroUniqueIdentification_TNS']                                   = identite.siren.split(' ').join('');


// Cadre 3

formFieldsPers2['personneLieePPNomNaissance_TNS']                = dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant ;
formFieldsPers2['personneLieePPNomUsage_TNS']                    = dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers2['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers2['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers2['personneLieeQualite_TNS']                                       = dirigeant2.personneLieeQualite.getLabel();

var nirDeclarant = socialDirigeant2.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers2['voletSocialNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers2['voletSocialNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}
formFieldsPers2['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;
formFieldsPers2['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers2['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers2['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers2['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers2['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant2.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
if (socialDirigeant2.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers2['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant2.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers2['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant2.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers2['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant2.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant2.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTmp = new Date(parseInt(socialDirigeant2.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers2['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers2['voletSocialActiviteSimultaneeOUI_TNS']                            = socialDirigeant2.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers2['voletSocialActiviteSimultaneeNON_TNS']                            = socialDirigeant2.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant2.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers2['voletSocialActiviteSimultaneeSalarie_TNS']                        = Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers2['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                = Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers2['voletSocialActiviteSimultaneeRetraitePensionne_TNS']              = Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers2['voletSocialActiviteSimultaneeAutre_TNS'] 				          = Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers2['voletSocialActiviteSimultaneeAutrePrecision_TNS']                 = Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant2.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
}
formFieldsPers2['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant2.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers2['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant2.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers2['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant2.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers2['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant2.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

// Cadre 5

formFieldsPers2['renseignementsComplementairesObservations_TNS']                    = '';

// Cadre 6
formFieldsPers2['renseignementsComplementairesLeDeclarant_TNS']                      = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFieldsPers2['renseignementsComplementairesLeMandataire_TNS']                     = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers2['formaliteSignataireNomPrenomDenomination_TNS']					     = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers2['formaliteSignataireAdresse_TNS']                                    = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '');
formFieldsPers2['formaliteSignataireAdresseComplement_TNS']  						 = (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
																					 + ' ' + signataire.adresseMandataire.dataCodePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;

}
if (signataire.formaliteSignatureDate !== null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['formaliteSignatureDate_TNS']          = date;
}
formFieldsPers2['formaliteSignatureLieu_TNS']                                         = signataire.formaliteSignatureLieu;
formFieldsPers2['signature_TNS']                                         = '';
}

// Dirigeant 3

if (socialDirigeant3.voletSocialNumeroSecuriteSocialTNS != null) {

// Cadres 1

formFieldsPers3['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers3['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers3['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers3['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers3['formulaireDependanceTNS_TNS']                                      = false;
formFieldsPers3['formulaireDependanceM3_TNS']                                       = true;
formFieldsPers3['formulaireDependanceM3SARL_TNS']                                   = false;

// Cadres 2
formFieldsPers3['entrepriseDenomination_TNS']                                       = identite.entrepriseDenomination;
formFieldsPers3['numeroUniqueIdentification_TNS']                                   = identite.siren.split(' ').join('');


// Cadre 3

formFieldsPers3['personneLieePPNomNaissance_TNS']                = dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant ;
formFieldsPers3['personneLieePPNomUsage_TNS']                    = dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers3['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers3['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers3['personneLieeQualite_TNS']                       = dirigeant3.personneLieeQualite.getLabel();

var nirDeclarant = socialDirigeant3.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers3['voletSocialNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers3['voletSocialNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}
formFieldsPers3['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;
formFieldsPers3['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers3['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers3['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers3['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers3['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant3.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
if (socialDirigeant3.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers3['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant3.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers3['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant3.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers3['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant3.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant3.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTmp = new Date(parseInt(socialDirigeant3.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers3['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers3['voletSocialActiviteSimultaneeOUI_TNS']                            = socialDirigeant3.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers3['voletSocialActiviteSimultaneeNON_TNS']                            = socialDirigeant3.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant3.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers3['voletSocialActiviteSimultaneeSalarie_TNS']                        = Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers3['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                = Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers3['voletSocialActiviteSimultaneeRetraitePensionne_TNS']              = Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers3['voletSocialActiviteSimultaneeAutre_TNS'] 				          = Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers3['voletSocialActiviteSimultaneeAutrePrecision_TNS']                 = Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant3.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
}
formFieldsPers3['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant3.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers3['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant3.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers3['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant3.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers3['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant3.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

// Cadre 5

formFieldsPers3['renseignementsComplementairesObservations_TNS']                    = '';

// Cadre 6
formFieldsPers3['renseignementsComplementairesLeDeclarant_TNS']                      = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFieldsPers3['renseignementsComplementairesLeMandataire_TNS']                     = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers3['formaliteSignataireNomPrenomDenomination_TNS']					     = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers3['formaliteSignataireAdresse_TNS']                                    = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '');
formFieldsPers3['formaliteSignataireAdresseComplement_TNS']  						 = (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
																					 + ' ' + signataire.adresseMandataire.dataCodePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;

}
if (signataire.formaliteSignatureDate !== null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers3['formaliteSignatureDate_TNS']          = date;
}
formFieldsPers3['formaliteSignatureLieu_TNS']                                         = signataire.formaliteSignatureLieu;
formFieldsPers3['signature_TNS']                                         = '';
}

// Dirigeant 4

if (socialDirigeant4.voletSocialNumeroSecuriteSocialTNS != null) {

// Cadres 1

formFieldsPers4['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers4['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers4['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers4['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers4['formulaireDependanceTNS_TNS']                                      = false;
formFieldsPers4['formulaireDependanceM3_TNS']                                       = true;
formFieldsPers4['formulaireDependanceM3SARL_TNS']                                   = false;

// Cadres 2
formFieldsPers4['entrepriseDenomination_TNS']                                       = identite.entrepriseDenomination;
formFieldsPers4['numeroUniqueIdentification_TNS']                                   = identite.siren.split(' ').join('');


// Cadre 3

formFieldsPers4['personneLieePPNomNaissance_TNS']                = dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant ;
formFieldsPers4['personneLieePPNomUsage_TNS']                    = dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers4['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers4['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers4['personneLieeQualite_TNS']                       = dirigeant4.personneLieeQualite.getLabel();

var nirDeclarant = socialDirigeant4.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers4['voletSocialNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers4['voletSocialNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}
formFieldsPers4['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;
formFieldsPers4['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers4['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers4['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers4['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers4['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant4.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
if (socialDirigeant4.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers4['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant4.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers4['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant4.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers4['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant4.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant4.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTmp = new Date(parseInt(socialDirigeant4.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers4['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers4['voletSocialActiviteSimultaneeOUI_TNS']                            = socialDirigeant4.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers4['voletSocialActiviteSimultaneeNON_TNS']                            = socialDirigeant4.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant4.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers4['voletSocialActiviteSimultaneeSalarie_TNS']                        = Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers4['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                = Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers4['voletSocialActiviteSimultaneeRetraitePensionne_TNS']              = Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers4['voletSocialActiviteSimultaneeAutre_TNS'] 				          = Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers4['voletSocialActiviteSimultaneeAutrePrecision_TNS']                 = Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant4.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
}
formFieldsPers4['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant4.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers4['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant4.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers4['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant4.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers4['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant4.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

// Cadre 5

formFieldsPers4['renseignementsComplementairesObservations_TNS']                    = '';

// Cadre 6
formFieldsPers4['renseignementsComplementairesLeDeclarant_TNS']                      = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFieldsPers4['renseignementsComplementairesLeMandataire_TNS']                     = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers4['formaliteSignataireNomPrenomDenomination_TNS']					     = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers4['formaliteSignataireAdresse_TNS']                                    = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '');
formFieldsPers4['formaliteSignataireAdresseComplement_TNS']  						 = (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
																					 + ' ' + signataire.adresseMandataire.dataCodePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;

}
if (signataire.formaliteSignatureDate !== null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers4['formaliteSignatureDate_TNS']          = date;
}
formFieldsPers4['formaliteSignatureLieu_TNS']                                         = signataire.formaliteSignatureLieu;
formFieldsPers4['signature_TNS']                                         = '';
}

// Dirigeant 5

if (socialDirigeant5.voletSocialNumeroSecuriteSocialTNS != null) {

// Cadres 1

formFieldsPers5['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers5['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers5['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers5['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers5['formulaireDependanceTNS_TNS']                                      = false;
formFieldsPers5['formulaireDependanceM3_TNS']                                       = true;
formFieldsPers5['formulaireDependanceM3SARL_TNS']                                   = false;

// Cadres 2
formFieldsPers5['entrepriseDenomination_TNS']                                       = identite.entrepriseDenomination;
formFieldsPers5['numeroUniqueIdentification_TNS']                                   = identite.siren.split(' ').join('');


// Cadre 3

formFieldsPers5['personneLieePPNomNaissance_TNS']                = dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant ;
formFieldsPers5['personneLieePPNomUsage_TNS']                    = dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers5['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers5['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers5['personneLieeQualite_TNS']                       = dirigeant5.personneLieeQualite.getLabel();

var nirDeclarant = socialDirigeant5.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers5['voletSocialNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers5['voletSocialNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}
formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;
formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers5['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant5.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
if (socialDirigeant5.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers5['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant5.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers5['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant5.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers5['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant5.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant5.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTmp = new Date(parseInt(socialDirigeant5.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers5['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers5['voletSocialActiviteSimultaneeOUI_TNS']                            = socialDirigeant5.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers5['voletSocialActiviteSimultaneeNON_TNS']                            = socialDirigeant5.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant5.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers5['voletSocialActiviteSimultaneeSalarie_TNS']                        = Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers5['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                = Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers5['voletSocialActiviteSimultaneeRetraitePensionne_TNS']              = Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers5['voletSocialActiviteSimultaneeAutre_TNS'] 				           = Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers5['voletSocialActiviteSimultaneeAutrePrecision_TNS']                 = Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant5.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
}
formFieldsPers5['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant5.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers5['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant5.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers5['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant5.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers5['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant5.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

// Cadre 5

formFieldsPers5['renseignementsComplementairesObservations_TNS']                    = '';

// Cadre 6
formFieldsPers5['renseignementsComplementairesLeDeclarant_TNS']                      = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFieldsPers5['renseignementsComplementairesLeMandataire_TNS']                     = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers5['formaliteSignataireNomPrenomDenomination_TNS']					     = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers5['formaliteSignataireAdresse_TNS']                                    = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '');
formFieldsPers5['formaliteSignataireAdresseComplement_TNS']  						 = (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
																					 + ' ' + signataire.adresseMandataire.dataCodePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;

}
if (signataire.formaliteSignatureDate !== null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers5['formaliteSignatureDate_TNS']          = date;
}
formFieldsPers5['formaliteSignatureLieu_TNS']                                         = signataire.formaliteSignatureLieu;
formFieldsPers5['signature_TNS']                                         = '';
}

// Dirigeant 6

if (socialDirigeant6.voletSocialNumeroSecuriteSocialTNS != null) {

// Cadres 1

formFieldsPers6['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers6['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers6['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers6['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers6['formulaireDependanceTNS_TNS']                                      = false;
formFieldsPers6['formulaireDependanceM3_TNS']                                       = true;
formFieldsPers6['formulaireDependanceM3SARL_TNS']                                   = false;

// Cadres 2
formFieldsPers6['entrepriseDenomination_TNS']                                       = identite.entrepriseDenomination;
formFieldsPers6['numeroUniqueIdentification_TNS']                                   = identite.siren.split(' ').join('');

// Cadre 3

formFieldsPers6['personneLieePPNomNaissance_TNS']                = dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant ;
formFieldsPers6['personneLieePPNomUsage_TNS']                    = dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers6['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers6['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers6['personneLieeQualite_TNS']                       = dirigeant6.personneLieeQualite.getLabel();

var nirDeclarant = socialDirigeant6.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers6['voletSocialNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers6['voletSocialNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}
formFieldsPers6['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;
formFieldsPers6['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers6['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers6['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers6['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers6['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant6.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
if (socialDirigeant6.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers6['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant6.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers6['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant6.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers6['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant6.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant6.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTmp = new Date(parseInt(socialDirigeant6.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers6['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers6['voletSocialActiviteSimultaneeOUI_TNS']                            = socialDirigeant6.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers6['voletSocialActiviteSimultaneeNON_TNS']                            = socialDirigeant6.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant6.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers6['voletSocialActiviteSimultaneeSalarie_TNS']                        = Value('id').of(socialDirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers6['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                = Value('id').of(socialDirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers6['voletSocialActiviteSimultaneeRetraitePensionne_TNS']              = Value('id').of(socialDirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers6['voletSocialActiviteSimultaneeAutre_TNS'] 				           = Value('id').of(socialDirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers6['voletSocialActiviteSimultaneeAutrePrecision_TNS']                 = Value('id').of(socialDirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant6.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
}
formFieldsPers6['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant6.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers6['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant6.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers6['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant6.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers6['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant6.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

// Cadre 5

formFieldsPers6['renseignementsComplementairesObservations_TNS']                    = '';

// Cadre 6
formFieldsPers6['renseignementsComplementairesLeDeclarant_TNS']                      = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFieldsPers6['renseignementsComplementairesLeMandataire_TNS']                     = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers6['formaliteSignataireNomPrenomDenomination_TNS']					     = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers6['formaliteSignataireAdresse_TNS']                                    = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '');
formFieldsPers6['formaliteSignataireAdresseComplement_TNS']  						 = (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
																					 + ' ' + signataire.adresseMandataire.dataCodePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;

}
if (signataire.formaliteSignatureDate !== null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers6['formaliteSignatureDate_TNS']          = date;
}
formFieldsPers6['formaliteSignatureLieu_TNS']                                         = signataire.formaliteSignatureLieu;
formFieldsPers6['signature_TNS']                                         = '';
}

// Dirigeant 7

if (socialDirigeant7.voletSocialNumeroSecuriteSocialTNS != null) {

// Cadres 1

formFieldsPers7['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers7['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers7['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers7['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers7['formulaireDependanceTNS_TNS']                                      = false;
formFieldsPers7['formulaireDependanceM3_TNS']                                       = true;
formFieldsPers7['formulaireDependanceM3SARL_TNS']                                   = false;

// Cadres 2
formFieldsPers7['entrepriseDenomination_TNS']                                       = identite.entrepriseDenomination;
formFieldsPers7['numeroUniqueIdentification_TNS']                                   = identite.siren.split(' ').join('');

// Cadre 3

formFieldsPers7['personneLieePPNomNaissance_TNS']                = dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant ;
formFieldsPers7['personneLieePPNomUsage_TNS']                    = dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers7['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers7['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers7['personneLieeQualite_TNS']                       = dirigeant7.personneLieeQualite.getLabel();

var nirDeclarant = socialDirigeant7.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers7['voletSocialNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers7['voletSocialNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}
formFieldsPers7['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;
formFieldsPers7['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers7['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers7['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                = Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers7['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                 = Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers7['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']       = Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant7.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
if (socialDirigeant7.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers7['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant7.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers7['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant7.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers7['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant7.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant7.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTmp = new Date(parseInt(socialDirigeant7.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers7['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers7['voletSocialActiviteSimultaneeOUI_TNS']                            = socialDirigeant7.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers7['voletSocialActiviteSimultaneeNON_TNS']                            = socialDirigeant7.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant7.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers7['voletSocialActiviteSimultaneeSalarie_TNS']                        = Value('id').of(socialDirigeant7.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers7['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                = Value('id').of(socialDirigeant7.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers7['voletSocialActiviteSimultaneeRetraitePensionne_TNS']              = Value('id').of(socialDirigeant7.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers7['voletSocialActiviteSimultaneeAutre_TNS'] 				           = Value('id').of(socialDirigeant7.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers7['voletSocialActiviteSimultaneeAutrePrecision_TNS']                 = Value('id').of(socialDirigeant7.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant7.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
}
formFieldsPers7['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant7.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers7['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant7.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers7['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant7.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers7['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant7.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

// Cadre 5

formFieldsPers7['renseignementsComplementairesObservations_TNS']                    = '';

// Cadre 6
formFieldsPers7['renseignementsComplementairesLeDeclarant_TNS']                      = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFieldsPers7['renseignementsComplementairesLeMandataire_TNS']                     = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers7['formaliteSignataireNomPrenomDenomination_TNS']					     = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers7['formaliteSignataireAdresse_TNS']                                    = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '');
formFieldsPers7['formaliteSignataireAdresseComplement_TNS']  						 = (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
																					 + ' ' + signataire.adresseMandataire.dataCodePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;

}
if (signataire.formaliteSignatureDate !== null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers7['formaliteSignatureDate_TNS']          = date;
}
formFieldsPers7['formaliteSignatureLieu_TNS']                                         = signataire.formaliteSignatureLieu;
formFieldsPers7['signature_TNS']                                         = '';
}

// Dirigeant 8

if (socialDirigeant8.voletSocialNumeroSecuriteSocialTNS != null) {

// Cadres 1

formFieldsPers8['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers8['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers8['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers8['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers8['formulaireDependanceTNS_TNS']                                      = false;
formFieldsPers8['formulaireDependanceM3_TNS']                                       = true;
formFieldsPers8['formulaireDependanceM3SARL_TNS']                                   = false;

// Cadres 2
formFieldsPers8['entrepriseDenomination_TNS']                                       = identite.entrepriseDenomination;
formFieldsPers8['numeroUniqueIdentification_TNS']                                   = identite.siren.split(' ').join('');

// Cadre 3

formFieldsPers8['personneLieePPNomNaissance_TNS']                = dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant ;
formFieldsPers8['personneLieePPNomUsage_TNS']                    = dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers8['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers8['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers8['personneLieeQualite_TNS']                       = dirigeant8.personneLieeQualite.getLabel();

var nirDeclarant = socialDirigeant8.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers8['voletSocialNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers8['voletSocialNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}
formFieldsPers8['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;
formFieldsPers8['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers8['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers8['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers8['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers8['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant8.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
if (socialDirigeant8.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers8['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant8.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers8['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant8.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers8['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant8.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant8.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTmp = new Date(parseInt(socialDirigeant8.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers8['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers8['voletSocialActiviteSimultaneeOUI_TNS']                            = socialDirigeant8.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers8['voletSocialActiviteSimultaneeNON_TNS']                            = socialDirigeant8.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant8.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers8['voletSocialActiviteSimultaneeSalarie_TNS']                        = Value('id').of(socialDirigeant8.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers8['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                = Value('id').of(socialDirigeant8.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers8['voletSocialActiviteSimultaneeRetraitePensionne_TNS']              = Value('id').of(socialDirigeant8.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers8['voletSocialActiviteSimultaneeAutre_TNS'] 				           = Value('id').of(socialDirigeant8.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers8['voletSocialActiviteSimultaneeAutrePrecision_TNS']                 = Value('id').of(socialDirigeant8.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant8.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
}
formFieldsPers8['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant8.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers8['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant8.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers8['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant8.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers8['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant8.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

// Cadre 5

formFieldsPers8['renseignementsComplementairesObservations_TNS']                    = '';

// Cadre 6
formFieldsPers8['renseignementsComplementairesLeDeclarant_TNS']                      = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFieldsPers8['renseignementsComplementairesLeMandataire_TNS']                     = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers8['formaliteSignataireNomPrenomDenomination_TNS']					     = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers8['formaliteSignataireAdresse_TNS']                                    = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '');
formFieldsPers8['formaliteSignataireAdresseComplement_TNS']  						 = (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
																					 + ' ' + signataire.adresseMandataire.dataCodePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;

}
if (signataire.formaliteSignatureDate !== null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers8['formaliteSignatureDate_TNS']          = date;
}
formFieldsPers8['formaliteSignatureLieu_TNS']                                         = signataire.formaliteSignatureLieu;
formFieldsPers8['signature_TNS']                                         = '';
}

// Dirigeant 9

if (socialDirigeant9.voletSocialNumeroSecuriteSocialTNS != null) {

// Cadres 1

formFieldsPers9['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers9['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers9['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers9['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers9['formulaireDependanceTNS_TNS']                                      = false;
formFieldsPers9['formulaireDependanceM3_TNS']                                       = true;
formFieldsPers9['formulaireDependanceM3SARL_TNS']                                   = false;

// Cadres 2
formFieldsPers9['entrepriseDenomination_TNS']                                       = identite.entrepriseDenomination;
formFieldsPers9['numeroUniqueIdentification_TNS']                                   = identite.siren.split(' ').join('');

// Cadre 3

formFieldsPers9['personneLieePPNomNaissance_TNS']                = dirigeant9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant ;
formFieldsPers9['personneLieePPNomUsage_TNS']                    = dirigeant9.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant9.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers9['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers9['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers9['personneLieeQualite_TNS']                       = dirigeant9.personneLieeQualite.getLabel();

var nirDeclarant = socialDirigeant9.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers9['voletSocialNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers9['voletSocialNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}
formFieldsPers9['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;
formFieldsPers9['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers9['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers9['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers9['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers9['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant9.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
if (socialDirigeant9.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers9['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant9.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers9['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant9.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers9['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant9.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant9.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTmp = new Date(parseInt(socialDirigeant9.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers9['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers9['voletSocialActiviteSimultaneeOUI_TNS']                            = socialDirigeant9.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers9['voletSocialActiviteSimultaneeNON_TNS']                            = socialDirigeant9.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant9.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers9['voletSocialActiviteSimultaneeSalarie_TNS']                        = Value('id').of(socialDirigeant9.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers9['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                = Value('id').of(socialDirigeant9.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers9['voletSocialActiviteSimultaneeRetraitePensionne_TNS']              = Value('id').of(socialDirigeant9.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers9['voletSocialActiviteSimultaneeAutre_TNS'] 				           = Value('id').of(socialDirigeant9.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers9['voletSocialActiviteSimultaneeAutrePrecision_TNS']                 = Value('id').of(socialDirigeant9.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant9.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
}
formFieldsPers9['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant9.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers9['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant9.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers9['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant9.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers9['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant9.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

// Cadre 5

formFieldsPers9['renseignementsComplementairesObservations_TNS']                    = '';

// Cadre 6
formFieldsPers9['renseignementsComplementairesLeDeclarant_TNS']                      = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFieldsPers9['renseignementsComplementairesLeMandataire_TNS']                     = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers9['formaliteSignataireNomPrenomDenomination_TNS']					     = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers9['formaliteSignataireAdresse_TNS']                                    = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '');
formFieldsPers9['formaliteSignataireAdresseComplement_TNS']  						 = (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
																					 + ' ' + signataire.adresseMandataire.dataCodePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;

}
if (signataire.formaliteSignatureDate !== null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers9['formaliteSignatureDate_TNS']          = date;
}
formFieldsPers9['formaliteSignatureLieu_TNS']                                         = signataire.formaliteSignatureLieu;
formFieldsPers9['signature_TNS']                                         = '';
}

// Dirigeant 10

if (socialDirigeant10.voletSocialNumeroSecuriteSocialTNS != null) {

// Cadres 1

formFieldsPers10['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers10['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers10['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers10['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers10['formulaireDependanceTNS_TNS']                                      = false;
formFieldsPers10['formulaireDependanceM3_TNS']                                       = true;
formFieldsPers10['formulaireDependanceM3SARL_TNS']                                   = false;

// Cadres 2
formFieldsPers10['entrepriseDenomination_TNS']                                       = identite.entrepriseDenomination;
formFieldsPers10['numeroUniqueIdentification_TNS']                                   = identite.siren.split(' ').join('');

// Cadre 3

formFieldsPers10['personneLieePPNomNaissance_TNS']                = dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant ;
formFieldsPers10['personneLieePPNomUsage_TNS']                    = dirigeant10.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant10.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers10['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers10['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers10['personneLieeQualite_TNS']                       = dirigeant10.personneLieeQualite.getLabel();

var nirDeclarant = socialDirigeant10.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers10['voletSocialNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers10['voletSocialNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}
formFieldsPers10['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;
formFieldsPers10['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers10['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers10['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers10['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers10['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant10.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
if (socialDirigeant10.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers10['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant10.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers10['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant10.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers10['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant10.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant10.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTmp = new Date(parseInt(socialDirigeant10.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers10['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers10['voletSocialActiviteSimultaneeOUI_TNS']                            = socialDirigeant10.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers10['voletSocialActiviteSimultaneeNON_TNS']                            = socialDirigeant10.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant10.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers10['voletSocialActiviteSimultaneeSalarie_TNS']                        = Value('id').of(socialDirigeant10.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers10['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                = Value('id').of(socialDirigeant10.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers10['voletSocialActiviteSimultaneeRetraitePensionne_TNS']              = Value('id').of(socialDirigeant10.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers10['voletSocialActiviteSimultaneeAutre_TNS'] 				           = Value('id').of(socialDirigeant10.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers10['voletSocialActiviteSimultaneeAutrePrecision_TNS']                 = Value('id').of(socialDirigeant10.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant10.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
}
formFieldsPers10['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant10.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers10['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant10.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers10['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant10.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers10['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant10.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

// Cadre 5

formFieldsPers10['renseignementsComplementairesObservations_TNS']                    = '';

// Cadre 6
formFieldsPers10['renseignementsComplementairesLeDeclarant_TNS']                      = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFieldsPers10['renseignementsComplementairesLeMandataire_TNS']                     = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers10['formaliteSignataireNomPrenomDenomination_TNS']					     = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers10['formaliteSignataireAdresse_TNS']                                    = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '');
formFieldsPers10['formaliteSignataireAdresseComplement_TNS']  						 = (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
																					 + ' ' + signataire.adresseMandataire.dataCodePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;

}
if (signataire.formaliteSignatureDate !== null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers10['formaliteSignatureDate_TNS']          = date;
}
formFieldsPers10['formaliteSignatureLieu_TNS']                                         = signataire.formaliteSignatureLieu;
formFieldsPers10['signature_TNS']                                         = '';
}

// Dirigeant 11

if (socialDirigeant11.voletSocialNumeroSecuriteSocialTNS != null) {

// Cadres 1

formFieldsPers11['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers11['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers11['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers11['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers11['formulaireDependanceTNS_TNS']                                      = false;
formFieldsPers11['formulaireDependanceM3_TNS']                                       = true;
formFieldsPers11['formulaireDependanceM3SARL_TNS']                                   = false;

// Cadres 2
formFieldsPers11['entrepriseDenomination_TNS']                                       = identite.entrepriseDenomination;
formFieldsPers11['numeroUniqueIdentification_TNS']                                   = identite.siren.split(' ').join('');

// Cadre 3

formFieldsPers11['personneLieePPNomNaissance_TNS']                = dirigeant11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant ;
formFieldsPers11['personneLieePPNomUsage_TNS']                    = dirigeant11.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant11.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers11['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers11['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers11['personneLieeQualite_TNS']                       = dirigeant11.personneLieeQualite.getLabel();

var nirDeclarant = socialDirigeant11.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers11['voletSocialNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers11['voletSocialNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}
formFieldsPers11['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;
formFieldsPers11['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers11['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers11['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers11['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers11['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant11.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
if (socialDirigeant11.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers11['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant11.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers11['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant11.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers11['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant11.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant11.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTmp = new Date(parseInt(socialDirigeant11.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers11['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers11['voletSocialActiviteSimultaneeOUI_TNS']                            = socialDirigeant11.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers11['voletSocialActiviteSimultaneeNON_TNS']                            = socialDirigeant11.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant11.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers11['voletSocialActiviteSimultaneeSalarie_TNS']                        = Value('id').of(socialDirigeant11.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers11['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                = Value('id').of(socialDirigeant11.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers11['voletSocialActiviteSimultaneeRetraitePensionne_TNS']              = Value('id').of(socialDirigeant11.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers11['voletSocialActiviteSimultaneeAutre_TNS'] 				           = Value('id').of(socialDirigeant11.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers11['voletSocialActiviteSimultaneeAutrePrecision_TNS']                 = Value('id').of(socialDirigeant11.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant11.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
}
formFieldsPers11['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant11.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers11['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant11.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers11['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant11.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers11['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant11.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

// Cadre 5

formFieldsPers11['renseignementsComplementairesObservations_TNS']                    = '';

// Cadre 6
formFieldsPers11['renseignementsComplementairesLeDeclarant_TNS']                      = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFieldsPers11['renseignementsComplementairesLeMandataire_TNS']                     = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers11['formaliteSignataireNomPrenomDenomination_TNS']					     = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers11['formaliteSignataireAdresse_TNS']                                    = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '');
formFieldsPers11['formaliteSignataireAdresseComplement_TNS']  						 = (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
																					 + ' ' + signataire.adresseMandataire.dataCodePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;

}
if (signataire.formaliteSignatureDate !== null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers11['formaliteSignatureDate_TNS']          = date;
}
formFieldsPers11['formaliteSignatureLieu_TNS']                                         = signataire.formaliteSignatureLieu;
formFieldsPers11['signature_TNS']                                         = '';
}

// Dirigeant 12

if (socialDirigeant12.voletSocialNumeroSecuriteSocialTNS != null) {

// Cadres 1

formFieldsPers12['formulaireDependanceMOSARL_TNS']                                   = false;
formFieldsPers12['formulaireDependanceM0SNC_TNS']                                    = false;
formFieldsPers12['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFieldsPers12['formulaireDependanceM2_TNS']                                       = false;
formFieldsPers12['formulaireDependanceTNS_TNS']                                      = false;
formFieldsPers12['formulaireDependanceM3_TNS']                                       = true;
formFieldsPers12['formulaireDependanceM3SARL_TNS']                                   = false;

// Cadres 2
formFieldsPers12['entrepriseDenomination_TNS']                                       = identite.entrepriseDenomination;
formFieldsPers12['numeroUniqueIdentification_TNS']                                   = identite.siren.split(' ').join('');

// Cadre 3

formFieldsPers12['personneLieePPNomNaissance_TNS']                = dirigeant12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant ;
formFieldsPers12['personneLieePPNomUsage_TNS']                    = dirigeant12.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant12.civilitePersonnePhysique.personneLieePPNomUsageGerant : (dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null ? dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant : '') ;
if (dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}                            
formFieldsPers12['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
} else if (dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null and dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);}                            
formFieldsPers12['personneLieePPPrenom_TNS']                                                      = prenoms.toString();
}
formFieldsPers12['personneLieeQualite_TNS']                       = dirigeant12.personneLieeQualite.getLabel();

var nirDeclarant = socialDirigeant12.voletSocialNumeroSecuriteSocialTNS;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFieldsPers12['voletSocialNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFieldsPers12['voletSocialNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}
formFieldsPers12['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;
formFieldsPers12['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFieldsPers12['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFieldsPers12['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFieldsPers12['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFieldsPers12['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant12.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
if (socialDirigeant12.voletSocialActiviteExerceeAnterieurementTNS) {
formFieldsPers12['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = socialDirigeant12.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFieldsPers12['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = socialDirigeant12.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFieldsPers12['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = socialDirigeant12.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(socialDirigeant12.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTmp = new Date(parseInt(socialDirigeant12.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers12['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFieldsPers12['voletSocialActiviteSimultaneeOUI_TNS']                            = socialDirigeant12.voletSocialActiviteSimultaneeOUINON ? true : false;
formFieldsPers12['voletSocialActiviteSimultaneeNON_TNS']                            = socialDirigeant12.voletSocialActiviteSimultaneeOUINON ? false : true;
if (socialDirigeant12.voletSocialActiviteSimultaneeOUINON) {
formFieldsPers12['voletSocialActiviteSimultaneeSalarie_TNS']                        = Value('id').of(socialDirigeant12.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? true : false;
formFieldsPers12['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                = Value('id').of(socialDirigeant12.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? true : false;
formFieldsPers12['voletSocialActiviteSimultaneeRetraitePensionne_TNS']              = Value('id').of(socialDirigeant12.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? true : false;
formFieldsPers12['voletSocialActiviteSimultaneeAutre_TNS'] 				           = Value('id').of(socialDirigeant12.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? true : false;
formFieldsPers12['voletSocialActiviteSimultaneeAutrePrecision_TNS']                 = Value('id').of(socialDirigeant12.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS') ? socialDirigeant12.voletSocialActiviteAutreQueDeclareeStatutAutreTNS : '';
}
formFieldsPers12['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant12.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers12['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant12.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFieldsPers12['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(socialDirigeant12.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFieldsPers12['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(socialDirigeant12.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

// Cadre 5

formFieldsPers12['renseignementsComplementairesObservations_TNS']                    = '';

// Cadre 6
formFieldsPers12['renseignementsComplementairesLeDeclarant_TNS']                      = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFieldsPers12['renseignementsComplementairesLeMandataire_TNS']                     = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? true : false;
if (Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
formFieldsPers12['formaliteSignataireNomPrenomDenomination_TNS']					     = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFieldsPers12['formaliteSignataireAdresse_TNS']                                    = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '');
formFieldsPers12['formaliteSignataireAdresseComplement_TNS']  						 = (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '')
																					 + ' ' + signataire.adresseMandataire.dataCodePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;

}
if (signataire.formaliteSignatureDate !== null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers12['formaliteSignatureDate_TNS']          = date;
}
formFieldsPers12['formaliteSignatureLieu_TNS']                                         = signataire.formaliteSignatureLieu;
formFieldsPers12['signature_TNS']                                         = '';
}
}

// JQPA 1

//cadre 1
var formFieldsPers19 = {}

// Cadre 1
formFieldsPers19['jqpa_intercalaire']                         = true;
formFieldsPers19['jqpa_formulaire']                           = false;

// Cadre 4
formFieldsPers19['jqpa_entreprise_denomination']              = identite.entrepriseDenomination;
formFieldsPers19['jqpa_entreprise_formeJuridique']            = identite.entrepriseFormeJuridique; 
formFieldsPers19['jqpa_entreprise_siren']                     = identite.siren.split(' ').join('');

 // Cadre 5A
formFieldsPers19['jqpa_pm_activite']                          = jqpa1.jqpaActiviteJqpaPP;

//Cadre 5B
formFieldsPers19['jqpa_aqpaSituationPM_diplome']              = Value('id').of(jqpa1.jqpaSituation).eq('jqpaSituationDiplome') or Value('id').of(jqpa1.jqpaSituation).eq('jqpaSituationExperience') ? true : false;
formFieldsPers19['jqpa_qualitePersonneQualifieePM_representant'] = Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPDeclarant') ? true : false;
formFieldsPers19['jqpa_qualitePersonneQualifieePM_conjoint']  = Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPConjoint') ? true : false;
formFieldsPers19['jqpa_qualitePersonneQualifieePM_salarie']   = Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') ? true : false;
formFieldsPers19['jqpa_qualitePersonneQualifieePM_autre']     = Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre') ? true : false;
formFieldsPers19['jqpa_qualitePersonneQualifieePMAutre']      = jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifieePPChampAutre;

formFieldsPers19['jqpa_identitePMNomNaissance']               = jqpa1.identitePersonneQualifiee.jqpaIdentitePPNomNaissance != null ? jqpa1.identitePersonneQualifiee.jqpaIdentitePPNomNaissance : '';
formFieldsPers19['jqpa_identitePMNomUsage']                   = jqpa1.identitePersonneQualifiee.jqpaIdentitePPNomUsage != null ? jqpa1.identitePersonneQualifiee.jqpaIdentitePPNomUsage : '';
var prenoms=[];
for ( i = 0; i < jqpa1.identitePersonneQualifiee.jqpaIdentitePPPrenom.size() ; i++ ){prenoms.push(jqpa1.identitePersonneQualifiee.jqpaIdentitePPPrenom[i]);}                            
formFieldsPers19['jqpa_identitePMPrenom']            = prenoms.toString();

if (jqpa1.identitePersonneQualifiee.jqpaIdentitePPDateNaissance !== null) {
	var dateTmp = new Date(parseInt(jqpa1.identitePersonneQualifiee.jqpaIdentitePPDateNaissance.getTimeInMillis()));
	var dateNaissance = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	dateNaissance = dateNaissance.concat(pad(month.toString()));
	dateNaissance = dateNaissance.concat(dateTmp.getFullYear().toString());
	formFieldsPers19['jqpa_identitePMDateNaissance']          = dateNaissance;
}
formFieldsPers19['jqpa_identitePMDepartement']                = jqpa1.identitePersonneQualifiee.jqpaIdentitePPDepartement != null ? jqpa1.identitePersonneQualifiee.jqpaIdentitePPDepartement.getId() : '';
formFieldsPers19['jqpa_identitePMLieuNaissancePays']          = (Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') or Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre')) ? ((jqpa1.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune != null ? jqpa1.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune : jqpa1.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceVille) + ' / ' + jqpa1.identitePersonneQualifiee.jqpaIdentitePPLieuNaissancePays) : '';

// Cadre 5C
formFieldsPers19['jqpa_aqpaSituationPM_engagement']           = Value('id').of(jqpa1.jqpaSituation).eq('jqpaSituationRecrutement') ? true : false;

// JQPA 2

//cadre 1
var formFieldsPers20 = {}

// Cadre 1
formFieldsPers20['jqpa_intercalaire']                         = true;
formFieldsPers20['jqpa_formulaire']                           = false;

// Cadre 4
formFieldsPers20['jqpa_entreprise_denomination']              = identite.entrepriseDenomination;
formFieldsPers20['jqpa_entreprise_formeJuridique']            = identite.entrepriseFormeJuridique; 
formFieldsPers20['jqpa_entreprise_siren']                     = identite.siren.split(' ').join('');

 // Cadre 5A
formFieldsPers20['jqpa_pm_activite']                          = jqpa2.jqpaActiviteJqpaPP;

//Cadre 5B
formFieldsPers20['jqpa_aqpaSituationPM_diplome']              = Value('id').of(jqpa2.jqpaSituation).eq('jqpaSituationDiplome') or Value('id').of(jqpa2.jqpaSituation).eq('jqpaSituationExperience') ? true : false;
formFieldsPers20['jqpa_qualitePersonneQualifieePM_representant'] = Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPDeclarant') ? true : false;
formFieldsPers20['jqpa_qualitePersonneQualifieePM_conjoint']  = Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPConjoint') ? true : false;
formFieldsPers20['jqpa_qualitePersonneQualifieePM_salarie']   = Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') ? true : false;
formFieldsPers20['jqpa_qualitePersonneQualifieePM_autre']     = Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre') ? true : false;
formFieldsPers20['jqpa_qualitePersonneQualifieePMAutre']      = jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifieePPChampAutre;

formFieldsPers20['jqpa_identitePMNomNaissance']               = jqpa2.identitePersonneQualifiee.jqpaIdentitePPNomNaissance != null ? jqpa2.identitePersonneQualifiee.jqpaIdentitePPNomNaissance : '';
formFieldsPers20['jqpa_identitePMNomUsage']                   = jqpa2.identitePersonneQualifiee.jqpaIdentitePPNomUsage != null ? jqpa2.identitePersonneQualifiee.jqpaIdentitePPNomUsage : '';
var prenoms=[];
for ( i = 0; i < jqpa2.identitePersonneQualifiee.jqpaIdentitePPPrenom.size() ; i++ ){prenoms.push(jqpa2.identitePersonneQualifiee.jqpaIdentitePPPrenom[i]);}                            
formFieldsPers20['jqpa_identitePMPrenom']            = prenoms.toString();

if (jqpa2.identitePersonneQualifiee.jqpaIdentitePPDateNaissance !== null) {
	var dateTmp = new Date(parseInt(jqpa2.identitePersonneQualifiee.jqpaIdentitePPDateNaissance.getTimeInMillis()));
	var dateNaissance = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	dateNaissance = dateNaissance.concat(pad(month.toString()));
	dateNaissance = dateNaissance.concat(dateTmp.getFullYear().toString());
	formFieldsPers20['jqpa_identitePMDateNaissance']          = dateNaissance;
}
formFieldsPers20['jqpa_identitePMDepartement']                = jqpa2.identitePersonneQualifiee.jqpaIdentitePPDepartement != null ? jqpa2.identitePersonneQualifiee.jqpaIdentitePPDepartement.getId() : '';
formFieldsPers20['jqpa_identitePMLieuNaissancePays']          = (Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') or Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre')) ? ((jqpa2.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune != null ? jqpa2.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune : jqpa2.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceVille) + ' / ' + jqpa2.identitePersonneQualifiee.jqpaIdentitePPLieuNaissancePays) : '';

// Cadre 5C
formFieldsPers20['jqpa_aqpaSituationPM_engagement']           = Value('id').of(jqpa2.jqpaSituation).eq('jqpaSituationRecrutement') ? true : false;

// JQPA 3

//cadre 1
var formFieldsPers21 = {}

// Cadre 1
formFieldsPers21['jqpa_intercalaire']                         = true;
formFieldsPers21['jqpa_formulaire']                           = false;

// Cadre 4
formFieldsPers21['jqpa_entreprise_denomination']              = identite.entrepriseDenomination;
formFieldsPers21['jqpa_entreprise_formeJuridique']            = identite.entrepriseFormeJuridique; 
formFieldsPers21['jqpa_entreprise_siren']                     = identite.siren.split(' ').join('');

 // Cadre 5A
formFieldsPers21['jqpa_pm_activite']                          = jqpa3.jqpaActiviteJqpaPP;

//Cadre 5B
formFieldsPers21['jqpa_aqpaSituationPM_diplome']              = Value('id').of(jqpa3.jqpaSituation).eq('jqpaSituationDiplome') or Value('id').of(jqpa3.jqpaSituation).eq('jqpaSituationExperience') ? true : false;
formFieldsPers21['jqpa_qualitePersonneQualifieePM_representant'] = Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPDeclarant') ? true : false;
formFieldsPers21['jqpa_qualitePersonneQualifieePM_conjoint']  = Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPConjoint') ? true : false;
formFieldsPers21['jqpa_qualitePersonneQualifieePM_salarie']   = Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') ? true : false;
formFieldsPers21['jqpa_qualitePersonneQualifieePM_autre']     = Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre') ? true : false;
formFieldsPers21['jqpa_qualitePersonneQualifieePMAutre']      = jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifieePPChampAutre;

formFieldsPers21['jqpa_identitePMNomNaissance']               = jqpa3.identitePersonneQualifiee.jqpaIdentitePPNomNaissance != null ? jqpa3.identitePersonneQualifiee.jqpaIdentitePPNomNaissance : '';
formFieldsPers21['jqpa_identitePMNomUsage']                   = jqpa3.identitePersonneQualifiee.jqpaIdentitePPNomUsage != null ? jqpa3.identitePersonneQualifiee.jqpaIdentitePPNomUsage : '';
var prenoms=[];
for ( i = 0; i < jqpa3.identitePersonneQualifiee.jqpaIdentitePPPrenom.size() ; i++ ){prenoms.push(jqpa3.identitePersonneQualifiee.jqpaIdentitePPPrenom[i]);}                            
formFieldsPers21['jqpa_identitePMPrenom']            = prenoms.toString();

if (jqpa3.identitePersonneQualifiee.jqpaIdentitePPDateNaissance !== null) {
	var dateTmp = new Date(parseInt(jqpa3.identitePersonneQualifiee.jqpaIdentitePPDateNaissance.getTimeInMillis()));
	var dateNaissance = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	dateNaissance = dateNaissance.concat(pad(month.toString()));
	dateNaissance = dateNaissance.concat(dateTmp.getFullYear().toString());
	formFieldsPers21['jqpa_identitePMDateNaissance']          = dateNaissance;
}
formFieldsPers21['jqpa_identitePMDepartement']                = jqpa3.identitePersonneQualifiee.jqpaIdentitePPDepartement != null ? jqpa3.identitePersonneQualifiee.jqpaIdentitePPDepartement.getId() : '';
formFieldsPers21['jqpa_identitePMLieuNaissancePays']          = (Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') or Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre')) ? ((jqpa3.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune != null ? jqpa3.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune : jqpa3.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceVille) + ' / ' + jqpa3.identitePersonneQualifiee.jqpaIdentitePPLieuNaissancePays) : '';

// Cadre 5C
formFieldsPers21['jqpa_aqpaSituationPM_engagement']           = Value('id').of(jqpa3.jqpaSituation).eq('jqpaSituationRecrutement') ? true : false;

// Intercalaire NDI

// Cadre 1

formFields['complete_immatriculation_ndi']                   = true;
formFields['complete_modification_ndi']                      = false;

// Cadre 2
formFields['siren_ndi']                     = identite.siren.split(' ').join('');
formFields['immatRCSGreffe_ndi']            = identite.immatRCSGreffe;         
formFields['denomination_ndi']              = identite.entrepriseDenomination;

formFields['adresseSiege_voie_ndi']                          = (identite.adresseSiege.siegeAdresseNumeroVoie != null ? identite.adresseSiege.siegeAdresseNumeroVoie : '')
															 + ' ' + (identite.adresseSiege.siegeAdresseIndiceVoie != null ? identite.adresseSiege.siegeAdresseIndiceVoie : '')
															 + ' ' + (identite.adresseSiege.siegeAdresseTypeVoie != null ? identite.adresseSiege.siegeAdresseTypeVoie : '')
															 +' ' + identite.adresseSiege.siegeAdresseNomVoie
															 + ' ' + (identite.adresseSiege.siegeAdresseComplementVoie != null ? identite.adresseSiege.siegeAdresseComplementVoie : '')
															 + ' ' + (identite.adresseSiege.siegeAdresseDistriutionSpecialeVoie != null ? identite.adresseSiege.siegeAdresseDistriutionSpecialeVoie : '');
formFields['adresseSiege_codePostal_ndi']                    = identite.adresseSiege.siegeAdresseCodePostal;
formFields['adresseSiege_commune_ndi']                       = identite.adresseSiege.siegeAdresseCommune;

// Cadre 3
if(activiteInfo.modifEtablissementNomDomaine or activite.etablissementNomDomaine!= null) {
if(activiteInfo.modifEtablissementNomDomaine and activiteInfo.cadreEtablissementNomDomaine.modifDateIdentification != null) {
	var dateTmp = new Date(parseInt(activiteInfo.cadreEtablissementNomDomaine.modifDateIdentification.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['dateDebutActivite_ndi'] = date;
} else if (fermeture.modifDateTransfert != null) {
	var dateTmp = new Date(parseInt(fermeture.modifDateTransfert.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['dateDebutActivite_ndi'] = date;
} else if (ouverture.modifDatePriseActiviteEtablissement != null) {
	var dateTmp = new Date(parseInt(ouverture.modifDatePriseActiviteEtablissement.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['dateDebutActivite_ndi'] = date;
} else if (ouverture.modifDateAdresseOuverture != null) {
	var dateTmp = new Date(parseInt(ouverture.modifDateAdresseOuverture.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['dateDebutActivite_ndi'] = date;
}

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
and objet.cadreObjetModificationEtablissement.lieuActiviteSiege)
or ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60M')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63M')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64M'))
and objet.cadreObjetModificationEtablissement.lieuSiege)) {
formFields['adresseEtablissement_voie_ndi']                  = (identite.adresseSiege.siegeAdresseNumeroVoie != null ? identite.adresseSiege.siegeAdresseNumeroVoie : '')
															 + ' ' + (identite.adresseSiege.siegeAdresseIndiceVoie != null ? identite.adresseSiege.siegeAdresseIndiceVoie : '')
															 + ' ' + (identite.adresseSiege.siegeAdresseTypeVoie != null ? identite.adresseSiege.siegeAdresseTypeVoie : '')
															 +' ' + identite.adresseSiege.siegeAdresseNomVoie
															 + ' ' + (identite.adresseSiege.siegeAdresseComplementVoie != null ? identite.adresseSiege.siegeAdresseComplementVoie : '')
															 + ' ' + (identite.adresseSiege.siegeAdresseDistriutionSpecialeVoie != null ? identite.adresseSiege.siegeAdresseDistriutionSpecialeVoie : '');
formFields['adresseEtablissement_codePostal_ndi']            = identite.adresseSiege.siegeAdresseCodePostal;
formFields['adresseEtablissement_commune_ndi']               = identite.adresseSiege.siegeAdresseCommune;
} else {
formFields['adresseEtablissement_voie_ndi']   				=(ouverture.cadreAdresseEtablissementOuvert.numeroAdresseEtablissement != null ? ouverture.cadreAdresseEtablissementOuvert.numeroAdresseEtablissement : '')
																	+ ' ' + (ouverture.cadreAdresseEtablissementOuvert.indiceAdresseEtablissement != null ? ouverture.cadreAdresseEtablissementOuvert.indiceAdresseEtablissement : '')
																	+ ' ' + (ouverture.cadreAdresseEtablissementOuvert.typeAdresseEtablissement != null ? ouverture.cadreAdresseEtablissementOuvert.typeAdresseEtablissement : '')
																	+' ' + ouverture.cadreAdresseEtablissementOuvert.voieAdresseEtablissement
																	+ ' ' + (ouverture.cadreAdresseEtablissementOuvert.complementAdresseEtablissement != null ? ouverture.cadreAdresseEtablissementOuvert.complementAdresseEtablissement : '')
																	+ ' ' + (ouverture.cadreAdresseEtablissementOuvert.distributionSpecialeAdresseEtablissement != null ? ouverture.cadreAdresseEtablissementOuvert.distributionSpecialeAdresseEtablissement : '');
formFields['adresseEtablissement_codePostal_ndi']                   = ouverture.cadreAdresseEtablissementOuvert.codePostalAdresseEtablissement;
formFields['adresseEtablissement_commune_ndi']                      = ouverture.cadreAdresseEtablissementOuvert.communeAdresseEtablissement;
}
formFields['nomDomaineEtablissement_ndi']                    = activite.etablissementNomDomaine != null ? activite.etablissementNomDomaine : activiteInfo.cadreEtablissementNomDomaine.modifNomDomaine;
formFields['suppressionNomDomaineEtablissement_ndi']         = Value('id').of(activiteInfo.cadreEtablissementNomDomaine.modifDomaineObjet).eq('suppression') ? true : false;
formFields['remplacementNomDomaineEtablissement_ndi']        = Value('id').of(activiteInfo.cadreEtablissementNomDomaine.modifDomaineObjet).eq('remplacement') ? true : false;
formFields['declarationInitialeNomDomaineEtablissement_ndi'] = (Value('id').of(activiteInfo.cadreEtablissementNomDomaine.modifDomaineObjet).eq('initial') or activite.etablissementNomDomaine != null) ? true : false;
}

// Cadre 4

if (identite.nomDomainePresence) {
   if (identite.societeNomDomaineDate != null) {
       var dateTmp = new Date(parseInt(identite.societeNomDomaineDate.getTimeInMillis()));
       var date = pad(dateTmp.getDate().toString());
       var month = dateTmp.getMonth() + 1;
       date = date.concat(pad(month.toString()));
       date = date.concat(dateTmp.getFullYear().toString());
       formFields['dateEffetNomDomainePM_ndi'] = date;
   }   
   formFields['nomDomainePM_ndi[0]'] = identite.societeNomDomaine1;
   formFields['nouveauNomDomainePM_ndi[0]'] = Value('id').of(identite.societeNomDomaineObjet1).eq('nouveau') ? true : false;
   formFields['suppressionNomDomainePM_ndi[0]'] = Value('id').of(identite.societeNomDomaineObjet1).eq('supprime') ? true : false;
if (identite.nomDomainePresenceAutre1) {
   formFields['nomDomainePM_ndi[1]'] = identite.societeNomDomaine2;
   formFields['nouveauNomDomainePM_ndi[1]'] = Value('id').of(identite.societeNomDomaineObjet2).eq('nouveau') ? true : false;
   formFields['suppressionNomDomainePM_ndi[1]'] = Value('id').of(identite.societeNomDomaineObjet2).eq('supprime') ? true : false;
}
if (identite.nomDomainePresenceAutre2) {
   formFields['nomDomainePM_ndi[2]'] = identite.societeNomDomaine3;
   formFields['nouveauNomDomainePM_ndi[2]'] = Value('id').of(identite.societeNomDomaineObjet3).eq('nouveau') ? true : false;
   formFields['suppressionNomDomainePM_ndi[2]'] = Value('id').of(identite.societeNomDomaineObjet3).eq('supprime') ? true : false;
}
if (identite.nomDomainePresenceAutre3) {
   formFields['nomDomainePM_ndi[3]'] = identite.societeNomDomaine4;
   formFields['nouveauNomDomainePM_ndi[3]'] = Value('id').of(identite.societeNomDomaineObjet4).eq('nouveau') ? true : false;
   formFields['suppressionNomDomainePM_ndi[3]'] = Value('id').of(identite.societeNomDomaineObjet4).eq('supprime') ? true : false;
}
if (identite.nomDomainePresenceAutre4) {
   formFields['nomDomainePM_ndi[4]'] = identite.societeNomDomaine5;
   formFields['nouveauNomDomainePM_ndi[4]'] = Value('id').of(identite.societeNomDomaineObjet5).eq('nouveau') ? true : false;
   formFields['suppressionNomDomainePM_ndi[4]'] = Value('id').of(identite.societeNomDomaineObjet5).eq('supprime') ? true : false;
}
if (identite.nomDomainePresenceAutre5) {
   formFields['nomDomainePM_ndi[5]'] = identite.societeNomDomaine6;
   formFields['nouveauNomDomainePM_ndi[5]'] = Value('id').of(identite.societeNomDomaineObjet6).eq('nouveau') ? true : false;
   formFields['suppressionNomDomainePM_ndi[5]'] = Value('id').of(identite.societeNomDomaineObjet6).eq('supprime') ? true : false;
}
if (identite.nomDomainePresenceAutre6) {
   formFields['nomDomainePM_ndi[6]'] = identite.societeNomDomaine7;
   formFields['nouveauNomDomainePM_ndi[6]'] = Value('id').of(identite.societeNomDomaineObjet7).eq('nouveau') ? true : false;
   formFields['suppressionNomDomainePM_ndi[6]'] = Value('id').of(identite.societeNomDomaineObjet7).eq('supprime') ? true : false;
}
if (identite.nomDomainePresenceAutre7) {
   formFields['nomDomainePM_ndi[7]'] = identite.societeNomDomaine8;
   formFields['nouveauNomDomainePM_ndi[7]'] = Value('id').of(identite.societeNomDomaineObjet8).eq('nouveau') ? true : false;
   formFields['suppressionNomDomainePM_ndi[7]'] = Value('id').of(identite.societeNomDomaineObjet8).eq('supprime') ? true : false;
}
if (identite.nomDomainePresenceAutre8) {
   formFields['nomDomainePM_ndi[8]'] = identite.societeNomDomaine9;
   formFields['nouveauNomDomainePM_ndi[8]'] = Value('id').of(identite.societeNomDomaineObjet9).eq('nouveau') ? true : false;
   formFields['suppressionNomDomainePM_ndi[8]'] = Value('id').of(identite.societeNomDomaineObjet9).eq('supprime') ? true : false;
}
}



/*
 * Création du dossier : Ajout du cerfa M2 avec volet social
 */

if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
var cerfaDoc1 = nash.doc //
	.load('models/cerfa_11682-06_M2_avec_volet_social.pdf') //
	.apply(formFields);
 
 var cerfaDoc2 = nash.doc //
	.load('models/cerfa_11682-06_M2_sans_volet_social.pdf') //
	.apply (formFields);
	cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire M3 SARL 1
 */
if (((dissolution.cadreIdentiteLiquidateur.autreDirigeant
or Value('id').of(objet.objetModification).contains('dirigeant')
or (ouverture.ajoutFondePouvoir and ($M2.dirigeant1Sarl.autrePersonneLieeEtablissement or $M2.dirigeant1.autrePersonneLieeEtablissement)))
and ((Value('id').of(identite.entrepriseFormeJuridique).eq('5499') or Value('id').of(identite.entrepriseFormeJuridique).eq('5410')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5415')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5422')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5426')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5430')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5431')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5432')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5442')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5443')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5451')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5453')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5454')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5455')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5458')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5459')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5460')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5485'))
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')))
or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')
and (modifFormeJuridique.formeJuridiqueModifDirigeant
or Value('id').of(objet.objetModification).contains('dirigeant')
or (ouverture.ajoutFondePouvoir and ($M2.dirigeant1Sarl.autrePersonneLieeEtablissement or $M2.dirigeant1.autrePersonneLieeEtablissement)))
and (Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5499') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5410')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5415')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5422')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5426')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5430')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5431')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5432')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5442')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5443')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5451')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5453')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5454')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5455')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5458')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5459')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5460')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5485')))) {
		
var m3Doc1 = nash.doc //
		.load('models/cerfa_14580-04_M3sarl_avec_volet_social.pdf') //
		.apply (formFieldsPers1);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(m3Doc1.save('cerfa.pdf'));
}

var m3Doc2 = nash.doc //
		.load('models/cerfa_14580-04_M3sarl_sans_volet_social.pdf') //
		.apply (formFieldsPers1);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(m3Doc2.save('cerfa.pdf'));
} else { m3Doc1.append(m3Doc2.save('cerfa.pdf')); }

	
/*
 * Ajout de l'intercalaire M3 SARL 2
 */
if ((pouvoir.length > 1) or (gerants.length > 2)) {
		
var m3Doc3 = nash.doc //
		.load('models/cerfa_14580-04_M3sarl_avec_volet_social.pdf') //
		.apply (formFieldsPers2);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(m3Doc3.save('cerfa.pdf'));
} else { m3Doc1.append(m3Doc3.save('cerfa.pdf')); }


var m3Doc4 = nash.doc //
		.load('models/cerfa_14580-04_M3sarl_sans_volet_social.pdf') //
		.apply (formFieldsPers2);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(m3Doc4.save('cerfa.pdf'));
} else { m3Doc1.append(m3Doc4.save('cerfa.pdf')); }
}
	
/*
 * Ajout de l'intercalaire M3 SARL 3
 */
if ((pouvoir.length > 2) or (gerants.length > 4)) {
		
var m3Doc5 = nash.doc //
		.load('models/cerfa_14580-04_M3sarl_avec_volet_social.pdf') //
		.apply (formFieldsPers3);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(m3Doc5.save('cerfa.pdf'));
} else { m3Doc1.append(m3Doc5.save('cerfa.pdf')); }


var m3Doc6 = nash.doc //
		.load('models/cerfa_14580-04_M3sarl_sans_volet_social.pdf') //
		.apply (formFieldsPers3);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(m3Doc6.save('cerfa.pdf'));
} else { m3Doc1.append(m3Doc6.save('cerfa.pdf')); }
}

/*
 * Ajout de l'intercalaire M3 SARL 4
 */
if (pouvoir.length > 3) {
		
var m3Doc7 = nash.doc //
		.load('models/cerfa_14580-04_M3sarl_avec_volet_social.pdf') //
		.apply (formFieldsPers4);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(m3Doc7.save('cerfa.pdf'));
} else { m3Doc1.append(m3Doc7.save('cerfa.pdf')); }


var m3Doc8 = nash.doc //
		.load('models/cerfa_14580-04_M3sarl_sans_volet_social.pdf') //
		.apply (formFieldsPers4);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(m3Doc8.save('cerfa.pdf'));
} else { m3Doc1.append(m3Doc8.save('cerfa.pdf')); }
}

/*
 * Ajout de l'intercalaire M3 SARL 5
 */
 
if (pouvoir.length > 4) {
		
var m3Doc9 = nash.doc //
		.load('models/cerfa_14580-04_M3sarl_avec_volet_social.pdf') //
		.apply (formFieldsPers5);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(m3Doc9.save('cerfa.pdf'));
} else { m3Doc1.append(m3Doc9.save('cerfa.pdf')); }


var m3Doc10 = nash.doc //
		.load('models/cerfa_14580-04_M3sarl_sans_volet_social.pdf') //
		.apply (formFieldsPers5);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(m3Doc10.save('cerfa.pdf'));
} else { m3Doc1.append(m3Doc10.save('cerfa.pdf')); }
}
}

/*
 * Ajout de l'intercalaire M3 HORS SARL 1
 */
if (((dissolution.cadreIdentiteLiquidateur.autreDirigeant
or Value('id').of(objet.objetModification).contains('dirigeant')
or (ouverture.ajoutFondePouvoir and ($M2.dirigeant1Sarl.autrePersonneLieeEtablissement or $M2.dirigeant1.autrePersonneLieeEtablissement)))
and not Value('id').of(identite.entrepriseFormeJuridique).eq('5499') and not Value('id').of(identite.entrepriseFormeJuridique).eq('5410')
and not Value('id').of(identite.entrepriseFormeJuridique).eq('5415') and not Value('id').of(identite.entrepriseFormeJuridique).eq('5422')
and not Value('id').of(identite.entrepriseFormeJuridique).eq('5426') and not Value('id').of(identite.entrepriseFormeJuridique).eq('5430')
and not Value('id').of(identite.entrepriseFormeJuridique).eq('5431') and not Value('id').of(identite.entrepriseFormeJuridique).eq('5432')
and not Value('id').of(identite.entrepriseFormeJuridique).eq('5442') and not Value('id').of(identite.entrepriseFormeJuridique).eq('5443')
and not Value('id').of(identite.entrepriseFormeJuridique).eq('5451') and not Value('id').of(identite.entrepriseFormeJuridique).eq('5453')
and not Value('id').of(identite.entrepriseFormeJuridique).eq('5454') and not Value('id').of(identite.entrepriseFormeJuridique).eq('5455')
and not Value('id').of(identite.entrepriseFormeJuridique).eq('5458') and not Value('id').of(identite.entrepriseFormeJuridique).eq('5459')
and not Value('id').of(identite.entrepriseFormeJuridique).eq('5460') and not Value('id').of(identite.entrepriseFormeJuridique).eq('5485')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M'))
or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')
and (modifFormeJuridique.formeJuridiqueModifDirigeant
or Value('id').of(objet.objetModification).contains('dirigeant')
or (ouverture.ajoutFondePouvoir and ($M2.dirigeant1Sarl.autrePersonneLieeEtablissement or $M2.dirigeant1.autrePersonneLieeEtablissement)))
and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5499') and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5410')
and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5415') and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5422')
and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5426') and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5430')
and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5431') and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5432')
and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5442') and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5443')
and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5451') and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5453')
and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5454') and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5455')
and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5458') and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5459')
and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5460') and not Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5485'))) {

var m3BDoc1 = nash.doc //
		.load('models/cerfa_11683-03_M3_avec_volet_social.pdf') //
		.apply (formFieldsPers6);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(m3BDoc1.save('cerfa.pdf'));
}

var m3BDoc2 = nash.doc //
		.load('models/cerfa_11683-03_M3_sans_volet_social.pdf') //
		.apply (formFieldsPers6);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(m3BDoc2.save('cerfa.pdf'));
} else { m3BDoc1.append(m3BDoc2.save('cerfa.pdf')); }

/*
 * Ajout de l'intercalaire M3 HORS SARL 2
 */

if ((representants.length > 2) or $M2.dirigeant5.autrePersonneLieeEtablissement or (fondePouvoir.length > 1)) {
var m3BDoc3 = nash.doc //
		.load('models/cerfa_11683-03_M3_avec_volet_social.pdf') //
		.apply (formFieldsPers7);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(m3BDoc3.save('cerfa.pdf'));
} else { m3BDoc1.append(m3BDoc3.save('cerfa.pdf')); }


var m3BDoc4 = nash.doc //
		.load('models/cerfa_11683-03_M3_sans_volet_social.pdf') //
		.apply (formFieldsPers7);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(m3BDoc4.save('cerfa.pdf'));
} else { m3BDoc1.append(m3BDoc4.save('cerfa.pdf')); }
}

/*
 * Ajout de l'intercalaire M3 HORS SARL 3
 */

if ((representants.length > 4) or $M2.dirigeant10.autrePersonneLieeEtablissement or (fondePouvoir.length > 2)) {
var m3BDoc5 = nash.doc //
		.load('models/cerfa_11683-03_M3_avec_volet_social.pdf') //
		.apply (formFieldsPers8);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(m3BDoc5.save('cerfa.pdf'));
} else { m3BDoc1.append(m3BDoc5.save('cerfa.pdf')); }


var m3BDoc6 = nash.doc //
		.load('models/cerfa_11683-03_M3_sans_volet_social.pdf') //
		.apply (formFieldsPers8);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(m3BDoc6.save('cerfa.pdf'));
} else { m3BDoc1.append(m3BDoc6.save('cerfa.pdf')); }
}

/*
 * Ajout de l'intercalaire M3 HORS SARL 4
 */

if ((representants.length > 6) or (fondePouvoir.length > 3)) {
var m3BDoc7 = nash.doc //
		.load('models/cerfa_11683-03_M3_avec_volet_social.pdf') //
		.apply (formFieldsPers9);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(m3BDoc7.save('cerfa.pdf'));
} else { m3BDoc1.append(m3BDoc7.save('cerfa.pdf')); }


var m3BDoc8 = nash.doc //
		.load('models/cerfa_11683-03_M3_sans_volet_social.pdf') //
		.apply (formFieldsPers9);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(m3BDoc8.save('cerfa.pdf'));
} else { m3BDoc1.append(m3BDoc8.save('cerfa.pdf')); }
}

/*
 * Ajout de l'intercalaire M3 HORS SARL 5
 */

if ((representants.length > 8) or (fondePouvoir.length > 4)) {
var m3BDoc9 = nash.doc //
		.load('models/cerfa_11683-03_M3_avec_volet_social.pdf') //
		.apply (formFieldsPers10);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(m3BDoc9.save('cerfa.pdf'));
} else { m3BDoc1.append(m3BDoc9.save('cerfa.pdf')); }


var m3BDoc10 = nash.doc //
		.load('models/cerfa_11683-03_M3_sans_volet_social.pdf') //
		.apply (formFieldsPers10);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(m3BDoc10.save('cerfa.pdf'));
} else { m3BDoc1.append(m3BDoc10.save('cerfa.pdf')); }
}

/*
 * Ajout de l'intercalaire M3 HORS SARL 6
 */

if ((representants.length > 10) or (fondePouvoir.length > 5)) {
var m3BDoc11 = nash.doc //
		.load('models/cerfa_11683-03_M3_avec_volet_social.pdf') //
		.apply (formFieldsPers11);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(m3BDoc11.save('cerfa.pdf'));
} else { m3BDoc1.append(m3BDoc11.save('cerfa.pdf')); }


var m3BDoc12 = nash.doc //
		.load('models/cerfa_11683-03_M3_sans_volet_social.pdf') //
		.apply (formFieldsPers11);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(m3BDoc12.save('cerfa.pdf'));
} else { m3BDoc1.append(m3BDoc12.save('cerfa.pdf')); }
}
}

/*
 * Ajout de l'intercalaire M' 
 */
if (fermeture.autreFermeture or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('fusion')) {
	var mPrimeDoc1 = nash.doc //
		.load('models/cerfa_11681-02_Mprime_avec_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(mPrimeDoc1.save('cerfa.pdf'));

	var mPrimeDoc2 = nash.doc //
		.load('models/cerfa_11681-02_Mprime_sans_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(mPrimeDoc2.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 1
 */

if (socialDirigeant1.voletSocialNumeroSecuriteSocialTNS != null) {
	var tnsDoc1 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers1);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(tnsDoc1.save('cerfa.pdf'));
} else if (((Value('id').of(identite.entrepriseFormeJuridique).eq('5499') or Value('id').of(identite.entrepriseFormeJuridique).eq('5410')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5415')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5422')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5426')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5430')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5431')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5432')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5442')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5443')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5451')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5453')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5454')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5455')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5458')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5459')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5460')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5485'))
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M'))
or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')
and (Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5499') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5410')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5415')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5422')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5426')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5430')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5431')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5432')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5442')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5443')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5451')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5453')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5454')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5455')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5458')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5459')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5460')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5485')))) { 
	m3Doc1.append(tnsDoc1.save('cerfa.pdf')); 
} else { m3BDoc1.append(tnsDoc1.save('cerfa.pdf')); }
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 2
 */

if (socialDirigeant2.voletSocialNumeroSecuriteSocialTNS != null) {
	var tnsDoc2 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers2);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(tnsDoc2.save('cerfa.pdf'));
} else if (((Value('id').of(identite.entrepriseFormeJuridique).eq('5499') or Value('id').of(identite.entrepriseFormeJuridique).eq('5410')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5415')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5422')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5426')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5430')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5431')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5432')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5442')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5443')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5451')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5453')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5454')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5455')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5458')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5459')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5460')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5485'))
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M'))
or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')
and (Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5499') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5410')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5415')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5422')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5426')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5430')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5431')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5432')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5442')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5443')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5451')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5453')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5454')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5455')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5458')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5459')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5460')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5485')))) { 
	m3Doc1.append(tnsDoc2.save('cerfa.pdf')); 
} else { m3BDoc1.append(tnsDoc2.save('cerfa.pdf')); }
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 3
 */

if (socialDirigeant3.voletSocialNumeroSecuriteSocialTNS != null) {
	var tnsDoc3 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers3);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(tnsDoc3.save('cerfa.pdf'));
} else if (((Value('id').of(identite.entrepriseFormeJuridique).eq('5499') or Value('id').of(identite.entrepriseFormeJuridique).eq('5410')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5415')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5422')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5426')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5430')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5431')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5432')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5442')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5443')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5451')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5453')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5454')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5455')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5458')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5459')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5460')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5485'))
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M'))
or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')
and (Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5499') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5410')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5415')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5422')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5426')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5430')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5431')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5432')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5442')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5443')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5451')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5453')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5454')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5455')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5458')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5459')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5460')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5485')))) { 
	m3Doc1.append(tnsDoc3.save('cerfa.pdf')); 
} else { m3BDoc1.append(tnsDoc3.save('cerfa.pdf')); }
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 4
 */

if (socialDirigeant4.voletSocialNumeroSecuriteSocialTNS != null) {
	var tnsDoc4 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers4);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(tnsDoc4.save('cerfa.pdf'));
} else if (((Value('id').of(identite.entrepriseFormeJuridique).eq('5499') or Value('id').of(identite.entrepriseFormeJuridique).eq('5410')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5415')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5422')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5426')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5430')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5431')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5432')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5442')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5443')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5451')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5453')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5454')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5455')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5458')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5459')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5460')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5485'))
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M'))
or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')
and (Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5499') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5410')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5415')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5422')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5426')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5430')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5431')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5432')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5442')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5443')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5451')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5453')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5454')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5455')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5458')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5459')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5460')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5485')))) { 
	m3Doc1.append(tnsDoc4.save('cerfa.pdf')); 
} else { m3BDoc1.append(tnsDoc4.save('cerfa.pdf')); }
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 5
 */

if (socialDirigeant5.voletSocialNumeroSecuriteSocialTNS != null) {
	var tnsDoc5 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers5);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(tnsDoc5.save('cerfa.pdf'));
} else if (((Value('id').of(identite.entrepriseFormeJuridique).eq('5499') or Value('id').of(identite.entrepriseFormeJuridique).eq('5410')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5415')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5422')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5426')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5430')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5431')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5432')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5442')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5443')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5451')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5453')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5454')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5455')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5458')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5459')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5460')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5485'))
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M'))
or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')
and (Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5499') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5410')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5415')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5422')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5426')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5430')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5431')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5432')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5442')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5443')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5451')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5453')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5454')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5455')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5458')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5459')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5460')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5485')))) { 
	m3Doc1.append(tnsDoc5.save('cerfa.pdf')); 
} else { m3BDoc1.append(tnsDoc5.save('cerfa.pdf')); }
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 6
 */

if (socialDirigeant6.voletSocialNumeroSecuriteSocialTNS != null) {
	var tnsDoc6 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers6);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(tnsDoc6.save('cerfa.pdf'));
} else if (((Value('id').of(identite.entrepriseFormeJuridique).eq('5499') or Value('id').of(identite.entrepriseFormeJuridique).eq('5410')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5415')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5422')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5426')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5430')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5431')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5432')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5442')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5443')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5451')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5453')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5454')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5455')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5458')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5459')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5460')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5485'))
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M'))
or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')
and (Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5499') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5410')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5415')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5422')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5426')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5430')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5431')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5432')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5442')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5443')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5451')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5453')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5454')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5455')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5458')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5459')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5460')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5485')))) { 
	m3Doc1.append(tnsDoc6.save('cerfa.pdf')); 
} else { m3BDoc1.append(tnsDoc6.save('cerfa.pdf')); }
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 7
 */

if (socialDirigeant7.voletSocialNumeroSecuriteSocialTNS != null) {
	var tnsDoc7 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers7);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(tnsDoc7.save('cerfa.pdf'));
} else if (((Value('id').of(identite.entrepriseFormeJuridique).eq('5499') or Value('id').of(identite.entrepriseFormeJuridique).eq('5410')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5415')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5422')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5426')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5430')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5431')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5432')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5442')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5443')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5451')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5453')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5454')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5455')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5458')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5459')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5460')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5485'))
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M'))
or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')
and (Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5499') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5410')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5415')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5422')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5426')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5430')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5431')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5432')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5442')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5443')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5451')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5453')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5454')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5455')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5458')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5459')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5460')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5485')))) { 
	m3Doc1.append(tnsDoc7.save('cerfa.pdf')); 
} else { m3BDoc1.append(tnsDoc7.save('cerfa.pdf')); }
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 8
 */

if (socialDirigeant8.voletSocialNumeroSecuriteSocialTNS != null) {
	var tnsDoc8 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers8);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(tnsDoc8.save('cerfa.pdf'));
} else if (((Value('id').of(identite.entrepriseFormeJuridique).eq('5499') or Value('id').of(identite.entrepriseFormeJuridique).eq('5410')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5415')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5422')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5426')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5430')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5431')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5432')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5442')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5443')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5451')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5453')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5454')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5455')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5458')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5459')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5460')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5485'))
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M'))
or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')
and (Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5499') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5410')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5415')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5422')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5426')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5430')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5431')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5432')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5442')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5443')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5451')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5453')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5454')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5455')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5458')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5459')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5460')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5485')))) { 
	m3Doc1.append(tnsDoc8.save('cerfa.pdf')); 
} else { m3BDoc1.append(tnsDoc8.save('cerfa.pdf')); }
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 9
 */

if (socialDirigeant9.voletSocialNumeroSecuriteSocialTNS != null) {
	var tnsDoc9 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers9);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(tnsDoc9.save('cerfa.pdf'));
} else if (((Value('id').of(identite.entrepriseFormeJuridique).eq('5499') or Value('id').of(identite.entrepriseFormeJuridique).eq('5410')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5415')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5422')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5426')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5430')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5431')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5432')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5442')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5443')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5451')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5453')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5454')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5455')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5458')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5459')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5460')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5485'))
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M'))
or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')
and (Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5499') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5410')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5415')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5422')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5426')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5430')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5431')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5432')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5442')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5443')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5451')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5453')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5454')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5455')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5458')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5459')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5460')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5485')))) { 
	m3Doc1.append(tnsDoc9.save('cerfa.pdf')); 
} else { m3BDoc1.append(tnsDoc9.save('cerfa.pdf')); }
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 10
 */

if (socialDirigeant10.voletSocialNumeroSecuriteSocialTNS != null) {
	var tnsDoc10 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers10);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(tnsDoc10.save('cerfa.pdf'));
} else if (((Value('id').of(identite.entrepriseFormeJuridique).eq('5499') or Value('id').of(identite.entrepriseFormeJuridique).eq('5410')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5415')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5422')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5426')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5430')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5431')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5432')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5442')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5443')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5451')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5453')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5454')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5455')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5458')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5459')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5460')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5485'))
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M'))
or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')
and (Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5499') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5410')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5415')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5422')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5426')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5430')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5431')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5432')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5442')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5443')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5451')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5453')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5454')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5455')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5458')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5459')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5460')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5485')))) { 
	m3Doc1.append(tnsDoc10.save('cerfa.pdf')); 
} else { m3BDoc1.append(tnsDoc10.save('cerfa.pdf')); }
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 11
 */

if (socialDirigeant11.voletSocialNumeroSecuriteSocialTNS != null) {
	var tnsDoc11 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers11);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(tnsDoc11.save('cerfa.pdf'));
} else if (((Value('id').of(identite.entrepriseFormeJuridique).eq('5499') or Value('id').of(identite.entrepriseFormeJuridique).eq('5410')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5415')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5422')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5426')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5430')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5431')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5432')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5442')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5443')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5451')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5453')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5454')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5455')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5458')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5459')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5460')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5485'))
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M'))
or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')
and (Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5499') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5410')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5415')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5422')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5426')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5430')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5431')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5432')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5442')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5443')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5451')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5453')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5454')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5455')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5458')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5459')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5460')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5485')))) { 
	m3Doc1.append(tnsDoc11.save('cerfa.pdf')); 
} else { m3BDoc1.append(tnsDoc11.save('cerfa.pdf')); }
}

/*
 * Ajout de l'intercalaire TNS Dirigeant 12
 */

if (socialDirigeant12.voletSocialNumeroSecuriteSocialTNS != null) {
	var tnsDoc12 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers12);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(tnsDoc12.save('cerfa.pdf'));
} else if (((Value('id').of(identite.entrepriseFormeJuridique).eq('5499') or Value('id').of(identite.entrepriseFormeJuridique).eq('5410')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5415')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5422')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5426')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5430')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5431')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5432')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5442')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5443')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5451')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5453')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5454')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5455')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5458')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5459')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5460')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5485'))
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M'))
or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')
and (Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5499') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5410')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5415')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5422')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5426')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5430')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5431')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5432')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5442')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5443')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5451')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5453')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5454')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5455')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5458')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5459')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5460')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5485')))) { 
	m3Doc1.append(tnsDoc12.save('cerfa.pdf')); 
} else { m3BDoc1.append(tnsDoc12.save('cerfa.pdf')); }
}

/*
 * Ajout de l'intercalaire TNS Conjoint associé 1
 */

if (socialAssocie1.voletSocialNumeroSecuriteSocialTNS != null) {
	var tnsDoc13 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers13);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(tnsDoc13.save('cerfa.pdf'));
} else { m3Doc1.append(tnsDoc13.save('cerfa.pdf')); }
}

/*
 * Ajout de l'intercalaire TNS Conjoint associé 2
 */

if (socialAssocie2.voletSocialNumeroSecuriteSocialTNS != null) {
	var tnsDoc14 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers14);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(tnsDoc14.save('cerfa.pdf'));
} else { m3Doc1.append(tnsDoc14.save('cerfa.pdf')); }
}

/*
 * Ajout de l'intercalaire TNS Conjoint associé 3
 */

if (socialAssocie3.voletSocialNumeroSecuriteSocialTNS != null) {
	var tnsDoc15 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers15);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(tnsDoc15.save('cerfa.pdf'));
} else { m3Doc1.append(tnsDoc15.save('cerfa.pdf')); }
}

/*
 * Ajout de l'intercalaire TNS Conjoint associé 4
 */

if (socialAssocie4.voletSocialNumeroSecuriteSocialTNS != null) {
	var tnsDoc16 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers16);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(tnsDoc16.save('cerfa.pdf'));
} else { m3Doc1.append(tnsDoc16.save('cerfa.pdf')); }
}

/*
 * Ajout de l'intercalaire TNS Conjoint associé 5
 */

if (socialAssocie5.voletSocialNumeroSecuriteSocialTNS != null) {
	var tnsDoc17 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFieldsPers17);
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	cerfaDoc1.append(tnsDoc17.save('cerfa.pdf'));
} else { m3Doc1.append(tnsDoc17.save('cerfa.pdf')); }
}
	
/*
 * Ajout de l'intercalaire JQPA 1
 */
 
  if (jqpa1.jqpaActiviteJqpaPP != null) {
	var p2JQPADoc1 = nash.doc //
		.load('models/cerfa_14077-02_JQPA.pdf') //
		.apply (formFieldsPers19);
	cerfaDoc1.append(p2JQPADoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire JQPA 2
 */
 
 if (jqpa2.jqpaActiviteJqpaPP != null) {
	var p2JQPADoc2 = nash.doc //
		.load('models/cerfa_14077-02_JQPA.pdf') //
		.apply (formFieldsPers20);
	cerfaDoc1.append(p2JQPADoc2.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire JQPA 3
 */
 
  if (jqpa3.jqpaActiviteJqpaPP != null) {
	var p2JQPADoc3 = nash.doc //
		.load('models/cerfa_14077-02_JQPA.pdf') //
		.apply (formFieldsPers21);
	cerfaDoc1.append(p2JQPADoc3.save('cerfa.pdf'));
}  

/*
 * Ajout de l'intercalaire NDI 1er exemplaire
 */
 
 if (activite.etablissementNomDomaine != null or activiteInfo.cadreEtablissementNomDomaine.modifNomDomaine != null or identite.nomDomainePresence) {
	var p2NDIDoc1 = nash.doc //
		.load('models/cerfa_14943-01_NDI_volet1.pdf') //
		.apply (formFields);
	cerfaDoc1.append(p2NDIDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire NDI 2ème exemplaire
 */
 
 if (activite.etablissementNomDomaine != null or activiteInfo.cadreEtablissementNomDomaine.modifNomDomaine != null or identite.nomDomainePresence) {
	var p2NDIDoc2 = nash.doc //
		.load('models/cerfa_14943-01_NDI_volet2.pdf') //
		.apply (formFields);
	cerfaDoc1.append(p2NDIDoc2.save('cerfa.pdf'));
}


/* Conditionnement du nom de la zoneId pour la signature en fonction du cerfa principal */
var zoneId = 'signatureM3';
if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
	zoneId = 'signature';
}
nash.instance.load('zoneId.xml').bind('context.signature', { 'zoneId' : zoneId });
log.info('Signature zone identifier : {}', zoneId);


/*
 * Ajout des PJs
 */

var pjUser = [];

function pushPjPreview(fld) {
	fld.forEach(function (elm) {
        pjUser.push(elm);
    });
}

// PJ Signataire

pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire);

if(Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPouvoir);
}

// PJ société

if(Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('10M') 
or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') 	
or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('capital')
and Value('id').of(modifCapital.modifCapitalType).contains('15M'))
or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('fusion')
or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('18M')
and (Value('id').of(modifEss.modifEssAdhesionSortie).eq('modifEssAdhesion') or modifEss.modifStatutESS))
or modifSocieteMission.modifStatutSocieteMission
or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('16M')
or activiteInfo.cadreEtablissementIdentification.modifNomCommercialStatut
or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
and (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege')
or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal')))
or activite.modifObjetSocial) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjStatuts);
}

if(Value('id').of(objet.objetModification).contains('dirigeant')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('10M') 
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') 	
and not Value('id').of(modifCapital.modifCapitalType).contains('15M')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('fusion')
and not Value('id').of(modifEss.modifEssAdhesionSortie).eq('modifEssAdhesion') 
and not modifEss.modifStatutESS
and not modifSocieteMission.modifStatutSocieteMission
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('16M')
and not activite.modifObjetSocial and not activiteInfo.cadreEtablissementIdentification.modifNomCommercialStatut) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjStatutsBis);
}
	
if(Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('10M')
or Value('id').of(objet.objetModification).contains('dirigeant')
or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('18M')
and (Value('id').of(modifEss.modifEssAdhesionSortie).eq('modifEssAdhesion') or modifEss.modifStatutESS))
or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('16M')
or activiteInfo.cadreEtablissementIdentification.modifNomCommercialStatut
or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
and (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege')
or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal')))
or activite.modifObjetSocial) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPV);
}

if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('capital')
and Value('id').of(modifCapital.modifCapitalType).contains('25M')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPVContinuation);
}

if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('capital')
and Value('id').of(modifCapital.modifCapitalType).contains('26M')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPVReconstitution);
} 

if((Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('capital')
and Value('id').of(modifCapital.modifCapitalType).contains('15M'))
or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('fusion')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPVEnregistre);
}

if(Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('fusion')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjRapportFusion);
}

if(Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPVEnregistreDisso);
}

if(Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPVEnregistreTransfo);
}

if((Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('10M')
or Value('id').of(objet.objetModification).contains('dirigeant')
or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')
or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('capital')
and (Value('id').of(modifCapital.modifCapitalType).contains('15M')
or Value('id').of(modifCapital.modifCapitalType).contains('25M'))) 
or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('fusion')
or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') 
or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('16M')
and Value('id').of(modifDuree.modifDureeExerciceSocial).contains('modifDuree'))
or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
and (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege')
or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal'))
and objet.cadreObjetModificationEtablissement.transfertDepartement)
or activite.modifObjetSocial)
and not (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
and (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege')
or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal'))
and not objet.cadreObjetModificationEtablissement.transfertDepartement)) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjJournalSociete);
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
and (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege')
or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal'))
and not objet.cadreObjetModificationEtablissement.transfertDepartement) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjJournalSocieteNewSiege);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjJournalSocieteOldSiege);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjListeSiege);
}

if(Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('capital')
and Value('id').of(modifCapital.modifCapitalType).contains('15M')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('fusion')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjRapportCAA);
}

if (ouverture.ouvertureDepartement
and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')
and (Value('id').of(identite.entrepriseFormeJuridique).eq('3120')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5800')
or Value('id').of(identite.entrepriseFormeJuridique).eq('3110'))) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjStatutsEtr);
}

if (Value('id').of(objet.objetModification).contains('dirigeant')
and (Value('id').of(identite.entrepriseFormeJuridique).eq('5202') or Value('id').of(identite.entrepriseFormeJuridique).eq('5203')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5710') or Value('id').of(identite.entrepriseFormeJuridique).eq('5720')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5785') or Value('id').of(identite.entrepriseFormeJuridique).eq('5599')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5505') or Value('id').of(identite.entrepriseFormeJuridique).eq('5510')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5515') or Value('id').of(identite.entrepriseFormeJuridique).eq('5520')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5522') or Value('id').of(identite.entrepriseFormeJuridique).eq('5525')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5530') or Value('id').of(identite.entrepriseFormeJuridique).eq('5531')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5532') or Value('id').of(identite.entrepriseFormeJuridique).eq('5542')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5543') or Value('id').of(identite.entrepriseFormeJuridique).eq('5546')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5547') or Value('id').of(identite.entrepriseFormeJuridique).eq('5548')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5551') or Value('id').of(identite.entrepriseFormeJuridique).eq('5552')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5553') or Value('id').of(identite.entrepriseFormeJuridique).eq('5554')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5555') or Value('id').of(identite.entrepriseFormeJuridique).eq('5558')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5559') or Value('id').of(identite.entrepriseFormeJuridique).eq('5560')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5699') or Value('id').of(identite.entrepriseFormeJuridique).eq('5605')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5610') or Value('id').of(identite.entrepriseFormeJuridique).eq('5615')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5620') or Value('id').of(identite.entrepriseFormeJuridique).eq('5622')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5625') or Value('id').of(identite.entrepriseFormeJuridique).eq('5630')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5631') or Value('id').of(identite.entrepriseFormeJuridique).eq('5632')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5642') or Value('id').of(identite.entrepriseFormeJuridique).eq('5643')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5646') or Value('id').of(identite.entrepriseFormeJuridique).eq('5647')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5648') or Value('id').of(identite.entrepriseFormeJuridique).eq('5651')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5652') or Value('id').of(identite.entrepriseFormeJuridique).eq('5653')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5654') or Value('id').of(identite.entrepriseFormeJuridique).eq('5655')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5658') or Value('id').of(identite.entrepriseFormeJuridique).eq('5659')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5660') or Value('id').of(identite.entrepriseFormeJuridique).eq('5585')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5685') or Value('id').of(identite.entrepriseFormeJuridique).eq('5306')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5307') or Value('id').of(identite.entrepriseFormeJuridique).eq('5308')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5309') or Value('id').of(identite.entrepriseFormeJuridique).eq('5385'))
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDecisionJudiciaire);
}

if ((Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5710') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5720')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5785') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5599')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5505') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5510')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5515') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5520')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5522') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5525')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5530') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5531')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5532') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5542')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5543') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5546')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5547') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5548')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5551') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5552')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5553') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5554')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5555') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5558')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5559') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5560')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5699') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5605')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5610') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5615')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5620') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5622')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5625') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5630')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5631') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5632')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5642') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5643')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5646') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5647')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5648') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5651')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5652') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5653')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5654') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5655')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5658') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5659')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5660') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5585')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5685') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5308')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5309') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5385'))
and Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjRapportCATransfo);
}

// PJ Liquidateur si disso

if(Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M')
	and not dissolution.cadreIdentiteLiquidateur.liquidateurDejaConnu
	and Value('id').of(dissolution.cadreIdentiteLiquidateur.liquidateurPersonnaliteJuridique).eq('personnePhysique')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCLiquidateur);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDLiquidateur);
}

if(Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M')
and Value('id').of(dissolution.cadreIdentiteLiquidateur.liquidateurPersonnaliteJuridique).eq('personneMorale')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatLiquidateur);
}

// PJ Etablissement

// Création

// Si  51M (prise), 52M, 54M, 11M

if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
and ((objet.cadreObjetModificationEtablissement.lieuActiviteSiege
and Value('id').of(objet.cadreObjetModificationEtablissement.priseRepriseActivite).eq('prise'))
or not objet.cadreObjetModificationEtablissement.lieuActiviteSiege))
or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
and (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege')
or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal')
or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
and Value('id').of(ouverture.etablissementDejaConnu).eq('non'))) 
and (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation')
or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsCreation')
or Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCocheAutre')
or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsCocheAutre')
or origine.precedentExploitant.cessionFonds or origine.precedentExploitant.fondsArtisanal)) {

	if (origine.creationDomiciliation) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjContratDomiciliation);
	} else if (not origine.creationDomiciliation or origine.precedentExploitant.cessionFonds or origine.precedentExploitant.fondsArtisanal) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjEtablissementCreation);
	}
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
and (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege')
or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal')
or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
and Value('id').of(ouverture.etablissementDejaConnu).eq('oui')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjEtablissementCreationBis);
}

// Achat / Apport

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
and ((objet.cadreObjetModificationEtablissement.lieuActiviteSiege
and Value('id').of(objet.cadreObjetModificationEtablissement.priseRepriseActivite).eq('prise'))
or not objet.cadreObjetModificationEtablissement.lieuActiviteSiege))
or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
and Value('id').of(ouverture.etablissementDejaConnu).eq('non')) 
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M')
or Value('id').of(activite.activiteEvenement).eq('61M')
or Value('id').of(activite.activiteEvenement).eq('61M62M')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63M')) {

if (((Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsAchat')
or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsAchat'))
and not origine.precedentExploitant.fondsArtisanal 
and not origine.precedentExploitant.cessionFonds)
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63M')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjAchat);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjAchatPublication);
}

if ((Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsApport')
or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsApport'))
and not origine.precedentExploitant.fondsArtisanal 
and not origine.precedentExploitant.cessionFonds) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjApport);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjApportPublication);
}
}

// Location-gérance / Gérance-mandat

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
and ((objet.cadreObjetModificationEtablissement.lieuActiviteSiege
and Value('id').of(objet.cadreObjetModificationEtablissement.priseRepriseActivite).eq('prise'))
or not objet.cadreObjetModificationEtablissement.lieuActiviteSiege)) 
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M')
or Value('id').of(activite.activiteEvenement).eq('61M')
or Value('id').of(activite.activiteEvenement).eq('61M62M')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64M')
or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
and Value('id').of(ouverture.etablissementDejaConnu).eq('non'))) {

if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsLocationGerance')
	or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsLocationGerance')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjLocationGerance);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjLocationGerancePublication);
}

if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsGeranceMandat')
	or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsGeranceMandat')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjGeranceMandat);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjGeranceMandatPublication);
}
}

// Renouvellement du contrat de location-gérance ou de gérance-mandat

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64M')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjLocationGeranceBis);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjLocationGerancePublicationBis);
}

// PJ Dirigeant

// Dirigeant PP

// Dirigeant 1

if ((ouverture.ajoutFondePouvoir or dirigeantSARL1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null
or dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null or dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)
and not Value('id').of(dirigeant1.personneLieeQualite).eq('71') and not Value('id').of(dirigeant1.personneLieeQualite).eq('72')) {
if (Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantNouveau') or Value('id').of(dirigeantSARL1.personneLieeObjet).eq('dirigeantNouveau') or ouverture.ajoutFondePouvoir) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant1);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIDirigeant1);
}
if (Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantMaintenu') and dirigeant1.dirigeantMaintenuModif) or Value('id').of(dirigeantSARL1.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeantSARL1.personneLieeObjet).eq('dirigeantMaintenu') and dirigeantSARL1.dirigeantMaintenuModif)) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIDirigeantModif1);
}
if (Value('id').of(dirigeantSARL1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') 
or (Value('id').of(dirigeantSARL1.cadre2InfosConjoint.objetModifConjoint).eq('modifMentionNouveauConjoint') 
and Value('id').of(dirigeantSARL1.cadre2InfosConjoint.statutModifConjoint).eq('modifStatutCollaborateur'))) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDConjoint1);
}
if (dirigeantSARL1.statutConjointCollaborateurGeranceNonMaj != null
or dirigeantSARL1.statutConjointCollaborateurGeranceMaj != null 
or Value('id').of(dirigeantSARL1.cadre2InfosConjoint.objetModifConjoint).eq('modifMentionNouveauConjoint')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjChoixStatutConjoint1);
}
}

// Dirigeant 2

if ((dirigeantSARL2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null
or dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null 
or dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)
and not Value('id').of(dirigeant2.personneLieeQualite).eq('71') and not Value('id').of(dirigeant2.personneLieeQualite).eq('72')) {
if (Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantNouveau') or Value('id').of(dirigeantSARL2.personneLieeObjet).eq('dirigeantNouveau')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant2);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIDirigeant2);
}
if (Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantMaintenu') and dirigeant2.dirigeantMaintenuModif) or Value('id').of(dirigeantSARL2.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeantSARL2.personneLieeObjet).eq('dirigeantMaintenu') and dirigeantSARL2.dirigeantMaintenuModif)) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIDirigeantModif2);
}
if (Value('id').of(dirigeantSARL2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') 
or (Value('id').of(dirigeantSARL2.cadre2InfosConjoint.objetModifConjoint).eq('modifMentionNouveauConjoint') 
and Value('id').of(dirigeantSARL2.cadre2InfosConjoint.statutModifConjoint).eq('modifStatutCollaborateur'))) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDConjoint2);
}
if (dirigeantSARL2.statutConjointCollaborateurGeranceNonMaj != null
or dirigeantSARL2.statutConjointCollaborateurGeranceMaj != null 
or Value('id').of(dirigeantSARL2.cadre2InfosConjoint.objetModifConjoint).eq('modifMentionNouveauConjoint')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjChoixStatutConjoint2);
}
}

// Dirigeant 3

if ((dirigeantSARL3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null
or dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null 
or dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)
and not Value('id').of(dirigeant3.personneLieeQualite).eq('71') and not Value('id').of(dirigeant3.personneLieeQualite).eq('72')) {
if (Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantNouveau') or Value('id').of(dirigeantSARL3.personneLieeObjet).eq('dirigeantNouveau')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant3);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIDirigeant3);
}
if (Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantMaintenu') and dirigeant3.dirigeantMaintenuModif) or Value('id').of(dirigeantSARL3.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeantSARL3.personneLieeObjet).eq('dirigeantMaintenu') and dirigeantSARL3.dirigeantMaintenuModif)) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIDirigeantModif3);
}
if (Value('id').of(dirigeantSARL3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') 
or (Value('id').of(dirigeantSARL3.cadre2InfosConjoint.objetModifConjoint).eq('modifMentionNouveauConjoint') 
and Value('id').of(dirigeantSARL3.cadre2InfosConjoint.statutModifConjoint).eq('modifStatutCollaborateur'))) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDConjoint3);
}
if (dirigeantSARL3.statutConjointCollaborateurGeranceNonMaj != null
or dirigeantSARL3.statutConjointCollaborateurGeranceMaj != null 
or Value('id').of(dirigeantSARL3.cadre2InfosConjoint.objetModifConjoint).eq('modifMentionNouveauConjoint')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjChoixStatutConjoint3);
}
}

// Dirigeant 4

if ((dirigeantSARL4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null
or dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null 
or dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)
and not Value('id').of(dirigeant4.personneLieeQualite).eq('71') and not Value('id').of(dirigeant4.personneLieeQualite).eq('72')) {
if (Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantNouveau') or Value('id').of(dirigeantSARL4.personneLieeObjet).eq('dirigeantNouveau')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant4);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIDirigeant4);
}
if (Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantMaintenu') and dirigeant4.dirigeantMaintenuModif) or Value('id').of(dirigeantSARL4.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeantSARL4.personneLieeObjet).eq('dirigeantMaintenu') and dirigeantSARL4.dirigeantMaintenuModif)) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIDirigeantModif4);
}
if (Value('id').of(dirigeantSARL4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') 
or (Value('id').of(dirigeantSARL4.cadre2InfosConjoint.objetModifConjoint).eq('modifMentionNouveauConjoint') 
and Value('id').of(dirigeantSARL4.cadre2InfosConjoint.statutModifConjoint).eq('modifStatutCollaborateur'))) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDConjoint4);
}
if (dirigeantSARL4.statutConjointCollaborateurGeranceNonMaj != null
or dirigeantSARL4.statutConjointCollaborateurGeranceMaj != null 
or Value('id').of(dirigeantSARL4.cadre2InfosConjoint.objetModifConjoint).eq('modifMentionNouveauConjoint')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjChoixStatutConjoint4);
}
}

// Dirigeant 5

if ((dirigeantSARL5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null
or dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null 
or dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)
and not Value('id').of(dirigeant5.personneLieeQualite).eq('71') and not Value('id').of(dirigeant5.personneLieeQualite).eq('72')) {
if (Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantNouveau') or Value('id').of(dirigeantSARL5.personneLieeObjet).eq('dirigeantNouveau')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant5);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIDirigeant5);
}
if (Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantMaintenu') and dirigeant5.dirigeantMaintenuModif) or Value('id').of(dirigeantSARL5.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeantSARL5.personneLieeObjet).eq('dirigeantMaintenu') and dirigeantSARL5.dirigeantMaintenuModif)) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIDirigeantModif5);
}
if (Value('id').of(dirigeantSARL5.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') 
or (Value('id').of(dirigeantSARL5.cadre2InfosConjoint.objetModifConjoint).eq('modifMentionNouveauConjoint') 
and Value('id').of(dirigeantSARL5.cadre2InfosConjoint.statutModifConjoint).eq('modifStatutCollaborateur'))) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDConjoint5);
}
if (dirigeantSARL5.statutConjointCollaborateurGeranceNonMaj != null
or dirigeantSARL5.statutConjointCollaborateurGeranceMaj != null 
or Value('id').of(dirigeantSARL5.cadre2InfosConjoint.objetModifConjoint).eq('modifMentionNouveauConjoint')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjChoixStatutConjoint5);
}
}

// Dirigeant 6

if ((dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null 
or dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)
and not Value('id').of(dirigeant6.personneLieeQualite).eq('71') and not Value('id').of(dirigeant6.personneLieeQualite).eq('72')) {
if (Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantNouveau')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant6);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIDirigeant6);
}
if (Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantMaintenu') and dirigeant6.dirigeantMaintenuModif)) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIDirigeantModif6);
}
}

// Dirigeant 7

if ((dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null 
or dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)
and not Value('id').of(dirigeant7.personneLieeQualite).eq('71') and not Value('id').of(dirigeant7.personneLieeQualite).eq('72')) {
if (Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantNouveau')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant7);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIDirigeant7);
}
if (Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantMaintenu') and dirigeant7.dirigeantMaintenuModif)) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIDirigeantModif7);
}
}

// Dirigeant 8

if ((dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null 
or dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)
and not Value('id').of(dirigeant8.personneLieeQualite).eq('71') and not Value('id').of(dirigeant8.personneLieeQualite).eq('72')) {
if (Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantNouveau')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant8);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIDirigeant8);
}
if (Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantMaintenu') and dirigeant8.dirigeantMaintenuModif)) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIDirigeantModif8);
}
}

// Dirigeant 9

if ((dirigeant9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null 
or dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)
and not Value('id').of(dirigeant9.personneLieeQualite).eq('71') and not Value('id').of(dirigeant9.personneLieeQualite).eq('72')) {
if (Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantNouveau')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant9);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIDirigeant9);
}
if (Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantMaintenu') and dirigeant9.dirigeantMaintenuModif)) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIDirigeantModif9);
}
}

// Dirigeant 10

if ((dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null 
or dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)
and not Value('id').of(dirigeant10.personneLieeQualite).eq('71') and not Value('id').of(dirigeant10.personneLieeQualite).eq('72')) {
if (Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantNouveau')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant10);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIDirigeant10);
}
if (Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantMaintenu') and dirigeant10.dirigeantMaintenuModif)) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIDirigeantModif10);
}
}

// Dirigeant 11

if ((dirigeant11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null 
or dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)
and not Value('id').of(dirigeant11.personneLieeQualite).eq('71') and not Value('id').of(dirigeant11.personneLieeQualite).eq('72')) {
if (Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantNouveau')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant11);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIDirigeant11);
}
if (Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantMaintenu') and dirigeant11.dirigeantMaintenuModif)) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIDirigeantModif11);
}
}

// Dirigeant 12

if ((dirigeant12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null 
or dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)
and not Value('id').of(dirigeant12.personneLieeQualite).eq('71') and not Value('id').of(dirigeant12.personneLieeQualite).eq('72')) {
if (Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantNouveau')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCDirigeant12);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIDirigeant12);
}
if (Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantMaintenu') and dirigeant12.dirigeantMaintenuModif)) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIDirigeantModif12);
}
}

// Dirigeant PM

// Dirigeant 1

if (Value('id').of(dirigeant1.personnaliteJuridique).eq('personneMorale')
and not Value('id').of(dirigeant1.personneLieeQualite).eq('71') and not Value('id').of(dirigeant1.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantPartant')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS1);
}

// Dirigeant 2

if (Value('id').of(dirigeant2.personnaliteJuridique).eq('personneMorale')
and not Value('id').of(dirigeant2.personneLieeQualite).eq('71') and not Value('id').of(dirigeant2.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantPartant')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS2);
}

// Dirigeant 3

if (Value('id').of(dirigeant3.personnaliteJuridique).eq('personneMorale')
and not Value('id').of(dirigeant3.personneLieeQualite).eq('71') and not Value('id').of(dirigeant3.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantPartant')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS3);
}

// Dirigeant 4

if (Value('id').of(dirigeant4.personnaliteJuridique).eq('personneMorale')
and not Value('id').of(dirigeant4.personneLieeQualite).eq('71') and not Value('id').of(dirigeant4.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantPartant')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS4);
}

// Dirigeant 5

if (Value('id').of(dirigeant5.personnaliteJuridique).eq('personneMorale')
and not Value('id').of(dirigeant5.personneLieeQualite).eq('71') and not Value('id').of(dirigeant5.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantPartant')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS5);
}

// Dirigeant 6

var userMorale6 = dirigeant6.civilitePersonneMorale.personneLieePMDenomination ;

if (Value('id').of(dirigeant6.personnaliteJuridique).eq('personneMorale')
and not Value('id').of(dirigeant6.personneLieeQualite).eq('71') and not Value('id').of(dirigeant6.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantPartant')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS6);
}

// Dirigeant 7

if (Value('id').of(dirigeant7.personnaliteJuridique).eq('personneMorale')
and not Value('id').of(dirigeant7.personneLieeQualite).eq('71') and not Value('id').of(dirigeant7.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantPartant')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS7);
}

// Dirigeant 8

if (Value('id').of(dirigeant8.personnaliteJuridique).eq('personneMorale')
and not Value('id').of(dirigeant8.personneLieeQualite).eq('71') and not Value('id').of(dirigeant8.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantPartant')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS8);
}

// Dirigeant 9

if (Value('id').of(dirigeant9.personnaliteJuridique).eq('personneMorale')
and not Value('id').of(dirigeant9.personneLieeQualite).eq('71') and not Value('id').of(dirigeant9.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantPartant')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS9);
}

// Dirigeant 10

if (Value('id').of(dirigeant10.personnaliteJuridique).eq('personneMorale')
and not Value('id').of(dirigeant10.personneLieeQualite).eq('71') and not Value('id').of(dirigeant10.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantPartant')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS10);
}

// Dirigeant 11

if (Value('id').of(dirigeant11.personnaliteJuridique).eq('personneMorale')
and not Value('id').of(dirigeant11.personneLieeQualite).eq('71') and not Value('id').of(dirigeant11.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantPartant')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS11);
}

// Dirigeant 12

if (Value('id').of(dirigeant12.personnaliteJuridique).eq('personneMorale')
and not Value('id').of(dirigeant12.personneLieeQualite).eq('71') and not Value('id').of(dirigeant12.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantPartant')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjExtraitImmatriculationRCS12);
}

// PJ Représentant Permanent 1

if (dirigeant1.civiliteRepresentant.personneLieePPNomNaissance != null
and not Value('id').of(dirigeant1.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')
and not Value('id').of(dirigeant1.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')){
if (Value('id').of(dirigeant1.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIRepresentantModif1);
} else {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant1);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIRepresentant1);
}
}

// PJ Représentant Permanent 2

if (dirigeant2.civiliteRepresentant.personneLieePPNomNaissance != null
and not Value('id').of(dirigeant2.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')
and not Value('id').of(dirigeant2.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')){
if (Value('id').of(dirigeant2.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIRepresentantModif2);
} else {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant2);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIRepresentant2);
}
}

// PJ Représentant Permanent 3

if (dirigeant3.civiliteRepresentant.personneLieePPNomNaissance != null
and not Value('id').of(dirigeant3.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')
and not Value('id').of(dirigeant3.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')){
if (Value('id').of(dirigeant3.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIRepresentantModif3);
} else {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant3);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIRepresentant3);
}
}

// PJ Représentant Permanent 4

if (dirigeant4.civiliteRepresentant.personneLieePPNomNaissance != null
and not Value('id').of(dirigeant4.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')
and not Value('id').of(dirigeant4.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')){
if (Value('id').of(dirigeant4.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIRepresentantModif4);
} else {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant4);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIRepresentant4);
}
}

// PJ Représentant Permanent 5

if (dirigeant5.civiliteRepresentant.personneLieePPNomNaissance != null
and not Value('id').of(dirigeant5.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')
and not Value('id').of(dirigeant5.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')){
if (Value('id').of(dirigeant5.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIRepresentantModif5);
} else {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant5);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIRepresentant5);
}
}

// PJ Représentant Permanent 6

if (dirigeant6.civiliteRepresentant.personneLieePPNomNaissance != null
and not Value('id').of(dirigeant6.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')
and not Value('id').of(dirigeant6.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')){
if (Value('id').of(dirigeant6.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIRepresentantModif6);
} else {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant6);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIRepresentant6);
}
}

// PJ Représentant Permanent 7

if (dirigeant7.civiliteRepresentant.personneLieePPNomNaissance != null
and not Value('id').of(dirigeant7.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')
and not Value('id').of(dirigeant7.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')){
if (Value('id').of(dirigeant7.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIRepresentantModif7);
} else {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant7);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIRepresentant7);
}
}

// PJ Représentant Permanent 8

if (dirigeant8.civiliteRepresentant.personneLieePPNomNaissance != null
and not Value('id').of(dirigeant8.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')
and not Value('id').of(dirigeant8.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')){
if (Value('id').of(dirigeant8.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIRepresentantModif8);
} else {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant8);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIRepresentant8);
}
}

// PJ Représentant Permanent 9

if (dirigeant9.civiliteRepresentant.personneLieePPNomNaissance != null
and not Value('id').of(dirigeant9.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')
and not Value('id').of(dirigeant9.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')){
if (Value('id').of(dirigeant9.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIRepresentantModif9);
} else {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant9);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIRepresentant9);
}
}

// PJ Représentant Permanent 10

if (dirigeant10.civiliteRepresentant.personneLieePPNomNaissance != null
and not Value('id').of(dirigeant10.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')
and not Value('id').of(dirigeant10.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')){
if (Value('id').of(dirigeant10.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIRepresentantModif10);
} else {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant10);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIRepresentant10);
}
}

// PJ Représentant Permanent 11

if (dirigeant11.civiliteRepresentant.personneLieePPNomNaissance != null
and not Value('id').of(dirigeant11.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')
and not Value('id').of(dirigeant11.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')){
if (Value('id').of(dirigeant11.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIRepresentantModif11);
} else {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant11);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIRepresentant11);
}
}

// PJ Représentant Permanent 12

if (dirigeant12.civiliteRepresentant.personneLieePPNomNaissance != null
and not Value('id').of(dirigeant12.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')
and not Value('id').of(dirigeant12.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')){
if (Value('id').of(dirigeant12.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIRepresentantModif12);
} else {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCRepresentant12);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjCNIRepresentant12);
}
}

// Pj CAC Titulaire 

if((Value('id').of(dirigeant1.personneLieeQualite).eq('71')
and not Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant2.personneLieeQualite).eq('71')
and not Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant3.personneLieeQualite).eq('71')
and not Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant4.personneLieeQualite).eq('71')
and not Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant5.personneLieeQualite).eq('71')
and not Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant6.personneLieeQualite).eq('71')
and not Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant7.personneLieeQualite).eq('71')
and not Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant8.personneLieeQualite).eq('71')
and not Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant9.personneLieeQualite).eq('71')
and not Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant10.personneLieeQualite).eq('71')
and not Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant11.personneLieeQualite).eq('71')
and not Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant12.personneLieeQualite).eq('71')
and not Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantPartant'))) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjJustificatifInscriptionCACTitulaire);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjLettreAcceptationCACTitulaire);
}

// Pj CAC Suppléant

if((Value('id').of(dirigeant1.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant2.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant3.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant4.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant5.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant6.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant7.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant8.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant9.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant10.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant11.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant12.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantPartant'))) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjJustificatifInscriptionCACSuppleant);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjLettreAcceptationCACSuppleant);
}

// PJ JQPA

var pj = $M2.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification1.jqpaSituation ;
if(Value('id').of(pj).eq('jqpaSituationDiplome')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDiplomes1);
}

if(Value('id').of(pj).eq('jqpaSituationExperience')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles1);
}

var pj=$M2.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification2.jqpaSituation ;
if(Value('id').of(pj).eq('jqpaSituationDiplome')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDiplomes2);
}

if(Value('id').of(pj).eq('jqpaSituationExperience')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles2);
}

var pj=$M2.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification3.jqpaSituation ;
if(Value('id').of(pj).eq('jqpaSituationDiplome')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDiplomes3);
}

if(Value('id').of(pj).eq('jqpaSituationExperience')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles3);
}

// PJ MBE

if (signataire.nombreMBE != null) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.formulaireBE1);
}

if (Value('id').of(signataire.nombreMBE).eq('deux') or Value('id').of(signataire.nombreMBE).eq('trois') or Value('id').of(signataire.nombreMBE).eq('quatre') or Value('id').of(signataire.nombreMBE).eq('cinq')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.formulaireBE2);
}

if (Value('id').of(signataire.nombreMBE).eq('trois') or Value('id').of(signataire.nombreMBE).eq('quatre') or Value('id').of(signataire.nombreMBE).eq('cinq')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.formulaireBE3);
}

if (Value('id').of(signataire.nombreMBE).eq('quatre') or Value('id').of(signataire.nombreMBE).eq('cinq')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.formulaireBE4);
}

if (Value('id').of(signataire.nombreMBE).eq('cinq')) {
pushPjPreview($attachmentPreprocess.attachmentPreprocess.formulaireBE5);
}

// PJ Activité réglementée

if(Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('10M')
or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')
or Value('id').of(objet.objetModification).contains('dirigeant')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDActiviteReglementee);
}

/*
/*
 * Enregistrement du fichier (en mémoire)
 */

if (Value('id').of(objet.objetModification).contains('situationPM') or Value('id').of(objet.objetModification).contains('etablissement')) {
var finalDoc = cerfaDoc1.save('M2_Modification.pdf');
} else if (((Value('id').of(identite.entrepriseFormeJuridique).eq('5499') or Value('id').of(identite.entrepriseFormeJuridique).eq('5410')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5415')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5422')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5426')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5430')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5431')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5432')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5442')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5443')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5451')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5453')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5454')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5455')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5458')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5459')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5460')	or Value('id').of(identite.entrepriseFormeJuridique).eq('5485'))
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M'))
or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')
and (Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5499') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5410')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5415')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5422')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5426')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5430')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5431')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5432')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5442')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5443')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5451')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5453')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5454')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5455')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5458')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5459')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5460')	or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5485')))) { 
var finalDoc = m3Doc1.save('M2_Modification.pdf');
} else { var finalDoc = m3BDoc1.save('M2_Modification.pdf');
}

var data = [ spec.createData({
    id : 'record',
    label : 'Déclaration de modification d\'une personne morale',
    description : 'Voici le formulaire obtenu à partir des données saisies.',
    type : 'FileReadOnly',
    value : [ finalDoc ]
}),  spec.createData({
    id : 'attachments',
    label : 'Pièces jointes',
    description : 'Pièces jointes ajoutées au formulaire.',
    type : 'FileReadOnly',
    value : pjUser
}) ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Déclaration de modification d\'une personne morale',
    groups : groups
});
