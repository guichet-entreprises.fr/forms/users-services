//check inout information

var docs = $review.generated.record;
_log.info("Documents are {}", docs);

var files = [];
for (var i = 0; i < docs.length; ++i) {
    files.push({
        'document': docs[i],
        'zoneId': nash.instance.load('/3-review/zoneId.xml').get('context.signature.zoneId').toString()
    });
}

var userResponse = nash.service.request('${account.ge.baseUrl.read}/private/users/{id}', nash.record.description().author) //
.accept('text') //
.get();

var user = userResponse.asObject();
	
var nfo = nash.hangout.stamp({
    'files' : files,
    'civility' : user.civility,
    'lastName' : user.lastName,
    'firstName' : user.firstName,
    'email' : user.email,
    'phone' : user.phone.replace('+', '00')
});

return nfo;
