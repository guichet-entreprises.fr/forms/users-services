var objet = $M2.cadreModificationSocieteGroup.cadre1ObjetModification;
var identite = $M2.cadreRappelIdentificationGroup.cadre1RappelIdentification;
var dissolution = $M2.modifDissolutionGroup.modifDissolution;
var modifDeno = $M2.cadreModificationPMGroup.cadre2ModificationPM.modificationDeno;
var modifFormeJuridique = $M2.cadreModificationPMGroup.cadre2ModificationPM.modifFormeJuridique;
var modifCapital = $M2.cadreModificationPMGroup.cadre2ModificationPM.modifCapital;
var modifDuree = $M2.cadreModificationPMGroup.cadre2ModificationPM.modificationDuree;
var modifEss = $M2.cadreModificationPMGroup.cadre2ModificationPM.modifEss;
var modifSocieteMission = $M2.cadreModificationPMGroup.cadre2ModificationPM.modifSocieteMission;
var modifContratAppui = $M2.cadreModificationPMGroup.cadre2ModificationPM.modifContratAppui;
var modifRenouvellementMaintienRCS = $M2.cadreModificationPMGroup.cadre2ModificationPM.modifRenouvellementMaintienRCS;
var siege = $M2.cadreRappelIdentificationGroup.cadre1RappelIdentification.adresseSiege;
var fusion = $M2.cadreModificationFusionGroup.cadreModificationFusion;
var fermeture = $M2.cadreEtablissementFermetureGroup.cadreEtablissementFermeture;
var fondsDonne = $M2.cadreFondsDonneLocationGeranceGroup.cadreFondsDonneLocationGerance;
var ouverture = $M2.cadreEtablissementOuvertureGroup.cadreEtablissementOuverture;
var activiteInfo = $M2.cadreEtablissementActiviteGroup.cadre7EtablissementActivite;
var activite = $M2.cadreEtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite;
var origine = $M2.cadre4OrigineFondsGroup.cadre4OrigineFonds;
var dirigeantSARL1 = $M2.dirigeant1Sarl.cadreIdentiteDirigeant;
var dirigeantSARL2 = $M2.dirigeant2Sarl.cadreIdentiteDirigeant;
var dirigeantSARL3 = $M2.dirigeant3Sarl.cadreIdentiteDirigeant;
var dirigeantSARL4 = $M2.dirigeant4Sarl.cadreIdentiteDirigeant;
var dirigeantSARL5 = $M2.dirigeant5Sarl.cadreIdentiteDirigeant;
var conjoint1 = $M2.dirigeant1Sarl.cadreIdentiteDirigeant.cadre2InfosConjoint;
var conjoint2 = $M2.dirigeant2Sarl.cadreIdentiteDirigeant.cadre2InfosConjoint;
var conjoint3 = $M2.dirigeant3Sarl.cadreIdentiteDirigeant.cadre2InfosConjoint;
var conjoint4 = $M2.dirigeant4Sarl.cadreIdentiteDirigeant.cadre2InfosConjoint;
var conjoint5 = $M2.dirigeant5Sarl.cadreIdentiteDirigeant.cadre2InfosConjoint;
var dirigeant1 = $M2.dirigeant1.cadreIdentiteDirigeant;
var dirigeant2 = $M2.dirigeant2.cadreIdentiteDirigeant;
var dirigeant3 = $M2.dirigeant3.cadreIdentiteDirigeant;
var dirigeant4 = $M2.dirigeant4.cadreIdentiteDirigeant;
var dirigeant5 = $M2.dirigeant5.cadreIdentiteDirigeant;
var dirigeant6 = $M2.dirigeant6.cadreIdentiteDirigeant;
var dirigeant7 = $M2.dirigeant7.cadreIdentiteDirigeant;
var dirigeant8 = $M2.dirigeant8.cadreIdentiteDirigeant;
var dirigeant9 = $M2.dirigeant9.cadreIdentiteDirigeant;
var dirigeant10 = $M2.dirigeant10.cadreIdentiteDirigeant;
var dirigeant11 = $M2.dirigeant11.cadreIdentiteDirigeant;
var dirigeant12 = $M2.dirigeant12.cadreIdentiteDirigeant;
var socialDirigeant1 = $M2.cadreDeclarationSociale1.cadre7DeclarationSociale;
var socialDirigeant2 = $M2.cadreDeclarationSociale2.cadre7DeclarationSociale;
var socialDirigeant3 = $M2.cadreDeclarationSociale3.cadre7DeclarationSociale;
var socialDirigeant4 = $M2.cadreDeclarationSociale4.cadre7DeclarationSociale;
var socialDirigeant5 = $M2.cadreDeclarationSociale5.cadre7DeclarationSociale;
var socialDirigeant6 = $M2.cadreDeclarationSociale6.cadre7DeclarationSociale;
var socialDirigeant7 = $M2.cadreDeclarationSociale7.cadre7DeclarationSociale;
var socialDirigeant8 = $M2.cadreDeclarationSociale8.cadre7DeclarationSociale;
var socialDirigeant9 = $M2.cadreDeclarationSociale9.cadre7DeclarationSociale;
var socialDirigeant10 = $M2.cadreDeclarationSociale10.cadre7DeclarationSociale;
var socialDirigeant11 = $M2.cadreDeclarationSociale11.cadre7DeclarationSociale;
var socialDirigeant12 = $M2.cadreDeclarationSociale12.cadre7DeclarationSociale;
var socialAssocie1 = $M2.cadreDeclarationSocialeConjointAssocie1.cadre7DeclarationSociale;
var socialAssocie2 = $M2.cadreDeclarationSocialeConjointAssocie2.cadre7DeclarationSociale;
var socialAssocie3 = $M2.cadreDeclarationSocialeConjointAssocie3.cadre7DeclarationSociale;
var socialAssocie4 = $M2.cadreDeclarationSocialeConjointAssocie4.cadre7DeclarationSociale;
var socialAssocie5 = $M2.cadreDeclarationSocialeConjointAssocie5.cadre7DeclarationSociale;
var correspondance = $M2.cadre8RensCompGroup.cadre8RensComp;
var signataire = $M2.cadre9SignatureGroup.cadre9Signature;
var jqpa1 = $M2.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification1;																					
var jqpa2 = $M2.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification2;																					
var jqpa3 = $M2.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification3;																					

// PJ Signataire

var userSignataire = signataire.adresseMandataire;
attachment('pjIDMandataireSignataire', 'pjIDMandataireSignataire', {label: userSignataire.nomPrenomDenominationMandataire, mandatory:"true"});

if(Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
    attachment('pjPouvoir', 'pjPouvoir', { mandatory:"true"});
}

// PJ société

if(Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('10M') 
or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') 	
or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('capital')
and Value('id').of(modifCapital.modifCapitalType).contains('15M'))
or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('fusion')
or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('18M')
and (Value('id').of(modifEss.modifEssAdhesionSortie).eq('modifEssAdhesion') or modifEss.modifStatutESS))
or modifSocieteMission.modifStatutSocieteMission
or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('16M')
or activiteInfo.cadreEtablissementIdentification.modifNomCommercialStatut
or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
and (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege')
or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal')))
or activite.modifObjetSocial) {
	attachment('pjStatuts', 'pjStatuts', {mandatory:"true"});
}

if(Value('id').of(objet.objetModification).contains('dirigeant')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('10M') 
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') 	
and not Value('id').of(modifCapital.modifCapitalType).contains('15M')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('fusion')
and not Value('id').of(modifEss.modifEssAdhesionSortie).eq('modifEssAdhesion') 
and not modifEss.modifStatutESS
and not modifSocieteMission.modifStatutSocieteMission
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('16M')
and not activite.modifObjetSocial and not activiteInfo.cadreEtablissementIdentification.modifNomCommercialStatut) {
	attachment('pjStatutsBis', 'pjStatutsBis', {mandatory:"false"});
}
	
if(Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('10M')
or Value('id').of(objet.objetModification).contains('dirigeant')
or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('18M')
and (Value('id').of(modifEss.modifEssAdhesionSortie).eq('modifEssAdhesion') or modifEss.modifStatutESS))
or activiteInfo.cadreEtablissementIdentification.modifNomCommercialStatut
or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('16M')
or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
and (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege')
or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal')))
or activite.modifObjetSocial) {
	attachment('pjPV', 'pjPV', { mandatory:"true"});
}

if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('capital')
and Value('id').of(modifCapital.modifCapitalType).contains('25M')) {
	attachment('pjPVContinuation', 'pjPVContinuation', { mandatory:"true"});
}

if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('capital')
and Value('id').of(modifCapital.modifCapitalType).contains('26M')) {
	attachment('pjPVReconstitution', 'pjPVReconstitution', { mandatory:"true"});
}

if((Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('capital')
and Value('id').of(modifCapital.modifCapitalType).contains('15M'))
or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('fusion')) {
	attachment('pjPVEnregistre', 'pjPVEnregistre', { mandatory:"true"});
}

if(Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('fusion')) {
	attachment('pjRapportFusion', 'pjRapportFusion', { mandatory:"true"});
}

if(Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M')) {
	attachment('pjPVEnregistreDisso', 'pjPVEnregistreDisso', { mandatory:"true"});
}

if(Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')) {
	attachment('pjPVEnregistreTransfo', 'pjPVEnregistreTransfo', { mandatory:"true"});
}

if((Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('10M')
or Value('id').of(objet.objetModification).contains('dirigeant')
or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')
or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('capital')
and (Value('id').of(modifCapital.modifCapitalType).contains('15M')
or Value('id').of(modifCapital.modifCapitalType).contains('25M')))
or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('fusion')
or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') 
or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('16M')
and Value('id').of(modifDuree.modifDureeExerciceSocial).contains('modifDuree'))
or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
and (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege')
or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal'))
and objet.cadreObjetModificationEtablissement.transfertDepartement)
or activite.modifObjetSocial)
and not (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
and (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege')
or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal'))
and not objet.cadreObjetModificationEtablissement.transfertDepartement)) {
	attachment('pjJournalSociete', 'pjJournalSociete', {mandatory:"true"});
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
and (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege')
or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal'))
and not objet.cadreObjetModificationEtablissement.transfertDepartement) {
	attachment('pjJournalSocieteNewSiege', 'pjJournalSocieteNewSiege', {mandatory:"true"});
	attachment('pjJournalSocieteOldSiege', 'pjJournalSocieteOldSiege', {mandatory:"true"});
	attachment('pjListeSiege', 'pjListeSiege', {mandatory:"true"});
}

if(Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('capital')
and Value('id').of(modifCapital.modifCapitalType).contains('15M')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('fusion')) {
	attachment('pjRapportCAA', 'pjRapportCAA', { mandatory:"false"});
}

if (ouverture.ouvertureDepartement
and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')
and (Value('id').of(identite.entrepriseFormeJuridique).eq('3120')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5800')
or Value('id').of(identite.entrepriseFormeJuridique).eq('3110'))) {
attachment('pjStatutsEtr', 'pjStatutsEtr', { mandatory:"true"});
}

if (Value('id').of(objet.objetModification).contains('dirigeant')
and (Value('id').of(identite.entrepriseFormeJuridique).eq('5202') or Value('id').of(identite.entrepriseFormeJuridique).eq('5203')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5710') or Value('id').of(identite.entrepriseFormeJuridique).eq('5720')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5785') or Value('id').of(identite.entrepriseFormeJuridique).eq('5599')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5505') or Value('id').of(identite.entrepriseFormeJuridique).eq('5510')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5515') or Value('id').of(identite.entrepriseFormeJuridique).eq('5520')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5522') or Value('id').of(identite.entrepriseFormeJuridique).eq('5525')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5530') or Value('id').of(identite.entrepriseFormeJuridique).eq('5531')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5532') or Value('id').of(identite.entrepriseFormeJuridique).eq('5542')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5543') or Value('id').of(identite.entrepriseFormeJuridique).eq('5546')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5547') or Value('id').of(identite.entrepriseFormeJuridique).eq('5548')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5551') or Value('id').of(identite.entrepriseFormeJuridique).eq('5552')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5553') or Value('id').of(identite.entrepriseFormeJuridique).eq('5554')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5555') or Value('id').of(identite.entrepriseFormeJuridique).eq('5558')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5559') or Value('id').of(identite.entrepriseFormeJuridique).eq('5560')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5699') or Value('id').of(identite.entrepriseFormeJuridique).eq('5605')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5610') or Value('id').of(identite.entrepriseFormeJuridique).eq('5615')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5620') or Value('id').of(identite.entrepriseFormeJuridique).eq('5622')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5625') or Value('id').of(identite.entrepriseFormeJuridique).eq('5630')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5631') or Value('id').of(identite.entrepriseFormeJuridique).eq('5632')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5642') or Value('id').of(identite.entrepriseFormeJuridique).eq('5643')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5646') or Value('id').of(identite.entrepriseFormeJuridique).eq('5647')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5648') or Value('id').of(identite.entrepriseFormeJuridique).eq('5651')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5652') or Value('id').of(identite.entrepriseFormeJuridique).eq('5653')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5654') or Value('id').of(identite.entrepriseFormeJuridique).eq('5655')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5658') or Value('id').of(identite.entrepriseFormeJuridique).eq('5659')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5660') or Value('id').of(identite.entrepriseFormeJuridique).eq('5585')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5685') or Value('id').of(identite.entrepriseFormeJuridique).eq('5306')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5307') or Value('id').of(identite.entrepriseFormeJuridique).eq('5308')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5309') or Value('id').of(identite.entrepriseFormeJuridique).eq('5385'))
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')) {
attachment('pjDecisionJudiciaire', 'pjDecisionJudiciaire', { mandatory:"false"});
}

if ((Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5710') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5720')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5785') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5599')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5505') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5510')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5515') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5520')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5522') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5525')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5530') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5531')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5532') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5542')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5543') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5546')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5547') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5548')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5551') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5552')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5553') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5554')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5555') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5558')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5559') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5560')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5699') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5605')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5610') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5615')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5620') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5622')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5625') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5630')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5631') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5632')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5642') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5643')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5646') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5647')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5648') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5651')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5652') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5653')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5654') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5655')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5658') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5659')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5660') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5585')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5685') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5308')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5309') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5385'))
and Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')) {
attachment('pjRapportCATransfo', 'pjRapportCATransfo', { mandatory:"true"});
}

// PJ Liquidateur si disso

var userLiquidateur;
if (dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPNomUsageLiquidateur != null) {
    var userLiquidateur = dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPNomUsageLiquidateur + '  ' + dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPPrenomLiquidateur[0] ;
} else {
    var userLiquidateur = dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPNomNaissanceLiquidateur + '  '+ dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPPrenomLiquidateur[0] ;
}

if(Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M')
	and not dissolution.cadreIdentiteLiquidateur.liquidateurDejaConnu
	and Value('id').of(dissolution.cadreIdentiteLiquidateur.liquidateurPersonnaliteJuridique).eq('personnePhysique')) {
attachment('pjDNCLiquidateur', 'pjDNCLiquidateur', {label: userLiquidateur, mandatory:"true"});
attachment('pjIDLiquidateur', 'pjIDLiquidateur', {label: userLiquidateur, mandatory:"true"});
}

var userLiquidateurPM = dissolution.cadreIdentiteLiquidateur.civilitePersonneMorale.personneLieePMDenomination;

if(Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M')
and Value('id').of(dissolution.cadreIdentiteLiquidateur.liquidateurPersonnaliteJuridique).eq('personneMorale')) {
attachment('pjExtraitImmatLiquidateur', 'pjExtraitImmatLiquidateur', {label: userLiquidateurPM, mandatory:"true"});
}

// Pas de PJ pour le 51M( reprise) 23M / 40M / 29M / 37M / 53M / 62M /67M / 80M /82M/ 84M / 81M / 68M

// PJ Etablissement

// Création

// Si  51M (prise), 52M, 54M, 11M

if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
and ((objet.cadreObjetModificationEtablissement.lieuActiviteSiege
and Value('id').of(objet.cadreObjetModificationEtablissement.priseRepriseActivite).eq('prise'))
or not objet.cadreObjetModificationEtablissement.lieuActiviteSiege))
or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
and (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege')
or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal')
or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
and Value('id').of(ouverture.etablissementDejaConnu).eq('non'))) 
and (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation')
or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsCreation')
or Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCocheAutre')
or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsCocheAutre')
or origine.precedentExploitant.cessionFonds or origine.precedentExploitant.fondsArtisanal)) {

	if (origine.creationDomiciliation) {
	attachment('pjContratDomiciliation', 'pjContratDomiciliation', {mandatory:"true"});
	} else if (not origine.creationDomiciliation or origine.precedentExploitant.cessionFonds or origine.precedentExploitant.fondsArtisanal) {
	attachment('pjEtablissementCreation', 'pjEtablissementCreation', {mandatory:"true"});
	}
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
and (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege')
or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal')
or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
and Value('id').of(ouverture.etablissementDejaConnu).eq('oui')) {
	attachment('pjEtablissementCreationBis', 'pjEtablissementCreationBis', {mandatory:"false"});
}

// Achat / Apport

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
and ((objet.cadreObjetModificationEtablissement.lieuActiviteSiege
and Value('id').of(objet.cadreObjetModificationEtablissement.priseRepriseActivite).eq('prise'))
or not objet.cadreObjetModificationEtablissement.lieuActiviteSiege))
or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
and Value('id').of(ouverture.etablissementDejaConnu).eq('non')) 
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M')
or Value('id').of(activite.activiteEvenement).eq('61M')
or Value('id').of(activite.activiteEvenement).eq('61M62M')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63M')) {

if (((Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsAchat')
or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsAchat'))
and not origine.precedentExploitant.fondsArtisanal 
and not origine.precedentExploitant.cessionFonds)
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63M')) {
attachment('pjAchat', 'pjAchat', {mandatory:"true"});
attachment('pjAchatPublication', 'pjAchatPublication', {mandatory:"true"});
}

if ((Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsApport')
or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsApport'))
and not origine.precedentExploitant.fondsArtisanal 
and not origine.precedentExploitant.cessionFonds) {
attachment('pjApport', 'pjApport', {mandatory:"true"});
attachment('pjApportPublication', 'pjApportPublication', {mandatory:"true"});
}
}

// Location-gérance / Gérance-mandat

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
and ((objet.cadreObjetModificationEtablissement.lieuActiviteSiege
and Value('id').of(objet.cadreObjetModificationEtablissement.priseRepriseActivite).eq('prise'))
or not objet.cadreObjetModificationEtablissement.lieuActiviteSiege)) 
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M')
or Value('id').of(activite.activiteEvenement).eq('61M')
or Value('id').of(activite.activiteEvenement).eq('61M62M')
or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
and Value('id').of(ouverture.etablissementDejaConnu).eq('non'))) {

if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsLocationGerance')
	or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsLocationGerance')) {
attachment('pjLocationGerance', 'pjLocationGerance', {mandatory:"true"});
attachment('pjLocationGerancePublication', 'pjLocationGerancePublication', {mandatory:"true"});
}

if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsGeranceMandat')
	or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsGeranceMandat')) {
attachment('pjGeranceMandat', 'pjGeranceMandat', {mandatory:"true"});
attachment('pjGeranceMandatPublication', 'pjGeranceMandatPublication', {mandatory:"true"});
}
}

// Renouvellement du contrat de location-gérance ou de gérance-mandat

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64M')) {
attachment('pjLocationGeranceBis', 'pjLocationGeranceBis', {mandatory:"true"});
attachment('pjLocationGerancePublicationBis', 'pjLocationGerancePublicationBis', {mandatory:"true"});
}

// PJ Dirigeant

// Dirigeant PP

// Dirigeant 1

var userDeclarant1;
if (dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant1 = dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant1 = dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  ' + dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant1 = dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  ' + dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else if (dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null){
    var userDeclarant1 = dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  ' + dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else if (dirigeantSARL1.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant1 = dirigeantSARL1.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeantSARL1.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeantSARL1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant1 = dirigeantSARL1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  ' + dirigeantSARL1.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} 

if ((ouverture.ajoutFondePouvoir or dirigeantSARL1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null
or dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null or dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)
and not Value('id').of(dirigeant1.personneLieeQualite).eq('71') and not Value('id').of(dirigeant1.personneLieeQualite).eq('72')) {
if (Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantNouveau') or Value('id').of(dirigeantSARL1.personneLieeObjet).eq('dirigeantNouveau') or ouverture.ajoutFondePouvoir) {
	attachment('pjDNCDirigeant1', 'pjDNCDirigeant1', {label: userDeclarant1, mandatory:"true"});
	attachment('pjCNIDirigeant1', 'pjCNIDirigeant1', {label: userDeclarant1, mandatory:"true"});
}
if (Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantMaintenu') and dirigeant1.dirigeantMaintenuModif) or Value('id').of(dirigeantSARL1.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeantSARL1.personneLieeObjet).eq('dirigeantMaintenu') and dirigeantSARL1.dirigeantMaintenuModif)) {
	attachment('pjCNIDirigeantModif1', 'pjCNIDirigeantModif1', {label: userDeclarant1, mandatory:"false"});
}
if (Value('id').of(dirigeantSARL1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') 
or (Value('id').of(dirigeantSARL1.cadre2InfosConjoint.objetModifConjoint).eq('modifMentionNouveauConjoint') 
and Value('id').of(dirigeantSARL1.cadre2InfosConjoint.statutModifConjoint).eq('modifStatutCollaborateur'))) {
	attachment('pjIDConjoint1', 'pjIDConjoint1', {label: userDeclarant1, mandatory:"true"});
}

if (dirigeantSARL1.statutConjointCollaborateurGeranceNonMaj != null
or dirigeantSARL1.statutConjointCollaborateurGeranceMaj != null 
or Value('id').of(dirigeantSARL1.cadre2InfosConjoint.objetModifConjoint).eq('modifMentionNouveauConjoint')) {
	attachment('pjChoixStatutConjoint1', 'pjChoixStatutConjoint1', {label: userDeclarant1, mandatory:"true"});
}
}

// Dirigeant 2

var userDeclarant2;
if (dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant2 = dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant2 = dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  ' + dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant2 = dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  ' + dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else if (dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null){
    var userDeclarant2 = dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  ' + dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else if (dirigeantSARL2.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant2 = dirigeantSARL2.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeantSARL2.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeantSARL2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant2 = dirigeantSARL2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  ' + dirigeantSARL2.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} 

if ((dirigeantSARL2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null
or dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null 
or dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)
and not Value('id').of(dirigeant2.personneLieeQualite).eq('71') and not Value('id').of(dirigeant2.personneLieeQualite).eq('72')) {
if (Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantNouveau') or Value('id').of(dirigeantSARL2.personneLieeObjet).eq('dirigeantNouveau')) {
	attachment('pjDNCDirigeant2', 'pjDNCDirigeant2', {label: userDeclarant2, mandatory:"true"});
	attachment('pjCNIDirigeant2', 'pjCNIDirigeant2', {label: userDeclarant2, mandatory:"true"});
}
if (Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantMaintenu') and dirigeant2.dirigeantMaintenuModif) or Value('id').of(dirigeantSARL2.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeantSARL2.personneLieeObjet).eq('dirigeantMaintenu') and dirigeantSARL2.dirigeantMaintenuModif)) {
	attachment('pjCNIDirigeantModif2', 'pjCNIDirigeantModif2', {label: userDeclarant2, mandatory:"false"});
}
if (Value('id').of(dirigeantSARL2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') 
or (Value('id').of(dirigeantSARL2.cadre2InfosConjoint.objetModifConjoint).eq('modifMentionNouveauConjoint') 
and Value('id').of(dirigeantSARL2.cadre2InfosConjoint.statutModifConjoint).eq('modifStatutCollaborateur'))) {
	attachment('pjIDConjoint2', 'pjIDConjoint2', {label: userDeclarant2, mandatory:"true"});
}
if (dirigeantSARL2.statutConjointCollaborateurGeranceNonMaj != null
or dirigeantSARL2.statutConjointCollaborateurGeranceMaj != null 
or Value('id').of(dirigeantSARL2.cadre2InfosConjoint.objetModifConjoint).eq('modifMentionNouveauConjoint')) {
	attachment('pjChoixStatutConjoint2', 'pjChoixStatutConjoint2', {label: userDeclarant2, mandatory:"true"});
}
}

// Dirigeant 3

var userDeclarant3;
if (dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant3 = dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant3 = dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  ' + dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant3 = dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  ' + dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else if (dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null){
    var userDeclarant3 = dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  ' + dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else if (dirigeantSARL3.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant3 = dirigeantSARL3.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeantSARL3.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeantSARL3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant3 = dirigeantSARL3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  ' + dirigeantSARL3.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} 

if ((dirigeantSARL3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null
or dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null 
or dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)
and not Value('id').of(dirigeant3.personneLieeQualite).eq('71') and not Value('id').of(dirigeant3.personneLieeQualite).eq('72')) {
if (Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantNouveau') or Value('id').of(dirigeantSARL3.personneLieeObjet).eq('dirigeantNouveau')) {
	attachment('pjDNCDirigeant3', 'pjDNCDirigeant3', {label: userDeclarant3, mandatory:"true"});
	attachment('pjCNIDirigeant3', 'pjCNIDirigeant3', {label: userDeclarant3, mandatory:"true"});
}
if (Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantMaintenu') and dirigeant3.dirigeantMaintenuModif) or Value('id').of(dirigeantSARL3.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeantSARL3.personneLieeObjet).eq('dirigeantMaintenu') and dirigeantSARL3.dirigeantMaintenuModif)) {
	attachment('pjCNIDirigeantModif3', 'pjCNIDirigeantModif3', {label: userDeclarant3, mandatory:"false"});
}
if (Value('id').of(dirigeantSARL3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') 
or (Value('id').of(dirigeantSARL3.cadre2InfosConjoint.objetModifConjoint).eq('modifMentionNouveauConjoint') 
and Value('id').of(dirigeantSARL3.cadre2InfosConjoint.statutModifConjoint).eq('modifStatutCollaborateur'))) {
	attachment('pjIDConjoint3', 'pjIDConjoint3', {label: userDeclarant3, mandatory:"true"});
}

if (dirigeantSARL3.statutConjointCollaborateurGeranceNonMaj != null
or dirigeantSARL3.statutConjointCollaborateurGeranceMaj != null 
or Value('id').of(dirigeantSARL3.cadre2InfosConjoint.objetModifConjoint).eq('modifMentionNouveauConjoint')) {
	attachment('pjChoixStatutConjoint3', 'pjChoixStatutConjoint3', {label: userDeclarant3, mandatory:"true"});
}
}

// Dirigeant 4

var userDeclarant4;
if (dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant4 = dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant4 = dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  ' + dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant4 = dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  ' + dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else if (dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null){
    var userDeclarant4 = dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  ' + dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else if (dirigeantSARL4.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant4 = dirigeantSARL4.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeantSARL4.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeantSARL4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant4 = dirigeantSARL4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  ' + dirigeantSARL4.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} 

if ((dirigeantSARL4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null
or dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null 
or dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)
and not Value('id').of(dirigeant4.personneLieeQualite).eq('71') and not Value('id').of(dirigeant4.personneLieeQualite).eq('72')) {
if (Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantNouveau') or Value('id').of(dirigeantSARL4.personneLieeObjet).eq('dirigeantNouveau')) {
	attachment('pjDNCDirigeant4', 'pjDNCDirigeant4', {label: userDeclarant4, mandatory:"true"});
	attachment('pjCNIDirigeant4', 'pjCNIDirigeant4', {label: userDeclarant4, mandatory:"true"});
}
if (Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantMaintenu') and dirigeant4.dirigeantMaintenuModif) or Value('id').of(dirigeantSARL4.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeantSARL4.personneLieeObjet).eq('dirigeantMaintenu') and dirigeantSARL4.dirigeantMaintenuModif)) {
	attachment('pjCNIDirigeantModif4', 'pjCNIDirigeantModif4', {label: userDeclarant4, mandatory:"false"});
}
if (Value('id').of(dirigeantSARL4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') 
or (Value('id').of(dirigeantSARL4.cadre2InfosConjoint.objetModifConjoint).eq('modifMentionNouveauConjoint') 
and Value('id').of(dirigeantSARL4.cadre2InfosConjoint.statutModifConjoint).eq('modifStatutCollaborateur'))) {
	attachment('pjIDConjoint4', 'pjIDConjoint4', {label: userDeclarant3, mandatory:"true"});
}
if (dirigeantSARL4.statutConjointCollaborateurGeranceNonMaj != null
or dirigeantSARL4.statutConjointCollaborateurGeranceMaj != null 
or Value('id').of(dirigeantSARL4.cadre2InfosConjoint.objetModifConjoint).eq('modifMentionNouveauConjoint')) {
	attachment('pjChoixStatutConjoint4', 'pjChoixStatutConjoint4', {label: userDeclarant4, mandatory:"true"});
}
}

// Dirigeant 5

var userDeclarant5;
if (dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant5 = dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant5 = dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  ' + dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant5 = dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  ' + dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else if (dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null){
    var userDeclarant5 = dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  ' + dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else if (dirigeantSARL5.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant5 = dirigeantSARL5.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeantSARL5.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeantSARL5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant5 = dirigeantSARL5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  ' + dirigeantSARL5.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} 

if ((dirigeantSARL5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null
or dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null 
or dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)
and not Value('id').of(dirigeant5.personneLieeQualite).eq('71') and not Value('id').of(dirigeant5.personneLieeQualite).eq('72')) {
if (Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantNouveau') or Value('id').of(dirigeantSARL5.personneLieeObjet).eq('dirigeantNouveau')) {
	attachment('pjDNCDirigeant5', 'pjDNCDirigeant5', {label: userDeclarant5, mandatory:"true"});
	attachment('pjCNIDirigeant5', 'pjCNIDirigeant5', {label: userDeclarant5, mandatory:"true"});
}
if (Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantMaintenu') and dirigeant5.dirigeantMaintenuModif) or Value('id').of(dirigeantSARL5.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeantSARL5.personneLieeObjet).eq('dirigeantMaintenu') and dirigeantSARL5.dirigeantMaintenuModif)) {
	attachment('pjCNIDirigeantModif5', 'pjCNIDirigeantModif5', {label: userDeclarant5, mandatory:"false"});
}
if (Value('id').of(dirigeantSARL5.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') 
or (Value('id').of(dirigeantSARL5.cadre2InfosConjoint.objetModifConjoint).eq('modifMentionNouveauConjoint') 
and Value('id').of(dirigeantSARL5.cadre2InfosConjoint.statutModifConjoint).eq('modifStatutCollaborateur'))) {
	attachment('pjIDConjoint5', 'pjIDConjoint5', {label: userDeclarant3, mandatory:"true"});
}
if (dirigeantSARL5.statutConjointCollaborateurGeranceNonMaj != null
or dirigeantSARL5.statutConjointCollaborateurGeranceMaj != null 
or Value('id').of(dirigeantSARL5.cadre2InfosConjoint.objetModifConjoint).eq('modifMentionNouveauConjoint')) {
	attachment('pjChoixStatutConjoint5', 'pjChoixStatutConjoint5', {label: userDeclarant5, mandatory:"true"});
}
}

// Dirigeant 6

var userDeclarant6;
if (dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant6 = dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant6 = dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  ' + dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant6 = dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  ' + dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else if (dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null){
    var userDeclarant6 = dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  ' + dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if ((dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null 
or dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)
and not Value('id').of(dirigeant6.personneLieeQualite).eq('71') and not Value('id').of(dirigeant6.personneLieeQualite).eq('72')) {
if (Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantNouveau')) {
	attachment('pjDNCDirigeant6', 'pjDNCDirigeant6', {label: userDeclarant6, mandatory:"true"});
	attachment('pjCNIDirigeant6', 'pjCNIDirigeant6', {label: userDeclarant6, mandatory:"true"});
}
if (Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantMaintenu') and dirigeant6.dirigeantMaintenuModif)) {
	attachment('pjCNIDirigeantModif6', 'pjCNIDirigeantModif6', {label: userDeclarant6, mandatory:"false"});
}
}

// Dirigeant 7

var userDeclarant7;
if (dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant7 = dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant7 = dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  ' + dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant7 = dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  ' + dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else if (dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null){
    var userDeclarant7 = dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  ' + dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if ((dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null 
or dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)
and not Value('id').of(dirigeant7.personneLieeQualite).eq('71') and not Value('id').of(dirigeant7.personneLieeQualite).eq('72')) {
if (Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantNouveau')) {
	attachment('pjDNCDirigeant7', 'pjDNCDirigeant7', {label: userDeclarant7, mandatory:"true"});
	attachment('pjCNIDirigeant7', 'pjCNIDirigeant7', {label: userDeclarant7, mandatory:"true"});
}
if (Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantMaintenu') and dirigeant7.dirigeantMaintenuModif)) {
	attachment('pjCNIDirigeantModif7', 'pjCNIDirigeantModif7', {label: userDeclarant7, mandatory:"false"});
}
}

// Dirigeant 8

var userDeclarant8;
if (dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant8 = dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant8 = dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  ' + dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant8 = dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  ' + dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else if (dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null){
    var userDeclarant8 = dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  ' + dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if ((dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null 
or dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)
and not Value('id').of(dirigeant8.personneLieeQualite).eq('71') and not Value('id').of(dirigeant8.personneLieeQualite).eq('72')) {
if (Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantNouveau')) {
	attachment('pjDNCDirigeant8', 'pjDNCDirigeant8', {label: userDeclarant8, mandatory:"true"});
	attachment('pjCNIDirigeant8', 'pjCNIDirigeant8', {label: userDeclarant8, mandatory:"true"});
}
if (Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantMaintenu') and dirigeant8.dirigeantMaintenuModif)) {
	attachment('pjCNIDirigeantModif8', 'pjCNIDirigeantModif8', {label: userDeclarant8, mandatory:"false"});
}
}

// Dirigeant 9

var userDeclarant9;
if (dirigeant9.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant9 = dirigeant9.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant9 = dirigeant9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  ' + dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant9 = dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  ' + dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else if (dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null){
    var userDeclarant9 = dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  ' + dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if ((dirigeant9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null 
or dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)
and not Value('id').of(dirigeant9.personneLieeQualite).eq('71') and not Value('id').of(dirigeant9.personneLieeQualite).eq('72')) {
if (Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantNouveau')) {
	attachment('pjDNCDirigeant9', 'pjDNCDirigeant9', {label: userDeclarant9, mandatory:"true"});
	attachment('pjCNIDirigeant9', 'pjCNIDirigeant9', {label: userDeclarant9, mandatory:"true"});
}
if (Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantMaintenu') and dirigeant9.dirigeantMaintenuModif)) {
	attachment('pjCNIDirigeantModif9', 'pjCNIDirigeantModif9', {label: userDeclarant9, mandatory:"false"});
}
}

// Dirigeant 10

var userDeclarant10;
if (dirigeant10.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant10 = dirigeant10.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant10 = dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  ' + dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant10 = dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  ' + dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else if (dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null){
    var userDeclarant10 = dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  ' + dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if ((dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null 
or dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)
and not Value('id').of(dirigeant10.personneLieeQualite).eq('71') and not Value('id').of(dirigeant10.personneLieeQualite).eq('72')) {
if (Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantNouveau')) {
	attachment('pjDNCDirigeant10', 'pjDNCDirigeant10', {label: userDeclarant10, mandatory:"true"});
	attachment('pjCNIDirigeant10', 'pjCNIDirigeant10', {label: userDeclarant10, mandatory:"true"});
}
if (Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantMaintenu') and dirigeant10.dirigeantMaintenuModif)) {
	attachment('pjCNIDirigeantModif10', 'pjCNIDirigeantModif10', {label: userDeclarant10, mandatory:"false"});
}
}

// Dirigeant 11

var userDeclarant11;
if (dirigeant11.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant11 = dirigeant11.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant11 = dirigeant11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  ' + dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant11 = dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  ' + dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else if (dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null){
    var userDeclarant11 = dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  ' + dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if ((dirigeant11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null 
or dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)
and not Value('id').of(dirigeant11.personneLieeQualite).eq('71') and not Value('id').of(dirigeant11.personneLieeQualite).eq('72')) {
if (Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantNouveau')) {
	attachment('pjDNCDirigeant11', 'pjDNCDirigeant11', {label: userDeclarant11, mandatory:"true"});
	attachment('pjCNIDirigeant11', 'pjCNIDirigeant11', {label: userDeclarant11, mandatory:"true"});
}
if (Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantMaintenu') and dirigeant11.dirigeantMaintenuModif)) {
	attachment('pjCNIDirigeantModif11', 'pjCNIDirigeantModif11', {label: userDeclarant11, mandatory:"false"});
}
}

// Dirigeant 12

var userDeclarant12;
if (dirigeant12.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
    var userDeclarant12 = dirigeant12.civilitePersonnePhysique.personneLieePPNomUsageGerant + '  ' + dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null){
    var userDeclarant12 = dirigeant12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant + '  ' + dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant[0] ;
} else if (dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
    var userDeclarant12 = dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant + '  ' + dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} else if (dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null){
    var userDeclarant12 = dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant + '  ' + dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[0] ;
} 

if ((dirigeant12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null 
or dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)
and not Value('id').of(dirigeant12.personneLieeQualite).eq('71') and not Value('id').of(dirigeant12.personneLieeQualite).eq('72')) {
if (Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantNouveau')) {
	attachment('pjDNCDirigeant12', 'pjDNCDirigeant12', {label: userDeclarant12, mandatory:"true"});
	attachment('pjCNIDirigeant12', 'pjCNIDirigeant12', {label: userDeclarant12, mandatory:"true"});
}
if (Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantModif') or (Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantMaintenu') and dirigeant12.dirigeantMaintenuModif)) {
	attachment('pjCNIDirigeantModif12', 'pjCNIDirigeantModif12', {label: userDeclarant12, mandatory:"false"});
}
}

// Dirigeant PM

// Dirigeant 1

var userMorale1 = dirigeant1.civilitePersonneMorale.personneLieePMDenomination ;

if (Value('id').of(dirigeant1.personnaliteJuridique).eq('personneMorale')
and not Value('id').of(dirigeant1.personneLieeQualite).eq('71') and not Value('id').of(dirigeant1.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantPartant')) {
	attachment('pjExtraitImmatriculationRCS1', 'pjExtraitImmatriculationRCS1', {label: userMorale1, mandatory:"true"});
}

// Dirigeant 2

var userMorale2 = dirigeant2.civilitePersonneMorale.personneLieePMDenomination ;

if (Value('id').of(dirigeant2.personnaliteJuridique).eq('personneMorale')
and not Value('id').of(dirigeant2.personneLieeQualite).eq('71') and not Value('id').of(dirigeant2.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantPartant')) {
	attachment('pjExtraitImmatriculationRCS2', 'pjExtraitImmatriculationRCS2', {label: userMorale2, mandatory:"true"});
}

// Dirigeant 3

var userMorale3 = dirigeant3.civilitePersonneMorale.personneLieePMDenomination ;

if (Value('id').of(dirigeant3.personnaliteJuridique).eq('personneMorale')
and not Value('id').of(dirigeant3.personneLieeQualite).eq('71') and not Value('id').of(dirigeant3.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantPartant')) {
	attachment('pjExtraitImmatriculationRCS3', 'pjExtraitImmatriculationRCS3', {label: userMorale3, mandatory:"true"});
}

// Dirigeant 4

var userMorale4 = dirigeant4.civilitePersonneMorale.personneLieePMDenomination ;

if (Value('id').of(dirigeant4.personnaliteJuridique).eq('personneMorale')
and not Value('id').of(dirigeant4.personneLieeQualite).eq('71') and not Value('id').of(dirigeant4.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantPartant')) {
	attachment('pjExtraitImmatriculationRCS4', 'pjExtraitImmatriculationRCS4', {label: userMorale4, mandatory:"true"});
}

// Dirigeant 5

var userMorale5 = dirigeant5.civilitePersonneMorale.personneLieePMDenomination ;

if (Value('id').of(dirigeant5.personnaliteJuridique).eq('personneMorale')
and not Value('id').of(dirigeant5.personneLieeQualite).eq('71') and not Value('id').of(dirigeant5.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantPartant')) {
	attachment('pjExtraitImmatriculationRCS5', 'pjExtraitImmatriculationRCS5', {label: userMorale5, mandatory:"true"});
}

// Dirigeant 6

var userMorale6 = dirigeant6.civilitePersonneMorale.personneLieePMDenomination ;

if (Value('id').of(dirigeant6.personnaliteJuridique).eq('personneMorale')
and not Value('id').of(dirigeant6.personneLieeQualite).eq('71') and not Value('id').of(dirigeant6.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantPartant')) {
	attachment('pjExtraitImmatriculationRCS6', 'pjExtraitImmatriculationRCS6', {label: userMorale6, mandatory:"true"});
}

// Dirigeant 7

var userMorale7 = dirigeant7.civilitePersonneMorale.personneLieePMDenomination ;

if (Value('id').of(dirigeant7.personnaliteJuridique).eq('personneMorale')
and not Value('id').of(dirigeant7.personneLieeQualite).eq('71') and not Value('id').of(dirigeant7.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantPartant')) {
	attachment('pjExtraitImmatriculationRCS7', 'pjExtraitImmatriculationRCS7', {label: userMorale7, mandatory:"true"});
}

// Dirigeant 8

var userMorale8 = dirigeant8.civilitePersonneMorale.personneLieePMDenomination ;

if (Value('id').of(dirigeant8.personnaliteJuridique).eq('personneMorale')
and not Value('id').of(dirigeant8.personneLieeQualite).eq('71') and not Value('id').of(dirigeant8.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantPartant')) {
	attachment('pjExtraitImmatriculationRCS8', 'pjExtraitImmatriculationRCS8', {label: userMorale8, mandatory:"true"});
}

// Dirigeant 9

var userMorale9 = dirigeant9.civilitePersonneMorale.personneLieePMDenomination ;

if (Value('id').of(dirigeant9.personnaliteJuridique).eq('personneMorale')
and not Value('id').of(dirigeant9.personneLieeQualite).eq('71') and not Value('id').of(dirigeant9.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantPartant')) {
	attachment('pjExtraitImmatriculationRCS9', 'pjExtraitImmatriculationRCS9', {label: userMorale9, mandatory:"true"});
}

// Dirigeant 10

var userMorale10 = dirigeant10.civilitePersonneMorale.personneLieePMDenomination ;

if (Value('id').of(dirigeant10.personnaliteJuridique).eq('personneMorale')
and not Value('id').of(dirigeant10.personneLieeQualite).eq('71') and not Value('id').of(dirigeant10.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantPartant')) {
	attachment('pjExtraitImmatriculationRCS10', 'pjExtraitImmatriculationRCS10', {label: userMorale10, mandatory:"true"});
}

// Dirigeant 11

var userMorale11 = dirigeant11.civilitePersonneMorale.personneLieePMDenomination ;

if (Value('id').of(dirigeant11.personnaliteJuridique).eq('personneMorale')
and not Value('id').of(dirigeant11.personneLieeQualite).eq('71') and not Value('id').of(dirigeant11.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantPartant')) {
	attachment('pjExtraitImmatriculationRCS11', 'pjExtraitImmatriculationRCS11', {label: userMorale11, mandatory:"true"});
}

// Dirigeant 12

var userMorale12 = dirigeant12.civilitePersonneMorale.personneLieePMDenomination ;

if (Value('id').of(dirigeant12.personnaliteJuridique).eq('personneMorale')
and not Value('id').of(dirigeant12.personneLieeQualite).eq('71') and not Value('id').of(dirigeant12.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantPartant')) {
	attachment('pjExtraitImmatriculationRCS12', 'pjExtraitImmatriculationRCS12', {label: userMorale12, mandatory:"true"});
}

// PJ Représentant Permanent 1

var userRepresentant1;
if (dirigeant1.civiliteRepresentant.personneLieePPNomUsage != null) {
    var userRepresentant1 = dirigeant1.civiliteRepresentant.personneLieePPNomUsage + '  ' + dirigeant1.civiliteRepresentant.personneLieePPPrenom[0] ;
} else {
    var userRepresentant1 = dirigeant1.civiliteRepresentant.personneLieePPNomNaissance + '  ' + dirigeant1.civiliteRepresentant.personneLieePPPrenom[0] ;
}

if (dirigeant1.civiliteRepresentant.personneLieePPNomNaissance != null
and not Value('id').of(dirigeant1.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')
and not Value('id').of(dirigeant1.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')){
if (Value('id').of(dirigeant1.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
	attachment('pjCNIRepresentantModif1', 'pjCNIRepresentantModif1',{ label: userRepresentant1, mandatory:"true"}) ;
} else {
	attachment('pjDNCRepresentant1', 'pjDNCRepresentant1',{ label: userRepresentant1, mandatory:"true"}) ;
    attachment('pjCNIRepresentant1', 'pjCNIRepresentant1', { label: userRepresentant1, mandatory:"true"});
}
}

// PJ Représentant Permanent 2

var userRepresentant2;
if (dirigeant2.civiliteRepresentant.personneLieePPNomUsage != null) {
    var userRepresentant2 = dirigeant2.civiliteRepresentant.personneLieePPNomUsage + '  ' + dirigeant2.civiliteRepresentant.personneLieePPPrenom[0] ;
} else {
    var userRepresentant2 = dirigeant2.civiliteRepresentant.personneLieePPNomNaissance + '  ' + dirigeant2.civiliteRepresentant.personneLieePPPrenom[0] ;
}

if (dirigeant2.civiliteRepresentant.personneLieePPNomNaissance != null
and not Value('id').of(dirigeant2.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')
and not Value('id').of(dirigeant2.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')){
if (Value('id').of(dirigeant2.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
	attachment('pjCNIRepresentantModif2', 'pjCNIRepresentantModif2',{ label: userRepresentant2, mandatory:"true"}) ;
} else {
	attachment('pjDNCRepresentant2', 'pjDNCRepresentant2',{ label: userRepresentant2, mandatory:"true"}) ;
    attachment('pjCNIRepresentant2', 'pjCNIRepresentant2', { label: userRepresentant2, mandatory:"true"});
}
}

// PJ Représentant Permanent 3

var userRepresentant3;
if (dirigeant3.civiliteRepresentant.personneLieePPNomUsage != null) {
    var userRepresentant3 = dirigeant3.civiliteRepresentant.personneLieePPNomUsage + '  ' + dirigeant3.civiliteRepresentant.personneLieePPPrenom[0] ;
} else {
    var userRepresentant3 = dirigeant3.civiliteRepresentant.personneLieePPNomNaissance + '  ' + dirigeant3.civiliteRepresentant.personneLieePPPrenom[0] ;
}

if (dirigeant3.civiliteRepresentant.personneLieePPNomNaissance != null
and not Value('id').of(dirigeant3.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')
and not Value('id').of(dirigeant3.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')){
if (Value('id').of(dirigeant3.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
	attachment('pjCNIRepresentantModif3', 'pjCNIRepresentantModif3',{ label: userRepresentant3, mandatory:"true"}) ;
} else {
	attachment('pjDNCRepresentant3', 'pjDNCRepresentant3',{ label: userRepresentant3, mandatory:"true"}) ;
    attachment('pjCNIRepresentant3', 'pjCNIRepresentant3', { label: userRepresentant3, mandatory:"true"});
}
}

// PJ Représentant Permanent 4

var userRepresentant4;
if (dirigeant4.civiliteRepresentant.personneLieePPNomUsage != null) {
    var userRepresentant4 = dirigeant4.civiliteRepresentant.personneLieePPNomUsage + '  ' + dirigeant4.civiliteRepresentant.personneLieePPPrenom[0] ;
} else {
    var userRepresentant4 = dirigeant4.civiliteRepresentant.personneLieePPNomNaissance + '  ' + dirigeant4.civiliteRepresentant.personneLieePPPrenom[0] ;
}

if (dirigeant4.civiliteRepresentant.personneLieePPNomNaissance != null
and not Value('id').of(dirigeant4.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')
and not Value('id').of(dirigeant4.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')){
if (Value('id').of(dirigeant4.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
	attachment('pjCNIRepresentantModif4', 'pjCNIRepresentantModif4',{ label: userRepresentant4, mandatory:"true"}) ;
} else {
	attachment('pjDNCRepresentant4', 'pjDNCRepresentant4',{ label: userRepresentant4, mandatory:"true"}) ;
    attachment('pjCNIRepresentant4', 'pjCNIRepresentant4', { label: userRepresentant4, mandatory:"true"});
}
}

// PJ Représentant Permanent 5

var userRepresentant5;
if (dirigeant5.civiliteRepresentant.personneLieePPNomUsage != null) {
    var userRepresentant5 = dirigeant5.civiliteRepresentant.personneLieePPNomUsage + '  ' + dirigeant5.civiliteRepresentant.personneLieePPPrenom[0] ;
} else {
    var userRepresentant5 = dirigeant5.civiliteRepresentant.personneLieePPNomNaissance + '  ' + dirigeant5.civiliteRepresentant.personneLieePPPrenom[0] ;
}

if (dirigeant5.civiliteRepresentant.personneLieePPNomNaissance != null
and not Value('id').of(dirigeant5.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')
and not Value('id').of(dirigeant5.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')){
if (Value('id').of(dirigeant5.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
	attachment('pjCNIRepresentantModif5', 'pjCNIRepresentantModif5',{ label: userRepresentant5, mandatory:"true"}) ;
} else {
	attachment('pjDNCRepresentant5', 'pjDNCRepresentant5',{ label: userRepresentant5, mandatory:"true"}) ;
    attachment('pjCNIRepresentant5', 'pjCNIRepresentant5', { label: userRepresentant5, mandatory:"true"});
}
}

// PJ Représentant Permanent 6

var userRepresentant6;
if (dirigeant6.civiliteRepresentant.personneLieePPNomUsage != null) {
    var userRepresentant6 = dirigeant6.civiliteRepresentant.personneLieePPNomUsage + '  ' + dirigeant6.civiliteRepresentant.personneLieePPPrenom[0] ;
} else {
    var userRepresentant6 = dirigeant6.civiliteRepresentant.personneLieePPNomNaissance + '  ' + dirigeant6.civiliteRepresentant.personneLieePPPrenom[0] ;
}

if (dirigeant6.civiliteRepresentant.personneLieePPNomNaissance != null
and not Value('id').of(dirigeant6.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')
and not Value('id').of(dirigeant6.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')){
if (Value('id').of(dirigeant6.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
	attachment('pjCNIRepresentantModif6', 'pjCNIRepresentantModif6',{ label: userRepresentant6, mandatory:"true"}) ;
} else {
	attachment('pjDNCRepresentant6', 'pjDNCRepresentant6',{ label: userRepresentant6, mandatory:"true"}) ;
    attachment('pjCNIRepresentant6', 'pjCNIRepresentant6', { label: userRepresentant6, mandatory:"true"});
}
}

// PJ Représentant Permanent 7

var userRepresentant7;
if (dirigeant7.civiliteRepresentant.personneLieePPNomUsage != null) {
    var userRepresentant7 = dirigeant7.civiliteRepresentant.personneLieePPNomUsage + '  ' + dirigeant7.civiliteRepresentant.personneLieePPPrenom[0] ;
} else {
    var userRepresentant7 = dirigeant7.civiliteRepresentant.personneLieePPNomNaissance + '  ' + dirigeant7.civiliteRepresentant.personneLieePPPrenom[0] ;
}

if (dirigeant7.civiliteRepresentant.personneLieePPNomNaissance != null
and not Value('id').of(dirigeant7.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')
and not Value('id').of(dirigeant7.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')){
if (Value('id').of(dirigeant7.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
	attachment('pjCNIRepresentantModif7', 'pjCNIRepresentantModif7',{ label: userRepresentant7, mandatory:"true"}) ;
} else {
	attachment('pjDNCRepresentant7', 'pjDNCRepresentant7',{ label: userRepresentant7, mandatory:"true"}) ;
    attachment('pjCNIRepresentant7', 'pjCNIRepresentant7', { label: userRepresentant7, mandatory:"true"});
}
}

// PJ Représentant Permanent 8

var userRepresentant8;
if (dirigeant8.civiliteRepresentant.personneLieePPNomUsage != null) {
    var userRepresentant8 = dirigeant8.civiliteRepresentant.personneLieePPNomUsage + '  ' + dirigeant8.civiliteRepresentant.personneLieePPPrenom[0] ;
} else {
    var userRepresentant8 = dirigeant8.civiliteRepresentant.personneLieePPNomNaissance + '  ' + dirigeant8.civiliteRepresentant.personneLieePPPrenom[0] ;
}

if (dirigeant8.civiliteRepresentant.personneLieePPNomNaissance != null
and not Value('id').of(dirigeant8.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')
and not Value('id').of(dirigeant8.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')){
if (Value('id').of(dirigeant8.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
	attachment('pjCNIRepresentantModif8', 'pjCNIRepresentantModif8',{ label: userRepresentant8, mandatory:"true"}) ;
} else {
	attachment('pjDNCRepresentant8', 'pjDNCRepresentant8',{ label: userRepresentant8, mandatory:"true"}) ;
    attachment('pjCNIRepresentant8', 'pjCNIRepresentant8', { label: userRepresentant8, mandatory:"true"});
}
}

// PJ Représentant Permanent 9

var userRepresentant9;
if (dirigeant9.civiliteRepresentant.personneLieePPNomUsage != null) {
    var userRepresentant9 = dirigeant9.civiliteRepresentant.personneLieePPNomUsage + '  ' + dirigeant9.civiliteRepresentant.personneLieePPPrenom[0] ;
} else {
    var userRepresentant9 = dirigeant9.civiliteRepresentant.personneLieePPNomNaissance + '  ' + dirigeant9.civiliteRepresentant.personneLieePPPrenom[0] ;
}

if (dirigeant9.civiliteRepresentant.personneLieePPNomNaissance != null
and not Value('id').of(dirigeant9.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')
and not Value('id').of(dirigeant9.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')){
if (Value('id').of(dirigeant9.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
	attachment('pjCNIRepresentantModif9', 'pjCNIRepresentantModif9',{ label: userRepresentant9, mandatory:"true"}) ;
} else {
	attachment('pjDNCRepresentant9', 'pjDNCRepresentant9',{ label: userRepresentant9, mandatory:"true"}) ;
    attachment('pjCNIRepresentant9', 'pjCNIRepresentant9', { label: userRepresentant9, mandatory:"true"});
}
}

// PJ Représentant Permanent 10

var userRepresentant10;
if (dirigeant10.civiliteRepresentant.personneLieePPNomUsage != null) {
    var userRepresentant10 = dirigeant10.civiliteRepresentant.personneLieePPNomUsage + '  ' + dirigeant10.civiliteRepresentant.personneLieePPPrenom[0] ;
} else {
    var userRepresentant10 = dirigeant10.civiliteRepresentant.personneLieePPNomNaissance + '  ' + dirigeant10.civiliteRepresentant.personneLieePPPrenom[0] ;
}

if (dirigeant10.civiliteRepresentant.personneLieePPNomNaissance != null
and not Value('id').of(dirigeant10.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')
and not Value('id').of(dirigeant10.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')){
if (Value('id').of(dirigeant10.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
	attachment('pjCNIRepresentantModif10', 'pjCNIRepresentantModif10',{ label: userRepresentant10, mandatory:"true"}) ;
} else {
	attachment('pjDNCRepresentant10', 'pjDNCRepresentant10',{ label: userRepresentant10, mandatory:"true"}) ;
    attachment('pjCNIRepresentant10', 'pjCNIRepresentant10', { label: userRepresentant10, mandatory:"true"});
}
}

// PJ Représentant Permanent 11

var userRepresentant11;
if (dirigeant11.civiliteRepresentant.personneLieePPNomUsage != null) {
    var userRepresentant11 = dirigeant11.civiliteRepresentant.personneLieePPNomUsage + '  ' + dirigeant11.civiliteRepresentant.personneLieePPPrenom[0] ;
} else {
    var userRepresentant11 = dirigeant11.civiliteRepresentant.personneLieePPNomNaissance + '  ' + dirigeant11.civiliteRepresentant.personneLieePPPrenom[0] ;
}

if (dirigeant11.civiliteRepresentant.personneLieePPNomNaissance != null
and not Value('id').of(dirigeant11.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')
and not Value('id').of(dirigeant11.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')){
if (Value('id').of(dirigeant11.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
	attachment('pjCNIRepresentantModif11', 'pjCNIRepresentantModif11',{ label: userRepresentant11, mandatory:"true"}) ;
} else {
	attachment('pjDNCRepresentant11', 'pjDNCRepresentant11',{ label: userRepresentant11, mandatory:"true"}) ;
    attachment('pjCNIRepresentant11', 'pjCNIRepresentant11', { label: userRepresentant11, mandatory:"true"});
}
}

// PJ Représentant Permanent 12

var userRepresentant12;
if (dirigeant12.civiliteRepresentant.personneLieePPNomUsage != null) {
    var userRepresentant12 = dirigeant12.civiliteRepresentant.personneLieePPNomUsage + '  ' + dirigeant12.civiliteRepresentant.personneLieePPPrenom[0] ;
} else {
    var userRepresentant12 = dirigeant12.civiliteRepresentant.personneLieePPNomNaissance + '  ' + dirigeant12.civiliteRepresentant.personneLieePPPrenom[0] ;
}

if (dirigeant12.civiliteRepresentant.personneLieePPNomNaissance != null
and not Value('id').of(dirigeant12.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')
and not Value('id').of(dirigeant12.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')){
if (Value('id').of(dirigeant12.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
	attachment('pjCNIRepresentantModif12', 'pjCNIRepresentantModif12',{ label: userRepresentant12, mandatory:"true"}) ;
} else {
	attachment('pjDNCRepresentant12', 'pjDNCRepresentant12',{ label: userRepresentant12, mandatory:"true"}) ;
    attachment('pjCNIRepresentant12', 'pjCNIRepresentant12', { label: userRepresentant12, mandatory:"true"});
}
}

// Pj CAC Titulaire 

if((Value('id').of(dirigeant1.personneLieeQualite).eq('71')
and not Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant2.personneLieeQualite).eq('71')
and not Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant3.personneLieeQualite).eq('71')
and not Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant4.personneLieeQualite).eq('71')
and not Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant5.personneLieeQualite).eq('71')
and not Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant6.personneLieeQualite).eq('71')
and not Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant7.personneLieeQualite).eq('71')
and not Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant8.personneLieeQualite).eq('71')
and not Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant9.personneLieeQualite).eq('71')
and not Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant10.personneLieeQualite).eq('71')
and not Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant11.personneLieeQualite).eq('71')
and not Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant12.personneLieeQualite).eq('71')
and not Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantPartant'))) {
    attachment('pjJustificatifInscriptionCACTitulaire', 'pjJustificatifInscriptionCACTitulaire', { mandatory:"false"});
	attachment('pjLettreAcceptationCACTitulaire', 'pjLettreAcceptationCACTitulaire', { mandatory:"true"});
}

// Pj CAC Suppléant

if((Value('id').of(dirigeant1.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant2.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant3.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant4.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant5.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant6.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant7.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant8.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant9.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant10.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant11.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantPartant'))
or (Value('id').of(dirigeant12.personneLieeQualite).eq('72')
and not Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantPartant'))) {
   attachment('pjJustificatifInscriptionCACSuppleant', 'pjJustificatifInscriptionCACSuppleant', { mandatory:"false"});
   attachment('pjLettreAcceptationCACSuppleant', 'pjLettreAcceptationCACSuppleant', { mandatory:"true"});
}


// PJ JQPA

var userJQPA1 = $M2.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification1.jqpaActiviteJqpaPP ;
var pj = $M2.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification1.jqpaSituation ;
if(Value('id').of(pj).eq('jqpaSituationDiplome')) {
	attachment('pjDiplomes1', 'pjDiplomes1', {label: userJQPA1, mandatory:"true"});
}
var pj=$M2.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification1.jqpaSituation ;
if(Value('id').of(pj).eq('jqpaSituationExperience')) {
	attachment('pjPiecesUtiles1', 'pjPiecesUtiles1', {label: userJQPA1, mandatory:"true"});
}

var userJQPA2 = $M2.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification2.jqpaActiviteJqpaPP ;
var pj=$M2.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification2.jqpaSituation ;
if(Value('id').of(pj).eq('jqpaSituationDiplome')) {
	attachment('pjDiplomes2', 'pjDiplomes2', {label: userJQPA2, mandatory:"true"});
}
var pj=$M2.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification2.jqpaSituation ;
if(Value('id').of(pj).eq('jqpaSituationExperience')) {
	attachment('pjPiecesUtiles2', 'pjPiecesUtiles2', {label: userJQPA2, mandatory:"true"});
}

var userJQPA3 = $M2.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification3.jqpaActiviteJqpaPP ;
var pj=$M2.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification3.jqpaSituation ;
if(Value('id').of(pj).eq('jqpaSituationDiplome')) {
	attachment('pjDiplomes3', 'pjDiplomes3', {label: userJQPA3, mandatory:"true"});
}
var pj=$M2.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification3.jqpaSituation ;
if(Value('id').of(pj).eq('jqpaSituationExperience')) {
	attachment('pjPiecesUtiles3', 'pjPiecesUtiles3', {label: userJQPA3, mandatory:"true"});
}

// PJ MBE	
var signataire = $M2.cadre9SignatureGroup.cadre9Signature ;

if (signataire.nombreMBE != null) {
attachment('formulaireBE1', 'formulaireBE1', {mandatory:"true"});
}

if (Value('id').of(signataire.nombreMBE).eq('deux') or Value('id').of(signataire.nombreMBE).eq('trois') or Value('id').of(signataire.nombreMBE).eq('quatre') or Value('id').of(signataire.nombreMBE).eq('cinq')) {
attachment('formulaireBE2', 'formulaireBE2', {mandatory:"true"});
}

if (Value('id').of(signataire.nombreMBE).eq('trois') or Value('id').of(signataire.nombreMBE).eq('quatre') or Value('id').of(signataire.nombreMBE).eq('cinq')) {
attachment('formulaireBE3', 'formulaireBE3', {mandatory:"true"});
}

if (Value('id').of(signataire.nombreMBE).eq('quatre') or Value('id').of(signataire.nombreMBE).eq('cinq')) {
attachment('formulaireBE4', 'formulaireBE4', {mandatory:"true"});
}

if (Value('id').of(signataire.nombreMBE).eq('cinq')) {
attachment('formulaireBE5', 'formulaireBE5', {mandatory:"true"});
}

// PJ Activité réglementée

// Pas pour le 29M / 22M / 18M /40M / 15M / 25M / 26M / 23M / 16M / 37M
//Pour le 10M / 13M / 51M / 52M / 53M / 54M 
if(Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('10M')
or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')
or Value('id').of(objet.objetModification).contains('dirigeant')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')) {
	attachment('pjIDActiviteReglementee', 'pjIDActiviteReglementee', {mandatory:"false"});
}
		
