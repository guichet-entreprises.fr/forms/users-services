//init

referentials = {
		'C': {
			'cerfa' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PMGUN'
			},
			'xmltc' :{
				'typepj' : 'TXMLTC',
				'suffixe' : 'PMGUN'
			} ,
			'xmlregent' :{
				'typepj' : 'TFORMA',
				'suffixe' : 'PMGUN'
			} ,
			'others' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PMGUN'
			} ,
		},

		'G': {
			'cerfa' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PGUEN'
			},
			'xmltc' :{
				'typepj' : 'TXMLTC',
				'suffixe' : 'PGUEN'
			} ,
			'xmlregent' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PGUEN'
			} ,
			'others' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PGUEN'
			} ,
		},
		'X': {
			'cerfa' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PMGUN'
			},
			'xmltc' :{
				'typepj' : 'TXMLTC',
				'suffixe' : 'PMGUN'
			} ,
			'xmlregent' :{
				'typepj' : 'TFORMA',
				'suffixe' : 'PMGUN'
			} ,
			'others' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PMGUN'
			} ,
		},
		'U': {
			'cerfa' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PMGUN'
			},
			'xmltc' :{
				'typepj' : 'TXMLTC',
				'suffixe' : 'PMGUN'
			} ,
			'xmlregent' :{
				'typepj' : 'TFORMA',
				'suffixe' : 'PMGUN'
			} ,
			'others' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PMGUN'
			} ,
		},
		'M': {
			'cerfa' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PMGUN'
			},
			'xmltc' :{
				'typepj' : 'TXMLTC',
				'suffixe' : 'PMGUN'
			} ,
			'xmlregent' :{
				'typepj' : 'TFORMA',
				'suffixe' : 'PMGUN'
			} ,
			'others' :{
				'typepj' : 'TPIJTE',
				'suffixe' : 'PMGUN'
			} ,
		},

};


//--> get file name of PJ : 
function getFileName(codeEdi, numLiasse, numOfpj, extension, fileType) {
	// --->
	_log.info("getFileName()");
	_log.info("Params[{}] [{}] [{}] [{}] [{}]", codeEdi, numLiasse, numOfpj,extension, fileType);

	var indiceOfpj;
	if (numOfpj > 9) {
		indiceOfpj = "0" + numOfpj;
	} else {
		indiceOfpj = "00" + numOfpj;
	}
	var network = codeEdi.charAt(0);
	var typepj = referentials[network][fileType]['typepj'];
	var suffixe = referentials[network][fileType]['suffixe'];
	var dateTemp = new Date();
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0"+ (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var dateFormatted = year.toString() + month.toString() + day.toString();
	// formatage de l'heure de génération
	var timeFormatted = dateTemp.toTimeString().substr(0, 8).replace(new RegExp(':', 'g'), '');

	var numberOfRecipients;
	switch (network) {
	
	case "G":
		numberOfRecipients = "1000A1001";
		break;
		
	case "C":
		numberOfRecipients = "0016A1002";
		break;
		
	case "M":
		numberOfRecipients = "0016A1003";
		break;
	case "X":
		numberOfRecipients = "0016A1004";
		break;
		
	case "U":
		numberOfRecipients = "0016A1005";
		break;
	}

	var label = 'C'+numberOfRecipients +'L'+ numLiasse.substr(6) + 'D' + dateFormatted + 'H'+ timeFormatted + typepj + 'S' + indiceOfpj + suffixe + "."+ extension.toLowerCase();
	_log.info("File name: {} ",label);
	
	return label;
}

var date = new Date();
var dateHeureGeneration = date.toISOString().slice(0,19);
var referencePaiement = null;
 var dateHeurePaiement = null;
//Infos dossier
var numeroDossierUnique = nash.record.description().recordUid; 
var nomDossier = nash.record.description().title; 
var denomination = $M2.cadreRappelIdentificationGroup.cadre1RappelIdentification.entrepriseDenomination;
//Infos de correspondance 
//==========>
//Nom et Prénom de correspondance
var resNomCorrespondant = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C36');
var resultNomCorrespondant = JSON.parse(resNomCorrespondant);
var nomCorrespondant = resultNomCorrespondant['C36'];

//===> [Début] Partié générique pour récupérer le nom du signataire <=====//
//Information signataire paiement
var resNomSignataire = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.1');
var resultNomSignataire = JSON.parse(resNomSignataire);
var nomSignataire = resultNomSignataire['C40.1'];
//===> [Fin] Partié générique pour récupérer le nom du signataire <=====//

//===> [Début] Partié générique pour récupérer l'adresse du signataire/Mandataire <=====//
//Informations d'adresse signataire/Mandataire
var signataire = $M2.cadre9SignatureGroup.cadre9Signature;
	// Numéro de voie
	var resNumVoiePayeur = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.5');
	var resultNumVoiePayeur = JSON.parse(resNumVoiePayeur);
	var numeroDeVoiePayeur = resultNumVoiePayeur['C40.3.5'];
	//Indice de répétition
	var resindRepPayeur = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.6');
	var resultindRepPayeur = JSON.parse(resindRepPayeur);
	var indRepPayeur = resultindRepPayeur['C40.3.6'];
	// Type de voie
	var resTypeDeVoiePayeur = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.11');
	var resultTypeDeVoiePayeur = JSON.parse(resTypeDeVoiePayeur);
	var typeDeVoiePayeur = resultTypeDeVoiePayeur['C40.3.11'];
	// Libellé voie
	var resLibelleDeVoiePayeur = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.12');
	var resultLibelleDeVoiePayeur = JSON.parse(resLibelleDeVoiePayeur);
	var libelleDeVoiePayeur = resultLibelleDeVoiePayeur['C40.3.12'];
	// Libellé de la localité
	var resLocalitePayeur = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.13');
	var resultLocalitePayeur = JSON.parse(resLocalitePayeur);
	var localitePayeur = resultLocalitePayeur['C40.3.13'];
	// Code postal
	var resCodePostalPayeur = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.8');
	var resultCodePostalPayeur = JSON.parse(resCodePostalPayeur);
	var codePostalPayeur = resultCodePostalPayeur['C40.3.8'];
	//complément de localisation
	var complementDeLocalisationPayeur ;

	var resComplLocaPayeur = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.10');
	var resultComplLocaPayeur = JSON.parse(resComplLocaPayeur);
	var complLocaPayeur = resultComplLocaPayeur['C40.3.10'];
	if (complLocaPayeur){
		complementDeLocalisationPayeur = complLocaPayeur ;
	}
	var resDestSpePayeur = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.7');
	var resultDestSpePayeur = JSON.parse(resDestSpePayeur);
	var destSpePayeur = resultDestSpePayeur['C40.3.7'];
	if (destSpePayeur){
		if(complementDeLocalisationPayeur){
			complementDeLocalisationPayeur = complementDeLocalisationPayeur + '-' + destSpePayeur; 
		}else{
			complementDeLocalisationPayeur = destSpePayeur; 
		}
	}
		// Adresse Email
		var resAdresseEmailPayeur = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.3');
		var resultAdresseEmailPayeur = JSON.parse(resAdresseEmailPayeur);
		var adresseEmailPayeur = resultAdresseEmailPayeur['C39.3'];
		if (adresseEmailPayeur == null || adresseEmailPayeur == '') {
		adresseEmailPayeur= 'no.mail@adresse.fr';
		}
//===> [Fin] Partié générique pour récupérer l'adresse du signataire/Mandataire <=====//

//Information adresse correspondant								   
//Numéro de voie
var resNumVoie = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.5');
var resultNumVoie = JSON.parse(resNumVoie);
var numeroDeVoie = resultNumVoie['C37.5'];
//Indice de répétition
var resindRep = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6');
var resultindRep = JSON.parse(resindRep);
var indRep = resultindRep['C37.6'];
//Type de voie
var resTypeDeVoie = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11');
var resultTypeDeVoie = JSON.parse(resTypeDeVoie);
var typeDeVoie = resultTypeDeVoie['C37.11'];
//Libellé voie
var resLibelleDeVoie = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12');
var resultLibelleDeVoie = JSON.parse(resLibelleDeVoie);
var libelleDeVoie = resultLibelleDeVoie['C37.12'];
//Libellé de la localité
var resLocalite = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13');
var resultLocalite = JSON.parse(resLocalite);
var localite = resultLocalite['C37.13'];
//Code postal
var resCodePostal = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8');
var resultCodePostal = JSON.parse(resCodePostal);
var codePostal = resultCodePostal['C37.8'];
//complément de localisation
var complementDeLocalisation ;

var resComplLoca = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.10');
var resultComplLoca = JSON.parse(resComplLoca);
var complLoca = resultComplLoca['C37.10'];
if (complLoca){
	complementDeLocalisation = complLoca ;
}
var resDestSpe = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.7');
var resultDestSpe = JSON.parse(resDestSpe);
var destSpe = resultDestSpe['C37.7'];
if (destSpe){
	if(complementDeLocalisation){
		complementDeLocalisation = complementDeLocalisation + '-' + destSpe; 
	}else{
		complementDeLocalisation = destSpe; 
	}
}
//Adresse Email
var resAdresseEmail = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.3');
var resultAdresseEmail = JSON.parse(resAdresseEmail);
var adresseEmail = resultAdresseEmail['C39.3'];
if (adresseEmail == null || adresseEmail == '') {
	adresseEmail= 'no.mail@adresse.fr';
}

//<=========

//Variables that should be setted by the formality redactor
var authorityType = "CFE"; // (CFE, TDR)
var authorityId = _INPUT_.dataGroup.codeEDI; // (code EDI)
var authorityLabel = _INPUT_.dataGroup.partnerLabel; // (label partenaire)
var destFuncId = _INPUT_.dataGroup.destFuncId; // (code interne de l'autorité)

var authorityType2 = "TDR"; // (CFE, TDR)
var authorityId2 = _INPUT_.dataGroup.codeEDI2; // (code EDI)
var authorityLabel2 = _INPUT_.dataGroup.partnerLabel2; // (label partenaire)
var destFuncId2 = _INPUT_.dataGroup.destFuncId2; // (code interne de l'autorité)

var listBindAttachments = [] ;
var listBindAttachmentsFirstAuthority = [] ;
var listBindAttachmentsSecondAuthority = [] ;											
var attachementElement ;
//nom du cerfa rempli 
var cheminLiasseCfePdf = 	_INPUT_.dataGroup.emailPj ;
var liasseCfePdf = cheminLiasseCfePdf.split("/")[3];

_log.info("dateHeureGeneration is " + dateHeureGeneration);
_log.info("authorityId is  {}", authorityId);
_log.info("authorityLabel is  {}", authorityLabel);
_log.info("nomCorrespondant is  {}", nomCorrespondant);
_log.info("typeDeVoie is  {}", typeDeVoie);
_log.info("libelleDeVoie is  {}", libelleDeVoie);
_log.info("localite is  {}", localite);
_log.info("numeroDeVoie is  {}", numeroDeVoie);
_log.info("codePostal is  {}", codePostal);
_log.info("bureauDistributeur is  {}", localite);
_log.info("adresseEmail is  {}", adresseEmail);
var jsonRecord = nash.util.convertToJson(_record);
_log.info("record info in step 9 is {}",jsonRecord);

var attachementsList1 = _record.xmlTcGenerationConfirmation.pjGeneratedAutority1 ; //
var attachementsList2 = _record.xmlTcGenerationConfirmation.pjGeneratedAutority2 ; //
var liasseList = _record.xmlTcGenerationConfirmation.confirmationMessageOk ; //
_log.info("attachementsList1 in step 9 is {}",attachementsList1);
_log.info("attachementsList1 values are  {}",attachementsList1.values());
_log.info("typeof attachementsList1 in step 9 is {}",typeof attachementsList1);

_log.info("attachementsList2 in step 9 is {}",attachementsList2);
_log.info("attachementsList2 values are  {}",attachementsList2.values());
_log.info("typeof attachementsList2 in step 9 is {}",typeof attachementsList2);

_log.info("liasseList in step 9 is {}",liasseList);
_log.info("typeof liasseList in step 9 is {}",typeof liasseList);

var result = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02');
var parsedResult = JSON.parse(result);
var numeroDeLiasse = parsedResult['C02'];

//formattage des nom de fichier pour l'envoie au greffes
var d = new Date;
var month = ((d.getMonth() + 1) < 10) ? "0" + (d.getMonth() + 1) : (d.getMonth() + 1);
var day = (d.getDate() < 10) ? "0" + (d.getDate()) : d.getDate();
var year = d.getFullYear();
var dateFormatted = year.toString() + month.toString() + day.toString();
var timeFormatted = d.toTimeString().substr(0,8).replace(new RegExp(':', 'g'), '');

var xmlRegent1Formatted =  liasseList["XML_REGENT_CFE"];
var pdfCfe1Formatted = liasseList["CERFA_PDF_CFE"];

if (authorityId2) {
	var xmlRegent2Formatted = liasseList["XML_REGENT_TDR"];
	var pdfCfe2Formatted = liasseList["CERFA_PDF_TDR"];
}

_log.info("numeroDeLiasse is  {}",numeroDeLiasse);

//Relier le numéro de dossier interne au numéro de liasse dans tracker
nash.tracker.post(numeroDeLiasse,"votre dossier sera transféré à l'organisme compétent sous le numéro "+numeroDeLiasse);
//TODO : Attachment part to be discussed how to generate it and how it should be managed
var indiceXmlCfe;
var indiceXmlTdr;
var indicesPjCFE = [] ;
var indicesPjTDR = [] ;

var xmlTcJson = {}
xmlTcJson["version"] = "V2012.02";
xmlTcJson["emetteur"] = "10001001";
xmlTcJson["destinataire"] = "10000001";
xmlTcJson["dateHeureGenerationXml"] = dateHeureGeneration;
xmlTcJson["commentaire"] = "";

var dossierUnique ={};
var identificationDossierUnique ={};

var identifiantDossierUnique ={};
identifiantDossierUnique["codePartenaireEmetteur"] = "FRONT-OFFICE";
identifiantDossierUnique["numeroDossierUnique"] = numeroDossierUnique;

var correspondant ={};

var adresseCorrespondant ={}
adresseCorrespondant["numeroDeVoie"] = numeroDeVoie				
adresseCorrespondant["indiceDeRepetition"] = indRep				
adresseCorrespondant["typeDeVoie"] = typeDeVoie	
adresseCorrespondant["libelleDeVoie"] = libelleDeVoie			
adresseCorrespondant["localite"] = localite	
adresseCorrespondant["complementDeLocalisation"] = complementDeLocalisation
adresseCorrespondant["codePostal"] = codePostal	
adresseCorrespondant["bureauDistributeur"] = localite			
adresseCorrespondant["adresseEmail"] = adresseEmail	

correspondant["identiteCorrespondant"] = { "nomCorrespondant": nomSignataire};	
correspondant["adresseCorrespondant"] = adresseCorrespondant;

identificationDossierUnique["identifiantDossierUnique"] = identifiantDossierUnique;
identificationDossierUnique["typeDossierUnique"] = "D1";
identificationDossierUnique["nomDossier"] = nomDossier;
identificationDossierUnique["correspondant"] = correspondant;

var destinataireDossierUnique = [];

var oneDestinataire = {};

var codeDestinataire = {};
codeDestinataire["codePartenaire"] = authorityId;
codeDestinataire["codeEdi"] = authorityId;
codeDestinataire["libelleIntervenant"] = authorityLabel;

oneDestinataire["roleDestinataire"] = authorityType;
oneDestinataire["codeDestinataire"] = codeDestinataire;

destinataireDossierUnique.push(oneDestinataire);

		//cas d'un deuxième destinataire
		if (authorityId2) {
			var secondDestinataire = {};
			var codeDestinataire2 = {};
			codeDestinataire2["codePartenaire"] = authorityId2;
			codeDestinataire2["codeEdi"] = authorityId2;
			codeDestinataire2["libelleIntervenant"] = authorityLabel2;
			secondDestinataire["roleDestinataire"] = authorityType2;
			secondDestinataire["codeDestinataire"] = codeDestinataire2;
			destinataireDossierUnique.push(secondDestinataire);
			}
		
		var listPieceJointes1 =[];
		var listPieceJointes2 =[];
		
		var indicePj = 2;
			//liasse cfe xml
			var pieceJointe1={};
			pieceJointe1["indicePieceJointe"] = indicePj;
			pieceJointe1["typePieceJointe"] = "LIASSE_CFE_EDI";
			pieceJointe1["formatPieceJointe"] = "XML";
			pieceJointe1["fichierPieceJointe"] = xmlRegent1Formatted;
			indiceXmlCfe = indicePj;
			indicePj++;

		
	//liasse cfe pdf
var pieceJointe2={};
pieceJointe2["indicePieceJointe"] = indicePj;
pieceJointe2["typePieceJointe"] = "LIASSE_CFE_PDF";
pieceJointe2["formatPieceJointe"] = "PDF";
pieceJointe2["fichierPieceJointe"] = pdfCfe1Formatted;
indicesPjCFE.push(indicePj);
indicePj++


listPieceJointes1.push(pieceJointe1);
listPieceJointes1.push(pieceJointe2);


if (authorityId2) {
	// liasse tdr xml
	var pieceJointe3={};
	pieceJointe3["indicePieceJointe"] = indicePj;
	pieceJointe3["typePieceJointe"] = "LIASSE_TDR_EDI";
	pieceJointe3["formatPieceJointe"] = "XML";
	pieceJointe3["fichierPieceJointe"] = xmlRegent2Formatted;//"XML_REGENT2.xml";
	indiceXmlTdr = indicePj;
	indicePj++
	listPieceJointes2.push(pieceJointe3);

	var pieceJointe4={};
	pieceJointe4["indicePieceJointe"] = indicePj;
	pieceJointe4["typePieceJointe"] = "LIASSE_TDR_PDF";
	pieceJointe4["formatPieceJointe"] = "PDF";
	pieceJointe4["fichierPieceJointe"] = pdfCfe2Formatted;//"cerfa2.xml";
	indicesPjTDR.push(indicePj);
	indicePj++
	listPieceJointes2.push(pieceJointe4);
}

//Ajout des pièces justificatives 
//var attachmentsNames = attachementsList.keys(); 
//_log.info("attachmentsNames in step 9 is {}",attachmentsNames);
//_log.info("attachmentsNames length in step 9 is {}",attachmentsNames.size());
//_log.info("typeof attachmentsNames in step 9 is {}",typeof attachmentsNames);	

//--> get CFE PJ
		for (var cle in attachementsList1 ) {
			_log.info("attachmentsNames1[i] in step 9 is {}",attachementsList1[cle]);
			_log.info("typeof attachmentsNames1[i] in step 9 is {}",typeof attachementsList1[cle]);
			var fileType = attachementsList1[cle]; 
			fileType = fileType.split('.');
			fileType = fileType[fileType.length - 1];			
	

			var pieceJointeUser={};
			pieceJointeUser["indicePieceJointe"] = indicePj;
			pieceJointeUser["typePieceJointe"] = "AUTRE_PJ";
			pieceJointeUser["formatPieceJointe"] = fileType.toUpperCase();
			pieceJointeUser["fichierPieceJointe"] = attachementsList1[cle];
			indicesPjCFE.push(indicePj);
			indicePj++; 
			attachementElement = {"id" : attachementsList1[cle],  "label" : "/" + attachementsList1[cle]};
			listBindAttachmentsFirstAuthority.push(attachementElement);
			listPieceJointes1.push(pieceJointeUser);			
			
		}
//--> get TDR PJ
		
		for (var cle in attachementsList2 ) {
			_log.info("attachmentsNames2[i] in step 9 is {}",attachementsList2[cle]);
			_log.info("typeof attachmentsNames2[i] in step 9 is {}",typeof attachementsList2[cle]);
			var fileType = attachementsList2[cle]; 
			fileType = fileType.split('.');
			fileType = fileType[fileType.length - 1];			

			var pieceJointeUser={};
			pieceJointeUser["indicePieceJointe"] = indicePj;
			pieceJointeUser["typePieceJointe"] = "AUTRE_PJ";
			pieceJointeUser["formatPieceJointe"] = fileType.toUpperCase();
			pieceJointeUser["fichierPieceJointe"] = attachementsList2[cle];
			indicesPjTDR.push(indicePj);
			indicePj++; 
			attachementElement = {"id" : attachementsList2[cle],  "label" : "/" + attachementsList2[cle]};
			listBindAttachmentsSecondAuthority.push(attachementElement);
			listPieceJointes2.push(pieceJointeUser);			
			
		}	

//Intégration du paiement dans le XML-TC si l'autorité a reçu un paiement	

var dossierCfe={};

var destinataireDossierCfe=[];

//dossier destinataire cfe 1
var oneDestinataireDossierCfe = {};


var indicePieceJointeCFE = [];

//insérer les indices des PJ pour CFE
var indicesPjLength = indicesPjCFE.length;
for (var i = 0; i < indicesPjLength; i++){
	indicePieceJointeCFE.push(indicesPjCFE[i]);
}

//Bloc à rajouter pour les paiements de la première autorité					





//Récupérer la liste des codes autorités présentes dans le data generated contenant les informations de paiement
if(_INPUT_.infoGenGroup != null){
	var listAuthoritiesCode=[];
	var listPayments = !_INPUT_.infoGenGroup.infoPartners.partnerPayment ? null : _INPUT_.infoGenGroup.infoPartners.partnerPayment;
	for (var i = 0; i < listPayments.length; i++){				
		if(listAuthoritiesCode.indexOf(listPayments[i]['partnerId'])==-1){
			listAuthoritiesCode.push(listPayments[i]['partnerId']);
			_log.info("listAuthoritiesCode in loop is {}", listAuthoritiesCode);
		}					
	}
	_log.info("listAuthoritiesCode  is {}", listAuthoritiesCode);
}	



if(_INPUT_.infoGenGroup != null){
	//Si la première autorité a reçue un paiement on l'ajoute au XML-TC		
	if(listAuthoritiesCode.indexOf(authorityId)!=-1){ 
		_log.info("Authority 1 have a payment");
		//Récupérer l'adresse du payeur : 
		var adressePayeur={};

		adressePayeur["numeroDeVoie"] = numeroDeVoiePayeur;
		adressePayeur["typeDeVoie"] = typeDeVoiePayeur;
		adressePayeur["libelleDeVoie"] = libelleDeVoiePayeur;
		adressePayeur["localite"] = localitePayeur;
		adressePayeur["codePostal"] = codePostalPayeur;
		adressePayeur["bureauDistributeur"] = localitePayeur;
		adressePayeur["adresseEmail"] = adresseEmailPayeur;

		//Récupérer les informations de paiements effectués dans le dossier et leurs informations
		var generalInformation = !_INPUT_.infoGenGroup.infoGen ? null : _INPUT_.infoGenGroup.infoGen;
		_log.info("generalInformation is {}",generalInformation);
		referencePaiement =  ! generalInformation.transactionId ? null : generalInformation.transactionId;
		_log.info("referencePaiement is {}",referencePaiement);
		dateHeurePaiement = ! generalInformation.transactionDate ? null : generalInformation.transactionDate;
		var listPayments = !_INPUT_.infoGenGroup.infoPartners.partnerPayment ? null : _INPUT_.infoGenGroup.infoPartners.partnerPayment;
		_log.info("listPayments is {}",listPayments);

		//2- Appel de directory pour avoir les infos ediCode de la première autorité 
		var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', destFuncId) //
		.connectionTimeout(10000) //
		.receiveTimeout(10000) //
		.accept('json') //
		.get();
		//result			     
		var receiverInfo = response.asObject();
		_log.info("receiverInfo is  {}",receiverInfo);
		// prepare all information of first authority
		var details = !receiverInfo.details ? null : receiverInfo.details;
		_log.info("details is  {}",details);


		var paymentPartnerIdFirstAuthority = ! details.ediCode ? null : details.ediCode;
		_log.info("paymentPartnerIdFirstAuthority is  {}",paymentPartnerIdFirstAuthority);

		//3- Faire la somme des paiements pour cette autorité
		var totalAmountPaymentsAuthority1 = 0.00;
		for (var i = 0; i < listPayments.length; i++){
			//Poster sur tracker le détail des paiements effectués pour une autorité:
			//nash.tracker.post("The authority"+ destFuncId +" received a payment of "+ listPayments[i]['paymentAmount'] +" € for the item :"+ listPayments[i]['description'] );		
			_log.info("payment partner id for the {} payment is  {}", i, listPayments[i]['partnerId']);

			if(listPayments[i]['partnerId']== paymentPartnerIdFirstAuthority){
				_log.info("amount for this payment is {}",listPayments[i]['paymentAmount']);
				totalAmountPaymentsAuthority1+= parseFloat(listPayments[i]['paymentAmount']);
			}
			_log.info("totalAmountPaymentsAuthority1 is  {}",totalAmountPaymentsAuthority1);
		}
		var frenchAmount = totalAmountPaymentsAuthority1.toFixed(2);
		frenchAmount = frenchAmount.replace('.', ',');
		_log.info("frenchAmount 1 is  {}",frenchAmount);

		var paiementDossier={};

		paiementDossier["identitePayeur"] = { "nomPayeur": nomSignataire };
		paiementDossier["adressePayeur"] = adressePayeur;
		paiementDossier["montantFormalite"] = frenchAmount;
		paiementDossier["referencePaiement"] = referencePaiement;
		paiementDossier["dateHeurePaiement"] = dateHeurePaiement;

		oneDestinataireDossierCfe["paiementDossier"]=paiementDossier;

		//Poster des messages d'informations sur le paiement
		//nash.tracker.post("The payment's transaction id is  :"+ referencePaiement);
	} 
}	


oneDestinataireDossierCfe["indicePieceJointe"]=indicePieceJointeCFE;
oneDestinataireDossierCfe["roleDestinataire"]= authorityType;
oneDestinataireDossierCfe["codeEdiDestinataire"]= authorityId;				

oneDestinataireDossierCfe["indiceLiasseXml"]=indiceXmlCfe;

destinataireDossierCfe.push(oneDestinataireDossierCfe);	

		
dossierCfe["numeroDeLiasse"] = numeroDeLiasse;			 
dossierCfe["dateHeureDepot"] = dateHeureGeneration;			 
dossierCfe["referenceLiasseFo"] = numeroDossierUnique;			 
dossierCfe["destinataireDossierCfe"] = destinataireDossierCfe;	

dossierUnique["identificationDossierUnique"] = identificationDossierUnique;
dossierUnique["destinataireDossierUnique"] = destinataireDossierUnique;
dossierUnique["pieceJointe"] = listPieceJointes1;
dossierUnique["dossierCfe"] = dossierCfe;

xmlTcJson["dossierUnique"] = dossierUnique;			





_log.info("xmlTcJson is  {}", xmlTcJson);
_log.info("JSON.stringify(xmlTcJson) is  {}", JSON.stringify(xmlTcJson));

if(null != referencePaiement){
//	Relier le numéro de dossier interne au numéro de liasse dans tracker
	nash.tracker.link(referencePaiement);
}																		
//Call to the XML-TC generation WS 
var response = nash.service.request('${regent.baseUrl}/private/v1/xml-tc/generate')
.dataType('application/json') //
.accept('application/json') //
.post(JSON.stringify(xmlTcJson));

//Record the generated XML-REGENT 
if (response != null && response.status == 200) { 
	var xmlTcFormatted1 =getFileName(authorityId,numeroDeLiasse,1,"xml","xmltc"); 

	var xmlTC = response.asBytes(); 
	nash.record.saveFile(xmlTcFormatted1,xmlTC);

	var xmlTcFormatted2;

	  if (destFuncId2) {
		  
		  	var secondDestinataire = {};
			var codeDestinataire2 = {};
		  	var destinataireDossierCfe=[];
			var oneDestinataireDossierCfe2 = {};
			var indicePieceJointeTDR = [];
			var destinataireDossierUnique = [];
			var indicesPjLength = indicesPjTDR.length;
			//insérer les indices des PJ pour ce partenaire
			for (var i = 0; i < indicesPjLength; i++){
			indicePieceJointeTDR.push(indicesPjTDR[i]);
			}
			
			codeDestinataire2["codePartenaire"] = authorityId2;
			codeDestinataire2["codeEdi"] = authorityId2;
			codeDestinataire2["libelleIntervenant"] = authorityLabel2;
			secondDestinataire["roleDestinataire"] = authorityType2;
			secondDestinataire["codeDestinataire"] = codeDestinataire2;
			
			destinataireDossierUnique.push(secondDestinataire);
			oneDestinataireDossierCfe2["indicePieceJointe"]=indicePieceJointeTDR;
			oneDestinataireDossierCfe2["roleDestinataire"]= authorityType2;
			oneDestinataireDossierCfe2["codeEdiDestinataire"]= authorityId2;
			oneDestinataireDossierCfe2["indiceLiasseXml"]=indiceXmlTdr;
			
			
			var indicePieceJointeTDR = [];
			var indicesPjLength = indicesPjTDR.length;
			//insérer les indices des PJ pour ce partenaire
			for (var i = 0; i < indicesPjLength; i++){
			indicePieceJointeTDR.push(indicesPjTDR[i]);
			}
	
			if(_INPUT_.infoGenGroup != null){	
				 if(listAuthoritiesCode.indexOf(authorityId2)!=-1){	 	
				_log.info(" Second authority have payments ");
				//Intégration du paiement pour la seconde autorité: 	
				
				
					
					//Récupérer les informations de paiements effectués dans le dossier et leurs informations
					var generalInformation = !_INPUT_.infoGenGroup.infoGen ? null : _INPUT_.infoGenGroup.infoGen;
					_log.info("generalInformation is {}",generalInformation);
					referencePaiement =  ! generalInformation.transactionId ? null : generalInformation.transactionId;
					_log.info("referencePaiement is {}",referencePaiement);
					dateHeurePaiement = ! generalInformation.transactionDate ? null : generalInformation.transactionDate;
					var listPayments = !_INPUT_.infoGenGroup.infoPartners.partnerPayment ? null : _INPUT_.infoGenGroup.infoPartners.partnerPayment;
					_log.info("listPayments is {}",listPayments);
					
					//Récupérer l'adresse du payeur : 
					var adressePayeur={};

						adressePayeur["numeroDeVoie"] = numeroDeVoiePayeur;
						adressePayeur["typeDeVoie"] = typeDeVoiePayeur;
						adressePayeur["libelleDeVoie"] = libelleDeVoiePayeur;
						adressePayeur["localite"] = localitePayeur;
						adressePayeur["codePostal"] = codePostalPayeur;
						adressePayeur["bureauDistributeur"] = localitePayeur;
						adressePayeur["adresseEmail"] = adresseEmailPayeur;
					//2- Appel de directory pour avoir le code payment partner de la première autorité 				
					var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', destFuncId2) //
						   .connectionTimeout(10000) //
						   .receiveTimeout(10000) //
						   .accept('json') //
						   .get();
						   
					//result			     
					var receiverInfo2 = response.asObject();
					_log.info("receiverInfo2 is  {}",receiverInfo2);
					// prepare all information of first authority
					var details2 = !receiverInfo2.details ? null : receiverInfo2.details;
					_log.info("details2 is  {}",details2);
					var paymentPartnerIdSecondAuthority = !details2.ediCode ? null : details2.ediCode;
					_log.info("paymentPartnerIdSecondAuthority is  {}",paymentPartnerIdSecondAuthority);
					
					//3- Faire la somme des paiements pour cette autorité
					var totalAmountPaymentsAuthority2 = 0.00;
					for (var i = 0; i < listPayments.length; i++){
						_log.info("payment partner id for the {} payment is  {}", i, listPayments[i]['partnerId']);
					//Poster sur tracker le détail des paiements effectués pour une autorité:
					//nash.tracker.post("The authority"+ destFuncId2 +" received a payment of "+ listPayments[i]['paymentAmount'] +" € for the item :"+ listPayments[i]['description'] );					
						if(listPayments[i]['partnerId']== paymentPartnerIdSecondAuthority){
							_log.info("amount for this payment is {}",listPayments[i]['paymentAmount']);
							totalAmountPaymentsAuthority2+= parseFloat(listPayments[i]['paymentAmount']);
						}
						_log.info("totalAmountPaymentsAuthority2 is  {}",totalAmountPaymentsAuthority2);
					}
					var frenchAmount = totalAmountPaymentsAuthority2.toFixed(2);
					frenchAmount = frenchAmount.replace('.', ',');
					_log.info("frenchAmount 2 is  {}",frenchAmount);
				var paiementDossier2={};				
					
					paiementDossier2["identitePayeur"] = { "nomPayeur": nomSignataire };
					paiementDossier2["adressePayeur"] = adressePayeur;
					paiementDossier2["montantFormalite"] = frenchAmount;
					paiementDossier2["referencePaiement"] = referencePaiement;
					paiementDossier2["dateHeurePaiement"] = dateHeurePaiement;
					oneDestinataireDossierCfe2["paiementDossier"]=paiementDossier2;
					
					//Poster des messages d'informations sur le paiement
					//nash.tracker.post("The payment's transaction id is  :"+ referencePaiement);
					
				}
			}
			
			destinataireDossierCfe.push(oneDestinataireDossierCfe2);
			dossierCfe["destinataireDossierCfe"] = destinataireDossierCfe;	
			
			dossierUnique["dossierCfe"] = dossierCfe;
			dossierUnique["pieceJointe"] = listPieceJointes2;
			dossierUnique["destinataireDossierUnique"] = destinataireDossierUnique;
			xmlTcJson["dossierUnique"] = dossierUnique;
		  
		  
			var response = nash.service.request('${regent.baseUrl}/private/v1/xml-tc/generate')
			.dataType('application/json') //
			.accept('application/json') //
			.post(JSON.stringify(xmlTcJson));
			var xmlTC = response.asBytes(); 
			xmlTcFormatted2 =getFileName(authorityId2,numeroDeLiasse,1,"xml","xmltc"); 
			nash.record.saveFile(xmlTcFormatted2,xmlTC);
		}

	//Récupérer la dénomination
	var denomination = $M2.cadreRappelIdentificationGroup.cadre1RappelIdentification.entrepriseDenomination;
	_log.info("denomination is  {}", denomination);
	_log.info("listBindAttachmentsFirstAuthority is  {}", listBindAttachmentsFirstAuthority);
	_log.info("listBindAttachmentsSecondAuthority is  {}", listBindAttachmentsSecondAuthority);  
	//bind return 
	var output1 = nash.instance.load("output1.xml");
	output1.bind("parameters",{
		"attachment" : {
			"cerfa" : "/" + pdfCfe1Formatted,
			"regent" : "/" + xmlRegent1Formatted,
			"others" : listBindAttachmentsFirstAuthority,
			"xmltc" : "/" + xmlTcFormatted1
		}
	});
	output1.bind("result", {
		"funcId" : destFuncId,
		"codeEdi" : authorityId,
		"denomination" : denomination
	});


	//bind return 2nd authorityI
	if (destFuncId2) {
		//bind return 
		var output2 = nash.instance.load("output2.xml")
		output2.bind("parameters",{
			"attachment" : {
				"cerfa" : "/" + pdfCfe2Formatted,
				"regent" : "/" + xmlRegent2Formatted,
				"others" : listBindAttachmentsSecondAuthority,
				"xmltc" : "/" + xmlTcFormatted2
			}
		});


		output2.bind("result", {
			"funcId" : destFuncId2,
			"codeEdi" : authorityId2,
			"denomination" : denomination
		});
	}
	return spec.create({
		id : 'xmlTcGenerationConfirmation',
		label : "Xml TC Regent confirmation message",
		groups : [ spec.createGroup({
			id : 'confirmationMessageOk',
			description : "Le fichier XML TC a été généré et ajouté au dossier.",
			data : []
		}) ]
	});
}else{
	return spec.create({
		id : 'xmlTcGenerationConfirmation',
		label : "Xml TC Regent confirmation message",
		groups : [ spec.createGroup({
			id : 'confirmationMessageKo',
			description : "Une erreur a été relevé lors de la génération du XML TC.",
			data : []
		}) ]
	});
}