function pad(s) { return (s < 10) ? '0' + s : s; }

var regEx = new RegExp('[0-9]{5}'); 
var objet = $M2.cadreModificationSocieteGroup.cadre1ObjetModification;
var identite = $M2.cadreRappelIdentificationGroup.cadre1RappelIdentification;
var dissolution = $M2.modifDissolutionGroup.modifDissolution;
var modifDeno = $M2.cadreModificationPMGroup.cadre2ModificationPM.modificationDeno;
var modifFormeJuridique = $M2.cadreModificationPMGroup.cadre2ModificationPM.modifFormeJuridique;
var modifCapital = $M2.cadreModificationPMGroup.cadre2ModificationPM.modifCapital;
var modifDuree = $M2.cadreModificationPMGroup.cadre2ModificationPM.modificationDuree;
var modifEss = $M2.cadreModificationPMGroup.cadre2ModificationPM.modifEss;
var modifSocieteMission = $M2.cadreModificationPMGroup.cadre2ModificationPM.modifSocieteMission;
var modifContratAppui = $M2.cadreModificationPMGroup.cadre2ModificationPM.modifContratAppui;
var modifRenouvellementMaintienRCS = $M2.cadreModificationPMGroup.cadre2ModificationPM.modifRenouvellementMaintienRCS;
var siege = $M2.cadreRappelIdentificationGroup.cadre1RappelIdentification.adresseSiege;
var fusion = $M2.cadreModificationFusionGroup.cadreModificationFusion;
var fermeture = $M2.cadreEtablissementFermetureGroup.cadreEtablissementFermeture;
var fondsDonne = $M2.cadreFondsDonneLocationGeranceGroup.cadreFondsDonneLocationGerance;
var ouverture = $M2.cadreEtablissementOuvertureGroup.cadreEtablissementOuverture;
var activiteInfo = $M2.cadreEtablissementActiviteGroup.cadre7EtablissementActivite;
var activite = $M2.cadreEtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite;
var origine = $M2.cadre4OrigineFondsGroup.cadre4OrigineFonds;
var dirigeantSARL1 = $M2.dirigeant1Sarl.cadreIdentiteDirigeant;
var dirigeantSARL2 = $M2.dirigeant2Sarl.cadreIdentiteDirigeant;
var dirigeantSARL3 = $M2.dirigeant3Sarl.cadreIdentiteDirigeant;
var dirigeantSARL4 = $M2.dirigeant4Sarl.cadreIdentiteDirigeant;
var dirigeantSARL5 = $M2.dirigeant5Sarl.cadreIdentiteDirigeant;
var conjoint1 = $M2.dirigeant1Sarl.cadreIdentiteDirigeant.cadre2InfosConjoint;
var conjoint2 = $M2.dirigeant2Sarl.cadreIdentiteDirigeant.cadre2InfosConjoint;
var conjoint3 = $M2.dirigeant3Sarl.cadreIdentiteDirigeant.cadre2InfosConjoint;
var conjoint4 = $M2.dirigeant4Sarl.cadreIdentiteDirigeant.cadre2InfosConjoint;
var conjoint5 = $M2.dirigeant5Sarl.cadreIdentiteDirigeant.cadre2InfosConjoint;
var dirigeant1 = $M2.dirigeant1.cadreIdentiteDirigeant;
var dirigeant2 = $M2.dirigeant2.cadreIdentiteDirigeant;
var dirigeant3 = $M2.dirigeant3.cadreIdentiteDirigeant;
var dirigeant4 = $M2.dirigeant4.cadreIdentiteDirigeant;
var dirigeant5 = $M2.dirigeant5.cadreIdentiteDirigeant;
var dirigeant6 = $M2.dirigeant6.cadreIdentiteDirigeant;
var dirigeant7 = $M2.dirigeant7.cadreIdentiteDirigeant;
var dirigeant8 = $M2.dirigeant8.cadreIdentiteDirigeant;
var dirigeant9 = $M2.dirigeant9.cadreIdentiteDirigeant;
var dirigeant10 = $M2.dirigeant10.cadreIdentiteDirigeant;
var dirigeant11 = $M2.dirigeant11.cadreIdentiteDirigeant;
var dirigeant12 = $M2.dirigeant12.cadreIdentiteDirigeant;
var socialDirigeant1 = $M2.cadreDeclarationSociale1.cadre7DeclarationSociale;
var socialDirigeant2 = $M2.cadreDeclarationSociale2.cadre7DeclarationSociale;
var socialDirigeant3 = $M2.cadreDeclarationSociale3.cadre7DeclarationSociale;
var socialDirigeant4 = $M2.cadreDeclarationSociale4.cadre7DeclarationSociale;
var socialDirigeant5 = $M2.cadreDeclarationSociale5.cadre7DeclarationSociale;
var socialDirigeant6 = $M2.cadreDeclarationSociale6.cadre7DeclarationSociale;
var socialDirigeant7 = $M2.cadreDeclarationSociale7.cadre7DeclarationSociale;
var socialDirigeant8 = $M2.cadreDeclarationSociale8.cadre7DeclarationSociale;
var socialDirigeant9 = $M2.cadreDeclarationSociale9.cadre7DeclarationSociale;
var socialDirigeant10 = $M2.cadreDeclarationSociale10.cadre7DeclarationSociale;
var socialDirigeant11 = $M2.cadreDeclarationSociale11.cadre7DeclarationSociale;
var socialDirigeant12 = $M2.cadreDeclarationSociale12.cadre7DeclarationSociale;
var socialAssocie1 = $M2.cadreDeclarationSocialeConjointAssocie1.cadre7DeclarationSociale;
var socialAssocie2 = $M2.cadreDeclarationSocialeConjointAssocie2.cadre7DeclarationSociale;
var socialAssocie3 = $M2.cadreDeclarationSocialeConjointAssocie3.cadre7DeclarationSociale;
var socialAssocie4 = $M2.cadreDeclarationSocialeConjointAssocie4.cadre7DeclarationSociale;
var socialAssocie5 = $M2.cadreDeclarationSocialeConjointAssocie5.cadre7DeclarationSociale;
var correspondance = $M2.cadre8RensCompGroup.cadre8RensComp;
var signataire = $M2.cadre9SignatureGroup.cadre9Signature;
var jqpa1 = $M2.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification1;																					
var jqpa2 = $M2.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification2;																					
var jqpa3 = $M2.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification3;																					


var regentVersion = "V2008.11";  // (V2008.11, V2016.02) 
var eventRegent = []; 
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('10M')) {
eventRegent.push("10M");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege')
	or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal'))) {
eventRegent.push("11M");
}
if (activite.modifObjetSocial) {
eventRegent.push("12M");
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')) {
eventRegent.push("13M");
}
if (Value('id').of(modifCapital.modifCapitalType).contains('15M') or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('fusion')) {
eventRegent.push("15M");
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('16M')) { 
eventRegent.push("16M");
}
if ((dirigeantSARL1.cadreGerance.typeGeranceModifiee and (Value('id').of(dirigeantSARL1.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire') or dirigeantSARL1.cadreGerance.ancienneGeranceMaj))
or (dirigeantSARL2.cadreGerance.typeGeranceModifiee and (Value('id').of(dirigeantSARL2.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire') or dirigeantSARL2.cadreGerance.ancienneGeranceMaj))
or (dirigeantSARL3.cadreGerance.typeGeranceModifiee and (Value('id').of(dirigeantSARL3.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire') or dirigeantSARL3.cadreGerance.ancienneGeranceMaj))
or (dirigeantSARL4.cadreGerance.typeGeranceModifiee and (Value('id').of(dirigeantSARL4.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire') or dirigeantSARL4.cadreGerance.ancienneGeranceMaj))
or (dirigeantSARL5.cadreGerance.typeGeranceModifiee and (Value('id').of(dirigeantSARL5.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire') or dirigeantSARL5.cadreGerance.ancienneGeranceMaj)))	{
eventRegent.push("19M");
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M')	and not dissolution.dissolutionTransmissionUniverselle) {
eventRegent.push("22M");
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('23M')) {
eventRegent.push("23M");
}
if ((not objet.immatriculationRM
	and ((activite.nouvelleActiviteGroup1.domaineAdjonctionNAFA != null
	and not Value('id').of(activite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup1.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	or (activite.nouvelleActiviteGroup2.domaineAdjonctionNAFA != null
	and not Value('id').of(activite.nouvelleActiviteGroup2.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup2.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup2.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup2.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup2.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	or (activite.nouvelleActiviteGroup3.domaineAdjonctionNAFA != null
	and not Value('id').of(activite.nouvelleActiviteGroup3.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup3.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup3.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup3.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup3.jqpaDomaineTransport).eq('jqpaTransportAutre')))
	and not activite.optionCMACCI)
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege')
	or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal')) 
	and not objet.transfertDepartement)) { 
eventRegent.push("24M");
}
if (Value('id').of(modifCapital.modifCapitalType).contains('25M')) {
eventRegent.push("25M");
}
if (Value('id').of(modifCapital.modifCapitalType).contains('26M')) {
eventRegent.push("26M");
}
if (dissolution.dissolutionTransmissionUniverselle
or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('29M') 
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('10M')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('capital')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('16M')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('fusion')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('37M')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('40M')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('23M')
and not Value('id').of(objet.objetModification).contains('etablissement') 
and not Value('id').of(objet.objetModification).contains('dirigeant'))
or ((Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).eq('18M')
or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).eq('societeMission')) 
and not Value('id').of(objet.objetModification).contains('etablissement') 
and not Value('id').of(objet.objetModification).contains('dirigeant'))) {
eventRegent.push("29M");
} 
var gerants = [];
['dirigeant1Sarl', 'dirigeant2Sarl', 'dirigeant3Sarl', 'dirigeant4Sarl', 'dirigeant5Sarl'].forEach(function(gerant){
	if (null != $M2[gerant] and ((Value('id').of($M2[gerant].cadreIdentiteDirigeant.personneLieeQualite).eq('30') or Value('id').of($M2[gerant].cadreIdentiteDirigeant.personneLieeQualite).eq('40'))
		or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and Value('id').of($M2[gerant].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant') and $M2[gerant].cadreIdentiteDirigeant.personneLieeQualiteAncienne2 != null))) {
		gerants.push($M2[gerant]);
}
});
if ((Value('id').of(objet.objetModification).contains('dirigeant') or modifFormeJuridique.formeJuridiqueModifDirigeant)
and ((gerants.length > 0) or (not Value('id').of(dirigeant1.personneLieeQualite).eq('66') and not ouverture.ajoutFondePouvoir)
or (dirigeant2.personneLieeQualite != null and not Value('id').of(dirigeant2.personneLieeQualite).eq('66')) 
or (dirigeant3.personneLieeQualite != null and not Value('id').of(dirigeant3.personneLieeQualite).eq('66')) 
or (dirigeant4.personneLieeQualite != null and not Value('id').of(dirigeant4.personneLieeQualite).eq('66')) 
or (dirigeant5.personneLieeQualite != null and not Value('id').of(dirigeant5.personneLieeQualite).eq('66')) 
or (dirigeant6.personneLieeQualite != null and not Value('id').of(dirigeant6.personneLieeQualite).eq('66')) 
or (dirigeant7.personneLieeQualite != null and not Value('id').of(dirigeant7.personneLieeQualite).eq('66')) 
or (dirigeant8.personneLieeQualite != null and not Value('id').of(dirigeant8.personneLieeQualite).eq('66')) 
or (dirigeant9.personneLieeQualite != null and not Value('id').of(dirigeant9.personneLieeQualite).eq('66')) 
or (dirigeant10.personneLieeQualite != null and not Value('id').of(dirigeant10.personneLieeQualite).eq('66')) 
or (dirigeant11.personneLieeQualite != null and not Value('id').of(dirigeant11.personneLieeQualite).eq('66')) 
or (dirigeant12.personneLieeQualite != null and not Value('id').of(dirigeant12.personneLieeQualite).eq('66')))) {
if (((Value('id').of(identite.entrepriseFormeJuridique).eq('5306') or Value('id').of(identite.entrepriseFormeJuridique).eq('5307')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5202') or Value('id').of(identite.entrepriseFormeJuridique).eq('5203')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6589') or Value('id').of(identite.entrepriseFormeJuridique).eq('6521')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6532') or Value('id').of(identite.entrepriseFormeJuridique).eq('6539')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6540') or Value('id').of(identite.entrepriseFormeJuridique).eq('6541')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6542') or Value('id').of(identite.entrepriseFormeJuridique).eq('6599')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6543') or Value('id').of(identite.entrepriseFormeJuridique).eq('6544')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6554') or Value('id').of(identite.entrepriseFormeJuridique).eq('6558')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6560') or Value('id').of(identite.entrepriseFormeJuridique).eq('6561')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6562') or Value('id').of(identite.entrepriseFormeJuridique).eq('6563')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6564') or Value('id').of(identite.entrepriseFormeJuridique).eq('6565')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6566') or Value('id').of(identite.entrepriseFormeJuridique).eq('6567')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6568') or Value('id').of(identite.entrepriseFormeJuridique).eq('6569')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6571') or Value('id').of(identite.entrepriseFormeJuridique).eq('6572')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6573') or Value('id').of(identite.entrepriseFormeJuridique).eq('6574')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6575') or Value('id').of(identite.entrepriseFormeJuridique).eq('6576')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6577') or Value('id').of(identite.entrepriseFormeJuridique).eq('6578')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6585') or Value('id').of(identite.entrepriseFormeJuridique).eq('6534')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6536') or Value('id').of(identite.entrepriseFormeJuridique).eq('6316'))
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M'))
or ((Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5306') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5307')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5202') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5203')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6589') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6521')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6532') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6539')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6540') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6541')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6542') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6599')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6543') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6544')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6554') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6558')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6560') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6561')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6562') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6563')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6564') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6565')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6566') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6567')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6568') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6569')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6571') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6572')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6573') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6574')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6575') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6576')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6577') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6578')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6585') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6534')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6536') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6316'))
and Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M'))) {
eventRegent.push("34M");
} else { eventRegent.push("35M"); }
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('37M')) {
eventRegent.push("37M");
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('40M') or dissolution.dissoMiseEnSommeil) {
eventRegent.push("40M");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
and objet.cadreObjetModificationEtablissement.lieuActiviteSiege) {
eventRegent.push("51M");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
and not objet.cadreObjetModificationEtablissement.lieuActiviteSiege
and Value('id').of(objet.cadreObjetModificationEtablissement.priseRepriseActivite).eq('prise')) {
eventRegent.push("52M");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M') 
	or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsreprise')) {
eventRegent.push("53M");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(ouverture.etablissementDejaConnu).eq('non')
	and not Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsreprise'))
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
	and not objet.cadreObjetModificationEtablissement.lieuActiviteSiege
	and Value('id').of(objet.cadreObjetModificationEtablissement.priseRepriseActivite).eq('reprise'))) {
eventRegent.push("54M");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60M') 
	or activiteInfo.modifEtablissementEnseigne) {
eventRegent.push("60M");
}
if (Value('id').of(activite.activiteEvenement).eq('61M') or Value('id').of(activite.activiteEvenement).eq('61M62M')
	or activiteInfo.modifRepriseExploitation) {
eventRegent.push("61M");
}
if ((Value('id').of(activite.activiteEvenement).eq('62M') or Value('id').of(activite.activiteEvenement).eq('61M62M'))) {
eventRegent.push("62M");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63M')) {
eventRegent.push("63M");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64M')) {
eventRegent.push("64M");
}
if (Value('id').of(activite.activiteEvenement).eq('67M')) {
eventRegent.push("67M");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68M')) {
eventRegent.push("68M");
}
var fondePouvoir70M = [];
['dirigeant1', 'dirigeant2', 'dirigeant3', 'dirigeant4', 'dirigeant5', 'dirigeant6', 'dirigeant7', 'dirigeant8', 'dirigeant9', 'dirigeant10', 'dirigeant11', 'dirigeant12'].forEach(function(fondePouvoirs70M){
	if (null != $M2[fondePouvoirs70M] and Value('id').of($M2[fondePouvoirs70M].cadreIdentiteDirigeant.personneLieeQualite).eq('66')) {
		fondePouvoir70M.push($M2[fondePouvoirs70M]);	
	}
});
var pouvoir70M = [];
['dirigeant1Sarl', 'dirigeant2Sarl', 'dirigeant3Sarl', 'dirigeant4Sarl', 'dirigeant5Sarl'].forEach(function(pouvoirs70M){
	if (null != $M2[pouvoirs70M] and Value('id').of($M2[pouvoirs70M].cadreIdentiteDirigeant.personneLieeQualite).eq('66')) {
		pouvoir70M.push($M2[pouvoirs70M]);	
	}
});
if ((fondePouvoir70M.length > 0) or (pouvoir70M.length > 0)) { 
eventRegent.push("70M");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80M') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and not Value('id').of(fermeture.destinationEtablissement3).eq('devientPrincipal') 
	and not Value('id').of(fermeture.destinationEtablissement3).eq('miseEnLocationPartielle')
	and not Value('id').of(fermeture.destinationEtablissement3).eq('miseEnLocationTotale')
	and not Value('id').of(fermeture.destinationEtablissement4).eq('devientSecondaire')
	and not Value('id').of(fermeture.destinationEtablissement5).eq('devientSiege')
	and not Value('id').of(fermeture.destinationEtablissement5).eq('devientSecondaire')
	and not Value('id').of(fermeture.destinationEtablissement6).eq('devientSecondaire')
	and not Value('id').of(fermeture.destinationEtablissement7).eq('miseEnLocationPartielle'))) {
eventRegent.push("80M");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('81M')
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(fermeture.destinationEtablissement5).eq('devientSiege'))) {
eventRegent.push("81M");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('82M') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and (Value('id').of(fermeture.destinationEtablissement3).eq('miseEnLocationPartielle') 
	or Value('id').of(fermeture.destinationEtablissement7).eq('miseEnLocationPartielle')))) {
eventRegent.push("82M");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('84M') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(fermeture.destinationEtablissement3).eq('miseEnLocationTotale'))) {
eventRegent.push("84M");
}

// Valeurs à compléter par Directory  
var authorityType = "CFE"; // (CFE, TDR) 
var authorityId = _INPUT_.dataGroup.codeEDI; // (code EDI) 


//Ajout du type de la deuxième autorité
var authorityType2 = "TDR"; // (CFE, TDR) 
//Ajout du code de la deuxième autorité
var authorityId2 = _INPUT_.dataGroup.codeEDI2; // (code EDI) 


var regentFields = {}; 

// A mettre toujours sous "Z1611"
regentFields['/REGENT-XML/Emetteur']="Z1611";

// A récuperer depuis directory
regentFields['/REGENT-XML/Destinataire']= authorityId;

// Completé par directory
regentFields['/REGENT-XML/DateHeureEmission']= null;
regentFields['/REGENT-XML/VersionMessage']= null;
regentFields['/REGENT-XML/VersionNorme']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Specification/NomService']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Specification/VersionService']= null;

					// Groupe GDF : Groupe données de service
					
// Sous groupe IDF : Identification de la formalité

// Généré par destiny
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C01']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C03']= "0";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C04']= null;
if (identite.immatriculationRM
or (not identite.immatriculationRM
and ((activite.nouvelleActiviteGroup1.domaineAdjonctionNAFA != null
and not Value('id').of(activite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
and not Value('id').of(activite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
and not Value('id').of(activite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
and not Value('id').of(activite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre')
and not Value('id').of(activite.nouvelleActiviteGroup1.jqpaDomaineTransport).eq('jqpaTransportAutre'))
or (activite.nouvelleActiviteGroup2.domaineAdjonctionNAFA != null
and not Value('id').of(activite.nouvelleActiviteGroup2.domaineAdjonctionNAFA).eq('nafaAutre')
and not Value('id').of(activite.nouvelleActiviteGroup2.nafaDomaineAlimentation).eq('alimentationAutre')
and not Value('id').of(activite.nouvelleActiviteGroup2.nafaDomaineAutresServices).eq('servicesAutre')
and not Value('id').of(activite.nouvelleActiviteGroup2.nafaDomaineProduction).eq('productionAutre')
and not Value('id').of(activite.nouvelleActiviteGroup2.jqpaDomaineTransport).eq('jqpaTransportAutre'))
or (activite.nouvelleActiviteGroup3.domaineAdjonctionNAFA != null
and not Value('id').of(activite.nouvelleActiviteGroup3.domaineAdjonctionNAFA).eq('nafaAutre')
and not Value('id').of(activite.nouvelleActiviteGroup3.nafaDomaineAlimentation).eq('alimentationAutre')
and not Value('id').of(activite.nouvelleActiviteGroup3.nafaDomaineAutresServices).eq('servicesAutre')
and not Value('id').of(activite.nouvelleActiviteGroup3.nafaDomaineProduction).eq('productionAutre')
and not Value('id').of(activite.nouvelleActiviteGroup3.jqpaDomaineTransport).eq('jqpaTransportAutre')))
and not activite.optionCMACCI)) 	{
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05']= "M";
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05']= "C";
}	

// Sous groupe EDF : Evènement déclaré

var occurrencesC10 = [];
var occurrence = {};

if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('10M')) {
occurrence['C10.1'] = '10M';
if(modifDeno.modifDateDeno != null) {
	var dateTemp = new Date(parseInt(modifDeno.modifDateDeno.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {};
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege')
	or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal'))) {
occurrence['C10.1'] = '11M';
if(fermeture.modifDateTransfert != null and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).contains('transfert')) {
	var dateTemp = new Date(parseInt(fermeture.modifDateTransfert.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {};
}
if (activite.modifObjetSocial) {
occurrence['C10.1'] = '12M';
if (activite.modifDateActivite != null) {
	var dateTemp = new Date(parseInt(activite.modifDateActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {};
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')) {
occurrence['C10.1'] = '13M';
if (modifFormeJuridique.modifDateFormeJuridique != null) {
	var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {};
}
if (Value('id').of(modifCapital.modifCapitalType).contains('15M') or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('fusion')) {
occurrence['C10.1'] = '15M';
if(modifCapital.modifDateCapital != null) {
	var dateTemp = new Date(parseInt(modifCapital.modifDateCapital.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if(fusion.modifDateFusion != null) {
	var dateTemp = new Date(parseInt(fusion.modifDateFusion.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {};
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('16M')) { 
occurrence['C10.1'] = '16M';
if (modifDuree.modifDateDuree != null) {
	var dateTemp = new Date(parseInt(modifDuree.modifDateDuree.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {};
}
if ((dirigeantSARL1.cadreGerance.typeGeranceModifiee and (Value('id').of(dirigeantSARL1.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire') or dirigeantSARL1.cadreGerance.ancienneGeranceMaj))
or (dirigeantSARL2.cadreGerance.typeGeranceModifiee and (Value('id').of(dirigeantSARL2.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire') or dirigeantSARL2.cadreGerance.ancienneGeranceMaj))
or (dirigeantSARL3.cadreGerance.typeGeranceModifiee and (Value('id').of(dirigeantSARL3.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire') or dirigeantSARL3.cadreGerance.ancienneGeranceMaj))
or (dirigeantSARL4.cadreGerance.typeGeranceModifiee and (Value('id').of(dirigeantSARL4.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire') or dirigeantSARL4.cadreGerance.ancienneGeranceMaj))
or (dirigeantSARL5.cadreGerance.typeGeranceModifiee and (Value('id').of(dirigeantSARL5.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire') or dirigeantSARL5.cadreGerance.ancienneGeranceMaj)))	{
occurrence['C10.1'] = '19M';
if((dirigeantSARL1.cadreGerance.typeGeranceModifiee and (Value('id').of(dirigeantSARL1.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire') or dirigeantSARL1.cadreGerance.ancienneGeranceMaj))
and dirigeantSARL1.dateModifDirigeant != null) {
	var dateTemp = new Date(parseInt(dirigeantSARL1.dateModifDirigeant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if((dirigeantSARL2.cadreGerance.typeGeranceModifiee and (Value('id').of(dirigeantSARL2.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire') or dirigeantSARL2.cadreGerance.ancienneGeranceMaj))
and dirigeantSARL2.dateModifDirigeant != null) {
	var dateTemp = new Date(parseInt(dirigeantSARL2.dateModifDirigeant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if((dirigeantSARL3.cadreGerance.typeGeranceModifiee and (Value('id').of(dirigeantSARL3.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire') or dirigeantSARL3.cadreGerance.ancienneGeranceMaj))
and dirigeantSARL3.dateModifDirigeant != null) {
	var dateTemp = new Date(parseInt(dirigeantSARL3.dateModifDirigeant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if((dirigeantSARL4.cadreGerance.typeGeranceModifiee and (Value('id').of(dirigeantSARL4.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire') or dirigeantSARL4.cadreGerance.ancienneGeranceMaj))
and dirigeantSARL4.dateModifDirigeant != null) {
	var dateTemp = new Date(parseInt(dirigeantSARL4.dateModifDirigeant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if((dirigeantSARL5.cadreGerance.typeGeranceModifiee and (Value('id').of(dirigeantSARL5.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire') or dirigeantSARL5.cadreGerance.ancienneGeranceMaj))
and dirigeantSARL5.dateModifDirigeant != null) {
	var dateTemp = new Date(parseInt(dirigeantSARL5.dateModifDirigeant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {};
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M')	and not dissolution.dissolutionTransmissionUniverselle) {
occurrence['C10.1'] = '22M';
if (dissolution.modifDateDissolution != null) {
	var dateTemp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {}; 
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('23M')) {
occurrence['C10.1'] = '23M';
if (modifRenouvellementMaintienRCS.modifDateRenouvellementMaintienRCS1 != null) {
	var dateTemp = new Date(parseInt(modifRenouvellementMaintienRCS.modifDateRenouvellementMaintienRCS1.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {}; 
}
if ((not objet.immatriculationRM
	and ((activite.nouvelleActiviteGroup1.domaineAdjonctionNAFA != null
	and not Value('id').of(activite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup1.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	or (activite.nouvelleActiviteGroup2.domaineAdjonctionNAFA != null
	and not Value('id').of(activite.nouvelleActiviteGroup2.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup2.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup2.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup2.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup2.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	or (activite.nouvelleActiviteGroup3.domaineAdjonctionNAFA != null
	and not Value('id').of(activite.nouvelleActiviteGroup3.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup3.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup3.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup3.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup3.jqpaDomaineTransport).eq('jqpaTransportAutre')))
	and not activite.optionCMACCI)
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege')
	or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal')) 
	and not objet.transfertDepartement)) { 
occurrence['C10.1'] = '24M';
if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or activiteInfo.modifActiviteTransfert or activiteInfo.modifActiviteReprise
	or activiteInfo.modifActiviteAcquisition or activiteInfo.modifRepriseExploitation)
	and activite.modifDateActivite != null)	{
	var dateTemp = new Date(parseInt(activite.modifDateActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and fermeture.modifDateTransfert != null) {
    var dateTemp = new Date(parseInt(fermeture.modifDateTransfert.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M') and ouverture.modifDateAdresseOuverture != null) {
	var dateTemp = new Date(parseInt(ouverture.modifDateAdresseOuverture.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite') and ouverture.modifDatePriseActiviteEtablissement != null) {
    var dateTemp = new Date(parseInt(ouverture.modifDatePriseActiviteEtablissement.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite') and activiteInfo.modifDatePriseActiviteSiege != null) {
    var dateTemp = new Date(parseInt(activiteInfo.modifDatePriseActiviteSiege.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M') and ouverture.modifDateRepriseExploitation != null) {
    var dateTemp = new Date(parseInt(ouverture.modifDateRepriseExploitation.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
}    
occurrencesC10.push(occurrence);
occurrence = {}; 
}
if (Value('id').of(modifCapital.modifCapitalType).contains('25M')) {
occurrence['C10.1'] = '25M';
if (modifCapital.modifDateContinuation != null) {
	var dateTemp = new Date(parseInt(modifCapital.modifDateContinuation.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {}; 
}
if (Value('id').of(modifCapital.modifCapitalType).contains('26M')) {
occurrence['C10.1'] = '26M';
if (modifCapital.modifDateReconstitution != null) {
	var dateTemp = new Date(parseInt(modifCapital.modifDateReconstitution.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {}; 
}
if (dissolution.dissolutionTransmissionUniverselle
or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('29M') 
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('10M')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('capital')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('16M')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('fusion')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('37M')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('40M')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('23M')
and not Value('id').of(objet.objetModification).contains('etablissement') 
and not Value('id').of(objet.objetModification).contains('dirigeant'))
or ((Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).eq('18M')
or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).eq('societeMission')) 
and not Value('id').of(objet.objetModification).contains('etablissement') 
and not Value('id').of(objet.objetModification).contains('dirigeant'))) {
occurrence['C10.1'] = '29M';
if (dissolution.dissolutionTransmissionUniverselle) {
if (dissolution.dissolutionTransmissionUniverselle and dissolution.modifDateDissolution != null) {
	var dateTemp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
}
occurrence['C10.3'] = "Transmission du patrimoine à l'associé unique restant";
occurrencesC10.push(occurrence);
occurrence = {}; 
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('29M') 
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('10M')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('capital')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('16M')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('fusion')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('37M')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('40M')
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('23M')
and not Value('id').of(objet.objetModification).contains('etablissement') 
and not Value('id').of(objet.objetModification).contains('dirigeant')) {
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('29M')	and objet.cadreModifDateActivite.newDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(objet.cadreModifDateActivite.newDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} 
occurrence['C10.3'] = "Modification de la date de début d'activité";
occurrencesC10.push(occurrence);
occurrence = {}; 
} else if ((Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).eq('18M')
or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).eq('societeMission')) 
and not Value('id').of(objet.objetModification).contains('etablissement') 
and not Value('id').of(objet.objetModification).contains('dirigeant')) {
if (modifEss.modifDateESS != null) {
	var dateTemp = new Date(parseInt(modifEss.modifDateESS.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
occurrence['C10.3'] = "économie sociale et solidaire";
} else if (modifSocieteMission.modifDateSocieteMission != null) {
	var dateTemp = new Date(parseInt(modifSocieteMission.modifDateSocieteMission.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
if (Value('id').of(modifSocieteMission.modifSocieteMissionAdhesionSortie).eq('modifSocieteMissionAdhesion')) {
occurrence['C10.3'] = "Obtention de la qualité de société à mission";
} else if (Value('id').of(modifSocieteMission.modifSocieteMissionAdhesionSortie).eq('modifSocieteMissionSortie')) {
occurrence['C10.3'] = "Suppression de la qualité de société à mission";
}
} 
occurrencesC10.push(occurrence);
occurrence = {}; 
}
}
if ((Value('id').of(objet.objetModification).contains('dirigeant') or modifFormeJuridique.formeJuridiqueModifDirigeant)
and ((gerants.length > 0) or (not Value('id').of(dirigeant1.personneLieeQualite).eq('66') and not ouverture.ajoutFondePouvoir)
or (dirigeant2.personneLieeQualite != null and not Value('id').of(dirigeant2.personneLieeQualite).eq('66')) 
or (dirigeant3.personneLieeQualite != null and not Value('id').of(dirigeant3.personneLieeQualite).eq('66')) 
or (dirigeant4.personneLieeQualite != null and not Value('id').of(dirigeant4.personneLieeQualite).eq('66')) 
or (dirigeant5.personneLieeQualite != null and not Value('id').of(dirigeant5.personneLieeQualite).eq('66')) 
or (dirigeant6.personneLieeQualite != null and not Value('id').of(dirigeant6.personneLieeQualite).eq('66')) 
or (dirigeant7.personneLieeQualite != null and not Value('id').of(dirigeant7.personneLieeQualite).eq('66')) 
or (dirigeant8.personneLieeQualite != null and not Value('id').of(dirigeant8.personneLieeQualite).eq('66')) 
or (dirigeant9.personneLieeQualite != null and not Value('id').of(dirigeant9.personneLieeQualite).eq('66')) 
or (dirigeant10.personneLieeQualite != null and not Value('id').of(dirigeant10.personneLieeQualite).eq('66')) 
or (dirigeant11.personneLieeQualite != null and not Value('id').of(dirigeant11.personneLieeQualite).eq('66')) 
or (dirigeant12.personneLieeQualite != null and not Value('id').of(dirigeant12.personneLieeQualite).eq('66')))) {
if (((Value('id').of(identite.entrepriseFormeJuridique).eq('5306') or Value('id').of(identite.entrepriseFormeJuridique).eq('5307')
or Value('id').of(identite.entrepriseFormeJuridique).eq('5202') or Value('id').of(identite.entrepriseFormeJuridique).eq('5203')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6589') or Value('id').of(identite.entrepriseFormeJuridique).eq('6521')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6532') or Value('id').of(identite.entrepriseFormeJuridique).eq('6539')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6540') or Value('id').of(identite.entrepriseFormeJuridique).eq('6541')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6542') or Value('id').of(identite.entrepriseFormeJuridique).eq('6599')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6543') or Value('id').of(identite.entrepriseFormeJuridique).eq('6544')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6554') or Value('id').of(identite.entrepriseFormeJuridique).eq('6558')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6560') or Value('id').of(identite.entrepriseFormeJuridique).eq('6561')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6562') or Value('id').of(identite.entrepriseFormeJuridique).eq('6563')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6564') or Value('id').of(identite.entrepriseFormeJuridique).eq('6565')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6566') or Value('id').of(identite.entrepriseFormeJuridique).eq('6567')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6568') or Value('id').of(identite.entrepriseFormeJuridique).eq('6569')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6571') or Value('id').of(identite.entrepriseFormeJuridique).eq('6572')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6573') or Value('id').of(identite.entrepriseFormeJuridique).eq('6574')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6575') or Value('id').of(identite.entrepriseFormeJuridique).eq('6576')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6577') or Value('id').of(identite.entrepriseFormeJuridique).eq('6578')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6585') or Value('id').of(identite.entrepriseFormeJuridique).eq('6534')
or Value('id').of(identite.entrepriseFormeJuridique).eq('6536') or Value('id').of(identite.entrepriseFormeJuridique).eq('6316'))
and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M'))
or ((Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5306') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5307')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5202') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5203')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6589') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6521')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6532') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6539')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6540') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6541')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6542') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6599')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6543') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6544')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6554') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6558')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6560') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6561')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6562') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6563')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6564') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6565')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6566') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6567')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6568') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6569')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6571') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6572')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6573') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6574')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6575') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6576')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6577') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6578')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6585') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6534')
or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6536') or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('6316'))
and Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M'))) {
occurrence['C10.1'] = '34M';
} else { occurrence['C10.1'] = '35M'; }
if (Value('id').of(objet.objetModification).contains('dirigeant') and dirigeantSARL1.dateModifDirigeant != null) {
	var dateTemp = new Date(parseInt(dirigeantSARL1.dateModifDirigeant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if (Value('id').of(objet.objetModification).contains('dirigeant') and dirigeant1.dateModifDirigeant != null) {
	var dateTemp = new Date(parseInt(dirigeant1.dateModifDirigeant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if (modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
	var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {}; 
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('37M')) {
occurrence['C10.1'] = '37M';
if (modifContratAppui.modifDateAppui != null) {
	var dateTemp = new Date(parseInt(modifContratAppui.modifDateAppui.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {}; 
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('40M') or dissolution.dissoMiseEnSommeil) {
occurrence['C10.1'] = '40M';
if(fermeture.modifDateMiseEnSommeil != null) {
	var dateTemp = new Date(parseInt(fermeture.modifDateMiseEnSommeil.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {};
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
and objet.cadreObjetModificationEtablissement.lieuActiviteSiege) {
occurrence['C10.1'] = '51M';
if(ouverture.modifDatePriseActiviteEtablissement != null) {
	var dateTemp = new Date(parseInt(ouverture.modifDatePriseActiviteEtablissement.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if(activiteInfo.modifDatePriseActiviteSiege != null) {
	var dateTemp = new Date(parseInt(activiteInfo.modifDatePriseActiviteSiege.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {};
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
and not objet.cadreObjetModificationEtablissement.lieuActiviteSiege
and Value('id').of(objet.cadreObjetModificationEtablissement.priseRepriseActivite).eq('prise')) {
occurrence['C10.1'] = '52M';
if(ouverture.modifDatePriseActiviteEtablissement != null) {
	var dateTemp = new Date(parseInt(ouverture.modifDatePriseActiviteEtablissement.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {};
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M') 
	or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsreprise')) {
occurrence['C10.1'] = '53M';
if(Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M') and ouverture.modifDateRepriseExploitation != null) {
	var dateTemp = new Date(parseInt(ouverture.modifDateRepriseExploitation.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if(Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsreprise') and fermeture.modifDateTransfert != null) {
	var dateTemp = new Date(parseInt(fermeture.modifDateTransfert.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {};
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(ouverture.etablissementDejaConnu).eq('non')
	and not Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsreprise'))
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
	and not objet.cadreObjetModificationEtablissement.lieuActiviteSiege
	and Value('id').of(objet.cadreObjetModificationEtablissement.priseRepriseActivite).eq('reprise'))) {
occurrence['C10.1'] = '54M';
if(Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M') and ouverture.modifDateAdresseOuverture != null) {
	var dateTemp = new Date(parseInt(ouverture.modifDateAdresseOuverture.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if(Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and fermeture.modifDateTransfert != null) {
	var dateTemp = new Date(parseInt(fermeture.modifDateTransfert.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite') and ouverture.modifDatePriseActiviteEtablissement != null) {
	var dateTemp = new Date(parseInt(ouverture.modifDatePriseActiviteEtablissement.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {};
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60M') 
	or activiteInfo.modifEtablissementEnseigne) {
occurrence['C10.1'] = '60M';
if(activiteInfo.cadreEtablissementIdentification.modifDateIdentification != null) {
	var dateTemp = new Date(parseInt(activiteInfo.cadreEtablissementIdentification.modifDateIdentification.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {};
}
if (Value('id').of(activite.activiteEvenement).eq('61M') or Value('id').of(activite.activiteEvenement).eq('61M62M')
	or activiteInfo.modifRepriseExploitation) {
occurrence['C10.1'] = '61M';
if (activite.modifDateActivite != null) {
	var dateTemp = new Date(parseInt(activite.modifDateActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if (activiteInfo.modifRepriseExploitation and ouverture.modifDateRepriseExploitation != null) {
	var dateTemp = new Date(parseInt(ouverture.modifDateRepriseExploitation.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
}  
occurrencesC10.push(occurrence);
occurrence = {};
}
if ((Value('id').of(activite.activiteEvenement).eq('62M') or Value('id').of(activite.activiteEvenement).eq('61M62M'))) {
occurrence['C10.1'] = '62M';
if (activite.modifDateActivite != null) {
	var dateTemp = new Date(parseInt(activite.modifDateActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {}; 
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63M')) {
occurrence['C10.1'] = '63M';
if (activiteInfo.modifDateAcquisition != null) {
	var dateTemp = new Date(parseInt(activiteInfo.modifDateAcquisition.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {}; 
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64M')) {
occurrence['C10.1'] = '64M';
if (activiteInfo.modifDateRenouvellement != null) {
	var dateTemp = new Date(parseInt(activiteInfo.modifDateRenouvellement.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {}; 
}
if (Value('id').of(activite.activiteEvenement).eq('67M')) {
occurrence['C10.1'] = '67M';
if (activite.modifDateActivite != null) {
	var dateTemp = new Date(parseInt(activite.modifDateActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[11]/C10.2']= date;
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {}; 
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68M')) {
occurrence['C10.1'] = '68M';
if (fondsDonne.dateMiseEnLocation != null) {
	var dateTemp = new Date(parseInt(fondsDonne.dateMiseEnLocation.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {}; 
}
if ((fondePouvoir70M.length > 0) or (pouvoir70M.length > 0)) { 
occurrence['C10.1'] = '70M';
if (ouverture.ajoutFondePouvoir and ouverture.modifDateAdresseOuverture != null) {
	var dateTemp = new Date(parseInt(ouverture.modifDateAdresseOuverture.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if (ouverture.ajoutFondePouvoir and fermeture.modifDateTransfert != null) {
	var dateTemp = new Date(parseInt(fermeture.modifDateTransfert.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if (ouverture.ajoutFondePouvoir and ouverture.modifDatePriseActiviteEtablissement != null) {
	var dateTemp = new Date(parseInt(ouverture.modifDatePriseActiviteEtablissement.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if (ouverture.ajoutFondePouvoir and ouverture.modifDateRepriseExploitation != null) {
	var dateTemp = new Date(parseInt(ouverture.modifDateRepriseExploitation.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if (not ouverture.ajoutFondePouvoir and (fondePouvoir70M.length > 0) and fondePouvoir70M[0].cadreIdentiteDirigeant.dateModifDirigeant != null) {
	var dateTemp = new Date(parseInt(fondePouvoir70M[0].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if (not ouverture.ajoutFondePouvoir and (pouvoir70M.length > 0) and pouvoir70M[0].cadreIdentiteDirigeant.dateModifDirigeant != null) {
	var dateTemp = new Date(parseInt(pouvoir70M[0].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {}; 
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80M') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and not Value('id').of(fermeture.destinationEtablissement3).eq('devientPrincipal') 
	and not Value('id').of(fermeture.destinationEtablissement3).eq('miseEnLocationPartielle')
	and not Value('id').of(fermeture.destinationEtablissement3).eq('miseEnLocationTotale')
	and not Value('id').of(fermeture.destinationEtablissement4).eq('devientSecondaire')
	and not Value('id').of(fermeture.destinationEtablissement5).eq('devientSiege')
	and not Value('id').of(fermeture.destinationEtablissement5).eq('devientSecondaire')
	and not Value('id').of(fermeture.destinationEtablissement6).eq('devientSecondaire')
	and not Value('id').of(fermeture.destinationEtablissement7).eq('miseEnLocationPartielle'))) {
occurrence['C10.1'] = '80M';
if(Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80M') and fermeture.modifDateFermeture != null) {
	var dateTemp = new Date(parseInt(fermeture.modifDateFermeture.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and fermeture.modifDateTransfert != null) {
	var dateTemp = new Date(parseInt(fermeture.modifDateTransfert.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {};
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('81M')
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(fermeture.destinationEtablissement5).eq('devientSiege'))) {
occurrence['C10.1'] = '81M';
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('81M') and fermeture.modifDateFinActiviteSiege != null) {
	var dateTemp = new Date(parseInt(fermeture.modifDateFinActiviteSiege.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and fermeture.modifDateTransfert != null) {
	var dateTemp = new Date(parseInt(fermeture.modifDateTransfert.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {};
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('82M') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and (Value('id').of(fermeture.destinationEtablissement3).eq('miseEnLocationPartielle') 
	or Value('id').of(fermeture.destinationEtablissement7).eq('miseEnLocationPartielle')))) {
occurrence['C10.1'] = '82M';
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('82M') and fondsDonne.dateMiseEnLocation != null) {
	var dateTemp = new Date(parseInt(fondsDonne.dateMiseEnLocation.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and fermeture.modifDateTransfert != null) {
	var dateTemp = new Date(parseInt(fermeture.modifDateTransfert.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {};
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('84M') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(fermeture.destinationEtablissement3).eq('miseEnLocationTotale'))) {
occurrence['C10.1'] = '84M';
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('84M') and fondsDonne.dateMiseEnLocation != null) {
	var dateTemp = new Date(parseInt(fondsDonne.dateMiseEnLocation.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and fermeture.modifDateTransfert != null) {
	var dateTemp = new Date(parseInt(fermeture.modifDateTransfert.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {};
}

occurrencesC10.forEach(function (value, i) {
	var idx = i+1;
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[' + idx + ']/C10.1']= value['C10.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[' + idx + ']/C10.2']= value['C10.2'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[' + idx + ']/C10.3']= value['C10.3'];	
});

// Sous groupe DMF : Destinataire de la formalité
   
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[1]']= authorityId;
if (authorityId != "" and authorityId2 != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[2]']= authorityId2;
}

// Sous groupe ADF :  adresse de correspondance

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C36']= correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance != null ?
																	correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance : 
																	(modifDeno.newDenomination != null ? modifDeno.newDenomination : identite.entrepriseDenomination);
																	
if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']= correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune.getId();
if (correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.5']= correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance;
}
if (correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance != null) {
   var monId1 = Value('id').of(correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId1;  
}
if (correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.7']= correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8']= correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal;
if (correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.10']= correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance;
}	
if (correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance != null) {
   var monId1 = Value('id').of(correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId1;  
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12']= correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune.getLabel();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('siege')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']= identite.adresseSiege.siegeAdresseCommune.getId();
if (identite.adresseSiege.siegeAdresseNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.5']= identite.adresseSiege.siegeAdresseNumeroVoie;
}
if (identite.adresseSiege.siegeAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(identite.adresseSiege.siegeAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId1;
}
if (identite.adresseSiege.siegeAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.7']= identite.adresseSiege.siegeAdresseDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8']= identite.adresseSiege.siegeAdresseCodePostal;
if (identite.adresseSiege.siegeAdresseComplementVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.10']= identite.adresseSiege.siegeAdresseComplementVoie;
}
if (identite.adresseSiege.siegeAdresseTypeVoie != null) {
   var monId1 = Value('id').of(identite.adresseSiege.siegeAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12']= identite.adresseSiege.siegeAdresseNomVoie;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= identite.adresseSiege.siegeAdresseCommune.getLabel();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']= fermeture.cadreAdresseEtablissementFerme.communeAdresseEtablissement.getId();
if (fermeture.cadreAdresseEtablissementFerme.numeroAdresseEtablissement != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.5']= fermeture.cadreAdresseEtablissementFerme.numeroAdresseEtablissement;
}
if (fermeture.cadreAdresseEtablissementFerme.indiceAdresseEtablissement != null) {
   var monId1 = Value('id').of(fermeture.cadreAdresseEtablissementFerme.indiceAdresseEtablissement)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId1;
}
if (fermeture.cadreAdresseEtablissementFerme.distributionSpecialeAdresseEtablissement != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.7']= fermeture.cadreAdresseEtablissementFerme.distributionSpecialeAdresseEtablissement;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8']= fermeture.cadreAdresseEtablissementFerme.codePostalAdresseEtablissement;
if (fermeture.cadreAdresseEtablissementFerme.complementAdresseEtablissement != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.10']= fermeture.cadreAdresseEtablissementFerme.complementAdresseEtablissement;
}
if (fermeture.cadreAdresseEtablissementFerme.typeAdresseEtablissement != null) {
   var monId1 = Value('id').of(fermeture.cadreAdresseEtablissementFerme.typeAdresseEtablissement)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12']= fermeture.cadreAdresseEtablissementFerme.voieAdresseEtablissement;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= fermeture.cadreAdresseEtablissementFerme.communeAdresseEtablissement.getLabel();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']= ouverture.cadreAdresseEtablissementOuvert.communeAdresseEtablissement.getId();
if (ouverture.cadreAdresseEtablissementOuvert.numeroAdresseEtablissement != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.5']= ouverture.cadreAdresseEtablissementOuvert.numeroAdresseEtablissement;
}
if (ouverture.cadreAdresseEtablissementOuvert.indiceAdresseEtablissement != null) {
   var monId1 = Value('id').of(ouverture.cadreAdresseEtablissementOuvert.indiceAdresseEtablissement)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId1;
}
if (ouverture.cadreAdresseEtablissementOuvert.distributionSpecialeAdresseEtablissement != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.7']= ouverture.cadreAdresseEtablissementOuvert.distributionSpecialeAdresseEtablissement;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8']= ouverture.cadreAdresseEtablissementOuvert.codePostalAdresseEtablissement;
if (ouverture.cadreAdresseEtablissementOuvert.complementAdresseEtablissement != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.10']= ouverture.cadreAdresseEtablissementOuvert.complementAdresseEtablissement;
}
if (ouverture.cadreAdresseEtablissementOuvert.typeAdresseEtablissement != null) {
   var monId1 = Value('id').of(ouverture.cadreAdresseEtablissementOuvert.typeAdresseEtablissement)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12']= ouverture.cadreAdresseEtablissementOuvert.voieAdresseEtablissement;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= ouverture.cadreAdresseEtablissementOuvert.communeAdresseEtablissement.getLabel();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('liquidation')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']= dissolution.adresseLiquidation.communeAdresseLiquidation.getId();
if (dissolution.adresseLiquidation.numeroVoieAdresseLiquidation != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.5']= dissolution.adresseLiquidation.numeroVoieAdresseLiquidation;
}
if (dissolution.adresseLiquidation.indiceVoieAdresseLiquidation != null) {
	var monId1 = Value('id').of(dissolution.adresseLiquidation.indiceVoieAdresseLiquidation)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId1;
}
if (dissolution.adresseLiquidation.voieDistributionSpecialeAdresseLiquidation != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.7']= dissolution.adresseLiquidation.voieDistributionSpecialeAdresseLiquidation;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8']= dissolution.adresseLiquidation.codePostalAdresseLiquidation;
if (dissolution.adresseLiquidation.voieComplementAdresseLiquidation != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.10']= dissolution.adresseLiquidation.voieComplementAdresseLiquidation;
}
if (dissolution.adresseLiquidation.typeVoieAdresseLiquidation != null) {
	var monId1 = Value('id').of(dissolution.adresseLiquidation.typeVoieAdresseLiquidation)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12']= dissolution.adresseLiquidation.nomVoieAdresseLiquidation;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= dissolution.adresseLiquidation.communeAdresseLiquidation.getLabel();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('fondsDonneEtablissement')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']= fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseCommune.getId();
if (fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.5']= fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseNumeroVoie;
}
if (fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseIndiceVoie != null) {
	var monId1 = Value('id').of(fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId1;
}
if (fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.7']= fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8']= fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseCodePostal;
if (fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseComplementVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.10']= fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseComplementVoie;
}
if (fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseTypeVoie != null) {
	var monId1 = Value('id').of(fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12']= fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseNomVoie;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseCommune.getLabel();
} 

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.1[1]']= correspondance.infosSup.formaliteTelephone2.e164.replace('+', '00');

if (correspondance.infosSup.formaliteTelephone1 != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.1[2]']= correspondance.infosSup.formaliteTelephone1.e164.replace('+', '00');
}

if (correspondance.infosSup.telecopie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.2']= correspondance.infosSup.telecopie.e164.replace('+', '00');
}

if (correspondance.infosSup.formaliteFaxCourriel != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.3']= correspondance.infosSup.formaliteFaxCourriel;
}


// Sous groupe SIF : Signature de la formalité

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.1']= signataire.adresseMandataire.nomPrenomDenominationMandataire;

if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') or Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteAutre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.2']= Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? "Mandataire" : "Autre personne";
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.3']= signataire.adresseMandataire.villeAdresseMandataire.getId();
if (signataire.adresseMandataire.numeroVoieMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.5']= signataire.adresseMandataire.numeroVoieMandataire;
}
if (signataire.adresseMandataire.indiceVoieMandataire != null) {
   var monId = Value('id').of(signataire.adresseMandataire.indiceVoieMandataire)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.6']          = monId;
}
if (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.7']= signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.8']= signataire.adresseMandataire.dataCodePostalMandataire;
if (signataire.adresseMandataire.complementVoieMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.10']= signataire.adresseMandataire.complementVoieMandataire;
}
if (signataire.adresseMandataire.typeVoieMandataire != null) {
   var monId = Value('id').of(signataire.adresseMandataire.typeVoieMandataire)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.11']          = monId;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.12']= signataire.adresseMandataire.nomVoieMandataire;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.13']= signataire.adresseMandataire.villeAdresseMandataire.getLabel();

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C41']= signataire.formaliteSignatureLieu;

if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C42']= date;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('29M')	and objet.cadreModifDateActivite.newDateDebutActivite != null) {
	var dateTemp = new Date(parseInt(objet.cadreModifDateActivite.newDateDebutActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
var dateDebutActivite = date;
} 

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C43']=  
(jqpa3.jqpaSituation != null ? "JQPA=3!" : (jqpa2.jqpaSituation != null ? "JQPA=2!" : 
(jqpa1.jqpaSituation != null ? "JQPA=1!" : '')))
+ '' + (identite.nomDomainePresence ? "M05=O!" : '') 
+ '' + (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('29M') ? ("M20=" +''+ dateDebutActivite +''+ "!") : '') 
+ '' + (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('18M') ? (Value('id').of(modifEss.modifEssAdhesionSortie).eq('modifEssAdhesion') ? "M22=O!" : "M22=N!") : '') 
+ '' + ((activiteInfo.cadreEtablissementNomDomaine.modifDateIdentification !=  null or activite.etablissementNomDomaine != null) ? "E06=O!" : '') 
+ ' ' + (correspondance.formaliteObservations != null ? correspondance.formaliteObservations : '') ;


// Groupe ICM : Identification complète de la personne morale

if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('10M') and Value('id').of(modifDeno.modifDenoSigle).contains('modifDeno')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/ICM/M01/M01.1']= modifDeno.newDenomination;
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/ICM/M01/M01.1']= identite.entrepriseDenomination;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('10M') and Value('id').of(modifDeno.modifDenoSigle).contains('modifSigle') and modifDeno.newSigle != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/ICM/M01/M01.2']= modifDeno.newSigle;
} else if (identite.entrepriseSigle != null and not Value('id').of(modifDeno.modifDenoSigle).contains('modifSigle')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/ICM/M01/M01.2']= identite.entrepriseSigle;
}

// Groupe AIM : Ancienne identification de la personne morale

if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('10M')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/AIM/M11/M11.1']= identite.entrepriseDenomination;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('10M') and Value('id').of(modifDeno.modifDenoSigle).contains('modifSigle') and identite.entrepriseSigle != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/AIM/M11/M11.2']= identite.entrepriseSigle;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/AIM/M12']= identite.entrepriseFormeJuridique.getId();
}

// Groupe FJM : Forme juridique de la personne morale

if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FJM/M21/M21.1']= modifFormeJuridique.newFormeJuridique.getId();
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FJM/M21/M21.1']= identite.entrepriseFormeJuridique.getId();
}

if ((Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and (modifFormeJuridique.entrepriseReduiteAssocieUnique1 or Value('id').of(modifFormeJuridique.newFormeJuridique).eq('5720'))) or (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and (Value('id').of(identite.entrepriseFormeJuridique).eq('5720') or identite.entrepriseAssocieUnique))) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FJM/M23']= "O";
}

// Groupe CSM : Caractéritiques de la personne morale

/*
if (Value('id').of(modifCapital.modifCapitalType).contains('15M')  
	or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('fusion')
	or ((not objet.immatriculationRM
	and ((activite.nouvelleActiviteGroup1.domaineAdjonctionNAFA != null
	and not Value('id').of(activite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup1.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	or (activite.nouvelleActiviteGroup2.domaineAdjonctionNAFA != null
	and not Value('id').of(activite.nouvelleActiviteGroup2.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup2.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup2.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup2.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup2.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	or (activite.nouvelleActiviteGroup3.domaineAdjonctionNAFA != null
	and not Value('id').of(activite.nouvelleActiviteGroup3.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup3.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup3.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup3.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of(activite.nouvelleActiviteGroup3.jqpaDomaineTransport).eq('jqpaTransportAutre')))
	and not activite.optionCMACCI)
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege')
	or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal')) 
	and not objet.transfertDepartement))) {
	}
*/

if (Value('id').of(modifCapital.modifCapitalType).contains('15M') or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('fusion')) {
if (modifCapital.newCapitalVariable or fusion.entrepriseCapitalVariable)	{
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M24/M24.1']= "2";
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M24/M24.2']= modifCapital.newCapitalMontant != null ? (modifCapital.newCapitalMontant +''+ "00") : (fusion.entrepriseCapitalMontant +''+ "00");
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M24/M24.3']= "eur";
if (modifCapital.newCapitalVariable or fusion.entrepriseCapitalVariable)	{
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M24/M24.4']= modifCapital.newCapitalVariableMinimum != null ? (modifCapital.newCapitalVariableMinimum +''+ "00") : (fusion.entrepriseCapitalVariableMinimum +''+ "00");
}
} 
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('16M') and Value('id').of(modifDuree.modifDureeExerciceSocial).contains('modifDuree')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M25']= modifDuree.newDuree;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('16M') and Value('id').of(modifDuree.modifDureeExerciceSocial).contains('modifExerciceSocial')) {
	var dateTemp = new Date(parseInt(modifDuree.newExerciceSocial.getTimeInMillis()));
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = "--" + month + "-" + day ; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/CSM/M26']= date;
}

// Groupe NGM : Nature de la gérance de la personne morale

if ((gerants.length > 0) and not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M')) {
if (Value('id').of(dirigeantSARL1.cadreGerance.typeGerance).eq('entrepriseNatureGeranceMajoritaire') or Value('id').of(dirigeantSARL1.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire')
or Value('id').of(dirigeantSARL2.cadreGerance.typeGerance).eq('entrepriseNatureGeranceMajoritaire') or Value('id').of(dirigeantSARL2.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire')
or Value('id').of(dirigeantSARL3.cadreGerance.typeGerance).eq('entrepriseNatureGeranceMajoritaire') or Value('id').of(dirigeantSARL3.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire')
or Value('id').of(dirigeantSARL4.cadreGerance.typeGerance).eq('entrepriseNatureGeranceMajoritaire') or Value('id').of(dirigeantSARL4.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire')
or Value('id').of(dirigeantSARL5.cadreGerance.typeGerance).eq('entrepriseNatureGeranceMajoritaire') or Value('id').of(dirigeantSARL5.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMajoritaire')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/NGM/M27']= "1";
} else if (Value('id').of(dirigeantSARL1.cadreGerance.typeGerance).eq('entrepriseNatureGeranceMinoritaireEgalitaire') or Value('id').of(dirigeantSARL1.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')
or Value('id').of(dirigeantSARL2.cadreGerance.typeGerance).eq('entrepriseNatureGeranceMinoritaireEgalitaire') or Value('id').of(dirigeantSARL2.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')
or Value('id').of(dirigeantSARL3.cadreGerance.typeGerance).eq('entrepriseNatureGeranceMinoritaireEgalitaire') or Value('id').of(dirigeantSARL3.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')
or Value('id').of(dirigeantSARL4.cadreGerance.typeGerance).eq('entrepriseNatureGeranceMinoritaireEgalitaire') or Value('id').of(dirigeantSARL4.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')
or Value('id').of(dirigeantSARL5.cadreGerance.typeGerance).eq('entrepriseNatureGeranceMinoritaireEgalitaire') or Value('id').of(dirigeantSARL5.cadreGerance.typeGeranceBis).eq('entrepriseNatureGeranceMinoritaireEgalitaire')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/NGM/M27']= (dirigeantSARL1.cadreGerance.entrepriseNatureGeranceSocieteAssocieeOui or dirigeantSARL2.cadreGerance.entrepriseNatureGeranceSocieteAssocieeOui or dirigeantSARL3.cadreGerance.entrepriseNatureGeranceSocieteAssocieeOui or dirigeantSARL4.cadreGerance.entrepriseNatureGeranceSocieteAssocieeOui or dirigeantSARL5.cadreGerance.entrepriseNatureGeranceSocieteAssocieeOui) ? "3" : "4";
}
}

// Groupe DPM : Dissolution de la personne morale

if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/DPM/M52/M52.1']= dissolution.dissolutionJournalAnnoncesLegalesNom;
if(dissolution.dissolutionJournalAnnoncesLegalesDateParution != null) {
var dateTemp = new Date(parseInt(dissolution.dissolutionJournalAnnoncesLegalesDateParution.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/DPM/M52/M52.2']= date;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/DPM/M57/M57.1']= Value('id').of(dissolution.adresseLiquidationLieu).eq('siege') ? "S" : (Value('id').of(dissolution.adresseLiquidationLieu).eq('liquidateur') ? "L" : "A");
if (Value('id').of(dissolution.adresseLiquidationLieu).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/DPM/M57/M57.2/M57.2.3']= dissolution.adresseLiquidation.communeAdresseLiquidation.getId();
if (dissolution.adresseLiquidation.numeroVoieAdresseLiquidation != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/DPM/M57/M57.2/M57.2.5']= dissolution.adresseLiquidation.numeroVoieAdresseLiquidation;
}
if (dissolution.adresseLiquidation.indiceVoieAdresseLiquidation != null) {
   var monId = Value('id').of(dissolution.adresseLiquidation.indiceVoieAdresseLiquidation)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/DPM/M57/M57.2/M57.2.6']          = monId;
}
if (dissolution.adresseLiquidation.voieDistributionSpecialeAdresseLiquidation != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/DPM/M57/M57.2/M57.2.7']= dissolution.adresseLiquidation.voieDistributionSpecialeAdresseLiquidation;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/DPM/M57/M57.2/M57.2.8']= dissolution.adresseLiquidation.codePostalAdresseLiquidation;
if (dissolution.adresseLiquidation.voieComplementAdresseLiquidation != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/DPM/M57/M57.2/M57.2.10']= dissolution.adresseLiquidation.voieComplementAdresseLiquidation;
}
if (dissolution.adresseLiquidation.typeVoieAdresseLiquidation != null) {
   var monId = Value('id').of(dissolution.adresseLiquidation.typeVoieAdresseLiquidation)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/DPM/M57/M57.2/M57.2.11']          = monId;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/DPM/M57/M57.2/M57.2.12']= dissolution.adresseLiquidation.nomVoieAdresseLiquidation;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/DPM/M57/M57.2/M57.2.13']= dissolution.adresseLiquidation.communeAdresseLiquidation.getLabel();
}
}

// Groupe FSM : Fusion / Scission de la personne morale

if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('fusion')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M54']= Value('id').of(fusion.fusionScission).eq('fusion') ? "1" : "2";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.1']= fusion.fusionGroup1.entrepriseLieeEntreprisePMDenomination;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.2']= fusion.fusionGroup1.entreperiseLieeEntreprisePMFormeJuridique.getId();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.3']= fusion.fusionGroup1.adresseFusionScission.communeAdresseFusion.getId();
if(fusion.fusionGroup1.adresseFusionScission.numeroAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.5']= fusion.fusionGroup1.adresseFusionScission.numeroAdresseFusion;
}
if (fusion.fusionGroup1.adresseFusionScission.indiceAdresseFusion !== null) {
   var monId = Value('id').of(fusion.fusionGroup1.adresseFusionScission.indiceAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.6']          = monId;
}
if(fusion.fusionGroup1.adresseFusionScission.distributionSpecialeAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.7']= fusion.fusionGroup1.adresseFusionScission.distributionSpecialeAdresseFusion;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.8']= fusion.fusionGroup1.adresseFusionScission.codePostalAdresseFusion;
if(fusion.fusionGroup1.adresseFusionScission.complementAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.10']= fusion.fusionGroup1.adresseFusionScission.complementAdresseFusion;
}
if (fusion.fusionGroup1.adresseFusionScission.typeAdresseFusion !== null) {
   var monId = Value('id').of(fusion.fusionGroup1.adresseFusionScission.typeAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.11']          = monId;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.12']= fusion.fusionGroup1.adresseFusionScission.voieAdresseFusion;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.3/M55.3.13']= fusion.fusionGroup1.adresseFusionScission.communeAdresseFusion.getLabel();

//Récupérer le EntityId du greffe
var greffeId = fusion.fusionGroup1.entrepriseLieeGreffe.id ;
//Appeler Directory pour récupérer l'autorité
_log.info('l entityId du greffe selectionne est {}', greffeId);
var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', greffeId) //
		   .connectionTimeout(10000) //
		   .receiveTimeout(10000) //
		   .accept('json') //
		   .get();
//result
var receiverInfo = response.asObject();
var contactInfo = !receiverInfo.details ? null : receiverInfo.details;
//Récupérer le code EDI
var codeEDIGreffe = !contactInfo.ediCode ? null : contactInfo.ediCode;
_log.info('le code edi du greffe selectionne est {}', codeEDIGreffe);
// À modifier quand Destiny fourni le réferentiel
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.4']=codeEDIGreffe;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[1]/M55.6']= fusion.fusionGroup1.entrepriseLieeSiren.split(' ').join('');
if (fusion.fusionGroup1.fusionAutreSociete) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.1']= fusion.fusionGroup2.entrepriseLieeEntreprisePMDenomination;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.2']= fusion.fusionGroup2.entreperiseLieeEntreprisePMFormeJuridique.getId();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.3']= fusion.fusionGroup2.adresseFusionScission.communeAdresseFusion.getId();
if(fusion.fusionGroup2.adresseFusionScission.numeroAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.5']= fusion.fusionGroup2.adresseFusionScission.numeroAdresseFusion;
}
if (fusion.fusionGroup2.adresseFusionScission.indiceAdresseFusion !== null) {
   var monId = Value('id').of(fusion.fusionGroup2.adresseFusionScission.indiceAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.6']          = monId;
}
if(fusion.fusionGroup2.adresseFusionScission.distributionSpecialeAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.7']= fusion.fusionGroup2.adresseFusionScission.distributionSpecialeAdresseFusion;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.8']= fusion.fusionGroup2.adresseFusionScission.codePostalAdresseFusion;
if(fusion.fusionGroup2.adresseFusionScission.complementAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.10']= fusion.fusionGroup2.adresseFusionScission.complementAdresseFusion;
}
if (fusion.fusionGroup2.adresseFusionScission.typeAdresseFusion !== null) {
   var monId = Value('id').of(fusion.fusionGroup2.adresseFusionScission.typeAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.11']          = monId;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.12']= fusion.fusionGroup2.adresseFusionScission.voieAdresseFusion;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.3/M55.3.13']= fusion.fusionGroup2.adresseFusionScission.communeAdresseFusion.getLabel();

//Récupérer le EntityId du greffe
var greffeId = fusion.fusionGroup2.entrepriseLieeGreffe.id ;
//Appeler Directory pour récupérer l'autorité
_log.info('l entityId du greffe selectionne est {}', greffeId);
var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', greffeId) //
		   .connectionTimeout(10000) //
		   .receiveTimeout(10000) //
		   .accept('json') //
		   .get();
//result
var receiverInfo = response.asObject();
var contactInfo = !receiverInfo.details ? null : receiverInfo.details;
//Récupérer le code EDI
var codeEDIGreffe = !contactInfo.ediCode ? null : contactInfo.ediCode;
_log.info('le code edi du greffe selectionne est {}', codeEDIGreffe);
// À modifier quand Destiny fourni le réferentiel
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.4']= codeEDIGreffe;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[2]/M55.6']= fusion.fusionGroup2.entrepriseLieeSiren.split(' ').join('');
}
if (fusion.fusionGroup2.fusionAutreSociete2) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.1']= fusion.fusionGroup3.entrepriseLieeEntreprisePMDenomination;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.2']= fusion.fusionGroup3.entreperiseLieeEntreprisePMFormeJuridique.getId();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.3']= fusion.fusionGroup3.adresseFusionScission.communeAdresseFusion.getId();
if(fusion.fusionGroup3.adresseFusionScission.numeroAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.5']= fusion.fusionGroup3.adresseFusionScission.numeroAdresseFusion;
}
if (fusion.fusionGroup3.adresseFusionScission.indiceAdresseFusion !== null) {
   var monId = Value('id').of(fusion.fusionGroup3.adresseFusionScission.indiceAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.6']          = monId;
}
if(fusion.fusionGroup3.adresseFusionScission.distributionSpecialeAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.7']= fusion.fusionGroup3.adresseFusionScission.distributionSpecialeAdresseFusion;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.8']= fusion.fusionGroup3.adresseFusionScission.codePostalAdresseFusion;
if(fusion.fusionGroup3.adresseFusionScission.complementAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.10']= fusion.fusionGroup3.adresseFusionScission.complementAdresseFusion;
}
if (fusion.fusionGroup3.adresseFusionScission.typeAdresseFusion !== null) {
   var monId = Value('id').of(fusion.fusionGroup3.adresseFusionScission.typeAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.11']          = monId;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.12']= fusion.fusionGroup3.adresseFusionScission.voieAdresseFusion;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.3/M55.3.13']= fusion.fusionGroup3.adresseFusionScission.communeAdresseFusion.getLabel();

//Récupérer le EntityId du greffe
var greffeId = fusion.fusionGroup3.entrepriseLieeGreffe.id ;
//Appeler Directory pour récupérer l'autorité
_log.info('l entityId du greffe selectionne est {}', greffeId);
var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', greffeId) //
		   .connectionTimeout(10000) //
		   .receiveTimeout(10000) //
		   .accept('json') //
		   .get();
//result
var receiverInfo = response.asObject();
var contactInfo = !receiverInfo.details ? null : receiverInfo.details;
//Récupérer le code EDI
var codeEDIGreffe = !contactInfo.ediCode ? null : contactInfo.ediCode;
_log.info('le code edi du greffe selectionne est {}', codeEDIGreffe);
// À modifier quand Destiny fourni le réferentiel
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.4']= codeEDIGreffe;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[3]/M55.6']= fusion.fusionGroup3.entrepriseLieeSiren.split(' ').join('');
}
if (fusion.fusionGroup3.fusionAutreSociete3) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[4]/M55.1']= fusion.fusionGroup4.entrepriseLieeEntreprisePMDenomination;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[4]/M55.2']= fusion.fusionGroup4.entreperiseLieeEntreprisePMFormeJuridique.getId();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[4]/M55.3/M55.3.3']= fusion.fusionGroup4.adresseFusionScission.communeAdresseFusion.getId();
if(fusion.fusionGroup4.adresseFusionScission.numeroAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[4]/M55.3/M55.3.5']= fusion.fusionGroup4.adresseFusionScission.numeroAdresseFusion;
}
if (fusion.fusionGroup4.adresseFusionScission.indiceAdresseFusion !== null) {
   var monId = Value('id').of(fusion.fusionGroup4.adresseFusionScission.indiceAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[4]/M55.3/M55.3.6']          = monId;
}
if(fusion.fusionGroup4.adresseFusionScission.distributionSpecialeAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[4]/M55.3/M55.3.7']= fusion.fusionGroup4.adresseFusionScission.distributionSpecialeAdresseFusion;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[4]/M55.3/M55.3.8']= fusion.fusionGroup4.adresseFusionScission.codePostalAdresseFusion;
if(fusion.fusionGroup4.adresseFusionScission.complementAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[4]/M55.3/M55.3.10']= fusion.fusionGroup4.adresseFusionScission.complementAdresseFusion;
}
if (fusion.fusionGroup4.adresseFusionScission.typeAdresseFusion !== null) {
   var monId = Value('id').of(fusion.fusionGroup4.adresseFusionScission.typeAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[4]/M55.3/M55.3.11']          = monId;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[4]/M55.3/M55.3.12']= fusion.fusionGroup4.adresseFusionScission.voieAdresseFusion;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[4]/M55.3/M55.3.13']= fusion.fusionGroup4.adresseFusionScission.communeAdresseFusion.getLabel();

//Récupérer le EntityId du greffe
var greffeId = fusion.fusionGroup4.entrepriseLieeGreffe.id ;
//Appeler Directory pour récupérer l'autorité
_log.info('l entityId du greffe selectionne est {}', greffeId);
var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', greffeId) //
		   .connectionTimeout(10000) //
		   .receiveTimeout(10000) //
		   .accept('json') //
		   .get();
//result
var receiverInfo = response.asObject();
var contactInfo = !receiverInfo.details ? null : receiverInfo.details;
//Récupérer le code EDI
var codeEDIGreffe = !contactInfo.ediCode ? null : contactInfo.ediCode;
_log.info('le code edi du greffe selectionne est {}', codeEDIGreffe);
// À modifier quand Destiny fourni le réferentiel
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[4]/M55.4']= codeEDIGreffe;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[4]/M55.6']= fusion.fusionGroup4.entrepriseLieeSiren.split(' ').join('');
}
if (fusion.fusionGroup4.fusionAutreSociete4) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[5]/M55.1']= fusion.fusionGroup5.entrepriseLieeEntreprisePMDenomination;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[5]/M55.2']= fusion.fusionGroup5.entreperiseLieeEntreprisePMFormeJuridique.getId();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[5]/M55.3/M55.3.3']= fusion.fusionGroup5.adresseFusionScission.communeAdresseFusion.getId();
if(fusion.fusionGroup5.adresseFusionScission.numeroAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[5]/M55.3/M55.3.5']= fusion.fusionGroup5.adresseFusionScission.numeroAdresseFusion;
}
if (fusion.fusionGroup5.adresseFusionScission.indiceAdresseFusion !== null) {
   var monId = Value('id').of(fusion.fusionGroup5.adresseFusionScission.indiceAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[5]/M55.3/M55.3.6']          = monId;
}
if(fusion.fusionGroup5.adresseFusionScission.distributionSpecialeAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[5]/M55.3/M55.3.7']= fusion.fusionGroup5.adresseFusionScission.distributionSpecialeAdresseFusion;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[5]/M55.3/M55.3.8']= fusion.fusionGroup5.adresseFusionScission.codePostalAdresseFusion;
if(fusion.fusionGroup5.adresseFusionScission.complementAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[5]/M55.3/M55.3.10']= fusion.fusionGroup5.adresseFusionScission.complementAdresseFusion;
}
if (fusion.fusionGroup5.adresseFusionScission.typeAdresseFusion !== null) {
   var monId = Value('id').of(fusion.fusionGroup5.adresseFusionScission.typeAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[5]/M55.3/M55.3.11']          = monId;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[5]/M55.3/M55.3.12']= fusion.fusionGroup5.adresseFusionScission.voieAdresseFusion;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[5]/M55.3/M55.3.13']= fusion.fusionGroup5.adresseFusionScission.communeAdresseFusion.getLabel();

//Récupérer le EntityId du greffe
var greffeId = fusion.fusionGroup5.entrepriseLieeGreffe.id ;
//Appeler Directory pour récupérer l'autorité
_log.info('l entityId du greffe selectionne est {}', greffeId);
var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', greffeId) //
		   .connectionTimeout(10000) //
		   .receiveTimeout(10000) //
		   .accept('json') //
		   .get();
//result
var receiverInfo = response.asObject();
var contactInfo = !receiverInfo.details ? null : receiverInfo.details;
//Récupérer le code EDI
var codeEDIGreffe = !contactInfo.ediCode ? null : contactInfo.ediCode;
_log.info('le code edi du greffe selectionne est {}', codeEDIGreffe);
// À modifier quand Destiny fourni le réferentiel
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[5]/M55.4']= codeEDIGreffe;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[5]/M55.6']= fusion.fusionGroup5.entrepriseLieeSiren.split(' ').join('');
}
if (fusion.fusionGroup5.fusionAutreSociete5) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[6]/M55.1']= fusion.fusionGroup6.entrepriseLieeEntreprisePMDenomination;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[6]/M55.2']= fusion.fusionGroup6.entreperiseLieeEntreprisePMFormeJuridique.getId();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[6]/M55.3/M55.3.3']= fusion.fusionGroup6.adresseFusionScission.communeAdresseFusion.getId();
if(fusion.fusionGroup6.adresseFusionScission.numeroAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[6]/M55.3/M55.3.5']= fusion.fusionGroup6.adresseFusionScission.numeroAdresseFusion;
}
if (fusion.fusionGroup6.adresseFusionScission.indiceAdresseFusion !== null) {
   var monId = Value('id').of(fusion.fusionGroup6.adresseFusionScission.indiceAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[6]/M55.3/M55.3.6']          = monId;
}
if(fusion.fusionGroup6.adresseFusionScission.distributionSpecialeAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[6]/M55.3/M55.3.7']= fusion.fusionGroup6.adresseFusionScission.distributionSpecialeAdresseFusion;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[6]/M55.3/M55.3.8']= fusion.fusionGroup6.adresseFusionScission.codePostalAdresseFusion;
if(fusion.fusionGroup6.adresseFusionScission.complementAdresseFusion != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[6]/M55.3/M55.3.10']= fusion.fusionGroup6.adresseFusionScission.complementAdresseFusion;
}
if (fusion.fusionGroup6.adresseFusionScission.typeAdresseFusion !== null) {
   var monId = Value('id').of(fusion.fusionGroup6.adresseFusionScission.typeAdresseFusion)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[6]/M55.3/M55.3.11']          = monId;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[6]/M55.3/M55.3.12']= fusion.fusionGroup6.adresseFusionScission.voieAdresseFusion;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[6]/M55.3/M55.3.13']= fusion.fusionGroup6.adresseFusionScission.communeAdresseFusion.getLabel();

//Récupérer le EntityId du greffe
var greffeId = fusion.fusionGroup6.entrepriseLieeGreffe.id ;
//Appeler Directory pour récupérer l'autorité
_log.info('l entityId du greffe selectionne est {}', greffeId);
var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', greffeId) //
		   .connectionTimeout(10000) //
		   .receiveTimeout(10000) //
		   .accept('json') //
		   .get();
//result
var receiverInfo = response.asObject();
var contactInfo = !receiverInfo.details ? null : receiverInfo.details;
//Récupérer le code EDI
var codeEDIGreffe = !contactInfo.ediCode ? null : contactInfo.ediCode;
_log.info('le code edi du greffe selectionne est {}', codeEDIGreffe);
// À modifier quand Destiny fourni le réferentiel
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[6]/M55.4']= codeEDIGreffe;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M55[6]/M55.6']= fusion.fusionGroup6.entrepriseLieeSiren.split(' ').join('');
}
if (fusion.fusionAugmentationCapital) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/FSM/M56']= "O";
}
}

// Groupe IPU : Immatriculation principale de l'enttreprise

//Récupérer le EntityId du greffe
var greffeId = identite.immatRCSGreffe.id ;
//Appeler Directory pour récupérer l'autorité
_log.info('l entityId du greffe selectionne est {}', greffeId);
var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', greffeId) //
		   .connectionTimeout(10000) //
		   .receiveTimeout(10000) //
		   .accept('json') //
		   .get();
//result
var receiverInfo = response.asObject();
var contactInfo = !receiverInfo.details ? null : receiverInfo.details;
//Récupérer le code EDI
var codeEDIGreffe = !contactInfo.ediCode ? null : contactInfo.ediCode;
_log.info('le code edi du greffe selectionne est {}', codeEDIGreffe);
// À modifier quand Destiny fourni le réferentiel
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U01/U01.1'] = codeEDIGreffe;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U02']= identite.siren.split(' ').join('');
if (identite.immatriculationRM) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U04']= identite.immatRMCMA.getId();
}
if(activite.activiteTemps != null and not Value('id').of(ouverture.etablissementDejaConnu).eq('oui')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U06']= activite.etablissementEffectifSalariePresenceOui ? "O" : "N";
}

// Groupe SIU : Siège de l'entreprise

if (origine.creationDomiciliation and (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege') or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal'))) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U10']= origine.entrepriseLieeNom;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U15']= origine.entrepriseLieeSiren.split(' ').join('');
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege') or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.3']= ouverture.cadreAdresseEtablissementOuvert.communeAdresseEtablissement.getId();
if (ouverture.cadreAdresseEtablissementOuvert.numeroAdresseEtablissement != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.5']= ouverture.cadreAdresseEtablissementOuvert.numeroAdresseEtablissement;
}
if (ouverture.cadreAdresseEtablissementOuvert.indiceAdresseEtablissement != null) {
   var monId1 = Value('id').of(ouverture.cadreAdresseEtablissementOuvert.indiceAdresseEtablissement)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.6']          = monId1;
}
if (ouverture.cadreAdresseEtablissementOuvert.distributionSpecialeAdresseEtablissement != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.7']= ouverture.cadreAdresseEtablissementOuvert.distributionSpecialeAdresseEtablissement;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.8']= ouverture.cadreAdresseEtablissementOuvert.codePostalAdresseEtablissement;
if (ouverture.cadreAdresseEtablissementOuvert.complementAdresseEtablissement != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.10']= ouverture.cadreAdresseEtablissementOuvert.complementAdresseEtablissement;
}
if (ouverture.cadreAdresseEtablissementOuvert.typeAdresseEtablissement != null) {
   var monId1 = Value('id').of(ouverture.cadreAdresseEtablissementOuvert.typeAdresseEtablissement)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.12']= ouverture.cadreAdresseEtablissementOuvert.voieAdresseEtablissement;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.13']= ouverture.cadreAdresseEtablissementOuvert.communeAdresseEtablissement.getLabel();
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.3']= identite.adresseSiege.siegeAdresseCommune.getId();
if (identite.adresseSiege.siegeAdresseNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.5']= identite.adresseSiege.siegeAdresseNumeroVoie;
}
if (identite.adresseSiege.siegeAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(identite.adresseSiege.siegeAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.6']          = monId1;
}
if (identite.adresseSiege.siegeAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.7']= identite.adresseSiege.siegeAdresseDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.8']= identite.adresseSiege.siegeAdresseCodePostal;
if (identite.adresseSiege.siegeAdresseComplementVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.10']= identite.adresseSiege.siegeAdresseComplementVoie;
}
if (identite.adresseSiege.siegeAdresseTypeVoie != null) {
   var monId1 = Value('id').of(identite.adresseSiege.siegeAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.12']= identite.adresseSiege.siegeAdresseNomVoie;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.13']= identite.adresseSiege.siegeAdresseCommune.getLabel();
}	

// Fin pour le 15M / 16M /23M /25M /26M /29M (transmission universselle du patrimoine / modification de la date de début d'activité / ESS)

// Groupe CPU : Caractéristiques de l'entreprise

if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).eq('10M') 
or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege') 
or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal')
or activite.modifObjetSocial
or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).eq('37M') 
or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).eq('40M') 
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M')
or Value('id').of(ouverture.etablissementDejaConnu).eq('non')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
or activiteInfo.modifActiviteTransfert or activiteInfo.modifActiviteReprise or activiteInfo.modifActiviteAcquisition or activiteInfo.modifRepriseExploitation
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63M')) { 

if (activite.etablissementActivitesAutres != null and (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege') 
or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal'))) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U21']= activite.etablissementActivitesAutres;
}
if (activite.cadreEffectifSalarie.totalEffectifSalarieNombre != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U22']= activite.cadreEffectifSalarie.totalEffectifSalarieNombre;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('37M')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.1']= "R";
}
}

// Fin Regent 37M //

// Groupe ISU : Greffes secondaires
						
if ((Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).eq('10M') 
or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege') 
or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal')
or activite.modifObjetSocial
or Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).eq('13M') 
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M')
or (not objet.immatriculationRM
and ((activite.nouvelleActiviteGroup1.domaineAdjonctionNAFA != null
and not Value('id').of(activite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
and not Value('id').of(activite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
and not Value('id').of(activite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
and not Value('id').of(activite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre')
and not Value('id').of(activite.nouvelleActiviteGroup1.jqpaDomaineTransport).eq('jqpaTransportAutre'))
or (activite.nouvelleActiviteGroup2.domaineAdjonctionNAFA != null
and not Value('id').of(activite.nouvelleActiviteGroup2.domaineAdjonctionNAFA).eq('nafaAutre')
and not Value('id').of(activite.nouvelleActiviteGroup2.nafaDomaineAlimentation).eq('alimentationAutre')
and not Value('id').of(activite.nouvelleActiviteGroup2.nafaDomaineAutresServices).eq('servicesAutre')
and not Value('id').of(activite.nouvelleActiviteGroup2.nafaDomaineProduction).eq('productionAutre')
and not Value('id').of(activite.nouvelleActiviteGroup2.jqpaDomaineTransport).eq('jqpaTransportAutre'))
or (activite.nouvelleActiviteGroup3.domaineAdjonctionNAFA != null
and not Value('id').of(activite.nouvelleActiviteGroup3.domaineAdjonctionNAFA).eq('nafaAutre')
and not Value('id').of(activite.nouvelleActiviteGroup3.nafaDomaineAlimentation).eq('alimentationAutre')
and not Value('id').of(activite.nouvelleActiviteGroup3.nafaDomaineAutresServices).eq('servicesAutre')
and not Value('id').of(activite.nouvelleActiviteGroup3.nafaDomaineProduction).eq('productionAutre')
and not Value('id').of(activite.nouvelleActiviteGroup3.jqpaDomaineTransport).eq('jqpaTransportAutre')))
and not activite.optionCMACCI))
and identite.greffeSecondaire != null) {

	var occurrencesU41 = [];
	var occurrence = {};

	_log.info("Accessed to : occurence U41 bloc");
	_log.info("Greffe secondaire bloc : " + identite.greffeSecondaire);
	_log.info("Greffe secondaire bloc nbr de greffe secondaire : " + identite.greffeSecondaire.size());
	_log.info("Detail de la taille 0: " + identite.greffeSecondaire[0]);
	if (identite.greffeSecondaire != null and identite.greffeSecondaire[0].length > 0) {
		_log.info("entré dans la boucle des elements");
		
		for (var i = 0; i < identite.greffeSecondaire.size(); i++) {
			var idx = i+1;	
			regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[' + idx + ']/U41.1']= identite.greffeSecondaire[i];
			regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[' + idx + ']/U41.2']= "000000000000000";
							
		}	
	}
}
	
// Fin Regent pour le 10M //
// Fin Regent pour le 12M //
// Fin Regent pour le 13M //

var fondePouvoir = [];
['dirigeant1', 'dirigeant2', 'dirigeant3', 'dirigeant4', 'dirigeant5', 'dirigeant6', 'dirigeant7', 'dirigeant8', 'dirigeant9', 'dirigeant10', 'dirigeant11', 'dirigeant12'].forEach(function(fondePouvoirs){
	if(fondePouvoirs=='dirigeant1' and null != $M2[fondePouvoirs] and (Value('id').of($M2[fondePouvoirs].cadreIdentiteDirigeant.personneLieeQualite).eq('66') or (ouverture.ajoutFondePouvoir and $M2[fondePouvoirs].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null))) {
		fondePouvoir.push($M2[fondePouvoirs]);	
	}
	if (fondePouvoirs!='dirigeant1' and null != $M2[fondePouvoirs] and Value('id').of($M2[fondePouvoirs].cadreIdentiteDirigeant.personneLieeQualite).eq('66')) {
		fondePouvoir.push($M2[fondePouvoirs]);	
	}
});
var pouvoir = [];
['dirigeant1Sarl', 'dirigeant2Sarl', 'dirigeant3Sarl', 'dirigeant4Sarl', 'dirigeant5Sarl'].forEach(function(pouvoirs){
	if(pouvoirs=='dirigeant1Sarl' and null != $M2[pouvoirs] and (Value('id').of($M2[pouvoirs].cadreIdentiteDirigeant.personneLieeQualite).eq('66') or (ouverture.ajoutFondePouvoir and $M2[pouvoirs].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null))) {
		pouvoir.push($M2[pouvoirs]);	
	}
	if ( pouvoirs!='dirigeant1Sarl' and null != $M2[pouvoirs] and Value('id').of($M2[pouvoirs].cadreIdentiteDirigeant.personneLieeQualite).eq('66')) {
		pouvoir.push($M2[pouvoirs]);	
	}
});

				// Groupe GRD : Groupe dirigeant personne personne physique
 
var occurrencesPersonnePhysiqueDirigeante = [];
var occurrence = {};
				
// Liquidateur			

if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).eq('22M') and not dissolution.dissolutionTransmissionUniverselle and Value('id').of(dissolution.cadreIdentiteLiquidateur.liquidateurPersonnaliteJuridique).eq('personnePhysique')) {				

//Groupe DIU
if (dissolution.cadreIdentiteLiquidateur.liquidateurDejaConnu) {
occurrence['DIU/U50/U50.1'] = "4";
} else {
occurrence['DIU/U50/U50.1'] = "1" ;
}
if (dissolution.modifDateDissolution != null) {
var dateTemp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= "40";
if (dissolution.cadreIdentiteLiquidateur.liquidateurDejaConnu) {
occurrence['DIU/U51/U51.2']= dissolution.cadreIdentiteLiquidateur.liquidateurAncienneQualite.getId();
}
occurrence['DIU/U52'] = "P";

// Groupe IPD
occurrence['IPD/D01/D01.2']= dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPNomNaissanceLiquidateur;
var prenoms=[];
for ( i = 0; i < dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPPrenomLiquidateur.size() ; i++ ){prenoms.push(dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPPrenomLiquidateur[i]);}      
occurrence['IPD/D01/D01.3']= prenoms;
if (dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPNomUsageLiquidateur != null) {
occurrence['IPD/D01/D01.4']= dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPNomUsageLiquidateur;
}
if(not dissolution.cadreIdentiteLiquidateur.liquidateurDejaConnu) {
if(dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPDateNaissanceLiquidateur != null) {
var dateTemp = new Date(parseInt(dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPDateNaissanceLiquidateur.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['IPD/D03/D03.1']= date;
}
if (not Value('id').of(dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPLieuNaissancePaysLiquidateur).eq('FRXXXXX')) {
	var idPays = dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPLieuNaissancePaysLiquidateur.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPLieuNaissancePaysLiquidateur).eq('FRXXXXX')) {
occurrence['IPD/D03/D03.2'] = dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneLiquidateur.getId();
}	
if (not Value('id').of(dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPLieuNaissancePaysLiquidateur).eq('FRXXXXX')) {																					 
occurrence['IPD/D03/D03.3']= dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPLieuNaissancePaysLiquidateur.getLabel();
}
if (Value('id').of(dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPLieuNaissancePaysLiquidateur).eq('FRXXXXX')) {	
occurrence['IPD/D03/D03.4']= dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneLiquidateur.getLabel();
}
}
occurrence['IPD/D04/D04.3'] = dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseCommune.getId();
if (dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseNumeroVoie != null) {
occurrence['IPD/D04/D04.5'] = dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseNumeroVoie;
}
if (dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseIndiceVoie != null) {
var monId1 = Value('id').of(dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseIndiceVoie)._eval();
occurrence['IPD/D04/D04.6']          = monId1;
}
if (dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['IPD/D04/D04.7'] = dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['IPD/D04/D04.8'] = dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseCodePostal;
if (dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseComplementAdresse != null) {
occurrence['IPD/D04/D04.10'] = dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseComplementAdresse;
}
if (dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseTypeVoie != null) {
var monId1 = Value('id').of(dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseTypeVoie)._eval();
occurrence['IPD/D04/D04.11']          = monId1;
}
occurrence['IPD/D04/D04.12'] = dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseNomVoie;
occurrence['IPD/D04/D04.13'] = dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseCommune.getLabel();

// Groupe NPD
occurrence['NPD/D21']= dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePPNationaliteLiquidateur;

// Groupe GCS
// ISS	
var nirLiquidateur = dissolution.cadreIdentiteLiquidateur.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
if(nirLiquidateur != null) {
    nirLiquidateur = nirLiquidateur.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirLiquidateur.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirLiquidateur.substring(13, 15);

// CAS
occurrence['GCS/CAS/A42/A42.1']= "."; 
//occurrence['GCS/CAS/A44']= "."; 
}
occurrencesPersonnePhysiqueDirigeante.push(occurrence);
occurrence = {}; 
}

// Dirigeant 1 SARL

if ((Value('id').of(dirigeantSARL1.personneLieeQualite).eq('30') or Value('id').of(dirigeantSARL1.personneLieeQualite).eq('40'))
	or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') 
	and Value('id').of(dirigeantSARL1.personneLieeObjet).eq('dirigeantPartant') 
	and dirigeantSARL1.personneLieeQualiteAncienne2 != null))	{					

//Groupe DIU
if(Value('id').of(dirigeantSARL1.personneLieeObjet).eq('dirigeantNouveau')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(dirigeantSARL1.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(dirigeantSARL1.personneLieeObjet).eq('dirigeantModif')) {
occurrence['DIU/U50/U50.1'] = "3" ;
} else if(Value('id').of(dirigeantSARL1.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U50/U50.1'] = "4" ;
}
if (conjoint1.personneLieePPNomNaissanceConjointPacse != null or Value('id').of(dirigeantSARL1.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutAssocie')) {
occurrence['DIU/U50/U50.3']= "O";
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null and modifFormeJuridique.formeJuridiqueModifDirigeant) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and dirigeantSARL1.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(dirigeantSARL1.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= dirigeantSARL1.personneLieeQualite != null ? (Value('id').of(dirigeantSARL1.personneLieeQualite).eq('40') ? "40" : "30") : dirigeantSARL1.personneLieeQualiteAncienne2.getId();
if (Value('id').of(dirigeantSARL1.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U51/U51.2']= dirigeantSARL1.personneLieeQualiteAncienne.getId();
}
occurrence['DIU/U52']= "P";

// Groupe IPD
occurrence['IPD/D01/D01.2']= dirigeantSARL1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
var prenoms=[];
for ( i = 0; i < dirigeantSARL1.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantSARL1.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}      
occurrence['IPD/D01/D01.3']= prenoms;
if (dirigeantSARL1.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['IPD/D01/D01.4']= dirigeantSARL1.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
if(Value('id').of(dirigeantSARL1.personneLieeObjet).eq('dirigeantNouveau')) {
if(dirigeantSARL1.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
var dateTemp = new Date(parseInt(dirigeantSARL1.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['IPD/D03/D03.1']= date;
}
if (not Value('id').of(dirigeantSARL1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeantSARL1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeantSARL1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
occurrence['IPD/D03/D03.2'] = dirigeantSARL1.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}	
if (not Value('id').of(dirigeantSARL1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
occurrence['IPD/D03/D03.3']= dirigeantSARL1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeantSARL1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
occurrence['IPD/D03/D03.4']= dirigeantSARL1.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
}
if (not Value('id').of(dirigeantSARL1.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.3']= dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['IPD/D04/D04.3']= result;
}
if (dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['IPD/D04/D04.5'] = dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
var monId1 = Value('id').of(dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
occurrence['IPD/D04/D04.6']          = monId1;
}
if (dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['IPD/D04/D04.7'] = dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['IPD/D04/D04.8'] = dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['IPD/D04/D04.10'] = dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
var monId1 = Value('id').of(dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
occurrence['IPD/D04/D04.11']          = monId1;
}
occurrence['IPD/D04/D04.12'] = dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.14']= dirigeantSARL1.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD
occurrence['NPD/D21']= dirigeantSARL1.civilitePersonnePhysique.personneLieePPNationaliteGerant;
}

// Groupe SCD
if (conjoint1.personneLieePPNomNaissanceConjointPacse != null or Value('id').of(dirigeantSARL1.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutAssocie')) {
if (Value('id').of(dirigeantSARL1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') or Value('id').of(conjoint1.statutModifConjoint).eq('modifStatutCollaborateur')) {
occurrence['SCD/D40']= "1";
} else if (Value('id').of(dirigeantSARL1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie') or Value('id').of(dirigeantSARL1.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutAssocie')) {
occurrence['SCD/D40']= "2";
} else if (Value('id').of(dirigeantSARL1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutSalarie') or Value('id').of(dirigeantSARL1.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutSalarie') or Value('id').of(conjoint1.statutModifConjoint).eq('modifStatutSalarie')) {
occurrence['SCD/D40']= "3";
}
occurrence['SCD/D41']= Value('id').of(conjoint1.objetModifConjoint).eq('suppressionMentionConjoint') ? "S" : "O";
}

// Groupe ICD
if (Value('id').of(dirigeantSARL1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') or Value('id').of(conjoint1.statutModifConjoint).eq('modifStatutCollaborateur')) {
occurrence['ICD/D42/D42.2'] = conjoint1.personneLieePPNomNaissanceConjointPacse;
var prenoms=[];
for ( i = 0; i < conjoint1.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(conjoint1.personneLieePPPrenomConjointPacse[i]);}      
occurrence['ICD/D42/D42.3']= prenoms;
if (conjoint1.personneLieePPNomUsageConjointPacse != null) {
occurrence['ICD/D42/D42.4']= conjoint1.personneLieePPNomUsageConjointPacse;
}
if (not Value('id').of(conjoint1.objetModifConjoint).eq('suppressionMentionConjoint')) {
if(conjoint1.personneLieePPDateNaissanceConjointPacse != null) {
var dateTemp = new Date(parseInt(conjoint1.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['ICD/D43/D43.1']= date;
}
if (not Value('id').of(conjoint1.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
	var idPays = conjoint1.personneLieePPPaysNaissanceConjointPacse.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['ICD/D43/D43.2']= result;
} else if (Value('id').of(conjoint1.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
occurrence['ICD/D43/D43.2'] = conjoint1.personneLieePPLieuNaissanceCommnuneConjointPacse.getId();
}	
if (not Value('id').of(conjoint1.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {																					 
occurrence['ICD/D43/D43.3']= conjoint1.personneLieePPPaysNaissanceConjointPacse.getLabel();
}
if (Value('id').of(conjoint1.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {	
occurrence['ICD/D43/D43.4']= conjoint1.personneLieePPLieuNaissanceCommnuneConjointPacse.getLabel();
}
}
if (conjoint1.personneLieePPNationaliteConjointPacse != null) {
occurrence['ICD/D44'] = conjoint1.personneLieePPNationaliteConjointPacse;
}
if (conjoint1.adresseConjointDifferente) {
if (Value('id').of(conjoint1.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['ICD/D45/D45.3']= conjoint1.adresseDomicileConjoint.adresseConjointCommune.getId();
} else if (not Value('id').of(conjoint1.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
		var idPays = conjoint1.adresseDomicileConjoint.adresseConjointPays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['ICD/D45/D45.3']= result;
}
if (conjoint1.adresseDomicileConjoint.adresseConjointNumeroVoie != null) {
occurrence['ICD/D45/D45.5'] = conjoint1.adresseDomicileConjoint.adresseConjointNumeroVoie;
}
if (conjoint1.adresseDomicileConjoint.adresseConjointIndiceVoie != null) {
var monId1 = Value('id').of(conjoint1.adresseDomicileConjoint.adresseConjointIndiceVoie)._eval();
occurrence['ICD/D45/D45.6']          = monId1;
}
if (conjoint1.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null) {
occurrence['ICD/D45/D45.7'] = conjoint1.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie;
}
occurrence['ICD/D45/D45.8'] = conjoint1.adresseDomicileConjoint.adresseConjointCodePostal != null ? conjoint1.adresseDomicileConjoint.adresseConjointCodePostal : ".";
if (conjoint1.adresseDomicileConjoint.adresseConjointComplementVoie != null) {
occurrence['ICD/D45/D45.10'] = conjoint1.adresseDomicileConjoint.adresseConjointComplementVoie;
}
if (conjoint1.adresseDomicileConjoint.adresseConjointTypeVoie != null) {
var monId1 = Value('id').of(conjoint1.adresseDomicileConjoint.adresseConjointTypeVoie)._eval();
occurrence['ICD/D45/D45.11']          = monId1;
}
occurrence['ICD/D45/D45.12'] = conjoint1.adresseDomicileConjoint.adresseConjointNomVoie;
if (Value('id').of(conjoint1.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['ICD/D45/D45.13']= conjoint1.adresseDomicileConjoint.adresseConjointCommune.getLabel();
} else if (not Value('id').of(conjoint1.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['ICD/D45/D45.13']= conjoint1.adresseDomicileConjoint.adresseConjointVille;
}
if (not Value('id').of(conjoint1.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['ICD/D45/D45.14']= conjoint1.adresseDomicileConjoint.adresseConjointPays.getLabel();
}
}
}

// Groupe GCS
if (dirigeantSARL1.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS != null) {
var nirExploitant = dirigeantSARL1.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);
occurrence['GCS/CAS/A42/A42.1']= "."; 
} 
if (socialDirigeant1.voletSocialNumeroSecuriteSocialTNS != null) {

// ISS
var nirExploitant = socialDirigeant1.voletSocialNumeroSecuriteSocialTNS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);

// SNS
if (socialDirigeant1.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant1.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
var dateTemp = new Date(parseInt(socialDirigeant1.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['GCS/SNS/A21/A21.2']= date;
}	
occurrence['GCS/SNS/A21/A21.3']= socialDirigeant1.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['GCS/SNS/A21/A21.4']= socialDirigeant1.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['GCS/SNS/A21/A21.6']= socialDirigeant1.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['GCS/SNS/A22/A22.3']= Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" :
								 (Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
								 (Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : " X"));
if (Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') or Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['GCS/SNS/A22/A22.4']= Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant1.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : "ENIM";
}
if (socialDirigeant1.voletSocialActiviteSimultaneeOUINON) {
occurrence['GCS/SNS/A24/A24.2']= Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" :
								 (Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
								 (Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : " 9"));
if (Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
occurrence['GCS/SNS/A24/A24.3']= socialDirigeant1.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}

// CAS
occurrence['GCS/CAS/A42/A42.1']= "."; 
//occurrence['GCS/CAS/A44']= "."; 

// SCS
if (Value('id').of(dirigeantSARL1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') and not Value('id').of(conjoint1.objetModifConjoint).eq('suppressionMentionConjoint')) {
occurrence['GCS/SCS/A53/A53.4']= conjoint1.voletSocialConjointCouvertAssuranceMaladieOui ? "O" : "N"; 
if (conjoint1.voletSocialConjointCollaborateurNumeroSecuriteSociale != null) {				
var nirExploitant = conjoint1.voletSocialConjointCollaborateurNumeroSecuriteSociale;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/SCS/A55/A55.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/SCS/A55/A55.2']=  nirExploitant.substring(13, 15);
} else if (socialAssocie1.voletSocialNumeroSecuriteSocialTNS != null) {				
var nirExploitant = socialAssocie1.voletSocialNumeroSecuriteSocialTNS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/SCS/A55/A55.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/SCS/A55/A55.2']=  nirExploitant.substring(13, 15);
}
}

// SAS
if (Value('id').of(dirigeantSARL1.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) {
occurrence['SAS/A68/A68.2'] = conjoint1.personneLieePPNomNaissanceConjointPacse;
var prenoms=[];
for ( i = 0; i < conjoint1.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(conjoint1.personneLieePPPrenomConjointPacse[i]);}      
occurrence['SAS/A68/A68.3']= prenoms;
if (conjoint1.personneLieePPNomUsageConjointPacse != null) {
occurrence['SAS/A68/A68.4']= conjoint1.personneLieePPNomUsageConjointPacse;
}
if(conjoint1.personneLieePPDateNaissanceConjointPacse != null) {
var dateTemp = new Date(parseInt(conjoint1.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['SAS/A69/A69.1']= date;
}
if (not Value('id').of(conjoint1.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
	var idPays = conjoint1.personneLieePPPaysNaissanceConjointPacse.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['SAS/A69/A69.2']= result;
} else if (Value('id').of(conjoint1.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
occurrence['SAS/A69/A69.2'] = conjoint1.personneLieePPLieuNaissanceCommnuneConjointPacse.getId();
}	
if (not Value('id').of(conjoint1.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {																					 
occurrence['SAS/A69/A69.3']= conjoint1.personneLieePPPaysNaissanceConjointPacse.getLabel();
}
if (Value('id').of(conjoint1.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {	
occurrence['SAS/A69/A69.4']= conjoint1.personneLieePPLieuNaissanceCommnuneConjointPacse.getLabel();
}
if (conjoint1.personneLieePPNationaliteConjointPacse != null) {
occurrence['SAS/A70'] = conjoint1.personneLieePPNationaliteConjointPacse;
}
if (conjoint1.adresseConjointDifferente) {
if (Value('id').of(conjoint1.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['SAS/A71/A71.3']= conjoint1.adresseDomicileConjoint.adresseConjointCommune.getId();
} else if (not Value('id').of(conjoint1.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
		var idPays = conjoint1.adresseDomicileConjoint.adresseConjointPays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['SAS/A71/A71.3']= result;
}
if (conjoint1.adresseDomicileConjoint.adresseConjointNumeroVoie != null) {
occurrence['SAS/A71/A71.5'] = conjoint1.adresseDomicileConjoint.adresseConjointNumeroVoie;
}
if (conjoint1.adresseDomicileConjoint.adresseConjointIndiceVoie != null) {
var monId1 = Value('id').of(conjoint1.adresseDomicileConjoint.adresseConjointIndiceVoie)._eval();
occurrence['SAS/A71/A71.6']          = monId1;
}
if (conjoint1.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null) {
occurrence['SAS/A71/A71.7'] = conjoint1.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie;
}
occurrence['SAS/A71/A71.8'] = conjoint1.adresseDomicileConjoint.adresseConjointCodePostal != null ? conjoint1.adresseDomicileConjoint.adresseConjointCodePostal : ".";
if (conjoint1.adresseDomicileConjoint.adresseConjointComplementVoie != null) {
occurrence['SAS/A71/A71.10'] = conjoint1.adresseDomicileConjoint.adresseConjointComplementVoie;
}
if (conjoint1.adresseDomicileConjoint.adresseConjointTypeVoie != null) {
var monId1 = Value('id').of(conjoint1.adresseDomicileConjoint.adresseConjointTypeVoie)._eval();
occurrence['SAS/A71/A71.11']          = monId1;
}
occurrence['SAS/A71/A71.12'] = conjoint1.adresseDomicileConjoint.adresseConjointNomVoie;
if (Value('id').of(conjoint1.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['SAS/A71/A71.13']= conjoint1.adresseDomicileConjoint.adresseConjointCommune.getLabel();
} else if (not Value('id').of(conjoint1.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['SAS/A71/A71.13']= conjoint1.adresseDomicileConjoint.adresseConjointVille;
}
if (not Value('id').of(conjoint1.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['SAS/A71/A71.14']= conjoint1.adresseDomicileConjoint.adresseConjointPays.getLabel();
}
}
}
}
occurrencesPersonnePhysiqueDirigeante.push(occurrence);
occurrence = {}; 
}

// Dirigeant 2 SARL

if ((Value('id').of(dirigeantSARL2.personneLieeQualite).eq('30') or Value('id').of(dirigeantSARL2.personneLieeQualite).eq('40')) 
	or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') 
	and Value('id').of(dirigeantSARL2.personneLieeObjet).eq('dirigeantPartant') 
	and dirigeantSARL2.personneLieeQualiteAncienne2 != null)){					

//Groupe DIU
if(Value('id').of(dirigeantSARL2.personneLieeObjet).eq('dirigeantNouveau')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(dirigeantSARL2.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(dirigeantSARL2.personneLieeObjet).eq('dirigeantModif')) {
occurrence['DIU/U50/U50.1'] = "3" ;
} else if(Value('id').of(dirigeantSARL2.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U50/U50.1'] = "4" ;
}
if (conjoint2.personneLieePPNomNaissanceConjointPacse != null or Value('id').of(dirigeantSARL2.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutAssocie')) {
occurrence['DIU/U50/U50.3']= "O";
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null and modifFormeJuridique.formeJuridiqueModifDirigeant) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and dirigeantSARL2.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(dirigeantSARL2.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= dirigeantSARL2.personneLieeQualite != null ? (Value('id').of(dirigeantSARL2.personneLieeQualite).eq('40') ? "40" : "30") : dirigeantSARL2.personneLieeQualiteAncienne2.getId();
if (Value('id').of(dirigeantSARL2.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U51/U51.2']= dirigeantSARL2.personneLieeQualiteAncienne.getId();
}
occurrence['DIU/U52']= "P";

// Groupe IPD
occurrence['IPD/D01/D01.2']= dirigeantSARL2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
var prenoms=[];
for ( i = 0; i < dirigeantSARL2.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantSARL2.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}      
occurrence['IPD/D01/D01.3']= prenoms;
if (dirigeantSARL2.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['IPD/D01/D01.4']= dirigeantSARL2.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
if(Value('id').of(dirigeantSARL2.personneLieeObjet).eq('dirigeantNouveau')) {
if(dirigeantSARL2.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
var dateTemp = new Date(parseInt(dirigeantSARL2.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['IPD/D03/D03.1']= date;
}
if (not Value('id').of(dirigeantSARL2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeantSARL2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeantSARL2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
occurrence['IPD/D03/D03.2'] = dirigeantSARL2.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}	
if (not Value('id').of(dirigeantSARL2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
occurrence['IPD/D03/D03.3']= dirigeantSARL2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeantSARL2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
occurrence['IPD/D03/D03.4']= dirigeantSARL2.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
}
if (not Value('id').of(dirigeantSARL2.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(dirigeantSARL2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.3']= dirigeantSARL2.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantSARL2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantSARL2.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['IPD/D04/D04.3']= result;
}
if (dirigeantSARL2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['IPD/D04/D04.5'] = dirigeantSARL2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantSARL2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
var monId1 = Value('id').of(dirigeantSARL2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
occurrence['IPD/D04/D04.6']          = monId1;
}
if (dirigeantSARL2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['IPD/D04/D04.7'] = dirigeantSARL2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['IPD/D04/D04.8'] = dirigeantSARL2.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantSARL2.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantSARL2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['IPD/D04/D04.10'] = dirigeantSARL2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantSARL2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
var monId1 = Value('id').of(dirigeantSARL2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
occurrence['IPD/D04/D04.11']          = monId1;
}
occurrence['IPD/D04/D04.12'] = dirigeantSARL2.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantSARL2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeantSARL2.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantSARL2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeantSARL2.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantSARL2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.14']= dirigeantSARL2.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD
occurrence['NPD/D21']= dirigeantSARL2.civilitePersonnePhysique.personneLieePPNationaliteGerant;
}

// Groupe SCD
if (conjoint2.personneLieePPNomNaissanceConjointPacse != null or Value('id').of(dirigeantSARL2.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutAssocie')) {
if (Value('id').of(dirigeantSARL2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') or Value('id').of(conjoint2.statutModifConjoint).eq('modifStatutCollaborateur')) {
occurrence['SCD/D40']= "1";
} else if (Value('id').of(dirigeantSARL2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie') or Value('id').of(dirigeantSARL2.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutAssocie')) {
occurrence['SCD/D40']= "2";
} else if (Value('id').of(dirigeantSARL2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutSalarie') or Value('id').of(dirigeantSARL2.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutSalarie') or Value('id').of(conjoint2.statutModifConjoint).eq('modifStatutSalarie')) {
occurrence['SCD/D40']= "3";
}
occurrence['SCD/D41']= Value('id').of(conjoint2.objetModifConjoint).eq('suppressionMentionConjoint') ? "S" : "O";
}

// Groupe ICD
if (Value('id').of(dirigeantSARL2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') or Value('id').of(conjoint2.statutModifConjoint).eq('modifStatutCollaborateur')) {
occurrence['ICD/D42/D42.2'] = conjoint2.personneLieePPNomNaissanceConjointPacse;
var prenoms=[];
for ( i = 0; i < conjoint2.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(conjoint2.personneLieePPPrenomConjointPacse[i]);}      
occurrence['ICD/D42/D42.3']= prenoms;
if (conjoint2.personneLieePPNomUsageConjointPacse != null) {
occurrence['ICD/D42/D42.4']= conjoint2.personneLieePPNomUsageConjointPacse;
}
if (not Value('id').of(conjoint2.objetModifConjoint).eq('suppressionMentionConjoint')) {
if(conjoint2.personneLieePPDateNaissanceConjointPacse != null) {
var dateTemp = new Date(parseInt(conjoint2.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['ICD/D43/D43.1']= date;
}
if (not Value('id').of(conjoint2.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
	var idPays = conjoint2.personneLieePPPaysNaissanceConjointPacse.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['ICD/D43/D43.2']= result;
} else if (Value('id').of(conjoint2.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
occurrence['ICD/D43/D43.2'] = conjoint2.personneLieePPLieuNaissanceCommnuneConjointPacse.getId();
}	
if (not Value('id').of(conjoint2.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {																					 
occurrence['ICD/D43/D43.3']= conjoint2.personneLieePPPaysNaissanceConjointPacse.getLabel();
}
if (Value('id').of(conjoint2.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {	
occurrence['ICD/D43/D43.4']= conjoint2.personneLieePPLieuNaissanceCommnuneConjointPacse.getLabel();
}
}
if (conjoint2.personneLieePPNationaliteConjointPacse != null) {
occurrence['ICD/D44'] = conjoint2.personneLieePPNationaliteConjointPacse;
}
if (conjoint2.adresseConjointDifferente) {
if (Value('id').of(conjoint2.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['ICD/D45/D45.3']= conjoint2.adresseDomicileConjoint.adresseConjointCommune.getId();
} else if (not Value('id').of(conjoint2.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
		var idPays = conjoint2.adresseDomicileConjoint.adresseConjointPays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['ICD/D45/D45.3']= result;
}
if (conjoint2.adresseDomicileConjoint.adresseConjointNumeroVoie != null) {
occurrence['ICD/D45/D45.5'] = conjoint2.adresseDomicileConjoint.adresseConjointNumeroVoie;
}
if (conjoint2.adresseDomicileConjoint.adresseConjointIndiceVoie != null) {
var monId1 = Value('id').of(conjoint2.adresseDomicileConjoint.adresseConjointIndiceVoie)._eval();
occurrence['ICD/D45/D45.6']          = monId1;
}
if (conjoint2.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null) {
occurrence['ICD/D45/D45.7'] = conjoint2.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie;
}
occurrence['ICD/D45/D45.8'] = conjoint2.adresseDomicileConjoint.adresseConjointCodePostal != null ? conjoint2.adresseDomicileConjoint.adresseConjointCodePostal : ".";
if (conjoint2.adresseDomicileConjoint.adresseConjointComplementVoie != null) {
occurrence['ICD/D45/D45.10'] = conjoint2.adresseDomicileConjoint.adresseConjointComplementVoie;
}
if (conjoint2.adresseDomicileConjoint.adresseConjointTypeVoie != null) {
var monId1 = Value('id').of(conjoint2.adresseDomicileConjoint.adresseConjointTypeVoie)._eval();
occurrence['ICD/D45/D45.11']          = monId1;
}
occurrence['ICD/D45/D45.12'] = conjoint2.adresseDomicileConjoint.adresseConjointNomVoie;
if (Value('id').of(conjoint2.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['ICD/D45/D45.13']= conjoint2.adresseDomicileConjoint.adresseConjointCommune.getLabel();
} else if (not Value('id').of(conjoint2.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['ICD/D45/D45.13']= conjoint2.adresseDomicileConjoint.adresseConjointVille;
}
if (not Value('id').of(conjoint2.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['ICD/D45/D45.14']= conjoint2.adresseDomicileConjoint.adresseConjointPays.getLabel();
}
}
}

// Groupe GCS
if (dirigeantSARL2.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS != null) {
var nirExploitant = dirigeantSARL2.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);
occurrence['GCS/CAS/A42/A42.1']= "."; 
} 
if (socialDirigeant2.voletSocialNumeroSecuriteSocialTNS != null) {

// ISS
var nirExploitant = socialDirigeant2.voletSocialNumeroSecuriteSocialTNS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);

// SNS
if (socialDirigeant2.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant2.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
var dateTemp = new Date(parseInt(socialDirigeant2.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['GCS/SNS/A21/A21.2']= date;
}	
occurrence['GCS/SNS/A21/A21.3']= socialDirigeant2.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['GCS/SNS/A21/A21.4']= socialDirigeant2.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['GCS/SNS/A21/A21.6']= socialDirigeant2.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['GCS/SNS/A22/A22.3']= Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" :
								 (Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
								 (Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : " X"));
if (Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') or Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['GCS/SNS/A22/A22.4']= Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant2.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : "ENIM";
}
if (socialDirigeant2.voletSocialActiviteSimultaneeOUINON) {
occurrence['GCS/SNS/A24/A24.2']= Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" :
								 (Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
								 (Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : " 9"));
if (Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
occurrence['GCS/SNS/A24/A24.3']= socialDirigeant2.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}

// CAS
occurrence['GCS/CAS/A42/A42.1']= "."; 
//occurrence['GCS/CAS/A44']= "."; 

// SCS
if (Value('id').of(dirigeantSARL2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') and not Value('id').of(conjoint2.objetModifConjoint).eq('suppressionMentionConjoint')) {
occurrence['GCS/SCS/A53/A53.4']= conjoint2.voletSocialConjointCouvertAssuranceMaladieOui ? "O" : "N"; 
if (conjoint2.voletSocialConjointCollaborateurNumeroSecuriteSociale != null) {				
var nirExploitant = conjoint2.voletSocialConjointCollaborateurNumeroSecuriteSociale;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/SCS/A55/A55.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/SCS/A55/A55.2']=  nirExploitant.substring(13, 15);
} else if (socialAssocie2.voletSocialNumeroSecuriteSocialTNS != null) {				
var nirExploitant = socialAssocie2.voletSocialNumeroSecuriteSocialTNS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/SCS/A55/A55.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/SCS/A55/A55.2']=  nirExploitant.substring(13, 15);
}
}

// SAS
if (Value('id').of(dirigeantSARL2.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) {
occurrence['SAS/A68/A68.2'] = conjoint2.personneLieePPNomNaissanceConjointPacse;
var prenoms=[];
for ( i = 0; i < conjoint2.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(conjoint2.personneLieePPPrenomConjointPacse[i]);}      
occurrence['SAS/A68/A68.3']= prenoms;
if (conjoint2.personneLieePPNomUsageConjointPacse != null) {
occurrence['SAS/A68/A68.4']= conjoint2.personneLieePPNomUsageConjointPacse;
}
if(conjoint2.personneLieePPDateNaissanceConjointPacse != null) {
var dateTemp = new Date(parseInt(conjoint2.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['SAS/A69/A69.1']= date;
}
if (not Value('id').of(conjoint2.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
	var idPays = conjoint2.personneLieePPPaysNaissanceConjointPacse.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['SAS/A69/A69.2']= result;
} else if (Value('id').of(conjoint2.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
occurrence['SAS/A69/A69.2'] = conjoint2.personneLieePPLieuNaissanceCommnuneConjointPacse.getId();
}	
if (not Value('id').of(conjoint2.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {																					 
occurrence['SAS/A69/A69.3']= conjoint2.personneLieePPPaysNaissanceConjointPacse.getLabel();
}
if (Value('id').of(conjoint2.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {	
occurrence['SAS/A69/A69.4']= conjoint2.personneLieePPLieuNaissanceCommnuneConjointPacse.getLabel();
}
if (conjoint2.personneLieePPNationaliteConjointPacse != null) {
occurrence['SAS/A70'] = conjoint2.personneLieePPNationaliteConjointPacse;
}
if (conjoint2.adresseConjointDifferente) {
if (Value('id').of(conjoint2.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['SAS/A71/A71.3']= conjoint2.adresseDomicileConjoint.adresseConjointCommune.getId();
} else if (not Value('id').of(conjoint2.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
		var idPays = conjoint2.adresseDomicileConjoint.adresseConjointPays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['SAS/A71/A71.3']= result;
}
if (conjoint2.adresseDomicileConjoint.adresseConjointNumeroVoie != null) {
occurrence['SAS/A71/A71.5'] = conjoint2.adresseDomicileConjoint.adresseConjointNumeroVoie;
}
if (conjoint2.adresseDomicileConjoint.adresseConjointIndiceVoie != null) {
var monId1 = Value('id').of(conjoint2.adresseDomicileConjoint.adresseConjointIndiceVoie)._eval();
occurrence['SAS/A71/A71.6']          = monId1;
}
if (conjoint2.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null) {
occurrence['SAS/A71/A71.7'] = conjoint2.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie;
}
occurrence['SAS/A71/A71.8'] = conjoint2.adresseDomicileConjoint.adresseConjointCodePostal != null ? conjoint2.adresseDomicileConjoint.adresseConjointCodePostal : ".";
if (conjoint2.adresseDomicileConjoint.adresseConjointComplementVoie != null) {
occurrence['SAS/A71/A71.10'] = conjoint2.adresseDomicileConjoint.adresseConjointComplementVoie;
}
if (conjoint2.adresseDomicileConjoint.adresseConjointTypeVoie != null) {
var monId1 = Value('id').of(conjoint2.adresseDomicileConjoint.adresseConjointTypeVoie)._eval();
occurrence['SAS/A71/A71.11']          = monId1;
}
occurrence['SAS/A71/A71.12'] = conjoint2.adresseDomicileConjoint.adresseConjointNomVoie;
if (Value('id').of(conjoint2.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['SAS/A71/A71.13']= conjoint2.adresseDomicileConjoint.adresseConjointCommune.getLabel();
} else if (not Value('id').of(conjoint2.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['SAS/A71/A71.13']= conjoint2.adresseDomicileConjoint.adresseConjointVille;
}
if (not Value('id').of(conjoint2.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['SAS/A71/A71.14']= conjoint2.adresseDomicileConjoint.adresseConjointPays.getLabel();
}
}
}
}
occurrencesPersonnePhysiqueDirigeante.push(occurrence);
occurrence = {}; 
}

// Dirigeant 3 SARL

if ((Value('id').of(dirigeantSARL3.personneLieeQualite).eq('30') or Value('id').of(dirigeantSARL3.personneLieeQualite).eq('40'))
	or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') 
	and Value('id').of(dirigeantSARL3.personneLieeObjet).eq('dirigeantPartant') 
	and dirigeantSARL3.personneLieeQualiteAncienne2 != null)){					

//Groupe DIU
if(Value('id').of(dirigeantSARL3.personneLieeObjet).eq('dirigeantNouveau')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(dirigeantSARL3.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(dirigeantSARL3.personneLieeObjet).eq('dirigeantModif')) {
occurrence['DIU/U50/U50.1'] = "3" ;
} else if(Value('id').of(dirigeantSARL3.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U50/U50.1'] = "4" ;
}
if (conjoint3.personneLieePPNomNaissanceConjointPacse != null or Value('id').of(dirigeantSARL3.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutAssocie')) {
occurrence['DIU/U50/U50.3']= "O";
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null and modifFormeJuridique.formeJuridiqueModifDirigeant) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and dirigeantSARL3.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(dirigeantSARL3.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= dirigeantSARL3.personneLieeQualite != null ? (Value('id').of(dirigeantSARL3.personneLieeQualite).eq('40') ? "40" : "30") : dirigeantSARL3.personneLieeQualiteAncienne2.getId();
if (Value('id').of(dirigeantSARL3.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U51/U51.2']= dirigeantSARL3.personneLieeQualiteAncienne.getId();
}
occurrence['DIU/U52']= "P";

// Groupe IPD
occurrence['IPD/D01/D01.2']= dirigeantSARL3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
var prenoms=[];
for ( i = 0; i < dirigeantSARL3.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantSARL3.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}      
occurrence['IPD/D01/D01.3']= prenoms;
if (dirigeantSARL3.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['IPD/D01/D01.4']= dirigeantSARL3.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
if(Value('id').of(dirigeantSARL3.personneLieeObjet).eq('dirigeantNouveau')) {
if(dirigeantSARL3.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
var dateTemp = new Date(parseInt(dirigeantSARL3.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['IPD/D03/D03.1']= date;
}
if (not Value('id').of(dirigeantSARL3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeantSARL3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeantSARL3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
occurrence['IPD/D03/D03.2'] = dirigeantSARL3.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}	
if (not Value('id').of(dirigeantSARL3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
occurrence['IPD/D03/D03.3']= dirigeantSARL3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeantSARL3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
occurrence['IPD/D03/D03.4']= dirigeantSARL3.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
}
if (not Value('id').of(dirigeantSARL3.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(dirigeantSARL3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.3']= dirigeantSARL3.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantSARL3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantSARL3.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['IPD/D04/D04.3']= result;
}
if (dirigeantSARL3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['IPD/D04/D04.5'] = dirigeantSARL3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantSARL3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
var monId1 = Value('id').of(dirigeantSARL3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
occurrence['IPD/D04/D04.6']          = monId1;
}
if (dirigeantSARL3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['IPD/D04/D04.7'] = dirigeantSARL3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['IPD/D04/D04.8'] = dirigeantSARL3.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantSARL3.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantSARL3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['IPD/D04/D04.10'] = dirigeantSARL3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantSARL3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
var monId1 = Value('id').of(dirigeantSARL3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
occurrence['IPD/D04/D04.11']          = monId1;
}
occurrence['IPD/D04/D04.12'] = dirigeantSARL3.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantSARL3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeantSARL3.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantSARL3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeantSARL3.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantSARL3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.14']= dirigeantSARL3.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD
occurrence['NPD/D21']= dirigeantSARL3.civilitePersonnePhysique.personneLieePPNationaliteGerant;
}

// Groupe SCD
if (conjoint3.personneLieePPNomNaissanceConjointPacse != null or Value('id').of(dirigeantSARL3.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutAssocie')) {
if (Value('id').of(dirigeantSARL3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') or Value('id').of(conjoint3.statutModifConjoint).eq('modifStatutCollaborateur')) {
occurrence['SCD/D40']= "1";
} else if (Value('id').of(dirigeantSARL3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie') or Value('id').of(dirigeantSARL3.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutAssocie')) {
occurrence['SCD/D40']= "2";
} else if (Value('id').of(dirigeantSARL3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutSalarie') or Value('id').of(dirigeantSARL3.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutSalarie') or Value('id').of(conjoint3.statutModifConjoint).eq('modifStatutSalarie')) {
occurrence['SCD/D40']= "3";
}
occurrence['SCD/D41']= Value('id').of(conjoint3.objetModifConjoint).eq('suppressionMentionConjoint') ? "S" : "O";
}

// Groupe ICD
if (Value('id').of(dirigeantSARL3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') or Value('id').of(conjoint3.statutModifConjoint).eq('modifStatutCollaborateur')) {
occurrence['ICD/D42/D42.2'] = conjoint3.personneLieePPNomNaissanceConjointPacse;
var prenoms=[];
for ( i = 0; i < conjoint3.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(conjoint3.personneLieePPPrenomConjointPacse[i]);}      
occurrence['ICD/D42/D42.3']= prenoms;
if (conjoint3.personneLieePPNomUsageConjointPacse != null) {
occurrence['ICD/D42/D42.4']= conjoint3.personneLieePPNomUsageConjointPacse;
}
if (not Value('id').of(conjoint3.objetModifConjoint).eq('suppressionMentionConjoint')) {
if(conjoint3.personneLieePPDateNaissanceConjointPacse != null) {
var dateTemp = new Date(parseInt(conjoint3.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['ICD/D43/D43.1']= date;
}
if (not Value('id').of(conjoint3.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
	var idPays = conjoint3.personneLieePPPaysNaissanceConjointPacse.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['ICD/D43/D43.2']= result;
} else if (Value('id').of(conjoint3.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
occurrence['ICD/D43/D43.2'] = conjoint3.personneLieePPLieuNaissanceCommnuneConjointPacse.getId();
}	
if (not Value('id').of(conjoint3.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {																					 
occurrence['ICD/D43/D43.3']= conjoint3.personneLieePPPaysNaissanceConjointPacse.getLabel();
}
if (Value('id').of(conjoint3.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {	
occurrence['ICD/D43/D43.4']= conjoint3.personneLieePPLieuNaissanceCommnuneConjointPacse.getLabel();
}
}
if (conjoint3.personneLieePPNationaliteConjointPacse != null) {
occurrence['ICD/D44'] = conjoint3.personneLieePPNationaliteConjointPacse;
}
if (conjoint3.adresseConjointDifferente) {
if (Value('id').of(conjoint3.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['ICD/D45/D45.3']= conjoint3.adresseDomicileConjoint.adresseConjointCommune.getId();
} else if (not Value('id').of(conjoint3.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
		var idPays = conjoint3.adresseDomicileConjoint.adresseConjointPays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['ICD/D45/D45.3']= result;
}
if (conjoint3.adresseDomicileConjoint.adresseConjointNumeroVoie != null) {
occurrence['ICD/D45/D45.5'] = conjoint3.adresseDomicileConjoint.adresseConjointNumeroVoie;
}
if (conjoint3.adresseDomicileConjoint.adresseConjointIndiceVoie != null) {
var monId1 = Value('id').of(conjoint3.adresseDomicileConjoint.adresseConjointIndiceVoie)._eval();
occurrence['ICD/D45/D45.6']          = monId1;
}
if (conjoint3.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null) {
occurrence['ICD/D45/D45.7'] = conjoint3.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie;
}
occurrence['ICD/D45/D45.8'] = conjoint3.adresseDomicileConjoint.adresseConjointCodePostal != null ? conjoint3.adresseDomicileConjoint.adresseConjointCodePostal : ".";
if (conjoint3.adresseDomicileConjoint.adresseConjointComplementVoie != null) {
occurrence['ICD/D45/D45.10'] = conjoint3.adresseDomicileConjoint.adresseConjointComplementVoie;
}
if (conjoint3.adresseDomicileConjoint.adresseConjointTypeVoie != null) {
var monId1 = Value('id').of(conjoint3.adresseDomicileConjoint.adresseConjointTypeVoie)._eval();
occurrence['ICD/D45/D45.11']          = monId1;
}
occurrence['ICD/D45/D45.12'] = conjoint3.adresseDomicileConjoint.adresseConjointNomVoie;
if (Value('id').of(conjoint3.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['ICD/D45/D45.13']= conjoint3.adresseDomicileConjoint.adresseConjointCommune.getLabel();
} else if (not Value('id').of(conjoint3.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['ICD/D45/D45.13']= conjoint3.adresseDomicileConjoint.adresseConjointVille;
}
if (not Value('id').of(conjoint3.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['ICD/D45/D45.14']= conjoint3.adresseDomicileConjoint.adresseConjointPays.getLabel();
}
}
}

// Groupe GCS
if (dirigeantSARL3.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS != null) {
var nirExploitant = dirigeantSARL3.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);
occurrence['GCS/CAS/A42/A42.1']= "."; 
} 
if (socialDirigeant3.voletSocialNumeroSecuriteSocialTNS != null) {

// ISS
var nirExploitant = socialDirigeant3.voletSocialNumeroSecuriteSocialTNS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);

// SNS
if (socialDirigeant3.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant3.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
var dateTemp = new Date(parseInt(socialDirigeant3.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['GCS/SNS/A21/A21.2']= date;
}	
occurrence['GCS/SNS/A21/A21.3']= socialDirigeant3.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['GCS/SNS/A21/A21.4']= socialDirigeant3.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['GCS/SNS/A21/A21.6']= socialDirigeant3.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['GCS/SNS/A22/A22.3']= Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" :
								 (Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
								 (Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : " X"));
if (Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') or Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['GCS/SNS/A22/A22.4']= Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant3.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : "ENIM";
}
if (socialDirigeant3.voletSocialActiviteSimultaneeOUINON) {
occurrence['GCS/SNS/A24/A24.2']= Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" :
								 (Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
								 (Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : " 9"));
if (Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
occurrence['GCS/SNS/A24/A24.3']= socialDirigeant3.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}

// CAS
occurrence['GCS/CAS/A42/A42.1']= "."; 
//occurrence['GCS/CAS/A44']= "."; 

// SCS
if (Value('id').of(dirigeantSARL3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') and not Value('id').of(conjoint3.objetModifConjoint).eq('suppressionMentionConjoint')) {
occurrence['GCS/SCS/A53/A53.4']= conjoint3.voletSocialConjointCouvertAssuranceMaladieOui ? "O" : "N"; 
if (conjoint3.voletSocialConjointCollaborateurNumeroSecuriteSociale != null) {				
var nirExploitant = conjoint3.voletSocialConjointCollaborateurNumeroSecuriteSociale;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/SCS/A55/A55.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/SCS/A55/A55.2']=  nirExploitant.substring(13, 15);
} else if (socialAssocie3.voletSocialNumeroSecuriteSocialTNS != null) {				
var nirExploitant = socialAssocie3.voletSocialNumeroSecuriteSocialTNS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/SCS/A55/A55.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/SCS/A55/A55.2']=  nirExploitant.substring(13, 15);
}
}

// SAS
if (Value('id').of(dirigeantSARL3.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) {
occurrence['SAS/A68/A68.2'] = conjoint3.personneLieePPNomNaissanceConjointPacse;
var prenoms=[];
for ( i = 0; i < conjoint3.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(conjoint3.personneLieePPPrenomConjointPacse[i]);}      
occurrence['SAS/A68/A68.3']= prenoms;
if (conjoint3.personneLieePPNomUsageConjointPacse != null) {
occurrence['SAS/A68/A68.4']= conjoint3.personneLieePPNomUsageConjointPacse;
}
if(conjoint3.personneLieePPDateNaissanceConjointPacse != null) {
var dateTemp = new Date(parseInt(conjoint3.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['SAS/A69/A69.1']= date;
}
if (not Value('id').of(conjoint3.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
	var idPays = conjoint3.personneLieePPPaysNaissanceConjointPacse.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['SAS/A69/A69.2']= result;
} else if (Value('id').of(conjoint3.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
occurrence['SAS/A69/A69.2'] = conjoint3.personneLieePPLieuNaissanceCommnuneConjointPacse.getId();
}	
if (not Value('id').of(conjoint3.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {																					 
occurrence['SAS/A69/A69.3']= conjoint3.personneLieePPPaysNaissanceConjointPacse.getLabel();
}
if (Value('id').of(conjoint3.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {	
occurrence['SAS/A69/A69.4']= conjoint3.personneLieePPLieuNaissanceCommnuneConjointPacse.getLabel();
}
if (conjoint3.personneLieePPNationaliteConjointPacse != null) {
occurrence['SAS/A70'] = conjoint3.personneLieePPNationaliteConjointPacse;
}
if (conjoint3.adresseConjointDifferente) {
if (Value('id').of(conjoint3.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['SAS/A71/A71.3']= conjoint3.adresseDomicileConjoint.adresseConjointCommune.getId();
} else if (not Value('id').of(conjoint3.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
		var idPays = conjoint3.adresseDomicileConjoint.adresseConjointPays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['SAS/A71/A71.3']= result;
}
if (conjoint3.adresseDomicileConjoint.adresseConjointNumeroVoie != null) {
occurrence['SAS/A71/A71.5'] = conjoint3.adresseDomicileConjoint.adresseConjointNumeroVoie;
}
if (conjoint3.adresseDomicileConjoint.adresseConjointIndiceVoie != null) {
var monId1 = Value('id').of(conjoint3.adresseDomicileConjoint.adresseConjointIndiceVoie)._eval();
occurrence['SAS/A71/A71.6']          = monId1;
}
if (conjoint3.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null) {
occurrence['SAS/A71/A71.7'] = conjoint3.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie;
}
occurrence['SAS/A71/A71.8'] = conjoint3.adresseDomicileConjoint.adresseConjointCodePostal != null ? conjoint3.adresseDomicileConjoint.adresseConjointCodePostal : ".";
if (conjoint3.adresseDomicileConjoint.adresseConjointComplementVoie != null) {
occurrence['SAS/A71/A71.10'] = conjoint3.adresseDomicileConjoint.adresseConjointComplementVoie;
}
if (conjoint3.adresseDomicileConjoint.adresseConjointTypeVoie != null) {
var monId1 = Value('id').of(conjoint3.adresseDomicileConjoint.adresseConjointTypeVoie)._eval();
occurrence['SAS/A71/A71.11']          = monId1;
}
occurrence['SAS/A71/A71.12'] = conjoint3.adresseDomicileConjoint.adresseConjointNomVoie;
if (Value('id').of(conjoint3.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['SAS/A71/A71.13']= conjoint3.adresseDomicileConjoint.adresseConjointCommune.getLabel();
} else if (not Value('id').of(conjoint3.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['SAS/A71/A71.13']= conjoint3.adresseDomicileConjoint.adresseConjointVille;
}
if (not Value('id').of(conjoint3.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['SAS/A71/A71.14']= conjoint3.adresseDomicileConjoint.adresseConjointPays.getLabel();
}
}
}
}
occurrencesPersonnePhysiqueDirigeante.push(occurrence);
occurrence = {}; 
}

// Dirigeant 4 SARL

if ((Value('id').of(dirigeantSARL4.personneLieeQualite).eq('30') or Value('id').of(dirigeantSARL4.personneLieeQualite).eq('40')) 
	or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') 
	and Value('id').of(dirigeantSARL4.personneLieeObjet).eq('dirigeantPartant') 
	and dirigeantSARL4.personneLieeQualiteAncienne2 != null)){					

//Groupe DIU
if(Value('id').of(dirigeantSARL4.personneLieeObjet).eq('dirigeantNouveau')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(dirigeantSARL4.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(dirigeantSARL4.personneLieeObjet).eq('dirigeantModif')) {
occurrence['DIU/U50/U50.1'] = "3" ;
} else if(Value('id').of(dirigeantSARL4.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U50/U50.1'] = "4" ;
}
if (conjoint4.personneLieePPNomNaissanceConjointPacse != null or Value('id').of(dirigeantSARL4.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutAssocie')) {
occurrence['DIU/U50/U50.3']= "O";
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null and modifFormeJuridique.formeJuridiqueModifDirigeant) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and dirigeantSARL4.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(dirigeantSARL4.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= dirigeantSARL4.personneLieeQualite != null ? (Value('id').of(dirigeantSARL4.personneLieeQualite).eq('40') ? "40" : "30") : dirigeantSARL4.personneLieeQualiteAncienne2.getId();
if (Value('id').of(dirigeantSARL4.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U51/U51.2']= dirigeantSARL4.personneLieeQualiteAncienne.getId();
}
occurrence['DIU/U52']= "P";

// Groupe IPD
occurrence['IPD/D01/D01.2']= dirigeantSARL4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
var prenoms=[];
for ( i = 0; i < dirigeantSARL4.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantSARL4.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}      
occurrence['IPD/D01/D01.3']= prenoms;
if (dirigeantSARL4.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['IPD/D01/D01.4']= dirigeantSARL4.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
if(Value('id').of(dirigeantSARL4.personneLieeObjet).eq('dirigeantNouveau')) {
if(dirigeantSARL4.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
var dateTemp = new Date(parseInt(dirigeantSARL4.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['IPD/D03/D03.1']= date;
}
if (not Value('id').of(dirigeantSARL4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeantSARL4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeantSARL4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
occurrence['IPD/D03/D03.2'] = dirigeantSARL4.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}	
if (not Value('id').of(dirigeantSARL4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
occurrence['IPD/D03/D03.3']= dirigeantSARL4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeantSARL4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
occurrence['IPD/D03/D03.4']= dirigeantSARL4.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
}
if (not Value('id').of(dirigeantSARL4.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(dirigeantSARL4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.3']= dirigeantSARL4.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantSARL4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantSARL4.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['IPD/D04/D04.3']= result;
}
if (dirigeantSARL4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['IPD/D04/D04.5'] = dirigeantSARL4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantSARL4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
var monId1 = Value('id').of(dirigeantSARL4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
occurrence['IPD/D04/D04.6']          = monId1;
}
if (dirigeantSARL4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['IPD/D04/D04.7'] = dirigeantSARL4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['IPD/D04/D04.8'] = dirigeantSARL4.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantSARL4.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantSARL4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['IPD/D04/D04.10'] = dirigeantSARL4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantSARL4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
var monId1 = Value('id').of(dirigeantSARL4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
occurrence['IPD/D04/D04.11']          = monId1;
}
occurrence['IPD/D04/D04.12'] = dirigeantSARL4.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantSARL4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeantSARL4.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantSARL4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeantSARL4.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantSARL4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.14']= dirigeantSARL4.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD
occurrence['NPD/D21']= dirigeantSARL4.civilitePersonnePhysique.personneLieePPNationaliteGerant;
}

// Groupe SCD
if (conjoint4.personneLieePPNomNaissanceConjointPacse != null or Value('id').of(dirigeantSARL4.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutAssocie')) {
if (Value('id').of(dirigeantSARL4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') or Value('id').of(conjoint4.statutModifConjoint).eq('modifStatutCollaborateur')) {
occurrence['SCD/D40']= "1";
} else if (Value('id').of(dirigeantSARL4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie') or Value('id').of(dirigeantSARL4.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutAssocie')) {
occurrence['SCD/D40']= "2";
} else if (Value('id').of(dirigeantSARL4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutSalarie') or Value('id').of(dirigeantSARL4.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutSalarie') or Value('id').of(conjoint4.statutModifConjoint).eq('modifStatutSalarie')) {
occurrence['SCD/D40']= "3";
}
occurrence['SCD/D41']= Value('id').of(conjoint4.objetModifConjoint).eq('suppressionMentionConjoint') ? "S" : "O";
}

// Groupe ICD
if (Value('id').of(dirigeantSARL4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') or Value('id').of(conjoint4.statutModifConjoint).eq('modifStatutCollaborateur')) {
occurrence['ICD/D42/D42.2'] = conjoint4.personneLieePPNomNaissanceConjointPacse;
var prenoms=[];
for ( i = 0; i < conjoint4.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(conjoint4.personneLieePPPrenomConjointPacse[i]);}      
occurrence['ICD/D42/D42.3']= prenoms;
if (conjoint4.personneLieePPNomUsageConjointPacse != null) {
occurrence['ICD/D42/D42.4']= conjoint4.personneLieePPNomUsageConjointPacse;
}
if (not Value('id').of(conjoint4.objetModifConjoint).eq('suppressionMentionConjoint')) {
if(conjoint4.personneLieePPDateNaissanceConjointPacse != null) {
var dateTemp = new Date(parseInt(conjoint4.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['ICD/D43/D43.1']= date;
}
if (not Value('id').of(conjoint4.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
	var idPays = conjoint4.personneLieePPPaysNaissanceConjointPacse.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['ICD/D43/D43.2']= result;
} else if (Value('id').of(conjoint4.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
occurrence['ICD/D43/D43.2'] = conjoint4.personneLieePPLieuNaissanceCommnuneConjointPacse.getId();
}	
if (not Value('id').of(conjoint4.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {																					 
occurrence['ICD/D43/D43.3']= conjoint4.personneLieePPPaysNaissanceConjointPacse.getLabel();
}
if (Value('id').of(conjoint4.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {	
occurrence['ICD/D43/D43.4']= conjoint4.personneLieePPLieuNaissanceCommnuneConjointPacse.getLabel();
}
}
if (conjoint4.personneLieePPNationaliteConjointPacse != null) {
occurrence['ICD/D44'] = conjoint4.personneLieePPNationaliteConjointPacse;
}
if (conjoint4.adresseConjointDifferente) {
if (Value('id').of(conjoint4.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['ICD/D45/D45.3']= conjoint4.adresseDomicileConjoint.adresseConjointCommune.getId();
} else if (not Value('id').of(conjoint4.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
		var idPays = conjoint4.adresseDomicileConjoint.adresseConjointPays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['ICD/D45/D45.3']= result;
}
if (conjoint4.adresseDomicileConjoint.adresseConjointNumeroVoie != null) {
occurrence['ICD/D45/D45.5'] = conjoint4.adresseDomicileConjoint.adresseConjointNumeroVoie;
}
if (conjoint4.adresseDomicileConjoint.adresseConjointIndiceVoie != null) {
var monId1 = Value('id').of(conjoint4.adresseDomicileConjoint.adresseConjointIndiceVoie)._eval();
occurrence['ICD/D45/D45.6']          = monId1;
}
if (conjoint4.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null) {
occurrence['ICD/D45/D45.7'] = conjoint4.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie;
}
occurrence['ICD/D45/D45.8'] = conjoint4.adresseDomicileConjoint.adresseConjointCodePostal != null ? conjoint4.adresseDomicileConjoint.adresseConjointCodePostal : ".";
if (conjoint4.adresseDomicileConjoint.adresseConjointComplementVoie != null) {
occurrence['ICD/D45/D45.10'] = conjoint4.adresseDomicileConjoint.adresseConjointComplementVoie;
}
if (conjoint4.adresseDomicileConjoint.adresseConjointTypeVoie != null) {
var monId1 = Value('id').of(conjoint4.adresseDomicileConjoint.adresseConjointTypeVoie)._eval();
occurrence['ICD/D45/D45.11']          = monId1;
}
occurrence['ICD/D45/D45.12'] = conjoint4.adresseDomicileConjoint.adresseConjointNomVoie;
if (Value('id').of(conjoint4.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['ICD/D45/D45.13']= conjoint4.adresseDomicileConjoint.adresseConjointCommune.getLabel();
} else if (not Value('id').of(conjoint4.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['ICD/D45/D45.13']= conjoint4.adresseDomicileConjoint.adresseConjointVille;
}
if (not Value('id').of(conjoint4.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['ICD/D45/D45.14']= conjoint4.adresseDomicileConjoint.adresseConjointPays.getLabel();
}
}
}

// Groupe GCS
if (dirigeantSARL4.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS != null) {
var nirExploitant = dirigeantSARL4.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);
occurrence['GCS/CAS/A42/A42.1']= "."; 
} 
if (socialDirigeant4.voletSocialNumeroSecuriteSocialTNS != null) {

// ISS
var nirExploitant = socialDirigeant4.voletSocialNumeroSecuriteSocialTNS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);

// SNS
if (socialDirigeant4.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant4.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
var dateTemp = new Date(parseInt(socialDirigeant4.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['GCS/SNS/A21/A21.2']= date;
}	
occurrence['GCS/SNS/A21/A21.3']= socialDirigeant4.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['GCS/SNS/A21/A21.4']= socialDirigeant4.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['GCS/SNS/A21/A21.6']= socialDirigeant4.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['GCS/SNS/A22/A22.3']= Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" :
								 (Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
								 (Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : " X"));
if (Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') or Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['GCS/SNS/A22/A22.4']= Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant4.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : "ENIM";
}
if (socialDirigeant4.voletSocialActiviteSimultaneeOUINON) {
occurrence['GCS/SNS/A24/A24.2']= Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" :
								 (Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
								 (Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : " 9"));
if (Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
occurrence['GCS/SNS/A24/A24.3']= socialDirigeant4.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}

// CAS
occurrence['GCS/CAS/A42/A42.1']= "."; 
//occurrence['GCS/CAS/A44']= "."; 

// SCS
if (Value('id').of(dirigeantSARL4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') and not Value('id').of(conjoint4.objetModifConjoint).eq('suppressionMentionConjoint')) {
occurrence['GCS/SCS/A53/A53.4']= conjoint4.voletSocialConjointCouvertAssuranceMaladieOui ? "O" : "N"; 
if (conjoint4.voletSocialConjointCollaborateurNumeroSecuriteSociale != null) {				
var nirExploitant = conjoint4.voletSocialConjointCollaborateurNumeroSecuriteSociale;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/SCS/A55/A55.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/SCS/A55/A55.2']=  nirExploitant.substring(13, 15);
} else if (socialAssocie4.voletSocialNumeroSecuriteSocialTNS != null) {				
var nirExploitant = socialAssocie4.voletSocialNumeroSecuriteSocialTNS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/SCS/A55/A55.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/SCS/A55/A55.2']=  nirExploitant.substring(13, 15);
}
}

// SAS
if (Value('id').of(dirigeantSARL4.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) {
occurrence['SAS/A68/A68.2'] = conjoint4.personneLieePPNomNaissanceConjointPacse;
var prenoms=[];
for ( i = 0; i < conjoint4.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(conjoint4.personneLieePPPrenomConjointPacse[i]);}      
occurrence['SAS/A68/A68.3']= prenoms;
if (conjoint4.personneLieePPNomUsageConjointPacse != null) {
occurrence['SAS/A68/A68.4']= conjoint4.personneLieePPNomUsageConjointPacse;
}
if(conjoint4.personneLieePPDateNaissanceConjointPacse != null) {
var dateTemp = new Date(parseInt(conjoint4.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['SAS/A69/A69.1']= date;
}
if (not Value('id').of(conjoint4.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
	var idPays = conjoint4.personneLieePPPaysNaissanceConjointPacse.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['SAS/A69/A69.2']= result;
} else if (Value('id').of(conjoint4.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
occurrence['SAS/A69/A69.2'] = conjoint4.personneLieePPLieuNaissanceCommnuneConjointPacse.getId();
}	
if (not Value('id').of(conjoint4.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {																					 
occurrence['SAS/A69/A69.3']= conjoint4.personneLieePPPaysNaissanceConjointPacse.getLabel();
}
if (Value('id').of(conjoint4.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {	
occurrence['SAS/A69/A69.4']= conjoint4.personneLieePPLieuNaissanceCommnuneConjointPacse.getLabel();
}
if (conjoint4.personneLieePPNationaliteConjointPacse != null) {
occurrence['SAS/A70'] = conjoint4.personneLieePPNationaliteConjointPacse;
}
if (conjoint4.adresseConjointDifferente) {
if (Value('id').of(conjoint4.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['SAS/A71/A71.3']= conjoint4.adresseDomicileConjoint.adresseConjointCommune.getId();
} else if (not Value('id').of(conjoint4.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
		var idPays = conjoint4.adresseDomicileConjoint.adresseConjointPays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['SAS/A71/A71.3']= result;
}
if (conjoint4.adresseDomicileConjoint.adresseConjointNumeroVoie != null) {
occurrence['SAS/A71/A71.5'] = conjoint4.adresseDomicileConjoint.adresseConjointNumeroVoie;
}
if (conjoint4.adresseDomicileConjoint.adresseConjointIndiceVoie != null) {
var monId1 = Value('id').of(conjoint4.adresseDomicileConjoint.adresseConjointIndiceVoie)._eval();
occurrence['SAS/A71/A71.6']          = monId1;
}
if (conjoint4.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null) {
occurrence['SAS/A71/A71.7'] = conjoint4.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie;
}
occurrence['SAS/A71/A71.8'] = conjoint4.adresseDomicileConjoint.adresseConjointCodePostal != null ? conjoint4.adresseDomicileConjoint.adresseConjointCodePostal : ".";
if (conjoint4.adresseDomicileConjoint.adresseConjointComplementVoie != null) {
occurrence['SAS/A71/A71.10'] = conjoint4.adresseDomicileConjoint.adresseConjointComplementVoie;
}
if (conjoint4.adresseDomicileConjoint.adresseConjointTypeVoie != null) {
var monId1 = Value('id').of(conjoint4.adresseDomicileConjoint.adresseConjointTypeVoie)._eval();
occurrence['SAS/A71/A71.11']          = monId1;
}
occurrence['SAS/A71/A71.12'] = conjoint4.adresseDomicileConjoint.adresseConjointNomVoie;
if (Value('id').of(conjoint4.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['SAS/A71/A71.13']= conjoint4.adresseDomicileConjoint.adresseConjointCommune.getLabel();
} else if (not Value('id').of(conjoint4.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['SAS/A71/A71.13']= conjoint4.adresseDomicileConjoint.adresseConjointVille;
}
if (not Value('id').of(conjoint4.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['SAS/A71/A71.14']= conjoint4.adresseDomicileConjoint.adresseConjointPays.getLabel();
}
}
}
}
occurrencesPersonnePhysiqueDirigeante.push(occurrence);
occurrence = {}; 
}

// Dirigeant 5 SARL

if ((Value('id').of(dirigeantSARL5.personneLieeQualite).eq('30') or Value('id').of(dirigeantSARL5.personneLieeQualite).eq('40'))
	or (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') 
	and Value('id').of(dirigeantSARL5.personneLieeObjet).eq('dirigeantPartant') 
	and dirigeantSARL5.personneLieeQualiteAncienne2 != null))	{					

//Groupe DIU
if(Value('id').of(dirigeantSARL5.personneLieeObjet).eq('dirigeantNouveau')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(dirigeantSARL5.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(dirigeantSARL5.personneLieeObjet).eq('dirigeantModif')) {
occurrence['DIU/U50/U50.1'] = "3" ;
} else if(Value('id').of(dirigeantSARL5.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U50/U50.1'] = "4" ;
}
if (conjoint5.personneLieePPNomNaissanceConjointPacse != null or Value('id').of(dirigeantSARL5.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutAssocie')) {
occurrence['DIU/U50/U50.3']= "O";
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null and modifFormeJuridique.formeJuridiqueModifDirigeant) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and dirigeantSARL5.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(dirigeantSARL5.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= dirigeantSARL5.personneLieeQualite != null ? (Value('id').of(dirigeantSARL5.personneLieeQualite).eq('40') ? "40" : "30") : dirigeantSARL5.personneLieeQualiteAncienne2.getId();
if (Value('id').of(dirigeantSARL5.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U51/U51.2']= dirigeantSARL5.personneLieeQualiteAncienne.getId();
}
occurrence['DIU/U52']= "P";

// Groupe IPD
occurrence['IPD/D01/D01.2']= dirigeantSARL5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
var prenoms=[];
for ( i = 0; i < dirigeantSARL5.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeantSARL5.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}      
occurrence['IPD/D01/D01.3']= prenoms;
if (dirigeantSARL5.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
occurrence['IPD/D01/D01.4']= dirigeantSARL5.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
if(Value('id').of(dirigeantSARL5.personneLieeObjet).eq('dirigeantNouveau')) {
if(dirigeantSARL5.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
var dateTemp = new Date(parseInt(dirigeantSARL5.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['IPD/D03/D03.1']= date;
}
if (not Value('id').of(dirigeantSARL5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeantSARL5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeantSARL5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
occurrence['IPD/D03/D03.2'] = dirigeantSARL5.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}	
if (not Value('id').of(dirigeantSARL5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
occurrence['IPD/D03/D03.3']= dirigeantSARL5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeantSARL5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
occurrence['IPD/D03/D03.4']= dirigeantSARL5.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
}
if (not Value('id').of(dirigeantSARL5.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(dirigeantSARL5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.3']= dirigeantSARL5.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeantSARL5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeantSARL5.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['IPD/D04/D04.3']= result;
}
if (dirigeantSARL5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['IPD/D04/D04.5'] = dirigeantSARL5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeantSARL5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
var monId1 = Value('id').of(dirigeantSARL5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
occurrence['IPD/D04/D04.6']          = monId1;
}
if (dirigeantSARL5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['IPD/D04/D04.7'] = dirigeantSARL5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['IPD/D04/D04.8'] = dirigeantSARL5.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeantSARL5.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeantSARL5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['IPD/D04/D04.10'] = dirigeantSARL5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeantSARL5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
var monId1 = Value('id').of(dirigeantSARL5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
occurrence['IPD/D04/D04.11']          = monId1;
}
occurrence['IPD/D04/D04.12'] = dirigeantSARL5.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeantSARL5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeantSARL5.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeantSARL5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeantSARL5.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeantSARL5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.14']= dirigeantSARL5.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD
occurrence['NPD/D21']= dirigeantSARL5.civilitePersonnePhysique.personneLieePPNationaliteGerant;
}

// Groupe SCD
if (conjoint5.personneLieePPNomNaissanceConjointPacse != null or Value('id').of(dirigeantSARL5.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutAssocie')) {
if (Value('id').of(dirigeantSARL5.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') or Value('id').of(conjoint5.statutModifConjoint).eq('modifStatutCollaborateur')) {
occurrence['SCD/D40']= "1";
} else if (Value('id').of(dirigeantSARL5.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie') or Value('id').of(dirigeantSARL5.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutAssocie')) {
occurrence['SCD/D40']= "2";
} else if (Value('id').of(dirigeantSARL5.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutSalarie') or Value('id').of(dirigeantSARL5.statutConjointCollaborateurGeranceNonMaj).eq('personneLieeConjointStatutSalarie') or Value('id').of(conjoint5.statutModifConjoint).eq('modifStatutSalarie')) {
occurrence['SCD/D40']= "3";
}
occurrence['SCD/D41']= Value('id').of(conjoint5.objetModifConjoint).eq('suppressionMentionConjoint') ? "S" : "O";
}

// Groupe ICD
if (Value('id').of(dirigeantSARL5.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') or Value('id').of(conjoint5.statutModifConjoint).eq('modifStatutCollaborateur')) {
occurrence['ICD/D42/D42.2'] = conjoint5.personneLieePPNomNaissanceConjointPacse;
var prenoms=[];
for ( i = 0; i < conjoint5.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(conjoint5.personneLieePPPrenomConjointPacse[i]);}      
occurrence['ICD/D42/D42.3']= prenoms;
if (conjoint5.personneLieePPNomUsageConjointPacse != null) {
occurrence['ICD/D42/D42.4']= conjoint5.personneLieePPNomUsageConjointPacse;
}
if (not Value('id').of(conjoint5.objetModifConjoint).eq('suppressionMentionConjoint')) {
if(conjoint5.personneLieePPDateNaissanceConjointPacse != null) {
var dateTemp = new Date(parseInt(conjoint5.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['ICD/D43/D43.1']= date;
}
if (not Value('id').of(conjoint5.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
	var idPays = conjoint5.personneLieePPPaysNaissanceConjointPacse.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['ICD/D43/D43.2']= result;
} else if (Value('id').of(conjoint5.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
occurrence['ICD/D43/D43.2'] = conjoint5.personneLieePPLieuNaissanceCommnuneConjointPacse.getId();
}	
if (not Value('id').of(conjoint5.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {																					 
occurrence['ICD/D43/D43.3']= conjoint5.personneLieePPPaysNaissanceConjointPacse.getLabel();
}
if (Value('id').of(conjoint5.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {	
occurrence['ICD/D43/D43.4']= conjoint5.personneLieePPLieuNaissanceCommnuneConjointPacse.getLabel();
}
}
if (conjoint5.personneLieePPNationaliteConjointPacse != null) {
occurrence['ICD/D44'] = conjoint5.personneLieePPNationaliteConjointPacse;
}
if (conjoint5.adresseConjointDifferente) {
if (Value('id').of(conjoint5.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['ICD/D45/D45.3']= conjoint5.adresseDomicileConjoint.adresseConjointCommune.getId();
} else if (not Value('id').of(conjoint5.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
		var idPays = conjoint5.adresseDomicileConjoint.adresseConjointPays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['ICD/D45/D45.3']= result;
}
if (conjoint5.adresseDomicileConjoint.adresseConjointNumeroVoie != null) {
occurrence['ICD/D45/D45.5'] = conjoint5.adresseDomicileConjoint.adresseConjointNumeroVoie;
}
if (conjoint5.adresseDomicileConjoint.adresseConjointIndiceVoie != null) {
var monId1 = Value('id').of(conjoint5.adresseDomicileConjoint.adresseConjointIndiceVoie)._eval();
occurrence['ICD/D45/D45.6']          = monId1;
}
if (conjoint5.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null) {
occurrence['ICD/D45/D45.7'] = conjoint5.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie;
}
occurrence['ICD/D45/D45.8'] = conjoint5.adresseDomicileConjoint.adresseConjointCodePostal != null ? conjoint5.adresseDomicileConjoint.adresseConjointCodePostal : ".";
if (conjoint5.adresseDomicileConjoint.adresseConjointComplementVoie != null) {
occurrence['ICD/D45/D45.10'] = conjoint5.adresseDomicileConjoint.adresseConjointComplementVoie;
}
if (conjoint5.adresseDomicileConjoint.adresseConjointTypeVoie != null) {
var monId1 = Value('id').of(conjoint5.adresseDomicileConjoint.adresseConjointTypeVoie)._eval();
occurrence['ICD/D45/D45.11']          = monId1;
}
occurrence['ICD/D45/D45.12'] = conjoint5.adresseDomicileConjoint.adresseConjointNomVoie;
if (Value('id').of(conjoint5.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['ICD/D45/D45.13']= conjoint5.adresseDomicileConjoint.adresseConjointCommune.getLabel();
} else if (not Value('id').of(conjoint5.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['ICD/D45/D45.13']= conjoint5.adresseDomicileConjoint.adresseConjointVille;
}
if (not Value('id').of(conjoint5.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['ICD/D45/D45.14']= conjoint5.adresseDomicileConjoint.adresseConjointPays.getLabel();
}
}
}

// Groupe GCS
if (dirigeantSARL5.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS != null) {
var nirExploitant = dirigeantSARL5.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);
occurrence['GCS/CAS/A42/A42.1']= "."; 
} 
if (socialDirigeant5.voletSocialNumeroSecuriteSocialTNS != null) {

// ISS
var nirExploitant = socialDirigeant5.voletSocialNumeroSecuriteSocialTNS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);

// SNS
if (socialDirigeant5.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant5.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
var dateTemp = new Date(parseInt(socialDirigeant5.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['GCS/SNS/A21/A21.2']= date;
}	
occurrence['GCS/SNS/A21/A21.3']= socialDirigeant5.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['GCS/SNS/A21/A21.4']= socialDirigeant5.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['GCS/SNS/A21/A21.6']= socialDirigeant5.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['GCS/SNS/A22/A22.3']= Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" :
								 (Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
								 (Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : " X"));
if (Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') or Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['GCS/SNS/A22/A22.4']= Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant5.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : "ENIM";
}
if (socialDirigeant5.voletSocialActiviteSimultaneeOUINON) {
occurrence['GCS/SNS/A24/A24.2']= Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" :
								 (Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
								 (Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : " 9"));
if (Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
occurrence['GCS/SNS/A24/A24.3']= socialDirigeant5.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}

// CAS
occurrence['GCS/CAS/A42/A42.1']= "."; 
//occurrence['GCS/CAS/A44']= "."; 

// SCS
if (Value('id').of(dirigeantSARL5.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutCollaborateur') and not Value('id').of(conjoint5.objetModifConjoint).eq('suppressionMentionConjoint')) {
occurrence['GCS/SCS/A53/A53.4']= conjoint5.voletSocialConjointCouvertAssuranceMaladieOui ? "O" : "N"; 
if (conjoint5.voletSocialConjointCollaborateurNumeroSecuriteSociale != null) {				
var nirExploitant = conjoint5.voletSocialConjointCollaborateurNumeroSecuriteSociale;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/SCS/A55/A55.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/SCS/A55/A55.2']=  nirExploitant.substring(13, 15);
} else if (socialAssocie5.voletSocialNumeroSecuriteSocialTNS != null) {				
var nirExploitant = socialAssocie5.voletSocialNumeroSecuriteSocialTNS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/SCS/A55/A55.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/SCS/A55/A55.2']=  nirExploitant.substring(13, 15);
}
}

// SAS
if (Value('id').of(dirigeantSARL5.statutConjointCollaborateurGeranceMaj).eq('personneLieeConjointStatutAssocie')) {
occurrence['SAS/A68/A68.2'] = conjoint5.personneLieePPNomNaissanceConjointPacse;
var prenoms=[];
for ( i = 0; i < conjoint5.personneLieePPPrenomConjointPacse.size() ; i++ ){prenoms.push(conjoint5.personneLieePPPrenomConjointPacse[i]);}      
occurrence['SAS/A68/A68.3']= prenoms;
if (conjoint5.personneLieePPNomUsageConjointPacse != null) {
occurrence['SAS/A68/A68.4']= conjoint5.personneLieePPNomUsageConjointPacse;
}
if(conjoint5.personneLieePPDateNaissanceConjointPacse != null) {
var dateTemp = new Date(parseInt(conjoint5.personneLieePPDateNaissanceConjointPacse.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['SAS/A69/A69.1']= date;
}
if (not Value('id').of(conjoint5.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
	var idPays = conjoint5.personneLieePPPaysNaissanceConjointPacse.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['SAS/A69/A69.2']= result;
} else if (Value('id').of(conjoint5.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {
occurrence['SAS/A69/A69.2'] = conjoint5.personneLieePPLieuNaissanceCommnuneConjointPacse.getId();
}	
if (not Value('id').of(conjoint5.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {																					 
occurrence['SAS/A69/A69.3']= conjoint5.personneLieePPPaysNaissanceConjointPacse.getLabel();
}
if (Value('id').of(conjoint5.personneLieePPPaysNaissanceConjointPacse).eq('FRXXXXX')) {	
occurrence['SAS/A69/A69.4']= conjoint5.personneLieePPLieuNaissanceCommnuneConjointPacse.getLabel();
}
if (conjoint5.personneLieePPNationaliteConjointPacse != null) {
occurrence['SAS/A70'] = conjoint5.personneLieePPNationaliteConjointPacse;
}
if (conjoint5.adresseConjointDifferente) {
if (Value('id').of(conjoint5.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['SAS/A71/A71.3']= conjoint5.adresseDomicileConjoint.adresseConjointCommune.getId();
} else if (not Value('id').of(conjoint5.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
		var idPays = conjoint5.adresseDomicileConjoint.adresseConjointPays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['SAS/A71/A71.3']= result;
}
if (conjoint5.adresseDomicileConjoint.adresseConjointNumeroVoie != null) {
occurrence['SAS/A71/A71.5'] = conjoint5.adresseDomicileConjoint.adresseConjointNumeroVoie;
}
if (conjoint5.adresseDomicileConjoint.adresseConjointIndiceVoie != null) {
var monId1 = Value('id').of(conjoint5.adresseDomicileConjoint.adresseConjointIndiceVoie)._eval();
occurrence['SAS/A71/A71.6']          = monId1;
}
if (conjoint5.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie != null) {
occurrence['SAS/A71/A71.7'] = conjoint5.adresseDomicileConjoint.adresseConjointDistriutionSpecialeVoie;
}
occurrence['SAS/A71/A71.8'] = conjoint5.adresseDomicileConjoint.adresseConjointCodePostal != null ? conjoint5.adresseDomicileConjoint.adresseConjointCodePostal : ".";
if (conjoint5.adresseDomicileConjoint.adresseConjointComplementVoie != null) {
occurrence['SAS/A71/A71.10'] = conjoint5.adresseDomicileConjoint.adresseConjointComplementVoie;
}
if (conjoint5.adresseDomicileConjoint.adresseConjointTypeVoie != null) {
var monId1 = Value('id').of(conjoint5.adresseDomicileConjoint.adresseConjointTypeVoie)._eval();
occurrence['SAS/A71/A71.11']          = monId1;
}
occurrence['SAS/A71/A71.12'] = conjoint5.adresseDomicileConjoint.adresseConjointNomVoie;
if (Value('id').of(conjoint5.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['SAS/A71/A71.13']= conjoint5.adresseDomicileConjoint.adresseConjointCommune.getLabel();
} else if (not Value('id').of(conjoint5.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['SAS/A71/A71.13']= conjoint5.adresseDomicileConjoint.adresseConjointVille;
}
if (not Value('id').of(conjoint5.adresseDomicileConjoint.adresseConjointPays).eq('FRXXXXX')) {
occurrence['SAS/A71/A71.14']= conjoint5.adresseDomicileConjoint.adresseConjointPays.getLabel();
}
}
}
}
occurrencesPersonnePhysiqueDirigeante.push(occurrence);
occurrence = {}; 
}

// Dirigeant 1 Hors SARL

if (not Value('id').of(dirigeant1.personneLieeQualite).eq('66') and dirigeant1.personneLieeQualite != null
	and (dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null or dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)) {

//Groupe DIU
if(Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantNouveau')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantModif')) {
occurrence['DIU/U50/U50.1'] = "3" ;
} else if(Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U50/U50.1'] = "4" ;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and dirigeant1.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(dirigeant1.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= dirigeant1.personneLieeQualite.getId();
if (Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U51/U51.2']= dirigeant1.personneLieeQualiteAncienne.getId();
}
occurrence['DIU/U52']= "P";

// Groupe IPD
occurrence['IPD/D01/D01.2'] = dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant1.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['IPD/D01/D01.3']= prenoms;
} else if (dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['IPD/D01/D01.3']= prenoms;
}
if (dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['IPD/D01/D01.4'] = dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant1.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantNouveau')) {
if(dirigeant1.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant1.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['IPD/D03/D03.1']= date;
} else if(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['IPD/D03/D03.1']= date;
}	
if (not Value('id').of(dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['IPD/D03/D03.2'] = dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['IPD/D03/D03.2'] = dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant1.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['IPD/D03/D03.3'] = dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['IPD/D03/D03.3'] = dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['IPD/D03/D03.4'] = dirigeant1.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['IPD/D03/D03.4'] = dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
}
if (not Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.3']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['IPD/D04/D04.3']= result;
}
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['IPD/D04/D04.5']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['IPD/D04/D04.6']          = monId1;  
} 
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['IPD/D04/D04.7']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['IPD/D04/D04.8']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['IPD/D04/D04.10']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['IPD/D04/D04.11']          = monId1;  
} 
occurrence['IPD/D04/D04.12']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.14']= dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD
occurrence['NPD/D21']= dirigeant1.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant1.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeant1.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;
}

// Groupe GCS
if (dirigeant1.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS != null and Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantPartant')) {
var nirExploitant = dirigeant1.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);
occurrence['GCS/CAS/A42/A42.1']= "."; 
} 
if (socialDirigeant1.voletSocialNumeroSecuriteSocialTNS != null) {

// ISS
var nirExploitant = socialDirigeant1.voletSocialNumeroSecuriteSocialTNS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);

// SNS
if (socialDirigeant1.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant1.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
var dateTemp = new Date(parseInt(socialDirigeant1.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['GCS/SNS/A21/A21.2']= date;
}	
occurrence['GCS/SNS/A21/A21.3']= socialDirigeant1.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['GCS/SNS/A21/A21.4']= socialDirigeant1.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['GCS/SNS/A21/A21.6']= socialDirigeant1.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['GCS/SNS/A22/A22.3']= Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" :
								 (Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
								 (Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : " X"));
if (Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') or Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['GCS/SNS/A22/A22.4']= Value('id').of(socialDirigeant1.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant1.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : "ENIM";
}
if (socialDirigeant1.voletSocialActiviteSimultaneeOUINON) {
occurrence['GCS/SNS/A24/A24.2']= Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" :
								 (Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
								 (Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : " 9"));
if (Value('id').of(socialDirigeant1.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
occurrence['GCS/SNS/A24/A24.3']= socialDirigeant1.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}

// CAS
occurrence['GCS/CAS/A42/A42.1']= "."; 
//occurrence['GCS/CAS/A44']= "."; 
}
occurrencesPersonnePhysiqueDirigeante.push(occurrence);
occurrence = {}; 
}

// Dirigeant 2 Hors SARL

if (not Value('id').of(dirigeant2.personneLieeQualite).eq('66')
	and (dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null or dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)) {

//Groupe DIU
if(Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantNouveau')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantModif')) {
occurrence['DIU/U50/U50.1'] = "3" ;
} else if(Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U50/U50.1'] = "4" ;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and dirigeant2.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(dirigeant2.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= dirigeant2.personneLieeQualite.getId();
if (Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U51/U51.2']= dirigeant2.personneLieeQualiteAncienne.getId();
}
occurrence['DIU/U52']= "P";

// Groupe IPD
occurrence['IPD/D01/D01.2'] = dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant2.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['IPD/D01/D01.3']= prenoms;
} else if (dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['IPD/D01/D01.3']= prenoms;
}
if (dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['IPD/D01/D01.4'] = dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant2.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantNouveau')) {
if(dirigeant2.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant2.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['IPD/D03/D03.1']= date;
} else if(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['IPD/D03/D03.1']= date;
}	
if (not Value('id').of(dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['IPD/D03/D03.2'] = dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['IPD/D03/D03.2'] = dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant2.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['IPD/D03/D03.3'] = dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['IPD/D03/D03.3'] = dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['IPD/D03/D03.4'] = dirigeant2.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['IPD/D03/D03.4'] = dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
}
if (not Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.3']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['IPD/D04/D04.3']= result;
}
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['IPD/D04/D04.5']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['IPD/D04/D04.6']          = monId1;  
} 
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['IPD/D04/D04.7']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['IPD/D04/D04.8']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['IPD/D04/D04.10']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['IPD/D04/D04.11']          = monId1;  
} 
occurrence['IPD/D04/D04.12']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.14']= dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD
occurrence['NPD/D21']= dirigeant2.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant2.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeant2.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;
}

// Groupe GCS
if (dirigeant2.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS != null and Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantPartant')) {
var nirExploitant = dirigeant2.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);
occurrence['GCS/CAS/A42/A42.1']= "."; 
} 
if (socialDirigeant2.voletSocialNumeroSecuriteSocialTNS != null) {

// ISS
var nirExploitant = socialDirigeant2.voletSocialNumeroSecuriteSocialTNS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);

// SNS
if (socialDirigeant2.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant2.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
var dateTemp = new Date(parseInt(socialDirigeant2.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['GCS/SNS/A21/A21.2']= date;
}	
occurrence['GCS/SNS/A21/A21.3']= socialDirigeant2.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['GCS/SNS/A21/A21.4']= socialDirigeant2.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['GCS/SNS/A21/A21.6']= socialDirigeant2.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['GCS/SNS/A22/A22.3']= Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" :
								 (Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
								 (Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : " X"));
if (Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') or Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['GCS/SNS/A22/A22.4']= Value('id').of(socialDirigeant2.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant2.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : "ENIM";
}
if (socialDirigeant2.voletSocialActiviteSimultaneeOUINON) {
occurrence['GCS/SNS/A24/A24.2']= Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" :
								 (Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
								 (Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : " 9"));
if (Value('id').of(socialDirigeant2.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
occurrence['GCS/SNS/A24/A24.3']= socialDirigeant2.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}

// CAS
occurrence['GCS/CAS/A42/A42.1']= "."; 
//occurrence['GCS/CAS/A44']= "."; 
}
occurrencesPersonnePhysiqueDirigeante.push(occurrence);
occurrence = {}; 
}

// Dirigeant 3 Hors SARL

if (not Value('id').of(dirigeant3.personneLieeQualite).eq('66')
	and (dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null or dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)) {

//Groupe DIU
if(Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantNouveau')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantModif')) {
occurrence['DIU/U50/U50.1'] = "3" ;
} else if(Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U50/U50.1'] = "4" ;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and dirigeant3.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(dirigeant3.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= dirigeant3.personneLieeQualite.getId();
if (Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U51/U51.2']= dirigeant3.personneLieeQualiteAncienne.getId();
}
occurrence['DIU/U52']= "P";

// Groupe IPD
occurrence['IPD/D01/D01.2'] = dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant3.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['IPD/D01/D01.3']= prenoms;
} else if (dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['IPD/D01/D01.3']= prenoms;
}
if (dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['IPD/D01/D01.4'] = dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant3.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantNouveau')) {
if(dirigeant3.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant3.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['IPD/D03/D03.1']= date;
} else if(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['IPD/D03/D03.1']= date;
}	
if (not Value('id').of(dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['IPD/D03/D03.2'] = dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['IPD/D03/D03.2'] = dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant3.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['IPD/D03/D03.3'] = dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['IPD/D03/D03.3'] = dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['IPD/D03/D03.4'] = dirigeant3.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['IPD/D03/D03.4'] = dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
}
if (not Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.3']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['IPD/D04/D04.3']= result;
}
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['IPD/D04/D04.5']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['IPD/D04/D04.6']          = monId1;  
} 
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['IPD/D04/D04.7']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['IPD/D04/D04.8']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['IPD/D04/D04.10']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['IPD/D04/D04.11']          = monId1;  
} 
occurrence['IPD/D04/D04.12']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.14']= dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD
occurrence['NPD/D21']= dirigeant3.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant3.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeant3.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;
}

// Groupe GCS
if (dirigeant3.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS != null and Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantPartant')) {
var nirExploitant = dirigeant3.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);
occurrence['GCS/CAS/A42/A42.1']= "."; 
} 
if (socialDirigeant3.voletSocialNumeroSecuriteSocialTNS != null) {

// ISS
var nirExploitant = socialDirigeant3.voletSocialNumeroSecuriteSocialTNS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);

// SNS
if (socialDirigeant3.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant3.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
var dateTemp = new Date(parseInt(socialDirigeant3.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['GCS/SNS/A21/A21.2']= date;
}	
occurrence['GCS/SNS/A21/A21.3']= socialDirigeant3.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['GCS/SNS/A21/A21.4']= socialDirigeant3.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['GCS/SNS/A21/A21.6']= socialDirigeant3.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['GCS/SNS/A22/A22.3']= Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" :
								 (Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
								 (Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : " X"));
if (Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') or Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['GCS/SNS/A22/A22.4']= Value('id').of(socialDirigeant3.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant3.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : "ENIM";
}
if (socialDirigeant3.voletSocialActiviteSimultaneeOUINON) {
occurrence['GCS/SNS/A24/A24.2']= Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" :
								 (Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
								 (Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : " 9"));
if (Value('id').of(socialDirigeant3.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
occurrence['GCS/SNS/A24/A24.3']= socialDirigeant3.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}

// CAS
occurrence['GCS/CAS/A42/A42.1']= "."; 
//occurrence['GCS/CAS/A44']= "."; 
}
occurrencesPersonnePhysiqueDirigeante.push(occurrence);
occurrence = {}; 
}

// Dirigeant 4 Hors SARL

if (not Value('id').of(dirigeant4.personneLieeQualite).eq('66')
	and (dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null or dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)) {

//Groupe DIU
if(Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantNouveau')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantModif')) {
occurrence['DIU/U50/U50.1'] = "3" ;
} else if(Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U50/U50.1'] = "4" ;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and dirigeant4.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(dirigeant4.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= dirigeant4.personneLieeQualite.getId();
if (Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U51/U51.2']= dirigeant4.personneLieeQualiteAncienne.getId();
}
occurrence['DIU/U52']= "P";

// Groupe IPD
occurrence['IPD/D01/D01.2'] = dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant4.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['IPD/D01/D01.3']= prenoms;
} else if (dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['IPD/D01/D01.3']= prenoms;
}
if (dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['IPD/D01/D01.4'] = dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant4.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantNouveau')) {
if(dirigeant4.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant4.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['IPD/D03/D03.1']= date;
} else if(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['IPD/D03/D03.1']= date;
}	
if (not Value('id').of(dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['IPD/D03/D03.2'] = dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['IPD/D03/D03.2'] = dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant4.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['IPD/D03/D03.3'] = dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['IPD/D03/D03.3'] = dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['IPD/D03/D03.4'] = dirigeant4.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['IPD/D03/D03.4'] = dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
}
if (not Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.3']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['IPD/D04/D04.3']= result;
}
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['IPD/D04/D04.5']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['IPD/D04/D04.6']          = monId1;  
} 
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['IPD/D04/D04.7']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['IPD/D04/D04.8']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['IPD/D04/D04.10']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['IPD/D04/D04.11']          = monId1;  
} 
occurrence['IPD/D04/D04.12']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.14']= dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD
occurrence['NPD/D21']= dirigeant4.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant4.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeant4.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;
}

// Groupe GCS
if (dirigeant4.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS != null and Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantPartant')) {
var nirExploitant = dirigeant4.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);
occurrence['GCS/CAS/A42/A42.1']= "."; 
} 
if (socialDirigeant4.voletSocialNumeroSecuriteSocialTNS != null) {

// ISS
var nirExploitant = socialDirigeant4.voletSocialNumeroSecuriteSocialTNS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);

// SNS
if (socialDirigeant4.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant4.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
var dateTemp = new Date(parseInt(socialDirigeant4.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['GCS/SNS/A21/A21.2']= date;
}	
occurrence['GCS/SNS/A21/A21.3']= socialDirigeant4.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['GCS/SNS/A21/A21.4']= socialDirigeant4.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['GCS/SNS/A21/A21.6']= socialDirigeant4.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['GCS/SNS/A22/A22.3']= Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" :
								 (Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
								 (Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : " X"));
if (Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') or Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['GCS/SNS/A22/A22.4']= Value('id').of(socialDirigeant4.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant4.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : "ENIM";
}
if (socialDirigeant4.voletSocialActiviteSimultaneeOUINON) {
occurrence['GCS/SNS/A24/A24.2']= Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" :
								 (Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
								 (Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : " 9"));
if (Value('id').of(socialDirigeant4.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
occurrence['GCS/SNS/A24/A24.3']= socialDirigeant4.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}

// CAS
occurrence['GCS/CAS/A42/A42.1']= "."; 
//occurrence['GCS/CAS/A44']= "."; 
}
occurrencesPersonnePhysiqueDirigeante.push(occurrence);
occurrence = {}; 
}

// Dirigeant 5 Hors SARL

if (not Value('id').of(dirigeant5.personneLieeQualite).eq('66')
	and (dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null or dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)) {

//Groupe DIU
if(Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantNouveau')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantModif')) {
occurrence['DIU/U50/U50.1'] = "3" ;
} else if(Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U50/U50.1'] = "4" ;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and dirigeant5.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(dirigeant5.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= dirigeant5.personneLieeQualite.getId();
if (Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U51/U51.2']= dirigeant5.personneLieeQualiteAncienne.getId();
}
occurrence['DIU/U52']= "P";

// Groupe IPD
occurrence['IPD/D01/D01.2'] = dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant5.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['IPD/D01/D01.3']= prenoms;
} else if (dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['IPD/D01/D01.3']= prenoms;
}
if (dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['IPD/D01/D01.4'] = dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant5.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantNouveau')) {
if(dirigeant5.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant5.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['IPD/D03/D03.1']= date;
} else if(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['IPD/D03/D03.1']= date;
}	
if (not Value('id').of(dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['IPD/D03/D03.2'] = dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['IPD/D03/D03.2'] = dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant5.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['IPD/D03/D03.3'] = dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['IPD/D03/D03.3'] = dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['IPD/D03/D03.4'] = dirigeant5.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['IPD/D03/D03.4'] = dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
}
if (not Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.3']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['IPD/D04/D04.3']= result;
}
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['IPD/D04/D04.5']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['IPD/D04/D04.6']          = monId1;  
} 
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['IPD/D04/D04.7']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['IPD/D04/D04.8']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['IPD/D04/D04.10']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['IPD/D04/D04.11']          = monId1;  
} 
occurrence['IPD/D04/D04.12']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.14']= dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD
occurrence['NPD/D21']= dirigeant5.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant5.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeant5.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;
}

// Groupe GCS
if (dirigeant5.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS != null and Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantPartant')) {
var nirExploitant = dirigeant5.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);
occurrence['GCS/CAS/A42/A42.1']= "."; 
} 
if (socialDirigeant5.voletSocialNumeroSecuriteSocialTNS != null) {

// ISS
var nirExploitant = socialDirigeant5.voletSocialNumeroSecuriteSocialTNS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);

// SNS
if (socialDirigeant5.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant5.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
var dateTemp = new Date(parseInt(socialDirigeant5.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['GCS/SNS/A21/A21.2']= date;
}	
occurrence['GCS/SNS/A21/A21.3']= socialDirigeant5.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['GCS/SNS/A21/A21.4']= socialDirigeant5.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['GCS/SNS/A21/A21.6']= socialDirigeant5.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['GCS/SNS/A22/A22.3']= Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" :
								 (Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
								 (Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : " X"));
if (Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') or Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['GCS/SNS/A22/A22.4']= Value('id').of(socialDirigeant5.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant5.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : "ENIM";
}
if (socialDirigeant5.voletSocialActiviteSimultaneeOUINON) {
occurrence['GCS/SNS/A24/A24.2']= Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" :
								 (Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
								 (Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : " 9"));
if (Value('id').of(socialDirigeant5.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
occurrence['GCS/SNS/A24/A24.3']= socialDirigeant5.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}

// CAS
occurrence['GCS/CAS/A42/A42.1']= "."; 
//occurrence['GCS/CAS/A44']= "."; 
}
occurrencesPersonnePhysiqueDirigeante.push(occurrence);
occurrence = {}; 
}

// Dirigeant 6 Hors SARL

if (not Value('id').of(dirigeant6.personneLieeQualite).eq('66')
	and (dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null or dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)) {

//Groupe DIU
if(Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantNouveau')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantModif')) {
occurrence['DIU/U50/U50.1'] = "3" ;
} else if(Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U50/U50.1'] = "4" ;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and dirigeant6.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(dirigeant6.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= dirigeant6.personneLieeQualite.getId();
if (Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U51/U51.2']= dirigeant6.personneLieeQualiteAncienne.getId();
}
occurrence['DIU/U52']= "P";

// Groupe IPD
occurrence['IPD/D01/D01.2'] = dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant6.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['IPD/D01/D01.3']= prenoms;
} else if (dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['IPD/D01/D01.3']= prenoms;
}
if (dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['IPD/D01/D01.4'] = dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant6.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantNouveau')) {
if(dirigeant6.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant6.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['IPD/D03/D03.1']= date;
} else if(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['IPD/D03/D03.1']= date;
}	
if (not Value('id').of(dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['IPD/D03/D03.2'] = dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['IPD/D03/D03.2'] = dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant6.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['IPD/D03/D03.3'] = dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['IPD/D03/D03.3'] = dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['IPD/D03/D03.4'] = dirigeant6.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['IPD/D03/D03.4'] = dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
}
if (not Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.3']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['IPD/D04/D04.3']= result;
}
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['IPD/D04/D04.5']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['IPD/D04/D04.6']          = monId1;  
} 
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['IPD/D04/D04.7']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['IPD/D04/D04.8']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['IPD/D04/D04.10']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['IPD/D04/D04.11']          = monId1;  
} 
occurrence['IPD/D04/D04.12']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.14']= dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD
occurrence['NPD/D21']= dirigeant6.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant6.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeant6.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;
}

// Groupe GCS
if (dirigeant6.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS != null and Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantPartant')) {
var nirExploitant = dirigeant6.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);
occurrence['GCS/CAS/A42/A42.1']= "."; 
} 
if (socialDirigeant6.voletSocialNumeroSecuriteSocialTNS != null) {

// ISS
var nirExploitant = socialDirigeant6.voletSocialNumeroSecuriteSocialTNS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);

// SNS
if (socialDirigeant6.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant6.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
var dateTemp = new Date(parseInt(socialDirigeant6.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['GCS/SNS/A21/A21.2']= date;
}	
occurrence['GCS/SNS/A21/A21.3']= socialDirigeant6.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['GCS/SNS/A21/A21.4']= socialDirigeant6.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['GCS/SNS/A21/A21.6']= socialDirigeant6.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['GCS/SNS/A22/A22.3']= Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" :
								 (Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
								 (Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : " X"));
if (Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') or Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['GCS/SNS/A22/A22.4']= Value('id').of(socialDirigeant6.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant6.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : "ENIM";
}
if (socialDirigeant6.voletSocialActiviteSimultaneeOUINON) {
occurrence['GCS/SNS/A24/A24.2']= Value('id').of(socialDirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" :
								 (Value('id').of(socialDirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
								 (Value('id').of(socialDirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : " 9"));
if (Value('id').of(socialDirigeant6.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
occurrence['GCS/SNS/A24/A24.3']= socialDirigeant6.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}

// CAS
occurrence['GCS/CAS/A42/A42.1']= "."; 
//occurrence['GCS/CAS/A44']= "."; 
}
occurrencesPersonnePhysiqueDirigeante.push(occurrence);
occurrence = {}; 
}

// Dirigeant 7 Hors SARL

if (not Value('id').of(dirigeant7.personneLieeQualite).eq('66')
	and (dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null or dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)) {

//Groupe DIU
if(Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantNouveau')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantModif')) {
occurrence['DIU/U50/U50.1'] = "3" ;
} else if(Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U50/U50.1'] = "4" ;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and dirigeant7.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(dirigeant7.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= dirigeant7.personneLieeQualite.getId();
if (Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U51/U51.2']= dirigeant7.personneLieeQualiteAncienne.getId();
}
occurrence['DIU/U52']= "P";

// Groupe IPD
occurrence['IPD/D01/D01.2'] = dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant7.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['IPD/D01/D01.3']= prenoms;
} else if (dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['IPD/D01/D01.3']= prenoms;
}
if (dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['IPD/D01/D01.4'] = dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant7.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantNouveau')) {
if(dirigeant7.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant7.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['IPD/D03/D03.1']= date;
} else if(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['IPD/D03/D03.1']= date;
}	
if (not Value('id').of(dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['IPD/D03/D03.2'] = dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['IPD/D03/D03.2'] = dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant7.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['IPD/D03/D03.3'] = dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['IPD/D03/D03.3'] = dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['IPD/D03/D03.4'] = dirigeant7.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['IPD/D03/D03.4'] = dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
}
if (not Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.3']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['IPD/D04/D04.3']= result;
}
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['IPD/D04/D04.5']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['IPD/D04/D04.6']          = monId1;  
} 
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['IPD/D04/D04.7']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['IPD/D04/D04.8']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['IPD/D04/D04.10']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['IPD/D04/D04.11']          = monId1;  
} 
occurrence['IPD/D04/D04.12']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.14']= dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD
occurrence['NPD/D21']= dirigeant7.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant7.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeant7.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;
}

// Groupe GCS
if (dirigeant7.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS != null and Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantPartant')) {
var nirExploitant = dirigeant7.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);
occurrence['GCS/CAS/A42/A42.1']= "."; 
} 
if (socialDirigeant7.voletSocialNumeroSecuriteSocialTNS != null) {

// ISS
var nirExploitant = socialDirigeant7.voletSocialNumeroSecuriteSocialTNS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);

// SNS
if (socialDirigeant7.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant7.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
var dateTemp = new Date(parseInt(socialDirigeant7.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['GCS/SNS/A21/A21.2']= date;
}	
occurrence['GCS/SNS/A21/A21.3']= socialDirigeant7.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['GCS/SNS/A21/A21.4']= socialDirigeant7.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['GCS/SNS/A21/A21.6']= socialDirigeant7.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['GCS/SNS/A22/A22.3']= Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" :
								 (Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
								 (Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : " X"));
if (Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') or Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['GCS/SNS/A22/A22.4']= Value('id').of(socialDirigeant7.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant7.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : "ENIM";
}
if (socialDirigeant7.voletSocialActiviteSimultaneeOUINON) {
occurrence['GCS/SNS/A24/A24.2']= Value('id').of(socialDirigeant7.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" :
								 (Value('id').of(socialDirigeant7.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
								 (Value('id').of(socialDirigeant7.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : " 9"));
if (Value('id').of(socialDirigeant7.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
occurrence['GCS/SNS/A24/A24.3']= socialDirigeant7.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}

// CAS
occurrence['GCS/CAS/A42/A42.1']= "."; 
//occurrence['GCS/CAS/A44']= "."; 
}
occurrencesPersonnePhysiqueDirigeante.push(occurrence);
occurrence = {}; 
}

// Dirigeant 8 Hors SARL

if (not Value('id').of(dirigeant8.personneLieeQualite).eq('66')
	and (dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null or dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)) {

//Groupe DIU
if(Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantNouveau')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantModif')) {
occurrence['DIU/U50/U50.1'] = "3" ;
} else if(Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U50/U50.1'] = "4" ;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and dirigeant8.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(dirigeant8.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= dirigeant8.personneLieeQualite.getId();
if (Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U51/U51.2']= dirigeant8.personneLieeQualiteAncienne.getId();
}
occurrence['DIU/U52']= "P";

// Groupe IPD
occurrence['IPD/D01/D01.2'] = dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant8.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['IPD/D01/D01.3']= prenoms;
} else if (dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['IPD/D01/D01.3']= prenoms;
}
if (dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['IPD/D01/D01.4'] = dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant8.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantNouveau')) {
if(dirigeant8.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant8.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['IPD/D03/D03.1']= date;
} else if(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['IPD/D03/D03.1']= date;
}	
if (not Value('id').of(dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['IPD/D03/D03.2'] = dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['IPD/D03/D03.2'] = dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant8.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['IPD/D03/D03.3'] = dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['IPD/D03/D03.3'] = dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['IPD/D03/D03.4'] = dirigeant8.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['IPD/D03/D03.4'] = dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
}
if (not Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.3']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['IPD/D04/D04.3']= result;
}
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['IPD/D04/D04.5']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['IPD/D04/D04.6']          = monId1;  
} 
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['IPD/D04/D04.7']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['IPD/D04/D04.8']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['IPD/D04/D04.10']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['IPD/D04/D04.11']          = monId1;  
} 
occurrence['IPD/D04/D04.12']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.14']= dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD
occurrence['NPD/D21']= dirigeant8.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant8.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeant8.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;
}

// Groupe GCS
if (dirigeant8.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS != null and Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantPartant')) {
var nirExploitant = dirigeant8.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);
occurrence['GCS/CAS/A42/A42.1']= "."; 
} 
if (socialDirigeant8.voletSocialNumeroSecuriteSocialTNS != null) {

// ISS
var nirExploitant = socialDirigeant8.voletSocialNumeroSecuriteSocialTNS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);

// SNS
if (socialDirigeant8.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant8.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
var dateTemp = new Date(parseInt(socialDirigeant8.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['GCS/SNS/A21/A21.2']= date;
}	
occurrence['GCS/SNS/A21/A21.3']= socialDirigeant8.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['GCS/SNS/A21/A21.4']= socialDirigeant8.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['GCS/SNS/A21/A21.6']= socialDirigeant8.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['GCS/SNS/A22/A22.3']= Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" :
								 (Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
								 (Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : " X"));
if (Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') or Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['GCS/SNS/A22/A22.4']= Value('id').of(socialDirigeant8.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant8.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : "ENIM";
}
if (socialDirigeant8.voletSocialActiviteSimultaneeOUINON) {
occurrence['GCS/SNS/A24/A24.2']= Value('id').of(socialDirigeant8.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" :
								 (Value('id').of(socialDirigeant8.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
								 (Value('id').of(socialDirigeant8.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : " 9"));
if (Value('id').of(socialDirigeant8.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
occurrence['GCS/SNS/A24/A24.3']= socialDirigeant8.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}

// CAS
occurrence['GCS/CAS/A42/A42.1']= "."; 
//occurrence['GCS/CAS/A44']= "."; 
}
occurrencesPersonnePhysiqueDirigeante.push(occurrence);
occurrence = {}; 
}

// Dirigeant 9 Hors SARL

if (not Value('id').of(dirigeant9.personneLieeQualite).eq('66')
	and (dirigeant9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null or dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)) {

//Groupe DIU
if(Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantNouveau')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantModif')) {
occurrence['DIU/U50/U50.1'] = "3" ;
} else if(Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U50/U50.1'] = "4" ;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and dirigeant9.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(dirigeant9.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= dirigeant9.personneLieeQualite.getId();
if (Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U51/U51.2']= dirigeant9.personneLieeQualiteAncienne.getId();
}
occurrence['DIU/U52']= "P";

// Groupe IPD
occurrence['IPD/D01/D01.2'] = dirigeant9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant9.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['IPD/D01/D01.3']= prenoms;
} else if (dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['IPD/D01/D01.3']= prenoms;
}
if (dirigeant9.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['IPD/D01/D01.4'] = dirigeant9.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant9.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantNouveau')) {
if(dirigeant9.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant9.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['IPD/D03/D03.1']= date;
} else if(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['IPD/D03/D03.1']= date;
}	
if (not Value('id').of(dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['IPD/D03/D03.2'] = dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['IPD/D03/D03.2'] = dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant9.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['IPD/D03/D03.3'] = dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['IPD/D03/D03.3'] = dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['IPD/D03/D03.4'] = dirigeant9.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['IPD/D03/D03.4'] = dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
}
if (not Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.3']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['IPD/D04/D04.3']= result;
}
if (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['IPD/D04/D04.5']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['IPD/D04/D04.6']          = monId1;  
} 
if (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['IPD/D04/D04.7']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['IPD/D04/D04.8']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant9.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['IPD/D04/D04.10']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['IPD/D04/D04.11']          = monId1;  
} 
occurrence['IPD/D04/D04.12']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.14']= dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD
occurrence['NPD/D21']= dirigeant9.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant9.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeant9.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;
}

// Groupe GCS
if (dirigeant9.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS != null and Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantPartant')) {
var nirExploitant = dirigeant9.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);
occurrence['GCS/CAS/A42/A42.1']= "."; 
} 
if (socialDirigeant9.voletSocialNumeroSecuriteSocialTNS != null) {

// ISS
var nirExploitant = socialDirigeant9.voletSocialNumeroSecuriteSocialTNS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);

// SNS
if (socialDirigeant9.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant9.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
var dateTemp = new Date(parseInt(socialDirigeant9.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['GCS/SNS/A21/A21.2']= date;
}	
occurrence['GCS/SNS/A21/A21.3']= socialDirigeant9.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['GCS/SNS/A21/A21.4']= socialDirigeant9.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['GCS/SNS/A21/A21.6']= socialDirigeant9.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['GCS/SNS/A22/A22.3']= Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" :
								 (Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
								 (Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : " X"));
if (Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') or Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['GCS/SNS/A22/A22.4']= Value('id').of(socialDirigeant9.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant9.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : "ENIM";
}
if (socialDirigeant9.voletSocialActiviteSimultaneeOUINON) {
occurrence['GCS/SNS/A24/A24.2']= Value('id').of(socialDirigeant9.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" :
								 (Value('id').of(socialDirigeant9.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
								 (Value('id').of(socialDirigeant9.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : " 9"));
if (Value('id').of(socialDirigeant9.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
occurrence['GCS/SNS/A24/A24.3']= socialDirigeant9.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}

// CAS
occurrence['GCS/CAS/A42/A42.1']= "."; 
//occurrence['GCS/CAS/A44']= "."; 
}
occurrencesPersonnePhysiqueDirigeante.push(occurrence);
occurrence = {}; 
}

// Dirigeant 10 Hors SARL

if (not Value('id').of(dirigeant10.personneLieeQualite).eq('66')
	and (dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null or dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)) {

//Groupe DIU
if(Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantNouveau')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantModif')) {
occurrence['DIU/U50/U50.1'] = "3" ;
} else if(Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U50/U50.1'] = "4" ;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and dirigeant10.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(dirigeant10.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= dirigeant10.personneLieeQualite.getId();
if (Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U51/U51.2']= dirigeant10.personneLieeQualiteAncienne.getId();
}
occurrence['DIU/U52']= "P";

// Groupe IPD
occurrence['IPD/D01/D01.2'] = dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant10.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['IPD/D01/D01.3']= prenoms;
} else if (dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['IPD/D01/D01.3']= prenoms;
}
if (dirigeant10.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['IPD/D01/D01.4'] = dirigeant10.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant10.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantNouveau')) {
if(dirigeant10.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant10.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['IPD/D03/D03.1']= date;
} else if(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['IPD/D03/D03.1']= date;
}	
if (not Value('id').of(dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['IPD/D03/D03.2'] = dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['IPD/D03/D03.2'] = dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant10.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['IPD/D03/D03.3'] = dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['IPD/D03/D03.3'] = dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['IPD/D03/D03.4'] = dirigeant10.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['IPD/D03/D03.4'] = dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
}
if (not Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.3']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['IPD/D04/D04.3']= result;
}
if (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['IPD/D04/D04.5']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['IPD/D04/D04.6']          = monId1;  
} 
if (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['IPD/D04/D04.7']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['IPD/D04/D04.8']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant10.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['IPD/D04/D04.10']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['IPD/D04/D04.11']          = monId1;  
} 
occurrence['IPD/D04/D04.12']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.14']= dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD
occurrence['NPD/D21']= dirigeant10.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant10.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeant10.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;
}

// Groupe GCS
if (dirigeant10.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS != null and Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantPartant')) {
var nirExploitant = dirigeant10.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);
occurrence['GCS/CAS/A42/A42.1']= "."; 
} 
if (socialDirigeant10.voletSocialNumeroSecuriteSocialTNS != null) {

// ISS
var nirExploitant = socialDirigeant10.voletSocialNumeroSecuriteSocialTNS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);

// SNS
if (socialDirigeant10.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant10.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
var dateTemp = new Date(parseInt(socialDirigeant10.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['GCS/SNS/A21/A21.2']= date;
}	
occurrence['GCS/SNS/A21/A21.3']= socialDirigeant10.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['GCS/SNS/A21/A21.4']= socialDirigeant10.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['GCS/SNS/A21/A21.6']= socialDirigeant10.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['GCS/SNS/A22/A22.3']= Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" :
								 (Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
								 (Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : " X"));
if (Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') or Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['GCS/SNS/A22/A22.4']= Value('id').of(socialDirigeant10.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant10.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : "ENIM";
}
if (socialDirigeant10.voletSocialActiviteSimultaneeOUINON) {
occurrence['GCS/SNS/A24/A24.2']= Value('id').of(socialDirigeant10.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" :
								 (Value('id').of(socialDirigeant10.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
								 (Value('id').of(socialDirigeant10.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : " 9"));
if (Value('id').of(socialDirigeant10.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
occurrence['GCS/SNS/A24/A24.3']= socialDirigeant10.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}

// CAS
occurrence['GCS/CAS/A42/A42.1']= "."; 
//occurrence['GCS/CAS/A44']= "."; 
}
occurrencesPersonnePhysiqueDirigeante.push(occurrence);
occurrence = {}; 
}

// Dirigeant 11 Hors SARL

if (not Value('id').of(dirigeant11.personneLieeQualite).eq('66')
	and (dirigeant11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null or dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)) {

//Groupe DIU
if(Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantNouveau')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantModif')) {
occurrence['DIU/U50/U50.1'] = "3" ;
} else if(Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U50/U50.1'] = "4" ;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and dirigeant11.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(dirigeant11.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= dirigeant11.personneLieeQualite.getId();
if (Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U51/U51.2']= dirigeant11.personneLieeQualiteAncienne.getId();
}
occurrence['DIU/U52']= "P";

// Groupe IPD
occurrence['IPD/D01/D01.2'] = dirigeant11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant11.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['IPD/D01/D01.3']= prenoms;
} else if (dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['IPD/D01/D01.3']= prenoms;
}
if (dirigeant11.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['IPD/D01/D01.4'] = dirigeant11.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant11.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantNouveau')) {
if(dirigeant11.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant11.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['IPD/D03/D03.1']= date;
} else if(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['IPD/D03/D03.1']= date;
}	
if (not Value('id').of(dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['IPD/D03/D03.2'] = dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['IPD/D03/D03.2'] = dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant11.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['IPD/D03/D03.3'] = dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['IPD/D03/D03.3'] = dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['IPD/D03/D03.4'] = dirigeant11.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['IPD/D03/D03.4'] = dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
}
if (not Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.3']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['IPD/D04/D04.3']= result;
}
if (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['IPD/D04/D04.5']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['IPD/D04/D04.6']          = monId1;  
} 
if (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['IPD/D04/D04.7']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['IPD/D04/D04.8']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant11.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['IPD/D04/D04.10']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['IPD/D04/D04.11']          = monId1;  
} 
occurrence['IPD/D04/D04.12']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.14']= dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD
occurrence['NPD/D21']= dirigeant11.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant11.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeant11.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;
}

// Groupe GCS
if (dirigeant11.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS != null and Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantPartant')) {
var nirExploitant = dirigeant11.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);
occurrence['GCS/CAS/A42/A42.1']= "."; 
} 
if (socialDirigeant11.voletSocialNumeroSecuriteSocialTNS != null) {

// ISS
var nirExploitant = socialDirigeant11.voletSocialNumeroSecuriteSocialTNS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);

// SNS
if (socialDirigeant11.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant11.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
var dateTemp = new Date(parseInt(socialDirigeant11.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['GCS/SNS/A21/A21.2']= date;
}	
occurrence['GCS/SNS/A21/A21.3']= socialDirigeant11.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['GCS/SNS/A21/A21.4']= socialDirigeant11.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['GCS/SNS/A21/A21.6']= socialDirigeant11.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['GCS/SNS/A22/A22.3']= Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" :
								 (Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
								 (Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : " X"));
if (Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') or Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['GCS/SNS/A22/A22.4']= Value('id').of(socialDirigeant11.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant11.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : "ENIM";
}
if (socialDirigeant11.voletSocialActiviteSimultaneeOUINON) {
occurrence['GCS/SNS/A24/A24.2']= Value('id').of(socialDirigeant11.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" :
								 (Value('id').of(socialDirigeant11.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
								 (Value('id').of(socialDirigeant11.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : " 9"));
if (Value('id').of(socialDirigeant11.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
occurrence['GCS/SNS/A24/A24.3']= socialDirigeant11.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}

// CAS
occurrence['GCS/CAS/A42/A42.1']= "."; 
//occurrence['GCS/CAS/A44']= "."; 
}
occurrencesPersonnePhysiqueDirigeante.push(occurrence);
occurrence = {}; 
}

// Dirigeant 12 Hors SARL

if (not Value('id').of(dirigeant12.personneLieeQualite).eq('66')
	and (dirigeant12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null or dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null)) {

//Groupe DIU
if(Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantNouveau')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantModif')) {
occurrence['DIU/U50/U50.1'] = "3" ;
} else if(Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U50/U50.1'] = "4" ;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.formeJuridiqueModifDirigeant and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and dirigeant12.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(dirigeant12.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= dirigeant12.personneLieeQualite.getId();
if (Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U51/U51.2']= dirigeant12.personneLieeQualiteAncienne.getId();
}
occurrence['DIU/U52']= "P";

// Groupe IPD
occurrence['IPD/D01/D01.2'] = dirigeant12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null ? dirigeant12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant : dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant;
if (dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant != null and dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant[0].length > 0) {
var prenoms=[];
for (i = 0; i < dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant12.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);
} occurrence['IPD/D01/D01.3']= prenoms;
} else if (dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant != null) {
var prenoms=[];
for (i = 0; i < dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPPrenomGerant[i]);
} occurrence['IPD/D01/D01.3']= prenoms;
}
if (dirigeant12.civilitePersonnePhysique.personneLieePPNomUsageGerant != null or dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant != null) {
occurrence['IPD/D01/D01.4'] = dirigeant12.civilitePersonnePhysique.personneLieePPNomUsageGerant != null ? dirigeant12.civilitePersonnePhysique.personneLieePPNomUsageGerant : dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomUsageGerant;
}
if(Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantNouveau')) {
if(dirigeant12.civilitePersonnePhysique.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant12.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['IPD/D03/D03.1']= date;
} else if(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant !== null) {
	var dateTemp = new Date(parseInt(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPDateNaissanceGerant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    occurrence['IPD/D03/D03.1']= date;
}	
if (not Value('id').of(dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {
	var idPays = dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['IPD/D03/D03.2'] = dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
} else if (not Value('id').of(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
	occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	occurrence['IPD/D03/D03.2'] = dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getId();
}		
if (not Value('id').of(dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant12.civilitePersonnePhysique.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['IPD/D03/D03.3'] = dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
} else if (not Value('id').of(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX') and dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNomNaissanceGerant != null) {																					 
	occurrence['IPD/D03/D03.3'] = dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['IPD/D03/D03.4'] = dirigeant12.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
} else if (Value('id').of(dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
	occurrence['IPD/D03/D03.4'] = dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
}
if (not Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.3']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['IPD/D04/D04.3']= result;
}
if (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['IPD/D04/D04.5']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['IPD/D04/D04.6']          = monId1;  
} 
if (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['IPD/D04/D04.7']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['IPD/D04/D04.8']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant12.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['IPD/D04/D04.10']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['IPD/D04/D04.11']          = monId1;  
} 
occurrence['IPD/D04/D04.12']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.13']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['IPD/D04/D04.14']= dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}

// Groupe NPD
occurrence['NPD/D21']= dirigeant12.civilitePersonnePhysique.personneLieePPNationaliteGerant != null ? dirigeant12.civilitePersonnePhysique.personneLieePPNationaliteGerant : dirigeant12.civilitePersonnePhysiqueBIS.personneLieePPNationaliteGerant;
}

// Groupe GCS
if (dirigeant12.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS != null and Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantPartant')) {
var nirExploitant = dirigeant12.civilitePersonnePhysique.personneLieePersonnePhysiqueLieuNumSS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);
occurrence['GCS/CAS/A42/A42.1']= "."; 
} 
if (socialDirigeant12.voletSocialNumeroSecuriteSocialTNS != null) {

// ISS
var nirExploitant = socialDirigeant12.voletSocialNumeroSecuriteSocialTNS;
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);

// SNS
if (socialDirigeant12.voletSocialActiviteExerceeAnterieurementTNS) {
if(socialDirigeant12.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
var dateTemp = new Date(parseInt(socialDirigeant12.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['GCS/SNS/A21/A21.2']= date;
}	
occurrence['GCS/SNS/A21/A21.3']= socialDirigeant12.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['GCS/SNS/A21/A21.4']= socialDirigeant12.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['GCS/SNS/A21/A21.6']= socialDirigeant12.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['GCS/SNS/A22/A22.3']= Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" :
								 (Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
								 (Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : " X"));
if (Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') or Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['GCS/SNS/A22/A22.4']= Value('id').of(socialDirigeant12.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? socialDirigeant12.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : "ENIM";
}
if (socialDirigeant12.voletSocialActiviteSimultaneeOUINON) {
occurrence['GCS/SNS/A24/A24.2']= Value('id').of(socialDirigeant12.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieTNS') ? "1" :
								 (Value('id').of(socialDirigeant12.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeSalarieAgricoleTNS') ? "2" : 
								 (Value('id').of(socialDirigeant12.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeRetraitePensionneTNS') ? "8" : " 9"));
if (Value('id').of(socialDirigeant12.voletSocialActiviteSimultanee).eq('voletSocialActiviteSimultaneeAutreTNS')) {
occurrence['GCS/SNS/A24/A24.3']= socialDirigeant12.voletSocialActiviteAutreQueDeclareeStatutAutreTNS;
}
}

// CAS
occurrence['GCS/CAS/A42/A42.1']= "."; 
//occurrence['GCS/CAS/A44']= "."; 
}
occurrencesPersonnePhysiqueDirigeante.push(occurrence);
occurrence = {}; 
}

occurrencesPersonnePhysiqueDirigeante.forEach(function (value, i) {
	var idx = i+1;
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/DIU/U50/U50.1']= value['DIU/U50/U50.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/DIU/U50/U50.3']= value['DIU/U50/U50.3'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/DIU/U50/U50.2']= value['DIU/U50/U50.2'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/DIU/U51/U51.1']= value['DIU/U51/U51.1'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/DIU/U51/U51.2']= value['DIU/U51/U51.2'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/DIU/U52']= value['DIU/U52'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D01/D01.2']= value['IPD/D01/D01.2'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D01/D01.3']= value['IPD/D01/D01.3'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D01/D01.4']= value['IPD/D01/D01.4'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D03/D03.1']= value['IPD/D03/D03.1'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D03/D03.2']= value['IPD/D03/D03.2'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D03/D03.3']= value['IPD/D03/D03.3'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D03/D03.4']= value['IPD/D03/D03.4'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.3']= value['IPD/D04/D04.3'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.5']= value['IPD/D04/D04.5'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.6']= value['IPD/D04/D04.6'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.7']= value['IPD/D04/D04.7'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.8']= value['IPD/D04/D04.8'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.10']= value['IPD/D04/D04.10'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.11']= value['IPD/D04/D04.11'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.12']= value['IPD/D04/D04.12'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.13']= value['IPD/D04/D04.13'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.14']= value['IPD/D04/D04.14'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/NPD/D21']= value['NPD/D21'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/SCD/D40']= value['SCD/D40'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/SCD/D41']= value['SCD/D41'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/ICD/D42/D42.2']= value['ICD/D42/D42.2'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/ICD/D42/D42.3']= value['ICD/D42/D42.3'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/ICD/D42/D42.4']= value['ICD/D42/D42.4'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/ICD/D43/D43.1']= value['ICD/D43/D43.1'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/ICD/D43/D43.2']= value['ICD/D43/D43.2'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/ICD/D43/D43.3']= value['ICD/D43/D43.3'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/ICD/D43/D43.4']= value['ICD/D43/D43.4'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/ICD/D44']= value['ICD/D44'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/ICD/D45/D45.3']= value['ICD/D45/D45.3'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/ICD/D45/D45.5']= value['ICD/D45/D45.5'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/ICD/D45/D45.6']= value['ICD/D45/D45.6'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/ICD/D45/D45.7']= value['ICD/D45/D45.7'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/ICD/D45/D45.8']= value['ICD/D45/D45.8'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/ICD/D45/D45.10']= value['ICD/D45/D45.10'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/ICD/D45/D45.11']= value['ICD/D45/D45.11'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/ICD/D45/D45.12']= value['ICD/D45/D45.12'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/ICD/D45/D45.13']= value['ICD/D45/D45.13'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/ICD/D45/D45.14']= value['ICD/D45/D45.14'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/ISS/A10/A10.1']= value['GCS/ISS/A10/A10.1'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/ISS/A10/A10.2']= value['GCS/ISS/A10/A10.2'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A21/A21.2']= value['GCS/SNS/A21/A21.2'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A21/A21.3']= value['GCS/SNS/A21/A21.3'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A21/A21.4']= value['GCS/SNS/A21/A21.4'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A21/A21.6']= value['GCS/SNS/A21/A21.6'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A22/A22.3']= value['GCS/SNS/A22/A22.3'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A22/A22.4']= value['GCS/SNS/A22/A22.4'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A24/A24.2']= value['GCS/SNS/A24/A24.2'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A24/A24.3']= value['GCS/SNS/A24/A24.3'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/CAS/A42/A42.1']= value['GCS/CAS/A42/A42.1'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SCS/A53/A53.4']= value['GCS/SCS/A53/A53.4'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SCS/A55/A55.1']= value['GCS/SCS/A55/A55.1'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SCS/A55/A55.2']= value['GCS/SCS/A55/A55.2'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SAS/A68/A68.2']= value['SAS/A68/A68.2'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SAS/A68/A68.3']= value['SAS/A68/A68.3'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SAS/A68/A68.4']= value['SAS/A68/A68.4'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SAS/A69/A69.1']= value['SAS/A69/A69.1'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SAS/A69/A69.2']= value['SAS/A69/A69.2'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SAS/A69/A69.3']= value['SAS/A69/A69.3'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SAS/A69/A69.4']= value['SAS/A69/A69.4'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SAS/A70']= value['SAS/A70'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SAS/A71/A71.3']= value['SAS/A71/A71.3'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SAS/A71/A71.5']= value['SAS/A71/A71.5'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SAS/A71/A71.6']= value['SAS/A71/A71.6'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SAS/A71/A71.7']= value['SAS/A71/A71.7'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SAS/A71/A71.8']= value['SAS/A71/A71.8'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SAS/A71/A71.10']= value['SAS/A71/A71.10'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SAS/A71/A71.11']= value['SAS/A71/A71.11'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SAS/A71/A71.12']= value['SAS/A71/A71.12'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SAS/A71/A71.13']= value['SAS/A71/A71.13'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SAS/A71/A71.14']= value['SAS/A71/A71.14'];	
});

// Fin du 19M	

// Groupe GDR : Dirigeant personne morale

var occurrencesGDR = [];
var occurrence = {};

// Liquidateur			

if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).eq('22M') and not dissolution.dissolutionTransmissionUniverselle and Value('id').of(dissolution.cadreIdentiteLiquidateur.liquidateurPersonnaliteJuridique).eq('personneMorale')) {				

//Groupe DIU
if (dissolution.cadreIdentiteLiquidateur.liquidateurDejaConnu) {
occurrence['DIU/U50/U50.1'] = "4";
} else {
occurrence['DIU/U50/U50.1'] = "1" ;
}
if (dissolution.modifDateDissolution != null) {
var dateTemp = new Date(parseInt(dissolution.modifDateDissolution.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= "40";
if (dissolution.cadreIdentiteLiquidateur.liquidateurDejaConnu) {
occurrence['DIU/U51/U51.2']= dissolution.cadreIdentiteLiquidateur.liquidateurAncienneQualite.getId();
}
occurrence['DIU/U52'] = "M";

// Groupe ICR

occurrence['ICR/R01/R01.1']= dissolution.cadreIdentiteLiquidateur.civilitePersonneMorale.personneLieePMDenomination;
occurrence['ICR/R02/R02.3'] = dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseCommune.getId();
if (dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseNumeroVoie != null) {
occurrence['ICR/R02/R02.5'] = dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseNumeroVoie;
}
if (dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseIndiceVoie != null) {
var monId1 = Value('id').of(dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseIndiceVoie)._eval();
occurrence['ICR/R02/R02.6']          = monId1;
}
if (dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['ICR/R02/R02.7'] = dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['ICR/R02/R02.8'] = dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseCodePostal;
if (dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseComplementAdresse != null) {
occurrence['ICR/R02/R02.10'] = dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseComplementAdresse;
}
if (dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseTypeVoie != null) {
var monId1 = Value('id').of(dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseTypeVoie)._eval();
occurrence['ICR/R02/R02.11']          = monId1;
}
occurrence['ICR/R02/R02.12'] = dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseNomVoie;
occurrence['ICR/R02/R02.13'] = dissolution.cadreIdentiteLiquidateur.cadreAdresseLiquidateur.dirigeantAdresseCommune.getLabel();
occurrence['ICR/R03'] = dissolution.cadreIdentiteLiquidateur.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
occurrence['ICR/R22/R22.2'] = dissolution.cadreIdentiteLiquidateur.civilitePersonneMorale.personneLieePMNumeroIdentification;
occurrence['ICR/R22/R22.3'] = dissolution.cadreIdentiteLiquidateur.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 1		

if (Value('id').of(dirigeant1.personnaliteJuridique).eq('personneMorale')) {				

//Groupe DIU
if(Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantNouveau')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantModif')) {
occurrence['DIU/U50/U50.1'] = "3" ;
} else if(Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U50/U50.1'] = "4" ;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and dirigeant1.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(dirigeant1.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= dirigeant1.personneLieeQualite.getId();
if (Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U51/U51.2']= dirigeant1.personneLieeQualiteAncienne.getId();
}
occurrence['DIU/U52']= "M";

// Groupe ICR

occurrence['ICR/R01/R01.1']= dirigeant1.civilitePersonneMorale.personneLieePMDenomination;
if (not Value('id').of(dirigeant1.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.3']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['ICR/R02/R02.3']= result;
}
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['ICR/R02/R02.5']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['ICR/R02/R02.6']          = monId1;  
} 
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['ICR/R02/R02.7']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['ICR/R02/R02.8']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['ICR/R02/R02.10']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['ICR/R02/R02.11']          = monId1;  
} 
occurrence['ICR/R02/R02.12']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.13']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.13']= dirigeant1.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.14']= dirigeant1.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['ICR/R03'] = dirigeant1.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
if (dirigeant1.civiliteRepresentant.personneLieePPNomNaissance != null) {
if (Value('id').of(dirigeant1.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['ICR/R08'] = "2";
} else if (Value('id').of(dirigeant1.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
occurrence['ICR/R08'] = "3";
} else if (not Value('id').of(dirigeant1.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['ICR/R08'] = "1";
}
occurrence['ICR/R04/R04.2'] = dirigeant1.civiliteRepresentant.personneLieePPNomNaissance;
var prenoms=[];
for (i = 0; i < dirigeant1.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(dirigeant1.civiliteRepresentant.personneLieePPPrenom[i]);
} occurrence['ICR/R04/R04.3']= prenoms;
if (dirigeant1.civiliteRepresentant.personneLieePPNomUsage != null) {
occurrence['ICR/R04/R04.4']= dirigeant1.civiliteRepresentant.personneLieePPNomUsage;
}
if (not Value('id').of(dirigeant1.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['ICR/R05/R05.3']= dirigeant1.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getId();
if (dirigeant1.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null) {
occurrence['ICR/R05/R05.5']= dirigeant1.civiliteRepresentant.adresseRepresentant.representantNumeroVoie;
}
if (dirigeant1.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant1.civiliteRepresentant.adresseRepresentant.representantIndiceVoie)._eval();
   occurrence['ICR/R05/R05.6']          = monId1;  
} 
if (dirigeant1.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null) {
occurrence['ICR/R05/R05.7']= dirigeant1.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale;
}
occurrence['ICR/R05/R05.8']= dirigeant1.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
if (dirigeant1.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null) {
occurrence['ICR/R05/R05.10']= dirigeant1.civiliteRepresentant.adresseRepresentant.representantComplementVoie;
}
if (dirigeant1.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant1.civiliteRepresentant.adresseRepresentant.representantTypeVoie)._eval();
   occurrence['ICR/R05/R05.11']          = monId1;  
} 
occurrence['ICR/R05/R05.12']= dirigeant1.civiliteRepresentant.adresseRepresentant.representantNomVoie;
occurrence['ICR/R05/R05.13']= dirigeant1.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getLabel();
if (dirigeant1.civiliteRepresentant.personneLieePPDateNaissance != null) {
var dateTemp = new Date(parseInt(dirigeant1.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['ICR/R06/R06.1']= date;
}
if (not Value('id').of(dirigeant1.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = dirigeant1.civiliteRepresentant.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['ICR/R06/R06.2']= result;
} else if (Value('id').of(dirigeant1.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
occurrence['ICR/R06/R06.2'] = dirigeant1.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getId();
}	
if (not Value('id').of(dirigeant1.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
occurrence['ICR/R06/R06.3']= dirigeant1.civiliteRepresentant.personneLieePPLieuNaissancePays.getLabel();
}
if (Value('id').of(dirigeant1.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
occurrence['ICR/R06/R06.4']= dirigeant1.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getLabel();
}
occurrence['ICR/R07']= dirigeant1.civiliteRepresentant.personneLieePPNationalite;
}
}
}
occurrence['ICR/R22/R22.2']= dirigeant1.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant1.civilitePersonneMorale.personneLieePMNumeroIdentification : dirigeant1.civilitePersonneMorale.personneLieePMPaysNonImmatriculee;
occurrence['ICR/R22/R22.3']= dirigeant1.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 2		

if (Value('id').of(dirigeant2.personnaliteJuridique).eq('personneMorale')) {				

//Groupe DIU
if(Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantNouveau')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantModif')) {
occurrence['DIU/U50/U50.1'] = "3" ;
} else if(Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U50/U50.1'] = "4" ;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and dirigeant2.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(dirigeant2.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= dirigeant2.personneLieeQualite.getId();
if (Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U51/U51.2']= dirigeant2.personneLieeQualiteAncienne.getId();
}
occurrence['DIU/U52']= "M";

// Groupe ICR

occurrence['ICR/R01/R01.1']= dirigeant2.civilitePersonneMorale.personneLieePMDenomination;
if (not Value('id').of(dirigeant2.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.3']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['ICR/R02/R02.3']= result;
}
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['ICR/R02/R02.5']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['ICR/R02/R02.6']          = monId1;  
} 
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['ICR/R02/R02.7']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['ICR/R02/R02.8']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['ICR/R02/R02.10']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['ICR/R02/R02.11']          = monId1;  
} 
occurrence['ICR/R02/R02.12']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.13']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.13']= dirigeant2.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.14']= dirigeant2.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['ICR/R03'] = dirigeant2.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
if (dirigeant2.civiliteRepresentant.personneLieePPNomNaissance != null) {
if (Value('id').of(dirigeant2.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['ICR/R08'] = "2";
} else if (Value('id').of(dirigeant2.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
occurrence['ICR/R08'] = "3";
} else if (not Value('id').of(dirigeant2.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['ICR/R08'] = "1";
}
occurrence['ICR/R04/R04.2'] = dirigeant2.civiliteRepresentant.personneLieePPNomNaissance;
var prenoms=[];
for (i = 0; i < dirigeant2.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(dirigeant2.civiliteRepresentant.personneLieePPPrenom[i]);
} occurrence['ICR/R04/R04.3']= prenoms;
if (dirigeant2.civiliteRepresentant.personneLieePPNomUsage != null) {
occurrence['ICR/R04/R04.4']= dirigeant2.civiliteRepresentant.personneLieePPNomUsage;
}
if (not Value('id').of(dirigeant2.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['ICR/R05/R05.3']= dirigeant2.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getId();
if (dirigeant2.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null) {
occurrence['ICR/R05/R05.5']= dirigeant2.civiliteRepresentant.adresseRepresentant.representantNumeroVoie;
}
if (dirigeant2.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant2.civiliteRepresentant.adresseRepresentant.representantIndiceVoie)._eval();
   occurrence['ICR/R05/R05.6']          = monId1;  
} 
if (dirigeant2.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null) {
occurrence['ICR/R05/R05.7']= dirigeant2.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale;
}
occurrence['ICR/R05/R05.8']= dirigeant2.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
if (dirigeant2.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null) {
occurrence['ICR/R05/R05.10']= dirigeant2.civiliteRepresentant.adresseRepresentant.representantComplementVoie;
}
if (dirigeant2.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant2.civiliteRepresentant.adresseRepresentant.representantTypeVoie)._eval();
   occurrence['ICR/R05/R05.11']          = monId1;  
} 
occurrence['ICR/R05/R05.12']= dirigeant2.civiliteRepresentant.adresseRepresentant.representantNomVoie;
occurrence['ICR/R05/R05.13']= dirigeant2.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getLabel();
if (dirigeant2.civiliteRepresentant.personneLieePPDateNaissance != null) {
var dateTemp = new Date(parseInt(dirigeant2.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['ICR/R06/R06.1']= date;
}
if (not Value('id').of(dirigeant2.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = dirigeant2.civiliteRepresentant.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['ICR/R06/R06.2']= result;
} else if (Value('id').of(dirigeant2.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
occurrence['ICR/R06/R06.2'] = dirigeant2.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getId();
}	
if (not Value('id').of(dirigeant2.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
occurrence['ICR/R06/R06.3']= dirigeant2.civiliteRepresentant.personneLieePPLieuNaissancePays.getLabel();
}
if (Value('id').of(dirigeant2.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
occurrence['ICR/R06/R06.4']= dirigeant2.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getLabel();
}
occurrence['ICR/R07']= dirigeant2.civiliteRepresentant.personneLieePPNationalite;
}
}
}
occurrence['ICR/R22/R22.2']= dirigeant2.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant2.civilitePersonneMorale.personneLieePMNumeroIdentification : dirigeant2.civilitePersonneMorale.personneLieePMPaysNonImmatriculee;
occurrence['ICR/R22/R22.3']= dirigeant2.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 3		

if (Value('id').of(dirigeant3.personnaliteJuridique).eq('personneMorale')) {				

//Groupe DIU
if(Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantNouveau')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantModif')) {
occurrence['DIU/U50/U50.1'] = "3" ;
} else if(Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U50/U50.1'] = "4" ;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and dirigeant3.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(dirigeant3.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= dirigeant3.personneLieeQualite.getId();
if (Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U51/U51.2']= dirigeant3.personneLieeQualiteAncienne.getId();
}
occurrence['DIU/U52']= "M";

// Groupe ICR

occurrence['ICR/R01/R01.1']= dirigeant3.civilitePersonneMorale.personneLieePMDenomination;
if (not Value('id').of(dirigeant3.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.3']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['ICR/R02/R02.3']= result;
}
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['ICR/R02/R02.5']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['ICR/R02/R02.6']          = monId1;  
} 
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['ICR/R02/R02.7']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['ICR/R02/R02.8']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['ICR/R02/R02.10']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['ICR/R02/R02.11']          = monId1;  
} 
occurrence['ICR/R02/R02.12']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.13']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.13']= dirigeant3.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.14']= dirigeant3.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['ICR/R03'] = dirigeant3.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
if (dirigeant3.civiliteRepresentant.personneLieePPNomNaissance != null) {
if (Value('id').of(dirigeant3.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['ICR/R08'] = "2";
} else if (Value('id').of(dirigeant3.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
occurrence['ICR/R08'] = "3";
} else if (not Value('id').of(dirigeant3.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['ICR/R08'] = "1";
}
occurrence['ICR/R04/R04.2'] = dirigeant3.civiliteRepresentant.personneLieePPNomNaissance;
var prenoms=[];
for (i = 0; i < dirigeant3.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(dirigeant3.civiliteRepresentant.personneLieePPPrenom[i]);
} occurrence['ICR/R04/R04.3']= prenoms;
if (dirigeant3.civiliteRepresentant.personneLieePPNomUsage != null) {
occurrence['ICR/R04/R04.4']= dirigeant3.civiliteRepresentant.personneLieePPNomUsage;
}
if (not Value('id').of(dirigeant3.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['ICR/R05/R05.3']= dirigeant3.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getId();
if (dirigeant3.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null) {
occurrence['ICR/R05/R05.5']= dirigeant3.civiliteRepresentant.adresseRepresentant.representantNumeroVoie;
}
if (dirigeant3.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant3.civiliteRepresentant.adresseRepresentant.representantIndiceVoie)._eval();
   occurrence['ICR/R05/R05.6']          = monId1;  
} 
if (dirigeant3.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null) {
occurrence['ICR/R05/R05.7']= dirigeant3.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale;
}
occurrence['ICR/R05/R05.8']= dirigeant3.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
if (dirigeant3.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null) {
occurrence['ICR/R05/R05.10']= dirigeant3.civiliteRepresentant.adresseRepresentant.representantComplementVoie;
}
if (dirigeant3.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant3.civiliteRepresentant.adresseRepresentant.representantTypeVoie)._eval();
   occurrence['ICR/R05/R05.11']          = monId1;  
} 
occurrence['ICR/R05/R05.12']= dirigeant3.civiliteRepresentant.adresseRepresentant.representantNomVoie;
occurrence['ICR/R05/R05.13']= dirigeant3.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getLabel();
if (dirigeant3.civiliteRepresentant.personneLieePPDateNaissance != null) {
var dateTemp = new Date(parseInt(dirigeant3.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['ICR/R06/R06.1']= date;
}
if (not Value('id').of(dirigeant3.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = dirigeant3.civiliteRepresentant.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['ICR/R06/R06.2']= result;
} else if (Value('id').of(dirigeant3.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
occurrence['ICR/R06/R06.2'] = dirigeant3.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getId();
}	
if (not Value('id').of(dirigeant3.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
occurrence['ICR/R06/R06.3']= dirigeant3.civiliteRepresentant.personneLieePPLieuNaissancePays.getLabel();
}
if (Value('id').of(dirigeant3.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
occurrence['ICR/R06/R06.4']= dirigeant3.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getLabel();
}
occurrence['ICR/R07']= dirigeant3.civiliteRepresentant.personneLieePPNationalite;
}
}
}
occurrence['ICR/R22/R22.2']= dirigeant3.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant3.civilitePersonneMorale.personneLieePMNumeroIdentification : dirigeant3.civilitePersonneMorale.personneLieePMPaysNonImmatriculee;
occurrence['ICR/R22/R22.3']= dirigeant3.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 4		

if (Value('id').of(dirigeant4.personnaliteJuridique).eq('personneMorale')) {				

//Groupe DIU
if(Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantNouveau')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantModif')) {
occurrence['DIU/U50/U50.1'] = "3" ;
} else if(Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U50/U50.1'] = "4" ;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and dirigeant4.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(dirigeant4.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= dirigeant4.personneLieeQualite.getId();
if (Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U51/U51.2']= dirigeant4.personneLieeQualiteAncienne.getId();
}
occurrence['DIU/U52']= "M";

// Groupe ICR

occurrence['ICR/R01/R01.1']= dirigeant4.civilitePersonneMorale.personneLieePMDenomination;
if (not Value('id').of(dirigeant4.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.3']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['ICR/R02/R02.3']= result;
}
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['ICR/R02/R02.5']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['ICR/R02/R02.6']          = monId1;  
} 
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['ICR/R02/R02.7']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['ICR/R02/R02.8']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['ICR/R02/R02.10']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['ICR/R02/R02.11']          = monId1;  
} 
occurrence['ICR/R02/R02.12']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.13']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.13']= dirigeant4.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.14']= dirigeant4.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['ICR/R03'] = dirigeant4.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
if (dirigeant4.civiliteRepresentant.personneLieePPNomNaissance != null) {
if (Value('id').of(dirigeant4.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['ICR/R08'] = "2";
} else if (Value('id').of(dirigeant4.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
occurrence['ICR/R08'] = "3";
} else if (not Value('id').of(dirigeant4.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['ICR/R08'] = "1";
}
occurrence['ICR/R04/R04.2'] = dirigeant4.civiliteRepresentant.personneLieePPNomNaissance;
var prenoms=[];
for (i = 0; i < dirigeant4.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(dirigeant4.civiliteRepresentant.personneLieePPPrenom[i]);
} occurrence['ICR/R04/R04.3']= prenoms;
if (dirigeant4.civiliteRepresentant.personneLieePPNomUsage != null) {
occurrence['ICR/R04/R04.4']= dirigeant4.civiliteRepresentant.personneLieePPNomUsage;
}
if (not Value('id').of(dirigeant4.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['ICR/R05/R05.3']= dirigeant4.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getId();
if (dirigeant4.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null) {
occurrence['ICR/R05/R05.5']= dirigeant4.civiliteRepresentant.adresseRepresentant.representantNumeroVoie;
}
if (dirigeant4.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant4.civiliteRepresentant.adresseRepresentant.representantIndiceVoie)._eval();
   occurrence['ICR/R05/R05.6']          = monId1;  
} 
if (dirigeant4.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null) {
occurrence['ICR/R05/R05.7']= dirigeant4.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale;
}
occurrence['ICR/R05/R05.8']= dirigeant4.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
if (dirigeant4.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null) {
occurrence['ICR/R05/R05.10']= dirigeant4.civiliteRepresentant.adresseRepresentant.representantComplementVoie;
}
if (dirigeant4.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant4.civiliteRepresentant.adresseRepresentant.representantTypeVoie)._eval();
   occurrence['ICR/R05/R05.11']          = monId1;  
} 
occurrence['ICR/R05/R05.12']= dirigeant4.civiliteRepresentant.adresseRepresentant.representantNomVoie;
occurrence['ICR/R05/R05.13']= dirigeant4.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getLabel();
if (dirigeant4.civiliteRepresentant.personneLieePPDateNaissance != null) {
var dateTemp = new Date(parseInt(dirigeant4.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['ICR/R06/R06.1']= date;
}
if (not Value('id').of(dirigeant4.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = dirigeant4.civiliteRepresentant.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['ICR/R06/R06.2']= result;
} else if (Value('id').of(dirigeant4.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
occurrence['ICR/R06/R06.2'] = dirigeant4.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getId();
}	
if (not Value('id').of(dirigeant4.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
occurrence['ICR/R06/R06.3']= dirigeant4.civiliteRepresentant.personneLieePPLieuNaissancePays.getLabel();
}
if (Value('id').of(dirigeant4.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
occurrence['ICR/R06/R06.4']= dirigeant4.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getLabel();
}
occurrence['ICR/R07']= dirigeant4.civiliteRepresentant.personneLieePPNationalite;
}
}
}
occurrence['ICR/R22/R22.2']= dirigeant4.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant4.civilitePersonneMorale.personneLieePMNumeroIdentification : dirigeant4.civilitePersonneMorale.personneLieePMPaysNonImmatriculee;
occurrence['ICR/R22/R22.3']= dirigeant4.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 5		

if (Value('id').of(dirigeant5.personnaliteJuridique).eq('personneMorale')) {				

//Groupe DIU
if(Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantNouveau')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantModif')) {
occurrence['DIU/U50/U50.1'] = "3" ;
} else if(Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U50/U50.1'] = "4" ;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and dirigeant5.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(dirigeant5.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= dirigeant5.personneLieeQualite.getId();
if (Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U51/U51.2']= dirigeant5.personneLieeQualiteAncienne.getId();
}
occurrence['DIU/U52']= "M";

// Groupe ICR

occurrence['ICR/R01/R01.1']= dirigeant5.civilitePersonneMorale.personneLieePMDenomination;
if (not Value('id').of(dirigeant5.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.3']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['ICR/R02/R02.3']= result;
}
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['ICR/R02/R02.5']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['ICR/R02/R02.6']          = monId1;  
} 
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['ICR/R02/R02.7']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['ICR/R02/R02.8']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['ICR/R02/R02.10']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['ICR/R02/R02.11']          = monId1;  
} 
occurrence['ICR/R02/R02.12']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.13']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.13']= dirigeant5.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.14']= dirigeant5.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['ICR/R03'] = dirigeant5.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
if (dirigeant5.civiliteRepresentant.personneLieePPNomNaissance != null) {
if (Value('id').of(dirigeant5.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['ICR/R08'] = "2";
} else if (Value('id').of(dirigeant5.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
occurrence['ICR/R08'] = "3";
} else if (not Value('id').of(dirigeant5.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['ICR/R08'] = "1";
}
occurrence['ICR/R04/R04.2'] = dirigeant5.civiliteRepresentant.personneLieePPNomNaissance;
var prenoms=[];
for (i = 0; i < dirigeant5.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(dirigeant5.civiliteRepresentant.personneLieePPPrenom[i]);
} occurrence['ICR/R04/R04.3']= prenoms;
if (dirigeant5.civiliteRepresentant.personneLieePPNomUsage != null) {
occurrence['ICR/R04/R04.4']= dirigeant5.civiliteRepresentant.personneLieePPNomUsage;
}
if (not Value('id').of(dirigeant5.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['ICR/R05/R05.3']= dirigeant5.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getId();
if (dirigeant5.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null) {
occurrence['ICR/R05/R05.5']= dirigeant5.civiliteRepresentant.adresseRepresentant.representantNumeroVoie;
}
if (dirigeant5.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant5.civiliteRepresentant.adresseRepresentant.representantIndiceVoie)._eval();
   occurrence['ICR/R05/R05.6']          = monId1;  
} 
if (dirigeant5.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null) {
occurrence['ICR/R05/R05.7']= dirigeant5.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale;
}
occurrence['ICR/R05/R05.8']= dirigeant5.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
if (dirigeant5.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null) {
occurrence['ICR/R05/R05.10']= dirigeant5.civiliteRepresentant.adresseRepresentant.representantComplementVoie;
}
if (dirigeant5.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant5.civiliteRepresentant.adresseRepresentant.representantTypeVoie)._eval();
   occurrence['ICR/R05/R05.11']          = monId1;  
} 
occurrence['ICR/R05/R05.12']= dirigeant5.civiliteRepresentant.adresseRepresentant.representantNomVoie;
occurrence['ICR/R05/R05.13']= dirigeant5.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getLabel();
if (dirigeant5.civiliteRepresentant.personneLieePPDateNaissance != null) {
var dateTemp = new Date(parseInt(dirigeant5.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['ICR/R06/R06.1']= date;
}
if (not Value('id').of(dirigeant5.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = dirigeant5.civiliteRepresentant.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['ICR/R06/R06.2']= result;
} else if (Value('id').of(dirigeant5.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
occurrence['ICR/R06/R06.2'] = dirigeant5.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getId();
}	
if (not Value('id').of(dirigeant5.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
occurrence['ICR/R06/R06.3']= dirigeant5.civiliteRepresentant.personneLieePPLieuNaissancePays.getLabel();
}
if (Value('id').of(dirigeant5.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
occurrence['ICR/R06/R06.4']= dirigeant5.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getLabel();
}
occurrence['ICR/R07']= dirigeant5.civiliteRepresentant.personneLieePPNationalite;
}
}
}
occurrence['ICR/R22/R22.2']= dirigeant5.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant5.civilitePersonneMorale.personneLieePMNumeroIdentification : dirigeant5.civilitePersonneMorale.personneLieePMPaysNonImmatriculee;
occurrence['ICR/R22/R22.3']= dirigeant5.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 6		

if (Value('id').of(dirigeant6.personnaliteJuridique).eq('personneMorale')) {				

//Groupe DIU
if(Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantNouveau')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantModif')) {
occurrence['DIU/U50/U50.1'] = "3" ;
} else if(Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U50/U50.1'] = "4" ;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and dirigeant6.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(dirigeant6.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= dirigeant6.personneLieeQualite.getId();
if (Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U51/U51.2']= dirigeant6.personneLieeQualiteAncienne.getId();
}
occurrence['DIU/U52']= "M";

// Groupe ICR

occurrence['ICR/R01/R01.1']= dirigeant6.civilitePersonneMorale.personneLieePMDenomination;
if (not Value('id').of(dirigeant6.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.3']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['ICR/R02/R02.3']= result;
}
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['ICR/R02/R02.5']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['ICR/R02/R02.6']          = monId1;  
} 
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['ICR/R02/R02.7']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['ICR/R02/R02.8']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['ICR/R02/R02.10']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant6.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['ICR/R02/R02.11']          = monId1;  
} 
occurrence['ICR/R02/R02.12']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.13']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.13']= dirigeant6.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.14']= dirigeant6.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['ICR/R03'] = dirigeant6.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
if (dirigeant6.civiliteRepresentant.personneLieePPNomNaissance != null) {
if (Value('id').of(dirigeant6.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['ICR/R08'] = "2";
} else if (Value('id').of(dirigeant6.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
occurrence['ICR/R08'] = "3";
} else if (not Value('id').of(dirigeant6.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['ICR/R08'] = "1";
}
occurrence['ICR/R04/R04.2'] = dirigeant6.civiliteRepresentant.personneLieePPNomNaissance;
var prenoms=[];
for (i = 0; i < dirigeant6.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(dirigeant6.civiliteRepresentant.personneLieePPPrenom[i]);
} occurrence['ICR/R04/R04.3']= prenoms;
if (dirigeant6.civiliteRepresentant.personneLieePPNomUsage != null) {
occurrence['ICR/R04/R04.4']= dirigeant6.civiliteRepresentant.personneLieePPNomUsage;
}
if (not Value('id').of(dirigeant6.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['ICR/R05/R05.3']= dirigeant6.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getId();
if (dirigeant6.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null) {
occurrence['ICR/R05/R05.5']= dirigeant6.civiliteRepresentant.adresseRepresentant.representantNumeroVoie;
}
if (dirigeant6.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant6.civiliteRepresentant.adresseRepresentant.representantIndiceVoie)._eval();
   occurrence['ICR/R05/R05.6']          = monId1;  
} 
if (dirigeant6.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null) {
occurrence['ICR/R05/R05.7']= dirigeant6.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale;
}
occurrence['ICR/R05/R05.8']= dirigeant6.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
if (dirigeant6.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null) {
occurrence['ICR/R05/R05.10']= dirigeant6.civiliteRepresentant.adresseRepresentant.representantComplementVoie;
}
if (dirigeant6.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant6.civiliteRepresentant.adresseRepresentant.representantTypeVoie)._eval();
   occurrence['ICR/R05/R05.11']          = monId1;  
} 
occurrence['ICR/R05/R05.12']= dirigeant6.civiliteRepresentant.adresseRepresentant.representantNomVoie;
occurrence['ICR/R05/R05.13']= dirigeant6.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getLabel();
if (dirigeant6.civiliteRepresentant.personneLieePPDateNaissance != null) {
var dateTemp = new Date(parseInt(dirigeant6.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['ICR/R06/R06.1']= date;
}
if (not Value('id').of(dirigeant6.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = dirigeant6.civiliteRepresentant.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['ICR/R06/R06.2']= result;
} else if (Value('id').of(dirigeant6.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
occurrence['ICR/R06/R06.2'] = dirigeant6.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getId();
}	
if (not Value('id').of(dirigeant6.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
occurrence['ICR/R06/R06.3']= dirigeant6.civiliteRepresentant.personneLieePPLieuNaissancePays.getLabel();
}
if (Value('id').of(dirigeant6.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
occurrence['ICR/R06/R06.4']= dirigeant6.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getLabel();
}
occurrence['ICR/R07']= dirigeant6.civiliteRepresentant.personneLieePPNationalite;
}
}
}
occurrence['ICR/R22/R22.2']= dirigeant6.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant6.civilitePersonneMorale.personneLieePMNumeroIdentification : dirigeant6.civilitePersonneMorale.personneLieePMPaysNonImmatriculee;
occurrence['ICR/R22/R22.3']= dirigeant6.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 7		

if (Value('id').of(dirigeant7.personnaliteJuridique).eq('personneMorale')) {				

//Groupe DIU
if(Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantNouveau')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantModif')) {
occurrence['DIU/U50/U50.1'] = "3" ;
} else if(Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U50/U50.1'] = "4" ;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and dirigeant7.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(dirigeant7.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= dirigeant7.personneLieeQualite.getId();
if (Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U51/U51.2']= dirigeant7.personneLieeQualiteAncienne.getId();
}
occurrence['DIU/U52']= "M";

// Groupe ICR

occurrence['ICR/R01/R01.1']= dirigeant7.civilitePersonneMorale.personneLieePMDenomination;
if (not Value('id').of(dirigeant7.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.3']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['ICR/R02/R02.3']= result;
}
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['ICR/R02/R02.5']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['ICR/R02/R02.6']          = monId1;  
} 
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['ICR/R02/R02.7']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['ICR/R02/R02.8']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['ICR/R02/R02.10']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant7.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['ICR/R02/R02.11']          = monId1;  
} 
occurrence['ICR/R02/R02.12']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.13']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.13']= dirigeant7.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.14']= dirigeant7.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['ICR/R03'] = dirigeant7.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
if (dirigeant7.civiliteRepresentant.personneLieePPNomNaissance != null) {
if (Value('id').of(dirigeant7.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['ICR/R08'] = "2";
} else if (Value('id').of(dirigeant7.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
occurrence['ICR/R08'] = "3";
} else if (not Value('id').of(dirigeant7.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['ICR/R08'] = "1";
}
occurrence['ICR/R04/R04.2'] = dirigeant7.civiliteRepresentant.personneLieePPNomNaissance;
var prenoms=[];
for (i = 0; i < dirigeant7.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(dirigeant7.civiliteRepresentant.personneLieePPPrenom[i]);
} occurrence['ICR/R04/R04.3']= prenoms;
if (dirigeant7.civiliteRepresentant.personneLieePPNomUsage != null) {
occurrence['ICR/R04/R04.4']= dirigeant7.civiliteRepresentant.personneLieePPNomUsage;
}
if (not Value('id').of(dirigeant7.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['ICR/R05/R05.3']= dirigeant7.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getId();
if (dirigeant7.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null) {
occurrence['ICR/R05/R05.5']= dirigeant7.civiliteRepresentant.adresseRepresentant.representantNumeroVoie;
}
if (dirigeant7.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant7.civiliteRepresentant.adresseRepresentant.representantIndiceVoie)._eval();
   occurrence['ICR/R05/R05.6']          = monId1;  
} 
if (dirigeant7.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null) {
occurrence['ICR/R05/R05.7']= dirigeant7.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale;
}
occurrence['ICR/R05/R05.8']= dirigeant7.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
if (dirigeant7.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null) {
occurrence['ICR/R05/R05.10']= dirigeant7.civiliteRepresentant.adresseRepresentant.representantComplementVoie;
}
if (dirigeant7.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant7.civiliteRepresentant.adresseRepresentant.representantTypeVoie)._eval();
   occurrence['ICR/R05/R05.11']          = monId1;  
} 
occurrence['ICR/R05/R05.12']= dirigeant7.civiliteRepresentant.adresseRepresentant.representantNomVoie;
occurrence['ICR/R05/R05.13']= dirigeant7.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getLabel();
if (dirigeant7.civiliteRepresentant.personneLieePPDateNaissance != null) {
var dateTemp = new Date(parseInt(dirigeant7.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['ICR/R06/R06.1']= date;
}
if (not Value('id').of(dirigeant7.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = dirigeant7.civiliteRepresentant.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['ICR/R06/R06.2']= result;
} else if (Value('id').of(dirigeant7.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
occurrence['ICR/R06/R06.2'] = dirigeant7.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getId();
}	
if (not Value('id').of(dirigeant7.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
occurrence['ICR/R06/R06.3']= dirigeant7.civiliteRepresentant.personneLieePPLieuNaissancePays.getLabel();
}
if (Value('id').of(dirigeant7.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
occurrence['ICR/R06/R06.4']= dirigeant7.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getLabel();
}
occurrence['ICR/R07']= dirigeant7.civiliteRepresentant.personneLieePPNationalite;
}
}
}
occurrence['ICR/R22/R22.2']= dirigeant7.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant7.civilitePersonneMorale.personneLieePMNumeroIdentification : dirigeant7.civilitePersonneMorale.personneLieePMPaysNonImmatriculee;
occurrence['ICR/R22/R22.3']= dirigeant7.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 8		

if (Value('id').of(dirigeant8.personnaliteJuridique).eq('personneMorale')) {				

//Groupe DIU
if(Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantNouveau')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantModif')) {
occurrence['DIU/U50/U50.1'] = "3" ;
} else if(Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U50/U50.1'] = "4" ;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and dirigeant8.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(dirigeant8.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= dirigeant8.personneLieeQualite.getId();
if (Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U51/U51.2']= dirigeant8.personneLieeQualiteAncienne.getId();
}
occurrence['DIU/U52']= "M";

// Groupe ICR

occurrence['ICR/R01/R01.1']= dirigeant8.civilitePersonneMorale.personneLieePMDenomination;
if (not Value('id').of(dirigeant8.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.3']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['ICR/R02/R02.3']= result;
}
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['ICR/R02/R02.5']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['ICR/R02/R02.6']          = monId1;  
} 
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['ICR/R02/R02.7']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['ICR/R02/R02.8']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['ICR/R02/R02.10']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant8.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['ICR/R02/R02.11']          = monId1;  
} 
occurrence['ICR/R02/R02.12']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.13']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.13']= dirigeant8.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.14']= dirigeant8.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['ICR/R03'] = dirigeant8.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
if (dirigeant8.civiliteRepresentant.personneLieePPNomNaissance != null) {
if (Value('id').of(dirigeant8.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['ICR/R08'] = "2";
} else if (Value('id').of(dirigeant8.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
occurrence['ICR/R08'] = "3";
} else if (not Value('id').of(dirigeant8.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['ICR/R08'] = "1";
}
occurrence['ICR/R04/R04.2'] = dirigeant8.civiliteRepresentant.personneLieePPNomNaissance;
var prenoms=[];
for (i = 0; i < dirigeant8.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(dirigeant8.civiliteRepresentant.personneLieePPPrenom[i]);
} occurrence['ICR/R04/R04.3']= prenoms;
if (dirigeant8.civiliteRepresentant.personneLieePPNomUsage != null) {
occurrence['ICR/R04/R04.4']= dirigeant8.civiliteRepresentant.personneLieePPNomUsage;
}
if (not Value('id').of(dirigeant8.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['ICR/R05/R05.3']= dirigeant8.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getId();
if (dirigeant8.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null) {
occurrence['ICR/R05/R05.5']= dirigeant8.civiliteRepresentant.adresseRepresentant.representantNumeroVoie;
}
if (dirigeant8.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant8.civiliteRepresentant.adresseRepresentant.representantIndiceVoie)._eval();
   occurrence['ICR/R05/R05.6']          = monId1;  
} 
if (dirigeant8.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null) {
occurrence['ICR/R05/R05.7']= dirigeant8.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale;
}
occurrence['ICR/R05/R05.8']= dirigeant8.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
if (dirigeant8.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null) {
occurrence['ICR/R05/R05.10']= dirigeant8.civiliteRepresentant.adresseRepresentant.representantComplementVoie;
}
if (dirigeant8.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant8.civiliteRepresentant.adresseRepresentant.representantTypeVoie)._eval();
   occurrence['ICR/R05/R05.11']          = monId1;  
} 
occurrence['ICR/R05/R05.12']= dirigeant8.civiliteRepresentant.adresseRepresentant.representantNomVoie;
occurrence['ICR/R05/R05.13']= dirigeant8.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getLabel();
if (dirigeant8.civiliteRepresentant.personneLieePPDateNaissance != null) {
var dateTemp = new Date(parseInt(dirigeant8.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['ICR/R06/R06.1']= date;
}
if (not Value('id').of(dirigeant8.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = dirigeant8.civiliteRepresentant.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['ICR/R06/R06.2']= result;
} else if (Value('id').of(dirigeant8.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
occurrence['ICR/R06/R06.2'] = dirigeant8.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getId();
}	
if (not Value('id').of(dirigeant8.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
occurrence['ICR/R06/R06.3']= dirigeant8.civiliteRepresentant.personneLieePPLieuNaissancePays.getLabel();
}
if (Value('id').of(dirigeant8.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
occurrence['ICR/R06/R06.4']= dirigeant8.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getLabel();
}
occurrence['ICR/R07']= dirigeant8.civiliteRepresentant.personneLieePPNationalite;
}
}
}
occurrence['ICR/R22/R22.2']= dirigeant8.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant8.civilitePersonneMorale.personneLieePMNumeroIdentification : dirigeant8.civilitePersonneMorale.personneLieePMPaysNonImmatriculee;
occurrence['ICR/R22/R22.3']= dirigeant8.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 9		

if (Value('id').of(dirigeant9.personnaliteJuridique).eq('personneMorale')) {				

//Groupe DIU
if(Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantNouveau')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantModif')) {
occurrence['DIU/U50/U50.1'] = "3" ;
} else if(Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U50/U50.1'] = "4" ;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and dirigeant9.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(dirigeant9.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= dirigeant9.personneLieeQualite.getId();
if (Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U51/U51.2']= dirigeant9.personneLieeQualiteAncienne.getId();
}
occurrence['DIU/U52']= "M";

// Groupe ICR

occurrence['ICR/R01/R01.1']= dirigeant9.civilitePersonneMorale.personneLieePMDenomination;
if (not Value('id').of(dirigeant9.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.3']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['ICR/R02/R02.3']= result;
}
if (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['ICR/R02/R02.5']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['ICR/R02/R02.6']          = monId1;  
} 
if (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['ICR/R02/R02.7']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['ICR/R02/R02.8']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant9.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['ICR/R02/R02.10']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant9.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['ICR/R02/R02.11']          = monId1;  
} 
occurrence['ICR/R02/R02.12']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.13']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.13']= dirigeant9.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.14']= dirigeant9.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['ICR/R03'] = dirigeant9.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
if (dirigeant9.civiliteRepresentant.personneLieePPNomNaissance != null) {
if (Value('id').of(dirigeant9.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['ICR/R08'] = "2";
} else if (Value('id').of(dirigeant9.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
occurrence['ICR/R08'] = "3";
} else if (not Value('id').of(dirigeant9.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['ICR/R08'] = "1";
}
occurrence['ICR/R04/R04.2'] = dirigeant9.civiliteRepresentant.personneLieePPNomNaissance;
var prenoms=[];
for (i = 0; i < dirigeant9.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(dirigeant9.civiliteRepresentant.personneLieePPPrenom[i]);
} occurrence['ICR/R04/R04.3']= prenoms;
if (dirigeant9.civiliteRepresentant.personneLieePPNomUsage != null) {
occurrence['ICR/R04/R04.4']= dirigeant9.civiliteRepresentant.personneLieePPNomUsage;
}
if (not Value('id').of(dirigeant9.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['ICR/R05/R05.3']= dirigeant9.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getId();
if (dirigeant9.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null) {
occurrence['ICR/R05/R05.5']= dirigeant9.civiliteRepresentant.adresseRepresentant.representantNumeroVoie;
}
if (dirigeant9.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant9.civiliteRepresentant.adresseRepresentant.representantIndiceVoie)._eval();
   occurrence['ICR/R05/R05.6']          = monId1;  
} 
if (dirigeant9.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null) {
occurrence['ICR/R05/R05.7']= dirigeant9.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale;
}
occurrence['ICR/R05/R05.8']= dirigeant9.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
if (dirigeant9.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null) {
occurrence['ICR/R05/R05.10']= dirigeant9.civiliteRepresentant.adresseRepresentant.representantComplementVoie;
}
if (dirigeant9.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant9.civiliteRepresentant.adresseRepresentant.representantTypeVoie)._eval();
   occurrence['ICR/R05/R05.11']          = monId1;  
} 
occurrence['ICR/R05/R05.12']= dirigeant9.civiliteRepresentant.adresseRepresentant.representantNomVoie;
occurrence['ICR/R05/R05.13']= dirigeant9.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getLabel();
if (dirigeant9.civiliteRepresentant.personneLieePPDateNaissance != null) {
var dateTemp = new Date(parseInt(dirigeant9.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['ICR/R06/R06.1']= date;
}
if (not Value('id').of(dirigeant9.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = dirigeant9.civiliteRepresentant.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['ICR/R06/R06.2']= result;
} else if (Value('id').of(dirigeant9.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
occurrence['ICR/R06/R06.2'] = dirigeant9.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getId();
}	
if (not Value('id').of(dirigeant9.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
occurrence['ICR/R06/R06.3']= dirigeant9.civiliteRepresentant.personneLieePPLieuNaissancePays.getLabel();
}
if (Value('id').of(dirigeant9.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
occurrence['ICR/R06/R06.4']= dirigeant9.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getLabel();
}
occurrence['ICR/R07']= dirigeant9.civiliteRepresentant.personneLieePPNationalite;
}
}
}
occurrence['ICR/R22/R22.2']= dirigeant9.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant9.civilitePersonneMorale.personneLieePMNumeroIdentification : dirigeant9.civilitePersonneMorale.personneLieePMPaysNonImmatriculee;
occurrence['ICR/R22/R22.3']= dirigeant9.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 10		

if (Value('id').of(dirigeant10.personnaliteJuridique).eq('personneMorale')) {				

//Groupe DIU
if(Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantNouveau')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantModif')) {
occurrence['DIU/U50/U50.1'] = "3" ;
} else if(Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U50/U50.1'] = "4" ;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and dirigeant10.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(dirigeant10.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= dirigeant10.personneLieeQualite.getId();
if (Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U51/U51.2']= dirigeant10.personneLieeQualiteAncienne.getId();
}
occurrence['DIU/U52']= "M";

// Groupe ICR

occurrence['ICR/R01/R01.1']= dirigeant10.civilitePersonneMorale.personneLieePMDenomination;
if (not Value('id').of(dirigeant10.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.3']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['ICR/R02/R02.3']= result;
}
if (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['ICR/R02/R02.5']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['ICR/R02/R02.6']          = monId1;  
} 
if (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['ICR/R02/R02.7']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['ICR/R02/R02.8']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant10.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['ICR/R02/R02.10']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant10.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['ICR/R02/R02.11']          = monId1;  
} 
occurrence['ICR/R02/R02.12']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.13']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.13']= dirigeant10.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.14']= dirigeant10.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['ICR/R03'] = dirigeant10.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
if (dirigeant10.civiliteRepresentant.personneLieePPNomNaissance != null) {
if (Value('id').of(dirigeant10.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['ICR/R08'] = "2";
} else if (Value('id').of(dirigeant10.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
occurrence['ICR/R08'] = "3";
} else if (not Value('id').of(dirigeant10.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['ICR/R08'] = "1";
}
occurrence['ICR/R04/R04.2'] = dirigeant10.civiliteRepresentant.personneLieePPNomNaissance;
var prenoms=[];
for (i = 0; i < dirigeant10.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(dirigeant10.civiliteRepresentant.personneLieePPPrenom[i]);
} occurrence['ICR/R04/R04.3']= prenoms;
if (dirigeant10.civiliteRepresentant.personneLieePPNomUsage != null) {
occurrence['ICR/R04/R04.4']= dirigeant10.civiliteRepresentant.personneLieePPNomUsage;
}
if (not Value('id').of(dirigeant10.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['ICR/R05/R05.3']= dirigeant10.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getId();
if (dirigeant10.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null) {
occurrence['ICR/R05/R05.5']= dirigeant10.civiliteRepresentant.adresseRepresentant.representantNumeroVoie;
}
if (dirigeant10.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant10.civiliteRepresentant.adresseRepresentant.representantIndiceVoie)._eval();
   occurrence['ICR/R05/R05.6']          = monId1;  
} 
if (dirigeant10.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null) {
occurrence['ICR/R05/R05.7']= dirigeant10.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale;
}
occurrence['ICR/R05/R05.8']= dirigeant10.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
if (dirigeant10.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null) {
occurrence['ICR/R05/R05.10']= dirigeant10.civiliteRepresentant.adresseRepresentant.representantComplementVoie;
}
if (dirigeant10.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant10.civiliteRepresentant.adresseRepresentant.representantTypeVoie)._eval();
   occurrence['ICR/R05/R05.11']          = monId1;  
} 
occurrence['ICR/R05/R05.12']= dirigeant10.civiliteRepresentant.adresseRepresentant.representantNomVoie;
occurrence['ICR/R05/R05.13']= dirigeant10.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getLabel();
if (dirigeant10.civiliteRepresentant.personneLieePPDateNaissance != null) {
var dateTemp = new Date(parseInt(dirigeant10.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['ICR/R06/R06.1']= date;
}
if (not Value('id').of(dirigeant10.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = dirigeant10.civiliteRepresentant.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['ICR/R06/R06.2']= result;
} else if (Value('id').of(dirigeant10.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
occurrence['ICR/R06/R06.2'] = dirigeant10.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getId();
}	
if (not Value('id').of(dirigeant10.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
occurrence['ICR/R06/R06.3']= dirigeant10.civiliteRepresentant.personneLieePPLieuNaissancePays.getLabel();
}
if (Value('id').of(dirigeant10.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
occurrence['ICR/R06/R06.4']= dirigeant10.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getLabel();
}
occurrence['ICR/R07']= dirigeant10.civiliteRepresentant.personneLieePPNationalite;
}
}
}
occurrence['ICR/R22/R22.2']= dirigeant10.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant10.civilitePersonneMorale.personneLieePMNumeroIdentification : dirigeant10.civilitePersonneMorale.personneLieePMPaysNonImmatriculee;
occurrence['ICR/R22/R22.3']= dirigeant10.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 11		

if (Value('id').of(dirigeant11.personnaliteJuridique).eq('personneMorale')) {				

//Groupe DIU
if(Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantNouveau')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantModif')) {
occurrence['DIU/U50/U50.1'] = "3" ;
} else if(Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U50/U50.1'] = "4" ;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and dirigeant11.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(dirigeant11.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= dirigeant11.personneLieeQualite.getId();
if (Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U51/U51.2']= dirigeant11.personneLieeQualiteAncienne.getId();
}
occurrence['DIU/U52']= "M";

// Groupe ICR

occurrence['ICR/R01/R01.1']= dirigeant11.civilitePersonneMorale.personneLieePMDenomination;
if (not Value('id').of(dirigeant11.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.3']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['ICR/R02/R02.3']= result;
}
if (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['ICR/R02/R02.5']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['ICR/R02/R02.6']          = monId1;  
} 
if (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['ICR/R02/R02.7']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['ICR/R02/R02.8']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant11.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['ICR/R02/R02.10']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant11.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['ICR/R02/R02.11']          = monId1;  
} 
occurrence['ICR/R02/R02.12']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.13']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.13']= dirigeant11.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.14']= dirigeant11.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['ICR/R03'] = dirigeant11.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
if (dirigeant11.civiliteRepresentant.personneLieePPNomNaissance != null) {
if (Value('id').of(dirigeant11.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['ICR/R08'] = "2";
} else if (Value('id').of(dirigeant11.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
occurrence['ICR/R08'] = "3";
} else if (not Value('id').of(dirigeant11.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['ICR/R08'] = "1";
}
occurrence['ICR/R04/R04.2'] = dirigeant11.civiliteRepresentant.personneLieePPNomNaissance;
var prenoms=[];
for (i = 0; i < dirigeant11.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(dirigeant11.civiliteRepresentant.personneLieePPPrenom[i]);
} occurrence['ICR/R04/R04.3']= prenoms;
if (dirigeant11.civiliteRepresentant.personneLieePPNomUsage != null) {
occurrence['ICR/R04/R04.4']= dirigeant11.civiliteRepresentant.personneLieePPNomUsage;
}
if (not Value('id').of(dirigeant11.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['ICR/R05/R05.3']= dirigeant11.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getId();
if (dirigeant11.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null) {
occurrence['ICR/R05/R05.5']= dirigeant11.civiliteRepresentant.adresseRepresentant.representantNumeroVoie;
}
if (dirigeant11.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant11.civiliteRepresentant.adresseRepresentant.representantIndiceVoie)._eval();
   occurrence['ICR/R05/R05.6']          = monId1;  
} 
if (dirigeant11.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null) {
occurrence['ICR/R05/R05.7']= dirigeant11.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale;
}
occurrence['ICR/R05/R05.8']= dirigeant11.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
if (dirigeant11.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null) {
occurrence['ICR/R05/R05.10']= dirigeant11.civiliteRepresentant.adresseRepresentant.representantComplementVoie;
}
if (dirigeant11.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant11.civiliteRepresentant.adresseRepresentant.representantTypeVoie)._eval();
   occurrence['ICR/R05/R05.11']          = monId1;  
} 
occurrence['ICR/R05/R05.12']= dirigeant11.civiliteRepresentant.adresseRepresentant.representantNomVoie;
occurrence['ICR/R05/R05.13']= dirigeant11.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getLabel();
if (dirigeant11.civiliteRepresentant.personneLieePPDateNaissance != null) {
var dateTemp = new Date(parseInt(dirigeant11.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['ICR/R06/R06.1']= date;
}
if (not Value('id').of(dirigeant11.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = dirigeant11.civiliteRepresentant.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['ICR/R06/R06.2']= result;
} else if (Value('id').of(dirigeant11.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
occurrence['ICR/R06/R06.2'] = dirigeant11.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getId();
}	
if (not Value('id').of(dirigeant11.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
occurrence['ICR/R06/R06.3']= dirigeant11.civiliteRepresentant.personneLieePPLieuNaissancePays.getLabel();
}
if (Value('id').of(dirigeant11.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
occurrence['ICR/R06/R06.4']= dirigeant11.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getLabel();
}
occurrence['ICR/R07']= dirigeant11.civiliteRepresentant.personneLieePPNationalite;
}
}
}
occurrence['ICR/R22/R22.2']= dirigeant11.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant11.civilitePersonneMorale.personneLieePMNumeroIdentification : dirigeant11.civilitePersonneMorale.personneLieePMPaysNonImmatriculee;
occurrence['ICR/R22/R22.3']= dirigeant11.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

// Dirigeant 12		

if (Value('id').of(dirigeant12.personnaliteJuridique).eq('personneMorale')) {				

//Groupe DIU
if(Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantNouveau')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantModif')) {
occurrence['DIU/U50/U50.1'] = "3" ;
} else if(Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U50/U50.1'] = "4" ;
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} else if (not Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('22M') and dirigeant12.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(dirigeant12.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}	
occurrence['DIU/U51/U51.1']= dirigeant12.personneLieeQualite.getId();
if (Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['DIU/U51/U51.2']= dirigeant12.personneLieeQualiteAncienne.getId();
}
occurrence['DIU/U52']= "M";

// Groupe ICR

occurrence['ICR/R01/R01.1']= dirigeant12.civilitePersonneMorale.personneLieePMDenomination;
if (not Value('id').of(dirigeant12.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.3']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
		occurrence['ICR/R02/R02.3']= result;
}
if (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
occurrence['ICR/R02/R02.5']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
   occurrence['ICR/R02/R02.6']          = monId1;  
} 
if (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
occurrence['ICR/R02/R02.7']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
occurrence['ICR/R02/R02.8']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? dirigeant12.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
occurrence['ICR/R02/R02.10']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (dirigeant12.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
   occurrence['ICR/R02/R02.11']          = monId1;  
} 
occurrence['ICR/R02/R02.12']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.13']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.13']= dirigeant12.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
occurrence['ICR/R02/R02.14']= dirigeant12.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
occurrence['ICR/R03'] = dirigeant12.civilitePersonneMorale.personneLieePMFormeJuridique.getId();
if (dirigeant12.civiliteRepresentant.personneLieePPNomNaissance != null) {
if (Value('id').of(dirigeant12.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['ICR/R08'] = "2";
} else if (Value('id').of(dirigeant12.civiliteRepresentant.personneLieeObjet).eq('dirigeantModif')) {
occurrence['ICR/R08'] = "3";
} else if (not Value('id').of(dirigeant12.civiliteRepresentant.personneLieeObjet).eq('dirigeantMaintenu')) {
occurrence['ICR/R08'] = "1";
}
occurrence['ICR/R04/R04.2'] = dirigeant12.civiliteRepresentant.personneLieePPNomNaissance;
var prenoms=[];
for (i = 0; i < dirigeant12.civiliteRepresentant.personneLieePPPrenom.size() ; i++ ){prenoms.push(dirigeant12.civiliteRepresentant.personneLieePPPrenom[i]);
} occurrence['ICR/R04/R04.3']= prenoms;
if (dirigeant12.civiliteRepresentant.personneLieePPNomUsage != null) {
occurrence['ICR/R04/R04.4']= dirigeant12.civiliteRepresentant.personneLieePPNomUsage;
}
if (not Value('id').of(dirigeant12.civiliteRepresentant.personneLieeObjet).eq('dirigeantPartant')) {
occurrence['ICR/R05/R05.3']= dirigeant12.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getId();
if (dirigeant12.civiliteRepresentant.adresseRepresentant.representantNumeroVoie != null) {
occurrence['ICR/R05/R05.5']= dirigeant12.civiliteRepresentant.adresseRepresentant.representantNumeroVoie;
}
if (dirigeant12.civiliteRepresentant.adresseRepresentant.representantIndiceVoie != null) {
   var monId1 = Value('id').of(dirigeant12.civiliteRepresentant.adresseRepresentant.representantIndiceVoie)._eval();
   occurrence['ICR/R05/R05.6']          = monId1;  
} 
if (dirigeant12.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale != null) {
occurrence['ICR/R05/R05.7']= dirigeant12.civiliteRepresentant.adresseRepresentant.representantDistributionSpeciale;
}
occurrence['ICR/R05/R05.8']= dirigeant12.civiliteRepresentant.adresseRepresentant.representantAdresseCodePostal;
if (dirigeant12.civiliteRepresentant.adresseRepresentant.representantComplementVoie != null) {
occurrence['ICR/R05/R05.10']= dirigeant12.civiliteRepresentant.adresseRepresentant.representantComplementVoie;
}
if (dirigeant12.civiliteRepresentant.adresseRepresentant.representantTypeVoie != null) {
   var monId1 = Value('id').of(dirigeant12.civiliteRepresentant.adresseRepresentant.representantTypeVoie)._eval();
   occurrence['ICR/R05/R05.11']          = monId1;  
} 
occurrence['ICR/R05/R05.12']= dirigeant12.civiliteRepresentant.adresseRepresentant.representantNomVoie;
occurrence['ICR/R05/R05.13']= dirigeant12.civiliteRepresentant.adresseRepresentant.representantAdresseCommune.getLabel();
if (dirigeant12.civiliteRepresentant.personneLieePPDateNaissance != null) {
var dateTemp = new Date(parseInt(dirigeant12.civiliteRepresentant.personneLieePPDateNaissance.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['ICR/R06/R06.1']= date;
}
if (not Value('id').of(dirigeant12.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = dirigeant12.civiliteRepresentant.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['ICR/R06/R06.2']= result;
} else if (Value('id').of(dirigeant12.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
occurrence['ICR/R06/R06.2'] = dirigeant12.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getId();
}	
if (not Value('id').of(dirigeant12.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
occurrence['ICR/R06/R06.3']= dirigeant12.civiliteRepresentant.personneLieePPLieuNaissancePays.getLabel();
}
if (Value('id').of(dirigeant12.civiliteRepresentant.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
occurrence['ICR/R06/R06.4']= dirigeant12.civiliteRepresentant.personneLieePPLieuNaissanceCommnune.getLabel();
}
occurrence['ICR/R07']= dirigeant12.civiliteRepresentant.personneLieePPNationalite;
}
}
}
occurrence['ICR/R22/R22.2']= dirigeant12.civilitePersonneMorale.personneLieePMNumeroIdentification != null ? dirigeant12.civilitePersonneMorale.personneLieePMNumeroIdentification : dirigeant12.civilitePersonneMorale.personneLieePMPaysNonImmatriculee;
occurrence['ICR/R22/R22.3']= dirigeant12.civilitePersonneMorale.personneLieePMLieuImmatriculation;
occurrencesGDR.push(occurrence);
occurrence = {};
}

occurrencesGDR.forEach(function (value, i) {
	var idx = i+1;
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/DIU/U50/U50.1']= value['DIU/U50/U50.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/DIU/U50/U50.2']= value['DIU/U50/U50.2'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/DIU/U51/U51.1']= value['DIU/U51/U51.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/DIU/U51/U51.2']= value['DIU/U51/U51.2'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/DIU/U52']= value['DIU/U52'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R01/R01.1']= value['ICR/R01/R01.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.3']= value['ICR/R02/R02.3'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.5']= value['ICR/R02/R02.5'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.6']= value['ICR/R02/R02.6'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.7']= value['ICR/R02/R02.7'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.8']= value['ICR/R02/R02.8'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.10']= value['ICR/R02/R02.10'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.11']= value['ICR/R02/R02.11'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.12']= value['ICR/R02/R02.12'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.13']= value['ICR/R02/R02.13'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R02/R02.14']= value['ICR/R02/R02.14'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R03']= value['ICR/R03'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R08']= value['ICR/R08'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R05/R05.3']= value['ICR/R05/R05.3'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R05/R05.5']= value['ICR/R05/R05.5'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R05/R05.6']= value['ICR/R05/R05.6'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R05/R05.7']= value['ICR/R05/R05.7'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R05/R05.8']= value['ICR/R05/R05.8'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R05/R05.10']= value['ICR/R05/R05.10'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R05/R05.11']= value['ICR/R05/R05.11'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R05/R05.12']= value['ICR/R05/R05.12'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R05/R05.13']= value['ICR/R05/R05.13'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R06/R06.1']= value['ICR/R06/R06.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R06/R06.2']= value['ICR/R06/R06.2'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R06/R06.3']= value['ICR/R06/R06.3'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R06/R06.4']= value['ICR/R06/R06.4'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R07']= value['ICR/R07'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R22/R22.2']= value['R22.2'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonneMoraleDirigeante[' + idx + ']/ICR/R22/R22.3']= value['R22.3'];
});

// Fin du 22M / 34M / 35M

	// Groupe ICE : Identification complète de l'établissement 

if (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or activiteInfo.modifActiviteTransfert
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M') 
	or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsreprise')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M')
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(ouverture.etablissementDejaConnu).eq('non'))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60M')
	or activiteInfo.modifEtablissementEnseigne
	or (fondePouvoir.length > 0) or (pouvoir.length > 0)) { 

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M')
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(ouverture.etablissementDejaConnu).eq('non'))
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M')
	and (not objet.cadreObjetModificationEtablissement.lieuSiege
	or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissementBis).eq('activiteSecondaire')))
	or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsreprise')
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
	and not objet.cadreObjetModificationEtablissement.lieuActiviteSiege)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E01']= "1";
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
	and objet.cadreObjetModificationEtablissement.lieuActiviteSiege 
	and Value('id').of(objet.cadreObjetModificationEtablissement.priseRepriseActivite).eq('reprise')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E01']= "4";
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E01']= "3";
}
if (ouverture.cadreAdresseEtablissementOuvert.communeAdresseEtablissement != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.3']= ouverture.cadreAdresseEtablissementOuvert.communeAdresseEtablissement.getId();
if (ouverture.cadreAdresseEtablissementOuvert.numeroAdresseEtablissement != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5']= ouverture.cadreAdresseEtablissementOuvert.numeroAdresseEtablissement;
}
if (ouverture.cadreAdresseEtablissementOuvert.indiceAdresseEtablissement != null) { 
   var monId1 = Value('id').of(ouverture.cadreAdresseEtablissementOuvert.indiceAdresseEtablissement)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.6']          = monId1;
}
if (ouverture.cadreAdresseEtablissementOuvert.distributionSpecialeAdresseEtablissement != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.7']= ouverture.cadreAdresseEtablissementOuvert.distributionSpecialeAdresseEtablissement;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.8']= ouverture.cadreAdresseEtablissementOuvert.codePostalAdresseEtablissement;
if (ouverture.cadreAdresseEtablissementOuvert.complementAdresseEtablissement != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.10']= ouverture.cadreAdresseEtablissementOuvert.complementAdresseEtablissement;
}
if (ouverture.cadreAdresseEtablissementOuvert.typeAdresseEtablissement != null) { 
   var monId1 = Value('id').of(ouverture.cadreAdresseEtablissementOuvert.typeAdresseEtablissement)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.12']= ouverture.cadreAdresseEtablissementOuvert.voieAdresseEtablissement;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.13']= ouverture.cadreAdresseEtablissementOuvert.communeAdresseEtablissement.getLabel();
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.3']= identite.adresseSiege.siegeAdresseCommune.getId();
if (identite.adresseSiege.siegeAdresseNumeroVoie != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5']= identite.adresseSiege.siegeAdresseNumeroVoie;
}
if (identite.adresseSiege.siegeAdresseIndiceVoie != null) { 
   var monId1 = Value('id').of(identite.adresseSiege.siegeAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.6']          = monId1;
}
if (identite.adresseSiege.siegeAdresseDistriutionSpecialeVoie != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.7']= identite.adresseSiege.siegeAdresseDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.8']= identite.adresseSiege.siegeAdresseCodePostal;
if (identite.adresseSiege.siegeAdresseComplementVoie != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.10']= identite.adresseSiege.siegeAdresseComplementVoie;
}
if (identite.adresseSiege.siegeAdresseTypeVoie != null) { 
   var monId1 = Value('id').of(identite.adresseSiege.siegeAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.12']= identite.adresseSiege.siegeAdresseNomVoie;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.13']= identite.adresseSiege.siegeAdresseCommune.getLabel();
}
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E03/E03.1']= "000000000";
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E03/E03.2']= "00000";

if (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')
	and (Value('id').of(identite.entrepriseFormeJuridique).eq('5800') or Value('id').of(identite.entrepriseFormeJuridique).eq('3110')
	or Value('id').of(identite.entrepriseFormeJuridique).eq('3120'))) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E04']= "5";
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E04']= "1";
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal')
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
	and objet.cadreObjetModificationEtablissement.lieuActiviteSiege)
	or (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissementBis).eq('activitePrincipal')
	and objet.cadreObjetModificationEtablissement.lieuSiege)
	or (Value('id').of(objet.cadreObjetModificationEtablissement.fondsDonneEtablissement).eq('fondsDonnePrincipal')
	and fondsDonne.lieuPrincipal)) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E04']= "2";
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite')
	and not objet.cadreObjetModificationEtablissement.lieuActiviteSiege)
	or (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissementBis).eq('activitePrincipal')
	and not objet.cadreObjetModificationEtablissement.lieuSiege)
	or (Value('id').of(objet.cadreObjetModificationEtablissement.fondsDonneEtablissement).eq('fondsDonnePrincipal')
	and not fondsDonne.lieuPrincipal)) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E04']= "3";
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')
	or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissementBis).eq('activiteSecondaire')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.fondsDonneEtablissement).eq('fondsDonneSecondaire')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E04']= "4";
} else if (((pouvoir.length > 0) and Value('id').of(pouvoir[0].cadreIdentiteDirigeant.categoriePouvoirLimite).eq('pouvoirLimiteSiegePrincipal'))
or ((fondePouvoir.length > 0) and Value('id').of(fondePouvoir[0].cadreIdentiteDirigeant.categoriePouvoirLimite).eq('pouvoirLimiteSiegePrincipal')))	{
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E04']= "2";
} else if (((pouvoir.length > 0) and Value('id').of(pouvoir[0].cadreIdentiteDirigeant.categoriePouvoirLimite).eq('pouvoirLimitePrincipal'))
or ((fondePouvoir.length > 0) and Value('id').of(fondePouvoir[0].cadreIdentiteDirigeant.categoriePouvoirLimite).eq('pouvoirLimitePrincipal')))	{
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E04']= "3";
} else if (((pouvoir.length > 0) and Value('id').of(pouvoir[0].cadreIdentiteDirigeant.categoriePouvoirLimite).eq('pouvoirLimiteSecondaire'))
or ((fondePouvoir.length > 0) and Value('id').of(fondePouvoir[0].cadreIdentiteDirigeant.categoriePouvoirLimite).eq('pouvoirLimiteSecondaire')))	{
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E04']= "4";
} else if (((pouvoir.length > 0) and pouvoir[0].cadreIdentiteDirigeant.categoriePouvoirLimite == null)
or ((fondePouvoir.length > 0) and fondePouvoir[0].cadreIdentiteDirigeant.categoriePouvoirLimite == null)){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E04']= "1";
} 


if (activiteInfo.cadreEtablissementIdentification.modifEnseigneNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E05']= activiteInfo.cadreEtablissementIdentification.modifEnseigneNew;
} else if (activite.etablissementEnseigne != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E05']= activite.etablissementEnseigne;
}
if (activiteInfo.cadreEtablissementIdentification.modifNomCommercialNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E09']= activiteInfo.cadreEtablissementIdentification.modifNomCommercialNew;
} else if (activite.etablissementNomCommercial != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E09']= activite.etablissementNomCommercial;
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M')
or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M')
and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissementBis).eq('activiteSecondaire'))
or (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')
and Value('id').of(ouverture.etablissementDejaConnu).eq('non'))) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E08']= ouverture.ajoutFondePouvoir ? "O" : "N";
} else if (((pouvoir.length > 0) and Value('id').of(pouvoir[0].cadreIdentiteDirigeant.categoriePouvoirLimite).eq('pouvoirLimiteSecondaire'))
or ((fondePouvoir.length > 0) and Value('id').of(fondePouvoir[0].cadreIdentiteDirigeant.categoriePouvoirLimite).eq('pouvoirLimiteSecondaire')))	{
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E08']= "O";
}
}

// Fin 60M seul

// Si 40M  : Groupe GFE

if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).eq('40M') or dissolution.dissoMiseEnSommeil) {			
				
				//Groupe IAE : Identification ancienne de l'établissement

if (fermeture.cadreAdresseEtablissementFerme.communeAdresseEtablissement != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[1]/IAE/E12/E12.3']= fermeture.cadreAdresseEtablissementFerme.communeAdresseEtablissement.getId();
if (fermeture.cadreAdresseEtablissementFerme.numeroAdresseEtablissement != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[1]/IAE/E12/E12.5']= fermeture.cadreAdresseEtablissementFerme.numeroAdresseEtablissement;
}
if (fermeture.cadreAdresseEtablissementFerme.indiceAdresseEtablissement != null) { 
   var monId1 = Value('id').of(fermeture.cadreAdresseEtablissementFerme.indiceAdresseEtablissement)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[1]/IAE/E12/E12.6']          = monId1;
}
if (fermeture.cadreAdresseEtablissementFerme.distributionSpecialeAdresseEtablissement != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[1]/IAE/E12/E12.7']= fermeture.cadreAdresseEtablissementFerme.distributionSpecialeAdresseEtablissement;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[1]/IAE/E12/E12.8']= fermeture.cadreAdresseEtablissementFerme.codePostalAdresseEtablissement;
if (fermeture.cadreAdresseEtablissementFerme.complementAdresseEtablissement != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[1]/IAE/E12/E12.10']= fermeture.cadreAdresseEtablissementFerme.complementAdresseEtablissement;
}
if (fermeture.cadreAdresseEtablissementFerme.typeAdresseEtablissement != null) { 
   var monId1 = Value('id').of(fermeture.cadreAdresseEtablissementFerme.typeAdresseEtablissement)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[1]/IAE/E12/E12.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[1]/IAE/E12/E12.12']= fermeture.cadreAdresseEtablissementFerme.voieAdresseEtablissement;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[1]/IAE/E12/E12.13']= fermeture.cadreAdresseEtablissementFerme.communeAdresseEtablissement.getLabel();
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[1]/IAE/E12/E12.3']= identite.adresseSiege.siegeAdresseCommune.getId();
if (identite.adresseSiege.siegeAdresseNumeroVoie != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[1]/IAE/E12/E12.5']= identite.adresseSiege.siegeAdresseNumeroVoie;
}
if (identite.adresseSiege.siegeAdresseIndiceVoie != null) { 
   var monId1 = Value('id').of(identite.adresseSiege.siegeAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[1]/IAE/E12/E12.6']          = monId1;
}
if (identite.adresseSiege.siegeAdresseDistriutionSpecialeVoie != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[1]/IAE/E12/E12.7']= identite.adresseSiege.siegeAdresseDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[1]/IAE/E12/E12.8']= identite.adresseSiege.siegeAdresseCodePostal;
if (identite.adresseSiege.siegeAdresseComplementVoie != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[1]/IAE/E12/E12.10']= identite.adresseSiege.siegeAdresseComplementVoie;
}
if (identite.adresseSiege.siegeAdresseTypeVoie != null) { 
   var monId1 = Value('id').of(identite.adresseSiege.siegeAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[1]/IAE/E12/E12.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[1]/IAE/E12/E12.12']= identite.adresseSiege.siegeAdresseNomVoie;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[1]/IAE/E12/E12.13']= identite.adresseSiege.siegeAdresseCommune.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[1]/IAE/E13/E13.1'] = "000000000";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[1]/IAE/E13/E13.2'] = "00000";
if (fermeture.dateCessationEmploi != null) {
var dateTemp = new Date(parseInt(fermeture.dateCessationEmploi.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[1]/IAE/E14']= date;
}
if (fermeture.lieuEtablissementPrincipal) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[1]/IAE/E17']= "2";
} else if (not fermeture.lieuEtablissementPrincipal) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[1]/IAE/E17']= "3";
}

			// Groupe DEE : Destination de l'établissement

if (Value('id').of(fermeture.destinationEtablissement1).eq('vendu')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[1]/DEE/E41/E41.1']= "3";
} else if (Value('id').of(fermeture.destinationEtablissement1).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[1]/DEE/E41/E41.1']= "9";
} else if (Value('id').of(fermeture.destinationEtablissement1).eq('supprime')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[1]/DEE/E41/E41.1']= "C";
}
if (Value('id').of(fermeture.destinationEtablissement1).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[1]/DEE/E41/E41.2']= fermeture.destinationAutre;
}
if (fermeture.autreFermeture) {
				//Groupe IAE : Identification ancienne de l'établissement

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[2]/IAE/E12/E12.3']= fermeture.cadreAdresseEtablissementFerme2.communeAdresseEtablissement.getId();
if (fermeture.cadreAdresseEtablissementFerme2.numeroAdresseEtablissement != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[2]/IAE/E12/E12.5']= fermeture.cadreAdresseEtablissementFerme2.numeroAdresseEtablissement;
}
if (fermeture.cadreAdresseEtablissementFerme2.indiceAdresseEtablissement != null) { 
   var monId1 = Value('id').of(fermeture.cadreAdresseEtablissementFerme2.indiceAdresseEtablissement)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[2]/IAE/E12/E12.6']          = monId1;
}
if (fermeture.cadreAdresseEtablissementFerme2.distributionSpecialeAdresseEtablissement != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[2]/IAE/E12/E12.7']= fermeture.cadreAdresseEtablissementFerme2.distributionSpecialeAdresseEtablissement;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[2]/IAE/E12/E12.8']= fermeture.cadreAdresseEtablissementFerme2.codePostalAdresseEtablissement;
if (fermeture.cadreAdresseEtablissementFerme2.complementAdresseEtablissement != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[2]/IAE/E12/E12.10']= fermeture.cadreAdresseEtablissementFerme2.complementAdresseEtablissement;
}
if (fermeture.cadreAdresseEtablissementFerme2.typeAdresseEtablissement != null) { 
   var monId1 = Value('id').of(fermeture.cadreAdresseEtablissementFerme2.typeAdresseEtablissement)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[2]/IAE/E12/E12.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[2]/IAE/E12/E12.12']= fermeture.cadreAdresseEtablissementFerme2.voieAdresseEtablissement;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[2]/IAE/E12/E12.13']= fermeture.cadreAdresseEtablissementFerme2.communeAdresseEtablissement.getLabel();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[2]/IAE/E13/E13.1'] = "000000000";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[2]/IAE/E13/E13.2'] = "00000";
if (fermeture.dateCessationEmploi2 != null) {
var dateTemp = new Date(parseInt(fermeture.dateCessationEmploi2.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[2]/IAE/E14']= date;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[2]/IAE/E17']= "4";


			// Groupe DEE : Destination de l'établissement

if (Value('id').of(fermeture.destinationEtablissementAutre1).eq('vendu')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[2]/DEE/E41/E41.1']= "3";
} else if (Value('id').of(fermeture.destinationEtablissementAutre1).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[2]/DEE/E41/E41.1']= "9";
} else if (Value('id').of(fermeture.destinationEtablissementAutre1).eq('supprime')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[2]/DEE/E41/E41.1']= "C";
}
if (Value('id').of(fermeture.destinationEtablissementAutre1).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[2]/DEE/E41/E41.2']= fermeture.destinationAutre2;
}
}
if (fermeture.autreFermeture2) {
				//Groupe IAE : Identification ancienne de l'établissement

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[3]/IAE/E12/E12.3']= fermeture.cadreAdresseEtablissementFerme3.communeAdresseEtablissement.getId();
if (fermeture.cadreAdresseEtablissementFerme3.numeroAdresseEtablissement != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[3]/IAE/E12/E12.5']= fermeture.cadreAdresseEtablissementFerme3.numeroAdresseEtablissement;
}
if (fermeture.cadreAdresseEtablissementFerme3.indiceAdresseEtablissement != null) { 
   var monId1 = Value('id').of(fermeture.cadreAdresseEtablissementFerme3.indiceAdresseEtablissement)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[3]/IAE/E12/E12.6']          = monId1;
}
if (fermeture.cadreAdresseEtablissementFerme3.distributionSpecialeAdresseEtablissement != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[3]/IAE/E12/E12.7']= fermeture.cadreAdresseEtablissementFerme3.distributionSpecialeAdresseEtablissement;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[3]/IAE/E12/E12.8']= fermeture.cadreAdresseEtablissementFerme3.codePostalAdresseEtablissement;
if (fermeture.cadreAdresseEtablissementFerme3.complementAdresseEtablissement != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[3]/IAE/E12/E12.10']= fermeture.cadreAdresseEtablissementFerme3.complementAdresseEtablissement;
}
if (fermeture.cadreAdresseEtablissementFerme3.typeAdresseEtablissement != null) { 
   var monId1 = Value('id').of(fermeture.cadreAdresseEtablissementFerme3.typeAdresseEtablissement)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[3]/IAE/E12/E12.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[3]/IAE/E12/E12.12']= fermeture.cadreAdresseEtablissementFerme3.voieAdresseEtablissement;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[3]/IAE/E12/E12.13']= fermeture.cadreAdresseEtablissementFerme3.communeAdresseEtablissement.getLabel();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[3]/IAE/E13/E13.1'] = "000000000";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[3]/IAE/E13/E13.2'] = "00000";
if (fermeture.dateCessationEmploi3 != null) {
var dateTemp = new Date(parseInt(fermeture.dateCessationEmploi3.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[3]/IAE/E14']= date;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[3]/IAE/E17']= "4";


			// Groupe DEE : Destination de l'établissement

if (Value('id').of(fermeture.destinationEtablissementAutre2).eq('vendu')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[3]/DEE/E41/E41.1']= "3";
} else if (Value('id').of(fermeture.destinationEtablissementAutre2).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[3]/DEE/E41/E41.1']= "9";
} else if (Value('id').of(fermeture.destinationEtablissementAutre2).eq('supprime')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[3]/DEE/E41/E41.1']= "C";
}
if (Value('id').of(fermeture.destinationEtablissementAutre2).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme[3]/DEE/E41/E41.2']= fermeture.destinationAutre3;
}
}
}
// Fin du 40M
			
			// Groupe IAE : Identification ancienne de l'établissement hors 40P / 42P / 43P

if (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('81M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('82M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('84M')
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
	and not Value('id').of(fermeture.destinationEtablissement3).eq('devientPrincipal')
	and not Value('id').of(fermeture.destinationEtablissement4).eq('devientSecondaire')
	and not Value('id').of(fermeture.destinationEtablissement5).eq('devientSiege')
	and not Value('id').of(fermeture.destinationEtablissement5).eq('devientSecondaire')
	and not Value('id').of(fermeture.destinationEtablissement6).eq('devientSecondaire'))) {
if (fermeture.cadreAdresseEtablissementFerme.communeAdresseEtablissement != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.3']= fermeture.cadreAdresseEtablissementFerme.communeAdresseEtablissement.getId();
if (fermeture.cadreAdresseEtablissementFerme.numeroAdresseEtablissement != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.5']= fermeture.cadreAdresseEtablissementFerme.numeroAdresseEtablissement;
}
if (fermeture.cadreAdresseEtablissementFerme.indiceAdresseEtablissement != null) { 
   var monId1 = Value('id').of(fermeture.cadreAdresseEtablissementFerme.indiceAdresseEtablissement)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.6']          = monId1;
}
if (fermeture.cadreAdresseEtablissementFerme.distributionSpecialeAdresseEtablissement != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.7']= fermeture.cadreAdresseEtablissementFerme.distributionSpecialeAdresseEtablissement;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.8']= fermeture.cadreAdresseEtablissementFerme.codePostalAdresseEtablissement;
if (fermeture.cadreAdresseEtablissementFerme.complementAdresseEtablissement != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.10']= fermeture.cadreAdresseEtablissementFerme.complementAdresseEtablissement;
}
if (fermeture.cadreAdresseEtablissementFerme.typeAdresseEtablissement != null) { 
   var monId1 = Value('id').of(fermeture.cadreAdresseEtablissementFerme.typeAdresseEtablissement)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.12']= fermeture.cadreAdresseEtablissementFerme.voieAdresseEtablissement;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.13']= fermeture.cadreAdresseEtablissementFerme.communeAdresseEtablissement.getLabel();
} else if (fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseCommune != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.3']= fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseCommune.getId();
if (fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseNumeroVoie != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.5']= fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseNumeroVoie;
}
if (fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseIndiceVoie != null) { 
   var monId1 = Value('id').of(fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.6']          = monId1;
}
if (fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseDistriutionSpecialeVoie != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.7']= fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.8']= fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseCodePostal;
if (fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseComplementVoie != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.10']= fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseComplementVoie;
}
if (fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseTypeVoie != null) { 
   var monId1 = Value('id').of(fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.12']= fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseNomVoie;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.13']= fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseCommune.getLabel();
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.3']= identite.adresseSiege.siegeAdresseCommune.getId();
if (identite.adresseSiege.siegeAdresseNumeroVoie != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.5']= identite.adresseSiege.siegeAdresseNumeroVoie;
}
if (identite.adresseSiege.siegeAdresseIndiceVoie != null) { 
   var monId1 = Value('id').of(identite.adresseSiege.siegeAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.6']          = monId1;
}
if (identite.adresseSiege.siegeAdresseDistriutionSpecialeVoie != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.7']= identite.adresseSiege.siegeAdresseDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.8']= identite.adresseSiege.siegeAdresseCodePostal;
if (identite.adresseSiege.siegeAdresseComplementVoie != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.10']= identite.adresseSiege.siegeAdresseComplementVoie;
}
if (identite.adresseSiege.siegeAdresseTypeVoie != null) { 
   var monId1 = Value('id').of(identite.adresseSiege.siegeAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.12']= identite.adresseSiege.siegeAdresseNomVoie;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.13']= identite.adresseSiege.siegeAdresseCommune.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E13/E13.1'] = "000000000";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E13/E13.2'] = "00000";
if (fermeture.dateCessationEmploi != null) {
var dateTemp = new Date(parseInt(fermeture.dateCessationEmploi.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E14']= date;
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege') and not fermeture.lieuEtablissementPrincipal) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E17']= "1";
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiegePrincipal') 
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('81M')
or fondsDonne.lieuPrincipal
or (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSiege') and fermeture.lieuEtablissementPrincipal)
or (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') and fermeture.lieuEtablissementPrincipal)) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E17']= "2";
} else if ((Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') and not fermeture.lieuEtablissementPrincipal)
or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('82M')
and Value('id').of(objet.cadreObjetModificationEtablissement.fondsDonneEtablissement).eq('fondsDonnePrincipal')
and not fondsDonne.lieuPrincipal)
or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('84M')
and not fondsDonne.lieuPrincipal)) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E17']= "3";
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire') 
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80M')
or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('82M')
and Value('id').of(objet.cadreObjetModificationEtablissement.fondsDonneEtablissement).eq('fondsDonneSecondaire'))) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E17']= "4";
}
}

// Fin 60P

			   // Groupe ORE : Origine de l'établissement

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(ouverture.etablissementDejaConnu).eq('non'))
	or Value('id').of(activite.activiteEvenement).eq('61M') 
	or Value('id').of(activite.activiteEvenement).eq('61M62M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63M') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64M')) {

if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation') 
or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsCreation')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.1']=  "1";
} else if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsAchat')
or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsAchat')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63M')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.1']=  "3";
} else if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsApport')
	or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsApport')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.1']=  "4";
} else if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsLocationGerance')
or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsLocationGerance')
or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64M') 
and Value('id').of(objet.cadreObjetModificationEtablissement.typeContratReprise).eq('repriseLocation'))) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.1']=  "6";
} else if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsGeranceMandat')
or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsGeranceMandat')
or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64M') 
and Value('id').of(objet.cadreObjetModificationEtablissement.typeContratReprise).eq('repriseGerance'))) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.1']=  "F";
} else if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCocheAutre')
	or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsCocheAutre')
	or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsreprise')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.1']=  "9";
} 
if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCocheAutre')
or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsCocheAutre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.2']= origine.etablissementOrigineFondsAutre;
} else if (Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsreprise')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.2']= "Reprise de l'exploitation d'un fonds";
} 
if (origine.precedentExploitant.etablissementJournalAnnoncesLegalesNom != null or origine.precedentExploitant.etablissementJournalAnnoncesLegalesNomBis != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E22/E22.1']= origine.precedentExploitant.etablissementJournalAnnoncesLegalesNom != null ? origine.precedentExploitant.etablissementJournalAnnoncesLegalesNom : origine.precedentExploitant.etablissementJournalAnnoncesLegalesNomBis;
}
if (origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParution != null) {
var dateTemp = new Date(parseInt(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParution.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E22/E22.2']= date;
} else if(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParutionBis != null) {
var dateTemp = new Date(parseInt(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParutionBis.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E22/E22.2']= date;
}
}

// Fin 63M
				
				// Groupe PEE : Précédent exploitant

if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsAchat')
or Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsApport')
or Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsLocationGerance')
or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsAchat')
or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsApport')
or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsLocationGerance')
or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsreprise')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.1']= origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant.split(' ').join('');
if (Value('id').of(origine.precedentExploitant.typePersonnePrecedentExploitant).eq('typePersonnePrecedentExploitantPP')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.2']= origine.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant;
if (origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.3']= origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.4']= origine.precedentExploitant.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant;
}
if (Value('id').of(origine.precedentExploitant.typePersonnePrecedentExploitant).eq('typePersonnePrecedentExploitantPM')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.7']= origine.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant;
}	
}

	
				// Groupe DEE : Destination de l'établissement

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('81M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('82M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('84M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')) {
		
if (Value('id').of(fermeture.destinationEtablissement1).eq('vendu')
or Value('id').of(fermeture.destinationEtablissement2).eq('vendu')
or Value('id').of(fermeture.destinationEtablissement3).eq('vendu')
or Value('id').of(fermeture.destinationEtablissement4).eq('vendu')
or Value('id').of(fermeture.destinationEtablissement5).eq('vendu')
or Value('id').of(fermeture.destinationEtablissement6).eq('vendu')
or Value('id').of(fermeture.destinationEtablissement7).eq('vendu')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.1']= "3";
} else if (Value('id').of(fermeture.destinationEtablissement5).eq('devientSiege')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.1']= "7";
} else if (Value('id').of(fermeture.destinationEtablissement3).eq('devientPrincipal')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.1']= "8";
} else if (Value('id').of(fermeture.destinationEtablissement1).eq('autre')
or Value('id').of(fermeture.destinationEtablissement2).eq('autre')
or Value('id').of(fermeture.destinationEtablissement3).eq('autre')
or Value('id').of(fermeture.destinationEtablissement4).eq('autre')
or Value('id').of(fermeture.destinationEtablissement5).eq('autre')
or Value('id').of(fermeture.destinationEtablissement6).eq('autre')
or Value('id').of(fermeture.destinationEtablissement7).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.1']= "9";
} else if (Value('id').of(fermeture.destinationEtablissement4).eq('devientSecondaire')
or Value('id').of(fermeture.destinationEtablissement5).eq('devientSecondaire')
or Value('id').of(fermeture.destinationEtablissement6).eq('devientSecondaire')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.1']= "A";
} else if (Value('id').of(fermeture.destinationEtablissement2).eq('ferme')
or Value('id').of(fermeture.destinationEtablissement3).eq('ferme')
or Value('id').of(fermeture.destinationEtablissement4).eq('ferme')
or Value('id').of(fermeture.destinationEtablissement5).eq('ferme')
or Value('id').of(fermeture.destinationEtablissement6).eq('ferme')
or Value('id').of(fermeture.destinationEtablissement7).eq('ferme')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.1']= "B";
} else if (Value('id').of(fermeture.destinationEtablissement2).eq('ferme')
or Value('id').of(fermeture.destinationEtablissement3).eq('ferme')
or Value('id').of(fermeture.destinationEtablissement4).eq('ferme')
or Value('id').of(fermeture.destinationEtablissement5).eq('ferme')
or Value('id').of(fermeture.destinationEtablissement6).eq('ferme')
or Value('id').of(fermeture.destinationEtablissement7).eq('ferme')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.1']= "B";
} else if (Value('id').of(fermeture.destinationEtablissement1).eq('supprime')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.1']= "C";
} else if (Value('id').of(fermeture.destinationEtablissement3).eq('miseEnLocationTotale')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('84M')
or Value('id').of(fondsDonne.infosChangement).eq('totalite')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.1']= "D";
} else if (Value('id').of(fermeture.destinationEtablissement3).eq('miseEnLocationPartielle')
or Value('id').of(fermeture.destinationEtablissement7).eq('miseEnLocationPartielle')
or Value('id').of(fondsDonne.infosChangement).eq('partie')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.1']= "E";
} 
if (Value('id').of(fermeture.destinationEtablissement1).eq('autre')
or Value('id').of(fermeture.destinationEtablissement2).eq('autre')
or Value('id').of(fermeture.destinationEtablissement3).eq('autre')
or Value('id').of(fermeture.destinationEtablissement4).eq('autre')
or Value('id').of(fermeture.destinationEtablissement5).eq('autre')
or Value('id').of(fermeture.destinationEtablissement6).eq('autre')
or Value('id').of(fermeture.destinationEtablissement7).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.2']= fermeture.destinationAutre;
} else if (fondsDonne.fondsDonnePartie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.2']= fondsDonne.fondsDonnePartie;
} 
}

// Fin Regent pour le 80M //
// Fin Regent pour le 81M //
// Fin Regent pour le 11M //
	
		// Groupe REE : Repreneur de l'établissement

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68M')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('82M')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('84M')
or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
and (Value('id').of(fermeture.destinationEtablissement3).eq('miseEnLocationTotale')
or Value('id').of(fermeture.destinationEtablissement3).eq('miseEnLocationPartielle')
or Value('id').of(fermeture.destinationEtablissement7).eq('miseEnLocationPartielle')))) {
if (Value('id').of(fondsDonne.fondsDonneInformation.typePersonneLocataireMandataire).eq('typePersonneLoueurMandantPP')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/REE/E51']=  fondsDonne.fondsDonneInformation.fondsDonneLocataireNomUsage != null ? (fondsDonne.fondsDonneInformation.fondsDonneLocataireNomUsage + ' ' + fondsDonne.fondsDonneInformation.fondsDonneLocatairePrenom) : (fondsDonne.fondsDonneInformation.fondsDonneLocataireNomNaissance + ' ' + fondsDonne.fondsDonneInformation.fondsDonneLocatairePrenom);		
}	
if (Value('id').of(fondsDonne.fondsDonneInformation.typePersonneLocataireMandataire).eq('typePersonneLoueurMandantPM')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/REE/E52']= fondsDonne.fondsDonneInformation.fondsDonneLocataireDenomination;		
}	
}

// Fin 68M
// Fin 82M
// Fin 84M	

				// Groupe LGE : Location-gérance ou gérance-mandat

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64M')
	or Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsLocationGerance') 
	or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsLocationGerance')
	or Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsGeranceMandat')
	or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsGeranceMandat')) {

if(origine.geranceMandat.etablissementLoueurMandantDuFondsDebutContrat != null) {
var dateTemp = new Date(parseInt(origine.geranceMandat.etablissementLoueurMandantDuFondsDebutContrat.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E61/E61.1']= date;
}
if(origine.geranceMandat.etablissementLoueurMandantDuFondsFinContrat != null) {
var dateTemp = new Date(parseInt(origine.geranceMandat.etablissementLoueurMandantDuFondsFinContrat.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E61/E61.2']= date;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E61/E61.3']= origine.geranceMandat.etablissementLoueurMandantDuFondsRenouvellementTaciteReconductionOui ? "O" : "N";
if (Value('id').of(origine.geranceMandat.typePersonneLoueurMandant).eq('typePersonneLoueurMandantPP')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E62']= origine.geranceMandat.entrepriseLieeEntreprisePMNomNaissanceLoueurMandantDuFonds;		
if (origine.geranceMandat.entrepriseLieeEntreprisePPNomUsageLoueurMandantDuFonds != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E64']= origine.geranceMandat.entrepriseLieeEntreprisePPNomUsageLoueurMandantDuFonds;		
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E65']= origine.geranceMandat.entrepriseLieeEntreprisePPPrenom1LoueurMandantDuFonds;		
}
if (Value('id').of(origine.geranceMandat.typePersonneLoueurMandant).eq('typePersonneLoueurMandantPM')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E66']= origine.geranceMandat.entrepriseLieeEntreprisePMDenominationLoueurMandantDuFonds;		
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.3'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.communeLoueurMandantDuFonds.getId();
if (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.numRueAdresseLoueurMandantDufonds != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.5'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.numRueAdresseLoueurMandantDufonds; 
}
if (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.indiceVoieLoueurMandantDuFonds != null) {																							
	var monId1 = Value('id').of(origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.indiceVoieLoueurMandantDuFonds)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.6']=monId1;
}
if (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.distributionSpecialeLoueurMandantDuFonds != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.7'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.distributionSpecialeLoueurMandantDuFonds;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.8'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.codePostalLoueurMandantDuFonds;
if (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.rueComplementAdresseLoueurMandantDuFonds != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.10'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.rueComplementAdresseLoueurMandantDuFonds;
}
if (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.typeVoieLoueurMandantDuFonds != null) {			
	var typeVoie = Value('id').of(origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.typeVoieLoueurMandantDuFonds)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.11']=typeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.12'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.nomVoieLoueurMandantDuFonds;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.13'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.communeLoueurMandantDuFonds.getLabel();
if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsGeranceMandat')
or Value('id').of(origine.etablisementOrigineFondsBis).eq('EtablissementOrigineFondsGeranceMandat')
or Value('id').of(objet.cadreObjetModificationEtablissement.typeContratReprise).eq('repriseGerance')){
	if (origine.geranceMandat.entrepriseLieeSirenGeranceMandat != null){
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E67']=origine.geranceMandat.entrepriseLieeSirenGeranceMandat.split(' ').join('');
	}
	//Récupérer le EntityId du greffe
	var greffeId = origine.geranceMandat.entrepriseLieeGreffeImmatriculation.id ;
	//Appeler Directory pour récupérer l'autorité
	_log.info('l entityId du greffe selectionne est {}', greffeId);
	var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', greffeId) //
	           .connectionTimeout(10000) //
	           .receiveTimeout(10000) //
	           .accept('json') //
	           .get();
	//result
	var receiverInfo = response.asObject();
	var contactInfo = !receiverInfo.details ? null : receiverInfo.details;
	//Récupérer le code EDI
	var codeEDIGreffe = !contactInfo.ediCode ? null : contactInfo.ediCode;
	_log.info('le code edi du greffe selectionne est {}', codeEDIGreffe);
	// À modifier quand Destiny fourni le réferentiel
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E68']=codeEDIGreffe;
}
}	

// Fin pour le 64M

		// Groupe ACE : Activité de l'établissement

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M')		
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
	and Value('id').of(ouverture.etablissementDejaConnu).eq('non'))
	or activiteInfo.modifActiviteTransfert or activiteInfo.modifActiviteAcquisition) {		
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E70']= activite.etablissementActivitesAutres ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E71']= activite.etablissementActivitesPrincipales2 != null ? activite.etablissementActivitesPrincipales2 : activite.etablissementActivitesAutres.substring(0, 140) ;
if (Value('id').of(activite.activiteEvenement).eq('61M') or activiteInfo.modifRepriseExploitation) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E79/E79.1']= "A";
} else if (Value('id').of(activite.activiteEvenement).eq('62M') and Value('id').of(activite.suppressionActivitePar).eq('suppressionParDisparition')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E79/E79.1']= "D";
} else if (Value('id').of(activite.activiteEvenement).eq('62M') and Value('id').of(activite.suppressionActivitePar).eq('suppressionParVente')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E79/E79.1']= "V";
} else if (Value('id').of(activite.activiteEvenement).eq('62M') and Value('id').of(activite.suppressionActivitePar).eq('suppressionParReprise')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E79/E79.1']= "R";
} else if (Value('id').of(activite.activiteEvenement).eq('62M') and Value('id').of(activite.suppressionActivitePar).eq('suppressionParAutre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E79/E79.1']= "U";
} else if (Value('id').of(activite.activiteEvenement).eq('61M62M') and Value('id').of(activite.suppressionActivitePar).eq('suppressionParDisparition')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E79/E79.1']= "E";
} else if (Value('id').of(activite.activiteEvenement).eq('61M62M') and Value('id').of(activite.suppressionActivitePar).eq('suppressionParVente')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E79/E79.1']= "W";
} else if (Value('id').of(activite.activiteEvenement).eq('61M62M') and Value('id').of(activite.suppressionActivitePar).eq('suppressionParReprise')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E79/E79.1']= "T";
} else if (Value('id').of(activite.activiteEvenement).eq('61M62M') and Value('id').of(activite.suppressionActivitePar).eq('suppressionParAutre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E79/E79.1']= "X";
}
if (Value('id').of(activite.suppressionActivitePar).eq('suppressionParAutre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E79/E79.2']= activite.suppressionActiviteAutre;
}	

		// Groupe CAE

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E73/E73.1']= Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonnierePermanente') ? 'P' : 'S';
if (activite.etablissementNonSedentariteQualiteNonSedentaire) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E73/E73.2']='A';
}
if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMagasin') or Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMarche') or Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceDetail')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.1']='10';
} else if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceGros')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.1']='09';
} else if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureFabricationProduction')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.1']='04';
} else if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureBatTravauxPublics')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.1']='14';
} else if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCocheAutre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.1']='99';
}
if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCocheAutre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.2']= activite.etablissementActiviteNatureAutre;
}
if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMagasin')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E77/E77.1']= "05";
} else if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMarche')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E77/E77.1']= "92";
} else if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceDetail')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E77/E77.1']= "99";
} 
if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceDetail')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E77/E77.2']= "Sur internet";
}
if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMagasin')) {
function lpad(str, padString, length) {
  var ch = str;
  while (ch.length < length) {
      ch = padString + ch;
  }
  log.info("lpad : "+ch);
  return ch;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E77/E77.3']= lpad(activite.etablissementActiviteLieuExerciceMagasinSurface.toString(), "0", 5);
}

		// Groupe SAE : salariés de l'établissement

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E84']= activite.etablissementEffectifSalariePresenceOui ? (activite.cadreEffectifSalarie.etablissementEffectifSalarieNombre != null ? activite.cadreEffectifSalarie.etablissementEffectifSalarieNombre : '') : "0";
if (activite.cadreEffectifSalarie.etablissementEffectifSalarieApprentis != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E85/E85.8']= activite.cadreEffectifSalarie.etablissementEffectifSalarieApprentis;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E86']= activite.etablissementEffectifSalariePresenceOui ? "O" : "N";
}

// Fin du 61M
// Fin du 62M
// Fin du 67M

			// Groupe IDE - Identification de la personne liée à l'exploitation
			
if ((fondePouvoir.length > 0) or (pouvoir.length > 0)) {		
if (pouvoir.length > 0)	{
if (pouvoir[0].cadreIdentiteDirigeant.personneLieeObjet == null or Value('id').of(pouvoir[0].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E91/E91.1'] = "1";
} else if (Value('id').of(pouvoir[0].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E91/E91.1'] = "2";
} else if (Value('id').of(pouvoir[0].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantModif') or Value('id').of(pouvoir[0].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantMaintenu')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E91/E91.1'] = "3";
}
if (pouvoir[0].cadreIdentiteDirigeant.personneLieeObjet == null and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M') and ouverture.modifDateAdresseOuverture != null) {
var dateTemp = new Date(parseInt(ouverture.modifDateAdresseOuverture.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E91/E91.2']= date;
} else if (pouvoir[0].cadreIdentiteDirigeant.personneLieeObjet == null and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M') and ouverture.modifDateRepriseExploitation != null) {
var dateTemp = new Date(parseInt(ouverture.modifDateRepriseExploitation.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E91/E91.2']= date;
} else if (pouvoir[0].cadreIdentiteDirigeant.personneLieeObjet == null and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and fermeture.modifDateTransfert != null) {
var dateTemp = new Date(parseInt(fermeture.modifDateTransfert.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E91/E91.2']= date;
} else if (pouvoir[0].cadreIdentiteDirigeant.personneLieeObjet == null and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite') and ouverture.modifDatePriseActiviteEtablissement != null) {
var dateTemp = new Date(parseInt(ouverture.modifDatePriseActiviteEtablissement.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E91/E91.2']= date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E91/E91.2']= date;
} else if (pouvoir[0].cadreIdentiteDirigeant.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(pouvoir[0].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E91/E91.2']= date;
}
if (pouvoir[0].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOui or pouvoir[0].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E92/E92.1']= "P";
} else if (not pouvoir[0].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOui and not pouvoir[0].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E92/E92.1']= "S";
}	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E93/E93.2']= pouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (pouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E93/E93.4']= pouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for ( i = 0; i < pouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(pouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E93/E93.3']= prenoms;
if (not Value('id').of(pouvoir[0].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.3']= pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.3']= result;
}
if (pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.5'] = pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
var monId1 = Value('id').of(pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.6']          = monId1;
}
if (pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.7'] = pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.8'] = pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.10'] = pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
var monId1 = Value('id').of(pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.12'] = pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.13']= pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.13']= pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.14']= pouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
}
if (pouvoir[0].cadreIdentiteDirigeant.personneLieeObjet == null or Value('id').of(pouvoir[0].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {
if (pouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
var dateTemp = new Date(parseInt(pouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E95/E95.1']= date;
}
if (not Value('id').of(pouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = pouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E95/E95.2']= result;
} else if (Value('id').of(pouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E95/E95.2'] = pouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}	
if (not Value('id').of(pouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E95/E95.3']= pouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(pouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E95/E95.4']= pouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E96']= pouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNationaliteGerant;
}
}
if (pouvoir.length > 1)	{
if (Value('id').of(pouvoir[1].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E91/E91.1'] = "1";
} else if (Value('id').of(pouvoir[1].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E91/E91.1'] = "2";
} else if (Value('id').of(pouvoir[1].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantModif') or Value('id').of(pouvoir[1].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantMaintenu')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E91/E91.1'] = "3";
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E91/E91.2']= date;
} else if (pouvoir[1].cadreIdentiteDirigeant.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(pouvoir[1].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E91/E91.2']= date;
}
if (pouvoir[1].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E92/E92.1']= "P";
} else if (not pouvoir[1].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E92/E92.1']= "S";
}	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E93/E93.2']= pouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (pouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E93/E93.4']= pouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for ( i = 0; i < pouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(pouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E93/E93.3']= prenoms;
if (not Value('id').of(pouvoir[1].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.3']= pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.3']= result;
}
if (pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.5'] = pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
var monId1 = Value('id').of(pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.6']          = monId1;
}
if (pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.7'] = pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.8'] = pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.10'] = pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
var monId1 = Value('id').of(pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.12'] = pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.13']= pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.13']= pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.14']= pouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
}
if (Value('id').of(pouvoir[1].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {
if (pouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
var dateTemp = new Date(parseInt(pouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E95/E95.1']= date;
}
if (not Value('id').of(pouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = pouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E95/E95.2']= result;
} else if (Value('id').of(pouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E95/E95.2'] = pouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}	
if (not Value('id').of(pouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E95/E95.3']= pouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(pouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E95/E95.4']= pouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E96']= pouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNationaliteGerant;
}
}
if (pouvoir.length > 2)	{
if (Value('id').of(pouvoir[2].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E91/E91.1'] = "1";
} else if (Value('id').of(pouvoir[2].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E91/E91.1'] = "2";
} else if (Value('id').of(pouvoir[2].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantModif') or Value('id').of(pouvoir[2].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantMaintenu')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E91/E91.1'] = "3";
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E91/E91.2']= date;
} else if (pouvoir[2].cadreIdentiteDirigeant.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(pouvoir[2].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E91/E91.2']= date;
}
if (pouvoir[2].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E92/E92.1']= "P";
} else if (not pouvoir[2].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E92/E92.1']= "S";
}	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E93/E93.2']= pouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (pouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E93/E93.4']= pouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for ( i = 0; i < pouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(pouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E93/E93.3']= prenoms;
if (not Value('id').of(pouvoir[2].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.3']= pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.3']= result;
}
if (pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.5'] = pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
var monId1 = Value('id').of(pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.6']          = monId1;
}
if (pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.7'] = pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.8'] = pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.10'] = pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
var monId1 = Value('id').of(pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.12'] = pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.13']= pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.13']= pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.14']= pouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
}
if (Value('id').of(pouvoir[2].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {
if (pouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
var dateTemp = new Date(parseInt(pouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E95/E95.1']= date;
}
if (not Value('id').of(pouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = pouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E95/E95.2']= result;
} else if (Value('id').of(pouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E95/E95.2'] = pouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}	
if (not Value('id').of(pouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E95/E95.3']= pouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(pouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E95/E95.4']= pouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E96']= pouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNationaliteGerant;
}
}
if (pouvoir.length > 3)	{
if (Value('id').of(pouvoir[3].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E91/E91.1'] = "1";
} else if (Value('id').of(pouvoir[3].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E91/E91.1'] = "2";
} else if (Value('id').of(pouvoir[3].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantModif') or Value('id').of(pouvoir[3].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantMaintenu')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E91/E91.1'] = "3";
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E91/E91.2']= date;
} else if (pouvoir[3].cadreIdentiteDirigeant.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(pouvoir[3].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E91/E91.2']= date;
}
if (pouvoir[3].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E92/E92.1']= "P";
} else if (not pouvoir[3].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E92/E92.1']= "S";
}	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E93/E93.2']= pouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (pouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E93/E93.4']= pouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for ( i = 0; i < pouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(pouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E93/E93.3']= prenoms;
if (not Value('id').of(pouvoir[3].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E94/E94.3']= pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E94/E94.3']= result;
}
if (pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E94/E94.5'] = pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
var monId1 = Value('id').of(pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E94/E94.6']          = monId1;
}
if (pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E94/E94.7'] = pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E94/E94.8'] = pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E94/E94.10'] = pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
var monId1 = Value('id').of(pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E94/E94.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E94/E94.12'] = pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E94/E94.13']= pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E94/E94.13']= pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E94/E94.14']= pouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
}
if (Value('id').of(pouvoir[3].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {
if (pouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
var dateTemp = new Date(parseInt(pouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E95/E95.1']= date;
}
if (not Value('id').of(pouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = pouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E95/E95.2']= result;
} else if (Value('id').of(pouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E95/E95.2'] = pouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}	
if (not Value('id').of(pouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E95/E95.3']= pouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(pouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E95/E95.4']= pouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E96']= pouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNationaliteGerant;
}
}
if (pouvoir.length > 4)	{
if (Value('id').of(pouvoir[4].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E91/E91.1'] = "1";
} else if (Value('id').of(pouvoir[4].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E91/E91.1'] = "2";
} else if (Value('id').of(pouvoir[4].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantModif') or Value('id').of(pouvoir[4].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantMaintenu')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E91/E91.1'] = "3";
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E91/E91.2']= date;
} else if (pouvoir[4].cadreIdentiteDirigeant.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(pouvoir[4].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E91/E91.2']= date;
}
if (pouvoir[4].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E92/E92.1']= "P";
} else if (not pouvoir[4].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E92/E92.1']= "S";
}	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E93/E93.2']= pouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (pouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E93/E93.4']= pouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for ( i = 0; i < pouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(pouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E93/E93.3']= prenoms;
if (not Value('id').of(pouvoir[4].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E94/E94.3']= pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E94/E94.3']= result;
}
if (pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E94/E94.5'] = pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
var monId1 = Value('id').of(pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E94/E94.6']          = monId1;
}
if (pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E94/E94.7'] = pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E94/E94.8'] = pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E94/E94.10'] = pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
var monId1 = Value('id').of(pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E94/E94.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E94/E94.12'] = pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E94/E94.13']= pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E94/E94.13']= pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E94/E94.14']= pouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
}
if (Value('id').of(pouvoir[4].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {
if (pouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
var dateTemp = new Date(parseInt(pouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E95/E95.1']= date;
}
if (not Value('id').of(pouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = pouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E95/E95.2']= result;
} else if (Value('id').of(pouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E95/E95.2'] = pouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}	
if (not Value('id').of(pouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E95/E95.3']= pouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(pouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E95/E95.4']= pouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E96']= pouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNationaliteGerant;
}
}
if (fondePouvoir.length > 0)	{
if (Value('id').of(fondePouvoir[0].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau') or fondePouvoir[0].cadreIdentiteDirigeant.personneLieeObjet ==null) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E91/E91.1'] = "1";
} else if (Value('id').of(fondePouvoir[0].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E91/E91.1'] = "2";
} else if (Value('id').of(fondePouvoir[0].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantModif') or Value('id').of(fondePouvoir[0].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantMaintenu')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E91/E91.1'] = "3";
}
if (fondePouvoir[0].cadreIdentiteDirigeant.personneLieeObjet == null and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54M') and ouverture.modifDateAdresseOuverture != null) {
var dateTemp = new Date(parseInt(ouverture.modifDateAdresseOuverture.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E91/E91.2']= date;
} else if (fondePouvoir[0].cadreIdentiteDirigeant.personneLieeObjet == null and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53M') and ouverture.modifDateRepriseExploitation != null) {
var dateTemp = new Date(parseInt(ouverture.modifDateRepriseExploitation.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E91/E91.2']= date;
} else if (fondePouvoir[0].cadreIdentiteDirigeant.personneLieeObjet == null and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and fermeture.modifDateTransfert != null) {
var dateTemp = new Date(parseInt(fermeture.modifDateTransfert.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E91/E91.2']= date;
} else if (fondePouvoir[0].cadreIdentiteDirigeant.personneLieeObjet == null and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('priseActivite') and ouverture.modifDatePriseActiviteEtablissement != null) {
var dateTemp = new Date(parseInt(ouverture.modifDatePriseActiviteEtablissement.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E91/E91.2']= date;
} else if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E91/E91.2']= date;
} else if (fondePouvoir[0].cadreIdentiteDirigeant.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(fondePouvoir[0].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E91/E91.2']= date;
}
if (fondePouvoir[0].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOui or fondePouvoir[0].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E92/E92.1']= "P";
} else if (not fondePouvoir[0].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOui and not fondePouvoir[0].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E92/E92.1']= "S";
}	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E93/E93.2']= fondePouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (fondePouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E93/E93.4']= fondePouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for ( i = 0; i < fondePouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(fondePouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E93/E93.3']= prenoms;
if (not Value('id').of(fondePouvoir[0].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(fondePouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.3']= fondePouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(fondePouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX') and fondePouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays != null) {
		var idPays = fondePouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.3']= result;
}
if (fondePouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.5'] = fondePouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (fondePouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
var monId1 = Value('id').of(fondePouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.6']          = monId1;
}
if (fondePouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.7'] = fondePouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.8'] = fondePouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? fondePouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (fondePouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.10'] = fondePouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (fondePouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
var monId1 = Value('id').of(fondePouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.12'] = fondePouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(fondePouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.13']= fondePouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(fondePouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.13']= fondePouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(fondePouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX') and fondePouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E94/E94.14']= fondePouvoir[0].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
}
if (fondePouvoir[0].cadreIdentiteDirigeant.personneLieeObjet == null or Value('id').of(fondePouvoir[0].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {
if (fondePouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
var dateTemp = new Date(parseInt(fondePouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E95/E95.1']= date;
}
if (not Value('id').of(fondePouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = fondePouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E95/E95.2']= result;
} else if (Value('id').of(fondePouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E95/E95.2'] = fondePouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}	
if (not Value('id').of(fondePouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E95/E95.3']= fondePouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(fondePouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E95/E95.4']= fondePouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[1]/E96']= fondePouvoir[0].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNationaliteGerant;
}
}
if (fondePouvoir.length > 1)	{
if (Value('id').of(fondePouvoir[1].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E91/E91.1'] = "1";
} else if (Value('id').of(fondePouvoir[1].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E91/E91.1'] = "2";
} else if (Value('id').of(fondePouvoir[1].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantModif') or Value('id').of(fondePouvoir[1].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantMaintenu')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E91/E91.1'] = "3";
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E91/E91.2']= date;
} else if (fondePouvoir[1].cadreIdentiteDirigeant.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(fondePouvoir[1].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E91/E91.2']= date;
}
if (fondePouvoir[1].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E92/E92.1']= "P";
} else if (not fondePouvoir[1].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E92/E92.1']= "S";
}	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E93/E93.2']= fondePouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (fondePouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E93/E93.4']= fondePouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for ( i = 0; i < fondePouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(fondePouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E93/E93.3']= prenoms;
if (not Value('id').of(fondePouvoir[1].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(fondePouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.3']= fondePouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(fondePouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = fondePouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.3']= result;
}
if (fondePouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.5'] = fondePouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (fondePouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
var monId1 = Value('id').of(fondePouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.6']          = monId1;
}
if (fondePouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.7'] = fondePouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.8'] = fondePouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? fondePouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (fondePouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.10'] = fondePouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (fondePouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
var monId1 = Value('id').of(fondePouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.12'] = fondePouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(fondePouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.13']= fondePouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(fondePouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.13']= fondePouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(fondePouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E94/E94.14']= fondePouvoir[1].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
}
if (Value('id').of(fondePouvoir[1].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {
if (fondePouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
var dateTemp = new Date(parseInt(fondePouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E95/E95.1']= date;
}
if (not Value('id').of(fondePouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = fondePouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E95/E95.2']= result;
} else if (Value('id').of(fondePouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E95/E95.2'] = fondePouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}	
if (not Value('id').of(fondePouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E95/E95.3']= fondePouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(fondePouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E95/E95.4']= fondePouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[2]/E96']= fondePouvoir[1].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNationaliteGerant;
}
}
if (fondePouvoir.length > 2)	{
if (Value('id').of(fondePouvoir[2].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E91/E91.1'] = "1";
} else if (Value('id').of(fondePouvoir[2].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E91/E91.1'] = "2";
} else if (Value('id').of(fondePouvoir[2].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantModif') or Value('id').of(fondePouvoir[2].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantMaintenu')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E91/E91.1'] = "3";
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E91/E91.2']= date;
} else if (fondePouvoir[2].cadreIdentiteDirigeant.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(fondePouvoir[2].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E91/E91.2']= date;
}
if (fondePouvoir[2].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E92/E92.1']= "P";
} else if (not fondePouvoir[2].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E92/E92.1']= "S";
}	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E93/E93.2']= fondePouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (fondePouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E93/E93.4']= fondePouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for ( i = 0; i < fondePouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(fondePouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E93/E93.3']= prenoms;
if (not Value('id').of(fondePouvoir[2].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(fondePouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.3']= fondePouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(fondePouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = fondePouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.3']= result;
}
if (fondePouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.5'] = fondePouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (fondePouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
var monId1 = Value('id').of(fondePouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.6']          = monId1;
}
if (fondePouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.7'] = fondePouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.8'] = fondePouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? fondePouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (fondePouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.10'] = fondePouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (fondePouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
var monId1 = Value('id').of(fondePouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.12'] = fondePouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(fondePouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.13']= fondePouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(fondePouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.13']= fondePouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(fondePouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E94/E94.14']= fondePouvoir[2].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
}
if (Value('id').of(fondePouvoir[2].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {
if (fondePouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
var dateTemp = new Date(parseInt(fondePouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E95/E95.1']= date;
}
if (not Value('id').of(fondePouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = fondePouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E95/E95.2']= result;
} else if (Value('id').of(fondePouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E95/E95.2'] = fondePouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}	
if (not Value('id').of(fondePouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E95/E95.3']= fondePouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(fondePouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E95/E95.4']= fondePouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[3]/E96']= fondePouvoir[2].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNationaliteGerant;
}
}
if (fondePouvoir.length > 3)	{
if (Value('id').of(fondePouvoir[3].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E91/E91.1'] = "1";
} else if (Value('id').of(fondePouvoir[3].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E91/E91.1'] = "2";
} else if (Value('id').of(fondePouvoir[3].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantModif') or Value('id').of(fondePouvoir[3].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantMaintenu')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E91/E91.1'] = "3";
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E91/E91.2']= date;
} else if (fondePouvoir[3].cadreIdentiteDirigeant.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(fondePouvoir[3].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E91/E91.2']= date;
}
if (fondePouvoir[3].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E92/E92.1']= "P";
} else if (not fondePouvoir[3].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E92/E92.1']= "S";
}	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E93/E93.2']= fondePouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (fondePouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E93/E93.4']= fondePouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for ( i = 0; i < fondePouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(fondePouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E93/E93.3']= prenoms;
if (not Value('id').of(fondePouvoir[3].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(fondePouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E94/E94.3']= fondePouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(fondePouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = fondePouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E94/E94.3']= result;
}
if (fondePouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E94/E94.5'] = fondePouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (fondePouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
var monId1 = Value('id').of(fondePouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E94/E94.6']          = monId1;
}
if (fondePouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E94/E94.7'] = fondePouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E94/E94.8'] = fondePouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? fondePouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (fondePouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E94/E94.10'] = fondePouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (fondePouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
var monId1 = Value('id').of(fondePouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E94/E94.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E94/E94.12'] = fondePouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(fondePouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E94/E94.13']= fondePouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(fondePouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E94/E94.13']= fondePouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(fondePouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E94/E94.14']= fondePouvoir[3].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
}
if (Value('id').of(fondePouvoir[3].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {
if (fondePouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
var dateTemp = new Date(parseInt(fondePouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E95/E95.1']= date;
}
if (not Value('id').of(fondePouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = fondePouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E95/E95.2']= result;
} else if (Value('id').of(fondePouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E95/E95.2'] = fondePouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}	
if (not Value('id').of(fondePouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E95/E95.3']= fondePouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(fondePouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E95/E95.4']= fondePouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[4]/E96']= fondePouvoir[3].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNationaliteGerant;
}
}
if (fondePouvoir.length > 4)	{
if (Value('id').of(fondePouvoir[4].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E91/E91.1'] = "1";
} else if (Value('id').of(fondePouvoir[4].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E91/E91.1'] = "2";
} else if (Value('id').of(fondePouvoir[4].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantModif') or Value('id').of(fondePouvoir[4].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantMaintenu')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E91/E91.1'] = "3";
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E91/E91.2']= date;
} else if (fondePouvoir[4].cadreIdentiteDirigeant.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(fondePouvoir[4].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E91/E91.2']= date;
}
if (fondePouvoir[4].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E92/E92.1']= "P";
} else if (not fondePouvoir[4].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E92/E92.1']= "S";
}	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E93/E93.2']= fondePouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (fondePouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E93/E93.4']= fondePouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for ( i = 0; i < fondePouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(fondePouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E93/E93.3']= prenoms;
if (not Value('id').of(fondePouvoir[4].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(fondePouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E94/E94.3']= fondePouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(fondePouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = fondePouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E94/E94.3']= result;
}
if (fondePouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E94/E94.5'] = fondePouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (fondePouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
var monId1 = Value('id').of(fondePouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E94/E94.6']          = monId1;
}
if (fondePouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E94/E94.7'] = fondePouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E94/E94.8'] = fondePouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? fondePouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (fondePouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E94/E94.10'] = fondePouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (fondePouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
var monId1 = Value('id').of(fondePouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E94/E94.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E94/E94.12'] = fondePouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(fondePouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E94/E94.13']= fondePouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(fondePouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E94/E94.13']= fondePouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(fondePouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E94/E94.14']= fondePouvoir[4].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
}
if (Value('id').of(fondePouvoir[4].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {
if (fondePouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
var dateTemp = new Date(parseInt(fondePouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E95/E95.1']= date;
}
if (not Value('id').of(fondePouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = fondePouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E95/E95.2']= result;
} else if (Value('id').of(fondePouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E95/E95.2'] = fondePouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}	
if (not Value('id').of(fondePouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E95/E95.3']= fondePouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(fondePouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E95/E95.4']= fondePouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[5]/E96']= fondePouvoir[4].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNationaliteGerant;
}
}
if (fondePouvoir.length > 5)	{
if (Value('id').of(fondePouvoir[5].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[6]/E91/E91.1'] = "1";
} else if (Value('id').of(fondePouvoir[5].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[6]/E91/E91.1'] = "2";
} else if (Value('id').of(fondePouvoir[5].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantModif') or Value('id').of(fondePouvoir[5].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantMaintenu')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[6]/E91/E91.1'] = "3";
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[6]/E91/E91.2']= date;
} else if (fondePouvoir[5].cadreIdentiteDirigeant.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(fondePouvoir[5].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[6]/E91/E91.2']= date;
}
if (fondePouvoir[5].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[6]/E92/E92.1']= "P";
} else if (not fondePouvoir[5].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[6]/E92/E92.1']= "S";
}	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[6]/E93/E93.2']= fondePouvoir[5].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (fondePouvoir[5].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[6]/E93/E93.4']= fondePouvoir[5].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for ( i = 0; i < fondePouvoir[5].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(fondePouvoir[5].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[6]/E93/E93.3']= prenoms;
if (not Value('id').of(fondePouvoir[5].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(fondePouvoir[5].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[6]/E94/E94.3']= fondePouvoir[5].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(fondePouvoir[5].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = fondePouvoir[5].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[6]/E94/E94.3']= result;
}
if (fondePouvoir[5].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[6]/E94/E94.5'] = fondePouvoir[5].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (fondePouvoir[5].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
var monId1 = Value('id').of(fondePouvoir[5].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[6]/E94/E94.6']          = monId1;
}
if (fondePouvoir[5].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[6]/E94/E94.7'] = fondePouvoir[5].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[6]/E94/E94.8'] = fondePouvoir[5].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? fondePouvoir[5].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (fondePouvoir[5].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[6]/E94/E94.10'] = fondePouvoir[5].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (fondePouvoir[5].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
var monId1 = Value('id').of(fondePouvoir[5].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[6]/E94/E94.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[6]/E94/E94.12'] = fondePouvoir[5].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(fondePouvoir[5].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[6]/E94/E94.13']= fondePouvoir[5].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(fondePouvoir[5].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[6]/E94/E94.13']= fondePouvoir[5].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(fondePouvoir[5].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[6]/E94/E94.14']= fondePouvoir[5].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
}
if (Value('id').of(fondePouvoir[5].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {
if (fondePouvoir[5].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
var dateTemp = new Date(parseInt(fondePouvoir[5].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[6]/E95/E95.1']= date;
}
if (not Value('id').of(fondePouvoir[5].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = fondePouvoir[5].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[6]/E95/E95.2']= result;
} else if (Value('id').of(fondePouvoir[5].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[6]/E95/E95.2'] = fondePouvoir[5].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}	
if (not Value('id').of(fondePouvoir[5].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[6]/E95/E95.3']= fondePouvoir[5].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(fondePouvoir[5].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[6]/E95/E95.4']= fondePouvoir[5].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[6]/E96']= fondePouvoir[5].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNationaliteGerant;
}
}
if (fondePouvoir.length > 6)	{
if (Value('id').of(fondePouvoir[6].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[7]/E91/E91.1'] = "1";
} else if (Value('id').of(fondePouvoir[6].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[7]/E91/E91.1'] = "2";
} else if (Value('id').of(fondePouvoir[6].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantModif') or Value('id').of(fondePouvoir[6].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantMaintenu')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[7]/E91/E91.1'] = "3";
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[7]/E91/E91.2']= date;
} else if (fondePouvoir[6].cadreIdentiteDirigeant.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(fondePouvoir[6].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[7]/E91/E91.2']= date;
}
if (fondePouvoir[6].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[7]/E92/E92.1']= "P";
} else if (not fondePouvoir[6].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[7]/E92/E92.1']= "S";
}	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[7]/E93/E93.2']= fondePouvoir[6].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (fondePouvoir[6].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[7]/E93/E93.4']= fondePouvoir[6].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for ( i = 0; i < fondePouvoir[6].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(fondePouvoir[6].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[7]/E93/E93.3']= prenoms;
if (not Value('id').of(fondePouvoir[6].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(fondePouvoir[6].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[7]/E94/E94.3']= fondePouvoir[6].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(fondePouvoir[6].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = fondePouvoir[6].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[7]/E94/E94.3']= result;
}
if (fondePouvoir[6].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[7]/E94/E94.5'] = fondePouvoir[6].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (fondePouvoir[6].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
var monId1 = Value('id').of(fondePouvoir[6].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[7]/E94/E94.6']          = monId1;
}
if (fondePouvoir[6].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[7]/E94/E94.7'] = fondePouvoir[6].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[7]/E94/E94.8'] = fondePouvoir[6].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? fondePouvoir[6].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (fondePouvoir[6].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[7]/E94/E94.10'] = fondePouvoir[6].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (fondePouvoir[6].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
var monId1 = Value('id').of(fondePouvoir[6].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[7]/E94/E94.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[7]/E94/E94.12'] = fondePouvoir[6].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(fondePouvoir[6].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[7]/E94/E94.13']= fondePouvoir[6].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(fondePouvoir[6].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[7]/E94/E94.13']= fondePouvoir[6].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(fondePouvoir[6].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[7]/E94/E94.14']= fondePouvoir[6].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
}
if (Value('id').of(fondePouvoir[6].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {
if (fondePouvoir[6].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
var dateTemp = new Date(parseInt(fondePouvoir[6].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[7]/E95/E95.1']= date;
}
if (not Value('id').of(fondePouvoir[6].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = fondePouvoir[6].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[7]/E95/E95.2']= result;
} else if (Value('id').of(fondePouvoir[6].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[7]/E95/E95.2'] = fondePouvoir[6].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}	
if (not Value('id').of(fondePouvoir[6].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[7]/E95/E95.3']= fondePouvoir[6].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(fondePouvoir[6].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[7]/E95/E95.4']= fondePouvoir[6].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[7]/E96']= fondePouvoir[6].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNationaliteGerant;
}
}
if (fondePouvoir.length > 7)	{
if (Value('id').of(fondePouvoir[7].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[8]/E91/E91.1'] = "1";
} else if (Value('id').of(fondePouvoir[7].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[8]/E91/E91.1'] = "2";
} else if (Value('id').of(fondePouvoir[7].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantModif') or Value('id').of(fondePouvoir[7].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantMaintenu')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[8]/E91/E91.1'] = "3";
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[8]/E91/E91.2']= date;
} else if (fondePouvoir[7].cadreIdentiteDirigeant.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(fondePouvoir[7].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[8]/E91/E91.2']= date;
}
if (fondePouvoir[7].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[8]/E92/E92.1']= "P";
} else if (not fondePouvoir[7].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[8]/E92/E92.1']= "S";
}	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[8]/E93/E93.2']= fondePouvoir[7].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (fondePouvoir[7].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[8]/E93/E93.4']= fondePouvoir[7].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for ( i = 0; i < fondePouvoir[7].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(fondePouvoir[7].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[8]/E93/E93.3']= prenoms;
if (not Value('id').of(fondePouvoir[7].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(fondePouvoir[7].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[8]/E94/E94.3']= fondePouvoir[7].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(fondePouvoir[7].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = fondePouvoir[7].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[8]/E94/E94.3']= result;
}
if (fondePouvoir[7].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[8]/E94/E94.5'] = fondePouvoir[7].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (fondePouvoir[7].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
var monId1 = Value('id').of(fondePouvoir[7].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[8]/E94/E94.6']          = monId1;
}
if (fondePouvoir[7].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[8]/E94/E94.7'] = fondePouvoir[7].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[8]/E94/E94.8'] = fondePouvoir[7].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? fondePouvoir[7].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (fondePouvoir[7].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[8]/E94/E94.10'] = fondePouvoir[7].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (fondePouvoir[7].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
var monId1 = Value('id').of(fondePouvoir[7].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[8]/E94/E94.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[8]/E94/E94.12'] = fondePouvoir[7].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(fondePouvoir[7].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[8]/E94/E94.13']= fondePouvoir[7].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(fondePouvoir[7].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[8]/E94/E94.13']= fondePouvoir[7].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(fondePouvoir[7].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[8]/E94/E94.14']= fondePouvoir[7].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
}
if (Value('id').of(fondePouvoir[7].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {
if (fondePouvoir[7].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
var dateTemp = new Date(parseInt(fondePouvoir[7].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[8]/E95/E95.1']= date;
}
if (not Value('id').of(fondePouvoir[7].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = fondePouvoir[7].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[8]/E95/E95.2']= result;
} else if (Value('id').of(fondePouvoir[7].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[8]/E95/E95.2'] = fondePouvoir[7].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}	
if (not Value('id').of(fondePouvoir[7].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[8]/E95/E95.3']= fondePouvoir[7].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(fondePouvoir[7].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[8]/E95/E95.4']= fondePouvoir[7].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[8]/E96']= fondePouvoir[7].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNationaliteGerant;
}
}
if (fondePouvoir.length > 8)	{
if (Value('id').of(fondePouvoir[8].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[9]/E91/E91.1'] = "1";
} else if (Value('id').of(fondePouvoir[8].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[9]/E91/E91.1'] = "2";
} else if (Value('id').of(fondePouvoir[8].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantModif') or Value('id').of(fondePouvoir[8].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantMaintenu')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[9]/E91/E91.1'] = "3";
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[9]/E91/E91.2']= date;
} else if (fondePouvoir[8].cadreIdentiteDirigeant.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(fondePouvoir[8].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[9]/E91/E91.2']= date;
}
if (fondePouvoir[8].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[9]/E92/E92.1']= "P";
} else if (not fondePouvoir[8].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[9]/E92/E92.1']= "S";
}	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[9]/E93/E93.2']= fondePouvoir[8].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (fondePouvoir[8].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[9]/E93/E93.4']= fondePouvoir[8].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for ( i = 0; i < fondePouvoir[8].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(fondePouvoir[8].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[9]/E93/E93.3']= prenoms;
if (not Value('id').of(fondePouvoir[8].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(fondePouvoir[8].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[9]/E94/E94.3']= fondePouvoir[8].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(fondePouvoir[8].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = fondePouvoir[8].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[9]/E94/E94.3']= result;
}
if (fondePouvoir[8].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[9]/E94/E94.5'] = fondePouvoir[8].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (fondePouvoir[8].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
var monId1 = Value('id').of(fondePouvoir[8].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[9]/E94/E94.6']          = monId1;
}
if (fondePouvoir[8].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[9]/E94/E94.7'] = fondePouvoir[8].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[9]/E94/E94.8'] = fondePouvoir[8].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? fondePouvoir[8].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (fondePouvoir[8].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[9]/E94/E94.10'] = fondePouvoir[8].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (fondePouvoir[8].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
var monId1 = Value('id').of(fondePouvoir[8].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[9]/E94/E94.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[9]/E94/E94.12'] = fondePouvoir[8].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(fondePouvoir[8].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[9]/E94/E94.13']= fondePouvoir[8].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(fondePouvoir[8].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[9]/E94/E94.13']= fondePouvoir[8].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(fondePouvoir[8].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[9]/E94/E94.14']= fondePouvoir[8].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
}
if (Value('id').of(fondePouvoir[8].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {
if (fondePouvoir[8].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
var dateTemp = new Date(parseInt(fondePouvoir[8].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[9]/E95/E95.1']= date;
}
if (not Value('id').of(fondePouvoir[8].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = fondePouvoir[8].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[9]/E95/E95.2']= result;
} else if (Value('id').of(fondePouvoir[8].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[9]/E95/E95.2'] = fondePouvoir[8].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}	
if (not Value('id').of(fondePouvoir[8].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[9]/E95/E95.3']= fondePouvoir[8].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(fondePouvoir[8].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[9]/E95/E95.4']= fondePouvoir[8].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[9]/E96']= fondePouvoir[8].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNationaliteGerant;
}
}
if (fondePouvoir.length > 9)	{
if (Value('id').of(fondePouvoir[9].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[10]/E91/E91.1'] = "1";
} else if (Value('id').of(fondePouvoir[9].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[10]/E91/E91.1'] = "2";
} else if (Value('id').of(fondePouvoir[9].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantModif') or Value('id').of(fondePouvoir[9].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantMaintenu')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[10]/E91/E91.1'] = "3";
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[10]/E91/E91.2']= date;
} else if (fondePouvoir[9].cadreIdentiteDirigeant.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(fondePouvoir[9].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[10]/E91/E91.2']= date;
}
if (fondePouvoir[9].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[10]/E92/E92.1']= "P";
} else if (not fondePouvoir[9].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[10]/E92/E92.1']= "S";
}	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[10]/E93/E93.2']= fondePouvoir[9].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (fondePouvoir[9].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[10]/E93/E93.4']= fondePouvoir[9].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for ( i = 0; i < fondePouvoir[9].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(fondePouvoir[9].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[10]/E93/E93.3']= prenoms;
if (not Value('id').of(fondePouvoir[9].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(fondePouvoir[9].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[10]/E94/E94.3']= fondePouvoir[9].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(fondePouvoir[9].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = fondePouvoir[9].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[10]/E94/E94.3']= result;
}
if (fondePouvoir[9].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[10]/E94/E94.5'] = fondePouvoir[9].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (fondePouvoir[9].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
var monId1 = Value('id').of(fondePouvoir[9].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[10]/E94/E94.6']          = monId1;
}
if (fondePouvoir[9].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[10]/E94/E94.7'] = fondePouvoir[9].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[10]/E94/E94.8'] = fondePouvoir[9].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? fondePouvoir[9].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (fondePouvoir[9].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[10]/E94/E94.10'] = fondePouvoir[9].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (fondePouvoir[9].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
var monId1 = Value('id').of(fondePouvoir[9].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[10]/E94/E94.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[10]/E94/E94.12'] = fondePouvoir[9].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(fondePouvoir[9].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[10]/E94/E94.13']= fondePouvoir[9].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(fondePouvoir[9].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[10]/E94/E94.13']= fondePouvoir[9].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(fondePouvoir[9].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[10]/E94/E94.14']= fondePouvoir[9].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
}
if (Value('id').of(fondePouvoir[9].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {
if (fondePouvoir[9].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
var dateTemp = new Date(parseInt(fondePouvoir[9].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[10]/E95/E95.1']= date;
}
if (not Value('id').of(fondePouvoir[9].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = fondePouvoir[9].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[10]/E95/E95.2']= result;
} else if (Value('id').of(fondePouvoir[9].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[10]/E95/E95.2'] = fondePouvoir[9].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}	
if (not Value('id').of(fondePouvoir[9].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[10]/E95/E95.3']= fondePouvoir[9].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(fondePouvoir[9].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[10]/E95/E95.4']= fondePouvoir[9].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[10]/E96']= fondePouvoir[9].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNationaliteGerant;
}
}
if (fondePouvoir.length > 10)	{
if (Value('id').of(fondePouvoir[10].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[11]/E91/E91.1'] = "1";
} else if (Value('id').of(fondePouvoir[10].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[11]/E91/E91.1'] = "2";
} else if (Value('id').of(fondePouvoir[10].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantModif') or Value('id').of(fondePouvoir[10].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantMaintenu')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[11]/E91/E91.1'] = "3";
}
if (Value('id').of(objet.cadreObjetModificationPM.objetSituationPM).contains('13M') and modifFormeJuridique.modifDateFormeJuridique != null) {
var dateTemp = new Date(parseInt(modifFormeJuridique.modifDateFormeJuridique.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[11]/E91/E91.2']= date;
} else if (fondePouvoir[10].cadreIdentiteDirigeant.dateModifDirigeant != null) {
var dateTemp = new Date(parseInt(fondePouvoir[10].cadreIdentiteDirigeant.dateModifDirigeant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[11]/E91/E91.2']= date;
}
if (fondePouvoir[10].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[11]/E92/E92.1']= "P";
} else if (not fondePouvoir[10].cadreIdentiteDirigeant.personneLieePersonnePouvoirLimiteEtablissementOuiBis) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[11]/E92/E92.1']= "S";
}	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[11]/E93/E93.2']= fondePouvoir[10].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomNaissanceGerant;
if (fondePouvoir[10].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[11]/E93/E93.4']= fondePouvoir[10].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNomUsageGerant;
}
var prenoms=[];
for ( i = 0; i < fondePouvoir[10].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant.size() ; i++ ){prenoms.push(fondePouvoir[10].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPPrenomGerant[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[11]/E93/E93.3']= prenoms;
if (not Value('id').of(fondePouvoir[10].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantPartant')) {
if (Value('id').of(fondePouvoir[10].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[11]/E94/E94.3']= fondePouvoir[10].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getId();
} else if (not Value('id').of(fondePouvoir[10].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
		var idPays = fondePouvoir[10].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[11]/E94/E94.3']= result;
}
if (fondePouvoir[10].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[11]/E94/E94.5'] = fondePouvoir[10].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNumeroVoie;
}
if (fondePouvoir[10].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie != null) {
var monId1 = Value('id').of(fondePouvoir[10].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseIndiceVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[11]/E94/E94.6']          = monId1;
}
if (fondePouvoir[10].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[11]/E94/E94.7'] = fondePouvoir[10].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[11]/E94/E94.8'] = fondePouvoir[10].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal != null ? fondePouvoir[10].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCodePostal : ".";
if (fondePouvoir[10].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[11]/E94/E94.10'] = fondePouvoir[10].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseComplementAdresse;
}
if (fondePouvoir[10].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie != null) {
var monId1 = Value('id').of(fondePouvoir[10].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[11]/E94/E94.11']          = monId1;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[11]/E94/E94.12'] = fondePouvoir[10].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseNomVoie;
if (Value('id').of(fondePouvoir[10].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[11]/E94/E94.13']= fondePouvoir[10].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseCommune.getLabel();
} else if (not Value('id').of(fondePouvoir[10].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[11]/E94/E94.13']= fondePouvoir[10].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdresseVille;
}
if (not Value('id').of(fondePouvoir[10].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[11]/E94/E94.14']= fondePouvoir[10].cadreIdentiteDirigeant.cadreAdresseDirigeant.dirigeantAdressePays.getLabel();
}
}
if (Value('id').of(fondePouvoir[10].cadreIdentiteDirigeant.personneLieeObjet).eq('dirigeantNouveau')) {
if (fondePouvoir[10].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant != null) {
var dateTemp = new Date(parseInt(fondePouvoir[10].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPDateNaissanceGerant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[11]/E95/E95.1']= date;
}
if (not Value('id').of(fondePouvoir[10].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
	var idPays = fondePouvoir[10].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[11]/E95/E95.2']= result;
} else if (Value('id').of(fondePouvoir[10].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[11]/E95/E95.2'] = fondePouvoir[10].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getId();
}	
if (not Value('id').of(fondePouvoir[10].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {																					 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[11]/E95/E95.3']= fondePouvoir[10].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant.getLabel();
}
if (Value('id').of(fondePouvoir[10].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissancePaysGerant).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[11]/E95/E95.4']= fondePouvoir[10].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPLieuNaissanceCommnuneGerant.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[11]/E96']= fondePouvoir[10].cadreIdentiteDirigeant.civilitePersonnePhysique.personneLieePPNationaliteGerant;
}
}
}


log.info("Regent fields : {}", regentFields);
//-->DEST-689 : Afficher les erreurs XSD dans la formalité quand il y'a une erreur de génération

// Call to the XML-REGENT generation WS 
var response = nash.service.request('${regent.baseUrl}/private/v1/xml-regent/generate/{regentVersion}/{authorityType}/{authorityId}', regentVersion, authorityType, authorityId) 
.dataType('application/json') // 
.accept('json') // 
.param('listTypeEvenement',eventRegent) 
//-->MINE-263 : Permettre à l'équipe FF de voir les problèmes de validation XSD et le regent sur studio
.continueOnError(true) //
.post(JSON.stringify(regentFields)); 

// Record the generated XML-REGENT 
//MOD déplacement du test de la réponse du premier ws
if (response != null && response.status == 200) { 
    var xmlRegentStr = response.asBytes(); 
    nash.record.saveFile("XML_REGENT.xml",xmlRegentStr); 

//debut de l'ajout
 if (authorityId2 != null) {
//MOD extraction du numéro de liasse du premeir regent généré
var numeroLiasse = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02');
numeroLiasse = JSON.parse(numeroLiasse);
_log.info('extracted numero de liasse is {}', numeroLiasse);
 regentFields['/REGENT-XML/Destinataire']= authorityId2;
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05']= "C";
 
 // Filtre regent greffe
var undefined;
// Groupe NGM
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/NGM/M27']= undefined;
// Groupe GCS/ISS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GDR/GCS/ISS/A10/A10.1']= undefined;	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GDR/GCS/ISS/A10/A10.2']= undefined;	
// Groupe GCS/SNS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GDR/GCS/SNS/A21/A21.2']= undefined;	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GDR/GCS/SNS/A21/A21.3']= undefined;	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GDR/GCS/SNS/A21/A21.4']= undefined;	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GDR/GCS/SNS/A21/A21.6']= undefined;	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GDR/GCS/SNS/A22/A22.3']= undefined;	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GDR/GCS/SNS/A22/A22.4']= undefined;	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GDR/GCS/SNS/A24/A24.2']= undefined;	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GDR/GCS/SNS/A24/A24.3']= undefined;	
// Groupe GCS/CAS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GDR/GCS/CAS/A42/A42.1']= undefined;	
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GDR/GCS/CAS/A44']= undefined;	
// Groupe GCS/SCS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GCS/SCS/A53/A53.4']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GCS/SCS/A55/A55.1'] = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GCS/SCS/A55/A55.2'] = undefined;
// Groupe GCS/SAS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GCS/SAS/A68/A68.2'] = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GCS/SAS/A68/A68.3']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GCS/SAS/A68/A68.4']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GCS/SAS/A69/A69.1']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GCS/SAS/A69/A69.2']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GCS/SAS/A69/A69.3']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GCS/SAS/A69/A69.4']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GCS/SAS/A70'] = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GCS/SAS/A71/A71.3']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GCS/SAS/A71/A71.5'] = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GCS/SAS/A71/A71.6'] = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GCS/SAS/A71/A71.7'] = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GCS/SAS/A71/A71.8'] = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GCS/SAS/A71/A71.10'] = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GCS/SAS/A71/A71.11'] = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GCS/SAS/A71/A71.12'] = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GCS/SAS/A71/A71.13'] = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GCS/SAS/A71/A71.14']= undefined;
// Groupe SAE
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E84']=undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E85/E85.8']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E86']= undefined;


 //MOD modification de l'appel pour la génération du deuxième régent afin de prendre en compte le numéro de liasse du premier
 var response2 = nash.service.request('${regent.baseUrl}/private/v1/xml-regent/generate/{regentVersion}/{authorityType}/{authorityId}', regentVersion, authorityType2, authorityId2) 
.dataType('application/json') // 
.accept('json') // 
.param('listTypeEvenement',eventRegent) 
.param('liasseNumber', numeroLiasse.C02) 
.continueOnError(true)
.post(JSON.stringify(regentFields)); 


	//Début de l'ajout
	//MOD modifier reponse en response2
	if (response2 != null && response2.status == 200) { 
		var xmlRegentStr = response2.asBytes(); 
		nash.record.saveFile("XML_REGENT2.xml",xmlRegentStr);
	}else{
		
		var returnedResponse = response2.asObject();
		_log.info("Call ws regent returned errors  {}", returnedResponse);
		
		
		var regent2 = returnedResponse['regent'];
		var errors2 = returnedResponse['errors']; 
		nash.record.saveFile("XML_REGENT2.xml",regent2.getBytes()); 
		nash.record.saveFile("XML_VALIDATION_ERRORS_2.xml",errors2.getBytes()); 
		
		//-->MINE-263 : Permettre à l'équipe FF de voir les problèmes de validation XSD et le regent sur studio
		
		return spec.create({
		id : 'xmlGenerationConfirmation',
		label : "Xml Regent confirmation message",
		groups : [ spec.createGroup({
				id : 'Regent ',
				description : "Une erreur s'est produite lors de la génération du XML Regent.",
				data : [
				spec.createData({
					id: 'regent',
					label: "XML regent généré",
					type: 'Text',
					mandatory: true,
					value: regent2
				}),spec.createData({
					id: 'errors',
					label: "Erreurs de validations xsd",
					type: 'Text',
					mandatory: false,
					value: errors2
				}),spec.createData({
					id: 'blockingField',
					label: "xsd erroné",
					help:"Ce champs sert à arrêter le process du dossier pour que le support puisse le consulter",
					type: 'StringReadOnly',
					mandatory: true
				})]
			})]
		});
	}	
}	

	return spec.create({
	id : 'xmlGenerationConfirmation',
	label : "Xml Regent confirmation message",
	groups : [ spec.createGroup({
		id : 'confirmationMessageOk',
		description : "Le fichier XML Regent a été généré et ajouté au dossier.",
		data : []
		}) ]
	});
	
}else{
	
	var returnedResponse = response.asObject();
	_log.info("Call ws regent returned errors  {}", returnedResponse);
	
	
	var regent = returnedResponse['regent'];
	var errors = returnedResponse['errors']; 
	nash.record.saveFile("XML_REGENT.xml",regent.getBytes()); 
	nash.record.saveFile("XML_VALIDATION_ERRORS.xml",errors.getBytes()); 
	
	
	return spec.create({
	id : 'xmlGenerationConfirmation',
	label : "Xml Regent confirmation message",
	groups : [ spec.createGroup({
		id : 'Regent ',
		description : "Une erreur s'est produite lors de la génération du XML Regent de la première autorité.",
		data : [
				spec.createData({
					id: 'regent',
					label: "XML regent généré",
					type: 'Text',
					mandatory: true,
					value: regent
				}),spec.createData({
					id: 'errors',
					label: "Erreurs de validations xsd",
					type: 'Text',
					mandatory: false,
					value: errors
				}),spec.createData({
					id: 'blockingField',
					label: "xsd erroné",
					help:"Ce champs sert à arrêter le process du dossier pour que le support puisse le consulter",
					type: 'StringReadOnly',
					mandatory: true
			})]
		}) ]
	});
}	