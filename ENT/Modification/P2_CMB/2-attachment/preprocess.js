var objet= $p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification;
var exploitant1 = $p2cmb.cadreExploitantIndivisionGroup.cadreExploitantIndivision;
var exploitant2 = $p2cmb.cadreExploitantIndivisionGroup.cadreExploitantIndivision2;
var etablissementNew = $p2cmb.cadre6EtablissementGroup.infoEtablissementNew;
var modifIdentiteDeclarant = $p2cmb.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle;
var activite = $p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite;

// PJ Signataire

var userDeclarant;
if ($p2cmb.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNouveauNomUsage != null and Value('id').of($p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) {
    var userDeclarant = $p2cmb.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNouveauNomUsage + '  ' + (Value('id').of($p2cmb.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNomPrenom).contains('modifPrenom') ? $p2cmb.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNouveauPrenoms[0] : $p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiquePrenom1[0]) ;
} else if ($p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiqueNomUsage != null and not Value('id').of($p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) {
    var userDeclarant = $p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiqueNomUsage + '  ' + (Value('id').of($p2cmb.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNomPrenom).contains('modifPrenom') ? $p2cmb.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNouveauPrenoms[0] : $p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiquePrenom1[0]) ;
} else {
    var userDeclarant = (Value('id').of($p2cmb.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNomPrenom).contains('modifNomNaissance') ? $p2cmb.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNouveauNomNaissance : $p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiqueNomNaissance) + '  '+ (Value('id').of($p2cmb.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNomPrenom).contains('modifPrenom') ? $p2cmb.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNouveauPrenoms[0] : $p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiquePrenom1[0]) ;
}

var pj=$p2cmb.cadre9SignatureGroup.cadre9Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteDeclarant')) {
    attachment('pjIDDeclarantSignataire', 'pjIDDeclarantSignataire', { label: userDeclarant, mandatory:"true"});
}

if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire')) {
    attachment('pjPouvoir', 'pjPouvoir', { mandatory:"true"});
}

var userMandataire = $p2cmb.cadre9SignatureGroup.cadre9Signature.adresseMandataire;
var pj=$p2cmb.cadre9SignatureGroup.cadre9Signature.soussigne;
if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') or Value('id').of(pj).eq('FormaliteSignataireQualiteAutre')) {
    attachment('pjIDMandataireSignataire', 'pjIDMandataireSignataire', {label: userMandataire.nomPrenomDenominationMandataire, mandatory:"true"});
}

// PJ 10P / 15P / 

if((Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or Value('id').of(pj).eq('FormaliteSignataireQualiteAutre')) 
	and (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P') 
	or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P') 
	and not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('17P'))) {
    attachment('pjIDDeclarantNonSignataire1', 'pjIDDeclarantNonSignataire1', { label: userDeclarant, mandatory:"true"});
}

// 16P 

if((Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or Value('id').of(pj).eq('FormaliteSignataireQualiteAutre')) 
	and (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P')
	and not	Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P')
	and not	Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')
	and not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('17P'))) {
    attachment('pjIDDeclarantNonSignataire3', 'pjIDDeclarantNonSignataire3', { label: userDeclarant, mandatory:"false"});
}

// PJ 17P

if((Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or Value('id').of(pj).eq('FormaliteSignataireQualiteAutre')) 
	and Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('17P')) {
    attachment('pjIDDeclarantNonSignataire2', 'pjIDDeclarantNonSignataire2', { label: userDeclarant, mandatory:"true"});
}

// PJ 22P ou 42P

if(Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('decesExploitant')) {
    attachment('pjIDActeDeces', 'pjIDActeDeces', { mandatory:"true"});
    attachment('pjIDActeNotoriete', 'pjIDActeNotoriete', { mandatory:"true"});
}

if (exploitant1.exploitantMineurEmancipe or exploitant2.exploitantMineurEmancipe) {
    attachment('pjIDMineurEmancipe', 'pjIDMineurEmancipe', { mandatory:"true"});
}	

// PJ 25P

if(($p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetDeclarationEIRL.declarationEIRL and not Value('id').of($p2cmb.cadre4DeclarationAffectationPatrimoineGroup.cadreDeclarationAffectationPatrimoine.eirlDepot).eq('eirlSansDepot')) or Value('id').of($p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEIRL.objetEIRL).contains('modificationEIRL') or $p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification.entrepriseEIRL) {
    attachment('pjDAP', 'pjDAP', { mandatory:"true"});
}
var pj=$p2cmb.cadre4DeclarationAffectationPatrimoineGroup;
if(Value('id').of(pj.cadreEIRLInformationsComplementaires.contenuDAP).contains('bienImmo') or Value('id').of(pj.cadreEIRLInformationsComplementairesBis.contenuDAPBis).contains('bienImmo') or Value('id').of($p2cmb.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.contenuDAP).contains('bienImmo')) {
    attachment('pjDAPActeNotarie', 'pjDAPActeNotarie', { mandatory:"true"});
}
if(Value('id').of(pj.cadreEIRLInformationsComplementaires.contenuDAP).contains('bienCommunIndivis') or Value('id').of(pj.cadreEIRLInformationsComplementairesBis.contenuDAPBis).contains('bienCommunIndivis') or Value('id').of($p2cmb.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.contenuDAP).contains('bienCommunIndivis')) {
    attachment('pjDAPAccordTiers', 'pjDAPAccordTiers', { mandatory:"true"});
}

// PJ 28P

if(Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('28P')) {
    attachment('pjInsaisissabiliteActeNotarie', 'pjInsaisissabiliteActeNotarie', { mandatory:"true"});
}

// PJ 30P

var conjoint = $p2cmb.cadre3ModificationConjointGroup.cadre3ModificationConjoint;

if(Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('30P') and Value('id').of(conjoint.objetModifStatut).eq('nouveauStatut') and Value('id').of(conjoint.objetModifConjoint).eq('conjointCollaborateur')) {
    attachment('pjIDConjoint', 'pjIDConjoint', { mandatory:"true"});
}

if(Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('30P') and (Value('id').of(conjoint.objetModifStatut).eq('nouveauStatut') or Value('id').of(conjoint.objetModifStatut).eq('modificationStatut'))) {
    attachment('pjChoixStatutConjoint', 'pjChoixStatutConjoint', { mandatory:"true"});
}


// PJ 54P / 11P / 80P 

if(objet.domicileEntreprise or objet.entrepriseDomicile) {
    attachment('pjDomicile', 'pjDomicile', { mandatory:"true"});
}

var pj=$p2cmb.cadre7EtablissementActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds ;
if((Value('id').of(pj).contains('EtablissementOrigineFondsCreation') 
	and (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')))
or ((Value('id').of(pj).contains('EtablissementOrigineFondsAchat')) 
and $p2cmb.cadre7EtablissementActiviteGroup.cadre4OrigineFonds.precedentExploitant.fondsArtisanal) 
or (Value('id').of(pj).contains('EtablissementOrigineFondsCocheAutre'))) {
    attachment('pjEtablissementCreation', 'pjEtablissementCreation', { mandatory:"true"});
}

var pj=$p2cmb.cadre7EtablissementActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsAchat') 
	and ($p2cmb.cadre7EtablissementActiviteGroup.cadre4OrigineFonds.precedentExploitant.cessionFonds)) {
    attachment('pjPlanCession', 'pjPlanCession', { mandatory:"true"});
}

// Egalement 61P et 64P

var pj=$p2cmb.cadre7EtablissementActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsLocationGerance')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre4OrigineFonds.geranceMandat.typeFonds).eq('typeFondsLocation')) {
    attachment('pjLocationGerance', 'pjLocationGerance', { mandatory:"true"});
	attachment('pjLocationGerancePublication', 'pjLocationGerancePublication', { mandatory:"true"});
}

var pj=$p2cmb.cadre7EtablissementActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsGeranceMandat')
    or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre4OrigineFonds.geranceMandat.typeFonds).eq('typeFondsGerance')) {
    attachment('pjGeranceMandat', 'pjGeranceMandat', { mandatory:"true"});
    attachment('pjGeranceMandatPublication', 'pjGeranceMandatPublication', { mandatory:"true"});
}

// Egalement 61P et 63P

var pj=$p2cmb.cadre7EtablissementActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds ;
if((Value('id').of(pj).contains('EtablissementOrigineFondsAchat') 
	and $p2cmb.cadre7EtablissementActiviteGroup.cadre4OrigineFonds.precedentExploitant.etablissementJournalAnnoncesLegalesNom != null)
	or Value('id').of($p2cmb.cadre6EtablissementGroup.infoEtablissementNew.origineAcquision).eq('achat')) {
    attachment('pjAchat', 'pjAchat', { mandatory:"true"});
    attachment('pjAchatPublication', 'pjAchatPublication', { mandatory:"true"});
}

if(Value('id').of(pj).contains('EtablissementOrigineFondsDonation') 
	or Value('id').of($p2cmb.cadre6EtablissementGroup.infoEtablissementNew.origineAcquision).eq('donation')) {
    attachment('pjDonation', 'pjDonation', { mandatory:"true"});
}
 
if(Value('id').of(pj).contains('EtablissementOrigineFondsDevolution') 
	or Value('id').of($p2cmb.cadre6EtablissementGroup.infoEtablissementNew.origineAcquision).eq('devolution')) {
    attachment('pjDevolutionSuccessorale', 'pjDevolutionSuccessorale', { mandatory:"true"});
}

// PJ 70P
var personneLiee1 = $p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.infoPeronnePouvoir;
var personneLiee2 = $p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.infoPeronnePouvoir2;
var personneLiee3 = $p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.infoPeronnePouvoir3;
var personneLiee4 = $p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.infoPeronnePouvoir4;
var personneLiee5 = $p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.infoPeronnePouvoir5;
var personneLiee6 = $p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.infoPeronnePouvoir6;
var personneLiee7 = $p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.infoPeronnePouvoir7;
var personneLiee8 = $p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.infoPeronnePouvoir8;
var personneLiee9 = $p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.infoPeronnePouvoir9;

// Fondé de pouvoir 1
var userPersonnePouvoir1 = personneLiee1.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir + ' ' + personneLiee1.personneLieePersonnePhysiquePrenom1PersonnePouvoir[0];
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee1.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee1.objetModifPersonneLiee).eq('partantPersonneLiee'))	{
	attachment('pjIDPersonnePouvoir', 'pjIDPersonnePouvoir', {label: userPersonnePouvoir1, mandatory:"true"});
}
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee1.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee1.objetModifPersonneLiee).eq('partantPersonneLiee')
	and not Value('id').of(personneLiee1.objetModifPersonneLiee).eq('modifPersonneLiee'))	{
	attachment('pjDNCPersonnePouvoir', 'pjDNCPersonnePouvoir', {label: userPersonnePouvoir1, mandatory:"true"});
}

// Fondé de pouvoir 2
var userPersonnePouvoir2 = personneLiee2.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir + ' ' + personneLiee2.personneLieePersonnePhysiquePrenom1PersonnePouvoir[0];
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee2.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee2.objetModifPersonneLiee).eq('partantPersonneLiee')
	and personneLiee1.autreDeclarationPersonneLiee or (Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')))	{
	attachment('pjIDPersonnePouvoir2', 'pjIDPersonnePouvoir2', {label: userPersonnePouvoir2, mandatory:"true"});
}
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee2.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee2.objetModifPersonneLiee).eq('partantPersonneLiee')
	and not Value('id').of(personneLiee2.objetModifPersonneLiee).eq('modifPersonneLiee')	
	and personneLiee1.autreDeclarationPersonneLiee or (Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')))	{
	attachment('pjDNCPersonnePouvoir2', 'pjDNCPersonnePouvoir2', {label: userPersonnePouvoir2, mandatory:"true"});
}

// Fondé de pouvoir 3
var userPersonnePouvoir3 = personneLiee3.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir + ' ' + personneLiee3.personneLieePersonnePhysiquePrenom1PersonnePouvoir[0];
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee3.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee3.objetModifPersonneLiee).eq('partantPersonneLiee')
	and personneLiee2.autreDeclarationPersonneLiee)	{
	attachment('pjIDPersonnePouvoir3', 'pjIDPersonnePouvoir3', {label: userPersonnePouvoir3, mandatory:"true"});
}
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee3.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee3.objetModifPersonneLiee).eq('partantPersonneLiee')
	and not Value('id').of(personneLiee3.objetModifPersonneLiee).eq('modifPersonneLiee')	
	and personneLiee2.autreDeclarationPersonneLiee)	{
	attachment('pjDNCPersonnePouvoir3', 'pjDNCPersonnePouvoir3', {label: userPersonnePouvoir3, mandatory:"true"});
}

// Fondé de pouvoir 4
var userPersonnePouvoir4 = personneLiee4.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir + ' ' + personneLiee4.personneLieePersonnePhysiquePrenom1PersonnePouvoir[0];
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee4.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee4.objetModifPersonneLiee).eq('partantPersonneLiee')
	and personneLiee3.autreDeclarationPersonneLiee)	{
	attachment('pjIDPersonnePouvoir4', 'pjIDPersonnePouvoir4', {label: userPersonnePouvoir4, mandatory:"true"});
}
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee4.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee4.objetModifPersonneLiee).eq('partantPersonneLiee')
	and not Value('id').of(personneLiee4.objetModifPersonneLiee).eq('modifPersonneLiee')	
	and personneLiee3.autreDeclarationPersonneLiee)	{
	attachment('pjDNCPersonnePouvoir4', 'pjDNCPersonnePouvoir4', {label: userPersonnePouvoir4, mandatory:"true"});
}

// Fondé de pouvoir 5
var userPersonnePouvoir5 = personneLiee5.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir + ' ' + personneLiee5.personneLieePersonnePhysiquePrenom1PersonnePouvoir[0];
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee5.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee5.objetModifPersonneLiee).eq('partantPersonneLiee')
	and personneLiee4.autreDeclarationPersonneLiee)	{
	attachment('pjIDPersonnePouvoir5', 'pjIDPersonnePouvoir5', {label: userPersonnePouvoir5, mandatory:"true"});
}
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee5.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee5.objetModifPersonneLiee).eq('partantPersonneLiee')
	and not Value('id').of(personneLiee5.objetModifPersonneLiee).eq('modifPersonneLiee')	
	and personneLiee4.autreDeclarationPersonneLiee)	{
	attachment('pjDNCPersonnePouvoir5', 'pjDNCPersonnePouvoir5', {label: userPersonnePouvoir5, mandatory:"true"});
}

// Fondé de pouvoir 6
var userPersonnePouvoir6 = personneLiee6.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir + ' ' + personneLiee6.personneLieePersonnePhysiquePrenom1PersonnePouvoir[0];
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee6.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee6.objetModifPersonneLiee).eq('partantPersonneLiee')
	and personneLiee5.autreDeclarationPersonneLiee)	{
	attachment('pjIDPersonnePouvoir6', 'pjIDPersonnePouvoir6', {label: userPersonnePouvoir6, mandatory:"true"});
}
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee6.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee6.objetModifPersonneLiee).eq('partantPersonneLiee')
	and not Value('id').of(personneLiee6.objetModifPersonneLiee).eq('modifPersonneLiee')	
	and personneLiee5.autreDeclarationPersonneLiee)	{
	attachment('pjDNCPersonnePouvoir6', 'pjDNCPersonnePouvoir6', {label: userPersonnePouvoir6, mandatory:"true"});
}

// Fondé de pouvoir 7
var userPersonnePouvoir7 = personneLiee7.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir + ' ' + personneLiee7.personneLieePersonnePhysiquePrenom1PersonnePouvoir[0];
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee7.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee7.objetModifPersonneLiee).eq('partantPersonneLiee')
	and personneLiee6.autreDeclarationPersonneLiee)	{
	attachment('pjIDPersonnePouvoir7', 'pjIDPersonnePouvoir7', {label: userPersonnePouvoir7, mandatory:"true"});
}
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee7.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee7.objetModifPersonneLiee).eq('partantPersonneLiee')
	and not Value('id').of(personneLiee7.objetModifPersonneLiee).eq('modifPersonneLiee')	
	and personneLiee6.autreDeclarationPersonneLiee)	{
	attachment('pjDNCPersonnePouvoir7', 'pjDNCPersonnePouvoir7', {label: userPersonnePouvoir7, mandatory:"true"});
}

// Fondé de pouvoir 8
var userPersonnePouvoir8 = personneLiee8.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir + ' ' + personneLiee8.personneLieePersonnePhysiquePrenom1PersonnePouvoir[0];
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee8.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee8.objetModifPersonneLiee).eq('partantPersonneLiee')
	and personneLiee7.autreDeclarationPersonneLiee)	{
	attachment('pjIDPersonnePouvoir8', 'pjIDPersonnePouvoir8', {label: userPersonnePouvoir8, mandatory:"true"});
}
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee8.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee8.objetModifPersonneLiee).eq('partantPersonneLiee')
	and not Value('id').of(personneLiee8.objetModifPersonneLiee).eq('modifPersonneLiee')	
	and personneLiee7.autreDeclarationPersonneLiee)	{
	attachment('pjDNCPersonnePouvoir8', 'pjDNCPersonnePouvoir8', {label: userPersonnePouvoir8, mandatory:"true"});
}

// Fondé de pouvoir 9
var userPersonnePouvoir9 = personneLiee9.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir + ' ' + personneLiee9.personneLieePersonnePhysiquePrenom1PersonnePouvoir[0];
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee9.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee9.objetModifPersonneLiee).eq('partantPersonneLiee')
	and personneLiee8.autreDeclarationPersonneLiee)	{
	attachment('pjIDPersonnePouvoir9', 'pjIDPersonnePouvoir9', {label: userPersonnePouvoir9, mandatory:"true"});
}
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee9.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee9.objetModifPersonneLiee).eq('partantPersonneLiee')
	and not Value('id').of(personneLiee9.objetModifPersonneLiee).eq('modifPersonneLiee')	
	and personneLiee8.autreDeclarationPersonneLiee)	{
	attachment('pjDNCPersonnePouvoir9', 'pjDNCPersonnePouvoir9', {label: userPersonnePouvoir9, mandatory:"true"});
}

// PJ JQPA

var userJQPA1 = $p2cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification1.jqpaActiviteJqpaPP ;
var pj = $p2cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification1.jqpaSituation ;
if(Value('id').of(pj).eq('jqpaSituationDiplome')) {
	attachment('pjDiplomes1', 'pjDiplomes1', {label: userJQPA1, mandatory:"true"});
}
var pj=$p2cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification1.jqpaSituation ;
if(Value('id').of(pj).eq('jqpaSituationExperience')) {
	attachment('pjPiecesUtiles1', 'pjPiecesUtiles1', {label: userJQPA1, mandatory:"true"});
}

var userJQPA2 = $p2cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification2.jqpaActiviteJqpaPP ;
var pj=$p2cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification2.jqpaSituation ;
if(Value('id').of(pj).eq('jqpaSituationDiplome')) {
	attachment('pjDiplomes2', 'pjDiplomes2', {label: userJQPA2, mandatory:"true"});
}
var pj=$p2cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification2.jqpaSituation ;
if(Value('id').of(pj).eq('jqpaSituationExperience')) {
	attachment('pjPiecesUtiles2', 'pjPiecesUtiles2', {label: userJQPA2, mandatory:"true"});
}

var userJQPA3 = $p2cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification3.jqpaActiviteJqpaPP ;
var pj=$p2cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification3.jqpaSituation ;
if(Value('id').of(pj).eq('jqpaSituationDiplome')) {
	attachment('pjDiplomes3', 'pjDiplomes3', {label: userJQPA3, mandatory:"true"});
}
var pj=$p2cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification3.jqpaSituation ;
if(Value('id').of(pj).eq('jqpaSituationExperience')) {
	attachment('pjPiecesUtiles3', 'pjPiecesUtiles3', {label: userJQPA3, mandatory:"true"});
}

// PJ Activité réglementée

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P')
	or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')
	or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P')
	or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('17P')
	or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('22P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
	or Value('id').of(activite.activiteEvenement).eq('61P')
	or Value('id').of(activite.activiteEvenement).eq('61P62P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')) {
	attachment('pjIDActiviteReglementee', 'pjIDActiviteReglementee', {mandatory:"false"});
}
		
