var attachment= "/4-signature/generated/proxyResult.files-1-PROXY_SIGNED_DOCUMENT.pdf";
var adresseNew = $p2cmb.cadre6EtablissementGroup.infoEtablissementNew.adresseEtablissementNew;
var adresseOld = $p2cmb.cadre6EtablissementGroup.infoEtablissementOld.adresseEtablissementOld;
var adressePro = $p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.adresseEtablissementPrincipal;
var adresseEIRL = $p2cmb.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreRappelIdentificationEIRL.cadreEirlRappelAdresse;
var adresseDom = $p2cmb.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifDomicile.newAdresseDomicile;
var adresseFondsDonne = $p2cmb.cadreFondsDonneLocationGeranceGroup.cadreFondsDonneLocationGerance.adresseFondsDonneLocationGerance;
var algo = "trouver destinataire";
var secteur2 = '';
if ((Value('id').of($p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.immatriculation).contains('immatRM')
	or (not Value('id').of($p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.immatriculation).contains('immatRM')
	and (($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA != null
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	or ($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA != null
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	or ($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA != null
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.jqpaDomaineTransport).eq('jqpaTransportAutre')))))
	and not $p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.optionCMACCI) 	{
var secteur1 =  "ARTISANAL";
} else { var secteur1 =  "COMMERCIAL"; }

if (Value('id').of($p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.immatriculation).contains('immatRCS')
	or (not Value('id').of($p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.immatriculation).contains('immatRCS')
	and (Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.jqpaDomaineTransport).eq('jqpaTransportAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA).eq('nafaAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineAlimentation).eq('alimentationAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineAutresServices).eq('servicesAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineProduction).eq('productionAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.jqpaDomaineTransport).eq('jqpaTransportAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA).eq('nafaAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineAlimentation).eq('alimentationAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineAutresServices).eq('servicesAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineProduction).eq('productionAutre') 
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.jqpaDomaineTransport).eq('jqpaTransportAutre')))) {
	
	secteur2 =  "COMMERCIAL";
}
var typePersonne = "PP";
var formJuridique = ($p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.formeJuridique or $p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetDeclarationEIRL.declarationEIRL) ? "EIRL" : "EI";
var optionCMACCI = "NON";
var formalityWithPayment = false;
var listFraisDestinataires=[];
var listDistinctAuthorities =[];
var destFuncId2 = '';		 
var codeEDI2 = '';
var partnerLabel2 = '';

			 
_log.info("algo is {}",algo);
_log.info("secteur1 is {}",secteur1);
_log.info("typePersonne is {}",typePersonne);
_log.info("formJuridique is {}",formJuridique);
_log.info("optionCMACCI is {}",optionCMACCI);

var codeCommune = adresseNew.etablissementAdresseCommuneNew != null ? adresseNew.etablissementAdresseCommuneNew.getId() : 
				($p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification.domicileEntreprise ? adresseDom.personneLieeAdresseCommuneNew.getId() : 
				((Value('id').of($p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).contains('modifEIRL')
				and Value('id').of($p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEIRL.objetModificationEIRL).eq('modifAdresse')
				and not Value('id').of($p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).contains('situationPerso')
				and not Value('id').of($p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).contains('etablissement')
				and not Value('id').of($p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).contains('dateActivite')) ?	
				adresseEIRL.communeAdresseEirl.getId() : 
				((Value('id').of($p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).contains('etablissement') 
				and Value('id').of($p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEtablissement.objetEtablissement).eq('80P')) ? 
				adresseOld.etablissementAdresseCommuneOld.getId() : 
				((Value('id').of($p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEtablissement.objetEtablissement).eq('miseEnLocation') and adresseFondsDonne.fondsDonneAdresseCommune != null) ?
				adresseFondsDonne.fondsDonneAdresseCommune.getId() : adressePro.etablissementAdresseCommune.getId()))));


_log.info("codeCommune is {}",codeCommune);


//=================================> calcul des destinataires Début<=========================================
var args = {
    "secteur1" : secteur1,
	"secteur2" : secteur2,
	"typePersonne" : typePersonne,
	"formJuridique" : formJuridique,
	"optionCMACCI":optionCMACCI,
	"codeCommune": codeCommune
};

_log.info("serviceParams  {}",JSON.stringify(args));

// call directory service to find functional id of authority 
var response = nash.service.request('${directory.baseUrl}/v1/algo/{code}/execute',algo) //
        .connectionTimeout(10000) //
        .receiveTimeout(10000)
        .accept('application/json') //
        .post(args);

var receiverInfo = JSON.parse(response.asString());

_log.info("receiverInfo  is {}", receiverInfo);

_log.info("listAuthorities  is {}", receiverInfo.listAuthorities);
_log.info("type of details of listAuthorities is  {}", typeof receiverInfo.listAuthorities);
_log.info("listAuthorities.length is {}",  receiverInfo.listAuthorities.length);

//===============================> Récupérer la liste des autorités distincte (sans doublons pour construire le bloc destiné à la génération du regent XML et le XML-TC <==============================================

	//Début<=========================================	
	for(var i=0; i<receiverInfo.listAuthorities.length; i++){	
		if(listDistinctAuthorities.indexOf(receiverInfo.listAuthorities[i]['authority'])== -1){	
			listDistinctAuthorities.push(receiverInfo.listAuthorities[i]['authority']);
		}
	}	
	_log.info("listAuthorities.length is {}",  listDistinctAuthorities.length);
	_log.info("listDistinctAuthorities  is {}", listDistinctAuthorities);
	//Fin ===========================================>
	
	//Récupérer les informations des destinataires 
		//Début<=========================================
		_log.info("1er destinataire  is {}", listDistinctAuthorities[0]);
		var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', listDistinctAuthorities[0]) //
				   .connectionTimeout(10000) //
				   .receiveTimeout(10000) //
				   .accept('json') //
				   .get();
		//result
		var receiverInfo2 = response.asObject();

		// prepare all information of receiver to create data.xml

		var funcId = !receiverInfo2.entityId ? null : receiverInfo2.entityId;
		var funcLabel = !receiverInfo2.label ? null :receiverInfo2.label;
		var contactInfo = !receiverInfo2.details ? null : receiverInfo2.details;
		var email = !contactInfo.profile.email ? null : contactInfo.profile.email;
		var tel = !contactInfo.profile.tel ? null : contactInfo.profile.tel;
		
		if(receiverInfo.listAuthorities[0]['role'] == "CFE"){
			_log.info("*********Première autorité est un CFE******");	
			var codeEDI = !contactInfo.ediCode ? null : contactInfo.ediCode;
		}else{
			_log.info("*********Première autorité est un TDR******");
			var codeEDI2 = !contactInfo.ediCode ? null : contactInfo.ediCode;	
		}
		
		var partnerLabel = !contactInfo.profile.address.recipientName ? null : contactInfo.profile.address.recipientName;

		if(listDistinctAuthorities.length == 2){
			_log.info("2eme destinataire  is {}", listDistinctAuthorities[1]);
			_log.info("******récupération du second destinataire******");
			var response2 = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', listDistinctAuthorities[1]) //
				   .connectionTimeout(10000) //
				   .receiveTimeout(10000) //
				   .accept('json') //
				   .get();
			//result
			var receiverInfo3 = response2.asObject();

			// prepare all information of receiver to create data.xml
			destFuncId2 = listDistinctAuthorities[1];
			contactInfo = !receiverInfo3.details ? null : receiverInfo3.details;

			if(receiverInfo.listAuthorities[1]['role'] == "CFE"){
				_log.info("*********Deuxième autorité est un CFE******");	
				var codeEDI = !contactInfo.ediCode ? null : contactInfo.ediCode;
			}else{
				_log.info("*********Deuxième autorité est un TDR******");	
				var codeEDI2 = !contactInfo.ediCode ? null : contactInfo.ediCode;	
			}

			 partnerLabel2 = !contactInfo.profile.address.recipientName ? null : contactInfo.profile.address.recipientName;
			
		}	
		var regentXmlTcBloc = spec.createGroup({
									id: 'dataGroup',
									label: "Contenu d'une autorité compétente",
									data: [spec.createData({
											id: 'destFuncId',
											label: "Le code de l'autorité compétente",
											description: 'Code interne',
											type: 'String',
											mandatory: true,
											value: funcId
										}), spec.createData({
											id: 'labelFunc',
											label: "Le libellé fonctionnelle de l'autorité compétente",
											type: 'String',
											mandatory : true,
											value: funcLabel
										}), spec.createData({
											id: 'email',
											label: "L'adresse email de l'autorité compétente",
											type: 'email',
											mandatory:true,
											value: email
										}),	spec.createData({
											id: 'emailPj',
											label: "Le chemin de la pièce jointe",
											type: 'String',
											value: attachment
									}),   spec.createData({
											id: 'tel',
											label: "telephone",
											type: 'String',
											value: tel
									}), spec.createData({
											id: 'partnerLabel',
											label: "Nom autorité",
											type: 'String',
											value: partnerLabel
									}),spec.createData({
											id: 'codeEDI',
											label: "code EDI",
											type: 'String',
											value: codeEDI
									}),
									//début de l'ajout
									spec.createData({
											id: 'destFuncId2',
											label: "code de la deuxième autorité",
											type: 'String',
											value: destFuncId2
									}),spec.createData({
											id: 'codeEDI2',
											label: "code EDI de la deuxième autorité",
											type: 'String',
											value: codeEDI2
									}),spec.createData({
											id: 'partnerLabel2',
											label: "Nom de la deuxième autorité",
											type: 'String',
											value: partnerLabel2
									})
									//fin de l'ajout	
									]
								});
	//Fin ===========================================>	
//=================================>  calcul des destinataires Fin <=========================================

	//=================================> Partie qui sera mise par le rédacteur pour conditionner le couple frais autoprité dans le cas où la formalité est payante et donc le bloc est conditionné

	//Début<=========================================	
//Condition pour dire qu'une formalité contient un paiement
var objet= $p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification;
var identite= $p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification;
var etablissement = $p2cmb.cadre6EtablissementGroup.infoEtablissementOld;
var etablissementNew = $p2cmb.cadre6EtablissementGroup.infoEtablissementNew;
var activite = $p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite;
var origine = $p2cmb.cadre7EtablissementActiviteGroup.cadre4OrigineFonds ;
var activiteInfo = $p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite;
var fondsDonne = $p2cmb.cadreFondsDonneLocationGeranceGroup.cadreFondsDonneLocationGerance;

if((not identite.optionME and (Value('id').of(identite.immatriculation).contains('immatRCS') 
	or (Value('id').of(identite.immatriculation).contains('immatRM') 
	and (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P')
	or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')
	or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P')
	or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('17P')
	or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('37P')
	or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('28P')
	or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('decesExploitant')
	or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('23P')
	or Value('id').of(objet.objetModification).contains('personnesLiees')
	or Value('id').of(objet.objetModification).contains('etablissement')
	or Value('id').of(objet.objetModification).contains('dateActivite')
	or (Value('id').of(objet.objetModification).contains('modifEIRL'))))))
	or (identite.optionME and Value('id').of(identite.immatriculation).contains('immatRCS') 
	and (Value('id').of(objet.objetModification).contains('modifEIRL') 
	or (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or objet.domicileEntreprise) and not objet.transfertDepartement)
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')
	and objet.transfertDepartement and Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementOuverture')
	and not Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation'))
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')
	and Value('id').of(etablissementNew.origineEtablissement2).eq('etablissementOuverture')
	and not Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation'))
	or ((Value('id').of(activite.activiteEvenement).eq('61P') or Value('id').of(activite.activiteEvenement).eq('61P62P'))
	and not Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation'))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P')
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	and (not objet.cadreObjetModificationEtablissement.ouvertureDepartement 
	or (objet.cadreObjetModificationEtablissement.ouvertureDepartement and not Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation'))))))) {
		
	//Mettre le Flag de paiement à "vrai"
	_log.info("*********Paiement existant sur la formalité!!******");
	formalityWithPayment =  true;
 
														  
	var listCoupleAutoriteFrais=[];
	for(var i=0; i<receiverInfo.listAuthorities.length; i++){
	
		// Dans ce cas nous avons un seul produit destioné aux greffes qui sont des TDR, on parcours la liste des authorités calculés puis on test chaque role retourné, quand on trouve un TDR on va lui affecter le code du frais de dépot qui nous interesse : 
			_log.info("receiverInfo.listAuthorities[i]['role'] for authority {} is {}", i, receiverInfo.listAuthorities[i]['role']);
			_log.info("secteur1 is {}", secteur1);
			_log.info("secteur2 is {}", secteur2);
			
		// Récupérer le réseau de l'autorité en cours de traitement
		
		var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', receiverInfo.listAuthorities[i]['authority']) //
				   .connectionTimeout(10000) //
				   .receiveTimeout(10000) //
				   .accept('json') //
				   .get();
		//result
		var currentAuthority = response.asObject();

		// prepare all information of receiver to create data.xml
		var details = !currentAuthority.details ? null : currentAuthority.details;
		var ediCode = !details.ediCode ? null : details.ediCode;
		_log.info("ediCode for authority {} is {}", i, ediCode);
		var authorityReseau = ediCode.charAt(0);
		_log.info("authorityReseau for authority {} is {}", i , authorityReseau);	
		
	// Frais pour le RCS et l'autorité est un GREFFE 	
		if(receiverInfo.listAuthorities[i]['role'] == "TDR" && (secteur1 == "COMMERCIAL" || secteur2 == "COMMERCIAL") && authorityReseau == 'G'){
			_log.info("Access cas : secteur 1 ou 2 commercial ===> Frais pour les greffes");
			
			//Récupérer les informations de chaque autorité calculée pour récupéré son libellé
			_log.info("Authority Code is {}", receiverInfo.listAuthorities[i]['authority']);
			var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', receiverInfo.listAuthorities[i]['authority']) //
						   .connectionTimeout(10000) //
						   .receiveTimeout(10000) //
						   .accept('json') //
						   .get();   
						   
			var authority = response.asObject();
			
			var funcLabel = !authority.label ? null :authority.label;

			_log.info("funcLabel is {}", funcLabel);
			
	// 1er tarif Non ME		
			if (not identite.optionME and Value('id').of(identite.immatriculation).contains('immatRCS')
				and ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
				and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
				or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise)
				and not objet.transfertDepartement 
				and (Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementOuverture')
				or Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementOuverture'))
				and not Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation')
				and Value('id').of(etablissement.destinationEtablissement1).eq('devientSecondaire')) {
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRCS012"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
			
	// 2ème tarif	Non ME	
			} else if (not identite.optionME and Value('id').of(identite.immatriculation).contains('immatRCS')
				and ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
				and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
				or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise)
				and not objet.transfertDepartement 
				and (Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementOuverture')
				or Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementOuverture'))
				and Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation')
				and Value('id').of(etablissement.destinationEtablissement1).eq('devientSecondaire')) {
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRCS013"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
	
	// 3ème tarif	Non ME	
			} else if (not identite.optionME and Value('id').of(identite.immatriculation).contains('immatRCS')
				and ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
				and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
				or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise)
				and not objet.transfertDepartement 
				and (Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementOuverture')
				or Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementOuverture'))
				and not Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation')
				and not Value('id').of(etablissement.destinationEtablissement1).eq('devientSecondaire'))	{
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRCS014"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
	
	// 4ème tarif Non ME
			}  else if (not identite.optionME and Value('id').of(identite.immatriculation).contains('immatRCS')
				and ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
				and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
				or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise or objet.domicileEntreprise)
				and not objet.transfertDepartement 
				and (((Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementOuverture')
				or Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementOuverture'))
				and Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation')
				and not Value('id').of(etablissement.destinationEtablissement1).eq('devientSecondaire'))
				or (not Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementOuverture')
				and not Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementOuverture')
				and not Value('id').of(etablissement.destinationEtablissement1).eq('devientSecondaire'))))		{
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRCS015"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
	
	// 5ème tarif	Non ME	
			} else if (not identite.optionME and Value('id').of(identite.immatriculation).contains('immatRCS')
				and ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
				and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
				or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise)
				and not objet.transfertDepartement 
				and (not Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementOuverture')
				and not Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementOuverture')
				and Value('id').of(etablissement.destinationEtablissement1).eq('devientSecondaire')))		{
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRCS016"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
	
	// 6ème tarif	Non ME
			} else if (not identite.optionME and Value('id').of(identite.immatriculation).contains('immatRCS')
				and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('miseEnLocation')
				and not Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetmiseEnLocation.infosMiseEnLocation).eq('82P'))		{
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRCS017"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
			
	// 7ème tarif	Non ME	
			} else if (not identite.optionME and Value('id').of(identite.immatriculation).contains('immatRCS')
				and (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P')
				or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')
				or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('decesExploitant')
				or Value('id').of(objet.objetModification).contains('dateActivite')
				or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')
				or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('cessationActivite')
				or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
				or Value('id').of(activite.activiteEvenement).eq('62P')
				or Value('id').of(activite.activiteEvenement).eq('61P62P')
				or (Value('id').of(activite.activiteEvenement).eq('61P')
				and Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation'))
				or ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')
				or activiteInfo.modifEtablissementEnseigne)
				and Value('id').of(activiteInfo.cadreEtablissementIdentification.modifNomEnseigne).contains('modifNomCommercial'))
				or (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
				and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
				or objet.domicileEntreprise)
				and objet.transfertDepartement
				and (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation')
				or objet.entrepriseDomicile
				or not Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementOuverture')))
				or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') 
				and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')))) {
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRCS018"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
			
	// 8ème tarif	Non ME	
			} else if (not identite.optionME and Value('id').of(identite.immatriculation).contains('immatRCS')
				and (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
				and not Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation')
				and objet.cadreObjetModificationEtablissement.ouvertureDepartement)) {
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRCS019"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
			
	// 9ème tarif	Non ME	
			} else if (not identite.optionME and Value('id').of(identite.immatriculation).contains('immatRCS')
				and (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
				and Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation')
				and objet.cadreObjetModificationEtablissement.ouvertureDepartement)) {
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRCS020"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
			
	// 10ème tarif	Non ME	
			} else if (not identite.optionME and Value('id').of(identite.immatriculation).contains('immatRCS')
				and (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
				and not Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation')
				and not objet.cadreObjetModificationEtablissement.ouvertureDepartement)) {
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRCS021"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
			
	// 11ème tarif		Non ME
			} else if (not identite.optionME and Value('id').of(identite.immatriculation).contains('immatRCS')
				and (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
				and Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation')
				and not objet.cadreObjetModificationEtablissement.ouvertureDepartement)) {
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRCS022"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
			
	// 12ème tarif		Non ME
			}else if (not identite.optionME and Value('id').of(identite.immatriculation).contains('immatRCS')
				and (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P')
				or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
				and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')
				and Value('id').of(etablissementNew.origineEtablissement2).eq('etablissementOuverture')
				and not Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation'))
				or ((Value('id').of(activite.activiteEvenement).eq('61P') or Value('id').of(activite.activiteEvenement).eq('61P62P'))
				and not Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation'))
				or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
				and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')
				and objet.transfertDepartement
				and Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementOuverture')
				and not Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation')))) {
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRCS023"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
	
	// 13ème tarif	Non ME	
			} else if (not identite.optionME and Value('id').of(identite.immatriculation).contains('immatRCS')
				and (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P')
				or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('17P')
				or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('23P')
				or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('28P')
				or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('30P')
				or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('37P')
				or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
				or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
				and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')
				and Value('id').of(etablissementNew.origineEtablissement2).eq('etablissementOuverture')
				and Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation'))
				or ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')
				or activiteInfo.modifEtablissementEnseigne)
				and Value('id').of(activiteInfo.cadreEtablissementIdentification.modifNomEnseigne).contains('modifEnseigne'))
				or Value('id').of(activite.activiteEvenement).eq('67P')
				or (Value('id').of(objet.objetModification).contains('modifEIRL')
				and not Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).eq('modifDAP'))
				or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P')
				and etablissement.radiationEtablissement)
				or (Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetmiseEnLocation.infosMiseEnLocation).eq('82P')
				and fondsDonne.radiationEtablissementBis)	
				or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') 
				and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))))  {
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRCS024"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
	
	// 14ème tarif	Non ME	
			} else if (not identite.optionME and Value('id').of(identite.immatriculation).contains('immatRCS')
				and ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P')
				and not etablissement.radiationEtablissement)
				or (Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetmiseEnLocation.infosMiseEnLocation).eq('82P')
				and not fondsDonne.radiationEtablissementBis)))	  {
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRCS026"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
			
	// 15ème tarif non ME	
			} else if (not Value('id').of($p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.immatriculation).contains('immatRCS') and not identite.optionME
	and (Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.jqpaDomaineTransport).eq('jqpaTransportAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA).eq('nafaAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineAlimentation).eq('alimentationAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineAutresServices).eq('servicesAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineProduction).eq('productionAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.jqpaDomaineTransport).eq('jqpaTransportAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA).eq('nafaAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineAlimentation).eq('alimentationAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineAutresServices).eq('servicesAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineProduction).eq('productionAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation'))	{
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRCS002"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
			
		// 16ème tarif non ME	
		}  else if (not Value('id').of($p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.immatriculation).contains('immatRCS') and not identite.optionME
	and (Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.jqpaDomaineTransport).eq('jqpaTransportAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA).eq('nafaAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineAlimentation).eq('alimentationAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineAutresServices).eq('servicesAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineProduction).eq('productionAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.jqpaDomaineTransport).eq('jqpaTransportAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA).eq('nafaAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineAlimentation).eq('alimentationAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineAutresServices).eq('servicesAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineProduction).eq('productionAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	and Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation'))	{
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRCS001"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
			
		// 17ème tarif ME	
		} else if (not identite.optionME and Value('id').of(objet.objetModification).contains('modifEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).eq('modifDAP'))	  {
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRCS025"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
			
	// 18ème tarif ME		
			}else if (identite.optionME and Value('id').of(identite.immatriculation).contains('immatRCS')
				and ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
				and (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')
				or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise))
				and not objet.transfertDepartement 
				and (Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementOuverture')
				or Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementOuverture'))
				and not Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation')))	  {
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRCS027"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
			
	// 19ème tarif ME		
			}  else if (identite.optionME and Value('id').of(identite.immatriculation).contains('immatRCS')
				and (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
				and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
				or objet.domicileEntreprise or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise)
				and not objet.transfertDepartement 
				and (((Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementOuverture')
				or Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementOuverture'))
				and Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation'))
				or (not Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementOuverture')
				and not Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementOuverture')))))	  {
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRCS028"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
			
	// 20ème tarif ME		
			}  else if (identite.optionME and Value('id').of(identite.immatriculation).contains('immatRCS')
				and Value('id').of(objet.objetModification).contains('modifEIRL')
				and not Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).eq('modifDAP'))	  {
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRCS024"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
	
	// 21ème tarif		
			}   else if (identite.optionME and Value('id').of(identite.immatriculation).contains('immatRCS')
				and Value('id').of(objet.objetModification).contains('modifEIRL')
				and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).eq('modifDAP'))	  {
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRCS025"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
	
	// 22ème tarif		
			}  else if (identite.optionME and Value('id').of(identite.immatriculation).contains('immatRCS')
				and (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
				and not Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation')
				and not objet.cadreObjetModificationEtablissement.ouvertureDepartement))	  {
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRCS029"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
			
	// 23ème tarif		
			} else if (identite.optionME and Value('id').of(identite.immatriculation).contains('immatRCS')
				and ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
				and Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation')
				and not objet.cadreObjetModificationEtablissement.ouvertureDepartement)
				or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P')
				or Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetmiseEnLocation.infosMiseEnLocation).eq('82P')))	  {
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRCS030"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
			
	// 24ème tarif		
			} else if (identite.optionME and Value('id').of(identite.immatriculation).contains('immatRCS')
				and (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
				and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
				or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise)
				and objet.transfertDepartement 
				and ((Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementOuverture')
				or Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementOuverture'))
				and not Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation'))
				or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P')
				or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
				and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')
				and Value('id').of(etablissementNew.origineEtablissement2).eq('etablissementOuverture')
				and not Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation'))
				or ((Value('id').of(activite.activiteEvenement).eq('61P') or Value('id').of(activite.activiteEvenement).eq('61P62P'))
				and not Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation'))))	  {
			var coupleAutoriteFrais = {
										"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
										"authorityLabel": funcLabel,
										"frais" : "IRCS031"
										};
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
			
			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);
			} 
	
	
	// Detection de la presence de frais pour la notification des greffes secondaires 
		
			if(identite.greffeSecondaire != null and (identite.greffeSecondaire[0].length > 0) and not identite.optionME 
				and (Value('id').of(objet.objetModification).contains('dateActivite')
				or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
				and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
				or objet.domicileEntreprise)) {
				//Rajouter le frais des notifications de maj et frais postaux
				var coupleAutoriteFrais = {
											"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
											"authorityLabel": funcLabel,
											"frais" : "MRCS001"
											};
			
			_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);			
			// On alimente notre liste autorité frais à payer
			listCoupleAutoriteFrais.push(coupleAutoriteFrais);

			}
		}	
			
		// Frais pour le RM et l'autorité est une CMA 
		if(receiverInfo.listAuthorities[i]['role'] == "TDR" && secteur1 == "ARTISANAL" && authorityReseau == 'M'){
			_log.info("Access cas : secteur 1 artisanal ===> Frais pour les CMA");
			_log.info("receiverInfo.listAuthorities[i]['role'] for authority {} is {}", i, receiverInfo.listAuthorities[i]['role']);
			_log.info("secteur1 is {}", secteur1);
		
	// 1er tarif 		
			if (not identite.optionME and Value('id').of(identite.immatriculation).contains('immatRM')
				and (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P')
				or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')
				or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P')
				or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('17P')
				or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('37P')
				or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('28P')
				or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('decesExploitant')
				or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('23P')
				or Value('id').of(objet.objetModification).contains('personnesLiees')
				or Value('id').of(objet.objetModification).contains('etablissement')
				or Value('id').of(objet.objetModification).contains('dateActivite'))) {
					var codeFrais = undefined;
					if(Date.now() < new Date('2020-01-01T00:00:00')){
						codeFrais = "IRM0002"
					}else if(secteur2 == "COMMERCIAL"){
						codeFrais = "IRM0008"
					}else{
						codeFrais = "IRM0007"
					}
					var coupleAutoriteFrais = {
												"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
												"authorityLabel": funcLabel,
												"frais" : codeFrais
												};
					_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);

					
					// On alimente notre liste autorité frais à payer
					listCoupleAutoriteFrais.push(coupleAutoriteFrais);
		
		// 2ème tarif	
			} else if (not identite.optionME and Value('id').of(identite.immatriculation).contains('immatRM')
				and not Value('id').of(objet.objetModification).contains('situationPerso')
				and not Value('id').of(objet.objetModification).contains('personnesLiees')
				and not Value('id').of(objet.objetModification).contains('etablissement')
				and not Value('id').of(objet.objetModification).contains('dateActivite')
				and Value('id').of(objet.objetModification).contains('modifEIRL')
				and objet.cadreObjetDeclarationEIRL.declarationEIRL) {
					var codeFrais = undefined;
					if(Date.now() < new Date('2020-01-01T00:00:00')){
						codeFrais = "IRM0003"
					}else if(secteur2 == "COMMERCIAL"){
						codeFrais = "IRM0010"
					}else{
						codeFrais = "IRM0003"
					}
					var coupleAutoriteFrais = {
												"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
												"authorityLabel": funcLabel,
												"frais" : codeFrais
												};
					_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
					
					// On alimente notre liste autorité frais à payer
					listCoupleAutoriteFrais.push(coupleAutoriteFrais);
		// 3ème tarif				
			} else if (not identite.optionME and Value('id').of(identite.immatriculation).contains('immatRM')
				and not Value('id').of(objet.objetModification).contains('situationPerso')
				and not Value('id').of(objet.objetModification).contains('personnesLiees')
				and not Value('id').of(objet.objetModification).contains('etablissement')
				and not Value('id').of(objet.objetModification).contains('dateActivite')
				and Value('id').of(objet.objetModification).contains('modifEIRL')
				and not objet.cadreObjetDeclarationEIRL.declarationEIRL) {
					var codeFrais = undefined;
					if(Date.now() < new Date('2020-01-01T00:00:00')){
						codeFrais = "IRM0004"
					}else if(secteur2 == "COMMERCIAL"){
						codeFrais = "IRM0011"
					}else{
						codeFrais = "IRM0004"
					}
					var coupleAutoriteFrais = {
												"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
												"authorityLabel": funcLabel,
												"frais" : codeFrais
												};
					_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
					
					// On alimente notre liste autorité frais à payer
					listCoupleAutoriteFrais.push(coupleAutoriteFrais);
		// 4ème tarif				
			} else if (not Value('id').of($p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.immatriculation).contains('immatRM') and not identite.optionME 
				and (($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA != null
				and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
				and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
				and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
				and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre'))
				or ($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA != null
				and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA).eq('nafaAutre')
				and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineAlimentation).eq('alimentationAutre')
				and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineAutresServices).eq('servicesAutre')
				and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineProduction).eq('productionAutre'))
				or ($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA != null
				and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA).eq('nafaAutre')
				and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineAlimentation).eq('alimentationAutre')
				and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineAutresServices).eq('servicesAutre')
				and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineProduction).eq('productionAutre'))
				and not $p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.optionCMACCI)) {
					var codeFrais = undefined;
					if(Date.now() < new Date('2020-01-01T00:00:00')){
						codeFrais = "IRM0001"
					}else{
						codeFrais = "IRM0006"
					}
					var coupleAutoriteFrais = {
												"authorityCode" : receiverInfo.listAuthorities[i]['authority'],
												"authorityLabel": funcLabel,
												"frais" : codeFrais
												};
					_log.info("coupleAutoriteFrais  is {}", coupleAutoriteFrais);
					
					// On alimente notre liste autorité frais à payer
					listCoupleAutoriteFrais.push(coupleAutoriteFrais);
			}
   
		}		
	}

	_log.info("listCoupleAutoriteFrais  is {}", listCoupleAutoriteFrais);
	//=================================> Partie qui sera mise par le rédacteur pour conditionner le couple frais autoprité Fin=========================================

	//=================================> Partie générique pour générer le specCreate à passer à paiement Début=========================================										
	for(var i=0; i<listCoupleAutoriteFrais.length; i++){
		//Récupérer les informations du frais
		_log.info(" entree dans la boucle du couple frais autorité {}", i);
		var filters = [];
		var prix = 0;
		filters.push("details.entityId:"+listCoupleAutoriteFrais[i]['frais']);
		var currentFraisAuthority = listCoupleAutoriteFrais[i];
		_log.info(" -- Couple authoriy frais recupere -- is {} for iteration {} ",  currentFraisAuthority, i);
		_log.info(" -- Code Authority -- is {} for iteration {} ",  currentFraisAuthority.authorityCode, i);
		_log.info(" -- filters -- is {}", filters);
		var response = nash.service.request('${directory.baseUrl}/private/v1/repository/{repositoryId}', "frais") //
				   .connectionTimeout(10000) //
				   .receiveTimeout(10000) //
				   .accept('json') //
				   .param("filters",filters)
				   .get();
				   
		var receiverInfo = response.asObject();
		var details = !receiverInfo.content[0].details ? null : receiverInfo.content[0].details;
		_log.info(" -- details -- is {}", details);
		
				if(details != null){
					var entityId = !details.entityId ? null : details.entityId;
					_log.info(" -- entityId -- is {}", entityId);
					
					//Si on a un tarif MRCS001 et donc autant de tarifs que de greffes secondaires
						//Si le frais en cours est le code des frais de notifications 
					if (entityId == "MRCS001"){
						//On parcours le nombre de greffes secondaires 
						for(var kamal=0; kamal<identite.greffeSecondaire.length; kamal++){
							//On calcule le prix total à payer pour ces notifications
							_log.info("Entrée dans la boucle du greffe secondaire {}", i);
							var prixGreffeSecondaire = !details.prix ? null : details.prix;	
							_log.info("prix à cumuler {} dans l'itaration {} is ", prixGreffeSecondaire, i);
							  prix += parseFloat(prixGreffeSecondaire);
							  _log.info("prix  cumulé dans l'itération {} is {} ", i, prixGreffeSecondaire);
						}
						_log.info(" -- prix total des frais de notification à payer pour les greffes secondaires -- is {}", prix);
					}else{ 
						 prix = !details.prix ? null : details.prix;
						_log.info(" -- prix unitaire-- is {}", prix);
					}
					
					var reseau = !details.reseau ? null : details.reseau;
					_log.info(" -- reseau -- is {}", reseau);
					var label = !details.label ? null : details.label;
					_log.info(" -- label -- is {}", label);
				
						//Construction de la page à afficher au user
					var  authorityCode = currentFraisAuthority.authorityCode;
					_log.info(" -- authorityCode -- is {}", authorityCode);
					var  authorityLabel = currentFraisAuthority.authorityLabel;
					_log.info(" -- authorityLabel -- is {}", authorityLabel);
					var fraisCode = currentFraisAuthority.frais;
					_log.info(" -- fraisCode -- is {}", fraisCode);
					_log.info(" -- Avant de creer la liste des frais ! {}", i);
					var fraisDestination = spec.createGroup({
											'id':'fraisDestinations['+i+']',
											'label':"Frais à payer",
											'maxOccurs':listCoupleAutoriteFrais.length,
											'data':[
													spec.createData({
														'id': 'authorityCode',
														'label': 'Authorité code :',
														'type': 'StringReadOnly',
														'value': authorityCode
														}),
													spec.createData({
														'id': 'authorityLabel',
														'label': 'Authorité label :',
														'type': 'StringReadOnly',
														'value':authorityLabel
														}),
													spec.createData({
														'id': 'fraisCode',
														'label': 'frais code :',
														'type': 'StringReadOnly',
														'value':fraisCode
														}),
													spec.createData({
														'id': 'fraisLabel',
														'label': 'frais label :',
														'type': 'StringReadOnly',
														'value':label
														}),
													spec.createData({
														'id': 'fraisPrix',
														'label': 'frais prix :',
														'type': 'StringReadOnly',
														'value':""+prix
														}),										
											]
										});
					listFraisDestinataires.push(fraisDestination);
					_log.info("listFraisDestinataires avec un frais pour l'iteration {}  is {}", i, listFraisDestinataires);
				}else{
					_log.error(" -- Error : code with the id {} not found in the repository with the id {}", listCodesFrais[i], "frais");
					return spec.create({
										id : 'codeFraisNotFound',
										label : 'Erreur lors de la recherche des codes de frais',
										groups : [ spec.createGroup({
											id : 'error',
											label : 'Erreur lors de la recherche des codes de frais',
											description : "Une erreur lors de la recherche des codes de frais dans le référentiel, veuillez contacter le support à l'adresse: support@guichet-entreprises.fr.",
											data : []
										}) ]
									});					
				}
	}
	
			var indiceFormalitePayante = spec.createGroup({
										'id':'flagFormalitePayante',
										'label':"la formalité est-elle payante?",
										'data':[
											spec.createData({
											'id': 'flagPayment',
											'label': 'flag du paiement :',
											'type': 'Boolean',
											'value':formalityWithPayment
											})
										]
									});
		listFraisDestinataires.push(indiceFormalitePayante);
		
		_log.info("listFraisDestinataires payant is {}", listFraisDestinataires);
	
}else{
	//Si la formalité n'est pas payante il ne faut afficher aucun frais et passer directement la step finish
	_log.info("***********Formalité n'est pas payante***************!!");
	var indiceFormalitePayante = spec.createGroup({
										'id':'flagFormalitePayante',
										'label':"la formalité est-elle payante?",
										'data':[
											spec.createData({
											'id': 'flagPayment',
											'label': 'flag du paiement :',
											'type': 'Boolean',
											'value':formalityWithPayment
											})
										]
									});
		listFraisDestinataires.push(indiceFormalitePayante);	
		
	var fraisDestination = spec.createGroup({
								'id':'listeFrais',
								'label':"Frais à payer",
								'data':[spec.createData({
											'id': 'aucunFrais',											
											'type': 'StringReadOnly',
											'value':"Aucun service payant n'est inclus dans votre procédure."
											})]
							});
		listFraisDestinataires.push(fraisDestination);
			_log.info("listFraisDestinataires non payant is {}", listFraisDestinataires);
}
//=================================> Partie générique pour générer le specCreate à passer à paiement Fin=========================================

//===========> Partie contenant les informations utilisateur à passer au paiement !A remplire par le redacteur de la formalité! =========================
var lastName = ($p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiqueNomUsage  != "" && $p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiqueNomUsage  != null)  ? $p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiqueNomUsage  :  $p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiqueNomNaissance;
var firstName = $p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiquePrenom1[0];				
var civility = " ";

var infoUser = spec.createGroup({
				id: 'infoUser',
				label: "Information de l'utilisateur",
				data: [spec.createData({
							id: 'userLastName',
							label: "Nom de l'utilisateur",
							description: "Nom de l'utilisateur",
							type: 'String',
							mandatory: true,
							value: lastName
									}),
						spec.createData({
							id: 'userFirstName',
							label: "Prénom de l'utilisateur",
							type: 'String',
							mandatory : true,
							value: firstName
									}),
						spec.createData({
							id: 'civility',
							label: "Civilité",
							type: 'String',
							mandatory:true,
							value: civility
									})	
					]
			});
//===========> Fin de la Partie contenant les informations utilisateur à passer au paiement !A remplire par le redacteur de la formalité! =========================

return spec.create({
    id : 'computeDestinataire',
    label : "Génération de la liste des frais pour le paiement",
	groups : [ spec.createGroup({
					id : 'information',
					label:"Liste des frais à payer",
					data : listFraisDestinataires
				}),
				regentXmlTcBloc,
				infoUser
	]
});