var formFields = {};
var cerfaFields = {};
function pad(s) { return (s < 10) ? '0' + s : s; }

// Cadre 1 - Objet de la formalité

var objet= $p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification;
var identite= $p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification;

formFields['modifSituationpersonnelle']                                  = (Value('id').of(objet.objetModification).contains('situationPerso') and not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).eq('23P')) ? true : false;
formFields['modifMiseEnGerance']                                         = Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('miseEnLocation') ? true : false;
formFields['modifRadiationRcs']                                          = Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetmiseEnLocation.infosMiseEnLocation).eq('83P') ? true : false;
formFields['modifMaintienImmatRcs']                                      = (Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetmiseEnLocation.infosMiseEnLocation).eq('82P') or Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetmiseEnLocation.infosMiseEnLocation).eq('84P')) ? true : false;
formFields['modifEtablissement']                                         = (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
																		   or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
																		   or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P')
																		   or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')
																		   or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P')
																		   or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P')
																		   or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P')) ? true : false;
formFields['modifTransfert']                                             = (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') or objet.domicileEntreprise) ? true : false;;
formFields['modifMiseEnSommeil']                                         = Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('cessationActivite') ? true : false;
formFields['modifReprise']                                               = Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P') ? true : false;
formFields['modifRenouvellement']                                        = Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).eq('23P') ? true : false;
formFields['modifAutre']                                                 = (Value('id').of(objet.objetModification).contains('modifEIRL') 
																		   or Value('id').of(objet.objetModification).contains('dateActivite')
																		   or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P')
																		   or Value('id').of(objet.objetModification).contains('personnesLiees')) ? true : false;

// Cadre 2 - Rappel d'identification

formFields['siren']                                                  = identite.siren.split(' ').join('');
formFields['immatRM']                                                = Value('id').of(identite.immatriculation).contains('immatRM') ? true : false;
formFields['immatRCS']                                               = Value('id').of(identite.immatriculation).contains('immatRCS') ? true : false;
formFields['immatRCSGreffe']                                         = Value('id').of(identite.immatriculation).contains('immatRCS') ? identite.immatRCSGreffe : '';
formFields['immatRMDept']                                            = Value('id').of(identite.immatriculation).contains('immatRM') ? identite.immatRMCMA : '';
var nirDeclarant = identite.voletSocialNumeroSecuriteSociale;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFields['numeroSS']                = nirDeclarant.substring(0, 13);
    formFields['numeroSSCle']             = nirDeclarant.substring(13, 15);
}

var greffeSecondaires=[];
for ( i = 0; i < identite.greffeSecondaire.size() ; i++ ){greffeSecondaires.push(identite.greffeSecondaire[i]);}                            
formFields['greffeSecondaire']                                      = greffeSecondaires.toString();
formFields['lieuDepotImpot']                                        = identite.lieuDepotImpot != null ? identite.lieuDepotImpot : '';
formFields['microSocialNon']                                        = identite.optionME ? false : true;
formFields['microSocialOui']                                        = identite.optionME ? true : false;


// Cadre 3A 

var modifIdentiteDeclarant = $p2cmb.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle;

if (modifIdentiteDeclarant.modifIdentite.modifDateIdentite !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifIdentite.modifDateIdentite.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateIdentite']          = date;
}

formFields['personneLiee_personnePhysique_nomNaissance']             = (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P') and Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifNomNaissance')) ? modifIdentiteDeclarant.modifIdentite.modifNouveauNomNaissance : identite.personneLieePersonnePhysiqueNomNaissance;
formFields['personneLiee_personnePhysique_nomUsage']                 = (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P') and Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPseudonyme).contains('modifNomUsage')) ? (modifIdentiteDeclarant.modifIdentite.modifNouveauNomUsage != null ? modifIdentiteDeclarant.modifIdentite.modifNouveauNomUsage : '') : (identite.personneLieePersonnePhysiqueNomUsage != null ? identite.personneLieePersonnePhysiqueNomUsage : '');

if (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom')) {	
var prenoms=[];
for ( i = 0; i < modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms.size() ; i++ ){prenoms.push(modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms[i]);}                            
formFields['personneLiee_personnePhysique_prenom1']                                      = prenoms.toString();
} else if (not Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom')) {
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFields['personneLiee_personnePhysique_prenom1']                                      = prenoms.toString();
}
formFields['personneLiee_personnePhysique_pseudonyme']                 = (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P') and Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPseudonyme).contains('modifPseudonyme')) ? (modifIdentiteDeclarant.modifIdentite.modifNouveauPseudonyme != null ? modifIdentiteDeclarant.modifIdentite.modifNouveauPseudonyme : '') : (identite.personneLieePersonnePhysiquePseudonyme != null ? identite.personneLieePersonnePhysiquePseudonyme : '');
if (identite.personneLieePersonnePhysiqueDateNaissance !== null) {
    var dateTmp = new Date(parseInt(identite.personneLieePersonnePhysiqueDateNaissance.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['personneLiee_personnePhysique_dateNaissance']          = date;
}
formFields['personneLiee_personnePhysique_lieuNaissanceDepartement'] = identite.personneLieePPLieuNaissanceDepartement != null ? identite.personneLieePPLieuNaissanceDepartement.getId() : '';
formFields['personneLiee_personnePhysique_lieuNaissanceCommune']     = identite.personneLieePPLieuNaissanceCommune != null ?  identite.personneLieePPLieuNaissanceCommune : identite.personneLieePPLieuNaissanceVille;
formFields['personneLiee_personnePhysique_lieuNaissancePays']        = identite.personneLieePPLieuNaissancePays;
formFields['personneLiee_personnePhysique_lieuNaissanceCommuneAncienne']  = identite.personneLieePPLieuNaissanceCommuneAncienne != null ?  identite.personneLieePPLieuNaissanceCommuneAncienne : '';

// Cadres 3B

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P') 
	or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) {
formFields['modifAncienNomNaissance']                                    = identite.personneLieePersonnePhysiqueNomNaissance;
formFields['modifAncienNomUsage']                                        = identite.personneLieePersonnePhysiqueNomUsage != null ? identite.personneLieePersonnePhysiqueNomUsage : '';
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFields['modifAncienPrenoms']                                      = prenoms.toString();
formFields['modifAncienPseudonyme']                                      = identite.personneLieePersonnePhysiquePseudonyme != null ? identite.personneLieePersonnePhysiquePseudonyme : '';
}

// Cadre 4 - Déclaration relative à la modification de la situation personnelle

// Modification du domicile

var etablissement = $p2cmb.cadre6EtablissementGroup.infoEtablissementOld;
var newAdresseDom = $p2cmb.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifDomicile.newAdresseDomicile;
var etablissementNew = $p2cmb.cadre6EtablissementGroup.infoEtablissementNew;

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P')) {
if (modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateDomicile']          = date;
}
formFields['personneLiee_adresse_voie']                             = (newAdresseDom.personneLieeAdresseVoieNew != null ? newAdresseDom.personneLieeAdresseVoieNew : '') 
																	+ ' ' + (newAdresseDom.personneLieeAdresseIndiceVoieNew != null ? newAdresseDom.personneLieeAdresseIndiceVoieNew : '')
																	+ ' ' + (newAdresseDom.personneLieeAdresseTypeVoieNew != null ? newAdresseDom.personneLieeAdresseTypeVoieNew : '')
																	+ ' ' + (newAdresseDom.personneLieeAdresseNomVoieNew != null ? newAdresseDom.personneLieeAdresseNomVoieNew : '');
formFields['personneLiee_adresse_complementAdresse']                = (newAdresseDom.personneLieeAdresseComplementAdresseNew != null ? newAdresseDom.personneLieeAdresseComplementAdresseNew : '')
																	+ ' ' + (newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null ? newAdresseDom.personneLieeAdresseDistriutionSpecialeNew : '');
formFields['personneLiee_adresse_codePostal']                       = newAdresseDom.personneLieeAdresseCodePostalNew != null ? newAdresseDom.personneLieeAdresseCodePostalNew : '';
formFields['personneLiee_adresse_commune']                          = newAdresseDom.personneLieeAdresseCommuneNew != null ? newAdresseDom.personneLieeAdresseCommuneNew : (newAdresseDom.personneLieeAdresseVilleNew + ' / ' + newAdresseDom.personneLieeAdressePaysNew);
formFields['deptAncienDomicile']                                    = modifIdentiteDeclarant.modifDomicile.domicileMemeDept != null ? modifIdentiteDeclarant.modifDomicile.domicileMemeDept : '';
} else if (objet.entrepriseDomicile) {
var newAdresseDom2 = $p2cmb.cadre6EtablissementGroup.infoEtablissementNew.adresseEtablissementNew;
if (etablissement.modifDateAdresseEntreprise !== null) {
    var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateDomicile']          = date;
}

formFields['personneLiee_adresse_voie']                             = (newAdresseDom2.etablissementAdresseNumeroVoieNew != null ? newAdresseDom2.etablissementAdresseNumeroVoieNew : '') 
																	+ ' ' + (newAdresseDom2.etablissementAdresseIndiceVoieNew != null ? newAdresseDom2.etablissementAdresseIndiceVoieNew : '')
																	+ ' ' + (newAdresseDom2.etablissementAdresseTypeVoieNew != null ? newAdresseDom2.etablissementAdresseTypeVoieNew : '')
																	+ ' ' + (newAdresseDom2.etablissementAdresseNomVoieNew != null ? newAdresseDom2.etablissementAdresseNomVoieNew : '');
formFields['personneLiee_adresse_complementAdresse'] 				= (newAdresseDom2.etablissementAdresseComplementVoieNew != null ? newAdresseDom2.etablissementAdresseComplementVoieNew : '')
																	+ ' ' + (newAdresseDom2.etablissementAdresseDistriutionSpecialeVoieNew != null ? newAdresseDom2.etablissementAdresseDistriutionSpecialeVoieNew : '');
formFields['personneLiee_adresse_codePostal']                       = newAdresseDom2.etablissementAdresseCodePostalNew;
formFields['personneLiee_adresse_commune']                          = newAdresseDom2.etablissementAdresseCommuneNew;
}

// Nouvelle nationalité

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('17P')) {
if (modifIdentiteDeclarant.modifNationalite.modifDateNationalite !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifNationalite.modifDateNationalite.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateNationalite']          = date;
}
formFields['modifNouvelleNationalite']                                   = modifIdentiteDeclarant.modifNationalite.modifNewNationalite;
}

// Décès de l'exploitant

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('decesExploitant')) {
if (objet.cadreObjet22P.modifDateDeces !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjet22P.modifDateDeces.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateDeces']          = date;
}
formFields['modifDecesMaintienImmatRCS']                                  = Value('id').of(identite.immatriculation).contains('immatRCS') ? true : false;
formFields['modifDecesMaintienImmatRM']                                   = Value('id').of(identite.immatriculation).contains('immatRM') ? true : false;
formFields['modifDecesPoursuiteHeritiersOui']                             = Value('id').of(objet.cadreObjet22P.infoDeces).eq('22P') ? true : false;
formFields['modifDecesPoursuiteHeritiersNon']                             = Value('id').of(objet.cadreObjet22P.infoDeces).eq('42P') ? true : false;
}

// Cessation totale d'activité

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('cessationActivite')) {
if (objet.cadreObjetModificationEtablissement.cadreObjetcessationActivite.dateCessationActivite !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjetcessationActivite.dateCessationActivite.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateMiseEnSommeil']          = date;
}
formFields['modifMiseEnSommeilMaintienRCS']                                  = Value('id').of(identite.immatriculation).contains('immatRCS') ? true : false;
formFields['modifMiseEnSommeilMaintienRM']                                   = Value('id').of(identite.immatriculation).contains('immatRM') ? true : false;
}

// Reprise

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')) {
if (objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateRepriseRenouvellement']          = date;
}	
formFields['modifRepriseActivite']                                                   = true;
}

// Demande de renouvellement

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('23P')) {
if (objet.cadreObjetRenouvellementMaintienRCS.modifDateRenouvellementMaintienRCS1 !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetRenouvellementMaintienRCS.modifDateRenouvellementMaintienRCS1.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateRepriseRenouvellement']          = date;
} else if (objet.cadreObjetRenouvellementMaintienRCS.modifDateRenouvellementMaintienRCS2 !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetRenouvellementMaintienRCS.modifDateRenouvellementMaintienRCS2.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateRepriseRenouvellement']          = date;
}	
formFields['modifRenouvellementMaintien']                                            = true;
formFields['modifRenouvellementMaintienImmatRCS']                                     = Value('id').of(identite.immatriculation).contains('immatRCS') ? true : false;
formFields['modifRenouvellementMaintienImmatRM']                                    = Value('id').of(identite.immatriculation).contains('immatRM') ? true : false;
}

// Cadres 5 - Modification EIRL

if ($p2cmb.cadre4DeclarationAffectationPatrimoineGroup.cadreDeclarationAffectationPatrimoine.modifDateDeclarationEIRL !== null) {
    var dateTmp = new Date(parseInt($p2cmb.cadre4DeclarationAffectationPatrimoineGroup.cadreDeclarationAffectationPatrimoine.modifDateDeclarationEIRL.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date1;
} else if ($p2cmb.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.modifDateEIRL !== null) {
    var dateTmp = new Date(parseInt($p2cmb.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.modifDateEIRL.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date2;
}  else if (objet.cadreObjet22P.poursuiteEIRL and objet.cadreObjet22P.modifDateDeces !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjet22P.modifDateDeces.getTimeInMillis()));
    var date3 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date3 = date3.concat(pad(month.toString()));
	date3 = date3.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date3;
}  else if ($p2cmb.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.finEIRLGroup.finEIRLDeclaration.modifDateFinEIRL !== null) {
    var dateTmp = new Date(parseInt($p2cmb.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.finEIRLGroup.finEIRLDeclaration.modifDateFinEIRL.getTimeInMillis()));
    var date4 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date4 = date4.concat(pad(month.toString()));
	date4 = date4.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date4;
}	else if (objet.entrepriseEIRL and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') and etablissement.modifDateAdresseEntreprise !== null) {
    var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
    var date5 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date5 = date5.concat(pad(month.toString()));
	date5 = date5.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date5;
} else if (objet.entrepriseEIRL and modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
    var date6 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date6 = date6.concat(pad(month.toString()));
	date6 = date6.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date6;
} 
formFields['eirl_immatriculation']                                                   = Value('id').of(objet.objetModification).contains('modifEIRL') ? (objet.cadreObjetDeclarationEIRL.declarationEIRL ? true : false) : false;
formFields['eirl_modification']                                                      = ((Value('id').of(objet.objetModification).contains('modifEIRL') and (Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDenoEIRL') or Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDateCloture') or Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifObjet') or Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse'))) or objet.entrepriseEIRL or objet.poursuiteEIRL) ? (objet.cadreObjetDeclarationEIRL.declarationEIRL ? false : true) : false;
formFields['eirl_affectationRetraitPatrimoine']                                      = (Value('id').of(objet.objetModification).contains('modifEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDAP')) ? true : false;

// Cadre 6 - Insaisissabilité

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('28P')) {
if (modifIdentiteDeclarant.modifInsaisissabilite.modifdateInsaisissabilite !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifInsaisissabilite.modifdateInsaisissabilite.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateInsaisissabilite']          = date;
}
formFields['insaisissabiliteRenonciationRP']                                         = Value('id').of(modifIdentiteDeclarant.modifInsaisissabilite.modifResidencePrincipale).eq('renonciation') ? true : false;
formFields['insaisissabilitePublicationRenonciationRP']                              = Value('id').of(modifIdentiteDeclarant.modifInsaisissabilite.modifResidencePrincipale).eq('renonciation') ? modifIdentiteDeclarant.modifInsaisissabilite.publicationInsaisssabiliteResidencePrincipale : '';
formFields['insaisissabiliteRevocationRP']                                           = Value('id').of(modifIdentiteDeclarant.modifInsaisissabilite.modifResidencePrincipale).eq('revocation') ? true : false;
formFields['insaisissabilitePublicationRevocationRP']                                = Value('id').of(modifIdentiteDeclarant.modifInsaisissabilite.modifResidencePrincipale).eq('revocation') ? modifIdentiteDeclarant.modifInsaisissabilite.publicationInsaisssabiliteResidencePrincipale : '';
formFields['insaisissabiliteDeclarationAutresBiens']                                 = Value('id').of(modifIdentiteDeclarant.modifInsaisissabilite.modifAutreBien).contains('declaration') ? true : false;
if (Value('id').of(modifIdentiteDeclarant.modifInsaisissabilite.modifAutreBien).contains('declaration')) {
var declarationAutresBiens=[];
for ( i = 0; i < modifIdentiteDeclarant.modifInsaisissabilite.publicationDeclarationInsaisssabiliteAutreBien.size() ; i++ ){declarationAutresBiens.push(modifIdentiteDeclarant.modifInsaisissabilite.publicationDeclarationInsaisssabiliteAutreBien[i]);}                            
formFields['insaisissabilitePublicationDeclarationAutresBiens']                      = declarationAutresBiens.toString();
}
formFields['insaisissabiliteRenonciationDeclarationAutresBiens']                     = Value('id').of(modifIdentiteDeclarant.modifInsaisissabilite.modifAutreBien).contains('renonciationAutre') ? true : false;
if (Value('id').of(modifIdentiteDeclarant.modifInsaisissabilite.modifAutreBien).contains('renonciationAutre')) {
var renonciationAutresBiens=[];
for ( i = 0; i < modifIdentiteDeclarant.modifInsaisissabilite.publicationRenonciationInsaisssabiliteAutreBien.size() ; i++ ){renonciationAutresBiens.push(modifIdentiteDeclarant.modifInsaisissabilite.publicationRenonciationInsaisssabiliteAutreBien[i]);}                            
formFields['insaisissabilitePublicationRenonciationDeclarationAutresBiens']          = renonciationAutresBiens.toString();
}
}

// Cadre 7 - Contrat d'appui

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('37P')) {
if (modifIdentiteDeclarant.modifContratAppui.modifDateAppui !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifContratAppui.modifDateAppui.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateContratAppui']          = date;
}
formFields['modifContratAppui']                                   = true;
}

// Cadre 8 - Conjoint collaborateur

var conjoint = $p2cmb.cadre3ModificationConjointGroup.cadre3ModificationConjoint;
var adresseConjoint = $p2cmb.cadre3ModificationConjointGroup.cadre3ModificationConjoint.adresseDomicileConjoint;

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('30P')) {
if (conjoint.modifDateConjoint !== null) {
    var dateTmp = new Date(parseInt(conjoint.modifDateConjoint.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateConjoint']          = date;
}
formFields['suppression_mentionConjoint']                                            = Value('id').of(conjoint.objetModifStatut).eq('suppressionStatut') ? true : false;
formFields['modification_mentionConjoint']                                           = Value('id').of(conjoint.objetModifStatut).eq('modificationStatut') ? true : false;
formFields['nouvelle_mentionConjoint']                                               = Value('id').of(conjoint.objetModifStatut).eq('nouveauStatut') ? true : false;

// Cadre 8B

if (not Value('id').of(conjoint.objetModifStatut).eq('suppressionStatut')) {
formFields['conjointSalarie']                                                        = Value('id').of(conjoint.objetModifConjoint).eq('conjointSalarie') ? true : false;
formFields['conjointCollaborateur']                                                  = Value('id').of(conjoint.objetModifConjoint).eq('conjointCollaborateur') ? true : false;
}

// Cadre 8C

if (not Value('id').of(conjoint.objetModifStatut).eq('suppressionStatut')) {
formFields['modifNomNaissanceConjoint']                                              = conjoint.modifNomNaissanceConjoint;
formFields['modifNomUsageConjoint']                                                  = conjoint.modifNomUsageConjoint != null ? conjoint.modifNomUsageConjoint : '';
var prenoms=[];
for ( i = 0; i < conjoint.modifPrenomConjoint.size() ; i++ ){prenoms.push(conjoint.modifPrenomConjoint[i]);}                            
formFields['modifPrenomConjoint']                                      = prenoms.toString();
if ($p2cmb.cadre3ModificationConjointGroup.cadre3ModificationConjoint.modifDateNaissanceConjoint !== null) {
    var dateTmp = new Date(parseInt($p2cmb.cadre3ModificationConjointGroup.cadre3ModificationConjoint.modifDateNaissanceConjoint.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateNaissanceConjoint']          = date;
}
formFields['modifLieuNaissanceDepartementConjoint']                                  = conjoint.modifLieuNaissanceDepartementConjoint != null ? conjoint.modifLieuNaissanceDepartementConjoint.getId() : '';
formFields['modifLieuNaissanceCommuneConjoint']                                      = conjoint.modifLieuNaissanceCommuneConjoint != null ? conjoint.modifLieuNaissanceCommuneConjoint : conjoint.modifLieuNaissanceVilleConjoint
formFields['modifLieuNaissancePaysConjoint']                                         = conjoint.modifLieuNaissancePaysConjoint;
formFields['modifNationaliteConjoint']                                               = conjoint.modifNationaliteConjoint;
var nirConjoint = conjoint.modifNumeroSSConjoint;
if(nirConjoint != null) {
    nirConjoint = nirConjoint.replace(/ /g, "");
    formFields['modifNumeroSSConjoint']    = nirConjoint.substring(0, 13);
    formFields['modifNumeroSSConjoint_cle'] = nirConjoint.substring(13, 15);
}
if (conjoint.adresseConjointDifferente) {
formFields['modifDomicileConjoint_voie']                    = (adresseConjoint.adresseConjointNumeroVoie != null ? adresseConjoint.adresseConjointNumeroVoie : '') 
															+ ' ' + (adresseConjoint.adresseConjointIndiceVoie != null ? adresseConjoint.adresseConjointIndiceVoie : '')
															+ ' ' + (adresseConjoint.adresseConjointTypeVoie != null ? adresseConjoint.adresseConjointTypeVoie : '')
															+ ' ' + (adresseConjoint.adresseConjointNomVoie != null ? adresseConjoint.adresseConjointNomVoie : '')
															+ ' ' + (adresseConjoint.adresseConjointComplementVoie != null ? adresseConjoint.adresseConjointComplementVoie : '')
															+ ' ' + (adresseConjoint.adresseConjointDistriutionSpecialeVoie != null ? adresseConjoint.adresseConjointDistriutionSpecialeVoie : '');
formFields['modifDomicileConjoint_codePostal']              = adresseConjoint.adresseConjointCodePostal;
formFields['modifDomicileConjoint_commune']                 = adresseConjoint.adresseConjointCommune;
formFields['modifAdresseAncienneCommuneConjoint']           = adresseConjoint.adresseConjointCommuneAncienne;
}
}
}

// Cadre 9 - Personne Liée : fondé de pourvoir, exploitant pour le compte de l'indivision, propriétaire indivis

// Exploitants pour le compte de l'indivision
var exploitant1 = $p2cmb.cadreExploitantIndivisionGroup.cadreExploitantIndivision;
var exploitant2 = $p2cmb.cadreExploitantIndivisionGroup.cadreExploitantIndivision2;
var exploitant3 = $p2cmb.cadreExploitantIndivisionGroup.cadreExploitantIndivision3;

if (Value('id').of(objet.cadreObjet22P.infoDeces).eq('22P')	or Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant')) {
if (Value('id').of(objet.cadreObjet22P.infoDeces).eq('22P') and objet.cadreObjet22P.modifDateDeces !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjet22P.modifDateDeces.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDatePersonneLiee']          = date;
} else if (Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant') and exploitant1.modifDateExploitantIndivision !== null) {
    var dateTmp = new Date(parseInt(exploitant1.modifDateExploitantIndivision.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDatePersonneLiee']          = date;
}
formFields['modifPersonneLieeType_exploitant']                                       = true;
if (Value('id').of(objet.cadreObjet22P.infoDeces).eq('22P')	or (Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant') and not Value('id').of(exploitant1.objetModifExploitant).eq('modifExploitant'))) {
formFields['modifPersonneLieeCategorie_nouveau']                                     = true;
}
if (Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant') and not Value('id').of(exploitant1.objetModifExploitant).eq('modifExploitant')) {
formFields['modifPersonneLieeCategorie_partant']                                     = true;
}
if (Value('id').of(exploitant1.objetModifExploitant).eq('modifExploitant')) {
formFields['modifPersonneLieeCategorie_modifie']                                     = true;
}
if (Value('id').of(objet.cadreObjet22P.infoDeces).eq('22P')	or (Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant') and not Value('id').of(exploitant1.objetModifExploitant).eq('partantExploitant'))) {
formFields['modifPersonneLIeeNomNaissance']                                          = exploitant1.nomNaissanceExploitant;
formFields['modifPersonneLIeeNomUsage']                                              = exploitant1.nomUsageExploitant != null ? exploitant1.nomUsageExploitant : '';
var prenoms=[];
for ( i = 0; i < exploitant1.prenomExploitant.size() ; i++ ){prenoms.push(exploitant1.prenomExploitant[i]);}                            
formFields['modifPersonneLIeePrenoms']                                      = prenoms.toString();
formFields['modifPersonneLIeeDomicile_voie']                                         = (exploitant1.adresseDomicileExploitant.exploitantAdresseNumeroVoie != null ? exploitant1.adresseDomicileExploitant.exploitantAdresseNumeroVoie : '')
																					 + ' ' + (exploitant1.adresseDomicileExploitant.exploitantAdresseIndiceVoie != null ? exploitant1.adresseDomicileExploitant.exploitantAdresseIndiceVoie : '')
																					 + ' ' + (exploitant1.adresseDomicileExploitant.exploitantAdresseTypeVoie != null ? exploitant1.adresseDomicileExploitant.exploitantAdresseTypeVoie : '')
																					 + ' ' + exploitant1.adresseDomicileExploitant.exploitantAdresseNomVoie
																					 + ' ' + (exploitant1.adresseDomicileExploitant.exploitantAdresseComplementAdresse != null ? exploitant1.adresseDomicileExploitant.exploitantAdresseComplementAdresse : '')
																					 + ' ' + (exploitant1.adresseDomicileExploitant.exploitantAdresseDistriutionSpeciale != null ? exploitant1.adresseDomicileExploitant.exploitantAdresseDistriutionSpeciale : '');
formFields['modifPersonneLIeeDomicile_codePostal']                                   = exploitant1.adresseDomicileExploitant.exploitantAdresseCodePostal;
formFields['modifPersonneLIeeDomicile_commune']                                      = exploitant1.adresseDomicileExploitant.exploitantAdresseCommune;
if (exploitant1.dateNaissanceExploitant !== null) {
    var dateTmp = new Date(parseInt(exploitant1.dateNaissanceExploitant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifPersonneLIeeDateNaissance']          = date;
}
formFields['modifPersonneLIeeLieuNaissanceDepartement']                              = exploitant1.lieuNaissanceDepartementExploitant != null ? exploitant1.lieuNaissanceDepartementExploitant.getId() : '';
formFields['modifPersonneLIeeLieuNaissanceCommune']                                  = exploitant1.lieuNaissanceCommuneExploitant != null ? exploitant1.lieuNaissanceCommuneExploitant : exploitant1.lieuNaissanceVilleExploitant;
formFields['modifPersonneLIeeLieuNaissancePays']                                     = exploitant1.lieuNaissancePaysExploitant;
formFields['modifPersonneLIeeNationalite']                                           = exploitant1.nationaliteExploitant;
formFields['modifPersonneLIeeMineurEmancipe']                                        = exploitant1.exploitantMineurEmancipe ? true : false;
} else if (Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant') and Value('id').of(exploitant1.objetModifExploitant).eq('partantExploitant')) {
formFields['modifPersonneLIeeNomNaissance']                                          = exploitant2.nomNaissanceExploitant;
formFields['modifPersonneLIeeNomUsage']                                              = exploitant2.nomUsageExploitant != null ? exploitant2.nomUsageExploitant : '';
var prenoms=[];
for ( i = 0; i < exploitant2.prenomExploitant.size() ; i++ ){prenoms.push(exploitant2.prenomExploitant[i]);}                            
formFields['modifPersonneLIeePrenoms']                                      = prenoms.toString();
formFields['modifPersonneLIeeDomicile_voie']                                         = (exploitant2.adresseDomicileExploitant2.exploitantAdresseNumeroVoie != null ? exploitant2.adresseDomicileExploitant2.exploitantAdresseNumeroVoie : '')
																					 + ' ' + (exploitant2.adresseDomicileExploitant2.exploitantAdresseIndiceVoie != null ? exploitant2.adresseDomicileExploitant2.exploitantAdresseIndiceVoie : '')
																					 + ' ' + (exploitant2.adresseDomicileExploitant2.exploitantAdresseTypeVoie != null ? exploitant2.adresseDomicileExploitant2.exploitantAdresseTypeVoie : '')
																					 + ' ' + exploitant2.adresseDomicileExploitant2.exploitantAdresseNomVoie
																					 + ' ' + (exploitant2.adresseDomicileExploitant2.exploitantAdresseComplementAdresse != null ? exploitant2.adresseDomicileExploitant2.exploitantAdresseComplementAdresse : '')
																					 + ' ' + (exploitant2.adresseDomicileExploitant2.exploitantAdresseDistriutionSpeciale != null ? exploitant2.adresseDomicileExploitant2.exploitantAdresseDistriutionSpeciale : '');
formFields['modifPersonneLIeeDomicile_codePostal']                                   = exploitant2.adresseDomicileExploitant2.exploitantAdresseCodePostal;
formFields['modifPersonneLIeeDomicile_commune']                                      = exploitant2.adresseDomicileExploitant2.exploitantAdresseCommune;
if (exploitant2.dateNaissanceExploitant !== null) {
    var dateTmp = new Date(parseInt(exploitant2.dateNaissanceExploitant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifPersonneLIeeDateNaissance']          = date;
}
formFields['modifPersonneLIeeLieuNaissanceDepartement']                              = exploitant2.lieuNaissanceDepartementExploitant != null ? exploitant2.lieuNaissanceDepartementExploitant.getId() : '';
formFields['modifPersonneLIeeLieuNaissanceCommune']                                  = exploitant2.lieuNaissanceCommuneExploitant != null ? exploitant2.lieuNaissanceCommuneExploitant : exploitant2.lieuNaissanceVilleExploitant;
formFields['modifPersonneLIeeLieuNaissancePays']                                     = exploitant2.lieuNaissancePaysExploitant;
formFields['modifPersonneLIeeNationalite']                                           = exploitant2.nationaliteExploitant;
formFields['modifPersonneLIeeMineurEmancipe']                                        = exploitant2.exploitantMineurEmancipe ? true : false;
}
if (Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant') and Value('id').of(exploitant1.objetModifExploitant).eq('nouveauExploitant')) {
formFields['modifPersonneLieeNomNaissance_partant1']                                 = exploitant3.nomNaissanceExploitant;
var prenoms=[];
for ( i = 0; i < exploitant3.prenomExploitant.size() ; i++ ){prenoms.push(exploitant3.prenomExploitant[i]);}                            
formFields['modifPersonneLieePrenoms_partant1']                                      = prenoms.toString();
} else if (Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant') and Value('id').of(exploitant1.objetModifExploitant).eq('partantExploitant')) {
formFields['modifPersonneLieeNomNaissance_partant1']                                 = exploitant1.nomNaissanceExploitant;
var prenoms=[];
for ( i = 0; i < exploitant1.prenomExploitant.size() ; i++ ){prenoms.push(exploitant1.prenomExploitant[i]);}                            
formFields['modifPersonneLieePrenoms_partant1']                                      = prenoms.toString();
}
}

// Fondés de pouvoir ou propriétaires indivis et héritiers (si pas de modification de l'exploitant ou non suite au décès)
var etablissementNew = $p2cmb.cadre6EtablissementGroup.infoEtablissementNew;
var personneLiee1 = $p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.infoPeronnePouvoir;
var personneLiee2 = $p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.infoPeronnePouvoir2;
var personneLiee3 = $p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.infoPeronnePouvoir3;
var personneLiee4 = $p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.infoPeronnePouvoir4;
var personneLiee5 = $p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.infoPeronnePouvoir5;
var personneLiee6 = $p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.infoPeronnePouvoir6;
var personneLiee7 = $p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.infoPeronnePouvoir7;
var personneLiee8 = $p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.infoPeronnePouvoir8;
var personneLiee9 = $p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.infoPeronnePouvoir9;
var formFieldsPers1 = {};
var formFieldsPers2 = {};

if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers'))
	and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant')) {
if ((Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers') or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P') or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')) and $p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.modifDatePersonneLiee !== null) {
    var dateTmp = new Date(parseInt($p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.modifDatePersonneLiee.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDatePersonneLiee']          = date;
} else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') and etablissementNew.modifDateAdresseOuverture != null) { 	
	var dateTmp = new Date(parseInt(etablissementNew.modifDateAdresseOuverture.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDatePersonneLiee']          = date;
} else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') and etablissementNew.modifDateAdresseReprise != null) { 	
	var dateTmp = new Date(parseInt(etablissementNew.modifDateAdresseReprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDatePersonneLiee']          = date;
} else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissement.modifDateAdresseEntreprise != null) { 	
	var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDatePersonneLiee']          = date;
} else if (etablissementNew.ajoutFondePouvoir and objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise and objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise != null) { 	
	var dateTmp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDatePersonneLiee']          = date;
} else if (modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir and modifIdentiteDeclarant.modifDomicile.modifDateDomicile != null) { 	
	var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDatePersonneLiee']          = date;
}

// Personne Liée 1
if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee1.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement')) {
formFields['modifPersonneLieeType_personnePouvoir']                              = true;
}
if ((not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and (Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers')))
	or Value('id').of(personneLiee1.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualiteProprietaireIndivis')) {
formFields['modifPersonneLieeType_proprietaireIndivis']                              = true;
}
if (etablissementNew.ajoutFondePouvoir or objet.domicileEntreprise or Value('id').of(personneLiee1.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
formFields['modifPersonneLieeCategorie_nouveau']                                     = true;
}
if (Value('id').of(personneLiee1.objetModifPersonneLiee).eq('modifPersonneLiee')) {
formFields['modifPersonneLieeCategorie_modifie']                                     = true;
}
if (Value('id').of(personneLiee1.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFields['modifPersonneLieeCategorie_partant']                                     = true;
}

if (etablissementNew.ajoutFondePouvoir or objet.domicileEntreprise or not Value('id').of(personneLiee1.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFields['modifPersonneLIeeNomNaissance']                                          = personneLiee1.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
formFields['modifPersonneLIeeNomUsage']                                              = personneLiee1.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? personneLiee1.personneLieePersonnePhysiqueNomUsagePersonnePouvoir : '';
var prenoms=[];
for ( i = 0; i < personneLiee1.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee1.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFields['modifPersonneLIeePrenoms']                                      = prenoms.toString();
formFields['modifPersonneLIeeDomicile_voie']                                         = (personneLiee1.cadreAdressePersonnePouvoir.rueNumeroAdressePersonnePouvoir != null ? personneLiee1.cadreAdressePersonnePouvoir.rueNumeroAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee1.cadreAdressePersonnePouvoir.indiceVoiePersonnePouvoir != null ? personneLiee1.cadreAdressePersonnePouvoir.indiceVoiePersonnePouvoir : '')
																					 + ' ' + (personneLiee1.cadreAdressePersonnePouvoir.typeVoiePersonnePouvoir != null ? personneLiee1.cadreAdressePersonnePouvoir.typeVoiePersonnePouvoir : '')
																					 + ' ' + personneLiee1.cadreAdressePersonnePouvoir.nomVoiePersonnePouvoir
																					 + ' ' + (personneLiee1.cadreAdressePersonnePouvoir.rueComplementAdressePersonnePouvoir != null ? personneLiee1.cadreAdressePersonnePouvoir.rueComplementAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee1.cadreAdressePersonnePouvoir.distriutionSpecialePersonnePouvoir != null ? personneLiee1.cadreAdressePersonnePouvoir.distriutionSpecialePersonnePouvoir : '');
formFields['modifPersonneLIeeDomicile_codePostal']                                   = personneLiee1.cadreAdressePersonnePouvoir.personneLieeAdresseCodePostalPersonnePouvoir;
formFields['modifPersonneLIeeDomicile_commune']                                      = personneLiee1.cadreAdressePersonnePouvoir.personneLieeAdresseCommunepersonnePouvoir;
if (personneLiee1.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir !== null) {
    var dateTmp = new Date(parseInt(personneLiee1.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifPersonneLIeeDateNaissance']          = date;
}
formFields['modifPersonneLIeeLieuNaissanceDepartement']                              = personneLiee1.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir != null ? personneLiee1.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir.getId() : '';
formFields['modifPersonneLIeeLieuNaissanceCommune']                                  = personneLiee1.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir != null ? personneLiee1.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir : (personneLiee1.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir != null ? personneLiee1.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir : '');
formFields['modifPersonneLIeeLieuNaissancePays']                                     = personneLiee1.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir;
formFields['modifPersonneLIeeNationalite']                                           = personneLiee1.personneLieePersonnePhysiqueNationalitePersonnePouvoir != null ? personneLiee1.personneLieePersonnePhysiqueNationalitePersonnePouvoir : '';
formFields['modifPersonneLIeeMineurEmancipe']                                        = personneLiee1.personneLieeMineurEmancipeMineurEmancipe ? true : false;
}
if (Value('id').of(personneLiee1.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFields['modifPersonneLieeNomNaissance_partant1']                                          = personneLiee1.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
var prenoms=[];
for ( i = 0; i < personneLiee1.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee1.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFields['modifPersonneLieePrenoms_partant1']                                      = prenoms.toString();
}

// Personne Liée 2
if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or personneLiee1.autreDeclarationPersonneLiee) {
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFieldsPers1['pprime_personnePhysique_nomNaissance_prenom']           = identite.personneLieePersonnePhysiqueNomNaissance + ' ' + prenoms.toString();
formFieldsPers1['pPrime_intercalaire_nombre']                  = "1";
formFieldsPers1['complete_p2cmb']                              = false;
formFieldsPers1['complete_p4cmb']                              = false;
formFieldsPers1['complete_cadre']                              = '';
formFieldsPers1['modification_dateAutre']                      = '';
formFieldsPers1['modif_autre']                                 = '';
if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee2.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement')) {
formFieldsPers1['qualitePersonneLiee2']                              = "Fondé de pouvoir";
} else if ((not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and (Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers')))
	or Value('id').of(personneLiee2.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualiteProprietaireIndivis')) {
formFieldsPers1['qualitePersonneLiee2']                              = "Propriétaire indivis";
}
if (etablissementNew.ajoutFondePouvoir or objet.domicileEntreprise or Value('id').of(personneLiee2.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
formFieldsPers1['modifPersonneLieeCategorie_nouveau2']                                     = true;
}
if (Value('id').of(personneLiee2.objetModifPersonneLiee).eq('modifPersonneLiee')) {
formFieldsPers1['modifPersonneLieeCategorie_modifie2']                                     = true;
}
if (Value('id').of(personneLiee2.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers1['modifPersonneLieeCategorie_partant2']                                     = true;
formFieldsPers1['modifPersonneLieeCategorie_partant2Bis']      = true;
}
if (etablissementNew.ajoutFondePouvoir or objet.domicileEntreprise or not Value('id').of(personneLiee2.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers1['modifPersonneLIeeNomNaissance2']                                         = personneLiee2.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
formFieldsPers1['modifPersonneLIeeNomUsage2']                                             = personneLiee2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? personneLiee2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir : '';
var prenoms=[];
for ( i = 0; i < personneLiee2.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee2.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers1['modifPersonneLIeePrenoms2']                                      = prenoms.toString();
formFieldsPers1['modifPersonneLIeeDomicile_voie2']                                        = (personneLiee2.cadreAdressePersonnePouvoir2.rueNumeroAdressePersonnePouvoir != null ? personneLiee2.cadreAdressePersonnePouvoir2.rueNumeroAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee2.cadreAdressePersonnePouvoir2.indiceVoiePersonnePouvoir != null ? personneLiee2.cadreAdressePersonnePouvoir2.indiceVoiePersonnePouvoir : '')
																					 + ' ' + (personneLiee2.cadreAdressePersonnePouvoir2.typeVoiePersonnePouvoir != null ? personneLiee2.cadreAdressePersonnePouvoir2.typeVoiePersonnePouvoir : '')
																					 + ' ' + personneLiee2.cadreAdressePersonnePouvoir2.nomVoiePersonnePouvoir
																					 + ' ' + (personneLiee2.cadreAdressePersonnePouvoir2.rueComplementAdressePersonnePouvoir != null ? personneLiee2.cadreAdressePersonnePouvoir2.rueComplementAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee2.cadreAdressePersonnePouvoir2.distriutionSpecialePersonnePouvoir != null ? personneLiee2.cadreAdressePersonnePouvoir2.distriutionSpecialePersonnePouvoir : '');
formFieldsPers1['modifPersonneLIeeDomicile_codePostal2']                                  = personneLiee2.cadreAdressePersonnePouvoir2.personneLieeAdresseCodePostalPersonnePouvoir;
formFieldsPers1['modifPersonneLIeeDomicile_commune2']                                     = personneLiee2.cadreAdressePersonnePouvoir2.personneLieeAdresseCommunepersonnePouvoir;
if (personneLiee2.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir !== null) {
    var dateTmp = new Date(parseInt(personneLiee2.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['modifPersonneLIeeDateNaissance2']          = date;
}
formFieldsPers1['modifPersonneLIeeLieuNaissanceDepartement2']                              = personneLiee2.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir != null ? personneLiee2.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir.getId() : '';
formFieldsPers1['modifPersonneLIeeLieuNaissanceCommune2']                                  = personneLiee2.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir != null ? personneLiee2.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir : (personneLiee2.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir != null ? (personneLiee2.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir + ' / ' + personneLiee2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir) : '');
formFieldsPers1['modifPersonneLIeeNationalite2']                                           = personneLiee2.personneLieePersonnePhysiqueNationalitePersonnePouvoir != null ? personneLiee2.personneLieePersonnePhysiqueNationalitePersonnePouvoir : '';
}
if (Value('id').of(personneLiee2.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers1['modifPersonneLieeNomNaissance_partant2bis']                                          = personneLiee2.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
var prenoms=[];
for ( i = 0; i < personneLiee2.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee2.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers1['modifPersonneLieePrenom_partant2bis']                                      = prenoms.toString();
}
}

// Personne Liée 3
if (personneLiee2.autreDeclarationPersonneLiee) {
if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee3.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement')) {
formFieldsPers1['qualitePersonneLiee3']                              = "Fondé de pouvoir";
} else if ((not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and (Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers')))
	or Value('id').of(personneLiee3.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualiteProprietaireIndivis')) {
formFieldsPers1['qualitePersonneLiee3']                              = "Propriétaire indivis";
}
if (etablissementNew.ajoutFondePouvoir or objet.domicileEntreprise or Value('id').of(personneLiee3.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
formFieldsPers1['modifPersonneLieeCategorie_nouveau3']                                     = true;
}
if (Value('id').of(personneLiee3.objetModifPersonneLiee).eq('modifPersonneLiee')) {
formFieldsPers1['modifPersonneLieeCategorie_modifie3']                                     = true;
}
if (Value('id').of(personneLiee3.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers1['modifPersonneLieeCategorie_partant3']                                     = true;
formFieldsPers1['modifPersonneLieeCategorie_partant3Bis']      = true;
}
if (etablissementNew.ajoutFondePouvoir or objet.domicileEntreprise or not Value('id').of(personneLiee3.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers1['modifPersonneLIeeNomNaissance3']                                         = personneLiee3.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
formFieldsPers1['modifPersonneLIeeNomUsage3']                                             = personneLiee3.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? personneLiee3.personneLieePersonnePhysiqueNomUsagePersonnePouvoir : '';
var prenoms=[];
for ( i = 0; i < personneLiee3.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee3.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers1['modifPersonneLIeePrenoms3']                                      = prenoms.toString();
formFieldsPers1['modifPersonneLIeeDomicile_voie3']                                        = (personneLiee3.cadreAdressePersonnePouvoir3.rueNumeroAdressePersonnePouvoir != null ? personneLiee3.cadreAdressePersonnePouvoir3.rueNumeroAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee3.cadreAdressePersonnePouvoir3.indiceVoiePersonnePouvoir != null ? personneLiee3.cadreAdressePersonnePouvoir3.indiceVoiePersonnePouvoir : '')
																					 + ' ' + (personneLiee3.cadreAdressePersonnePouvoir3.typeVoiePersonnePouvoir != null ? personneLiee3.cadreAdressePersonnePouvoir3.typeVoiePersonnePouvoir : '')
																					 + ' ' + personneLiee3.cadreAdressePersonnePouvoir3.nomVoiePersonnePouvoir
																					 + ' ' + (personneLiee3.cadreAdressePersonnePouvoir3.rueComplementAdressePersonnePouvoir != null ? personneLiee3.cadreAdressePersonnePouvoir3.rueComplementAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee3.cadreAdressePersonnePouvoir3.distriutionSpecialePersonnePouvoir != null ? personneLiee3.cadreAdressePersonnePouvoir3.distriutionSpecialePersonnePouvoir : '');
formFieldsPers1['modifPersonneLIeeDomicile_codePostal3']                                  = personneLiee3.cadreAdressePersonnePouvoir3.personneLieeAdresseCodePostalPersonnePouvoir;
formFieldsPers1['modifPersonneLIeeDomicile_commune3']                                     = personneLiee3.cadreAdressePersonnePouvoir3.personneLieeAdresseCommunepersonnePouvoir;
if (personneLiee3.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir !== null) {
    var dateTmp = new Date(parseInt(personneLiee3.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['modifPersonneLIeeDateNaissance3']          = date;
}
formFieldsPers1['modifPersonneLIeeLieuNaissanceDepartement3']                              = personneLiee3.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir != null ? personneLiee3.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir.getId() : '';
formFieldsPers1['modifPersonneLIeeLieuNaissanceCommune3']                                  = personneLiee3.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir != null ? personneLiee3.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir : 
																						(personneLiee3.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir != null ? (personneLiee3.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir + ' / ' + personneLiee3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir) : '');
formFieldsPers1['modifPersonneLIeeNationalite3']                                           = personneLiee3.personneLieePersonnePhysiqueNationalitePersonnePouvoir != null ? personneLiee3.personneLieePersonnePhysiqueNationalitePersonnePouvoir : '';
}
if (Value('id').of(personneLiee3.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers1['modifPersonneLieeNomNaissance_partant3']                                          = personneLiee3.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
var prenoms=[];
for ( i = 0; i < personneLiee3.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee3.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers1['modifPersonneLieePrenom_partant3']                                      = prenoms.toString();
}
}

// Personne Liée 4
if (personneLiee3.autreDeclarationPersonneLiee) {
if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee4.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement')) {
formFieldsPers1['qualitePersonneLiee4']                              = "Fondé de pouvoir";
} else if ((not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and (Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers')))
	or Value('id').of(personneLiee4.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualiteProprietaireIndivis')) {
formFieldsPers1['qualitePersonneLiee4']                              = "Propriétaire indivis";
}
if (etablissementNew.ajoutFondePouvoir or objet.domicileEntreprise or Value('id').of(personneLiee4.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
formFieldsPers1['modifPersonneLieeCategorie_nouveau4']                                     = true;
}
if (Value('id').of(personneLiee4.objetModifPersonneLiee).eq('modifPersonneLiee')) {
formFieldsPers1['modifPersonneLieeCategorie_modifie4']                                     = true;
}
if (Value('id').of(personneLiee4.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers1['modifPersonneLieeCategorie_partant4']                                     = true;
formFieldsPers1['modifPersonneLieeCategorie_partant4Bis']      = true;
}
if (etablissementNew.ajoutFondePouvoir or objet.domicileEntreprise or not Value('id').of(personneLiee4.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers1['modifPersonneLIeeNomNaissance4']                                         = personneLiee4.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
formFieldsPers1['modifPersonneLIeeNomUsage4']                                             = personneLiee4.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? personneLiee4.personneLieePersonnePhysiqueNomUsagePersonnePouvoir : '';
var prenoms=[];
for ( i = 0; i < personneLiee4.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee4.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers1['modifPersonneLIeePrenoms4']                                      = prenoms.toString();
formFieldsPers1['modifPersonneLIeeDomicile_voie4']                                        = (personneLiee4.cadreAdressePersonnePouvoir4.rueNumeroAdressePersonnePouvoir != null ? personneLiee4.cadreAdressePersonnePouvoir4.rueNumeroAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee4.cadreAdressePersonnePouvoir4.indiceVoiePersonnePouvoir != null ? personneLiee4.cadreAdressePersonnePouvoir4.indiceVoiePersonnePouvoir : '')
																					 + ' ' + (personneLiee4.cadreAdressePersonnePouvoir4.typeVoiePersonnePouvoir != null ? personneLiee4.cadreAdressePersonnePouvoir4.typeVoiePersonnePouvoir : '')
																					 + ' ' + personneLiee4.cadreAdressePersonnePouvoir4.nomVoiePersonnePouvoir
																					 + ' ' + (personneLiee4.cadreAdressePersonnePouvoir4.rueComplementAdressePersonnePouvoir != null ? personneLiee4.cadreAdressePersonnePouvoir4.rueComplementAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee4.cadreAdressePersonnePouvoir4.distriutionSpecialePersonnePouvoir != null ? personneLiee4.cadreAdressePersonnePouvoir4.distriutionSpecialePersonnePouvoir : '');
formFieldsPers1['modifPersonneLIeeDomicile_codePostal4']                                  = personneLiee4.cadreAdressePersonnePouvoir4.personneLieeAdresseCodePostalPersonnePouvoir;
formFieldsPers1['modifPersonneLIeeDomicile_commune4']                                     = personneLiee4.cadreAdressePersonnePouvoir4.personneLieeAdresseCommunepersonnePouvoir;
if (personneLiee4.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir !== null) {
    var dateTmp = new Date(parseInt(personneLiee4.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['modifPersonneLIeeDateNaissance4']          = date;
}
formFieldsPers1['modifPersonneLIeeLieuNaissanceDepartement4']                              = personneLiee4.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir != null ? personneLiee4.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir.getId() : '';
formFieldsPers1['modifPersonneLIeeLieuNaissanceCommune4']                                  = personneLiee4.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir != null ? personneLiee4.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir : 
																						(personneLiee4.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir != null ? (personneLiee4.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir + ' / ' + personneLiee4.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir) : '');
formFieldsPers1['modifPersonneLIeeNationalite4']                                           = personneLiee4.personneLieePersonnePhysiqueNationalitePersonnePouvoir != null ? personneLiee4.personneLieePersonnePhysiqueNationalitePersonnePouvoir : '';
}
if (Value('id').of(personneLiee4.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers1['modifPersonneLieeNomNaissance_partant4']                                          = personneLiee4.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
var prenoms=[];
for ( i = 0; i < personneLiee4.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee4.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers1['modifPersonneLieePrenom_partant4']                                      = prenoms.toString();
}
}

// Personne Liée 5
if (personneLiee4.autreDeclarationPersonneLiee) {
if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee5.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement')) {
formFieldsPers1['qualitePersonneLiee5']                              = "Fondé de pouvoir";
} else if ((not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and (Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers')))
	or Value('id').of(personneLiee5.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualiteProprietaireIndivis')) {
formFieldsPers1['qualitePersonneLiee5']                              = "Propriétaire indivis";
}
if (etablissementNew.ajoutFondePouvoir or objet.domicileEntreprise or Value('id').of(personneLiee5.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
formFieldsPers1['modifPersonneLieeCategorie_nouveau5']                                     = true;
}
if (Value('id').of(personneLiee5.objetModifPersonneLiee).eq('modifPersonneLiee')) {
formFieldsPers1['modifPersonneLieeCategorie_modifie5']                                     = true;
}
if (Value('id').of(personneLiee5.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers1['modifPersonneLieeCategorie_partant5']                                     = true;
formFieldsPers1['modifPersonneLieeCategorie_partant5Bis']      = true;
}
if (etablissementNew.ajoutFondePouvoir or objet.domicileEntreprise or not Value('id').of(personneLiee5.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers1['modifPersonneLIeeNomNaissance5']                                         = personneLiee5.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
formFieldsPers1['modifPersonneLIeeNomUsage5']                                             = personneLiee5.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? personneLiee5.personneLieePersonnePhysiqueNomUsagePersonnePouvoir : '';
var prenoms=[];
for ( i = 0; i < personneLiee5.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee5.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers1['modifPersonneLIeePrenoms5']                                      = prenoms.toString();
formFieldsPers1['modifPersonneLIeeDomicile_voie5']                                        = (personneLiee5.cadreAdressePersonnePouvoir5.rueNumeroAdressePersonnePouvoir != null ? personneLiee5.cadreAdressePersonnePouvoir5.rueNumeroAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee5.cadreAdressePersonnePouvoir5.indiceVoiePersonnePouvoir != null ? personneLiee5.cadreAdressePersonnePouvoir5.indiceVoiePersonnePouvoir : '')
																					 + ' ' + (personneLiee5.cadreAdressePersonnePouvoir5.typeVoiePersonnePouvoir != null ? personneLiee5.cadreAdressePersonnePouvoir5.typeVoiePersonnePouvoir : '')
																					 + ' ' + personneLiee5.cadreAdressePersonnePouvoir5.nomVoiePersonnePouvoir
																					 + ' ' + (personneLiee5.cadreAdressePersonnePouvoir5.rueComplementAdressePersonnePouvoir != null ? personneLiee5.cadreAdressePersonnePouvoir5.rueComplementAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee5.cadreAdressePersonnePouvoir5.distriutionSpecialePersonnePouvoir != null ? personneLiee5.cadreAdressePersonnePouvoir5.distriutionSpecialePersonnePouvoir : '');
formFieldsPers1['modifPersonneLIeeDomicile_codePostal5']                                  = personneLiee5.cadreAdressePersonnePouvoir5.personneLieeAdresseCodePostalPersonnePouvoir;
formFieldsPers1['modifPersonneLIeeDomicile_commune5']                                     = personneLiee5.cadreAdressePersonnePouvoir5.personneLieeAdresseCommunepersonnePouvoir;
if (personneLiee5.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir !== null) {
    var dateTmp = new Date(parseInt(personneLiee5.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['modifPersonneLIeeDateNaissance5']          = date;
}
formFieldsPers1['modifPersonneLIeeLieuNaissanceDepartement5']                              = personneLiee5.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir != null ? personneLiee5.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir.getId() : '';
formFieldsPers1['modifPersonneLIeeLieuNaissanceCommune5']                                  = personneLiee5.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir != null ? personneLiee5.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir : 
																						(personneLiee5.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir != null ? (personneLiee5.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir + ' / ' + personneLiee5.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir) : '');
formFieldsPers1['modifPersonneLIeeNationalite5']                                           = personneLiee5.personneLieePersonnePhysiqueNationalitePersonnePouvoir != null ? personneLiee5.personneLieePersonnePhysiqueNationalitePersonnePouvoir : '';
}
if (Value('id').of(personneLiee5.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers1['modifPersonneLieeNomNaissance_partant5']                                          = personneLiee5.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
var prenoms=[];
for ( i = 0; i < personneLiee5.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee5.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers1['modifPersonneLieePrenom_partant5']                                      = prenoms.toString();
}
}

// Personne Liée 6
if (personneLiee5.autreDeclarationPersonneLiee) {
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFieldsPers2['pprime_personnePhysique_nomNaissance_prenom']           = identite.personneLieePersonnePhysiqueNomNaissance + ' ' + prenoms.toString();
formFieldsPers2['pPrime_intercalaire_nombre']                  = "2";
formFieldsPers2['complete_p2cmb']                              = false;
formFieldsPers2['complete_p4cmb']                              = false;
formFieldsPers2['complete_cadre']                              = '';
formFieldsPers2['modification_dateAutre']                      = '';
formFieldsPers2['modif_autre']                                 = '';
if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee6.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement')) {
formFieldsPers2['qualitePersonneLiee2']                              = "Fondé de pouvoir";
} else if ((not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and (Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers')))
	or Value('id').of(personneLiee6.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualiteProprietaireIndivis')) {
formFieldsPers2['qualitePersonneLiee2']                              = "Propriétaire indivis";
}
if (etablissementNew.ajoutFondePouvoir or objet.domicileEntreprise or Value('id').of(personneLiee6.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
formFieldsPers2['modifPersonneLieeCategorie_nouveau2']                                     = true;
}
if (Value('id').of(personneLiee6.objetModifPersonneLiee).eq('modifPersonneLiee')) {
formFieldsPers2['modifPersonneLieeCategorie_modifie2']                                     = true;
}
if (Value('id').of(personneLiee6.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers2['modifPersonneLieeCategorie_partant2']                                     = true;
formFieldsPers2['modifPersonneLieeCategorie_partant2Bis']      = true;
}
if (etablissementNew.ajoutFondePouvoir or objet.domicileEntreprise or not Value('id').of(personneLiee6.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers2['modifPersonneLIeeNomNaissance2']                                         = personneLiee6.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
formFieldsPers2['modifPersonneLIeeNomUsage2']                                             = personneLiee6.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? personneLiee6.personneLieePersonnePhysiqueNomUsagePersonnePouvoir : '';
var prenoms=[];
for ( i = 0; i < personneLiee6.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee6.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers2['modifPersonneLIeePrenoms2']                                      = prenoms.toString();
formFieldsPers2['modifPersonneLIeeDomicile_voie2']                                        = (personneLiee6.cadreAdressePersonnePouvoir6.rueNumeroAdressePersonnePouvoir != null ? personneLiee6.cadreAdressePersonnePouvoir6.rueNumeroAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee6.cadreAdressePersonnePouvoir6.indiceVoiePersonnePouvoir != null ? personneLiee6.cadreAdressePersonnePouvoir6.indiceVoiePersonnePouvoir : '')
																					 + ' ' + (personneLiee6.cadreAdressePersonnePouvoir6.typeVoiePersonnePouvoir != null ? personneLiee6.cadreAdressePersonnePouvoir6.typeVoiePersonnePouvoir : '')
																					 + ' ' + personneLiee6.cadreAdressePersonnePouvoir6.nomVoiePersonnePouvoir
																					 + ' ' + (personneLiee6.cadreAdressePersonnePouvoir6.rueComplementAdressePersonnePouvoir != null ? personneLiee6.cadreAdressePersonnePouvoir6.rueComplementAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee6.cadreAdressePersonnePouvoir6.distriutionSpecialePersonnePouvoir != null ? personneLiee6.cadreAdressePersonnePouvoir6.distriutionSpecialePersonnePouvoir : '');
formFieldsPers2['modifPersonneLIeeDomicile_codePostal2']                                  = personneLiee6.cadreAdressePersonnePouvoir6.personneLieeAdresseCodePostalPersonnePouvoir;
formFieldsPers2['modifPersonneLIeeDomicile_commune2']                                     = personneLiee6.cadreAdressePersonnePouvoir6.personneLieeAdresseCommunepersonnePouvoir;
if (personneLiee6.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir !== null) {
    var dateTmp = new Date(parseInt(personneLiee6.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['modifPersonneLIeeDateNaissance2']          = date;
}
formFieldsPers2['modifPersonneLIeeLieuNaissanceDepartement2']                              = personneLiee6.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir != null ? personneLiee6.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir.getId() : '';
formFieldsPers2['modifPersonneLIeeLieuNaissanceCommune2']                                  = personneLiee6.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir != null ? personneLiee6.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir : 
																						(personneLiee6.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir != null ? (personneLiee6.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir + ' / ' + personneLiee6.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir) : '');
formFieldsPers2['modifPersonneLIeeNationalite2']                                           = personneLiee6.personneLieePersonnePhysiqueNationalitePersonnePouvoir != null ? personneLiee6.personneLieePersonnePhysiqueNationalitePersonnePouvoir : '';
}
if (Value('id').of(personneLiee6.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers2['modifPersonneLieeNomNaissance_partant2bis']                                          = personneLiee6.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
var prenoms=[];
for ( i = 0; i < personneLiee6.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee6.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers2['modifPersonneLieePrenom_partant2bis']                                      = prenoms.toString();
}
}

// Personne Liée 7
if (personneLiee6.autreDeclarationPersonneLiee) {
if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee7.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement')) {
formFieldsPers2['qualitePersonneLiee3']                              = "Fondé de pouvoir";
} else if ((not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and (Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers')))
	or Value('id').of(personneLiee7.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualiteProprietaireIndivis')) {
formFieldsPers2['qualitePersonneLiee3']                              = "Propriétaire indivis";
}
if (etablissementNew.ajoutFondePouvoir or objet.domicileEntreprise or Value('id').of(personneLiee7.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
formFieldsPers2['modifPersonneLieeCategorie_nouveau3']                                     = true;
}
if (Value('id').of(personneLiee7.objetModifPersonneLiee).eq('modifPersonneLiee')) {
formFieldsPers2['modifPersonneLieeCategorie_modifie3']                                     = true;
}
if (Value('id').of(personneLiee7.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers2['modifPersonneLieeCategorie_partant3']                                     = true;
formFieldsPers2['modifPersonneLieeCategorie_partant3Bis']      = true;
}
if (etablissementNew.ajoutFondePouvoir or objet.domicileEntreprise or not Value('id').of(personneLiee7.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers2['modifPersonneLIeeNomNaissance3']                                         = personneLiee7.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
formFieldsPers2['modifPersonneLIeeNomUsage3']                                             = personneLiee7.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? personneLiee7.personneLieePersonnePhysiqueNomUsagePersonnePouvoir : '';
var prenoms=[];
for ( i = 0; i < personneLiee7.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee7.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers2['modifPersonneLIeePrenoms3']                                      = prenoms.toString();
formFieldsPers2['modifPersonneLIeeDomicile_voie3']                                        = (personneLiee7.cadreAdressePersonnePouvoir7.rueNumeroAdressePersonnePouvoir != null ? personneLiee7.cadreAdressePersonnePouvoir7.rueNumeroAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee7.cadreAdressePersonnePouvoir7.indiceVoiePersonnePouvoir != null ? personneLiee7.cadreAdressePersonnePouvoir7.indiceVoiePersonnePouvoir : '')
																					 + ' ' + (personneLiee7.cadreAdressePersonnePouvoir7.typeVoiePersonnePouvoir != null ? personneLiee7.cadreAdressePersonnePouvoir7.typeVoiePersonnePouvoir : '')
																					 + ' ' + personneLiee7.cadreAdressePersonnePouvoir7.nomVoiePersonnePouvoir
																					 + ' ' + (personneLiee7.cadreAdressePersonnePouvoir7.rueComplementAdressePersonnePouvoir != null ? personneLiee7.cadreAdressePersonnePouvoir7.rueComplementAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee7.cadreAdressePersonnePouvoir7.distriutionSpecialePersonnePouvoir != null ? personneLiee7.cadreAdressePersonnePouvoir7.distriutionSpecialePersonnePouvoir : '');
formFieldsPers2['modifPersonneLIeeDomicile_codePostal3']                                  = personneLiee7.cadreAdressePersonnePouvoir7.personneLieeAdresseCodePostalPersonnePouvoir;
formFieldsPers2['modifPersonneLIeeDomicile_commune3']                                     = personneLiee7.cadreAdressePersonnePouvoir7.personneLieeAdresseCommunepersonnePouvoir;
if (personneLiee7.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir !== null) {
    var dateTmp = new Date(parseInt(personneLiee7.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['modifPersonneLIeeDateNaissance3']          = date;
}
formFieldsPers2['modifPersonneLIeeLieuNaissanceDepartement3']                              = personneLiee7.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir != null ? personneLiee7.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir.getId() : '';
formFieldsPers2['modifPersonneLIeeLieuNaissanceCommune3']                                  = personneLiee7.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir != null ? personneLiee7.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir : 
																						(personneLiee7.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir != null ? (personneLiee7.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir + ' / ' + personneLiee7.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir) : '');
formFieldsPers2['modifPersonneLIeeNationalite3']                                           = personneLiee7.personneLieePersonnePhysiqueNationalitePersonnePouvoir != null ? personneLiee7.personneLieePersonnePhysiqueNationalitePersonnePouvoir : '';
}
if (Value('id').of(personneLiee7.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers2['modifPersonneLieeNomNaissance_partant3']                                          = personneLiee7.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
var prenoms=[];
for ( i = 0; i < personneLiee7.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee7.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers2['modifPersonneLieePrenom_partant3']                                      = prenoms.toString();
}
}

// Personne Liée 8
if (personneLiee7.autreDeclarationPersonneLiee) {
if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee8.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement')) {
formFieldsPers2['qualitePersonneLiee4']                              = "Fondé de pouvoir";
} else if ((not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and (Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers')))
	or Value('id').of(personneLiee8.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualiteProprietaireIndivis')) {
formFieldsPers2['qualitePersonneLiee4']                              = "Propriétaire indivis";
}
if (etablissementNew.ajoutFondePouvoir or objet.domicileEntreprise or Value('id').of(personneLiee8.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
formFieldsPers2['modifPersonneLieeCategorie_nouveau4']                                     = true;
}
if (Value('id').of(personneLiee8.objetModifPersonneLiee).eq('modifPersonneLiee')) {
formFieldsPers2['modifPersonneLieeCategorie_modifie4']                                     = true;
}
if (Value('id').of(personneLiee8.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers2['modifPersonneLieeCategorie_partant4']                                     = true;
formFieldsPers2['modifPersonneLieeCategorie_partant4Bis']      = true;
}
if (etablissementNew.ajoutFondePouvoir or objet.domicileEntreprise or not Value('id').of(personneLiee8.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers2['modifPersonneLIeeNomNaissance4']                                         = personneLiee8.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
formFieldsPers2['modifPersonneLIeeNomUsage4']                                             = personneLiee8.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? personneLiee8.personneLieePersonnePhysiqueNomUsagePersonnePouvoir : '';
var prenoms=[];
for ( i = 0; i < personneLiee8.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee8.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers2['modifPersonneLIeePrenoms4']                                      = prenoms.toString();
formFieldsPers2['modifPersonneLIeeDomicile_voie4']                                        = (personneLiee8.cadreAdressePersonnePouvoir8.rueNumeroAdressePersonnePouvoir != null ? personneLiee8.cadreAdressePersonnePouvoir8.rueNumeroAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee8.cadreAdressePersonnePouvoir8.indiceVoiePersonnePouvoir != null ? personneLiee8.cadreAdressePersonnePouvoir8.indiceVoiePersonnePouvoir : '')
																					 + ' ' + (personneLiee8.cadreAdressePersonnePouvoir8.typeVoiePersonnePouvoir != null ? personneLiee8.cadreAdressePersonnePouvoir8.typeVoiePersonnePouvoir : '')
																					 + ' ' + personneLiee8.cadreAdressePersonnePouvoir8.nomVoiePersonnePouvoir
																					 + ' ' + (personneLiee8.cadreAdressePersonnePouvoir8.rueComplementAdressePersonnePouvoir != null ? personneLiee8.cadreAdressePersonnePouvoir8.rueComplementAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee8.cadreAdressePersonnePouvoir8.distriutionSpecialePersonnePouvoir != null ? personneLiee8.cadreAdressePersonnePouvoir8.distriutionSpecialePersonnePouvoir : '');
formFieldsPers2['modifPersonneLIeeDomicile_codePostal4']                                  = personneLiee8.cadreAdressePersonnePouvoir8.personneLieeAdresseCodePostalPersonnePouvoir;
formFieldsPers2['modifPersonneLIeeDomicile_commune4']                                     = personneLiee8.cadreAdressePersonnePouvoir8.personneLieeAdresseCommunepersonnePouvoir;
if (personneLiee8.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir !== null) {
    var dateTmp = new Date(parseInt(personneLiee8.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['modifPersonneLIeeDateNaissance4']          = date;
}
formFieldsPers2['modifPersonneLIeeLieuNaissanceDepartement4']                              = personneLiee8.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir != null ? personneLiee8.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir.getId() : '';
formFieldsPers2['modifPersonneLIeeLieuNaissanceCommune4']                                  = personneLiee8.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir != null ? personneLiee8.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir : 
																						(personneLiee8.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir != null ? (personneLiee8.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir + ' / ' + personneLiee8.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir) : '');
formFieldsPers2['modifPersonneLIeeNationalite4']                                           = personneLiee8.personneLieePersonnePhysiqueNationalitePersonnePouvoir != null ? personneLiee8.personneLieePersonnePhysiqueNationalitePersonnePouvoir : '';
}
if (Value('id').of(personneLiee8.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers2['modifPersonneLieeNomNaissance_partant4']                                          = personneLiee8.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
var prenoms=[];
for ( i = 0; i < personneLiee8.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee8.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers2['modifPersonneLieePrenom_partant4']                                      = prenoms.toString();
}
}

// Personne Liée 9
if (personneLiee8.autreDeclarationPersonneLiee) {
if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee9.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement')) {
formFieldsPers2['qualitePersonneLiee5']                              = "Fondé de pouvoir";
} else if ((not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and (Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers')))
	or Value('id').of(personneLiee1.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualiteProprietaireIndivis')) {
formFieldsPers2['qualitePersonneLiee5']                              = "Propriétaire indivis";
}
if (etablissementNew.ajoutFondePouvoir or objet.domicileEntreprise or Value('id').of(personneLiee9.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
formFieldsPers2['modifPersonneLieeCategorie_nouveau5']                                     = true;
}
if (Value('id').of(personneLiee9.objetModifPersonneLiee).eq('modifPersonneLiee')) {
formFieldsPers2['modifPersonneLieeCategorie_modifie5']                                     = true;
}
if (Value('id').of(personneLiee9.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers2['modifPersonneLieeCategorie_partant5']                                     = true;
formFieldsPers2['modifPersonneLieeCategorie_partant5Bis']      = true;
}
if (etablissementNew.ajoutFondePouvoir or objet.domicileEntreprise or not Value('id').of(personneLiee9.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers2['modifPersonneLIeeNomNaissance5']                                         = personneLiee9.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
formFieldsPers2['modifPersonneLIeeNomUsage5']                                             = personneLiee9.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? personneLiee9.personneLieePersonnePhysiqueNomUsagePersonnePouvoir : '';
var prenoms=[];
for ( i = 0; i < personneLiee9.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee9.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers2['modifPersonneLIeePrenoms5']                                      = prenoms.toString();
formFieldsPers2['modifPersonneLIeeDomicile_voie5']                                        = (personneLiee9.cadreAdressePersonnePouvoir9.rueNumeroAdressePersonnePouvoir != null ? personneLiee9.cadreAdressePersonnePouvoir9.rueNumeroAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee9.cadreAdressePersonnePouvoir9.indiceVoiePersonnePouvoir != null ? personneLiee9.cadreAdressePersonnePouvoir9.indiceVoiePersonnePouvoir : '')
																					 + ' ' + (personneLiee9.cadreAdressePersonnePouvoir9.typeVoiePersonnePouvoir != null ? personneLiee9.cadreAdressePersonnePouvoir9.typeVoiePersonnePouvoir : '')
																					 + ' ' + personneLiee9.cadreAdressePersonnePouvoir9.nomVoiePersonnePouvoir
																					 + ' ' + (personneLiee9.cadreAdressePersonnePouvoir9.rueComplementAdressePersonnePouvoir != null ? personneLiee9.cadreAdressePersonnePouvoir9.rueComplementAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee9.cadreAdressePersonnePouvoir9.distriutionSpecialePersonnePouvoir != null ? personneLiee9.cadreAdressePersonnePouvoir9.distriutionSpecialePersonnePouvoir : '');
formFieldsPers2['modifPersonneLIeeDomicile_codePostal5']                                  = personneLiee9.cadreAdressePersonnePouvoir9.personneLieeAdresseCodePostalPersonnePouvoir;
formFieldsPers2['modifPersonneLIeeDomicile_commune5']                                     = personneLiee9.cadreAdressePersonnePouvoir9.personneLieeAdresseCommunepersonnePouvoir;
if (personneLiee9.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir !== null) {
    var dateTmp = new Date(parseInt(personneLiee9.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['modifPersonneLIeeDateNaissance5']          = date;
}
formFieldsPers2['modifPersonneLIeeLieuNaissanceDepartement5']                              = personneLiee9.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir != null ? personneLiee9.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir.getId() : '';
formFieldsPers2['modifPersonneLIeeLieuNaissanceCommune5']                                  = personneLiee9.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir != null ? personneLiee9.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir : 
																						(personneLiee9.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir != null ? (personneLiee9.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir + ' / ' + personneLiee9.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir) : '');
formFieldsPers2['modifPersonneLIeeNationalite5']                                           = personneLiee9.personneLieePersonnePhysiqueNationalitePersonnePouvoir != null ? personneLiee9.personneLieePersonnePhysiqueNationalitePersonnePouvoir : '';
}
if (Value('id').of(personneLiee9.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers2['modifPersonneLieeNomNaissance_partant5']                                          = personneLiee9.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
var prenoms=[];
for ( i = 0; i < personneLiee9.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee9.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers2['modifPersonneLieePrenom_partant5']                                      = prenoms.toString();
}
}
}

// Si héritiers et exploitant (et fondés de pouvoir et propriétaires indivis)
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	and (Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers')
	and Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant')))
	or	(Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers')
	and Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant'))
	or objet.cadreObjet22P.declarationIndivisaire){
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFieldsPers1['pprime_personnePhysique_nomNaissance_prenom']           = identite.personneLieePersonnePhysiqueNomNaissance + ' ' + prenoms.toString();
formFieldsPers1['pPrime_intercalaire_nombre']                  = "1";
formFieldsPers1['complete_p2cmb']                              = false;
formFieldsPers1['complete_p4cmb']                              = false;
formFieldsPers1['complete_cadre']                              = '';
formFieldsPers1['modification_dateAutre']                      = '';
formFieldsPers1['modif_autre']                                 = '';

// Personne Liée 1
if(Value('id').of(personneLiee1.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement')) {
formFieldsPers1['qualitePersonneLiee2']                              = "Fondé de pouvoir";
} else {
formFieldsPers1['qualitePersonneLiee2']                              = "Propriétaire indivis";
}
if (objet.cadreObjet22P.declarationIndivisaire or Value('id').of(personneLiee1.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
formFieldsPers1['modifPersonneLieeCategorie_nouveau2']                                     = true;
}
if (Value('id').of(personneLiee1.objetModifPersonneLiee).eq('modifPersonneLiee')) {
formFieldsPers1['modifPersonneLieeCategorie_modifie2']                                     = true;
}
if (Value('id').of(personneLiee1.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers1['modifPersonneLieeCategorie_partant2']                                     = true;
formFieldsPers1['modifPersonneLieeCategorie_partant2Bis']      = true;
}

if (objet.cadreObjet22P.declarationIndivisaire or not Value('id').of(personneLiee1.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers1['modifPersonneLIeeNomNaissance2']                                          = personneLiee1.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
formFieldsPers1['modifPersonneLIeeNomUsage2']                                              = personneLiee1.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? personneLiee1.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null : '';
var prenoms=[];
for ( i = 0; i < personneLiee1.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee1.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers1['modifPersonneLIeePrenoms2']                                      = prenoms.toString();
formFieldsPers1['modifPersonneLIeeDomicile_voie2']                                         = (personneLiee1.cadreAdressePersonnePouvoir.rueNumeroAdressePersonnePouvoir != null ? personneLiee1.cadreAdressePersonnePouvoir.rueNumeroAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee1.cadreAdressePersonnePouvoir.indiceVoiePersonnePouvoir != null ? personneLiee1.cadreAdressePersonnePouvoir.indiceVoiePersonnePouvoir : '')
																					 + ' ' + (personneLiee1.cadreAdressePersonnePouvoir.typeVoiePersonnePouvoir != null ? personneLiee1.cadreAdressePersonnePouvoir.typeVoiePersonnePouvoir : '')
																					 + ' ' + personneLiee1.cadreAdressePersonnePouvoir.nomVoiePersonnePouvoir
																					 + ' ' + (personneLiee1.cadreAdressePersonnePouvoir.rueComplementAdressePersonnePouvoir != null ? personneLiee1.cadreAdressePersonnePouvoir.rueComplementAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee1.cadreAdressePersonnePouvoir.distriutionSpecialePersonnePouvoir != null ? personneLiee1.cadreAdressePersonnePouvoir.distriutionSpecialePersonnePouvoir : '');
formFieldsPers1['modifPersonneLIeeDomicile_codePostal2']                                   = personneLiee1.cadreAdressePersonnePouvoir.personneLieeAdresseCodePostalPersonnePouvoir;
formFieldsPers1['modifPersonneLIeeDomicile_commune2']                                      = personneLiee1.cadreAdressePersonnePouvoir.personneLieeAdresseCommunepersonnePouvoir;
if (personneLiee1.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir !== null) {
    var dateTmp = new Date(parseInt(personneLiee1.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['modifPersonneLIeeDateNaissance2']          = date;
}
formFieldsPers1['modifPersonneLIeeLieuNaissanceDepartement2']                              = personneLiee1.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir != null ? personneLiee1.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir.getId() : '';
formFieldsPers1['modifPersonneLIeeLieuNaissanceCommune2']                                  = personneLiee1.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir != null ? personneLiee1.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir : 
																						(personneLiee1.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir != null ? (personneLiee1.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir + ' / ' + personneLiee1.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir) : '');
formFieldsPers1['modifPersonneLIeeNationalite2']                                           = personneLiee1.personneLieePersonnePhysiqueNationalitePersonnePouvoir != null ? personneLiee1.personneLieePersonnePhysiqueNationalitePersonnePouvoir : '';
}
if (Value('id').of(personneLiee1.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers1['modifPersonneLieeNomNaissance_partant2bis']                                          = personneLiee1.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
var prenoms=[];
for ( i = 0; i < personneLiee1.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee1.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers1['modifPersonneLieePrenom_partant2bis']                                      = prenoms.toString();
}

// Personne Liée 2
if (personneLiee1.autreDeclarationPersonneLiee) {
if(Value('id').of(personneLiee2.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement')) {
formFieldsPers1['qualitePersonneLiee3']                              = "Fondé de pouvoir";
} else {
formFieldsPers1['qualitePersonneLiee3']                              = "Propriétaire indivis";
}
if (objet.cadreObjet22P.declarationIndivisaire or Value('id').of(personneLiee2.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
formFieldsPers1['modifPersonneLieeCategorie_nouveau3']                                     = true;
}
if (Value('id').of(personneLiee2.objetModifPersonneLiee).eq('modifPersonneLiee')) {
formFieldsPers1['modifPersonneLieeCategorie_modifie3']                                     = true;
}
if (Value('id').of(personneLiee2.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers1['modifPersonneLieeCategorie_partant3']                                     = true;
formFieldsPers1['modifPersonneLieeCategorie_partant3Bis']      = true;
}
if (objet.cadreObjet22P.declarationIndivisaire or not Value('id').of(personneLiee2.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers1['modifPersonneLIeeNomNaissance3']                                         = personneLiee2.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
formFieldsPers1['modifPersonneLIeeNomUsage3']                                             = personneLiee2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? personneLiee2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null : '';
var prenoms=[];
for ( i = 0; i < personneLiee2.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee2.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers1['modifPersonneLIeePrenoms3']                                      = prenoms.toString();
formFieldsPers1['modifPersonneLIeeDomicile_voie3']                                        = (personneLiee2.cadreAdressePersonnePouvoir2.rueNumeroAdressePersonnePouvoir != null ? personneLiee2.cadreAdressePersonnePouvoir2.rueNumeroAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee2.cadreAdressePersonnePouvoir2.indiceVoiePersonnePouvoir != null ? personneLiee2.cadreAdressePersonnePouvoir2.indiceVoiePersonnePouvoir : '')
																					 + ' ' + (personneLiee2.cadreAdressePersonnePouvoir2.typeVoiePersonnePouvoir != null ? personneLiee2.cadreAdressePersonnePouvoir2.typeVoiePersonnePouvoir : '')
																					 + ' ' + personneLiee2.cadreAdressePersonnePouvoir2.nomVoiePersonnePouvoir
																					 + ' ' + (personneLiee2.cadreAdressePersonnePouvoir2.rueComplementAdressePersonnePouvoir != null ? personneLiee2.cadreAdressePersonnePouvoir2.rueComplementAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee2.cadreAdressePersonnePouvoir2.distriutionSpecialePersonnePouvoir != null ? personneLiee2.cadreAdressePersonnePouvoir2.distriutionSpecialePersonnePouvoir : '');
formFieldsPers1['modifPersonneLIeeDomicile_codePostal3']                                  = personneLiee2.cadreAdressePersonnePouvoir2.personneLieeAdresseCodePostalPersonnePouvoir;
formFieldsPers1['modifPersonneLIeeDomicile_commune3']                                     = personneLiee2.cadreAdressePersonnePouvoir2.personneLieeAdresseCommunepersonnePouvoir;
if (personneLiee2.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir !== null) {
    var dateTmp = new Date(parseInt(personneLiee2.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['modifPersonneLIeeDateNaissance3']          = date;
}
formFieldsPers1['modifPersonneLIeeLieuNaissanceDepartement3']                              = personneLiee2.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir != null ? personneLiee2.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir.getId() : '';
formFieldsPers1['modifPersonneLIeeLieuNaissanceCommune3']                                  = personneLiee2.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir != null ? personneLiee2.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir : 
																						(personneLiee2.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir != null ? (personneLiee2.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir + ' / ' + personneLiee2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir) : '');
formFieldsPers1['modifPersonneLIeeNationalite3']                                           = personneLiee2.personneLieePersonnePhysiqueNationalitePersonnePouvoir != null ? personneLiee2.personneLieePersonnePhysiqueNationalitePersonnePouvoir : '';
}
if (Value('id').of(personneLiee2.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers1['modifPersonneLieeNomNaissance_partant3']                                          = personneLiee2.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
var prenoms=[];
for ( i = 0; i < personneLiee2.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee2.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers1['modifPersonneLieePrenom_partant3']                                      = prenoms.toString();
}
}

// Personne Liée 3
if (personneLiee2.autreDeclarationPersonneLiee) {
if(Value('id').of(personneLiee3.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement')) {
formFieldsPers1['qualitePersonneLiee4']                              = "Fondé de pouvoir";
} else {
formFieldsPers1['qualitePersonneLiee4']                              = "Propriétaire indivis";
}
if (objet.cadreObjet22P.declarationIndivisaire or Value('id').of(personneLiee3.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
formFieldsPers1['modifPersonneLieeCategorie_nouveau4']                                     = true;
}
if (Value('id').of(personneLiee3.objetModifPersonneLiee).eq('modifPersonneLiee')) {
formFieldsPers1['modifPersonneLieeCategorie_modifie4']                                     = true;
}
if (Value('id').of(personneLiee3.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers1['modifPersonneLieeCategorie_partant4']                                     = true;
formFieldsPers1['modifPersonneLieeCategorie_partant4Bis']      = true;
}
if (objet.cadreObjet22P.declarationIndivisaire or not Value('id').of(personneLiee3.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers1['modifPersonneLIeeNomNaissance4']                                         = personneLiee3.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
formFieldsPers1['modifPersonneLIeeNomUsage4']                                             = personneLiee3.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? personneLiee3.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null : '';
var prenoms=[];
for ( i = 0; i < personneLiee3.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee3.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers1['modifPersonneLIeePrenoms4']                                      = prenoms.toString();
formFieldsPers1['modifPersonneLIeeDomicile_voie4']                                        = (personneLiee3.cadreAdressePersonnePouvoir3.rueNumeroAdressePersonnePouvoir != null ? personneLiee3.cadreAdressePersonnePouvoir3.rueNumeroAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee3.cadreAdressePersonnePouvoir3.indiceVoiePersonnePouvoir != null ? personneLiee3.cadreAdressePersonnePouvoir3.indiceVoiePersonnePouvoir : '')
																					 + ' ' + (personneLiee3.cadreAdressePersonnePouvoir3.typeVoiePersonnePouvoir != null ? personneLiee3.cadreAdressePersonnePouvoir3.typeVoiePersonnePouvoir : '')
																					 + ' ' + personneLiee3.cadreAdressePersonnePouvoir3.nomVoiePersonnePouvoir
																					 + ' ' + (personneLiee3.cadreAdressePersonnePouvoir3.rueComplementAdressePersonnePouvoir != null ? personneLiee3.cadreAdressePersonnePouvoir3.rueComplementAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee3.cadreAdressePersonnePouvoir3.distriutionSpecialePersonnePouvoir != null ? personneLiee3.cadreAdressePersonnePouvoir3.distriutionSpecialePersonnePouvoir : '');
formFieldsPers1['modifPersonneLIeeDomicile_codePostal4']                                  = personneLiee3.cadreAdressePersonnePouvoir3.personneLieeAdresseCodePostalPersonnePouvoir;
formFieldsPers1['modifPersonneLIeeDomicile_commune4']                                     = personneLiee3.cadreAdressePersonnePouvoir3.personneLieeAdresseCommunepersonnePouvoir;
if (personneLiee3.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir !== null) {
    var dateTmp = new Date(parseInt(personneLiee3.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['modifPersonneLIeeDateNaissance4']          = date;
}
formFieldsPers1['modifPersonneLIeeLieuNaissanceDepartement4']                              = personneLiee3.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir != null ? personneLiee3.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir.getId() : '';
formFieldsPers1['modifPersonneLIeeLieuNaissanceCommune4']                                  = personneLiee3.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir != null ? personneLiee3.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir : 
																						(personneLiee3.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir != null ? (personneLiee3.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir + ' / ' + personneLiee3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir) : '');
formFieldsPers1['modifPersonneLIeeNationalite4']                                           = personneLiee3.personneLieePersonnePhysiqueNationalitePersonnePouvoir != null ? personneLiee3.personneLieePersonnePhysiqueNationalitePersonnePouvoir : '';
}
if (Value('id').of(personneLiee3.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers1['modifPersonneLieeNomNaissance_partant4']                                          = personneLiee3.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
var prenoms=[];
for ( i = 0; i < personneLiee3.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee3.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers1['modifPersonneLieePrenom_partant4']                                      = prenoms.toString();
}
}

// Personne Liée 4
if (personneLiee3.autreDeclarationPersonneLiee) {
if(Value('id').of(personneLiee4.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement')) {
formFieldsPers1['qualitePersonneLiee5']                              = "Fondé de pouvoir";
} else {
formFieldsPers1['qualitePersonneLiee5']                              = "Propriétaire indivis";
}
if (objet.cadreObjet22P.declarationIndivisaire or Value('id').of(personneLiee4.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
formFieldsPers1['modifPersonneLieeCategorie_nouveau5']                                     = true;
}
if (Value('id').of(personneLiee4.objetModifPersonneLiee).eq('modifPersonneLiee')) {
formFieldsPers1['modifPersonneLieeCategorie_modifie5']                                     = true;
}
if (Value('id').of(personneLiee4.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers1['modifPersonneLieeCategorie_partant5']                                     = true;
formFieldsPers1['modifPersonneLieeCategorie_partant5Bis']      = true;
}
if (objet.cadreObjet22P.declarationIndivisaire or not Value('id').of(personneLiee4.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers1['modifPersonneLIeeNomNaissance5']                                         = personneLiee4.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
formFieldsPers1['modifPersonneLIeeNomUsage5']                                             = personneLiee4.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? personneLiee4.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null : '';
var prenoms=[];
for ( i = 0; i < personneLiee4.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee4.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers1['modifPersonneLIeePrenoms5']                                      = prenoms.toString();
formFieldsPers1['modifPersonneLIeeDomicile_voie5']                                        = (personneLiee4.cadreAdressePersonnePouvoir4.rueNumeroAdressePersonnePouvoir != null ? personneLiee4.cadreAdressePersonnePouvoir4.rueNumeroAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee4.cadreAdressePersonnePouvoir4.indiceVoiePersonnePouvoir != null ? personneLiee4.cadreAdressePersonnePouvoir4.indiceVoiePersonnePouvoir : '')
																					 + ' ' + (personneLiee4.cadreAdressePersonnePouvoir4.typeVoiePersonnePouvoir != null ? personneLiee4.cadreAdressePersonnePouvoir4.typeVoiePersonnePouvoir : '')
																					 + ' ' + personneLiee4.cadreAdressePersonnePouvoir4.nomVoiePersonnePouvoir
																					 + ' ' + (personneLiee4.cadreAdressePersonnePouvoir4.rueComplementAdressePersonnePouvoir != null ? personneLiee4.cadreAdressePersonnePouvoir4.rueComplementAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee4.cadreAdressePersonnePouvoir4.distriutionSpecialePersonnePouvoir != null ? personneLiee4.cadreAdressePersonnePouvoir4.distriutionSpecialePersonnePouvoir : '');
formFieldsPers1['modifPersonneLIeeDomicile_codePostal5']                                  = personneLiee4.cadreAdressePersonnePouvoir4.personneLieeAdresseCodePostalPersonnePouvoir;
formFieldsPers1['modifPersonneLIeeDomicile_commune5']                                     = personneLiee4.cadreAdressePersonnePouvoir4.personneLieeAdresseCommunepersonnePouvoir;
if (personneLiee4.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir !== null) {
    var dateTmp = new Date(parseInt(personneLiee4.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers1['modifPersonneLIeeDateNaissance5']          = date;
}
formFieldsPers1['modifPersonneLIeeLieuNaissanceDepartement5']                              = personneLiee4.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir != null ? personneLiee4.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir.getId() : '';
formFieldsPers1['modifPersonneLIeeLieuNaissanceCommune5']                                  = personneLiee4.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir != null ? personneLiee4.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir : 
																						(personneLiee4.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir != null ? (personneLiee4.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir + ' / ' + personneLiee4.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir) : '');
formFieldsPers1['modifPersonneLIeeNationalite5']                                           = personneLiee4.personneLieePersonnePhysiqueNationalitePersonnePouvoir != null ? personneLiee4.personneLieePersonnePhysiqueNationalitePersonnePouvoir : '';
}
if (Value('id').of(personneLiee4.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers1['modifPersonneLieeNomNaissance_partant5']                                          = personneLiee4.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
var prenoms=[];
for ( i = 0; i < personneLiee4.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee4.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers1['modifPersonneLieePrenom_partant5']                                      = prenoms.toString();
}
}

// Personne Liée 5
if (personneLiee4.autreDeclarationPersonneLiee) {
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFieldsPers2['pprime_personnePhysique_nomNaissance_prenom']           = identite.personneLieePersonnePhysiqueNomNaissance + ' ' + prenoms.toString();
formFieldsPers2['pPrime_intercalaire_nombre']                  = "2";
formFieldsPers2['complete_p2cmb']                              = false;
formFieldsPers2['complete_p4cmb']                              = false;
formFieldsPers2['complete_cadre']                              = '';
formFieldsPers2['modification_dateAutre']                      = '';
formFieldsPers2['modif_autre']                                 = '';
if(Value('id').of(personneLiee5.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement')) {
formFieldsPers2['qualitePersonneLiee2']                              = "Fondé de pouvoir";
} else {
formFieldsPers2['qualitePersonneLiee2']                              = "Propriétaire indivis";
}
if (objet.cadreObjet22P.declarationIndivisaire or Value('id').of(personneLiee5.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
formFieldsPers2['modifPersonneLieeCategorie_nouveau2']                                     = true;
}
if (Value('id').of(personneLiee5.objetModifPersonneLiee).eq('modifPersonneLiee')) {
formFieldsPers2['modifPersonneLieeCategorie_modifie2']                                     = true;
}
if (Value('id').of(personneLiee5.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers2['modifPersonneLieeCategorie_partant2']                                     = true;
formFieldsPers2['modifPersonneLieeCategorie_partant2Bis']      = true;
}
if (objet.cadreObjet22P.declarationIndivisaire or not Value('id').of(personneLiee5.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers2['modifPersonneLIeeNomNaissance2']                                         = personneLiee5.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
formFieldsPers2['modifPersonneLIeeNomUsage2']                                             = personneLiee5.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? personneLiee5.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null : '';
var prenoms=[];
for ( i = 0; i < personneLiee5.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee5.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers2['modifPersonneLIeePrenoms2']                                      = prenoms.toString();
formFieldsPers2['modifPersonneLIeeDomicile_voie2']                                        = (personneLiee5.cadreAdressePersonnePouvoir5.rueNumeroAdressePersonnePouvoir != null ? personneLiee5.cadreAdressePersonnePouvoir5.rueNumeroAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee5.cadreAdressePersonnePouvoir5.indiceVoiePersonnePouvoir != null ? personneLiee5.cadreAdressePersonnePouvoir5.indiceVoiePersonnePouvoir : '')
																					 + ' ' + (personneLiee5.cadreAdressePersonnePouvoir5.typeVoiePersonnePouvoir != null ? personneLiee5.cadreAdressePersonnePouvoir5.typeVoiePersonnePouvoir : '')
																					 + ' ' + personneLiee5.cadreAdressePersonnePouvoir5.nomVoiePersonnePouvoir
																					 + ' ' + (personneLiee5.cadreAdressePersonnePouvoir5.rueComplementAdressePersonnePouvoir != null ? personneLiee5.cadreAdressePersonnePouvoir5.rueComplementAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee5.cadreAdressePersonnePouvoir5.distriutionSpecialePersonnePouvoir != null ? personneLiee5.cadreAdressePersonnePouvoir5.distriutionSpecialePersonnePouvoir : '');
formFieldsPers2['modifPersonneLIeeDomicile_codePostal2']                                  = personneLiee5.cadreAdressePersonnePouvoir5.personneLieeAdresseCodePostalPersonnePouvoir;
formFieldsPers2['modifPersonneLIeeDomicile_commune2']                                     = personneLiee5.cadreAdressePersonnePouvoir5.personneLieeAdresseCommunepersonnePouvoir;
if (personneLiee5.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir !== null) {
    var dateTmp = new Date(parseInt(personneLiee5.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['modifPersonneLIeeDateNaissance2']          = date;
}
formFieldsPers2['modifPersonneLIeeLieuNaissanceDepartement2']                              = personneLiee5.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir != null ? personneLiee5.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir.getId() : '';
formFieldsPers2['modifPersonneLIeeLieuNaissanceCommune2']                                  = personneLiee5.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir != null ? personneLiee5.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir : 
																						(personneLiee5.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir != null ? (personneLiee5.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir + ' / ' + personneLiee5.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir) : '');
formFieldsPers2['modifPersonneLIeeNationalite2']                                           = personneLiee5.personneLieePersonnePhysiqueNationalitePersonnePouvoir != null ? personneLiee5.personneLieePersonnePhysiqueNationalitePersonnePouvoir : '';
}
if (Value('id').of(personneLiee5.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers2['modifPersonneLieeNomNaissance_partant2bis']                                          = personneLiee5.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
var prenoms=[];
for ( i = 0; i < personneLiee5.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee5.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers2['modifPersonneLieePrenom_partant2bis']                                      = prenoms.toString();
}
}

// Personne Liée 6
if (personneLiee5.autreDeclarationPersonneLiee) {
if(Value('id').of(personneLiee6.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement')) {
formFieldsPers2['qualitePersonneLiee3']                              = "Fondé de pouvoir";
} else {
formFieldsPers2['qualitePersonneLiee3']                              = "Propriétaire indivis";
}
if (objet.cadreObjet22P.declarationIndivisaire or Value('id').of(personneLiee6.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
formFieldsPers2['modifPersonneLieeCategorie_nouveau3']                                     = true;
}
if (Value('id').of(personneLiee6.objetModifPersonneLiee).eq('modifPersonneLiee')) {
formFieldsPers2['modifPersonneLieeCategorie_modifie3']                                     = true;
}
if (Value('id').of(personneLiee6.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers2['modifPersonneLieeCategorie_partant3']                                     = true;
formFieldsPers2['modifPersonneLieeCategorie_partant3Bis']      = true;
}
if (objet.cadreObjet22P.declarationIndivisaire or not Value('id').of(personneLiee6.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers2['modifPersonneLIeeNomNaissance3']                                         = personneLiee6.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
formFieldsPers2['modifPersonneLIeeNomUsage3']                                             = personneLiee6.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? personneLiee6.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null : '';
var prenoms=[];
for ( i = 0; i < personneLiee6.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee6.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers2['modifPersonneLIeePrenoms3']                                      = prenoms.toString();
formFieldsPers2['modifPersonneLIeeDomicile_voie3']                                        = (personneLiee6.cadreAdressePersonnePouvoir6.rueNumeroAdressePersonnePouvoir != null ? personneLiee6.cadreAdressePersonnePouvoir6.rueNumeroAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee6.cadreAdressePersonnePouvoir6.indiceVoiePersonnePouvoir != null ? personneLiee6.cadreAdressePersonnePouvoir6.indiceVoiePersonnePouvoir : '')
																					 + ' ' + (personneLiee6.cadreAdressePersonnePouvoir6.typeVoiePersonnePouvoir != null ? personneLiee6.cadreAdressePersonnePouvoir6.typeVoiePersonnePouvoir : '')
																					 + ' ' + personneLiee6.cadreAdressePersonnePouvoir6.nomVoiePersonnePouvoir
																					 + ' ' + (personneLiee6.cadreAdressePersonnePouvoir6.rueComplementAdressePersonnePouvoir != null ? personneLiee6.cadreAdressePersonnePouvoir6.rueComplementAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee6.cadreAdressePersonnePouvoir6.distriutionSpecialePersonnePouvoir != null ? personneLiee6.cadreAdressePersonnePouvoir6.distriutionSpecialePersonnePouvoir : '');
formFieldsPers2['modifPersonneLIeeDomicile_codePostal3']                                  = personneLiee6.cadreAdressePersonnePouvoir6.personneLieeAdresseCodePostalPersonnePouvoir;
formFieldsPers2['modifPersonneLIeeDomicile_commune3']                                     = personneLiee6.cadreAdressePersonnePouvoir6.personneLieeAdresseCommunepersonnePouvoir;
if (personneLiee6.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir !== null) {
    var dateTmp = new Date(parseInt(personneLiee6.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['modifPersonneLIeeDateNaissance3']          = date;
}
formFieldsPers2['modifPersonneLIeeLieuNaissanceDepartement3']                              = personneLiee6.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir != null ? personneLiee6.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir.getId() : '';
formFieldsPers2['modifPersonneLIeeLieuNaissanceCommune3']                                  = personneLiee6.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir != null ? personneLiee6.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir : 
																						(personneLiee6.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir != null ? (personneLiee6.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir + ' / ' + personneLiee6.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir) : '');
formFieldsPers2['modifPersonneLIeeNationalite3']                                           = personneLiee6.personneLieePersonnePhysiqueNationalitePersonnePouvoir != null ? personneLiee6.personneLieePersonnePhysiqueNationalitePersonnePouvoir : '';
}
if (Value('id').of(personneLiee6.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers2['modifPersonneLieeNomNaissance_partant3']                                          = personneLiee6.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
var prenoms=[];
for ( i = 0; i < personneLiee6.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee6.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers2['modifPersonneLieePrenom_partant3']                                      = prenoms.toString();
}
}

// Personne Liée 7
if (personneLiee6.autreDeclarationPersonneLiee) {
if(Value('id').of(personneLiee7.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement')) {
formFieldsPers2['qualitePersonneLiee4']                              = "Fondé de pouvoir";
} else {
formFieldsPers2['qualitePersonneLiee4']                              = "Propriétaire indivis";
}
if (objet.cadreObjet22P.declarationIndivisaire or Value('id').of(personneLiee7.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
formFieldsPers2['modifPersonneLieeCategorie_nouveau4']                                     = true;
}
if (Value('id').of(personneLiee7.objetModifPersonneLiee).eq('modifPersonneLiee')) {
formFieldsPers2['modifPersonneLieeCategorie_modifie4']                                     = true;
}
if (Value('id').of(personneLiee7.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers2['modifPersonneLieeCategorie_partant4']                                     = true;
formFieldsPers2['modifPersonneLieeCategorie_partant4Bis']      = true;
}
if (objet.cadreObjet22P.declarationIndivisaire or not Value('id').of(personneLiee7.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers2['modifPersonneLIeeNomNaissance4']                                         = personneLiee7.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
formFieldsPers2['modifPersonneLIeeNomUsage4']                                             = personneLiee7.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? personneLiee7.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null : '';
var prenoms=[];
for ( i = 0; i < personneLiee7.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee7.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers2['modifPersonneLIeePrenoms4']                                      = prenoms.toString();
formFieldsPers2['modifPersonneLIeeDomicile_voie4']                                        = (personneLiee7.cadreAdressePersonnePouvoir7.rueNumeroAdressePersonnePouvoir != null ? personneLiee7.cadreAdressePersonnePouvoir7.rueNumeroAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee7.cadreAdressePersonnePouvoir7.indiceVoiePersonnePouvoir != null ? personneLiee7.cadreAdressePersonnePouvoir7.indiceVoiePersonnePouvoir : '')
																					 + ' ' + (personneLiee7.cadreAdressePersonnePouvoir7.typeVoiePersonnePouvoir != null ? personneLiee7.cadreAdressePersonnePouvoir7.typeVoiePersonnePouvoir : '')
																					 + ' ' + personneLiee7.cadreAdressePersonnePouvoir7.nomVoiePersonnePouvoir
																					 + ' ' + (personneLiee7.cadreAdressePersonnePouvoir7.rueComplementAdressePersonnePouvoir != null ? personneLiee7.cadreAdressePersonnePouvoir7.rueComplementAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee7.cadreAdressePersonnePouvoir7.distriutionSpecialePersonnePouvoir != null ? personneLiee7.cadreAdressePersonnePouvoir7.distriutionSpecialePersonnePouvoir : '');
formFieldsPers2['modifPersonneLIeeDomicile_codePostal4']                                  = personneLiee7.cadreAdressePersonnePouvoir7.personneLieeAdresseCodePostalPersonnePouvoir;
formFieldsPers2['modifPersonneLIeeDomicile_commune4']                                     = personneLiee7.cadreAdressePersonnePouvoir7.personneLieeAdresseCommunepersonnePouvoir;
if (personneLiee7.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir !== null) {
    var dateTmp = new Date(parseInt(personneLiee7.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['modifPersonneLIeeDateNaissance4']          = date;
}
formFieldsPers2['modifPersonneLIeeLieuNaissanceDepartement4']                              = personneLiee7.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir != null ? personneLiee7.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir.getId() : '';
formFieldsPers2['modifPersonneLIeeLieuNaissanceCommune4']                                  = personneLiee7.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir != null ? personneLiee7.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir : 
																						(personneLiee7.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir != null ? (personneLiee7.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir + ' / ' + personneLiee7.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir) : '');
formFieldsPers2['modifPersonneLIeeNationalite4']                                           = personneLiee7.personneLieePersonnePhysiqueNationalitePersonnePouvoir != null ? personneLiee7.personneLieePersonnePhysiqueNationalitePersonnePouvoir : '';
}
if (Value('id').of(personneLiee7.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers2['modifPersonneLieeNomNaissance_partant4']                                          = personneLiee7.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
var prenoms=[];
for ( i = 0; i < personneLiee7.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee7.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers2['modifPersonneLieePrenom_partant4']                                      = prenoms.toString();
}
}

// Personne Liée 8
if (personneLiee7.autreDeclarationPersonneLiee) {
if(Value('id').of(personneLiee8.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement')) {
formFieldsPers2['qualitePersonneLiee5']                              = "Fondé de pouvoir";
} else {
formFieldsPers2['qualitePersonneLiee5']                              = "Propriétaire indivis";
}
if (objet.cadreObjet22P.declarationIndivisaire or etablissementNew.ajoutFondePouvoir or objet.domicileEntreprise or Value('id').of(personneLiee8.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
formFieldsPers2['modifPersonneLieeCategorie_nouveau5']                                     = true;
}
if (Value('id').of(personneLiee8.objetModifPersonneLiee).eq('modifPersonneLiee')) {
formFieldsPers2['modifPersonneLieeCategorie_modifie5']                                     = true;
}
if (Value('id').of(personneLiee8.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers2['modifPersonneLieeCategorie_partant5']                                     = true;
formFieldsPers2['modifPersonneLieeCategorie_partant5Bis']      = true;
}
if (objet.cadreObjet22P.declarationIndivisaire or not Value('id').of(personneLiee8.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers2['modifPersonneLIeeNomNaissance5']                                         = personneLiee8.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
formFieldsPers2['modifPersonneLIeeNomUsage5']                                             = personneLiee8.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? personneLiee8.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null : '';
var prenoms=[];
for ( i = 0; i < personneLiee8.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee8.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers2['modifPersonneLIeePrenoms5']                                      = prenoms.toString();
formFieldsPers2['modifPersonneLIeeDomicile_voie5']                                        = (personneLiee8.cadreAdressePersonnePouvoir8.rueNumeroAdressePersonnePouvoir != null ? personneLiee8.cadreAdressePersonnePouvoir8.rueNumeroAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee8.cadreAdressePersonnePouvoir8.indiceVoiePersonnePouvoir != null ? personneLiee8.cadreAdressePersonnePouvoir8.indiceVoiePersonnePouvoir : '')
																					 + ' ' + (personneLiee8.cadreAdressePersonnePouvoir8.typeVoiePersonnePouvoir != null ? personneLiee8.cadreAdressePersonnePouvoir8.typeVoiePersonnePouvoir : '')
																					 + ' ' + personneLiee8.cadreAdressePersonnePouvoir8.nomVoiePersonnePouvoir
																					 + ' ' + (personneLiee8.cadreAdressePersonnePouvoir8.rueComplementAdressePersonnePouvoir != null ? personneLiee8.cadreAdressePersonnePouvoir8.rueComplementAdressePersonnePouvoir : '')
																					 + ' ' + (personneLiee8.cadreAdressePersonnePouvoir8.distriutionSpecialePersonnePouvoir != null ? personneLiee8.cadreAdressePersonnePouvoir8.distriutionSpecialePersonnePouvoir : '');
formFieldsPers2['modifPersonneLIeeDomicile_codePostal5']                                  = personneLiee8.cadreAdressePersonnePouvoir8.personneLieeAdresseCodePostalPersonnePouvoir;
formFieldsPers2['modifPersonneLIeeDomicile_commune5']                                     = personneLiee8.cadreAdressePersonnePouvoir8.personneLieeAdresseCommunepersonnePouvoir;
if (personneLiee8.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir !== null) {
    var dateTmp = new Date(parseInt(personneLiee8.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFieldsPers2['modifPersonneLIeeDateNaissance5']          = date;
}
formFieldsPers2['modifPersonneLIeeLieuNaissanceDepartement5']                              = personneLiee8.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir != null ? personneLiee8.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir.getId() : '';
formFieldsPers2['modifPersonneLIeeLieuNaissanceCommune5']                                  = personneLiee8.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir != null ? personneLiee8.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir : 
																						(personneLiee8.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir != null ? (personneLiee8.personneLieePersonnePhysiqueLieuNaissanceVillePersonnePouvoir + ' / ' + personneLiee8.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir) : '');
formFieldsPers2['modifPersonneLIeeNationalite5']                                           = personneLiee8.personneLieePersonnePhysiqueNationalitePersonnePouvoir != null ? personneLiee8.personneLieePersonnePhysiqueNationalitePersonnePouvoir : '';
}
if (Value('id').of(personneLiee8.objetModifPersonneLiee).eq('partantPersonneLiee')) {
formFieldsPers2['modifPersonneLieeNomNaissance_partant5']                                          = personneLiee8.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
var prenoms=[];
for ( i = 0; i < personneLiee8.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee8.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}                            
formFieldsPers2['modifPersonneLieePrenom_partant5']                                      = prenoms.toString();
}
}
}

// Cadre 10 - Déclaration relative au lieu d'exercice ou à l'établissement 

var activiteInfo = $p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite;
var fondsDonne = $p2cmb.cadreFondsDonneLocationGeranceGroup.cadreFondsDonneLocationGerance;
formFields['modifConcerneActivite']                                      = (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite') 
																		or activiteInfo.modifActiviteTransfert
																		or activiteInfo.modifActiviteReprise 				
																		or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P') 
																		or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('cessationActivite')
																		or objet.cadreObjetModificationEtablissement.cadreObjet21P.activiteReprise
																		or objet.cadreObjetModificationEtablissement.cadreObjet63P.activiteAcquisition
																		or objet.cadreObjetModificationEtablissement.cadreObjet64P.activiteRenouvellement) ? true : false;
formFields['modifConcerneTransfert']                                     = (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
																		or objet.domicileEntreprise
																		or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise) ? true : false;
formFields['modifConcerneOuverture']                                     = Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') ? true : false;
formFields['modifConcerneFermeture']                                     = Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P') ? true : false;
formFields['modifConcerneGeranceMandat']                                 = ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('miseEnLocation')
																		or Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationPartielle')
																		or Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationTotale')
																		or Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationCessation')
																		or Value('id').of(etablissement.destinationEtablissement2).eq('miseEnLocationPartielle')
																		or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P')
																		or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P')
																		or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P')
																		or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P'))
																		and Value('id').of(fondsDonne.fondsDonneType).eq('fondsDonneGerance'))	? true : false;
formFields['modifConcerneLocationGerance']                              = ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('miseEnLocation')
																		or Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationPartielle')
																		or Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationTotale')
																		or Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationCessation')
																		or Value('id').of(etablissement.destinationEtablissement2).eq('miseEnLocationPartielle')
																		or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P')
																		or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P')
																		or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P')
																		or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P'))
																		and Value('id').of(fondsDonne.fondsDonneType).eq('fondsDonneLocation')) ? true : false;
formFields['modifConcerneAutre']                                        = (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P') 
																		or Value('id').of(objet.objetModification).contains('dateActivite')
																		or objet.cadreObjetModificationEtablissement.cadreObjet63P.enseigneAcquisition
																		or objet.cadreObjetModificationEtablissement.cadreObjet64P.enseigneRenouvellement
																		or objet.cadreObjetModificationEtablissement.cadreObjet68P.enseigneChangement
																		or activiteInfo.modifEtablissementEnseigne) ? true : false;

// Cadre 11 - Etablissement transféré ou fermé

var adresseEntreprise = $p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.adresseEtablissementPrincipal;

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P') 
	or objet.domicileEntreprise
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('cessationActivite')) {
if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P')) and etablissement.modifDateAdresseEntreprise !== null) {
    var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementTransfererFermer']          = date;
} else if (objet.domicileEntreprise and modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementTransfererFermer']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('cessationActivite') and objet.cadreObjetModificationEtablissement.cadreObjetcessationActivite.dateCessationActivite !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjetcessationActivite.dateCessationActivite.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementTransfererFermer']          = date;
}

formFields['modifCategarieEtablissementTransfererFermer_principal']      = (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') or objet.domicileEntreprise or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('cessationActivite')) ? true : false;
formFields['modifCategarieEtablissementTransfererFermer_secondaire']     = (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire') or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).contains('80P'))? true : false;

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')) or objet.domicileEntreprise or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('cessationActivite')) {
formFields['modifAncienneAdresseEtablissement_adresseVoie']             = (adresseEntreprise.etablissementAdresseNumeroVoie != null ? adresseEntreprise.etablissementAdresseNumeroVoie : '')
																		+ ' ' + (adresseEntreprise.etablissementAdresseIndiceVoie != null ? adresseEntreprise.etablissementAdresseIndiceVoie : '')
																		+ ' ' + (adresseEntreprise.etablissementAdresseTypeVoie != null ? adresseEntreprise.etablissementAdresseTypeVoie : '')
																		+ ' ' + (adresseEntreprise.etablissementAdresseNomVoie != null ? adresseEntreprise.etablissementAdresseNomVoie : '');
formFields['modifAncienneAdresseEtablissement_adresseComplement']        = (adresseEntreprise.etablissementAdresseComplementVoie != null ? adresseEntreprise.etablissementAdresseComplementVoie : '')
																		+ ' ' + (adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null ? adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie : '');
formFields['modifAncienneAdresseEtablissement_codePostal']               = adresseEntreprise.etablissementAdresseCodePostal;
formFields['modifAncienneAdresseEtablisseement_commune']          = adresseEntreprise.etablissementAdresseCommune;
} else if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')) or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P')) {
formFields['modifAncienneAdresseEtablissement_adresseVoie']             = (etablissement.adresseEtablissementOld.etablissementAdresseNumeroVoieOld != null ? etablissement.adresseEtablissementOld.etablissementAdresseNumeroVoieOld : '') 
																		+ ' ' + (etablissement.adresseEtablissementOld.etablissementAdresseIndiceVoieOld != null ? etablissement.adresseEtablissementOld.etablissementAdresseIndiceVoieOld : '')
																		+ ' ' + (etablissement.adresseEtablissementOld.etablissementAdresseTypeVoieOld != null ? etablissement.adresseEtablissementOld.etablissementAdresseTypeVoieOld : '')
																		+ ' ' + (etablissement.adresseEtablissementOld.etablissementAdresseNomVoieOld != null ? etablissement.adresseEtablissementOld.etablissementAdresseNomVoieOld : '');
formFields['modifAncienneAdresseEtablissement_adresseComplement']       = (etablissement.adresseEtablissementOld.etablissementAdresseComplementVoieOld != null ? etablissement.adresseEtablissementOld.etablissementAdresseComplementVoieOld : '')
																		+ ' ' + (etablissement.adresseEtablissementOld.etablissementAdresseDistriutionSpecialeVoieOld != null ? etablissement.adresseEtablissementOld.etablissementAdresseDistriutionSpecialeVoieOld : '');
formFields['modifAncienneAdresseEtablissement_codePostal']               = etablissement.adresseEtablissementOld.etablissementAdresseCodePostalOld;
formFields['modifAncienneAdresseEtablisseement_commune']          = etablissement.adresseEtablissementOld.etablissementAdresseCommuneOld;
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') or objet.domicileEntreprise) {
formFields['modifDestinationEtablissement_vendu']               = (Value('id').of(etablissement.destinationEtablissement1).eq('vendu') or Value('id').of(etablissement.destinationEtablissement2).eq('vendu')) ? true : false;
formFields['modifDestinationEtablissement_ferme1']               = (Value('id').of(etablissement.destinationEtablissement1).eq('ferme') or Value('id').of(etablissement.destinationEtablissement2).eq('ferme')  or objet.domicileEntreprise) ? true : false;
formFields['modifDestinationEtablissement_autre1']               = (Value('id').of(etablissement.destinationEtablissement1).eq('autre') 
																 or Value('id').of(etablissement.destinationEtablissement2).eq('autre') 
																 or Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationPartielle') 
																 or Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationTotale') 
																 or Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationCessation')
																 or Value('id').of(etablissement.destinationEtablissement2).eq('miseEnLocationPartielle')) ? true : false;
formFields['modifDestinationEtablissementAutre1_libelle']        = (Value('id').of(etablissement.destinationEtablissement1).eq('autre') or Value('id').of(etablissement.destinationEtablissement2).eq('autre')) ? etablissement.destinationAutre : 
																((Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationTotale') 
																 or Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationCessation')
																 or Value('id').of(fondsDonne.fondsDonneType).eq('fondsDonneLocation')) ? "Mise en location-gérance" :
																 (Value('id').of(fondsDonne.fondsDonneType).eq('fondsDonneGerance') ? "Mise en gérance-mandat" : ''));
formFields['modifDestinationEtablissement_devientPrincipal']             = Value('id').of(etablissement.destinationEtablissement2).eq('devientPrincipal') ? true : false;
formFields['modifDestinationEtablissement_devientSecondaire']            = Value('id').of(etablissement.destinationEtablissement1).eq('devientSecondaire') ? true : false;
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P') or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('cessationActivite')) {
formFields['modifDestinationEtablissement_supprime']               = Value('id').of(etablissement.destinationEtablissement3).eq('supprime') ? true : false;
formFields['modifDestinationEtablissement_vendu2']                   = Value('id').of(etablissement.destinationEtablissement3).eq('vendu') ? true : false;
formFields['modifDestinationEtablissement_autre2']                   = Value('id').of(etablissement.destinationEtablissement3).eq('autre') ? true : false;
formFields['modifDestinationEtablissementAutre2_libelle']            = Value('id').of(etablissement.destinationEtablissement3).eq('autre') ? etablissement.destinationAutre : '';
if (etablissement.dateCessationEmploi !== null) {
    var dateTmp = new Date(parseInt(etablissement.dateCessationEmploi.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDestinationEtablissementDateFinEmploiSalarie']          = date;
}
}
}

// Cadre 12 - Etablissement créé ou modifié

var activite = $p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite;

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
	or objet.domicileEntreprise
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')
	or (Value('id').of(objet.objetModification).contains('dateActivite') and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 !== null or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 !== null))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P')
	or ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')))) {
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissement.modifDateAdresseEntreprise !== null) {
    var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
} else if (objet.domicileEntreprise and modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') and etablissementNew.modifDateAdresseOuverture !== null) {
    var dateTmp = new Date(parseInt(etablissementNew.modifDateAdresseOuverture.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite') and activiteInfo.cadreActivite.modifDateActivite !== null) {
    var dateTmp = new Date(parseInt(activiteInfo.cadreActivite.modifDateActivite.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
}  else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P') and activiteInfo.cadreEtablissementIdentification.modifDateIdentification !== null) {
    var dateTmp = new Date(parseInt(activiteInfo.cadreEtablissementIdentification.modifDateIdentification.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
} else if (Value('id').of(objet.objetModification).contains('dateActivite') and objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
}  else if (Value('id').of(objet.objetModification).contains('dateActivite') and objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
}  else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P') and objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') and etablissementNew.modifDateAdresseReprise !== null) {
    var dateTmp = new Date(parseInt(etablissementNew.modifDateAdresseReprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P') and objet.cadreObjetModificationEtablissement.cadreObjet63P.dateAcquisitionFonds !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet63P.dateAcquisitionFonds.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P') and objet.cadreObjetModificationEtablissement.cadreObjet64P.dateRenouvellementContrat !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet64P.dateRenouvellementContrat.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
}  else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P') and objet.cadreObjetModificationEtablissement.cadreObjet68P.dateLocataireChangement !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet68P.dateLocataireChangement.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
} else if (Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P') or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')) {
	var dateTmp = new Date(parseInt($p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.modifDatePersonneLiee.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;			
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P') and objet.cadreObjetModificationEtablissement.cadreObjet68P.dateLocataireChangement !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet68P.dateLocataireChangement.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
}
 
																									  
																						  
																						   
																						   
																						   
																							 
																										  
																																																									
																																											 
																					
																				   
																												   
																																															
																																			
																																		
																																	   
																																																   
																																									   
																														   
																														
if (objet.domicileEntreprise) {
formFields['modifNouvelleAdresseEtablissement_voie']                    = (newAdresseDom.personneLieeAdresseVoieNew != null ? newAdresseDom.personneLieeAdresseVoieNew : '') 
																		+ ' ' + (newAdresseDom.personneLieeAdresseIndiceVoieNew != null ? newAdresseDom.personneLieeAdresseIndiceVoieNew : '')
																		+ ' ' + (newAdresseDom.personneLieeAdresseTypeVoieNew != null ? newAdresseDom.personneLieeAdresseTypeVoieNew : '')
																		+ ' ' + (newAdresseDom.personneLieeAdresseNomVoieNew != null ? newAdresseDom.personneLieeAdresseNomVoieNew : '');
formFields['modifNouvelleAdresseEtablissement_complementAdresse']		= (newAdresseDom.personneLieeAdresseComplementAdresseNew != null ? newAdresseDom.personneLieeAdresseComplementAdresseNew : '')
																		+ ' ' + (newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null ? newAdresseDom.personneLieeAdresseDistriutionSpecialeNew : '');
formFields['modifNouvelleAdresseEtablissement_codePostal']              = newAdresseDom.personneLieeAdresseCodePostalNew;
formFields['modifNouvelleAdresseEtablisseement_commune']                = newAdresseDom.personneLieeAdresseCommuneNew;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') 
	or objet.	cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise
	or ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P'))  
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	and Value('id').of(objet.cadreObjetPersonneLiee.typeEtablissementLiee).contains('etablissementSecondaire'))) {
formFields['modifNouvelleAdresseEtablissement_voie']                    = (etablissementNew.adresseEtablissementNew.etablissementAdresseNumeroVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseNumeroVoieNew : '') 
																		+ ' ' + (etablissementNew.adresseEtablissementNew.etablissementAdresseIndiceVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseIndiceVoieNew : '')
																		+ ' ' + (etablissementNew.adresseEtablissementNew.etablissementAdresseTypeVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseTypeVoieNew : '')
																		+ ' ' + (etablissementNew.adresseEtablissementNew.etablissementAdresseNomVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseNomVoieNew : '');
formFields['modifNouvelleAdresseEtablissement_complementAdresse']		= (etablissementNew.adresseEtablissementNew.etablissementAdresseComplementVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseComplementVoieNew : '')
																		+ ' ' + (etablissementNew.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew : '');
formFields['modifNouvelleAdresseEtablissement_codePostal']              = etablissementNew.adresseEtablissementNew.etablissementAdresseCodePostalNew;
formFields['modifNouvelleAdresseEtablisseement_commune']                = etablissementNew.adresseEtablissementNew.etablissementAdresseCommuneNew;
} else if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P'))  
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or (Value('id').of(objet.objetModification).contains('dateActivite') and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 != null or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 !== null))
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P') and not objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise)
	or ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	and not Value('id').of(objet.cadreObjetPersonneLiee.typeEtablissementLiee).contains('etablissementSecondaire')))	{
formFields['modifNouvelleAdresseEtablissement_voie']                    = (adresseEntreprise.etablissementAdresseNumeroVoie != null ? adresseEntreprise.etablissementAdresseNumeroVoie : '')
																		+ ' ' + (adresseEntreprise.etablissementAdresseIndiceVoie != null ? adresseEntreprise.etablissementAdresseIndiceVoie : '')
																		+ ' ' + (adresseEntreprise.etablissementAdresseTypeVoie != null ? adresseEntreprise.etablissementAdresseTypeVoie : '')
																		+ ' ' + (adresseEntreprise.etablissementAdresseNomVoie != null ? adresseEntreprise.etablissementAdresseNomVoie : '');
formFields['modifNouvelleAdresseEtablissement_complementAdresse']       =(adresseEntreprise.etablissementAdresseComplementVoie != null ? adresseEntreprise.etablissementAdresseComplementVoie : '')
																		+ ' ' + (adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null ? adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie : '');
formFields['modifNouvelleAdresseEtablissement_codePostal']              = adresseEntreprise.etablissementAdresseCodePostal;
formFields['modifNouvelleAdresseEtablisseement_commune']                = adresseEntreprise.etablissementAdresseCommune;
}

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and not Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementOuverture')
	and not Value('id').of(etablissementNew.origineEtablissement2).eq('etablissementOuverture')
	and not Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementReprise'))
	or (Value('id').of(objet.objetModification).contains('dateActivite') 
	and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 !== null 
	or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 !== null))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')
	and not objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise)
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P')
	or ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	and Value('id').of(objet.cadreObjetPersonneLiee.typeEtablissementLiee).contains('etablissementSecondaire'))) {

if (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P') 
	or (Value('id').of(objet.objetModification).contains('dateActivite') 
	and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 != null 
	or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 !== null))
	or ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	and not Value('id').of(objet.cadreObjetPersonneLiee.typeEtablissementLiee).contains('etablissementSecondaire'))) {
formFields['modifCategarieEtablissementModifier_principal']              = true;
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')
	or ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	and Value('id').of(objet.cadreObjetPersonneLiee.typeEtablissementLiee).contains('etablissementSecondaire'))) {
formFields['modifCategarieEtablissementModifier_secondaire']             = true;
}
}

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') 
	and (Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementOuverture')
	or Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementReprise')))
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise 
	or objet.domicileEntreprise
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))) {
formFields['modifCategarieEtablissementOuvert_principal']                = true;
}
if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire') 
	and Value('id').of(etablissementNew.origineEtablissement2).eq('etablissementOuverture'))
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')) 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')) {
formFields['modifCategarieEtablissementOuvert_secondaire']               = true;
}
}

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and (Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementOuverture')
	or Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementReprise')
	or Value('id').of(etablissementNew.origineEtablissement2).eq('etablissementOuverture')))
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise 
	or objet.domicileEntreprise
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')) {
formFields['modifCategarieEtablissementOuvert_fondePouvoirOui']                      = (etablissementNew.ajoutFondePouvoir or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir) ? true : false ;
formFields['modifCategarieEtablissementOuvert_fondePouvoirNon']                      = (etablissementNew.ajoutFondePouvoir or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir) ? false : true;
formFields['modifCategarieEtablissementOuvert_effectifNombre']                       = activite.etablissementEffectifSalariePresenceOui ? activite.cadreEffectifSalarie.etablissementEffectifSalarieNombre : "0";
formFields['modifCategarieEtablissementOuvert_effectifTotal']						 = activite.etablissementEffectifSalariePresenceOui ? (activite.cadreEffectifSalarie.totalEffectifSalarieNombre != null ? activite.cadreEffectifSalarie.totalEffectifSalarieNombre : '') : '';	
formFields['modifCategarieEtablissementOuvert_effectifApprentis']                    = activite.etablissementEffectifSalariePresenceOui ? (activite.cadreEffectifSalarie.etablissementEffectifSalarieApprentis != null ? activite.cadreEffectifSalarie.etablissementEffectifSalarieApprentis : '') : '';
formFields['modifCategarieEtablissementOuvert_effectifVRP']                          = activite.etablissementEffectifSalariePresenceOui ? (activite.cadreEffectifSalarie.etablissementEffectifSalarieVRP != null ? activite.cadreEffectifSalarie.etablissementEffectifSalarieVRP : '') : '';
}

// Cadre 13 - Activité

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
	and (Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementOuverture')
	or Value('id').of(etablissementNew.origineEtablissement2).eq('etablissementOuverture')
	or Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementReprise')))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	or objet.domicileEntreprise		    
	or activiteInfo.modifActiviteTransfert
	or activiteInfo.modifActiviteReprise
	or objet.cadreObjetModificationEtablissement.cadreObjet63P.activiteAcquisition
	or objet.cadreObjetModificationEtablissement.cadreObjet64P.activiteRenouvellement) {
					    
if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or activiteInfo.modifActiviteTransfert or activiteInfo.modifActiviteReprise
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.activiteReprise
	or objet.cadreObjetModificationEtablissement.cadreObjet63P.activiteAcquisition
	or objet.cadreObjetModificationEtablissement.cadreObjet64P.activiteRenouvellement)
	and activite.modifDateActivite != null)	{
    var dateTmp = new Date(parseInt(activite.modifDateActivite.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateActivite']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and (Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementOuverture')
	or Value('id').of(etablissementNew.origineEtablissement2).eq('etablissementOuverture')) and etablissement.modifDateAdresseEntreprise !== null) {
    var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateActivite']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') and etablissementNew.modifDateAdresseOuverture !== null) {
    var dateTmp = new Date(parseInt(etablissementNew.modifDateAdresseOuverture.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateActivite']          = date;
} else if (objet.domicileEntreprise and modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateActivite']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P') and not objet.cadreObjetModificationEtablissement.cadreObjet21P.activiteReprise and objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateActivite']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') and not activiteInfo.modifActiviteReprise and etablissementNew.modifDateAdresseReprise !== null) {
    var dateTmp = new Date(parseInt(etablissementNew.modifDateAdresseReprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateActivite']          = date;
} 

formFields['etablissement_activites']                                    = activite.etablissementActivitesAutres;
if (activite.etablissementActivitesPrincipales2 != null) {
formFields['etablissement_activitePlusImportante']                       = activite.etablissementActivitesPrincipales2;
}
formFields['entreprise_activitePermanenteSaisonniere_permanente']        = Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonnierePermanente') ? true : false;
formFields['entreprise_activitePermanenteSaisonniere_saisonniere']       = Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonniereSaisonniere') ? true : false;
formFields['etablissment_nonSedentariteQualite_nonSedentaire']           = activite.etablissementNonSedentariteQualiteNonSedentaire ? true : false;
formFields['etablissement_activiteLieuExercice_magasin']                 = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMagasin') ? true : false;
formFields['etablissement_activiteLieuExerciceMagasin']                  = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMagasin') ? activite.etablissementActiviteLieuExerciceMagasinSurface : '';
formFields['etablissement_activiteLieuExercice_marche']                  = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMarche') ? true : false;
formFields['etablissement_activiteNature_commerceDetail']                = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceDetail') ? true : false;
formFields['etablissement_activiteNature_commerceGros']                  = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceGros') ? true : false;
formFields['etablissement_activiteNature_batTravauxPublics']             = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureBatTravauxPublics') ? true : false;
formFields['etablissement_activiteNature_fabricationProduction']         = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureFabricationProduction') ? true : false;
formFields['etablissement_activiteNature_cocheAutre']                    = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCocheAutre') ? true : false;
formFields['etablissement_activiteNature_autre']                         = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCocheAutre') ? activite.etablissementActiviteNatureAutre : '';
formFields['activiteDevientPrincipale_oui']                              = activite.activiteDevientPrincipale ? true : false ;
formFields['activiteDevientPrincipale_non']                              = activite.activiteDevientPrincipale ? false : true;

if (Value('id').of(activite.activiteEvenement).eq('61P') or Value('id').of(activite.activiteEvenement).eq('61P62P') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') and activiteInfo.modifActiviteReprise)) {
formFields['modifActiviteSuiteAdjonction']                                           = true;
}
if (Value('id').of(activite.activiteEvenement).eq('62P') or Value('id').of(activite.activiteEvenement).eq('61P62P')) { 
formFields['modifActiviteSuiteSuppression']                                          = true;
formFields['modifActiviteSuiteSuppression_disparition']                              = Value('id').of(activite.suppressionActivitePar).eq('suppressionParDisparition') ? true : false;
formFields['modifActiviteSuiteSuppression_vente']                                    = Value('id').of(activite.suppressionActivitePar).eq('suppressionParVente') ? true : false;
formFields['modifActiviteSuiteSuppression_reprise']                                  = Value('id').of(activite.suppressionActivitePar).eq('suppressionParReprise') ? true : false;
formFields['modifActiviteSuiteSuppression_autre']                                    = Value('id').of(activite.suppressionActivitePar).eq('suppressionParAutre') ? true : false;
formFields['modifActiviteSuiteSuppression_autreLibelle']                             = Value('id').of(activite.suppressionActivitePar).eq('suppressionParAutre') ? activite.suppressionActiviteAutre : '';
}
}

// Cadres 14 - Enseigne

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P') 
	or activiteInfo.modifEtablissementEnseigne 
	or objet.cadreObjetModificationEtablissement.cadreObjet68P.enseigneChangement
	or objet.cadreObjetModificationEtablissement.cadreObjet63P.enseigneAcquisition
	or objet.cadreObjetModificationEtablissement.cadreObjet64P.enseigneRenouvellement) {
if (activiteInfo.cadreEtablissementIdentification.modifDateIdentification !== null) {
    var dateTmp = new Date(parseInt(activiteInfo.cadreEtablissementIdentification.modifDateIdentification.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateNomEtablissement']          = date;
}  
formFields['etablissement_enseigne']                                     = Value('id').of(activiteInfo.cadreEtablissementIdentification.modifNomEnseigne).contains('modifEnseigne') ? activiteInfo.cadreEtablissementIdentification.modifEnseigneNew : '';
formFields['etablissement_nomCommercialProfessionnel']                   = Value('id').of(activiteInfo.cadreEtablissementIdentification.modifNomEnseigne).contains('modifNomCommercial') ? activiteInfo.cadreEtablissementIdentification.modifNomCommercialNew : '';
} else if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') or objet.domicileEntreprise
	or Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementOuverture')
	or Value('id').of(etablissementNew.origineEtablissement2).eq('etablissementOuverture')
	or Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementOuverture')) 
	and (activite.modifEnseigneBis != null or activite.modifNomCommercialBis != null)) {
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissement.modifDateAdresseEntreprise !== null) {
    var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateNomEtablissement']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') and etablissementNew.modifDateAdresseOuverture !== null) {
    var dateTmp = new Date(parseInt(etablissementNew.modifDateAdresseOuverture.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateNomEtablissement']          = date;
} else if (Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementOuverture') and objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateNomEtablissement']          = date;
} else if (objet.domicileEntreprise and modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateNomEtablissement']          = date;
} 
formFields['etablissement_enseigne']                                     = activite.modifEnseigneBis != null ? activite.modifEnseigneBis : '';
formFields['etablissement_nomCommercialProfessionnel']                   = activite.modifNomCommercialBis != null ? activite.modifNomCommercialBis : '';;
} 


// Cadre 15 - origine de l'activité

var origine = $p2cmb.cadre7EtablissementActiviteGroup.cadre4OrigineFonds ;

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
	and not Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementExistant')
	and not Value('id').of(etablissementNew.origineEtablissement2).eq('etablissementExistant'))
	or Value('id').of(activite.activiteEvenement).eq('61P')
	or Value('id').of(activite.activiteEvenement).eq('61P62P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P')
	or objet.cadreObjetModificationEtablissement.cadreObjet68P.renouvellementChangement
	or objet.domicileEntreprise) {
if (Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementReprise')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P')) {		
formFields['etablissement_origineFonds_autre']                           = true;
formFields['etablissement_origineFonds_autreLibelle']                    = "Reprise de l'exploitation du fonds mis en location-gérance";
} else if (Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementAcquisition')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P')) {		
formFields['etablissement_origineFonds_autre']                           = true;
formFields['etablissement_origineFonds_autreLibelle']                    = "Acquisition du fonds par l'exploitant";
} else if (Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementRenouvellement')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P')
	or objet.cadreObjetModificationEtablissement.cadreObjet68P.renouvellementChangement) {		
formFields['etablissement_origineFonds_autre']                           = true;
formFields['etablissement_origineFonds_autreLibelle']                    = "Renouvellement du contrat de location-gérance";
} else {		
formFields['etablissement_origineFonds_creation']                        = (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation') or objet.domicileEntreprise) ? true : false;
formFields['etablissement_origineFonds_achat']                           = Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsAchat') ? true : false;
formFields['etablissement_origineFonds_locationGerance']                 = Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsLocationGerance') ? true : false;
formFields['etablissement_origineFonds_geranceMandat']                   = Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsGeranceMandat') ? true : false;
formFields['etablissement_origineFonds_autre']                           = (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCocheAutre') 
																			or Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsDonation')
																			or Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsDevolution'))? true : false;
formFields['etablissement_origineFonds_autreLibelle']                    =  Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsDonation') ? "Donationtion" :
																			(Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsDevolution') ? "Dévolution successorale" : 
																			(Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCocheAutre') ? activite.etablissementOrigineFondsAutre : ''));
}

if ((Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsAchat') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P'))
	or Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementReprise')
	or Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementReprise')) {
formFields['entrepriseLiee_entreprisePP_nom_precedentExploitant']        = Value('id').of(origine.precedentExploitant.typePersonnePrecedentExploitant).eq('typePersonnePrecedentExploitantPP') ? origine.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant : origine.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant;
formFields['entrepriseLiee_entreprisePP_nomUsage_precedentExploitant']   = (Value('id').of(origine.precedentExploitant.typePersonnePrecedentExploitant).eq('typePersonnePrecedentExploitantPP') and origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant != null) ? origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant : '';
formFields['entrepriseLiee_entreprisePP_prenom1_precedentExploitant']    = Value('id').of(origine.precedentExploitant.typePersonnePrecedentExploitant).eq('typePersonnePrecedentExploitantPP') ? origine.precedentExploitant.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant : '';
formFields['denomination_precedentExploitant']                           = origine.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant;
formFields['entrepriseLiee_siren_precedentExploitant']                   = origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant.split(' ').join('');
}

if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsLocationGerance')
	or Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsGeranceMandat')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P')
	or objet.cadreObjetModificationEtablissement.cadreObjet68P.renouvellementChangement) {
if(origine.geranceMandat.etablissementLoueurMandantDuFondsDebutContrat != null) {
	var dateTmp = new Date(parseInt(origine.geranceMandat.etablissementLoueurMandantDuFondsDebutContrat.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_loueurMandantDuFondsDebutContrat'] = date;
}
if(origine.geranceMandat.etablissementLoueurMandantDuFondsFinContrat != null) {
	var dateTmp = new Date(parseInt(origine.geranceMandat.etablissementLoueurMandantDuFondsFinContrat.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_loueurMandantDuFondsFinContrat'] = date;
}
formFields['etablissement_loueurMandantDuFondsRenouvellementTaciteReconduction_oui']    = origine.geranceMandat.etablissementLoueurMandantDuFondsRenouvellementTaciteReconductionOui ? true : false;
formFields['etablissement_loueurMandantDuFondsRenouvellementTaciteReconduction_non']    = origine.geranceMandat.etablissementLoueurMandantDuFondsRenouvellementTaciteReconductionOui ? false : true;
formFields['entrepriseLiee_entreprisePM_nom_loueurMandantDuFonds']                      = origine.geranceMandat.entrepriseLieeEntreprisePMDenominationLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeEntreprisePMDenominationLoueurMandantDuFonds : (origine.geranceMandat.entrepriseLieeEntreprisePMNomNaissanceLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeEntreprisePMNomNaissanceLoueurMandantDuFonds : '');
formFields['entrepriseLiee_entreprisePP_nomUsage_loueurMandantDuFonds']                 = origine.geranceMandat.entrepriseLieeEntreprisePPNomUsageLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeEntreprisePPNomUsageLoueurMandantDuFonds : '';
formFields['entrepriseLiee_entreprisePP_prenom1_loueurMandantDuFonds']                  = origine.geranceMandat.entrepriseLieeEntreprisePPPrenom1LoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeEntreprisePPPrenom1LoueurMandantDuFonds : '';

formFields['entrepriseLiee_adresse_nomVoie_loueurMandantDuFonds']                       = (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.numRueAdresseLoueurMandantDufonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.numRueAdresseLoueurMandantDufonds : '') 
																						+ ' ' + (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.indiceVoieLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.indiceVoieLoueurMandantDuFonds : '') 
																						+ ' ' + (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.typeVoieLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.typeVoieLoueurMandantDuFonds : '') 
																						+ ' ' + (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.nomVoieLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.nomVoieLoueurMandantDuFonds : '')                                                                                                                              
																				        + ' ' + (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.rueComplementAdresseLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.rueComplementAdresseLoueurMandantDuFonds : '') 
																						+ ' ' + (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.distributionSpecialeLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.distributionSpecialeLoueurMandantDuFonds : '');
formFields['entrepriseLiee_adresse_codePostal_loueurMandantDuFonds']                    = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.codePostalLoueurMandantDuFonds;
formFields['entrepriseLiee_adresse_commune_loueurMandantDuFonds']                       = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.communeLoueurMandantDuFonds;
}

if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsGeranceMandat')
	or Value('id').of(origine.typeFonds).eq('typeFondsGerance')) {
formFields['entrepriseLiee_siren_geranceMandat']                                        = origine.geranceMandat.entrepriseLieeSirenGeranceMandat != null ? origine.geranceMandat.entrepriseLieeSirenGeranceMandat.split(' ').join('') : '';
formFields['entrepriseLiee_greffeImmatriculation']                                      = origine.geranceMandat.entrepriseLieeGreffeImmatriculation;
}

if (Value('id').of(origine.etablisementOrigineFonds).eq('etablissement_origineFonds_achat')) {  
if(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParution != null) {
	var dateTmp = new Date(parseInt(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParution.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_journalAnnoncesLegalesDateParution'] = date;
}
formFields['etablissement_journalAnnoncesLegalesNom']                                   = origine.precedentExploitant.etablissementJournalAnnoncesLegalesNom;
} else if (Value('id').of(etablissementNew.origineAcquision).eq('achat')) {  
if(etablissementNew.acquisitionJournalAnnoncesLegalesDateParution != null) {
	var dateTmp = new Date(parseInt(etablissementNew.acquisitionJournalAnnoncesLegalesDateParution.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_journalAnnoncesLegalesDateParution'] = date;
}
formFields['etablissement_journalAnnoncesLegalesNom']                                   = etablissementNew.acquisitionJournalAnnoncesLegalesNom;
}
}

// Cadre 16 - Fonds donné en location-gérance

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('miseEnLocation')
	or Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationPartielle')
	or Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationTotale')
	or Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationCessation')
	or Value('id').of(etablissement.destinationEtablissement2).eq('miseEnLocationPartielle')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P')
	or objet.cadreObjetModificationEtablissement.cadreObjet64P.changementRenouvellement) {
if(Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('miseEnLocation') and fondsDonne.dateMiseEnLocation != null) {
	var dateTmp = new Date(parseInt(fondsDonne.dateMiseEnLocation.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['modifDateMiseEnGerance'] = date;
} else if(not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('miseEnLocation') and etablissement.modifDateAdresseEntreprise != null) {
	var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['modifDateMiseEnGerance'] = date;
} else if(not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P') and objet.cadreObjetModificationEtablissement.cadreObjet68P.dateLocataireChangement != null) {
	var dateTmp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet68P.dateLocataireChangement.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['modifDateMiseEnGerance'] = date;
} else if(objet.cadreObjetModificationEtablissement.cadreObjet64P.changementRenouvellement and objet.cadreObjetModificationEtablissement.cadreObjet64P.dateRenouvellementContrat != null) {
	var dateTmp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet64P.dateRenouvellementContrat.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['modifDateMiseEnGerance'] = date;
} 

if ((Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetmiseEnLocation.infosMiseEnLocation).eq('82P') 
	or Value('id').of($p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEtablissement.objetEtablissement).eq('68P')
	or $p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEtablissement.cadreObjet64P.changementRenouvellement)
	 and Value('id').of(fondsDonne.fondsDonneEtablissement).eq('fondsDonneSecondaire')) {
formFields['modifMiseEnGerance_voie']                                                = (fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseNumeroVoie != null ? fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseNumeroVoie : '')
																					+ ' ' + (fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseIndiceVoie != null ? fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseIndiceVoie : '')
																					+ ' ' + (fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseTypeVoie != null ? fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseTypeVoie : '')
																					+ ' ' + fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseNomVoie
																					+ ' ' + (fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseComplementVoie != null ? fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseComplementVoie : '')
																					+ ' ' + (fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseDistriutionSpecialeVoie != null ? fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseDistriutionSpecialeVoie : '');
formFields['modifMiseEnGerance_codePostal']                                          = fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseCodePostal;
formFields['modifMiseEnGerance_commune']                                             = fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseCommune;
} else if (Value('id').of(etablissement.destinationEtablissement2).eq('miseEnLocationPartielle')) {	
formFields['modifMiseEnGerance_voie']                                                = (etablissement.adresseEtablissementOld.etablissementAdresseNumeroVoieOld != null ? etablissement.adresseEtablissementOld.etablissementAdresseNumeroVoieOld : '')
																					+ ' ' + (etablissement.adresseEtablissementOld.etablissementAdresseIndiceVoieOld != null ? etablissement.adresseEtablissementOld.etablissementAdresseIndiceVoieOld : '')
																					+ ' ' + (etablissement.adresseEtablissementOld.etablissementAdresseTypeVoieOld != null ? etablissement.adresseEtablissementOld.etablissementAdresseTypeVoieOld : '')
																					+ ' ' + etablissement.adresseEtablissementOld.etablissementAdresseNomVoieOld
																					+ ' ' + (etablissement.adresseEtablissementOld.etablissementAdresseComplementVoieOld != null ? etablissement.adresseEtablissementOld.etablissementAdresseComplementVoieOld : '')
																					+ ' ' + (etablissement.adresseEtablissementOld.etablissementAdresseDistriutionSpecialeVoieOld != null ? etablissement.adresseEtablissementOld.etablissementAdresseDistriutionSpecialeVoieOld : '');
formFields['modifMiseEnGerance_codePostal']                                          = etablissement.adresseEtablissementOld.etablissementAdresseCodePostalOld;
formFields['modifMiseEnGerance_commune']                                             = etablissement.adresseEtablissementOld.etablissementAdresseCommuneOld;
} else {
formFields['modifMiseEnGerance_voie']                                                = (adresseEntreprise.etablissementAdresseNumeroVoie != null ? adresseEntreprise.etablissementAdresseNumeroVoie : '')
																	            	+ ' ' + (adresseEntreprise.etablissementAdresseIndiceVoie != null ? adresseEntreprise.etablissementAdresseIndiceVoie : '')
																	             	+ ' ' + (adresseEntreprise.etablissementAdresseTypeVoie != null ? adresseEntreprise.etablissementAdresseTypeVoie : '')
															            			+ ' ' + (adresseEntreprise.etablissementAdresseNomVoie != null ? adresseEntreprise.etablissementAdresseNomVoie : '')
																					+ ' ' + (adresseEntreprise.etablissementAdresseComplementVoie != null ? adresseEntreprise.etablissementAdresseComplementVoie : '')
																					+ ' ' + (adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null ? adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie : '');
formFields['modifMiseEnGerance_codePostal']              							 = adresseEntreprise.etablissementAdresseCodePostal;
formFields['modifMiseEnGerance_commune']          									 = adresseEntreprise.etablissementAdresseCommune;
}	
formFields['modifNomLocataireGerant']                                                = fondsDonne.fondsDonneInformation.fondsDonneLocataireDenomination != null ? fondsDonne.fondsDonneInformation.fondsDonneLocataireDenomination : 
																					(fondsDonne.fondsDonneInformation.fondsDonneLocataireNomUsage != null ? 
																					(fondsDonne.fondsDonneInformation.fondsDonneLocataireNomUsage + ' ' + "né(e)" + ' ' + fondsDonne.fondsDonneInformation.fondsDonneLocataireNomNaissance + ' ' + fondsDonne.fondsDonneInformation.fondsDonneLocatairePrenom) : 
																					(fondsDonne.fondsDonneInformation.fondsDonneLocataireNomNaissance + ' ' + fondsDonne.fondsDonneInformation.fondsDonneLocatairePrenom));
formFields['modifMiseEnGerance_totaliteFonds']                                       = (Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationTotale')
																					or Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationCessation')
																					or Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetmiseEnLocation.infosMiseEnLocation).eq('83P')
																					or Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetmiseEnLocation.infosMiseEnLocation).eq('84P')) ? true : false;
formFields['modifMiseEnGerance_PartieFonds']                                         = (Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationPartielle')
																					or Value('id').of(etablissement.destinationEtablissement2).eq('miseEnLocationPartielle')
																					or Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetmiseEnLocation.infosMiseEnLocation).eq('82P')) ? true : false;
formFields['modifMiseEnGerance_PartieFondsLibelle']                                  = fondsDonne.fondsDonnePartie != null ? fondsDonne.fondsDonnePartie : '';
formFields['modifMiseEnGerance_etablissementPrincipal']                              = (Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationTotale')
																					or Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationCessation')
																					or Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationCessation')
																					or Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetmiseEnLocation.infosMiseEnLocation).eq('83P')
																					or Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetmiseEnLocation.infosMiseEnLocation).eq('84P')
																					or Value('id').of(fondsDonne.fondsDonneEtablissement).eq('fondsDonnePrincipal')) ? true : false;
formFields['modifMiseEnGerance_etablissementSecondaire']                             = (Value('id').of(etablissement.destinationEtablissement2).eq('miseEnLocationPartielle')
																					or Value('id').of(fondsDonne.fondsDonneEtablissement).eq('fondsDonneSecondaire')) ? true : false ;
formFields['modifMiseEnGerancePresenceSalarie_oui']                                  = fondsDonne.fondsDonneEffectifSalariePresenceOui ? true : (activite.etablissementEffectifSalariePresenceOui ? true : false);
formFields['modifMiseEnGerancePresenceSalarie_non']                                  = fondsDonne.fondsDonneEffectifSalariePresenceOui ? false : (activite.etablissementEffectifSalariePresenceOui ? false : true);

if (Value('id').of(fondsDonne.fondsDonneType).eq('fondsDonneGerance')) {
formFields['modifGerantMandataire_siren']                                            = fondsDonne.fondsDonneInformation.fondsDonneSirenGeranceMandat.split(' ').join('');
formFields['modifGerantMandataire_greffe']                                           = fondsDonne.fondsDonneInformation.fondsDonneGreffeGeranceMandat;
formFields['modifGerantMandataire_adresseVoie']                                      = (fondsDonne.fondsDonneInformation.adresseGerantMandataire.gerantMandataireAdresseNumeroVoie !=  null ? fondsDonne.fondsDonneInformation.adresseGerantMandataire.gerantMandataireAdresseNumeroVoie : '')
																					+ ' ' + (fondsDonne.fondsDonneInformation.adresseGerantMandataire.gerantMandataireAdresseIndiceVoie !=  null ? fondsDonne.fondsDonneInformation.adresseGerantMandataire.gerantMandataireAdresseIndiceVoie : '')
																					+ ' ' + (fondsDonne.fondsDonneInformation.adresseGerantMandataire.gerantMandataireAdresseTypeVoie !=  null ? fondsDonne.fondsDonneInformation.adresseGerantMandataire.gerantMandataireAdresseTypeVoie : '')
																					+ ' ' + fondsDonne.fondsDonneInformation.adresseGerantMandataire.gerantMandataireAdresseNomVoie
																					+ ' ' + (fondsDonne.fondsDonneInformation.adresseGerantMandataire.gerantMandataireAdresseComplementVoie !=  null ? fondsDonne.fondsDonneInformation.adresseGerantMandataire.gerantMandataireAdresseComplementVoie : '')
																					+ ' ' + (fondsDonne.fondsDonneInformation.adresseGerantMandataire.gerantMandataireDistriutionSpecialeVoie !=  null ? fondsDonne.fondsDonneInformation.adresseGerantMandataire.gerantMandataireDistriutionSpecialeVoie : '')																					;
formFields['modifGerantMandataire_adresseCodePostal']                                = fondsDonne.fondsDonneInformation.adresseGerantMandataire.gerantMandataireAdresseCodePostal;
formFields['modifGerantMandataire_adresseCommune']                                   = fondsDonne.fondsDonneInformation.adresseGerantMandataire.gerantMandataireAdresseCommune;
}	
formFields['modifMiseEnGerance_RenouvellementMaintienImmat']                         = false;
formFields['modifMiseEnGerance_maintienImmat']                                       = (Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationTotale')
																						or Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetmiseEnLocation.infosMiseEnLocation).eq('84P')) ? true : false;
formFields['modifMiseEnGerance_radiation']                                           = (Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationCessation')
																						or Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetmiseEnLocation.infosMiseEnLocation).eq('83P')) ? true : false;
}

// Cadre 17 - Observation

var correspondance = $p2cmb.cadre8RensCompGroup.cadre8RensComp;
var signataire = $p2cmb.cadre9SignatureGroup.cadre9Signature;

if (Value('id').of(objet.objetModification).contains('dateActivite') and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 !== null or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 !== null)) {
if (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateObservations']          = date;
} else if (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateObservations']          = date;
}
formFields['observations']                 = "Modification de la date de début d'activité" + ' / ' +  (correspondance.formaliteObservations != null ? correspondance.formaliteObservations : '');
} else if (not Value('id').of(objet.objetModification).contains('dateActivite') and objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 == null and objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 == null and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('cessationActivite')) {	
if (objet.cadreObjetModificationEtablissement.cadreObjetcessationActivite.dateCessationActivite !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjetcessationActivite.dateCessationActivite.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateObservations']          = date;
}	
formFields['observations']                 = (Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetcessationActivite.infosCessation).eq('40P') ? ("Cessation temporaire d'activité. Motif :" + ' ' + objet.cadreObjetModificationEtablissement.cadreObjetcessationActivite.motifCessation) : "Cessation définitive d'activité") + ' ' + (correspondance.formaliteObservations != null ? correspondance.formaliteObservations : '');
} else if (not Value('id').of(objet.objetModification).contains('dateActivite') and objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 == null and objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 == null and not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('cessationActivite')) {	
formFields['observations']                 = correspondance.formaliteObservations != null ? correspondance.formaliteObservations : '';
}


// Cadre 18 - Adresse de correspondance

formFields['adresseCorrespondanceCadre_coche']            = (Value('id').of(correspondance.adresseCorrespond).eq('domi') 
															or Value('id').of(objet.objetModification).contains('dateActivite')
															or Value('id').of(correspondance.adresseCorrespond).eq('fondsDonneEtablissement') 
															or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')
															or Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement') 
															or Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') 
															or (Value('id').of(correspondance.adresseCorrespond).eq('prof') 
															and (objet.domicileEntreprise 
															or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
															and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))))) ? true : false;
formFields['adresseCorrespondanceCadre_numero']           = Value('id').of(correspondance.adresseCorrespond).eq('domi') ? "4" : 
															((Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement') 
															or(Value('id').of(correspondance.adresseCorrespond).eq('prof') 
															and (objet.domicileEntreprise 
															or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
															and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))))) ? "11" :
															((Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') or Value('id').of(objet.objetModification).contains('dateActivite') 
															or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')) ? "12" : 
															(Value('id').of(correspondance.adresseCorrespond).eq('fondsDonneEtablissement') ? "16" : '')));
formFields['adressesCorrespondanceAutre']                 = (Value('id').of(correspondance.adresseCorrespond).eq('autre') 
															or (Value('id').of(correspondance.adresseCorrespond).eq('prof') 
															and not Value('id').of(objet.objetModification).contains('dateActivite')
															and not objet.domicileEntreprise 
															and not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')
															and (not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
															or not Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')))) ? true : false;

if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
formFields['adresseCorrespondance_voie1']                 = correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance
															+ ' ' + (correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance : '')
															+ ' ' + (correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance : '')
															+ ' ' + (correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance : '')
															+ ' ' + (correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance : '');
formFields['adresseCorrespondance_voie2']                 = (correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance != null ? correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance : '')
															+ ' ' + (correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance != null ? correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance : '');
formFields['adresseCorrespondance_codePostal']            = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal;
formFields['adresseCorrespondance_commune']               = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('prof') 
			and not Value('id').of(objet.objetModification).contains('dateActivite')
			and not objet.domicileEntreprise 
			and not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P') 
			and (not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
			or not Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))) {
formFields['adresseCorrespondance_voie1']             = (adresseEntreprise.etablissementAdresseNumeroVoie != null ? adresseEntreprise.etablissementAdresseNumeroVoie : '')
														+ ' ' + (adresseEntreprise.etablissementAdresseIndiceVoie != null ? adresseEntreprise.etablissementAdresseIndiceVoie : '')
														+ ' ' + (adresseEntreprise.etablissementAdresseTypeVoie != null ? adresseEntreprise.etablissementAdresseTypeVoie : '')
														+ ' ' + (adresseEntreprise.etablissementAdresseNomVoie != null ? adresseEntreprise.etablissementAdresseNomVoie : '');
formFields['adresseCorrespondance_voie2']			  = (adresseEntreprise.etablissementAdresseComplementVoie != null ? adresseEntreprise.etablissementAdresseComplementVoie : '')
														+ ' ' + (adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null ? adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie : '');
formFields['adresseCorrespondance_codePostal']        = adresseEntreprise.etablissementAdresseCodePostal;
formFields['adresseCorrespondance_commune']           = adresseEntreprise.etablissementAdresseCommune;
}

formFields['telephone2']                                                             = correspondance.infosSup.formaliteTelephone1 != null ? correspondance.infosSup.formaliteTelephone1 : '';
formFields['telephone1']                                                             = correspondance.infosSup.formaliteTelephone2;
formFields['courriel']                                                               = correspondance.infosSup.formaliteFaxCourriel != null ? correspondance.infosSup.formaliteFaxCourriel : (correspondance.infosSup.telecopie != null ? correspondance.infosSup.telecopie : '');


// Cadre 19 - Diffusion informations

formFields['diffusionInformation']                                                   = Value('id').of(signataire.diffusionInfo).eq('formaliteNonDiffusionInformationOui') ? true : false;
formFields['nonDiffusionInformation']                                                = Value('id').of(signataire.diffusionInfo).eq('formaliteNonDiffusionInformationNon') ? true : false;

// Cadre 20 - Signataire

formFields['signataireDeclarant']                                                    = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
if (Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') or Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteAutre')) {
formFields['signataireMandataire']                                                   = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') ? true : false;
formFields['signataireAutre']                                                       = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteAutre') ? true : false;
formFields['nomPrenomDenominationMandataire']					                     = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFields['signataireAdresse1']                                                      = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '');
formFields['signataireAdresseCP']  													 = signataire.adresseMandataire.dataCodePostalMandataire;
formFields['signataireAdresseCommune']  											 = signataire.adresseMandataire.villeAdresseMandataire;
}
if (signataire.formaliteSignatureDate !== null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['signatureDate']          = date;
}
formFields['signatureLieu']                                                          = signataire.formaliteSignatureLieu;
formFields['estEIRL_oui']                                                            = Value('id').of(objet.objetModification).contains('modifEIRL') ? true : false;
formFields['estEIRL_non']                                                            = Value('id').of(objet.objetModification).contains('modifEIRL') ? false : true;
formFields['intercalaireNombre']                                                     = ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
																					or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
																					or etablissementNew.ajoutFondePouvoir
																					or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
																					or (Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers')
																					and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant')))
																					and personneLiee1.autreDeclarationPersonneLiee) ? 
																					(personneLiee5.autreDeclarationPersonneLiee ? "2" : "1") : 
																					((objet.cadreObjet22P.declarationIndivisaire
																					or (Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers')
																					and Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant'))) ? 
																					(personneLiee4.autreDeclarationPersonneLiee ? "2" : "1") : '0');

var jqpa1 = $p2cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification1;																					
var jqpa2 = $p2cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification2;																					
var jqpa3 = $p2cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification3;																					
formFields['signature']                                                              = '';


                          // Intercalaire PEIRL 
								
//Cadre 1 - Déclaration ou modification d'affectation de patrimoine

formFields['eirl_complete_p0cmb']                                                         = false;
formFields['eirl_complete_p2cmb']                                                         = true;
formFields['eirl_complete_p4cmb']                                                         = false;
formFields['eirl_complete_p0me']                                                          = false;

formFields['declaration_initiale']															= objet.cadreObjetDeclarationEIRL.declarationEIRL ? true : false;
formFields['declaration_modification']                                                      = (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') or Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('finEIRL') or objet.entrepriseEIRL or objet.poursuiteEIRL) ? true : false;


// Cadre 2 - Rappel d'identification

formFields['eirl_siren']                                                  = identite.siren.split(' ').join('');
formFields['eirl_nomNaissance']                                           = identite.personneLieePersonnePhysiqueNomNaissance;
formFields['eirl_nomUsage']                                               = identite.personneLieePersonnePhysiqueNomUsage != null ? identite.personneLieePersonnePhysiqueNomUsage : '';
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFields['eirl_prenom']                                      = prenoms.toString();


// Cadre 3 - Déclaration d'affectation de patrimoine

var declarationAffectation = $p2cmb.cadre4DeclarationAffectationPatrimoineGroup.cadreDeclarationAffectationPatrimoine;

if (objet.cadreObjetDeclarationEIRL.declarationEIRL) { 
formFields['eirl_statutEIRL_declarationInitialeAvecDepot']                                  = Value('id').of(declarationAffectation.eirlDepot).eq('eirlAvecDepot') ? true : false;
formFields['eirl_statutEIRL_declarationInitialeSansDepot']                                  = Value('id').of(declarationAffectation.eirlDepot).eq('eirlSansDepot') ? true : false;
formFields['eirl_statutEIRL_reprise']                                                       = Value('id').of(declarationAffectation.eirlStatut).eq('EirlStatutEIRLReprise') ? true : false;
formFields['eirl_denomination']                                                             = declarationAffectation.declarationPatrimoine.eirlDenomination;
if (declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable !== null) {
    var dateTmp = new Date(parseInt(declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable.getTimeInMillis()));
    var dateClotureEc = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateClotureEc = dateClotureEc.concat(pad(month.toString()));
    formFields['eirl_dateClotureExerciceComptable']          = dateClotureEc;
}
formFields['eirl_objet']                                                                    = declarationAffectation.declarationPatrimoine.eirlObjet;
formFields['eirl_depotRCS']                                                       = Value('id').of(declarationAffectation.declarationPatrimoine.eirlChoixDepotRegistre).eq('EirlDepotRCS') ? true : false;
formFields['eirl_depotRM']                                                        = Value('id').of(declarationAffectation.declarationPatrimoine.eirlChoixDepotRegistre).eq('EirlDepotRM') ? true : false;

if (Value('id').of(declarationAffectation.eirlStatut).eq('EirlStatutEIRLReprise')) {
formFields['eirl_precedentEIRLDenomination']                                                = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLDenomination;
formFields['eirl_precedentEIRLLieuImmatriculation']                                         = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLLieuImmatriculation;
formFields['eirl_precedentEIRLSIREN']                                                       = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN != null ? (declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN.split(' ').join('')) : '';
formFields['eirl_precedentEIRLRegistre_rcs']                                      = Value('id').of(declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLRegistre).eq('EirlPrecedentEIRLRegistreRcs') ? true : false;
formFields['eirl_precedentEIRLRegistre_rm']                                       = Value('id').of(declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLRegistre).eq('EirlPrecedentEIRLRegistreRm') ? true : false;
formFields['eirl_precedentEIRLRegistre_rseirl']                                   = Value('id').of(declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLRegistre).eq('EirlPrecedentEIRLRegistreRseirl') ? true : false;
}
}

// Cadre 4 - Rappel d'identification reltif à l'EIRL

var rappelIdentification = $p2cmb.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreRappelIdentificationEIRL;

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') or Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('finEIRL') or objet.entrepriseEIRL or objet.poursuiteEIRL) { 
formFields['eirl_rappelDenomination']                   = rappelIdentification.eirlRappelDenomination;
formFields['eirl_rappelLieuImmatriculation']            = rappelIdentification.eirlRappelLieuImmatriculation;

if (objet.cadreObjetModificationEIRL.memeAdresseEIRL or objet.entrepriseEIRL or objet.poursuiteEIRL) {
formFields['eirl_rappelAdresse']                    = (adresseEntreprise.etablissementAdresseNumeroVoie != null ? adresseEntreprise.etablissementAdresseNumeroVoie : '')
													+ ' ' + (adresseEntreprise.etablissementAdresseIndiceVoie != null ? adresseEntreprise.etablissementAdresseIndiceVoie : '')
													+ ' ' + (adresseEntreprise.etablissementAdresseTypeVoie != null ? adresseEntreprise.etablissementAdresseTypeVoie : '')
													+ ' ' + (adresseEntreprise.etablissementAdresseNomVoie != null ? adresseEntreprise.etablissementAdresseNomVoie : '')
													+ ' ' + (adresseEntreprise.etablissementAdresseComplementVoie != null ? adresseEntreprise.etablissementAdresseComplementVoie : '')
													+ ' ' + (adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null ? adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie : '')
													+ ' ' + adresseEntreprise.etablissementAdresseCodePostal
													+ ' ' + adresseEntreprise.etablissementAdresseCommune;
} else if (not objet.cadreObjetModificationEIRL.memeAdresseEIRL) {
formFields['eirl_rappelAdresse']                    = (rappelIdentification.cadreEirlRappelAdresse.numeroAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.numeroAdresseEirl : '')
													+ ' ' + (rappelIdentification.cadreEirlRappelAdresse.indiceVoieAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.indiceVoieAdresseEirl : '')
													+ ' ' + (rappelIdentification.cadreEirlRappelAdresse.typeVoieAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.typeVoieAdresseEirl : '')
													+ ' ' + rappelIdentification.cadreEirlRappelAdresse.nomVoieAdresseEirl
													+ ' ' + (rappelIdentification.cadreEirlRappelAdresse.complementAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.complementAdresseEirl : '')
													+ ' ' + (rappelIdentification.cadreEirlRappelAdresse.distriutionSpecialeAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.distriutionSpecialeAdresseEirl : '')
													+ ' ' + rappelIdentification.cadreEirlRappelAdresse.codePostalAdresseEirl
													+ ' ' + rappelIdentification.cadreEirlRappelAdresse.communeAdresseEirl;
}
formFields['eirl_rappelDepotRCS']                                                 = Value('id').of(rappelIdentification.eirlRappelgreffeDepot).eq('eirlRappelDepotRCS');
formFields['eirl_rappelDepotRM']                                                  = Value('id').of(rappelIdentification.eirlRappelgreffeDepot).eq('eirlRappelDepotRM');
formFields['eirl_rappelDepotRSEIRL']                                              = Value('id').of(rappelIdentification.eirlRappelgreffeDepot).eq('eirlRappelDepotRSEIRL');
}

// Cadre 5 et 6- Déclaration de modification

var modifEIRL = $p2cmb.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL;                                         

if(Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDenoEIRL')) {
if (modifEIRL.modifDateEIRL !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateDenoEIRL']          = dateModif;
}
formFields['new_DenoEIRL']                                                                  = modifEIRL.newDenoEIRL;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDateCloture')) {
if (modifEIRL.modifDateEIRL !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateClotureExerciceComptableEIRL']          = dateModif;
}                                         
if (modifEIRL.eirlNewDateClotureExerciceComptable !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.eirlNewDateClotureExerciceComptable.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    formFields['eirl_newDateClotureExerciceComptable']          = dateModif;
}
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifObjet')) {
if (modifEIRL.modifDateEIRL !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateObjetEIRL']          = dateModif;
}
formFields['new_objetEIRL']                                                                 = modifEIRL.newObjetEIRL;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse')) {
if (modifEIRL.modifDateEIRL !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateAdresseEIRL']          = dateModif;
}	
formFields['new_adresseEIRL']                           = (modifEIRL.newAdresseEIRL.numeroAdresseEirlNew != null ? modifEIRL.newAdresseEIRL.numeroAdresseEirlNew : '')
														+ ' ' + (modifEIRL.newAdresseEIRL.indiceVoieAdresseEirlNew != null ? modifEIRL.newAdresseEIRL.indiceVoieAdresseEirlNew : '')
														+ ' ' + (modifEIRL.newAdresseEIRL.typeVoieAdresseEirlNew != null ? modifEIRL.newAdresseEIRL.typeVoieAdresseEirlNew : '')
														+ ' ' + modifEIRL.newAdresseEIRL.nomVoieAdresseEirlNew
														+ ' ' + (modifEIRL.newAdresseEIRL.complementAdresseEirlNew != null ? modifEIRL.newAdresseEIRL.complementAdresseEirlNew : '')
														+ ' ' + (modifEIRL.newAdresseEIRL.distriutionSpecialeAdresseEirlNew != null ? modifEIRL.newAdresseEIRL.distriutionSpecialeAdresseEirlNew : '')
														+ ' ' + modifEIRL.newAdresseEIRL.codePostalAdresseEirlNew
														+ ' ' + modifEIRL.newAdresseEIRL.communeAdresseEirlNew;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and objet.entrepriseEIRL) {
if (etablissement.modifDateAdresseEntreprise !== null) {
    var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateAdresseEIRL']          = date;
}
formFields['new_adresseEIRL']                            = (etablissementNew.adresseEtablissementNew.etablissementAdresseNumeroVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseNumeroVoieNew : '') 
														+ ' ' + (etablissementNew.adresseEtablissementNew.etablissementAdresseIndiceVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseIndiceVoieNew : '')
														+ ' ' + (etablissementNew.adresseEtablissementNew.etablissementAdresseTypeVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseTypeVoieNew : '')
														+ ' ' + (etablissementNew.adresseEtablissementNew.etablissementAdresseNomVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseNomVoieNew : '')
														+ ' ' + (etablissementNew.adresseEtablissementNew.etablissementAdresseComplementVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseComplementVoieNew : '')
														+ ' ' + (etablissementNew.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew : '')
														+ ' ' + etablissementNew.adresseEtablissementNew.etablissementAdresseCodePostalNew
														+ ' ' + etablissementNew.adresseEtablissementNew.etablissementAdresseCommuneNew;
} else if (objet.domicileEntreprise and objet.entrepriseEIRL) {
if (modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateAdresseEIRL']          = date;
}
formFields['new_adresseEIRL']                             = (newAdresseDom.personneLieeAdresseVoieNew != null ? newAdresseDom.personneLieeAdresseVoieNew : '') 
														  + ' ' + (newAdresseDom.personneLieeAdresseIndiceVoieNew != null ? newAdresseDom.personneLieeAdresseIndiceVoieNew : '')
														  + ' ' + (newAdresseDom.personneLieeAdresseTypeVoieNew != null ? newAdresseDom.personneLieeAdresseTypeVoieNew : '')
														  + ' ' + newAdresseDom.personneLieeAdresseNomVoieNew
														  + ' ' + (newAdresseDom.personneLieeAdresseComplementAdresseNew != null ? newAdresseDom.personneLieeAdresseComplementAdresseNew : '')
														  + ' ' + (newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null ? newAdresseDom.personneLieeAdresseDistriutionSpecialeNew : '')
														  + ' ' + newAdresseDom.personneLieeAdresseCodePostalNew
														  + ' ' + newAdresseDom.personneLieeAdresseCommuneNew;
}

var finDAP = $p2cmb.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.finEIRLGroup;
if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('finEIRL')) { 																							
if (finDAP.finEIRLDeclaration.modifDateFinEIRL !== null) {
    var dateTmp = new Date(parseInt(finDAP.finEIRLDeclaration.modifDateFinEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateFinEIRL']          = dateModif;
}
formFields['finEIRL_renonciationAvecPoursuite']                                             = Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).contains('finEIRL') ? (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLRenonciationAvecPoursuite') ? true : false) : false;
formFields['finEIRL_renonciationSansPoursuite']                                             = Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).contains('finEIRL') ? (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLRenonciationSansPoursuite') ? true : false) : false;
formFields['finEIRL_cessionPP']                                                             = Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).contains('finEIRL') ? (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLCessionPP') ? true : false) : false;
formFields['finEIRL_cessionPM']                                                             = Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).contains('finEIRL') ? (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLCessionPM') ? true : false) : false;
}	

var intentionPoursuite = $p2cmb.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.poursuiteEIRLGroup;

if (objet.cadreObjet22P.poursuiteEIRL) {
if (objet.cadreObjet22P.modifDateDeces !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjet22P.modifDateDeces.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDatePoursuiteEIRL']          = date;
}
formFields['modif_pouruiteEIRL'] = true;
var prenoms=[];
for ( i = 0; i < intentionPoursuite.eIRLPoursuitePrenom.size() ; i++ ){prenoms.push(intentionPoursuite.eIRLPoursuitePrenom[i]);}                            
formFields['EIRL_poursuiteNomPrenom']                      = intentionPoursuite.eIRLPoursuiteNom + ' ' + prenoms.toString();
}


// Cadre 7 - Options fiscales

var fiscalEIRL = $p2cmb.cadre4DeclarationAffectationPatrimoineGroup.cadre3OptionsFiscalesEIRLnonME;
if (objet.cadreObjetDeclarationEIRL.declarationEIRL and not identite.optionME) {
formFields['eirl_regimeFiscal_regimeImpositionBenefices_mBIC']                    = Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('EirlRegimeFiscalRegimeImpositionBeneficesMBIC') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rsBIC']                   = (Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('EirlRegimeFiscalRegimeImpositionBeneficesReelBIC') and Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalReel).eq('EirlRegimeFiscalRegimeImpositionBeneficesRs')) ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rnBIC']                   = (Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('EirlRegimeFiscalRegimeImpositionBeneficesReelBIC') and Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalReel).eq('EirlRegimeFiscalRegimeImpositionBeneficesRn')) ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_optionIS']                = Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('EirlRegimeFiscalRegimeImpositionBeneficesOptionIS') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rsIS']                    = (Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('EirlRegimeFiscalRegimeImpositionBeneficesOptionIS') and Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalReel).eq('EirlRegimeFiscalRegimeImpositionBeneficesRs')) ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rnIS']                    = (Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('EirlRegimeFiscalRegimeImpositionBeneficesOptionIS') and Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalReel).eq('EirlRegimeFiscalRegimeImpositionBeneficesRn')) ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVA_rfTVA']                         = Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('EirlRegimeFiscalRegimeImpositionBeneficesMBIC') ? true : ((Value('id').of(fiscalEIRL.optionsTVA.regimeTVA1).eq('EirlRegimeFiscalRegimeImpositionTVARfTVA') or Value('id').of(fiscalEIRL.optionsTVA.regimeTVA2).eq('EirlRegimeFiscalRegimeImpositionTVARfTVA')) ? true : false);
formFields['eirl_regimeFiscal_regimeImpositionTVA_rsTVA']                         = Value('id').of(fiscalEIRL.optionsTVA.regimeTVA1).eq('EirlRegimeFiscalRegimeImpositionTVARsTVA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVA_mrTVA']                         = Value('id').of(fiscalEIRL.optionsTVA.regimeTVA1).eq('EirlRegimeFiscalRegimeImpositionTVAMrTVA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVA_rnTVA']                         = Value('id').of(fiscalEIRL.optionsTVA.regimeTVA2).eq('EirlRegimeFiscalRegimeImpositionTVARnTVA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVAOptionsParticulieres2']          = fiscalEIRL.optionsTVA.eirlRegimeFiscalRegimeImpositionTVAOptionsParticulieres2 ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVAOptionsParticulieres1']          = fiscalEIRL.optionsTVA.eirlregimeFiscalRegimeImpositionTVAOptionsParticulieres1 ? true : false;
}
if (objet.cadreObjetDeclarationEIRL.declarationEIRL and identite.optionME) {
formFields['eirl_regimeFiscal_regimeImpositionBeneficesVersementLiberatoire_oui'] = $p2cmb.cadre4DeclarationAffectationPatrimoineGroup.cadre3OptionsFiscalesEIRLME.regimeFiscalRegimeImpositionBeneficesVersementLiberatoire ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBeneficesVersementLiberatoire_non'] = $p2cmb.cadre4DeclarationAffectationPatrimoineGroup.cadre3OptionsFiscalesEIRLME.regimeFiscalRegimeImpositionBeneficesVersementLiberatoire ? false : true;
}


// TNS

// Cadres 1
formFields['formulaireDependanceMOSARL_TNS']                                   = false;
formFields['formulaireDependanceM0SNC_TNS']                                    = false;
formFields['formulaireDependanceM0SotCiv_TNS']                                 = false;
formFields['formulaireDependanceM2_TNS']                                       = false;
formFields['formulaireDependanceM3_TNS']                                       = false;
formFields['formulaireDependanceM3SARL_TNS']                                   = false;
formFields['formulaireDependanceTNS_TNS']                                      = true;

// Cadres 2
formFields['entrepriseDenomination_TNS']                                       = '';
formFields['numeroUniqueIdentification_TNS']                                   = identite.siren.split(' ').join('');

// Cadre 3
var social = $p2cmb.cadreDeclarationSocialeGroup.cadreDeclarationSociale;

if (Value('id').of(objet.cadreObjet22P.infoDeces).eq('22P')	or (Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant') 
	and Value('id').of(exploitant1.objetModifExploitant).eq('nouveauExploitant'))) {
formFields['personneLieePPNomNaissance_TNS']                                    = exploitant1.nomNaissanceExploitant;
formFields['personneLieePPNomUsage_TNS']                                        = exploitant1.nomUsageExploitant != null ? exploitant1.nomUsageExploitant : '';
var prenoms=[];
for ( i = 0; i < exploitant1.prenomExploitant.size() ; i++ ){prenoms.push(exploitant1.prenomExploitant[i]);}                            
formFields['personneLieePPPrenom_TNS']                                      = prenoms.toString();
} else if (Value('id').of(exploitant2.objetModifExploitant).eq('nouveauExploitant')) {
formFields['personneLieePPNomNaissance_TNS']                                    = exploitant2.nomNaissanceExploitant;
formFields['personneLieePPNomUsage_TNS']                                        = exploitant2.nomUsageExploitant != null ? exploitant1.nomUsageExploitant : '';
var prenoms=[];
for ( i = 0; i < exploitant2.prenomExploitant.size() ; i++ ){prenoms.push(exploitant2.prenomExploitant[i]);}                            
formFields['personneLieePPPrenom_TNS']                                      = prenoms.toString();
}	
formFields['personneLieeQualite_TNS']                                          = "Exploitant pour le compte de l'indivision";

var nirDeclarant = social.voletSocialNumeroSecuriteSociale;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFields['voletSocialNumeroSecuriteSocial_TNS']                = nirDeclarant.substring(0, 13);
    formFields['voletSocialNumeroSecuriteSocialCle_TNS']             = nirDeclarant.substring(13, 15);
}

formFields['voletSocialRegimeAssuranceMaladieRegimeGeneral_TNS']               = Value('id').of(social.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? true : false;
formFields['voletSocialRegimeAssuranceMaladieRegimeAgricole_TNS']              = Value('id').of(social.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? true : false;
formFields['voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricole_TNS'] = Value('id').of(social.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? true : false;
formFields['voletSocialRegimeAssuranceMaladieRegimeAutre_TNS']                 = Value('id').of(social.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? true : false;
formFields['voletSocialRegimeAssuranceMaladieRegimeEnim_TNS']                  = Value('id').of(social.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? true : false;
formFields['voletSocialRegimeAssuranceMaladieRegimeAutrePrecision_TNS']        = Value('id').of(social.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') ? social.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS : '';
if (social.voletSocialActiviteExerceeAnterieurementTNS) {
formFields['voletSocialActiviteExerceeAnterieurementLibelle_TNS']              = social.voletSocialActiviteExerceeAnterieurementLibelleTNS;
formFields['voletSocialActiviteExerceeAnterieurementDepartement_TNS']          = social.voletSocialActiviteExerceeAnterieurementDepartementTNS.getId();
formFields['voletSocialActiviteExerceeAnterieurementCommune_TNS']              = social.voletSocialActiviteExerceeAnterieurementCommuneTNS;
if(social.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
	var dateTmp = new Date(parseInt(social.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['voletSocialActiviteExerceeAnterieurementDateCessation_TNS'] = date;
}
}
formFields['voletSocialActiviteSimultaneeOUI_TNS']                            = social.voletSocialActiviteAutreQueDeclareeStatutOui ? true : false;
formFields['voletSocialActiviteSimultaneeNON_TNS']                            = social.voletSocialActiviteAutreQueDeclareeStatutOui ? false : true;
if (social.voletSocialActiviteAutreQueDeclareeStatutOui) {
formFields['voletSocialActiviteSimultaneeSalarie_TNS']                        = Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutSalarie') ? true : false;
formFields['voletSocialActiviteSimultaneeSalarieAgricole_TNS']                = Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutSalarieAgricole') ? true : false;
formFields['voletSocialActiviteSimultaneeRetraitePensionne_TNS']              = Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutRetraitePensionne') ? true : false;
formFields['voletSocialActiviteSimultaneeAutre_TNS'] 				          = Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutCocheAutre') ? true : false;
formFields['voletSocialActiviteSimultaneeAutrePrecision_TNS']                 = social.voletSocialActiviteAutreQueDeclareeStatutAutre;
}
formFields['voletSocialPharmacienRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(social.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFields['voletSocialPharmacienRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(social.voletSocialPharmacienRegimeAuxiliairesMedicauxOUINON).eq('voletSocialPharmacienRegimeAuxiliairesMedicauxNONTNS') ? true : false;
formFields['voletSocialBiologisteRegimeAuxiliairesMedicauxOUI_TNS']           = Value('id').of(social.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxOUITNS') ? true : false;
formFields['voletSocialBiologisteRegimeAuxiliairesMedicauxNON_TNS']           = Value('id').of(social.voletSocialBiologisteRegimeAuxiliairesMedicauxOUINON).eq('voletSocialBiologisteRegimeAuxiliairesMedicauxNONTNS') ? true : false;

// Cadre 6

formFields['renseignementsComplementairesLeDeclarant_TNS']                           = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
if (Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire') or Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteAutre')) {
formFields['renseignementsComplementairesLeMandataire_TNS']                          = true;
formFields['formaliteSignataireNomPrenomDenomination_TNS']	                         = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFields['formaliteSignataireAdresse_TNS']                                         = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '');
formFields['formaliteSignataireAdresseComplement_TNS']								 = signataire.adresseMandataire.dataCodePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire;
}
if(signataire.formaliteSignatureDate != null) {
	var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['formaliteSignatureDate_TNS'] = date;
}
formFields['formaliteSignatureLieu_TNS']                                             = signataire.formaliteSignatureLieu;
formFields['signature_TNS']                                                    = '';


// JQPA 1

var formFieldsPers3  = {};

// Cadre 1
formFieldsPers3['jqpa_intercalaire']                         = true;
formFieldsPers3['jqpa_formulaire']                           = false;

// Cadre 2
formFieldsPers3['entreprise_siren']                          = identite.siren.split(' ').join('');
formFieldsPers3['entreprise_dirigeants_ppNomUsage']          = identite.personneLieePersonnePhysiqueNomUsage != null ? identite.personneLieePersonnePhysiqueNomUsage : ''  ;
formFieldsPers3['entreprise_dirigeants_ppNomNaissance']      = identite.personneLieePersonnePhysiqueNomNaissance;
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFieldsPers3['entreprise_dirigeants_ppPrenom']            = prenoms.toString();
if(identite.personneLieePersonnePhysiqueDateNaissance != null) {
	var dateTmp = new Date(parseInt(identite.personneLieePersonnePhysiqueDateNaissance.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers3['entreprise_dirigeants_ppDateNaissance'] = date;
}
 
 // Cadre 3A
formFieldsPers3['jqpa_activiteJqpa_PP']                      = jqpa1.jqpaActiviteJqpaPP;

//Cadre 3B
formFieldsPers3['jqpa_aqpaSituationPP_diplome']              = Value('id').of(jqpa1.jqpaSituation).eq('jqpaSituationDiplome') or Value('id').of(jqpa1.jqpaSituation).eq('jqpaSituationExperience') ? true : false;

// Cadre 3C
formFieldsPers3['jqpa_qualitePersonneQualifieePP_declarant'] = Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPDeclarant') ? true : false;
formFieldsPers3['jqpa_qualitePersonneQualifieePP_conjoint']  = Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPConjoint') ? true : false;
formFieldsPers3['jqpa_qualitePersonneQualifieePP_salarie']   = Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') ? true : false;
formFieldsPers3['jqpa_qualitePersonneQualifieePP_autre']     = Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre') ? true : false;
formFieldsPers3['jqpa_qualitePersonneQualifieePPAutre']      = jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifieePPChampAutre;

formFieldsPers3['jqpa_identitePPNomNaissance']               = jqpa1.identitePersonneQualifiee.jqpaIdentitePPNomNaissance != null ? jqpa1.identitePersonneQualifiee.jqpaIdentitePPNomNaissance : '';
formFieldsPers3['jqpa_identitePPNomUsage']                   = jqpa1.identitePersonneQualifiee.jqpaIdentitePPNomUsage != null ? jqpa1.identitePersonneQualifiee.jqpaIdentitePPNomUsage : '';
var prenoms=[];
for ( i = 0; i < jqpa1.identitePersonneQualifiee.jqpaIdentitePPPrenom.size() ; i++ ){prenoms.push(jqpa1.identitePersonneQualifiee.jqpaIdentitePPPrenom[i]);}                            
formFieldsPers3['jqpa_identitePPPrenom']            = prenoms.toString();

if (jqpa1.identitePersonneQualifiee.jqpaIdentitePPDateNaissance !== null) {
	var dateTmp = new Date(parseInt(jqpa1.identitePersonneQualifiee.jqpaIdentitePPDateNaissance.getTimeInMillis()));
	var dateNaissance = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	dateNaissance = dateNaissance.concat(pad(month.toString()));
	dateNaissance = dateNaissance.concat(dateTmp.getFullYear().toString());
	formFieldsPers3['jqpa_identitePPDateNaissance']          = dateNaissance;
}
formFieldsPers3['jqpa_identitePPDepartement']                = jqpa1.identitePersonneQualifiee.jqpaIdentitePPDepartement != null ? jqpa1.identitePersonneQualifiee.jqpaIdentitePPDepartement.getId() : '';
formFieldsPers3['jqpa_identitePPLieuNaissancePays']          = (Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') or Value('id').of(jqpa1.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre')) ? ((jqpa1.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune != null ? jqpa1.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune : jqpa1.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceVille) + ' / ' + jqpa1.identitePersonneQualifiee.jqpaIdentitePPLieuNaissancePays) : '';

// Cadre 3C
formFieldsPers3['jqpa_aqpaSituationPP_engagement']           = Value('id').of(jqpa1.jqpaSituation).eq('jqpaSituationRecrutement') ? true : false;

/* A NE GENERER QUE SI FORMULAIRE ET NON EN TANT QU'INTERCALAIRE
// Cadre 6
formFieldsPers3['jqpa_formalite_declarant']                  = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFieldsPers3['jqpa_formalite_representant']               = false;
formFieldsPers3['jqpa_formalite_mandataire']                 = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
formFieldsPers3['jqpa_formalite_mandataireInfo']             = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? signataire.adresseMandataire.nomPrenomDenominationMandataire : '';	
formFieldsPers3['jqpa_formalite_lieu']                       = signataire.formaliteSignatureLieu;
formFieldsPers3['jqpa_formalite_date']                       = signataire.formaliteSignatureDate;
formFieldsPers3['jqpa_formalite_signature']                  = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce";
*/

// JQPA 2

var formFieldsPers4 = {};

// Cadre 1
formFieldsPers4['jqpa_intercalaire']                         = true;
formFieldsPers4['jqpa_formulaire']                           = false;


// Cadre 2
formFieldsPers4['entreprise_siren']                          = identite.siren.split(' ').join('');
formFieldsPers4['entreprise_dirigeants_ppNomUsage']          = identite.personneLieePersonnePhysiqueNomUsage != null ? identite.personneLieePersonnePhysiqueNomUsage : ''  ;
formFieldsPers4['entreprise_dirigeants_ppNomNaissance']      = identite.personneLieePersonnePhysiqueNomNaissance;
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFieldsPers4['entreprise_dirigeants_ppPrenom']            = prenoms.toString();
if(identite.personneLieePersonnePhysiqueDateNaissance != null) {
	var dateTmp = new Date(parseInt(identite.personneLieePersonnePhysiqueDateNaissance.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers4['entreprise_dirigeants_ppDateNaissance'] = date;
}

 // Cadre 3A
formFieldsPers4['jqpa_activiteJqpa_PP']                      = jqpa2.jqpaActiviteJqpaPP;

//Cadre 3B
formFieldsPers4['jqpa_aqpaSituationPP_diplome']              = Value('id').of(jqpa2.jqpaSituation).eq('jqpaSituationDiplome') or Value('id').of(jqpa2.jqpaSituation).eq('jqpaSituationExperience') ? true : false;

// Cadre 3C
formFieldsPers4['jqpa_qualitePersonneQualifieePP_declarant'] = Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPDeclarant') ? true : false;
formFieldsPers4['jqpa_qualitePersonneQualifieePP_conjoint']  = Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPConjoint') ? true : false;
formFieldsPers4['jqpa_qualitePersonneQualifieePP_salarie']   = Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') ? true : false;
formFieldsPers4['jqpa_qualitePersonneQualifieePP_autre']     = Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre') ? true : false;
formFieldsPers4['jqpa_qualitePersonneQualifieePPAutre']      = jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifieePPChampAutre;

formFieldsPers4['jqpa_identitePPNomNaissance']               = jqpa2.identitePersonneQualifiee.jqpaIdentitePPNomNaissance != null ? jqpa2.identitePersonneQualifiee.jqpaIdentitePPNomNaissance : '';
formFieldsPers4['jqpa_identitePPNomUsage']                   = jqpa2.identitePersonneQualifiee.jqpaIdentitePPNomUsage != null ? jqpa2.identitePersonneQualifiee.jqpaIdentitePPNomUsage : '';
var prenoms=[];
for ( i = 0; i < jqpa2.identitePersonneQualifiee.jqpaIdentitePPPrenom.size() ; i++ ){prenoms.push(jqpa2.identitePersonneQualifiee.jqpaIdentitePPPrenom[i]);}                            
formFieldsPers4['jqpa_identitePPPrenom']            = prenoms.toString();

if (jqpa2.identitePersonneQualifiee.jqpaIdentitePPDateNaissance !== null) {
	var dateTmp = new Date(parseInt(jqpa2.identitePersonneQualifiee.jqpaIdentitePPDateNaissance.getTimeInMillis()));
	var dateNaissance = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	dateNaissance = dateNaissance.concat(pad(month.toString()));
	dateNaissance = dateNaissance.concat(dateTmp.getFullYear().toString());
	formFieldsPers4['jqpa_identitePPDateNaissance']          = dateNaissance;
}
formFieldsPers4['jqpa_identitePPDepartement']                = jqpa2.identitePersonneQualifiee.jqpaIdentitePPDepartement != null ? jqpa2.identitePersonneQualifiee.jqpaIdentitePPDepartement.getId() : '';
formFieldsPers4['jqpa_identitePPLieuNaissancePays']          = (Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') or Value('id').of(jqpa2.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre')) ? ((jqpa2.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune != null ? jqpa2.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune : jqpa2.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceVille) + ' / ' + jqpa2.identitePersonneQualifiee.jqpaIdentitePPLieuNaissancePays) : '';

// Cadre 3C
formFieldsPers4['jqpa_aqpaSituationPP_engagement']           = Value('id').of(jqpa2.jqpaSituation).eq('jqpaSituationRecrutement') ? true : false;


// JQPA 3

var formFieldsPers5 = {};

// Cadre 1
formFieldsPers5['jqpa_intercalaire']                         = true;
formFieldsPers5['jqpa_formulaire']                           = false;


// Cadre 2
formFieldsPers5['entreprise_siren']                          = identite.siren.split(' ').join('');
formFieldsPers5['entreprise_dirigeants_ppNomUsage']          = identite.personneLieePersonnePhysiqueNomUsage != null ? identite.personneLieePersonnePhysiqueNomUsage : ''  ;
formFieldsPers5['entreprise_dirigeants_ppNomNaissance']      = identite.personneLieePersonnePhysiqueNomNaissance;
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFieldsPers5['entreprise_dirigeants_ppPrenom']            = prenoms.toString();
if(identite.personneLieePersonnePhysiqueDateNaissance != null) {
	var dateTmp = new Date(parseInt(identite.personneLieePersonnePhysiqueDateNaissance.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFieldsPers5['entreprise_dirigeants_ppDateNaissance'] = date;
}


 // Cadre 3A
formFieldsPers5['jqpa_activiteJqpa_PP']                      = jqpa3.jqpaActiviteJqpaPP;

//Cadre 3B
formFieldsPers5['jqpa_aqpaSituationPP_diplome']              = Value('id').of(jqpa3.jqpaSituation).eq('jqpaSituationDiplome') or Value('id').of(jqpa3.jqpaSituation).eq('jqpaSituationExperience') ? true : false;

// Cadre 3C
formFieldsPers5['jqpa_qualitePersonneQualifieePP_declarant'] = Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPDeclarant') ? true : false;
formFieldsPers5['jqpa_qualitePersonneQualifieePP_conjoint']  = Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPConjoint') ? true : false;
formFieldsPers5['jqpa_qualitePersonneQualifieePP_salarie']   = Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') ? true : false;
formFieldsPers5['jqpa_qualitePersonneQualifieePP_autre']     = Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre') ? true : false;
formFieldsPers5['jqpa_qualitePersonneQualifieePPAutre']      = jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifieePPChampAutre;

formFieldsPers5['jqpa_identitePPNomNaissance']               = jqpa3.identitePersonneQualifiee.jqpaIdentitePPNomNaissance != null ? jqpa3.identitePersonneQualifiee.jqpaIdentitePPNomNaissance : '';
formFieldsPers5['jqpa_identitePPNomUsage']                   = jqpa3.identitePersonneQualifiee.jqpaIdentitePPNomUsage != null ? jqpa3.identitePersonneQualifiee.jqpaIdentitePPNomUsage : '';
var prenoms=[];
for ( i = 0; i < jqpa3.identitePersonneQualifiee.jqpaIdentitePPPrenom.size() ; i++ ){prenoms.push(jqpa3.identitePersonneQualifiee.jqpaIdentitePPPrenom[i]);}                            
formFieldsPers5['jqpa_identitePPPrenom']            = prenoms.toString();

if (jqpa3.identitePersonneQualifiee.jqpaIdentitePPDateNaissance !== null) {
	var dateTmp = new Date(parseInt(jqpa3.identitePersonneQualifiee.jqpaIdentitePPDateNaissance.getTimeInMillis()));
	var dateNaissance = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	dateNaissance = dateNaissance.concat(pad(month.toString()));
	dateNaissance = dateNaissance.concat(dateTmp.getFullYear().toString());
	formFieldsPers5['jqpa_identitePPDateNaissance']          = dateNaissance;
}
formFieldsPers5['jqpa_identitePPDepartement']                = jqpa3.identitePersonneQualifiee.jqpaIdentitePPDepartement != null ? jqpa3.identitePersonneQualifiee.jqpaIdentitePPDepartement.getId() : '';
formFieldsPers5['jqpa_identitePPLieuNaissancePays']          = (Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPSalarie') or Value('id').of(jqpa3.qualitePersonneQualifiee.jqpaQualitePersonneQualifiee).eq('jqpaQualitePersonneQualifieePPAutre')) ? ((jqpa3.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune != null ? jqpa3.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceCommune : jqpa3.identitePersonneQualifiee.jqpaIdentitePPLieuNaissanceVille) + ' / ' + jqpa3.identitePersonneQualifiee.jqpaIdentitePPLieuNaissancePays) : '';

// Cadre 3C
formFieldsPers5['jqpa_aqpaSituationPP_engagement']           = Value('id').of(jqpa3.jqpaSituation).eq('jqpaSituationRecrutement') ? true : false;


// NDI
formFields['immatRCSGreffe']                                         = Value('id').of(identite.immatriculation).contains('immatRCS') ? identite.immatRCSGreffe : '';

formFields['complete_immatriculation_ndi']                                 = true;
formFields['complete_modification_ndi']                                    = false;
formFields['siren_ndi']                                                    = identite.siren.split(' ').join('');
formFields['immatRCSGreffe_ndi']                                           = Value('id').of(identite.immatriculation).contains('immatRCS') ? identite.immatRCSGreffe : '';
formFields['nomNaissance_ndi']                                             = identite.personneLieePersonnePhysiqueNomNaissance;
formFields['nomUsage_ndi']                                                 = identite.personneLieePersonnePhysiqueNomUsage != null ? identite.personneLieePersonnePhysiqueNomUsage : '';
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFields['prenom_ndi']                                                   = prenoms.toString();

if (activiteInfo.cadreEtablissementNomDomaine.modifDateIdentification != null) {
	var dateTmp = new Date(parseInt(activiteInfo.cadreEtablissementNomDomaine.modifDateIdentification.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['dateDebutActivite_ndi'] = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') and etablissementNew.modifDateAdresseOuverture != null) {
	var dateTmp = new Date(parseInt(etablissementNew.modifDateAdresseOuverture.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['dateDebutActivite_ndi'] = date;
} else if (objet.domicileEntreprise and modifIdentiteDeclarant.modifDomicile.modifDateDomicile != null) {
	var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['dateDebutActivite_ndi'] = date;
} else if ((Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementOuverture')	or Value('id').of(etablissementNew.origineEtablissement2).eq('etablissementOuverture')) 
	and etablissement.modifDateAdresseEntreprise != null) {
	var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['dateDebutActivite_ndi'] = date;
} else if (Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementOuverture')	and objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise != null) {
	var dateTmp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['dateDebutActivite_ndi'] = date;
}

formFields['nomDomaineEtablissement_ndi']                                               = activite.etablissementNomDomaine != null ? activite.etablissementNomDomaine : (activiteInfo.cadreEtablissementNomDomaine.modifNomDomaine != null ? activiteInfo.cadreEtablissementNomDomaine.modifNomDomaine : '');


	
formFields['adresseEtablissement_voie_ndi']                                             = etablissementNew.adresseEtablissementNew.etablissementAdresseCommuneNew != null ? 
																						((etablissementNew.adresseEtablissementNew.etablissementAdresseNumeroVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseNumeroVoieNew : '') 
																						+ ' ' + (etablissementNew.adresseEtablissementNew.etablissementAdresseIndiceVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseIndiceVoieNew : '') 
																						+ ' ' + (etablissementNew.adresseEtablissementNew.etablissementAdresseTypeVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseTypeVoieNew : '') 
																						+ ' ' + etablissementNew.adresseEtablissementNew.etablissementAdresseNomVoieNew           
																						+ ' ' + (etablissementNew.adresseEtablissementNew.etablissementAdresseComplementVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseComplementVoieNew : '') 
																						+ ' ' + (etablissementNew.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew :''))
																						: ((adresseEntreprise.etablissementAdresseNumeroVoie != null ? adresseEntreprise.etablissementAdresseNumeroVoie : '') 
																						+ ' ' + (adresseEntreprise.etablissementAdresseIndiceVoie != null ? adresseEntreprise.etablissementAdresseIndiceVoie : '') 
																						+ ' ' + (adresseEntreprise.etablissementAdresseTypeVoie != null ? adresseEntreprise.etablissementAdresseTypeVoie : '') 
																						+ ' ' + adresseEntreprise.etablissementAdresseNomVoie
																				        + ' ' + (adresseEntreprise.etablissementAdresseComplementVoie != null ? adresseEntreprise.etablissementAdresseComplementVoie : '') 
																						+ ' ' + (adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null ? adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie : ''));

formFields['adresseEtablissement_codePostal_ndi']                                       = etablissementNew.adresseEtablissementNew.etablissementAdresseCodePostalNew != null ?  
																						etablissementNew.adresseEtablissementNew.etablissementAdresseCodePostalNew : adresseEntreprise.etablissementAdresseCodePostal;
formFields['adresseEtablissement_commune_ndi']                                            =  etablissementNew.adresseEtablissementNew.etablissementAdresseCommuneNew != null ? 
																						etablissementNew.adresseEtablissementNew.etablissementAdresseCommuneNew : adresseEntreprise.etablissementAdresseCommune;

formFields['declarationInitialeNomDomaineEtablissement_ndi']                            = true;

/*
 * Création du dossier avec volet social : ajout du cerfa avec volet social
 */
 
var cerfaDoc1 = nash.doc //
	.load('models/cerfa_11678-06_p2cmb_avec_volet_social.pdf') //
	.apply(formFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));

/*
 * Création du dossier sans volet social : ajout du cerfa sans volet social
 */
 
 var cerfaDoc2 = nash.doc //
	.load('models/cerfa_11678-06_p2cmb_sans_volet_social.pdf') //
	.apply (formFields);
	cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));

/*
 * Ajout de l'intercalaire TNS
 */
if (Value('id').of(objet.cadreObjet22P.infoDeces).eq('22P')	or (Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant') 
	and (Value('id').of(exploitant1.objetModifExploitant).eq('nouveauExploitant') or Value('id').of(exploitant2.objetModifExploitant).eq('nouveauExploitant')))) {
	var tnsDoc1 = nash.doc //
		.load('models/cerfa_11686-07_TNS.pdf') //
		.apply (formFields);
	cerfaDoc1.append(tnsDoc1.save('cerfa.pdf'));
}
	
/*
 * Ajout de l'intercalaire P' 1
 */
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P') or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	or etablissementNew.ajoutFondePouvoir or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or (Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers') and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant')))
	and personneLiee1.autreDeclarationPersonneLiee)
	or (objet.cadreObjet22P.declarationIndivisaire
	or (Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers')	and Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant')))) {
	var pPrimeDoc1 = nash.doc //
		.load('models/cerfa_11677-01_pPrime.pdf') //
		.apply (formFieldsPers1);
	cerfaDoc1.append(pPrimeDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire P' 2
 */
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P') or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	or etablissementNew.ajoutFondePouvoir or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or (Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers') and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant')))
	and personneLiee5.autreDeclarationPersonneLiee)
	or ((objet.cadreObjet22P.declarationIndivisaire
	or (Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers')	and Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant')))
	and personneLiee4.autreDeclarationPersonneLiee)) {
	var pPrimeDoc2 = nash.doc //
		.load('models/cerfa_11677-01_pPrime.pdf') //
		.apply (formFieldsPers2);
	cerfaDoc1.append(pPrimeDoc2.save('cerfa.pdf'));
}
	
/*
 * Ajout de l'intercalaire PEIRL avec option fiscale
 */
if (Value('id').of(objet.objetModification).contains('modifEIRL') or objet.entrepriseEIRL or objet.poursuiteEIRL) {
	var peirlDoc1 = nash.doc //
		.load('models/cerfa_14215-04_peirlcmb_avec_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(peirlDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire PEIRL sans option fiscale
 */
if (Value('id').of(objet.objetModification).contains('modifEIRL') or objet.entrepriseEIRL or objet.poursuiteEIRL) {
	var peirlDoc2 = nash.doc //
		.load('models/cerfa_14215-04_peirlcmb_sans_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(peirlDoc2.save('cerfa.pdf'));
}


/*
 * Ajout de l'intercalaire JQPA 1
 */
 
  if (jqpa1.jqpaActiviteJqpaPP != null) {
	var p2JQPADoc1 = nash.doc //
		.load('models/cerfa_14077-02_JQPA.pdf') //
		.apply (formFieldsPers3);
	cerfaDoc1.append(p2JQPADoc1.save('cerfa.pdf'));
}
/*
 * Ajout de l'intercalaire JQPA 2
 */
 
 if (jqpa2.jqpaActiviteJqpaPP != null) {
	var p2JQPADoc2 = nash.doc //
		.load('models/cerfa_14077-02_JQPA.pdf') //
		.apply (formFieldsPers4);
	cerfaDoc1.append(p2JQPADoc2.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire JQPA 3
 */
 
  if (jqpa3.jqpaActiviteJqpaPP != null) {
	var p2JQPADoc3 = nash.doc //
		.load('models/cerfa_14077-02_JQPA.pdf') //
		.apply (formFieldsPers5);
	cerfaDoc1.append(p2JQPADoc3.save('cerfa.pdf'));
}  

/*
 * Ajout de l'intercalaire NDI 1er exemplaire
 */
 
 if (activite.etablissementNomDomaine != null or activiteInfo.cadreEtablissementNomDomaine.modifNomDomaine != null) {
	var p2NDIDoc1 = nash.doc //
		.load('models/cerfa_14943-01_NDI_volet1.pdf') //
		.apply (formFields);
	cerfaDoc1.append(p2NDIDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire NDI 2ème exemplaire
 */
 
 if (activite.etablissementNomDomaine != null or activiteInfo.cadreEtablissementNomDomaine.modifNomDomaine != null) {
	var p2NDIDoc2 = nash.doc //
		.load('models/cerfa_14943-01_NDI_volet2.pdf') //
		.apply (formFields);
	cerfaDoc1.append(p2NDIDoc2.save('cerfa.pdf'));
}


/*
 * Ajout des PJs
 */

var pjUser = [];
var metas = [];
function pushPjPreview(fld) {
	fld.forEach(function (elm) {
        pjUser.push(elm);
        metas.push({'name':'document', 'value': '/'+elm.getAbsolutePath()});
        
    });
}

// PJ Signataire
var pj=$p2cmb.cadre9SignatureGroup.cadre9Signature.soussigne;
if(Value('id').of(pj).eq('FormaliteSignataireQualiteDeclarant')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantSignataire);
}

if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPouvoir);
}

if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') or Value('id').of(pj).eq('FormaliteSignataireQualiteAutre')) {
   pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire);
}

// PJ 10P / 15P   

if((Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or Value('id').of(pj).eq('FormaliteSignataireQualiteAutre')) 
	and (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P') 
	or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P') 
	and not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('17P'))) {
   pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantNonSignataire1);
}

// 16P 

if((Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or Value('id').of(pj).eq('FormaliteSignataireQualiteAutre')) 
	and (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P')
	and not	Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P')
	and not	Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')
	and not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('17P'))) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantNonSignataire3);
}


// PJ 17P

if((Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') 
	or Value('id').of(pj).eq('FormaliteSignataireQualiteAutre')) 
	and Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('17P')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantNonSignataire2);
}

// PJ 22P ou 42P

if(Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('decesExploitant')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDActeDeces);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDActeNotoriete);
}

if (exploitant1.exploitantMineurEmancipe or exploitant2.exploitantMineurEmancipe) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDMineurEmancipe);
}	

// PJ 25P

if(($p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetDeclarationEIRL.declarationEIRL and not Value('id').of($p2cmb.cadre4DeclarationAffectationPatrimoineGroup.cadreDeclarationAffectationPatrimoine.eirlDepot).eq('eirlSansDepot')) or Value('id').of($p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEIRL.objetEIRL).contains('modificationEIRL') or $p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification.entrepriseEIRL) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAP);
}

var pj=$p2cmb.cadre4DeclarationAffectationPatrimoineGroup;
if(Value('id').of(pj.cadreEIRLInformationsComplementaires.contenuDAP).contains('bienImmo') or Value('id').of(pj.cadreEIRLInformationsComplementairesBis.contenuDAPBis).contains('bienImmo') or Value('id').of($p2cmb.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.contenuDAP).contains('bienImmo')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAPActeNotarie);
}

if(Value('id').of(pj.cadreEIRLInformationsComplementaires.contenuDAP).contains('bienCommunIndivis') or Value('id').of(pj.cadreEIRLInformationsComplementairesBis.contenuDAPBis).contains('bienCommunIndivis') or Value('id').of($p2cmb.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.contenuDAP).contains('bienCommunIndivis')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAPAccordTiers);
}

// PJ 28P

if(Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('28P')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjInsaisissabiliteActeNotarie);
}

// PJ 30P

if(Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('30P') and Value('id').of(conjoint.objetModifStatut).eq('nouveauStatut') and Value('id').of(conjoint.objetModifConjoint).eq('conjointCollaborateur')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDConjoint);
}

if(Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('30P') and (Value('id').of(conjoint.objetModifStatut).eq('nouveauStatut') or Value('id').of(conjoint.objetModifStatut).eq('modificationStatut'))) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjChoixStatutConjoint);
}


// PJ 54P / 11P / 80P 

if(objet.domicileEntreprise or objet.entrepriseDomicile) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDomicile);
}

var pj=$p2cmb.cadre7EtablissementActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds ;
if((Value('id').of(pj).contains('EtablissementOrigineFondsCreation') 
	and (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')))
or ((Value('id').of(pj).contains('EtablissementOrigineFondsAchat')) 
and $p2cmb.cadre7EtablissementActiviteGroup.cadre4OrigineFonds.precedentExploitant.fondsArtisanal) 
or (Value('id').of(pj).contains('EtablissementOrigineFondsCocheAutre'))) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjEtablissementCreation);
}

if(Value('id').of(pj).contains('EtablissementOrigineFondsAchat') 
	and ($p2cmb.cadre7EtablissementActiviteGroup.cadre4OrigineFonds.precedentExploitant.cessionFonds)) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPlanCession);
}

// Egalement 61P et 64P

if(Value('id').of(pj).contains('EtablissementOrigineFondsLocationGerance')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre4OrigineFonds.geranceMandat.typeFonds).eq('typeFondsLocation')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjLocationGerance);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjLocationGerancePublication);
}

if(Value('id').of(pj).contains('EtablissementOrigineFondsGeranceMandat')
    or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre4OrigineFonds.geranceMandat.typeFonds).eq('typeFondsGerance')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjGeranceMandat);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjGeranceMandatPublication);
}

// Egalement 61P et 63P

if((Value('id').of(pj).contains('EtablissementOrigineFondsAchat') 
	and $p2cmb.cadre7EtablissementActiviteGroup.cadre4OrigineFonds.precedentExploitant.etablissementJournalAnnoncesLegalesNom != null)
	or Value('id').of($p2cmb.cadre6EtablissementGroup.infoEtablissementNew.origineAcquision).eq('achat')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjAchat);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjAchatPublication);
}

if(Value('id').of(pj).contains('EtablissementOrigineFondsDonation') 
	or Value('id').of($p2cmb.cadre6EtablissementGroup.infoEtablissementNew.origineAcquision).eq('donation')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDonation);
}
 
if(Value('id').of(pj).contains('EtablissementOrigineFondsDevolution') 
	or Value('id').of($p2cmb.cadre6EtablissementGroup.infoEtablissementNew.origineAcquision).eq('devolution')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDevolutionSuccessorale);
}

// PJ 70P
// Fondé de pouvoir 1
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee1.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee1.objetModifPersonneLiee).eq('partantPersonneLiee'))	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDPersonnePouvoir);
}

if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee1.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee1.objetModifPersonneLiee).eq('partantPersonneLiee')
	and not Value('id').of(personneLiee1.objetModifPersonneLiee).eq('modifPersonneLiee'))	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCPersonnePouvoir);
}

// Fondé de pouvoir 2
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee2.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee2.objetModifPersonneLiee).eq('partantPersonneLiee')
	and personneLiee1.autreDeclarationPersonneLiee or (Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')))	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDPersonnePouvoir2);
}

if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee2.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee2.objetModifPersonneLiee).eq('partantPersonneLiee')
	and not Value('id').of(personneLiee2.objetModifPersonneLiee).eq('modifPersonneLiee')	
	and personneLiee1.autreDeclarationPersonneLiee or (Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')))	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCPersonnePouvoir2);
}

// Fondé de pouvoir 3
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee3.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee3.objetModifPersonneLiee).eq('partantPersonneLiee')
	and personneLiee2.autreDeclarationPersonneLiee)	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDPersonnePouvoir3);
}

if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee3.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee3.objetModifPersonneLiee).eq('partantPersonneLiee')
	and not Value('id').of(personneLiee3.objetModifPersonneLiee).eq('modifPersonneLiee')	
	and personneLiee2.autreDeclarationPersonneLiee)	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCPersonnePouvoir3);
}

// Fondé de pouvoir 4
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee4.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee4.objetModifPersonneLiee).eq('partantPersonneLiee')
	and personneLiee3.autreDeclarationPersonneLiee)	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDPersonnePouvoir4);
}

if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee4.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee4.objetModifPersonneLiee).eq('partantPersonneLiee')
	and not Value('id').of(personneLiee4.objetModifPersonneLiee).eq('modifPersonneLiee')	
	and personneLiee3.autreDeclarationPersonneLiee)	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCPersonnePouvoir4);
}

// Fondé de pouvoir 5
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee5.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee5.objetModifPersonneLiee).eq('partantPersonneLiee')
	and personneLiee4.autreDeclarationPersonneLiee)	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDPersonnePouvoir5);
}

if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee5.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee5.objetModifPersonneLiee).eq('partantPersonneLiee')
	and not Value('id').of(personneLiee5.objetModifPersonneLiee).eq('modifPersonneLiee')	
	and personneLiee4.autreDeclarationPersonneLiee)	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCPersonnePouvoir5);
}

// Fondé de pouvoir 6
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee6.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee6.objetModifPersonneLiee).eq('partantPersonneLiee')
	and personneLiee5.autreDeclarationPersonneLiee)	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDPersonnePouvoir6);
}

if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee6.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee6.objetModifPersonneLiee).eq('partantPersonneLiee')
	and not Value('id').of(personneLiee6.objetModifPersonneLiee).eq('modifPersonneLiee')	
	and personneLiee5.autreDeclarationPersonneLiee)	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCPersonnePouvoir6);
}

// Fondé de pouvoir 7
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee7.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee7.objetModifPersonneLiee).eq('partantPersonneLiee')
	and personneLiee6.autreDeclarationPersonneLiee)	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDPersonnePouvoir7);
}

if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee7.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee7.objetModifPersonneLiee).eq('partantPersonneLiee')
	and not Value('id').of(personneLiee7.objetModifPersonneLiee).eq('modifPersonneLiee')	
	and personneLiee6.autreDeclarationPersonneLiee)	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCPersonnePouvoir7);
}

// Fondé de pouvoir 8
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee8.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee8.objetModifPersonneLiee).eq('partantPersonneLiee')
	and personneLiee7.autreDeclarationPersonneLiee)	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDPersonnePouvoir8);
}

if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee8.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee8.objetModifPersonneLiee).eq('partantPersonneLiee')
	and not Value('id').of(personneLiee8.objetModifPersonneLiee).eq('modifPersonneLiee')	
	and personneLiee7.autreDeclarationPersonneLiee)	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCPersonnePouvoir8);
}

// Fondé de pouvoir 9
if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee9.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee9.objetModifPersonneLiee).eq('partantPersonneLiee')
	and personneLiee8.autreDeclarationPersonneLiee)	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDPersonnePouvoir9);
}

if (((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee9.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and not Value('id').of(personneLiee9.objetModifPersonneLiee).eq('partantPersonneLiee')
	and not Value('id').of(personneLiee9.objetModifPersonneLiee).eq('modifPersonneLiee')	
	and personneLiee8.autreDeclarationPersonneLiee)	{
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDNCPersonnePouvoir9);
}

// PJ JQPA

var pj = $p2cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification1.jqpaSituation ;
if(Value('id').of(pj).eq('jqpaSituationDiplome')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDiplomes1);
}

if(Value('id').of(pj).eq('jqpaSituationExperience')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles1);
}

var pj=$p2cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification2.jqpaSituation ;
if(Value('id').of(pj).eq('jqpaSituationDiplome')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDiplomes2);
}

if(Value('id').of(pj).eq('jqpaSituationExperience')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles2);
}

var pj=$p2cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification3.jqpaSituation ;
if(Value('id').of(pj).eq('jqpaSituationDiplome')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDiplomes3);
}

if(Value('id').of(pj).eq('jqpaSituationExperience')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles3);
}

// PJ Activité réglementée

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P')
	or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')
	or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P')
	or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('17P')
	or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('22P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
	or Value('id').of(activite.activiteEvenement).eq('61P')
	or Value('id').of(activite.activiteEvenement).eq('61P62P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDActiviteReglementee);
}


//Remove old metas before insert
var metasToDelete = [];
var recordMetas = nash.record.meta() != null ? nash.record.meta().metas : null;
var recordMetasSize = recordMetas != null ? recordMetas.size() : 0;

for (var i = 0; i < recordMetasSize; i++) {
    if (recordMetas.get(i).name == 'document' && !recordMetas.get(i).value.contains("proxy")) {
        metasToDelete.push({'name':'document', 'value': recordMetas.get(i).value});
    }
}
nash.record.removeMeta(metasToDelete);

// Insert new metas
nash.record.meta(metas);


/*
 * Enregistrement du fichier (en mémoire)
 */

var finalDoc = cerfaDoc1.save('p2cmb_Modification.pdf');

var data = [ spec.createData({
    id : 'record',
    label : 'Déclaration de modification d\'une entreprise commerciale et/ou artisanale',
    description : 'Voici le formulaire obtenu à partir des données saisies.',
    type : 'FileReadOnly',
    value : [ finalDoc ]
}),  spec.createData({
    id : 'attachments',
    label : 'Pièces jointes',
    description : 'Pièces jointes ajoutées au formulaire.',
    type : 'FileReadOnly',
    value : pjUser
}) ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Déclaration de modification d\'une entreprise commerciale et/ou artisanale',
    groups : groups
});