//prepare info to send 

var adresseNew = $p2cmb.cadre6EtablissementGroup.infoEtablissementNew.adresseEtablissementNew;
var adresseOld = $p2cmb.cadre6EtablissementGroup.infoEtablissementOld.adresseEtablissementOld;
var adressePro = $p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.adresseEtablissementPrincipal;
var adresseEIRL = $p2cmb.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreRappelIdentificationEIRL.cadreEirlRappelAdresse;
var adresseDom = $p2cmb.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifDomicile.newAdresseDomicile;
var adresseFondsDonne = $p2cmb.cadreFondsDonneLocationGeranceGroup.cadreFondsDonneLocationGerance.adresseFondsDonneLocationGerance;

var algo = "trouver destinataire";

if ((Value('id').of($p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.immatriculation).contains('immatRM')
	or (not Value('id').of($p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.immatriculation).contains('immatRM')
	and (($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA != null
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	or ($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA != null
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	or ($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA != null
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.jqpaDomaineTransport).eq('jqpaTransportAutre')))))
	and not $p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.optionCMACCI) 	{
var secteur1 =  "ARTISANAL";
} else { var secteur1 =  "COMMERCIAL"; }

if (Value('id').of($p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.immatriculation).contains('immatRCS')
	or (not Value('id').of($p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.immatriculation).contains('immatRCS')
	and (Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.jqpaDomaineTransport).eq('jqpaTransportAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA).eq('nafaAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineAlimentation).eq('alimentationAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineAutresServices).eq('servicesAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineProduction).eq('productionAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.jqpaDomaineTransport).eq('jqpaTransportAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA).eq('nafaAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineAlimentation).eq('alimentationAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineAutresServices).eq('servicesAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineProduction).eq('productionAutre') 
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.jqpaDomaineTransport).eq('jqpaTransportAutre')))) {
	
var secteur2 =  "COMMERCIAL";
}

var typePersonne = "PP";

var formJuridique = $p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.formeJuridique ? "EIRL" : "EI";

var optionCMACCI = "NON";

var codeCommune = adresseNew.etablissementAdresseCommuneNew != null ? adresseNew.etablissementAdresseCommuneNew.getId() : 
				($p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification.domicileEntreprise ? adresseDom.personneLieeAdresseCommuneNew.getId() : 
				((Value('id').of($p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).eq('modifEIRL')
				and Value('id').of($p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEIRL.objetModificationEIRL).eq('modifAdresse')
				and not Value('id').of($p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).eq('situationPerso')
				and not Value('id').of($p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).eq('etablissement')
				and not Value('id').of($p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).eq('dateActivite')) ?	
				adresseEIRL.communeAdresseEirl.getId() : 
				((Value('id').of($p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).eq('etablissement') 
				and Value('id').of($p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEtablissement.objetEtablissement).eq('80P')) ? 
				adresseOld.etablissementAdresseCommuneOld.getId() : 
				((Value('id').of($p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEtablissement.objetEtablissement).eq('miseEnLocation') and adresseFondsDonne.fondsDonneAdresseCommune != null) ?
				adresseFondsDonne.fondsDonneAdresseCommune.getId() : adressePro.etablissementAdresseCommune.getId()))));

var attachement= "/4-signature/generated/proxyResult.files-1-PROXY_SIGNED_DOCUMENT.pdf";

return spec.create({
		id: 'prepareSend',
		label: "Préparation de la recherche du destinataire",
		groups: [spec.createGroup({
				id: 'view',
				label: "Informations",
				data: [spec.createData({
						id: 'algo',
						label: "Algo",
						type: 'String',
						mandatory: true,
						value: algo
					}), spec.createData({
						id: 'secteur1',
						label: "Secteur",
						type: 'String',
						value: secteur1
					}), spec.createData({
						id: 'typePersonne',
						label: "Type personne",
						type: 'String',
						value: typePersonne
					}), spec.createData({
						id: 'formJuridique',
						label: "Forme juridique",
						type: 'String',
						value: formJuridique
					}),spec.createData({
						id: 'optionCMACCI',
						label: "Option CMACCI",
						type: 'String',
						value: optionCMACCI
					}),spec.createData({
						id: 'codeCommune',
						label: "Code commune",
						type: 'String',
						value: codeCommune
					}) ,spec.createData({
					    id: 'attachement',
					    label: "Pièce jointe",
					    type: 'String',
					    value: attachement
					})
				]
			})]
});