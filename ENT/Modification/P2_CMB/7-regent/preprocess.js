function pad(s) { return (s < 10) ? '0' + s : s; }
function sanitize(s){return s.replaceAll("’","'").replaceAll("–","-").replaceAll("­","");}

var regEx = new RegExp('[0-9]{5}'); 
var objet= $p2cmb.cadre1ObjetModificationGroup.cadre1ObjetModification;
var identite= $p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification;
var modifIdentiteDeclarant = $p2cmb.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle;
var etablissement = $p2cmb.cadre6EtablissementGroup.infoEtablissementOld;
var newAdresseDom = $p2cmb.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifDomicile.newAdresseDomicile;
var newAdresseDom2 = $p2cmb.cadre6EtablissementGroup.infoEtablissementNew.adresseEtablissementNew;
var conjoint = $p2cmb.cadre3ModificationConjointGroup.cadre3ModificationConjoint;
var adresseConjoint = $p2cmb.cadre3ModificationConjointGroup.cadre3ModificationConjoint.adresseDomicileConjoint;
var exploitant1 = $p2cmb.cadreExploitantIndivisionGroup.cadreExploitantIndivision;
var exploitant2 = $p2cmb.cadreExploitantIndivisionGroup.cadreExploitantIndivision2;
var exploitant3 = $p2cmb.cadreExploitantIndivisionGroup.cadreExploitantIndivision3;
var activiteInfo = $p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite;
var fondsDonne = $p2cmb.cadreFondsDonneLocationGeranceGroup.cadreFondsDonneLocationGerance;
var etablissementNew = $p2cmb.cadre6EtablissementGroup.infoEtablissementNew;
var personneLiee = $p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir;
var personneLiee1 = $p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.infoPeronnePouvoir;
var personneLiee2 = $p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.infoPeronnePouvoir2;
var personneLiee3 = $p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.infoPeronnePouvoir3;
var personneLiee4 = $p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.infoPeronnePouvoir4;
var personneLiee5 = $p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.infoPeronnePouvoir5;
var personneLiee6 = $p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.infoPeronnePouvoir6;
var personneLiee7 = $p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.infoPeronnePouvoir7;
var personneLiee8 = $p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.infoPeronnePouvoir8;
var personneLiee9 = $p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.infoPeronnePouvoir9;
var adresseEntreprise = $p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.adresseEtablissementPrincipal;
var activite = $p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite;
var origine = $p2cmb.cadre7EtablissementActiviteGroup.cadre4OrigineFonds ;
var correspondance = $p2cmb.cadre8RensCompGroup.cadre8RensComp;
var signataire = $p2cmb.cadre9SignatureGroup.cadre9Signature;
var jqpa1 = $p2cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification1;																					
var jqpa2 = $p2cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification2;																					
var jqpa3 = $p2cmb.justificationQualificationProfessionnelleGroup.justificationQualificationProfessionnelle.justification3;																					
var declarationAffectation = $p2cmb.cadre4DeclarationAffectationPatrimoineGroup.cadreDeclarationAffectationPatrimoine;
var rappelIdentification = $p2cmb.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreRappelIdentificationEIRL;
var modifEIRL = $p2cmb.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL;                                         
var finDAP = $p2cmb.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.finEIRLGroup;
var intentionPoursuite = $p2cmb.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.poursuiteEIRLGroup;
var fiscalEIRL = $p2cmb.cadre4DeclarationAffectationPatrimoineGroup.cadre3OptionsFiscalesEIRLnonME;
var fiscalEIRLME = $p2cmb.cadre4DeclarationAffectationPatrimoineGroup.cadre3OptionsFiscalesEIRLME;
var social = $p2cmb.cadreDeclarationSocialeGroup.cadreDeclarationSociale;


var regentVersion = "V2008.11";  // (V2008.11, V2016.02) 
var eventRegent = []; 
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P')) {
eventRegent.push("10P");
}
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) {
eventRegent.push("15P");
}
if ((Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') or objet.entrepriseDomicile)) {
eventRegent.push("16P");
}
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('17P')) { 
eventRegent.push("17P");
}
if (Value('id').of(objet.objetModification).contains('dateActivite') and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 != null or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 != null)) {
eventRegent.push("20P");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')) {
eventRegent.push("21P");
}
if (Value('id').of(objet.cadreObjet22P.infoDeces).contains('22P')) { 
eventRegent.push("22P");
}
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('23P')) { 
eventRegent.push("23P");
}
if ((not Value('id').of($p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.immatriculation).contains('immatRM')
	and (($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA != null
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	or ($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA != null
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	or ($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA != null
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.jqpaDomaineTransport).eq('jqpaTransportAutre')))
	and not $p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.optionCMACCI)
	or (not Value('id').of($p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.immatriculation).contains('immatRCS')
	and (Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.jqpaDomaineTransport).eq('jqpaTransportAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA).eq('nafaAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineAlimentation).eq('alimentationAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineAutresServices).eq('servicesAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineProduction).eq('productionAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.jqpaDomaineTransport).eq('jqpaTransportAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA).eq('nafaAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineAlimentation).eq('alimentationAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineAutresServices).eq('servicesAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineProduction).eq('productionAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.jqpaDomaineTransport).eq('jqpaTransportAutre')
	or $p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.optionCMACCI))
	or (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')) 
	or objet.domicileEntreprise
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise)
	and not objet.transfertDepartement)) { 
eventRegent.push("24P");
}
if (Value('id').of(objet.objetModification).contains('modifEIRL') or objet.entrepriseEIRL) {
eventRegent.push("25P");
}
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('28P')) {
eventRegent.push("28P");
}
if (Value('id').of(objet.objetModification).eq('situationPerso')
	and Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).eq('30P')
	and Value('id').of(conjoint.objetModifConjoint).eq('conjointSalarie')
	and (Value('id').of(conjoint.objetModifStatut).eq('suppressionStatut')
	or Value('id').of(conjoint.objetModifStatut).eq('nouveauStatut'))) { 
eventRegent.push("29P");
}
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('30P')
	and (Value('id').of(conjoint.objetModifConjoint).eq('conjointCollaborateur')
	or Value('id').of(conjoint.objetModifStatut).eq('modificationStatut'))) { 
eventRegent.push("30P");
}
if (Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('31P')) { 
eventRegent.push("31P");
}
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('37P')) { 
eventRegent.push("37P");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetcessationActivite).eq('40P')) {
eventRegent.push("40P");
}
if (Value('id').of(objet.cadreObjet22P.infoDeces).contains('42P')) { 
eventRegent.push("42P");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetcessationActivite).eq('43P')) {
eventRegent.push("43P");
}
if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')) 
	or objet.domicileEntreprise
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise) {
eventRegent.push("11P");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') 
	or Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementReprise') 
	or Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementReprise')) {
eventRegent.push("53P");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')
	and Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementOuverture')) 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')
	and Value('id').of(etablissementNew.origineEtablissement2).eq('etablissementOuverture')
	and not Value('id').of(etablissement.destinationEtablissement2).eq('vendu') and not Value('id').of(etablissement.destinationEtablissement2).eq('ferme') and not Value('id').of(etablissement.destinationEtablissement2).eq('autre'))
	or objet.domicileEntreprise
	or (objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise 
	and Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementOuverture'))) {
eventRegent.push("54P");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')
	and Value('id').of(etablissementNew.origineEtablissement2).eq('etablissementOuverture')
	and (Value('id').of(etablissement.destinationEtablissement2).eq('vendu') or Value('id').of(etablissement.destinationEtablissement2).eq('ferme') or Value('id').of(etablissement.destinationEtablissement2).eq('autre'))) {
eventRegent.push("56");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')
	and (Value('id').of(etablissement.destinationEtablissement1).eq('vendu') or Value('id').of(etablissement.destinationEtablissement1).eq('ferme') or Value('id').of(etablissement.destinationEtablissement1).eq('autre'))) 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')
	and not Value('id').of(etablissementNew.origineEtablissement2).eq('etablissementOuverture')
	and (Value('id').of(etablissement.destinationEtablissement2).eq('vendu') or Value('id').of(etablissement.destinationEtablissement2).eq('ferme') or Value('id').of(etablissement.destinationEtablissement2).eq('autre'))) 
	or objet.domicileEntreprise) {
eventRegent.push("80P");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetmiseEnLocation.infosMiseEnLocation).eq('82P') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')
	and Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationPartielle')) 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')
	and Value('id').of(etablissement.destinationEtablissement2).eq('miseEnLocationPartielle'))) {
eventRegent.push("82P");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetmiseEnLocation.infosMiseEnLocation).eq('83P') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')
	and Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationCessation'))) {
eventRegent.push("83P");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetmiseEnLocation.infosMiseEnLocation).eq('84P') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')
	and Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationTotale'))) {
eventRegent.push("84P");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P') 
	or activiteInfo.modifEtablissementEnseigne
	or objet.cadreObjetModificationEtablissement.cadreObjet64P.enseigneRenouvellement
	or objet.cadreObjetModificationEtablissement.cadreObjet63P.enseigneAcquisition
	or objet.cadreObjetModificationEtablissement.cadreObjet68P.enseigneChangement) {
eventRegent.push("60P");
}
if (Value('id').of(activite.activiteEvenement).eq('61P') or Value('id').of(activite.activiteEvenement).eq('61P62P')
	or activiteInfo.modifActiviteReprise) {
eventRegent.push("61P");
}
if ((Value('id').of(activite.activiteEvenement).eq('62P') or Value('id').of(activite.activiteEvenement).eq('61P62P'))) {
eventRegent.push("62P");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P') 
	or Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementAcquisition') 
	or Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementAcquisition')) {
eventRegent.push("63P");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P') 
	or Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementRenouvellement') 
	or Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementRenouvellement')
	or objet.cadreObjetModificationEtablissement.cadreObjet68P.renouvellementChangement) {
eventRegent.push("64P");
}
if (Value('id').of(activite.activiteEvenement).eq('67P')) {
eventRegent.push("67P");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P') 
	or objet.cadreObjetModificationEtablissement.cadreObjet64P.changementRenouvellement) {
eventRegent.push("68P");
}
if (Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')) { 
eventRegent.push("70P");
}
if (Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')) { 
eventRegent.push("77P");
}


// Valeurs à compléter par Directory  
var authorityType = "CFE"; // (CFE, TDR) 
var authorityId = _INPUT_.dataGroup.codeEDI; // (code EDI) 


//Ajout du type de la deuxième autorité
var authorityType2 = "TDR"; // (CFE, TDR) 
//Ajout du code de la deuxième autorité
var authorityId2 = _INPUT_.dataGroup.codeEDI2; // (code EDI) 


var regentFields = {}; 

// A mettre toujours sous "Z1611"
regentFields['/REGENT-XML/Emetteur']="Z1611";

// A récuperer depuis directory
regentFields['/REGENT-XML/Destinataire']= authorityId;

// Completé par directory
regentFields['/REGENT-XML/DateHeureEmission']= null;
regentFields['/REGENT-XML/VersionMessage']= null;
regentFields['/REGENT-XML/VersionNorme']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Specification/NomService']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Specification/VersionService']= null;

					// Groupe GDF : Groupe données de service
					
// Sous groupe IDF : Identification de la formalité

// Généré par destiny
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C01']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C03']= "0";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C04']= null;
if (Value('id').of($p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.immatriculation).contains('immatRM') 
	or (($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA != null
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	or ($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA != null
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	or ($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA != null
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	and not $p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.optionCMACCI)) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05']= "M";
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05']= "C";
}	

// Sous groupe EDF : Evènement déclaré

var occurrencesC10 = [];
var occurrence = {};

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P')) {
occurrence['C10.1'] = '10P';
if(modifIdentiteDeclarant.modifIdentite.modifDateIdentite != null) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifIdentite.modifDateIdentite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[1]/C10.2']= date;
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) {
occurrence['C10.1'] = '15P';
if(modifIdentiteDeclarant.modifIdentite.modifDateIdentite != null) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifIdentite.modifDateIdentite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[2]/C10.2']= date;
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') or objet.entrepriseDomicile) {
occurrence['C10.1'] = '16P';
if (modifIdentiteDeclarant.modifDomicile.modifDateDomicile != null and Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P')) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[6]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (etablissement.modifDateAdresseEntreprise != null and objet.entrepriseDomicile) {
	var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[6]/C10.2']= date;
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('17P')) {
occurrence['C10.1'] = '17P';
if(modifIdentiteDeclarant.modifNationalite.modifDateNationalite != null) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifNationalite.modifDateNationalite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[3]/C10.2']= date;
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(objet.objetModification).contains('dateActivite') and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 != null or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 != null)) {
occurrence['C10.1'] = '20P';
if (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 != null) {
	var dateTemp = new Date(parseInt(objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[5]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 != null) {
	var dateTemp = new Date(parseInt(objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[5]/C10.2']= date;
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')) {
occurrence['C10.1'] = '21P';
if (objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise != null) {
	var dateTemp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[5]/C10.2']= date;
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(objet.cadreObjet22P.infoDeces).contains('22P')) { 
occurrence['C10.1'] = '22P';
if (objet.cadreObjet22P.modifDateDeces != null) {
	var dateTemp = new Date(parseInt(objet.cadreObjet22P.modifDateDeces.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('23P') and (objet.cadreObjetRenouvellementMaintienRCS.modifDateRenouvellementMaintienRCS1 != null or objet.cadreObjetRenouvellementMaintienRCS.modifDateRenouvellementMaintienRCS2 != null)) {
occurrence['C10.1'] = '23P';
if (objet.cadreObjetRenouvellementMaintienRCS.modifDateRenouvellementMaintienRCS1 != null) {
	var dateTemp = new Date(parseInt(objet.cadreObjetRenouvellementMaintienRCS.modifDateRenouvellementMaintienRCS1.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[5]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (objet.cadreObjetRenouvellementMaintienRCS.modifDateRenouvellementMaintienRCS2 != null) {
	var dateTemp = new Date(parseInt(objet.cadreObjetRenouvellementMaintienRCS.modifDateRenouvellementMaintienRCS2.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[5]/C10.2']= date;
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if ((not Value('id').of($p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.immatriculation).contains('immatRM')
	and (($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA != null
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	or ($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA != null
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.jqpaDomaineTransport).eq('jqpaTransportAutre'))
	or ($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA != null
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA).eq('nafaAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineAlimentation).eq('alimentationAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineAutresServices).eq('servicesAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineProduction).eq('productionAutre')
	and not Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.jqpaDomaineTransport).eq('jqpaTransportAutre')))
	and not $p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.optionCMACCI)
	or (not Value('id').of($p2cmb.cadre1RappelIdentificationGroup.cadre1RappelIdentification.immatriculation).contains('immatRCS')
	and (Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.domaineAdjonctionNAFA).eq('nafaAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineAlimentation).eq('alimentationAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineAutresServices).eq('servicesAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.nafaDomaineProduction).eq('productionAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup1.jqpaDomaineTransport).eq('jqpaTransportAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.domaineAdjonctionNAFA).eq('nafaAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineAlimentation).eq('alimentationAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineAutresServices).eq('servicesAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.nafaDomaineProduction).eq('productionAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup2.jqpaDomaineTransport).eq('jqpaTransportAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.domaineAdjonctionNAFA).eq('nafaAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineAlimentation).eq('alimentationAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineAutresServices).eq('servicesAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.nafaDomaineProduction).eq('productionAutre')
	or Value('id').of($p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.nouvelleActiviteGroup3.jqpaDomaineTransport).eq('jqpaTransportAutre')
	or $p2cmb.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite.optionCMACCI))
	or (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')
	and (Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementOuverture')
	or Value('id').of(etablissementNew.origineEtablissement2).eq('etablissementOuverture'))) 
	or objet.domicileEntreprise
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise)
	and not objet.transfertDepartement)) { 
occurrence['C10.1'] = '24P';
if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or activiteInfo.modifActiviteTransfert or activiteInfo.modifActiviteReprise
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.activiteReprise
	or objet.cadreObjetModificationEtablissement.cadreObjet63P.activiteAcquisition
	or objet.cadreObjetModificationEtablissement.cadreObjet64P.activiteRenouvellement)
	and activite.modifDateActivite != null)	{
	var dateTemp = new Date(parseInt(activite.modifDateActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and (Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementOuverture')
	or Value('id').of(etablissementNew.origineEtablissement2).eq('etablissementOuverture')) and etablissement.modifDateAdresseEntreprise != null) {
    var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') and etablissementNew.modifDateAdresseOuverture != null) {
	var dateTemp = new Date(parseInt(etablissementNew.modifDateAdresseOuverture.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if (objet.domicileEntreprise and modifIdentiteDeclarant.modifDomicile.modifDateDomicile != null) {
    var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P') and not objet.cadreObjetModificationEtablissement.cadreObjet21P.activiteReprise and objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise != null) {
    var dateTemp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') and not activiteInfo.modifActiviteReprise and etablissementNew.modifDateAdresseReprise != null) {
    var dateTemp = new Date(parseInt(etablissementNew.modifDateAdresseReprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
}    
    
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(objet.objetModification).contains('modifEIRL') or objet.entrepriseEIRL) {
occurrence['C10.1'] = '25P';
if (declarationAffectation.modifDateDeclarationEIRL != null) {
	var dateTemp = new Date(parseInt(declarationAffectation.modifDateDeclarationEIRL.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[7]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (modifEIRL.modifDateEIRL != null) {
	var dateTemp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[7]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (Value('id').of(objet.cadreObjet22P.infoDeces).contains('22P') and objet.cadreObjet22P.modifDateDeces != null) {
	var dateTemp = new Date(parseInt(objet.cadreObjet22P.modifDateDeces.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[7]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (finDAP.finEIRLDeclaration.modifDateFinEIRL != null) {
	var dateTemp = new Date(parseInt(finDAP.finEIRLDeclaration.modifDateFinEIRL.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[7]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (objet.entrepriseEIRL and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissement.modifDateAdresseEntreprise != null) {
	var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[7]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (objet.entrepriseEIRL and modifIdentiteDeclarant.modifDomicile.modifDateDomicile != null) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[7]/C10.2']= date;
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('28P')) {
occurrence['C10.1'] = '28P';
if (modifIdentiteDeclarant.modifInsaisissabilite.modifdateInsaisissabilite != null) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifInsaisissabilite.modifdateInsaisissabilite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[4]/C10.2']= date;
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(objet.objetModification).eq('situationPerso')
	and Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).eq('30P')
	and Value('id').of(conjoint.objetModifConjoint).eq('conjointSalarie')
	and (Value('id').of(conjoint.objetModifStatut).eq('suppressionStatut')
	or Value('id').of(conjoint.objetModifStatut).eq('nouveauStatut'))) { 
occurrence['C10.1'] = '29P';
if (conjoint.modifDateConjoint !== null) {
	var dateTemp = new Date(parseInt(conjoint.modifDateConjoint.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[4]/C10.2']= date;
occurrence['C10.2'] = date;
}
if (Value('id').of(conjoint.objetModifStatut).eq('nouveauStatut')) { 
occurrence['C10.3'] = "Déclaration du statut de conjoint salarié";
} else {
occurrence['C10.3'] = "Suppression du statut de conjoint salarié";
}
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('30P')
	and (Value('id').of(conjoint.objetModifConjoint).eq('conjointCollaborateur')
	or Value('id').of(conjoint.objetModifStatut).eq('modificationStatut'))) { 
occurrence['C10.1'] = '30P';
if (conjoint.modifDateConjoint != null) {
	var dateTemp = new Date(parseInt(conjoint.modifDateConjoint.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[4]/C10.2']= date;
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('31P')) {
occurrence['C10.1'] = '31P';
if ($p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.modifDatePersonneLiee != null and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant')) {
	var dateTemp = new Date(parseInt($p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.modifDatePersonneLiee.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[4]/C10.2']= date;
occurrence['C10.2'] = date;
} else if ($p2cmb.cadreExploitantIndivisionGroup.cadreExploitantIndivision.modifDateExploitantIndivision != null and Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant')) {
	var dateTemp = new Date(parseInt($p2cmb.cadreExploitantIndivisionGroup.cadreExploitantIndivision.modifDateExploitantIndivision.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[4]/C10.2']= date;
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('37P')) { 
occurrence['C10.1'] = '37P';
if (modifIdentiteDeclarant.modifContratAppui.modifDateAppui != null) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifContratAppui.modifDateAppui.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[5]/C10.2']= date;
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetcessationActivite.infosCessation).eq('40P')) {
occurrence['C10.1'] = '40P';
if(objet.cadreObjetModificationEtablissement.cadreObjetcessationActivite.dateCessationActivite != null) {
	var dateTemp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjetcessationActivite.dateCessationActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[8]/C10.2']= date;
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(objet.cadreObjet22P.infoDeces).contains('42P')) { 
occurrence['C10.1'] = '42P';
if (objet.cadreObjet22P.modifDateDeces != null) {
	var dateTemp = new Date(parseInt(objet.cadreObjet22P.modifDateDeces.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetcessationActivite.infosCessation).eq('43P')) {
occurrence['C10.1'] = '43P';
if(objet.cadreObjetModificationEtablissement.cadreObjetcessationActivite.dateCessationActivite != null) {
	var dateTemp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjetcessationActivite.dateCessationActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[8]/C10.2']= date;
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {};
}

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')) or objet.domicileEntreprise or 
(Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P') and objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise)) {
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[8]/C10.1']= "11P";
occurrence['C10.1'] = '11P';
if(etablissement.modifDateAdresseEntreprise != null and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).contains('transfert')) {
	var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[8]/C10.2']= date;
occurrence['C10.2'] = date;
} else if(modifIdentiteDeclarant.modifDomicile.modifDateDomicile != null and objet.domicileEntreprise) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[8]/C10.2']= date;
occurrence['C10.2'] = date;
} else if(objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise != null and objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise) {
	var dateTemp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[8]/C10.2']= date;
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') 
	or Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementReprise') 
	or Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementReprise')) {
occurrence['C10.1'] = '53P';
if(etablissement.modifDateAdresseEntreprise != null and Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementReprise')) {
	var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[9]/C10.2']= date;
occurrence['C10.2'] = date;
} else if(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise != null and Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementReprise')) {
	var dateTemp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[9]/C10.2']= date;
occurrence['C10.2'] = date;
} else if(etablissementNew.modifDateAdresseReprise != null and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P')) {
	var dateTemp = new Date(parseInt(etablissementNew.modifDateAdresseReprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[9]/C10.2']= date;
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') 
	or Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementOuverture') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')
	and Value('id').of(etablissementNew.origineEtablissement2).eq('etablissementOuverture')
	and not Value('id').of(etablissement.destinationEtablissement2).eq('vendu') and not Value('id').of(etablissement.destinationEtablissement2).eq('ferme') and not Value('id').of(etablissement.destinationEtablissement2).eq('autre'))
	or objet.domicileEntreprise
	or (objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise 
	and Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementOuverture'))) {
occurrence['C10.1'] = '54P';
if(etablissement.modifDateAdresseEntreprise != null and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).contains('transfert')) {
	var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[9]/C10.2']= date;
occurrence['C10.2'] = date;
} else if(modifIdentiteDeclarant.modifDomicile.modifDateDomicile != null and objet.domicileEntreprise) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[9]/C10.2']= date;
occurrence['C10.2'] = date;
} else if(etablissementNew.modifDateAdresseOuverture != null and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')) {
	var dateTemp = new Date(parseInt(etablissementNew.modifDateAdresseOuverture.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[9]/C10.2']= date;
occurrence['C10.2'] = date;
} else if(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise != null and Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementOuverture')) {
	var dateTemp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[9]/C10.2']= date;
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')
	and Value('id').of(etablissementNew.origineEtablissement2).eq('etablissementOuverture')
	and (Value('id').of(etablissement.destinationEtablissement2).eq('vendu') or Value('id').of(etablissement.destinationEtablissement2).eq('ferme')	or Value('id').of(etablissement.destinationEtablissement2).eq('autre'))) {
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[10]/C10.1']= "56P";
occurrence['C10.1'] = '56P';
if(etablissement.modifDateAdresseEntreprise != null) {
	var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[10]/C10.2']= date;
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')
	and (Value('id').of(etablissement.destinationEtablissement1).eq('vendu') or Value('id').of(etablissement.destinationEtablissement1).eq('ferme') or Value('id').of(etablissement.destinationEtablissement1).eq('autre'))) 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')
	and not Value('id').of(etablissementNew.origineEtablissement2).eq('etablissementOuverture')
	and (Value('id').of(etablissement.destinationEtablissement2).eq('vendu') or Value('id').of(etablissement.destinationEtablissement2).eq('ferme') or Value('id').of(etablissement.destinationEtablissement2).eq('autre'))) 
	or objet.domicileEntreprise) {
occurrence['C10.1'] = '80P';
if(modifIdentiteDeclarant.modifDomicile.modifDateDomicile != null and objet.domicileEntreprise) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[9]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (etablissement.modifDateAdresseEntreprise != null) {
	var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[10]/C10.2']= date;
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetmiseEnLocation.infosMiseEnLocation).eq('82P') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')
	and Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationPartielle')) 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')
	and Value('id').of(etablissement.destinationEtablissement2).eq('miseEnLocationPartielle'))) {
occurrence['C10.1'] = '82P';
if (etablissement.modifDateAdresseEntreprise != null) {
	var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[10]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetmiseEnLocation.infosMiseEnLocation).eq('82P') and fondsDonne.dateMiseEnLocation != null) {
	var dateTemp = new Date(parseInt(fondsDonne.dateMiseEnLocation.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[10]/C10.2']= date;
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetmiseEnLocation.infosMiseEnLocation).eq('83P') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')
	and Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationCessation'))) {
occurrence['C10.1'] = '83P';
if (etablissement.modifDateAdresseEntreprise != null) {
	var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[10]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetmiseEnLocation.infosMiseEnLocation).eq('83P') and fondsDonne.dateMiseEnLocation != null) {
	var dateTemp = new Date(parseInt(fondsDonne.dateMiseEnLocation.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[10]/C10.2']= date;
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetmiseEnLocation.infosMiseEnLocation).eq('84P') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')
	and Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationTotale'))) {
occurrence['C10.1'] = '84P';
if (etablissement.modifDateAdresseEntreprise != null) {
	var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[10]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetmiseEnLocation.infosMiseEnLocation).eq('84P') and fondsDonne.dateMiseEnLocation != null) {
	var dateTemp = new Date(parseInt(fondsDonne.dateMiseEnLocation.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[10]/C10.2']= date;
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P') 
	or activiteInfo.modifEtablissementEnseigne
	or objet.cadreObjetModificationEtablissement.cadreObjet64P.enseigneRenouvellement
	or objet.cadreObjetModificationEtablissement.cadreObjet63P.enseigneAcquisition
	or objet.cadreObjetModificationEtablissement.cadreObjet68P.enseigneChangement) {
occurrence['C10.1'] = '60P';
if (activiteInfo.cadreEtablissementIdentification.modifDateIdentification != null) {
	var dateTemp = new Date(parseInt(activiteInfo.cadreEtablissementIdentification.modifDateIdentification.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[11]/C10.2']= date;
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(activite.activiteEvenement).eq('61P') or Value('id').of(activite.activiteEvenement).eq('61P62P')
	or activiteInfo.modifActiviteReprise) {
occurrence['C10.1'] = '61P';
if (activite.modifDateActivite != null) {
	var dateTemp = new Date(parseInt(activite.modifDateActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[11]/C10.2']= date;
occurrence['C10.2'] = date;
}  
occurrencesC10.push(occurrence);
occurrence = {};
}

if ((Value('id').of(activite.activiteEvenement).eq('62P') or Value('id').of(activite.activiteEvenement).eq('61P62P'))) {
occurrence['C10.1'] = '62P';
if (activite.modifDateActivite != null) {
	var dateTemp = new Date(parseInt(activite.modifDateActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[11]/C10.2']= date;
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P') 
	or Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementAcquisition') 
	or Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementAcquisition')) {
occurrence['C10.1'] = '63P';
if (etablissement.modifDateAdresseEntreprise != null and Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementAcquisition')) {
	var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[9]/C10.2']= date;
occurrence['C10.2'] = date;
} else if(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise != null and Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementAcquisition')) {
	var dateTemp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[9]/C10.2']= date;
occurrence['C10.2'] = date;
} else if(objet.cadreObjetModificationEtablissement.cadreObjet63P.dateAcquisitionFonds != null and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P')) {
	var dateTemp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet63P.dateAcquisitionFonds.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[9]/C10.2']= date;
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P') 
	or Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementRenouvellement') 
	or Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementRenouvellement')
	or objet.cadreObjetModificationEtablissement.cadreObjet68P.renouvellementChangement) {
occurrence['C10.1'] = '64P';
if (etablissement.modifDateAdresseEntreprise != null and Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementRenouvellement')) {
	var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[9]/C10.2']= date;
occurrence['C10.2'] = date;
} else if(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise != null and Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementRenouvellement')) {
	var dateTemp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[9]/C10.2']= date;
occurrence['C10.2'] = date;
} else if(objet.cadreObjetModificationEtablissement.cadreObjet64P.dateRenouvellementContrat != null and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P')) {
	var dateTemp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet64P.dateRenouvellementContrat.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[9]/C10.2']= date;
occurrence['C10.2'] = date;
} else if(objet.cadreObjetModificationEtablissement.cadreObjet68P.dateLocataireChangement != null and objet.cadreObjetModificationEtablissement.cadreObjet68P.renouvellementChangement) {
	var dateTemp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet68P.dateLocataireChangement.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[9]/C10.2']= date;
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(activite.activiteEvenement).eq('67P')) {
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[13]/C10.1']= "67P";
occurrence['C10.1'] = '67P';
if (activite.modifDateActivite != null) {
	var dateTemp = new Date(parseInt(activite.modifDateActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[11]/C10.2']= date;
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P') 
	or objet.cadreObjetModificationEtablissement.cadreObjet64P.changementRenouvellement) {
occurrence['C10.1'] = '68P';
if (objet.cadreObjetModificationEtablissement.cadreObjet64P.dateRenouvellementContrat != null and objet.cadreObjetModificationEtablissement.cadreObjet64P.changementRenouvellement) {
	var dateTemp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet64P.dateRenouvellementContrat.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[9]/C10.2']= date;
occurrence['C10.2'] = date;
} else if(objet.cadreObjetModificationEtablissement.cadreObjet68P.dateLocataireChangement != null and Value('id').of(objet.cadreObjetModificationEtablissement).eq('68')) {
	var dateTemp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet68P.dateLocataireChangement.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[9]/C10.2']= date;
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')) {
occurrence['C10.1'] = '70P';
if ($p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.modifDatePersonneLiee != null) {
	var dateTemp = new Date(parseInt($p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.modifDatePersonneLiee.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[4]/C10.2']= date;
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')) {
occurrence['C10.1'] = '77P';
if ($p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.modifDatePersonneLiee != null) {
	var dateTemp = new Date(parseInt($p2cmb.cadrePeronnePouvoirGroup.cadrePeronnePouvoir.modifDatePersonneLiee.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[4]/C10.2']= date;
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {}; 
}

occurrencesC10.forEach(function (value, i) {
	var idx = i+1;
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[' + idx + ']/C10.1']= value['C10.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[' + idx + ']/C10.2']= value['C10.2'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[' + idx + ']/C10.3']= value['C10.3'];
});

// Sous groupe DMF : Destinataire de la formalité
   
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[1]']= authorityId;
if (authorityId != "" and authorityId2 != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[2]']= authorityId2;
}

// Sous groupe ADF :  adresse de correspondance

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C36']= correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance != null ? 
																	 correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance : 
																	((modifIdentiteDeclarant.modifIdentite.modifNouveauNomUsage != null 
																	and Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) ?
																	(modifIdentiteDeclarant.modifIdentite.modifNouveauNomUsage 
																	+ ' ' + (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom') ? modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms[0] : identite.personneLieePersonnePhysiquePrenom1[0])) :
																	((identite.personneLieePersonnePhysiqueNomUsage != null
																	and not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) ?
																	(identite.personneLieePersonnePhysiqueNomUsage 
																	+ ' ' + (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom') ? modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms[0] : identite.personneLieePersonnePhysiquePrenom1[0])) :
																	((Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifNomNaissance') ? modifIdentiteDeclarant.modifIdentite.modifNouveauNomNaissance : identite.personneLieePersonnePhysiqueNomNaissance) + ' ' + (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom') ? modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms[0] : identite.personneLieePersonnePhysiquePrenom1[0]))));
																	
if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']= correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune.getId();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('prof')) {
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']= adresseEntreprise.etablissementAdresseCommune.getId();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']=etablissement.adresseEtablissementOld.etablissementAdresseCommuneOld.getId();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']=etablissementNew.adresseEtablissementNew.etablissementAdresseCommuneNew.getId();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('domi')) {
if (Value('id').of(newAdresseDom.personneLieeAdressePaysNew).eq('FRXXXXX') or newAdresseDom.personneLieeAdresseCommuneNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']= newAdresseDom.personneLieeAdresseCommuneNew.getId();
} else if (not Value('id').of(newAdresseDom.personneLieeAdressePaysNew).eq('FRXXXXX') and newAdresseDom.personneLieeAdresseVilleNew != null) {
	var idPays = newAdresseDom.personneLieeAdressePaysNew.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']= result;
}	
} else if (Value('id').of(correspondance.adresseCorrespond).eq('fondsDonneEtablissement')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']= fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseCommune.getId();
} 
																	
if ((Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adresseEntreprise.etablissementAdresseNumeroVoie != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement') and etablissement.adresseEtablissementOld.etablissementAdresseNumeroVoieOld != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') and etablissementNew.adresseEtablissementNew.etablissementAdresseNumeroVoieNew != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('domi') and newAdresseDom.personneLieeAdresseVoieNew != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('fondsDonneEtablissement') and fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseNumeroVoie != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.5']= Value('id').of(correspondance.adresseCorrespond).eq('autre') ? correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance :
																			(Value('id').of(correspondance.adresseCorrespond).eq('prof') ? adresseEntreprise.etablissementAdresseNumeroVoie : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement') ? etablissement.adresseEtablissementOld.etablissementAdresseNumeroVoieOld : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('fondsDonneEtablissement') ? fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseNumeroVoie : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('domi') ? newAdresseDom.personneLieeAdresseVoieNew :  
																			etablissementNew.adresseEtablissementNew.etablissementAdresseNumeroVoieNew))));
}
	
if (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance != null) {
   var monId1 = Value('id').of(correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId1;  
} else if (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adresseEntreprise.etablissementAdresseIndiceVoie != null) {
   var monId2 = Value('id').of(adresseEntreprise.etablissementAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId2;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement') and etablissement.adresseEtablissementOld.etablissementAdresseIndiceVoieOld != null) {
   var monId3 = Value('id').of(etablissement.adresseEtablissementOld.etablissementAdresseIndiceVoieOld)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId3;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') and etablissementNew.adresseEtablissementNew.etablissementAdresseIndiceVoieNew != null) {
   var monId4 = Value('id').of(etablissementNew.adresseEtablissementNew.etablissementAdresseIndiceVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId4;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('domi') and newAdresseDom.personneLieeAdresseIndiceVoieNew != null) {
	var monId5 = Value('id').of(newAdresseDom.personneLieeAdresseIndiceVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId5;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('fondsDonneEtablissement') and fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseIndiceVoie != null) {
	var monId6 = Value('id').of(fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId6;
}

if ((Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement') and etablissement.adresseEtablissementOld.etablissementAdresseDistriutionSpecialeVoieOld != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') and etablissementNew.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('domi') and newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null) 
	or (Value('id').of(correspondance.adresseCorrespond).eq('fondsDonneEtablissement') and fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseDistriutionSpecialeVoie != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.7']= Value('id').of(correspondance.adresseCorrespond).eq('autre') ? correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance :
																			(Value('id').of(correspondance.adresseCorrespond).eq('prof') ? adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement') ? etablissement.adresseEtablissementOld.etablissementAdresseDistriutionSpecialeVoieOld : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('fondsDonneEtablissement') ? fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseDistriutionSpecialeVoie : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('domi') ? newAdresseDom.personneLieeAdresseDistriutionSpecialeNew :  
																			etablissementNew.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew))));
}

if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8']= correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('prof')) {
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8']= adresseEntreprise.etablissementAdresseCodePostal;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8']=etablissement.adresseEtablissementOld.etablissementAdresseCodePostalOld;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8']=etablissementNew.adresseEtablissementNew.etablissementAdresseCodePostalNew;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('domi')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8']= newAdresseDom.personneLieeAdresseCodePostalNew != null ? newAdresseDom.personneLieeAdresseCodePostalNew : '.';
}  else if (Value('id').of(correspondance.adresseCorrespond).eq('fondsDonneEtablissement')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8']= fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseCodePostal;
}

if ((Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adresseEntreprise.etablissementAdresseComplementVoie != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement') and etablissement.adresseEtablissementOld.etablissementAdresseComplementVoieOld != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') and etablissementNew.adresseEtablissementNew.etablissementAdresseComplementVoieNew != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('domi') and newAdresseDom.personneLieeAdresseComplementAdresseNew != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('fondsDonneEtablissement') and fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseComplementVoie != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.10']= Value('id').of(correspondance.adresseCorrespond).eq('autre') ? correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance :
																			(Value('id').of(correspondance.adresseCorrespond).eq('prof') ? adresseEntreprise.etablissementAdresseComplementVoie : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement') ? etablissement.adresseEtablissementOld.etablissementAdresseComplementVoieOld : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('fondsDonneEtablissement') ? fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseComplementVoie : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('domi') ? newAdresseDom.personneLieeAdresseComplementAdresseNew :  
																			etablissementNew.adresseEtablissementNew.etablissementAdresseComplementVoieNew))));
}

if (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance != null) {
   var monId1 = Value('id').of(correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId1;  
} else if (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adresseEntreprise.etablissementAdresseTypeVoie != null) {
   var monId2 = Value('id').of(adresseEntreprise.etablissementAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId2;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement') and etablissement.adresseEtablissementOld.etablissementAdresseTypeVoieOld != null) {
   var monId3 = Value('id').of(etablissement.adresseEtablissementOld.etablissementAdresseTypeVoieOld)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId3;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') and etablissementNew.adresseEtablissementNew.etablissementAdresseTypeVoieNew != null) {
   var monId4 = Value('id').of(etablissementNew.adresseEtablissementNew.etablissementAdresseTypeVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId4;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('domi') and newAdresseDom.personneLieeAdresseTypeVoieNew != null) {
	var monId5 = Value('id').of(newAdresseDom.personneLieeAdresseTypeVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId5;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('fondsDonneEtablissement') and fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseTypeVoie != null) {
	var monId6 = Value('id').of(fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId6;
}

if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12']= correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('prof')) {
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12']= adresseEntreprise.etablissementAdresseNomVoie;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12']=etablissement.adresseEtablissementOld.etablissementAdresseNomVoieOld;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12']=etablissementNew.adresseEtablissementNew.etablissementAdresseNomVoieNew;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('domi')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12']= newAdresseDom.personneLieeAdresseNomVoieNew;
}   else if (Value('id').of(correspondance.adresseCorrespond).eq('fondsDonneEtablissement')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12']= fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseNomVoie;
}

if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune.getLabel();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('prof')) {
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= adresseEntreprise.etablissementAdresseCommune.getLabel();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']=etablissement.adresseEtablissementOld.etablissementAdresseCommuneOld.getLabel();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']=etablissementNew.adresseEtablissementNew.etablissementAdresseCommuneNew.getLabel();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('domi')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= newAdresseDom.personneLieeAdresseCommuneNew != null ? newAdresseDom.personneLieeAdresseCommuneNew.getLabel() : newAdresseDom.personneLieeAdresseVilleNew;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('fondsDonneEtablissement')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseCommune.getLabel();
} 

if (Value('id').of(correspondance.adresseCorrespond).eq('domi') and not Value('id').of(newAdresseDom.personneLieeAdressePaysNew).eq('FRXXXXX') and newAdresseDom.personneLieeAdresseVilleNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.14']= newAdresseDom.personneLieeAdressePaysNew.getLabel().substring(0, 38);
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.1[1]']= correspondance.infosSup.formaliteTelephone2.e164.replace('+', '00');

if (correspondance.infosSup.formaliteTelephone1 != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.1[2]']= correspondance.infosSup.formaliteTelephone1.e164.replace('+', '00');
}

if (correspondance.infosSup.telecopie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.2']= correspondance.infosSup.telecopie.e164.replace('+', '00');
}

if (correspondance.infosSup.formaliteFaxCourriel != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.3']= correspondance.infosSup.formaliteFaxCourriel;
}


// Sous groupe SIF : Signature de la formalité

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.1']= (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') or Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteAutre')) ? 
																			signataire.adresseMandataire.nomPrenomDenominationMandataire : 
																			((modifIdentiteDeclarant.modifIdentite.modifNouveauNomUsage != null 
																			and Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) ?
																			(modifIdentiteDeclarant.modifIdentite.modifNouveauNomUsage 
																			+ ' ' + (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom') ? modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms[0] : identite.personneLieePersonnePhysiquePrenom1[0])) :
																			((identite.personneLieePersonnePhysiqueNomUsage != null
																			and not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) ?
																			(identite.personneLieePersonnePhysiqueNomUsage 
																			+ ' ' + (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom') ? modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms[0] : identite.personneLieePersonnePhysiquePrenom1[0])) :
																			((Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifNomNaissance') ? modifIdentiteDeclarant.modifIdentite.modifNouveauNomNaissance : identite.personneLieePersonnePhysiqueNomNaissance) + ' ' + (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom') ? modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms[0] : identite.personneLieePersonnePhysiquePrenom1[0]))));

if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') or Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteAutre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.2']= Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? "Mandataire" : "Autre personne";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.3']= signataire.adresseMandataire.villeAdresseMandataire.getId();

if (signataire.adresseMandataire.numeroVoieMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.5']= signataire.adresseMandataire.numeroVoieMandataire;
}

if (signataire.adresseMandataire.indiceVoieMandataire != null) {
   var monId = Value('id').of(signataire.adresseMandataire.indiceVoieMandataire)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.6']          = monId;
}

if (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.7']= signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.8']= signataire.adresseMandataire.dataCodePostalMandataire;

if (signataire.adresseMandataire.complementVoieMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.10']= signataire.adresseMandataire.complementVoieMandataire;
}

if (signataire.adresseMandataire.typeVoieMandataire != null) {
   var monId = Value('id').of(signataire.adresseMandataire.typeVoieMandataire)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.11']          = monId;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.12']= signataire.adresseMandataire.nomVoieMandataire;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.13']= signataire.adresseMandataire.villeAdresseMandataire.getLabel();
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C41']= signataire.formaliteSignatureLieu;

if(signataire.formaliteSignatureDate != null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C42']= date;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C43']= (identite.optionME ? "ME=O!" : "ME=N!") 
+''+ (Value('id').of(signataire.diffusionInfo).eq('formaliteNonDiffusionInformationOui') ? "C45=O!" : "C45=N!") 
+''+ (jqpa3.jqpaSituation != null ? "JQPA=3!" : (jqpa2.jqpaSituation != null ? "JQPA=2!" : 
(jqpa1.jqpaSituation != null ? "JQPA=1!" : '')))
+''+ (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('28P') ?
((Value('id').of(modifIdentiteDeclarant.modifInsaisissabilite.modifResidencePrincipale).eq('renonciation') ? ("P90.1=A!" +''+ modifIdentiteDeclarant.modifInsaisissabilite.publicationInsaisssabiliteResidencePrincipale +''+ "!") : '')
+ '' + (Value('id').of(modifIdentiteDeclarant.modifInsaisissabilite.modifResidencePrincipale).eq('revocation') ? ("P90.1=B!" +''+ modifIdentiteDeclarant.modifInsaisissabilite.publicationInsaisssabiliteResidencePrincipale +''+ "!") : '')
+ '' + ((Value('id').of(modifIdentiteDeclarant.modifInsaisissabilite.modifAutreBien).contains('declaration') 
and (modifIdentiteDeclarant.modifInsaisissabilite.publicationDeclarationInsaisssabiliteAutreBien[0].length > 0)) ? 
("P90.2=C1!"+modifIdentiteDeclarant.modifInsaisissabilite.publicationDeclarationInsaisssabiliteAutreBien[0]+"!") : '')
+''+ ((modifIdentiteDeclarant.modifInsaisissabilite.publicationDeclarationInsaisssabiliteAutreBien.size() > 1) ? ("C2" +''+ modifIdentiteDeclarant.modifInsaisissabilite.publicationDeclarationInsaisssabiliteAutreBien[1] +''+ "!") : '')
+''+ ((modifIdentiteDeclarant.modifInsaisissabilite.publicationDeclarationInsaisssabiliteAutreBien.size() > 2) ? ("C3" +''+ modifIdentiteDeclarant.modifInsaisissabilite.publicationDeclarationInsaisssabiliteAutreBien[2] +''+ "!") : '')
+''+ ((Value('id').of(modifIdentiteDeclarant.modifInsaisissabilite.modifAutreBien).contains('renonciationAutre') 
and (modifIdentiteDeclarant.modifInsaisissabilite.publicationRenonciationInsaisssabiliteAutreBien[0].length > 0)) ?  ("P90.2=D1!" +''+ modifIdentiteDeclarant.modifInsaisissabilite.publicationRenonciationInsaisssabiliteAutreBien[0] +''+ "!") : '')
+''+ ((modifIdentiteDeclarant.modifInsaisissabilite.publicationRenonciationInsaisssabiliteAutreBien.size() > 1) ? ("D2" +''+ modifIdentiteDeclarant.modifInsaisissabilite.publicationRenonciationInsaisssabiliteAutreBien[1] +''+ "!") : '')
+''+ ((modifIdentiteDeclarant.modifInsaisissabilite.publicationRenonciationInsaisssabiliteAutreBien.size() > 2) ? ("D3" +''+ modifIdentiteDeclarant.modifInsaisissabilite.publicationRenonciationInsaisssabiliteAutreBien[2] +''+ "!") : '')
) : '')
+''+ ((activiteInfo.cadreEtablissementNomDomaine.modifDateIdentification !=  null or activite.modifNomCommercialBis != null) ? "E06=O!" : '') 
+''+ (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('cessationActivite') ? 
(Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetcessationActivite.infosCessation).eq('43P') ? "Cessation définitive d'activité" : ("Cessation temporaire d'activité" + ' ' + objet.cadreObjetModificationEtablissement.cadreObjetcessationActivite.motifCessation)) 
: '')
+ ' ' + (correspondance.formaliteObservations != null ? sanitize(correspondance.formaliteObservations) : '') ;

							
								// Groupe ICP : Identification complète de la personne physique

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.2']= (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P') and Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifNomNaissance')) ? 
																					modifIdentiteDeclarant.modifIdentite.modifNouveauNomNaissance : identite.personneLieePersonnePhysiqueNomNaissance;

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P') and Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom')) {
var prenoms=[];
for ( i = 0; i < modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms.size() ; i++ ){prenoms.push(modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.3']= prenoms;
} else {
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.3']= prenoms;
} 

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P') and Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPseudonyme).contains('modifNomUsage') and modifIdentiteDeclarant.modifIdentite.modifNouveauNomUsage != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P02/P02.1']= modifIdentiteDeclarant.modifIdentite.modifNouveauNomUsage;
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P') and identite.personneLieePersonnePhysiqueNomUsage != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P02/P02.1']= identite.personneLieePersonnePhysiqueNomUsage;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P') and Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPseudonyme).contains('modifPseudonyme') and modifIdentiteDeclarant.modifIdentite.modifNouveauPseudonyme != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P02/P02.2']= modifIdentiteDeclarant.modifIdentite.modifNouveauPseudonyme;
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P') and identite.personneLieePersonnePhysiquePseudonyme != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P02/P02.2']= identite.personneLieePersonnePhysiquePseudonyme;
} 

if(identite.personneLieePersonnePhysiqueDateNaissance != null) {
	var dateTemp = new Date(parseInt(identite.personneLieePersonnePhysiqueDateNaissance.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.1']= date;
}

if (not Value('id').of(identite.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = identite.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.2']= result;
} else if (Value('id').of(identite.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.2'] = identite.personneLieePPLieuNaissanceCommune.getId();
}	

if (not Value('id').of(identite.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.3']= identite.personneLieePPLieuNaissancePays.getLabel();
}

if (Value('id').of(identite.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.4']= identite.personneLieePPLieuNaissanceCommune.getLabel();
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') or objet.entrepriseDomicile) {
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P')) {
if (Value('id').of(newAdresseDom.personneLieeAdressePaysNew).eq('FRXXXXX') or newAdresseDom.personneLieeAdresseCommuneNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.3']= newAdresseDom.personneLieeAdresseCommuneNew.getId();
} else if (not Value('id').of(newAdresseDom.personneLieeAdressePaysNew).eq('FRXXXXX') and newAdresseDom.personneLieeAdresseVilleNew != null) {
	var idPays = newAdresseDom.personneLieeAdressePaysNew.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.3']= result;
}
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and objet.entrepriseDomicile) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.3']= newAdresseDom2.etablissementAdresseCommuneNew.getId();
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and newAdresseDom.personneLieeAdresseVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.5']= newAdresseDom.personneLieeAdresseVoieNew;
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and objet.entrepriseDomicile and newAdresseDom2.etablissementAdresseNumeroVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.5']= newAdresseDom2.etablissementAdresseNumeroVoieNew;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and newAdresseDom.personneLieeAdresseIndiceVoieNew != null) {
   var monId1 = Value('id').of(newAdresseDom.personneLieeAdresseIndiceVoieNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.6']          = monId1;
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and objet.entrepriseDomicile and newAdresseDom2.etablissementAdresseIndiceVoieNew != null) {
   var monId2 = Value('id').of(newAdresseDom2.etablissementAdresseIndiceVoieNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.6']          = monId2;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.7']= newAdresseDom.personneLieeAdresseDistriutionSpecialeNew;
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and objet.entrepriseDomicile and newAdresseDom2.etablissementAdresseDistriutionSpecialeVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.7']= newAdresseDom2.etablissementAdresseDistriutionSpecialeVoieNew;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.8']= newAdresseDom.personneLieeAdresseCodePostalNew != null ? newAdresseDom.personneLieeAdresseCodePostalNew : '.';
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and objet.entrepriseDomicile and newAdresseDom2.etablissementAdresseCodePostalNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.8']= newAdresseDom2.etablissementAdresseCodePostalNew;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and newAdresseDom.personneLieeAdresseComplementAdresseNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.10']= newAdresseDom.personneLieeAdresseComplementAdresseNew;
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and objet.entrepriseDomicile and newAdresseDom2.etablissementAdresseComplementVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.10']= newAdresseDom2.etablissementAdresseComplementVoieNew;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and newAdresseDom.personneLieeAdresseTypeVoieNew != null) {
   var monId1 = Value('id').of(newAdresseDom.personneLieeAdresseTypeVoieNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.11']          = monId1;
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and objet.entrepriseDomicile and newAdresseDom2.etablissementAdresseTypeVoieNew != null) {
   var monId2 = Value('id').of(newAdresseDom2.etablissementAdresseTypeVoieNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.11']          = monId2;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.12']= newAdresseDom.personneLieeAdresseNomVoieNew;
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and objet.entrepriseDomicile) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.12']= newAdresseDom2.etablissementAdresseNomVoieNew;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.13']= newAdresseDom.personneLieeAdresseCommuneNew != null ? newAdresseDom.personneLieeAdresseCommuneNew.getLabel() : newAdresseDom.personneLieeAdresseVilleNew;
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and objet.entrepriseDomicile) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.13']= newAdresseDom2.etablissementAdresseCommuneNew.getLabel();
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and not Value('id').of(newAdresseDom.personneLieeAdressePaysNew).eq('FRXXXXX') and newAdresseDom.personneLieeAdresseVilleNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.14']= newAdresseDom.personneLieeAdressePaysNew.getLabel().substring(0, 38);
}

if (modifIdentiteDeclarant.modifDomicile.domicileMemeDept != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P05']= modifIdentiteDeclarant.modifDomicile.domicileMemeDept.getId();
}
}

if (activite.etablissementNonSedentariteQualiteNonSedentaire) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P06']= "A";
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).contains('modificationEIRL') or Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).contains('finEIRL') or objet.entrepriseEIRL) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.1']= "O";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.2']= rappelIdentification.eirlRappelDenomination;

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.3']= rappelIdentification.cadreEirlRappelAdresse.communeAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.communeAdresseEirl.getId() : adresseEntreprise.etablissementAdresseCommune.getId();

if (not objet.cadreObjetModificationEIRL.memeAdresseEIRL and rappelIdentification.cadreEirlRappelAdresse.numeroAdresseEirl != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.5']= rappelIdentification.cadreEirlRappelAdresse.numeroAdresseEirl;
} else if ((objet.cadreObjetModificationEIRL.memeAdresseEIRL or objet.entrepriseEIRL) and adresseEntreprise.etablissementAdresseNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.5']= adresseEntreprise.etablissementAdresseNumeroVoie;
}

if (not objet.cadreObjetModificationEIRL.memeAdresseEIRL and rappelIdentification.cadreEirlRappelAdresse.indiceVoieAdresseEirl != null) {
   var monId1 = Value('id').of(rappelIdentification.cadreEirlRappelAdresse.indiceVoieAdresseEirl)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.6']          = monId1;
} else if ((objet.cadreObjetModificationEIRL.memeAdresseEIRL or objet.entrepriseEIRL) and adresseEntreprise.etablissementAdresseIndiceVoie != null) {
   var monId2 = Value('id').of(adresseEntreprise.etablissementAdresseIndiceVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.6']          = monId2;
}

if (not objet.cadreObjetModificationEIRL.memeAdresseEIRL and rappelIdentification.cadreEirlRappelAdresse.distriutionSpecialeAdresseEirl != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.7']= rappelIdentification.cadreEirlRappelAdresse.distriutionSpecialeAdresseEirl;
} else if ((objet.cadreObjetModificationEIRL.memeAdresseEIRL or objet.entrepriseEIRL) and adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.7']= adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.8']= rappelIdentification.cadreEirlRappelAdresse.codePostalAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.codePostalAdresseEirl : adresseEntreprise.etablissementAdresseCodePostal;

if (not objet.cadreObjetModificationEIRL.memeAdresseEIRL and rappelIdentification.cadreEirlRappelAdresse.complementAdresseEirl != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.10']= rappelIdentification.cadreEirlRappelAdresse.complementAdresseEirl;
} else if ((objet.cadreObjetModificationEIRL.memeAdresseEIRL or objet.entrepriseEIRL) and adresseEntreprise.etablissementAdresseComplementVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.10']= adresseEntreprise.etablissementAdresseComplementVoie;
}

if (not objet.cadreObjetModificationEIRL.memeAdresseEIRL and rappelIdentification.cadreEirlRappelAdresse.typeVoieAdresseEirl != null) {
   var monId1 = Value('id').of(rappelIdentification.cadreEirlRappelAdresse.typeVoieAdresseEirl)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.11']          = monId1;
} else if ((objet.cadreObjetModificationEIRL.memeAdresseEIRL or objet.entrepriseEIRL) and adresseEntreprise.etablissementAdresseTypeVoie != null) {
   var monId2 = Value('id').of(adresseEntreprise.etablissementAdresseTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.11']          = monId2;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.12']= rappelIdentification.cadreEirlRappelAdresse.nomVoieAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.nomVoieAdresseEirl : adresseEntreprise.etablissementAdresseNomVoie;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.13']= rappelIdentification.cadreEirlRappelAdresse.communeAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.communeAdresseEirl.getLabel() : adresseEntreprise.etablissementAdresseCommune.getLabel();

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.4']= Value('id').of(rappelIdentification.eirlRappelgreffeDepot).eq('eirlRappelDepotRCS') ? "1" : (Value('id').of(rappelIdentification.eirlRappelgreffeDepot).eq('eirlRappelDepotRM') ? "2" : "3");
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.5']= rappelIdentification.eirlRappelLieuImmatriculation;
}


								// Groupe AIP : Ancienne identification de la personne physique 

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P11']= identite.personneLieePersonnePhysiqueNomNaissance;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P') and identite.personneLieePersonnePhysiqueNomUsage != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P13']= identite.personneLieePersonnePhysiqueNomUsage;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P')) {
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P14']= prenoms;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P') and identite.personneLieePersonnePhysiquePseudonyme != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P15']= identite.personneLieePersonnePhysiquePseudonyme;
}


								// Groupe NAP : Nationalité de la personne physique
								
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('17P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/NAP/P21']= modifIdentiteDeclarant.modifNationalite.modifNewNationalite;
}

								// Groupe MEP : Mineur émancipé
/*
if (Value('id').of(objet.cadreObjet22P.infoDeces).eq('22P') and (exploitant1.exploitantMineurEmancipe or exploitant2.exploitantMineurEmancipe)) {								
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/MEP/P22']= "O";
}								
*/
								// Groupe ISP : Insaisissabilité
								
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('28P')) {								
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ISP/P90']= ".";
}								

								// Groupe DAP : Déclaration d'affectation du patrimoine (si option pour l'EIRL)
								
if (Value('id').of(objet.objetModification).contains('modifEIRL') or objet.entrepriseEIRL) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P70']= objet.cadreObjetDeclarationEIRL.declarationEIRL ?
																			   (Value('id').of(declarationAffectation.eirlStatut).eq('EirlStatutEIRLReprise') ? "R" : "O") : 
																			   ((Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') or objet.entrepriseEIRL) ? "M" : "F");

if (objet.cadreObjetDeclarationEIRL.declarationEIRL) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P71']= declarationAffectation.declarationPatrimoine.eirlDenomination;
} else if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDenoEIRL')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P71']= modifEIRL.newDenoEIRL;
}

if (objet.cadreObjetDeclarationEIRL.declarationEIRL and declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable != null) {
    var dateTmp = new Date(parseInt(declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable.getTimeInMillis()));
    var dateClotureEc = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateClotureEc = dateClotureEc.concat(pad(month.toString()));
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P72']          = dateClotureEc;
} else if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDateCloture')) {
    var dateTmp = new Date(parseInt(modifEIRL.eirlNewDateClotureExerciceComptable.getTimeInMillis()));
    var dateClotureEc = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateClotureEc = dateClotureEc.concat(pad(month.toString()));
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P72']          = dateClotureEc;
}

if (objet.cadreObjetDeclarationEIRL.declarationEIRL) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P73']= declarationAffectation.declarationPatrimoine.eirlObjet;
} else if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifObjet')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P73']= modifEIRL.newObjetEIRL;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.3']= modifEIRL.newAdresseEIRL.communeAdresseEirlNew.getId();
} else if (objet.entrepriseEIRL and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).contains('transfert')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.3']= etablissementNew.adresseEtablissementNew.etablissementAdresseCommuneNew.getId();
} else if (objet.entrepriseEIRL and objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.3']= newAdresseDom.personneLieeAdresseCommuneNew.getId();
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse') and modifEIRL.newAdresseEIRL.numeroAdresseEirlNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.5']= modifEIRL.newAdresseEIRL.numeroAdresseEirlNew;
} else if (objet.entrepriseEIRL and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissementNew.adresseEtablissementNew.etablissementAdresseNumeroVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.5']= etablissementNew.adresseEtablissementNew.etablissementAdresseNumeroVoieNew;
} else if (objet.entrepriseEIRL and objet.domicileEntreprise and newAdresseDom.personneLieeAdresseVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.5']= newAdresseDom.personneLieeAdresseVoieNew;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse') and modifEIRL.newAdresseEIRL.indiceVoieAdresseEirlNew != null) {
   var monId1 = Value('id').of(modifEIRL.newAdresseEIRL.indiceVoieAdresseEirlNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.6']          = monId1;
} else if (objet.entrepriseEIRL and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissementNew.adresseEtablissementNew.etablissementAdresseIndiceVoieNew != null) {
   var monId2 = Value('id').of(etablissementNew.adresseEtablissementNew.etablissementAdresseIndiceVoieNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.6']          = monId2;
} else if (objet.entrepriseEIRL and objet.domicileEntreprise and newAdresseDom.personneLieeAdresseIndiceVoieNew != null) {
   var monId3 = Value('id').of(newAdresseDom.personneLieeAdresseIndiceVoieNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.6']          = monId3;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse') and modifEIRL.newAdresseEIRL.distriutionSpecialeAdresseEirlNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.7']= modifEIRL.newAdresseEIRL.distriutionSpecialeAdresseEirlNew;
} else if (objet.entrepriseEIRL and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissementNew.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.7']= etablissementNew.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew;
} else if (objet.entrepriseEIRL and objet.domicileEntreprise and newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.7']= newAdresseDom.personneLieeAdresseDistriutionSpecialeNew;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.8']= modifEIRL.newAdresseEIRL.codePostalAdresseEirlNew;
} else if (objet.entrepriseEIRL and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).contains('transfert')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.8']= etablissementNew.adresseEtablissementNew.etablissementAdresseCodePostalNew;
} else if (objet.entrepriseEIRL and objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.8']= newAdresseDom.personneLieeAdresseCodePostalNew;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse') and modifEIRL.newAdresseEIRL.complementAdresseEirlNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.10']= modifEIRL.newAdresseEIRL.complementAdresseEirlNew;
} else if (objet.entrepriseEIRL and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissementNew.adresseEtablissementNew.etablissementAdresseComplementVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.10']= etablissementNew.adresseEtablissementNew.etablissementAdresseComplementVoieNew;
} else if (objet.entrepriseEIRL and objet.domicileEntreprise and newAdresseDom.personneLieeAdresseComplementAdresseNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.10']= newAdresseDom.personneLieeAdresseComplementAdresseNew;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse') and modifEIRL.newAdresseEIRL.typeVoieAdresseEirlNew != null) {
   var monId1 = Value('id').of(modifEIRL.newAdresseEIRL.typeVoieAdresseEirlNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.11']          = monId1;
} else if (objet.entrepriseEIRL and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissementNew.adresseEtablissementNew.etablissementAdresseTypeVoieNew != null) {
   var monId2 = Value('id').of(etablissementNew.adresseEtablissementNew.etablissementAdresseTypeVoieNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.11']          = monId2;
} else if (objet.entrepriseEIRL and objet.domicileEntreprise and newAdresseDom.personneLieeAdresseTypeVoieNew != null) {
   var monId3 = Value('id').of(newAdresseDom.personneLieeAdresseTypeVoieNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.11']          = monId3;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.12']= modifEIRL.newAdresseEIRL.nomVoieAdresseEirlNew;
} else if (objet.entrepriseEIRL and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).contains('transfert')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.12']= etablissementNew.adresseEtablissementNew.etablissementAdresseNomVoieNew;
} else if (objet.entrepriseEIRL and objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.12']= newAdresseDom.personneLieeAdresseNomVoieNew;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.13']= modifEIRL.newAdresseEIRL.communeAdresseEirlNew.getLabel();
} else if (objet.entrepriseEIRL and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).contains('transfert')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.13']= etablissementNew.adresseEtablissementNew.etablissementAdresseCommuneNew.getLabel();
} else if (objet.entrepriseEIRL and objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.13']= newAdresseDom.personneLieeAdresseCommuneNew.getLabel();
}

if (objet.cadreObjetDeclarationEIRL.declarationEIRL and declarationAffectation.declarationPatrimoine.eirlChoixDepotRegistre != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P75']= Value('id').of(declarationAffectation.declarationPatrimoine.eirlChoixDepotRegistre).eq('EirlDepotRCS') ? "1" : "2";
}

if (Value('id').of(declarationAffectation.eirlStatut).eq('EirlStatutEIRLReprise')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P76/P76.1']= declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLDenomination;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P76/P76.2']= Value('id').of(declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLRegistre).eq('EirlPrecedentEIRLRegistreRcs') ? "1" : (Value('id').of(declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLRegistre).eq('EirlPrecedentEIRLRegistreRm') ? "2" : "3");
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P76/P76.3']= declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLLieuImmatriculation;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P76/P76.4']= declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN.split(' ').join('');
}

if (modifEIRL.modifDateEIRL != null) {
	var dateTemp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P77']= date;
} else if (objet.cadreObjet22P.poursuiteEIRL and objet.cadreObjet22P.modifDateDeces != null) {
	var dateTemp = new Date(parseInt(objet.cadreObjet22P.modifDateDeces.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P77']= date;
} else if (finDAP.finEIRLDeclaration.modifDateFinEIRL != null) {
	var dateTemp = new Date(parseInt(finDAP.finEIRLDeclaration.modifDateFinEIRL.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P77']= date;
} else if (objet.entrepriseEIRL and etablissement.modifDateAdresseEntreprise != null) {
	var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P77']= date;
} else if (objet.entrepriseEIRL and modifIdentiteDeclarant.modifDomicile.modifDateDomicile != null) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P77']= date;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDAP')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P78']= "O";
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('finEIRL')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P79']= Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLRenonciationAvecPoursuite') ? "1" : 
																			   (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLRenonciationSansPoursuite') ? "2" : 
																			   (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLCessionPP') ? "3" : "4"));
}
}

								// Groupe IPP : Intention de poursuite de l'EIRL

if (objet.cadreObjet22P.poursuiteEIRL) {								
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/IPP/P92']= "O";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/IPP/P93/P93.1']= intentionPoursuite.eIRLPoursuiteNom;
var prenoms=[];
for ( i = 0; i < intentionPoursuite.eIRLPoursuitePrenom.size() ; i++ ){prenoms.push(intentionPoursuite.eIRLPoursuitePrenom[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/IPP/P93/P93.2']= prenoms;
}								
							
								// Groupe SMP : Situation matrimoniale de la personne physique (conditionnel)

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('30P')
	and (Value('id').of(conjoint.objetModifConjoint).eq('conjointCollaborateur')
	or Value('id').of(conjoint.objetModifStatut).eq('modificationStatut'))) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P25/P25.2']= conjoint.modifNomNaissanceConjoint;
var prenoms=[];
for ( i = 0; i < conjoint.modifPrenomConjoint.size() ; i++ ){prenoms.push(conjoint.modifPrenomConjoint[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P25/P25.3']= prenoms;
if(conjoint.modifNomUsageConjoint != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P25/P25.4']= conjoint.modifNomUsageConjoint;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P40']= (Value('id').of(conjoint.objetModifStatut).eq('suppressionStatut') or (Value('id').of(conjoint.objetModifConjoint).eq('conjointSalarie') and Value('id').of(conjoint.objetModifStatut).eq('modificationStatut'))) ? "S" : "O";
if (Value('id').of(conjoint.objetModifStatut).eq('nouveauStatut')
	or (Value('id').of(conjoint.objetModifConjoint).eq('conjointCollaborateur') and Value('id').of(conjoint.objetModifStatut).eq('modificationStatut'))) {
if(conjoint.modifDateNaissanceConjoint != null) {
	var dateTemp = new Date(parseInt(conjoint.modifDateNaissanceConjoint.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.1']= date;
}
if	(Value('id').of(conjoint.modifLieuNaissancePaysConjoint).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.2']= conjoint.modifLieuNaissanceCommuneConjoint.getId();
} else if (not Value('id').of(conjoint.modifLieuNaissancePaysConjoint).eq('FRXXXXX')) {
		var idPays = conjoint.modifLieuNaissancePaysConjoint.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.2']= result;
}
if (not Value('id').of(conjoint.modifLieuNaissancePaysConjoint).eq('FRXXXXX')) {																					 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.3']= conjoint.modifLieuNaissancePaysConjoint.getLabel();
}																					 
if (Value('id').of(conjoint.modifLieuNaissancePaysConjoint).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.4']= conjoint.modifLieuNaissanceCommuneConjoint.getLabel();
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P42']= conjoint.modifNationaliteConjoint;
if (conjoint.adresseConjointDifferente) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.3']= adresseConjoint.adresseConjointCommune.getId();
if (adresseConjoint.adresseConjointNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.5']= adresseConjoint.adresseConjointNumeroVoie;
}
if (adresseConjoint.adresseConjointIndiceVoie != null) {
   var monId = Value('id').of(adresseConjoint.adresseConjointIndiceVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.6']          = monId;
}
if (adresseConjoint.adresseConjointDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.7']= adresseConjoint.adresseConjointDistriutionSpecialeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.8']= adresseConjoint.adresseConjointCodePostal;
if (adresseConjoint.adresseConjointComplementVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.10']= adresseConjoint.adresseConjointComplementVoie;
}
if (adresseConjoint.adresseConjointTypeVoie != null) {
   var monId = Value('id').of(adresseConjoint.adresseConjointTypeVoie)._eval();
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.11']          = monId;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.12']= adresseConjoint.adresseConjointNomVoie;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.13']= adresseConjoint.adresseConjointCommune.getLabel();
}
}
}

								// Groupe CAP : Cessation totale d'activité
								
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('cessationActivite') or Value('id').of(objet.cadreObjet22P.infoDeces).eq('42P')) {								
if (Value('id').of(identite.immatriculation).contains('immatRCS')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/CAP/P53']= "O";
}
if (Value('id').of(identite.immatriculation).contains('immatRM')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/CAP/P54']= "O";
}
}
								
								// Groupe GCS : Groupe complément social

// Sous groupe ISS : Immatriculation sécurité sociale du travailleur non salarié
/*
if (Value('id').of(objet.objetModification).contains('dateActivite') and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 != null or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 != null)) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A10/A10.1']  = "0000000000000";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A10/A10.2']  = "00";
}*/

// Sous groupe SCS : Situation du conjoint vis-à-vis de la sécurité sociale
/*
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('30P') and Value ('id').of(conjoint.objetModifConjoint).eq('modifMentionNouveauConjoint')) {
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A53/A53.4']= ".";
var nirConjoint = conjoint.modifNumeroSSConjoint;
if(nirConjoint != null) {
    nirConjoint = nirConjoint.replace(/ /g, "");
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A55/A55.1']    = nirConjoint.substring(0, 13);
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A55/A55.2'] = nirConjoint.substring(13, 15);
}
}*/


// Groupe IPU : Immatriculation principale de l'enttreprise

if (Value('id').of(identite.immatriculation).contains('immatRCS')) { 
//Récupérer le EntityId du greffe
var greffeId = identite.immatRCSGreffe.id ;
//Appeler Directory pour récupérer l'autorité
_log.info('l entityId du greffe selectionne est {}', greffeId);
var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', greffeId) //
		   .connectionTimeout(10000) //
		   .receiveTimeout(10000) //
		   .accept('json') //
		   .get();
//result
var receiverInfo = response.asObject();
var contactInfo = !receiverInfo.details ? null : receiverInfo.details;
//Récupérer le code EDI
var codeEDIGreffe = !contactInfo.ediCode ? null : contactInfo.ediCode;
_log.info('le code edi du greffe selectionne est {}', codeEDIGreffe);
// À modifier quand Destiny fourni le réferentiel
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U01/U01.1'] = codeEDIGreffe;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U02']= identite.siren.split(' ').join('');

if (Value('id').of(identite.immatriculation).contains('immatRM')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U04']= identite.immatRMCMA.getId();
}

if(activite.activiteTemps != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U06']= (activite.activiteTemps!= null and activite.etablissementEffectifSalariePresenceOui) ? "O" : "N";
}

// Fin regent pour le 20P//
// Fin regent pour le 28P//

// Groupe SIU : siège de l'entreprise (adresse de l'entreprise individuelle)

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('miseEnLocation') 
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P') 
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P') 
	or objet.domicileEntreprise) { 

if (objet.entrepriseDomicile or objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U16']= "O";
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.3']= etablissementNew.adresseEtablissementNew.etablissementAdresseCommuneNew.getId();
} else if (objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.3']= newAdresseDom.personneLieeAdresseCommuneNew.getId();
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.3']= adresseEntreprise.etablissementAdresseCommune.getId();
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') and etablissementNew.adresseEtablissementNew.etablissementAdresseNumeroVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.5']= etablissementNew.adresseEtablissementNew.etablissementAdresseNumeroVoieNew;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.5']= newAdresseDom.personneLieeAdresseVoieNew;
} else if ((not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and not Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))) 
	and not objet.domicileEntreprise and adresseEntreprise.etablissementAdresseNumeroVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.5']= adresseEntreprise.etablissementAdresseNumeroVoie;
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') and etablissementNew.adresseEtablissementNew.etablissementAdresseIndiceVoieNew != null) {
   var monId1 = Value('id').of(etablissementNew.adresseEtablissementNew.etablissementAdresseIndiceVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.6']          = monId1;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseIndiceVoieNew != null) {
   var monId2 = Value('id').of(newAdresseDom.personneLieeAdresseIndiceVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.6']          = monId2;
} else if ((not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and not Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))) 
	and not objet.domicileEntreprise and adresseEntreprise.etablissementAdresseIndiceVoie != null){
var monId3 = Value('id').of(adresseEntreprise.etablissementAdresseIndiceVoie)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.6']		  = monId3;
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') and etablissementNew.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.7']= etablissementNew.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.7']= newAdresseDom.personneLieeAdresseDistriutionSpecialeNew;
} else if ((not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and not Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))) 
	and not objet.domicileEntreprise and adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.7']= adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie;
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.8']= etablissementNew.adresseEtablissementNew.etablissementAdresseCodePostalNew;
} else if (objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.8']= newAdresseDom.personneLieeAdresseCodePostalNew;
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.8']= adresseEntreprise.etablissementAdresseCodePostal;
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') and etablissementNew.adresseEtablissementNew.etablissementAdresseComplementVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.10']= etablissementNew.adresseEtablissementNew.etablissementAdresseComplementVoieNew;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseComplementAdresseNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.10']= newAdresseDom.personneLieeAdresseComplementAdresseNew;
}  else if ((not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and not Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))) 
	and not objet.domicileEntreprise and adresseEntreprise.etablissementAdresseComplementVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.10']= adresseEntreprise.etablissementAdresseComplementVoie;
}	

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') and etablissementNew.adresseEtablissementNew.etablissementAdresseTypeVoieNew != null) {
   var monId1 = Value('id').of(etablissementNew.adresseEtablissementNew.etablissementAdresseTypeVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.11']          = monId1;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseTypeVoieNew != null) {
   var monId2 = Value('id').of(newAdresseDom.personneLieeAdresseTypeVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.11']          = monId2;
}  else if ((not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and not Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))) 
	and not objet.domicileEntreprise and adresseEntreprise.etablissementAdresseTypeVoie != null){
	var monId3 = Value('id').of(adresseEntreprise.etablissementAdresseTypeVoie)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.11']		  = monId3;
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.12']= etablissementNew.adresseEtablissementNew.etablissementAdresseNomVoieNew;
} else if (objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.12']= newAdresseDom.personneLieeAdresseNomVoieNew;
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.12']= adresseEntreprise.etablissementAdresseNomVoie;
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.13']= etablissementNew.adresseEtablissementNew.etablissementAdresseCommuneNew.getLabel();
} else if (objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.13']= newAdresseDom.personneLieeAdresseCommuneNew.getLabel();
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.13']= adresseEntreprise.etablissementAdresseCommune.getLabel();
}
}

							// Groupe CPU : Caractéristiques de l'entreprise

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')
	or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('37P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P') 
	or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('37P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('cessationActivite')
	or Value('id').of(objet.cadreObjet22P.infoDeces).eq('42P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite') 
	or objet.domicileEntreprise) { 

if (activite.etablissementActivitesAutres != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U21']= sanitize(activite.etablissementActivitesAutres);
}
				
if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and (Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementOuverture')
	or Value('id').of(etablissementNew.origineEtablissement2).eq('etablissementOuverture')))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')	
	or objet.domicileEntreprise) 
	and activite.etablissementEffectifSalariePresenceOui) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U22']= activite.cadreEffectifSalarie.totalEffectifSalarieNombre;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('37P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.1']= "R";
}
}

// Fin Regent 37P //

						// Groupe ISU : Greffes secondaires
						
if ((Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P')
	or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or objet.domicileEntreprise
	or Value('id').of(objet.cadreObjet22P.infoDeces).eq('22P')
	or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('23P')
	or Value('if').of(objet.objetModification).contains('modifEIRL')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P'))
	and identite.greffeSecondaire != null) {

			var occurrencesU41 = [];
			var occurrence = {};

			_log.info("Accessed to : occurence U41 bloc");
			_log.info("Greffe secondaire bloc : " + identite.greffeSecondaire);
			_log.info("Greffe secondaire bloc nbr de greffe secondaire : " + identite.greffeSecondaire.size());
			_log.info("Detail de la taille 0: " + identite.greffeSecondaire[0]);
			if (identite.greffeSecondaire != null and identite.greffeSecondaire[0].length > 0) {
					_log.info("entré dans la boucle des elements");
					
					for (var i = 0; i < identite.greffeSecondaire.size(); i++) {
						var idx = i+1;	
						regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[' + idx + ']/U41.1']= identite.greffeSecondaire[i];
						regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[' + idx + ']/U41.2']= "000000000000000";
										
					}	


			/* 	occurrencesU41.forEach(function (value, i) {
					var idx = i+1;
					regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[' + idx + ']/U41.1']= value['U41.1'];
					regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[' + idx + ']/U41.2']= "000000000000000";	
				}); */					
			}
			
			/* if (identite.greffeSecondaire != null) {	
				occurrencesU41.forEach(function (value, i) {
				var idx = i+1;
				regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[' + idx + ']/U41.1']= value['U41.1'];
				regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/ISU/U41[' + idx + ']/U41.2']= "000000000000000";	
			}); */
}

// Groupe OFU : Option Fiscale EIRL (si EIRL) 
							
if (objet.cadreObjetDeclarationEIRL.declarationEIRL) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U70']= (Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('EirlRegimeFiscalRegimeImpositionBeneficesMBIC') or identite.optionME) ? '116' :
																		((Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('EirlRegimeFiscalRegimeImpositionBeneficesReelBIC') 
																		and Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalReel).eq('EirlRegimeFiscalRegimeImpositionBeneficesRs')) ? '112' : 
																		((Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('EirlRegimeFiscalRegimeImpositionBeneficesReelBIC') 
																		and Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalReel).eq('EirlRegimeFiscalRegimeImpositionBeneficesRn')) ? '113' : 
																		((Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('EirlRegimeFiscalRegimeImpositionBeneficesOptionIS') 
																		and Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalReel).eq('EirlRegimeFiscalRegimeImpositionBeneficesRs')) ? '114' : "115")));

if (Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('EirlRegimeFiscalRegimeImpositionBeneficesOptionIS')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U71']= "211";
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U72/U72.1']= (Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('EirlRegimeFiscalRegimeImpositionBeneficesMBIC')
																				or Value('id').of(fiscalEIRL.optionsTVA.regimeTVA1).eq('EirlRegimeFiscalRegimeImpositionTVARfTVA')
																				or Value('id').of(fiscalEIRL.optionsTVA.regimeTVA2).eq('EirlRegimeFiscalRegimeImpositionTVARfTVA')
																				or identite.optionME) ? "310" : 
																				(Value('id').of(fiscalEIRL.optionsTVA.regimeTVA1).eq('EirlRegimeFiscalRegimeImpositionTVARsTVA') ? "311" :
																				(Value('id').of(fiscalEIRL.optionsTVA.regimeTVA1).eq('EirlRegimeFiscalRegimeImpositionTVAMrTVA') ? "313" : "312"));
																				
if (fiscalEIRL.optionsTVA.eirlregimeFiscalRegimeImpositionTVAOptionsParticulieres1 or fiscalEIRL.optionsTVA.eirlRegimeFiscalRegimeImpositionTVAOptionsParticulieres2) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U72/U72.2']= fiscalEIRL.optionsTVA.eirlregimeFiscalRegimeImpositionTVAOptionsParticulieres1 ? "410" : "411";	
}																			
}	

if (objet.cadreObjetDeclarationEIRL.declarationEIRL and identite.optionME) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U74']= fiscalEIRLME.regimeFiscalRegimeImpositionBeneficesVersementLiberatoire ? "O" : "N"; 
}

// Fin Regent pour le 25P //


// Groupe LIU lieu d'imposition de l'entreprise

if (((Value('id').of(objet.objetModification).contains('situationPerso') and not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('28P')
	and not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('37P'))
	or Value('id').of(objet.objetModification).contains('personnesLiees')
	or Value('id').of(objet.objetModification).contains('modifEIRL')
	or Value('id').of(objet.objetModification).contains('etablissement'))
	and identite.lieuDepotImpot != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/LIU/U36/U36.2']= identite.lieuDepotImpot;
}
// Fin Regent pour le 10P //
// Fin Regent pour le 15P //
// Fin Regent pour le 16P //
// Fin Regent pour le 17P //
// Fin Regent pour le 23P //
// Fin Regent pour le 25P //
// Fin Regent pour le 30P //


				// Groupe GRD : Groupe dirigeant personne personne physique
 
var occurrencesPersonnePhysiqueDirigeante = [];
var occurrence = {};
				
				
if (Value('id').of(objet.cadreObjet22P.infoDeces).eq('22P') or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('31P')) {				

// 1er exploitant
//Groupe DIU
if(Value('id').of(objet.cadreObjet22P.infoDeces).eq('22P') or Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant')) {					
if(Value('id').of(objet.cadreObjet22P.infoDeces).eq('22P') or Value('id').of(exploitant1. objetModifExploitant).eq('nouveauExploitant')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(exploitant1. objetModifExploitant).eq('partantExploitant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(exploitant1. objetModifExploitant).eq('modifExploitant')) {
occurrence['DIU/U50/U50.1'] = "3" ;
}
if (Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('31P')) {
if(exploitant1.modifDateExploitantIndivision != null) {
var dateTemp = new Date(parseInt(exploitant1.modifDateExploitantIndivision.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}}	
occurrence['DIU/U51/U51.1']= "86";
occurrence['DIU/U52']= "P";

// Groupe IPD
occurrence['IPD/D01/D01.2']= exploitant1.nomNaissanceExploitant;
var prenoms=[];
for ( i = 0; i < exploitant1.prenomExploitant.size() ; i++ ){prenoms.push(exploitant1.prenomExploitant[i]);}      
occurrence['IPD/D01/D01.3']= prenoms;
if (exploitant1.nomUsageExploitant != null) {
occurrence['IPD/D01/D01.4']= exploitant1.nomUsageExploitant;
}
if(Value('id').of(objet.cadreObjet22P.infoDeces).eq('22P') or Value('id').of(exploitant1.objetModifExploitant).eq('nouveauExploitant')) {
if(exploitant1.dateNaissanceExploitant != null) {
var dateTemp = new Date(parseInt(exploitant1.dateNaissanceExploitant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['IPD/D03/D03.1']= date;
}
if (not Value('id').of(exploitant1.lieuNaissancePaysExploitant).eq('FRXXXXX')) {
	var idPays = exploitant1.lieuNaissancePaysExploitant.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(exploitant1.lieuNaissancePaysExploitant).eq('FRXXXXX')) {
occurrence['IPD/D03/D03.2'] = exploitant1.lieuNaissanceCommuneExploitant.getId();
}	
if (not Value('id').of(exploitant1.lieuNaissancePaysExploitant).eq('FRXXXXX')) {																					 
occurrence['IPD/D03/D03.3']= exploitant1.lieuNaissancePaysExploitant.getLabel();
}
if (Value('id').of(exploitant1.lieuNaissancePaysExploitant).eq('FRXXXXX')) {	
occurrence['IPD/D03/D03.4']= exploitant1.lieuNaissanceCommuneExploitant.getLabel();
}
}
if(Value('id').of(objet.cadreObjet22P.infoDeces).eq('22P') or Value('id').of(exploitant1.objetModifExploitant).eq('nouveauExploitant') or Value('id').of(exploitant1.objetModifExploitant).eq('modifExploitant')) {
occurrence['IPD/D04/D04.3'] = exploitant1.adresseDomicileExploitant.exploitantAdresseCommune.getId();
if (exploitant1.adresseDomicileExploitant.exploitantAdresseNumeroVoie != null) {
occurrence['IPD/D04/D04.5'] = exploitant1.adresseDomicileExploitant.exploitantAdresseNumeroVoie;
}
if (exploitant1.adresseDomicileExploitant.exploitantAdresseIndiceVoie != null) {
var monId1 = Value('id').of(exploitant1.adresseDomicileExploitant.exploitantAdresseIndiceVoie)._eval();
occurrence['IPD/D04/D04.6']          = monId1;
}
if (exploitant1.adresseDomicileExploitant.exploitantAdresseDistriutionSpeciale != null) {
occurrence['IPD/D04/D04.7'] = exploitant1.adresseDomicileExploitant.exploitantAdresseDistriutionSpeciale;
}
occurrence['IPD/D04/D04.8'] = exploitant1.adresseDomicileExploitant.exploitantAdresseCodePostal;
if (exploitant1.adresseDomicileExploitant.exploitantAdresseComplementAdresse != null) {
occurrence['IPD/D04/D04.10'] = exploitant1.adresseDomicileExploitant.exploitantAdresseComplementAdresse;
}
if (exploitant1.adresseDomicileExploitant.exploitantAdresseTypeVoie != null) {
var monId1 = Value('id').of(exploitant1.adresseDomicileExploitant.exploitantAdresseTypeVoie)._eval();
occurrence['IPD/D04/D04.11']          = monId1;
}
occurrence['IPD/D04/D04.12'] = exploitant1.adresseDomicileExploitant.exploitantAdresseNomVoie;
occurrence['IPD/D04/D04.13'] = exploitant1.adresseDomicileExploitant.exploitantAdresseCommune.getLabel();
}

// Groupe NPD
if (exploitant1.nationaliteExploitant != null) {
occurrence['NPD/D21']= exploitant1.nationaliteExploitant;
}

// Groupe GCS

if(Value('id').of(objet.cadreObjet22P.infoDeces).eq('22P') or Value('id').of(exploitant1.objetModifExploitant).eq('nouveauExploitant')) {
// ISS	
var nirExploitant = social.voletSocialNumeroSecuriteSociale;
if(nirExploitant != null) {
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);
}	
// SNS
if (social.voletSocialActiviteExerceeAnterieurementTNS) {
if(social.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
var dateTemp = new Date(parseInt(social.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['GCS/SNS/A21/A21.2']= date;
}	
occurrence['GCS/SNS/A21/A21.3']= social.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['GCS/SNS/A21/A21.4']= social.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['GCS/SNS/A21/A21.6']= social.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['GCS/SNS/A22/A22.3']= Value('id').of(social.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" :
								 (Value('id').of(social.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
								 (Value('id').of(social.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : " X"));
if (Value('id').of(social.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS') or Value('id').of(social.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS')) {
occurrence['GCS/SNS/A22/A22.4']= Value('id').of(social.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeENIMTNS') ? "ENIM" : social.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
}
if (social.voletSocialActiviteAutreQueDeclareeStatutOui) {
occurrence['GCS/SNS/A24/A24.2']= Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutSalarie') ? "1" :
								 (Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutSalarieAgricole') ? "2" : 
								 (Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutRetraitePensionne') ? "8" : " 9"));
if (Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutCocheAutre')) {
occurrence['GCS/SNS/A24/A24.3']= social.voletSocialActiviteAutreQueDeclareeStatutAutre;
}
}
// CAS
occurrence['GCS/CAS/A42/A42.1']= "."; 
//occurrence['GCS/CAS/A44']= "."; 
}
occurrencesPersonnePhysiqueDirigeante.push(occurrence);
occurrence = {}; 
}

// 2ème exploitant
//Groupe DIU
if(Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant') and not Value('id').of(exploitant1.objetModifExploitant).eq('modifExploitant')) {					
if(Value('id').of(exploitant1.objetModifExploitant).eq('partantExploitant')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(exploitant1.objetModifExploitant).eq('nouveauExploitant')) {
occurrence['DIU/U50/U50.1'] = "2" ;
}
if (Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant')) {
if(exploitant1.modifDateExploitantIndivision != null) {
var dateTemp = new Date(parseInt(exploitant1.modifDateExploitantIndivision.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}}	
occurrence['DIU/U51/U51.1']= "86";
occurrence['DIU/U52']= "P";

// Groupe IPD
occurrence['IPD/D01/D01.2']= exploitant2.nomNaissanceExploitant != null ? exploitant2.nomNaissanceExploitant : exploitant3.nomNaissanceExploitant;


if (exploitant2.prenomExploitant != null and exploitant2.prenomExploitant[0].length > 0) {
var prenoms=[];
for ( i = 0; i < exploitant2.prenomExploitant.size() ; i++ ){prenoms.push(exploitant2.prenomExploitant[i]);}      
occurrence['IPD/D01/D01.3']= prenoms;
} else if (exploitant3.prenomExploitant != null and exploitant3.prenomExploitant[0].length > 0){
var prenoms=[];
for ( i = 0; i < exploitant3.prenomExploitant.size() ; i++ ){prenoms.push(exploitant3.prenomExploitant[i]);}      
occurrence['IPD/D01/D01.3']= prenoms;
} 

if (exploitant2.nomUsageExploitant != null or exploitant3.nomUsageExploitant != null) {
occurrence['IPD/D01/D01.4']= exploitant2.nomUsageExploitant != null ? exploitant2.nomUsageExploitant : (exploitant3.nomUsageExploitant != null ? exploitant3.nomUsageExploitant : '');
}
if(Value('id').of(exploitant1. objetModifExploitant).eq('partantExploitant')) {
if(exploitant2.dateNaissanceExploitant != null) {
var dateTemp = new Date(parseInt(exploitant2.dateNaissanceExploitant.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['IPD/D03/D03.1']= date;
}
if (not Value('id').of(exploitant2.lieuNaissancePaysExploitant).eq('FRXXXXX')) {
	var idPays = exploitant2.lieuNaissancePaysExploitant.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(exploitant2.lieuNaissancePaysExploitant).eq('FRXXXXX')) {
occurrence['IPD/D03/D03.2'] = exploitant2.lieuNaissanceCommuneExploitant.getId();
}	
if (not Value('id').of(exploitant2.lieuNaissancePaysExploitant).eq('FRXXXXX')) {																					 
occurrence['IPD/D03/D03.3']= exploitant2.lieuNaissancePaysExploitant.getLabel();
}
if (Value('id').of(exploitant2.lieuNaissancePaysExploitant).eq('FRXXXXX')) {	
occurrence['IPD/D03/D03.4']= exploitant2.lieuNaissanceCommuneExploitant.getLabel();
}
occurrence['IPD/D04/D04.3'] = exploitant2.adresseDomicileExploitant2.exploitantAdresseCommune.getId();
if (exploitant2.adresseDomicileExploitant2.exploitantAdresseNumeroVoie != null) {
occurrence['IPD/D04/D04.5'] = exploitant2.adresseDomicileExploitant2.exploitantAdresseNumeroVoie;
}
if (exploitant2.adresseDomicileExploitant2.exploitantAdresseIndiceVoie != null) {
var monId1 = Value('id').of(exploitant2.adresseDomicileExploitant2.exploitantAdresseIndiceVoie)._eval();
occurrence['IPD/D04/D04.6']          = monId1;
}
if (exploitant2.adresseDomicileExploitant2.exploitantAdresseDistriutionSpeciale != null) {
occurrence['IPD/D04/D04.7'] = exploitant2.adresseDomicileExploitant2.exploitantAdresseDistriutionSpeciale;
}
occurrence['IPD/D04/D04.8'] = exploitant2.adresseDomicileExploitant2.exploitantAdresseCodePostal;
if (exploitant2.adresseDomicileExploitant2.exploitantAdresseComplementAdresse != null) {
occurrence['IPD/D04/D04.10'] = exploitant2.adresseDomicileExploitant2.exploitantAdresseComplementAdresse;
}
if (exploitant2.adresseDomicileExploitant2.exploitantAdresseTypeVoie != null) {
var monId1 = Value('id').of(exploitant2.adresseDomicileExploitant2.exploitantAdresseTypeVoie)._eval();
occurrence['IPD/D04/D04.11']          = monId1;
}
occurrence['IPD/D04/D04.12'] = exploitant2.adresseDomicileExploitant2.exploitantAdresseNomVoie;
occurrence['IPD/D04/D04.13'] = exploitant2.adresseDomicileExploitant2.exploitantAdresseCommune.getLabel();

// Groupe NPD
if (exploitant2.nationaliteExploitant != null) {
occurrence['NPD/D21']= exploitant2.nationaliteExploitant;
}

// Groupe GCS
// ISS	
var nirExploitant = social.voletSocialNumeroSecuriteSociale;
if(nirExploitant != null) {
    nirExploitant = nirExploitant.replace(/ /g, "");
occurrence['GCS/ISS/A10/A10.1']=  nirExploitant.substring(0, 13);
occurrence['GCS/ISS/A10/A10.2']=  nirExploitant.substring(13, 15);
}	
// SNS
if (social.voletSocialActiviteExerceeAnterieurementTNS) {
if(social.voletSocialActiviteExerceeAnterieurementDateCessationTNS != null) {
var dateTemp = new Date(parseInt(social.voletSocialActiviteExerceeAnterieurementDateCessationTNS.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['GCS/SNS/A21/A21.2']= date;
}	
occurrence['GCS/SNS/A21/A21.3']= social.voletSocialActiviteExerceeAnterieurementLibelleTNS;
occurrence['GCS/SNS/A21/A21.4']= social.voletSocialActiviteExerceeAnterieurementCommuneTNS.getId();
occurrence['GCS/SNS/A21/A21.6']= social.voletSocialActiviteExerceeAnterieurementCommuneTNS.getLabel();
}
occurrence['GCS/SNS/A22/A22.3']= Value('id').of(social.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeGeneralTNS') ? "R" :
								 (Value('id').of(social.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAgricoleTNS') ? "A" : 
								 (Value('id').of(social.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeNonSalarieNonAgricoleTNS') ? "N" : " X"));
if (Value('id').of(social.voletSocialRegimeAssuranceMaladieRegime).eq('voletSocialRegimeAssuranceMaladieRegimeAutreTNS')) {
occurrence['GCS/SNS/A22/A22.4']= social.voletSocialRegimeAssuranceMaladieRegimeAutrePrecisionTNS;
}
if (social.voletSocialActiviteAutreQueDeclareeStatutOui) {
occurrence['GCS/SNS/A24/A24.2']= Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutSalarie') ? "1" :
								 (Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutSalarieAgricole') ? "2" : 
								 (Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutRetraitePensionne') ? "8" : " 9"));
if (Value('id').of(social.voletSocialActiviteAutreQueDeclaree).eq('VoletSocialActiviteAutreQueDeclareeStatutCocheAutre')) {
occurrence['GCS/SNS/A24/A24.3']= social.voletSocialActiviteAutreQueDeclareeStatutAutre;
}
}
// CAS
occurrence['GCS/CAS/A42/A42.1']= "."; 
//occurrence['GCS/CAS/A44']= "."; 
}
occurrencesPersonnePhysiqueDirigeante.push(occurrence);
occurrence = {}; 
}

// 1er héritier indivisaire
//Groupe DIU
if(Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers')
	and ((not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or personneLiee1.indivisaireHeritier)) {
if(Value('id').of(personneLiee1.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(personneLiee1.objetModifPersonneLiee).eq('partantPersonneLiee')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(personneLiee1.objetModifPersonneLiee).eq('modifPersonneLiee')) {
occurrence['DIU/U50/U50.1'] = "3" ;
}
if (Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant') and exploitant1.modifDateExploitantIndivision != null) {
var dateTemp = new Date(parseInt(exploitant1.modifDateExploitantIndivision.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} if (not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant') and personneLiee.modifDatePersonneLiee != null) {
var dateTemp = new Date(parseInt(personneLiee.modifDatePersonneLiee.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}		
occurrence['DIU/U51/U51.1']= "82";
occurrence['DIU/U52']= "P";

// Groupe IPD
occurrence['IPD/D01/D01.2']= personneLiee1.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
var prenoms=[];
for ( i = 0; i < personneLiee1.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee1.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}      
occurrence['IPD/D01/D01.3']= prenoms;
if (personneLiee1.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null) {
occurrence['IPD/D01/D01.4']= exploitant2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir;
}
if(not Value('id').of(personneLiee1.objetModifPersonneLiee).eq('partantPersonneLiee')) {
/*
if(personneLiee1.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir != null) {
var dateTemp = new Date(parseInt(personneLiee1.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['IPD/D03/D03.1']= date;
}
if (not Value('id').of(personneLiee1.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
	var idPays = personneLiee1.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(personneLiee1.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
occurrence['IPD/D03/D03.2'] = personneLiee1.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getId();
}	
if (not Value('id').of(personneLiee1.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {																					 
occurrence['IPD/D03/D03.3']= personneLiee1.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getLabel();
}
if (Value('id').of(personneLiee1.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {	
occurrence['IPD/D03/D03.4']= personneLiee1.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getLabel();
}
*/
occurrence['IPD/D04/D04.3'] = personneLiee1.cadreAdressePersonnePouvoir.personneLieeAdresseCommunepersonnePouvoir.getId();
if (personneLiee1.cadreAdressePersonnePouvoir.rueNumeroAdressePersonnePouvoir != null) {
occurrence['IPD/D04/D04.5'] = personneLiee1.cadreAdressePersonnePouvoir.rueNumeroAdressePersonnePouvoir;
}
if (personneLiee1.cadreAdressePersonnePouvoir.indiceVoiePersonnePouvoir != null) {
var monId1 = Value('id').of(personneLiee1.cadreAdressePersonnePouvoir.indiceVoiePersonnePouvoir)._eval();
occurrence['IPD/D04/D04.6']          = monId1;
}
if (personneLiee1.cadreAdressePersonnePouvoir.rueComplementAdressePersonnePouvoir != null) {
occurrence['IPD/D04/D04.7'] = personneLiee1.cadreAdressePersonnePouvoir.rueComplementAdressePersonnePouvoir;
}
occurrence['IPD/D04/D04.8'] = personneLiee1.cadreAdressePersonnePouvoir.personneLieeAdresseCodePostalPersonnePouvoir;
if (personneLiee1.cadreAdressePersonnePouvoir.rueComplementAdressePersonnePouvoir != null) {
occurrence['IPD/D04/D04.10'] = personneLiee1.cadreAdressePersonnePouvoir.rueComplementAdressePersonnePouvoir;
}
if (personneLiee1.cadreAdressePersonnePouvoir.typeVoiePersonnePouvoir != null) {
var monId1 = Value('id').of(personneLiee1.cadreAdressePersonnePouvoir.typeVoiePersonnePouvoir)._eval();
occurrence['IPD/D04/D04.11']          = monId1;
}
occurrence['IPD/D04/D04.12'] = personneLiee1.cadreAdressePersonnePouvoir.nomVoiePersonnePouvoir;
occurrence['IPD/D04/D04.13'] = personneLiee1.cadreAdressePersonnePouvoir.personneLieeAdresseCommunepersonnePouvoir.getLabel();

// Groupe NPD
/*
if (personneLiee1.nationaliteExploitant != null) {
occurrence['NPD/D21']= exploitant2.nationaliteExploitant;
}
*/
}
occurrencesPersonnePhysiqueDirigeante.push(occurrence);
occurrence = {}; 
}

// 2ème héritier indivisaire
//Groupe DIU
if((Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers')
	and ((not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or personneLiee2.indivisaireHeritier))
	and personneLiee2.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir != null)	{
if(Value('id').of(personneLiee2.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(personneLiee2.objetModifPersonneLiee).eq('partantPersonneLiee')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(personneLiee2.objetModifPersonneLiee).eq('modifPersonneLiee')) {
occurrence['DIU/U50/U50.1'] = "3" ;
}
if (Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant') and exploitant1.modifDateExploitantIndivision != null) {
var dateTemp = new Date(parseInt(exploitant1.modifDateExploitantIndivision.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} if (not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant') and personneLiee.modifDatePersonneLiee != null) {
var dateTemp = new Date(parseInt(personneLiee.modifDatePersonneLiee.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}		
occurrence['DIU/U51/U51.1']= "82";
occurrence['DIU/U52']= "P";

// Groupe IPD
occurrence['IPD/D01/D01.2']= personneLiee2.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
var prenoms=[];
for ( i = 0; i < personneLiee2.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee2.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}      
occurrence['IPD/D01/D01.3']= prenoms;
if (personneLiee2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null) {
occurrence['IPD/D01/D01.4']= exploitant2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir;
}
if(not Value('id').of(personneLiee2.objetModifPersonneLiee).eq('partantPersonneLiee')) {
/*
if(personneLiee2.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir != null) {
var dateTemp = new Date(parseInt(personneLiee2.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['IPD/D03/D03.1']= date;
}
if (not Value('id').of(personneLiee2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
	var idPays = personneLiee2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(personneLiee2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
occurrence['IPD/D03/D03.2'] = personneLiee2.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getId();
}	
if (not Value('id').of(personneLiee2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {																					 
occurrence['IPD/D03/D03.3']= personneLiee2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getLabel();
}
if (Value('id').of(personneLiee2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {	
occurrence['IPD/D03/D03.4']= personneLiee2.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getLabel();
}
*/
occurrence['IPD/D04/D04.3'] = personneLiee2.cadreAdressePersonnePouvoir2.personneLieeAdresseCommunepersonnePouvoir.getId();
if (personneLiee2.cadreAdressePersonnePouvoir2.rueNumeroAdressePersonnePouvoir != null) {
occurrence['IPD/D04/D04.5'] = personneLiee2.cadreAdressePersonnePouvoir2.rueNumeroAdressePersonnePouvoir;
}
if (personneLiee2.cadreAdressePersonnePouvoir2.indiceVoiePersonnePouvoir != null) {
var monId1 = Value('id').of(personneLiee2.cadreAdressePersonnePouvoir2.indiceVoiePersonnePouvoir)._eval();
occurrence['IPD/D04/D04.6']          = monId1;
}
if (personneLiee2.cadreAdressePersonnePouvoir2.rueComplementAdressePersonnePouvoir != null) {
occurrence['IPD/D04/D04.7'] = personneLiee2.cadreAdressePersonnePouvoir2.rueComplementAdressePersonnePouvoir;
}
occurrence['IPD/D04/D04.8'] = personneLiee2.cadreAdressePersonnePouvoir2.personneLieeAdresseCodePostalPersonnePouvoir;
if (personneLiee2.cadreAdressePersonnePouvoir2.rueComplementAdressePersonnePouvoir != null) {
occurrence['IPD/D04/D04.10'] = personneLiee2.cadreAdressePersonnePouvoir2.rueComplementAdressePersonnePouvoir;
}
if (personneLiee2.cadreAdressePersonnePouvoir2.typeVoiePersonnePouvoir != null) {
var monId1 = Value('id').of(personneLiee2.cadreAdressePersonnePouvoir2.typeVoiePersonnePouvoir)._eval();
occurrence['IPD/D04/D04.11']          = monId1;
}
occurrence['IPD/D04/D04.12'] = personneLiee2.cadreAdressePersonnePouvoir2.nomVoiePersonnePouvoir;
occurrence['IPD/D04/D04.13'] = personneLiee2.cadreAdressePersonnePouvoir2.personneLieeAdresseCommunepersonnePouvoir.getLabel();

// Groupe NPD
/*
if (personneLiee2.nationaliteExploitant != null) {
occurrence['NPD/D21']= exploitant2.nationaliteExploitant;
}
*/
}
occurrencesPersonnePhysiqueDirigeante.push(occurrence);
occurrence = {}; 
}

// 3ème héritier indivisaire
//Groupe DIU
if((Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers')
	and ((not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or personneLiee3.indivisaireHeritier))
	and personneLiee2.autreDeclarationPersonneLiee)	{
if(Value('id').of(personneLiee3.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(personneLiee3.objetModifPersonneLiee).eq('partantPersonneLiee')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(personneLiee3.objetModifPersonneLiee).eq('modifPersonneLiee')) {
occurrence['DIU/U50/U50.1'] = "3" ;
}
if (Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant') and exploitant1.modifDateExploitantIndivision != null) {
var dateTemp = new Date(parseInt(exploitant1.modifDateExploitantIndivision.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} if (not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant') and personneLiee.modifDatePersonneLiee != null) {
var dateTemp = new Date(parseInt(personneLiee.modifDatePersonneLiee.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}		
occurrence['DIU/U51/U51.1']= "82";
occurrence['DIU/U52']= "P";

// Groupe IPD
occurrence['IPD/D01/D01.2']= personneLiee3.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
var prenoms=[];
for ( i = 0; i < personneLiee3.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee3.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}      
occurrence['IPD/D01/D01.3']= prenoms;
if (personneLiee3.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null) {
occurrence['IPD/D01/D01.4']= exploitant2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir;
}
if(not Value('id').of(personneLiee3.objetModifPersonneLiee).eq('partantPersonneLiee')) {
/*
if(personneLiee3.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir != null) {
var dateTemp = new Date(parseInt(personneLiee3.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['IPD/D03/D03.1']= date;
}
if (not Value('id').of(personneLiee3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
	var idPays = personneLiee3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(personneLiee3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
occurrence['IPD/D03/D03.2'] = personneLiee3.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getId();
}	
if (not Value('id').of(personneLiee3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {																					 
occurrence['IPD/D03/D03.3']= personneLiee3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getLabel();
}
if (Value('id').of(personneLiee3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {	
occurrence['IPD/D03/D03.4']= personneLiee3.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getLabel();
}
*/
occurrence['IPD/D04/D04.3'] = personneLiee3.cadreAdressePersonnePouvoir3.personneLieeAdresseCommunepersonnePouvoir.getId();
if (personneLiee3.cadreAdressePersonnePouvoir3.rueNumeroAdressePersonnePouvoir != null) {
occurrence['IPD/D04/D04.5'] = personneLiee3.cadreAdressePersonnePouvoir3.rueNumeroAdressePersonnePouvoir;
}
if (personneLiee3.cadreAdressePersonnePouvoir3.indiceVoiePersonnePouvoir != null) {
var monId1 = Value('id').of(personneLiee3.cadreAdressePersonnePouvoir3.indiceVoiePersonnePouvoir)._eval();
occurrence['IPD/D04/D04.6']          = monId1;
}
if (personneLiee3.cadreAdressePersonnePouvoir3.rueComplementAdressePersonnePouvoir != null) {
occurrence['IPD/D04/D04.7'] = personneLiee3.cadreAdressePersonnePouvoir3.rueComplementAdressePersonnePouvoir;
}
occurrence['IPD/D04/D04.8'] = personneLiee3.cadreAdressePersonnePouvoir3.personneLieeAdresseCodePostalPersonnePouvoir;
if (personneLiee3.cadreAdressePersonnePouvoir3.rueComplementAdressePersonnePouvoir != null) {
occurrence['IPD/D04/D04.10'] = personneLiee3.cadreAdressePersonnePouvoir3.rueComplementAdressePersonnePouvoir;
}
if (personneLiee3.cadreAdressePersonnePouvoir3.typeVoiePersonnePouvoir != null) {
var monId1 = Value('id').of(personneLiee3.cadreAdressePersonnePouvoir3.typeVoiePersonnePouvoir)._eval();
occurrence['IPD/D04/D04.11']          = monId1;
}
occurrence['IPD/D04/D04.12'] = personneLiee3.cadreAdressePersonnePouvoir3.nomVoiePersonnePouvoir;
occurrence['IPD/D04/D04.13'] = personneLiee3.cadreAdressePersonnePouvoir3.personneLieeAdresseCommunepersonnePouvoir.getLabel();

// Groupe NPD
/*
if (personneLiee3.nationaliteExploitant != null) {
occurrence['NPD/D21']= exploitant2.nationaliteExploitant;
}
*/
}
occurrencesPersonnePhysiqueDirigeante.push(occurrence);
occurrence = {}; 
}

// 4ème héritier indivisaire
//Groupe DIU
if((Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers')
	and ((not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or personneLiee4.indivisaireHeritier))
	and personneLiee3.autreDeclarationPersonneLiee)	{
if(Value('id').of(personneLiee4.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(personneLiee4.objetModifPersonneLiee).eq('partantPersonneLiee')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(personneLiee4.objetModifPersonneLiee).eq('modifPersonneLiee')) {
occurrence['DIU/U50/U50.1'] = "3" ;
}
if (Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant') and exploitant1.modifDateExploitantIndivision != null) {
var dateTemp = new Date(parseInt(exploitant1.modifDateExploitantIndivision.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} if (not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant') and personneLiee.modifDatePersonneLiee != null) {
var dateTemp = new Date(parseInt(personneLiee.modifDatePersonneLiee.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}		
occurrence['DIU/U51/U51.1']= "82";
occurrence['DIU/U52']= "P";

// Groupe IPD
occurrence['IPD/D01/D01.2']= personneLiee4.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
var prenoms=[];
for ( i = 0; i < personneLiee4.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee4.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}      
occurrence['IPD/D01/D01.3']= prenoms;
if (personneLiee4.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null) {
occurrence['IPD/D01/D01.4']= exploitant2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir;
}
if(not Value('id').of(personneLiee4.objetModifPersonneLiee).eq('partantPersonneLiee')) {
/*
if(personneLiee4.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir != null) {
var dateTemp = new Date(parseInt(personneLiee4.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['IPD/D03/D03.1']= date;
}
if (not Value('id').of(personneLiee4.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
	var idPays = personneLiee4.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(personneLiee4.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
occurrence['IPD/D03/D03.2'] = personneLiee4.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getId();
}	
if (not Value('id').of(personneLiee4.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {																					 
occurrence['IPD/D03/D03.3']= personneLiee4.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getLabel();
}
if (Value('id').of(personneLiee4.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {	
occurrence['IPD/D03/D03.4']= personneLiee4.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getLabel();
}
*/
occurrence['IPD/D04/D04.3'] = personneLiee4.cadreAdressePersonnePouvoir4.personneLieeAdresseCommunepersonnePouvoir.getId();
if (personneLiee4.cadreAdressePersonnePouvoir4.rueNumeroAdressePersonnePouvoir != null) {
occurrence['IPD/D04/D04.5'] = personneLiee4.cadreAdressePersonnePouvoir4.rueNumeroAdressePersonnePouvoir;
}
if (personneLiee4.cadreAdressePersonnePouvoir4.indiceVoiePersonnePouvoir != null) {
var monId1 = Value('id').of(personneLiee4.cadreAdressePersonnePouvoir4.indiceVoiePersonnePouvoir)._eval();
occurrence['IPD/D04/D04.6']          = monId1;
}
if (personneLiee4.cadreAdressePersonnePouvoir4.rueComplementAdressePersonnePouvoir != null) {
occurrence['IPD/D04/D04.7'] = personneLiee4.cadreAdressePersonnePouvoir4.rueComplementAdressePersonnePouvoir;
}
occurrence['IPD/D04/D04.8'] = personneLiee4.cadreAdressePersonnePouvoir4.personneLieeAdresseCodePostalPersonnePouvoir;
if (personneLiee4.cadreAdressePersonnePouvoir4.rueComplementAdressePersonnePouvoir != null) {
occurrence['IPD/D04/D04.10'] = personneLiee4.cadreAdressePersonnePouvoir4.rueComplementAdressePersonnePouvoir;
}
if (personneLiee4.cadreAdressePersonnePouvoir4.typeVoiePersonnePouvoir != null) {
var monId1 = Value('id').of(personneLiee4.cadreAdressePersonnePouvoir4.typeVoiePersonnePouvoir)._eval();
occurrence['IPD/D04/D04.11']          = monId1;
}
occurrence['IPD/D04/D04.12'] = personneLiee4.cadreAdressePersonnePouvoir4.nomVoiePersonnePouvoir;
occurrence['IPD/D04/D04.13'] = personneLiee4.cadreAdressePersonnePouvoir4.personneLieeAdresseCommunepersonnePouvoir.getLabel();

// Groupe NPD
/*
if (personneLiee4.nationaliteExploitant != null) {
occurrence['NPD/D21']= exploitant2.nationaliteExploitant;
}
*/
}
occurrencesPersonnePhysiqueDirigeante.push(occurrence);
occurrence = {}; 
}

// 5ème héritier indivisaire
//Groupe DIU
if((Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers')
	and ((not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or personneLiee5.indivisaireHeritier))
	and personneLiee4.autreDeclarationPersonneLiee)	{
if(Value('id').of(personneLiee5.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(personneLiee5.objetModifPersonneLiee).eq('partantPersonneLiee')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(personneLiee5.objetModifPersonneLiee).eq('modifPersonneLiee')) {
occurrence['DIU/U50/U50.1'] = "3" ;
}
if (Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant') and exploitant1.modifDateExploitantIndivision != null) {
var dateTemp = new Date(parseInt(exploitant1.modifDateExploitantIndivision.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} if (not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant') and personneLiee.modifDatePersonneLiee !=null) {
var dateTemp = new Date(parseInt(personneLiee.modifDatePersonneLiee.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}		
occurrence['DIU/U51/U51.1']= "82";
occurrence['DIU/U52']= "P";

// Groupe IPD
occurrence['IPD/D01/D01.2']= personneLiee5.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
var prenoms=[];
for ( i = 0; i < personneLiee5.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee5.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}      
occurrence['IPD/D01/D01.3']= prenoms;
if (personneLiee5.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null) {
occurrence['IPD/D01/D01.4']= exploitant2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir;
}
if(not Value('id').of(personneLiee5.objetModifPersonneLiee).eq('partantPersonneLiee')) {
/*
if(personneLiee5.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir != null) {
var dateTemp = new Date(parseInt(personneLiee5.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['IPD/D03/D03.1']= date;
}
if (not Value('id').of(personneLiee5.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
	var idPays = personneLiee5.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(personneLiee5.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
occurrence['IPD/D03/D03.2'] = personneLiee5.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getId();
}	
if (not Value('id').of(personneLiee5.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {																					 
occurrence['IPD/D03/D03.3']= personneLiee5.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getLabel();
}
if (Value('id').of(personneLiee5.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {	
occurrence['IPD/D03/D03.4']= personneLiee5.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getLabel();
}
*/
occurrence['IPD/D04/D04.3'] = personneLiee5.cadreAdressePersonnePouvoir5.personneLieeAdresseCommunepersonnePouvoir.getId();
if (personneLiee5.cadreAdressePersonnePouvoir5.rueNumeroAdressePersonnePouvoir != null) {
occurrence['IPD/D04/D04.5'] = personneLiee5.cadreAdressePersonnePouvoir5.rueNumeroAdressePersonnePouvoir;
}
if (personneLiee5.cadreAdressePersonnePouvoir5.indiceVoiePersonnePouvoir != null) {
var monId1 = Value('id').of(personneLiee5.cadreAdressePersonnePouvoir5.indiceVoiePersonnePouvoir)._eval();
occurrence['IPD/D04/D04.6']          = monId1;
}
if (personneLiee5.cadreAdressePersonnePouvoir5.rueComplementAdressePersonnePouvoir != null) {
occurrence['IPD/D04/D04.7'] = personneLiee5.cadreAdressePersonnePouvoir5.rueComplementAdressePersonnePouvoir;
}
occurrence['IPD/D04/D04.8'] = personneLiee5.cadreAdressePersonnePouvoir5.personneLieeAdresseCodePostalPersonnePouvoir;
if (personneLiee5.cadreAdressePersonnePouvoir5.rueComplementAdressePersonnePouvoir != null) {
occurrence['IPD/D04/D04.10'] = personneLiee5.cadreAdressePersonnePouvoir5.rueComplementAdressePersonnePouvoir;
}
if (personneLiee5.cadreAdressePersonnePouvoir5.typeVoiePersonnePouvoir != null) {
var monId1 = Value('id').of(personneLiee5.cadreAdressePersonnePouvoir5.typeVoiePersonnePouvoir)._eval();
occurrence['IPD/D04/D04.11']          = monId1;
}
occurrence['IPD/D04/D04.12'] = personneLiee5.cadreAdressePersonnePouvoir5.nomVoiePersonnePouvoir;
occurrence['IPD/D04/D04.13'] = personneLiee5.cadreAdressePersonnePouvoir5.personneLieeAdresseCommunepersonnePouvoir.getLabel();

// Groupe NPD
/*
if (personneLiee5.nationaliteExploitant != null) {
occurrence['NPD/D21']= exploitant2.nationaliteExploitant;
}
*/
}
occurrencesPersonnePhysiqueDirigeante.push(occurrence);
occurrence = {}; 
}

// 6ème héritier indivisaire
//Groupe DIU
if((Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers')
	and ((not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or personneLiee6.indivisaireHeritier))
	and personneLiee5.autreDeclarationPersonneLiee)	{
if(Value('id').of(personneLiee6.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(personneLiee6.objetModifPersonneLiee).eq('partantPersonneLiee')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(personneLiee6.objetModifPersonneLiee).eq('modifPersonneLiee')) {
occurrence['DIU/U50/U50.1'] = "3" ;
}
if (Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant') and exploitant1.modifDateExploitantIndivision != null) {
var dateTemp = new Date(parseInt(exploitant1.modifDateExploitantIndivision.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} if (not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant') and personneLiee.modifDatePersonneLiee != null) {
var dateTemp = new Date(parseInt(personneLiee.modifDatePersonneLiee.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}		
occurrence['DIU/U51/U51.1']= "82";
occurrence['DIU/U52']= "P";

// Groupe IPD
occurrence['IPD/D01/D01.2']= personneLiee6.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
var prenoms=[];
for ( i = 0; i < personneLiee6.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee6.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}      
occurrence['IPD/D01/D01.3']= prenoms;
if (personneLiee6.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null) {
occurrence['IPD/D01/D01.4']= exploitant2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir;
}
if(not Value('id').of(personneLiee6.objetModifPersonneLiee).eq('partantPersonneLiee')) {
/*
if(personneLiee6.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir != null) {
var dateTemp = new Date(parseInt(personneLiee6.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['IPD/D03/D03.1']= date;
}
if (not Value('id').of(personneLiee6.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
	var idPays = personneLiee6.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(personneLiee6.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
occurrence['IPD/D03/D03.2'] = personneLiee6.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getId();
}	
if (not Value('id').of(personneLiee6.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {																					 
occurrence['IPD/D03/D03.3']= personneLiee6.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getLabel();
}
if (Value('id').of(personneLiee6.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {	
occurrence['IPD/D03/D03.4']= personneLiee6.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getLabel();
}
*/
occurrence['IPD/D04/D04.3'] = personneLiee6.cadreAdressePersonnePouvoir6.personneLieeAdresseCommunepersonnePouvoir.getId();
if (personneLiee6.cadreAdressePersonnePouvoir6.rueNumeroAdressePersonnePouvoir != null) {
occurrence['IPD/D04/D04.5'] = personneLiee6.cadreAdressePersonnePouvoir6.rueNumeroAdressePersonnePouvoir;
}
if (personneLiee6.cadreAdressePersonnePouvoir6.indiceVoiePersonnePouvoir != null) {
var monId1 = Value('id').of(personneLiee6.cadreAdressePersonnePouvoir6.indiceVoiePersonnePouvoir)._eval();
occurrence['IPD/D04/D04.6']          = monId1;
}
if (personneLiee6.cadreAdressePersonnePouvoir6.rueComplementAdressePersonnePouvoir != null) {
occurrence['IPD/D04/D04.7'] = personneLiee6.cadreAdressePersonnePouvoir6.rueComplementAdressePersonnePouvoir;
}
occurrence['IPD/D04/D04.8'] = personneLiee6.cadreAdressePersonnePouvoir6.personneLieeAdresseCodePostalPersonnePouvoir;
if (personneLiee6.cadreAdressePersonnePouvoir6.rueComplementAdressePersonnePouvoir != null) {
occurrence['IPD/D04/D04.10'] = personneLiee6.cadreAdressePersonnePouvoir6.rueComplementAdressePersonnePouvoir;
}
if (personneLiee6.cadreAdressePersonnePouvoir6.typeVoiePersonnePouvoir != null) {
var monId1 = Value('id').of(personneLiee6.cadreAdressePersonnePouvoir6.typeVoiePersonnePouvoir)._eval();
occurrence['IPD/D04/D04.11']          = monId1;
}
occurrence['IPD/D04/D04.12'] = personneLiee6.cadreAdressePersonnePouvoir6.nomVoiePersonnePouvoir;
occurrence['IPD/D04/D04.13'] = personneLiee6.cadreAdressePersonnePouvoir6.personneLieeAdresseCommunepersonnePouvoir.getLabel();

// Groupe NPD
/*
if (personneLiee6.nationaliteExploitant != null) {
occurrence['NPD/D21']= exploitant2.nationaliteExploitant;
}
*/
}
occurrencesPersonnePhysiqueDirigeante.push(occurrence);
occurrence = {}; 
}

// 7ème héritier indivisaire
//Groupe DIU
if((Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers')
	and ((not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or personneLiee7.indivisaireHeritier))
	and personneLiee6.autreDeclarationPersonneLiee)	{
if(Value('id').of(personneLiee7.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(personneLiee7.objetModifPersonneLiee).eq('partantPersonneLiee')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(personneLiee7.objetModifPersonneLiee).eq('modifPersonneLiee')) {
occurrence['DIU/U50/U50.1'] = "3" ;
}
if (Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant') and exploitant1.modifDateExploitantIndivision != null) {
var dateTemp = new Date(parseInt(exploitant1.modifDateExploitantIndivision.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} if (not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant') and personneLiee.modifDatePersonneLiee != null) {
var dateTemp = new Date(parseInt(personneLiee.modifDatePersonneLiee.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}		
occurrence['DIU/U51/U51.1']= "82";
occurrence['DIU/U52']= "P";

// Groupe IPD
occurrence['IPD/D01/D01.2']= personneLiee7.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
var prenoms=[];
for ( i = 0; i < personneLiee7.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee7.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}      
occurrence['IPD/D01/D01.3']= prenoms;
if (personneLiee7.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null) {
occurrence['IPD/D01/D01.4']= exploitant2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir;
}
if(not Value('id').of(personneLiee7.objetModifPersonneLiee).eq('partantPersonneLiee')) {
/*
if(personneLiee7.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir != null) {
var dateTemp = new Date(parseInt(personneLiee7.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['IPD/D03/D03.1']= date;
}
if (not Value('id').of(personneLiee7.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
	var idPays = personneLiee7.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(personneLiee7.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
occurrence['IPD/D03/D03.2'] = personneLiee7.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getId();
}	
if (not Value('id').of(personneLiee7.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {																					 
occurrence['IPD/D03/D03.3']= personneLiee7.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getLabel();
}
if (Value('id').of(personneLiee7.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {	
occurrence['IPD/D03/D03.4']= personneLiee7.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getLabel();
}
*/
occurrence['IPD/D04/D04.3'] = personneLiee7.cadreAdressePersonnePouvoir7.personneLieeAdresseCommunepersonnePouvoir.getId();
if (personneLiee7.cadreAdressePersonnePouvoir7.rueNumeroAdressePersonnePouvoir != null) {
occurrence['IPD/D04/D04.5'] = personneLiee7.cadreAdressePersonnePouvoir7.rueNumeroAdressePersonnePouvoir;
}
if (personneLiee7.cadreAdressePersonnePouvoir7.indiceVoiePersonnePouvoir != null) {
var monId1 = Value('id').of(personneLiee7.cadreAdressePersonnePouvoir7.indiceVoiePersonnePouvoir)._eval();
occurrence['IPD/D04/D04.6']          = monId1;
}
if (personneLiee7.cadreAdressePersonnePouvoir7.rueComplementAdressePersonnePouvoir != null) {
occurrence['IPD/D04/D04.7'] = personneLiee7.cadreAdressePersonnePouvoir7.rueComplementAdressePersonnePouvoir;
}
occurrence['IPD/D04/D04.8'] = personneLiee7.cadreAdressePersonnePouvoir7.personneLieeAdresseCodePostalPersonnePouvoir;
if (personneLiee7.cadreAdressePersonnePouvoir7.rueComplementAdressePersonnePouvoir != null) {
occurrence['IPD/D04/D04.10'] = personneLiee7.cadreAdressePersonnePouvoir7.rueComplementAdressePersonnePouvoir;
}
if (personneLiee7.cadreAdressePersonnePouvoir7.typeVoiePersonnePouvoir != null) {
var monId1 = Value('id').of(personneLiee7.cadreAdressePersonnePouvoir7.typeVoiePersonnePouvoir)._eval();
occurrence['IPD/D04/D04.11']          = monId1;
}
occurrence['IPD/D04/D04.12'] = personneLiee7.cadreAdressePersonnePouvoir7.nomVoiePersonnePouvoir;
occurrence['IPD/D04/D04.13'] = personneLiee7.cadreAdressePersonnePouvoir7.personneLieeAdresseCommunepersonnePouvoir.getLabel();

// Groupe NPD
/*
if (personneLiee7.nationaliteExploitant != null) {
occurrence['NPD/D21']= exploitant2.nationaliteExploitant;
}
*/
}
occurrencesPersonnePhysiqueDirigeante.push(occurrence);
occurrence = {}; 
}

// 8ème héritier indivisaire
//Groupe DIU
if((Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers')
	and ((not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or personneLiee8.indivisaireHeritier))
	and personneLiee7.autreDeclarationPersonneLiee)	{
if(Value('id').of(personneLiee8.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(personneLiee8.objetModifPersonneLiee).eq('partantPersonneLiee')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(personneLiee8.objetModifPersonneLiee).eq('modifPersonneLiee')) {
occurrence['DIU/U50/U50.1'] = "3" ;
}
if (Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant') and exploitant1.modifDateExploitantIndivision != null) {
var dateTemp = new Date(parseInt(exploitant1.modifDateExploitantIndivision.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} if (not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant') and personneLiee.modifDatePersonneLiee != null) {
var dateTemp = new Date(parseInt(personneLiee.modifDatePersonneLiee.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}		
occurrence['DIU/U51/U51.1']= "82";
occurrence['DIU/U52']= "P";

// Groupe IPD
occurrence['IPD/D01/D01.2']= personneLiee8.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
var prenoms=[];
for ( i = 0; i < personneLiee8.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee8.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}      
occurrence['IPD/D01/D01.3']= prenoms;
if (personneLiee8.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null) {
occurrence['IPD/D01/D01.4']= exploitant2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir;
}
if(not Value('id').of(personneLiee8.objetModifPersonneLiee).eq('partantPersonneLiee')) {
/*
if(personneLiee8.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir != null) {
var dateTemp = new Date(parseInt(personneLiee8.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['IPD/D03/D03.1']= date;
}
if (not Value('id').of(personneLiee8.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
	var idPays = personneLiee8.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(personneLiee8.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
occurrence['IPD/D03/D03.2'] = personneLiee8.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getId();
}	
if (not Value('id').of(personneLiee8.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {																					 
occurrence['IPD/D03/D03.3']= personneLiee8.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getLabel();
}
if (Value('id').of(personneLiee8.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {	
occurrence['IPD/D03/D03.4']= personneLiee8.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getLabel();
}
*/
occurrence['IPD/D04/D04.3'] = personneLiee8.cadreAdressePersonnePouvoir8.personneLieeAdresseCommunepersonnePouvoir.getId();
if (personneLiee8.cadreAdressePersonnePouvoir8.rueNumeroAdressePersonnePouvoir != null) {
occurrence['IPD/D04/D04.5'] = personneLiee8.cadreAdressePersonnePouvoir8.rueNumeroAdressePersonnePouvoir;
}
if (personneLiee8.cadreAdressePersonnePouvoir8.indiceVoiePersonnePouvoir != null) {
var monId1 = Value('id').of(personneLiee8.cadreAdressePersonnePouvoir8.indiceVoiePersonnePouvoir)._eval();
occurrence['IPD/D04/D04.6']          = monId1;
}
if (personneLiee8.cadreAdressePersonnePouvoir8.rueComplementAdressePersonnePouvoir != null) {
occurrence['IPD/D04/D04.7'] = personneLiee8.cadreAdressePersonnePouvoir8.rueComplementAdressePersonnePouvoir;
}
occurrence['IPD/D04/D04.8'] = personneLiee8.cadreAdressePersonnePouvoir8.personneLieeAdresseCodePostalPersonnePouvoir;
if (personneLiee8.cadreAdressePersonnePouvoir8.rueComplementAdressePersonnePouvoir != null) {
occurrence['IPD/D04/D04.10'] = personneLiee8.cadreAdressePersonnePouvoir8.rueComplementAdressePersonnePouvoir;
}
if (personneLiee8.cadreAdressePersonnePouvoir8.typeVoiePersonnePouvoir != null) {
var monId1 = Value('id').of(personneLiee8.cadreAdressePersonnePouvoir8.typeVoiePersonnePouvoir)._eval();
occurrence['IPD/D04/D04.11']          = monId1;
}
occurrence['IPD/D04/D04.12'] = personneLiee8.cadreAdressePersonnePouvoir8.nomVoiePersonnePouvoir;
occurrence['IPD/D04/D04.13'] = personneLiee8.cadreAdressePersonnePouvoir8.personneLieeAdresseCommunepersonnePouvoir.getLabel();

// Groupe NPD
/*
if (personneLiee8.nationaliteExploitant != null) {
occurrence['NPD/D21']= exploitant2.nationaliteExploitant;
}
*/
}
occurrencesPersonnePhysiqueDirigeante.push(occurrence);
occurrence = {}; 
}

// 9ème héritier indivisaire
//Groupe DIU
if((Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers')
	and ((not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	or personneLiee9.indivisaireHeritier))
	and personneLiee8.autreDeclarationPersonneLiee)	{
if(Value('id').of(personneLiee9.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
occurrence['DIU/U50/U50.1'] = "1";
} else if(Value('id').of(personneLiee9.objetModifPersonneLiee).eq('partantPersonneLiee')) {
occurrence['DIU/U50/U50.1'] = "2" ;
} else if(Value('id').of(personneLiee9.objetModifPersonneLiee).eq('modifPersonneLiee')) {
occurrence['DIU/U50/U50.1'] = "3" ;
}
if (Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant') and exploitant1.modifDateExploitantIndivision != null) {
var dateTemp = new Date(parseInt(exploitant1.modifDateExploitantIndivision.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
} if (not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('exploitant') and personneLiee.modifDatePersonneLiee != null) {
var dateTemp = new Date(parseInt(personneLiee.modifDatePersonneLiee.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['DIU/U50/U50.2']= date;
}		
occurrence['DIU/U51/U51.1']= "82";
occurrence['DIU/U52']= "P";

// Groupe IPD
occurrence['IPD/D01/D01.2']= personneLiee9.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
var prenoms=[];
for ( i = 0; i < personneLiee9.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee9.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}      
occurrence['IPD/D01/D01.3']= prenoms;
if (personneLiee9.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null) {
occurrence['IPD/D01/D01.4']= exploitant2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir;
}
if(not Value('id').of(personneLiee9.objetModifPersonneLiee).eq('partantPersonneLiee')) {
/*
if(personneLiee9.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir != null) {
var dateTemp = new Date(parseInt(personneLiee9.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['IPD/D03/D03.1']= date;
}
if (not Value('id').of(personneLiee9.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
	var idPays = personneLiee9.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['IPD/D03/D03.2']= result;
} else if (Value('id').of(personneLiee9.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
occurrence['IPD/D03/D03.2'] = personneLiee9.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getId();
}	
if (not Value('id').of(personneLiee9.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {																					 
occurrence['IPD/D03/D03.3']= personneLiee9.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getLabel();
}
if (Value('id').of(personneLiee9.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {	
occurrence['IPD/D03/D03.4']= personneLiee9.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getLabel();
}
*/
occurrence['IPD/D04/D04.3'] = personneLiee9.cadreAdressePersonnePouvoir9.personneLieeAdresseCommunepersonnePouvoir.getId();
if (personneLiee9.cadreAdressePersonnePouvoir9.rueNumeroAdressePersonnePouvoir != null) {
occurrence['IPD/D04/D04.5'] = personneLiee9.cadreAdressePersonnePouvoir9.rueNumeroAdressePersonnePouvoir;
}
if (personneLiee9.cadreAdressePersonnePouvoir9.indiceVoiePersonnePouvoir != null) {
var monId1 = Value('id').of(personneLiee9.cadreAdressePersonnePouvoir9.indiceVoiePersonnePouvoir)._eval();
occurrence['IPD/D04/D04.6']          = monId1;
}
if (personneLiee9.cadreAdressePersonnePouvoir9.rueComplementAdressePersonnePouvoir != null) {
occurrence['IPD/D04/D04.7'] = personneLiee9.cadreAdressePersonnePouvoir9.rueComplementAdressePersonnePouvoir;
}
occurrence['IPD/D04/D04.8'] = personneLiee9.cadreAdressePersonnePouvoir9.personneLieeAdresseCodePostalPersonnePouvoir;
if (personneLiee9.cadreAdressePersonnePouvoir9.rueComplementAdressePersonnePouvoir != null) {
occurrence['IPD/D04/D04.10'] = personneLiee9.cadreAdressePersonnePouvoir9.rueComplementAdressePersonnePouvoir;
}
if (personneLiee9.cadreAdressePersonnePouvoir9.typeVoiePersonnePouvoir != null) {
var monId1 = Value('id').of(personneLiee9.cadreAdressePersonnePouvoir9.typeVoiePersonnePouvoir)._eval();
occurrence['IPD/D04/D04.11']          = monId1;
}
occurrence['IPD/D04/D04.12'] = personneLiee9.cadreAdressePersonnePouvoir9.nomVoiePersonnePouvoir;
occurrence['IPD/D04/D04.13'] = personneLiee9.cadreAdressePersonnePouvoir9.personneLieeAdresseCommunepersonnePouvoir.getLabel();

// Groupe NPD
/*
if (personneLiee9.nationaliteExploitant != null) {
occurrence['NPD/D21']= exploitant2.nationaliteExploitant;
}
*/
}
occurrencesPersonnePhysiqueDirigeante.push(occurrence);
occurrence = {}; 
}

occurrencesPersonnePhysiqueDirigeante.forEach(function (value, i) {
	var idx = i+1;
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/DIU/U50/U50.1']= value['DIU/U50/U50.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/DIU/U50/U50.2']= value['DIU/U50/U50.2'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/DIU/U51/U51.1']= value['DIU/U51/U51.1'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/DIU/U52']= value['DIU/U52'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D01/D01.2']= value['IPD/D01/D01.2'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D01/D01.3']= value['IPD/D01/D01.3'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D01/D01.4']= value['IPD/D01/D01.4'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D03/D03.1']= value['IPD/D03/D03.1'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D03/D03.2']= value['IPD/D03/D03.2'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D03/D03.3']= value['IPD/D03/D03.3'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D03/D03.4']= value['IPD/D03/D03.4'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.3']= value['IPD/D04/D04.3'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.5']= value['IPD/D04/D04.5'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.6']= value['IPD/D04/D04.6'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.7']= value['IPD/D04/D04.7'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.8']= value['IPD/D04/D04.8'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.10']= value['IPD/D04/D04.10'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.11']= value['IPD/D04/D04.11'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.12']= value['IPD/D04/D04.12'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/IPD/D04/D04.13']= value['IPD/D04/D04.13'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/NPD/D21']= value['NPD/D21'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/ISS/A10/A10.1']= value['GCS/ISS/A10/A10.1'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/ISS/A10/A10.2']= value['GCS/ISS/A10/A10.2'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A21/A21.2']= value['GCS/SNS/A21/A21.2'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A21/A21.3']= value['GCS/SNS/A21/A21.3'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A21/A21.4']= value['GCS/SNS/A21/A21.4'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A21/A21.6']= value['GCS/SNS/A21/A21.6'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A22/A22.3']= value['GCS/SNS/A22/A22.3'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A22/A22.4']= value['GCS/SNS/A22/A22.4'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A24/A24.2']= value['GCS/SNS/A24/A24.2'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/SNS/A24/A24.3']= value['GCS/SNS/A24/A24.3'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/CAS/A42/A42.1']= value['GCS/CAS/A42/A42.1'];	
	//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/PersonnePhysiqueDirigeante[' + idx + ']/GCS/CAS/A44']= value['GCS/CAS/A44'];	
});
}

// Fin 31P	

	// Groupe ICE : Identification complète de l'établissement 

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or objet.domicileEntreprise
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).eq('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).eq('77P')) { 

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and (Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementOuverture')
	or Value('id').of(etablissementNew.origineEtablissement2).eq('etablissementOuverture')))
	or objet.domicileEntreprise
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E01']= "1";
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E01']= "3";
}
	
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') 
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise
	or ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P'))  
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	and Value('id').of(objet.cadreObjetPersonneLiee.typeEtablissementLiee).contains('etablissementSecondaire'))) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.3']= etablissementNew.adresseEtablissementNew.etablissementAdresseCommuneNew.getId();
} else if (objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.3']= newAdresseDom.personneLieeAdresseCommuneNew.getId();
} else if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P'))  
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or (Value('id').of(objet.objetModification).contains('dateActivite') and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 != null or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 != null))
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P') and not objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise)
	or ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	and not Value('id').of(objet.cadreObjetPersonneLiee.typeEtablissementLiee).contains('etablissementSecondaire')))	{
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.3']= adresseEntreprise.etablissementAdresseCommune.getId();
}

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') 
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') 
	or ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P'))  
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	and Value('id').of(objet.cadreObjetPersonneLiee.typeEtablissementLiee).contains('etablissementSecondaire')))
	and etablissementNew.adresseEtablissementNew.etablissementAdresseNumeroVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5']= etablissementNew.adresseEtablissementNew.etablissementAdresseNumeroVoieNew;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5']= newAdresseDom.personneLieeAdresseVoieNew;
} else if ((((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P'))  
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or (Value('id').of(objet.objetModification).contains('dateActivite') and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 != null or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 != null))
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P') and not objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise)
	or ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	and not Value('id').of(objet.cadreObjetPersonneLiee.typeEtablissementLiee).contains('etablissementSecondaire')))
	and adresseEntreprise.etablissementAdresseNumeroVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5']= adresseEntreprise.etablissementAdresseNumeroVoie;
}

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') 
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise
	or ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P'))  
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	and Value('id').of(objet.cadreObjetPersonneLiee.typeEtablissementLiee).contains('etablissementSecondaire')))
	and etablissementNew.adresseEtablissementNew.etablissementAdresseIndiceVoieNew != null) {
   var monId1 = Value('id').of(etablissementNew.adresseEtablissementNew.etablissementAdresseIndiceVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.6']          = monId1;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseIndiceVoieNew != null) {
   var monId2 = Value('id').of(newAdresseDom.personneLieeAdresseIndiceVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.6']          = monId2;
} else if ((((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P'))  
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or (Value('id').of(objet.objetModification).contains('dateActivite') and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 != null or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 != null))
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P') and not objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise)
	or ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	and not Value('id').of(objet.cadreObjetPersonneLiee.typeEtablissementLiee).contains('etablissementSecondaire')))
	and adresseEntreprise.etablissementAdresseIndiceVoie != null){
   var monId3 = Value('id').of(adresseEntreprise.etablissementAdresseIndiceVoie)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.6']		  = monId3;
}

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') 
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise
	or ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P'))  
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	and Value('id').of(objet.cadreObjetPersonneLiee.typeEtablissementLiee).contains('etablissementSecondaire')))  
	and etablissementNew.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.7']= etablissementNew.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.7']= newAdresseDom.personneLieeAdresseDistriutionSpecialeNew;
} else if ((((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P'))  
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or (Value('id').of(objet.objetModification).contains('dateActivite') and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 != null or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 != null))
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P') and not objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise)
	or ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	and not Value('id').of(objet.cadreObjetPersonneLiee.typeEtablissementLiee).contains('etablissementSecondaire')))
	and adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.7']= adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie;
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') 
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise
	or ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P'))  
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	and Value('id').of(objet.cadreObjetPersonneLiee.typeEtablissementLiee).contains('etablissementSecondaire'))) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.8']= etablissementNew.adresseEtablissementNew.etablissementAdresseCodePostalNew;
} else if (objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.8']= newAdresseDom.personneLieeAdresseCodePostalNew;
} else if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P'))  
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or (Value('id').of(objet.objetModification).contains('dateActivite') and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 != null or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 != null))
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P') and not objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise)
	or ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	and not Value('id').of(objet.cadreObjetPersonneLiee.typeEtablissementLiee).contains('etablissementSecondaire'))) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.8']= adresseEntreprise.etablissementAdresseCodePostal;
}

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') 
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise
	or ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P'))  
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	and Value('id').of(objet.cadreObjetPersonneLiee.typeEtablissementLiee).contains('etablissementSecondaire')))
	and etablissementNew.adresseEtablissementNew.etablissementAdresseComplementVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.10']= etablissementNew.adresseEtablissementNew.etablissementAdresseComplementVoieNew;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseComplementAdresseNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.10']= newAdresseDom.personneLieeAdresseComplementAdresseNew;
} else if ((((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P'))  
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or (Value('id').of(objet.objetModification).contains('dateActivite') and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 != null or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 != null))
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P') and not objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise)
	or ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	and not Value('id').of(objet.cadreObjetPersonneLiee.typeEtablissementLiee).contains('etablissementSecondaire')))
	and adresseEntreprise.etablissementAdresseComplementVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.10']= adresseEntreprise.etablissementAdresseComplementVoie;
}	

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') 
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise
	or ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P'))  
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	and Value('id').of(objet.cadreObjetPersonneLiee.typeEtablissementLiee).contains('etablissementSecondaire')))
	and etablissementNew.adresseEtablissementNew.etablissementAdresseTypeVoieNew != null) {
   var monId1 = Value('id').of(etablissementNew.adresseEtablissementNew.etablissementAdresseTypeVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.11']          = monId1;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseTypeVoieNew != null) {
   var monId2 = Value('id').of(newAdresseDom.personneLieeAdresseTypeVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.11']          = monId2;
} else if ((((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P'))  
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or (Value('id').of(objet.objetModification).contains('dateActivite') and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 != null or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 != null))
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P') and not objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise)
	or ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	and not Value('id').of(objet.cadreObjetPersonneLiee.typeEtablissementLiee).contains('etablissementSecondaire')))
	and adresseEntreprise.etablissementAdresseTypeVoie != null){
   var monId3 = Value('id').of(adresseEntreprise.etablissementAdresseTypeVoie)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.11']		  = monId3;
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') 
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise
	or ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P'))  
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	and Value('id').of(objet.cadreObjetPersonneLiee.typeEtablissementLiee).contains('etablissementSecondaire'))) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.12']= etablissementNew.adresseEtablissementNew.etablissementAdresseNomVoieNew;
} else if (objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.12']= newAdresseDom.personneLieeAdresseNomVoieNew;
} else if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P'))  
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or (Value('id').of(objet.objetModification).contains('dateActivite') and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 != null or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 != null))
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P') and not objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise)
	or ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	and not Value('id').of(objet.cadreObjetPersonneLiee.typeEtablissementLiee).contains('etablissementSecondaire'))) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.12']= adresseEntreprise.etablissementAdresseNomVoie;
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') 
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise
	or ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P'))  
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	and Value('id').of(objet.cadreObjetPersonneLiee.typeEtablissementLiee).contains('etablissementSecondaire'))) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.13']= etablissementNew.adresseEtablissementNew.etablissementAdresseCommuneNew.getLabel();
} else if (objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.13']= newAdresseDom.personneLieeAdresseCommuneNew.getLabel();
} else if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P'))  
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or (Value('id').of(objet.objetModification).contains('dateActivite') and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 != null or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 != null))
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P') and not objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise)
	or ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	and not Value('id').of(objet.cadreObjetPersonneLiee.typeEtablissementLiee).contains('etablissementSecondaire'))) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.13']= adresseEntreprise.etablissementAdresseCommune.getLabel();
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E03/E03.1']= "000000000";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E03/E03.2']= "00000";

if (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	or ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P'))
	and Value('id').of(objet.cadreObjetPersonneLiee.typeEtablissementLiee).contains('etablissementSecondaire'))) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E04']= "4";
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E04']= "3";
}

if (activiteInfo.cadreEtablissementIdentification.modifEnseigneNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E05']= activiteInfo.cadreEtablissementIdentification.modifEnseigneNew;
} else if (activite.modifEnseigneBis != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E05']= activite.modifEnseigneBis;
}

if (activiteInfo.cadreEtablissementIdentification.modifNomCommercialNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E09']= activiteInfo.cadreEtablissementIdentification.modifNomCommercialNew;
} else if (activite.modifNomCommercialBis != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E09']= activite.modifNomCommercialBis;
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P')
	or Value('id').of(etablissementNew.origineEtablissement2).eq('etablissementOuverture')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E08']= etablissementNew.ajoutFondePouvoir ? "O" : "N";
}
}

// fin Regent 21P

				// Groupe IAE : Identification ancienne de l'établissement hors 40P / 42P / 43P

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and ((Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') 
	and not Value('id').of(etablissement.destinationEtablissement1).eq('devientSecondaire'))
	or (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire') 
	and not Value('id').of(etablissement.destinationEtablissement2).eq('devientSecondaire'))))
	or objet.domicileEntreprise
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('miseEnLocation')) {

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P'))	{
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.3']= etablissement.adresseEtablissementOld.etablissementAdresseCommuneOld.getId();
} else if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or objet.domicileEntreprise
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('miseEnLocation')
	and not Value('id').of(fondsDonne.fondsDonneEtablissement).eq('fondsDonneSecondaire'))) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.3']= adresseEntreprise.etablissementAdresseCommune.getId();
} else if (Value('id').of(fondsDonne.fondsDonneEtablissement).eq('fondsDonneSecondaire')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.3']= fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseCommune.getId();
}

if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P'))
	and etablissement.adresseEtablissementOld.etablissementAdresseNumeroVoieOld != null)	{
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.5']= etablissement.adresseEtablissementOld.etablissementAdresseNumeroVoieOld;
} else if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or objet.domicileEntreprise
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('miseEnLocation')
	and not Value('id').of(fondsDonne.fondsDonneEtablissement).eq('fondsDonneSecondaire')))
	and adresseEntreprise.etablissementAdresseNumeroVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.5']= adresseEntreprise.etablissementAdresseNumeroVoie;
} else if (Value('id').of(fondsDonne.fondsDonneEtablissement).eq('fondsDonneSecondaire')
		and fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.5']= fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseNumeroVoie;
}

if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P'))
	and etablissement.adresseEtablissementOld.etablissementAdresseIndiceVoieOld != null)	{
   var monId1 = Value('id').of(etablissement.adresseEtablissementOld.etablissementAdresseIndiceVoieOld)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.6']		  = monId1;
} else if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or objet.domicileEntreprise
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('miseEnLocation')
	and not Value('id').of(fondsDonne.fondsDonneEtablissement).eq('fondsDonneSecondaire')))
	and adresseEntreprise.etablissementAdresseIndiceVoie != null){
   var monId2 = Value('id').of(adresseEntreprise.etablissementAdresseIndiceVoie)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.6']		  = monId2;
} else if (Value('id').of(fondsDonne.fondsDonneEtablissement).eq('fondsDonneSecondaire')
		and fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseIndiceVoie != null) {
	var monId3 = Value('id').of(fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseIndiceVoie)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.6']		  = monId3;
	}

if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P'))
	and etablissement.adresseEtablissementOld.etablissementAdresseDistriutionSpecialeVoieOld != null)	{
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.7']= etablissement.adresseEtablissementOld.etablissementAdresseDistriutionSpecialeVoieOld;
} else if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or objet.domicileEntreprise
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('miseEnLocation')
	and not Value('id').of(fondsDonne.fondsDonneEtablissement).eq('fondsDonneSecondaire')))
	and adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.7']= adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie;
} else if (Value('id').of(fondsDonne.fondsDonneEtablissement).eq('fondsDonneSecondaire')
	and fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.7']= fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseDistriutionSpecialeVoie;
}

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P'))	{
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.8']= etablissement.adresseEtablissementOld.etablissementAdresseCodePostalOld;
} else if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or objet.domicileEntreprise
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('miseEnLocation')
	and not Value('id').of(fondsDonne.fondsDonneEtablissement).eq('fondsDonneSecondaire'))) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.8']= adresseEntreprise.etablissementAdresseCodePostal;
} else if (Value('id').of(fondsDonne.fondsDonneEtablissement).eq('fondsDonneSecondaire')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.8']= fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseCodePostal.getId();
}

if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P'))
	and etablissement.adresseEtablissementOld.etablissementAdresseComplementVoieOld != null)	{
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.10']= etablissement.adresseEtablissementOld.etablissementAdresseComplementVoieOld;
} else if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or objet.domicileEntreprise
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('miseEnLocation')
	and not Value('id').of(fondsDonne.fondsDonneEtablissement).eq('fondsDonneSecondaire')))
	and adresseEntreprise.etablissementAdresseComplementVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.10']= adresseEntreprise.etablissementAdresseComplementVoie;
}  else if (Value('id').of(fondsDonne.fondsDonneEtablissement).eq('fondsDonneSecondaire')
	and fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseComplementVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.10']= fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseComplementVoie;
}

if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P'))
	and etablissement.adresseEtablissementOld.etablissementAdresseTypeVoieOld != null)	{
   var monId1 = Value('id').of(etablissement.adresseEtablissementOld.etablissementAdresseTypeVoieOld)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.11']		  = monId1;
} else if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or objet.domicileEntreprise
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('miseEnLocation')
	and not Value('id').of(fondsDonne.fondsDonneEtablissement).eq('fondsDonneSecondaire')))
	and adresseEntreprise.etablissementAdresseTypeVoie != null){
   var monId2 = Value('id').of(adresseEntreprise.etablissementAdresseTypeVoie)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.11']		  = monId2;
}  else if (Value('id').of(fondsDonne.fondsDonneEtablissement).eq('fondsDonneSecondaire')
		and fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseTypeVoie != null) {
	var monId3 = Value('id').of(fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseTypeVoie)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.11']		  = monId3;}

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P'))	{
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.12']= etablissement.adresseEtablissementOld.etablissementAdresseNomVoieOld;
} else if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or objet.domicileEntreprise
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('miseEnLocation')
	and not Value('id').of(fondsDonne.fondsDonneEtablissement).eq('fondsDonneSecondaire'))) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.12']= adresseEntreprise.etablissementAdresseNomVoie;
} else if (Value('id').of(fondsDonne.fondsDonneEtablissement).eq('fondsDonneSecondaire')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.12']= fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseNomVoie;
}

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P'))	{
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.13']= etablissement.adresseEtablissementOld.etablissementAdresseCommuneOld.getLabel();
} else if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or objet.domicileEntreprise
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('miseEnLocation')
	and not Value('id').of(fondsDonne.fondsDonneEtablissement).eq('fondsDonneSecondaire'))) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.13']= adresseEntreprise.etablissementAdresseCommune.getLabel();
} else if (Value('id').of(fondsDonne.fondsDonneEtablissement).eq('fondsDonneSecondaire')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.13']= fondsDonne.adresseFondsDonneLocationGerance.fondsDonneAdresseCommune.getLabel();
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E13/E13.1']= "000000000";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E13/E13.2']= "00000";

if(Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P') and etablissement.dateCessationEmploi != null) {
var dateTemp = new Date(parseInt(etablissement.dateCessationEmploi.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E14']= date;
}

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P'))	{
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E17']= "4";
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E17']= "3";
}
}			   

// Fin 60P

			   // Groupe ORE : Origine de l'établissement

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and not Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementExistant'))
	or objet.domicileEntreprise 
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise
	or Value('id').of(activite.activiteEvenement).eq('61P') 
	or Value('id').of(activite.activiteEvenement).eq('61P62P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('63P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P')
	or objet.cadreObjetModificationEtablissement.cadreObjet68P.renouvellementChangement) {
if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation') 
	or objet.domicileEntreprise 
	or (objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise
	and Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementOuverture'))) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.1']=  "1";
} else if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCocheAutre')
	or Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsDonation')
	or Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsDevolution')
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementReprise'))
	or (objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise
	and Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementReprise'))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P')
	or Value('id').of(etablissementNew.origineAcquision).eq('donation')
	or Value('id').of(etablissementNew.origineAcquision).eq('devolution')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.1']=  "9";
}  else if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsAchat')
	or Value('id').of(etablissementNew.origineAcquision).eq('achat')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.1']=  "3";
} else if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsLocationGerance') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P')
	or objet.cadreObjetModificationEtablissement.cadreObjet68P.renouvellementChangement
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementRenouvellement'))
	or (objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise
	and Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementRenouvellement'))
	and Value('id').of(origine.geranceMandat.typeFonds).eq('typeFondsLocation'))) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.1']=  "6";
} else if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsGeranceMandat') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P')
	or objet.cadreObjetModificationEtablissement.cadreObjet68P.renouvellementChangement
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementRenouvellement'))
	or (objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise
	and Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementRenouvellement'))
	and Value('id').of(origine.geranceMandat.typeFonds).eq('typeFondsGerance'))) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.1']=  "F";
}

if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCocheAutre')
	or Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsDonation')
	or Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsDevolution')
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementReprise'))
	or (objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise
	and Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementReprise'))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P')
	or Value('id').of(etablissementNew.origineAcquision).eq('donation')
	or Value('id').of(etablissementNew.origineAcquision).eq('devolution')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.2']=  (Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementReprise')
																							  or Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementReprise')
																							  or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P')) ? "Reprise de l'exploitation du fonds" : 
																							  ((Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsDonation')
																							  or Value('id').of(etablissementNew.origineAcquision).eq('donation')) ? "Donation" : 
																							  ((Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsDevolution')
																							  or Value('id').of(etablissementNew.origineAcquision).eq('devolution')) ? "Dévolution successorale" : sanitize(origine.etablissementOrigineFondsAutre)));
}

if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsAchat')
	or Value('id').of(etablissementNew.origineAcquision).eq('achat')) {
if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsAchat') and origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParution != null) {		
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E22/E22.1']= origine.precedentExploitant.etablissementJournalAnnoncesLegalesNom;
if(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParution != null) {
var dateTemp = new Date(parseInt(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParution.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E22/E22.2']= date;
}
} else if (Value('id').of(etablissementNew.origineAcquision).eq('achat') and etablissementNew.acquisitionJournalAnnoncesLegalesDateParution != null) {		
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E22/E22.1']= etablissementNew.acquisitionJournalAnnoncesLegalesNom;
if(etablissementNew.acquisitionJournalAnnoncesLegalesDateParution != null) {
var dateTemp = new Date(parseInt(etablissementNew.acquisitionJournalAnnoncesLegalesDateParution.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E22/E22.2']= date;
}
}
}
}				

// Fin 63P
				
				// Groupe PEE : Précédent exploitant
				
if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsAchat') 
	or Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementReprise')
	or Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementReprise')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P')) {				
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.1']= origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant.split(' ').join('');

if (Value('id').of(origine.precedentExploitant.typePersonnePrecedentExploitant).eq('typePersonnePrecedentExploitantPP')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.2']= origine.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant;
if (origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.3']= origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.4']= origine.precedentExploitant.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant;
}

if (Value('id').of(origine.precedentExploitant.typePersonnePrecedentExploitant).eq('typePersonnePrecedentExploitantPM')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.7']= origine.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant;
}	
}

	
				// Groupe DEE : Destination de l'établissement

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or objet.domicileEntreprise 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('miseEnLocation')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P')
	or objet.cadreObjetModificationEtablissement.cadreObjet64P.changementRenouvellement
	or Value('id').of(activite.activiteEvenement).eq('62P') 
	or Value('id').of(activite.activiteEvenement).eq('61P62P')) {
		
if (Value('id').of(etablissement.destinationEtablissement1).eq('vendu')
	or Value('id').of(etablissement.destinationEtablissement2).eq('vendu')
	or Value('id').of(etablissement.destinationEtablissement3).eq('vendu')
	or Value('id').of(activite.suppressionActivitePar).eq('suppressionParVente')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.1']= "3";
} else if (Value('id').of(etablissement.destinationEtablissement2).eq('devientPrincipal')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.1']= "8";
} else if (Value('id').of(etablissement.destinationEtablissement1).eq('autre')
	or Value('id').of(etablissement.destinationEtablissement2).eq('autre')
	or Value('id').of(etablissement.destinationEtablissement3).eq('autre')
	or Value('id').of(activite.suppressionActivitePar).eq('suppressionParAutre')
	or Value('id').of(activite.suppressionActivitePar).eq('suppressionParReprise')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.1']= "9";
} else if (Value('id').of(etablissement.destinationEtablissement1).eq('devientSecondaire')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.1']= "A";
} else if (Value('id').of(etablissement.destinationEtablissement1).eq('ferme')
	or Value('id').of(etablissement.destinationEtablissement2).eq('ferme')
	or objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.1']= "B";
} else if (Value('id').of(etablissement.destinationEtablissement3).eq('supprime')
	or Value('id').of(activite.suppressionActivitePar).eq('suppressionParDisparition')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.1']= "C";
} else if (Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationPartielle')
	or Value('id').of(etablissement.destinationEtablissement2).eq('miseEnLocationPartielle')
	or Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetmiseEnLocation.infosMiseEnLocation).eq('82P')
	or Value('id').of(fondsDonne.infosChangement).eq('82P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.1']= "E";
} else if (Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationTotale')
	or Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationCessation')
	or Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetmiseEnLocation.infosMiseEnLocation).eq('84P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetmiseEnLocation.infosMiseEnLocation).eq('83P')
	or Value('id').of(fondsDonne.infosChangement).eq('84P')
	or Value('id').of(fondsDonne.infosChangement).eq('83P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.1']= "D";
} 

if (Value('id').of(etablissement.destinationEtablissement1).eq('autre')
	or Value('id').of(etablissement.destinationEtablissement2).eq('autre')
	or Value('id').of(etablissement.destinationEtablissement3).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.2']= etablissement.destinationAutre;
} else if (Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationPartielle')
	or Value('id').of(etablissement.destinationEtablissement2).eq('miseEnLocationPartielle')
	or Value('id').of(fondsDonne.infosChangement).eq('82P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.cadreObjetmiseEnLocation.infosMiseEnLocation).eq('82P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.2']= fondsDonne.fondsDonnePartie;
} else if (Value('id').of(activite.suppressionActivitePar).eq('suppressionParAutre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.2']= activite.suppressionActiviteAutre;
} else if (Value('id').of(activite.suppressionActivitePar).eq('suppressionParReprise')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.2']= "Reprise par le propriétaire";
}
}

// Fin Regent pour le 80P //
// Fin Regent pour le 11P //
	
		// Groupe REE : Repreneur de l'établissement

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('miseEnLocation')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('68P')
	or objet.cadreObjetModificationEtablissement.cadreObjet64P.changementRenouvellement
	or Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationTotale')
	or Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationCessation')
	or Value('id').of(etablissement.destinationEtablissement1).eq('miseEnLocationPartielle')
	or Value('id').of(etablissement.destinationEtablissement2).eq('miseEnLocationPartielle')) {

if (Value('id').of(fondsDonne.fondsDonneInformation.typePersonneLocataireMandataire).eq('typePersonneLoueurMandantPP')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/REE/E51']= fondsDonne.fondsDonneInformation.fondsDonneLocataireNomNaissance + ' ' + fondsDonne.fondsDonneInformation.fondsDonneLocatairePrenom;		
}	

if (Value('id').of(fondsDonne.fondsDonneInformation.typePersonneLocataireMandataire).eq('typePersonneLoueurMandantPM')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/REE/E52']= fondsDonne.fondsDonneInformation.fondsDonneLocataireDenomination;		
}	
}

// Fin 68P
// Fin 82P
// Fin 83P
// Fin 84P	

				// Groupe LGE : Location-gérance ou gérance-mandat

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('64P')
	or objet.cadreObjetModificationEtablissement.cadreObjet68P.renouvellementChangement
	or Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementRenouvellement') 
	or Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementRenouvellement')
	or Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsLocationGerance')
	or Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsGeranceMandat')) {

if(origine.geranceMandat.etablissementLoueurMandantDuFondsDebutContrat != null) {
var dateTemp = new Date(parseInt(origine.geranceMandat.etablissementLoueurMandantDuFondsDebutContrat.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E61/E61.1']= date;
}

if(origine.geranceMandat.etablissementLoueurMandantDuFondsFinContrat != null) {
var dateTemp = new Date(parseInt(origine.geranceMandat.etablissementLoueurMandantDuFondsFinContrat.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E61/E61.2']= date;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E61/E61.3']= origine.geranceMandat.etablissementLoueurMandantDuFondsRenouvellementTaciteReconductionOui ? "O" : "N";

if (Value('id').of(origine.geranceMandat.typePersonneLoueurMandant).eq('typePersonneLoueurMandantPP')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E62']= origine.geranceMandat.entrepriseLieeEntreprisePMNomNaissanceLoueurMandantDuFonds;		
if (origine.geranceMandat.entrepriseLieeEntreprisePPNomUsageLoueurMandantDuFonds != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E64']= origine.geranceMandat.entrepriseLieeEntreprisePPNomUsageLoueurMandantDuFonds;		
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E65']= origine.geranceMandat.entrepriseLieeEntreprisePPPrenom1LoueurMandantDuFonds;		
}
if (Value('id').of(origine.geranceMandat.typePersonneLoueurMandant).eq('typePersonneLoueurMandantPM')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E66']= origine.geranceMandat.entrepriseLieeEntreprisePMDenominationLoueurMandantDuFonds;		
}
if (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.communeLoueurMandantDuFonds != null) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.3'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.communeLoueurMandantDuFonds.getId();
}
if (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.numRueAdresseLoueurMandantDufonds != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.5'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.numRueAdresseLoueurMandantDufonds; 
}
if (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.indiceVoieLoueurMandantDuFonds != null) {																							
	var monId1 = Value('id').of(origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.indiceVoieLoueurMandantDuFonds)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.6']=monId1;
}
if (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.distributionSpecialeLoueurMandantDuFonds != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.7'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.distributionSpecialeLoueurMandantDuFonds;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.8'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.codePostalLoueurMandantDuFonds;
if (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.rueComplementAdresseLoueurMandantDuFonds != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.10'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.rueComplementAdresseLoueurMandantDuFonds;
}
if (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.typeVoieLoueurMandantDuFonds != null) {			
	var typeVoie = Value('id').of(origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.typeVoieLoueurMandantDuFonds)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.11']=typeVoie;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.12'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.nomVoieLoueurMandantDuFonds;
if (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.communeLoueurMandantDuFonds != null) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E63/E63.13'] = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.communeLoueurMandantDuFonds.getLabel();
}


if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsGeranceMandat')){
	if (origine.geranceMandat.entrepriseLieeSirenGeranceMandat != null){
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E67']=origine.geranceMandat.entrepriseLieeSirenGeranceMandat.split(' ').join('');
	}
	//Récupérer le EntityId du greffe
	var greffeId = origine.geranceMandat.entrepriseLieeGreffeImmatriculation.id ;
	//Appeler Directory pour récupérer l'autorité
	_log.info('l entityId du greffe selectionne est {}', greffeId);
	var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', greffeId) //
	           .connectionTimeout(10000) //
	           .receiveTimeout(10000) //
	           .accept('json') //
	           .get();
	//result
	var receiverInfo = response.asObject();
	var contactInfo = !receiverInfo.details ? null : receiverInfo.details;
	//Récupérer le code EDI
	var codeEDIGreffe = !contactInfo.ediCode ? null : contactInfo.ediCode;
	_log.info('le code edi du greffe selectionne est {}', codeEDIGreffe);
	// À modifier quand Destiny fourni le réferentiel
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/LGE/E68']=codeEDIGreffe;
}
}	

// Fin pour le 64P

		// Groupe ACE : Activité de l'établissement

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and (Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementOuverture')
	or Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementReprise'))
	or Value('id').of(etablissementNew.origineEtablissement2).eq('etablissementOuverture'))
	or objet.domicileEntreprise
	or Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementOuverture')
	or Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementReprise')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.activiteReprise
	or objet.cadreObjetModificationEtablissement.cadreObjet63P.activiteAcquisition
	or objet.cadreObjetModificationEtablissement.cadreObjet64P.activiteRenouvellement
	or activiteInfo.modifActiviteTransfert
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P')) {		
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E70']= sanitize(activite.etablissementActivitesAutres) ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E71']= activite.etablissementActivitesPrincipales2 != null ? sanitize(activite.etablissementActivitesPrincipales2) : sanitize(activite.etablissementActivitesAutres.substring(0, 140)) ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E78']= activite.activiteDevientPrincipale ? "O" : "N" ;

if (Value('id').of(activite.activiteEvenement).eq('61P') or activiteInfo.modifActiviteReprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E79/E79.1']= "A";
} else if (Value('id').of(activite.activiteEvenement).eq('62P') and Value('id').of(activite.suppressionActivitePar).eq('suppressionParDisparition')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E79/E79.1']= "D";
} else if (Value('id').of(activite.activiteEvenement).eq('62P') and Value('id').of(activite.suppressionActivitePar).eq('suppressionParVente')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E79/E79.1']= "V";
} else if (Value('id').of(activite.activiteEvenement).eq('62P') and Value('id').of(activite.suppressionActivitePar).eq('suppressionParReprise')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E79/E79.1']= "R";
} else if (Value('id').of(activite.activiteEvenement).eq('62P') and Value('id').of(activite.suppressionActivitePar).eq('suppressionParAutre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E79/E79.1']= "U";
} else if (Value('id').of(activite.activiteEvenement).eq('61P62P') and Value('id').of(activite.suppressionActivitePar).eq('suppressionParDisparition')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E79/E79.1']= "E";
} else if (Value('id').of(activite.activiteEvenement).eq('61P62P') and Value('id').of(activite.suppressionActivitePar).eq('suppressionParVente')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E79/E79.1']= "W";
} else if (Value('id').of(activite.activiteEvenement).eq('61P62P') and Value('id').of(activite.suppressionActivitePar).eq('suppressionParReprise')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E79/E79.1']= "T";
} else if (Value('id').of(activite.activiteEvenement).eq('61P62P') and Value('id').of(activite.suppressionActivitePar).eq('suppressionParAutre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E79/E79.1']= "X";
}
if (Value('id').of(activite.suppressionActivitePar).eq('suppressionParAutre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E79/E79.2']= activite.suppressionActiviteAutre;
}	
}

		// Groupe CAE

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and (Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementOuverture')
	or Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementReprise'))
	or Value('id').of(etablissementNew.origineEtablissement2).eq('etablissementOuverture'))
	or objet.domicileEntreprise
	or Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementOuverture')
	or Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementReprise')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.activiteReprise
	or objet.cadreObjetModificationEtablissement.cadreObjet63P.activiteAcquisition
	or objet.cadreObjetModificationEtablissement.cadreObjet64P.activiteRenouvellement
	or activiteInfo.modifActiviteTransfert
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E73/E73.1']= Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonnierePermanente') ? 'P' : 'S';
if (activite.etablissementNonSedentariteQualiteNonSedentaire) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E73/E73.2']='A';
}

if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMagasin') or Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMarche') or Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceDetail')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.1']='10';
} else if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceGros')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.1']='09';
} else if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureFabricationProduction')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.1']='04';
} else if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureBatTravauxPublics')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.1']='14';
} else if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCocheAutre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.1']='99';
}

if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCocheAutre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E75/E75.2']= sanitize(activite.etablissementActiviteNatureAutre).substring(0, 50);
}

if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMagasin')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E77/E77.1']= "05";
} else if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMarche')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E77/E77.1']= "92";
} else if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceDetail')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E77/E77.1']= "99";
} 

if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceDetail')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E77/E77.2']= "Sur internet";
}
if (Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMagasin')) {
function lpad(str, padString, length) {
  var ch = str;
  while (ch.length < length) {
      ch = padString + ch;
  }
  log.info("lpad : "+ch);
  return ch;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E77/E77.3']= lpad(activite.etablissementActiviteLieuExerciceMagasinSurface.toString(), "0", 5);
}
}
	

			// Groupe SAE : salariés de l'établissement

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and (Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementOuverture')
	or Value('id').of(etablissementNew.origineEtablissement1).eq('etablissementReprise'))
	or Value('id').of(etablissementNew.origineEtablissement2).eq('etablissementOuverture'))
	or objet.domicileEntreprise
	or Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementOuverture')
	or Value('id').of(etablissementNew.origineEtablissement3).eq('etablissementReprise')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.activiteReprise
	or objet.cadreObjetModificationEtablissement.cadreObjet63P.activiteAcquisition
	or objet.cadreObjetModificationEtablissement.cadreObjet64P.activiteRenouvellement
	or activiteInfo.modifActiviteTransfert
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P')) {		
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E84']= activite.etablissementEffectifSalariePresenceOui ? activite.cadreEffectifSalarie.etablissementEffectifSalarieNombre : "0";
if (activite.cadreEffectifSalarie.etablissementEffectifSalarieApprentis != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E85/E85.8']= activite.cadreEffectifSalarie.etablissementEffectifSalarieApprentis;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E86']= activite.etablissementEffectifSalariePresenceOui ? "O" : "N";
}

// Fin du 61P
// Fin du 62P
// Fin du 67P

			// Groupe IDE - Identification de la personne liée à l'exploitation

var occurrencesPersonneLiee = [];
var occurrence = {};
			
if (objet.cadreObjet22P.declarationIndivisaire
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir) {
		
// 1ère personne liée
if (not personneLiee1.indivisaireHeritier) {		
if (objet.cadreObjet22P.declarationIndivisaire
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee1.objetModifPersonneLiee).eq('nouveauPersonneLiee'))		{
occurrence['E91/E91.1'] = "1";
}	else if (Value('id').of(personneLiee1.objetModifPersonneLiee).eq('partantPersonneLiee')) {
occurrence['E91/E91.1'] = "2";
}	else if (Value('id').of(personneLiee1.objetModifPersonneLiee).eq('modifPersonneLiee')) {
occurrence['E91/E91.1'] = "3";
}
if (objet.cadreObjet22P.declarationIndivisaire and objet.cadreObjet22P.modifDateDeces != null) {
var dateTemp = new Date(parseInt(objet.cadreObjet22P.modifDateDeces.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
} else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') and etablissementNew.modifDateAdresseOuverture != null) {
var dateTemp = new Date(parseInt(etablissementNew.modifDateAdresseOuverture.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
} else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') and etablissementNew.modifDateAdresseReprise != null) {
var dateTemp = new Date(parseInt(etablissementNew.modifDateAdresseReprise.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
} else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissement.modifDateAdresseEntreprise != null) {
var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
} else if (etablissementNew.ajoutFondePouvoir and objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise and objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise != null) {
var dateTemp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
} else if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P') or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')) and personneLiee.modifDatePersonneLiee != null) {
var dateTemp = new Date(parseInt(personneLiee.modifDatePersonneLiee.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir and modifIdentiteDeclarant.modifDomicile.modifDateDomicile != null) {
var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}
if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee1.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement')) {
occurrence['E92/E92.1']= "P";
} else if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not personneLiee1.indivisaireHeritier)
	or Value('id').of(personneLiee1.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualiteProprietaireIndivis')
	or objet.cadreObjet22P.declarationIndivisaire) {
occurrence['E92/E92.1']= "N";
}	
occurrence['E93/E93.2']= personneLiee1.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
if (personneLiee1.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null) {
occurrence['E93/E93.4']= personneLiee1.personneLieePersonnePhysiqueNomUsagePersonnePouvoir;
}
var prenoms=[];
for ( i = 0; i < personneLiee1.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee1.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}      
occurrence['E93/E93.3']= prenoms;
if (not Value('id').of(personneLiee1.objetModifPersonneLiee).eq('partantPersonneLiee')) {
occurrence['E94/E94.3']= personneLiee1.cadreAdressePersonnePouvoir.personneLieeAdresseCommunepersonnePouvoir.getId();
if (personneLiee1.cadreAdressePersonnePouvoir.rueNumeroAdressePersonnePouvoir != null) {
occurrence['E94/E94.5'] = personneLiee1.cadreAdressePersonnePouvoir.rueNumeroAdressePersonnePouvoir;
}
if (personneLiee1.cadreAdressePersonnePouvoir.indiceVoiePersonnePouvoir != null) {																							
	var monId1 = Value('id').of(personneLiee1.cadreAdressePersonnePouvoir.indiceVoiePersonnePouvoir)._eval();
occurrence['E94/E94.6']= monId1;
}
if (personneLiee1.cadreAdressePersonnePouvoir.distriutionSpecialePersonnePouvoir != null) {
occurrence['E94/E94.7'] = personneLiee1.cadreAdressePersonnePouvoir.distriutionSpecialePersonnePouvoir;
}
occurrence['E94/E94.8'] = personneLiee1.cadreAdressePersonnePouvoir.personneLieeAdresseCodePostalPersonnePouvoir;
if (personneLiee1.cadreAdressePersonnePouvoir.rueComplementAdressePersonnePouvoir != null) {
occurrence['E94/E94.10'] = personneLiee1.cadreAdressePersonnePouvoir.rueComplementAdressePersonnePouvoir;
}
if (personneLiee1.cadreAdressePersonnePouvoir.typeVoiePersonnePouvoir != null) {																							
	var monId1 = Value('id').of(personneLiee1.cadreAdressePersonnePouvoir.typeVoiePersonnePouvoir)._eval();
occurrence['E94/E94.11']= monId1;
}
occurrence['E94/E94.12'] = personneLiee1.cadreAdressePersonnePouvoir.nomVoiePersonnePouvoir;
occurrence['E94/E94.13']= personneLiee1.cadreAdressePersonnePouvoir.personneLieeAdresseCommunepersonnePouvoir.getLabel();
}
if ((((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers'))
	or Value('id').of(personneLiee1.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and Value('id').of(personneLiee1.objetModifPersonneLiee).eq('nouveauPersonneLiee')) 
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir) {
if (personneLiee1.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir != null) {
var dateTemp = new Date(parseInt(personneLiee1.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E95/E95.1']= date;
}	
if (not Value('id').of(personneLiee1.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
	var idPays = personneLiee1.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['E95/E95.2']= result;
} else if (Value('id').of(personneLiee1.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
occurrence['E95/E95.2'] = personneLiee1.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getId();
}	
if (not Value('id').of(personneLiee1.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {																					 
occurrence['E95/E95.3']= personneLiee1.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getLabel();
}
if (Value('id').of(personneLiee1.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {	
occurrence['E95/E95.4']= personneLiee1.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getLabel();
}	
occurrence['E96']= personneLiee1.personneLieePersonnePhysiqueNationalitePersonnePouvoir;
}
occurrencesPersonneLiee.push(occurrence);
occurrence = {}; 
}

// 2ème personne liée
if ((personneLiee1.autreDeclarationPersonneLiee
	or (Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and (Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers')))
	or (Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and (Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	or Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers'))))
	and not personneLiee2.indivisaireHeritier) {
if (objet.cadreObjet22P.declarationIndivisaire
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or 	Value('id').of(personneLiee2.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
occurrence['E91/E91.1'] = "1";
}	else if (Value('id').of(personneLiee2.objetModifPersonneLiee).eq('partantPersonneLiee')) {
occurrence['E91/E91.1'] = "2";
}	else if (Value('id').of(personneLiee2.objetModifPersonneLiee).eq('modifPersonneLiee')) {
occurrence['E91/E91.1'] = "3";
}
if (objet.cadreObjet22P.declarationIndivisaire and objet.cadreObjet22P.modifDateDeces != null) {
var dateTemp = new Date(parseInt(objet.cadreObjet22P.modifDateDeces.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') and etablissementNew.modifDateAdresseOuverture != null) {
var dateTemp = new Date(parseInt(etablissementNew.modifDateAdresseOuverture.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') and etablissementNew.modifDateAdresseReprise != null) {
var dateTemp = new Date(parseInt(etablissementNew.modifDateAdresseReprise.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissement.modifDateAdresseEntreprise != null) {
var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise and objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise != null) {
var dateTemp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P') or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')) and personneLiee.modifDatePersonneLiee != null) {
var dateTemp = new Date(parseInt(personneLiee.modifDatePersonneLiee.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
} 	else if (modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir and modifIdentiteDeclarant.modifDomicile.modifDateDomicile != null) {
var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}
if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee2.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement')) {
occurrence['E92/E92.1']= "P";
} else if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not personneLiee2.indivisaireHeritier)
	or Value('id').of(personneLiee2.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualiteProprietaireIndivis')
	or objet.cadreObjet22P.declarationIndivisaire) {
occurrence['E92/E92.1']= "N";
}	
occurrence['E93/E93.2']= personneLiee2.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
if (personneLiee2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null) {
occurrence['E93/E93.4']= personneLiee2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir;
}
var prenoms=[];
for ( i = 0; i < personneLiee2.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee2.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}      
occurrence['E93/E93.3']= prenoms;
if (not Value('id').of(personneLiee2.objetModifPersonneLiee).eq('partantPersonneLiee')) {
occurrence['E94/E94.3']= personneLiee2.cadreAdressePersonnePouvoir2.personneLieeAdresseCommunepersonnePouvoir.getId();
if (personneLiee2.cadreAdressePersonnePouvoir2.rueNumeroAdressePersonnePouvoir != null) {
occurrence['E94/E94.5'] = personneLiee2.cadreAdressePersonnePouvoir2.rueNumeroAdressePersonnePouvoir;
}
if (personneLiee2.cadreAdressePersonnePouvoir2.indiceVoiePersonnePouvoir != null) {																							
	var monId1 = Value('id').of(personneLiee2.cadreAdressePersonnePouvoir2.indiceVoiePersonnePouvoir)._eval();
occurrence['E94/E94.6']= monId1;
}
if (personneLiee2.cadreAdressePersonnePouvoir2.distriutionSpecialePersonnePouvoir != null) {
occurrence['E94/E94.7'] = personneLiee2.cadreAdressePersonnePouvoir2.distriutionSpecialePersonnePouvoir;
}
occurrence['E94/E94.8'] = personneLiee2.cadreAdressePersonnePouvoir2.personneLieeAdresseCodePostalPersonnePouvoir;
if (personneLiee2.cadreAdressePersonnePouvoir2.rueComplementAdressePersonnePouvoir != null) {
occurrence['E94/E94.10'] = personneLiee2.cadreAdressePersonnePouvoir2.rueComplementAdressePersonnePouvoir;
}
if (personneLiee2.cadreAdressePersonnePouvoir2.typeVoiePersonnePouvoir != null) {																							
	var monId1 = Value('id').of(personneLiee2.cadreAdressePersonnePouvoir2.typeVoiePersonnePouvoir)._eval();
occurrence['E94/E94.11']= monId1;
}
occurrence['E94/E94.12'] = personneLiee2.cadreAdressePersonnePouvoir2.nomVoiePersonnePouvoir;
occurrence['E94/E94.13']= personneLiee2.cadreAdressePersonnePouvoir2.personneLieeAdresseCommunepersonnePouvoir.getLabel();
}
if ((((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers'))
	or Value('id').of(personneLiee2.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and Value('id').of(personneLiee2.objetModifPersonneLiee).eq('nouveauPersonneLiee')) 
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir) {
if (personneLiee2.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir != null) {
var dateTemp = new Date(parseInt(personneLiee2.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E95/E95.1']= date;
}	
if (not Value('id').of(personneLiee2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
	var idPays = personneLiee2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['E95/E95.2']= result;
} else if (Value('id').of(personneLiee2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
occurrence['E95/E95.2'] = personneLiee2.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getId();
}	
if (not Value('id').of(personneLiee2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {																					 
occurrence['E95/E95.3']= personneLiee2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getLabel();
}
if (Value('id').of(personneLiee2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {	
occurrence['E95/E95.4']= personneLiee2.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getLabel();
}	
occurrence['E96']= personneLiee2.personneLieePersonnePhysiqueNationalitePersonnePouvoir;
}
occurrencesPersonneLiee.push(occurrence);
occurrence = {}; 
}

// 3ème personne liée
if (personneLiee2.autreDeclarationPersonneLiee and not personneLiee3.indivisaireHeritier) {
if (objet.cadreObjet22P.declarationIndivisaire
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or 	Value('id').of(personneLiee3.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
occurrence['E91/E91.1'] = "1";
}	else if (Value('id').of(personneLiee3.objetModifPersonneLiee).eq('partantPersonneLiee')) {
occurrence['E91/E91.1'] = "2";
}	else if (Value('id').of(personneLiee3.objetModifPersonneLiee).eq('modifPersonneLiee')) {
occurrence['E91/E91.1'] = "3";
}
if (objet.cadreObjet22P.declarationIndivisaire and objet.cadreObjet22P.modifDateDeces != null) {
var dateTemp = new Date(parseInt(objet.cadreObjet22P.modifDateDeces.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') and etablissementNew.modifDateAdresseOuverture != null) {
var dateTemp = new Date(parseInt(etablissementNew.modifDateAdresseOuverture.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') and etablissementNew.modifDateAdresseReprise != null) {
var dateTemp = new Date(parseInt(etablissementNew.modifDateAdresseReprise.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissement.modifDateAdresseEntreprise != null) {
var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise and objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise != null) {
var dateTemp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P') or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')) and personneLiee.modifDatePersonneLiee != null) {
var dateTemp = new Date(parseInt(personneLiee.modifDatePersonneLiee.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
} 	else if (modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir and modifIdentiteDeclarant.modifDomicile.modifDateDomicile != null) {
var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	
if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee3.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement')) {
occurrence['E92/E92.1']= "P";
} else if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not personneLiee3.indivisaireHeritier)
	or Value('id').of(personneLiee3.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualiteProprietaireIndivis')
	or objet.cadreObjet22P.declarationIndivisaire) {
occurrence['E92/E92.1']= "N";
}	
occurrence['E93/E93.2']= personneLiee3.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
if (personneLiee3.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null) {
occurrence['E93/E93.4']= personneLiee3.personneLieePersonnePhysiqueNomUsagePersonnePouvoir;
}
var prenoms=[];
for ( i = 0; i < personneLiee3.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee3.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}      
occurrence['E93/E93.3']= prenoms;
if (not Value('id').of(personneLiee3.objetModifPersonneLiee).eq('partantPersonneLiee')) {
occurrence['E94/E94.3']= personneLiee3.cadreAdressePersonnePouvoir3.personneLieeAdresseCommunepersonnePouvoir.getId();
if (personneLiee3.cadreAdressePersonnePouvoir3.rueNumeroAdressePersonnePouvoir != null) {
occurrence['E94/E94.5'] = personneLiee3.cadreAdressePersonnePouvoir3.rueNumeroAdressePersonnePouvoir;
}
if (personneLiee3.cadreAdressePersonnePouvoir3.indiceVoiePersonnePouvoir != null) {																							
	var monId1 = Value('id').of(personneLiee3.cadreAdressePersonnePouvoir3.indiceVoiePersonnePouvoir)._eval();
occurrence['E94/E94.6']= monId1;
}
if (personneLiee3.cadreAdressePersonnePouvoir3.distriutionSpecialePersonnePouvoir != null) {
occurrence['E94/E94.7'] = personneLiee3.cadreAdressePersonnePouvoir3.distriutionSpecialePersonnePouvoir;
}
occurrence['E94/E94.8'] = personneLiee3.cadreAdressePersonnePouvoir3.personneLieeAdresseCodePostalPersonnePouvoir;
if (personneLiee3.cadreAdressePersonnePouvoir3.rueComplementAdressePersonnePouvoir != null) {
occurrence['E94/E94.10'] = personneLiee3.cadreAdressePersonnePouvoir3.rueComplementAdressePersonnePouvoir;
}
if (personneLiee3.cadreAdressePersonnePouvoir3.typeVoiePersonnePouvoir != null) {																							
	var monId1 = Value('id').of(personneLiee3.cadreAdressePersonnePouvoir3.typeVoiePersonnePouvoir)._eval();
occurrence['E94/E94.11']= monId1;
}
occurrence['E94/E94.12'] = personneLiee3.cadreAdressePersonnePouvoir3.nomVoiePersonnePouvoir;
occurrence['E94/E94.13']= personneLiee3.cadreAdressePersonnePouvoir3.personneLieeAdresseCommunepersonnePouvoir.getLabel();
}
if ((((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers'))
	or Value('id').of(personneLiee3.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and Value('id').of(personneLiee3.objetModifPersonneLiee).eq('nouveauPersonneLiee')) 
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir) {
if (personneLiee3.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir != null) {
var dateTemp = new Date(parseInt(personneLiee3.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E95/E95.1']= date;
}	
if (not Value('id').of(personneLiee3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
	var idPays = personneLiee3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['E95/E95.2']= result;
} else if (Value('id').of(personneLiee3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
occurrence['E95/E95.2'] = personneLiee3.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getId();
}	
if (not Value('id').of(personneLiee3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {																					 
occurrence['E95/E95.3']= personneLiee3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getLabel();
}
if (Value('id').of(personneLiee3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {	
occurrence['E95/E95.4']= personneLiee3.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getLabel();
}	
occurrence['E96']= personneLiee3.personneLieePersonnePhysiqueNationalitePersonnePouvoir;
}
occurrencesPersonneLiee.push(occurrence);
occurrence = {}; 
}

// 4ème personne liée
if (personneLiee3.autreDeclarationPersonneLiee and not personneLiee4.indivisaireHeritier) {
if (objet.cadreObjet22P.declarationIndivisaire
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or 	Value('id').of(personneLiee4.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
occurrence['E91/E91.1'] = "1";
}	else if (Value('id').of(personneLiee4.objetModifPersonneLiee).eq('partantPersonneLiee')) {
occurrence['E91/E91.1'] = "2";
}	else if (Value('id').of(personneLiee4.objetModifPersonneLiee).eq('modifPersonneLiee')) {
occurrence['E91/E91.1'] = "3";
}
if (objet.cadreObjet22P.declarationIndivisaire and objet.cadreObjet22P.modifDateDeces != null) {
var dateTemp = new Date(parseInt(objet.cadreObjet22P.modifDateDeces.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') and etablissementNew.modifDateAdresseOuverture != null) {
var dateTemp = new Date(parseInt(etablissementNew.modifDateAdresseOuverture.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') and etablissementNew.modifDateAdresseReprise != null) {
var dateTemp = new Date(parseInt(etablissementNew.modifDateAdresseReprise.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissement.modifDateAdresseEntreprise != null) {
var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise and objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise != null) {
var dateTemp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P') or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')) and personneLiee.modifDatePersonneLiee != null) {
var dateTemp = new Date(parseInt(personneLiee.modifDatePersonneLiee.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
} else if (modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir and modifIdentiteDeclarant.modifDomicile.modifDateDomicile != null) {
var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	
if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee4.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement')) {
occurrence['E92/E92.1']= "P";
} else if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not personneLiee4.indivisaireHeritier)
	or Value('id').of(personneLiee4.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualiteProprietaireIndivis')
	or objet.cadreObjet22P.declarationIndivisaire) {
occurrence['E92/E92.1']= "N";
}	
occurrence['E93/E93.2']= personneLiee4.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
if (personneLiee4.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null) {
occurrence['E93/E93.4']= personneLiee4.personneLieePersonnePhysiqueNomUsagePersonnePouvoir;
}
var prenoms=[];
for ( i = 0; i < personneLiee4.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee4.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}      
occurrence['E93/E93.3']= prenoms;
if (not Value('id').of(personneLiee4.objetModifPersonneLiee).eq('partantPersonneLiee')) {
occurrence['E94/E94.3']= personneLiee4.cadreAdressePersonnePouvoir4.personneLieeAdresseCommunepersonnePouvoir.getId();
if (personneLiee4.cadreAdressePersonnePouvoir4.rueNumeroAdressePersonnePouvoir != null) {
occurrence['E94/E94.5'] = personneLiee4.cadreAdressePersonnePouvoir4.rueNumeroAdressePersonnePouvoir;
}
if (personneLiee4.cadreAdressePersonnePouvoir4.indiceVoiePersonnePouvoir != null) {																							
	var monId1 = Value('id').of(personneLiee4.cadreAdressePersonnePouvoir4.indiceVoiePersonnePouvoir)._eval();
occurrence['E94/E94.6']= monId1;
}
if (personneLiee4.cadreAdressePersonnePouvoir4.distriutionSpecialePersonnePouvoir != null) {
occurrence['E94/E94.7'] = personneLiee4.cadreAdressePersonnePouvoir4.distriutionSpecialePersonnePouvoir;
}
occurrence['E94/E94.8'] = personneLiee4.cadreAdressePersonnePouvoir4.personneLieeAdresseCodePostalPersonnePouvoir;
if (personneLiee4.cadreAdressePersonnePouvoir4.rueComplementAdressePersonnePouvoir != null) {
occurrence['E94/E94.10'] = personneLiee4.cadreAdressePersonnePouvoir4.rueComplementAdressePersonnePouvoir;
}
if (personneLiee4.cadreAdressePersonnePouvoir4.typeVoiePersonnePouvoir != null) {																							
	var monId1 = Value('id').of(personneLiee4.cadreAdressePersonnePouvoir4.typeVoiePersonnePouvoir)._eval();
occurrence['E94/E94.11']= monId1;
}
occurrence['E94/E94.12'] = personneLiee4.cadreAdressePersonnePouvoir4.nomVoiePersonnePouvoir;
occurrence['E94/E94.13']= personneLiee4.cadreAdressePersonnePouvoir4.personneLieeAdresseCommunepersonnePouvoir.getLabel();
}
if ((((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers'))
	or Value('id').of(personneLiee4.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and Value('id').of(personneLiee4.objetModifPersonneLiee).eq('nouveauPersonneLiee')) 
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir) {
if (personneLiee4.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir != null) {
var dateTemp = new Date(parseInt(personneLiee4.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E95/E95.1']= date;
}	
if (not Value('id').of(personneLiee4.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
	var idPays = personneLiee4.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['E95/E95.2']= result;
} else if (Value('id').of(personneLiee4.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
occurrence['E95/E95.2'] = personneLiee4.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getId();
}	
if (not Value('id').of(personneLiee4.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {																					 
occurrence['E95/E95.3']= personneLiee4.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getLabel();
}
if (Value('id').of(personneLiee4.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {	
occurrence['E95/E95.4']= personneLiee4.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getLabel();
}	
occurrence['E96']= personneLiee4.personneLieePersonnePhysiqueNationalitePersonnePouvoir;
}
occurrencesPersonneLiee.push(occurrence);
occurrence = {}; 
}

// 5ème personne liée
if (personneLiee4.autreDeclarationPersonneLiee and not personneLiee5.indivisaireHeritier) {
if (objet.cadreObjet22P.declarationIndivisaire
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or 	Value('id').of(personneLiee5.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
occurrence['E91/E91.1'] = "1";
}	else if (Value('id').of(personneLiee5.objetModifPersonneLiee).eq('partantPersonneLiee')) {
occurrence['E91/E91.1'] = "2";
}	else if (Value('id').of(personneLiee5.objetModifPersonneLiee).eq('modifPersonneLiee')) {
occurrence['E91/E91.1'] = "3";
}
if (objet.cadreObjet22P.declarationIndivisaire and objet.cadreObjet22P.modifDateDeces != null) {
var dateTemp = new Date(parseInt(objet.cadreObjet22P.modifDateDeces.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') and etablissementNew.modifDateAdresseOuverture != null) {
var dateTemp = new Date(parseInt(etablissementNew.modifDateAdresseOuverture.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') and etablissementNew.modifDateAdresseReprise != null) {
var dateTemp = new Date(parseInt(etablissementNew.modifDateAdresseReprise.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissement.modifDateAdresseEntreprise != null) {
var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise and objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise != null) {
var dateTemp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P') or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')) and personneLiee.modifDatePersonneLiee != null) {
var dateTemp = new Date(parseInt(personneLiee.modifDatePersonneLiee.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir and modifIdentiteDeclarant.modifDomicile.modifDateDomicile != null) {
var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}
if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee5.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement')) {
occurrence['E92/E92.1']= "P";
} else if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not personneLiee5.indivisaireHeritier)
	or Value('id').of(personneLiee5.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualiteProprietaireIndivis')
	or objet.cadreObjet22P.declarationIndivisaire) {
occurrence['E92/E92.1']= "N";
}	
occurrence['E93/E93.2']= personneLiee5.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
if (personneLiee5.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null) {
occurrence['E93/E93.4']= personneLiee5.personneLieePersonnePhysiqueNomUsagePersonnePouvoir;
}
var prenoms=[];
for ( i = 0; i < personneLiee5.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee5.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}      
occurrence['E93/E93.3']= prenoms;
if (not Value('id').of(personneLiee5.objetModifPersonneLiee).eq('partantPersonneLiee')) {
occurrence['E94/E94.3']= personneLiee5.cadreAdressePersonnePouvoir5.personneLieeAdresseCommunepersonnePouvoir.getId();
if (personneLiee5.cadreAdressePersonnePouvoir5.rueNumeroAdressePersonnePouvoir != null) {
occurrence['E94/E94.5'] = personneLiee5.cadreAdressePersonnePouvoir5.rueNumeroAdressePersonnePouvoir;
}
if (personneLiee5.cadreAdressePersonnePouvoir5.indiceVoiePersonnePouvoir != null) {																							
	var monId1 = Value('id').of(personneLiee5.cadreAdressePersonnePouvoir5.indiceVoiePersonnePouvoir)._eval();
occurrence['E94/E94.6']= monId1;
}
if (personneLiee5.cadreAdressePersonnePouvoir5.distriutionSpecialePersonnePouvoir != null) {
occurrence['E94/E94.7'] = personneLiee5.cadreAdressePersonnePouvoir5.distriutionSpecialePersonnePouvoir;
}
occurrence['E94/E94.8'] = personneLiee5.cadreAdressePersonnePouvoir5.personneLieeAdresseCodePostalPersonnePouvoir;
if (personneLiee5.cadreAdressePersonnePouvoir5.rueComplementAdressePersonnePouvoir != null) {
occurrence['E94/E94.10'] = personneLiee5.cadreAdressePersonnePouvoir5.rueComplementAdressePersonnePouvoir;
}
if (personneLiee5.cadreAdressePersonnePouvoir5.typeVoiePersonnePouvoir != null) {																							
	var monId1 = Value('id').of(personneLiee5.cadreAdressePersonnePouvoir5.typeVoiePersonnePouvoir)._eval();
occurrence['E94/E94.11']= monId1;
}
occurrence['E94/E94.12'] = personneLiee5.cadreAdressePersonnePouvoir5.nomVoiePersonnePouvoir;
occurrence['E94/E94.13']= personneLiee5.cadreAdressePersonnePouvoir5.personneLieeAdresseCommunepersonnePouvoir.getLabel();
}
if ((((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers'))
	or Value('id').of(personneLiee5.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and Value('id').of(personneLiee5.objetModifPersonneLiee).eq('nouveauPersonneLiee')) 
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir) {
if (personneLiee5.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir != null) {
var dateTemp = new Date(parseInt(personneLiee5.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E95/E95.1']= date;
}	
if (not Value('id').of(personneLiee5.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
	var idPays = personneLiee5.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['E95/E95.2']= result;
} else if (Value('id').of(personneLiee5.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
occurrence['E95/E95.2'] = personneLiee5.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getId();
}	
if (not Value('id').of(personneLiee5.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {																					 
occurrence['E95/E95.3']= personneLiee5.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getLabel();
}
if (Value('id').of(personneLiee5.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {	
occurrence['E95/E95.4']= personneLiee5.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getLabel();
}	
occurrence['E96']= personneLiee5.personneLieePersonnePhysiqueNationalitePersonnePouvoir;
}
occurrencesPersonneLiee.push(occurrence);
occurrence = {}; 
}

// 6ème personne liée
if (personneLiee5.autreDeclarationPersonneLiee and not personneLiee6.indivisaireHeritier) {
if (objet.cadreObjet22P.declarationIndivisaire
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or 	Value('id').of(personneLiee6.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
occurrence['E91/E91.1'] = "1";
}	else if (Value('id').of(personneLiee6.objetModifPersonneLiee).eq('partantPersonneLiee')) {
occurrence['E91/E91.1'] = "2";
}	else if (Value('id').of(personneLiee6.objetModifPersonneLiee).eq('modifPersonneLiee')) {
occurrence['E91/E91.1'] = "3";
}
if (objet.cadreObjet22P.declarationIndivisaire and objet.cadreObjet22P.modifDateDeces != null) {
var dateTemp = new Date(parseInt(objet.cadreObjet22P.modifDateDeces.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') and etablissementNew.modifDateAdresseOuverture != null) {
var dateTemp = new Date(parseInt(etablissementNew.modifDateAdresseOuverture.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') and etablissementNew.modifDateAdresseReprise != null) {
var dateTemp = new Date(parseInt(etablissementNew.modifDateAdresseReprise.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissement.modifDateAdresseEntreprise != null) {
var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise and objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise != null) {
var dateTemp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P') or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')) and personneLiee.modifDatePersonneLiee != null) {
var dateTemp = new Date(parseInt(personneLiee.modifDatePersonneLiee.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir and modifIdentiteDeclarant.modifDomicile.modifDateDomicile != null) {
var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}
if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee6.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement')) {
occurrence['E92/E92.1']= "P";
} else if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not personneLiee6.indivisaireHeritier)
	or Value('id').of(personneLiee6.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualiteProprietaireIndivis')
	or objet.cadreObjet22P.declarationIndivisaire) {
occurrence['E92/E92.1']= "N";
}	
occurrence['E93/E93.2']= personneLiee6.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
if (personneLiee6.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null) {
occurrence['E93/E93.4']= personneLiee6.personneLieePersonnePhysiqueNomUsagePersonnePouvoir;
}
var prenoms=[];
for ( i = 0; i < personneLiee6.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee6.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}      
occurrence['E93/E93.3']= prenoms;
if (not Value('id').of(personneLiee6.objetModifPersonneLiee).eq('partantPersonneLiee')) {
occurrence['E94/E94.3']= personneLiee6.cadreAdressePersonnePouvoir6.personneLieeAdresseCommunepersonnePouvoir.getId();
if (personneLiee6.cadreAdressePersonnePouvoir6.rueNumeroAdressePersonnePouvoir != null) {
occurrence['E94/E94.5'] = personneLiee6.cadreAdressePersonnePouvoir6.rueNumeroAdressePersonnePouvoir;
}
if (personneLiee6.cadreAdressePersonnePouvoir6.indiceVoiePersonnePouvoir != null) {																							
	var monId1 = Value('id').of(personneLiee6.cadreAdressePersonnePouvoir6.indiceVoiePersonnePouvoir)._eval();
occurrence['E94/E94.6']= monId1;
}
if (personneLiee6.cadreAdressePersonnePouvoir6.distriutionSpecialePersonnePouvoir != null) {
occurrence['E94/E94.7'] = personneLiee6.cadreAdressePersonnePouvoir6.distriutionSpecialePersonnePouvoir;
}
occurrence['E94/E94.8'] = personneLiee6.cadreAdressePersonnePouvoir6.personneLieeAdresseCodePostalPersonnePouvoir;
if (personneLiee6.cadreAdressePersonnePouvoir6.rueComplementAdressePersonnePouvoir != null) {
occurrence['E94/E94.10'] = personneLiee6.cadreAdressePersonnePouvoir6.rueComplementAdressePersonnePouvoir;
}
if (personneLiee6.cadreAdressePersonnePouvoir6.typeVoiePersonnePouvoir != null) {																							
	var monId1 = Value('id').of(personneLiee6.cadreAdressePersonnePouvoir6.typeVoiePersonnePouvoir)._eval();
occurrence['E94/E94.11']= monId1;
}
occurrence['E94/E94.12'] = personneLiee6.cadreAdressePersonnePouvoir6.nomVoiePersonnePouvoir;
occurrence['E94/E94.13']= personneLiee6.cadreAdressePersonnePouvoir6.personneLieeAdresseCommunepersonnePouvoir.getLabel();
}
if ((((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers'))
	or Value('id').of(personneLiee6.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and Value('id').of(personneLiee6.objetModifPersonneLiee).eq('nouveauPersonneLiee')) 
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir) {
if (personneLiee6.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir != null) {
var dateTemp = new Date(parseInt(personneLiee6.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E95/E95.1']= date;
}	
if (not Value('id').of(personneLiee6.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
	var idPays = personneLiee6.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['E95/E95.2']= result;
} else if (Value('id').of(personneLiee6.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
occurrence['E95/E95.2'] = personneLiee6.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getId();
}	
if (not Value('id').of(personneLiee6.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {																					 
occurrence['E95/E95.3']= personneLiee6.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getLabel();
}
if (Value('id').of(personneLiee6.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {	
occurrence['E95/E95.4']= personneLiee6.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getLabel();
}	
occurrence['E96']= personneLiee6.personneLieePersonnePhysiqueNationalitePersonnePouvoir;
}
occurrencesPersonneLiee.push(occurrence);
occurrence = {}; 
}

// 7ème personne liée
if (personneLiee6.autreDeclarationPersonneLiee and not personneLiee7.indivisaireHeritier) {
if (objet.cadreObjet22P.declarationIndivisaire
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or 	Value('id').of(personneLiee7.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
occurrence['E91/E91.1'] = "1";
}	else if (Value('id').of(personneLiee7.objetModifPersonneLiee).eq('partantPersonneLiee')) {
occurrence['E91/E91.1'] = "2";
}	else if (Value('id').of(personneLiee7.objetModifPersonneLiee).eq('modifPersonneLiee')) {
occurrence['E91/E91.1'] = "3";
}
if (objet.cadreObjet22P.declarationIndivisaire and objet.cadreObjet22P.modifDateDeces != null) {
var dateTemp = new Date(parseInt(objet.cadreObjet22P.modifDateDeces.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') and etablissementNew.modifDateAdresseOuverture != null) {
var dateTemp = new Date(parseInt(etablissementNew.modifDateAdresseOuverture.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') and etablissementNew.modifDateAdresseReprise != null) {
var dateTemp = new Date(parseInt(etablissementNew.modifDateAdresseReprise.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissement.modifDateAdresseEntreprise != null) {
var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise and objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise != null) {
var dateTemp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P') or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')) and personneLiee.modifDatePersonneLiee != null) {
var dateTemp = new Date(parseInt(personneLiee.modifDatePersonneLiee.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir and modifIdentiteDeclarant.modifDomicile.modifDateDomicile != null) {
var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	
if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee7.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement')) {
occurrence['E92/E92.1']= "P";
} else if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not personneLiee7.indivisaireHeritier)
	or Value('id').of(personneLiee7.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualiteProprietaireIndivis')
	or objet.cadreObjet22P.declarationIndivisaire) {
occurrence['E92/E92.1']= "N";
}	
occurrence['E93/E93.2']= personneLiee7.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
if (personneLiee7.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null) {
occurrence['E93/E93.4']= personneLiee7.personneLieePersonnePhysiqueNomUsagePersonnePouvoir;
}
var prenoms=[];
for ( i = 0; i < personneLiee7.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee7.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}      
occurrence['E93/E93.3']= prenoms;
if (not Value('id').of(personneLiee7.objetModifPersonneLiee).eq('partantPersonneLiee')) {
occurrence['E94/E94.3']= personneLiee7.cadreAdressePersonnePouvoir7.personneLieeAdresseCommunepersonnePouvoir.getId();
if (personneLiee7.cadreAdressePersonnePouvoir7.rueNumeroAdressePersonnePouvoir != null) {
occurrence['E94/E94.5'] = personneLiee7.cadreAdressePersonnePouvoir7.rueNumeroAdressePersonnePouvoir;
}
if (personneLiee7.cadreAdressePersonnePouvoir7.indiceVoiePersonnePouvoir != null) {																							
	var monId1 = Value('id').of(personneLiee7.cadreAdressePersonnePouvoir7.indiceVoiePersonnePouvoir)._eval();
occurrence['E94/E94.6']= monId1;
}
if (personneLiee7.cadreAdressePersonnePouvoir7.distriutionSpecialePersonnePouvoir != null) {
occurrence['E94/E94.7'] = personneLiee7.cadreAdressePersonnePouvoir7.distriutionSpecialePersonnePouvoir;
}
occurrence['E94/E94.8'] = personneLiee7.cadreAdressePersonnePouvoir7.personneLieeAdresseCodePostalPersonnePouvoir;
if (personneLiee7.cadreAdressePersonnePouvoir7.rueComplementAdressePersonnePouvoir != null) {
occurrence['E94/E94.10'] = personneLiee7.cadreAdressePersonnePouvoir7.rueComplementAdressePersonnePouvoir;
}
if (personneLiee7.cadreAdressePersonnePouvoir7.typeVoiePersonnePouvoir != null) {																							
	var monId1 = Value('id').of(personneLiee7.cadreAdressePersonnePouvoir7.typeVoiePersonnePouvoir)._eval();
occurrence['E94/E94.11']= monId1;
}
occurrence['E94/E94.12'] = personneLiee7.cadreAdressePersonnePouvoir7.nomVoiePersonnePouvoir;
occurrence['E94/E94.13']= personneLiee7.cadreAdressePersonnePouvoir7.personneLieeAdresseCommunepersonnePouvoir.getLabel();
}
if ((((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers'))
	or Value('id').of(personneLiee7.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and Value('id').of(personneLiee7.objetModifPersonneLiee).eq('nouveauPersonneLiee')) 
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir) {
if (personneLiee7.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir != null) {
var dateTemp = new Date(parseInt(personneLiee7.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E95/E95.1']= date;
}	
if (not Value('id').of(personneLiee7.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
	var idPays = personneLiee7.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['E95/E95.2']= result;
} else if (Value('id').of(personneLiee7.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
occurrence['E95/E95.2'] = personneLiee7.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getId();
}	
if (not Value('id').of(personneLiee7.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {																					 
occurrence['E95/E95.3']= personneLiee7.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getLabel();
}
if (Value('id').of(personneLiee7.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {	
occurrence['E95/E95.4']= personneLiee7.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getLabel();
}	
occurrence['E96']= personneLiee7.personneLieePersonnePhysiqueNationalitePersonnePouvoir;
}
occurrencesPersonneLiee.push(occurrence);
occurrence = {}; 
}

// 8ème personne liée
if (personneLiee7.autreDeclarationPersonneLiee and not personneLiee8.indivisaireHeritier) {
if (objet.cadreObjet22P.declarationIndivisaire
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or 	Value('id').of(personneLiee8.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
occurrence['E91/E91.1'] = "1";
}	else if (Value('id').of(personneLiee8.objetModifPersonneLiee).eq('partantPersonneLiee')) {
occurrence['E91/E91.1'] = "2";
}	else if (Value('id').of(personneLiee8.objetModifPersonneLiee).eq('modifPersonneLiee')) {
occurrence['E91/E91.1'] = "3";
}
if (objet.cadreObjet22P.declarationIndivisaire and objet.cadreObjet22P.modifDateDeces != null) {
var dateTemp = new Date(parseInt(objet.cadreObjet22P.modifDateDeces.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') and etablissementNew.modifDateAdresseOuverture != null) {
var dateTemp = new Date(parseInt(etablissementNew.modifDateAdresseOuverture.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') and etablissementNew.modifDateAdresseReprise != null) {
var dateTemp = new Date(parseInt(etablissementNew.modifDateAdresseReprise.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissement.modifDateAdresseEntreprise != null) {
var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise and objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise != null) {
var dateTemp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P') or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')) and personneLiee.modifDatePersonneLiee != null) {
var dateTemp = new Date(parseInt(personneLiee.modifDatePersonneLiee.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir and modifIdentiteDeclarant.modifDomicile.modifDateDomicile != null) {
var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	
if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee8.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement')) {
occurrence['E92/E92.1']= "P";
} else if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not personneLiee8.indivisaireHeritier)
	or Value('id').of(personneLiee8.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualiteProprietaireIndivis')
	or objet.cadreObjet22P.declarationIndivisaire) {
occurrence['E92/E92.1']= "N";
}	
occurrence['E93/E93.2']= personneLiee8.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
if (personneLiee8.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null) {
occurrence['E93/E93.4']= personneLiee8.personneLieePersonnePhysiqueNomUsagePersonnePouvoir;
}
var prenoms=[];
for ( i = 0; i < personneLiee8.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee8.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}      
occurrence['E93/E93.3']= prenoms;
if (not Value('id').of(personneLiee8.objetModifPersonneLiee).eq('partantPersonneLiee')) {
occurrence['E94/E94.3']= personneLiee8.cadreAdressePersonnePouvoir8.personneLieeAdresseCommunepersonnePouvoir.getId();
if (personneLiee8.cadreAdressePersonnePouvoir8.rueNumeroAdressePersonnePouvoir != null) {
occurrence['E94/E94.5'] = personneLiee8.cadreAdressePersonnePouvoir8.rueNumeroAdressePersonnePouvoir;
}
if (personneLiee8.cadreAdressePersonnePouvoir8.indiceVoiePersonnePouvoir != null) {																							
	var monId1 = Value('id').of(personneLiee8.cadreAdressePersonnePouvoir8.indiceVoiePersonnePouvoir)._eval();
occurrence['E94/E94.6']= monId1;
}
if (personneLiee8.cadreAdressePersonnePouvoir8.distriutionSpecialePersonnePouvoir != null) {
occurrence['E94/E94.7'] = personneLiee8.cadreAdressePersonnePouvoir8.distriutionSpecialePersonnePouvoir;
}
occurrence['E94/E94.8'] = personneLiee8.cadreAdressePersonnePouvoir8.personneLieeAdresseCodePostalPersonnePouvoir;
if (personneLiee8.cadreAdressePersonnePouvoir8.rueComplementAdressePersonnePouvoir != null) {
occurrence['E94/E94.10'] = personneLiee8.cadreAdressePersonnePouvoir8.rueComplementAdressePersonnePouvoir;
}
if (personneLiee8.cadreAdressePersonnePouvoir8.typeVoiePersonnePouvoir != null) {																							
	var monId1 = Value('id').of(personneLiee8.cadreAdressePersonnePouvoir8.typeVoiePersonnePouvoir)._eval();
occurrence['E94/E94.11']= monId1;
}
occurrence['E94/E94.12'] = personneLiee8.cadreAdressePersonnePouvoir8.nomVoiePersonnePouvoir;
occurrence['E94/E94.13']= personneLiee8.cadreAdressePersonnePouvoir8.personneLieeAdresseCommunepersonnePouvoir.getLabel();
}
if ((((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers'))
	or Value('id').of(personneLiee8.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and Value('id').of(personneLiee8.objetModifPersonneLiee).eq('nouveauPersonneLiee')) 
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir) {
if (personneLiee8.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir != null) {
var dateTemp = new Date(parseInt(personneLiee8.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E95/E95.1']= date;
}	
if (not Value('id').of(personneLiee8.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
	var idPays = personneLiee8.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['E95/E95.2']= result;
} else if (Value('id').of(personneLiee8.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
occurrence['E95/E95.2'] = personneLiee8.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getId();
}	
if (not Value('id').of(personneLiee8.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {																					 
occurrence['E95/E95.3']= personneLiee8.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getLabel();
}
if (Value('id').of(personneLiee8.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {	
occurrence['E95/E95.4']= personneLiee8.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getLabel();
}	
occurrence['E96']= personneLiee8.personneLieePersonnePhysiqueNationalitePersonnePouvoir;
}
occurrencesPersonneLiee.push(occurrence);
occurrence = {}; 
}

// 9ème personne liée
if (personneLiee8.autreDeclarationPersonneLiee and not personneLiee9.indivisaireHeritier) {
if (objet.cadreObjet22P.declarationIndivisaire
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or 	Value('id').of(personneLiee9.objetModifPersonneLiee).eq('nouveauPersonneLiee')) {
occurrence['E91/E91.1'] = "1";
}	else if (Value('id').of(personneLiee9.objetModifPersonneLiee).eq('partantPersonneLiee')) {
occurrence['E91/E91.1'] = "2";
}	else if (Value('id').of(personneLiee9.objetModifPersonneLiee).eq('modifPersonneLiee')) {
occurrence['E91/E91.1'] = "3";
}
if (objet.cadreObjet22P.declarationIndivisaire and objet.cadreObjet22P.modifDateDeces != null) {
var dateTemp = new Date(parseInt(objet.cadreObjet22P.modifDateDeces.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') and etablissementNew.modifDateAdresseOuverture != null) {
var dateTemp = new Date(parseInt(etablissementNew.modifDateAdresseOuverture.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('53P') and etablissementNew.modifDateAdresseReprise != null) {
var dateTemp = new Date(parseInt(etablissementNew.modifDateAdresseReprise.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissement.modifDateAdresseEntreprise != null) {
var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (etablissementNew.ajoutFondePouvoir and objet.cadreObjetModificationEtablissement.cadreObjet21P.transfertReprise and objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise != null) {
var dateTemp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P') or Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')) and personneLiee.modifDatePersonneLiee != null) {
var dateTemp = new Date(parseInt(personneLiee.modifDatePersonneLiee.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	else if (modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir and modifIdentiteDeclarant.modifDomicile.modifDateDomicile != null) {
var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E91/E91.2']= date;
}	
if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers'))
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir
	or Value('id').of(personneLiee9.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement')) {
occurrence['E92/E92.1']= "P";
} else if ((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not personneLiee9.indivisaireHeritier)
	or Value('id').of(personneLiee9.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualiteProprietaireIndivis')
	or objet.cadreObjet22P.declarationIndivisaire) {
occurrence['E92/E92.1']= "N";
}	
occurrence['E93/E93.2']= personneLiee9.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
if (personneLiee9.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null) {
occurrence['E93/E93.4']= personneLiee9.personneLieePersonnePhysiqueNomUsagePersonnePouvoir;
}
var prenoms=[];
for ( i = 0; i < personneLiee9.personneLieePersonnePhysiquePrenom1PersonnePouvoir.size() ; i++ ){prenoms.push(personneLiee9.personneLieePersonnePhysiquePrenom1PersonnePouvoir[i]);}      
occurrence['E93/E93.3']= prenoms;
if (not Value('id').of(personneLiee9.objetModifPersonneLiee).eq('partantPersonneLiee')) {
occurrence['E94/E94.3']= personneLiee9.cadreAdressePersonnePouvoir9.personneLieeAdresseCommunepersonnePouvoir.getId();
if (personneLiee9.cadreAdressePersonnePouvoir9.rueNumeroAdressePersonnePouvoir != null) {
occurrence['E94/E94.5'] = personneLiee9.cadreAdressePersonnePouvoir9.rueNumeroAdressePersonnePouvoir;
}
if (personneLiee9.cadreAdressePersonnePouvoir9.indiceVoiePersonnePouvoir != null) {																							
	var monId1 = Value('id').of(personneLiee9.cadreAdressePersonnePouvoir9.indiceVoiePersonnePouvoir)._eval();
occurrence['E94/E94.6']= monId1;
}
if (personneLiee9.cadreAdressePersonnePouvoir9.distriutionSpecialePersonnePouvoir != null) {
occurrence['E94/E94.7'] = personneLiee9.cadreAdressePersonnePouvoir9.distriutionSpecialePersonnePouvoir;
}
occurrence['E94/E94.8'] = personneLiee9.cadreAdressePersonnePouvoir9.personneLieeAdresseCodePostalPersonnePouvoir;
if (personneLiee9.cadreAdressePersonnePouvoir9.rueComplementAdressePersonnePouvoir != null) {
occurrence['E94/E94.10'] = personneLiee9.cadreAdressePersonnePouvoir9.rueComplementAdressePersonnePouvoir;
}
if (personneLiee9.cadreAdressePersonnePouvoir9.typeVoiePersonnePouvoir != null) {																							
	var monId1 = Value('id').of(personneLiee9.cadreAdressePersonnePouvoir9.typeVoiePersonnePouvoir)._eval();
occurrence['E94/E94.11']= monId1;
}
occurrence['E94/E94.12'] = personneLiee9.cadreAdressePersonnePouvoir9.nomVoiePersonnePouvoir;
occurrence['E94/E94.13']= personneLiee9.cadreAdressePersonnePouvoir9.personneLieeAdresseCommunepersonnePouvoir.getLabel();
}
if ((((Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('70P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objetPersonneLiee).contains('77P')
	and not Value('id').of(objet.cadreObjetPersonneLiee.objet31P).contains('heritiers'))
	or Value('id').of(personneLiee9.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement'))
	and Value('id').of(personneLiee9.objetModifPersonneLiee).eq('nouveauPersonneLiee')) 
	or etablissementNew.ajoutFondePouvoir
	or modifIdentiteDeclarant.modifDomicile.domicileAjoutFondePouvoir) {
if (personneLiee9.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir != null) {
var dateTemp = new Date(parseInt(personneLiee9.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
occurrence['E95/E95.1']= date;
}	
if (not Value('id').of(personneLiee9.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
	var idPays = personneLiee9.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getId();
	var result = idPays.match(regEx)[0]; 
occurrence['E95/E95.2']= result;
} else if (Value('id').of(personneLiee9.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {
occurrence['E95/E95.2'] = personneLiee9.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getId();
}	
if (not Value('id').of(personneLiee9.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {																					 
occurrence['E95/E95.3']= personneLiee9.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir.getLabel();
}
if (Value('id').of(personneLiee9.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir).eq('FRXXXXX')) {	
occurrence['E95/E95.4']= personneLiee9.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir.getLabel();
}	
occurrence['E96']= personneLiee9.personneLieePersonnePhysiqueNationalitePersonnePouvoir;
}
occurrencesPersonneLiee.push(occurrence);
occurrence = {}; 
}

occurrencesPersonneLiee.forEach(function (value, i) {
	var idx = i+1;
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E91/E91.1']= value['E91/E91.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E91/E91.2']= value['E91/E91.2'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E92/E92.1']= value['E92/E92.1'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E93/E93.2']= value['E93/E93.2'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E93/E93.4']= value['E93/E93.4'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E93/E93.3']= value['E93/E93.3'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E94/E94.3']= value['E94/E94.3'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E94/E94.5']= value['E94/E94.5'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E94/E94.6']= value['E94/E94.6'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E94/E94.7']= value['E94/E94.7'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E94/E94.8']= value['E94/E94.8'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E94/E94.10']= value['E94/E94.10'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E94/E94.11']= value['E94/E94.11'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E94/E94.12']= value['E94/E94.12'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E94/E94.13']= value['E94/E94.13'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E95/E95.1']= value['E95/E95.1'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E95/E95.2']= value['E95/E95.2'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E95/E95.3']= value['E95/E95.3'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E95/E95.4']= value['E95/E95.4'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IDE[' + idx + ']/E96']= value['E96'];	
	});

}

				// Si 40P, 42P, 43P  : Groupe GFE

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('cessationActivite') 
	or Value('id').of(objet.cadreObjet22P.infoDeces).eq('42P')) {			
				
				//Groupe IAE : Identification ancienne de l'établissement
				
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E12/E12.3'] = adresseEntreprise.etablissementAdresseCommune.getId();
																	
if (adresseEntreprise.etablissementAdresseNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E12/E12.5'] = adresseEntreprise.etablissementAdresseNumeroVoie;
}
	
if (adresseEntreprise.etablissementAdresseIndiceVoie != null) {
   var monId1 = Value('id').of(adresseEntreprise.etablissementAdresseIndiceVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E12/E12.6']          = monId1;
}  

if (adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E12/E12.7']= adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E12/E12.8']= adresseEntreprise.etablissementAdresseCodePostal;

if (adresseEntreprise.etablissementAdresseComplementVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E12/E12.10']= adresseEntreprise.etablissementAdresseComplementVoie ;
}

if (adresseEntreprise.etablissementAdresseTypeVoie != null) {
   var monId1 = Value('id').of(adresseEntreprise.etablissementAdresseTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E12/E12.11']          = monId1;
} 

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E12/E12.12']= adresseEntreprise.etablissementAdresseNomVoie;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E12/E12.13']= adresseEntreprise.etablissementAdresseCommune.getLabel();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E13/E13.1'] = "000000000";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E13/E13.2'] = "00000";

if(etablissement.dateCessationEmploi != null) {
var dateTemp = new Date(parseInt(etablissement.dateCessationEmploi.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E14']= date;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E17']= "3";

			// Groupe DEE : Destination de l'établissement

if (Value('id').of(etablissement.destinationEtablissement3).eq('vendu')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/DEE/E41/E41.1']= "3";
} else if (Value('id').of(etablissement.destinationEtablissement3).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/DEE/E41/E41.1']= "9";
} else if (Value('id').of(etablissement.destinationEtablissement3).eq('supprime')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/DEE/E41/E41.1']= "C";
}

if (Value('id').of(etablissement.destinationEtablissement3).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/DEE/E41/E41.2']= etablissement.destinationAutre;
}
}

// Fin Regent 40P
// Fin Regent 42P
// Fin Regent 43P


log.info("Regent fields : {}", regentFields);
//-->DEST-689 : Afficher les erreurs XSD dans la formalité quand il y'a une erreur de génération

// Call to the XML-REGENT generation WS 
var response = nash.service.request('${regent.baseUrl}/private/v1/xml-regent/generate/{regentVersion}/{authorityType}/{authorityId}', regentVersion, authorityType, authorityId) 
.dataType('application/json') // 
.accept('json') // 
.param('listTypeEvenement',eventRegent) 
//-->MINE-263 : Permettre à l'équipe FF de voir les problèmes de validation XSD et le regent sur studio
.continueOnError(true) //
.post(JSON.stringify(regentFields)); 

// Record the generated XML-REGENT 
//MOD déplacement du test de la réponse du premier ws
if (response != null && response.status == 200) { 
    var xmlRegentStr = response.asBytes(); 
    nash.record.saveFile("XML_REGENT.xml",xmlRegentStr); 

//debut de l'ajout
 if (authorityId2 != null) {
//MOD extraction du numéro de liasse du premeir regent généré
var numeroLiasse = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02');
numeroLiasse = JSON.parse(numeroLiasse);
_log.info('extracted numero de liasse is {}', numeroLiasse);
 regentFields['/REGENT-XML/Destinataire']= authorityId2;
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05']= "C";
 
 // Filtre regent greffe
var undefined;
// Groupe GCS/ISS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A10/A10.1']  = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A10/A10.2']  = undefined;
// Groupe GCS/SCS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A53/A53.4']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A55/A55.1'] = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A55/A55.2'] = undefined;
// Groupe OFU
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U70']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U71']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U72/U72.1']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U72/U72.2'] = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U74']= undefined; 
// GroupeLIU
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/LIU/U36/U36.2']= undefined;
// Groupe GRD (partie GCS)
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GDR/GCS/ISS/A10/A10.1']= undefined;	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GDR/GCS/ISS/A10/A10.2']= undefined;	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GDR/GCS/SNS/A21/A21.2']= undefined;	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GDR/GCS/SNS/A21/A21.3']= undefined;	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GDR/GCS/SNS/A21/A21.4']= undefined;	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GDR/GCS/SNS/A21/A21.6']= undefined;	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GDR/GCS/SNS/A22/A22.3']= undefined;	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GDR/GCS/SNS/A22/A22.4']= undefined;	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GDR/GCS/SNS/A24/A24.2']= undefined;	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GDR/GCS/SNS/A24/A24.3']= undefined;	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GDR/GCS/CAS/A42/A42.1']= undefined;	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysiqueDirigeante/GDR/GCS/CAS/A44']= undefined;	

// Groupe SAE
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E84']=undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E85/E85.8']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E86']= undefined;

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS']  = undefined;


 //MOD modification de l'appel pour la génération du deuxième régent afin de prendre en compte le numéro de liasse du premier
 var response2 = nash.service.request('${regent.baseUrl}/private/v1/xml-regent/generate/{regentVersion}/{authorityType}/{authorityId}', regentVersion, authorityType2, authorityId2) 
.dataType('application/json') // 
.accept('json') // 
.param('listTypeEvenement',eventRegent) 
.param('liasseNumber', numeroLiasse.C02) 
.continueOnError(true)
.post(JSON.stringify(regentFields)); 


	//Début de l'ajout
	//MOD modifier reponse en response2
	if (response2 != null && response2.status == 200) { 
		var xmlRegentStr = response2.asBytes(); 
		nash.record.saveFile("XML_REGENT2.xml",xmlRegentStr);
	}else{
		
		var returnedResponse = response2.asObject();
		_log.info("Call ws regent returned errors  {}", returnedResponse);
		
		
		var regent2 = returnedResponse['regent'];
		var errors2 = returnedResponse['errors']; 
		nash.record.saveFile("XML_REGENT2.xml",regent2.getBytes()); 
		nash.record.saveFile("XML_VALIDATION_ERRORS_2.xml",errors2.getBytes()); 
		
		//-->MINE-263 : Permettre à l'équipe FF de voir les problèmes de validation XSD et le regent sur studio
		
		return spec.create({
		id : 'xmlGenerationConfirmation',
		label : "Xml Regent confirmation message",
		groups : [ spec.createGroup({
				id : 'Regent ',
				description : "Une erreur s'est produite lors de la génération du XML Regent.",
				data : [
				spec.createData({
					id: 'regent',
					label: "XML regent généré",
					type: 'Text',
					mandatory: true,
					value: regent2
				}),spec.createData({
					id: 'errors',
					label: "Erreurs de validations xsd",
					type: 'Text',
					mandatory: false,
					value: errors2
				}),spec.createData({
					id: 'blockingField',
					label: "xsd erroné",
					help:"Ce champs sert à arrêter le process du dossier pour que le support puisse le consulter",
					type: 'StringReadOnly',
					mandatory: true
				})]
			})]
		});
	}	
}	

	return spec.create({
	id : 'xmlGenerationConfirmation',
	label : "Xml Regent confirmation message",
	groups : [ spec.createGroup({
		id : 'confirmationMessageOk',
		description : "Le fichier XML Regent a été généré et ajouté au dossier.",
		data : []
		}) ]
	});
	
}else{
	
	var returnedResponse = response.asObject();
	_log.info("Call ws regent returned errors  {}", returnedResponse);
	
	
	var regent = returnedResponse['regent'];
	var errors = returnedResponse['errors']; 
	nash.record.saveFile("XML_REGENT.xml",regent.getBytes()); 
	nash.record.saveFile("XML_VALIDATION_ERRORS.xml",errors.getBytes()); 
	
	
	return spec.create({
	id : 'xmlGenerationConfirmation',
	label : "Xml Regent confirmation message",
	groups : [ spec.createGroup({
		id : 'Regent ',
		description : "Une erreur s'est produite lors de la génération du XML Regent de la première autorité.",
		data : [
				spec.createData({
					id: 'regent',
					label: "XML regent généré",
					type: 'Text',
					mandatory: true,
					value: regent
				}),spec.createData({
					id: 'errors',
					label: "Erreurs de validations xsd",
					type: 'Text',
					mandatory: false,
					value: errors
				}),spec.createData({
					id: 'blockingField',
					label: "xsd erroné",
					help:"Ce champs sert à arrêter le process du dossier pour que le support puisse le consulter",
					type: 'StringReadOnly',
					mandatory: true
			})]
		}) ]
	});
}	