var data = nash.instance.load("data.xml");
if (_input.parcoursUsagerGroup.lieuActivite.etablissementAdresseCommune == null){
    return;
}
var codeCommune = _input.parcoursUsagerGroup.lieuActivite.etablissementAdresseCommune.getId();

var responseObject = nash.service.request('${directory.baseUrl}/v1/authority/{authorityId}', codeCommune)
                    .accept('json')
                    .get().asObject();
var funcId = responseObject.details.CA;
var dest = nash.service.request('${directory.baseUrl}/v1/authority/{authorityId}', funcId)
                .accept('json')
                .get().asObject();

 var  cityNumber =   nash.service.request('${directory.baseUrl}/v1/authority/{authorityId}', dest.details.profile.address.cityNumber)
 .accept('json')
 .get().asObject();  

data.bind("parcoursUsagerGroup", {
    "lieuActivite" : {
        "authority" : dest.label,
        "email" : dest.details.profile.email,
        "tel" : dest.details.profile.tel,
        /* Ajout de l'adresse */
        "recipientName" : dest.details.profile.address.recipientName,
        "addressNameCompl" : dest.details.profile.address.addressNameCompl,
        "compl": dest.details.profile.address.compl,
        "special": dest.details.profile.address.special,
        "postalCode": dest.details.profile.address.postalCode,
        "cedex": dest.details.profile.address.cedex,
        "cityNumber": cityNumber.label
    }
});

