// PJ Déclarant

var userDeclarant;
if ($rcmb.cadre1IdentiteGroup.cadre1Identite.personneLieePersonnePhysiqueNomUsage != null) {
    var userDeclarant = $rcmb.cadre1IdentiteGroup.cadre1Identite.personneLieePersonnePhysiqueNomUsage + '  ' + $rcmb.cadre1IdentiteGroup.cadre1Identite.personneLieePersonnePhysiquePrenom1 ;
} else {
    var userDeclarant = $rcmb.cadre1IdentiteGroup.cadre1Identite.personneLieePersonnePhysiqueNomNaissance + '  '+ $rcmb.cadre1IdentiteGroup.cadre1Identite.personneLieePersonnePhysiquePrenom1 ;
}
attachment('pjDNCDeclarant', 'pjDNCDeclarant',{ label: userDeclarant, mandatory:"true"}) ;

var pj=$rcmb.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteDeclarant')) {
    attachment('pjIDDeclarantSignataire', 'pjIDDeclarantSignataire', { label: userDeclarant, mandatory:"true"});
}

var pj=$rcmb.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
    attachment('pjIDDeclarantNonSignataire', 'pjIDDeclarantNonSignataire', { label: userDeclarant, mandatory:"true"});
}

// PJ Mandataire

var pj=$rcmb.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
    attachment('pjPouvoir', 'pjPouvoir', { mandatory:"true"});
}

var userMandataire=$rcmb.cadre11SignatureGroup.cadre11Signature.adresseMandataire

var pj=$rcmb.cadre11SignatureGroup.cadre11Signature.soussigne;
if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
    attachment('pjIDMandataireSignataire', 'pjIDMandataireSignataire', {label: userMandataire.nomPrenomDenominationMandataire, mandatory:"true"});
}

// PJ Conjoint

var pj=$rcmb.cadre2conjointGroup.cadre2Conjoint.statutConjointCollaborateur ;
if(Value('id').of(pj).contains('PersonneLieeConjointCollaborateurSalarieCollaborateur')) {
    attachment('pjIDConjoint', 'pjIDConjoint', { mandatory:"true"});
}

var pj=$rcmb.cadre2conjointGroup.cadre2Conjoint ;
if(pj.conjointCommunauteBiens and pj.conjoint) {
    attachment('pjInformationConjoint', 'pjInformationConjoint', { mandatory:"true"});
}

// PJ Fondé de pouvoir 1

var user=$rcmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir

var pj=$rcmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.personneLieePersonneLieeEtablissementQualite ;
if(Value('id').of(pj).contains('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement') and $rcmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.personneLieePersonneLieeEtablissement) {
    attachment('pjDNCPersonnePouvoir', 'pjDNCPersonnePouvoir', { label : (user.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? user.personneLieePersonnePhysiqueNomUsagePersonnePouvoir : user.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir) + ' '+user.personneLieePersonnePhysiquePrenom1PersonnePouvoir, mandatory:"true"});
}

var pj=$rcmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.personneLieePersonneLieeEtablissementQualite ;
if(Value('id').of(pj).contains('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement') and $rcmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.personneLieePersonneLieeEtablissement) {
    attachment('pjIDPersonnePouvoir', 'pjIDPersonnePouvoir', { label : (user.personneLieePersonnePhysiqueNomUsagePersonnePouvoir != null ? user.personneLieePersonnePhysiqueNomUsagePersonnePouvoir : user.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir) + ' '+user.personneLieePersonnePhysiquePrenom1PersonnePouvoir, mandatory:"true"});
}

// PJ Fondé de pouvoir 2

var userFondePouvoir2=$rcmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir2

var pj=$rcmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir2.personneLieePersonneLieeEtablissementQualite2 ;
if(Value('id').of(pj).contains('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement2') and $rcmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir.autreDeclarationPersonneLiee) {
    attachment('pjDNCPersonnePouvoir2', 'pjDNCPersonnePouvoir2', { label : (userFondePouvoir2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir2 != null ? userFondePouvoir2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir2 : userFondePouvoir2.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir2) + ' '+userFondePouvoir2.personneLieePersonnePhysiquePrenom1PersonnePouvoir2, mandatory:"true"});
}

var pj=$rcmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir2.personneLieePersonneLieeEtablissementQualite2 ;
if(Value('id').of(pj).contains('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement2') and $rcmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir.autreDeclarationPersonneLiee) {
    attachment('pjIDPersonnePouvoir2', 'pjIDPersonnePouvoir2', { label : (userFondePouvoir2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir2 != null ? userFondePouvoir2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir2 : userFondePouvoir2.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir2) + ' '+userFondePouvoir2.personneLieePersonnePhysiquePrenom1PersonnePouvoir2, mandatory:"true"});
}

// PJ Fondé de pouvoir 3

var userFondePouvoir3=$rcmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir3

var pj=$rcmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir3.personneLieePersonneLieeEtablissementQualite3 ;
if(Value('id').of(pj).contains('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement3') and $rcmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir2.autreDeclarationPersonneLiee2) {
    attachment('pjDNCPersonnePouvoir3', 'pjDNCPersonnePouvoir3', { label : (userFondePouvoir3.personneLieePersonnePhysiqueNomUsagePersonnePouvoir3 != null ? userFondePouvoir3.personneLieePersonnePhysiqueNomUsagePersonnePouvoir3 : userFondePouvoir3.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir3) + ' '+userFondePouvoir3.personneLieePersonnePhysiquePrenom1PersonnePouvoir3, mandatory:"true"});
}

var pj=$rcmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir3.personneLieePersonneLieeEtablissementQualite3 ;
if(Value('id').of(pj).contains('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement3') and $rcmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir2.autreDeclarationPersonneLiee2) {
    attachment('pjIDPersonnePouvoir3', 'pjIDPersonnePouvoir3', { label : (userFondePouvoir3.personneLieePersonnePhysiqueNomUsagePersonnePouvoir3 != null ? userFondePouvoir3.personneLieePersonnePhysiqueNomUsagePersonnePouvoir3 : userFondePouvoir3.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir3) + ' '+userFondePouvoir3.personneLieePersonnePhysiquePrenom1PersonnePouvoir3, mandatory:"true"});
}

// PJ mineur émancipé

var pj=$rcmb.cadre1IdentiteGroup.cadre1Identite ;
if(pj.personneLieePersonnePhysiqueMineurEmancipe) {
    attachment('pjIDMineurEmancipe', 'pjIDMineurEmancipe', { mandatory:"true"});
}

// PJ Insaisissabilité

var pj=$rcmb.cadre3InformationsComplementairesGroup.cadre3InformationsComplementaires ;
if(pj.cadre3DeclarationInsaisissabilite.entrepriseInsaisissabilitePublicationDeclarationAutresBiens) {
    attachment('pjInsaisissabilite', 'pjInsaisissabilite', { mandatory:"true"});
}

// PJ Etablissement

var pj=$rcmb.cadre4AdresseActiviteGroup.cadre4AdresseActivite.activiteLieu ;
if(Value('id').of(pj).contains('entrepriseAdresseEntrepriseDomicile')) {
    attachment('pjDomicile', 'pjDomicile', { mandatory:"true"});
}

var pj=$rcmb.cadre4AdresseActiviteGroup.cadre4AdresseActivite.activiteLieu ;
if(Value('id').of(pj).contains('etablissementDomiciliationOui')) {
    attachment('pjContratDomiciliation', 'pjContratDomiciliation', { mandatory:"true"});
}

var pj=$rcmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsCreation') or ((Value('id').of(pj).contains('EtablissementOrigineFondsAchat')) and $rcmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds.precedentExploitant.fondsArtisanal) or (Value('id').of(pj).contains('EtablissementOrigineFondsCocheAutre'))) {
    attachment('pjEtablissementCreation', 'pjEtablissementCreation', { mandatory:"true"});
}

var pj=$rcmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsLocationGerance')) {
    attachment('pjLocationGerance', 'pjLocationGerance', { mandatory:"true"});
}

var pj=$rcmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsLocationGerance')) {
    attachment('pjLocationGerancePublication', 'pjLocationGerancePublication', { mandatory:"true"});
}

var pj=$rcmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsGeranceMandat')) {
    attachment('pjGeranceMandat', 'pjGeranceMandat', { mandatory:"true"});
}

var pj=$rcmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsGeranceMandat')) {
    attachment('pjGeranceMandatPublication', 'pjGeranceMandatPublication', { mandatory:"true"});
}

var pj=$rcmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds ;
if((Value('id').of(pj).contains('EtablissementOrigineFondsAchat')) and $rcmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds.precedentExploitant.cessionFonds) {
    attachment('pjPlanCession', 'pjPlanCession', { mandatory:"true"});
}

var pj=$rcmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds ;
if((Value('id').of(pj).contains('EtablissementOrigineFondsAchat')) and ($rcmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds.precedentExploitant.etablissementJournalAnnoncesLegalesNom != null)) {
    attachment('pjAchat', 'pjAchat', { mandatory:"true"});
}

var pj=$rcmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds ;
if((Value('id').of(pj).contains('EtablissementOrigineFondsAchat')) and ($rcmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds.precedentExploitant.etablissementJournalAnnoncesLegalesNom != null)) {
    attachment('pjAchatPublication', 'pjAchatPublication', { mandatory:"true"});
}


// SPI
var pj=$rcmb.cadre9modificationGroup.cadre9modification.objetModification ;
if(Value('id').of(pj).contains('modifActivite')) {
    attachment('pjSPI', 'pjSPI', { mandatory:"dalse"});
}