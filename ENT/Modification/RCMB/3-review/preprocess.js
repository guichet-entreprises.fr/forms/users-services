function pad(s) { return (s < 10) ? '0' + s : s; }
var formFields = {};

// Cadres 1 - Activité non salariée

var identite = $rcmb.cadre1IdentiteGroup.cadre1Identite;

formFields['personneliee_personnePhysique_SIREN']                                       = identite.personnelieePersonnePhysiqueSIREN != null ? identite.personnelieePersonnePhysiqueSIREN.split(' ').join('') : '';

// Cadres 2 - Identité

formFields['personneLiee_personnePhysique_nomNaissance']                                = identite.personneLieePersonnePhysiqueNomNaissance;
formFields['personneLiee_personnePhysique_nomUsage']                                    = identite.personneLieePersonnePhysiqueNomUsage;
formFields['personneLiee_personnePhysique_prenom1']                                     = identite.personneLieePersonnePhysiquePrenom1;
formFields['personneLiee_personnePhysique_pseudonyme']                                  = identite.personneLieePersonnePhysiquePseudonyme;
formFields['personneLiee_personnePhysique_nationalite']                                 = identite.personneLieePersonnePhysiqueNationalite;
formFields['personneLiee_personnePhysique_civiliteMasculin']                            = Value('id').of(identite.civilite).eq('PersonneLieePersonnePhysiqueCiviliteMasculin') ? true : false;
formFields['personneLiee_personnePhysique_civiliteFeminin']                             = Value('id').of(identite.civilite).eq('PersonneLieePersonnePhysiqueCiviliteFeminin') ? true : false;
if(identite.personneLieePersonnePhysiqueDateNaissance != null) {
	var dateTmp = new Date(parseInt(identite.personneLieePersonnePhysiqueDateNaissance.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['personneLiee_personnePhysique_dateNaissance'] = date;
}
formFields['personneLiee_personnePhysique_lieuNaissanceDepartement']                    = identite.personneLieePersonnePhysiqueLieuNaissanceDepartement;
formFields['personneLiee_personnePhysique_lieuNaissanceCommune']                        = identite.personneLieePersonnePhysiqueLieuNaissanceCommune + ' / ' + identite.personneLieePersonnePhysiqueLieuNaissancePays;
formFields['personneLiee_personnePhysique_mineurEmancipe']                              = identite.personneLieePersonnePhysiqueMineurEmancipe ? true : false;

var adresse = $rcmb.cadre1IdentiteGroup.cadre1AdresseDeclarant;

formFields['personneLiee_adresse_voie1']                                                = identite.etablissementNonSedentariteQualiteForain ? '' : (adresse.rueNumeroAdresseDeclarant != null ? adresse.rueNumeroAdresseDeclarant : '');
formFields['personneLiee_adresse_voie2']                                                = identite.etablissementNonSedentariteQualiteForain ? '' : (adresse.rueComplementAdresseDeclarant != null ? adresse.rueComplementAdresseDeclarant : '');
formFields['personneLiee_adresse_codePostal']                                           = identite.etablissementNonSedentariteQualiteForain ? '' : adresse.personneLieeAdresseCodePostal;
formFields['personneLiee_adresse_commune']                                              = identite.etablissementNonSedentariteQualiteForain ? '' : adresse.personneLieeAdresseCommune + ' / ' + adresse.paysAdresseDeclarant;
formFields['etablissement_nonSedentariteQualite_forain']                                = identite.etablissementNonSedentariteQualiteForain ? true : false;
formFields['personneLiee_adresse_codePostal_forain']                                    = identite.personneLieeAdresseCodePostalForain;
formFields['personneLiee_adresse_commune_forain']                                       = identite.personneLieeAdresseCommuneForain;

// Cadre 3 - Autre Etblissement UE

var accre = $rcmb.cadre3InformationsComplementairesGroup.cadre3InformationsComplementaires;
formFields['entreprise_autreEtablissementUE']                                           = accre.entrepriseAutreEtablissementUE ? true : false;

// Etablissements UE sur l'intercalaire P0'
formFields['etablissementUE1']                                                          = accre.entrepriseAutreEtablissementUE ? (accre.cadre3AdresseAutreEtablissementUE.rueNumeroAdresseAutreEtablissementUE + '  ' + (accre.cadre3AdresseAutreEtablissementUE.rueComplementAdresseAutreEtablissementUE != null ? accre.cadre3AdresseAutreEtablissementUE.rueComplementAdresseAutreEtablissementUE : '') + '  ' + (accre.cadre3AdresseAutreEtablissementUE.personneLieeAdresseAutreEtablissementUECodePostal != null ? accre.cadre3AdresseAutreEtablissementUE.personneLieeAdresseAutreEtablissementUECodePostal : '') + '  ' + accre.cadre3AdresseAutreEtablissementUE.personneLieeAdresseAutreEtablissementUECommune + ' /  ' + accre.cadre3AdresseAutreEtablissementUE.paysAdresseAutreEtablissementUE) : '';
formFields['etablissementUE2']                                                          = accre.cadre3AdresseAutreEtablissementUE.declarationAutreEtablissementUE ? (accre.cadre3AdresseAutreEtablissementUE2.rueNumeroAdresseAutreEtablissementUE2 + '  ' + (accre.cadre3AdresseAutreEtablissementUE2.rueComplementAdresseAutreEtablissementUE2 != null ? accre.cadre3AdresseAutreEtablissementUE2.rueComplementAdresseAutreEtablissementUE2 : '') + '  ' + (accre.cadre3AdresseAutreEtablissementUE2.personneLieeAdresseAutreEtablissementUECodePostal2 != null ? accre.cadre3AdresseAutreEtablissementUE2.personneLieeAdresseAutreEtablissementUECodePostal2 : '') + '  ' + accre.cadre3AdresseAutreEtablissementUE2.personneLieeAdresseAutreEtablissementUECommune2 + ' /  ' + accre.cadre3AdresseAutreEtablissementUE2.paysAdresseAutreEtablissementUE2) : '';

// Cadre 4 - Statut Conjoint

var conjoint = $rcmb.cadre2conjointGroup.cadre2Conjoint;

formFields['personneLiee_conjointCollaborateurSalarie_collaborateur']                   = Value('id').of(conjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieCollaborateur') ? true : false;
formFields['personneLiee_conjointCollaborateurSalarie_salarie']                         = Value('id').of(conjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieSalarie') ? true : false;

// Cadre 5 - Déclaration relative à l'insaisissabilité

var insaisissabilite = $rcmb.cadre3InformationsComplementairesGroup.cadre3InformationsComplementaires.cadre3DeclarationInsaisissabilite;

formFields['entreprise_insaisissabilitePublication']             						= insaisissabilite.entrepriseInsaisissabilitePublication != null ?  insaisissabilite.entrepriseInsaisissabilitePublication : '';

// Cadre 5 bis - EIRL

formFields['entreprise_eirlRegistreDepotRSEIRL']                                        = Value('id').of(accre.cadreEIRL.eirlRegistre).eq('entrepriseEirlRegistreDepotRSEIRL') ? true : false;
formFields['entreprise_eirlRegistreDepotRM']                                            = Value('id').of(accre.cadreEIRL.eirlRegistre).eq('entrepriseEirlRegistreDepotRM') ? true : false;
formFields['entreprise_eirlRegistreLieu']                                               = accre.cadreEIRL.entrepriseEirlRegistreLieu;

// Cadre 6 - Adresse de l'entreprise

var adressePro = $rcmb.cadre4AdresseActiviteGroup.cadre4AdresseActivite;

formFields['etablissement_domiciliation_non']                                           = Value('id').of(adressePro.activiteLieu).eq('etablissementDomiciliationNon') ? true : false;;
formFields['etablissement_domiciliation_oui']                                           = Value('id').of(adressePro.activiteLieu).eq('etablissementDomiciliationOui') ? true : false;
formFields['entreprise_adresseEntreprise_domicile']                                     = Value('id').of(adressePro.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? true : false;

//Cadre 7 - Adresse de l'établissement

formFields['entrepriseLiee_adresse_nomVoie']                                            = (adressePro.cadre4AdresseProfessionnelle.numRueAdresseActivite != null ? adressePro.cadre4AdresseProfessionnelle.numRueAdresseActivite : '')
																				            + ' ' + (adressePro.cadre4AdresseProfessionnelle.rueComplementAdresseActivite != null ? adressePro.cadre4AdresseProfessionnelle.rueComplementAdresseActivite : '');
formFields['entrepriseLiee_adresse_codePostal']                                         = adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseCodePostal;
formFields['entrepriseLiee_adresse_commune']                                            = adressePro.cadre4AdresseProfessionnelle.entrepriseLieeAdresseCommune;
formFields['entrepriseLiee_siren']                                                      = adressePro.entrepriseLieeSiren != null ? adressePro.entrepriseLieeSiren.split(' ').join('') : '';
formFields['entrepriseLiee_nom']                                                        = adressePro.entrepriseLieeNom;

//Cadre 6bis - Ambulant UE

var activite = $rcmb.cadre5EtablissementActiviteGroup.cadre5EtablissementActivite;

formFields['etablissement_estAmbulantUE']                                               = activite.etablissementEstAmbulantUE ? true : false;
formFields['personneLiee_adresse_codePostal_ambulantUE']                                = activite.personneLieeAdresseCodePostalAmbulantUE;
formFields['personneLiee_adresse_commune_ambulantUE']                                   = activite.personneLieeAdresseCommuneAmbulantUE;

// Cadre 8 - Nom commercial

formFields['etablissement_nomCommercialProfessionnel']                                  = activite.etablissementNomCommercialProfessionnel;
formFields['etablissement_enseigne']                                                    = activite.etablissementEnseigne;

// Cadre 9 - Activité

if(activite.etablissementDateDebutActivite != null) {
	var dateTmp = new Date(parseInt(activite.etablissementDateDebutActivite.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_dateDebutActivite'] = date;
}	
formFields['entreprise_activitePermanenteSaisonniere_permanente']                       = Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonnierePermanente') ? true : false;
formFields['entreprise_activitePermanenteSaisonniere_saisonniere']                      = Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonniereSaisonniere') ? true : false;
formFields['etablissment_nonSedentariteQualite_nonSedentaire']                          = activite.etablissmentNonSedentariteQualiteNonSedentaire ? true : false;
//formFields['etablissement_activites1']                                                = ' ';
formFields['etablissement_activites2']                    			                    = activite.etablissementActivites2;
formFields['etablissement_activitePlusImportante']                                      = activite.etablissementActivitePlusImportante;
formFields['etablissement_activiteLieuExercice_magasin']                                = activite.etablissementActiviteLieuExerciceMagasinSurface;
formFields['etablissement_activiteLieuExercice_marche']                                 = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMarche') ? true : false;
formFields['etablissement_activiteNature_commerceGros']                                 = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceGros') ? true : false;
formFields['etablissement_activiteNature_batTravauxPublics']                            = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureBatTravauxPublics') ? true : false;
formFields['etablissement_activiteLieuExerciceMagasin']                                 = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteLieuExerciceMagasin') ? true : false;
formFields['etablissement_activiteNature_commerceDetail']                               = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCommerceDetail') ? true : false;
formFields['etablissement_activiteNature_fabricationProduction']                        = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureFabricationProduction') ? true : false;
formFields['etablissement_activiteNature_cocheAutre']                                   = Value('id').of(activite.etablissementActiviteModeExercice).eq('EtablissementActiviteNatureCocheAutre') ? true : false;
formFields['etablissement_activiteNature_autre']                                        = activite.etablissementActiviteNatureAutre;

// Cadre 10 - Origine Fonds

var origine = $rcmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds;

formFields['etablissement_origineFonds_creation']                                       = Value('id').of($rcmb.cadre4AdresseActiviteGroup.cadre4AdresseActivite.activiteLieu).eq('entrepriseAdresseEntrepriseDomicile') ? true : (Value('id').of($rcmb.cadre4AdresseActiviteGroup.cadre4AdresseActivite.activiteLieu).eq('etablissementDomiciliationOui') ? true : (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation') ? true : false));
formFields['etablissement_origineFonds_locationGerance']                                = Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsLocationGerance') ? true : false;
formFields['etablissement_origineFonds_geranceMandat']                                  = Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsGeranceMandat') ? true : false;
formFields['etablissement_origineFonds_achat']                                          = Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsAchat') ? true : false;
formFields['etablissementOrigineFondsCocheAutre']                                       = Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCocheAutre') ? true : false;
formFields['etablissementOrigineFondsAutre']                                            = origine.etablissementOrigineFondsAutre;
formFields['entrepriseLiee_siren_precedentExploitant']                                  = origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant.split(' ').join('') : '';
formFields['entrepriseLiee_entreprisePP_nomNaissance_precedentExploitant']              = origine.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant : (origine.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant : '');
formFields['entrepriseLiee_entreprisePP_nomUsage_precedentExploitant']                  = origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant;
formFields['entrepriseLiee_entreprisePP_prenom1_precedentExploitant']                   = origine.precedentExploitant.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant;
if(origine.geranceMandat.etablissementLoueurMandantDuFondsDebutContrat != null) {
	var dateTmp = new Date(parseInt(origine.geranceMandat.etablissementLoueurMandantDuFondsDebutContrat.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_loueurMandantDuFondsDebutContrat'] = date;
}
if(origine.geranceMandat.etablissementLoueurMandantDuFondsFinContrat != null) {
	var dateTmp = new Date(parseInt(origine.geranceMandat.etablissementLoueurMandantDuFondsFinContrat.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_loueurMandantDuFondsFinContrat'] = date;
}
formFields['etablissement_loueurMandantDuFondsRenouvellementTaciteReconduction_oui']    = (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsLocationGerance') or  Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsGeranceMandat')) ? (origine.geranceMandat.etablissementLoueurMandantDuFondsRenouvellementTaciteReconductionOui ? true : false) : false;
formFields['etablissement_loueurMandantDuFondsRenouvellementTaciteReconduction_non']    = (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsLocationGerance') or  Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsGeranceMandat')) ? (origine.geranceMandat.etablissementLoueurMandantDuFondsRenouvellementTaciteReconductionOui ? false : true) : false;
formFields['entrepriseLiee_entreprisePM_nomNaissance_loueurMandantDuFonds']             = origine.geranceMandat.entrepriseLieeEntreprisePMDenominationLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeEntreprisePMDenominationLoueurMandantDuFonds : (origine.geranceMandat.entrepriseLieeEntreprisePMNomNaissanceLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeEntreprisePMNomNaissanceLoueurMandantDuFonds : '');
formFields['entrepriseLiee_entreprisePP_nomUsage_loueurMandantDuFonds']                 = origine.geranceMandat.entrepriseLieeEntreprisePPNomUsageLoueurMandantDuFonds;
formFields['entrepriseLiee_entreprisePP_prenom1_loueurMandantDuFonds']                  = origine.geranceMandat.entrepriseLieeEntreprisePPPrenom1LoueurMandantDuFonds;
formFields['entrepriseLiee_adresse_nomVoie_loueurMandantDuFonds']                          = (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.numRueAdresseLoueurMandantDufonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.numRueAdresseLoueurMandantDufonds : '')
																				            + ' ' + (origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.rueComplementAdresseLoueurMandantDuFonds != null ? origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.rueComplementAdresseLoueurMandantDuFonds : '');
formFields['entrepriseLiee_adresse_codePostal_loueurMandantDuFonds']                    = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.entrepriseLieeAdresseCodePostalLoueurMandantDuFonds;
formFields['entrepriseLiee_adresse_commune_loueurMandantDuFonds']                       = origine.geranceMandat.entrepriseLieeAdresseMandantDuFonds.entrepriseLieeAdresseCommuneLoueurMandantDuFonds;
formFields['entrepriseLiee_siren_geranceMandat']                                        = origine.geranceMandat.entrepriseLieeSirenGeranceMandat != null ? origine.geranceMandat.entrepriseLieeSirenGeranceMandat.split(' ').join('') : '';
formFields['entrepriseLiee_greffeImmatriculation']                                      = origine.geranceMandat.entrepriseLieeGreffeImmatriculation;
if(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParution != null) {
	var dateTmp = new Date(parseInt(origine.precedentExploitant.etablissementJournalAnnoncesLegalesDateParution.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['etablissement_journalAnnoncesLegalesDateParution'] = date;
}
formFields['etablissement_journalAnnoncesLegalesNom']                                   = origine.precedentExploitant.etablissementJournalAnnoncesLegalesNom;

// Cadre 11 - Conjoint collaborateur

var conjointInfos = $rcmb.cadre2conjointGroup.cadre2Conjoint.cadre2InfosConjoint;

formFields['personneLiee_conjointCollaborateurSalarie_collaborateur_1']                 = Value('id').of($rcmb.cadre2conjointGroup.cadre2Conjoint.statutConjointCollaborateur).eq('PersonneLieeConjointCollaborateurSalarieCollaborateur') ? true : false;
formFields['personneLiee_personnePhysique_nomNaissance_conjointPacse']                  = conjointInfos.personneLieePersonnePhysiqueNomNaissanceConjointPacse;
formFields['personneLiee_personnePhysique_nomUsage_conjointPacse']                      = conjointInfos.personneLieePersonnePhysiqueNomUsageConjointPacse;
formFields['personneLiee_personnePhysique_prenom1_conjointPacse']                       = conjointInfos.personneLieePersonnePhysiquePrenom1ConjointPacse;
formFields['personneLiee_personnePhysique_nationalite_conjointPacse']                   = conjointInfos.personneLieePersonnePhysiqueNationaliteConjointPacse;
if(conjointInfos.personneLieePersonnePhysiqueDateNaissanceConjointPacse != null) {
	var dateTmp = new Date(parseInt(conjointInfos.personneLieePersonnePhysiqueDateNaissanceConjointPacse.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['personneLiee_personnePhysique_dateNaissance_conjointPacse'] = date;
}
formFields['personneLiee_personnePhysique_lieuNaissanceDepartement_conjointPacse']      = conjointInfos.personneLieePersonnePhysiqueLieuNaissanceDepartementConjointPacse;
formFields['personneLiee_personnePhysique_lieuNaissanceCommune_conjointPacse']          = conjointInfos.personneLieePersonnePhysiqueLieuNaissanceCommuneConjointPacse != null ? (conjointInfos.personneLieePersonnePhysiqueLieuNaissanceCommuneConjointPacse + ' / ' + conjointInfos.personneLieePersonnePhysiquePaysNaissanceConjointPacse) : '';

// Cadre 12 - Personne pouvoir

var personnePouvoir = $rcmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir;

formFields['personneLiee_personneLieeEtablissementQualite_pouvoirEngagerEtablissement'] = Value('id').of($rcmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement');
formFields['personneLiee_personneLieeEtablissementQualite_proprietaireIndivis']         = Value('id').of($rcmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.personneLieePersonneLieeEtablissementQualite).eq('PersonneLieePersonneLieeEtablissementQualiteProprietaireIndivis');
formFields['personneLiee_personnePhysique_nomNaissance_personnePouvoir']                = personnePouvoir.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir;
formFields['personneLiee_personnePhysique_nomUsage_personnePouvoir']                    = personnePouvoir.personneLieePersonnePhysiqueNomUsagePersonnePouvoir;
formFields['personneLiee_personnePhysique_prenom1_personnePouvoir']                     = personnePouvoir.personneLieePersonnePhysiquePrenom1PersonnePouvoir;
formFields['personneLiee_adresse_nomVoie_personnePouvoir']                                 = (personnePouvoir.cadre6AdressePersonnePouvoir.rueNumeroAdressePersonnePouvoir != null ? personnePouvoir.cadre6AdressePersonnePouvoir.rueNumeroAdressePersonnePouvoir : '')
																				            + ' ' + (personnePouvoir.cadre6AdressePersonnePouvoir.rueComplementAdressePersonnePouvoir != null ? personnePouvoir.cadre6AdressePersonnePouvoir.rueComplementAdressePersonnePouvoir : '');
formFields['personneLiee_adresse_codePostal_personnePouvoir']                           = personnePouvoir.cadre6AdressePersonnePouvoir.personneLieeAdresseCodePostalPersonnePouvoir;
formFields['personneLiee_adresse_commune_personnePouvoir']                               = personnePouvoir.cadre6AdressePersonnePouvoir.personneLieeAdresseCommunepersonnePouvoir;
if(personnePouvoir.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir != null) {
	var dateTmp = new Date(parseInt(personnePouvoir.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['personneLiee_personnePhysique_dateNaissance_personnePouvoir'] = date;
}
formFields['personneLiee_personnePhysique_lieuNaissanceDepartement_personnePouvoir']    = personnePouvoir.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir;
formFields['personneLiee_personnePhysique_lieuNaissanceCommune_personnePouvoir']        = personnePouvoir.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir != null ? (personnePouvoir.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir + ' / ' + personnePouvoir.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir) : '';
formFields['personneLiee_personnePhysique_nationalite_personnePouvoir']                 = personnePouvoir.personneLieePersonnePhysiqueNationalitePersonnePouvoir;

// Deuxième fondé de pouvoir à indiquer sur le PO' intercalaire

var personnePouvoir2 = $rcmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir2;

formFields['personneLiee_personneLieeEtablissementQualite_pouvoirEngagerEtablissement2'] = Value('id').of(personnePouvoir2.personneLieePersonneLieeEtablissementQualite2).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement2');
formFields['personneLiee_personneLieeEtablissementQualite_proprietaireIndivis2']         = Value('id').of(personnePouvoir2.personneLieePersonneLieeEtablissementQualite2).eq('PersonneLieePersonneLieeEtablissementQualiteProprietaireIndivis2');
formFields['personneLiee_personnePhysique_nomNaissance_personnePouvoir2']                = personnePouvoir2.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir2;
formFields['personneLiee_personnePhysique_nomUsage_personnePouvoir2']                    = personnePouvoir2.personneLieePersonnePhysiqueNomUsagePersonnePouvoir2;
formFields['personneLiee_personnePhysique_prenom1_personnePouvoir2']                     = personnePouvoir2.personneLieePersonnePhysiquePrenom1PersonnePouvoir2;
formFields['personneLiee_adresse_nomVoie_personnePouvoir2']                              = (personnePouvoir2.cadre6AdressePersonnePouvoir2.rueNumeroAdressePersonnePouvoir != null ? personnePouvoir2.cadre6AdressePersonnePouvoir2.rueNumeroAdressePersonnePouvoir : '')
																				            + ' ' + (personnePouvoir2.cadre6AdressePersonnePouvoir2.rueComplementAdressePersonnePouvoir != null ? personnePouvoir2.cadre6AdressePersonnePouvoir2.rueComplementAdressePersonnePouvoir : '');
formFields['personneLiee_adresse_codePostal_personnePouvoir2']                           = personnePouvoir2.cadre6AdressePersonnePouvoir2.personneLieeAdresseCodePostalPersonnePouvoir;
formFields['personneLiee_adresse_commune_personnePouvoir2']                              = personnePouvoir2.cadre6AdressePersonnePouvoir2.personneLieeAdresseCommunepersonnePouvoir;
if(personnePouvoir2.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir2 != null) {
	var dateTmp = new Date(parseInt(personnePouvoir2.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir2.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['personneLiee_personnePhysique_dateNaissance_personnePouvoir2'] = date;
}
formFields['personneLiee_personnePhysique_lieuNaissanceDepartement_personnePouvoir2']    = personnePouvoir2.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir2;
formFields['personneLiee_personnePhysique_lieuNaissanceCommune_personnePouvoir2']        = personnePouvoir2.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir2 != null ? (personnePouvoir2.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir2 + ' / ' + personnePouvoir2.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir2) : '';
formFields['personneLiee_personnePhysique_nationalite_personnePouvoir2']                 = personnePouvoir2.personneLieePersonnePhysiqueNationalitePersonnePouvoir2;

// Troisième fondé de pouvoir

var personnePouvoir3 = $rcmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir3;

formFields['personneLiee_personneLieeEtablissementQualite_pouvoirEngagerEtablissement3'] = Value('id').of(personnePouvoir3.personneLieePersonneLieeEtablissementQualite3).eq('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement3');
formFields['personneLiee_personneLieeEtablissementQualite_proprietaireIndivis3']         = Value('id').of(personnePouvoir3.personneLieePersonneLieeEtablissementQualite3).eq('PersonneLieePersonneLieeEtablissementQualiteProprietaireIndivis3');
formFields['personneLiee_personnePhysique_nomNaissance_personnePouvoir3']                = personnePouvoir3.personneLieePersonnePhysiqueNomNaissancePersonnePouvoir3;
formFields['personneLiee_personnePhysique_nomUsage_personnePouvoir3']                    = personnePouvoir3.personneLieePersonnePhysiqueNomUsagePersonnePouvoir3;
formFields['personneLiee_personnePhysique_prenom1_personnePouvoir3']                     = personnePouvoir3.personneLieePersonnePhysiquePrenom1PersonnePouvoir3;
formFields['personneLiee_adresse_nomVoie_personnePouvoir3']                              = (personnePouvoir3.cadre6AdressePersonnePouvoir3.rueNumeroAdressePersonnePouvoir != null ? personnePouvoir3.cadre6AdressePersonnePouvoir3.rueNumeroAdressePersonnePouvoir : '')
																				            + ' ' + (personnePouvoir3.cadre6AdressePersonnePouvoir3.rueComplementAdressePersonnePouvoir != null ? personnePouvoir3.cadre6AdressePersonnePouvoir3.rueComplementAdressePersonnePouvoir : '');
formFields['personneLiee_adresse_codePostal_personnePouvoir3']                           = personnePouvoir3.cadre6AdressePersonnePouvoir3.personneLieeAdresseCodePostalPersonnePouvoir;
formFields['personneLiee_adresse_commune_personnePouvoir3']                              = personnePouvoir3.cadre6AdressePersonnePouvoir3.personneLieeAdresseCommunepersonnePouvoir;
if(personnePouvoir3.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir3 != null) {
	var dateTmp = new Date(parseInt(personnePouvoir3.personneLieePersonnePhysiqueDateNaissancePersonnePouvoir3.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['personneLiee_personnePhysique_dateNaissance_personnePouvoir3'] = date;
}
formFields['personneLiee_personnePhysique_lieuNaissanceDepartement_personnePouvoir3']    = personnePouvoir3.personneLieePersonnePhysiqueLieuNaissanceDepartementPersonnePouvoir3;
formFields['personneLiee_personnePhysique_lieuNaissanceCommune_personnePouvoir3']        = personnePouvoir3.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir3 != null ? (personnePouvoir3.personneLieePersonnePhysiqueLieuNaissanceCommunePersonnePouvoir3 + ' / ' + personnePouvoir3.personneLieePersonnePhysiquePaysNaissancePersonnePouvoir3) : '';
formFields['personneLiee_personnePhysique_nationalite_personnePouvoir3']                 = personnePouvoir3.personneLieePersonnePhysiqueNationalitePersonnePouvoir3;

// Cadre 13 - Ancienne Identité

var modification = $rcmb.cadre9modificationGroup

if(modification.cadre9ModificationSituationPersonnelle.modifDateIdentite != null) {
	var dateTmp = new Date(parseInt(modification.cadre9ModificationSituationPersonnelle.modifDateIdentite.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['modifDateIdentite'] = date;
}
formFields['modifAncienNomNaissance']                                                   = (Value('id').of(modification.cadre9modification.objetModification).contains('10P') or Value('id').of(modification.cadre9modification.objetModification).contains('15P')) ? (Value('id').of(modification.cadre9ModificationSituationPersonnelle.modifNomPrenom).contains('modifNomNaissance') ? modification.cadre9ModificationSituationPersonnelle.modifAncienNomNaissance : identite.personneLieePersonnePhysiqueNomNaissance) : '';
formFields['modifAncienNomUsage']                                                       = (Value('id').of(modification.cadre9modification.objetModification).contains('10P') or Value('id').of(modification.cadre9modification.objetModification).contains('15P')) ? (Value('id').of(modification.cadre9modification.objetModification).contains('15P') ? modification.cadre9ModificationSituationPersonnelle.modifAncienNomUsage : identite.personneLieePersonnePhysiqueNomUsage) : '';
formFields['modifAncienPrenoms']                                                        = (Value('id').of(modification.cadre9modification.objetModification).contains('10P') or Value('id').of(modification.cadre9modification.objetModification).contains('15P')) ? (Value('id').of(modification.cadre9ModificationSituationPersonnelle.modifNomPrenom).contains('modifPrenom') ? modification.cadre9ModificationSituationPersonnelle.modifAncienPrenoms : identite.personneLieePersonnePhysiquePrenom1) : '';
formFields['modifAncienneNationalite']                                                  = Value('id').of(modification.cadre9modification.objetModification).contains('17P') ? (modification.cadre9ModificationSituationPersonnelle.modifAncienneNationalite) : '';
formFields['modifAncienDomicile_adresse']												= Value('id').of(modification.cadre9modification.objetModification).contains('16P') ? ((modification.cadre9ModificationSituationPersonnelle.ancienneAdresseDomicile.modifAncienDomicileAdresse != null ? modification.cadre9ModificationSituationPersonnelle.ancienneAdresseDomicile.modifAncienDomicileAdresse : '') + ' ' + (modification.cadre9ModificationSituationPersonnelle.ancienneAdresseDomicile.modifAncienDomicileAdresseComplement != null ? modification.cadre9ModificationSituationPersonnelle.ancienneAdresseDomicile.modifAncienDomicileAdresseComplement : '')) : '';
formFields['modifAncienDomicile_codePostal']                                            = Value('id').of(modification.cadre9modification.objetModification).contains('16P') ? (modification.cadre9ModificationSituationPersonnelle.ancienneAdresseDomicile.modifAncienDomicileCodePostal != null ? modification.cadre9ModificationSituationPersonnelle.ancienneAdresseDomicile.modifAncienDomicileCodePostal : '') : '';
formFields['modifAncienDomicile_commune']                                               = Value('id').of(modification.cadre9modification.objetModification).contains('16P') ? (modification.cadre9ModificationSituationPersonnelle.ancienneAdresseDomicile.modifAncienDomicileCommune != null ? modification.cadre9ModificationSituationPersonnelle.ancienneAdresseDomicile.modifAncienDomicileCommune : '') : '';

// Cadre 14 - Ancienne adresse entreprise

if(modification.cadre9ModificationAdresseEntreprise.modifDateAdresseEntreprise != null) {
	var dateTmp = new Date(parseInt(modification.cadre9ModificationAdresseEntreprise.modifDateAdresseEntreprise.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['modifDateAdresseEntreprise'] = date;
}
formFields['modifAncienneAdresseEntreprise_voie']                                       = Value('id').of(modification.cadre9modification.objetModification).contains('11P') ? ((modification.cadre9ModificationAdresseEntreprise.ancienneAdresseEntreprise.modifAncienneAdresseEntrepriseVoie != null ? modification.cadre9ModificationAdresseEntreprise.ancienneAdresseEntreprise.modifAncienneAdresseEntrepriseVoie : '') + ' ' + (modification.cadre9ModificationAdresseEntreprise.ancienneAdresseEntreprise.modifAncienneAdresseEntrepriseComplement != null ? modification.cadre9ModificationAdresseEntreprise.ancienneAdresseEntreprise.modifAncienneAdresseEntrepriseComplement : '')) : '';
formFields['modifAncienneAdresseEntreprise_codePostal']                                 = Value('id').of(modification.cadre9modification.objetModification).contains('11P') ? (modification.cadre9ModificationAdresseEntreprise.ancienneAdresseEntreprise.modifAncienneAdresseEntrepriseCodePostal != null ? modification.cadre9ModificationAdresseEntreprise.ancienneAdresseEntreprise.modifAncienneAdresseEntrepriseCodePostal : '') : '';
formFields['modifAncienneAdresseEntreprise_commune']                                    = Value('id').of(modification.cadre9modification.objetModification).contains('11P') ? (modification.cadre9ModificationAdresseEntreprise.ancienneAdresseEntreprise.modifAncienneAdresseEntrepriseCommune != null ? modification.cadre9ModificationAdresseEntreprise.ancienneAdresseEntreprise.modifAncienneAdresseEntrepriseCommune : '') : '';

// Cadre 15 - Ancienne activité

if(modification.cadre9ModificationActivite.modifDateActivite != null) {
	var dateTmp = new Date(parseInt(modification.cadre9ModificationActivite.modifDateActivite.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['modifDateActivite'] = date;
}
formFields['modifAncienneActivite']                                                     = modification.cadre9ModificationActivite.modifAncienneActivite;

//Cadre 16 - Observations

var correspondance = $rcmb.cadre10RensCompGroup.cadre10RensComp ;

formFields['formalite_observations']                                                    = correspondance.formaliteObservations;

// Cadre 17 - Adresse de correspondance

formFields['adresseCorrespondanceDeclaree']                                             = (Value('id').of(correspondance.adresseCorrespond).eq('domi') or Value('id').of(correspondance.adresseCorrespond).eq('prof')) ? true : false;
formFields['adresseDeclareeCadre']                                                      = Value('id').of(correspondance.adresseCorrespond).eq('domi') ? "2" : (Value('id').of(correspondance.adresseCorrespond).eq('prof') ? "7" : ' ');
formFields['formalite_correspondanceAdresse']                                           = Value('id').of(correspondance.adresseCorrespond).eq('autre') ? true : false;
formFields['formalite_correspondanceAdresse_numeroVoie']                                = correspondance.adresseCorrespondance.numeroRueAdresseCorrespondance;
formFields['formalite_correspondanceAdresse_complementVoie']                            = correspondance.adresseCorrespondance.rueComplementAdresseCorrespondance;
formFields['formalite_correspondanceAdresse_codePostal']                                = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal;
formFields['formalite_correspondanceAdresse_commune']									= correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune;
formFields['formalite_telephone1']                                                      = correspondance.infosSup.formaliteTelephone1;
formFields['formalite_telephone2']                                                      = correspondance.infosSup.formaliteTelephone2;
formFields['formalite_fax_courriel']                                                    = correspondance.infosSup.formaliteFaxCourriel != null ? correspondance.infosSup.formaliteFaxCourriel : correspondance.infosSup.telecopie;

// Cadres 18 - Signature

var signataire = $rcmb.cadre11SignatureGroup.cadre11Signature ;

formFields['formalite_signataireQualiteDeclarant']                                      = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteDeclarant') ? true : false;
formFields['formalite_signataireQualiteMandataire']                                     = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? true : false;
formFields['formalite_signataireNom']											        = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? signataire.adresseMandataire.nomPrenomDenominationMandataire : '';	
formFields['formalite_adresseSignature']                                                = Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? (signataire.adresseMandataire.numeroNomRueMandataire + ' ' + signataire.adresseMandataire.dataCodePostalMandataire + ' ' + signataire.adresseMandataire.villeAdresseMandataire) : '';
formFields['formalite_signatureLieu']                                                   = signataire.formaliteSignatureLieu;
formFields['formalite_signatureDate']                                                   = signataire.formaliteSignatureDate;
// A conditionner selon l'activité
formFields['intercalaireNombre']                                                        = (accre.entrepriseAutreEtablissementUE or personnePouvoir.autreDeclarationPersonneLiee) ? "1" : "0";
formFields['signature']                                                                 = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce";
formFields['formulaireNdiOui']                                                          = false;
formFields['formulaireNdiNon']                                                          = true;

//P0' Intercalaire

formFields['complete_p0pl']                                                              = false;
formFields['complete_aco']                                                               = false;
formFields['complete_p0agr']                                                             = false;
formFields['complete_p0cmb']                                                             = false;
formFields['personneLiee_personnePhysique_nomNaissance_prenom']                          = identite.personneLieePersonnePhysiqueNomNaissance + ' ' + identite.personneLieePersonnePhysiquePrenom1;
formFields['personneLiee_personnePhysique_dateNaissance_P0Prime']                        = identite.personneLieePersonnePhysiqueDateNaissance;
formFields['numero_intercalaire']                                                        = "1";
formFields['complete_cadre']                                                             = '';
formFields['entrepriseLieeContratAppui']                                                 = '';
formFields['complete_cerfa']                                                             = '';

/*
 * Création du dossier avec volet social : ajout du cerfa avec volet social
 */
var cerfaDoc1 = nash.doc //
	.load('models/cerfa_Rcmb_avec_volet_social.pdf') //
	.apply(formFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));

/*
 * Création du dossier sans volet social : ajout du cerfa sans volet social
 */
 
 var cerfaDoc2 = nash.doc //
	.load('models/cerfa_Rcmb_sans_volet_social.pdf') //
	.apply (formFields);
	cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));


/*
 * Ajout de l'intercalaire P0 Prime avec volet social
 */
 
 if ((accre.entrepriseAutreEtablissementUE == true) or (personnePouvoir.autreDeclarationPersonneLiee == true))
{
	var p0PrimeDoc1 = nash.doc //
		.load('models/cerfa_11771-02_P0_Prime_avec_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(p0PrimeDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire P0 Prime sans volet social
 */
 
 if ((accre.entrepriseAutreEtablissementUE == true) or (personnePouvoir.autreDeclarationPersonneLiee == true))
{
	var p0PrimeDoc2 = nash.doc //
		.load('models/cerfa_11771-02_P0_Prime_sans_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(p0PrimeDoc2.save('cerfa.pdf'));
}

function appendPj(fld) {
	fld.forEach(function (elm) {
        cerfaDoc1.append(elm);
    });
}

/*
 * Ajout des PJs
 */

// PJ Déclarant

appendPj($attachmentPreprocess.attachmentPreprocess.pjDNCDeclarant);

var pj=$rcmb.cadre11SignatureGroup.cadre11Signature.soussigne;

if(Value('id').of(pj).contains('FormaliteSignataireQualiteDeclarant')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantSignataire);
}

if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantNonSignataire);
} 

// PJ Mandataire

if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjPouvoir);
}

if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire);
}

// PJ Conjoint

var pj=$rcmb.cadre2conjointGroup.cadre2Conjoint.statutConjointCollaborateur ;
if(Value('id').of(pj).contains('PersonneLieeConjointCollaborateurSalarieCollaborateur')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjIDConjoint);
}

var pj=$rcmb.cadre2conjointGroup.cadre2Conjoint ;
if(pj.conjointCommunauteBiens and pj.conjoint) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjInformationConjoint);
}

// PJ Fondé de pouvoir 1

var pj=$rcmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.personneLieePersonneLieeEtablissementQualite ;
if(Value('id').of(pj).contains('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement') and $rcmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.personneLieePersonneLieeEtablissement) {
    appendPj($attachmentPreprocess.attachmentPreprocess.pjDNCPersonnePouvoir);
	appendPj($attachmentPreprocess.attachmentPreprocess.pjIDPersonnePouvoir);
}

// PJ Fondé de pouvoir 2

var pj=$rcmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir2.personneLieePersonneLieeEtablissementQualite2 ;
if(Value('id').of(pj).contains('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement2') and $rcmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir.autreDeclarationPersonneLiee) {
    appendPj($attachmentPreprocess.attachmentPreprocess.pjDNCPersonnePouvoir2);
	appendPj($attachmentPreprocess.attachmentPreprocess.pjIDPersonnePouvoir2);
}

// PJ Fondé de pouvoir 3

var pj=$rcmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir3.personneLieePersonneLieeEtablissementQualite3 ;
if(Value('id').of(pj).contains('PersonneLieePersonneLieeEtablissementQualitePouvoirEngagerEtablissement3') and $rcmb.cadre6PeronnePouvoirGroup.cadre6PeronnePouvoir.infoPeronnePouvoir2.autreDeclarationPersonneLiee2) {
    appendPj($attachmentPreprocess.attachmentPreprocess.pjDNCPersonnePouvoir3);
	appendPj($attachmentPreprocess.attachmentPreprocess.pjIDPersonnePouvoir3);
}

// PJ mineur émancipé

var pj=$rcmb.cadre1IdentiteGroup.cadre1Identite ;
if(pj.personneLieePersonnePhysiqueMineurEmancipe) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjIDMineurEmancipe);
}

// PJ Insaisissabilité

var pj=$rcmb.cadre3InformationsComplementairesGroup.cadre3InformationsComplementaires ;
if(pj.cadre3DeclarationInsaisissabilite.entrepriseInsaisissabilitePublicationDeclarationAutresBiens) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjInsaisissabilite);
}

// PJ Etablissement

var pj=$rcmb.cadre4AdresseActiviteGroup.cadre4AdresseActivite.activiteLieu ;
if(Value('id').of(pj).contains('entrepriseAdresseEntrepriseDomicile')) {
    appendPj($attachmentPreprocess.attachmentPreprocess.pjDomicile);
}

var pj=$rcmb.cadre4AdresseActiviteGroup.cadre4AdresseActivite.activiteLieu ;
if(Value('id').of(pj).contains('etablissementDomiciliationOui')) {
    appendPj($attachmentPreprocess.attachmentPreprocess.pjContratDomiciliation);
}

var pj=$rcmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsCreation') or ((Value('id').of(pj).contains('EtablissementOrigineFondsAchat')) and $rcmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds.precedentExploitant.fondsArtisanal) or (Value('id').of(pj).contains('EtablissementOrigineFondsCocheAutre'))) {
    appendPj($attachmentPreprocess.attachmentPreprocess.pjEtablissementCreation);
}

var pj=$rcmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsLocationGerance')) {
    appendPj($attachmentPreprocess.attachmentPreprocess.pjLocationGerance);
	appendPj($attachmentPreprocess.attachmentPreprocess.pjLocationGerancePublication);
}

var pj=$rcmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds ;
if(Value('id').of(pj).contains('EtablissementOrigineFondsGeranceMandat')) {
    appendPj($attachmentPreprocess.attachmentPreprocess.pjGeranceMandat);
	appendPj($attachmentPreprocess.attachmentPreprocess.pjGeranceMandatPublication);
}

var pj=$rcmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds ;
if((Value('id').of(pj).contains('EtablissementOrigineFondsAchat')) and $rcmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds.precedentExploitant.cessionFonds) {
    appendPj($attachmentPreprocess.attachmentPreprocess.pjPlanCession);
}

var pj=$rcmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds.etablisementOrigineFonds ;
if((Value('id').of(pj).contains('EtablissementOrigineFondsAchat')) and ($rcmb.cadre4AdresseActiviteGroup.cadre4OrigineFonds.precedentExploitant.etablissementJournalAnnoncesLegalesNom != null)) {
    appendPj($attachmentPreprocess.attachmentPreprocess.pjAchat);
	appendPj($attachmentPreprocess.attachmentPreprocess.pjAchatPublication);
}

var pj=$rcmb.cadre9modificationGroup.cadre9modification.objetModification ;
if(Value('id').of(pj).contains('modifActivite')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjSPI);
}


/*
 * Enregistrement du fichier (en mémoire)
 */

var finalDoc = cerfaDoc1.save('PE_RCMB.pdf');

var data = [ spec.createData({
    id : 'record',
    label : 'Demande d\'immatriculation au RCS, RM ou REB d\'une personne exerçant déjà une activité commerciale et/ou artisanale et relevant du régime micro-social simplifié',
    description : 'Voici le formulaire obtenu à partir des données saisies.',
    type : 'FileReadOnly',
    value : [ finalDoc ]
}) ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Demande d\'immatriculation au RCS, RM ou REB d\'une personne exerçant déjà une activité commerciale et/ou artisanale et relevant du régime micro-social simplifié',
    groups : groups
});