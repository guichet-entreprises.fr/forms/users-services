var formFields = {};
var cerfaFields = {};
function pad(s) { return (s < 10) ? '0' + s : s; }

// Cadre 1 - Objet de la formalité

var objet= $p2pl.cadre1ObjetModificationGroup.cadre1ObjetModification;
var identite= $p2pl.cadre1RappelIdentificationGroup.cadre1RappelIdentification;

formFields['modifSituationpersonnelle']                                  = Value('id').of(objet.objetModification).contains('situationPerso') ? true : false;
formFields['modifEtablissement']                                         = (Value('id').of(objet.objetModification).contains('etablissement') and not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')) ? true : false;
formFields['modifTransfert']                                             = ((Value('id').of(objet.objetModification).contains('etablissement') and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')) or objet.domicileEntreprise) ? true : false;;
formFields['modifAutre']                                                 = (Value('id').of(objet.objetModification).contains('modifEIRL') or Value('id').of(objet.objetModification).contains('dateActivite')) ? true : false;
formFields['p2_professionLiberale']                                      = Value('id').of(identite.objetP2).eq('professionLiberale') ? true : false;
formFields['p2_artisteAuteur']                                           = Value('id').of(identite.objetP2).eq('artisteAuteur') ? true : false;

// Cadre 2 - Rappel d'identification

formFields['siren']                                                  = identite.siren.split(' ').join('');
formFields['microentrepreneurOui']                                   = identite.microEntrepreneurOuiNon ? true : false;
formFields['microentrepreneurNon']                                   = identite.microEntrepreneurOuiNon ? false : true;
var nirDeclarant = identite.voletSocialNumeroSecuriteSociale;
if(nirDeclarant != null) {
    nirDeclarant = nirDeclarant.replace(/ /g, "");
    formFields['numeroSS']                = nirDeclarant.substring(0, 13);
    formFields['numeroSSCle']             = nirDeclarant.substring(13, 15);
}

// Cadre 3A 

var modifIdentiteDeclarant = $p2pl.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle;

if (modifIdentiteDeclarant.modifIdentite.modifDateIdentite !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifIdentite.modifDateIdentite.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateIdentite']          = date;
}

formFields['personneLiee_personnePhysique_nomNaissance']             = (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P') and Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifNomNaissance')) ? modifIdentiteDeclarant.modifIdentite.modifNouveauNomNaissance : identite.personneLieePersonnePhysiqueNomNaissance;
formFields['personneLiee_personnePhysique_nomUsage']                 = (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P') and Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPseudonyme).contains('modifNomUsage')) ? (modifIdentiteDeclarant.modifIdentite.modifNouveauNomUsage != null ? modifIdentiteDeclarant.modifIdentite.modifNouveauNomUsage : '') : (identite.personneLieePersonnePhysiqueNomUsage != null ? identite.personneLieePersonnePhysiqueNomUsage : '');

if (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom')) {	
var prenoms=[];
for ( i = 0; i < modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms.size() ; i++ ){prenoms.push(modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms[i]);}                            
formFields['personneLiee_personnePhysique_prenom1']                                      = prenoms.toString();
} else if (not Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom')) {
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFields['personneLiee_personnePhysique_prenom1']                                      = prenoms.toString();
}

// Cadres 3B

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P') 
	or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) {
formFields['modifAncienNomNaissance']                                    = identite.personneLieePersonnePhysiqueNomNaissance;
formFields['modifAncienNomUsage']                                        = identite.personneLieePersonnePhysiqueNomUsage != null ? identite.personneLieePersonnePhysiqueNomUsage : '';
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFields['modifAncienPrenoms']                                      = prenoms.toString();
}

// Cadre 4

if (identite.personneLieePersonnePhysiqueDateNaissance !== null) {
    var dateTmp = new Date(parseInt(identite.personneLieePersonnePhysiqueDateNaissance.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['personneLiee_personnePhysique_dateNaissance']          = date;
}
formFields['personneLiee_personnePhysique_lieuNaissanceDepartement'] = identite.personneLieePPLieuNaissanceDepartement != null ? identite.personneLieePPLieuNaissanceDepartement.getId() : '';
formFields['personneLiee_personnePhysique_lieuNaissanceCommune']     = identite.personneLieePPLieuNaissanceCommune != null ?  identite.personneLieePPLieuNaissanceCommune : identite.personneLieePPLieuNaissanceVille
formFields['personneLiee_personnePhysique_lieuNaissancePays']        = identite.personneLieePPLieuNaissancePays;

// Cadre 5 - Déclaration relative à la modification de la situation personnelle

// Modification du domicile

var etablissement = $p2pl.cadre6EtablissementGroup.infoEtablissementOld;
var newAdresseDom = $p2pl.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifDomicile.newAdresseDomicile;

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P')) {
	if (modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
		var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
		var date = pad(dateTmp.getDate().toString());
		var month = dateTmp.getMonth() + 1;
		date = date.concat(pad(month.toString()));
		date = date.concat(dateTmp.getFullYear().toString());
		formFields['modifDateDomicile']          = date;
	}
	formFields['personneLiee_adresse_voie']                             = (newAdresseDom.personneLieeAdresseVoieNew != null ? newAdresseDom.personneLieeAdresseVoieNew : '') 
																		+ ' ' + (newAdresseDom.personneLieeAdresseIndiceVoieNew != null ? newAdresseDom.personneLieeAdresseIndiceVoieNew : '')
																		+ ' ' + (newAdresseDom.personneLieeAdresseTypeVoieNew != null ? newAdresseDom.personneLieeAdresseTypeVoieNew : '')
																		+ ' ' + (newAdresseDom.personneLieeAdresseNomVoieNew != null ? newAdresseDom.personneLieeAdresseNomVoieNew : '')
																		+ ' ' + (newAdresseDom.personneLieeAdresseComplementAdresseNew != null ? newAdresseDom.personneLieeAdresseComplementAdresseNew : '')
																		+ ' ' + (newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null ? newAdresseDom.personneLieeAdresseDistriutionSpecialeNew : '');
	formFields['personneLiee_adresse_codePostal']                       = newAdresseDom.personneLieeAdresseCodePostalNew != null ? newAdresseDom.personneLieeAdresseCodePostalNew : '';
	formFields['personneLiee_adresse_commune']                          = newAdresseDom.personneLieeAdresseCommuneNew != null ? newAdresseDom.personneLieeAdresseCommuneNew : newAdresseDom.personneLieeAdresseVilleNew;
	formFields['personneLiee_adresse_pays']                             = newAdresseDom.personneLieeAdressePaysNew;
	formFields['personneLiee_adresse_commune_AncienneCommune']          = newAdresseDom.personneLieeAdresseCommuneNewAncienne != null ? newAdresseDom.personneLieeAdresseCommuneNewAncienne : '';
} else if (objet.entrepriseDomicile) {
	var newAdresseDom2 = $p2pl.cadre6EtablissementGroup.infoEtablissementNew.adresseEtablissementNew;
	if (etablissement.modifDateAdresseEntreprise !== null) {
		var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
		var date = pad(dateTmp.getDate().toString());
		var month = dateTmp.getMonth() + 1;
		date = date.concat(pad(month.toString()));
		date = date.concat(dateTmp.getFullYear().toString());
		formFields['modifDateDomicile']          = date;
	}

	formFields['personneLiee_adresse_voie']                             = (newAdresseDom2.etablissementAdresseNumeroVoieNew != null ? newAdresseDom2.etablissementAdresseNumeroVoieNew : '') 
																		+ ' ' + (newAdresseDom2.etablissementAdresseIndiceVoieNew != null ? newAdresseDom2.etablissementAdresseIndiceVoieNew : '')
																		+ ' ' + (newAdresseDom2.etablissementAdresseTypeVoieNew != null ? newAdresseDom2.etablissementAdresseTypeVoieNew : '')
																		+ ' ' + (newAdresseDom2.etablissementAdresseNomVoieNew != null ? newAdresseDom2.etablissementAdresseNomVoieNew : '')
																		+ ' ' + (newAdresseDom2.etablissementAdresseComplementVoieNew != null ? newAdresseDom2.etablissementAdresseComplementVoieNew : '')
																		+ ' ' + (newAdresseDom2.etablissementAdresseDistriutionSpecialeVoieNew != null ? newAdresseDom2.etablissementAdresseDistriutionSpecialeVoieNew : '');
	formFields['personneLiee_adresse_codePostal']                       = newAdresseDom2.etablissementAdresseCodePostalNew;
	formFields['personneLiee_adresse_commune']                          = newAdresseDom2.etablissementAdresseCommuneNew;
}

// Nouvelle nationalité

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('17P')) {
if (modifIdentiteDeclarant.modifNationalite.modifDateNationalite !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifNationalite.modifDateNationalite.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateNationalite']          = date;
}
formFields['modifNouvelleNationalite']                                   = modifIdentiteDeclarant.modifNationalite.modifNewNationalite;
}

// Nouveau pseudonyme

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P') and Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPseudonyme).contains('modifPseudonyme')) {
if (modifIdentiteDeclarant.modifIdentite.modifDateIdentite !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifIdentite.modifDateIdentite.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDatePseudonyme']          = date;
}
formFields['modifNouveauPseudonyme']                                     = modifIdentiteDeclarant.modifIdentite.modifNouveauPseudonyme != null ? modifIdentiteDeclarant.modifIdentite.modifNouveauPseudonyme : '';
}

// Mise en sommeil

if (Value('id').of(objet.objetModification).contains('etablissement') and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('40P')) {
if (objet.cadreObjetModificationEtablissement.cadreObjet40P.dateMiseEnSommeil !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet40P.dateMiseEnSommeil.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateMiseEnSommeil']          = date;
}
}

// Reprise d'activité

if (Value('id').of(objet.objetModification).contains('etablissement') and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')) {
if (objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateRepriseActivite']          = date;
}
}

// Cadres 6 - Modification EIRL

var poursuite = $p2pl.cadreDecesAvecPoursuiteGroup.cadreDecesAvecPoursuite;

if ($p2pl.cadre4DeclarationAffectationPatrimoineGroup.cadreDeclarationAffectationPatrimoine.modifDateDeclarationEIRL !== null) {
    var dateTmp = new Date(parseInt($p2pl.cadre4DeclarationAffectationPatrimoineGroup.cadreDeclarationAffectationPatrimoine.modifDateDeclarationEIRL.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date1;
} else if ($p2pl.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.modifDateEIRL !== null) {
    var dateTmp = new Date(parseInt($p2pl.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.modifDateEIRL.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date2;
}  else if (objet.poursuiteEIRL and poursuite.modifDateDeces !== null) {
    var dateTmp = new Date(parseInt(poursuite.modifDateDeces.getTimeInMillis()));
    var date3 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date3 = date3.concat(pad(month.toString()));
	date3 = date3.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date3;
}  else if ($p2pl.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.finEIRLGroup.finEIRLDeclaration.modifDateFinEIRL !== null) {
    var dateTmp = new Date(parseInt($p2pl.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.finEIRLGroup.finEIRLDeclaration.modifDateFinEIRL.getTimeInMillis()));
    var date4 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date4 = date4.concat(pad(month.toString()));
	date4 = date4.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date4;
}	else if (objet.entrepriseEIRL and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') and etablissement.modifDateAdresseEntreprise !== null) {
    var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
    var date5 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date5 = date5.concat(pad(month.toString()));
	date5 = date5.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date5;
} else if (objet.entrepriseEIRL and modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
    var date6 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date6 = date6.concat(pad(month.toString()));
	date6 = date6.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date6;
} 
formFields['eirl_immatriculation']                                                   = Value('id').of(objet.objetModification).contains('modifEIRL') ? (objet.cadreObjetDeclarationEIRL.declarationEIRL ? true : false) : false;
formFields['eirl_modification']                                                      = ((Value('id').of(objet.objetModification).contains('modifEIRL') and (Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDenoEIRL') or Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDateCloture') or Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifObjet') or Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse'))) or objet.entrepriseEIRL or objet.poursuiteEIRL) ? (objet.cadreObjetDeclarationEIRL.declarationEIRL ? false : true) : false;
formFields['eirl_affectationRetraitPatrimoine']                                      = (Value('id').of(objet.objetModification).contains('modifEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDAP')) ? true : false;

// Cadre 7 Décès de la personne titulaire de la charge ou de la fonction

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('22P')) { 
if (poursuite.modifDateDeces !== null) {
    var dateTmp = new Date(parseInt(poursuite.modifDateDeces.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateDeces']          = date;
}
formFields['nomNaissanceSuppleant']                                      = poursuite.nomNaissanceSuppleant;
formFields['nomUsageSuppleant']                                          = poursuite.nomUsageSuppleant != null ? poursuite.nomUsageSuppleant : '';
var prenoms=[];
for ( i = 0; i < poursuite.prenomSuppleant.size() ; i++ ){prenoms.push(poursuite.prenomSuppleant[i]);}                            
formFields['prenomSuppleant']                                      = prenoms.toString();
formFields['nationaliteSuppleant']                                       = poursuite.nationaliteSuppleant;
if (poursuite.dateNaissanceSuppleant !== null) {
    var dateTmp = new Date(parseInt(poursuite.dateNaissanceSuppleant.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['dateNaissanceSuppleant']          = date;
}
formFields['dptNaissanceSuppleant']                                      = poursuite.lieuNaissanceDepartementSuppleant != null ? poursuite.lieuNaissanceDepartementSuppleant.getId() : '';;
formFields['communeNaissanceSuppleant']                                  = (poursuite.lieuNaissanceCommuneSuppleant != null ?  poursuite.lieuNaissanceCommuneSuppleant : poursuite.lieuNaissanceVilleSuppleant) + ' / ' + poursuite.lieuNaissancePaysSuppleant;
formFields['paysNaissanceSuppleant']                                  = (poursuite.lieuNaissanceCommuneSuppleant != null ?  poursuite.lieuNaissanceCommuneSuppleant : poursuite.lieuNaissanceVilleSuppleant) + ' / ' + poursuite.lieuNaissancePaysSuppleant;
formFields['adresseVoieSuppleant']                      = (poursuite.adresseDomicileSuppleant.suppleantAdresseVoie != null ? poursuite.adresseDomicileSuppleant.suppleantAdresseVoie : '') 
														+ ' ' + (poursuite.adresseDomicileSuppleant.suppleantAdresseIndiceVoie != null ? poursuite.adresseDomicileSuppleant.suppleantAdresseIndiceVoie : '')
														+ ' ' + (poursuite.adresseDomicileSuppleant.suppleantAdresseTypeVoie != null ? poursuite.adresseDomicileSuppleant.suppleantAdresseTypeVoie : '')
														+ ' ' + (poursuite.adresseDomicileSuppleant.suppleantAdresseNomVoie != null ? poursuite.adresseDomicileSuppleant.suppleantAdresseNomVoie : '')
														+ ' ' + (poursuite.adresseDomicileSuppleant.suppleantAdresseComplementAdresse != null ? poursuite.adresseDomicileSuppleant.suppleantAdresseComplementAdresse : '')
														+ ' ' + (poursuite.adresseDomicileSuppleant.suppleantAdresseDistriutionSpeciale != null ? poursuite.adresseDomicileSuppleant.suppleantAdresseDistriutionSpeciale : '');
formFields['adresseCPSuppleant']                        = poursuite.adresseDomicileSuppleant.suppleantAdresseCodePostal != null ? poursuite.adresseDomicileSuppleant.suppleantAdresseCodePostal : '';
formFields['adresseCommuneSuppleant']                   = poursuite.adresseDomicileSuppleant.suppleantAdresseCommune != null ? poursuite.adresseDomicileSuppleant.suppleantAdresseCommune : poursuite.adresseDomicileSuppleant.suppleantAdresseVille;
formFields['adressePaysSuppleant']                      = poursuite.adresseDomicileSuppleant.suppleantAdressePays;
}

// Cadre 8A - Conjoint collaborateur

var conjoint = $p2pl.cadre3ModificationConjointGroup.cadre3ModificationConjoint;
var adresseConjoint = $p2pl.cadre3ModificationConjointGroup.cadre3ModificationConjoint.adresseDomicileConjoint;

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('30P')) {
if (conjoint.modifDateConjoint !== null) {
    var dateTmp = new Date(parseInt(conjoint.modifDateConjoint.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateConjoint']          = date;
}
formFields['suppression_mentionConjoint']                                            = Value('id').of(conjoint.objetModifStatut).eq('suppressionStatut') ? true : false;
formFields['modification_mentionConjoint']                                           = Value('id').of(conjoint.objetModifStatut).eq('modificationStatut') ? true : false;
formFields['nouvelle_mentionConjoint']                                               = Value('id').of(conjoint.objetModifStatut).eq('nouveauStatut') ? true : false;

// Cadre 8B

if (not Value('id').of(conjoint.objetModifStatut).eq('suppressionStatut')) {
formFields['conjointSalarie']                                                        = Value('id').of(conjoint.objetModifConjoint).eq('conjointSalarie') ? true : false;
formFields['conjointCollaborateur']                                                  = Value('id').of(conjoint.objetModifConjoint).eq('conjointCollaborateur') ? true : false;

// Cadre 8C


formFields['modifNomNaissanceConjoint']                                              = conjoint.modifNomNaissanceConjoint;
formFields['modifNomUsageConjoint']                                                  = conjoint.modifNomUsageConjoint != null ? conjoint.modifNomUsageConjoint : '';
var prenoms=[];
for ( i = 0; i < conjoint.modifPrenomConjoint.size() ; i++ ){prenoms.push(conjoint.modifPrenomConjoint[i]);}                            
formFields['modifPrenomConjoint']                                      = prenoms.toString();
if ($p2pl.cadre3ModificationConjointGroup.cadre3ModificationConjoint.modifDateNaissanceConjoint !== null) {
    var dateTmp = new Date(parseInt($p2pl.cadre3ModificationConjointGroup.cadre3ModificationConjoint.modifDateNaissanceConjoint.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateNaissanceConjoint']          = date;
}
formFields['modifLieuNaissanceDepartementConjoint']                                  = conjoint.modifLieuNaissanceDepartementConjoint != null ? conjoint.modifLieuNaissanceDepartementConjoint.getId() : '';
formFields['modifLieuNaissanceCommuneConjoint']                                      = conjoint.modifLieuNaissanceCommuneConjoint != null ? conjoint.modifLieuNaissanceCommuneConjoint : conjoint.modifLieuNaissanceVilleConjoint;
formFields['modifLieuNaissancePaysConjoint']                                         = conjoint.modifLieuNaissancePaysConjoint;

if (conjoint.adresseConjointDifferente) {
formFields['modifAdresseVoieConjoint']                      = (adresseConjoint.adresseConjointNumeroVoie != null ? adresseConjoint.adresseConjointNumeroVoie : '') 
															+ ' ' + (adresseConjoint.adresseConjointIndiceVoie != null ? adresseConjoint.adresseConjointIndiceVoie : '')
															+ ' ' + (adresseConjoint.adresseConjointTypeVoie != null ? adresseConjoint.adresseConjointTypeVoie : '')
															+ ' ' + (adresseConjoint.adresseConjointNomVoie != null ? adresseConjoint.adresseConjointNomVoie : '')
															+ ' ' + (adresseConjoint.adresseConjointComplementVoie != null ? adresseConjoint.adresseConjointComplementVoie : '')
															+ ' ' + (adresseConjoint.adresseConjointDistriutionSpecialeVoie != null ? adresseConjoint.adresseConjointDistriutionSpecialeVoie : '');
formFields['modifAdresseCPConjoint']                        = adresseConjoint.adresseConjointCodePostal;
formFields['modifAdresseCommuneConjoint']                   = adresseConjoint.adresseConjointCommune;
formFields['modifAdresseAncienneCommuneConjoint']           = adresseConjoint.adresseConjointCommuneAncienne;
}
}
}

// Cadre 9 - Contrat d'appui

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('37P')) {
if (modifIdentiteDeclarant.modifContratAppui.modifDateAppui !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifContratAppui.modifDateAppui.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateContratAppui']          = date;
}
formFields['modifContratAppui']                                   = true;
}

// Cadre 10 - Déclaration relative au lieu d'exercice ou à l'établissement 

var activiteInfo = $p2pl.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite;

formFields['modifConcerneActivite']                                      = (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite') 
																		or activiteInfo.modifActiviteTransfert 
																		or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P') 
																		or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('40P')) ? true : false;
formFields['modifConcerneTransfert']                                     = (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') or objet.domicileEntreprise) ? true : false;
formFields['modifConcerneOuverture']                                     = Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') ? true : false;
formFields['modifConcerneFermeture']                                     = Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P') ? true : false;
formFields['modifConcerneAutre']                                         = (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P') or Value('id').of(objet.objetModification).contains('dateActivite')) ? true : false;;

// Cadre 11 - Etablissement transféré ou fermé

var adresseEntreprise = $p2pl.cadre1RappelIdentificationGroup.cadre1RappelIdentification.adresseEtablissementPrincipal;

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P') 
	or objet.domicileEntreprise
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('40P')) {
if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P')) and etablissement.modifDateAdresseEntreprise !== null) {
    var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementTransfererFermer']          = date;
} else if (objet.domicileEntreprise and modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementTransfererFermer']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('40P') and objet.cadreObjetModificationEtablissement.cadreObjet40P.dateMiseEnSommeil !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet40P.dateMiseEnSommeil.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementTransfererFermer']          = date;
}

formFields['modifCategarieEtablissementTransfererFermer_principal']      = (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') or objet.domicileEntreprise or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('40P')) ? true : false;
formFields['modifCategarieEtablissementTransfererFermer_secondaire']     = (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire') or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).contains('80P'))? true : false;

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')) or objet.domicileEntreprise or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('40P')) {
formFields['modifAncienneAdresseEtablissement_adresseVoie']             = (adresseEntreprise.etablissementAdresseNumeroVoie != null ? adresseEntreprise.etablissementAdresseNumeroVoie : '')
																		+ ' ' + (adresseEntreprise.etablissementAdresseIndiceVoie != null ? adresseEntreprise.etablissementAdresseIndiceVoie : '')
																		+ ' ' + (adresseEntreprise.etablissementAdresseTypeVoie != null ? adresseEntreprise.etablissementAdresseTypeVoie : '')
																		+ ' ' + (adresseEntreprise.etablissementAdresseNomVoie != null ? adresseEntreprise.etablissementAdresseNomVoie : '');
formFields['modifAncienneAdresseEtablissement_adresseComplement']        = (adresseEntreprise.etablissementAdresseComplementVoie != null ? adresseEntreprise.etablissementAdresseComplementVoie : '')
																		+ ' ' + (adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null ? adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie : '');
formFields['modifAncienneAdresseEtablissement_adresseCP']               = adresseEntreprise.etablissementAdresseCodePostal;
formFields['modifAncienneAdresseEtablissement_adresseCommune']          = adresseEntreprise.etablissementAdresseCommune;
} else if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')) or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P')) {
formFields['modifAncienneAdresseEtablissement_adresseVoie']             = (etablissement.adresseEtablissementOld.etablissementAdresseNumeroVoieOld != null ? etablissement.adresseEtablissementOld.etablissementAdresseNumeroVoieOld : '') 
																		+ ' ' + (etablissement.adresseEtablissementOld.etablissementAdresseIndiceVoieOld != null ? etablissement.adresseEtablissementOld.etablissementAdresseIndiceVoieOld : '')
																		+ ' ' + (etablissement.adresseEtablissementOld.etablissementAdresseTypeVoieOld != null ? etablissement.adresseEtablissementOld.etablissementAdresseTypeVoieOld : '')
																		+ ' ' + (etablissement.adresseEtablissementOld.etablissementAdresseNomVoieOld != null ? etablissement.adresseEtablissementOld.etablissementAdresseNomVoieOld : '');
formFields['modifAncienneAdresseEtablissement_adresseComplement']       = (etablissement.adresseEtablissementOld.etablissementAdresseComplementVoieOld != null ? etablissement.adresseEtablissementOld.etablissementAdresseComplementVoieOld : '')
																		+ ' ' + (etablissement.adresseEtablissementOld.etablissementAdresseDistriutionSpecialeVoieOld != null ? etablissement.adresseEtablissementOld.etablissementAdresseDistriutionSpecialeVoieOld : '');
formFields['modifAncienneAdresseEtablissement_adresseCP']               = etablissement.adresseEtablissementOld.etablissementAdresseCodePostalOld;
formFields['modifAncienneAdresseEtablissement_adresseCommune']          = etablissement.adresseEtablissementOld.etablissementAdresseCommuneOld;
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') or objet.domicileEntreprise) {
formFields['modifDestinationEtablissementTransfert_vendu']               = (Value('id').of(etablissement.destinationEtablissement1).eq('vendu') or Value('id').of(etablissement.destinationEtablissement2).eq('vendu')) ? true : false;
formFields['modifDestinationEtablissementTransfert_ferme']               = (Value('id').of(etablissement.destinationEtablissement1).eq('ferme') or Value('id').of(etablissement.destinationEtablissement2).eq('ferme')  or objet.domicileEntreprise) ? true : false;
formFields['modifDestinationEtablissementTransfert_autre']               = (Value('id').of(etablissement.destinationEtablissement1).eq('autre') or Value('id').of(etablissement.destinationEtablissement2).eq('autre')) ? true : false;
formFields['modifDestinationEtablissementTransfert_autreLibelle']        = (Value('id').of(etablissement.destinationEtablissement1).eq('autre') or Value('id').of(etablissement.destinationEtablissement2).eq('autre')) ? etablissement.destinationAutre : '';
formFields['modifDestinationEtablissement_devientPrincipal']             = Value('id').of(etablissement.destinationEtablissement2).eq('devientPrincipal') ? true : false;
formFields['modifDestinationEtablissement_devientSecondaire']            = Value('id').of(etablissement.destinationEtablissement1).eq('devientSecondaire') ? true : false;
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P') or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('40P')) {
formFields['modifDestinationEtablissementFerme_supprimer']               = Value('id').of(etablissement.destinationEtablissement3).eq('supprime') ? true : false;
formFields['modifDestinationEtablissementFerme_vendu']                   = Value('id').of(etablissement.destinationEtablissement3).eq('vendu') ? true : false;
formFields['modifDestinationEtablissementFerme_autre']                   = Value('id').of(etablissement.destinationEtablissement3).eq('autre') ? true : false;
formFields['modifDestinationEtablissementFerme_autreLibelle']            = Value('id').of(etablissement.destinationEtablissement3).eq('autre') ? etablissement.destinationAutre : '';
if (etablissement.dateCessationEmploi !== null) {
    var dateTmp = new Date(parseInt(etablissement.dateCessationEmploi.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDestinationEtablissementDateFinEmploiSalarie']          = date;
}
}
}

// Cadre 12 - Etablissement créé ou modifié

var etablissementNew =$p2pl.cadre6EtablissementGroup.infoEtablissementNew;
var activite = $p2pl.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite;

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
	or (Value('id').of(objet.objetModification).contains('dateActivite') and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 !== null or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 !== null))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')
	or objet.domicileEntreprise) {
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissement.modifDateAdresseEntreprise !== null) {
    var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
} else if (objet.domicileEntreprise and modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') and etablissementNew.modifDateAdresseOuverture !== null) {
    var dateTmp = new Date(parseInt(etablissementNew.modifDateAdresseOuverture.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite') and activiteInfo.cadreActivite.modifDateActivite !== null) {
    var dateTmp = new Date(parseInt(activiteInfo.cadreActivite.modifDateActivite.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
}  else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P') and activiteInfo.cadreEtablissementIdentification.modifDateIdentification !== null) {
    var dateTmp = new Date(parseInt(activiteInfo.cadreEtablissementIdentification.modifDateIdentification.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
} else if (Value('id').of(objet.objetModification).contains('dateActivite') and objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
}  else if (Value('id').of(objet.objetModification).contains('dateActivite') and objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
}  else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P') and objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementOuvertModifier']          = date;
} 

if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P'))  
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or (Value('id').of(objet.objetModification).contains('dateActivite') and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 != null or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 !== null))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P'))	{
formFields['modifNouvelleAdresseEtablissement_voieAdresse']             = (adresseEntreprise.etablissementAdresseNumeroVoie != null ? adresseEntreprise.etablissementAdresseNumeroVoie : '')
																		+ ' ' + (adresseEntreprise.etablissementAdresseIndiceVoie != null ? adresseEntreprise.etablissementAdresseIndiceVoie : '')
																		+ ' ' + (adresseEntreprise.etablissementAdresseTypeVoie != null ? adresseEntreprise.etablissementAdresseTypeVoie : '')
																		+ ' ' + (adresseEntreprise.etablissementAdresseNomVoie != null ? adresseEntreprise.etablissementAdresseNomVoie : '')
																		+ ' ' + (adresseEntreprise.etablissementAdresseComplementVoie != null ? adresseEntreprise.etablissementAdresseComplementVoie : '')
																		+ ' ' + (adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null ? adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie : '');
formFields['modifNouvelleAdresseEtablissement_cpAdresse']               = adresseEntreprise.etablissementAdresseCodePostal;
formFields['modifNouvelleAdresseEtablissement_communeAdresse']          = adresseEntreprise.etablissementAdresseCommune;
} else if (objet.domicileEntreprise) {
formFields['modifNouvelleAdresseEtablissement_voieAdresse']             = (newAdresseDom.personneLieeAdresseVoieNew != null ? newAdresseDom.personneLieeAdresseVoieNew : '') 
																		+ ' ' + (newAdresseDom.personneLieeAdresseIndiceVoieNew != null ? newAdresseDom.personneLieeAdresseIndiceVoieNew : '')
																		+ ' ' + (newAdresseDom.personneLieeAdresseTypeVoieNew != null ? newAdresseDom.personneLieeAdresseTypeVoieNew : '')
																		+ ' ' + (newAdresseDom.personneLieeAdresseNomVoieNew != null ? newAdresseDom.personneLieeAdresseNomVoieNew : '')
																		+ ' ' + (newAdresseDom.personneLieeAdresseComplementAdresseNew != null ? newAdresseDom.personneLieeAdresseComplementAdresseNew : '')
																		+ ' ' + (newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null ? newAdresseDom.personneLieeAdresseDistriutionSpecialeNew : '');
formFields['modifNouvelleAdresseEtablissement_cpAdresse']               = newAdresseDom.personneLieeAdresseCodePostalNew;
formFields['modifNouvelleAdresseEtablissement_communeAdresse']          = newAdresseDom.personneLieeAdresseCommuneNew;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
		  or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') 
		  or ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
		  or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P'))  
		  and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))) {
formFields['modifNouvelleAdresseEtablissement_voieAdresse']             = (etablissementNew.adresseEtablissementNew.etablissementAdresseNumeroVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseNumeroVoieNew : '') 
																		+ ' ' + (etablissementNew.adresseEtablissementNew.etablissementAdresseIndiceVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseIndiceVoieNew : '')
																		+ ' ' + (etablissementNew.adresseEtablissementNew.etablissementAdresseTypeVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseTypeVoieNew : '')
																		+ ' ' + (etablissementNew.adresseEtablissementNew.etablissementAdresseNomVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseNomVoieNew : '')
																		+ ' ' + (etablissementNew.adresseEtablissementNew.etablissementAdresseComplementVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseComplementVoieNew : '')
																		+ ' ' + (etablissementNew.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew : '');
formFields['modifNouvelleAdresseEtablissement_cpAdresse']               = etablissementNew.adresseEtablissementNew.etablissementAdresseCodePostalNew;
formFields['modifNouvelleAdresseEtablissement_communeAdresse']          = etablissementNew.adresseEtablissementNew.etablissementAdresseCommuneNew;
}

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(etablissementNew.origineEtablissement).eq('etablissementExistant'))
	or (Value('id').of(objet.objetModification).contains('dateActivite') 
	and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 !== null 
	or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 !== null))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')) {
formFields['modifCategarieEtablissementOuvert_effectifPresenceOui']      = activite.etablissementEffectifSalariePresenceOui ? true : false;
formFields['modifCategarieEtablissementOuvert_effectifPresenceNon']      = activite.etablissementEffectifSalariePresenceOui ? false : true;

if (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P') 
	or (Value('id').of(objet.objetModification).contains('dateActivite') 
	and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 != null 
	or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 !== null))) {
formFields['modifCategarieEtablissementModifier_principal']              = true;
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')) {
formFields['modifCategarieEtablissementModifier_secondaire']             = true;
}
}

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') and Value('id').of(etablissementNew.origineEtablissement).eq('etablissementOuverture')) or objet.domicileEntreprise) {
formFields['modifCategarieEtablissementOuvert_principal']                = true;
}
if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire') and Value('id').of(etablissementNew.origineEtablissement).eq('etablissementOuverture')) or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')) {
formFields['modifCategarieEtablissementOuvert_secondaire']               = true;
}
}

// Cadre 13 - Activité

if(Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')
or objet.domicileEntreprise) {		    
		
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite') and activite.modifDateActivite !== null) {
    var dateTmp = new Date(parseInt(activite.modifDateActivite.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateActivite']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissement.modifDateAdresseEntreprise !== null) {
    var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateActivite']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') and etablissementNew.modifDateAdresseOuverture !== null) {
    var dateTmp = new Date(parseInt(etablissementNew.modifDateAdresseOuverture.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateActivite']          = date;
} else if (objet.domicileEntreprise and modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateActivite']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P') and objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateActivite']          = date;
} 

formFields['etablissement_activites']                                    = activite.etablissementActivitesAutres;
if (activite.etablissementActivitesPrincipales2 != null) {
formFields['etablissement_activitePlusImportante']                       = activite.etablissementActivitesPrincipales2;
}
formFields['activiteDevientPrincipale_oui']                              = activite.activiteDevientPrincipale ? true : false ;
formFields['activiteDevientPrincipale_non']                              = activite.activiteDevientPrincipale ? false : true;
formFields['entreprise_activiteSaisonniere']                             = Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonniereSaisonniere') ? true : false;
if (Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonniereSaisonniere')) {
formFields['activiteSaisonnierePeriodeDu']                                              = "Du";
formFields['activiteSaisonnierePeriodeDebut']                                           = activite.activiteSaisonnierePeriode.from;
formFields['activiteSaisonnierePeriodeAu']                                              = "au";
formFields['activiteSaisonnierePeriodeFin']                                             = activite.activiteSaisonnierePeriode.to;
formFields['activiteSaisonnierePeriode2Du']                                             = activite.autrePeriode ? "Du" : '';
formFields['activiteSaisonnierePeriode2Debut']                                          = activite.autrePeriode ? activite.activiteSaisonnierePeriode2.from : '';
formFields['activiteSaisonnierePeriode2Au']                                             = activite.autrePeriode ? "au" : '';
formFields['activiteSaisonnierePeriode2Fin']                                            = activite.autrePeriode ? activite.activiteSaisonnierePeriode2.to : '';
}
formFields['etablissment_nonSedentariteQualite']                         = activite.etablissementNonSedentariteQualiteNonSedentaire ? true : false;
}

// Cadres 14 - Enseigne

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P') or (activiteInfo.modifEtablissementEnseigne and not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P'))) {
if (activiteInfo.cadreEtablissementIdentification.modifDateIdentification !== null) {
    var dateTmp = new Date(parseInt(activiteInfo.cadreEtablissementIdentification.modifDateIdentification.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateNomEtablissement']          = date;
} 
formFields['etablissement_enseigne']                                     = activiteInfo.cadreEtablissementIdentification.modifEnseigne;
} else if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') or objet.domicileEntreprise
	or Value('id').of(etablissementNew.origineEtablissement).eq('etablissementOuverture')) and activite.modifEnseigneBis != null) {
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissement.modifDateAdresseEntreprise !== null) {
    var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateNomEtablissement']          = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') and etablissementNew.modifDateAdresseOuverture !== null) {
    var dateTmp = new Date(parseInt(etablissementNew.modifDateAdresseOuverture.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateNomEtablissement']          = date;
} 
formFields['etablissement_enseigne']                                     = activite.modifEnseigneBis;
}  else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')) {
if (objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateNomEtablissement']          = date;
}
formFields['etablissement_enseigne']                                     =  activiteInfo.cadreEtablissementIdentification.modifEnseigne;
}

// Cadre 15 - origine de l'activité

var origine = $p2pl.cadre7EtablissementActiviteGroup.cadre4OrigineFonds ;

if (Value('id').of(etablissementNew.origineEtablissement).eq('etablissementOuverture')
				or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
			    or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).contains('21P')
				or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
			    or activiteInfo.modifActiviteTransfert
				or objet.domicileEntreprise) {
formFields['etablissement_origineFonds_creation']                        = (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation') or objet.domicileEntreprise) ? true : false;
formFields['etablissement_origineFonds_reprise']                         = Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsReprise') ? true : false;
if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsReprise') and Value('id').of(origine.precedentExploitant.typePersonnePrecedentExploitant).eq('typePersonnePrecedentExploitantPP')) {
formFields['nomNaissance_precedentExploitant']                           = origine.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant;
formFields['nomUsage_precedentExploitant']                               = origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant != null ? origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant : '';
formFields['prenom_precedentExploitant']                                 = origine.precedentExploitant.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant;
}
if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsReprise') and Value('id').of(origine.precedentExploitant.typePersonnePrecedentExploitant).eq('typePersonnePrecedentExploitantPM')) {
formFields['denomination_precedentExploitant']                           = origine.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant;
}
if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsReprise')) {
formFields['siren_precedentExploitant']                                  = origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant.split(' ').join('');
}
}

// Cadre 16 - Effectif salarié établissement créé

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).contains('21P')
	or Value('id').of(etablissementNew.origineEtablissement).eq('etablissementOuverture')
	or objet.domicileEntreprise) {
formFields['modifCategarieEtablissementOuvert_effectifNombre']           = activite.etablissementEffectifSalariePresenceOui ? activite.cadreEffectifSalarie.etablissementEffectifSalarieNombre : "0";
formFields['modifCategarieEtablissementOuvert_effectifNombreEntreprise'] = activite.etablissementEffectifSalariePresenceOui ? activite.cadreEffectifSalarie.totalEffectifSalarieNombre : "0";
formFields['modifCategarieEtablissementOuvert_effectifApprentis']        = activite.cadreEffectifSalarie.etablissementEffectifSalarieApprentis != null ? activite.cadreEffectifSalarie.etablissementEffectifSalarieApprentis : '';
formFields['modifCategarieEtablissementOuvert_effectifVRP']              = activite.cadreEffectifSalarie.etablissementEffectifSalarieVRP != null ? activite.cadreEffectifSalarie.etablissementEffectifSalarieVRP : '';
}

// Cadre 17 - Observation

var correspondance = $p2pl.cadre8RensCompGroup.cadre8RensComp;
var signataire = $p2pl.cadre9SignatureGroup.cadre9Signature;

if (Value('id').of(objet.objetModification).contains('dateActivite') and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 !== null or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 !== null)) {
if (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateObservations']          = date;
} else if (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateObservations']          = date;
}
formFields['observations']                 = "Modification de la date de début d'activité" + ' / ' +  (correspondance.formaliteObservations != null ? correspondance.formaliteObservations : '');
} else if (not Value('id').of(objet.objetModification).contains('dateActivite') and objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 == null and objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 == null) {	
formFields['observations']                 = correspondance.formaliteObservations != null ? correspondance.formaliteObservations : '';
}

// Cadre 18 - Adresse de correspondance

formFields['adresseCorrespondanceCadre_coche']            = (Value('id').of(correspondance.adresseCorrespond).eq('domi') 
															or Value('id').of(objet.objetModification).contains('dateActivite')
															or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')
															or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('40P')
															or Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement') 
															or Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') 
															or (Value('id').of(correspondance.adresseCorrespond).eq('prof') 
															and (objet.domicileEntreprise 
															or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
															and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))))) ? true : false;
formFields['adresseCorrespondanceCadre_numero']           = Value('id').of(correspondance.adresseCorrespond).eq('domi') ? "5" : 
															((Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement') 
															or(Value('id').of(correspondance.adresseCorrespond).eq('prof') 
															and (objet.domicileEntreprise 
															or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('40P')
															or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
															and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))))) ? "11" :
															(Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') or Value('id').of(objet.objetModification).contains('dateActivite') or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')) ? "12" : '');
formFields['adressesCorrespondanceAutre']                 = (Value('id').of(correspondance.adresseCorrespond).eq('autre') 
															or (Value('id').of(correspondance.adresseCorrespond).eq('prof') 
															and not Value('id').of(objet.objetModification).contains('dateActivite')
															and not objet.domicileEntreprise 
															and not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')
															and not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('40P')		
															and (not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
															or not Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')))) ? true : false;

if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
formFields['adresseCorrespondance_voie1']                 = correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance
															+ ' ' + (correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance : '')
															+ ' ' + (correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance : '')
															+ ' ' + (correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance : '')
															+ ' ' + (correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance : '');
formFields['adresseCorrespondance_voie2']                 = (correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance != null ? correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance : '')
															+ ' ' + (correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance != null ? correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance : '');
formFields['adresseCorrespondance_codePostal']            = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal;
formFields['adresseCorrespondance_commune']               = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('prof') 
			and not Value('id').of(objet.objetModification).contains('dateActivite')
			and not objet.domicileEntreprise 
			and not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P') 
			and not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('40P')
			and (not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
			or not Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))) {
formFields['adresseCorrespondance_voie1']             = (adresseEntreprise.etablissementAdresseNumeroVoie != null ? adresseEntreprise.etablissementAdresseNumeroVoie : '')
														+ ' ' + (adresseEntreprise.etablissementAdresseIndiceVoie != null ? adresseEntreprise.etablissementAdresseIndiceVoie : '')
														+ ' ' + (adresseEntreprise.etablissementAdresseTypeVoie != null ? adresseEntreprise.etablissementAdresseTypeVoie : '')
														+ ' ' + (adresseEntreprise.etablissementAdresseNomVoie != null ? adresseEntreprise.etablissementAdresseNomVoie : '');
formFields['adresseCorrespondance_voie2']			  = (adresseEntreprise.etablissementAdresseComplementVoie != null ? adresseEntreprise.etablissementAdresseComplementVoie : '')
														+ ' ' + (adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null ? adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie : '');
formFields['adresseCorrespondance_codePostal']        = adresseEntreprise.etablissementAdresseCodePostal;
formFields['adresseCorrespondance_commune']           = adresseEntreprise.etablissementAdresseCommune;
}

formFields['telephone1']                                                             = correspondance.infosSup.formaliteTelephone1 != null ? correspondance.infosSup.formaliteTelephone1 : '';
formFields['telephone2']                                                             = correspondance.infosSup.formaliteTelephone2;
formFields['courriel']                                                               = correspondance.infosSup.formaliteFaxCourriel != null ? correspondance.infosSup.formaliteFaxCourriel : (correspondance.infosSup.telecopie != null ? correspondance.infosSup.telecopie : '');

// Cadre 19 - Diffusion informations

formFields['diffusionInformation']                                                   = Value('id').of(signataire.diffusionInfo).eq('formaliteNonDiffusionInformationOui') ? true : false;
formFields['nonDiffusionInformation']                                                = Value('id').of(signataire.diffusionInfo).eq('formaliteNonDiffusionInformationNon') ? true : false;

// Cadre 20 - Signataire

formFields['signataireDeclarant']                                                    = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
if (Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
formFields['signataireMandataire']                                                   = true;
formFields['nomPrenomDenominationMandataire']					                     = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFields['signataireAdresse']                                                      = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '');
formFields['signataireAdresseCP']	 		 							    		 = signataire.adresseMandataire.dataCodePostalMandataire;
formFields['signataireAdresseCommune']		     									 = signataire.adresseMandataire.villeAdresseMandataire;
}
if (signataire.formaliteSignatureDate !== null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['signatureDate']          = date;
}
formFields['signatureLieu']                                                          = signataire.formaliteSignatureLieu;
formFields['estEIRL_oui']                                                            = Value('id').of(objet.objetModification).contains('modifEIRL') ? true : false;
formFields['estEIRL_non']                                                            = Value('id').of(objet.objetModification).contains('modifEIRL') ? false : true;
formFields['signature']                                                              = '';


                          // Intercalaire PEIRL ME
								
//Cadre 1 - Déclaration ou modification d'affectation de patrimoine

formFields['formulaire_dependance_P0PL']                                                         = false;
formFields['formulaire_dependance_P0PLME']                                                       = false;
formFields['formulaire_dependance_P2PL']                                                         = true;
formFields['formulaire_dependance_P4PL']                                                         = false;
formFields['formulaire_dependance_AC0']                                                          = false;
formFields['formulaire_dependance_AC2']                                                          = false;
formFields['formulaire_dependance_AC4']                                                          = false;

formFields['declaration_initiale']															= objet.cadreObjetDeclarationEIRL.declarationEIRL ? true : false;
formFields['declaration_modification']                                                      = (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') or Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('finEIRL') or objet.entrepriseEIRL or objet.poursuiteEIRL) ? true : false;

// Cadre 2 - Rappel d'identification

formFields['eirl_siren']                                                  = identite.siren.split(' ').join('');
formFields['eirl_nomNaissance']                                           = identite.personneLieePersonnePhysiqueNomNaissance;
formFields['eirl_nomUsage']                                               = identite.personneLieePersonnePhysiqueNomUsage != null ? identite.personneLieePersonnePhysiqueNomUsage : '';
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFields['eirl_prenom']                                      = prenoms.toString();

// Cadre 3 - Déclaration d'affectation de patrimoine

var declarationAffectation = $p2pl.cadre4DeclarationAffectationPatrimoineGroup.cadreDeclarationAffectationPatrimoine;

if (objet.cadreObjetDeclarationEIRL.declarationEIRL) { 
formFields['eirl_statutEIRL_declarationInitialeSansDepot']                                  = Value('id').of(declarationAffectation.eirlDepot).eq('eirlSansDepot') ? true : false;
formFields['eirl_statutEIRL_declarationInitialeAvecDepot']                                  = Value('id').of(declarationAffectation.eirlDepot).eq('eirlAvecDepot') ? true : false;
formFields['eirl_statutEIRL_reprise']                                                       = Value('id').of(declarationAffectation.eirlStatut).eq('EirlStatutEIRLReprise') ? true : false;
formFields['eirl_denomination']                                                             = declarationAffectation.declarationPatrimoine.eirlDenomination;
if (declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable !== null) {
    var dateTmp = new Date(parseInt(declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable.getTimeInMillis()));
    var dateClotureEc = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateClotureEc = dateClotureEc.concat(pad(month.toString()));
    formFields['eirl_dateClotureExerciceComptable']          = dateClotureEc;
}
formFields['eirl_objet']                                                                    = declarationAffectation.declarationPatrimoine.eirlObjet;

if (Value('id').of(declarationAffectation.eirlStatut).eq('EirlStatutEIRLReprise')) {
formFields['eirl_precedentEIRLDenomination']                                                = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLDenomination;
formFields['eirl_precedentEIRLLieuImmatriculation']                                         = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLLieuImmatriculation;
formFields['eirl_precedentEIRLSIREN']                                                       = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN != null ? (declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN.split(' ').join('')) : '';
formFields['eirl_precedentEIRLRegistre_rseirl']                                             = true;
formFields['eirl_precedentEIRLRegistre_rsac']                                               = false;
}
}

// Cadre 4 - Rappel d'identification reltif à l'EIRL

var rappelIdentification = $p2pl.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreRappelIdentificationEIRL;

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') or Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('finEIRL') or objet.entrepriseEIRL or objet.poursuiteEIRL) { 
formFields['eirl_rappelDenomination']                   = rappelIdentification.eirlRappelDenomination;
formFields['eirl_rappelLieuImmatriculation']            = rappelIdentification.eirlRappelLieuImmatriculation;

if (objet.cadreObjetModificationEIRL.memeAdresseEIRL or objet.entrepriseEIRL or objet.poursuiteEIRL) {
formFields['eirl_rappelAdresse']                    = (adresseEntreprise.etablissementAdresseNumeroVoie != null ? adresseEntreprise.etablissementAdresseNumeroVoie : '')
													+ ' ' + (adresseEntreprise.etablissementAdresseIndiceVoie != null ? adresseEntreprise.etablissementAdresseIndiceVoie : '')
													+ ' ' + (adresseEntreprise.etablissementAdresseTypeVoie != null ? adresseEntreprise.etablissementAdresseTypeVoie : '')
													+ ' ' + (adresseEntreprise.etablissementAdresseNomVoie != null ? adresseEntreprise.etablissementAdresseNomVoie : '')
													+ ' ' + (adresseEntreprise.etablissementAdresseComplementVoie != null ? adresseEntreprise.etablissementAdresseComplementVoie : '')
													+ ' ' + (adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null ? adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie : '')
													+ ' ' + adresseEntreprise.etablissementAdresseCodePostal
													+ ' ' + adresseEntreprise.etablissementAdresseCommune;
} else if (not objet.cadreObjetModificationEIRL.memeAdresseEIRL) {
formFields['eirl_rappelAdresse']                    = (rappelIdentification.cadreEirlRappelAdresse.numeroAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.numeroAdresseEirl : '')
													+ ' ' + (rappelIdentification.cadreEirlRappelAdresse.indiceVoieAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.indiceVoieAdresseEirl : '')
													+ ' ' + (rappelIdentification.cadreEirlRappelAdresse.typeVoieAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.typeVoieAdresseEirl : '')
													+ ' ' + rappelIdentification.cadreEirlRappelAdresse.nomVoieAdresseEirl
													+ ' ' + (rappelIdentification.cadreEirlRappelAdresse.complementAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.complementAdresseEirl : '')
													+ ' ' + (rappelIdentification.cadreEirlRappelAdresse.distriutionSpecialeAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.distriutionSpecialeAdresseEirl : '')
													+ ' ' + rappelIdentification.cadreEirlRappelAdresse.codePostalAdresseEirl
													+ ' ' + rappelIdentification.cadreEirlRappelAdresse.communeAdresseEirl;
}
formFields['eirl_rappelDepotRSEIRL']                                                             = true;
formFields['eirl_rappelDepotRSAC']                                                               = false;
}

// Cadre 5 et 6- Déclaration de modification

var modifEIRL = $p2pl.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL;                                         

if(Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDenoEIRL')) {
if (modifEIRL.modifDateEIRL !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateDenoEIRL']          = dateModif;
}
formFields['new_DenoEIRL']                                                                  = modifEIRL.newDenoEIRL;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDateCloture')) {
if (modifEIRL.modifDateEIRL !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateClotureExerciceComptableEIRL']          = dateModif;
}                                         
if (modifEIRL.eirlNewDateClotureExerciceComptable !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.eirlNewDateClotureExerciceComptable.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    formFields['eirl_newDateClotureExerciceComptable']          = dateModif;
}
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifObjet')) {
if (modifEIRL.modifDateEIRL !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateObjetEIRL']          = dateModif;
}
formFields['new_objetEIRL']                                                                 = modifEIRL.newObjetEIRL;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse')) {
if (modifEIRL.modifDateEIRL !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateAdresseEIRL']          = dateModif;
}	
formFields['new_adresseEIRL']                           = (modifEIRL.newAdresseEIRL.numeroAdresseEirlNew != null ? modifEIRL.newAdresseEIRL.numeroAdresseEirlNew : '')
														+ ' ' + (modifEIRL.newAdresseEIRL.indiceVoieAdresseEirlNew != null ? modifEIRL.newAdresseEIRL.indiceVoieAdresseEirlNew : '')
														+ ' ' + (modifEIRL.newAdresseEIRL.typeVoieAdresseEirlNew != null ? modifEIRL.newAdresseEIRL.typeVoieAdresseEirlNew : '')
														+ ' ' + modifEIRL.newAdresseEIRL.nomVoieAdresseEirlNew;
formFields['new_adresseEIRL2']                          = (modifEIRL.newAdresseEIRL.complementAdresseEirlNew != null ? modifEIRL.newAdresseEIRL.complementAdresseEirlNew : '')
														+ ' ' + (modifEIRL.newAdresseEIRL.distriutionSpecialeAdresseEirlNew != null ? modifEIRL.newAdresseEIRL.distriutionSpecialeAdresseEirlNew : '')
														+ ' ' + modifEIRL.newAdresseEIRL.codePostalAdresseEirlNew
														+ ' ' + modifEIRL.newAdresseEIRL.communeAdresseEirlNew;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and objet.entrepriseEIRL) {
if (etablissement.modifDateAdresseEntreprise !== null) {
    var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateAdresseEIRL']          = date;
}
formFields['new_adresseEIRL']                             = (etablissementNew.adresseEtablissementNew.etablissementAdresseNumeroVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseNumeroVoieNew : '') 
														  + ' ' + (etablissementNew.adresseEtablissementNew.etablissementAdresseIndiceVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseIndiceVoieNew : '')
														  + ' ' + (etablissementNew.adresseEtablissementNew.etablissementAdresseTypeVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseTypeVoieNew : '')
														  + ' ' + (etablissementNew.adresseEtablissementNew.etablissementAdresseNomVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseNomVoieNew : '');
formFields['new_adresseEIRL2']							  = (etablissementNew.adresseEtablissementNew.etablissementAdresseComplementVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseComplementVoieNew : '')
														+ ' ' + (etablissementNew.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew != null ? etablissementNew.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew : '')
														+ ' ' + etablissementNew.adresseEtablissementNew.etablissementAdresseCodePostalNew
														+ ' ' + etablissementNew.adresseEtablissementNew.etablissementAdresseCommuneNew;
} else if (objet.domicileEntreprise and objet.entrepriseEIRL) {
if (modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateAdresseEIRL']          = date;
}
formFields['new_adresseEIRL']                             = (newAdresseDom.personneLieeAdresseVoieNew != null ? newAdresseDom.personneLieeAdresseVoieNew : '') 
														  + ' ' + (newAdresseDom.personneLieeAdresseIndiceVoieNew != null ? newAdresseDom.personneLieeAdresseIndiceVoieNew : '')
														  + ' ' + (newAdresseDom.personneLieeAdresseTypeVoieNew != null ? newAdresseDom.personneLieeAdresseTypeVoieNew : '')
														  + ' ' + newAdresseDom.personneLieeAdresseNomVoieNew;
formFields['new_adresseEIRL2']							  = (newAdresseDom.personneLieeAdresseComplementAdresseNew != null ? newAdresseDom.personneLieeAdresseComplementAdresseNew : '')
														  + ' ' + (newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null ? newAdresseDom.personneLieeAdresseDistriutionSpecialeNew : '')
														  + ' ' + newAdresseDom.personneLieeAdresseCodePostalNew
														  + ' ' + newAdresseDom.personneLieeAdresseCommuneNew;
}

var finDAP = $p2pl.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.finEIRLGroup;
if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('finEIRL')) { 																							
if (finDAP.finEIRLDeclaration.modifDateFinEIRL !== null) {
    var dateTmp = new Date(parseInt(finDAP.finEIRLDeclaration.modifDateFinEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateFinEIRL']          = dateModif;
}
formFields['finEIRL_renonciationAvecPoursuite']                                             = Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).contains('finEIRL') ? (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLRenonciationAvecPoursuite') ? true : false) : false;
formFields['finEIRL_renonciationSansPoursuite']                                             = Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).contains('finEIRL') ? (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLRenonciationSansPoursuite') ? true : false) : false;
formFields['finEIRL_cessionPP']                                                             = Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).contains('finEIRL') ? (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLCessionPP') ? true : false) : false;
formFields['finEIRL_cessionPM']                                                             = Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).contains('finEIRL') ? (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLCessionPM') ? true : false) : false;
}	

var intentionPoursuite = $p2pl.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.poursuiteEIRLGroup;

if (objet.poursuiteEIRL) {
if (poursuite.modifDateDeces !== null) {
    var dateTmp = new Date(parseInt(poursuite.modifDateDeces.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDatePoursuiteEIRL']          = date;
}
formFields['modif_pouruiteEIRL'] = true;
formFields['eIRL_poursuiteNomPrenom']                      = intentionPoursuite.eIRLPoursuiteNom + ' ' + intentionPoursuite.eIRLPoursuitePrenom;
}

// Cadre 7 - Options fiscales

var fiscalEIRL = $p2pl.cadre4DeclarationAffectationPatrimoineGroup.cadre3OptionsFiscalesEIRL;
if (objet.cadreObjetDeclarationEIRL.declarationEIRL) {
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rsBNC']                                  = (Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesRsBNC') or identite.microEntrepreneurOuiNon)? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_dcBNC']                                  = Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesDcBNC') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBeneficesOptionsParticulieres_optionCreanceDette'] = fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionCreanceDette ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBeneficesOptionsParticulieres_optionIS']           = Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionIS') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rsIS']                                   = Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalIS).eq('RegimeFiscalRegimeImpositionBeneficesRsIS') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rnIS']                                   = Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalIS).eq('RegimeFiscalRegimeImpositionBeneficesRnIS') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVA_rfTVA']                                        = (Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesRsBNC') or identite.microEntrepreneurOuiNon) ? true : (Value('id').of(fiscalEIRL.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARfTVA') ? true : false);
formFields['eirl_regimeFiscal_regimeImpositionTVA_rsTVA']                                        = Value('id').of(fiscalEIRL.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARsTVA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVA_rnTVA']                                        = Value('id').of(fiscalEIRL.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARnTVA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVAOptionsParticulieres1']                         = fiscalEIRL.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres1DC ? true : false;
}

var fiscalEIRLME = $p2pl.cadre4DeclarationAffectationPatrimoineGroup.cadreOptionsFiscalesEIRLME;
if (objet.cadreObjetDeclarationEIRL.declarationEIRL and identite.microEntrepreneurOuiNon) {
formFields['eirl_regimeFiscal_regimeImpositionBeneficesVersementLiberatoire'] = fiscalEIRLME.regimeFiscalRegimeImpositionBeneficesVersementLiberatoire ? true : false;
}

/*
 * Création du dossier avec volet social : ajout du cerfa avec volet social
 */
 
var cerfaDoc1 = nash.doc //
	.load('models/cerfa_11931-05_p2pl_avec_volet_social.pdf') //
	.apply(formFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));

/*
 * Création du dossier sans volet social : ajout du cerfa sans volet social
 */
 
 var cerfaDoc2 = nash.doc //
	.load('models/cerfa_11931-05_p2pl_sans_volet_social.pdf') //
	.apply (formFields);
	cerfaDoc1.append(cerfaDoc2.save('cerfa.pdf'));

/*
 * Ajout de l'intercalaire PEIRL ME avec option fiscale
 */
if (Value('id').of(objet.objetModification).contains('modifEIRL') or objet.entrepriseEIRL or objet.poursuiteEIRL) {
	var peirlMEDoc1 = nash.doc //
		.load('models/cerfa_14218-03_peirl_PL_AC_avec_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(peirlMEDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire PEIRL ME sans option fiscale
 */
if (Value('id').of(objet.objetModification).contains('modifEIRL') or objet.entrepriseEIRL or objet.poursuiteEIRL) {
	var peirlMEDoc2 = nash.doc //
		.load('models/cerfa_14218-03_peirl_PL_AC_sans_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(peirlMEDoc2.save('cerfa.pdf'));
}


/*
 * Ajout des PJs
 */

// PJ Déclarant

var pj=$p2pl.cadre9SignatureGroup.cadre9Signature.soussigne;
var pjUser = [];
var metas = [];

function pushPjPreview(fld) {
	fld.forEach(function (elm) {
        pjUser.push(elm);
		metas.push({'name':'document', 'value': '/'+elm.getAbsolutePath()});
    });
}

if(Value('id').of(pj).contains('FormaliteSignataireQualiteDeclarant')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantSignataire);
}

if (Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire') 
	and (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P') 
	or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P'))) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantNonSignataire);
} 

// PJ Mandataire

if(Value('id').of(pj).contains('FormaliteSignataireQualiteMandataire')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPouvoir);
}

// PJ CONJOINT

if (Value('id').of($p2pl.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationPerso.objetSituationPerso).contains('30P') and (Value('id').of($p2pl.cadre3ModificationConjointGroup.cadre3ModificationConjoint.objetModifStatut).eq('nouveauStatut') or Value('id').of($p2pl.cadre3ModificationConjointGroup.cadre3ModificationConjoint.objetModifStatut).eq('modificationStatut'))) {
 	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjChoixStatutConjoint);
}

// PJ Eirl

var pj=$p2pl.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEIRL.objetEIRL;
if(($p2pl.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetDeclarationEIRL.declarationEIRL and not Value('id').of($p2pl.cadre4DeclarationAffectationPatrimoineGroup.cadreDeclarationAffectationPatrimoine.eirlDepot).eq('eirlSansDepot')) or Value('id').of(pj).contains('modificationEIRL') or $p2pl.cadre1ObjetModificationGroup.cadre1ObjetModification.entrepriseEIRL) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAP);
}

var pj=$p2pl.cadre4DeclarationAffectationPatrimoineGroup;
if(Value('id').of(pj.cadreEIRLInformationsComplementaires.contenuDAP).contains('bienImmo') or Value('id').of(pj.cadreEIRLInformationsComplementairesBis.contenuDAPBis).contains('bienImmo') or Value('id').of($p2pl.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.contenuDAP).contains('bienImmo')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAPActeNotarie);
}

if(Value('id').of(pj.cadreEIRLInformationsComplementaires.contenuDAP).contains('bienCommunIndivis') or Value('id').of(pj.cadreEIRLInformationsComplementairesBis.contenuDAPBis).contains('bienCommunIndivis') or Value('id').of($p2pl.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.contenuDAP).contains('bienCommunIndivis')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAPAccordTiers);
}

/*
 * Enregistrement du fichier (en mémoire)
 */

var finalDoc = cerfaDoc1.save('P2PL_Modification.pdf');

// Remove old metas before insert
var metasToDelete = [];
var recordMetas = nash.record.meta() != null ? nash.record.meta().metas : null;
var recordMetasSize = recordMetas != null ? recordMetas.size() : 0;

for (var i = 0; i < recordMetasSize; i++) {
   if (recordMetas.get(i).name == 'document' && !recordMetas.get(i).value.contains("proxy")) {
       metasToDelete.push({'name':'document', 'value': recordMetas.get(i).value});
   }
}
nash.record.removeMeta(metasToDelete);

nash.record.meta(metas);

var data = [ spec.createData({
    id : 'record',
    label : 'Déclaration de modification d\'une entreprise libérale ou assimilée ou d\'un artiste-auteur',
    description : 'Voici le formulaire obtenu à partir des données saisies.',
    type : 'FileReadOnly',
    value : [ finalDoc ]
}),  spec.createData({
    id : 'attachments',
    label : 'Pièces jointes',
    description : 'Pièces jointes ajoutées au formulaire.',
    type : 'FileReadOnly',
    value : pjUser
}) ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Déclaration de modification d\'une entreprise libérale ou assimilée ou d\'un artiste-auteur',
    groups : groups
});