function pad(s) { return (s < 10) ? '0' + s : s; }
function sanitize(s){return s.replaceAll("’","'").replaceAll("–","-").replaceAll("­","");}

var regEx = new RegExp('[0-9]{5}'); 
var objet= $p2pl.cadre1ObjetModificationGroup.cadre1ObjetModification;
var identite= $p2pl.cadre1RappelIdentificationGroup.cadre1RappelIdentification;
var modifIdentiteDeclarant = $p2pl.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle;
var etablissement = $p2pl.cadre6EtablissementGroup.infoEtablissementOld;
var newAdresseDom = $p2pl.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifDomicile.newAdresseDomicile;
var newAdresseDom2 = $p2pl.cadre6EtablissementGroup.infoEtablissementNew.adresseEtablissementNew;
var poursuite = $p2pl.cadreDecesAvecPoursuiteGroup.cadreDecesAvecPoursuite;
var conjoint = $p2pl.cadre3ModificationConjointGroup.cadre3ModificationConjoint;
var adresseConjoint = $p2pl.cadre3ModificationConjointGroup.cadre3ModificationConjoint.adresseDomicileConjoint;
var activiteInfo = $p2pl.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite;
var adresseEntreprise = $p2pl.cadre1RappelIdentificationGroup.cadre1RappelIdentification.adresseEtablissementPrincipal;
var etablissementNew =$p2pl.cadre6EtablissementGroup.infoEtablissementNew;
var activite = $p2pl.cadre7EtablissementActiviteGroup.cadre7EtablissementActivite.cadreActivite;
var origine = $p2pl.cadre7EtablissementActiviteGroup.cadre4OrigineFonds ;
var correspondance = $p2pl.cadre8RensCompGroup.cadre8RensComp;
var signataire = $p2pl.cadre9SignatureGroup.cadre9Signature;
var declarationAffectation = $p2pl.cadre4DeclarationAffectationPatrimoineGroup.cadreDeclarationAffectationPatrimoine;
var rappelIdentification = $p2pl.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreRappelIdentificationEIRL;
var modifEIRL = $p2pl.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL;                                         
var finDAP = $p2pl.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.finEIRLGroup;
var fiscalEIRL = $p2pl.cadre4DeclarationAffectationPatrimoineGroup.cadre3OptionsFiscalesEIRL;
var fiscalEIRLME = $p2pl.cadre4DeclarationAffectationPatrimoineGroup.cadreOptionsFiscalesEIRLME;
var intentionPoursuite = $p2pl.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.poursuiteEIRLGroup;


var regentVersion = "V2008.11";  // (V2008.11, V2016.02) 
var eventRegent = []; 
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P')) {
eventRegent.push("10P");
}
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) {
eventRegent.push("15P");
}
if ((Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') or objet.entrepriseDomicile)) {
eventRegent.push("16P");
}
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('17P')) { 
eventRegent.push("17P");
}
if (Value('id').of(objet.objetModification).contains('dateActivite') and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 != null or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 != null)) {
eventRegent.push("20P");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')) {
eventRegent.push("21P");
}
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('22P')) { 
eventRegent.push("22P");
}
if (Value('id').of(objet.objetModification).contains('modifEIRL') or objet.entrepriseEIRL) {
eventRegent.push("25P");
}
if (Value('id').of(objet.objetModification).eq('situationPerso')
	and Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).eq('30P')
	and Value('id').of(conjoint.objetModifConjoint).eq('conjointSalarie')
	and (Value('id').of(conjoint.objetModifStatut).eq('suppressionStatut')
	or Value('id').of(conjoint.objetModifStatut).eq('nouveauStatut'))) { 
eventRegent.push("29P");
}
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('30P')
	and (Value('id').of(conjoint.objetModifConjoint).eq('conjointCollaborateur')
	or Value('id').of(conjoint.objetModifStatut).eq('modificationStatut'))) { 
eventRegent.push("30P");
}
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('37P')) { 
eventRegent.push("37P");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('40P')) {
eventRegent.push("40P");
}
if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')) or objet.domicileEntreprise) {
eventRegent.push("11P");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')
	and Value('id').of(etablissementNew.origineEtablissement).eq('etablissementOuverture')) 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')
	and Value('id').of(etablissementNew.origineEtablissement).eq('etablissementOuverture')
	and not Value('id').of(etablissement.destinationEtablissement2).eq('vendu') and not Value('id').of(etablissement.destinationEtablissement2).eq('ferme'))
	or objet.domicileEntreprise) {
eventRegent.push("54P");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')
	and Value('id').of(etablissementNew.origineEtablissement).eq('etablissementOuverture')
	and (Value('id').of(etablissement.destinationEtablissement2).eq('vendu') or Value('id').of(etablissement.destinationEtablissement2).eq('ferme'))) {
eventRegent.push("56");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')
	and (Value('id').of(etablissement.destinationEtablissement1).eq('vendu') or Value('id').of(etablissement.destinationEtablissement1).eq('ferme'))) 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')
	and not Value('id').of(etablissementNew.origineEtablissement).eq('etablissementOuverture')
	and (Value('id').of(etablissement.destinationEtablissement2).eq('vendu') or Value('id').of(etablissement.destinationEtablissement2).eq('ferme')))
	or objet.domicileEntreprise) {
eventRegent.push("80P");
}
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P') or activiteInfo.modifEtablissementEnseigne) {
eventRegent.push("60P");
}
if ((Value('id').of(activite.activiteEvenement).eq('61P') or Value('id').of(activite.activiteEvenement).eq('61P62P'))) {
eventRegent.push("61P");
}
if ((Value('id').of(activite.activiteEvenement).eq('62P') or Value('id').of(activite.activiteEvenement).eq('61P62P'))) {
eventRegent.push("62P");
}
if (Value('id').of(activite.activiteEvenement).eq('67P')) {
eventRegent.push("67P");
}


// Valeurs à compléter par Directory  
var authorityType = "CFE"; // (CFE, TDR) 
var authorityId = _INPUT_.dataGroup.codeEDI; // (code EDI) 


//Ajout du type de la deuxième autorité
var authorityType2 = "TDR"; // (CFE, TDR) 
//Ajout du code de la deuxième autorité
var authorityId2 = _INPUT_.dataGroup.codeEDI2; // (code EDI) 


var regentFields = {}; 

// A mettre toujours sous "Z1611"
regentFields['/REGENT-XML/Emetteur']="Z1611";

// A récuperer depuis directory
regentFields['/REGENT-XML/Destinataire']= authorityId;

// Completé par directory
regentFields['/REGENT-XML/DateHeureEmission']= null;
regentFields['/REGENT-XML/VersionMessage']= null;
regentFields['/REGENT-XML/VersionNorme']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Specification/NomService']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Specification/VersionService']= null;

					// Groupe GDF : Groupe données de service
					
// Sous groupe IDF : Identification de la formalité

// Généré par destiny
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C01']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C03']= "0";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C04']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05']= "L";


// Sous groupe EDF : Evènement déclaré

var occurrencesC10 = [];
var occurrence = {}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P')) {
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[1]/C10.1']= "10P";
occurrence['C10.1'] = '10P';
if(modifIdentiteDeclarant.modifIdentite.modifDateIdentite !== null) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifIdentite.modifDateIdentite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[1]/C10.2']= date;
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) {
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[2]/C10.1']= "15P";
occurrence['C10.1'] = '15P';
if(modifIdentiteDeclarant.modifIdentite.modifDateIdentite !== null) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifIdentite.modifDateIdentite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[2]/C10.2']= date;
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') or objet.entrepriseDomicile) {
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[6]/C10.1']= "16P";
occurrence['C10.1'] = '16P';
if (modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null and Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P')) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[6]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (etablissement.modifDateAdresseEntreprise !== null and objet.entrepriseDomicile) {
	var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[6]/C10.2']= date;
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('17P')) {
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[3]/C10.1']= "17P";
occurrence['C10.1'] = '17P';
if(modifIdentiteDeclarant.modifNationalite.modifDateNationalite !== null) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifNationalite.modifDateNationalite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[3]/C10.2']= date;
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(objet.objetModification).contains('dateActivite') and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 != null or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 != null)) {
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[5]/C10.1']= "20P";
occurrence['C10.1'] = '20P';
if (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 !== null) {
	var dateTemp = new Date(parseInt(objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[5]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 !== null) {
	var dateTemp = new Date(parseInt(objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[5]/C10.2']= date;
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')) {
	//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[5]/C10.1']= "21P";
occurrence['C10.1'] = '21P';
if (objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise !== null) {
	var dateTemp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[5]/C10.2']= date;
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('22P')) { 
	//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[5]/C10.1']= "22P";
occurrence['C10.1'] = '22P';
if (poursuite.modifDateDeces !== null) {
	var dateTemp = new Date(parseInt(poursuite.modifDateDeces.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[5]/C10.2']= date;
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(objet.objetModification).contains('modifEIRL') or objet.entrepriseEIRL) {
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[7]/C10.1']= "25P";
occurrence['C10.1'] = '25P';
if (declarationAffectation.modifDateDeclarationEIRL !== null) {
	var dateTemp = new Date(parseInt(declarationAffectation.modifDateDeclarationEIRL.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[7]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (modifEIRL.modifDateEIRL !== null) {
	var dateTemp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[7]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (objet.poursuiteEIRL and poursuite.modifDateDeces !== null) {
	var dateTemp = new Date(parseInt(poursuite.modifDateDeces.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[7]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (finDAP.finEIRLDeclaration.modifDateFinEIRL !== null) {
	var dateTemp = new Date(parseInt(finDAP.finEIRLDeclaration.modifDateFinEIRL.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[7]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (objet.entrepriseEIRL and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissement.modifDateAdresseEntreprise !== null) {
	var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[7]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (objet.entrepriseEIRL and modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[7]/C10.2']= date;
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(objet.objetModification).eq('situationPerso')
	and Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).eq('30P')
	and Value('id').of(conjoint.objetModifConjoint).eq('conjointSalarie')
	and (Value('id').of(conjoint.objetModifStatut).eq('suppressionStatut')
	or Value('id').of(conjoint.objetModifStatut).eq('nouveauStatut'))) { 
occurrence['C10.1'] = '29P';
if (conjoint.modifDateConjoint !== null) {
	var dateTemp = new Date(parseInt(conjoint.modifDateConjoint.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[4]/C10.2']= date;
occurrence['C10.2'] = date;
}
if (Value('id').of(conjoint.objetModifStatut).eq('nouveauStatut')) { 
occurrence['C10.3'] = "Déclaration du statut de conjoint salarié";
} else {
occurrence['C10.3'] = "Suppression du statut de conjoint salarié";
}
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('30P')
	and (Value('id').of(conjoint.objetModifConjoint).eq('conjointCollaborateur')
	or Value('id').of(conjoint.objetModifStatut).eq('modificationStatut'))) { 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[4]/C10.1']= "30P";
occurrence['C10.1'] = '30P';
if (conjoint.modifDateConjoint !== null) {
	var dateTemp = new Date(parseInt(conjoint.modifDateConjoint.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[4]/C10.2']= date;
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('37P')) { 
	//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[5]/C10.1']= "37P";
occurrence['C10.1'] = '37P';
if (modifIdentiteDeclarant.modifContratAppui.modifDateAppui !== null) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifContratAppui.modifDateAppui.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[5]/C10.2']= date;
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('40P')) {
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[8]/C10.1']= "40P";
occurrence['C10.1'] = '40P';
if(objet.cadreObjetModificationEtablissement.cadreObjet40P.dateMiseEnSommeil !== null and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('40P')) {
	var dateTemp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet40P.dateMiseEnSommeil.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[8]/C10.2']= date;
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {};
}

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')) or objet.domicileEntreprise) {
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[8]/C10.1']= "11P";
occurrence['C10.1'] = '11P';
if(etablissement.modifDateAdresseEntreprise !== null and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).contains('transfert')) {
	var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[8]/C10.2']= date;
occurrence['C10.2'] = date;
} else if(modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null and objet.domicileEntreprise) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[8]/C10.2']= date;
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')
	and Value('id').of(etablissementNew.origineEtablissement).eq('etablissementOuverture')) 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')
	and Value('id').of(etablissementNew.origineEtablissement).eq('etablissementOuverture')
	and not Value('id').of(etablissement.destinationEtablissement2).eq('vendu') and not Value('id').of(etablissement.destinationEtablissement2).eq('ferme') and not Value('id').of(etablissement.destinationEtablissement2).eq('autre'))
	or objet.domicileEntreprise) {
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[9]/C10.1']= "54P";
occurrence['C10.1'] = '54P';
if(etablissement.modifDateAdresseEntreprise !== null and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).contains('transfert')) {
	var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[9]/C10.2']= date;
occurrence['C10.2'] = date;
} else if(modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null and objet.domicileEntreprise) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[9]/C10.2']= date;
occurrence['C10.2'] = date;
} else if(etablissementNew.modifDateAdresseOuverture !== null and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')) {
	var dateTemp = new Date(parseInt(etablissementNew.modifDateAdresseOuverture.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[9]/C10.2']= date;
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')
	and Value('id').of(etablissementNew.origineEtablissement).eq('etablissementOuverture')
	and (Value('id').of(etablissement.destinationEtablissement2).eq('vendu') or Value('id').of(etablissement.destinationEtablissement2).eq('ferme')
	or Value('id').of(etablissement.destinationEtablissement2).eq('autre'))) {
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[10]/C10.1']= "56P";
occurrence['C10.1'] = '56P';
if(etablissement.modifDateAdresseEntreprise !== null) {
	var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[10]/C10.2']= date;
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')
	and (Value('id').of(etablissement.destinationEtablissement1).eq('vendu') 
	or Value('id').of(etablissement.destinationEtablissement1).eq('ferme') 
	or Value('id').of(etablissement.destinationEtablissement1).eq('autre') )) 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')
	and not Value('id').of(etablissementNew.origineEtablissement).eq('etablissementOuverture')
	and (Value('id').of(etablissement.destinationEtablissement2).eq('vendu') 
	or Value('id').of(etablissement.destinationEtablissement2).eq('ferme')
	or Value('id').of(etablissement.destinationEtablissement2).eq('autre')))
	or objet.domicileEntreprise) {
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[10]/C10.1']= "80P";
occurrence['C10.1'] = '80P';
if(modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null and objet.domicileEntreprise) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[9]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (etablissement.modifDateAdresseEntreprise !== null) {
	var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[10]/C10.2']= date;
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P') or activiteInfo.modifEtablissementEnseigne) {
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[11]/C10.1']= "60P";
occurrence['C10.1'] = '60P';
if (activiteInfo.cadreEtablissementIdentification.modifDateIdentification !== null) {
	var dateTemp = new Date(parseInt(activiteInfo.cadreEtablissementIdentification.modifDateIdentification.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[11]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).contains('21P') and objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise !== null) {
	var dateTemp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[11]/C10.2']= date;
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(activite.activiteEvenement).eq('61P') or Value('id').of(activite.activiteEvenement).eq('61P62P')) {
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[11]/C10.1']= "61P";
occurrence['C10.1'] = '61P';
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite') 
	and activite.modifDateActivite !== null) {
	var dateTemp = new Date(parseInt(activite.modifDateActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[11]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and etablissement.modifDateAdresseEntreprise !== null) {
	var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[11]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (objet.cadreObjetModificationEtablissement.cadreObjet21P.activiteReprise 
	and objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise !== null) {
	var dateTemp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[11]/C10.2']= date;
occurrence['C10.2'] = date;
} 
 
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(activite.activiteEvenement).eq('62P') or Value('id').of(activite.activiteEvenement).eq('61P62P')) {
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[12]/C10.1']= "62P";
occurrence['C10.1'] = '62P';
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite') 
	and activite.modifDateActivite !== null) {
	var dateTemp = new Date(parseInt(activite.modifDateActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[11]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and etablissement.modifDateAdresseEntreprise !== null) {
	var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[11]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (objet.cadreObjetModificationEtablissement.cadreObjet21P.activiteReprise 
	and objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise !== null) {
	var dateTemp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[11]/C10.2']= date;
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(activite.activiteEvenement).eq('67P')) {
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[13]/C10.1']= "67P";
occurrence['C10.1'] = '67P';
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite') 
	and activite.modifDateActivite !== null) {
	var dateTemp = new Date(parseInt(activite.modifDateActivite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[11]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and etablissement.modifDateAdresseEntreprise !== null) {
	var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[11]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (objet.cadreObjetModificationEtablissement.cadreObjet21P.activiteReprise 
	and objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise !== null) {
	var dateTemp = new Date(parseInt(objet.cadreObjetModificationEtablissement.cadreObjet21P.dateReprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[11]/C10.2']= date;
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {}; 
}

occurrencesC10.forEach(function (value, i) {
	var idx = i+1;
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[' + idx + ']/C10.1']= value['C10.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[' + idx + ']/C10.2']= value['C10.2'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[' + idx + ']/C10.3']= value['C10.3'];	
});

// Sous groupe DMF : Destinataire de la formalité
   
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[1]']= authorityId;
if (authorityId != "" and authorityId2 != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[2]']= authorityId2;
}

// Sous groupe ADF :  adresse de correspondance

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C36']= correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance != null ? 
																	 correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance : 
																	((modifIdentiteDeclarant.modifIdentite.modifNouveauNomUsage != null 
																	and Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) ?
																	(modifIdentiteDeclarant.modifIdentite.modifNouveauNomUsage 
																	+ ' ' + (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom') ? modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms[0] : identite.personneLieePersonnePhysiquePrenom1[0])) :
																	((identite.personneLieePersonnePhysiqueNomUsage != null
																	and not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) ?
																	(identite.personneLieePersonnePhysiqueNomUsage 
																	+ ' ' + (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom') ? modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms[0] : identite.personneLieePersonnePhysiquePrenom1[0])) :
																	((Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifNomNaissance') ? modifIdentiteDeclarant.modifIdentite.modifNouveauNomNaissance : identite.personneLieePersonnePhysiqueNomNaissance) + ' ' + (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom') ? modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms[0] : identite.personneLieePersonnePhysiquePrenom1[0]))));
																	
if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']= correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune.getId();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('prof')) {
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']= adresseEntreprise.etablissementAdresseCommune.getId();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']=etablissement.adresseEtablissementOld.etablissementAdresseCommuneOld.getId();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']=etablissementNew.adresseEtablissementNew.etablissementAdresseCommuneNew.getId();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('domi') and Value('id').of(newAdresseDom.personneLieeAdressePaysNew).eq('FRXXXXX')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']= newAdresseDom.personneLieeAdresseCommuneNew.getId();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('domi') and not Value('id').of(newAdresseDom.personneLieeAdressePaysNew).eq('FRXXXXX')) {
	var idPays = newAdresseDom.personneLieeAdressePaysNew.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']= result;
}
																	
if ((Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adresseEntreprise.etablissementAdresseNumeroVoie != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement') and etablissement.adresseEtablissementOld.etablissementAdresseNumeroVoieOld != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') and etablissementNew.adresseEtablissementNew.etablissementAdresseNumeroVoieNew != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('domi') and newAdresseDom.personneLieeAdresseVoieNew != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.5']= Value('id').of(correspondance.adresseCorrespond).eq('autre') ? correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance :
																			(Value('id').of(correspondance.adresseCorrespond).eq('prof') ? adresseEntreprise.etablissementAdresseNumeroVoie : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement') ? etablissement.adresseEtablissementOld.etablissementAdresseNumeroVoieOld : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') ? etablissementNew.adresseEtablissementNew.etablissementAdresseNumeroVoieNew : newAdresseDom.personneLieeAdresseVoieNew)));
}
	
if (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance != null) {
   var monId1 = Value('id').of(correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId1;  
} else if (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adresseEntreprise.etablissementAdresseIndiceVoie != null) {
   var monId2 = Value('id').of(adresseEntreprise.etablissementAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId2;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement') and etablissement.adresseEtablissementOld.etablissementAdresseIndiceVoieOld != null) {
   var monId3 = Value('id').of(etablissement.adresseEtablissementOld.etablissementAdresseIndiceVoieOld)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId3;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') and etablissementNew.adresseEtablissementNew.etablissementAdresseIndiceVoieNew != null) {
   var monId4 = Value('id').of(etablissementNew.adresseEtablissementNew.etablissementAdresseIndiceVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId4;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('domi') and newAdresseDom.personneLieeAdresseIndiceVoieNew != null) {
	var monId5 = Value('id').of(newAdresseDom.personneLieeAdresseIndiceVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId5;
}

if ((Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement') and etablissement.adresseEtablissementOld.etablissementAdresseDistriutionSpecialeVoieOld != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') and etablissementNew.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('domi') and newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.7']= Value('id').of(correspondance.adresseCorrespond).eq('autre') ? correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance :
																			(Value('id').of(correspondance.adresseCorrespond).eq('prof') ? adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement') ? etablissement.adresseEtablissementOld.etablissementAdresseDistriutionSpecialeVoieOld : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') ? etablissementNew.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew : newAdresseDom.personneLieeAdresseDistriutionSpecialeNew)));
}

if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8']= correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('prof')) {
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8']= adresseEntreprise.etablissementAdresseCodePostal;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8']=etablissement.adresseEtablissementOld.etablissementAdresseCodePostalOld;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8']=etablissementNew.adresseEtablissementNew.etablissementAdresseCodePostalNew;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('domi')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8']= newAdresseDom.personneLieeAdresseCodePostalNew != null ? newAdresseDom.personneLieeAdresseCodePostalNew : ".";
} 

if ((Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adresseEntreprise.etablissementAdresseComplementVoie != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement') and etablissement.adresseEtablissementOld.etablissementAdresseComplementVoieOld != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') and etablissementNew.adresseEtablissementNew.etablissementAdresseComplementVoieNew != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('domi') and newAdresseDom.personneLieeAdresseComplementAdresseNew != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.10']= Value('id').of(correspondance.adresseCorrespond).eq('autre') ? correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance :
																			(Value('id').of(correspondance.adresseCorrespond).eq('prof') ? adresseEntreprise.etablissementAdresseComplementVoie : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement') ? etablissement.adresseEtablissementOld.etablissementAdresseComplementVoieOld : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') ? etablissementNew.adresseEtablissementNew.etablissementAdresseComplementVoieNew :
																			newAdresseDom.personneLieeAdresseComplementAdresseNew)));
}

if (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance != null) {
   var monId1 = Value('id').of(correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId1;  
} else if (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adresseEntreprise.etablissementAdresseTypeVoie != null) {
   var monId2 = Value('id').of(adresseEntreprise.etablissementAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId2;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement') and etablissement.adresseEtablissementOld.etablissementAdresseTypeVoieOld != null) {
   var monId3 = Value('id').of(etablissement.adresseEtablissementOld.etablissementAdresseTypeVoieOld)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId3;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') and etablissementNew.adresseEtablissementNew.etablissementAdresseTypeVoieNew != null) {
   var monId4 = Value('id').of(etablissementNew.adresseEtablissementNew.etablissementAdresseTypeVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId4;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('domi') and newAdresseDom.personneLieeAdresseTypeVoieNew != null) {
	var monId5 = Value('id').of(newAdresseDom.personneLieeAdresseTypeVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId5;
}

if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12']= correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('prof')) {
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12']= adresseEntreprise.etablissementAdresseNomVoie;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12']=etablissement.adresseEtablissementOld.etablissementAdresseNomVoieOld;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12']=etablissementNew.adresseEtablissementNew.etablissementAdresseNomVoieNew;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('domi')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12']= newAdresseDom.personneLieeAdresseNomVoieNew;
} 

if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune.getLabel();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('prof')) {
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= adresseEntreprise.etablissementAdresseCommune.getLabel();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('ancienEtablissement')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']=etablissement.adresseEtablissementOld.etablissementAdresseCommuneOld.getLabel();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']=etablissementNew.adresseEtablissementNew.etablissementAdresseCommuneNew.getLabel();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('domi')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= newAdresseDom.personneLieeAdresseCommuneNew != null ? newAdresseDom.personneLieeAdresseCommuneNew.getLabel() : newAdresseDom.personneLieeAdresseVilleNew;;
} 

if (not Value('id').of(newAdresseDom.personneLieeAdressePaysNew).eq('FRXXXXX') and newAdresseDom.personneLieeAdressePaysNew !=null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= newAdresseDom.personneLieeAdressePaysNew.getLabel();
}


regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.1[1]']= correspondance.infosSup.formaliteTelephone2.e164.replace('+', '00');

if (correspondance.infosSup.formaliteTelephone1 != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.1[2]']= correspondance.infosSup.formaliteTelephone1.e164.replace('+', '00');
}

if (correspondance.infosSup.telecopie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.2']= correspondance.infosSup.telecopie.e164.replace('+', '00');
}

if (correspondance.infosSup.formaliteFaxCourriel != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.3']= correspondance.infosSup.formaliteFaxCourriel;
}


// Sous groupe SIF : Signature de la formalité

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.1']= Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? 
																			signataire.adresseMandataire.nomPrenomDenominationMandataire : 
																			((modifIdentiteDeclarant.modifIdentite.modifNouveauNomUsage != null 
																			and Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) ?
																			(modifIdentiteDeclarant.modifIdentite.modifNouveauNomUsage 
																			+ ' ' + (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom') ? modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms[0] : identite.personneLieePersonnePhysiquePrenom1[0])) :
																			((identite.personneLieePersonnePhysiqueNomUsage != null
																			and not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) ?
																			(identite.personneLieePersonnePhysiqueNomUsage 
																			+ ' ' + (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom') ? modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms[0] : identite.personneLieePersonnePhysiquePrenom1[0])) :
																			((Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifNomNaissance') ? modifIdentiteDeclarant.modifIdentite.modifNouveauNomNaissance : identite.personneLieePersonnePhysiqueNomNaissance) + ' ' + (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom') ? modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms[0] : identite.personneLieePersonnePhysiquePrenom1[0]))));

if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.2']= "Mandataire";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.3']= signataire.adresseMandataire.villeAdresseMandataire.getId();

if (signataire.adresseMandataire.numeroVoieMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.5']= signataire.adresseMandataire.numeroVoieMandataire;
}

if (signataire.adresseMandataire.indiceVoieMandataire !== null) {
   var monId = Value('id').of(signataire.adresseMandataire.indiceVoieMandataire)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.6']          = monId;
}

if (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.7']= signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.8']= signataire.adresseMandataire.dataCodePostalMandataire;

if (signataire.adresseMandataire.complementVoieMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.10']= signataire.adresseMandataire.complementVoieMandataire;
}

if (signataire.adresseMandataire.typeVoieMandataire !== null) {
   var monId = Value('id').of(signataire.adresseMandataire.typeVoieMandataire)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.11']          = monId;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.12']= signataire.adresseMandataire.nomVoieMandataire;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.13']= signataire.adresseMandataire.villeAdresseMandataire.getLabel();
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C41']= signataire.formaliteSignatureLieu;

if(signataire.formaliteSignatureDate !== null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C42']= date;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C43']= (identite.microEntrepreneurOuiNon ? "ME=O!" : "ME=N!") 
+ '' + (Value('id').of(signataire.diffusionInfo).eq('formaliteNonDiffusionInformationOui') ? "C45=O!" : "C45=N!") 
+ ' ' + (correspondance.formaliteObservations != null ? sanitize(correspondance.formaliteObservations) : '') ;

							
								// Groupe ICP : Identification complète de la personne physique

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.2']= (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P') and Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifNomNaissance')) ? 
																					modifIdentiteDeclarant.modifIdentite.modifNouveauNomNaissance : identite.personneLieePersonnePhysiqueNomNaissance;

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P') and Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom')) {
var prenoms=[];
for ( i = 0; i < modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms.size() ; i++ ){prenoms.push(modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.3']= prenoms;
} else {
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.3']= prenoms;
} 

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P') and Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPseudonyme).contains('modifNomUsage') and modifIdentiteDeclarant.modifIdentite.modifNouveauNomUsage != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P02/P02.1']= modifIdentiteDeclarant.modifIdentite.modifNouveauNomUsage;
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P') and identite.personneLieePersonnePhysiqueNomUsage != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P02/P02.1']= identite.personneLieePersonnePhysiqueNomUsage;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P') and Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPseudonyme).contains('modifPseudonyme') and modifIdentiteDeclarant.modifIdentite.modifNouveauPseudonyme != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P02/P02.2']= modifIdentiteDeclarant.modifIdentite.modifNouveauPseudonyme;
} 

if(identite.personneLieePersonnePhysiqueDateNaissance !== null) {
	var dateTemp = new Date(parseInt(identite.personneLieePersonnePhysiqueDateNaissance.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.1']= date;
}

if (not Value('id').of(identite.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = identite.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.2']= result;
} else if (Value('id').of(identite.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.2'] = identite.personneLieePPLieuNaissanceCommune.getId();
}	

if (not Value('id').of(identite.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.3']= identite.personneLieePPLieuNaissancePays.getLabel();
}

if (Value('id').of(identite.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.4']= identite.personneLieePPLieuNaissanceCommune.getLabel();
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') or objet.entrepriseDomicile) {
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P')) {
if (Value('id').of(newAdresseDom.personneLieeAdressePaysNew).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.3']= newAdresseDom.personneLieeAdresseCommuneNew.getId();
} else if (not Value('id').of(newAdresseDom.personneLieeAdressePaysNew).eq('FRXXXXX')) {
	var idPays = newAdresseDom.personneLieeAdressePaysNew.getId();
	var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.3']= result;
}
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and objet.entrepriseDomicile) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.3']= newAdresseDom2.etablissementAdresseCommuneNew.getId();
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and newAdresseDom.personneLieeAdresseVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.5']= newAdresseDom.personneLieeAdresseVoieNew;
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and objet.entrepriseDomicile and newAdresseDom2.etablissementAdresseNumeroVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.5']= newAdresseDom2.etablissementAdresseNumeroVoieNew;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and newAdresseDom.personneLieeAdresseIndiceVoieNew != null) {
   var monId1 = Value('id').of(newAdresseDom.personneLieeAdresseIndiceVoieNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.6']          = monId1;
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and objet.entrepriseDomicile and newAdresseDom2.etablissementAdresseIndiceVoieNew != null) {
   var monId2 = Value('id').of(newAdresseDom2.etablissementAdresseIndiceVoieNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.6']          = monId2;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.7']= newAdresseDom.personneLieeAdresseDistriutionSpecialeNew;
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and objet.entrepriseDomicile and newAdresseDom2.etablissementAdresseDistriutionSpecialeVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.7']= newAdresseDom2.etablissementAdresseDistriutionSpecialeVoieNew;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.8']= newAdresseDom.personneLieeAdresseCodePostalNew != null ? newAdresseDom.personneLieeAdresseCodePostalNew : ".";
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and objet.entrepriseDomicile and newAdresseDom2.etablissementAdresseCodePostalNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.8']= newAdresseDom2.etablissementAdresseCodePostalNew;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and newAdresseDom.personneLieeAdresseComplementAdresseNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.10']= newAdresseDom.personneLieeAdresseComplementAdresseNew;
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and objet.entrepriseDomicile and newAdresseDom2.etablissementAdresseComplementVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.10']= newAdresseDom2.etablissementAdresseComplementVoieNew;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and newAdresseDom.personneLieeAdresseTypeVoieNew != null) {
   var monId1 = Value('id').of(newAdresseDom.personneLieeAdresseTypeVoieNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.11']          = monId1;
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and objet.entrepriseDomicile and newAdresseDom2.etablissementAdresseTypeVoieNew != null) {
   var monId2 = Value('id').of(newAdresseDom2.etablissementAdresseTypeVoieNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.11']          = monId2;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.12']= newAdresseDom.personneLieeAdresseNomVoieNew;
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and objet.entrepriseDomicile) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.12']= newAdresseDom2.etablissementAdresseNomVoieNew;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.13']= newAdresseDom.personneLieeAdresseCommuneNew != null ? newAdresseDom.personneLieeAdresseCommuneNew.getLabel() : newAdresseDom.personneLieeAdresseVilleNew;
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and objet.entrepriseDomicile) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.13']= newAdresseDom2.etablissementAdresseCommuneNew.getLabel();
}

if (not Value('id').of(newAdresseDom.personneLieeAdressePaysNew).eq('FRXXXXX') and newAdresseDom.personneLieeAdressePaysNew !=null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.14']= newAdresseDom.personneLieeAdressePaysNew.getLabel();
}

if (modifIdentiteDeclarant.modifDomicile.domicileMemeDept != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P05']= modifIdentiteDeclarant.modifDomicile.domicileMemeDept.getId();
}
}

if (activite.etablissementNonSedentariteQualiteNonSedentaire) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P06']= "A";
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).contains('modificationEIRL') 
	or Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).contains('finEIRL') 
	or objet.entrepriseEIRL or objet.poursuiteEIRL) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.1']= "O";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.2']= rappelIdentification.eirlRappelDenomination;

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.3']= rappelIdentification.cadreEirlRappelAdresse.communeAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.communeAdresseEirl.getId() : adresseEntreprise.etablissementAdresseCommune.getId();

if (not objet.cadreObjetModificationEIRL.memeAdresseEIRL and rappelIdentification.cadreEirlRappelAdresse.numeroAdresseEirl != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.5']= rappelIdentification.cadreEirlRappelAdresse.numeroAdresseEirl;
} else if ((objet.cadreObjetModificationEIRL.memeAdresseEIRL or objet.entrepriseEIRL) and adresseEntreprise.etablissementAdresseNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.5']= adresseEntreprise.etablissementAdresseNumeroVoie;
}

if (not objet.cadreObjetModificationEIRL.memeAdresseEIRL and rappelIdentification.cadreEirlRappelAdresse.indiceVoieAdresseEirl != null) {
   var monId1 = Value('id').of(rappelIdentification.cadreEirlRappelAdresse.indiceVoieAdresseEirl)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.6']          = monId1;
} else if ((objet.cadreObjetModificationEIRL.memeAdresseEIRL or objet.entrepriseEIRL) and adresseEntreprise.etablissementAdresseIndiceVoie != null) {
   var monId2 = Value('id').of(adresseEntreprise.etablissementAdresseIndiceVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.6']          = monId2;
}

if (not objet.cadreObjetModificationEIRL.memeAdresseEIRL and rappelIdentification.cadreEirlRappelAdresse.distriutionSpecialeAdresseEirl != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.7']= rappelIdentification.cadreEirlRappelAdresse.distriutionSpecialeAdresseEirl;
} else if ((objet.cadreObjetModificationEIRL.memeAdresseEIRL or objet.entrepriseEIRL) and adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.7']= adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.8']= rappelIdentification.cadreEirlRappelAdresse.codePostalAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.codePostalAdresseEirl : adresseEntreprise.etablissementAdresseCodePostal;

if (not objet.cadreObjetModificationEIRL.memeAdresseEIRL and rappelIdentification.cadreEirlRappelAdresse.complementAdresseEirl != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.10']= rappelIdentification.cadreEirlRappelAdresse.complementAdresseEirl;
} else if ((objet.cadreObjetModificationEIRL.memeAdresseEIRL or objet.entrepriseEIRL) and adresseEntreprise.etablissementAdresseComplementVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.10']= adresseEntreprise.etablissementAdresseComplementVoie;
}

if (not objet.cadreObjetModificationEIRL.memeAdresseEIRL and rappelIdentification.cadreEirlRappelAdresse.typeVoieAdresseEirl != null) {
   var monId1 = Value('id').of(rappelIdentification.cadreEirlRappelAdresse.typeVoieAdresseEirl)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.11']          = monId1;
} else if ((objet.cadreObjetModificationEIRL.memeAdresseEIRL or objet.entrepriseEIRL) and adresseEntreprise.etablissementAdresseTypeVoie != null) {
   var monId2 = Value('id').of(adresseEntreprise.etablissementAdresseTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.11']          = monId2;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.12']= rappelIdentification.cadreEirlRappelAdresse.nomVoieAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.nomVoieAdresseEirl : adresseEntreprise.etablissementAdresseNomVoie;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.13']= rappelIdentification.cadreEirlRappelAdresse.communeAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.communeAdresseEirl.getLabel() : adresseEntreprise.etablissementAdresseCommune.getLabel();

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.4']= "3";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.5']= rappelIdentification.eirlRappelLieuImmatriculation;
}


								// Groupe AIP : Ancienne identification de la personne physique 

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P11']= identite.personneLieePersonnePhysiqueNomNaissance;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P') and identite.personneLieePersonnePhysiqueNomUsage != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P13']= identite.personneLieePersonnePhysiqueNomUsage;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P')) {
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P14']= prenoms;
}

								// Groupe NAP : Nationalité de la personne physique
								
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('17P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/NAP/P21']= modifIdentiteDeclarant.modifNationalite.modifNewNationalite;
}

								// Groupe IPP : Intention de poursuite de l'EIRL

if (objet.poursuiteEIRL) {								
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/IPP/P92']= "O";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/IPP/P93/P93.1']= intentionPoursuite.eIRLPoursuiteNom;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/IPP/P93/P93.2']= intentionPoursuite.eIRLPoursuitePrenom;
}								

								// Groupe DAP : Déclaration d'affectation du patrimoine (si option pour l'EIRL)
								
if (Value('id').of(objet.objetModification).contains('modifEIRL') or objet.entrepriseEIRL) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P70']= objet.cadreObjetDeclarationEIRL.declarationEIRL ?
																			   (Value('id').of(declarationAffectation.eirlStatut).eq('EirlStatutEIRLReprise') ? "R" : "O") : 
																			   ((Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') or objet.entrepriseEIRL) ? "M" : "F");

if (objet.cadreObjetDeclarationEIRL.declarationEIRL) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P71']= declarationAffectation.declarationPatrimoine.eirlDenomination;
} else if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDenoEIRL')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P71']= modifEIRL.newDenoEIRL;
}

if (objet.cadreObjetDeclarationEIRL.declarationEIRL and declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable !== null) {
    var dateTmp = new Date(parseInt(declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable.getTimeInMillis()));
    var dateClotureEc = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateClotureEc = dateClotureEc.concat(pad(month.toString()));
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P72']          = dateClotureEc;
} else if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDateCloture')) {
    var dateTmp = new Date(parseInt(modifEIRL.eirlNewDateClotureExerciceComptable.getTimeInMillis()));
    var dateClotureEc = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateClotureEc = dateClotureEc.concat(pad(month.toString()));
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P72']          = dateClotureEc;
}

if (objet.cadreObjetDeclarationEIRL.declarationEIRL) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P73']= declarationAffectation.declarationPatrimoine.eirlObjet;
} else if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifObjet')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P73']= modifEIRL.newObjetEIRL;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.3']= modifEIRL.newAdresseEIRL.communeAdresseEirlNew.getId();
} else if (objet.entrepriseEIRL and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).contains('transfert')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.3']= etablissementNew.adresseEtablissementNew.etablissementAdresseCommuneNew.getId();
} else if (objet.entrepriseEIRL and objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.3']= newAdresseDom.personneLieeAdresseCommuneNew.getId();
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse') and modifEIRL.newAdresseEIRL.numeroAdresseEirlNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.5']= modifEIRL.newAdresseEIRL.numeroAdresseEirlNew;
} else if (objet.entrepriseEIRL and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissementNew.adresseEtablissementNew.etablissementAdresseNumeroVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.5']= etablissementNew.adresseEtablissementNew.etablissementAdresseNumeroVoieNew;
} else if (objet.entrepriseEIRL and objet.domicileEntreprise and newAdresseDom.personneLieeAdresseVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.5']= newAdresseDom.personneLieeAdresseVoieNew;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse') and modifEIRL.newAdresseEIRL.indiceVoieAdresseEirlNew != null) {
   var monId1 = Value('id').of(modifEIRL.newAdresseEIRL.indiceVoieAdresseEirlNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.6']          = monId1;
} else if (objet.entrepriseEIRL and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissementNew.adresseEtablissementNew.etablissementAdresseIndiceVoieNew != null) {
   var monId2 = Value('id').of(etablissementNew.adresseEtablissementNew.etablissementAdresseIndiceVoieNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.6']          = monId2;
} else if (objet.entrepriseEIRL and objet.domicileEntreprise and newAdresseDom.personneLieeAdresseIndiceVoieNew != null) {
   var monId3 = Value('id').of(newAdresseDom.personneLieeAdresseIndiceVoieNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.6']          = monId3;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse') and modifEIRL.newAdresseEIRL.distriutionSpecialeAdresseEirlNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.7']= modifEIRL.newAdresseEIRL.distriutionSpecialeAdresseEirlNew;
} else if (objet.entrepriseEIRL and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissementNew.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.7']= etablissementNew.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew;
} else if (objet.entrepriseEIRL and objet.domicileEntreprise and newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.7']= newAdresseDom.personneLieeAdresseDistriutionSpecialeNew;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.8']= modifEIRL.newAdresseEIRL.codePostalAdresseEirlNew;
} else if (objet.entrepriseEIRL and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).contains('transfert')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.8']= etablissementNew.adresseEtablissementNew.etablissementAdresseCodePostalNew;
} else if (objet.entrepriseEIRL and objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.8']= newAdresseDom.personneLieeAdresseCodePostalNew;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse') and modifEIRL.newAdresseEIRL.complementAdresseEirlNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.10']= modifEIRL.newAdresseEIRL.complementAdresseEirlNew;
} else if (objet.entrepriseEIRL and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissementNew.adresseEtablissementNew.etablissementAdresseComplementVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.10']= etablissementNew.adresseEtablissementNew.etablissementAdresseComplementVoieNew;
} else if (objet.entrepriseEIRL and objet.domicileEntreprise and newAdresseDom.personneLieeAdresseComplementAdresseNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.10']= newAdresseDom.personneLieeAdresseComplementAdresseNew;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse') and modifEIRL.newAdresseEIRL.typeVoieAdresseEirlNew != null) {
   var monId1 = Value('id').of(modifEIRL.newAdresseEIRL.typeVoieAdresseEirlNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.11']          = monId1;
} else if (objet.entrepriseEIRL and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and etablissementNew.adresseEtablissementNew.etablissementAdresseTypeVoieNew != null) {
   var monId2 = Value('id').of(etablissementNew.adresseEtablissementNew.etablissementAdresseTypeVoieNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.11']          = monId2;
} else if (objet.entrepriseEIRL and objet.domicileEntreprise and newAdresseDom.personneLieeAdresseTypeVoieNew != null) {
   var monId3 = Value('id').of(newAdresseDom.personneLieeAdresseTypeVoieNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.11']          = monId3;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.12']= modifEIRL.newAdresseEIRL.nomVoieAdresseEirlNew;
} else if (objet.entrepriseEIRL and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).contains('transfert')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.12']= etablissementNew.adresseEtablissementNew.etablissementAdresseNomVoieNew;
} else if (objet.entrepriseEIRL and objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.12']= newAdresseDom.personneLieeAdresseNomVoieNew;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.13']= modifEIRL.newAdresseEIRL.communeAdresseEirlNew.getLabel();
} else if (objet.entrepriseEIRL and Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).contains('transfert')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.13']= etablissementNew.adresseEtablissementNew.etablissementAdresseCommuneNew.getLabel();
} else if (objet.entrepriseEIRL and objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.13']= newAdresseDom.personneLieeAdresseCommuneNew.getLabel();
}

if (Value('id').of(declarationAffectation.eirlStatut).eq('EirlStatutEIRLReprise')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P76/P76.1']= declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLDenomination;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P76/P76.2']= "3";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P76/P76.3']= declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLLieuImmatriculation;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P76/P76.4']= declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN.split(' ').join('');
}

if (modifEIRL.modifDateEIRL !== null) {
	var dateTemp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P77']= date;
} else if (objet.poursuiteEIRL and poursuite.modifDateDeces !== null) {
	var dateTemp = new Date(parseInt(poursuite.modifDateDeces.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P77']= date;
} else if (finDAP.finEIRLDeclaration.modifDateFinEIRL !== null) {
	var dateTemp = new Date(parseInt(finDAP.finEIRLDeclaration.modifDateFinEIRL.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P77']= date;
} else if (objet.entrepriseEIRL and etablissement.modifDateAdresseEntreprise !== null) {
	var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P77']= date;
} else if (objet.entrepriseEIRL and modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P77']= date;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDAP')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P78']= "O";
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('finEIRL')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P79']= Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLRenonciationAvecPoursuite') ? "1" : 
																			   (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLRenonciationSansPoursuite') ? "2" : 
																			   (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLCessionPP') ? "3" : "4"));
}
}

							
								// Groupe SMP : Situation matrimoniale de la personne physique (conditionnel)

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('30P')
	and (Value('id').of(conjoint.objetModifConjoint).eq('conjointCollaborateur')
	or Value('id').of(conjoint.objetModifStatut).eq('modificationStatut'))) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P25/P25.2']= conjoint.modifNomNaissanceConjoint;

var prenoms=[];
for ( i = 0; i < conjoint.modifPrenomConjoint.size() ; i++ ){prenoms.push(conjoint.modifPrenomConjoint[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P25/P25.3']= prenoms;

if(conjoint.modifNomUsageConjoint != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P25/P25.4']= conjoint.modifNomUsageConjoint;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P40']= (Value('id').of(conjoint.objetModifStatut).eq('suppressionStatut') or (Value('id').of(conjoint.objetModifConjoint).eq('conjointSalarie') and Value('id').of(conjoint.objetModifStatut).eq('modificationStatut'))) ? "S" : "O";

if (Value('id').of(conjoint.objetModifStatut).eq('nouveauStatut')
	or (Value('id').of(conjoint.objetModifConjoint).eq('conjointCollaborateur') and Value('id').of(conjoint.objetModifStatut).eq('modificationStatut'))) {
if(conjoint.modifDateNaissanceConjoint !== null) {
	var dateTemp = new Date(parseInt(conjoint.modifDateNaissanceConjoint.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.1']= date;
}
 
if	(Value('id').of(conjoint.modifLieuNaissancePaysConjoint).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.2']= conjoint.modifLieuNaissanceCommuneConjoint.getId();
} else if (not Value('id').of(conjoint.modifLieuNaissancePaysConjoint).eq('FRXXXXX')) {
		var idPays = conjoint.modifLieuNaissancePaysConjoint.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.2']= result;
}

if (not Value('id').of(conjoint.modifLieuNaissancePaysConjoint).eq('FRXXXXX')) {																					 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.3']= conjoint.modifLieuNaissancePaysConjoint.getLabel();
}																					 

if (Value('id').of(conjoint.modifLieuNaissancePaysConjoint).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.4']= conjoint.modifLieuNaissanceCommuneConjoint.getLabel();
}

if (conjoint.adresseConjointDifferente) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.3']= adresseConjoint.adresseConjointCommune.getId();

if (adresseConjoint.adresseConjointNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.5']= adresseConjoint.adresseConjointNumeroVoie;
}

if (adresseConjoint.adresseConjointIndiceVoie !== null) {
   var monId = Value('id').of(adresseConjoint.adresseConjointIndiceVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.6']          = monId;
}

if (adresseConjoint.adresseConjointDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.7']= adresseConjoint.adresseConjointDistriutionSpecialeVoie;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.8']= adresseConjoint.adresseConjointCodePostal;

if (adresseConjoint.adresseConjointComplementVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.10']= adresseConjoint.adresseConjointComplementVoie;
}

if (adresseConjoint.adresseConjointTypeVoie !== null) {
   var monId = Value('id').of(adresseConjoint.adresseConjointTypeVoie)._eval();
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.11']          = monId;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.12']= adresseConjoint.adresseConjointNomVoie;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P43/P43.13']= adresseConjoint.adresseConjointCommune.getLabel();
}
}
}

								// Groupe SUP : Suppléant désigné par une instance professionnelle

if(Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('22P')) {								
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P80/P80.2']= poursuite.nomNaissanceSuppleant;

var prenoms=[];
for ( i = 0; i < poursuite.prenomSuppleant.size() ; i++ ){prenoms.push(poursuite.prenomSuppleant[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P80/P80.3']= prenoms;

if (poursuite.nomUsageSuppleant != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P80/P80.4']= poursuite.nomUsageSuppleant;
}

if(poursuite.dateNaissanceSuppleant !== null) {
	var dateTemp = new Date(parseInt(poursuite.dateNaissanceSuppleant.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P81/P81.1']= date;
}
 
if	(Value('id').of(poursuite.lieuNaissancePaysSuppleant).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P81/P81.2']= poursuite.lieuNaissanceCommuneSuppleant.getId();
} else if (not Value('id').of(poursuite.lieuNaissancePaysSuppleant).eq('FRXXXXX')) {
		var idPays = poursuite.lieuNaissancePaysSuppleant.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P81/P81.2']= result;
}

if (not Value('id').of(poursuite.lieuNaissancePaysSuppleant).eq('FRXXXXX')) {																					 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P81/P81.3']= poursuite.lieuNaissancePaysSuppleant.getLabel();
}																					 

if (Value('id').of(poursuite.lieuNaissancePaysSuppleant).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P81/P81.4']= poursuite.lieuNaissanceCommuneSuppleant.getLabel();
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P82']= poursuite.nationaliteSuppleant;

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P83/P83.3']= poursuite.adresseDomicileSuppleant.suppleantAdresseCommune.getId();

if (poursuite.adresseDomicileSuppleant.suppleantAdresseVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P83/P83.5']= poursuite.adresseDomicileSuppleant.suppleantAdresseVoie;
}

if (poursuite.adresseDomicileSuppleant.suppleantAdresseIndiceVoie !== null) {
   var monId = Value('id').of(poursuite.adresseDomicileSuppleant.suppleantAdresseIndiceVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P83/P83.6']          = monId;
}

if (poursuite.adresseDomicileSuppleant.suppleantAdresseDistriutionSpeciale != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P83/P83.7']= poursuite.adresseDomicileSuppleant.suppleantAdresseDistriutionSpeciale;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P83/P83.8']= poursuite.adresseDomicileSuppleant.suppleantAdresseCodePostal;

if (poursuite.adresseDomicileSuppleant.suppleantAdresseComplementAdresse != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P83/P83.10']= poursuite.adresseDomicileSuppleant.suppleantAdresseComplementAdresse;
}

if (poursuite.adresseDomicileSuppleant.suppleantAdresseTypeVoie !== null) {
   var monId = Value('id').of(poursuite.adresseDomicileSuppleant.suppleantAdresseTypeVoie)._eval();
 regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P83/P83.11']          = monId;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P83/P83.12']= poursuite.adresseDomicileSuppleant.suppleantAdresseNomVoie;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P83/P83.13']= poursuite.adresseDomicileSuppleant.suppleantAdresseCommune.getLabel();
}								
								
								// Groupe GCS : Groupe complément social

// Sous groupe ISS : Immatriculation sécurité sociale du travailleur non salarié
/*
if (Value('id').of(objet.objetModification).contains('dateActivite') and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 != null or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 != null)) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A10/A10.1']  = "0000000000000";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A10/A10.2']  = "00";
}
*/

// Sous groupe SCS : Situation du conjoint vis-à-vis de la sécurité sociale
/*
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('30P') and Value ('id').of(conjoint.objetModifConjoint).eq('modifMentionNouveauConjoint')) {
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A53/A53.4']= conjoint.voletSocialConjointCouvertAssuranceMaladieOui ? "O" : "N";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A55/A55.1']  = "0000000000000";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A55/A55.2']  = "00";
}
*/

// Groupe IPU : Immatriculation principale de l'enttreprise

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U02']= identite.siren.split(' ').join('');
if(activite.activiteTemps != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U06']= activite.etablissementEffectifSalariePresenceOui ? "O" : "N";
}
// Fin regent pour le 10P//
// Fin regent pour le 15P//
// Fin regent pour le 16P//
// Fin regent pour le 17P//
// Fin regent pour le 20P//
// Fin regent pour le 30P//

// Groupe SIU : siège de l'entreprise (adresse de l'entreprise individuelle)

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P') 
	or objet.domicileEntreprise) { 

if (objet.entrepriseDomicile or objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U16']= "O";
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.3']= etablissementNew.adresseEtablissementNew.etablissementAdresseCommuneNew.getId();
} else if (objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.3']= newAdresseDom.personneLieeAdresseCommuneNew.getId();
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.3']= adresseEntreprise.etablissementAdresseCommune.getId();
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') and etablissementNew.adresseEtablissementNew.etablissementAdresseNumeroVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.5']= etablissementNew.adresseEtablissementNew.etablissementAdresseNumeroVoieNew;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.5']= newAdresseDom.personneLieeAdresseVoieNew;
} else if ((not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and not Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))) 
	and not objet.domicileEntreprise and adresseEntreprise.etablissementAdresseNumeroVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.5']= adresseEntreprise.etablissementAdresseNumeroVoie;
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') and etablissementNew.adresseEtablissementNew.etablissementAdresseIndiceVoieNew != null) {
   var monId1 = Value('id').of(etablissementNew.adresseEtablissementNew.etablissementAdresseIndiceVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.6']          = monId1;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseIndiceVoieNew != null) {
   var monId2 = Value('id').of(newAdresseDom.personneLieeAdresseIndiceVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.6']          = monId2;
} else if ((not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and not Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))) 
	and not objet.domicileEntreprise and adresseEntreprise.etablissementAdresseIndiceVoie != null){
var monId3 = Value('id').of(adresseEntreprise.etablissementAdresseIndiceVoie)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.6']		  = monId3;
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') and etablissementNew.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.7']= etablissementNew.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.7']= newAdresseDom.personneLieeAdresseDistriutionSpecialeNew;
} else if ((not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and not Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))) 
	and not objet.domicileEntreprise and adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.7']= adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie;
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.8']= etablissementNew.adresseEtablissementNew.etablissementAdresseCodePostalNew;
} else if (objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.8']= newAdresseDom.personneLieeAdresseCodePostalNew;
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.8']= adresseEntreprise.etablissementAdresseCodePostal;
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') and etablissementNew.adresseEtablissementNew.etablissementAdresseComplementVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.10']= etablissementNew.adresseEtablissementNew.etablissementAdresseComplementVoieNew;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseComplementAdresseNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.10']= newAdresseDom.personneLieeAdresseComplementAdresseNew;
}  else if ((not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and not Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))) 
	and not objet.domicileEntreprise and adresseEntreprise.etablissementAdresseComplementVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.10']= adresseEntreprise.etablissementAdresseComplementVoie;
}	

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') and etablissementNew.adresseEtablissementNew.etablissementAdresseTypeVoieNew != null) {
   var monId1 = Value('id').of(etablissementNew.adresseEtablissementNew.etablissementAdresseTypeVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.11']          = monId1;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseTypeVoieNew != null) {
   var monId2 = Value('id').of(newAdresseDom.personneLieeAdresseTypeVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.11']          = monId2;
}  else if ((not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and not Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))) 
	and not objet.domicileEntreprise and adresseEntreprise.etablissementAdresseTypeVoie != null){
	var monId3 = Value('id').of(adresseEntreprise.etablissementAdresseTypeVoie)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.11']		  = monId3;
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.12']= etablissementNew.adresseEtablissementNew.etablissementAdresseNomVoieNew;
} else if (objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.12']= newAdresseDom.personneLieeAdresseNomVoieNew;
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.12']= adresseEntreprise.etablissementAdresseNomVoie;
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.13']= etablissementNew.adresseEtablissementNew.etablissementAdresseCommuneNew.getLabel();
} else if (objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.13']= newAdresseDom.personneLieeAdresseCommuneNew.getLabel();
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.13']= adresseEntreprise.etablissementAdresseCommune.getLabel();
}
}

							// Groupe CPU : Caractéristiques de l'entreprise

if (activite.activiteDevientPrincipale) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U21']= activite.etablissementActivitesPrincipales2 != null ? activite.etablissementActivitesPrincipales2 : activite.etablissementActivitesAutres;
}
				
if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(etablissementNew.origineEtablissement).eq('etablissementOuverture'))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')	
	or objet.domicileEntreprise) 
	and activite.etablissementEffectifSalariePresenceOui) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U22']= activite.cadreEffectifSalarie.totalEffectifSalarieNombre;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('37P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.1']= "R";
}

// Fin Regent 37P //	
	
// Groupe OFU : Option Fiscale EIRL (si EIRL) 
							
if (objet.cadreObjetDeclarationEIRL.declarationEIRL) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U70']= (Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesRsBNC') or identite.microEntrepreneurOuiNon) ? '110' :
																		(Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesDcBNC') ? '111' : 
																		(Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalIS).eq('RegimeFiscalRegimeImpositionBeneficesRsIS') ? "114" : "115"));

if (fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionCreanceDette) {																		
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U71']= "210";
}
else if (Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionIS')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U71']= "211";
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U72/U72.1'] = (Value('id').of(fiscalEIRL.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARfTVA') or Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesRsBNC') or identite.microEntrepreneurOuiNon) ? "310" :
																				(Value('id').of(fiscalEIRL.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARsTVA') ? "311" : "312");

if (fiscalEIRL.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres1DC) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U72/U72.2'] = "410";
}

/*																			
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U74']= "N";
*/
}

// Fin Regent pour le 25P //


/*
// Groupe LIU lieu d'imposition de l'entreprise

if (Value('id').of(objet.objetModification).contains('situationPerso') or Value('id').of(objet.objetModification).contains('modifEIRL') or Value('id').of(objet.objetModification).contains('etablissement')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/LIU/U36/U36.2']= identite.personneLieePersonnePhysiqueLieuDepotImpot;
}
*/
// Fin Regent pour le 22P //

				// Groupe ICE : Identification complète de l'établissement 

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') or Value('id').of(etablissementNew.origineEtablissement).eq('etablissementOuverture')))
or objet.domicileEntreprise
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
or activiteInfo.modifActiviteTransfert
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')
or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')) { 

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') and Value('id').of(etablissementNew.origineEtablissement).eq('etablissementOuverture'))
	or objet.domicileEntreprise
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E01']= "1";
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E01']= "3";
}
	
if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	or (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire') 
	and (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')))) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.3']= etablissementNew.adresseEtablissementNew.etablissementAdresseCommuneNew.getId();
} else if (objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.3']= newAdresseDom.personneLieeAdresseCommuneNew.getId();
} else if (not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and not objet.domicileEntreprise 
	and not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	and not Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.3']= adresseEntreprise.etablissementAdresseCommune.getId();
}

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	or (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire') 
	and (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')))) 
	and etablissementNew.adresseEtablissementNew.etablissementAdresseNumeroVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5']= etablissementNew.adresseEtablissementNew.etablissementAdresseNumeroVoieNew;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5']= newAdresseDom.personneLieeAdresseVoieNew;
} else if (not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and not objet.domicileEntreprise 
	and not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') 
	and not Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')
	and adresseEntreprise.etablissementAdresseNumeroVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5']= adresseEntreprise.etablissementAdresseNumeroVoie;
}

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	or (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire') 
	and (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')))) 
	and etablissementNew.adresseEtablissementNew.etablissementAdresseIndiceVoieNew != null) {
   var monId1 = Value('id').of(etablissementNew.adresseEtablissementNew.etablissementAdresseIndiceVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.6']          = monId1;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseIndiceVoieNew != null) {
   var monId2 = Value('id').of(newAdresseDom.personneLieeAdresseIndiceVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.6']          = monId2;
} else if (not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') 
	and not objet.domicileEntreprise 
	and not Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')
	and adresseEntreprise.etablissementAdresseIndiceVoie != null){
   var monId3 = Value('id').of(adresseEntreprise.etablissementAdresseIndiceVoie)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.6']		  = monId3;
}

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	or (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire') 
	and (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P'))))  
	and etablissementNew.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.7']= etablissementNew.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.7']= newAdresseDom.personneLieeAdresseDistriutionSpecialeNew;
} else if (not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and not objet.domicileEntreprise 
	and not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	and not Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')	
	and adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.7']= adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie;
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	or (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire') 
	and (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')))) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.8']= etablissementNew.adresseEtablissementNew.etablissementAdresseCodePostalNew;
} else if (objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.8']= newAdresseDom.personneLieeAdresseCodePostalNew;
} else if (not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and not objet.domicileEntreprise 
	and not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	and not Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.8']= adresseEntreprise.etablissementAdresseCodePostal;
}

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	or (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire') 
	and (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')))) 
	and etablissementNew.adresseEtablissementNew.etablissementAdresseComplementVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.10']= etablissementNew.adresseEtablissementNew.etablissementAdresseComplementVoieNew;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseComplementAdresseNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.10']= newAdresseDom.personneLieeAdresseComplementAdresseNew;
} else if (not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and not objet.domicileEntreprise 
	and not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P') 
	and not Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')
	and adresseEntreprise.etablissementAdresseComplementVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.10']= adresseEntreprise.etablissementAdresseComplementVoie;
}	

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	or (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire') 
	and (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')))) 
	and etablissementNew.adresseEtablissementNew.etablissementAdresseTypeVoieNew != null) {
   var monId1 = Value('id').of(etablissementNew.adresseEtablissementNew.etablissementAdresseTypeVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.11']          = monId1;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseTypeVoieNew != null) {
   var monId2 = Value('id').of(newAdresseDom.personneLieeAdresseTypeVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.11']          = monId2;
} else if (not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and not objet.domicileEntreprise 
	and not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	and not Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')	
	and adresseEntreprise.etablissementAdresseTypeVoie != null){
   var monId3 = Value('id').of(adresseEntreprise.etablissementAdresseTypeVoie)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.11']		  = monId3;
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	or (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire') 
	and (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')))) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.12']= etablissementNew.adresseEtablissementNew.etablissementAdresseNomVoieNew;
} else if (objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.12']= newAdresseDom.personneLieeAdresseNomVoieNew;
} else if (not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and not objet.domicileEntreprise 
	and not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	and not Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.12']= adresseEntreprise.etablissementAdresseNomVoie;
}

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	or (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire') 
	and (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite') 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('60P')))) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.13']= etablissementNew.adresseEtablissementNew.etablissementAdresseCommuneNew.getLabel();
} else if (objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.13']= newAdresseDom.personneLieeAdresseCommuneNew.getLabel();
} else if (not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and not objet.domicileEntreprise 
	and not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')
	and not Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.13']= adresseEntreprise.etablissementAdresseCommune.getLabel();
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E03/E03.1']= "000000000";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E03/E03.2']= "00000";

if (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire') or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E04']= "4";
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E04']= "3";
}

if (activiteInfo.cadreEtablissementIdentification.modifEnseigne != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E05']= activiteInfo.cadreEtablissementIdentification.modifEnseigne;
} else if (activite.modifEnseigneBis != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E05']= activite.modifEnseigneBis;
}
}

// fin Regent 21P
// fin Regent 60P

				// Si 40P  

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('40P')) {			
				
				//Groupe IAE : Identification ancienne de l'établissement
				
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E12/E12.3'] = adresseEntreprise.etablissementAdresseCommune.getId();
																	
if (adresseEntreprise.etablissementAdresseNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E12/E12.5'] = adresseEntreprise.etablissementAdresseNumeroVoie;
}
	
if (adresseEntreprise.etablissementAdresseIndiceVoie !== null) {
   var monId1 = Value('id').of(adresseEntreprise.etablissementAdresseIndiceVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E12/E12.6']          = monId1;
}  

if (adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E12/E12.7']= adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E12/E12.8']= adresseEntreprise.etablissementAdresseCodePostal;

if (adresseEntreprise.etablissementAdresseComplementVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E12/E12.10']= adresseEntreprise.etablissementAdresseComplementVoie ;
}

if (adresseEntreprise.etablissementAdresseTypeVoie !== null) {
   var monId1 = Value('id').of(adresseEntreprise.etablissementAdresseTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E12/E12.11']          = monId1;
} 

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E12/E12.12']= adresseEntreprise.etablissementAdresseNomVoie;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E12/E12.13']= adresseEntreprise.etablissementAdresseCommune.getLabel();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E13/E13.1'] = "000000000";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E13/E13.2'] = "00000";

if(etablissement.dateCessationEmploi !== null) {
var dateTemp = new Date(parseInt(etablissement.dateCessationEmploi.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E14']= date;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/IAE/E17']= "3";

			// Groupe DEE : Destination de l'établissement

if (Value('id').of(etablissement.destinationEtablissement3).eq('vendu')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/DEE/E41/E41.1']= "3";
} else if (Value('id').of(etablissement.destinationEtablissement3).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/DEE/E41/E41.1']= "9";
} else if (Value('id').of(etablissement.destinationEtablissement3).eq('supprime')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/DEE/E41/E41.1']= "C";
}

if (Value('id').of(etablissement.destinationEtablissement3).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/EtablissementFerme/DEE/E41/E41.2']= etablissement.destinationAutre;
}

// Fin Regent 40P
}

				// Groupe IAE : Identification ancienne de l'établissement hors 40P

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and (Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal') 
	or Value('id').of(etablissement.destinationEtablissement1).eq('vendu')
	or Value('id').of(etablissement.destinationEtablissement1).eq('ferme')
	or Value('id').of(etablissement.destinationEtablissement1).eq('autre')
	or Value('id').of(etablissement.destinationEtablissement2).eq('vendu')
	or Value('id').of(etablissement.destinationEtablissement2).eq('ferme')
	or Value('id').of(etablissement.destinationEtablissement2).eq('autre')
	or Value('id').of(etablissement.destinationEtablissement3).eq('supprime')
	or Value('id').of(etablissement.destinationEtablissement3).eq('vendu'))) 
	or (Value('id').of(etablissement.destinationEtablissement3).eq('autre')
	and not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('40P'))
	or objet.domicileEntreprise
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P')) {

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P'))	{
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.3']= etablissement.adresseEtablissementOld.etablissementAdresseCommuneOld.getId();
} else if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.3']= adresseEntreprise.etablissementAdresseCommune.getId();
}

if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P')) 
	and etablissement.adresseEtablissementOld.etablissementAdresseNumeroVoieOld != null)	{
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.5']= etablissement.adresseEtablissementOld.etablissementAdresseNumeroVoieOld;
} else if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or objet.domicileEntreprise)
	and adresseEntreprise.etablissementAdresseNumeroVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.5']= adresseEntreprise.etablissementAdresseNumeroVoie;
}

if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P')) 
	and etablissement.adresseEtablissementOld.etablissementAdresseIndiceVoieOld != null)	{
   var monId1 = Value('id').of(etablissement.adresseEtablissementOld.etablissementAdresseIndiceVoieOld)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.6']		  = monId1;
} else if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or objet.domicileEntreprise)
	and adresseEntreprise.etablissementAdresseIndiceVoie != null){
   var monId2 = Value('id').of(adresseEntreprise.etablissementAdresseIndiceVoie)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.6']		  = monId2;
}

if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P')) 
	and etablissement.adresseEtablissementOld.etablissementAdresseDistriutionSpecialeVoieOld != null)	{
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.7']= etablissement.adresseEtablissementOld.etablissementAdresseDistriutionSpecialeVoieOld;
} else if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or objet.domicileEntreprise)
	and adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.7']= adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie;
}

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P'))	{
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.8']= etablissement.adresseEtablissementOld.etablissementAdresseCodePostalOld;
} else if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.8']= adresseEntreprise.etablissementAdresseCodePostal;
}

if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P')) 
	and etablissement.adresseEtablissementOld.etablissementAdresseComplementVoieOld != null)	{
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.10']= etablissement.adresseEtablissementOld.etablissementAdresseComplementVoieOld;
} else if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or objet.domicileEntreprise)
	and adresseEntreprise.etablissementAdresseComplementVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.10']= adresseEntreprise.etablissementAdresseComplementVoie;
}

if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P')) 
	and etablissement.adresseEtablissementOld.etablissementAdresseTypeVoieOld != null)	{
   var monId1 = Value('id').of(etablissement.adresseEtablissementOld.etablissementAdresseTypeVoieOld)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.11']		  = monId1;
} else if (((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or objet.domicileEntreprise)
	and adresseEntreprise.etablissementAdresseTypeVoie != null){
   var monId2 = Value('id').of(adresseEntreprise.etablissementAdresseTypeVoie)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.11']		  = monId2;
}

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P'))	{
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.12']= etablissement.adresseEtablissementOld.etablissementAdresseNomVoieOld;
} else if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.12']= adresseEntreprise.etablissementAdresseNomVoie;
}

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P'))	{
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.13']= etablissement.adresseEtablissementOld.etablissementAdresseCommuneOld.getLabel();
} else if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertPrincipal'))
	or objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.13']= adresseEntreprise.etablissementAdresseCommune.getLabel();
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E13/E13.1']= "000000000";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E13/E13.2']= "00000";

if(Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P') and etablissement.dateCessationEmploi !== null) {
var dateTemp = new Date(parseInt(etablissement.dateCessationEmploi.getTimeInMillis()));
var year = dateTemp.getFullYear();
var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
var date = year + "-" + month + "-" + day ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E14']= date;
}

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(objet.cadreObjetModificationEtablissement.typeEtablissement).eq('transfertSecondaire'))
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P'))	{
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E17']= "4";
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E17']= "3";
}
}			   

			   // Groupe ORE : Origine de l'établissement

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(etablissementNew.origineEtablissement).eq('etablissementOuverture'))
	or objet.domicileEntreprise 
	or Value('id').of(activite.activiteEvenement).eq('61P') 
	or Value('id').of(activite.activiteEvenement).eq('61P62P')
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')) {
if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsCreation') or objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.1']=  "1";
} else if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsReprise')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.1']=  "8";
}
}

				// Groupe PEE : Précédent exploitant
				
if (Value('id').of(origine.etablisementOrigineFonds).eq('EtablissementOrigineFondsReprise')
	and (not Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('21P')
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.activiteReprise)) {				
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.1']= origine.precedentExploitant.entrepriseLieeSirenPrecedentExploitant.split(' ').join('');

if (Value('id').of(origine.precedentExploitant.typePersonnePrecedentExploitant).eq('typePersonnePrecedentExploitantPP')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.2']= origine.precedentExploitant.entrepriseLieeEntreprisePPNomNaissancePrecedentExploitant;
if (origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.3']= origine.precedentExploitant.entrepriseLieeEntreprisePPNomUsagePrecedentExploitant;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.4']= origine.precedentExploitant.entrepriseLieeEntreprisePPPrenom1PrecedentExploitant;
}

if (Value('id').of(origine.precedentExploitant.typePersonnePrecedentExploitant).eq('typePersonnePrecedentExploitantPM')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/PEE/E34/E34.7']= origine.precedentExploitant.entrepriseLieeEntreprisePPDenominationPrecedentExploitant;
}	
}

	
				// Groupe DEE : Destination de l'établissement

if (Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	or objet.domicileEntreprise 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('80P')) {				
if (Value('id').of(etablissement.destinationEtablissement1).eq('vendu')
	or Value('id').of(etablissement.destinationEtablissement2).eq('vendu')
	or Value('id').of(etablissement.destinationEtablissement3).eq('vendu')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.1']= "3";
} else if (Value('id').of(etablissement.destinationEtablissement2).eq('devientPrincipal')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.1']= "8";
} else if (Value('id').of(etablissement.destinationEtablissement1).eq('autre')
	or Value('id').of(etablissement.destinationEtablissement2).eq('autre')
	or Value('id').of(etablissement.destinationEtablissement3).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.1']= "9";
} else if (Value('id').of(etablissement.destinationEtablissement1).eq('devientSecondaire')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.1']= "A";
} else if (Value('id').of(etablissement.destinationEtablissement1).eq('ferme')
	or Value('id').of(etablissement.destinationEtablissement2).eq('ferme')
	or objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.1']= "B";
} else if (Value('id').of(etablissement.destinationEtablissement3).eq('supprime')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.1']= "C";
}

if (Value('id').of(etablissement.destinationEtablissement1).eq('autre')
	or Value('id').of(etablissement.destinationEtablissement2).eq('autre')
	or Value('id').of(etablissement.destinationEtablissement3).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.2']= etablissement.destinationAutre;
}
}

// Fin Regent pour le 80P //
// Fin Regent pour le 11P //
	

		// Groupe ACE : Activité de l'établissement

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(etablissementNew.origineEtablissement).eq('etablissementOuverture'))
	or objet.domicileEntreprise 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.activiteReprise
	or activiteInfo.modifActiviteTransfert
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')) {		
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E70']= activite.etablissementActivitesAutres ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E71']= activite.etablissementActivitesPrincipales2 != null ? activite.etablissementActivitesPrincipales2 : activite.etablissementActivitesAutres.substring(0, 140) ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E78']= activite.activiteDevientPrincipale ? "O" : "N" ;

if (Value('id').of(activite.activiteEvenement).eq('61P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E79/E79.1']= "A";
} else if (Value('id').of(activite.activiteEvenement).eq('62P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E79/E79.1']= "D";
} else if (Value('id').of(activite.activiteEvenement).eq('61P62P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E79/E79.1']= "E";
}
}


		// Groupe CAE

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(etablissementNew.origineEtablissement).eq('etablissementOuverture'))
	or objet.domicileEntreprise 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.activiteReprise
	or activiteInfo.modifActiviteTransfert
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')) {				
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E73/E73.1']= Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonnierePermanente') ? 'P' : 'S';

if(Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonniereSaisonniere')) {
	var dateTemp = new Date(parseInt((activite.activiteSaisonnierePeriode.from).getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E73/E73.11[1]/E73.111']= date;
}
if (Value('id').of(activite.activiteTemps).eq('EntrepriseActivitePermanenteSaisonniereSaisonniere')) {
	var dateTemp = new Date(parseInt((activite.activiteSaisonnierePeriode.to).getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E73/E73.11[1]/E73.112']= date;
}
if (activite.autrePeriode) {
	var dateTemp = new Date(parseInt((activite.activiteSaisonnierePeriode2.from).getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E73/E73.11[2]/E73.111']= date;
}
if (activite.autrePeriode) {
	var dateTemp = new Date(parseInt((activite.activiteSaisonnierePeriode2.to).getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E73/E73.11[2]/E73.112']= date;
}	
if (activite.etablissmentNonSedentariteQualiteNonSedentaire) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E73/E73.2']='A';
}
}
	

			// Groupe SAE : salariés de l'établissement

if ((Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('transfert') 
	and Value('id').of(etablissementNew.origineEtablissement).eq('etablissementOuverture'))
	or objet.domicileEntreprise 
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('modifActivite')
	or objet.cadreObjetModificationEtablissement.cadreObjet21P.activiteReprise
	or activiteInfo.modifActiviteTransfert
	or Value('id').of(objet.cadreObjetModificationEtablissement.objetEtablissement).eq('54P')) {							
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E84']= activite.etablissementEffectifSalariePresenceOui ? activite.cadreEffectifSalarie.etablissementEffectifSalarieNombre : "0";
if (activite.cadreEffectifSalarie.etablissementEffectifSalarieApprentis != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E85/E85.8']= activite.cadreEffectifSalarie.etablissementEffectifSalarieApprentis;
}
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E86']= activite.etablissementEffectifSalariePresenceOui ? "O" : "N";
}


log.info("Regent fields : {}", regentFields);
//-->DEST-689 : Afficher les erreurs XSD dans la formalité quand il y'a une erreur de génération

// Call to the XML-REGENT generation WS 
var response = nash.service.request('${regent.baseUrl}/private/v1/xml-regent/generate/{regentVersion}/{authorityType}/{authorityId}', regentVersion, authorityType, authorityId) 
.dataType('application/json') // 
.accept('json') // 
.param('listTypeEvenement',eventRegent) 
//-->MINE-263 : Permettre à l'équipe FF de voir les problèmes de validation XSD et le regent sur studio
.continueOnError(true) //
.post(JSON.stringify(regentFields)); 

// Record the generated XML-REGENT 
//MOD déplacement du test de la réponse du premier ws
if (response != null && response.status == 200) { 
    var xmlRegentStr = response.asBytes(); 
    nash.record.saveFile("XML_REGENT.xml",xmlRegentStr); 

//debut de l'ajout
 if (authorityId2 != null) {
//MOD extraction du numéro de liasse du premeir regent généré
var numeroLiasse = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02');
numeroLiasse = JSON.parse(numeroLiasse);
_log.info('extracted numero de liasse is {}', numeroLiasse);
 regentFields['/REGENT-XML/Destinataire']= authorityId2;
 
 // Filtre regent greffe
var undefined;
// Groupe SUP
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P80/P80.2']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P80/P80.3']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P80/P80.4']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P81/P81.1']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P81/P81.2']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P81/P81.3']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P81/P81.4']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P82']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P83/P83.3']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P83/P83.5']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P83/P83.6']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P83/P83.7']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P83/P83.8']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P83/P83.10']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P83/P83.11']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P83/P83.12']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SUP/P83/P83.13']= undefined;
// Groupe GCS/ISS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A10/A10.1']  = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A10/A10.2'] = undefined;
// Groupe GCS/SCS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A53/A53.4']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A55/A55.1'] = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A55/A55.2'] = undefined;
// Groupe OFU
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U70']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U71']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U72/U72.1']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U72/U72.2'] = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U74']= undefined; 
// Groupe SAE
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E84']=undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E85/E85.8']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E86']= undefined;

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS']  = undefined;


 //MOD modification de l'appel pour la génération du deuxième régent afin de prendre en compte le numéro de liasse du premier
 var response2 = nash.service.request('${regent.baseUrl}/private/v1/xml-regent/generate/{regentVersion}/{authorityType}/{authorityId}', regentVersion, authorityType2, authorityId2) 
.dataType('application/json') // 
.accept('json') // 
.param('listTypeEvenement',eventRegent) 
.param('liasseNumber', numeroLiasse.C02) 
.continueOnError(true)
.post(JSON.stringify(regentFields)); 


	//Début de l'ajout
	//MOD modifier reponse en response2
	if (response2 != null && response2.status == 200) { 
		var xmlRegentStr = response2.asBytes(); 
		nash.record.saveFile("XML_REGENT2.xml",xmlRegentStr);
	}else{
		
		var returnedResponse = response2.asObject();
		_log.info("Call ws regent returned errors  {}", returnedResponse);
		
		
		var regent2 = returnedResponse['regent'];
		var errors2 = returnedResponse['errors']; 
		nash.record.saveFile("XML_REGENT2.xml",regent2.getBytes()); 
		nash.record.saveFile("XML_VALIDATION_ERRORS_2.xml",errors2.getBytes()); 
		
		//-->MINE-263 : Permettre à l'équipe FF de voir les problèmes de validation XSD et le regent sur studio
		
		return spec.create({
		id : 'xmlGenerationConfirmation',
		label : "Xml Regent confirmation message",
		groups : [ spec.createGroup({
				id : 'Regent ',
				description : "Une erreur s'est produite lors de la génération du XML Regent.",
				data : [
				spec.createData({
					id: 'regent',
					label: "XML regent généré",
					type: 'Text',
					mandatory: true,
					value: regent2
				}),spec.createData({
					id: 'errors',
					label: "Erreurs de validations xsd",
					type: 'Text',
					mandatory: false,
					value: errors2
				}),spec.createData({
					id: 'blockingField',
					label: "xsd erroné",
					help:"Ce champs sert à arrêter le process du dossier pour que le support puisse le consulter",
					type: 'StringReadOnly',
					mandatory: true
				})]
			})]
		});
	}	
}	

	return spec.create({
	id : 'xmlGenerationConfirmation',
	label : "Xml Regent confirmation message",
	groups : [ spec.createGroup({
		id : 'confirmationMessageOk',
		description : "Le fichier XML Regent a été généré et ajouté au dossier.",
		data : []
		}) ]
	});
	
}else{
	
	var returnedResponse = response.asObject();
	_log.info("Call ws regent returned errors  {}", returnedResponse);
	
	
	var regent = returnedResponse['regent'];
	var errors = returnedResponse['errors']; 
	nash.record.saveFile("XML_REGENT.xml",regent.getBytes()); 
	nash.record.saveFile("XML_VALIDATION_ERRORS.xml",errors.getBytes()); 
	
	
	return spec.create({
	id : 'xmlGenerationConfirmation',
	label : "Xml Regent confirmation message",
	groups : [ spec.createGroup({
		id : 'Regent ',
		description : "Une erreur s'est produite lors de la génération du XML Regent de la première autorité.",
		data : [
				spec.createData({
					id: 'regent',
					label: "XML regent généré",
					type: 'Text',
					mandatory: true,
					value: regent
				}),spec.createData({
					id: 'errors',
					label: "Erreurs de validations xsd",
					type: 'Text',
					mandatory: false,
					value: errors
				}),spec.createData({
					id: 'blockingField',
					label: "xsd erroné",
					help:"Ce champs sert à arrêter le process du dossier pour que le support puisse le consulter",
					type: 'StringReadOnly',
					mandatory: true
			})]
		}) ]
	});
}	