// PJ Déclarant

var userDeclarant;
if ($ac2.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNouveauNomUsage != null and Value('id').of($ac2.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) {
    var userDeclarant = $ac2.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNouveauNomUsage + '  ' + (Value('id').of($ac2.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNomPrenom).contains('modifPrenom') ? $ac2.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNouveauPrenoms[0] : $ac2.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiquePrenom1[0]) ;
} else if ($ac2.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiqueNomUsage != null and not Value('id').of($ac2.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) {
    var userDeclarant = $ac2.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiqueNomUsage + '  ' + (Value('id').of($ac2.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNomPrenom).contains('modifPrenom') ? $ac2.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNouveauPrenoms[0] : $ac2.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiquePrenom1[0]) ;
} else {
    var userDeclarant = (Value('id').of($ac2.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNomPrenom).contains('modifNomNaissance') ? $ac2.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNouveauNomNaissance : $ac2.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiqueNomNaissance) + '  '+ (Value('id').of($ac2.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNomPrenom).contains('modifPrenom') ? $ac2.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifIdentite.modifNouveauPrenoms[0] : $ac2.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiquePrenom1[0]) ;
}

var pj=$ac2.cadre9SignatureGroup.cadre9Signature.soussigne;
if(Value('id').of(pj).eq('FormaliteSignataireQualiteDeclarant')) {
    attachment('pjIDDeclarantSignataire', 'pjIDDeclarantSignataire', { label: userDeclarant, mandatory:"true"});
}

var pj=$ac2.cadre9SignatureGroup.cadre9Signature.soussigne;
if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') and 
(Value('id').of($ac2.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationPerso.objetSituationPerso).contains('10P') 
	or Value('id').of($ac2.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationPerso.objetSituationPerso).contains('15P') 
	or Value('id').of($ac2.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationPerso.objetSituationPerso).contains('17P'))) {
    attachment('pjIDDeclarantNonSignataire', 'pjIDDeclarantNonSignataire', { label: userDeclarant, mandatory:"true"});
}

// PJ Mandataire

var userMandataire=$ac2.cadre9SignatureGroup.cadre9Signature.adresseMandataire

var pj=$ac2.cadre9SignatureGroup.cadre9Signature.soussigne;
if (Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire')) {
    attachment('pjIDMandataireSignataire', 'pjIDMandataireSignataire', {label: userMandataire.nomPrenomDenominationMandataire, mandatory:"true"});
    attachment('pjPouvoir', 'pjPouvoir', {mandatory:"true"});
}

// PJ conjoint

if(Value('id').of($ac2.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationPerso.objetSituationPerso).contains('30P') and Value('id').of($ac2.cadre3ModificationConjointGroup.cadre3ModificationConjoint.objetModifConjoint).eq('modifMentionNouveauConjoint')) {    
    attachment('pjIDConjoint', 'pjIDConjoint', {mandatory:"true"});
}

// PJ Insaisissabilité

if(Value('id').of($ac2.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationPerso.objetSituationPerso).contains('28P')) {
   attachment('pjInsaisissabiliteActeNotarie', 'pjInsaisissabiliteActeNotarie', { mandatory:"true"});
}

// PJ EIRL

var pj=$ac2.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEIRL.objetEIRL;
if(($ac2.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetDeclarationEIRL.declarationEIRL and not Value('id').of($ac2.cadre4DeclarationAffectationPatrimoineGroup.cadreDeclarationAffectationPatrimoine.eirlDepot).eq('eirlSansDepot')) or Value('id').of(pj).contains('modificationEIRL') or $ac2.cadre1ObjetModificationGroup.cadre1ObjetModification.entrepriseEIRL) {
    attachment('pjDAP', 'pjDAP', { mandatory:"true"});
}

var pj=$ac2.cadre4DeclarationAffectationPatrimoineGroup;
if(Value('id').of(pj.cadreEIRLInformationsComplementaires.contenuDAP).contains('bienImmo') or Value('id').of(pj.cadreEIRLInformationsComplementairesBis.contenuDAPBis).contains('bienImmo') or Value('id').of($ac2.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.contenuDAP).contains('bienImmo')) {
    attachment('pjDAPActeNotarie', 'pjDAPActeNotarie', { mandatory:"true"});
}

if(Value('id').of(pj.cadreEIRLInformationsComplementaires.contenuDAP).contains('bienCommunIndivis') or Value('id').of(pj.cadreEIRLInformationsComplementairesBis.contenuDAPBis).contains('bienCommunIndivis') or Value('id').of($ac2.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.contenuDAP).contains('bienCommunIndivis')) {
    attachment('pjDAPAccordTiers', 'pjDAPAccordTiers', { mandatory:"true"});
}