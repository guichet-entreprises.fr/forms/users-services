//prepare info to send 

var adresseNew = $ac2.cadre6EtablissementGroup.cadre6Etablissement.adresseEtablissementNew;
var adressePro = $ac2.cadre1RappelIdentificationGroup.cadre1RappelIdentification.adresseEtablissementPrincipal;
var adresseEIRL = $ac2.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreRappelIdentificationEIRL.cadreEirlRappelAdresse;
var adresseDom = $ac2.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifDomicile.newAdresseDomicile;

var algo = "trouver destinataire";

var secteur1 = "AGENT COMMERCIAL";

var typePersonne = "PP";

var formJuridique = ($ac2.cadre1RappelIdentificationGroup.cadre1RappelIdentification.formeJuridique or $ac2.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetDeclarationEIRL.declarationEIRL) ? "EIRL" : "EI";

var optionCMACCI = "NON";

var codeCommune = Value('id').of($ac2.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).eq('11P') ? adresseNew.etablissementAdresseCommuneNew.getId() : 
				($ac2.cadre1ObjetModificationGroup.cadre1ObjetModification.domicileEntreprise ? adresseDom.personneLieeAdresseCommuneNew.getId() : 
				((Value('id').of($ac2.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).eq('modifEIRL')
				and Value('id').of($ac2.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEIRL.objetModificationEIRL).eq('modifAdresse')
				and not Value('id').of($ac2.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).eq('situationPerso')
				and not Value('id').of($ac2.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).eq('11P')
				and not Value('id').of($ac2.cadre1ObjetModificationGroup.cadre1ObjetModification.objetModification).eq('dateActivite')) ?
				adresseEIRL.communeAdresseEirl.getId() : adressePro.etablissementAdresseCommune.getId()));

var attachement= "/4-signature/generated/proxyResult.files-1-PROXY_SIGNED_DOCUMENT.pdf";

return spec.create({
		id: 'prepareSend',
		label: "Préparation de la recherche du destinataire",
		groups: [spec.createGroup({
				id: 'view',
				label: "Informations",
				data: [spec.createData({
						id: 'algo',
						label: "Algo",
						type: 'String',
						mandatory: true,
						value: algo
					}), spec.createData({
						id: 'secteur1',
						label: "Secteur",
						type: 'String',
						value: secteur1
					}), spec.createData({
						id: 'typePersonne',
						label: "Type personne",
						type: 'String',
						value: typePersonne
					}), spec.createData({
						id: 'formJuridique',
						label: "Forme juridique",
						type: 'String',
						value: formJuridique
					}),spec.createData({
						id: 'optionCMACCI',
						label: "Option CMACCI",
						type: 'String',
						value: optionCMACCI
					}),spec.createData({
						id: 'codeCommune',
						label: "Code commune",
						type: 'String',
						value: codeCommune
					}) ,spec.createData({
					    id: 'attachement',
					    label: "Pièce jointe",
					    type: 'String',
					    value: attachement
					})
				]
			})]
});