function pad(s) { return (s < 10) ? '0' + s : s; }

var regEx = new RegExp('[0-9]{5}'); 
var objet= $ac2.cadre1ObjetModificationGroup.cadre1ObjetModification;
var identite= $ac2.cadre1RappelIdentificationGroup.cadre1RappelIdentification;
var modifIdentiteDeclarant = $ac2.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle;
var etablissement = $ac2.cadre6EtablissementGroup.cadre6Etablissement;
var newAdresseDom = $ac2.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifDomicile.newAdresseDomicile;
var newAdresseDom2 = $ac2.cadre6EtablissementGroup.cadre6Etablissement.adresseEtablissementNew;
var conjoint = $ac2.cadre3ModificationConjointGroup.cadre3ModificationConjoint;
var adresseEntreprise = $ac2.cadre1RappelIdentificationGroup.cadre1RappelIdentification.adresseEtablissementPrincipal;
var correspondance = $ac2.cadre8RensCompGroup.cadre8RensComp;
var signataire = $ac2.cadre9SignatureGroup.cadre9Signature;
var declarationAffectation = $ac2.cadre4DeclarationAffectationPatrimoineGroup.cadreDeclarationAffectationPatrimoine;
var rappelIdentification = $ac2.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreRappelIdentificationEIRL;
var modifEIRL = $ac2.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL;                                         
var finDAP = $ac2.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.finEIRLGroup;
var fiscalEIRL = $ac2.cadre4DeclarationAffectationPatrimoineGroup.cadreOptionsFiscalesEIRLnonME;
var fiscalEIRLME = $ac2.cadre4DeclarationAffectationPatrimoineGroup.cadreOptionsFiscalesEIRLME;


var regentVersion = "V2008.11";  // (V2008.11, V2016.02) 
var eventRegent = []; 
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P')) {
eventRegent.push("10P");
}
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) {
eventRegent.push("15P");
}
if ((Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') or objet.entrepriseDomicile)) {
eventRegent.push("16P");
}
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('17P')) { 
eventRegent.push("17P");
}
if (Value('id').of(objet.objetModification).contains('dateActivite') and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 != null or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 != null)) {
eventRegent.push("20P");
}
if (Value('id').of(objet.objetModification).contains('modifEIRL') or objet.entrepriseEIRL) {
eventRegent.push("25P");
}
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('28P')) {
eventRegent.push("28P");
}
if (Value('id').of(objet.objetModification).eq('situationPerso')
	and Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).eq('30P')
	and Value('id').of(conjoint.objetModifConjoint).eq('conjointSalarie')
	and (Value('id').of(conjoint.objetModifStatut).eq('suppressionStatut')
	or Value('id').of(conjoint.objetModifStatut).eq('nouveauStatut'))) { 
eventRegent.push("29P");
}
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('30P')
	and (Value('id').of(conjoint.objetModifConjoint).eq('conjointCollaborateur')
	or Value('id').of(conjoint.objetModifStatut).eq('modificationStatut'))) { 
eventRegent.push("30P");
}
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('37P')) { 
eventRegent.push("37P");
}
if (Value('id').of(objet.objetModification).contains('11P') or objet.domicileEntreprise) {
eventRegent.push("11P");
eventRegent.push("54P");
eventRegent.push("80P");
}


// Valeurs à compléter par Directory  
var authorityType = "CFE"; // (CFE, TDR) 
var authorityId = _INPUT_.dataGroup.codeEDI; // (code EDI) 


//Ajout du type de la deuxième autorité
var authorityType2 = "TDR"; // (CFE, TDR) 
//Ajout du code de la deuxième autorité
var authorityId2 = _INPUT_.dataGroup.codeEDI2; // (code EDI) 


var regentFields = {}; 

// A mettre toujours sous "Z1611"
regentFields['/REGENT-XML/Emetteur']="Z1611";

// A récuperer depuis directory
regentFields['/REGENT-XML/Destinataire']= authorityId;

// Completé par directory
regentFields['/REGENT-XML/DateHeureEmission']= null;
regentFields['/REGENT-XML/VersionMessage']= null;
regentFields['/REGENT-XML/VersionNorme']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Specification/NomService']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Specification/VersionService']= null;

					// Groupe GDF : Groupe données de service
					
// Sous groupe IDF : Identification de la formalité

// Généré par destiny
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C01']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C03']= "0";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C04']= null;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05']= "R";


// Sous groupe EDF : Evènement déclaré

var occurrencesC10 = [];
var occurrence = {}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P')) {
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[1]/C10.1']= "10P";
occurrence['C10.1'] = '10P';
if(modifIdentiteDeclarant.modifIdentite.modifDateIdentite !== null) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifIdentite.modifDateIdentite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[1]/C10.2']= date;
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) {
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[2]/C10.1']= "15P";
occurrence['C10.1'] = '15P';
if(modifIdentiteDeclarant.modifIdentite.modifDateIdentite !== null) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifIdentite.modifDateIdentite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[2]/C10.2']= date;
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') or objet.entrepriseDomicile) {
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[6]/C10.1']= "16P";
occurrence['C10.1'] = '16P';
if (modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null and Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P')) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[6]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (etablissement.modifDateAdresseEntreprise !== null and objet.entrepriseDomicile) {
	var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[6]/C10.2']= date;
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('17P')) {
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[3]/C10.1']= "17P";
occurrence['C10.1'] = '17P';
if(modifIdentiteDeclarant.modifNationalite.modifDateNationalite !== null) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifNationalite.modifDateNationalite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[3]/C10.2']= date;
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(objet.objetModification).contains('dateActivite') and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 != null or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 != null)) {
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[5]/C10.1']= "20P";
occurrence['C10.1'] = '20P';
if (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 !== null) {
	var dateTemp = new Date(parseInt(objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[5]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 !== null) {
	var dateTemp = new Date(parseInt(objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[5]/C10.2']= date;
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(objet.objetModification).contains('modifEIRL') or objet.entrepriseEIRL) {
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[7]/C10.1']= "25P";
occurrence['C10.1'] = '25P';
if (declarationAffectation.modifDateDeclarationEIRL !== null) {
	var dateTemp = new Date(parseInt(declarationAffectation.modifDateDeclarationEIRL.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[7]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (modifEIRL.modifDateEIRL !== null) {
	var dateTemp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[7]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (finDAP.finEIRLDeclaration.modifDateFinEIRL !== null) {
	var dateTemp = new Date(parseInt(finDAP.finEIRLDeclaration.modifDateFinEIRL.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[7]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (objet.entrepriseEIRL and Value('id').of(objet.objetModification).contains('11P') and etablissement.modifDateAdresseEntreprise !== null) {
	var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[7]/C10.2']= date;
occurrence['C10.2'] = date;
} else if (objet.entrepriseEIRL and Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[7]/C10.2']= date;
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(objet.objetModification).eq('situationPerso')
	and Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).eq('30P')
	and Value('id').of(conjoint.objetModifConjoint).eq('conjointSalarie')
	and (Value('id').of(conjoint.objetModifStatut).eq('suppressionStatut')
	or Value('id').of(conjoint.objetModifStatut).eq('nouveauStatut'))) { 
occurrence['C10.1'] = '29P';
if (conjoint.modifDateConjoint !== null) {
	var dateTemp = new Date(parseInt(conjoint.modifDateConjoint.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[4]/C10.2']= date;
occurrence['C10.2'] = date;
}
if (Value('id').of(conjoint.objetModifStatut).eq('nouveauStatut')) { 
occurrence['C10.3'] = "Déclaration du statut de conjoint salarié";
} else {
occurrence['C10.3'] = "Suppression du statut de conjoint salarié";
}
occurrencesC10.push(occurrence);
occurrence = {}; 
}
	
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('30P')
	and (Value('id').of(conjoint.objetModifConjoint).eq('conjointCollaborateur')
	or Value('id').of(conjoint.objetModifStatut).eq('modificationStatut'))) { 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[4]/C10.1']= "30P";
occurrence['C10.1'] = '30P';
if (conjoint.modifDateConjoint !== null) {
	var dateTemp = new Date(parseInt(conjoint.modifDateConjoint.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[4]/C10.2']= date;
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('28P')) {
occurrence['C10.1'] = '28P';
if (modifIdentiteDeclarant.modifInsaisissabilite.modifdateInsaisissabilite !== null) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifInsaisissabilite.modifdateInsaisissabilite.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[4]/C10.2']= date;
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('37P')) { 
	//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[5]/C10.1']= "37P";
occurrence['C10.1'] = '37P';
if (modifIdentiteDeclarant.modifContratAppui.modifDateAppui !== null) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifContratAppui.modifDateAppui.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[5]/C10.2']= date;
occurrence['C10.2'] = date;
} 
occurrencesC10.push(occurrence);
occurrence = {}; 
}

if (Value('id').of(objet.objetModification).contains('11P') or objet.domicileEntreprise) {
occurrence['C10.1'] = '11P';
 if (Value('id').of(objet.objetModification).contains('11P') and etablissement.modifDateAdresseEntreprise != null) {
	var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
}  else if (objet.domicileEntreprise and modifIdentiteDeclarant.modifDomicile.modifDateDomicile != null) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(objet.objetModification).contains('11P') or objet.domicileEntreprise) {
occurrence['C10.1'] = '54P';
 if (Value('id').of(objet.objetModification).contains('11P') and etablissement.modifDateAdresseEntreprise != null) {
	var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
}  else if (objet.domicileEntreprise and modifIdentiteDeclarant.modifDomicile.modifDateDomicile != null) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {};
}

if (Value('id').of(objet.objetModification).contains('11P') or objet.domicileEntreprise) {
occurrence['C10.1'] = '80P';
 if (Value('id').of(objet.objetModification).contains('11P') and etablissement.modifDateAdresseEntreprise != null) {
	var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
}  else if (objet.domicileEntreprise and modifIdentiteDeclarant.modifDomicile.modifDateDomicile != null) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
occurrence['C10.2'] = date;
}
occurrencesC10.push(occurrence);
occurrence = {};
}

occurrencesC10.forEach(function (value, i) {
	var idx = i+1;
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[' + idx + ']/C10.1']= value['C10.1'];
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[' + idx + ']/C10.2']= value['C10.2'];	
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10[' + idx + ']/C10.3']= value['C10.3'];	
});

// Sous groupe DMF : Destinataire de la formalité
   
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[1]']= authorityId;
if (authorityId != "" and authorityId2 != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/DMF/C20[2]']= authorityId2;
}

// Sous groupe ADF :  adresse de correspondance

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C36']= correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance != null ? 
																	 correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance : 
																	((modifIdentiteDeclarant.modifIdentite.modifNouveauNomUsage != null 
																	and Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) ?
																	(modifIdentiteDeclarant.modifIdentite.modifNouveauNomUsage 
																	+ ' ' + (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom') ? modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms[0] : identite.personneLieePersonnePhysiquePrenom1[0])) :
																	((identite.personneLieePersonnePhysiqueNomUsage != null
																	and not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) ?
																	(identite.personneLieePersonnePhysiqueNomUsage 
																	+ ' ' + (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom') ? modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms[0] : identite.personneLieePersonnePhysiquePrenom1[0])) :
																	((Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifNomNaissance') ? modifIdentiteDeclarant.modifIdentite.modifNouveauNomNaissance : identite.personneLieePersonnePhysiqueNomNaissance) + ' ' + (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom') ? modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms[0] : identite.personneLieePersonnePhysiquePrenom1[0]))));
																	
if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']= correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune.getId();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('prof')) {
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']= adresseEntreprise.etablissementAdresseCommune.getId();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']= etablissement.adresseEtablissementNew.etablissementAdresseCommuneNew.getId();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('domi')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.3']= newAdresseDom.personneLieeAdresseCommuneNew.getId();
} 
																	
if ((Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adresseEntreprise.etablissementAdresseNumeroVoie != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') and etablissement.adresseEtablissementNew.etablissementAdresseNumeroVoieNew != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('domi') and newAdresseDom.personneLieeAdresseVoieNew != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.5']= Value('id').of(correspondance.adresseCorrespond).eq('autre') ? correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance :
																			(Value('id').of(correspondance.adresseCorrespond).eq('prof') ? adresseEntreprise.etablissementAdresseNumeroVoie : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') ? etablissement.adresseEtablissementNew.etablissementAdresseNumeroVoieNew : newAdresseDom.personneLieeAdresseVoieNew));
}
	
if (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance != null) {
   var monId1 = Value('id').of(correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId1;  
} else if (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adresseEntreprise.etablissementAdresseIndiceVoie != null) {
   var monId2 = Value('id').of(adresseEntreprise.etablissementAdresseIndiceVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId2;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') and etablissement.adresseEtablissementNew.etablissementAdresseIndiceVoieNew != null) {
   var monId4 = Value('id').of(etablissement.adresseEtablissementNew.etablissementAdresseIndiceVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId4;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('domi') and newAdresseDom.personneLieeAdresseIndiceVoieNew != null) {
	var monId5 = Value('id').of(newAdresseDom.personneLieeAdresseIndiceVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.6']          = monId5;
}

if ((Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') and etablissement.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('domi') and newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.7']= Value('id').of(correspondance.adresseCorrespond).eq('autre') ? correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance :
																			(Value('id').of(correspondance.adresseCorrespond).eq('prof') ? adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') ? etablissement.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew : newAdresseDom.personneLieeAdresseDistriutionSpecialeNew));
}

if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8']= correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('prof')) {
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8']= adresseEntreprise.etablissementAdresseCodePostal;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8']=etablissement.adresseEtablissementNew.etablissementAdresseCodePostalNew;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('domi')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.8']= newAdresseDom.personneLieeAdresseCodePostalNew;
} 

if ((Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adresseEntreprise.etablissementAdresseComplementVoie != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') and etablissement.adresseEtablissementNew.etablissementAdresseComplementVoieNew != null)
	or (Value('id').of(correspondance.adresseCorrespond).eq('domi') and newAdresseDom.personneLieeAdresseComplementAdresseNew != null)) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.10']= Value('id').of(correspondance.adresseCorrespond).eq('autre') ? correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance :
																			(Value('id').of(correspondance.adresseCorrespond).eq('prof') ? adresseEntreprise.etablissementAdresseComplementVoie : 
																			(Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') ? etablissement.adresseEtablissementNew.etablissementAdresseComplementVoieNew :
																			newAdresseDom.personneLieeAdresseComplementAdresseNew));
}

if (Value('id').of(correspondance.adresseCorrespond).eq('autre') and correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance != null) {
   var monId1 = Value('id').of(correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId1;  
} else if (Value('id').of(correspondance.adresseCorrespond).eq('prof') and adresseEntreprise.etablissementAdresseTypeVoie != null) {
   var monId2 = Value('id').of(adresseEntreprise.etablissementAdresseTypeVoie)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId2;
}else if (Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') and etablissement.adresseEtablissementNew.etablissementAdresseTypeVoieNew != null) {
   var monId4 = Value('id').of(etablissement.adresseEtablissementNew.etablissementAdresseTypeVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId4;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('domi') and newAdresseDom.personneLieeAdresseTypeVoieNew != null) {
	var monId5 = Value('id').of(newAdresseDom.personneLieeAdresseTypeVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.11']          = monId5;
}

if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12']= correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('prof')) {
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12']= adresseEntreprise.etablissementAdresseNomVoie;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12']=etablissement.adresseEtablissementNew.etablissementAdresseNomVoieNew;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('domi')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.12']= newAdresseDom.personneLieeAdresseNomVoieNew;
} 

if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune.getLabel();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('prof')) {
		regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= adresseEntreprise.etablissementAdresseCommune.getLabel();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']=etablissement.adresseEtablissementNew.etablissementAdresseCommuneNew.getLabel();
} else if (Value('id').of(correspondance.adresseCorrespond).eq('domi')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C37/C37.13']= newAdresseDom.personneLieeAdresseCommuneNew.getLabel();
} 

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.1[1]']= correspondance.infosSup.formaliteTelephone2.e164.replace('+', '00');

if (correspondance.infosSup.formaliteTelephone1 != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.1[2]']= correspondance.infosSup.formaliteTelephone1.e164.replace('+', '00');
}

if (correspondance.infosSup.telecopie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.2']= correspondance.infosSup.telecopie.e164.replace('+', '00');
}

if (correspondance.infosSup.formaliteFaxCourriel != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/ADF/C39/C39.3']= correspondance.infosSup.formaliteFaxCourriel;
}


// Sous groupe SIF : Signature de la formalité

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.1']= Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire') ? 
																			signataire.adresseMandataire.nomPrenomDenominationMandataire : 
																			((modifIdentiteDeclarant.modifIdentite.modifNouveauNomUsage != null 
																			and Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) ?
																			(modifIdentiteDeclarant.modifIdentite.modifNouveauNomUsage 
																			+ ' ' + (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom') ? modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms[0] : identite.personneLieePersonnePhysiquePrenom1[0])) :
																			((identite.personneLieePersonnePhysiqueNomUsage != null
																			and not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) ?
																			(identite.personneLieePersonnePhysiqueNomUsage 
																			+ ' ' + (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom') ? modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms[0] : identite.personneLieePersonnePhysiquePrenom1[0])) :
																			((Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifNomNaissance') ? modifIdentiteDeclarant.modifIdentite.modifNouveauNomNaissance : identite.personneLieePersonnePhysiqueNomNaissance) + ' ' + (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom') ? modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms[0] : identite.personneLieePersonnePhysiquePrenom1[0]))));

if (Value('id').of(signataire.soussigne). eq('FormaliteSignataireQualiteMandataire')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.2']= "Mandataire";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.3']= signataire.adresseMandataire.villeAdresseMandataire.getId();

if (signataire.adresseMandataire.numeroVoieMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.5']= signataire.adresseMandataire.numeroVoieMandataire;
}

if (signataire.adresseMandataire.indiceVoieMandataire !== null) {
   var monId = Value('id').of(signataire.adresseMandataire.indiceVoieMandataire)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.6']          = monId;
}

if (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.7']= signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.8']= signataire.adresseMandataire.dataCodePostalMandataire;

if (signataire.adresseMandataire.complementVoieMandataire != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.10']= signataire.adresseMandataire.complementVoieMandataire;
}

if (signataire.adresseMandataire.typeVoieMandataire !== null) {
   var monId = Value('id').of(signataire.adresseMandataire.typeVoieMandataire)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.11']          = monId;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.12']= signataire.adresseMandataire.nomVoieMandataire;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C40/C40.3/C40.3.13']= signataire.adresseMandataire.villeAdresseMandataire.getLabel();
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C41']= signataire.formaliteSignatureLieu;

if(signataire.formaliteSignatureDate !== null) {
	var dateTemp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C42']= date;
}


if (Value('id').of(modifIdentiteDeclarant.modifInsaisissabilite.modifAutreBien).contains('declaration')) {
var declarationAutreBien = modifIdentiteDeclarant.modifInsaisissabilite.publicationDeclarationInsaisssabiliteAutreBien.size();
}

log.info("Nombre déclaration Autres biens : {}", declarationAutreBien)

if (Value('id').of(modifIdentiteDeclarant.modifInsaisissabilite.modifAutreBien).contains('renonciationAutre')) {
var renonciationAutresBien = modifIdentiteDeclarant.modifInsaisissabilite.publicationRenonciationInsaisssabiliteAutreBien.size();
}

log.info("Nombre renonciation Autres biens : {}", renonciationAutresBien)

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Service/SIF/C43']= 
(Value('id').of(signataire.diffusionInfo).eq('formaliteNonDiffusionInformationOui') ? "C45=O!" : "C45=N!") 
+''+ (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('28P') ?
((Value('id').of(modifIdentiteDeclarant.modifInsaisissabilite.modifResidencePrincipale).eq('renonciation') ? ("P90.1=A!" +''+ modifIdentiteDeclarant.modifInsaisissabilite.publicationInsaisssabiliteResidencePrincipale +''+ "!") : '')
+ '' + (Value('id').of(modifIdentiteDeclarant.modifInsaisissabilite.modifResidencePrincipale).eq('revocation') ? ("P90.1=B!" +''+ modifIdentiteDeclarant.modifInsaisissabilite.publicationInsaisssabiliteResidencePrincipale +''+ "!") : '')
+ '' + ((Value('id').of(modifIdentiteDeclarant.modifInsaisissabilite.modifAutreBien).contains('declaration') 
and (modifIdentiteDeclarant.modifInsaisissabilite.publicationDeclarationInsaisssabiliteAutreBien[0].length > 0)) ? 
("P90.2=C1!"+modifIdentiteDeclarant.modifInsaisissabilite.publicationDeclarationInsaisssabiliteAutreBien[0]+"!") : '')
+''+ ((modifIdentiteDeclarant.modifInsaisissabilite.publicationDeclarationInsaisssabiliteAutreBien.size() > 1) ? ("C2" +''+ modifIdentiteDeclarant.modifInsaisissabilite.publicationDeclarationInsaisssabiliteAutreBien[1] +''+ "!") : '')
+''+ ((modifIdentiteDeclarant.modifInsaisissabilite.publicationDeclarationInsaisssabiliteAutreBien.size() > 2) ? ("C3" +''+ modifIdentiteDeclarant.modifInsaisissabilite.publicationDeclarationInsaisssabiliteAutreBien[2] +''+ "!") : '')
+''+ ((Value('id').of(modifIdentiteDeclarant.modifInsaisissabilite.modifAutreBien).contains('renonciationAutre') 
and (modifIdentiteDeclarant.modifInsaisissabilite.publicationRenonciationInsaisssabiliteAutreBien[0].length > 0)) ?  ("P90.2=D1!" +''+ modifIdentiteDeclarant.modifInsaisissabilite.publicationRenonciationInsaisssabiliteAutreBien[0] +''+ "!") : '')
+''+ ((modifIdentiteDeclarant.modifInsaisissabilite.publicationRenonciationInsaisssabiliteAutreBien.size() > 1) ? ("D2" +''+ modifIdentiteDeclarant.modifInsaisissabilite.publicationRenonciationInsaisssabiliteAutreBien[1] +''+ "!") : '')
+''+ ((modifIdentiteDeclarant.modifInsaisissabilite.publicationRenonciationInsaisssabiliteAutreBien.size() > 2) ? ("D3" +''+ modifIdentiteDeclarant.modifInsaisissabilite.publicationRenonciationInsaisssabiliteAutreBien[2] +''+ "!") : '')
) : '')
+ ' ' + (correspondance.formaliteObservations != null ? correspondance.formaliteObservations : '') ;


								// Groupe ICP : Identification complète de la personne physique

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.2']= (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P') and Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifNomNaissance')) ? 
																					modifIdentiteDeclarant.modifIdentite.modifNouveauNomNaissance : identite.personneLieePersonnePhysiqueNomNaissance;

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P') and Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom')) {
var prenoms=[];
for ( i = 0; i < modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms.size() ; i++ ){prenoms.push(modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.3']= prenoms;
} else {
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.3']= prenoms;
} 

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P') and modifIdentiteDeclarant.modifIdentite.modifNouveauNomUsage != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P02/P02.1']= modifIdentiteDeclarant.modifIdentite.modifNouveauNomUsage;
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P') and identite.personneLieePersonnePhysiqueNomUsage != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P02/P02.1']= identite.personneLieePersonnePhysiqueNomUsage;
}

if(identite.personneLieePersonnePhysiqueDateNaissance !== null) {
	var dateTemp = new Date(parseInt(identite.personneLieePersonnePhysiqueDateNaissance.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.1']= date;
}

if (not Value('id').of(identite.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	var idPays = identite.personneLieePPLieuNaissancePays.getId();
	var result = idPays.match(regEx)[0]; 
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.2']= result;
} else if (Value('id').of(identite.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.2'] = identite.personneLieePPLieuNaissanceCommune.getId();
}	

if (not Value('id').of(identite.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {																					 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.3']= identite.personneLieePPLieuNaissancePays.getLabel();
}

if (Value('id').of(identite.personneLieePPLieuNaissancePays).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P03/P03.4']= identite.personneLieePPLieuNaissanceCommune.getLabel();
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') or objet.entrepriseDomicile) {
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.3']= newAdresseDom.personneLieeAdresseCommuneNew.getId();
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and objet.entrepriseDomicile) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.3']= newAdresseDom2.etablissementAdresseCommuneNew.getId();
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and newAdresseDom.personneLieeAdresseVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.5']= newAdresseDom.personneLieeAdresseVoieNew;
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and objet.entrepriseDomicile and newAdresseDom2.etablissementAdresseNumeroVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.5']= newAdresseDom2.etablissementAdresseNumeroVoieNew;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and newAdresseDom.personneLieeAdresseIndiceVoieNew != null) {
   var monId1 = Value('id').of(newAdresseDom.personneLieeAdresseIndiceVoieNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.6']          = monId1;
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and objet.entrepriseDomicile and newAdresseDom2.etablissementAdresseIndiceVoieNew != null) {
   var monId2 = Value('id').of(newAdresseDom2.etablissementAdresseIndiceVoieNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.6']          = monId2;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.7']= newAdresseDom.personneLieeAdresseDistriutionSpecialeNew;
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and objet.entrepriseDomicile and newAdresseDom2.etablissementAdresseDistriutionSpecialeVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.7']= newAdresseDom2.etablissementAdresseDistriutionSpecialeVoieNew;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and newAdresseDom.personneLieeAdresseCodePostalNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.8']= newAdresseDom.personneLieeAdresseCodePostalNew;
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and objet.entrepriseDomicile and newAdresseDom2.etablissementAdresseCodePostalNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.8']= newAdresseDom2.etablissementAdresseCodePostalNew;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and newAdresseDom.personneLieeAdresseComplementAdresseNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.10']= newAdresseDom.personneLieeAdresseComplementAdresseNew;
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and objet.entrepriseDomicile and newAdresseDom2.etablissementAdresseComplementVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.10']= newAdresseDom2.etablissementAdresseComplementVoieNew;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and newAdresseDom.personneLieeAdresseTypeVoieNew != null) {
   var monId1 = Value('id').of(newAdresseDom.personneLieeAdresseTypeVoieNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.11']          = monId1;
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and objet.entrepriseDomicile and newAdresseDom2.etablissementAdresseTypeVoieNew != null) {
   var monId2 = Value('id').of(newAdresseDom2.etablissementAdresseTypeVoieNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.11']          = monId2;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.12']= newAdresseDom.personneLieeAdresseNomVoieNew;
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and objet.entrepriseDomicile) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.12']= newAdresseDom2.etablissementAdresseNomVoieNew;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.13']= newAdresseDom.personneLieeAdresseCommuneNew.getLabel();
} else if (not Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P') and objet.entrepriseDomicile) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P04/P04.13']= newAdresseDom2.etablissementAdresseCommuneNew.getLabel();
}

if (modifIdentiteDeclarant.modifDomicile.domicileMemeDept != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P05']= modifIdentiteDeclarant.modifDomicile.domicileMemeDept.getId();
}
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).contains('modificationEIRL') or Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).contains('finEIRL') or objet.entrepriseEIRL) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.1']= "O";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.2']= rappelIdentification.eirlRappelDenomination;

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.3']= rappelIdentification.cadreEirlRappelAdresse.communeAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.communeAdresseEirl.getId() : adresseEntreprise.etablissementAdresseCommune.getId();

if (not objet.cadreObjetModificationEIRL.memeAdresseEIRL and rappelIdentification.cadreEirlRappelAdresse.numeroAdresseEirl != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.5']= rappelIdentification.cadreEirlRappelAdresse.numeroAdresseEirl;
} else if ((objet.cadreObjetModificationEIRL.memeAdresseEIRL or objet.entrepriseEIRL) and adresseEntreprise.etablissementAdresseNumeroVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.5']= adresseEntreprise.etablissementAdresseNumeroVoie;
}

if (not objet.cadreObjetModificationEIRL.memeAdresseEIRL and rappelIdentification.cadreEirlRappelAdresse.indiceVoieAdresseEirl != null) {
   var monId1 = Value('id').of(rappelIdentification.cadreEirlRappelAdresse.indiceVoieAdresseEirl)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.6']          = monId1;
} else if ((objet.cadreObjetModificationEIRL.memeAdresseEIRL or objet.entrepriseEIRL) and adresseEntreprise.etablissementAdresseIndiceVoie != null) {
   var monId2 = Value('id').of(adresseEntreprise.etablissementAdresseIndiceVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.6']          = monId2;
}

if (not objet.cadreObjetModificationEIRL.memeAdresseEIRL and rappelIdentification.cadreEirlRappelAdresse.distriutionSpecialeAdresseEirl != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.7']= rappelIdentification.cadreEirlRappelAdresse.distriutionSpecialeAdresseEirl;
} else if ((objet.cadreObjetModificationEIRL.memeAdresseEIRL or objet.entrepriseEIRL) and adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.7']= adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.8']= rappelIdentification.cadreEirlRappelAdresse.codePostalAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.codePostalAdresseEirl : adresseEntreprise.etablissementAdresseCodePostal;

if (not objet.cadreObjetModificationEIRL.memeAdresseEIRL and rappelIdentification.cadreEirlRappelAdresse.complementAdresseEirl != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.10']= rappelIdentification.cadreEirlRappelAdresse.complementAdresseEirl;
} else if ((objet.cadreObjetModificationEIRL.memeAdresseEIRL or objet.entrepriseEIRL) and adresseEntreprise.etablissementAdresseComplementVoie != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.10']= adresseEntreprise.etablissementAdresseComplementVoie;
}

if (not objet.cadreObjetModificationEIRL.memeAdresseEIRL and rappelIdentification.cadreEirlRappelAdresse.typeVoieAdresseEirl != null) {
   var monId1 = Value('id').of(rappelIdentification.cadreEirlRappelAdresse.typeVoieAdresseEirl)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.11']          = monId1;
} else if ((objet.cadreObjetModificationEIRL.memeAdresseEIRL or objet.entrepriseEIRL) and adresseEntreprise.etablissementAdresseTypeVoie != null) {
   var monId2 = Value('id').of(adresseEntreprise.etablissementAdresseTypeVoie)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.11']          = monId2;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.12']= rappelIdentification.cadreEirlRappelAdresse.nomVoieAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.nomVoieAdresseEirl : adresseEntreprise.etablissementAdresseNomVoie;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.3/P95.3.13']= rappelIdentification.cadreEirlRappelAdresse.communeAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.communeAdresseEirl.getLabel() : adresseEntreprise.etablissementAdresseCommune.getLabel();

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.4']= "4";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P95/P95.5']= rappelIdentification.eirlRappelLieuImmatriculation;
}


								// Groupe AIP : Ancienne identification de la personne physique 

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P11']= identite.personneLieePersonnePhysiqueNomNaissance;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P') and identite.personneLieePersonnePhysiqueNomUsage != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P13']= identite.personneLieePersonnePhysiqueNomUsage;
}

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P')) {
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/AIP/P14']= prenoms;
}

								// Groupe NAP : Nationalité de la personne physique
								
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('17P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/NAP/P21']= modifIdentiteDeclarant.modifNationalite.modifNewNationalite;
}

								// Groupe ISP : Insaisissabilité
								
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('28P')) {								
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ISP/P90']= ".";
}								


								// Groupe DAP : Déclaration d'affectation du patrimoine (si option pour l'EIRL)
								
if (Value('id').of(objet.objetModification).contains('modifEIRL') or objet.entrepriseEIRL) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P70']= objet.cadreObjetDeclarationEIRL.declarationEIRL ?
																			   (Value('id').of(declarationAffectation.eirlStatut).eq('EirlStatutEIRLReprise') ? "R" : "O") : 
																			   ((Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') or objet.entrepriseEIRL) ? "M" : "F");

if (objet.cadreObjetDeclarationEIRL.declarationEIRL) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P71']= declarationAffectation.declarationPatrimoine.eirlDenomination;
} else if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDenoEIRL')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P71']= modifEIRL.newDenoEIRL;
}

if (objet.cadreObjetDeclarationEIRL.declarationEIRL and declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable !== null) {
    var dateTmp = new Date(parseInt(declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable.getTimeInMillis()));
    var dateClotureEc = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateClotureEc = dateClotureEc.concat(pad(month.toString()));
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P72']          = dateClotureEc;
} else if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDateCloture')) {
    var dateTmp = new Date(parseInt(modifEIRL.eirlNewDateClotureExerciceComptable.getTimeInMillis()));
    var dateClotureEc = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateClotureEc = dateClotureEc.concat(pad(month.toString()));
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P72']          = dateClotureEc;
}

if (objet.cadreObjetDeclarationEIRL.declarationEIRL) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P73']= declarationAffectation.declarationPatrimoine.eirlObjet;
} else if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifObjet')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P73']= modifEIRL.newObjetEIRL;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.3']= modifEIRL.newAdresseEIRL.communeAdresseEirlNew.getId();
} else if (objet.entrepriseEIRL and Value('id').of(objet.objetEtablissement).contains('11P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.3']= etablissement.adresseEtablissementNew.etablissementAdresseCommuneNew.getId();
} else if (objet.entrepriseEIRL and objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.3']= newAdresseDom.personneLieeAdresseCommuneNew.getId();
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse') and modifEIRL.newAdresseEIRL.numeroAdresseEirlNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.5']= modifEIRL.newAdresseEIRL.numeroAdresseEirlNew;
} else if (objet.entrepriseEIRL and Value('id').of(objet.objetEtablissement).contains('11P') and etablissement.adresseEtablissementNew.etablissementAdresseNumeroVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.5']= etablissement.adresseEtablissementNew.etablissementAdresseNumeroVoieNew;
} else if (objet.entrepriseEIRL and objet.domicileEntreprise and newAdresseDom.personneLieeAdresseVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.5']= newAdresseDom.personneLieeAdresseVoieNew;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse') and modifEIRL.newAdresseEIRL.indiceVoieAdresseEirlNew != null) {
   var monId1 = Value('id').of(modifEIRL.newAdresseEIRL.indiceVoieAdresseEirlNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.6']          = monId1;
} else if (objet.entrepriseEIRL and Value('id').of(objet.objetEtablissement).contains('11P') and etablissement.adresseEtablissementNew.etablissementAdresseIndiceVoieNew != null) {
   var monId2 = Value('id').of(etablissement.adresseEtablissementNew.etablissementAdresseIndiceVoieNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.6']          = monId2;
} else if (objet.entrepriseEIRL and objet.domicileEntreprise and newAdresseDom.personneLieeAdresseIndiceVoieNew != null) {
   var monId3 = Value('id').of(newAdresseDom.personneLieeAdresseIndiceVoieNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.6']          = monId3;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse') and modifEIRL.newAdresseEIRL.distriutionSpecialeAdresseEirlNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.7']= modifEIRL.newAdresseEIRL.distriutionSpecialeAdresseEirlNew;
} else if (objet.entrepriseEIRL and Value('id').of(objet.objetEtablissement).contains('11P') and etablissement.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.7']= etablissement.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew;
} else if (objet.entrepriseEIRL and objet.domicileEntreprise and newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.7']= newAdresseDom.personneLieeAdresseDistriutionSpecialeNew;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.8']= modifEIRL.newAdresseEIRL.codePostalAdresseEirlNew;
} else if (objet.entrepriseEIRL and Value('id').of(objet.objetEtablissement).contains('11P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.8']= etablissement.adresseEtablissementNew.etablissementAdresseCodePostalNew;
} else if (objet.entrepriseEIRL and objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.8']= newAdresseDom.personneLieeAdresseCodePostalNew;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse') and modifEIRL.newAdresseEIRL.complementAdresseEirlNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.10']= modifEIRL.newAdresseEIRL.complementAdresseEirlNew;
} else if (objet.entrepriseEIRL and Value('id').of(objet.objetEtablissement).contains('11P') and etablissement.adresseEtablissementNew.etablissementAdresseComplementVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.10']= etablissement.adresseEtablissementNew.etablissementAdresseComplementVoieNew;
} else if (objet.entrepriseEIRL and objet.domicileEntreprise and newAdresseDom.personneLieeAdresseComplementAdresseNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.10']= newAdresseDom.personneLieeAdresseComplementAdresseNew;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse') and modifEIRL.newAdresseEIRL.typeVoieAdresseEirlNew != null) {
   var monId1 = Value('id').of(modifEIRL.newAdresseEIRL.typeVoieAdresseEirlNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.11']          = monId1;
} else if (objet.entrepriseEIRL and Value('id').of(objet.objetEtablissement).contains('11P') and etablissement.adresseEtablissementNew.etablissementAdresseTypeVoieNew != null) {
   var monId2 = Value('id').of(etablissement.adresseEtablissementNew.etablissementAdresseTypeVoieNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.11']          = monId2;
} else if (objet.entrepriseEIRL and objet.domicileEntreprise and newAdresseDom.personneLieeAdresseTypeVoieNew != null) {
   var monId3 = Value('id').of(newAdresseDom.personneLieeAdresseTypeVoieNew)._eval();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.11']          = monId3;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.12']= modifEIRL.newAdresseEIRL.nomVoieAdresseEirlNew;
} else if (objet.entrepriseEIRL and  Value('id').of(objet.objetEtablissement).contains('11P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.12']= etablissement.adresseEtablissementNew.etablissementAdresseNomVoieNew;
} else if (objet.entrepriseEIRL and objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.12']= newAdresseDom.personneLieeAdresseNomVoieNew;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.13']= modifEIRL.newAdresseEIRL.communeAdresseEirlNew.getLabel();
} else if (objet.entrepriseEIRL and Value('id').of(objet.objetEtablissement).contains('11P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.13']= etablissement.adresseEtablissementNew.etablissementAdresseCommuneNew.getLabel();
} else if (objet.entrepriseEIRL and objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P74/P74.13']= newAdresseDom.personneLieeAdresseCommuneNew.getLabel();
}

if (Value('id').of(declarationAffectation.eirlStatut).eq('EirlStatutEIRLReprise')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P76/P76.1']= declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLDenomination;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P76/P76.2']= "4";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P76/P76.3']= declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLLieuImmatriculation;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P76/P76.4']= declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN.split(' ').join('');
}

if (modifEIRL.modifDateEIRL !== null) {
	var dateTemp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P77']= date;
} else if (finDAP.finEIRLDeclaration.modifDateFinEIRL !== null) {
	var dateTemp = new Date(parseInt(finDAP.finEIRLDeclaration.modifDateFinEIRL.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P77']= date;
} else if (objet.entrepriseEIRL and etablissement.modifDateAdresseEntreprise !== null) {
	var dateTemp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P77']= date;
} else if (objet.entrepriseEIRL and modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
	var dateTemp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P77']= date;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDAP')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P78']= "O";
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('finEIRL')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/DAP/P79']= Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLRenonciationAvecPoursuite') ? "1" : 
																			   (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLRenonciationSansPoursuite') ? "2" : 
																			   (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLCessionPP') ? "3" : "4"));
}
}

							
								// Groupe SMP : Situation matrimoniale de la personne physique (conditionnel)

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('30P')
	and (Value('id').of(conjoint.objetModifConjoint).eq('conjointCollaborateur')
	or Value('id').of(conjoint.objetModifStatut).eq('modificationStatut'))) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P25/P25.2']= conjoint.modifNomNaissanceConjoint;

var prenoms=[];
for ( i = 0; i < conjoint.modifPrenomConjoint.size() ; i++ ){prenoms.push(conjoint.modifPrenomConjoint[i]);}      
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P25/P25.3']= prenoms;

if(conjoint.modifNomUsageConjoint != null) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P25/P25.4']= conjoint.modifNomUsageConjoint;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P40']= (Value('id').of(conjoint.objetModifStatut).eq('suppressionStatut') or (Value('id').of(conjoint.objetModifConjoint).eq('conjointSalarie') and Value('id').of(conjoint.objetModifStatut).eq('modificationStatut'))) ? "S" : "O";

if (Value('id').of(conjoint.objetModifStatut).eq('nouveauStatut')
	or (Value('id').of(conjoint.objetModifConjoint).eq('conjointCollaborateur') and Value('id').of(conjoint.objetModifStatut).eq('modificationStatut'))) {
if(conjoint.modifDateNaissanceConjoint !== null) {
	var dateTemp = new Date(parseInt(conjoint.modifDateNaissanceConjoint.getTimeInMillis()));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	var date = year + "-" + month + "-" + day ; 
    regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.1']= date;
}
 
if	(Value('id').of(conjoint.modifLieuNaissancePaysConjoint).eq('FRXXXXX')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.2']= conjoint.modifLieuNaissanceCommuneConjoint.getId();
} else if (not Value('id').of(conjoint.modifLieuNaissancePaysConjoint).eq('FRXXXXX')) {
		var idPays = conjoint.modifLieuNaissancePaysConjoint.getId();
		var result = idPays.match(regEx)[0]; 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.2']= result;
}

if (not Value('id').of(conjoint.modifLieuNaissancePaysConjoint).eq('FRXXXXX')) {																					 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.3']= conjoint.modifLieuNaissancePaysConjoint.getLabel();
}																					 

if (Value('id').of(conjoint.modifLieuNaissancePaysConjoint).eq('FRXXXXX')) {	
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/SMP/P41/P41.4']= conjoint.modifLieuNaissanceCommuneConjoint.getLabel();
}
}
}

								// Groupe GCS : Groupe complément social

// Sous groupe ISS : Immatriculation sécurité sociale du travailleur non salarié
/*
if (Value('id').of(objet.objetModification).contains('dateActivite') and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 != null or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 != null)) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A10/A10.1']  = "0000000000000";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A10/A10.2']  = "00";
}
*/

// Sous groupe SCS : Situation du conjoint vis-à-vis de la sécurité sociale
/*
if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('30P') and Value ('id').of(conjoint.objetModifConjoint).eq('modifMentionNouveauConjoint')) {
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A53/A53.4']= conjoint.voletSocialConjointCouvertAssuranceMaladieOui ? "O" : "N";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A55/A55.1']  = "0000000000000";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A55/A55.2']  = "00";
}
*/

// Groupe IPU : Immatriculation principale de l'enttreprise

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/IPU/U02']= identite.siren.split(' ').join('');

// Fin regent pour le 20P//
// Fin regent pour le 28P//

// Groupe SIU : siège de l'entreprise (adresse de l'entreprise individuelle)

if (Value('id').of(objet.objetModification).contains('11P') or objet.domicileEntreprise) { 

if (objet.entrepriseDomicile or objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U16']= "O";
}

if (Value('id').of(objet.objetModification).contains('11P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.3']= etablissement.adresseEtablissementNew.etablissementAdresseCommuneNew.getId();
} else if (objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.3']= newAdresseDom.personneLieeAdresseCommuneNew.getId();
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.3']= adresseEntreprise.etablissementAdresseCommune.getId();
}

if (Value('id').of(objet.objetModification).contains('11P') and etablissement.adresseEtablissementNew.etablissementAdresseNumeroVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.5']= etablissement.adresseEtablissementNew.etablissementAdresseNumeroVoieNew;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.5']= newAdresseDom.personneLieeAdresseVoieNew;
} else if (not Value('id').of(objet.objetModification).contains('11P') 
	and not objet.domicileEntreprise and adresseEntreprise.etablissementAdresseNumeroVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.5']= adresseEntreprise.etablissementAdresseNumeroVoie;
}

if (Value('id').of(objet.objetModification).contains('11P') and etablissement.adresseEtablissementNew.etablissementAdresseIndiceVoieNew != null) {
   var monId1 = Value('id').of(etablissement.adresseEtablissementNew.etablissementAdresseIndiceVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.6']          = monId1;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseIndiceVoieNew != null) {
   var monId2 = Value('id').of(newAdresseDom.personneLieeAdresseIndiceVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.6']          = monId2;
} else if (not Value('id').of(objet.objetModification).contains('11P')
	and not objet.domicileEntreprise and adresseEntreprise.etablissementAdresseIndiceVoie != null){
var monId3 = Value('id').of(adresseEntreprise.etablissementAdresseIndiceVoie)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.6']		  = monId3;
}

if (Value('id').of(objet.objetModification).contains('11P') and etablissement.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.7']= etablissement.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.7']= newAdresseDom.personneLieeAdresseDistriutionSpecialeNew;
} else if (not Value('id').of(objet.objetModification).contains('11P')
	and not objet.domicileEntreprise and adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.7']= adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie;
}

if (Value('id').of(objet.objetModification).contains('11P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.8']= etablissement.adresseEtablissementNew.etablissementAdresseCodePostalNew;
} else if (objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.8']= newAdresseDom.personneLieeAdresseCodePostalNew;
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.8']= adresseEntreprise.etablissementAdresseCodePostal;
}

if (Value('id').of(objet.objetModification).contains('11P') and etablissement.adresseEtablissementNew.etablissementAdresseComplementVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.10']= etablissement.adresseEtablissementNew.etablissementAdresseComplementVoieNew;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseComplementAdresseNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.10']= newAdresseDom.personneLieeAdresseComplementAdresseNew;
}  else if (not Value('id').of(objet.objetModification).contains('11P')
	and not objet.domicileEntreprise and adresseEntreprise.etablissementAdresseComplementVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.10']= adresseEntreprise.etablissementAdresseComplementVoie;
}	

if (Value('id').of(objet.objetModification).contains('11P') and etablissement.adresseEtablissementNew.etablissementAdresseTypeVoieNew != null) {
   var monId1 = Value('id').of(etablissement.adresseEtablissementNew.etablissementAdresseTypeVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.11']          = monId1;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseTypeVoieNew != null) {
   var monId2 = Value('id').of(newAdresseDom.personneLieeAdresseTypeVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.11']          = monId2;
}  else if (not Value('id').of(objet.objetModification).contains('11P')
	and not objet.domicileEntreprise and adresseEntreprise.etablissementAdresseTypeVoie != null){
	var monId3 = Value('id').of(adresseEntreprise.etablissementAdresseTypeVoie)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.11']		  = monId3;
}

if (Value('id').of(objet.objetModification).contains('11P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.12']= etablissement.adresseEtablissementNew.etablissementAdresseNomVoieNew;
} else if (objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.12']= newAdresseDom.personneLieeAdresseNomVoieNew;
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.12']= adresseEntreprise.etablissementAdresseNomVoie;
}

if (Value('id').of(objet.objetModification).contains('11P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.13']= etablissement.adresseEtablissementNew.etablissementAdresseCommuneNew.getLabel();
} else if (objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.13']= newAdresseDom.personneLieeAdresseCommuneNew.getLabel();
} else {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/SIU/U11/U11.13']= adresseEntreprise.etablissementAdresseCommune.getLabel();
}
}

							// Groupe CPU : Caractéristiques de l'entreprise

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('37P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/CPU/U40/U40.1']= "R";
}

// Fin Regent 37P //	
	
// Groupe OFU : Option Fiscale EIRL (si EIRL) 
							
if (objet.cadreObjetDeclarationEIRL.declarationEIRL) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U70']= (Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesRsBNC') or identite.optionME) ? '110' :
																		(Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesDcBNC') ? '111' : 
																		(Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalIS).eq('RegimeFiscalRegimeImpositionBeneficesRsIS') ? "114" : "115"));

if (fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionCreanceDette) {																		
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U71']= "210";
}
else if (Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionIS')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U71']= "211";
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U72/U72.1'] = (Value('id').of(fiscalEIRL.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARfTVA') or Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesRsBNC') or identite.optionME) ? "310" :
																				(Value('id').of(fiscalEIRL.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARsTVA') ? "311" : "312");

if (fiscalEIRL.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres1DC) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U72/U72.2'] = "410";
}

if (identite.optionME) {																			
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U74']= fiscalEIRLME.regimeFiscalRegimeImpositionBeneficesVersementLiberatoire ? "O" : "N";
}
}


// Groupe LIU lieu d'imposition de l'entreprise

//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/LIU/U36/U36.2']= identite.lieuDepotImpot;


// Fin regent pour le 10P//
// Fin regent pour le 15P//
// Fin regent pour le 16P//
// Fin regent pour le 17P//
// Fin Regent pour le 25P //
// Fin regent pour le 30P//

				// Groupe ICE : Identification complète de l'établissement 

if (Value('id').of(objet.objetModification).contains('11P')
or objet.domicileEntreprise) { 
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E01']= "1";
	
if (Value('id').of(objet.objetModification).contains('11P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.3']= etablissement.adresseEtablissementNew.etablissementAdresseCommuneNew.getId();
} else if (objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.3']= newAdresseDom.personneLieeAdresseCommuneNew.getId();
} 

if (Value('id').of(objet.objetModification).contains('11P')
	and etablissement.adresseEtablissementNew.etablissementAdresseNumeroVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5']= etablissement.adresseEtablissementNew.etablissementAdresseNumeroVoieNew;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.5']= newAdresseDom.personneLieeAdresseVoieNew;
} 

if (Value('id').of(objet.objetModification).contains('11P')
	and etablissement.adresseEtablissementNew.etablissementAdresseIndiceVoieNew != null) {
   var monId1 = Value('id').of(etablissement.adresseEtablissementNew.etablissementAdresseIndiceVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.6']          = monId1;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseIndiceVoieNew != null) {
   var monId2 = Value('id').of(newAdresseDom.personneLieeAdresseIndiceVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.6']          = monId2;
} 

if (Value('id').of(objet.objetModification).contains('11P')
	and etablissement.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.7']= etablissement.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.7']= newAdresseDom.personneLieeAdresseDistriutionSpecialeNew;
} 

if (Value('id').of(objet.objetModification).contains('11P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.8']= etablissement.adresseEtablissementNew.etablissementAdresseCodePostalNew;
} else if (objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.8']= newAdresseDom.personneLieeAdresseCodePostalNew;
}

if (Value('id').of(objet.objetModification).contains('11P')
	and etablissement.adresseEtablissementNew.etablissementAdresseComplementVoieNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.10']= etablissement.adresseEtablissementNew.etablissementAdresseComplementVoieNew;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseComplementAdresseNew != null) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.10']= newAdresseDom.personneLieeAdresseComplementAdresseNew;
} 	

if (Value('id').of(objet.objetModification).contains('11P')
	and etablissement.adresseEtablissementNew.etablissementAdresseTypeVoieNew != null) {
   var monId1 = Value('id').of(etablissement.adresseEtablissementNew.etablissementAdresseTypeVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.11']          = monId1;
} else if (objet.domicileEntreprise and newAdresseDom.personneLieeAdresseTypeVoieNew != null) {
   var monId2 = Value('id').of(newAdresseDom.personneLieeAdresseTypeVoieNew)._eval();
   regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.11']          = monId2;
} 

if (Value('id').of(objet.objetModification).contains('11P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.12']= etablissement.adresseEtablissementNew.etablissementAdresseNomVoieNew;
} else if (objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.12']= newAdresseDom.personneLieeAdresseNomVoieNew;
} 

if (Value('id').of(objet.objetModification).contains('11P')) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.13']= etablissement.adresseEtablissementNew.etablissementAdresseCommuneNew.getLabel();
} else if (objet.domicileEntreprise) {
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E02/E02.13']= newAdresseDom.personneLieeAdresseCommuneNew.getLabel();
} 

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E03/E03.1']= "000000000";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E03/E03.2']= "00000";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ICE/E04']= "3";

				// Groupe IAE : Identification ancienne de l'établissement

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.3']= adresseEntreprise.etablissementAdresseCommune.getId();

if (adresseEntreprise.etablissementAdresseNumeroVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.5']= adresseEntreprise.etablissementAdresseNumeroVoie;
}

if (adresseEntreprise.etablissementAdresseIndiceVoie != null){
   var monId2 = Value('id').of(adresseEntreprise.etablissementAdresseIndiceVoie)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.6']		  = monId2;
}

if (adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.7']= adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.8']= adresseEntreprise.etablissementAdresseCodePostal;

if (adresseEntreprise.etablissementAdresseComplementVoie != null){
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.10']= adresseEntreprise.etablissementAdresseComplementVoie;
}

if (adresseEntreprise.etablissementAdresseTypeVoie != null){
   var monId2 = Value('id').of(adresseEntreprise.etablissementAdresseTypeVoie)._eval();
	regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.11']		  = monId2;
}

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.12']= adresseEntreprise.etablissementAdresseNomVoie;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E12/E12.13']= adresseEntreprise.etablissementAdresseCommune.getLabel();
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E13/E13.1']= "000000000";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E13/E13.2']= "00000";
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/IAE/E17']= "3";

			   

			   // Groupe ORE : Origine de l'établissement

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ORE/E21/E21.1']=  "1";


				// Groupe DEE : Destination de l'établissement

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/DEE/E41/E41.1']= "B";


// Fin Regent pour le 11P //
	

		// Groupe ACE : Activité de l'établissement

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E70']= etablissement.etablissementSecteursActivites ;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/ACE/E71']= etablissement.etablissementActivitePlusImportante != null ? etablissement.etablissementActivitePlusImportante : etablissement.etablissementSecteursActivites ;

		// Groupe CAE

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/CAE/E73/E73.1']= 'P';

		// Groupe SAE : salariés de l'établissement

//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E84']= activite.etablissementEffectifSalariePresenceOui ? activite.cadreEffectifSalarie.etablissementEffectifSalarieNombre : "0";
//regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E86']= activite.etablissementEffectifSalariePresenceOui ? "O" : "N";
}


log.info("Regent fields : {}", regentFields);
//-->DEST-689 : Afficher les erreurs XSD dans la formalité quand il y'a une erreur de génération

// Call to the XML-REGENT generation WS 
var response = nash.service.request('${regent.baseUrl}/private/v1/xml-regent/generate/{regentVersion}/{authorityType}/{authorityId}', regentVersion, authorityType, authorityId) 
.dataType('application/json') // 
.accept('json') // 
.param('listTypeEvenement',eventRegent) 
//-->MINE-263 : Permettre à l'équipe FF de voir les problèmes de validation XSD et le regent sur studio
.continueOnError(true) //
.post(JSON.stringify(regentFields)); 

// Record the generated XML-REGENT 
//MOD déplacement du test de la réponse du premier ws
if (response != null && response.status == 200) { 
    var xmlRegentStr = response.asBytes(); 
    nash.record.saveFile("XML_REGENT.xml",xmlRegentStr); 

//debut de l'ajout
 if (authorityId2 != null) {
//MOD extraction du numéro de liasse du premeir regent généré
var numeroLiasse = nash.xml.extract('/XML_REGENT.xml', '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02');
numeroLiasse = JSON.parse(numeroLiasse);
_log.info('extracted numero de liasse is {}', numeroLiasse);
 regentFields['/REGENT-XML/Destinataire']= authorityId2;
 
 // Filtre regent greffe
var undefined;
// Groupe SUP
// Groupe GCS/ISS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A10/A10.1']  = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/ISS/A10/A10.2'] = undefined;
// Groupe GCS/SCS
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A53/A53.4']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A55/A55.1'] = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS/SCS/A55/A55.2'] = undefined;
// Groupe OFU
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U70']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U71']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U72/U72.1']= undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U72/U72.2'] = undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/OFU/U74']= undefined; 
// Groupe SAE
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E84']=undefined;
regentFields['/REGENT-XML/ServiceApplicatif/Liasse/Entreprise/Etablissement/SAE/E86']= undefined;

regentFields['/REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/GCS']  = undefined;


 //MOD modification de l'appel pour la génération du deuxième régent afin de prendre en compte le numéro de liasse du premier
 var response2 = nash.service.request('${regent.baseUrl}/private/v1/xml-regent/generate/{regentVersion}/{authorityType}/{authorityId}', regentVersion, authorityType2, authorityId2) 
.dataType('application/json') // 
.accept('json') // 
.param('listTypeEvenement',eventRegent) 
.param('liasseNumber', numeroLiasse.C02) 
.continueOnError(true)
.post(JSON.stringify(regentFields)); 


	//Début de l'ajout
	//MOD modifier reponse en response2
	if (response2 != null && response2.status == 200) { 
		var xmlRegentStr = response2.asBytes(); 
		nash.record.saveFile("XML_REGENT2.xml",xmlRegentStr);
	}else{
		
		var returnedResponse = response2.asObject();
		_log.info("Call ws regent returned errors  {}", returnedResponse);
		
		
		var regent2 = returnedResponse['regent'];
		var errors2 = returnedResponse['errors']; 
		nash.record.saveFile("XML_REGENT2.xml",regent2.getBytes()); 
		nash.record.saveFile("XML_VALIDATION_ERRORS_2.xml",errors2.getBytes()); 
		
		//-->MINE-263 : Permettre à l'équipe FF de voir les problèmes de validation XSD et le regent sur studio
		
		return spec.create({
		id : 'xmlGenerationConfirmation',
		label : "Xml Regent confirmation message",
		groups : [ spec.createGroup({
				id : 'Regent ',
				description : "Une erreur s'est produite lors de la génération du XML Regent.",
				data : [
				spec.createData({
					id: 'regent',
					label: "XML regent généré",
					type: 'Text',
					mandatory: true,
					value: regent2
				}),spec.createData({
					id: 'errors',
					label: "Erreurs de validations xsd",
					type: 'Text',
					mandatory: false,
					value: errors2
				}),spec.createData({
					id: 'blockingField',
					label: "xsd erroné",
					help:"Ce champs sert à arrêter le process du dossier pour que le support puisse le consulter",
					type: 'StringReadOnly',
					mandatory: true
				})]
			})]
		});
	}	
}	

	return spec.create({
	id : 'xmlGenerationConfirmation',
	label : "Xml Regent confirmation message",
	groups : [ spec.createGroup({
		id : 'confirmationMessageOk',
		description : "Le fichier XML Regent a été généré et ajouté au dossier.",
		data : []
		}) ]
	});
	
}else{
	
	var returnedResponse = response.asObject();
	_log.info("Call ws regent returned errors  {}", returnedResponse);
	
	
	var regent = returnedResponse['regent'];
	var errors = returnedResponse['errors']; 
	nash.record.saveFile("XML_REGENT.xml",regent.getBytes()); 
	nash.record.saveFile("XML_VALIDATION_ERRORS.xml",errors.getBytes()); 
	
	
	return spec.create({
	id : 'xmlGenerationConfirmation',
	label : "Xml Regent confirmation message",
	groups : [ spec.createGroup({
		id : 'Regent ',
		description : "Une erreur s'est produite lors de la génération du XML Regent de la première autorité.",
		data : [
				spec.createData({
					id: 'regent',
					label: "XML regent généré",
					type: 'Text',
					mandatory: true,
					value: regent
				}),spec.createData({
					id: 'errors',
					label: "Erreurs de validations xsd",
					type: 'Text',
					mandatory: false,
					value: errors
				}),spec.createData({
					id: 'blockingField',
					label: "xsd erroné",
					help:"Ce champs sert à arrêter le process du dossier pour que le support puisse le consulter",
					type: 'StringReadOnly',
					mandatory: true
			})]
		}) ]
	});
}	