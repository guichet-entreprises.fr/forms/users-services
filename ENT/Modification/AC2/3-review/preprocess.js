var formFields = {};
var cerfaFields = {};
function pad(s) { return (s < 10) ? '0' + s : s; }

formFields['formulaire_dependance_personnePhysique']                        = true;
formFields['formulaire_dependance_personneMorale']                          = false;

// Cadre 1 - Objet de la formalité

var objet= $ac2.cadre1ObjetModificationGroup.cadre1ObjetModification;
var identite= $ac2.cadre1RappelIdentificationGroup.cadre1RappelIdentification;
var modifIdentiteDeclarant = $ac2.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle;

formFields['siren']                                                  = identite.siren.split(' ').join('');
formFields['immatRSACGreffe']                                        = identite.immatRSACGreffe;

// Cadre 2

if (modifIdentiteDeclarant.modifIdentite.modifDateIdentite !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifIdentite.modifDateIdentite.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateIdentite']          = date;
}

formFields['personneLiee_personnePhysique_nomNaissance']             = (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P') and Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifNomNaissance')) ? modifIdentiteDeclarant.modifIdentite.modifNouveauNomNaissance : identite.personneLieePersonnePhysiqueNomNaissance;
formFields['personneLiee_personnePhysique_nomUsage']                 = Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P') ? (modifIdentiteDeclarant.modifIdentite.modifNouveauNomUsage != null ? modifIdentiteDeclarant.modifIdentite.modifNouveauNomUsage : '') : (identite.personneLieePersonnePhysiqueNomUsage != null ? identite.personneLieePersonnePhysiqueNomUsage : '');

if (Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom')) {	
var prenoms=[];
for ( i = 0; i < modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms.size() ; i++ ){prenoms.push(modifIdentiteDeclarant.modifIdentite.modifNouveauPrenoms[i]);}                            
formFields['personneLiee_personnePhysique_prenom1']                                      = prenoms.toString();
} else if (not Value('id').of(modifIdentiteDeclarant.modifIdentite.modifNomPrenom).contains('modifPrenom')) {
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFields['personneLiee_personnePhysique_prenom1']                                      = prenoms.toString();
}

if (identite.personneLieePersonnePhysiqueDateNaissance !== null) {
    var dateTmp = new Date(parseInt(identite.personneLieePersonnePhysiqueDateNaissance.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['personneLiee_personnePhysique_dateNaissance']          = date;
}
formFields['personneLiee_personnePhysique_lieuNaissanceDepartement'] = identite.personneLieePPLieuNaissanceDepartement != null ? identite.personneLieePPLieuNaissanceDepartement.getId() : '';
formFields['personneLiee_personnePhysique_lieuNaissanceCommune']     = identite.personneLieePPLieuNaissanceCommune != null ?  identite.personneLieePPLieuNaissanceCommune : identite.personneLieePPLieuNaissanceVille;
formFields['personneLiee_personnePhysique_lieuNaissancePays']        = identite.personneLieePPLieuNaissancePays;

// Cadres 2B

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('10P') 
	or Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('15P')) {
formFields['modifAncienNomNaissance']                                    = identite.personneLieePersonnePhysiqueNomNaissance;
formFields['modifAncienNomUsage']                                        = identite.personneLieePersonnePhysiqueNomUsage != null ? identite.personneLieePersonnePhysiqueNomUsage : '';
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFields['modifAncienPrenoms']                                      = prenoms.toString();
}

// Cadre 3

// Modification du domicile

var etablissement = $ac2.cadre6EtablissementGroup.cadre6Etablissement;
var newAdresseDom = $ac2.cadre2ModificationSituationPersonnelleGroup.cadre2ModificationSituationPersonnelle.modifDomicile.newAdresseDomicile;

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('16P')) {
if (modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateDomicile']          = date;
}
formFields['personneLiee_adresse_voie']                             = (newAdresseDom.personneLieeAdresseVoieNew != null ? newAdresseDom.personneLieeAdresseVoieNew : '') 
																	+ ' ' + (newAdresseDom.personneLieeAdresseIndiceVoieNew != null ? newAdresseDom.personneLieeAdresseIndiceVoieNew : '')
																	+ ' ' + (newAdresseDom.personneLieeAdresseTypeVoieNew != null ? newAdresseDom.personneLieeAdresseTypeVoieNew : '')
																	+ ' ' + (newAdresseDom.personneLieeAdresseNomVoieNew != null ? newAdresseDom.personneLieeAdresseNomVoieNew : '')
																	+ ' ' + (newAdresseDom.personneLieeAdresseComplementAdresseNew != null ? newAdresseDom.personneLieeAdresseComplementAdresseNew : '')
																	+ ' ' + (newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null ? newAdresseDom.personneLieeAdresseDistriutionSpecialeNew : '');
formFields['personneLiee_adresse_codePostal']                       = newAdresseDom.personneLieeAdresseCodePostalNew;
formFields['personneLiee_adresse_commune']                          = newAdresseDom.personneLieeAdresseCommuneNew;
formFields['personneLiee_adresse_commune_AncienneCommune']          = newAdresseDom.personneLieeAdresseCommuneNewAncienne != null ? newAdresseDom.personneLieeAdresseCommuneNewAncienne : '';
} else if (objet.entrepriseDomicile) {
var newAdresseDom2 = $ac2.cadre6EtablissementGroup.cadre6Etablissement.adresseEtablissementNew;
if (etablissement.modifDateAdresseEntreprise !== null) {
    var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateDomicile']          = date;
}

formFields['personneLiee_adresse_voie']                             = (newAdresseDom2.etablissementAdresseNumeroVoieNew != null ? newAdresseDom2.etablissementAdresseNumeroVoieNew : '') 
																	+ ' ' + (newAdresseDom2.etablissementAdresseIndiceVoieNew != null ? newAdresseDom2.etablissementAdresseIndiceVoieNew : '')
																	+ ' ' + (newAdresseDom2.etablissementAdresseTypeVoieNew != null ? newAdresseDom2.etablissementAdresseTypeVoieNew : '')
																	+ ' ' + (newAdresseDom2.etablissementAdresseNomVoieNew != null ? newAdresseDom2.etablissementAdresseNomVoieNew : '')
																	+ ' ' + (newAdresseDom2.etablissementAdresseComplementVoieNew != null ? newAdresseDom2.etablissementAdresseComplementVoieNew : '')
																	+ ' ' + (newAdresseDom2.etablissementAdresseDistriutionSpecialeVoieNew != null ? newAdresseDom2.etablissementAdresseDistriutionSpecialeVoieNew : '');
formFields['personneLiee_adresse_codePostal']                       = newAdresseDom2.etablissementAdresseCodePostalNew;
formFields['personneLiee_adresse_commune']                          = newAdresseDom2.etablissementAdresseCommuneNew;
}

// Nouvelle nationalité

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('17P')) {
if (modifIdentiteDeclarant.modifNationalite.modifDateNationalite !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifNationalite.modifDateNationalite.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateNationalite']          = date;
}
formFields['modifNouvelleNationalite']                                   = modifIdentiteDeclarant.modifNationalite.modifNewNationalite;
}

// Cadre 4A - Modification du conjoint

var conjoint = $ac2.cadre3ModificationConjointGroup.cadre3ModificationConjoint;

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('30P')) {
if (conjoint.modifDateConjoint !== null) {
    var dateTmp = new Date(parseInt(conjoint.modifDateConjoint.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateConjoint']          = date;
}
formFields['suppression_mentionConjoint']                                            = Value('id').of(conjoint.objetModifStatut).eq('suppressionStatut') ? true : false;
formFields['modification_mentionConjoint']                                           = Value('id').of(conjoint.objetModifStatut).eq('modificationStatut') ? true : false;
formFields['nouvelle_mentionConjoint']                                               = Value('id').of(conjoint.objetModifStatut).eq('nouveauStatut') ? true : false;

// Cadre 4B  

if (not Value('id').of(conjoint.objetModifStatut).eq('suppressionStatut')) {
formFields['conjointSalarie']                                                        = Value('id').of(conjoint.objetModifConjoint).eq('conjointSalarie') ? true : false;
formFields['conjointCollaborateur']                                                  = Value('id').of(conjoint.objetModifConjoint).eq('conjointCollaborateur') ? true : false;

// Cadre 4C

formFields['modifNomNaissanceConjoint']                                              = conjoint.modifNomNaissanceConjoint;
formFields['modifNomUsageConjoint']                                                  = conjoint.modifNomUsageConjoint != null ? conjoint.modifNomUsageConjoint : '';
var prenoms=[];
for ( i = 0; i < conjoint.modifPrenomConjoint.size() ; i++ ){prenoms.push(conjoint.modifPrenomConjoint[i]);}                            
formFields['modifPrenomConjoint']                                      = prenoms.toString();
if ($ac2.cadre3ModificationConjointGroup.cadre3ModificationConjoint.modifDateNaissanceConjoint !== null) {
    var dateTmp = new Date(parseInt($ac2.cadre3ModificationConjointGroup.cadre3ModificationConjoint.modifDateNaissanceConjoint.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateNaissanceConjoint']          = date;
}
formFields['modifLieuNaissanceDepartementConjoint']                                  = conjoint.modifLieuNaissanceDepartementConjoint != null ? conjoint.modifLieuNaissanceDepartementConjoint.getId() : '';
formFields['modifLieuNaissanceCommuneConjoint']                                      = conjoint.modifLieuNaissanceCommuneConjoint != null ? conjoint.modifLieuNaissanceCommuneConjoint : conjoint.modifLieuNaissanceVilleConjoint;
formFields['modifLieuNaissancePaysConjoint']                                         = conjoint.modifLieuNaissancePaysConjoint;
}
}

// Cadres 5 - Modification EIRL

if ($ac2.cadre4DeclarationAffectationPatrimoineGroup.cadreDeclarationAffectationPatrimoine.modifDateDeclarationEIRL !== null) {
    var dateTmp = new Date(parseInt($ac2.cadre4DeclarationAffectationPatrimoineGroup.cadreDeclarationAffectationPatrimoine.modifDateDeclarationEIRL.getTimeInMillis()));
    var date1 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date1 = date1.concat(pad(month.toString()));
	date1 = date1.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date1;
} else if ($ac2.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.modifDateEIRL !== null) {
    var dateTmp = new Date(parseInt($ac2.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.modifDateEIRL.getTimeInMillis()));
    var date2 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date2 = date2.concat(pad(month.toString()));
	date2 = date2.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date2;
}  else if ($ac2.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.finEIRLGroup.finEIRLDeclaration.modifDateFinEIRL !== null) {
    var dateTmp = new Date(parseInt($ac2.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.finEIRLGroup.finEIRLDeclaration.modifDateFinEIRL.getTimeInMillis()));
    var date4 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date4 = date4.concat(pad(month.toString()));
	date4 = date4.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date4;
}	else if (objet.entrepriseEIRL and etablissement.modifDateAdresseEntreprise !== null) {
    var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
    var date5 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date5 = date5.concat(pad(month.toString()));
	date5 = date5.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date5;
} else if (objet.entrepriseEIRL and modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
    var date6 = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date6 = date6.concat(pad(month.toString()));
	date6 = date6.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEIRL']          = date6;
} 
formFields['eirl_immatriculation']                                                   = Value('id').of(objet.objetModification).contains('modifEIRL') ? (objet.cadreObjetDeclarationEIRL.declarationEIRL ? true : false) : false;
formFields['eirl_modification']                                                      = ((Value('id').of(objet.objetModification).contains('modifEIRL') and (Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDenoEIRL') or Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDateCloture') or Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifObjet') or Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse'))) or objet.entrepriseEIRL or objet.poursuiteEIRL) ? (objet.cadreObjetDeclarationEIRL.declarationEIRL ? false : true) : false;
formFields['eirl_affectationRetraitPatrimoine']                                      = (Value('id').of(objet.objetModification).contains('modifEIRL') and Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDAP')) ? true : false;

// Cadre 6 - Adresse professionnelle

if (Value('id').of(objet.objetModification).contains('11P') and etablissement.modifDateAdresseEntreprise !== null) {
    var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementTransfere']          = date;
formFields['modifNouvelleAdresseEtablissement_voieAdresse']                    = (etablissement.adresseEtablissementNew.etablissementAdresseNumeroVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseNumeroVoieNew : '') 
																				+ ' ' + (etablissement.adresseEtablissementNew.etablissementAdresseIndiceVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseIndiceVoieNew : '')
																				+ ' ' + (etablissement.adresseEtablissementNew.etablissementAdresseTypeVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseTypeVoieNew : '')
																				+ ' ' + (etablissement.adresseEtablissementNew.etablissementAdresseNomVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseNomVoieNew : '')
																				+ ' ' + (etablissement.adresseEtablissementNew.etablissementAdresseComplementVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseComplementVoieNew : '')
																				+ ' ' + (etablissement.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew : '');
formFields['modifNouvelleAdresseEtablissement_codePostal']  		            = etablissement.adresseEtablissementNew.etablissementAdresseCodePostalNew;
formFields['modifNouvelleAdresseEtablissement_communeAdresse']                = etablissement.adresseEtablissementNew.etablissementAdresseCommuneNew;
} else if (objet.domicileEntreprise and modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateEtablissementTransfere']          = date;
formFields['modifNouvelleAdresseEtablissement_voieAdresse']                    = (newAdresseDom.personneLieeAdresseVoieNew != null ? newAdresseDom.personneLieeAdresseVoieNew : '') 
																		+ ' ' + (newAdresseDom.personneLieeAdresseIndiceVoieNew != null ? newAdresseDom.personneLieeAdresseIndiceVoieNew : '')
																		+ ' ' + (newAdresseDom.personneLieeAdresseTypeVoieNew != null ? newAdresseDom.personneLieeAdresseTypeVoieNew : '')
																		+ ' ' + (newAdresseDom.personneLieeAdresseNomVoieNew != null ? newAdresseDom.personneLieeAdresseNomVoieNew : '')
																		+ ' ' + (newAdresseDom.personneLieeAdresseComplementAdresseNew != null ? newAdresseDom.personneLieeAdresseComplementAdresseNew : '')
																		+ ' ' + (newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null ? newAdresseDom.personneLieeAdresseDistriutionSpecialeNew : '');
formFields['modifNouvelleAdresseEtablissement_codePostal']              = newAdresseDom.personneLieeAdresseCodePostalNew;
formFields['modifNouvelleAdresseEtablissement_communeAdresse']                = newAdresseDom.personneLieeAdresseCommuneNew;
}

// Cadre 7 - Insaisissabilité

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('28P')) {
if (modifIdentiteDeclarant.modifInsaisissabilite.modifdateInsaisissabilite !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifInsaisissabilite.modifdateInsaisissabilite.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateInsaisissabilite']          = date;
}
formFields['insaisissabiliteRenonciationRP']                                         = Value('id').of(modifIdentiteDeclarant.modifInsaisissabilite.modifResidencePrincipale).eq('renonciation') ? true : false;
formFields['insaisissabilitePublicationRenonciationRP']                              = Value('id').of(modifIdentiteDeclarant.modifInsaisissabilite.modifResidencePrincipale).eq('renonciation') ? modifIdentiteDeclarant.modifInsaisissabilite.publicationInsaisssabiliteResidencePrincipale : '';
formFields['insaisissabiliteRevocationRP']                                           = Value('id').of(modifIdentiteDeclarant.modifInsaisissabilite.modifResidencePrincipale).eq('revocation') ? true : false;
formFields['insaisissabilitePublicationRevocationRP']                                = Value('id').of(modifIdentiteDeclarant.modifInsaisissabilite.modifResidencePrincipale).eq('revocation') ? modifIdentiteDeclarant.modifInsaisissabilite.publicationInsaisssabiliteResidencePrincipale : '';
formFields['insaisissabiliteDeclarationAutresBiens']                                 = Value('id').of(modifIdentiteDeclarant.modifInsaisissabilite.modifAutreBien).contains('declaration') ? true : false;
if (Value('id').of(modifIdentiteDeclarant.modifInsaisissabilite.modifAutreBien).contains('declaration')) {
var declarationAutresBiens=[];
for ( i = 0; i < modifIdentiteDeclarant.modifInsaisissabilite.publicationDeclarationInsaisssabiliteAutreBien.size() ; i++ ){declarationAutresBiens.push(modifIdentiteDeclarant.modifInsaisissabilite.publicationDeclarationInsaisssabiliteAutreBien[i]);}                            
formFields['insaisissabilitePublicationDeclarationAutresBiens']                      = declarationAutresBiens.toString();
}
formFields['insaisissabiliteRenonciationDeclarationAutresBiens']                     = Value('id').of(modifIdentiteDeclarant.modifInsaisissabilite.modifAutreBien).contains('renonciationAutre') ? true : false;
if (Value('id').of(modifIdentiteDeclarant.modifInsaisissabilite.modifAutreBien).contains('renonciationAutre')) {
var renonciationAutresBiens=[];
for ( i = 0; i < modifIdentiteDeclarant.modifInsaisissabilite.publicationRenonciationInsaisssabiliteAutreBien.size() ; i++ ){renonciationAutresBiens.push(modifIdentiteDeclarant.modifInsaisissabilite.publicationRenonciationInsaisssabiliteAutreBien[i]);}                            
formFields['insaisissabilitePublicationRenonciationDeclarationAutresBiens']          = renonciationAutresBiens.toString();
}
}

// Cadre 8 - Contrat d'appui

if (Value('id').of(objet.cadreObjetModificationPerso.objetSituationPerso).contains('37P')) {
if (modifIdentiteDeclarant.modifContratAppui.modifDateAppui !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifContratAppui.modifDateAppui.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateContratAppui']          = date;
}
formFields['modifContratAppui']                                   = true;
}

// Cadre 10 - Observations

var correspondance = $ac2.cadre8RensCompGroup.cadre8RensComp;
var signataire = $ac2.cadre9SignatureGroup.cadre9Signature;

if (Value('id').of(objet.objetModification).contains('dateActivite') and (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 !== null or objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 !== null)) {
if (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateObservations']          = date;
} else if (objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 !== null) {
    var dateTmp = new Date(parseInt(objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateObservations']          = date;
}
formFields['observations']                 = "Modification de la date de début d'activité" + ' / ' +  (correspondance.formaliteObservations != null ? correspondance.formaliteObservations : '');
} else if (not Value('id').of(objet.objetModification).contains('dateActivite') and objet.cadreObjetModificationDebutActivite.modifDateDebutActivite1 == null and objet.cadreObjetModificationDebutActivite.modifDateDebutActivite2 == null) {	
formFields['observations']                 = correspondance.formaliteObservations != null ? correspondance.formaliteObservations : '';
}

// Cadre 11 - Adresse de correspondance

var adresseEntreprise = $ac2.cadre1RappelIdentificationGroup.cadre1RappelIdentification.adresseEtablissementPrincipal;

formFields['adresseCorrespondanceCadre_coche']            = (Value('id').of(correspondance.adresseCorrespond).eq('domi') 
															or Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement')) ? true : false;
formFields['adresseCorrespondanceCadre_numero']           = Value('id').of(correspondance.adresseCorrespond).eq('domi') ? "3" : 
															(Value('id').of(correspondance.adresseCorrespond).eq('newEtablissement') ? "6" : '');
formFields['adressesCorrespondanceAutre']                 = (Value('id').of(correspondance.adresseCorrespond).eq('autre') 
															or Value('id').of(correspondance.adresseCorrespond).eq('prof')) ? true : false;

if (Value('id').of(correspondance.adresseCorrespond).eq('autre')) {
formFields['adresseCorrespondance_voie1']                 = correspondance.adresseCorrespondance.nomPrenomDenominationCorrespondance
															+ ' ' + (correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.numeroVoieAdresseCorrespondance : '')
															+ ' ' + (correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.indiceVoieAdresseCorrespondance : '')
															+ ' ' + (correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.typeVoieAdresseCorrespondance : '')
															+ ' ' + (correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance != null ? correspondance.adresseCorrespondance.nomVoieAdresseCorrespondance : '');
formFields['adresseCorrespondance_voie2']                 = (correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance != null ? correspondance.adresseCorrespondance.voieComplementAdresseCorrespondance : '')
															+ ' ' + (correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance != null ? correspondance.adresseCorrespondance.voieDistributionSpecialeAdresseCorrespondance : '');
formFields['adresseCorrespondance_codePostal']            = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCodePostal;
formFields['adresseCorrespondance_commune']               = correspondance.adresseCorrespondance.formaliteCorrespondanceAdresseCommune;
} else if (Value('id').of(correspondance.adresseCorrespond).eq('prof')) {
formFields['adresseCorrespondance_voie1']             = (adresseEntreprise.etablissementAdresseNumeroVoie != null ? adresseEntreprise.etablissementAdresseNumeroVoie : '')
														+ ' ' + (adresseEntreprise.etablissementAdresseIndiceVoie != null ? adresseEntreprise.etablissementAdresseIndiceVoie : '')
														+ ' ' + (adresseEntreprise.etablissementAdresseTypeVoie != null ? adresseEntreprise.etablissementAdresseTypeVoie : '')
														+ ' ' + (adresseEntreprise.etablissementAdresseNomVoie != null ? adresseEntreprise.etablissementAdresseNomVoie : '');
formFields['adresseCorrespondance_voie2']			  = (adresseEntreprise.etablissementAdresseComplementVoie != null ? adresseEntreprise.etablissementAdresseComplementVoie : '')
														+ ' ' + (adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null ? adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie : '');
formFields['adresseCorrespondance_codePostal']        = adresseEntreprise.etablissementAdresseCodePostal;
formFields['adresseCorrespondance_commune']           = adresseEntreprise.etablissementAdresseCommune;
}

formFields['telephone1']                                                             = correspondance.infosSup.formaliteTelephone1 != null ? correspondance.infosSup.formaliteTelephone1 : '';
formFields['telephone2']                                                             = correspondance.infosSup.formaliteTelephone2;
formFields['courriel']                                                               = correspondance.infosSup.formaliteFaxCourriel != null ? correspondance.infosSup.formaliteFaxCourriel : (correspondance.infosSup.telecopie != null ? correspondance.infosSup.telecopie : '');

// Cadre 12 - Diffusion informations

formFields['diffusionInformation']                                                   = Value('id').of(signataire.diffusionInfo).eq('formaliteNonDiffusionInformationOui') ? true : false;
formFields['nonDiffusionInformation']                                                = Value('id').of(signataire.diffusionInfo).eq('formaliteNonDiffusionInformationNon') ? true : false;

// Cadre 13 - Signataire

formFields['signataireDeclarant']                                                    = Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteDeclarant') ? true : false;
if (Value('id').of(signataire.soussigne).eq('FormaliteSignataireQualiteMandataire')) {
formFields['signataireMandataire']                                                   = true;
formFields['nomPrenomDenominationMandataire']					                     = signataire.adresseMandataire.nomPrenomDenominationMandataire;
formFields['signataireAdresse']                                                      = (signataire.adresseMandataire.numeroVoieMandataire != null ? signataire.adresseMandataire.numeroVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.indiceVoieMandataire != null ? signataire.adresseMandataire.indiceVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.typeVoieMandataire != null ? signataire.adresseMandataire.typeVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.nomVoieMandataire != null ? signataire.adresseMandataire.nomVoieMandataire : '')
																					 + ' ' + (signataire.adresseMandataire.complementVoieMandataire != null ? signataire.adresseMandataire.complementVoieMandataire : '') 
																					 + ' ' + (signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire != null ? signataire.adresseMandataire.voieDistributionSpecialeAdresseMandataire : '');
formFields['signataireAdresseCP']													= signataire.adresseMandataire.dataCodePostalMandataire;
formFields['signataireAdresseCommune']                                              = signataire.adresseMandataire.villeAdresseMandataire;
}
if (signataire.formaliteSignatureDate !== null) {
    var dateTmp = new Date(parseInt(signataire.formaliteSignatureDate.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['signatureDate']          = date;
}
formFields['signatureLieu']                                                          = signataire.formaliteSignatureLieu;
formFields['estEIRL_oui']                                                            = Value('id').of(objet.objetModification).contains('modifEIRL') ? true : false;
formFields['estEIRL_non']                                                            = Value('id').of(objet.objetModification).contains('modifEIRL') ? false : true;
formFields['peirlNombre']                                                            = Value('id').of(objet.objetModification).contains('modifEIRL') ? "1" : "0";
formFields['signature']                                                              = '';

                          // Intercalaire PEIRL ME
								
//Cadre 1 - Déclaration ou modification d'affectation de patrimoine

formFields['formulaire_dependance_P0PL']                                                         = false;
formFields['formulaire_dependance_P0PLME']                                                       = false;
formFields['formulaire_dependance_P2PL']                                                         = false;
formFields['formulaire_dependance_P4PL']                                                         = false;
formFields['formulaire_dependance_AC0']                                                          = false;
formFields['formulaire_dependance_AC2']                                                          = true;
formFields['formulaire_dependance_AC4']                                                          = false;

formFields['declaration_initiale']															= objet.cadreObjetDeclarationEIRL.declarationEIRL ? true : false;
formFields['declaration_modification']                                                      = (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') or Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('finEIRL') or objet.entrepriseEIRL or objet.poursuiteEIRL) ? true : false;

// Cadre 2 - Rappel d'identification

formFields['eirl_siren']                                                  = identite.siren.split(' ').join('');
formFields['eirl_nomNaissance']                                           = identite.personneLieePersonnePhysiqueNomNaissance;
formFields['eirl_nomUsage']                                               = identite.personneLieePersonnePhysiqueNomUsage != null ? identite.personneLieePersonnePhysiqueNomUsage : '';
var prenoms=[];
for ( i = 0; i < identite.personneLieePersonnePhysiquePrenom1.size() ; i++ ){prenoms.push(identite.personneLieePersonnePhysiquePrenom1[i]);}                            
formFields['eirl_prenom']                                      = prenoms.toString();

// Cadre 3 - Déclaration d'affectation de patrimoine

var declarationAffectation = $ac2.cadre4DeclarationAffectationPatrimoineGroup.cadreDeclarationAffectationPatrimoine;

if (objet.cadreObjetDeclarationEIRL.declarationEIRL) { 
formFields['eirl_statutEIRL_declarationInitialeSansDepot']                                  = Value('id').of(declarationAffectation.eirlDepot).eq('eirlSansDepot') ? true : false;
formFields['eirl_statutEIRL_declarationInitialeAvecDepot']                                  = Value('id').of(declarationAffectation.eirlDepot).eq('eirlAvecDepot') ? true : false;
formFields['eirl_statutEIRL_reprise']                                                       = Value('id').of(declarationAffectation.eirlStatut).eq('EirlStatutEIRLReprise') ? true : false;
formFields['eirl_denomination']                                                             = declarationAffectation.declarationPatrimoine.eirlDenomination;
if (declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable !== null) {
    var dateTmp = new Date(parseInt(declarationAffectation.declarationPatrimoine.eirlDateClotureExerciceComptable.getTimeInMillis()));
    var dateClotureEc = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateClotureEc = dateClotureEc.concat(pad(month.toString()));
    formFields['eirl_dateClotureExerciceComptable']          = dateClotureEc;
}
formFields['eirl_objet']                                                                    = declarationAffectation.declarationPatrimoine.eirlObjet;
if (Value('id').of(declarationAffectation.eirlStatut).eq('EirlStatutEIRLReprise')) {
formFields['eirl_precedentEIRLDenomination']                                                = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLDenomination;
formFields['eirl_precedentEIRLLieuImmatriculation']                                         = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLLieuImmatriculation;
formFields['eirl_precedentEIRLSIREN']                                                       = declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN != null ? (declarationAffectation.reprisePatrimoine.eirlPrecedentEIRLSIREN.split(' ').join('')) : '';
formFields['eirl_precedentEIRLRegistre_rseirl']                                             = false;
formFields['eirl_precedentEIRLRegistre_rsac']                                               = true;
}
}

// Cadre 4 - Rappel d'identification relatif à l'EIRL

var rappelIdentification = $ac2.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreRappelIdentificationEIRL;

if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('modificationEIRL') or Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('finEIRL') or objet.entrepriseEIRL) { 
formFields['eirl_rappelDenomination']                   = rappelIdentification.eirlRappelDenomination;
formFields['eirl_rappelLieuImmatriculation']            = rappelIdentification.eirlRappelLieuImmatriculation;

if (objet.cadreObjetModificationEIRL.memeAdresseEIRL or objet.entrepriseEIRL) {
formFields['eirl_rappelAdresse']                    = (adresseEntreprise.etablissementAdresseNumeroVoie != null ? adresseEntreprise.etablissementAdresseNumeroVoie : '')
													+ ' ' + (adresseEntreprise.etablissementAdresseIndiceVoie != null ? adresseEntreprise.etablissementAdresseIndiceVoie : '')
													+ ' ' + (adresseEntreprise.etablissementAdresseTypeVoie != null ? adresseEntreprise.etablissementAdresseTypeVoie : '')
													+ ' ' + (adresseEntreprise.etablissementAdresseNomVoie != null ? adresseEntreprise.etablissementAdresseNomVoie : '')
													+ ' ' + (adresseEntreprise.etablissementAdresseComplementVoie != null ? adresseEntreprise.etablissementAdresseComplementVoie : '')
													+ ' ' + (adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie != null ? adresseEntreprise.etablissementAdresseDistriutionSpecialeVoie : '')
													+ ' ' + adresseEntreprise.etablissementAdresseCodePostal
													+ ' ' + adresseEntreprise.etablissementAdresseCommune;
} else if (not objet.cadreObjetModificationEIRL.memeAdresseEIRL) {
formFields['eirl_rappelAdresse']                    = (rappelIdentification.cadreEirlRappelAdresse.numeroAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.numeroAdresseEirl : '')
													+ ' ' + (rappelIdentification.cadreEirlRappelAdresse.indiceVoieAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.indiceVoieAdresseEirl : '')
													+ ' ' + (rappelIdentification.cadreEirlRappelAdresse.typeVoieAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.typeVoieAdresseEirl : '')
													+ ' ' + rappelIdentification.cadreEirlRappelAdresse.nomVoieAdresseEirl
													+ ' ' + (rappelIdentification.cadreEirlRappelAdresse.complementAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.complementAdresseEirl : '')
													+ ' ' + (rappelIdentification.cadreEirlRappelAdresse.distriutionSpecialeAdresseEirl != null ? rappelIdentification.cadreEirlRappelAdresse.distriutionSpecialeAdresseEirl : '')
													+ ' ' + rappelIdentification.cadreEirlRappelAdresse.codePostalAdresseEirl
													+ ' ' + rappelIdentification.cadreEirlRappelAdresse.communeAdresseEirl;
}
formFields['eirl_rappelDepotRSEIRL']                                                           = false;
formFields['eirl_rappelDepotRSAC']                                                             = true;
}

// Cadre 5 et 6- Déclaration de modification

var modifEIRL = $ac2.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL;                                         

if(Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDenoEIRL')) {
if (modifEIRL.modifDateEIRL !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateDenoEIRL']          = dateModif;
}
formFields['new_DenoEIRL']                                                                  = modifEIRL.newDenoEIRL;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifDateCloture')) {
if (modifEIRL.modifDateEIRL !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateClotureExerciceComptableEIRL']          = dateModif;
}                                         
if (modifEIRL.eirlNewDateClotureExerciceComptable !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.eirlNewDateClotureExerciceComptable.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    formFields['eirl_newDateClotureExerciceComptable']          = dateModif;
}
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifObjet')) {
if (modifEIRL.modifDateEIRL !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateObjetEIRL']          = dateModif;
}
formFields['new_objetEIRL']                                                                 = modifEIRL.newObjetEIRL;
}

if (Value('id').of(objet.cadreObjetModificationEIRL.objetModificationEIRL).contains('modifAdresse')) {
if (modifEIRL.modifDateEIRL !== null) {
    var dateTmp = new Date(parseInt(modifEIRL.modifDateEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateAdresseEIRL']          = dateModif;
}	
formFields['new_adresseEIRL']                           = (modifEIRL.newAdresseEIRL.numeroAdresseEirlNew != null ? modifEIRL.newAdresseEIRL.numeroAdresseEirlNew : '')
														+ ' ' + (modifEIRL.newAdresseEIRL.indiceVoieAdresseEirlNew != null ? modifEIRL.newAdresseEIRL.indiceVoieAdresseEirlNew : '')
														+ ' ' + (modifEIRL.newAdresseEIRL.typeVoieAdresseEirlNew != null ? modifEIRL.newAdresseEIRL.typeVoieAdresseEirlNew : '')
														+ ' ' + modifEIRL.newAdresseEIRL.nomVoieAdresseEirlNew;
formFields['new_adresseEIRL2']                          = (modifEIRL.newAdresseEIRL.complementAdresseEirlNew != null ? modifEIRL.newAdresseEIRL.complementAdresseEirlNew : '')
														+ ' ' + (modifEIRL.newAdresseEIRL.distriutionSpecialeAdresseEirlNew != null ? modifEIRL.newAdresseEIRL.distriutionSpecialeAdresseEirlNew : '')
														+ ' ' + modifEIRL.newAdresseEIRL.codePostalAdresseEirlNew
														+ ' ' + modifEIRL.newAdresseEIRL.communeAdresseEirlNew;
} else if (Value('id').of(objet.objetModification).contains('11P') and objet.entrepriseEIRL) {
if (etablissement.modifDateAdresseEntreprise !== null) {
    var dateTmp = new Date(parseInt(etablissement.modifDateAdresseEntreprise.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateAdresseEIRL']          = date;
}
formFields['new_adresseEIRL']                             = (etablissement.adresseEtablissementNew.etablissementAdresseNumeroVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseNumeroVoieNew : '') 
														  + ' ' + (etablissement.adresseEtablissementNew.etablissementAdresseIndiceVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseIndiceVoieNew : '')
														  + ' ' + (etablissement.adresseEtablissementNew.etablissementAdresseTypeVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseTypeVoieNew : '')
														  + ' ' + (etablissement.adresseEtablissementNew.etablissementAdresseNomVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseNomVoieNew : '');
formFields['new_adresseEIRL2']							  = (etablissement.adresseEtablissementNew.etablissementAdresseComplementVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseComplementVoieNew : '')
														+ ' ' + (etablissement.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew != null ? etablissement.adresseEtablissementNew.etablissementAdresseDistriutionSpecialeVoieNew : '')
														+ ' ' + etablissement.adresseEtablissementNew.etablissementAdresseCodePostalNew
														+ ' ' + etablissement.adresseEtablissementNew.etablissementAdresseCommuneNew;
} else if (objet.domicileEntreprise and objet.entrepriseEIRL) {
if (modifIdentiteDeclarant.modifDomicile.modifDateDomicile !== null) {
    var dateTmp = new Date(parseInt(modifIdentiteDeclarant.modifDomicile.modifDateDomicile.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
    formFields['modifDateAdresseEIRL']          = date;
}
formFields['new_adresseEIRL']                             = (newAdresseDom.personneLieeAdresseVoieNew != null ? newAdresseDom.personneLieeAdresseVoieNew : '') 
														  + ' ' + (newAdresseDom.personneLieeAdresseIndiceVoieNew != null ? newAdresseDom.personneLieeAdresseIndiceVoieNew : '')
														  + ' ' + (newAdresseDom.personneLieeAdresseTypeVoieNew != null ? newAdresseDom.personneLieeAdresseTypeVoieNew : '')
														  + ' ' + newAdresseDom.personneLieeAdresseNomVoieNew;
formFields['new_adresseEIRL2']							  = (newAdresseDom.personneLieeAdresseComplementAdresseNew != null ? newAdresseDom.personneLieeAdresseComplementAdresseNew : '')
														  + ' ' + (newAdresseDom.personneLieeAdresseDistriutionSpecialeNew != null ? newAdresseDom.personneLieeAdresseDistriutionSpecialeNew : '')
														  + ' ' + newAdresseDom.personneLieeAdresseCodePostalNew
														  + ' ' + newAdresseDom.personneLieeAdresseCommuneNew;
}

var finDAP = $ac2.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.finEIRLGroup;
if (Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).eq('finEIRL')) { 																							
if (finDAP.finEIRLDeclaration.modifDateFinEIRL !== null) {
    var dateTmp = new Date(parseInt(finDAP.finEIRLDeclaration.modifDateFinEIRL.getTimeInMillis()));
    var dateModif = pad(dateTmp.getDate().toString());
    var month = dateTmp.getMonth() + 1;
    dateModif = dateModif.concat(pad(month.toString()));
    dateModif = dateModif.concat(dateTmp.getFullYear().toString());
    formFields['modifDateFinEIRL']          = dateModif;
}
formFields['finEIRL_renonciationAvecPoursuite']                                             = Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).contains('finEIRL') ? (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLRenonciationAvecPoursuite') ? true : false) : false;
formFields['finEIRL_renonciationSansPoursuite']                                             = Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).contains('finEIRL') ? (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLRenonciationSansPoursuite') ? true : false) : false;
formFields['finEIRL_cessionPP']                                                             = Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).contains('finEIRL') ? (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLCessionPP') ? true : false) : false;
formFields['finEIRL_cessionPM']                                                             = Value('id').of(objet.cadreObjetModificationEIRL.objetEIRL).contains('finEIRL') ? (Value('id').of(finDAP.finEIRLDeclaration.motifFinEIRL).eq('FinEIRLCessionPM') ? true : false) : false;
}	

// Cadre 7 - Options fiscales

var fiscalEIRL = $ac2.cadre4DeclarationAffectationPatrimoineGroup.cadreOptionsFiscalesEIRLnonME;
if (objet.cadreObjetDeclarationEIRL.declarationEIRL and not identite.optionME) {
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rsBNC']                                  = Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesRsBNC') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_dcBNC']                                  = Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesDcBNC') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBeneficesOptionsParticulieres_optionCreanceDette'] = fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionCreanceDette ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBeneficesOptionsParticulieres_optionIS']           = Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesOptionsParticulieresOptionIS') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rsIS']                                   = Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalIS).eq('RegimeFiscalRegimeImpositionBeneficesRsIS') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionBenefices_rnIS']                                   = Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalIS).eq('RegimeFiscalRegimeImpositionBeneficesRnIS') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVA_rfTVA']                                        = Value('id').of(fiscalEIRL.impositionBeneficesEIRL.regimeFiscalRegimeImpositionBenefice).eq('RegimeFiscalRegimeImpositionBeneficesRsBNC') ? true : (Value('id').of(fiscalEIRL.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARfTVA') ? true : false);
formFields['eirl_regimeFiscal_regimeImpositionTVA_rsTVA']                                        = Value('id').of(fiscalEIRL.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARsTVA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVA_rnTVA']                                        = Value('id').of(fiscalEIRL.optionsTVA.regimeTVA).eq('RegimeFiscalRegimeImpositionTVARnTVA') ? true : false;
formFields['eirl_regimeFiscal_regimeImpositionTVAOptionsParticulieres1']                         = fiscalEIRL.optionsTVA.regimeFiscalRegimeImpositionTVAOptionsParticulieres1DC ? true : false;
}

var fiscalEIRLME = $ac2.cadre4DeclarationAffectationPatrimoineGroup.cadreOptionsFiscalesEIRLME;
if (objet.cadreObjetDeclarationEIRL.declarationEIRL and identite.optionME) {
formFields['eirl_regimeFiscal_regimeImpositionBeneficesVersementLiberatoire'] = fiscalEIRLME.regimeFiscalRegimeImpositionBeneficesVersementLiberatoire ? true : false;
}


/*
 * Création du dossier avec volet social : ajout du cerfa avec volet social
 */
 
var cerfaDoc1 = nash.doc //
	.load('models/cerfa_14213-03-AC2_avec_volet_social.pdf') //
	.apply(formFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));


/*
 * Ajout de l'intercalaire PEIRL PL avec option fiscale
 */
if (Value('id').of(objet.objetModification).contains('modifEIRL') or objet.entrepriseEIRL) {
	var peirlMEDoc1 = nash.doc //
		.load('models/cerfa_14218-03_peirl_PL_AC_avec_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(peirlMEDoc1.save('cerfa.pdf'));
}

/*
 * Ajout de l'intercalaire PEIRL ME sans option fiscale
 */
if (Value('id').of(objet.objetModification).contains('modifEIRL') or objet.entrepriseEIRL) {
	var peirlMEDoc2 = nash.doc //
		.load('models/cerfa_14218-03_peirl_PL_AC_sans_volet_social.pdf') //
		.apply (formFields);
	cerfaDoc1.append(peirlMEDoc2.save('cerfa.pdf'));
}


/*
 * Ajout des PJs
 */

// PJ Déclarant

var pj=$ac2.cadre9SignatureGroup.cadre9Signature.soussigne;
var pjUser = [];
var metas = [];

function pushPjPreview(fld) {
	fld.forEach(function (elm) {
        pjUser.push(elm);
        metas.push({'name':'document', 'value': '/'+elm.getAbsolutePath()});
    });
}

if(Value('id').of(pj).eq('FormaliteSignataireQualiteDeclarant')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantSignataire);
}

if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire') and 
	(Value('id').of($ac2.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationPerso.objetSituationPerso).contains('10P') 
	or Value('id').of($ac2.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationPerso.objetSituationPerso).contains('15P') 
	or Value('id').of($ac2.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationPerso.objetSituationPerso).contains('17P'))) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDDeclarantNonSignataire);
} 

// PJ Mandataire

if(Value('id').of(pj).eq('FormaliteSignataireQualiteMandataire')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDMandataireSignataire);
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjPouvoir);
}

// PJ conjoint

if(Value('id').of($ac2.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationPerso.objetSituationPerso).contains('30P') and Value('id').of($ac2.cadre3ModificationConjointGroup.cadre3ModificationConjoint.objetModifConjoint).eq('modifMentionNouveauConjoint')) {    
  	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjIDConjoint);
 }

// PJ Insaisissabilité

if(Value('id').of($ac2.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationPerso.objetSituationPerso).contains('28P')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjInsaisissabiliteActeNotarie);
 }

// PJ Eirl

var pj=$ac2.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetModificationEIRL.objetEIRL;
if(($ac2.cadre1ObjetModificationGroup.cadre1ObjetModification.cadreObjetDeclarationEIRL.declarationEIRL and not Value('id').of($ac2.cadre4DeclarationAffectationPatrimoineGroup.cadreDeclarationAffectationPatrimoine.eirlDepot).eq('eirlSansDepot')) or Value('id').of(pj).contains('modificationEIRL') or $ac2.cadre1ObjetModificationGroup.cadre1ObjetModification.entrepriseEIRL) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAP);
}

var pj=$ac2.cadre4DeclarationAffectationPatrimoineGroup;
if(Value('id').of(pj.cadreEIRLInformationsComplementaires.contenuDAP).contains('bienImmo') or Value('id').of(pj.cadreEIRLInformationsComplementairesBis.contenuDAPBis).contains('bienImmo') or Value('id').of($ac2.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.contenuDAP).contains('bienImmo')) {
    pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAPActeNotarie);
}

if(Value('id').of(pj.cadreEIRLInformationsComplementaires.contenuDAP).contains('bienCommunIndivis') or Value('id').of(pj.cadreEIRLInformationsComplementairesBis.contenuDAPBis).contains('bienCommunIndivis') or Value('id').of($ac2.cadre5ModificationAffectationPatrimoineGroup.cadreModificationAffectationPatrimoine.cadreModificationEIRL.contenuDAP).contains('bienCommunIndivis')) {
	pushPjPreview($attachmentPreprocess.attachmentPreprocess.pjDAPAccordTiers);
}

/*
 * Enregistrement du fichier (en mémoire)
 */

var finalDoc = cerfaDoc1.save('AC2_Modification.pdf');


//Remove old metas before insert
var metasToDelete = [];
var recordMetas = nash.record.meta() != null ? nash.record.meta().metas : null;
var recordMetasSize = recordMetas != null ? recordMetas.size() : 0;

for (var i = 0; i < recordMetasSize; i++) {
    if (recordMetas.get(i).name == 'document' && !recordMetas.get(i).value.contains("proxy")) {
        metasToDelete.push({'name':'document', 'value': recordMetas.get(i).value});
    }
}
nash.record.removeMeta(metasToDelete);

// Insert new metas
nash.record.meta(metas);

var data = [ spec.createData({
    id : 'record',
    label : 'Déclaration de modification d\'un agent commercial (personne physique)',
    description : 'Voici le formulaire obtenu à partir des données saisies.',
    type : 'FileReadOnly',
    value : [ finalDoc ]
}),  spec.createData({
    id : 'attachments',
    label : 'Pièces jointes',
    description : 'Pièces jointes ajoutées au formulaire.',
    type : 'FileReadOnly',
    value : pjUser
}) ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Déclaration de modification d\'un agent commercial (personne physique)',
    groups : groups
});