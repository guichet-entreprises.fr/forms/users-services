if ( Value('id').of($ndi.signataireGroup.signataire.soussignePM).eq('representantLegal') ) {
    attachment('pjIDDeclarantSignataire', 'pjIDDeclarantSignataire', { mandatory:"true"});
} else {
    attachment('pjIDMandataireSignataire', 'pjIDMandataireSignataire', { mandatory:"true"});
}

if ( Value('id').of($ndi.signataireGroup.signataire.soussignePM).eq('mandataire') or Value('id').of($ndi.signataireGroup.signataire.soussignePP).eq('mandataire') ) {
    attachment('pjPouvoir', 'pjPouvoir', { mandatory:"true"});
}
