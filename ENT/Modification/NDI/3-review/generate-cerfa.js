//--------------- FUNCTIONS -------------------//
function pad(s) { return (s < 10) ? '0' + s : s; }

function parseDate(dateField, separator) {
	if (null == dateField) {
		return '';
	}
	
	if (null == separator) {
		separator = '';
	}
	
    var dateTmp = new Date(parseInt(dateField.getTimeInMillis()));
    var date = pad(dateTmp.getDate().toString());
	date = date.concat(separator);
    var month = dateTmp.getMonth() + 1;
    date = date.concat(pad(month.toString()));
	date = date.concat(separator);
	date = date.concat(dateTmp.getFullYear().toString());
	
    return date;
}

function getFieldValue(field) { return null == field ? '' : field; }

function pushPjPreview(fld) { fld.forEach(function (elm) { pjUser.push(elm); metas.push({'name':'document', 'value': '/'+elm.getAbsolutePath()}); }); }

function updateMetas(metas) {	
	//Remove old metas before insert
	var metasToDelete = [];
	var recordMetas = nash.record.meta() != null ? nash.record.meta().metas : null;
	var recordMetasSize = recordMetas != null ? recordMetas.size() : 0;

	for (var i = 0; i < recordMetasSize; i++) {
		if (recordMetas.get(i).name == 'document') {
			metasToDelete.push({'name':'document', 'value': recordMetas.get(i).value});
		}
	}
	nash.record.removeMeta(metasToDelete);

	// Insert new metas
	nash.record.meta(metas);
}
//--------------- FUNCTIONS -------------------//

//--------------- Variables init -------------------//
var pdfFields = {};
pdfFields['complete_immatriculation_ndi']    = false;
pdfFields['complete_modification_ndi']       = true;
var obj;
var pjUser = [];
var metas = [];
//--------------- Variables init -------------------//

(function (obj) {
	if (null == obj) { return; }
	
    pdfFields['siren_ndi']                         = obj.siren.split(' ').join('');
    pdfFields['immatRCSGreffe_ndi']                = obj.immatRCSGreffe;
	
	if ( Value('id').of(obj.type).eq('pp') ) {
		var personnePhysique = obj.personnePhysique;
		
		pdfFields['nomNaissance_ndi'] = personnePhysique.nomNaissance;
		pdfFields['nomUsage_ndi']     = personnePhysique.nomUsage;
		var prenomsData = personnePhysique.prenom;
		var prenoms = [];
		for (var idx = 0; idx < prenomsData.size(); idx++) { prenoms.push( prenomsData.get(idx) ); }
		pdfFields['prenom_ndi']       = prenoms.join(', ');
		
	} else if ( Value('id').of(obj.type).eq('pm') ) {
		var personneMorale = obj.personneMorale;
		pdfFields['denomination_ndi']            = personneMorale.denomination;
		pdfFields['adresseSiege_voie_ndi']       = personneMorale.adresse;
		pdfFields['adresseSiege_codePostal_ndi'] = personneMorale.codePostal;
		pdfFields['adresseSiege_commune_ndi']    = personneMorale.commune;
	}	
})(_record.ndi.cadre1Identite.cadre1RappelIdentification);


(function (obj) {
	if (null == obj) { return; }
	
    pdfFields['dateDebutActivite_ndi']                          = parseDate(obj.dateDebutActivite);
	
	var voieNdi = getFieldValue(obj.numeroVoie) + //
				' ' + getFieldValue(obj.indiceVoie) + //
				' ' + getFieldValue(obj.typeVoie) + //
				' ' + getFieldValue(obj.complementVoie) + //
				' ' + getFieldValue(obj.distributionSpecialeVoie) //
				;
    pdfFields['adresseEtablissement_voie_ndi']                  = voieNdi;
    pdfFields['adresseEtablissement_codePostal_ndi']            = obj.codePostal;
    pdfFields['adresseEtablissement_commune_ndi']               = obj.commune;
    pdfFields['nomDomaineEtablissement_ndi']                    = obj.nomDomaine;

    pdfFields['declarationInitialeNomDomaineEtablissement_ndi'] = Value('id').of(obj.type).eq('initiale');
    pdfFields['remplacementNomDomaineEtablissement_ndi']        = Value('id').of(obj.type).eq('remplacement');
    pdfFields['suppressionNomDomaineEtablissement_ndi']         = Value('id').of(obj.type).eq('suppression');
})(_record.ndi.domaines.etablissement);


(function (obj) {
	if (null == obj) { return; }
	
    pdfFields['dateEffetNomDomainePM_ndi']                  = parseDate(obj.dateEffet);
    for (var i = 0; i < obj.domaines.length; i++) {
        pdfFields['nomDomainePM_ndi[' + i + ']']            = obj.domaines[i].nom;
        pdfFields['nouveauNomDomainePM_ndi[' + i + ']']     = Value('id').of(obj.domaines[i].type).eq('nouveau');
        pdfFields['suppressionNomDomainePM_ndi[' + i + ']'] = Value('id').of(obj.domaines[i].type).eq('supprime');
    }
})(_record.ndi.domaines.personneMorale);

(function (obj) {
	if (null == obj) { return; }
	
    pdfFields['observation_ndi']                                        = getFieldValue(obj.observations);
	if (null != obj.adresseCorrespondance && null != obj.adresseCorrespondance.nomPrenomDenomination) {
		var adresseCorrespondance = obj.adresseCorrespondance;

		var voie1AdresseCorrespondance = getFieldValue(adresseCorrespondance.numeroVoie) + //
															' ' + getFieldValue(adresseCorrespondance.indiceVoie) + //
															' ' + getFieldValue(adresseCorrespondance.typeVoie)
															;

		var voie2AdresseCorrespondance = getFieldValue(adresseCorrespondance.complementVoie) + ' ' + getFieldValue(adresseCorrespondance.distributionSpecialeVoie);
															;
		pdfFields['formalite_correspondance_ndi']                   = true;
		pdfFields['formalite_correspondanceAdresse_voie1_ndi']      = voie1AdresseCorrespondance;
		pdfFields['formalite_correspondanceAdresse_voie2_ndi']      = voie2AdresseCorrespondance;
		pdfFields['formalite_correspondanceAdresse_codePostal_ndi'] = adresseCorrespondance.codePostal;
		pdfFields['formalite_correspondanceAdresse_commune_ndi']    = adresseCorrespondance.commune;
		pdfFields['formalite_telephone1_ndi']                       = getFieldValue(obj.infosSup.telephone);
		pdfFields['formalite_telephone2_ndi']                       = getFieldValue(obj.infosSup.telecopie);
		pdfFields['formalite_fax_courriel_ndi']                     = getFieldValue(obj.infosSup.courriel);
	} else {	
	
		pdfFields['adresseCorrespondanceDeclaree_ndi']              = true;
		if ( null != obj.declareCadrePP ) {
			pdfFields['adresseDeclareeCadre_ndi']                       = "3";
		} else if ( null != obj.declareCadrePMTous ) { 
			pdfFields['adresseDeclareeCadre_ndi']                       = Value('id').of( (obj.declareCadrePMTous ) )._eval();
		} else if ( null != obj.declareCadrePMSiege ) { 
			pdfFields['adresseDeclareeCadre_ndi']                       = Value('id').of( (obj.declareCadrePMSiege ) )._eval();
		}
	}
})(_record.ndi.renseignementsComplementaires.renseignementsComp);

(function (obj) {
	if (null == obj) { return; }
	
	pdfFields['formalite_signatureLieu_ndi'] = obj.lieu;
    pdfFields['formalite_signatureDate_ndi'] = obj.date;
    pdfFields['formalite_signataireQualite_declarant_ndi']      = Value('id').of(_record.ndi.cadre1Identite.cadre1RappelIdentification.type).eq('pp') and Value('id').of(obj.soussignePP).eq('declarant') ? true : false;
    pdfFields['formalite_signataireQualite_representant_ndi']   = ( null != obj.soussignePM and Value('id').of(obj.soussignePM).eq('representantLegal') ) ? true : false;
    pdfFields['formalite_signataireQualite_mandataire_ndi']     = Value('id').of(obj.soussignePP).eq('mandataire') or Value('id').of(obj.soussignePM).eq('mandataire');

	if ( Value('id').of(obj.soussignePP).eq('mandataire') or Value('id').of(obj.soussignePM).eq('mandataire') or Value('id').of(obj.soussignePM).eq('representantLegal') ) {
		var adresseMandataire = obj.adresseMandataire;
		pdfFields['nomPrenomDenominationMandataire_ndi']    = getFieldValue(adresseMandataire.nomPrenomDenomination);	
		pdfFields['formalite_signataireNomAdresse_ndi']     = getFieldValue(adresseMandataire.numeroVoie) + //
															' ' + getFieldValue(adresseMandataire.indiceVoie) + //
															' ' + getFieldValue(adresseMandataire.typeVoie) + //
															' ' + getFieldValue(adresseMandataire.nomVoie) + //
															' ' + getFieldValue(adresseMandataire.complementVoie) + //
															' ' + getFieldValue(adresseMandataire.distributionSpecialeVoie)
															;
		pdfFields['formalite_signataireCommuneAdresse_ndi']       = adresseMandataire.commune;
	}
})(_record.ndi.signataireGroup.signataire);

pdfFields['signature_ndi']                       = "Cette déclaration respecte les attendus de l'article A123-4 du code de commerce";

if ( Value('id').of($ndi.signataireGroup.signataire.soussignePM).eq('representantLegal') ) {
    pushPjPreview($preprocess.preprocess.pjIDDeclarantSignataire);
} else {
    pushPjPreview($preprocess.preprocess.pjIDMandataireSignataire);
}

if ( Value('id').of($ndi.signataireGroup.signataire.soussignePP).eq('mandataire') or Value('id').of($ndi.signataireGroup.signataire.soussignePM).eq('mandataire') ) {
	pushPjPreview($preprocess.preprocess.pjPouvoir);
}

//--------------- METADATA DOCUMENT -------------------//
updateMetas(metas);

var data = [ spec.createData({
    id : 'record',
    label : 'Déclaration relative au(x) nom(s) de domaine du ou des site(s) internet',
    description : 'Voici le formulaire obtenu à partir des données saisies.',
    type : 'FileReadOnly',
    value : [ nash.doc.load('models/cerfa_14943-01_NDI_volet.pdf').apply(pdfFields).save('NDI.pdf') ]
}),  spec.createData({
    id : 'attachments',
    label : 'Pièces jointes',
    description : 'Pièces jointes ajoutées au formulaire.',
    type : 'FileReadOnly',
    value : pjUser
}) ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Review',
    groups : groups
});
