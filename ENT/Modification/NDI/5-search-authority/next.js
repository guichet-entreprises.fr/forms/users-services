//--------------- Functions --------------//
function getAuthority(authorityId) {			
	var response = null;
	try {
		response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', authorityId) //
			   .connectionTimeout(10000) //
			   .receiveTimeout(10000) //
			   .accept('json') //
			   .get()
		;
	} catch (e) {	
		log.error(e);			
		throw 'An technical error occured when calling directory with authority uid : ' + authorityId;
	}
	if (response != null && response.getStatus() == 200) {			
		return response.asObject();
	}
	throw 'Cannot find any authority uid : ' + authorityId;
}

function updateAuthorityDetails(authorityId, authorityInfo) {			
	var response = null;
	try {
		response = nash.service.request('${directory.baseUrl}/v1/authority/merge') //
		   .connectionTimeout(10000) //
		   .receiveTimeout(10000) //
		   .dataType('application/json')//
		   .accept('json') //
		   .put(authorityInfo) //
		 ;
	} catch (e) {	
		log.error(e);			
		throw 'An technical error occured when calling directory with authority uid : ' + authorityId;
	}
	if (response != null && response.getStatus() == 200) {			
		return true;
	}
	throw 'Cannot find any authority uid : ' + authorityId;
}

function map(src, mapping) {
    var dst = {};
    Object.keys(mapping).forEach(function(key) {
        dst[key] = src[mapping[key]];
    });
    return dst;
}
//--------------- Functions --------------//

//--------------- Variables init --------------//
var destFuncId = _input.result.funcId;
var destFuncLabel = _input.result.funcLabel;
var transferChannels = {
	"backoffice": {
		"state": "disabled"
	},
	"email": {
		"emails": [],
		"state": "disabled"
	},
	"address": {
		"addressDetail": {
			"recipientName": "",
			"recipientNameCompl": "",
			"addressName": "",
			"addressNameCompl": "",
			"cityName": "",
			"postalCode": ""
		},
		"state": "disabled"
	},
	"ftp": {
		"token": "",
		"state": "disabled"
	}
};
//--------------- Variables init --------------//

//--------------- Searching authority and its details --------------//
var authorityInfo = getAuthority(destFuncId);
var authorityDetails = !authorityInfo.details ? null : authorityInfo.details;
if ( null == authorityDetails.transferChannels ) {
	authorityDetails.transferChannels = transferChannels;
}

authorityDetails.transferChannels["address"] = {
	'state' : 'disabled',
	'addressDetail' : {
		'recipientName' : _input.result.address.recipientName,
		'recipientNameCompl' : _input.result.address.recipientNameCompl,
		'addressName' : _input.result.address.recipientAddressName,
		'addressNameCompl' : _input.result.address.recipientAddressNameCompl,
		'postalCode' : _input.result.address.recipientPostalCode,
		'cityName' : _input.result.address.recipientCity,
	}
};
authorityInfo.details = authorityDetails;
//--------------- Searching authority and its details --------------//

//--------------- Update authority details --------------//
updateAuthorityDetails(destFuncId, authorityInfo);
//--------------- Update authority details --------------//

//--------------- Bind output result --------------//
nash.instance //
    .load('output.xml') //
    .bind('result', {
    	'funcId' : destFuncId,
    	'funcLabel': destFuncLabel,
        'address' : map(_input.result.address, {
			'referenceId' : 'referenceId',
			'recipientName' : 'recipientName',
			'recipientNameCompl' : 'recipientNameCompl',
			'recipientAddressName' : 'recipientAddressName',
			'recipientAddressNameCompl' : 'recipientAddressNameCompl',
			'recipientPostalCode' : 'recipientPostalCode',
			'recipientCity' : 'recipientCity',
		})
    });
//--------------- Bind output result --------------//
