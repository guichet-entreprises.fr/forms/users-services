var cerfaFields = {};

//Etat Civil

var civNomPrenom = $qp005PE4.information.etatCivil.nomNaissance + ' ' + $qp005PE4.information.etatCivil.prenom + ',';

cerfaFields['nub']                                      = $qp005PE4.information.etatCivil.nub;
cerfaFields['nomUsage']                                 = $qp005PE4.information.etatCivil.nomUsage;
cerfaFields['nomNaissance']                             = $qp005PE4.information.etatCivil.nomNaissance;
cerfaFields['prenomNaissance']                          = $qp005PE4.information.etatCivil.prenom;
cerfaFields['dateNaissance']                            = $qp005PE4.information.etatCivil.dateNaissance;
cerfaFields['villeNaissance']                           = $qp005PE4.information.etatCivil.villeNaissance;
cerfaFields['paysNaissance']                            = $qp005PE4.information.etatCivil.paysNaissance;

cerfaFields['numeroLibelleAdresseDeclarant']            = $qp005PE4.coordonneesGroup.coordonnees.numeroLibelleAdresse;

cerfaFields['complementAdresseDeclarant']               = $qp005PE4.coordonneesGroup.coordonnees.complementAdresse;
cerfaFields['codePostalAdresseDeclarant']               = $qp005PE4.coordonneesGroup.coordonnees.codePostalAdresse;
cerfaFields['villeAdresseDeclarant']                    = $qp005PE4.coordonneesGroup.coordonnees.villeAdresse;
cerfaFields['telephoneMobileAdresseDeclarant']          = $qp005PE4.coordonneesGroup.coordonnees.telephoneAdresse;
cerfaFields['mailAdresseDeclarant']                     = $qp005PE4.coordonneesGroup.coordonnees.courrielAdresse;
cerfaFields['distributionCourrier']                     = $qp005PE4.coordonneesGroup.coordonnees.distributionCourrierAdresse;

cerfaFields['hebergeur']     							= ($qp005PE4.coordonneesGroup.heberger.civilite != null ? $qp005PE4.coordonneesGroup.heberger.civilite + ' ' : '') 
														+  ($qp005PE4.coordonneesGroup.heberger.nomHebergeur != null? $qp005PE4.coordonneesGroup.heberger.nomHebergeur + ' ' : '')
														+ ($qp005PE4.coordonneesGroup.heberger.prenomHebergeur != null ? $qp005PE4.coordonneesGroup.heberger.prenomHebergeur : '');

cerfaFields['lieuSignature']                            = $qp005PE4.finSaisie.certifie.lieuSignature;
cerfaFields['dateSignature']                            = $qp005PE4.finSaisie.certifie.dateSignature;
cerfaFields['attesteHonneur']                           = $qp005PE4.finSaisie.certifie.attesteHonneur;
cerfaFields['agentRecherchesPrivees']                   = true;

/* var cerfa = pdf.create('models/demande_CP.pdf', cerfaFields);

var cerfaPdf = pdf.save(' Agent de protection physique des personnes - RQP LE.pdf', cerfa); */

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */

   var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp005PE4.finSaisie.certifie.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
  
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var accompDoc = nash.doc //
	.load('models/Courrier au premier dossier v1.6 LE.pdf') //
	.apply({
		date: $qp005PE4.finSaisie.certifie.dateSignature,
	});

finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/formulaire renouvellement CP.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('agent_protection_physique_personnes_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : ' Agent de protection physique des personnes - renouvellement de la demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du formulaire',
        data : [ spec.createData({
            id : 'lpsAutor',
            label : 'Renouvellement de la demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement pour la profession d\'agent de protection physique des personnes.',
            description : 'Voici le formulaire obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
