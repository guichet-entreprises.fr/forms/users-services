var cerfaFields = {};

//Etat Civil

var civNomPrenom = $qp005PE3.information.etatCivil.civilite + ' ' + $qp005PE3.information.etatCivil.nomNaissance + ' ' + $qp005PE3.information.etatCivil.prenom + ',';

cerfaFields['madameEtatCivil']       	               		= ($qp005PE3.information.etatCivil.civilite =='Madame');
cerfaFields['monsieurEtatCivil']        	           	 	= ($qp005PE3.information.etatCivil.civilite =='Monsieur');
cerfaFields['nomUsageEtatCivil']            	        	= $qp005PE3.information.etatCivil.nomUsage;
cerfaFields['nomEtatCivil1']                    	    	= $qp005PE3.information.etatCivil.nomNaissance;
cerfaFields['prenomEtatCivil1']                     		= $qp005PE3.information.etatCivil.prenom;
cerfaFields['dateNaissanceEtatCivil']    	           		= $qp005PE3.information.etatCivil.dateNaissance;
cerfaFields['lieuNaissanceEtatCivil']       	        	= $qp005PE3.information.etatCivil.villeNaissance;
cerfaFields['paysNaissanceEtatCivil']           	   		= $qp005PE3.information.etatCivil.paysNaissance;
 
cerfaFields['numeroLibelleAdresseDeclarant']	        	= $qp005PE3.coordonneesGroup.coordonnees.numeroLibelleAdresse;
cerfaFields['adresseDeclarant']           					= ($qp005PE3.coordonneesGroup.coordonnees.complementAdresse != null ? $qp005PE3.coordonneesGroup.coordonnees.complementAdresse + ' ' : '')
															+ ($qp005PE3.coordonneesGroup.coordonnees.codePostalAdresse != null ? $qp005PE3.coordonneesGroup.coordonnees.codePostalAdresse + ' ' : '')
															+ $qp005PE3.coordonneesGroup.coordonnees.villeAdresse;
cerfaFields['numeroLibelleComplementAdresseDeclarant']		= $qp005PE3.coordonneesGroup.coordonnees.numeroLibelleAdresse + ' '+ ($qp005PE3.coordonneesGroup.coordonnees.complementAdresse != null ? $qp005PE3.coordonneesGroup.coordonnees.complementAdresse + ' ' : '');
cerfaFields['codePostalAdresseDeclarant']     			    = $qp005PE3.coordonneesGroup.coordonnees.codePostalAdresse;
cerfaFields['villeAdresseDeclarant']           				= $qp005PE3.coordonneesGroup.coordonnees.villeAdresse;

cerfaFields['telephoneMobileAdresseDeclarant']      		= $qp005PE3.coordonneesGroup.coordonnees.telephoneAdresse;
cerfaFields['mailAdresseDeclarant']                 		= $qp005PE3.coordonneesGroup.coordonnees.courrielAdresse;
cerfaFields['distributionCourrier']                 		= $qp005PE3.coordonneesGroup.coordonnees.distributionCourrierAdresse;

cerfaFields['lieuSignature']                        		= $qp005PE3.finSaisie.certifie.lieuSignature;
cerfaFields['dateSignature']                        		= $qp005PE3.finSaisie.certifie.dateSignature;
cerfaFields['hebergeur']									= ($qp005PE3.coordonneesGroup.coordonnees.distributionCourrierAdresse ? $qp005PE3.coordonneesGroup.heberger.civilite + ' ' 
															+ $qp005PE3.coordonneesGroup.heberger.nomHebergeur + ' ' + $qp005PE3.coordonneesGroup.heberger.prenomHebergeur : '');
cerfaFields['agentRecherchesPrivees']               		= true;
cerfaFields['attesteHonneur']                       		= $qp005PE3.finSaisie.certifie.attesteHonneur;
cerfaFields['nomPrenomEtatCivil1']							= $qp005PE3.information.etatCivil.prenom + ' ' + ($qp005PE3.information.etatCivil.nomUsage != null ? $qp005PE3.information.etatCivil.nomUsage : $qp005PE3.information.etatCivil.nomNaissance);
cerfaFields['monsieurNon']									= ($qp005PE3.information.etatCivil.civilite =='Madame' ? "-----------" : '');
cerfaFields['madameNon']									= ($qp005PE3.information.etatCivil.civilite =='Monsieur' ? "-----------" : '');

/* var cerfa = pdf.create('models/demande_CP.pdf', cerfaFields);

var cerfaPdf = pdf.save(' Agent de protection physique des personnes - RQP LE.pdf', cerfa); */

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */

   var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp005PE3.finSaisie.certifie.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
  
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var accompDoc = nash.doc //
	.load('models/Courrier au premier dossier v1.6 LE.pdf') //
	.apply({
		date: $qp005PE3.finSaisie.certifie.dateSignature,
		autoriteHabilitee : "CNAPS"
	});

finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/demande_CP.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveQualifications);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('agent_protection_physique_personnes_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : ' Agent de protection physique des personnes - demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du formulaire',
        data : [ spec.createData({
            id : 'lpsAutor',
            label : 'Demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement pour la profession d\'agent de protection physique des personnes.',
            description : 'Voici le formulaire obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
