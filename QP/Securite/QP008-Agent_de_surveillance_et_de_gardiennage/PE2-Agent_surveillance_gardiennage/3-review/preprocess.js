var cerfaFields = {};

//Etat Civil

var civNomPrenom = $qp008PE2.information.etatCivil.civilite + ' ' + $qp008PE2.information.etatCivil.nomNaissance + ' ' + $qp008PE2.information.etatCivil.prenom + ',';

cerfaFields['madameEtatCivil']       	               		= ($qp008PE2.information.etatCivil.civilite =='Madame');
cerfaFields['monsieurEtatCivil']        	           	 	= ($qp008PE2.information.etatCivil.civilite =='Monsieur');
cerfaFields['nomUsageEtatCivil']            	        	= $qp008PE2.information.etatCivil.nomUsage;
cerfaFields['nomEtatCivil1']                    	    	= $qp008PE2.information.etatCivil.nomNaissance;
cerfaFields['prenomEtatCivil1']                     		= $qp008PE2.information.etatCivil.prenom;
cerfaFields['dateNaissanceEtatCivil']    	           		= $qp008PE2.information.etatCivil.dateNaissance;
cerfaFields['lieuNaissanceEtatCivil']       	        	= $qp008PE2.information.etatCivil.villeNaissance;
cerfaFields['paysNaissanceEtatCivil']           	   		= $qp008PE2.information.etatCivil.paysNaissance;
 
cerfaFields['numeroLibelleAdresseDeclarant']	        	= $qp008PE2.coordonneesGroup.coordonnees.numeroLibelleAdresse;
cerfaFields['adresseDeclarant']           					= ($qp008PE2.coordonneesGroup.coordonnees.complementAdresse != null ? $qp008PE2.coordonneesGroup.coordonnees.complementAdresse + ' ' : '')
															+ ($qp008PE2.coordonneesGroup.coordonnees.codePostalAdresse != null ? $qp008PE2.coordonneesGroup.coordonnees.codePostalAdresse + ' ' : '')
															+ $qp008PE2.coordonneesGroup.coordonnees.villeAdresse;
cerfaFields['numeroLibelleComplementAdresseDeclarant']		= $qp008PE2.coordonneesGroup.coordonnees.numeroLibelleAdresse + ' '+ ($qp008PE2.coordonneesGroup.coordonnees.complementAdresse != null ? $qp008PE2.coordonneesGroup.coordonnees.complementAdresse + ' ' : '');
cerfaFields['codePostalAdresseDeclarant']     			    = $qp008PE2.coordonneesGroup.coordonnees.codePostalAdresse;
cerfaFields['villeAdresseDeclarant']           				= $qp008PE2.coordonneesGroup.coordonnees.villeAdresse;

cerfaFields['telephoneMobileAdresseDeclarant']      		= $qp008PE2.coordonneesGroup.coordonnees.telephoneAdresse;
cerfaFields['mailAdresseDeclarant']                 		= $qp008PE2.coordonneesGroup.coordonnees.courrielAdresse;
cerfaFields['distributionCourrier']                 		= $qp008PE2.coordonneesGroup.coordonnees.distributionCourrierAdresse;

cerfaFields['lieuSignature']                        		= $qp008PE2.finSaisie.certifie.lieuSignature;
cerfaFields['dateSignature']                        		= $qp008PE2.finSaisie.certifie.dateSignature;
cerfaFields['hebergeur']									= ($qp008PE2.coordonneesGroup.coordonnees.distributionCourrierAdresse ? $qp008PE2.coordonneesGroup.heberger.civilite + ' ' 
															+ $qp008PE2.coordonneesGroup.heberger.nomHebergeur + ' ' + $qp008PE2.coordonneesGroup.heberger.prenomHebergeur : '');
cerfaFields['attesteHonneur']                       		= $qp008PE2.finSaisie.certifie.attesteHonneur;
cerfaFields['nomPrenomEtatCivil1']							= $qp008PE2.information.etatCivil.prenom + ' ' + ($qp008PE2.information.etatCivil.nomUsage != null ? $qp008PE2.information.etatCivil.nomUsage : $qp008PE2.information.etatCivil.nomNaissance);
cerfaFields['monsieurNon']									= ($qp008PE2.information.etatCivil.civilite =='Madame' ? "-----------" : '');
cerfaFields['madameNon']									= ($qp008PE2.information.etatCivil.civilite =='Monsieur' ? "-----------" : '');
cerfaFields['agentRecherchesPrivees']                       = true;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */

   var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp008PE2.finSaisie.certifie.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
  
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var accompDoc = nash.doc //
	.load('models/Courrier au premier dossier v1.6 LE.pdf') //
	.apply({
		date: $qp008PE2.finSaisie.certifie.dateSignature,
		autoriteHabilitee : "CNAPS"
	});

finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/demande_CP.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveQualifications);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Agent_surveillance_gardiennage_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Agent de surveillance et de gardiennage - Demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du formulaire',
        data : [ spec.createData({
            id : 'lpsAutor',
            label : 'Demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement pour la profession d\'agent de surveillance et de gardiennage.',
            description : 'Voici le formulaire obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
