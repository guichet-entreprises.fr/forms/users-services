var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp007PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp007PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp007PE4.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']  	= $qp007PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp007PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp007PE4.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance'] 	= $qp007PE4.etatCivil.identificationDeclarant.lieuNaissanceDeclarant +', ' + $qp007PE4.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']        	= $qp007PE4.etatCivil.identificationDeclarant.nationaliteDeclarant;

cerfaFields['dateNaissance']      	= $qp007PE4.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse']   			= $qp007PE4.adressePersonnelle.adressePersonnelle1.numeroLibelleAdresseDeclarant + ($qp007PE4.adressePersonnelle.adressePersonnelle1.complementAdresseDeclarant != null?', ' + $qp007PE4.adressePersonnelle.adressePersonnelle1.complementAdresseDeclarant : '');
cerfaFields['telephoneMobile']      = $qp007PE4.adressePersonnelle.adressePersonnelle1.telephoneMobileAdresseDeclarant; 
cerfaFields['villePays']          	= ($qp007PE4.adressePersonnelle.adressePersonnelle1.codePostalAdresseDeclarant != null ? $qp007PE4.adressePersonnelle.adressePersonnelle1.codePostalAdresseDeclarant +' ' : ' ') + $qp007PE4.adressePersonnelle.adressePersonnelle1.villeAdresseDeclarant + ', ' + $qp007PE4.adressePersonnelle.adressePersonnelle1.paysAdresseDeclarant;
cerfaFields['telephoneFixe']        = $qp007PE4.adressePersonnelle.adressePersonnelle1.telephoneAdresseDeclarant;
cerfaFields['telephoneMobile']      = $qp007PE4.adressePersonnelle.adressePersonnelle1.telephoneMobileAdresseDeclarant;
cerfaFields['courriel']           	= $qp007PE4.adressePersonnelle.adressePersonnelle1.mailAdresseDeclarant;


//signature
cerfaFields['date']                	= $qp007PE4.signature.signature.dateSignature;
cerfaFields['signature']           	= $qp007PE4.signature.signature.signature;
cerfaFields['lieuSignature']        = $qp007PE4.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']  = $qp007PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp007PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp007PE4.etatCivil.identificationDeclarant.prenomDeclarant;


/* var cerfa = pdf.create('models/demande_CP.pdf', cerfaFields);

var cerfaPdf = pdf.save(' Agent de protection physique des personnes - RQP LE.pdf', cerfa); */

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */

   var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp007PE4.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
  
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var accompDoc = nash.doc //
	.load('models/Courrier au premier dossier v1.6 LE.pdf') //
	.apply({
		date: $qp007PE4.signature.signature.dateSignature,
	});

finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier_libre_LE_V3.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveQualifications);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);
appendPj($attachmentPreprocess.attachmentPreprocess.declarationConnaissanceReglementationFrancais);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('agent_service_incendie_assistance_personne_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Agent de service de sécurité incendie et d\'assistance à la personne - demande de reconnaissance de qualifications professionnelles en vue d’un libre établissement.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du formulaire',
        data : [ spec.createData({
            id : 'lpsAutor',
            label : 'Demande de reconnaissance de qualifications professionnelles en vue d’un libre établissement pour la profession d\'agent de service de sécurité incendie et d\'assistance à la personne.',
            description : 'Voici le formulaire obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});