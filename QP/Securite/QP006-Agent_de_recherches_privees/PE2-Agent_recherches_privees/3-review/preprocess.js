var cerfaFields = {};

//Etat Civil

var civNomPrenom = $qp006PE2.information.etatCivil.civilite + ' ' + $qp006PE2.information.etatCivil.nomNaissance + ' ' + $qp006PE2.information.etatCivil.prenom + ',';

cerfaFields['madameEtatCivil']       	               		= ($qp006PE2.information.etatCivil.civilite =='Madame');
cerfaFields['monsieurEtatCivil']        	           	 	= ($qp006PE2.information.etatCivil.civilite =='Monsieur');
cerfaFields['nomUsageEtatCivil']            	        	= $qp006PE2.information.etatCivil.nomUsage;
cerfaFields['nomEtatCivil1']                    	    	= $qp006PE2.information.etatCivil.nomNaissance;
cerfaFields['prenomEtatCivil1']                     		= $qp006PE2.information.etatCivil.prenom;
cerfaFields['dateNaissanceEtatCivil']    	           		= $qp006PE2.information.etatCivil.dateNaissance;
cerfaFields['lieuNaissanceEtatCivil']       	        	= $qp006PE2.information.etatCivil.villeNaissance;
cerfaFields['paysNaissanceEtatCivil']           	   		= $qp006PE2.information.etatCivil.paysNaissance;
 
cerfaFields['numeroLibelleAdresseDeclarant']	        	= $qp006PE2.coordonneesGroup.coordonnees.numeroLibelleAdresse;
cerfaFields['adresseDeclarant']           					= ($qp006PE2.coordonneesGroup.coordonnees.complementAdresse != null ? $qp006PE2.coordonneesGroup.coordonnees.complementAdresse + ' ' : '')
															+ ($qp006PE2.coordonneesGroup.coordonnees.codePostalAdresse != null ? $qp006PE2.coordonneesGroup.coordonnees.codePostalAdresse + ' ' : '')
															+ $qp006PE2.coordonneesGroup.coordonnees.villeAdresse;
cerfaFields['numeroLibelleComplementAdresseDeclarant']		= $qp006PE2.coordonneesGroup.coordonnees.numeroLibelleAdresse + ' '+ ($qp006PE2.coordonneesGroup.coordonnees.complementAdresse != null ? $qp006PE2.coordonneesGroup.coordonnees.complementAdresse + ' ' : '');
cerfaFields['codePostalAdresseDeclarant']     			    = $qp006PE2.coordonneesGroup.coordonnees.codePostalAdresse;
cerfaFields['villeAdresseDeclarant']           				= $qp006PE2.coordonneesGroup.coordonnees.villeAdresse;

cerfaFields['telephoneMobileAdresseDeclarant']      		= $qp006PE2.coordonneesGroup.coordonnees.telephoneAdresse;
cerfaFields['mailAdresseDeclarant']                 		= $qp006PE2.coordonneesGroup.coordonnees.courrielAdresse;
cerfaFields['distributionCourrier']                 		= $qp006PE2.coordonneesGroup.coordonnees.distributionCourrierAdresse;

cerfaFields['lieuSignature']                        		= $qp006PE2.finSaisie.certifie.lieuSignature;
cerfaFields['dateSignature']                        		= $qp006PE2.finSaisie.certifie.dateSignature;
cerfaFields['hebergeur']									= ($qp006PE2.coordonneesGroup.coordonnees.distributionCourrierAdresse ? $qp006PE2.coordonneesGroup.heberger.civilite + ' ' 
															+ $qp006PE2.coordonneesGroup.heberger.nomHebergeur + ' ' + $qp006PE2.coordonneesGroup.heberger.prenomHebergeur : '');
cerfaFields['agentRecherchesPrivees']               		= true;
cerfaFields['attesteHonneur']                       		= $qp006PE2.finSaisie.certifie.attesteHonneur;
cerfaFields['nomPrenomEtatCivil1']							= $qp006PE2.information.etatCivil.prenom + ' ' + ($qp006PE2.information.etatCivil.nomUsage != null ? $qp006PE2.information.etatCivil.nomUsage : $qp006PE2.information.etatCivil.nomNaissance);
cerfaFields['monsieurNon']									= ($qp006PE2.information.etatCivil.civilite =='Madame' ? "-----------" : '');
cerfaFields['madameNon']									= ($qp006PE2.information.etatCivil.civilite =='Monsieur' ? "-----------" : '');



/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */

   var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp006PE2.finSaisie.certifie.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
  
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var accompDoc = nash.doc //
	.load('models/Courrier au premier dossier v1.6 LE.pdf') //
	.apply({
		date: $qp006PE2.finSaisie.certifie.dateSignature,
		autoriteHabilitee : "CNAPS"
	});

finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/demande_CP.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveQualifications);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('agent_recherches_privées_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : ' Agent de recherches privées - demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du formulaire',
        data : [ spec.createData({
            id : 'lpsAutor',
            label : 'Demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement pour la profession d\'agent de recherches privées.',
            description : 'Voici le formulaire obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
