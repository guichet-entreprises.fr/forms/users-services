var cerfaFields = {};

//Etat Civil

var civNomPrenom = $qp006PE4.information.etatCivil.nomNaissance + ' ' + $qp006PE4.information.etatCivil.prenom + ',';

cerfaFields['nub']                                                            = $qp006PE4.information.etatCivil.nub;
cerfaFields['nomUsage']                                                                = $qp006PE4.information.etatCivil.nomUsage;

cerfaFields['nomNaissance']                                                            = $qp006PE4.information.etatCivil.nomNaissance;
cerfaFields['prenomNaissance']                                                         = $qp006PE4.information.etatCivil.prenom;
cerfaFields['dateNaissance']                                                           = $qp006PE4.information.etatCivil.dateNaissance;
cerfaFields['villeNaissance']                                                          = $qp006PE4.information.etatCivil.villeNaissance;
cerfaFields['paysNaissance']                                                           = $qp006PE4.information.etatCivil.paysNaissance;

cerfaFields['numeroLibelleAdresseDeclarant']                                           = $qp006PE4.coordonneesGroup.coordonnees.numeroLibelleAdresse;

cerfaFields['complementAdresseDeclarant']                                              = $qp006PE4.coordonneesGroup.coordonnees.complementAdresse;
cerfaFields['codePostalAdresseDeclarant']                                              = $qp006PE4.coordonneesGroup.coordonnees.codePostalAdresse;
cerfaFields['villeAdresseDeclarant']                                                   = $qp006PE4.coordonneesGroup.coordonnees.villeAdresse;
cerfaFields['telephoneMobileAdresseDeclarant']                                         = $qp006PE4.coordonneesGroup.coordonnees.telephoneAdresse;
cerfaFields['mailAdresseDeclarant']                                                    = $qp006PE4.coordonneesGroup.coordonnees.courrielAdresse;
cerfaFields['distributionCourrier']                                                    = $qp006PE4.coordonneesGroup.coordonnees.distributionCourrierAdresse;

cerfaFields['hebergeur']     							= ($qp006PE4.coordonneesGroup.heberger.civilite != null ? $qp006PE4.coordonneesGroup.heberger.civilite + ' ' : '') 
														+  ($qp006PE4.coordonneesGroup.heberger.nomHebergeur != null? $qp006PE4.coordonneesGroup.heberger.nomHebergeur + ' ' : '')
														+ ($qp006PE4.coordonneesGroup.heberger.prenomHebergeur != null ? $qp006PE4.coordonneesGroup.heberger.prenomHebergeur : '');

cerfaFields['lieuSignature']                                                           = $qp006PE4.finSaisie.certifie.lieuSignature;
cerfaFields['dateSignature']                                                           = $qp006PE4.finSaisie.certifie.dateSignature;
cerfaFields['attesteHonneur']                                                          = $qp006PE4.finSaisie.certifie.attesteHonneur;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */

   var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp006PE4.finSaisie.certifie.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
  
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var accompDoc = nash.doc //
	.load('models/Courrier au premier dossier v1.6 LE.pdf') //
	.apply({
		date: $qp006PE4.finSaisie.certifie.dateSignature,
	});

finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/formulaire renouvellement CP.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('agent_recherches_privées_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : ' Agent de recherches privées - renouvellement de la demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du formulaire',
        data : [ spec.createData({
            id : 'lpsAutor',
            label : 'Renouvellement de la demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement pour la profession d\'agent de recherches privées.',
            description : 'Voici le formulaire obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});