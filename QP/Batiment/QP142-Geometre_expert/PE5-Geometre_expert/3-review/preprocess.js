var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp142PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp142PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp142PE5.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['civiliteNomPrenom']            = $qp142PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp142PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp142PE5.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']           = $qp142PE5.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp142PE5.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                  = $qp142PE5.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']                = $qp142PE5.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse']                      = $qp142PE5.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp142PE5.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp142PE5.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']                    = ($qp142PE5.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp142PE5.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp142PE5.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp142PE5.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']              = $qp142PE5.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']                = $qp142PE5.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']                     = $qp142PE5.adresse.adresseContact.mailAdresseDeclarant;

//signature
cerfaFields['date']                         = $qp142PE5.signature.signature.dateSignature;
cerfaFields['signature']                    = $qp142PE5.signature.signature.signature;
cerfaFields['lieuSignature']                = $qp142PE5.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']   = $qp142PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp142PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp142PE5.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['libelleProfession']				= "Géomètre expert"

// Courrier d'accompagnement

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp142PE5.signature.signature.dateSignature,
		autoriteHabilitee :"Ministère de l’écologie, du développement durable et de l’énergie",
		demandeContexte : "Déclaration préalable en vue d’une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});	
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
    .load('models/Geometre_expert.pdf') //
    .apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLPS);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDetailDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurancePro);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Geometre_expert_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Géomètre expert - déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de géomètre expert',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
