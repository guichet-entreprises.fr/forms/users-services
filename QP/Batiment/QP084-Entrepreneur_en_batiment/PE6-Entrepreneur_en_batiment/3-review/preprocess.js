var cerfaFields = {};

var civNomPrenom = $qp084PE6.identiteGroup.identite.civilite + ' ' + $qp084PE6.identiteGroup.identite.nomDeclarant + ' ' + $qp084PE6.identiteGroup.identite.prenomsDeclarant;

//etatCivil
cerfaFields['civiliteMadame']              = ($qp084PE6.identiteGroup.identite.civilite=='Madame');
cerfaFields['civiliteMonsieur']            = ($qp084PE6.identiteGroup.identite.civilite=='Monsieur');
cerfaFields['nomDeclarant']	               = $qp084PE6.identiteGroup.identite.nomDeclarant 
+($qp084PE6.identiteGroup.identite.nomJeuneFilleDeclarante != null ? ', née ' + $qp084PE6.identiteGroup.identite.nomJeuneFilleDeclarante : '');
cerfaFields['prenomsDeclarant']            = $qp084PE6.identiteGroup.identite.prenomsDeclarant;
cerfaFields['dateNaissanceDeclarant']      = $qp084PE6.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['lieuNaissanceDeclarant']      = $qp084PE6.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp084PE6.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp084PE6.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant  
+($qp084PE6.coordonneesGroup.coordonnees.complementAdresse != null ? ', '+ $qp084PE6.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['communeResidenceDeclarant']   = $qp084PE6.coordonneesGroup.coordonnees.communeResidenceDeclarant+', ' +$qp084PE6.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['codePostalDeclarant']         = $qp084PE6.coordonneesGroup.coordonnees.codePostalDeclarant;
cerfaFields['telephoneMobileDeclarant']    = $qp084PE6.coordonneesGroup.coordonnees.telephoneMobileDeclarant;

var adresse = $qp084PE6.coordonneesGroup.coordonnees.adresseCourriel;
	cerfaFields['adresseCourrielAvant']              = '';
	cerfaFields['adresseCourrielApres']              = '';
if(adresse != null) {
	var adresseSplite = adresse.split("@");
	cerfaFields['adresseCourrielAvant']              = adresseSplite[0];
	cerfaFields['adresseCourrielApres']              = adresseSplite[1];
}

//entreprise
cerfaFields['natureJuridiqueEntreprise']   = $qp084PE6.entrepriseGroup.entreprise.natureJuridiqueEntreprise;

//diplomesExperiencesProfessionnelles
cerfaFields['diplomeProfessionConsideree'] = 'Entrepreneur en bâtiment';

//signature
cerfaFields['dateDeclaration']             = $qp084PE6.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp084PE6.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp084PE6.signatureGroup.signature.signatureElectronique;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qpxxxPEx.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp084PE6.signatureGroup.signature.dateDeclaration,
		autoriteHabilitee :"Chambre des métiers et de l’artisanat",
		demandeContexte : "Demande de reconnaissance de qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Formulaire LE.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
 
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificatifExercice);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveQualifications);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Entrepreneur_en_batiment_RQP.pdf');

/*
 * Persistance des données obtenues
 */


return spec.create({
    id : 'review',
    label : 'Entrepreneur en bâtiment - Demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement',
    groups : [ spec.createGroup({
			id : 'generated',
			label : 'Génération du dossier',
			data : [ spec.createData({
					id : 'formulaire',
					label : 'Demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession d\'entrepreneur en bâtiment',
					description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
					type : 'FileReadOnly',
					value : [ finalDocItem ]
			}) ]
	}) ]
});