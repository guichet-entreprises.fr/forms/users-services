var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp085PE6.etatCivil.identificationDeclarant.civilite + ' ' + $qp085PE6.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp085PE6.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['civiliteNom']                      = $qp085PE6.etatCivil.identificationDeclarant.nomDeclarant;
cerfaFields['prenom']                           = $qp085PE6.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']               = $qp085PE6.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp085PE6.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                      = $qp085PE6.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']                    = $qp085PE6.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse']                          = $qp085PE6.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp085PE6.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp085PE6.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']                        = ($qp085PE6.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp085PE6.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp085PE6.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp085PE6.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']                  = $qp085PE6.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']                    = $qp085PE6.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']                         = $qp085PE6.adresse.adresseContact.mailAdresseDeclarant;

//profession
cerfaFields['naturePrestation']                  = $qp085PE6.profession.profession.naturePrestation;



//signature
cerfaFields['date']                             = $qp085PE6.signature.signature.dateSignature;
cerfaFields['lieuSignature']                    = $qp085PE6.signature.signature.lieuSignature;
cerfaFields['civiliteNomPrenomSignature']       = $qp085PE6.etatCivil.identificationDeclarant.civilite + ' ' + $qp085PE6.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp085PE6.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['engagementPrescription']            = $qp085PE6.signature.signature.engagementPrescription;
cerfaFields['engagementInformation']             = $qp085PE6.signature.signature.engagementInformation;
cerfaFields['declarationHonneur']                = $qp085PE6.signature.signature.declarationHonneur;
cerfaFields['civiliteNomPrenom']                 = $qp085PE6.etatCivil.identificationDeclarant.civilite + ' ' + $qp085PE6.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp085PE6.etatCivil.identificationDeclarant.prenomDeclarant;

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp085PE6.signature.signature.lieuSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp085PE6.signature.signature.dateSignature,
		autoriteHabilitee :"Ministère de la cohésion et des territoires",
		demandeContexte : "Demande de renouvellement de la demande d’agrément en vue d’exercer la profession de contrôleur technique de la construction ",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));E6.signature.signature.declarationHonneur;

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
    .load('models/Controleur_technique_de_la_construction_LPS_Renouv.pdf') //
    .apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveQualifications);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurancePro);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Controleur_technique_de_la_construction_LPS_Renouv.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Contrôleur technique de la construction - renouvellement de la déclaration préalable en vue d’une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de renouvellement de la demande d’agrément en vue d’exercer la profession de contrôleur technique de la construction',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
