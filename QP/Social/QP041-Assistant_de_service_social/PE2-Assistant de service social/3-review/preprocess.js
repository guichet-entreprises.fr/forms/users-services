var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp041PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp041PE2.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp041PE2.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']          	    = $qp041PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp041PE2.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp041PE2.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			 	= $qp041PE2.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp041PE2.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                		= $qp041PE2.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             		= $qp041PE2.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle

var regionExercice = $qp041PE2.adresse.regionExerciceGroup.regionExercice;

cerfaFields['adresse'] 							= $qp041PE2.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp041PE2.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp041PE2.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       					= ($qp041PE2.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp041PE2.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp041PE2.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp041PE2.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      			= $qp041PE2.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          			= $qp041PE2.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          				= $qp041PE2.adresse.adresseContact.mailAdresseDeclarant;
cerfaFields['regionExercice']               	= regionExercice;

//signature
cerfaFields['date']                				= $qp041PE2.signature.signature.dateSignature;
cerfaFields['signature']           				= $qp041PE2.signature.signature.signature;
cerfaFields['lieuSignature']                  	= $qp041PE2.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']		= $qp041PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp041PE2.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp041PE2.etatCivil.identificationDeclarant.prenomDeclarant;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */

var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp041PE2.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var accompDoc = nash.doc //
	.load('models/Courrier au premier dossier v1.6 LE.pdf') //
	.apply({
		date: $qp041PE2.signature.signature.dateSignature
	});

finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier libre LE V3.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitreFormation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCurriculum);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCourrier);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPieceDeclarant);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Assistant_service_social_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Assistant de service social - demande de reconnaissance de qualifications professionnelles',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'La démarche de demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession d\'assistant de service social est maintenant terminée.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
