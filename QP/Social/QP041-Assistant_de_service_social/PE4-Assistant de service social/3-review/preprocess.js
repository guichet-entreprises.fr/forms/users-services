var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp041PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp041PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp041PE4.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']          	    = $qp041PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp041PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp041PE4.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			 	= $qp041PE4.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp041PE4.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                		= $qp041PE4.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             		= $qp041PE4.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle

var regionExercice = $qp041PE4.adresse.regionExerciceGroup.regionExercice;

cerfaFields['adresse'] 							= $qp041PE4.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp041PE4.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp041PE4.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       					= ($qp041PE4.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp041PE4.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp041PE4.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp041PE4.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      			= $qp041PE4.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          			= $qp041PE4.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          				= $qp041PE4.adresse.adresseContact.mailAdresseDeclarant;
cerfaFields['regionExercice']               	= regionExercice;


//signature
cerfaFields['date']                				= $qp041PE4.signature.signature.dateSignature;
cerfaFields['signature']           				= $qp041PE4.signature.signature.signature;
cerfaFields['lieuSignature']                  	= $qp041PE4.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']		= $qp041PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp041PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp041PE4.etatCivil.identificationDeclarant.prenomDeclarant;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */

var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp041PE4.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var accompDoc = nash.doc //
	.load('models/Courrier au premier dossier v1.6 LPS.pdf') //
	.apply({
		date: $qp041PE4.signature.signature.dateSignature
	});

finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier libre LPS V3.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitreFormation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurance);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Assistant_service_social_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Assistant de service social - déclaration de libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'La déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession d\'assistant de service social est maintenant terminée.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
